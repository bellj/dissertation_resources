package com.whatsapp.mentions;

import X.AbstractC14640lm;
import X.AbstractC14780m2;
import X.AbstractC28571Oc;
import X.AbstractC37001l6;
import X.AbstractC37031lC;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass01V;
import X.AnonymousClass01d;
import X.AnonymousClass13H;
import X.AnonymousClass1JO;
import X.AnonymousClass1MW;
import X.AnonymousClass1Y6;
import X.AnonymousClass1YO;
import X.AnonymousClass1lB;
import X.AnonymousClass2xg;
import X.AnonymousClass3ME;
import X.AnonymousClass5UP;
import X.C15380n4;
import X.C15550nR;
import X.C15580nU;
import X.C15600nX;
import X.C16630pM;
import X.C27631Ih;
import X.C52442ao;
import X.C624037c;
import X.C63873Dg;
import X.C64753Gt;
import X.C73533gO;
import X.C73543gP;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.core.view.inputmethod.InputConnectionCompat;
import androidx.core.view.inputmethod.InputContentInfoCompat;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.mentions.MentionableEntry;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class MentionableEntry extends AbstractC37001l6 implements AnonymousClass1lB, AbstractC28571Oc, AbstractC37031lC {
    public static final String[] A0K = C64753Gt.A01;
    public int A00;
    public int A01;
    public int A02;
    public Bundle A03;
    public View A04;
    public View A05;
    public ViewGroup A06;
    public C15550nR A07;
    public C15600nX A08;
    public C15580nU A09;
    public AnonymousClass1lB A0A;
    public MentionPickerView A0B;
    public C73533gO A0C;
    public AnonymousClass5UP A0D;
    public AnonymousClass13H A0E;
    public C16630pM A0F;
    public boolean A0G;
    public boolean A0H;
    public final TextWatcher A0I = new AnonymousClass3ME(this);
    public final C63873Dg A0J = new C63873Dg();

    public MentionableEntry(Context context) {
        super(context);
    }

    public MentionableEntry(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MentionableEntry(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final int A06(Editable editable, int i) {
        int lastIndexOf = editable.toString().substring(i, getSelectionEnd()).lastIndexOf("@");
        for (C73533gO r0 : (C73533gO[]) editable.getSpans(lastIndexOf, lastIndexOf + 1, C73533gO.class)) {
            if (r0.A00) {
                return -1;
            }
        }
        return lastIndexOf;
    }

    public final String A07(int i, int i2) {
        Editable newEditable = Editable.Factory.getInstance().newEditable(getText().subSequence(i, i2));
        C73543gP[] r5 = (C73543gP[]) newEditable.getSpans(0, newEditable.length(), C73543gP.class);
        for (C73543gP r3 : r5) {
            newEditable.replace(newEditable.getSpanStart(r3) - 1, newEditable.getSpanEnd(r3), r3.A01);
        }
        return newEditable.toString();
    }

    public void A08() {
        removeTextChangedListener(this.A0I);
        setText((String) null);
        setCursorVisible(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        if (r1 != '_') goto L_0x0051;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A09(android.text.Editable r8) {
        /*
            r7 = this;
            r0 = 0
            int r3 = r7.A06(r8, r0)
            int r2 = r3 + 1
            int r1 = r7.getSelectionEnd()
            java.lang.Class<X.3gP> r0 = X.C73543gP.class
            java.lang.Object[] r6 = r8.getSpans(r2, r1, r0)
            X.3gP[] r6 = (X.C73543gP[]) r6
            int r5 = r6.length
            if (r5 <= 0) goto L_0x0032
            r4 = 0
            r3 = 0
        L_0x0018:
            r2 = r6[r4]
            int r1 = r8.getSpanEnd(r2)
            if (r1 <= r3) goto L_0x002a
            int r0 = r7.getSelectionEnd()
            if (r1 > r0) goto L_0x002a
            int r3 = r8.getSpanEnd(r2)
        L_0x002a:
            int r4 = r4 + 1
            if (r4 < r5) goto L_0x0018
            int r3 = r7.A06(r8, r3)
        L_0x0032:
            if (r3 < 0) goto L_0x0047
            r0 = 1
            if (r3 == 0) goto L_0x0051
            int r0 = r3 - r0
            char r1 = r8.charAt(r0)
            boolean r0 = java.lang.Character.isLetterOrDigit(r1)
            if (r0 != 0) goto L_0x0047
            r0 = 95
            if (r1 != r0) goto L_0x0051
        L_0x0047:
            X.3gO r0 = r7.A0C
            r7.A0C(r0)
            r0 = 0
            r7.A0E(r0)
        L_0x0050:
            return
        L_0x0051:
            java.lang.String r2 = r8.toString()
            int r1 = r3 + 1
            int r0 = r7.getSelectionEnd()
            java.lang.String r0 = r2.substring(r1, r0)
            r7.A0E(r0)
            boolean r0 = r7.A0H
            if (r0 == 0) goto L_0x0050
            r7.A0A(r8, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.whatsapp.mentions.MentionableEntry.A09(android.text.Editable):void");
    }

    public final void A0A(Editable editable, int i) {
        int i2 = i + 1;
        if (((C73533gO[]) editable.getSpans(i, i2, C73533gO.class)).length < 1) {
            A0C(this.A0C);
            C73533gO r1 = new C73533gO(this.A00, false);
            this.A0C = r1;
            editable.setSpan(r1, i, i2, 33);
        }
    }

    public final void A0B(SpannableStringBuilder spannableStringBuilder, Collection collection, boolean z) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC14640lm r2 = (AbstractC14640lm) it.next();
            if (r2 != null) {
                AnonymousClass13H r1 = this.A0E;
                AnonymousClass009.A05(r1);
                C15550nR r0 = this.A07;
                AnonymousClass009.A05(r0);
                String A01 = r1.A01(r0.A0B(r2));
                StringBuilder sb = new StringBuilder("@");
                String str = r2.user;
                AnonymousClass009.A05(str);
                sb.append(str);
                String obj = sb.toString();
                int indexOf = TextUtils.indexOf(spannableStringBuilder, obj);
                if (indexOf < 0) {
                    StringBuilder sb2 = new StringBuilder("unable to set mention for ");
                    sb2.append(r2);
                    sb2.append(" in ");
                    sb2.append((Object) spannableStringBuilder);
                    Log.w(sb2.toString());
                } else {
                    do {
                        StringBuilder sb3 = new StringBuilder("@");
                        sb3.append(A01);
                        spannableStringBuilder.replace(indexOf, obj.length() + indexOf, sb3.toString());
                        if (z) {
                            C73533gO r4 = new C73533gO(this.A00, true);
                            int i = indexOf + 1;
                            spannableStringBuilder.setSpan(r4, indexOf, i, 33);
                            spannableStringBuilder.setSpan(new C73543gP(r4, obj, this.A01), i, A01.length() + i, 33);
                        }
                        indexOf = TextUtils.indexOf(spannableStringBuilder, obj, indexOf + 1);
                    } while (indexOf >= 0);
                }
            }
        }
    }

    public final void A0C(ForegroundColorSpan foregroundColorSpan) {
        if (foregroundColorSpan != null) {
            getText().removeSpan(foregroundColorSpan);
        }
    }

    public void A0D(ViewGroup viewGroup, C15580nU r5, boolean z, boolean z2, boolean z3) {
        this.A09 = r5;
        addTextChangedListener(this.A0I);
        Context context = getContext();
        int i = R.color.link_color;
        if (z) {
            i = R.color.link_color_on_dark;
        }
        this.A01 = AnonymousClass00T.A00(context, i);
        Context context2 = getContext();
        int i2 = R.color.mention_annotation_on_white;
        if (z) {
            i2 = R.color.mention_annotation_on_white_on_dark;
        }
        this.A00 = AnonymousClass00T.A00(context2, i2);
        A09(getText());
        this.A06 = viewGroup;
        Bundle bundle = new Bundle();
        this.A03 = bundle;
        bundle.putString("ARG_GID", C15380n4.A03(r5));
        this.A03.putBoolean("ARG_IS_DARK_THEME", z);
        this.A03.putBoolean("ARG_HIDE_END_DIVIDER", z2);
        this.A03.putBoolean("ARG_WITH_BACKGROUND", z3);
    }

    public final void A0E(String str) {
        if (this.A06 == null) {
            return;
        }
        if (str != null) {
            MentionPickerView mentionPickerView = this.A0B;
            if (mentionPickerView == null) {
                MentionPickerView mentionPickerView2 = (MentionPickerView) LayoutInflater.from(getContext()).inflate(R.layout.mentions_list, this.A06, false);
                this.A0B = mentionPickerView2;
                this.A06.addView(mentionPickerView2);
                this.A0B.setup(this, this.A03);
                View view = this.A05;
                if (view != null) {
                    this.A0B.setAnchorWidthView(view);
                }
                View view2 = this.A04;
                if (view2 != null) {
                    ((AnonymousClass2xg) this.A0B).A03 = view2;
                }
                mentionPickerView = this.A0B;
                mentionPickerView.A0A = this;
            }
            if (mentionPickerView.A0F) {
                mentionPickerView.A0C.getFilter().filter(str);
            } else {
                mentionPickerView.A0E.Aaz(new C624037c(mentionPickerView.A06, mentionPickerView.A09, mentionPickerView, str), mentionPickerView.A08);
            }
        } else {
            MentionPickerView mentionPickerView3 = this.A0B;
            if (mentionPickerView3 != null) {
                mentionPickerView3.A0C.getFilter().filter(null);
            }
        }
    }

    @Override // X.AbstractC28571Oc
    public void A5j(AbstractC14780m2 r2) {
        this.A0J.A01(r2);
    }

    @Override // X.AnonymousClass1lB
    public void ANo(boolean z) {
        int A06;
        this.A0H = z;
        AnonymousClass1lB r0 = this.A0A;
        if (r0 != null) {
            r0.ANo(z);
        }
        if (!z || (A06 = A06(getEditableText(), 0)) < 0) {
            A0C(this.A0C);
            this.A0C = null;
            return;
        }
        A0A(getEditableText(), A06);
    }

    @Override // android.view.View
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        this.A0J.A00();
    }

    public List getMentions() {
        HashSet hashSet = new HashSet();
        for (C73543gP r0 : (C73543gP[]) getText().getSpans(0, getText().length(), C73543gP.class)) {
            try {
                hashSet.add(C27631Ih.A02(r0.A01.substring(1)));
            } catch (AnonymousClass1MW unused) {
            }
        }
        return new ArrayList(hashSet);
    }

    public String getStringText() {
        return A07(0, getText().length());
    }

    @Override // X.AnonymousClass1l9, com.whatsapp.WaEditText, X.AnonymousClass011, android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        String[] strArr = A0K;
        if (strArr.length <= 0) {
            return onCreateInputConnection;
        }
        EditorInfoCompat.setContentMimeTypes(editorInfo, strArr);
        return InputConnectionCompat.createWrapper(onCreateInputConnection, editorInfo, new InputConnectionCompat.OnCommitContentListener() { // from class: X.4rt
            @Override // androidx.core.view.inputmethod.InputConnectionCompat.OnCommitContentListener
            public final boolean onCommitContent(InputContentInfoCompat inputContentInfoCompat, int i, Bundle bundle) {
                AnonymousClass5UP r0 = MentionableEntry.this.A0D;
                if (r0 != null) {
                    return r0.onCommitContent(inputContentInfoCompat, i, bundle);
                }
                Log.e("mentionable/entry/no on commit content listener");
                return false;
            }
        });
    }

    @Override // android.widget.TextView, android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        C52442ao r6 = (C52442ao) parcelable;
        super.onRestoreInstanceState(r6.getSuperState());
        String str = r6.A00;
        if (!TextUtils.isEmpty(str)) {
            String str2 = r6.A01;
            if (!TextUtils.isEmpty(str2)) {
                int selectionStart = getSelectionStart();
                int selectionEnd = getSelectionEnd();
                AnonymousClass009.A05(str2);
                setMentionableText(str2, AnonymousClass1Y6.A01(str));
                setSelection(selectionStart, selectionEnd);
            }
        }
    }

    @Override // android.widget.TextView, android.view.View
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        AnonymousClass009.A05(onSaveInstanceState);
        return new C52442ao(onSaveInstanceState, getStringText(), AnonymousClass1Y6.A00(getMentions()));
    }

    @Override // android.widget.TextView
    public void onSelectionChanged(int i, int i2) {
        super.onSelectionChanged(i, i2);
        Editable editableText = getEditableText();
        if (!TextUtils.isEmpty(editableText)) {
            C73543gP[] r6 = (C73543gP[]) editableText.getSpans(i, i, C73543gP.class);
            for (C73543gP r1 : r6) {
                int spanStart = editableText.getSpanStart(r1) - 1;
                int spanEnd = editableText.getSpanEnd(r1);
                i = spanEnd;
                if (i <= ((spanStart + spanEnd) >> 1)) {
                    i = spanStart;
                }
            }
            C73543gP[] r62 = (C73543gP[]) editableText.getSpans(i2, i2, C73543gP.class);
            for (C73543gP r12 : r62) {
                int spanStart2 = editableText.getSpanStart(r12) - 1;
                int spanEnd2 = editableText.getSpanEnd(r12);
                i2 = spanEnd2;
                if (i2 <= ((spanStart2 + spanEnd2) >> 1)) {
                    i2 = spanStart2;
                }
            }
            setSelection(i, i2);
        }
    }

    @Override // X.C37011l7, com.whatsapp.WaEditText, X.AnonymousClass011, android.widget.TextView
    public boolean onTextContextMenuItem(int i) {
        int i2;
        CharSequence charSequence;
        int length = getText().length();
        if (isFocused()) {
            int selectionStart = getSelectionStart();
            int selectionEnd = getSelectionEnd();
            i2 = Math.max(0, Math.min(selectionStart, selectionEnd));
            length = Math.max(0, Math.max(selectionStart, selectionEnd));
        } else {
            i2 = 0;
        }
        if (i == 16908322) {
            if (this.A09 != null) {
                AnonymousClass01d r0 = ((WaEditText) this).A02;
                AnonymousClass009.A05(r0);
                ClipboardManager A0B = r0.A0B();
                if (A0B == null) {
                    Log.w("mentionableentry/on-text-context-menu-item cm=null");
                } else {
                    ClipData primaryClip = A0B.getPrimaryClip();
                    if (!(primaryClip == null || primaryClip.getItemCount() == 0)) {
                        ClipData.Item itemAt = primaryClip.getItemAt(0);
                        if (itemAt != null) {
                            charSequence = itemAt.getText();
                        } else {
                            charSequence = "";
                        }
                        SharedPreferences A01 = this.A0F.A01(AnonymousClass01V.A07);
                        String string = A01.getString("copied_message", "");
                        String string2 = A01.getString("copied_message_jids", "");
                        String string3 = A01.getString("copied_message_without_mentions", "");
                        if (!TextUtils.isEmpty(charSequence) && TextUtils.equals(charSequence, string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                            List A012 = AnonymousClass1Y6.A01(string2);
                            HashSet hashSet = new HashSet(A012);
                            HashSet hashSet2 = new HashSet(A012);
                            C15580nU r1 = this.A09;
                            if (r1 != null) {
                                C15600nX r02 = this.A08;
                                AnonymousClass009.A05(r02);
                                AnonymousClass1JO A07 = r02.A02(r1).A07();
                                HashSet hashSet3 = new HashSet();
                                Iterator it = A07.iterator();
                                while (it.hasNext()) {
                                    hashSet3.add(((AnonymousClass1YO) it.next()).A03);
                                }
                                hashSet.retainAll(hashSet3);
                                hashSet2.removeAll(hashSet3);
                            } else {
                                hashSet2 = null;
                            }
                            AnonymousClass009.A05(string3);
                            if (this.A0H) {
                                A0E(null);
                            }
                            A0C(this.A0C);
                            this.A0C = null;
                            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string3);
                            A0B(spannableStringBuilder, hashSet, true);
                            if (hashSet2 != null) {
                                A0B(spannableStringBuilder, hashSet2, false);
                            }
                            getText().replace(i2, length, spannableStringBuilder);
                            return true;
                        }
                    }
                }
            }
        } else if (i == 16908320 || i == 16908321) {
            this.A0F.A01(AnonymousClass01V.A07).edit().putString("copied_message_without_mentions", A07(i2, length)).putString("copied_message", getText().subSequence(i2, length).toString()).putString("copied_message_jids", AnonymousClass1Y6.A00(getMentions())).apply();
        }
        return super.onTextContextMenuItem(i);
    }

    public void setMentionPickerVisibilityChangeListener(AnonymousClass1lB r1) {
        this.A0A = r1;
    }

    public void setMentionableText(String str, Collection collection) {
        if (collection == null || collection.isEmpty()) {
            setText(str);
            return;
        }
        if (this.A0H) {
            A0E(null);
        }
        A0C(this.A0C);
        this.A0C = null;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        A0B(spannableStringBuilder, collection, true);
        setText(spannableStringBuilder);
    }

    public void setOnCommitContentListener(AnonymousClass5UP r1) {
        this.A0D = r1;
    }

    public void setText(String str) {
        C73543gP[] r3 = (C73543gP[]) getText().getSpans(0, getText().length(), C73543gP.class);
        for (C73543gP r1 : r3) {
            A0C(r1.A00);
            A0C(r1);
        }
        A0C(this.A0C);
        this.A0C = null;
        super.setText((CharSequence) str);
    }
}
