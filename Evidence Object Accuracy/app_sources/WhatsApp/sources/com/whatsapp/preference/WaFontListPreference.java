package com.whatsapp.preference;

import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes3.dex */
public class WaFontListPreference extends WaListPreference {
    public static int A00;

    public WaFontListPreference(Context context) {
        super(context);
    }

    public WaFontListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
