package com.whatsapp.preference;

import X.AnonymousClass0FF;
import X.C12960it;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.preference.Preference;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WaPreference extends Preference {
    public WaPreference(Context context) {
        super(context);
    }

    public WaPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WaPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public static void A00(View view) {
        View findViewById = view.findViewById(16908304);
        if (findViewById != null && (findViewById instanceof TextView)) {
            ((TextView) findViewById).setTextColor(C12960it.A09(view).getColorStateList(R.color.secondary_text_selector));
        }
    }

    public static void A01(View view) {
        View findViewById = view.findViewById(16908310);
        if (findViewById instanceof TextView) {
            ((TextView) findViewById).setTextColor(C12960it.A09(view).getColorStateList(R.color.palette_onsurface_primary_color_selector));
        }
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r2) {
        super.A0R(r2);
        A01(r2.A0H);
    }
}
