package com.whatsapp.preference;

import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes3.dex */
public class WaLanguageListPreference extends WaListPreference {
    public WaLanguageListPreference(Context context) {
        super(context);
    }

    public WaLanguageListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
