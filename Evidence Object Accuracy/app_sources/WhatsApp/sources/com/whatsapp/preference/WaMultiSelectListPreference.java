package com.whatsapp.preference;

import android.content.Context;
import android.util.AttributeSet;

/* loaded from: classes3.dex */
public class WaMultiSelectListPreference extends WaListPreference {
    public WaMultiSelectListPreference(Context context) {
        super(context, null);
    }

    public WaMultiSelectListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
