package com.whatsapp.preference;

import X.AnonymousClass0FF;
import X.C12990iw;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import androidx.preference.Preference;

/* loaded from: classes2.dex */
public class WaRingtonePreference extends Preference {
    public int A00;
    public String A01;
    public boolean A02;
    public boolean A03;

    public WaRingtonePreference(Context context) {
        super(context);
    }

    public WaRingtonePreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WaRingtonePreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r2) {
        super.A0R(r2);
        View view = r2.A0H;
        WaPreference.A01(view);
        WaPreference.A00(view);
    }

    public Intent A0S() {
        Uri parse;
        Intent A0E = C12990iw.A0E("android.intent.action.RINGTONE_PICKER");
        if (TextUtils.isEmpty(this.A01)) {
            parse = null;
        } else {
            parse = Uri.parse(this.A01);
        }
        A0E.putExtra("android.intent.extra.ringtone.EXISTING_URI", parse);
        A0E.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", this.A02);
        if (this.A02) {
            A0E.putExtra("android.intent.extra.ringtone.DEFAULT_URI", RingtoneManager.getDefaultUri(this.A00));
        }
        A0E.putExtra("android.intent.extra.ringtone.SHOW_SILENT", this.A03);
        A0E.putExtra("android.intent.extra.ringtone.TYPE", this.A00);
        A0E.putExtra("android.intent.extra.ringtone.TITLE", this.A0H);
        if (this.A02) {
            int i = this.A00;
            if (!((i & 1) == 0 || (i & 2) == 0)) {
                A0E.putExtra("android.intent.extra.ringtone.DEFAULT_URI", Settings.System.DEFAULT_NOTIFICATION_URI);
            }
        }
        if (Build.MANUFACTURER.equalsIgnoreCase("Xiaomi")) {
            A0E.putExtra("android.intent.extra.ringtone.TYPE", 2);
        }
        Intent A0E2 = C12990iw.A0E("android.intent.action.CHOOSER");
        A0E2.putExtra("android.intent.extra.INTENT", A0E);
        return A0E2;
    }
}
