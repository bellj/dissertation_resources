package com.whatsapp.preference;

import X.AnonymousClass00T;
import X.AnonymousClass0FF;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import com.whatsapp.R;

/* loaded from: classes3.dex */
public class WaPreferenceCategory extends PreferenceCategory {
    public WaPreferenceCategory(Context context) {
        super(context);
    }

    public WaPreferenceCategory(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WaPreferenceCategory(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // androidx.preference.PreferenceCategory, androidx.preference.Preference
    public void A0R(AnonymousClass0FF r4) {
        super.A0R(r4);
        View findViewById = r4.A0H.findViewById(16908310);
        if (findViewById != null && (findViewById instanceof TextView)) {
            ((TextView) findViewById).setTextColor(AnonymousClass00T.A00(((Preference) this).A05, R.color.settings_title_accent));
        }
    }
}
