package com.whatsapp.preference;

import X.AnonymousClass0FF;
import X.AnonymousClass2GE;
import X.C12960it;
import X.C12970iu;
import X.C41691tw;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class ChatHistoryPreference extends WaPreference {
    public int A00;
    public int A01;

    public ChatHistoryPreference(Context context) {
        this(context, null);
    }

    public ChatHistoryPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ChatHistoryPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A00 = C41691tw.A00(context, R.attr.settingsIconColor, R.color.settings_icon);
        this.A01 = C41691tw.A00(context, R.attr.settingsTitleTextColor, R.color.settings_item_title_text);
    }

    @Override // com.whatsapp.preference.WaPreference, androidx.preference.Preference
    public void A0R(AnonymousClass0FF r4) {
        super.A0R(r4);
        View view = r4.A0H;
        AnonymousClass2GE.A07(C12970iu.A0L(view, 16908294), this.A00);
        C12960it.A0J(view, 16908310).setTextColor(this.A01);
    }
}
