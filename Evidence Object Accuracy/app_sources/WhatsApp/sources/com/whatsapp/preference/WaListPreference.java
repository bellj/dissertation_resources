package com.whatsapp.preference;

import X.AnonymousClass0FF;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.preference.DialogPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class WaListPreference extends ListPreference {
    public WaListPreference(Context context) {
        super(context);
    }

    public WaListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // androidx.preference.Preference
    public void A0R(AnonymousClass0FF r3) {
        super.A0R(r3);
        ((DialogPreference) this).A04 = ((Preference) this).A05.getString(R.string.cancel);
        View view = r3.A0H;
        WaPreference.A01(view);
        WaPreference.A00(view);
    }
}
