package com.whatsapp.audioRecording;

import android.media.AudioRecord;

/* loaded from: classes2.dex */
public class AudioRecordFactory {
    public AudioRecord createAudioRecord(int i, int i2) {
        return new AudioRecord(0, i, 16, 2, i2);
    }
}
