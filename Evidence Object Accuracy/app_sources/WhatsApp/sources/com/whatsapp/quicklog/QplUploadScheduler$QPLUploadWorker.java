package com.whatsapp.quicklog;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass01d;
import X.AnonymousClass029;
import X.AnonymousClass042;
import X.AnonymousClass043;
import X.AnonymousClass0GL;
import X.AnonymousClass15Z;
import X.AnonymousClass1O1;
import X.AnonymousClass23S;
import X.AnonymousClass2BK;
import X.AnonymousClass2BL;
import X.C006503b;
import X.C14350lI;
import X.C20470vo;
import X.C251718j;
import X.C28471Ni;
import X.C68943Xi;
import android.app.ActivityManager;
import android.content.Context;
import android.net.TrafficStats;
import android.os.Build;
import android.os.ConditionVariable;
import android.telephony.TelephonyManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class QplUploadScheduler$QPLUploadWorker extends Worker {
    public final C251718j A00;

    public QplUploadScheduler$QPLUploadWorker(Context context, WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.A00 = (C251718j) ((AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class)).AH9.get();
    }

    @Override // androidx.work.Worker
    public AnonymousClass043 A04() {
        boolean z;
        AnonymousClass043 r2;
        String str;
        C251718j r1 = this.A00;
        AnonymousClass15Z r0 = r1.A03;
        try {
            z = r0.A05.tryAcquire(5, TimeUnit.SECONDS);
        } catch (InterruptedException unused) {
            z = false;
        }
        if (!z) {
            return new AnonymousClass042();
        }
        try {
            r1.A00 = false;
            File[] A01 = r0.A01(".txt");
            long currentTimeMillis = System.currentTimeMillis() - AnonymousClass15Z.A07;
            for (int i = 0; i < A01.length; i++) {
                if (A01[i].lastModified() < currentTimeMillis) {
                    r0.A00(A01[i]);
                }
            }
            File[] A012 = r0.A01(".txt");
            File file = new File(r0.A01.A00.getCacheDir(), "qpl");
            ArrayList arrayList = new ArrayList();
            for (File file2 : A012) {
                try {
                    File A04 = C14350lI.A04(file2, file, file2.getName());
                    if (A04 != null) {
                        arrayList.add(A04);
                    }
                } catch (IOException e) {
                    r0.A04.A9a(e.getMessage());
                }
            }
            File[] fileArr = (File[]) arrayList.toArray(new File[0]);
            int length = fileArr.length;
            if (length == 0) {
                r1.A06.A01.A00.edit().putLong("qpl_last_upload_ts", System.currentTimeMillis()).apply();
                r2 = new AnonymousClass0GL(C006503b.A01);
            } else {
                try {
                    ConditionVariable conditionVariable = new ConditionVariable();
                    C68943Xi r3 = new C68943Xi(conditionVariable, r1);
                    TrafficStats.setThreadStatsTag(17);
                    C28471Ni r15 = new C28471Ni(r1.A01, r3, r1.A07, "https://graph.whatsapp.net/wa_qpl_data", r1.A08.A00(), 8, false, false, false);
                    r15.A06("access_token", "1063127757113399|745146ffa34413f9dbb5469f5370b7af");
                    C20470vo r7 = r1.A04;
                    r15.A06("app_id", AnonymousClass029.A0B);
                    for (File file3 : fileArr) {
                        try {
                            r15.A0A.add(new AnonymousClass23S(new FileInputStream(file3), "batches[]", file3.getName(), 0, 0, file3.length()));
                        } catch (FileNotFoundException e2) {
                            r1.A05.A9d(e2.getMessage());
                        }
                    }
                    r15.A06("upload_time", String.valueOf(System.currentTimeMillis()));
                    r15.A06("user_id", String.valueOf(r7.A06.A00()));
                    try {
                        JSONObject jSONObject = new JSONObject();
                        AnonymousClass01d r13 = r7.A00;
                        TelephonyManager A0N = r13.A0N();
                        if (A0N != null && A0N.getPhoneType() == 1) {
                            jSONObject.put("carrier", A0N.getNetworkOperatorName());
                            jSONObject.put("country", A0N.getSimCountryIso());
                        }
                        StringBuilder sb = new StringBuilder();
                        String str2 = Build.MANUFACTURER;
                        sb.append(str2);
                        sb.append("-");
                        String str3 = Build.MODEL;
                        sb.append(str3);
                        jSONObject.put("device_name", sb.toString());
                        jSONObject.put("device_code_name", Build.DEVICE);
                        jSONObject.put("device_manufacturer", str2);
                        jSONObject.put("device_model", str3);
                        jSONObject.put("year_class", AnonymousClass2BK.A02(r13, r7.A04));
                        int i2 = AnonymousClass1O1.A00;
                        if (i2 == -1) {
                            ActivityManager A03 = r13.A03();
                            if (A03 == null) {
                                Log.w("memoryclassprovider am=null");
                                i2 = 16;
                            } else {
                                AnonymousClass1O1.A00 = A03.getMemoryClass();
                                i2 = A03.getMemoryClass();
                            }
                        }
                        jSONObject.put("mem_class", i2);
                        jSONObject.put("device_os_version", Build.VERSION.RELEASE);
                        jSONObject.put("is_employee", false);
                        jSONObject.put("oc_version", AnonymousClass2BL.A00(r7.A01.A00));
                        str = jSONObject.toString();
                    } catch (Exception e3) {
                        r7.A05.AKL(-1, e3.getMessage());
                        str = null;
                    }
                    r15.A06("batch_info", str);
                    r15.A02(null);
                    conditionVariable.block(100000);
                } catch (Exception | OutOfMemoryError e4) {
                    r1.A05.A9d(e4.getMessage());
                    r1.A00 = false;
                }
                TrafficStats.clearThreadStatsTag();
                for (File file4 : fileArr) {
                    r0.A00(file4);
                }
                if (!r1.A00) {
                    r2 = new AnonymousClass042();
                } else {
                    for (File file5 : A012) {
                        r0.A00(file5);
                    }
                    r1.A06.A01.A00.edit().putLong("qpl_last_upload_ts", System.currentTimeMillis()).apply();
                    r2 = new AnonymousClass0GL(C006503b.A01);
                }
            }
            return r2;
        } finally {
            r0.A05.release();
        }
    }
}
