package com.whatsapp;

import X.AbstractC14640lm;
import X.ActivityC000900k;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass018;
import X.AnonymousClass19Z;
import X.AnonymousClass1SF;
import X.AnonymousClass27Q;
import X.C004802e;
import X.C015607k;
import X.C12960it;
import X.C12980iv;
import X.C14820m6;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C15600nX;
import X.C28141Kv;
import X.DialogC53322dr;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.CallConfirmationFragment;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.quickcontact.QuickContactActivity;
import java.util.List;

/* loaded from: classes2.dex */
public class CallConfirmationFragment extends Hilt_CallConfirmationFragment {
    public C15570nT A00;
    public C15550nR A01;
    public C14820m6 A02;
    public AnonymousClass018 A03;
    public C15600nX A04;
    public AnonymousClass19Z A05;
    public boolean A06 = false;
    public final List A07 = C12960it.A0l();

    @Override // androidx.fragment.app.DialogFragment
    public Dialog A1A(Bundle bundle) {
        Dialog A0L;
        ActivityC000900k A0C = A0C();
        boolean z = A03().getBoolean("is_video_call");
        AbstractC14640lm A01 = AbstractC14640lm.A01(A03().getString("jid"));
        AnonymousClass009.A05(A01);
        C15370n3 A0B = this.A01.A0B(A01);
        if (A0B.A0K()) {
            A0L = new DialogC53322dr(A0C, 0);
            A0L.setContentView(R.layout.call_group_confirmation_bottom_sheet);
            TextView textView = (TextView) A0L.findViewById(R.id.call_button);
            if (textView != null) {
                int i = R.drawable.ic_btn_call_audio;
                if (z) {
                    i = R.drawable.ic_btn_call_video;
                }
                Drawable A04 = AnonymousClass00T.A04(A0C, i);
                if (A04 != null) {
                    A04 = C015607k.A03(A04);
                    C015607k.A0A(A04, AnonymousClass00T.A00(A0C, R.color.audio_video_bottom_sheet_icon_color));
                }
                if (C28141Kv.A01(this.A03)) {
                    textView.setCompoundDrawablesWithIntrinsicBounds(A04, (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, A04, (Drawable) null);
                }
                textView.setOnClickListener(new View.OnClickListener(A0C, this, A0B, z) { // from class: X.3le
                    public final /* synthetic */ Activity A00;
                    public final /* synthetic */ CallConfirmationFragment A01;
                    public final /* synthetic */ C15370n3 A02;
                    public final /* synthetic */ boolean A03;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                        this.A02 = r3;
                        this.A03 = r4;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        CallConfirmationFragment callConfirmationFragment = this.A01;
                        callConfirmationFragment.A1K(this.A00, this.A02, this.A03);
                        callConfirmationFragment.A1C();
                    }
                });
            }
            View findViewById = A0L.findViewById(R.id.design_bottom_sheet);
            if (findViewById != null) {
                findViewById.setBackgroundResource(R.drawable.rounded_bottom_sheet_dialog);
            }
        } else {
            C004802e A0S = C12980iv.A0S(A0C);
            int i2 = R.string.audio_call_confirmation_text;
            if (z) {
                i2 = R.string.video_call_confirmation_text;
            }
            A0S.A06(i2);
            A0L = C12960it.A0L(new DialogInterface.OnClickListener(A0C, this, A0B, z) { // from class: X.3Kn
                public final /* synthetic */ Activity A00;
                public final /* synthetic */ CallConfirmationFragment A01;
                public final /* synthetic */ C15370n3 A02;
                public final /* synthetic */ boolean A03;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                    this.A02 = r3;
                    this.A03 = r4;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    CallConfirmationFragment callConfirmationFragment = this.A01;
                    Activity activity = this.A00;
                    C15370n3 r4 = this.A02;
                    boolean z2 = this.A03;
                    int A012 = C12970iu.A01(callConfirmationFragment.A02.A00, "call_confirmation_dialog_count");
                    C12960it.A0u(callConfirmationFragment.A02.A00, "call_confirmation_dialog_count", A012 + 1);
                    callConfirmationFragment.A1K(activity, r4, z2);
                }
            }, A0S, R.string.call);
        }
        A0L.setCanceledOnTouchOutside(true);
        if (A0C instanceof AnonymousClass27Q) {
            this.A07.add(A0C);
        }
        return A0L;
    }

    public final void A1K(Activity activity, C15370n3 r11, boolean z) {
        int i = A03().getInt("call_from_ui");
        this.A05.A03(activity, (GroupJid) r11.A0B(C15580nU.class), AnonymousClass1SF.A0C(this.A00, this.A01, this.A04, r11), i, z);
        this.A06 = true;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.A06) {
            for (AnonymousClass27Q r1 : this.A07) {
                ((QuickContactActivity) r1).A2g(false);
            }
        }
        this.A07.clear();
    }
}
