package com.caverock.androidsvg;

import X.AnonymousClass0AH;
import X.AnonymousClass0AJ;
import X.AnonymousClass0Q5;
import X.AnonymousClass0SI;
import X.C04490Lx;
import X.C06560Ud;
import X.C11160fq;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;

/* loaded from: classes.dex */
public class SVGImageView extends ImageView {
    public static Method A02;
    public AnonymousClass0SI A00 = new AnonymousClass0SI();
    public AnonymousClass0Q5 A01 = null;

    static {
        try {
            A02 = View.class.getMethod("setLayerType", Integer.TYPE, Paint.class);
        } catch (NoSuchMethodException unused) {
        }
    }

    public SVGImageView(Context context) {
        super(context);
    }

    public SVGImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        A01(attributeSet, 0);
    }

    public SVGImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01(attributeSet, i);
    }

    public final void A00() {
        AnonymousClass0Q5 r1 = this.A01;
        if (r1 != null) {
            Picture A00 = r1.A00(this.A00);
            Method method = A02;
            if (method != null) {
                try {
                    method.invoke(this, Integer.valueOf(View.class.getField("LAYER_TYPE_SOFTWARE").getInt(new View(getContext()))), null);
                } catch (Exception e) {
                    Log.w("SVGImageView", "Unexpected failure calling setLayerType", e);
                }
            }
            setImageDrawable(new PictureDrawable(A00));
        }
    }

    public final void A01(AttributeSet attributeSet, int i) {
        if (!isInEditMode()) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, C04490Lx.A00, i, 0);
            try {
                String string = obtainStyledAttributes.getString(0);
                if (string != null) {
                    this.A00.A00(string);
                }
                int resourceId = obtainStyledAttributes.getResourceId(1, -1);
                if (resourceId != -1) {
                    setImageResource(resourceId);
                } else {
                    String string2 = obtainStyledAttributes.getString(1);
                    if (string2 != null) {
                        try {
                            try {
                                new AnonymousClass0AH(this).execute(getContext().getContentResolver().openInputStream(Uri.parse(string2)));
                            } catch (FileNotFoundException unused) {
                                new AnonymousClass0AH(this).execute(getContext().getAssets().open(string2));
                            }
                        } catch (IOException unused2) {
                            setFromString(string2);
                        }
                    }
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }

    public void setCSS(String str) {
        this.A00.A00(str);
        A00();
    }

    private void setFromString(String str) {
        try {
            this.A01 = new C06560Ud().A0N(new ByteArrayInputStream(str.getBytes()));
            A00();
        } catch (C11160fq unused) {
            StringBuilder sb = new StringBuilder("Could not find SVG at: ");
            sb.append(str);
            Log.e("SVGImageView", sb.toString());
        }
    }

    public void setImageAsset(String str) {
        try {
            new AnonymousClass0AH(this).execute(getContext().getAssets().open(str));
        } catch (IOException unused) {
            StringBuilder sb = new StringBuilder("File not found: ");
            sb.append(str);
            Log.e("SVGImageView", sb.toString());
        }
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        new AnonymousClass0AJ(getContext(), this, i).execute(new Integer[0]);
    }

    @Override // android.widget.ImageView
    public void setImageURI(Uri uri) {
        try {
            new AnonymousClass0AH(this).execute(getContext().getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException unused) {
            StringBuilder sb = new StringBuilder("File not found: ");
            sb.append(uri);
            Log.e("SVGImageView", sb.toString());
        }
    }

    public void setSVG(AnonymousClass0Q5 r3) {
        if (r3 != null) {
            this.A01 = r3;
            A00();
            return;
        }
        throw new IllegalArgumentException("Null value passed to setSVG()");
    }

    public void setSVG(AnonymousClass0Q5 r3, String str) {
        if (r3 != null) {
            this.A01 = r3;
            this.A00.A00(str);
            A00();
            return;
        }
        throw new IllegalArgumentException("Null value passed to setSVG()");
    }
}
