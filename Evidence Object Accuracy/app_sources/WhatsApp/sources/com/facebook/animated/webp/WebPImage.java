package com.facebook.animated.webp;

import X.AbstractC39021p8;
import X.AnonymousClass4A8;
import X.C91744Sy;
import X.C93074Yx;
import X.EnumC869749s;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public class WebPImage implements AbstractC39021p8 {
    public long mNativeContext;

    public static native WebPImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer);

    public static native WebPImage nativeCreateFromNativeMemory(long j, int i);

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDuration();

    /* access modifiers changed from: private */
    /* renamed from: nativeGetFrame */
    public native WebPFrame getFrame(int i);

    private native int nativeGetFrameCount();

    private native int[] nativeGetFrameDurations();

    private native int nativeGetHeight();

    private native int nativeGetLoopCount();

    private native int nativeGetSizeInBytes();

    private native int nativeGetWidth();

    @Override // X.AbstractC39021p8
    public boolean doesRenderSupportScaling() {
        return true;
    }

    public WebPImage() {
    }

    public WebPImage(long j) {
        this.mNativeContext = j;
    }

    public static WebPImage createFromByteArray(byte[] bArr) {
        C93074Yx.A00();
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(bArr.length);
        allocateDirect.put(bArr);
        allocateDirect.rewind();
        return nativeCreateFromDirectByteBuffer(allocateDirect);
    }

    public void dispose() {
        nativeDispose();
    }

    public void finalize() {
        nativeFinalize();
    }

    @Override // X.AbstractC39021p8
    public WebPFrame getFrame(int i) {
        return getFrame(i);
    }

    @Override // X.AbstractC39021p8
    public int getFrameCount() {
        return nativeGetFrameCount();
    }

    @Override // X.AbstractC39021p8
    public int[] getFrameDurations() {
        return nativeGetFrameDurations();
    }

    @Override // X.AbstractC39021p8
    public C91744Sy getFrameInfo(int i) {
        EnumC869749s r3;
        WebPFrame nativeGetFrame = getFrame(i);
        try {
            int xOffset = nativeGetFrame.getXOffset();
            int yOffset = nativeGetFrame.getYOffset();
            int width = nativeGetFrame.getWidth();
            int height = nativeGetFrame.getHeight();
            if (nativeGetFrame.isBlendWithPreviousFrame()) {
                r3 = EnumC869749s.BLEND_WITH_PREVIOUS;
            } else {
                r3 = EnumC869749s.NO_BLEND;
            }
            return new C91744Sy(r3, nativeGetFrame.shouldDisposeToBackgroundColor() ? AnonymousClass4A8.DISPOSE_TO_BACKGROUND : AnonymousClass4A8.DISPOSE_DO_NOT, xOffset, yOffset, width, height);
        } finally {
            nativeGetFrame.dispose();
        }
    }

    @Override // X.AbstractC39021p8
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // X.AbstractC39021p8
    public int getLoopCount() {
        return nativeGetLoopCount();
    }

    @Override // X.AbstractC39021p8
    public int getSizeInBytes() {
        return nativeGetSizeInBytes();
    }

    @Override // X.AbstractC39021p8
    public int getWidth() {
        return nativeGetWidth();
    }
}
