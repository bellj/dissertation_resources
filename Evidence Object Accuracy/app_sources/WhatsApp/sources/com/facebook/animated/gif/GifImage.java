package com.facebook.animated.gif;

import X.AbstractC39021p8;
import X.AnonymousClass4A8;
import X.C91744Sy;
import X.EnumC869749s;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public class GifImage implements AbstractC39021p8 {
    public static volatile boolean sInitialized;
    public long mNativeContext;

    public static native GifImage nativeCreateFromDirectByteBuffer(ByteBuffer byteBuffer, int i, boolean z);

    public static native GifImage nativeCreateFromFileDescriptor(int i, int i2, boolean z);

    public static native GifImage nativeCreateFromNativeMemory(long j, int i, int i2, boolean z);

    private native void nativeDispose();

    private native void nativeFinalize();

    private native int nativeGetDuration();

    /* access modifiers changed from: private */
    /* renamed from: nativeGetFrame */
    public native GifFrame getFrame(int i);

    private native int nativeGetFrameCount();

    private native int[] nativeGetFrameDurations();

    private native int nativeGetHeight();

    private native int nativeGetLoopCount();

    private native int nativeGetSizeInBytes();

    private native int nativeGetWidth();

    private native boolean nativeIsAnimated();

    @Override // X.AbstractC39021p8
    public boolean doesRenderSupportScaling() {
        return false;
    }

    public GifImage() {
    }

    public GifImage(long j) {
        this.mNativeContext = j;
    }

    public void dispose() {
        nativeDispose();
    }

    public void finalize() {
        nativeFinalize();
    }

    public static AnonymousClass4A8 fromGifDisposalMethod(int i) {
        if (!(i == 0 || i == 1)) {
            if (i == 2) {
                return AnonymousClass4A8.DISPOSE_TO_BACKGROUND;
            }
            if (i == 3) {
                return AnonymousClass4A8.DISPOSE_TO_PREVIOUS;
            }
        }
        return AnonymousClass4A8.DISPOSE_DO_NOT;
    }

    public int getDuration() {
        return nativeGetDuration();
    }

    @Override // X.AbstractC39021p8
    public int getFrameCount() {
        return nativeGetFrameCount();
    }

    @Override // X.AbstractC39021p8
    public int[] getFrameDurations() {
        return nativeGetFrameDurations();
    }

    @Override // X.AbstractC39021p8
    public C91744Sy getFrameInfo(int i) {
        GifFrame nativeGetFrame = getFrame(i);
        try {
            return new C91744Sy(EnumC869749s.BLEND_WITH_PREVIOUS, fromGifDisposalMethod(nativeGetFrame.getDisposalMode()), nativeGetFrame.getXOffset(), nativeGetFrame.getYOffset(), nativeGetFrame.getWidth(), nativeGetFrame.getHeight());
        } finally {
            nativeGetFrame.dispose();
        }
    }

    @Override // X.AbstractC39021p8
    public int getHeight() {
        return nativeGetHeight();
    }

    @Override // X.AbstractC39021p8
    public int getLoopCount() {
        int nativeGetLoopCount = nativeGetLoopCount();
        if (nativeGetLoopCount == -1) {
            return 1;
        }
        int i = 1 + nativeGetLoopCount;
        if (nativeGetLoopCount == 0) {
            return 0;
        }
        return i;
    }

    @Override // X.AbstractC39021p8
    public int getSizeInBytes() {
        return nativeGetSizeInBytes();
    }

    @Override // X.AbstractC39021p8
    public int getWidth() {
        return nativeGetWidth();
    }

    public boolean isAnimated() {
        return nativeIsAnimated();
    }
}
