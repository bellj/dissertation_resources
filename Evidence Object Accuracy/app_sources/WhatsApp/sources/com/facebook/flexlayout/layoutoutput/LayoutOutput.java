package com.facebook.flexlayout.layoutoutput;

import X.AnonymousClass49W;
import X.AnonymousClass49X;

/* loaded from: classes3.dex */
public class LayoutOutput {
    public float[] arr;
    public Object[] measureResults;

    public LayoutOutput(int i) {
        this.measureResults = new Object[i];
        this.arr = new float[AnonymousClass49X.values().length + (i * AnonymousClass49W.values().length)];
    }
}
