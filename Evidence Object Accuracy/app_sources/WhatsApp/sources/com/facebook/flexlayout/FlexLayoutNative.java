package com.facebook.flexlayout;

import com.facebook.flexlayout.layoutoutput.LayoutOutput;
import com.facebook.flexlayout.styles.FlexItemCallback;
import com.facebook.soloader.SoLoader;

/* loaded from: classes3.dex */
public class FlexLayoutNative {
    public static native int jni_AddNumbers(int i, int i2);

    public static native void jni_calculateLayout(float[] fArr, float[][] fArr2, float f, float f2, float f3, float f4, float f5, float f6, LayoutOutput layoutOutput, FlexItemCallback[] flexItemCallbackArr);

    public static native float jni_getBaseline(long j, float f, float f2);

    static {
        SoLoader.A04("flexlayout");
    }
}
