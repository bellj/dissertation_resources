package com.facebook.flexlayout.styles;

import X.C12990iw;
import X.C64613Gf;

/* loaded from: classes2.dex */
public class FlexItemCallback {
    public final C64613Gf mMeasureFunction;

    public FlexItemCallback(C64613Gf r1) {
        this.mMeasureFunction = r1;
    }

    public final float baseline(float f, float f2) {
        throw C12990iw.A0m("Baseline function isn't defined!");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a3, code lost:
        if (java.lang.Float.isNaN(r12) == false) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b2, code lost:
        if (java.lang.Float.isNaN(r2) == false) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r3.A01 == 13320) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.facebook.flexlayout.layoutoutput.MeasureOutput measure(float r17, float r18, float r19, float r20, float r21, float r22) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.flexlayout.styles.FlexItemCallback.measure(float, float, float, float, float, float):com.facebook.flexlayout.layoutoutput.MeasureOutput");
    }
}
