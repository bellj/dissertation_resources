package com.facebook.minscript.compiler;

import com.facebook.soloader.SoLoader;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public class MinsCompilerImpl$Helper {
    public static native ByteBuffer doCompile(String str);

    static {
        SoLoader.A04("minscompiler-jni");
    }
}
