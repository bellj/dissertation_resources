package com.facebook.fresco.animation.factory;

import X.AbstractC11600gY;
import X.AnonymousClass4UM;
import X.AnonymousClass5Pk;
import X.C105914up;
import X.C87754Cu;
import X.C88914Hy;

/* loaded from: classes3.dex */
public class AnimatedFactoryV2Impl {
    public C88914Hy A00;
    public C87754Cu A01;
    public AbstractC11600gY A02;
    public final AnonymousClass4UM A03;
    public final C105914up A04;
    public final AnonymousClass5Pk A05;
    public final boolean A06;

    public AnimatedFactoryV2Impl(AnonymousClass4UM r1, AnonymousClass5Pk r2, C105914up r3, boolean z) {
        this.A03 = r1;
        this.A05 = r2;
        this.A04 = r3;
        this.A06 = z;
    }
}
