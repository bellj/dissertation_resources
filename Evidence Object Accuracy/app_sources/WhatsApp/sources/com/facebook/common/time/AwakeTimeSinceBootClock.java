package com.facebook.common.time;

import X.AbstractC12250hc;
import android.os.SystemClock;

/* loaded from: classes.dex */
public class AwakeTimeSinceBootClock implements AbstractC12250hc {
    public static final AwakeTimeSinceBootClock INSTANCE = new AwakeTimeSinceBootClock();

    public static AwakeTimeSinceBootClock get() {
        return INSTANCE;
    }

    @Override // X.AbstractC12250hc
    public long now() {
        return SystemClock.uptimeMillis();
    }

    public long nowNanos() {
        return System.nanoTime();
    }
}
