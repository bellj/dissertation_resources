package com.facebook.common.time;

import X.AbstractC12250hc;
import android.os.SystemClock;

/* loaded from: classes.dex */
public class RealtimeSinceBootClock implements AbstractC12250hc {
    public static final RealtimeSinceBootClock A00 = new RealtimeSinceBootClock();

    public static RealtimeSinceBootClock get() {
        return A00;
    }

    @Override // X.AbstractC12250hc
    public long now() {
        return SystemClock.elapsedRealtime();
    }
}
