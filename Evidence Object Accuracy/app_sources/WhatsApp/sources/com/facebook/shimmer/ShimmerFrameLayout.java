package com.facebook.shimmer;

import X.AbstractC64543Fy;
import X.AnonymousClass2ZV;
import X.AnonymousClass3C3;
import X.AnonymousClass4Fa;
import X.C12990iw;
import X.C56012kB;
import X.C76483li;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/* loaded from: classes2.dex */
public class ShimmerFrameLayout extends FrameLayout {
    public boolean A00 = true;
    public final Paint A01 = C12990iw.A0F();
    public final AnonymousClass2ZV A02 = new AnonymousClass2ZV();

    public ShimmerFrameLayout(Context context) {
        super(context);
        A04(context, null);
    }

    public ShimmerFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A04(context, attributeSet);
    }

    public ShimmerFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A04(context, attributeSet);
    }

    public ShimmerFrameLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A04(context, attributeSet);
    }

    public void A00() {
        if (this.A00) {
            A03();
            this.A00 = false;
            invalidate();
        }
    }

    public void A01() {
        if (!this.A00) {
            this.A00 = true;
            A02();
        }
    }

    public void A02() {
        AnonymousClass2ZV r1 = this.A02;
        ValueAnimator valueAnimator = r1.A00;
        if (valueAnimator != null && !valueAnimator.isStarted() && r1.getCallback() != null) {
            r1.A00.start();
        }
    }

    public void A03() {
        AnonymousClass2ZV r1 = this.A02;
        ValueAnimator valueAnimator = r1.A00;
        if (valueAnimator != null && valueAnimator.isStarted()) {
            r1.A00.cancel();
        }
    }

    public final void A04(Context context, AttributeSet attributeSet) {
        AbstractC64543Fy r0;
        int i;
        Paint paint;
        int i2;
        Paint paint2;
        setWillNotDraw(false);
        AnonymousClass2ZV r3 = this.A02;
        r3.setCallback(this);
        if (attributeSet == null) {
            AnonymousClass3C3 A01 = new C76483li().A01();
            r3.A02(A01);
            if (A01.A0H) {
                i2 = 2;
                paint2 = this.A01;
            } else {
                i2 = 0;
                paint2 = null;
            }
            setLayerType(i2, paint2);
            return;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass4Fa.A00, 0, 0);
        try {
            if (!obtainStyledAttributes.hasValue(4) || !obtainStyledAttributes.getBoolean(4, false)) {
                r0 = new C76483li();
            } else {
                r0 = new C56012kB();
            }
            r0.A00(obtainStyledAttributes);
            AnonymousClass3C3 A012 = r0.A01();
            r3.A02(A012);
            if (A012.A0H) {
                i = 2;
                paint = this.A01;
            } else {
                i = 0;
                paint = null;
            }
            setLayerType(i, paint);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.A00) {
            this.A02.draw(canvas);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A02.A00();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A03();
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.A02.setBounds(0, 0, getWidth(), getHeight());
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.A02;
    }
}
