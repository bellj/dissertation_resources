package com.facebook.msys.util;

import com.facebook.simplejni.NativeHolder;

/* loaded from: classes2.dex */
public final class NativeRunnable {
    public NativeHolder mNativeHolder;

    public native void run();

    public NativeRunnable(NativeHolder nativeHolder) {
        this.mNativeHolder = nativeHolder;
    }
}
