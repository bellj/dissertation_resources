package com.facebook.msys.util;

import X.AnonymousClass5S8;

/* loaded from: classes3.dex */
public final class McfReferenceHolder implements AnonymousClass5S8 {
    public long nativeReference = 0;

    @Override // X.AnonymousClass5S8
    public long getNativeReference() {
        return this.nativeReference;
    }

    private void setNativeReference(long j) {
        this.nativeReference = j;
    }
}
