package com.facebook.msys.util;

import X.AnonymousClass009;

/* loaded from: classes2.dex */
public class Environment {
    public static native synchronized boolean setenvNative(String str, String str2, boolean z);

    public static boolean setenv(String str, String str2) {
        return setenv(str, str2, true);
    }

    public static boolean setenv(String str, String str2, boolean z) {
        AnonymousClass009.A05(str);
        AnonymousClass009.A05(str2);
        return setenvNative(str, str2, z);
    }
}
