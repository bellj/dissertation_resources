package com.facebook.msys.util;

import X.AnonymousClass5S8;
import com.facebook.simplejni.NativeHolder;

/* loaded from: classes3.dex */
public final class NotificationScope implements AnonymousClass5S8 {
    public final McfReferenceHolder mMcfReference = new McfReferenceHolder();
    public final NativeHolder mNativeHolder = initNativeHolder(this);

    public static native NativeHolder initNativeHolder(NotificationScope notificationScope);

    @Override // X.AnonymousClass5S8
    public long getNativeReference() {
        return this.mMcfReference.nativeReference;
    }
}
