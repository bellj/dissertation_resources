package com.facebook.msys.mci;

import X.C003401m;
import X.C37521mX;

/* loaded from: classes2.dex */
public class Log {
    public static boolean sRegistered;

    public static int getWaLogLevel(int i) {
        if (i == 2) {
            return 5;
        }
        if (i == 3) {
            return 4;
        }
        if (i == 4) {
            return 3;
        }
        if (i != 5) {
            return i != 7 ? 1 : 0;
        }
        return 2;
    }

    public static native void registerLoggerNative(long j, int i, boolean z);

    public static native void setLogLevel(int i);

    public static void log(int i, String str) {
        int waLogLevel = getWaLogLevel(i);
        StringBuilder sb = new StringBuilder("wamsys/");
        sb.append(str);
        com.whatsapp.util.Log.log(waLogLevel, sb.toString());
    }

    public static synchronized boolean registerLogger(C37521mX r5) {
        boolean z;
        synchronized (Log.class) {
            C003401m.A01("registerLogger");
            if (sRegistered) {
                z = false;
            } else {
                registerLoggerNative(r5.A01, r5.A00, false);
                setLogLevel(4);
                z = true;
                sRegistered = true;
            }
            C003401m.A00();
        }
        return z;
    }
}
