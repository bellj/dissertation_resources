package com.facebook.msys.mci;

import X.AnonymousClass009;
import android.util.Pair;
import com.facebook.simplejni.NativeHolder;
import java.util.Map;

/* loaded from: classes2.dex */
public class UrlResponse {
    public final NativeHolder mNativeHolder;

    public static native NativeHolder initNativeHolder(UrlRequest urlRequest, int i, String[] strArr, String[] strArr2);

    public UrlResponse(UrlRequest urlRequest, int i, Map map) {
        AnonymousClass009.A05(urlRequest);
        AnonymousClass009.A05(map);
        Pair httpHeaderMapToKeysAndValues = NetworkUtils.httpHeaderMapToKeysAndValues(map);
        this.mNativeHolder = initNativeHolder(urlRequest, i, (String[]) httpHeaderMapToKeysAndValues.first, (String[]) httpHeaderMapToKeysAndValues.second);
    }
}
