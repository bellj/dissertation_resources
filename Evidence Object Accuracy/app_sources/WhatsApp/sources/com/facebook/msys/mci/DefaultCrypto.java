package com.facebook.msys.mci;

import X.C04160Kp;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import android.text.TextUtils;
import com.whatsapp.wamsys.Hex;
import com.whatsapp.wamsys.SecureUriParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import javax.crypto.Mac;

/* loaded from: classes2.dex */
public class DefaultCrypto implements Crypto {
    public static final int BUFFER_SIZE = 8192;
    public static final String HMAC_SHA256 = "HmacSHA256";
    public static final Class TAG = DefaultCrypto.class;
    public static final String UTF_8 = "UTF-8";
    public static final Crypto mCrypto = new DefaultCrypto();

    @Override // com.facebook.msys.mci.Crypto
    public byte[] computeMd5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            byte[] digest = instance.digest();
            StringBuilder A0h = C12960it.A0h();
            for (byte b : digest) {
                Object[] A1b = C12970iu.A1b();
                A1b[0] = Byte.valueOf(b);
                A0h.append(String.format("%02x", A1b));
            }
            return A0h.toString().getBytes();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 algorithm was not found. Should not happen", e);
        }
    }

    @Override // com.facebook.msys.mci.Crypto
    public String computeSHA256(String str, String str2) {
        Throwable th;
        NoSuchAlgorithmException e;
        UnsupportedEncodingException e2;
        InvalidKeyException e3;
        IOException e4;
        IllegalStateException e5;
        int read;
        String path = SecureUriParser.parseEncodedRFC2396(str).getPath();
        FileInputStream fileInputStream = null;
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            try {
                if (file.exists()) {
                    try {
                        FileInputStream fileInputStream2 = new FileInputStream(file);
                        int i = BUFFER_SIZE;
                        try {
                            byte[] bArr = new byte[BUFFER_SIZE];
                            long length = file.length();
                            Mac instance = Mac.getInstance(HMAC_SHA256);
                            C12990iw.A1S(HMAC_SHA256, instance, str2.getBytes());
                            long j = length;
                            long j2 = 0;
                            boolean z = true;
                            while (j > 0 && (read = fileInputStream2.read(bArr)) != -1) {
                                int i2 = 0;
                                while (z && i2 < i) {
                                    z = C12960it.A1T(bArr[i2]);
                                    i2++;
                                }
                                int min = Math.min((int) j, read);
                                instance.update(bArr, 0, min);
                                j2 += (long) read;
                                j -= (long) min;
                                i = BUFFER_SIZE;
                            }
                            if (!z && j2 == length && j == 0) {
                                String lowerCase = Hex.encodeHex(instance.doFinal(), false).toLowerCase(Locale.US);
                                try {
                                    fileInputStream2.close();
                                    return lowerCase;
                                } catch (IOException e6) {
                                    throw new RuntimeException("Couldn't close the stream.", e6);
                                }
                            } else {
                                try {
                                    fileInputStream2.close();
                                    return null;
                                } catch (IOException e7) {
                                    throw new RuntimeException("Couldn't close the stream.", e7);
                                }
                            }
                        } catch (UnsupportedEncodingException e8) {
                            e2 = e8;
                            throw new RuntimeException("UTF_8 encoding is not supported.", e2);
                        } catch (IOException e9) {
                            e4 = e9;
                            throw new RuntimeException("Couldn't read the content.", e4);
                        } catch (IllegalStateException e10) {
                            e5 = e10;
                            throw new RuntimeException("Couldn't update the hash.", e5);
                        } catch (InvalidKeyException e11) {
                            e3 = e11;
                            throw new RuntimeException("Invalid secret key.", e3);
                        } catch (NoSuchAlgorithmException e12) {
                            e = e12;
                            throw new RuntimeException("HMAC SHA256 algorithm is not found.", e);
                        } catch (Throwable th2) {
                            th = th2;
                            fileInputStream = fileInputStream2;
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                    throw th;
                                } catch (IOException e13) {
                                    throw new RuntimeException("Couldn't close the stream.", e13);
                                }
                            } else {
                                throw th;
                            }
                        }
                    } catch (UnsupportedEncodingException e14) {
                        e2 = e14;
                    } catch (IOException e15) {
                        e4 = e15;
                    } catch (IllegalStateException e16) {
                        e5 = e16;
                    } catch (InvalidKeyException e17) {
                        e3 = e17;
                    } catch (NoSuchAlgorithmException e18) {
                        e = e18;
                    }
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }
        return null;
    }

    @Override // com.facebook.msys.mci.Crypto
    public String computeSHA256(byte[] bArr, String str) {
        C04160Kp.A00(bArr, "file bytes can not be null");
        try {
            Mac instance = Mac.getInstance(HMAC_SHA256);
            C12990iw.A1S(HMAC_SHA256, instance, str.getBytes(UTF_8));
            return Hex.encodeHex(instance.doFinal(bArr), false).toLowerCase(Locale.US);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF_8 encoding is not supported.", e);
        } catch (InvalidKeyException e2) {
            throw new RuntimeException("Invalid secret key.", e2);
        } catch (NoSuchAlgorithmException e3) {
            throw new RuntimeException("HMAC SHA256 algorithm is not found.", e3);
        }
    }

    public static Crypto get() {
        return mCrypto;
    }
}
