package com.facebook.msys.mci;

import X.C003401m;

/* loaded from: classes2.dex */
public class JsonSerialization {
    public static boolean sInitialized;

    public static native void nativeInitialize();

    public static synchronized boolean initialize() {
        boolean z;
        synchronized (JsonSerialization.class) {
            C003401m.A01("JsonSerialization.initialize");
            if (sInitialized) {
                z = false;
            } else {
                nativeInitialize();
                z = true;
                sInitialized = true;
            }
            C003401m.A00();
        }
        return z;
    }
}
