package com.facebook.msys.mci;

import X.AnonymousClass009;
import X.AnonymousClass5S8;
import X.C55412iQ;
import X.C89794Lk;
import X.C93284Zv;
import com.facebook.simplejni.NativeHolder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class NotificationCenter {
    public static final String TAG = "NotificationCenter";
    public final Set mMainConfig;
    public NativeHolder mNativeHolder;
    public final Map mNativeScopeToJavaScope;
    public final Map mObserverConfigs;

    /* loaded from: classes3.dex */
    public interface NotificationCallback {
        void onNewNotification(String str, AnonymousClass5S8 v, Map map);
    }

    private native void addObserverNative(String str);

    private native NativeHolder initNativeHolder();

    private native void removeObserverNative(String str);

    public NotificationCenter() {
        this(false);
    }

    public NotificationCenter(boolean z) {
        this.mNativeScopeToJavaScope = new HashMap();
        this.mObserverConfigs = new HashMap();
        this.mMainConfig = new HashSet();
        if (!z) {
            this.mNativeHolder = initNativeHolder();
        }
    }

    public synchronized void addObserver(NotificationCallback notificationCallback, String str, AnonymousClass5S8 r4) {
        AnonymousClass009.A05(notificationCallback);
        AnonymousClass009.A05(str);
        if (!observerHasConfig(notificationCallback, str, r4)) {
            if (r4 != null) {
                addScopeToMappingOfNativeToJava(r4);
            }
            addObserverConfig(notificationCallback, str, r4);
            if (this.mMainConfig.add(str)) {
                addObserverNative(str);
            }
        }
    }

    private boolean addObserverConfig(NotificationCallback notificationCallback, String str, AnonymousClass5S8 r5) {
        Set set;
        C93284Zv r1 = (C93284Zv) this.mObserverConfigs.get(notificationCallback);
        if (r1 == null) {
            r1 = new C93284Zv();
            this.mObserverConfigs.put(notificationCallback, r1);
        }
        if (r5 == null) {
            set = r1.A01;
        } else {
            Map map = r1.A00;
            set = (Set) map.get(r5);
            if (set == null) {
                set = new HashSet();
                map.put(r5, set);
            }
        }
        return set.add(str);
    }

    private void addScopeToMappingOfNativeToJava(AnonymousClass5S8 r4) {
        this.mNativeScopeToJavaScope.put(Long.valueOf(r4.getNativeReference()), r4);
    }

    private void dispatchNotificationToCallbacks(String str, Long l, Object obj) {
        Set set;
        if (obj == null || (obj instanceof Map)) {
            Map map = (Map) obj;
            ArrayList arrayList = new ArrayList();
            AnonymousClass5S8 r4 = null;
            synchronized (this) {
                if (l != null) {
                    r4 = (AnonymousClass5S8) this.mNativeScopeToJavaScope.get(l);
                }
                for (Map.Entry entry : this.mObserverConfigs.entrySet()) {
                    C93284Zv r1 = (C93284Zv) entry.getValue();
                    if (r1.A01.contains(str) || ((set = (Set) r1.A00.get(r4)) != null && set.contains(str))) {
                        arrayList.add((NotificationCallback) entry.getKey());
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                Execution.executePossiblySync(new C55412iQ(r4, this, str, arrayList, map), 1);
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("Native layer of msys reported a notification whose payload could not be serialized into a Java Map. Instead, it's of type ");
        sb.append(obj.getClass().getName());
        throw new RuntimeException(sb.toString());
    }

    private boolean notificationNameExistsInSomeObserver(String str) {
        for (Map.Entry entry : this.mObserverConfigs.entrySet()) {
            C93284Zv r1 = (C93284Zv) entry.getValue();
            if (r1.A01.contains(str)) {
                return true;
            }
            Iterator it = new HashSet(r1.A00.entrySet()).iterator();
            while (it.hasNext()) {
                if (((Set) ((Map.Entry) it.next()).getValue()).contains(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean observerHasConfig(NotificationCallback notificationCallback, String str, AnonymousClass5S8 r4) {
        Set set;
        C93284Zv r0 = (C93284Zv) this.mObserverConfigs.get(notificationCallback);
        if (r0 == null) {
            return false;
        }
        if (r4 == null) {
            set = r0.A01;
        } else {
            set = (Set) r0.A00.get(r4);
            if (set == null) {
                return false;
            }
        }
        return set.contains(str);
    }

    public synchronized void removeEveryObserver(NotificationCallback notificationCallback) {
        C93284Zv r5;
        AnonymousClass009.A05(notificationCallback);
        C93284Zv r7 = (C93284Zv) this.mObserverConfigs.get(notificationCallback);
        if (r7 != null) {
            C89794Lk r6 = new C89794Lk(notificationCallback, this);
            synchronized (r7) {
                HashSet hashSet = new HashSet(r7.A01);
                HashMap hashMap = new HashMap();
                Iterator it = new HashSet(r7.A00.entrySet()).iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    hashMap.put((AnonymousClass5S8) entry.getKey(), new HashSet((Collection) entry.getValue()));
                }
                r5 = new C93284Zv(hashMap, hashSet);
            }
            for (String str : r5.A01) {
                r6.A01.removeObserver(r6.A00, str, null);
            }
            for (Map.Entry entry2 : r5.A00.entrySet()) {
                AnonymousClass5S8 r4 = (AnonymousClass5S8) entry2.getKey();
                for (String str2 : (Set) entry2.getValue()) {
                    r6.A01.removeObserver(r6.A00, str2, r4);
                }
            }
            this.mObserverConfigs.remove(notificationCallback);
        }
    }

    public synchronized void removeObserver(NotificationCallback notificationCallback, String str, AnonymousClass5S8 r4) {
        AnonymousClass009.A05(notificationCallback);
        AnonymousClass009.A05(str);
        if (observerHasConfig(notificationCallback, str, r4)) {
            removeObserverConfig(notificationCallback, str, r4);
            if (r4 != null && !scopeExistInAnyConfig(r4)) {
                removeScopeFromNativeToJavaMappings(r4);
            }
            if (!notificationNameExistsInSomeObserver(str)) {
                this.mMainConfig.remove(str);
                removeObserverNative(str);
            }
        }
    }

    private boolean removeObserverConfig(NotificationCallback notificationCallback, String str, AnonymousClass5S8 r7) {
        boolean z;
        C93284Zv r3 = (C93284Zv) this.mObserverConfigs.get(notificationCallback);
        if (r3 == null) {
            return false;
        }
        if (r7 == null) {
            z = r3.A01.remove(str);
        } else {
            Map map = r3.A00;
            Set set = (Set) map.get(r7);
            if (set != null) {
                z = set.remove(str);
                if (set.isEmpty()) {
                    map.remove(r7);
                }
            } else {
                z = false;
            }
        }
        if (r3.A01.isEmpty() && r3.A00.isEmpty()) {
            this.mObserverConfigs.remove(notificationCallback);
        }
        return z;
    }

    private void removeScopeFromNativeToJavaMappings(AnonymousClass5S8 r4) {
        this.mNativeScopeToJavaScope.remove(Long.valueOf(r4.getNativeReference()));
    }

    private boolean scopeExistInAnyConfig(AnonymousClass5S8 r4) {
        if (r4 != null) {
            for (Map.Entry entry : this.mObserverConfigs.entrySet()) {
                if (((C93284Zv) entry.getValue()).A00.containsKey(r4)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void setNativeHolder(NativeHolder nativeHolder) {
        this.mNativeHolder = nativeHolder;
    }
}
