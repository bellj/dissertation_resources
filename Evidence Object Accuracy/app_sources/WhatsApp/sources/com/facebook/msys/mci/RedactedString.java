package com.facebook.msys.mci;

import X.AnonymousClass009;
import com.facebook.simplejni.NativeHolder;

/* loaded from: classes2.dex */
public class RedactedString {
    public NativeHolder mNativeHolder;

    private native boolean equalsNative(Object obj);

    public static native NativeHolder initNativeHolder(String str);

    public static native NativeHolder initNativeHolder(String str, int i);

    public native String getOriginalString();

    public native int hashCode();

    public native int leakAllowance();

    public native String toString();

    public RedactedString(NativeHolder nativeHolder) {
        this.mNativeHolder = nativeHolder;
    }

    public RedactedString(String str) {
        AnonymousClass009.A05(str);
        this.mNativeHolder = initNativeHolder(str);
    }

    public RedactedString(String str, int i) {
        AnonymousClass009.A05(str);
        this.mNativeHolder = initNativeHolder(str, i);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof RedactedString)) {
            return false;
        }
        return equalsNative(obj);
    }
}
