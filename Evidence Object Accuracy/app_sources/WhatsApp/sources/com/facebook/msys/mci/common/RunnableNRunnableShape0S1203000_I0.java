package com.facebook.msys.mci.common;

import X.AbstractRunnableC47782Cq;
import com.facebook.msys.mci.NetworkSession;

/* loaded from: classes2.dex */
public class RunnableNRunnableShape0S1203000_I0 extends AbstractRunnableC47782Cq {
    public int A00;
    public int A01;
    public int A02;
    public Object A03;
    public Object A04;
    public String A05;
    public final int A06;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public RunnableNRunnableShape0S1203000_I0(com.facebook.msys.mci.NetworkSession r2, X.C37561md r3, java.lang.String r4, int r5, int r6, int r7, int r8) {
        /*
            r1 = this;
            r1.A06 = r8
            if (r8 == 0) goto L_0x0017
            java.lang.String r0 = "updateDataTaskUploadProgress"
            r1.A03 = r3
            r1.A04 = r2
            r1.A05 = r4
            r1.A00 = r5
            r1.A01 = r6
            r1.A02 = r7
        L_0x0013:
            r1.<init>(r0)
            return
        L_0x0017:
            java.lang.String r0 = "updateDataTaskDownloadProgress"
            r1.A03 = r3
            r1.A04 = r2
            r1.A05 = r4
            r1.A02 = r5
            r1.A01 = r6
            r1.A00 = r7
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.msys.mci.common.RunnableNRunnableShape0S1203000_I0.<init>(com.facebook.msys.mci.NetworkSession, X.1md, java.lang.String, int, int, int, int):void");
    }

    @Override // java.lang.Runnable
    public void run() {
        switch (this.A06) {
            case 0:
                ((NetworkSession) this.A04).updateDataTaskDownloadProgress(this.A05, (long) this.A02, (long) this.A01, (long) this.A00);
                return;
            case 1:
                ((NetworkSession) this.A04).updateDataTaskUploadProgress(this.A05, (long) this.A00, (long) this.A01, (long) this.A02);
                return;
            default:
                return;
        }
    }
}
