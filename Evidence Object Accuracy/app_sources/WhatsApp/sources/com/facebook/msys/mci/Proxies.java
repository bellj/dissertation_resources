package com.facebook.msys.mci;

/* loaded from: classes2.dex */
public class Proxies {
    public static boolean sConfigured;

    public static native void configureInternal(ProxyProvider proxyProvider);

    public static synchronized void configure(ProxyProvider proxyProvider) {
        synchronized (Proxies.class) {
            if (!sConfigured) {
                sConfigured = true;
                configureInternal(proxyProvider);
            } else {
                throw new IllegalStateException();
            }
        }
    }
}
