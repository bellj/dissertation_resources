package com.facebook.msys.mci;

import X.AnonymousClass2DJ;
import X.C003401m;
import android.util.Log;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class FileManager {
    public static final String CACHE_SCHEMA = "cache://";
    public static final String FILE_SCHEMA = "file://";
    public static final String TAG = "FileManager";
    public static File mCacheDir;
    public static boolean sInitialized;

    public static native void nativeInitialize();

    public static boolean copyFile(String str, String str2) {
        File fileFromPathWithOptionalScheme = getFileFromPathWithOptionalScheme(str);
        File fileFromPathWithOptionalScheme2 = getFileFromPathWithOptionalScheme(str2);
        if (fileFromPathWithOptionalScheme2.exists()) {
            return false;
        }
        tryToCreateDirectoryOfFile(str2);
        try {
            FileInputStream fileInputStream = new FileInputStream(fileFromPathWithOptionalScheme);
            FileOutputStream fileOutputStream = new FileOutputStream(fileFromPathWithOptionalScheme2);
            try {
                copyInputStreamIntoOutputStream(fileInputStream, fileOutputStream);
                fileOutputStream.close();
                fileInputStream.close();
                return true;
            } catch (Throwable th) {
                try {
                    fileOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException unused2) {
            return false;
        }
    }

    public static void copyInputStreamIntoOutputStream(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
        while (true) {
            int read = inputStream.read(bArr);
            if (read >= 0) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static String createCacheDirectory(String str) {
        File file = new File(mCacheDir, str);
        if ((!file.exists() || !file.isDirectory()) && !file.mkdirs()) {
            return null;
        }
        return file.toString();
    }

    public static boolean createDirectory(String str) {
        File fileFromPathWithOptionalScheme = getFileFromPathWithOptionalScheme(str);
        if (!fileFromPathWithOptionalScheme.exists() || !fileFromPathWithOptionalScheme.isDirectory()) {
            return fileFromPathWithOptionalScheme.mkdirs();
        }
        return true;
    }

    public static boolean deleteItem(String str) {
        return deleteItemRecursive(getFileFromPathWithOptionalScheme(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0022 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean deleteItemRecursive(java.io.File r7) {
        /*
            boolean r0 = r7.isDirectory()
            if (r0 == 0) goto L_0x0036
            java.io.File[] r6 = r7.listFiles()
            int r5 = r6.length
            r4 = 0
            r3 = 0
            r2 = 1
        L_0x000e:
            if (r3 >= r5) goto L_0x002c
            r1 = r6[r3]
            boolean r0 = r1.isDirectory()
            if (r0 == 0) goto L_0x0025
            if (r2 == 0) goto L_0x0021
            boolean r0 = deleteItemRecursive(r1)
        L_0x001e:
            r2 = 1
            if (r0 != 0) goto L_0x0022
        L_0x0021:
            r2 = 0
        L_0x0022:
            int r3 = r3 + 1
            goto L_0x000e
        L_0x0025:
            if (r2 == 0) goto L_0x0021
            boolean r0 = r1.delete()
            goto L_0x001e
        L_0x002c:
            boolean r0 = r7.delete()
            if (r0 == 0) goto L_0x0035
            if (r2 == 0) goto L_0x0035
            r4 = 1
        L_0x0035:
            return r4
        L_0x0036:
            boolean r0 = r7.delete()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.msys.mci.FileManager.deleteItemRecursive(java.io.File):boolean");
    }

    public static String getCacheDirectory() {
        return mCacheDir.toString();
    }

    public static synchronized File getFileFromPathWithOptionalScheme(String str) {
        File file;
        synchronized (FileManager.class) {
            if (str.startsWith(FILE_SCHEMA)) {
                try {
                    file = new File(new URI(str));
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            } else if (str.startsWith(CACHE_SCHEMA)) {
                file = new File(mCacheDir, str.substring(8));
            } else {
                file = new File(str);
            }
        }
        return file;
    }

    public static synchronized boolean initialize(File file) {
        boolean z;
        synchronized (FileManager.class) {
            C003401m.A01("FileManager.initialize");
            if (sInitialized) {
                z = false;
            } else {
                mCacheDir = file;
                nativeInitialize();
                z = true;
                sInitialized = true;
            }
            C003401m.A00();
        }
        return z;
    }

    public static boolean itemExists(String str) {
        return getFileFromPathWithOptionalScheme(str).exists();
    }

    public static String[] listDirectory(String str) {
        File[] listFiles = getFileFromPathWithOptionalScheme(str).listFiles();
        sortFilesByNewestFirst(listFiles);
        if (listFiles == null) {
            return null;
        }
        int length = listFiles.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            strArr[i] = listFiles[i].getAbsolutePath();
        }
        return strArr;
    }

    public static boolean moveFile(String str, String str2) {
        File fileFromPathWithOptionalScheme = getFileFromPathWithOptionalScheme(str);
        File fileFromPathWithOptionalScheme2 = getFileFromPathWithOptionalScheme(str2);
        if (fileFromPathWithOptionalScheme.equals(fileFromPathWithOptionalScheme2) || fileFromPathWithOptionalScheme.renameTo(fileFromPathWithOptionalScheme2)) {
            return true;
        }
        if (copyFile(str, str2)) {
            return fileFromPathWithOptionalScheme.delete();
        }
        return false;
    }

    public static byte[] readFile(String str) {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(getFileFromPathWithOptionalScheme(str), "r");
            long length = randomAccessFile.length();
            if (length <= 2147483647L) {
                byte[] bArr = new byte[(int) length];
                randomAccessFile.readFully(bArr);
                randomAccessFile.close();
                return bArr;
            }
            Log.e("FileUtils", "Cannot read more than 2GB into an array");
            throw new IOException("Cannot read more than 2GB into an array");
        } catch (IOException unused) {
            return null;
        }
    }

    public static File[] sortFilesByNewestFirst(File[] fileArr) {
        if (fileArr != null && fileArr.length > 1) {
            Arrays.sort(fileArr, new AnonymousClass2DJ());
        }
        return fileArr;
    }

    public static void tryToCreateDirectoryOfFile(String str) {
        File parentFile = getFileFromPathWithOptionalScheme(str).getParentFile();
        if (parentFile != null && !parentFile.exists()) {
            parentFile.mkdirs();
        }
    }

    public static boolean writeDataToFile(byte[] bArr, String str, boolean z) {
        tryToCreateDirectoryOfFile(str);
        File fileFromPathWithOptionalScheme = getFileFromPathWithOptionalScheme(str);
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            FileOutputStream fileOutputStream = new FileOutputStream(fileFromPathWithOptionalScheme, z);
            copyInputStreamIntoOutputStream(byteArrayInputStream, fileOutputStream);
            fileOutputStream.close();
            byteArrayInputStream.close();
            return true;
        } catch (IOException unused) {
            return false;
        }
    }
}
