package com.facebook.msys.mci;

import X.AbstractRunnableC47782Cq;
import X.C003401m;
import X.C47772Cp;
import X.RunnableC47812Ct;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S0001000_I0;
import com.facebook.simplejni.NativeHolder;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/* loaded from: classes2.dex */
public class Execution {
    public static final int INVALID_THREAD_PRIORITY = Integer.MIN_VALUE;
    public static volatile boolean sInitialized;
    public static final Set sThreadIds = Collections.newSetFromMap(new ConcurrentHashMap());
    public static final ThreadLocal sThreadLocalExecutionContext = new C47772Cp();
    public static final AtomicInteger sThreadPriority = new AtomicInteger(Integer.MIN_VALUE);
    public static Executor sUiThreadSchedulingExecutor;

    public static native int nativeGetExecutionContext();

    public static native void nativeInitialize();

    public static native void nativeResetExecutorsTestingOnly();

    public static native boolean nativeScheduleTask(Runnable runnable, int i, int i2, double d, String str);

    public static native void nativeStartExecutor(int i);

    public static native void setLoggingThresholds(double d, double d2);

    public static void assertInitialized(String str) {
        if (!sInitialized) {
            StringBuilder sb = new StringBuilder("Execution was called by ");
            sb.append(str);
            sb.append(" but was not initialized before being used");
            throw new RuntimeException(sb.toString());
        }
    }

    public static boolean callingThreadMatchesExecutionContext(int i) {
        try {
            return getExecutionContext() == i;
        } catch (RuntimeException unused) {
            return false;
        }
    }

    public static void ensureNotOnDiskIoThread() {
        if (sInitialized && getExecutionContext() == 2) {
            throw new IllegalStateException("The task can not run on DiskIO thread");
        }
    }

    public static void ensureNotOnMsysThread() {
        if (isOnMsysThread()) {
            throw new IllegalStateException("The task cannot run on any MSYS thread");
        }
    }

    public static void executeAfter(AbstractRunnableC47782Cq r1, int i, long j) {
        executeAfterWithPriority(r1, i, 0, j);
    }

    public static void executeAfterWithPriority(AbstractRunnableC47782Cq r2, int i, int i2, long j) {
        assertInitialized(r2.toString());
        if (sUiThreadSchedulingExecutor == null || Looper.myLooper() != Looper.getMainLooper()) {
            executeAfterWithPriorityInternal(r2, i, i2, j);
        } else {
            sUiThreadSchedulingExecutor.execute(new RunnableC47812Ct(r2, i, i2, j));
        }
    }

    public static void executeAfterWithPriorityInternal(AbstractRunnableC47782Cq r6, int i, int i2, long j) {
        if (!nativeScheduleTask(r6, i, i2, ((double) j) / 1000.0d, r6.toString())) {
            StringBuilder sb = new StringBuilder("UNKNOWN execution context ");
            sb.append(i);
            throw new RuntimeException(sb.toString());
        }
    }

    public static void executeAsync(AbstractRunnableC47782Cq r1, int i) {
        executeAsyncWithPriority(r1, i, 0);
    }

    public static void executeAsyncWithPriority(AbstractRunnableC47782Cq r2, int i, int i2) {
        assertInitialized(r2.toString());
        executeAfterWithPriority(r2, i, i2, 0);
    }

    public static void executePossiblySync(AbstractRunnableC47782Cq r1, int i) {
        assertInitialized(r1.toString());
        if (callingThreadMatchesExecutionContext(i)) {
            r1.run();
        } else {
            executeAsyncWithPriority(r1, i, 0);
        }
    }

    public static int getExecutionContext() {
        return ((Number) sThreadLocalExecutionContext.get()).intValue();
    }

    public static synchronized boolean initialize() {
        boolean initialize;
        synchronized (Execution.class) {
            initialize = initialize(null);
        }
        return initialize;
    }

    public static synchronized boolean initialize(Executor executor) {
        boolean z;
        synchronized (Execution.class) {
            C003401m.A01("Execution.initialize");
            if (sInitialized) {
                z = false;
            } else {
                sUiThreadSchedulingExecutor = executor;
                nativeInitialize();
                synchronized (TaskTracker.class) {
                    int i = 0;
                    if (!TaskTracker.sInitialized) {
                        TaskTracker[] taskTrackerArr = {TaskTracker.TRACKER_MAIN, TaskTracker.TRACKER_DISK_IO, TaskTracker.TRACKER_NETWORK, TaskTracker.TRACKER_DECODING, TaskTracker.TRACKER_CRYPTO};
                        int[] iArr = new int[5];
                        String[] strArr = new String[5];
                        int i2 = 0;
                        do {
                            iArr[i2] = taskTrackerArr[i2].mExecutionContext;
                            strArr[i2] = taskTrackerArr[i2].mQueueName;
                            i2++;
                        } while (i2 < 5);
                        NativeHolder[] initNativeHolders = TaskTracker.initNativeHolders(iArr, strArr);
                        do {
                            taskTrackerArr[i].mNativeHolder = initNativeHolders[i];
                            i++;
                        } while (i < 5);
                        TaskTracker.sInitialized = true;
                    }
                }
                z = true;
                sInitialized = true;
            }
            C003401m.A00();
        }
        return z;
    }

    public static boolean isOnMsysThread() {
        return sInitialized && getExecutionContext() != 0;
    }

    public static void resetExecutorsForTestingOnly() {
        sThreadIds.clear();
        nativeResetExecutorsTestingOnly();
    }

    public static boolean setInitializedForTestingOnly(boolean z) {
        boolean z2 = sInitialized;
        sInitialized = z;
        return z2;
    }

    public static void setThreadPriorities(Integer num) {
        sThreadPriority.set(num.intValue());
    }

    public static Executor setUiThreadSchedulingExecutorForTestingOnly(Executor executor) {
        Executor executor2 = sUiThreadSchedulingExecutor;
        sUiThreadSchedulingExecutor = executor;
        return executor2;
    }

    public static void startExecutorThread(int i, String str) {
        RunnableBRunnable0Shape0S0001000_I0 runnableBRunnable0Shape0S0001000_I0 = new RunnableBRunnable0Shape0S0001000_I0(i, 0);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("Context");
        new Thread(runnableBRunnable0Shape0S0001000_I0, sb.toString()).start();
    }
}
