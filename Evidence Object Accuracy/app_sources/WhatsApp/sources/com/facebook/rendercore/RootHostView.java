package com.facebook.rendercore;

import X.AnonymousClass2k6;
import X.AnonymousClass3H1;
import X.AnonymousClass3HL;
import X.AnonymousClass4SN;
import X.C12990iw;
import X.C13000ix;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/* loaded from: classes2.dex */
public class RootHostView extends AnonymousClass2k6 {
    public static final int[] A01 = C13000ix.A07();
    public final AnonymousClass4SN A00;

    public RootHostView(Context context) {
        this(context, null);
    }

    public RootHostView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A00 = new AnonymousClass4SN(this);
    }

    @Override // android.view.View
    public void offsetLeftAndRight(int i) {
        super.offsetLeftAndRight(i);
    }

    @Override // android.view.View
    public void offsetTopAndBottom(int i) {
        super.offsetTopAndBottom(i);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A00.A04.A07();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A00.A04.A08();
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        AnonymousClass4SN r7 = this.A00;
        int[] iArr = A01;
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i) == 1073741824 && View.MeasureSpec.getMode(i2) == 1073741824) {
            r7.A02 = true;
            iArr[0] = size;
            iArr[1] = size2;
        } else {
            AnonymousClass3HL r0 = r7.A00;
            if (r0 != null) {
                r0.A01(i, iArr, i2);
                r7.A02 = false;
            } else {
                super.onMeasure(i, i2);
                return;
            }
        }
        setMeasuredDimension(iArr[0], iArr[1]);
    }

    public void setRenderState(AnonymousClass3HL r4) {
        AnonymousClass3H1 r1;
        AnonymousClass4SN r2 = this.A00;
        AnonymousClass3HL r12 = r2.A00;
        if (r12 != r4) {
            if (r12 != null) {
                r12.A09 = null;
            }
            r2.A00 = r4;
            if (r4 != null) {
                AnonymousClass4SN r0 = r4.A09;
                if (r0 == null || r0 == r2) {
                    r4.A09 = r2;
                    r1 = r4.A08;
                } else {
                    throw C12990iw.A0m("Must detach from previous host listener first");
                }
            } else {
                r1 = null;
            }
            if (r2.A01 != r1) {
                if (r1 == null) {
                    r2.A04.A09();
                }
                r2.A01 = r1;
                r2.A03.requestLayout();
            }
        }
    }

    @Override // android.view.View
    public void setTranslationX(float f) {
        super.setTranslationX(f);
    }

    @Override // android.view.View
    public void setTranslationY(float f) {
        super.setTranslationY(f);
    }
}
