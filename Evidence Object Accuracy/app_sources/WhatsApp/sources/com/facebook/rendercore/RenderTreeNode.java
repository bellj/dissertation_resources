package com.facebook.rendercore;

import X.AbstractC65073Ia;
import X.AnonymousClass3H1;
import X.C12960it;
import X.C12980iv;
import android.graphics.Rect;
import java.util.List;
import java.util.Locale;

/* loaded from: classes2.dex */
public class RenderTreeNode {
    public List A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final Rect A04;
    public final Rect A05;
    public final RenderTreeNode A06;
    public final AbstractC65073Ia A07;
    public final Object A08;

    public RenderTreeNode(Rect rect, Rect rect2, RenderTreeNode renderTreeNode, AbstractC65073Ia r6, Object obj, int i) {
        int i2;
        int i3;
        this.A06 = renderTreeNode;
        this.A07 = r6;
        this.A08 = obj;
        this.A04 = rect;
        if (renderTreeNode != null) {
            i2 = renderTreeNode.A01 + rect.left;
        } else {
            i2 = rect.left;
        }
        this.A01 = i2;
        if (renderTreeNode != null) {
            i3 = renderTreeNode.A02 + rect.top;
        } else {
            i3 = rect.top;
        }
        this.A02 = i3;
        this.A05 = rect2;
        this.A03 = i;
    }

    public String A00(AnonymousClass3H1 r12) {
        int i;
        int i2;
        long j;
        AbstractC65073Ia r0 = this.A07;
        long A02 = r0.A02();
        String A03 = r0.A03();
        if (r12 != null) {
            i = C12960it.A05(r12.A02.get(A02, -1));
        } else {
            i = -1;
        }
        String shortString = this.A04.toShortString();
        List list = this.A00;
        if (list != null) {
            i2 = list.size();
        } else {
            i2 = 0;
        }
        RenderTreeNode renderTreeNode = this.A06;
        if (renderTreeNode != null) {
            j = renderTreeNode.A07.A02();
        } else {
            j = -1;
        }
        Locale locale = Locale.US;
        Object[] objArr = new Object[9];
        C12980iv.A1U(objArr, 0, A02);
        objArr[1] = A03;
        C12960it.A1P(objArr, i, 2);
        C12960it.A1P(objArr, this.A03, 3);
        objArr[4] = shortString;
        C12960it.A1P(objArr, this.A01, 5);
        C12960it.A1P(objArr, this.A02, 6);
        C12960it.A1P(objArr, i2, 7);
        C12980iv.A1U(objArr, 8, j);
        return String.format(locale, "Id=%d; renderUnit='%s'; indexInTree=%d; posInParent=%d; bounds=%s; absPosition=[%d, %d]; childCount=%d; parentId=%d;", objArr);
    }
}
