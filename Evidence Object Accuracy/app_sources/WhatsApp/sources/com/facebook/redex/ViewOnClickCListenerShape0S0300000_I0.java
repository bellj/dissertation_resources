package com.facebook.redex;

import X.AbstractActivityC28171Kz;
import X.AbstractActivityC36611kC;
import X.AbstractC13860kS;
import X.AbstractC14440lR;
import X.AbstractC14640lm;
import X.AbstractC14750lz;
import X.AbstractC15340mz;
import X.AbstractC16830pp;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass12P;
import X.AnonymousClass1A8;
import X.AnonymousClass1IR;
import X.AnonymousClass1IS;
import X.AnonymousClass1OY;
import X.AnonymousClass1m4;
import X.AnonymousClass2BP;
import X.AnonymousClass2V6;
import X.AnonymousClass426;
import X.AnonymousClass4S9;
import X.C15370n3;
import X.C15570nT;
import X.C15580nU;
import X.C17070qD;
import X.C20370ve;
import X.C33651en;
import X.C36021jC;
import X.C36591kA;
import X.C38211ni;
import X.C42461vF;
import X.C42581vS;
import X.C42611vV;
import X.C64043Ea;
import X.C90274Ng;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.community.CommunitySubgroupsBottomSheet;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.invites.RevokeInviteDialogFragment;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.util.Log;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.VoipActivityV2;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0300000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public ViewOnClickCListenerShape0S0300000_I0(Object obj, Object obj2, Object obj3, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AbstractC14440lR r2;
        RunnableBRunnable0Shape0S0300000_I0 runnableBRunnable0Shape0S0300000_I0;
        AnonymousClass1IS r3;
        Class AFc;
        switch (this.A03) {
            case 0:
                ((C36591kA) this.A00).A03.A1c(((C15370n3) this.A01).A0K, ((AnonymousClass426) this.A02).A00);
                return;
            case 1:
                AbstractActivityC36611kC r4 = (AbstractActivityC36611kC) this.A00;
                C15370n3 r32 = (C15370n3) this.A01;
                C64043Ea r22 = (C64043Ea) this.A02;
                if (!r4.A38(r32) || r32.A0d) {
                    if (r4.A38(r32) && r32.A0d) {
                        r22.A00(r4.getString(R.string.tap_unblock), true);
                    }
                    r4.A32(r32);
                    return;
                }
                r4.A33(r32);
                return;
            case 2:
                Jid A0B = ((C15370n3) this.A02).A0B(C15580nU.class);
                AnonymousClass009.A05(A0B);
                CommunitySubgroupsBottomSheet.A00((C15580nU) A0B).A1F(((ActivityC000900k) this.A01).A0V(), "SUBGROUP_PICKER_TAG");
                return;
            case 3:
                View view2 = (View) this.A00;
                ((AbstractC16830pp) this.A01).ALp(view2.getContext(), (AbstractC13860kS) AnonymousClass12P.A01(view2.getContext(), Conversation.class), ((AbstractC15340mz) this.A02).A0L);
                return;
            case 4:
                View view3 = (View) this.A00;
                view3.getContext();
                AnonymousClass12P.A01(view3.getContext(), Conversation.class);
                return;
            case 5:
                C42461vF r42 = (C42461vF) this.A00;
                AbstractC15340mz r8 = (AbstractC15340mz) this.A01;
                AbstractC16830pp r7 = (AbstractC16830pp) this.A02;
                if (!r8.A0L.A0F() || !r8.A0L.A0A()) {
                    r3 = r8.A0z;
                } else {
                    C17070qD r0 = r42.A1A;
                    r0.A03();
                    C20370ve r1 = r0.A08;
                    r3 = r8.A0z;
                    AnonymousClass1IR A0L = r1.A0L(r3.A01);
                    if (A0L != null) {
                        AnonymousClass1IR r5 = r8.A0L;
                        if (r5.A02 != 18) {
                            r3 = new AnonymousClass1IS(A0L.A0C, A0L.A0L, A0L.A0Q);
                            if (r5.A01() != null) {
                                r42.A07.A02(r8.A0L.A01());
                            }
                        }
                    }
                }
                if (!(r7 == null || (AFc = r7.AFc()) == null)) {
                    Intent intent = new Intent(r42.getContext(), AFc);
                    intent.putExtra("referral_screen", "chat");
                    C38211ni.A00(intent, r3);
                    r42.getContext().startActivity(intent);
                    return;
                }
                return;
            case 6:
                C42461vF r33 = (C42461vF) this.A00;
                AbstractC15340mz r43 = (AbstractC15340mz) this.A01;
                AbstractC16830pp r52 = (AbstractC16830pp) this.A02;
                if (((AnonymousClass1OY) r33).A0L.A0F(r43.A0L.A0D) || ((AnonymousClass1OY) r33).A0L.A0F(r43.A0L.A0E)) {
                    Intent intent2 = new Intent(r33.getContext(), r52.AFc());
                    C38211ni.A00(intent2, r43.A0z);
                    r33.getContext().startActivity(intent2);
                    return;
                }
                AnonymousClass1A8 r23 = r33.A08;
                String str = r43.A0L.A0K;
                if (!TextUtils.isEmpty(str) && r23.A00.contains(str)) {
                    AnonymousClass1A8 r24 = r33.A08;
                    AnonymousClass1IR r53 = r43.A0L;
                    if (!(r53 == null || r53.A0K == null || r53.A03 != 1000)) {
                        C15570nT r12 = r24.A04;
                        if (!r12.A0F(r53.A0D) && !r12.A0F(r43.A0L.A0E)) {
                            r24.A00.remove(r43.A0L.A0K);
                            r24.A03(r43.A0z, r43.A0L.A0K);
                        }
                    }
                    r33.A0B.setVisibility(0);
                    r33.A0C.setVisibility(8);
                    return;
                }
                return;
            case 7:
                ActivityC13830kP r54 = (ActivityC13830kP) this.A00;
                r54.A05.Ab6(new RunnableBRunnable0Shape0S0300000_I0(r54, this.A01, this.A02, 32));
                return;
            case 8:
                C42611vV r55 = (C42611vV) this.A00;
                AbstractCollection abstractCollection = (AbstractCollection) this.A01;
                Object obj = this.A02;
                Iterator it = abstractCollection.iterator();
                while (it.hasNext()) {
                    AbstractC14640lm r72 = (AbstractC14640lm) it.next();
                    ConversationsFragment conversationsFragment = r55.A0F;
                    conversationsFragment.A1F.A05(r72, false);
                    conversationsFragment.A1j.A04(r72, 4, 0, 0);
                }
                r2 = r55.A0F.A2B;
                runnableBRunnable0Shape0S0300000_I0 = new RunnableBRunnable0Shape0S0300000_I0(r55, abstractCollection, obj, 34);
                break;
            case 9:
                C42581vS r44 = (C42581vS) this.A00;
                AbstractC14640lm r6 = (AbstractC14640lm) this.A01;
                Object obj2 = this.A02;
                r44.A07.A05(r6, false);
                r44.A0C.A04(r6, 4, 0, 0);
                if (obj2 != null) {
                    r2 = r44.A0F;
                    runnableBRunnable0Shape0S0300000_I0 = new RunnableBRunnable0Shape0S0300000_I0(r44, r6, obj2, 35);
                    break;
                } else {
                    return;
                }
            case 10:
                Jid A0B2 = ((C15370n3) this.A01).A0B(UserJid.class);
                AnonymousClass009.A05(A0B2);
                ((ActivityC13810kN) AnonymousClass12P.A00(((AnonymousClass2BP) this.A00).A03)).Adm(RevokeInviteDialogFragment.A00((UserJid) A0B2, ((C90274Ng) this.A02).A01));
                return;
            case 11:
                Context context = ((View) this.A00).getContext();
                ((AbstractC16830pp) this.A01).ALp(context, (AbstractC13860kS) AnonymousClass12P.A01(context, ActivityC13810kN.class), (AnonymousClass1IR) this.A02);
                return;
            case 12:
            default:
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C36021jC.A00((Activity) this.A00, 130);
                ((Runnable) this.A01).run();
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
                ((StatusPlaybackContactFragment) this.A00).A1N((C15370n3) this.A02, (C33651en) this.A01);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                Set set = (Set) this.A01;
                set.size();
                ((AbstractC14750lz) this.A02).AGo(R.string.deleting_status, 2000, false).A01();
                ((AnonymousClass1m4) this.A00).A02.A0V(set, true);
                return;
            case 17:
                ViewSharedContactArrayActivity viewSharedContactArrayActivity = ((AnonymousClass2V6) this.A00).A01;
                viewSharedContactArrayActivity.A0H.A01(viewSharedContactArrayActivity, (C15370n3) this.A02, 15, false);
                return;
            case 18:
                Object obj3 = this.A02;
                ViewSharedContactArrayActivity viewSharedContactArrayActivity2 = ((AnonymousClass2V6) this.A00).A01;
                viewSharedContactArrayActivity2.A04.A01(viewSharedContactArrayActivity2, (UserJid) this.A01, ((AnonymousClass4S9) obj3).A03);
                return;
            case 19:
                VoipActivityV2 voipActivityV2 = (VoipActivityV2) this.A00;
                CallInfo callInfo = (CallInfo) this.A02;
                View view4 = voipActivityV2.A0J;
                AnonymousClass009.A03(view4);
                view4.setVisibility(8);
                Log.i("VoipActivityV2 vm call back onclick");
                ArrayList arrayList = new ArrayList();
                for (AbstractC14640lm r13 : (List) this.A01) {
                    C15370n3 A0A = ((AbstractActivityC28171Kz) voipActivityV2).A03.A0A(r13);
                    if (A0A != null) {
                        arrayList.add(A0A);
                    }
                }
                if (!arrayList.isEmpty()) {
                    voipActivityV2.A1J.A03(voipActivityV2, callInfo.groupJid, arrayList, 30, callInfo.videoEnabled);
                    return;
                }
                return;
        }
        r2.Ab2(runnableBRunnable0Shape0S0300000_I0);
    }
}
