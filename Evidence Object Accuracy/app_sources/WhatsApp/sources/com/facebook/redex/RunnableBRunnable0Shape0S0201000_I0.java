package com.facebook.redex;

import X.AbstractC116385Vf;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC16110oT;
import X.AbstractC16130oV;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass12H;
import X.AnonymousClass15Y;
import X.AnonymousClass16D;
import X.AnonymousClass1BJ;
import X.AnonymousClass1EG;
import X.AnonymousClass1IS;
import X.AnonymousClass1Lx;
import X.AnonymousClass1N8;
import X.AnonymousClass1N9;
import X.AnonymousClass1Ux;
import X.AnonymousClass1VN;
import X.AnonymousClass1X7;
import X.AnonymousClass2BT;
import X.AnonymousClass2Bc;
import X.C06260Su;
import X.C14820m6;
import X.C14900mE;
import X.C15050mT;
import X.C15110ma;
import X.C15650ng;
import X.C16120oU;
import X.C16170oZ;
import X.C16310on;
import X.C16330op;
import X.C16490p7;
import X.C17040qA;
import X.C18350sJ;
import X.C18370sL;
import X.C19490uC;
import X.C20020v5;
import X.C20720wD;
import X.C20750wG;
import X.C20870wS;
import X.C20900wV;
import X.C21320xE;
import X.C21380xK;
import X.C21390xL;
import X.C21400xM;
import X.C22130yZ;
import X.C22380yz;
import X.C22420z3;
import X.C22500zB;
import X.C22520zD;
import X.C25991Bp;
import X.C26701Em;
import X.C27151Gf;
import X.C29631Ua;
import X.C29841Uw;
import X.C30041Vv;
import X.C30311Wx;
import X.C33761f2;
import X.C35261hX;
import X.C35271hY;
import X.C35551iF;
import X.C35671iU;
import X.C35681iV;
import X.C35691iW;
import X.C37381mH;
import X.C39451pv;
import X.C40481rf;
import X.C40501rh;
import X.C40511ri;
import X.C40531rk;
import X.C40581rp;
import X.C40601rr;
import X.C40621rt;
import X.C42611vV;
import X.C43521xA;
import X.C43951xu;
import X.C471629h;
import X.C47682By;
import X.C49512La;
import X.C77453nJ;
import X.C853742m;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Message;
import android.os.SystemClock;
import android.util.SparseArray;
import android.view.View;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SyncDevicesJob;
import com.whatsapp.jobqueue.job.SyncProfilePictureJob;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0201000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0201000_I0(AbstractC15340mz r2, C26701Em r3) {
        this.A03 = 27;
        this.A01 = r3;
        this.A02 = r2;
        this.A00 = 56;
    }

    public RunnableBRunnable0Shape0S0201000_I0(C20020v5 r2, Jid jid, int i, int i2) {
        this.A03 = i2;
        if (10 - i2 != 0) {
            this.A01 = r2;
            this.A02 = jid;
            this.A00 = i;
            return;
        }
        this.A01 = r2;
        this.A02 = jid;
        this.A00 = 3;
    }

    public RunnableBRunnable0Shape0S0201000_I0(AnonymousClass1BJ r2, C39451pv r3, int i) {
        this.A03 = 20;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = i;
    }

    public RunnableBRunnable0Shape0S0201000_I0(View view, BottomSheetBehavior bottomSheetBehavior, int i, int i2) {
        this.A03 = i2;
        this.A01 = bottomSheetBehavior;
        if (1 - i2 != 0) {
            this.A02 = view;
            this.A00 = i;
            return;
        }
        this.A02 = view;
        this.A00 = i;
    }

    public RunnableBRunnable0Shape0S0201000_I0(Object obj, Object obj2, int i, int i2) {
        this.A03 = i2;
        this.A01 = obj;
        this.A00 = i;
        this.A02 = obj2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C16310on A02;
        String str;
        Intent intent;
        AbstractC14640lm r6;
        C30311Wx r2;
        Object e;
        StringBuilder sb;
        String str2;
        C40511ri A00;
        switch (this.A03) {
            case 0:
                int i = this.A00;
                C15050mT r1 = (C15050mT) this.A02;
                if (((AbstractC116385Vf) ((C15110ma) this.A01).A00).A6s(i)) {
                    r1.A09("Local AnalyticsService processed last dispatch request");
                    return;
                }
                return;
            case 1:
                ((BottomSheetBehavior) this.A01).A0N((View) this.A02, this.A00);
                return;
            case 2:
                BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) this.A01;
                C06260Su r0 = bottomSheetBehavior.A0D;
                if (r0 == null || !r0.A0A()) {
                    bottomSheetBehavior.A0K(this.A00);
                    return;
                } else {
                    ((View) this.A02).postOnAnimation(this);
                    return;
                }
            case 3:
                ((C14900mE) this.A01).A0E((CharSequence) this.A02, this.A00);
                return;
            case 4:
                C22520zD r62 = (C22520zD) this.A01;
                AbstractC15340mz r4 = (AbstractC15340mz) this.A02;
                if (r62.A0S.A07(508)) {
                    C22500zB r22 = r62.A0D;
                    if (r22.A03.A07(508)) {
                        AnonymousClass1IS r12 = r4.A0z;
                        UserJid of = UserJid.of(r12.A00);
                        if (!(of == null || !r12.A02 || (r4.A0j == null && r4.A0i == null))) {
                            int i2 = r4.A0C;
                            if (i2 == 4) {
                                for (C40531rk r02 : r22.A00.A01()) {
                                    C20020v5 r5 = r02.A00;
                                    if (r5.A0M.A07(1022) && !r5.A01.A0F(of)) {
                                        C40501rh r03 = r5.A09.A01;
                                        String string = r03.A00.A01(r03.A01).getString(of.getRawString(), null);
                                        if (!(string == null || (A00 = C40501rh.A00(string)) == null)) {
                                            r5.A0R.execute(new RunnableBRunnable0Shape0S0300000_I0(r5, of, A00, 27));
                                        }
                                    }
                                }
                            } else if (i2 == 5 || i2 == 13) {
                                C40501rh r04 = r22.A02.A01;
                                r04.A00.A01(r04.A01).edit().remove(of.getRawString()).apply();
                            }
                        }
                    }
                }
                C22380yz r3 = r62.A0Z;
                AnonymousClass1IS r13 = r4.A0z;
                UserJid of2 = UserJid.of(r13.A00);
                if (of2 != null) {
                    boolean z = r13.A02;
                    boolean A01 = C37381mH.A01(r4.A0C);
                    if (z && A01 && r4.A0d != null && r4.A0e != null) {
                        r3.A05.A01(of2);
                        r3.A02.A01().A00(of2);
                        return;
                    }
                    return;
                }
                return;
            case 5:
                C22520zD r14 = (C22520zD) this.A01;
                AbstractC15340mz r23 = (AbstractC15340mz) this.A02;
                r14.A0N.A02(r23);
                C22420z3 r15 = r14.A0i;
                r15.A0H.A00(r23);
                r15.A0D.A02(r23);
                return;
            case 6:
                int i3 = this.A00;
                AbstractC15340mz r52 = (AbstractC15340mz) this.A02;
                C17040qA r42 = ((C16170oZ) this.A01).A14;
                int i4 = 3;
                if (i3 != 4) {
                    i4 = 2;
                    if (i3 != 3) {
                        i4 = 0;
                        if (i3 == 2) {
                            i4 = 1;
                        }
                    }
                }
                r42.A02(i4, C33761f2.A00(r52.A0y, r52.A08, false));
                return;
            case 7:
                ActivityC13810kN r32 = (ActivityC13810kN) this.A01;
                int i5 = this.A00;
                Intent intent2 = ((C77453nJ) this.A02).mIntent;
                if (intent2 == null) {
                    intent = null;
                } else {
                    intent = new Intent(intent2);
                }
                r32.A2E(intent, i5);
                return;
            case 8:
                C29631Ua.A05((C29631Ua) this.A01, (List) this.A02, this.A00);
                return;
            case 9:
                ((C20720wD) this.A01).A00.A00(new SyncDevicesJob((UserJid[]) ((List) this.A02).toArray(new UserJid[0]), this.A00));
                return;
            case 10:
                C20020v5 r05 = (C20020v5) this.A01;
                Jid jid = (Jid) this.A02;
                int i6 = this.A00;
                r05.A07();
                SharedPreferences A04 = r05.A04();
                String rawString = jid.getRawString();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(rawString);
                sb2.append("_businessTools");
                String[] split = A04.getString(sb2.toString(), "null,null,null,null").split(",");
                C40621rt r33 = new C40621rt(C40601rr.A03(split, 0), C40601rr.A03(split, 1), C40601rr.A03(split, 2), C40601rr.A03(split, 3));
                if (i6 == 0) {
                    r33.A02 = Long.valueOf(C40601rr.A00(r33.A02, (long) 1));
                } else if (i6 == 1) {
                    r33.A00 = Long.valueOf(C40601rr.A00(r33.A00, (long) 1));
                } else if (i6 == 2) {
                    r33.A01 = Long.valueOf(C40601rr.A00(r33.A01, (long) 1));
                } else if (i6 == 3) {
                    r33.A03 = Long.valueOf(C40601rr.A00(r33.A03, (long) 1));
                } else {
                    StringBuilder sb3 = new StringBuilder("BusinessToolsRowCount/update - unhandled fieldIdx: ");
                    sb3.append(i6);
                    throw new IllegalArgumentException(sb3.toString());
                }
                SharedPreferences.Editor edit = A04.edit();
                String rawString2 = jid.getRawString();
                StringBuilder sb4 = new StringBuilder();
                sb4.append(rawString2);
                sb4.append("_businessTools");
                edit.putString(sb4.toString(), r33.toString()).apply();
                return;
            case 11:
                C20020v5 r16 = (C20020v5) this.A01;
                int i7 = this.A00;
                r16.A07();
                SharedPreferences A042 = r16.A04();
                String rawString3 = ((Jid) this.A02).getRawString();
                C40581rp A002 = C40581rp.A00(A042.getString(rawString3, "0,0,0,0,0,0,0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null"));
                A002.A01(i7);
                C20020v5.A01(A042, A002, rawString3);
                return;
            case 12:
                C42611vV r63 = (C42611vV) this.A01;
                int i8 = this.A00;
                for (AbstractC14640lm r34 : (Set) this.A02) {
                    ConversationsFragment conversationsFragment = r63.A0F;
                    conversationsFragment.A10.A01(r34, conversationsFragment.A1A.A00());
                }
                ConversationsFragment conversationsFragment2 = r63.A0F;
                conversationsFragment2.A0L.A0F(conversationsFragment2.A02().getQuantityString(R.plurals.pin_toast, i8), 0);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                int i9 = this.A00;
                Runnable runnable = (Runnable) this.A02;
                if (((C19490uC) this.A01).A01.A00.size() == i9) {
                    runnable.run();
                    return;
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                ((C15650ng) this.A01).A0p.A07((AbstractC15340mz) this.A02, this.A00);
                return;
            case 15:
                ((C15650ng) this.A01).A0i((Collection) this.A02, this.A00);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                AbstractC15340mz r64 = (AbstractC15340mz) this.A02;
                int i10 = this.A00;
                C20900wV r8 = ((C15650ng) this.A01).A1A;
                C16490p7 r06 = r8.A00;
                A02 = r06.A02();
                try {
                    AnonymousClass1Lx A003 = A02.A00();
                    r06.A04();
                    if (r06.A05.A0E(A02)) {
                        AnonymousClass1IS r9 = r64.A0z;
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("send_count", Integer.valueOf(i10));
                        AbstractC14640lm r07 = r9.A00;
                        AnonymousClass009.A05(r07);
                        String rawString4 = r07.getRawString();
                        C16330op r35 = A02.A03;
                        String[] strArr = new String[3];
                        strArr[0] = rawString4;
                        if (r9.A02) {
                            str = "1";
                        } else {
                            str = "0";
                        }
                        strArr[1] = str;
                        strArr[2] = r9.A01;
                        r35.A00("messages", contentValues, "key_remote_jid = ? AND key_from_me = ? AND key_id = ?", strArr);
                    }
                    long j = r64.A11;
                    C21390xL r10 = r8.A01;
                    if (r10.A01("send_count_ready", 0) == 1 || r10.A01("migration_message_send_count_index", 0) >= j) {
                        C20900wV.A00(r64);
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put("message_row_id", Long.valueOf(r64.A11));
                        contentValues2.put("send_count", Integer.valueOf(i10));
                        boolean z2 = false;
                        if (A02.A03.A06(contentValues2, "message_send_count", 5) == r64.A11) {
                            z2 = true;
                        }
                        AnonymousClass009.A0C("SendCountMessageStore/insertOrUpdateSendCount/inserted row should have same row_id", z2);
                    }
                    A003.A00();
                    A003.close();
                    return;
                } finally {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                }
            case 17:
                C15650ng r24 = (C15650ng) this.A01;
                AbstractC15340mz r65 = (AbstractC15340mz) this.A02;
                int i11 = this.A00;
                AnonymousClass1IS r36 = r65.A0z;
                if (r36.A02 && !r65.A1B) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    C20870wS r53 = r24.A06;
                    int i12 = r65.A0A;
                    long j2 = uptimeMillis - r65.A16;
                    long j3 = uptimeMillis - r65.A1E;
                    r53.A0G(r65, null, 4, 0, 0, 0, 0, 0, 0, i12, j2, j3, j3, false, false, false, false, false);
                }
                if (r24.A0r(r65, i11)) {
                    C21320xE r08 = r24.A0R;
                    AbstractC14640lm r25 = r36.A00;
                    for (C27151Gf r09 : r08.A01()) {
                        r09.A00(r25);
                    }
                    return;
                }
                return;
            case 18:
                C15650ng r17 = (C15650ng) this.A01;
                AbstractC15340mz r43 = (AbstractC15340mz) this.A02;
                int i13 = this.A00;
                if (r17.A0t(r43, i13)) {
                    Message.obtain(r17.A0e.A02, 2, i13, 0, r43).sendToTarget();
                    return;
                }
                return;
            case 19:
                ((C21380xK) this.A01).A01((AbstractC15340mz) this.A02, this.A00);
                return;
            case C43951xu.A01:
                ((AnonymousClass1BJ) this.A01).A0B((C39451pv) this.A02, new AnonymousClass1VN(1, 987), null, this.A00);
                return;
            case 21:
                C16120oU r26 = (C16120oU) this.A01;
                int i14 = this.A00;
                AbstractC16110oT r37 = (AbstractC16110oT) ((AbstractC16110oT) this.A02).clone();
                if (r26.A0I()) {
                    if (r26.A0O) {
                        if (!r26.A0M) {
                            C14820m6 r66 = r26.A0B;
                            SharedPreferences sharedPreferences = r66.A00;
                            String string2 = sharedPreferences.getString("private_stats_id", null);
                            r26.A0E.A06(string2, C471629h.A00());
                            r26.A0M = true;
                            if (string2 != null && r26.A0C.A07(1070) && !sharedPreferences.getBoolean("events_ps_phase3_migration_done", false)) {
                                r66.A0h(null);
                                AnonymousClass1N8 r67 = r26.A01;
                                if (r67.A04 == 2) {
                                    String absolutePath = r67.A07.getAbsolutePath();
                                    String substring = absolutePath.substring(0, absolutePath.lastIndexOf(File.separatorChar) + 1);
                                    StringBuilder sb5 = new StringBuilder();
                                    sb5.append(substring);
                                    sb5.append("wamprivatestats.wam");
                                    File file = new File(sb5.toString());
                                    if (file.exists()) {
                                        AnonymousClass1N8 r11 = new AnonymousClass1N8(null, file, 3, 0, 2, true);
                                        try {
                                            r11.A03(AnonymousClass2Bc.A00.length - 1);
                                        } catch (AnonymousClass2BT e2) {
                                            Log.e("wambuffer/copyPhase2WamFileToUploadQueue: failed to initialize with new phase 2 file", e2);
                                        }
                                        AnonymousClass1N9 r18 = r11.A00;
                                        if (r18.A8e().A05()) {
                                            Log.i("wambuffer/copyPhase2WamFileToUploadQueue: WAM Buffer is empty, stop migration");
                                        } else {
                                            try {
                                                r11.A02();
                                                if (r11.A01) {
                                                    r11.A01();
                                                }
                                                SparseArray ACP = r18.ACP();
                                                for (int i15 = 0; i15 < ACP.size(); i15++) {
                                                    int keyAt = ACP.keyAt(i15);
                                                    byte[] bArr = (byte[]) ACP.get(keyAt);
                                                    if (bArr.length > 8) {
                                                        int i16 = keyAt + 1000;
                                                        StringBuilder sb6 = new StringBuilder();
                                                        sb6.append(substring);
                                                        sb6.append("wampsid");
                                                        sb6.append(i16);
                                                        try {
                                                            RandomAccessFile randomAccessFile = new RandomAccessFile(new File(sb6.toString()), "rwd");
                                                            C47682By r19 = new C47682By(null, randomAccessFile, i16);
                                                            r19.A04(r19.A03, i16);
                                                            r19.A06(bArr, bArr.length);
                                                            randomAccessFile.close();
                                                        } catch (FileNotFoundException e3) {
                                                            e = e3;
                                                            sb = new StringBuilder();
                                                            str2 = "wamBuffer/writeToPhase3UploadQueue filenotfound exceptions for phase 2 files ";
                                                            sb.append(str2);
                                                            sb.append(e);
                                                            Log.e(sb.toString());
                                                        } catch (IOException e4) {
                                                            e = e4;
                                                            sb = new StringBuilder();
                                                            str2 = "wamBuffer/writeToPhase3UploadQueue filenotfound ioException while reading phase 2 files ";
                                                            sb.append(str2);
                                                            sb.append(e);
                                                            Log.e(sb.toString());
                                                        }
                                                    }
                                                }
                                                RandomAccessFile randomAccessFile2 = r11.A08;
                                                if (randomAccessFile2 != null) {
                                                    try {
                                                        randomAccessFile2.close();
                                                    } catch (IOException unused2) {
                                                        throw new NullPointerException("setWamErrorCloseFile");
                                                    }
                                                }
                                            } catch (Throwable unused3) {
                                                Log.e("wambuffer/copyPhase2WamFileToUploadQueue: Fail to rotate WAM Buffer, stop migration");
                                            }
                                        }
                                        file.delete();
                                    }
                                }
                            }
                        }
                        try {
                            r26.A0F.A02(r26.A0E.A02(r37.psIdKey), 6005, 2);
                        } catch (IllegalArgumentException unused4) {
                            r26.A0D("invalid ps-id key");
                            return;
                        }
                    } else {
                        if (!r26.A0N) {
                            try {
                                AnonymousClass16D r54 = r26.A0E;
                                r26.A0B.A0h(r54.A02(113760892));
                                if (r54.A03 != null) {
                                    r54.A00().clear().apply();
                                    r54.A03.clear();
                                    r54.A03 = null;
                                }
                                r54.A01().edit().clear().apply();
                                AnonymousClass1N8 r55 = r26.A01;
                                if (r55.A04 == 2) {
                                    String absolutePath2 = r55.A07.getAbsolutePath();
                                    int i17 = 0;
                                    String substring2 = absolutePath2.substring(0, absolutePath2.lastIndexOf(File.separatorChar) + 1);
                                    StringBuilder sb7 = new StringBuilder();
                                    sb7.append(substring2);
                                    sb7.append("wamdit3.wam");
                                    File file2 = new File(sb7.toString());
                                    if (file2.exists()) {
                                        file2.delete();
                                    }
                                    while (true) {
                                        StringBuilder sb8 = new StringBuilder();
                                        sb8.append(substring2);
                                        sb8.append("wampsid");
                                        sb8.append(i17);
                                        File file3 = new File(sb8.toString());
                                        if (file3.exists()) {
                                            file3.delete();
                                        }
                                        i17++;
                                        if (i17 < 8) {
                                        }
                                    }
                                }
                            } catch (IllegalArgumentException unused5) {
                            }
                            AnonymousClass15Y r7 = r26.A0F;
                            C14820m6 r68 = r26.A0B;
                            String string3 = r68.A00.getString("private_stats_id", null);
                            if (string3 == null) {
                                string3 = UUID.randomUUID().toString();
                                r68.A0h(string3);
                                r26.A07(new C853742m());
                            }
                            r7.A02(string3, 6005, 2);
                            r26.A0N = true;
                        }
                        if (r37.psIdKey != 113760892) {
                            return;
                        }
                    }
                    r26.A03.A03(r37, i14);
                    r26.A03.A01();
                    r26.A02();
                    return;
                }
                return;
            case 22:
                C16120oU r44 = (C16120oU) this.A01;
                AbstractC16110oT r110 = (AbstractC16110oT) this.A02;
                int i18 = this.A00;
                int i19 = r110.channel;
                AbstractC16110oT r111 = (AbstractC16110oT) r110.clone();
                if (i19 != 1) {
                    r44.A0A(r111, i18, false);
                    return;
                } else if (r44.A0J()) {
                    r44.A04.A03(r111, i18);
                    r44.A04.A01();
                    r44.A03();
                    return;
                } else {
                    return;
                }
            case 23:
                ((AnonymousClass12H) this.A01).A08((AbstractC15340mz) this.A02, this.A00);
                return;
            case 24:
                DeviceJid deviceJid = (DeviceJid) this.A02;
                int i20 = this.A00;
                C22130yZ r27 = ((C43521xA) this.A01).A0I;
                boolean z3 = false;
                if (i20 == 406) {
                    z3 = true;
                }
                r27.A09(deviceJid, z3);
                return;
            case 25:
                ((C20750wG) this.A01).A05.A00(new SyncProfilePictureJob((UserJid[]) ((Set) this.A02).toArray(new UserJid[0]), this.A00));
                return;
            case 26:
                C26701Em r112 = (C26701Em) this.A01;
                AbstractC15340mz r69 = (AbstractC15340mz) this.A02;
                byte b = (byte) this.A00;
                if (b == 56 || b == 68) {
                    C21400xM r45 = r112.A04;
                    HashSet hashSet = new HashSet();
                    if (b == 56) {
                        AnonymousClass1EG r102 = r45.A0H;
                        C40481rf r010 = r69.A0V;
                        if (r010 != null) {
                            Collection<AbstractC15340mz> A022 = r010.A02();
                            HashSet hashSet2 = new HashSet();
                            for (AbstractC15340mz r113 : A022) {
                                if (!r113.A0z.A02 && r113.A0C != 17) {
                                    hashSet2.add(Long.valueOf(r113.A11));
                                }
                            }
                            if (!hashSet2.isEmpty()) {
                                String[] strArr2 = new String[hashSet2.size()];
                                Iterator it = hashSet2.iterator();
                                int i21 = 0;
                                while (it.hasNext()) {
                                    strArr2[i21] = Long.toString(((Number) it.next()).longValue());
                                    i21++;
                                }
                                C29841Uw r114 = new C29841Uw(strArr2, 975);
                                HashSet hashSet3 = new HashSet();
                                A02 = r102.A04.get();
                                try {
                                    Iterator it2 = r114.iterator();
                                    while (it2.hasNext()) {
                                        String[] strArr3 = (String[]) it2.next();
                                        int length = strArr3.length;
                                        StringBuilder sb9 = new StringBuilder("SELECT _id FROM message_add_on WHERE ");
                                        sb9.append("status = ");
                                        sb9.append(17);
                                        sb9.append(" AND ");
                                        sb9.append("from_me = 0");
                                        sb9.append(" AND ");
                                        sb9.append("_id IN ");
                                        sb9.append(AnonymousClass1Ux.A00(length));
                                        Cursor A09 = A02.A03.A09(sb9.toString(), strArr3);
                                        while (A09.moveToNext()) {
                                            hashSet3.add(Long.valueOf(A09.getLong(0)));
                                        }
                                        A09.close();
                                    }
                                    A02.close();
                                    for (AbstractC15340mz r28 : A022) {
                                        if (hashSet3.contains(Long.valueOf(r28.A11))) {
                                            r28.A0Y(17);
                                        }
                                    }
                                } finally {
                                }
                            }
                            for (AbstractC15340mz r115 : A022) {
                                if (!r115.A0z.A02 && r115.A0C != 17) {
                                    hashSet.add(r115);
                                    r115.A0Y(17);
                                }
                            }
                        }
                    } else if (b == 68 && (r2 = r69.A18) != null && !r2.A0z.A02 && r2.A0C != 17) {
                        hashSet.add(r2);
                        r2.A0Y(17);
                    }
                    long A012 = r45.A01(hashSet);
                    AnonymousClass1IS r38 = r69.A0z;
                    if (r38.A02 && (r6 = r38.A00) != null && C21400xM.A0L.contains(Byte.valueOf(b))) {
                        r45.A07.A0F(r6, A012);
                        return;
                    }
                    return;
                }
                return;
            case 27:
                C26701Em r116 = (C26701Em) this.A01;
                AbstractC15340mz r29 = (AbstractC15340mz) this.A02;
                byte b2 = (byte) this.A00;
                r116.A01(r29, b2);
                r116.A03(r29, null, b2, true, false);
                return;
            case 28:
                ((C49512La) this.A01).A00((Integer) this.A02, this.A00);
                return;
            case 29:
                C35271hY r56 = (C35271hY) this.A01;
                int i22 = this.A00;
                for (AbstractC15340mz r210 : (Collection) this.A02) {
                    r56.A00.A0f.A07(r210.A0z);
                }
                StatusPlaybackContactFragment statusPlaybackContactFragment = r56.A00;
                if (i22 >= statusPlaybackContactFragment.A0b.size()) {
                    StatusPlaybackContactFragment.A02(statusPlaybackContactFragment, 4, 6);
                    return;
                }
                statusPlaybackContactFragment.A1L();
                statusPlaybackContactFragment.A00 = -1;
                statusPlaybackContactFragment.A1M(i22);
                statusPlaybackContactFragment.A1P(statusPlaybackContactFragment.A1I(), 4, 6);
                return;
            case C25991Bp.A0S:
                C35261hX r46 = (C35261hX) this.A01;
                int i23 = this.A00;
                AbstractC15340mz r211 = (AbstractC15340mz) this.A02;
                C35551iF r117 = r46.A01;
                r117.A0H();
                if (3 == i23) {
                    r117.A0A().A06();
                    r117.A0M(r117.A0O());
                    if (!r46.A00) {
                        r117.A0F();
                        return;
                    }
                    return;
                } else if (i23 == 12) {
                    r117.A0A().A06();
                    r117.A0M(r117.A0O());
                    if ((r211 instanceof AnonymousClass1X7) && C30041Vv.A0z((AbstractC16130oV) r211)) {
                        if (!r46.A00) {
                            r117.A0F();
                        }
                        r46.A00 = true;
                        return;
                    }
                    return;
                } else {
                    return;
                }
            case 31:
                AbstractC15340mz r72 = (AbstractC15340mz) this.A02;
                int i24 = this.A00;
                C35681iV r57 = ((C35671iU) this.A01).A00;
                if (r57.A01 || r72.A12 < -1) {
                    r57.A0H();
                    if (3 == i24) {
                        r57.A0A().A06();
                        r57.A0M(r57.A0O());
                        r57.A0F();
                    }
                } else {
                    r57.A0Q();
                }
                if (C37381mH.A00(r72.A0C, 4) > 0) {
                    C35691iW r118 = r57.A00;
                    if (r118 != null) {
                        r118.A03(true);
                    }
                    C35691iW r212 = new C35691iW(r57);
                    r57.A00 = r212;
                    r57.A0R.Aaz(r212, new Void[0]);
                    return;
                }
                return;
            case 32:
                C18370sL r119 = (C18370sL) this.A01;
                Context context = (Context) this.A02;
                int i25 = this.A00;
                if (r119.A00.A00 == null) {
                    Log.i("banmanager/startPermanentBanFlow/showLoginFailureNotificationIfNeeded");
                    C18350sJ r47 = r119.A06;
                    r47.A0m.Ab2(new RunnableBRunnable0Shape7S0200000_I0_7(r47, 31, r47.A0L.A00));
                    return;
                }
                Log.i("banmanager/startPermanentBanFlow/launching-banappeals");
                Intent intent3 = new Intent();
                intent3.setClassName(context.getPackageName(), "com.whatsapp.userban.ui.BanAppealActivity");
                intent3.putExtra("appeal_request_token", (String) null);
                intent3.putExtra("ban_violation_type", i25);
                intent3.putExtra("launch_source", 3);
                intent3.setFlags(268468224);
                context.startActivity(intent3);
                return;
            default:
                return;
        }
    }
}
