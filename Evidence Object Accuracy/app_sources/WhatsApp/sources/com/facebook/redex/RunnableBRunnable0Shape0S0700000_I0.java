package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC41531tg;
import X.AnonymousClass19O;
import X.AnonymousClass1E5;
import X.AnonymousClass1PE;
import X.C14900mE;
import X.C16310on;
import X.C16460p3;
import X.C16510p9;
import X.C241214h;
import X.C28181Ma;
import android.database.Cursor;
import android.graphics.Bitmap;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0700000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public Object A06;
    public final int A07;

    public RunnableBRunnable0Shape0S0700000_I0(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, int i) {
        this.A07 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A04 = obj5;
        this.A05 = obj6;
        this.A06 = obj7;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Bitmap bitmap;
        switch (this.A07) {
            case 0:
                CommunityTabViewModel communityTabViewModel = (CommunityTabViewModel) this.A00;
                C14900mE r7 = (C14900mE) this.A01;
                Object obj = this.A02;
                Object obj2 = this.A03;
                Object obj3 = this.A04;
                Object obj4 = this.A05;
                Object obj5 = this.A06;
                C241214h r1 = communityTabViewModel.A08.A09;
                C28181Ma r14 = new C28181Ma("CommunityChatStore/getCommunityChats");
                C16510p9 r13 = r1.A00;
                C16310on A01 = r13.A04.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT _id FROM chat WHERE group_type = ?", new String[]{Integer.toString(1)});
                    ArrayList arrayList = new ArrayList(A09.getCount());
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
                    while (A09.moveToNext()) {
                        arrayList.add(Long.valueOf(A09.getLong(columnIndexOrThrow)));
                    }
                    A09.close();
                    A01.close();
                    ArrayList arrayList2 = new ArrayList(arrayList.size());
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        long longValue = ((Number) it.next()).longValue();
                        AbstractC14640lm A05 = r13.A05(longValue);
                        if (A05 == null) {
                            StringBuilder sb = new StringBuilder("CommunityChatStore/failed to find chatJid by row id: ");
                            sb.append(longValue);
                            Log.w(sb.toString());
                        } else {
                            arrayList2.add(A05);
                        }
                    }
                    r14.A01();
                    Iterator it2 = arrayList2.iterator();
                    while (it2.hasNext()) {
                        AnonymousClass1PE A06 = communityTabViewModel.A0B.A06((AbstractC14640lm) it2.next());
                        if (A06 != null) {
                            List A052 = communityTabViewModel.A05(A06, communityTabViewModel.A05);
                            if (A052 != null) {
                                Collections.sort(A052, CommunityTabViewModel.A0S);
                                communityTabViewModel.A04.put(A06, A052);
                                if (A052.size() == 1) {
                                }
                            }
                            AnonymousClass1E5 r11 = communityTabViewModel.A0L;
                            int i = A06.A01;
                            if (i == 3 || i == 1) {
                                if (r11.A00(r11.A00.A0B(A06.A05()))) {
                                    communityTabViewModel.A06.add(A06);
                                    Map map = communityTabViewModel.A04;
                                    if (map.get(A06) == null) {
                                        map.put(A06, Collections.emptyList());
                                    }
                                }
                            }
                        }
                    }
                    communityTabViewModel.A06(true);
                    r7.A0H(new RunnableBRunnable0Shape0S0600000_I0(communityTabViewModel, obj2, obj, obj4, obj5, obj3, 2));
                    return;
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            case 1:
                AnonymousClass19O r4 = (AnonymousClass19O) this.A00;
                AbstractC41531tg r3 = (AbstractC41531tg) this.A02;
                Object obj6 = this.A03;
                Object obj7 = this.A04;
                Object obj8 = this.A05;
                Object obj9 = this.A06;
                byte[] A07 = ((C16460p3) this.A01).A07();
                if (A07 == null || A07.length <= 0) {
                    bitmap = null;
                } else {
                    bitmap = r3.A00();
                }
                RunnableBRunnable0Shape0S0500000_I0 runnableBRunnable0Shape0S0500000_I0 = new RunnableBRunnable0Shape0S0500000_I0(obj7, bitmap, obj8, obj9, obj6, 4);
                List list = r4.A06;
                synchronized (list) {
                    list.add(runnableBRunnable0Shape0S0500000_I0);
                }
                r4.A01.post(new RunnableBRunnable0Shape13S0100000_I0_13(r4, 15));
                r3.A01();
                return;
            default:
                return;
        }
    }
}
