package com.facebook.redex;

import X.AbstractActivityC13750kH;
import X.AbstractC14640lm;
import X.ActivityC13810kN;
import X.AnonymousClass11W;
import X.AnonymousClass1GD;
import X.AnonymousClass1JU;
import X.AnonymousClass2KO;
import X.AnonymousClass2KP;
import X.C15380n4;
import X.C16310on;
import X.C18460sU;
import X.C22100yW;
import X.C246216f;
import X.C28601Of;
import X.C40971sf;
import android.content.ContentValues;
import com.whatsapp.Conversation;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import java.util.Set;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0200100_I0 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0200100_I0(Object obj, Object obj2, int i, long j) {
        this.A03 = i;
        this.A01 = obj;
        this.A02 = obj2;
        this.A00 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C16310on A02;
        AnonymousClass1JU r0;
        switch (this.A03) {
            case 0:
                Conversation conversation = (Conversation) this.A01;
                AbstractC14640lm r3 = (AbstractC14640lm) this.A02;
                long j = this.A00;
                Set A04 = ((AbstractActivityC13750kH) conversation).A0N.A04(r3);
                conversation.A2h.A08(r3, 3, C15380n4.A09(((ActivityC13810kN) conversation).A03, A04).size(), A04.size(), j);
                return;
            case 1:
                ((AnonymousClass2KO) this.A01).A00.A01((AnonymousClass2KP) this.A02, this.A00, true);
                return;
            case 2:
            case 3:
            case 4:
                ((C18460sU) this.A01).A0B((Jid) this.A02, this.A00);
                return;
            case 5:
                C22100yW r10 = (C22100yW) this.A01;
                Object obj = this.A02;
                long j2 = this.A00;
                if (r10.A0L.A03()) {
                    C246216f r9 = r10.A0I.A04;
                    AnonymousClass1JU r02 = (AnonymousClass1JU) r9.A00().A00.get(obj);
                    if (r02 != null) {
                        r02.A00 = j2;
                        DeviceJid deviceJid = r02.A05;
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("last_active", Long.valueOf(j2));
                        A02 = r9.A02.A02();
                        try {
                            A02.A03.A00("devices", contentValues, "device_id = ?", new String[]{deviceJid.getRawString()});
                            synchronized (r9) {
                                C28601Of r03 = r9.A00;
                                if (!(r03 == null || (r0 = (AnonymousClass1JU) r03.A00.get(deviceJid)) == null)) {
                                    r0.A00 = j2;
                                }
                            }
                            A02.close();
                            for (AnonymousClass1GD r1 : r10.A01()) {
                                if (r1 instanceof C40971sf) {
                                    ((C40971sf) r1).A00.A0R.A0A(null);
                                }
                            }
                            return;
                        } finally {
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 6:
                Jid jid = (Jid) this.A02;
                long j3 = this.A00;
                A02 = ((AnonymousClass11W) this.A01).A01.A01.A02();
                try {
                    ContentValues contentValues2 = new ContentValues(2);
                    contentValues2.put("chat_jid", jid.getRawString());
                    contentValues2.put("timestamp", Long.valueOf(j3));
                    A02.A03.A04(contentValues2, "dismissed_chat");
                    return;
                } finally {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                }
            default:
                return;
        }
    }
}
