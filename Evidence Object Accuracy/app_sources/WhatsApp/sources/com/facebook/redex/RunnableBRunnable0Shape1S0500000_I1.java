package com.facebook.redex;

import X.AbstractC14200l1;
import X.AbstractC14440lR;
import X.AnonymousClass28D;
import X.C14210l2;
import X.C14260l7;
import X.C28701Oq;
import android.view.View;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0500000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public final int A05;

    public RunnableBRunnable0Shape1S0500000_I1(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i) {
        this.A05 = i;
        this.A04 = obj;
        this.A00 = obj2;
        this.A02 = obj3;
        this.A01 = obj4;
        this.A03 = obj5;
    }

    @Override // java.lang.Runnable
    public void run() {
        switch (this.A05) {
            case 0:
                ((View) this.A04).setTag(R.id.render_lifecycle_extension_runnable, null);
                AnonymousClass28D r4 = (AnonymousClass28D) this.A00;
                AbstractC14200l1 A0G = ((AnonymousClass28D) this.A02).A0G(35);
                C14210l2 A00 = C14210l2.A00(r4);
                C14260l7 r1 = (C14260l7) this.A01;
                C28701Oq.A01(r1, r4, C14210l2.A01(A00, r1, 1), A0G);
                Runnable runnable = (Runnable) this.A03;
                if (runnable != null) {
                    runnable.run();
                    return;
                }
                return;
            case 1:
                Object obj = this.A00;
                ((AbstractC14440lR) this.A01).Ab6(new RunnableBRunnable0Shape1S0400000_I1(this.A03, this.A04, this.A02, obj, 3));
                return;
            default:
                return;
        }
    }
}
