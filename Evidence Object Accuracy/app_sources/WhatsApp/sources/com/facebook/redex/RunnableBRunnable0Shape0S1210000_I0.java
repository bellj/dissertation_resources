package com.facebook.redex;

import X.C238013b;
import X.C40511ri;
import X.C40521rj;
import X.C43761xY;
import com.whatsapp.deeplink.DeepLinkActivity;
import com.whatsapp.jid.UserJid;
import java.util.Set;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1210000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public String A02;
    public boolean A03;
    public final int A04;

    public RunnableBRunnable0Shape0S1210000_I0(Object obj, Object obj2, String str, int i, boolean z) {
        this.A04 = i;
        this.A00 = obj;
        this.A03 = z;
        this.A01 = obj2;
        this.A02 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        String str;
        switch (this.A04) {
            case 0:
                C43761xY r4 = (C43761xY) this.A00;
                boolean z = this.A03;
                Set set = (Set) this.A01;
                String str2 = this.A02;
                C238013b r1 = r4.A05;
                if (z) {
                    r1.A0F(null);
                } else {
                    r1.A0G(str2, set);
                }
                r4.A0E.run();
                return;
            case 1:
                DeepLinkActivity deepLinkActivity = (DeepLinkActivity) this.A00;
                UserJid userJid = (UserJid) this.A01;
                String str3 = this.A02;
                if (this.A03) {
                    str = "whatsapp";
                } else {
                    str = null;
                }
                deepLinkActivity.A0G.A00(new C40511ri(new C40521rj(userJid, str3, str, System.currentTimeMillis(), System.currentTimeMillis())));
                return;
            default:
                return;
        }
    }
}
