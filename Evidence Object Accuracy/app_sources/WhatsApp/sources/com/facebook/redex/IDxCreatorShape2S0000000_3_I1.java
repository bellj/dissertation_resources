package com.facebook.redex;

import X.AbstractC121025h8;
import X.AbstractC128515wE;
import X.AbstractC1316063k;
import X.AbstractC28901Pl;
import X.AbstractC30851Zb;
import X.AbstractC30871Zd;
import X.AnonymousClass009;
import X.AnonymousClass102;
import X.AnonymousClass1ZR;
import X.AnonymousClass1ZX;
import X.AnonymousClass60R;
import X.AnonymousClass63X;
import X.AnonymousClass63Y;
import X.AnonymousClass63Z;
import X.AnonymousClass686;
import X.AnonymousClass6F2;
import X.C117305Zk;
import X.C117565aA;
import X.C119695ex;
import X.C119705ey;
import X.C119715ez;
import X.C119725f0;
import X.C119735f1;
import X.C119745f2;
import X.C119755f3;
import X.C119765f4;
import X.C119775f5;
import X.C119785f6;
import X.C119795f7;
import X.C119805f8;
import X.C119815f9;
import X.C119825fA;
import X.C119835fB;
import X.C120935gz;
import X.C120945h0;
import X.C120985h4;
import X.C120995h5;
import X.C121005h6;
import X.C121015h7;
import X.C121035h9;
import X.C128505wD;
import X.C12960it;
import X.C12970iu;
import X.C12990iw;
import X.C1315063a;
import X.C1315163b;
import X.C1315263c;
import X.C1315363d;
import X.C1315463e;
import X.C1315563f;
import X.C1315663g;
import X.C1315763h;
import X.C1315863i;
import X.C1316163l;
import X.C1316263m;
import X.C1316363n;
import X.C1316463o;
import X.C1316563p;
import X.C1316663q;
import X.C1316763r;
import X.C25991Bp;
import X.C30821Yy;
import X.C43951xu;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import org.chromium.net.UrlRequest;

/* loaded from: classes4.dex */
public class IDxCreatorShape2S0000000_3_I1 implements Parcelable.Creator {
    public final int A00;

    public IDxCreatorShape2S0000000_3_I1(int i) {
        this.A00 = i;
    }

    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        Boolean bool;
        AbstractC128515wE r4;
        switch (this.A00) {
            case 0:
                return new C117565aA(parcel);
            case 1:
                C119795f7 r5 = new C119795f7();
                r5.A08 = parcel.readString();
                ((AnonymousClass1ZX) r5).A02 = parcel.readString();
                r5.A06 = parcel.readString();
                r5.A03 = parcel.readString();
                r5.A04 = parcel.readString();
                r5.A00 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r5.A02 = parcel.readString();
                r5.A01 = (C119755f3) C12990iw.A0I(parcel, C119755f3.class);
                return r5;
            case 2:
                return new C1316463o(parcel.readString(), parcel.readString(), parcel.readInt(), parcel.readLong());
            case 3:
                return new C119725f0(parcel);
            case 4:
                return new C119705ey(parcel);
            case 5:
                C119755f3 r52 = new C119755f3();
                r52.A0A = parcel.readString();
                r52.A03 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r52.A05 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r52.A07 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r52.A04 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r52.A08 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                boolean z = false;
                r52.A0H = C12960it.A1V(parcel.readInt(), 1);
                r52.A06 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                ((AbstractC30851Zb) r52).A03 = parcel.readString();
                ((AbstractC30851Zb) r52).A04 = parcel.readString();
                r52.A09 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r52.A0F = parcel.readString();
                r52.A0C = parcel.readString();
                r52.A01 = parcel.readInt();
                r52.A0D = parcel.readString();
                r52.A0E = parcel.readString();
                ArrayList A0l = C12960it.A0l();
                r52.A0G = A0l;
                parcel.readStringList(A0l);
                int readInt = parcel.readInt();
                if (readInt != 0) {
                    byte[] bArr = new byte[readInt];
                    ((AbstractC30851Zb) r52).A09 = bArr;
                    parcel.readByteArray(bArr);
                }
                ((AbstractC30851Zb) r52).A06 = parcel.readString();
                ((AbstractC30851Zb) r52).A01 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                ((AbstractC30851Zb) r52).A02 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                ((AbstractC30851Zb) r52).A00 = parcel.readLong();
                ((AbstractC30851Zb) r52).A07 = C12960it.A1V(parcel.readInt(), 1);
                ((AbstractC30851Zb) r52).A08 = C12960it.A1V(parcel.readInt(), 1);
                r52.A0B = parcel.readString();
                if (parcel.readInt() == 1) {
                    z = true;
                }
                r52.A0I = z;
                return r52;
            case 6:
                Bundle readBundle = parcel.readBundle(getClass().getClassLoader());
                C119715ez r53 = new C119715ez();
                r53.A00 = readBundle;
                return r53;
            case 7:
                return new AnonymousClass686(parcel);
            case 8:
                C119835fB r54 = new C119835fB();
                r54.A0P(parcel);
                r54.A0A = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r54.A0P = parcel.readString();
                r54.A0N = parcel.readString();
                r54.A0L = parcel.readString();
                r54.A0M = parcel.readString();
                r54.A08 = C117305Zk.A0I(C117305Zk.A0J(), String.class, parcel.readString(), "legalName");
                r54.A0J = parcel.readString();
                r54.A0K = parcel.readString();
                r54.A07 = C117305Zk.A0I(C117305Zk.A0J(), String.class, parcel.readString(), "legalName");
                r54.A05 = parcel.readLong();
                r54.A0E = parcel.readString();
                r54.A04 = parcel.readLong();
                r54.A01 = parcel.readInt();
                r54.A00 = parcel.readInt();
                r54.A02 = parcel.readInt();
                r54.A0Q = parcel.readString();
                r54.A09 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r54.A0F = parcel.readString();
                r54.A0O = parcel.readString();
                r54.A0H = parcel.readString();
                r54.A0I = parcel.readString();
                String readString = parcel.readString();
                if (readString != null) {
                    r54.A0B = new AnonymousClass60R(readString);
                }
                int readInt2 = parcel.readInt();
                if (readInt2 > 0) {
                    bool = Boolean.TRUE;
                } else {
                    bool = readInt2 == 0 ? Boolean.FALSE : null;
                }
                r54.A0D = bool;
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    r54.A0C = new AnonymousClass686(readString2);
                }
                r54.A0G = parcel.readString();
                r54.A06 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                return r54;
            case 9:
                ClassLoader classLoader = C1315463e.class.getClassLoader();
                return new C1315463e((AnonymousClass63Y) C117305Zk.A03(parcel, classLoader), (C1316263m) C117305Zk.A03(parcel, classLoader), C12990iw.A0p(parcel), C12990iw.A0p(parcel), C12990iw.A0p(parcel));
            case 10:
                C119745f2 r55 = new C119745f2();
                ((AbstractC30851Zb) r55).A01 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r55.A02 = parcel.readString();
                r55.A00 = parcel.readInt();
                r55.A03 = parcel.readString();
                ((AbstractC30851Zb) r55).A03 = parcel.readString();
                r55.A04 = C12970iu.A1W(parcel.readInt());
                r55.A05 = parcel.readString();
                r55.A06 = parcel.readString();
                ((AbstractC30851Zb) r55).A02 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                ((AbstractC30851Zb) r55).A00 = parcel.readLong();
                return r55;
            case 11:
                C119775f5 r56 = new C119775f5();
                boolean z2 = false;
                r56.A0a = C12960it.A1V(parcel.readByte(), 1);
                r56.A08 = (AnonymousClass1ZR) C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                r56.A0B = parcel.readString();
                r56.A0A = parcel.readString();
                r56.A0O = parcel.readString();
                r56.A0Q = C12960it.A1V(parcel.readByte(), 1);
                ((AbstractC30871Zd) r56).A03 = parcel.readInt();
                r56.A0Y = C12960it.A1V(parcel.readByte(), 1);
                r56.A0U = C12960it.A1V(parcel.readByte(), 1);
                ((AbstractC30871Zd) r56).A06 = parcel.readLong();
                ((AbstractC30871Zd) r56).A04 = parcel.readInt();
                r56.A0G = parcel.readString();
                r56.A0H = parcel.readString();
                ((AbstractC30871Zd) r56).A00 = parcel.readInt();
                r56.A0W = C12960it.A1V(parcel.readByte(), 1);
                r56.A0V = C12960it.A1V(parcel.readByte(), 1);
                r56.A0S = C12960it.A1V(parcel.readByte(), 1);
                r56.A0R = C12960it.A1V(parcel.readByte(), 1);
                r56.A0J = parcel.readString();
                ((AbstractC30871Zd) r56).A05 = parcel.readLong();
                ((AbstractC30871Zd) r56).A01 = parcel.readInt();
                if (parcel.readByte() == 1) {
                    z2 = true;
                }
                r56.A07 = z2;
                r56.A03 = parcel.readString();
                r56.A06 = parcel.readString();
                r56.A00 = parcel.readInt();
                r56.A04 = parcel.readString();
                r56.A01 = parcel.readInt();
                r56.A0C = parcel.readString();
                r56.A0E = parcel.readString();
                r56.A0D = parcel.readString();
                r56.A09 = Long.valueOf(parcel.readLong());
                r56.A05 = parcel.readString();
                r56.A0F = parcel.readString();
                r56.A0I = parcel.readString();
                boolean z3 = false;
                r56.A0P = C12960it.A1V(parcel.readByte(), 1);
                r56.A0Z = C12960it.A1V(parcel.readByte(), 1);
                r56.A0X = C12960it.A1V(parcel.readByte(), 1);
                if (parcel.readByte() == 1) {
                    z3 = true;
                }
                r56.A0T = z3;
                r56.A0N = parcel.readString();
                r56.A0M = parcel.readString();
                r56.A0L = parcel.readString();
                r56.A0K = parcel.readString();
                return r56;
            case 12:
                return new C119695ex(parcel);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C119785f6 r57 = new C119785f6();
                ((AnonymousClass1ZX) r57).A00 = parcel.readInt();
                r57.A08 = parcel.readString();
                r57.A0B = parcel.readString();
                ((AnonymousClass1ZX) r57).A02 = parcel.readString();
                r57.A02 = parcel.readString();
                r57.A06 = parcel.readString();
                ((AnonymousClass1ZX) r57).A03 = parcel.readString();
                r57.A04 = parcel.readString();
                ((AnonymousClass1ZX) r57).A01 = parcel.readLong();
                r57.A00 = parcel.readInt();
                r57.A01 = parcel.readString();
                r57.A05 = parcel.readString();
                r57.A03 = parcel.readString();
                boolean z4 = false;
                r57.A0D = C12960it.A1V(parcel.readByte(), 1);
                if (parcel.readByte() == 1) {
                    z4 = true;
                }
                r57.A0E = z4;
                r57.A0A = parcel.readString();
                r57.A07 = parcel.readString();
                return r57;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                C119815f9 r58 = new C119815f9();
                r58.A0P(parcel);
                r58.A04 = parcel.readString();
                r58.A03 = parcel.readString();
                r58.A02 = parcel.readString();
                r58.A01 = (Boolean) parcel.readSerializable();
                return r58;
            case 15:
                return new AnonymousClass6F2(AnonymousClass102.A00(parcel), (C30821Yy) C117305Zk.A03(parcel, AnonymousClass6F2.class.getClassLoader()));
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                parcel.readString();
                return new AnonymousClass63Y(AnonymousClass102.A00(parcel), AnonymousClass102.A00(parcel), parcel.readString());
            case 17:
                ClassLoader classLoader2 = C1315863i.class.getClassLoader();
                return new C1315863i((AnonymousClass63Y) C117305Zk.A03(parcel, classLoader2), (AnonymousClass63Y) C117305Zk.A03(parcel, classLoader2), (C1316563p) C117305Zk.A03(parcel, classLoader2), (C1316563p) parcel.readParcelable(classLoader2), (C1316563p) C117305Zk.A03(parcel, classLoader2), (C1316563p) C117305Zk.A03(parcel, classLoader2));
            case 18:
                long readLong = parcel.readLong();
                long readLong2 = parcel.readLong();
                Parcelable A0I = C12990iw.A0I(parcel, C1316563p.class);
                AnonymousClass009.A05(A0I);
                Serializable readSerializable = parcel.readSerializable();
                AnonymousClass009.A05(readSerializable);
                return new C1316563p((AnonymousClass6F2) A0I, C12990iw.A0p(parcel), C12990iw.A0p(parcel), (BigDecimal) readSerializable, readLong, readLong2);
            case 19:
                ClassLoader classLoader3 = C1316263m.class.getClassLoader();
                return new C1316263m((AnonymousClass6F2) C117305Zk.A03(parcel, classLoader3), (AnonymousClass6F2) C117305Zk.A03(parcel, classLoader3), parcel.readLong());
            case C43951xu.A01:
                C119735f1 r59 = new C119735f1();
                ((AbstractC30851Zb) r59).A05 = parcel.readString();
                r59.A06 = parcel.readString();
                ((AbstractC30851Zb) r59).A00 = parcel.readLong();
                boolean z5 = false;
                r59.A08 = C12960it.A1V(parcel.readInt(), 1);
                r59.A07 = C12960it.A1V(parcel.readInt(), 1);
                r59.A04 = C12990iw.A0p(parcel);
                Parcelable A0I2 = C12990iw.A0I(parcel, AnonymousClass1ZR.class);
                AnonymousClass009.A05(A0I2);
                r59.A01 = (AnonymousClass1ZR) A0I2;
                r59.A03 = C12990iw.A0p(parcel);
                r59.A02 = C12990iw.A0p(parcel);
                if (1 == parcel.readInt()) {
                    z5 = true;
                }
                r59.A05 = z5;
                r59.A00 = parcel.readLong();
                return r59;
            case 21:
                C119765f4 r510 = new C119765f4();
                r510.A0G = parcel.readString();
                r510.A0H = parcel.readString();
                ((AbstractC30871Zd) r510).A00 = parcel.readInt();
                ((AbstractC30871Zd) r510).A05 = parcel.readLong();
                ((AbstractC30871Zd) r510).A01 = parcel.readInt();
                boolean z6 = false;
                r510.A0W = C12960it.A1V(parcel.readInt(), 1);
                r510.A0V = C12960it.A1V(parcel.readInt(), 1);
                r510.A0S = C12960it.A1V(parcel.readInt(), 1);
                r510.A0R = C12960it.A1V(parcel.readInt(), 1);
                r510.A0I = C12990iw.A0p(parcel);
                r510.A05 = C12990iw.A0p(parcel);
                r510.A04 = C12990iw.A0p(parcel);
                r510.A03 = C12990iw.A0p(parcel);
                r510.A02 = new C128505wD(parcel.readString());
                r510.A01 = new C128505wD(parcel.readString());
                if (1 == parcel.readInt()) {
                    z6 = true;
                }
                r510.A06 = z6;
                r510.A00 = parcel.readLong();
                return r510;
            case 22:
                return new C119805f8(parcel);
            case 23:
                return new C1316163l(parcel);
            case 24:
                return new C121035h9(parcel);
            case 25:
                if (parcel.readInt() == 0) {
                    r4 = new C120935gz(parcel.readString(), parcel.readString());
                } else {
                    r4 = new C120945h0(new C128505wD(parcel.readString()), new C128505wD(parcel.readString()), C12990iw.A0p(parcel), parcel.readInt());
                }
                Parcelable A0I3 = C12990iw.A0I(parcel, C1316563p.class);
                AnonymousClass009.A05(A0I3);
                Parcelable A0I4 = C12990iw.A0I(parcel, C1316363n.class);
                AnonymousClass009.A05(A0I4);
                return new C1315063a((C1316563p) A0I3, r4, (C1316363n) A0I4, parcel.readInt());
            case 26:
                return new C120995h5(parcel);
            case 27:
                ClassLoader classLoader4 = C1315163b.class.getClassLoader();
                AnonymousClass63Z r9 = (AnonymousClass63Z) C117305Zk.A03(parcel, classLoader4);
                return new C1315163b((C1316463o) parcel.readParcelable(classLoader4), (AnonymousClass6F2) parcel.readParcelable(classLoader4), (C1316563p) C117305Zk.A03(parcel, classLoader4), r9, (C1316763r) parcel.readParcelable(classLoader4), (C1315263c) C117305Zk.A03(parcel, classLoader4), parcel.readString());
            case 28:
                Parcelable A0I5 = C12990iw.A0I(parcel, AbstractC28901Pl.class);
                AnonymousClass009.A05(A0I5);
                Parcelable A0I6 = C12990iw.A0I(parcel, C1316363n.class);
                AnonymousClass009.A05(A0I6);
                return new AnonymousClass63X((AbstractC28901Pl) A0I5, (C1316363n) A0I6);
            case 29:
                C119825fA r511 = new C119825fA();
                r511.A0P(parcel);
                r511.A03 = parcel.readString();
                r511.A01 = (AbstractC1316063k) C12990iw.A0I(parcel, C119825fA.class);
                return r511;
            case C25991Bp.A0S:
                return new C120985h4(parcel);
            case 31:
                throw new BadParcelableException("NoviTransactionWithdrawal is abstract class, should not initiate from Parcel, please refer its sub class");
            case 32:
                return new C121005h6(parcel);
            case 33:
                return new C121015h7(parcel);
            case 34:
                return new AnonymousClass63Z((UserJid) C12990iw.A0I(parcel, UserJid.class), (C1316363n) C117305Zk.A03(parcel, AnonymousClass63Z.class.getClassLoader()), parcel.readString());
            case 35:
                return new C1316763r(C1316763r.A00(parcel.readString()), parcel.readInt());
            case 36:
                String readString3 = parcel.readString();
                Parcelable A0I7 = C12990iw.A0I(parcel, C1315263c.class);
                AnonymousClass009.A05(A0I7);
                return new C1315263c((C1316363n) A0I7, readString3);
            case 37:
                ClassLoader classLoader5 = C1316363n.class.getClassLoader();
                return new C1316363n((AnonymousClass6F2) C117305Zk.A03(parcel, classLoader5), (AnonymousClass6F2) C117305Zk.A03(parcel, classLoader5), parcel.readLong());
            case 38:
                return new C1316663q(parcel);
            case 39:
                return new C1315763h(parcel);
            case 40:
                return new C1315563f(parcel);
            case 41:
                return new C1315363d(parcel);
            case 42:
                return new C1315663g(parcel);
            default:
                return null;
        }
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        switch (this.A00) {
            case 0:
                return new C117565aA[i];
            case 1:
                return new C119795f7[i];
            case 2:
                return new C1316463o[i];
            case 3:
                return new C119725f0[i];
            case 4:
                return new C119705ey[i];
            case 5:
                return new C119755f3[i];
            case 6:
                return new C119715ez[i];
            case 7:
                return new AnonymousClass686[i];
            case 8:
                return new C119835fB[i];
            case 9:
                return new C1315463e[i];
            case 10:
                return new C119745f2[i];
            case 11:
                return new C119775f5[i];
            case 12:
                return new C119695ex[i];
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new C119785f6[i];
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new C119815f9[0];
            case 15:
                return new AnonymousClass6F2[i];
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new AnonymousClass63Y[i];
            case 17:
                return new C1315863i[i];
            case 18:
                return new C1316563p[i];
            case 19:
                return new C1316263m[i];
            case C43951xu.A01:
                return new C119735f1[i];
            case 21:
                return new C119765f4[i];
            case 22:
                return new C119805f8[i];
            case 23:
                return new C1316163l[i];
            case 24:
                return new C121035h9[i];
            case 25:
                return new C1315063a[i];
            case 26:
                return new C120995h5[i];
            case 27:
                return new C1315163b[i];
            case 28:
                return new AnonymousClass63X[i];
            case 29:
                return new C119825fA[i];
            case C25991Bp.A0S:
                return new C120985h4[i];
            case 31:
                return new AbstractC121025h8[i];
            case 32:
                return new C121005h6[i];
            case 33:
                return new C121015h7[i];
            case 34:
                return new AnonymousClass63Z[i];
            case 35:
                return new C1316763r[i];
            case 36:
                return new C1315263c[i];
            case 37:
                return new C1316363n[i];
            case 38:
                return new C1316663q[i];
            case 39:
                return new C1315763h[i];
            case 40:
                return new C1315563f[i];
            case 41:
                return new C1315363d[i];
            case 42:
                return new C1315663g[i];
            default:
                return new Object[0];
        }
    }
}
