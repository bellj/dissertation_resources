package com.facebook.redex;

import android.view.View;

/* loaded from: classes4.dex */
public class IDxCListenerShape2S0200000_3_I1 implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public final int A02;

    public IDxCListenerShape2S0200000_3_I1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = obj2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:111:0x028f  */
    /* JADX WARNING: Removed duplicated region for block: B:308:? A[RETURN, SYNTHETIC] */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r31) {
        /*
        // Method dump skipped, instructions count: 2294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxCListenerShape2S0200000_3_I1.onClick(android.view.View):void");
    }
}
