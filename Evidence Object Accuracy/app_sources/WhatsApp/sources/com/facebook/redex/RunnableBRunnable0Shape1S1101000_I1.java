package com.facebook.redex;

import X.ActivityC13810kN;
import X.AnonymousClass2GV;
import X.AnonymousClass34O;
import X.AnonymousClass3ZF;
import X.C1095652e;
import X.C12960it;
import X.C12970iu;
import android.os.SystemClock;
import com.whatsapp.R;
import com.whatsapp.qrcode.contactqr.ContactQrContactCardView;
import com.whatsapp.qrcode.contactqr.ContactQrMyCodeFragment;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S1101000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public String A02;
    public final int A03;

    public RunnableBRunnable0Shape1S1101000_I1(Object obj, String str, int i, int i2) {
        this.A03 = i2;
        this.A01 = obj;
        this.A02 = str;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        int i;
        switch (this.A03) {
            case 0:
                AnonymousClass34O r3 = (AnonymousClass34O) this.A01;
                String str = this.A02;
                int i2 = this.A00;
                r3.A0X = false;
                r3.AaN();
                if (str != null) {
                    r3.A0U = str;
                    ContactQrMyCodeFragment contactQrMyCodeFragment = r3.A0R;
                    if (contactQrMyCodeFragment != null) {
                        contactQrMyCodeFragment.A02 = str;
                        ContactQrContactCardView contactQrContactCardView = contactQrMyCodeFragment.A01;
                        if (contactQrContactCardView != null) {
                            contactQrContactCardView.setQrCode(C12960it.A0d(str, C12960it.A0k("https://wa.me/qr/")));
                        }
                    }
                    if (r3.A01) {
                        i = R.string.contact_qr_revoke_success;
                    } else {
                        return;
                    }
                } else if (i2 == 0) {
                    if (r3.A0U == null) {
                        ((ActivityC13810kN) r3).A05.A07(R.string.no_internet_message, 1);
                        return;
                    }
                    return;
                } else if (r3.A01) {
                    i = R.string.contact_qr_revoke_failure;
                } else {
                    r3.A2L(new AnonymousClass2GV() { // from class: X.52g
                        @Override // X.AnonymousClass2GV
                        public final void AO1() {
                            AnonymousClass34O.this.A2g(false);
                        }
                    }, new C1095652e(r3), R.string.contact_qr_try_again, R.string.contact_qr_failed_title, R.string.contact_qr_try_again, R.string.contact_qr_failed_go_back);
                    return;
                }
                r3.Ado(i);
                return;
            case 1:
                String str2 = this.A02;
                int i3 = this.A00;
                AnonymousClass34O r2 = (AnonymousClass34O) ((AnonymousClass3ZF) this.A01).A02.A01.get();
                if (r2 != null) {
                    if (!(str2 == null && i3 == 0)) {
                        C12970iu.A1D(C12960it.A08(((ActivityC13810kN) r2).A09), "contact_qr_code", str2);
                    }
                    long elapsedRealtime = SystemClock.elapsedRealtime() - r2.A00;
                    ((ActivityC13810kN) r2).A05.A0J(new RunnableBRunnable0Shape1S1101000_I1(r2, str2, i3, 0), elapsedRealtime < 500 ? 500 - elapsedRealtime : 0);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
