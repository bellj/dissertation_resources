package com.facebook.redex;

import X.C14960mK;
import android.view.View;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0110000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public boolean A01;
    public final int A02;

    public ViewOnClickCListenerShape0S0110000_I0(Object obj, int i, boolean z) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = z;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A02) {
            case 0:
                RestoreFromBackupActivity restoreFromBackupActivity = (RestoreFromBackupActivity) this.A00;
                boolean z = this.A01;
                restoreFromBackupActivity.A0a.A02("backup_found", "restore");
                if (restoreFromBackupActivity.A35()) {
                    return;
                }
                if (z) {
                    restoreFromBackupActivity.A2q(2);
                    restoreFromBackupActivity.startActivityForResult(C14960mK.A07(restoreFromBackupActivity, 2), 0);
                    return;
                }
                restoreFromBackupActivity.A2w(null, 27);
                restoreFromBackupActivity.A2j();
                restoreFromBackupActivity.A2x(true);
                return;
            case 1:
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = (VoipCallControlBottomSheetV2) this.A00;
                int i = 0;
                if (this.A01) {
                    i = 2;
                }
                voipCallControlBottomSheetV2.A1O(i);
                return;
            default:
                return;
        }
    }
}
