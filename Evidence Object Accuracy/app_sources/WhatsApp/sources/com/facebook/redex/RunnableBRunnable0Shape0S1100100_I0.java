package com.facebook.redex;

import X.AnonymousClass009;
import X.AnonymousClass11E;
import X.C16310on;
import X.C254419k;
import X.C254619m;
import android.content.ContentValues;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1100100_I0 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public Object A01;
    public String A02;
    public final int A03;

    public RunnableBRunnable0Shape0S1100100_I0(Object obj, String str, int i, long j) {
        this.A03 = i;
        this.A01 = obj;
        this.A00 = j;
        this.A02 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A03) {
            case 0:
                long j = this.A00;
                ((AnonymousClass11E) this.A01).A01.put(Long.valueOf(j), this.A02);
                return;
            case 1:
                String str = this.A02;
                long j2 = this.A00;
                C254619m r6 = ((C254419k) this.A01).A00;
                synchronized (r6) {
                    AnonymousClass009.A00();
                    C16310on A02 = r6.A00.A02();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("chat_jid", str);
                    contentValues.put("page_number", Long.valueOf(j2));
                    A02.A03.A04(contentValues, "draft_voice_note_metadata");
                    A02.close();
                }
                return;
            default:
                return;
        }
    }
}
