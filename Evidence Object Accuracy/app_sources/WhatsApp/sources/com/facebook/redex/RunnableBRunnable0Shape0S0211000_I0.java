package com.facebook.redex;

import X.AbstractActivityC33001d7;
import X.AbstractC15340mz;
import X.AnonymousClass00T;
import X.AnonymousClass109;
import X.AnonymousClass1IS;
import X.AnonymousClass1OY;
import X.AnonymousClass1US;
import X.AnonymousClass2GF;
import X.C15370n3;
import X.C15380n4;
import X.C30041Vv;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.conversation.ConversationListView;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.group.view.custom.GroupDetailsCard;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.util.Set;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0211000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public boolean A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0211000_I0(Object obj, Object obj2, int i, int i2, boolean z) {
        this.A04 = i2;
        this.A01 = obj;
        this.A02 = obj2;
        this.A00 = i;
        this.A03 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A04) {
            case 0:
                ConversationListView conversationListView = (ConversationListView) this.A01;
                AbstractC15340mz r6 = (AbstractC15340mz) this.A02;
                int i = this.A00;
                boolean z = this.A03;
                AnonymousClass1IS r2 = r6.A0z;
                AnonymousClass1OY A00 = conversationListView.A00(r2);
                if (A00 != null) {
                    if (i == 8) {
                        A00.A0w();
                        return;
                    } else if (i == 12) {
                        A00.A0t();
                        return;
                    } else if (i == 20) {
                        conversationListView.getConversationCursorAdapter().A0S.add(r2);
                        return;
                    } else {
                        if (i == 28 || i == 27) {
                            if (C30041Vv.A08(conversationListView.A05, conversationListView.A06, r6) == null) {
                                A00.A1C(r6, i);
                                return;
                            }
                        } else if (i == 30) {
                            return;
                        } else {
                            if (z) {
                                A00.A1E(r6, true);
                                return;
                            }
                        }
                        A00.A1D(r6, true);
                        return;
                    }
                } else if (conversationListView.getConversationCursorAdapter().A0S.add(r2)) {
                    StringBuilder sb = new StringBuilder("conversation/refresh: no view for ");
                    sb.append(r2.A01);
                    sb.append(" ");
                    sb.append(conversationListView.getFirstVisiblePosition());
                    sb.append("-");
                    sb.append(conversationListView.getLastVisiblePosition());
                    sb.append(" (");
                    sb.append(conversationListView.getCount());
                    sb.append(")");
                    Log.i(sb.toString());
                    return;
                } else {
                    return;
                }
            case 1:
                GroupChatInfo groupChatInfo = (GroupChatInfo) this.A01;
                boolean z2 = this.A03;
                C15370n3 r9 = (C15370n3) this.A02;
                int i2 = this.A00;
                GroupDetailsCard groupDetailsCard = groupChatInfo.A18;
                if (groupDetailsCard != null) {
                    if (z2) {
                        groupDetailsCard.setSubtitleText(groupChatInfo.getResources().getString(R.string.announcement_subtitle));
                    } else if (!AnonymousClass1US.A0C(groupChatInfo.A1a)) {
                        groupChatInfo.A18.setSubtitleText(groupChatInfo.getResources().getString(R.string.subgroup_info_subtitle_redesign, groupChatInfo.A1a));
                    }
                }
                View A05 = AnonymousClass00T.A05(groupChatInfo, R.id.link_to_community_home_card);
                ((ImageView) groupChatInfo.findViewById(R.id.link_to_community_home_arrow)).setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(groupChatInfo, R.drawable.chevron_right), ((AbstractActivityC33001d7) groupChatInfo).A08));
                A05.setVisibility(0);
                A05.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(groupChatInfo, 44));
                groupChatInfo.A0a.A06((ImageView) AnonymousClass00T.A05(groupChatInfo, R.id.link_to_community_icon), r9);
                ((TextView) AnonymousClass00T.A05(groupChatInfo, R.id.link_to_community_parent_name)).setText(groupChatInfo.A0Y.A04(r9));
                ((TextView) AnonymousClass00T.A05(groupChatInfo, R.id.link_to_community_parent_description)).setText(groupChatInfo.getResources().getQuantityString(R.plurals.link_to_community_description, i2, Integer.valueOf(i2)));
                return;
            case 2:
                AnonymousClass109 r22 = (AnonymousClass109) this.A01;
                AbstractC15340mz r4 = (AbstractC15340mz) this.A02;
                int i3 = this.A00;
                boolean z3 = this.A03;
                Set A002 = r22.A0A.A00(r4.A0z);
                r22.A05.A0F(r4, i3, 1, C15380n4.A09(r22.A02, A002).size(), A002.size(), r22.A06.A00() - r4.A0I, z3, false, false);
                return;
            default:
                return;
        }
    }
}
