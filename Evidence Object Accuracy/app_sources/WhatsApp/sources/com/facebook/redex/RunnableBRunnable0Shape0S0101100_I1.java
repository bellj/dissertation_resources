package com.facebook.redex;

import X.AnonymousClass4ME;

/* loaded from: classes3.dex */
public class RunnableBRunnable0Shape0S0101100_I1 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public long A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0101100_I1(Object obj, int i, int i2, long j) {
        this.A03 = i2;
        this.A02 = obj;
        this.A00 = i;
        this.A01 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A03) {
            case 0:
                ((AnonymousClass4ME) this.A02).A01.APX(this.A00, this.A01);
                return;
            case 1:
                ((AnonymousClass4ME) this.A02).A01.AYI(this.A01, this.A00);
                return;
            default:
                return;
        }
    }
}
