package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC21570xd;
import X.AbstractC469028d;
import X.AnonymousClass17Z;
import X.AnonymousClass1JA;
import X.AnonymousClass1JB;
import X.AnonymousClass1Lx;
import X.AnonymousClass1PE;
import X.AnonymousClass1V8;
import X.AnonymousClass1W9;
import X.AnonymousClass2GV;
import X.AnonymousClass3ZC;
import X.C1114859o;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C14350lI;
import X.C15370n3;
import X.C15380n4;
import X.C15650ng;
import X.C16170oZ;
import X.C16310on;
import X.C16700pc;
import X.C17070qD;
import X.C17220qS;
import X.C18780t0;
import X.C19990v2;
import X.C252018m;
import X.C28181Ma;
import X.C42391v8;
import X.C50912Rv;
import X.C50942Ry;
import X.C50952Rz;
import X.C53892fb;
import X.C58902rx;
import X.C69173Yf;
import X.C92474Wb;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.avatar.home.AvatarHomeActivity;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.bottomsheet.LockableBottomSheetBehavior;
import com.whatsapp.community.AddGroupsToCommunityActivity;
import com.whatsapp.components.MainChildCoordinatorLayout;
import com.whatsapp.conversation.conversationrow.Hilt_IdentityChangeDialogFragment;
import com.whatsapp.conversation.conversationrow.SecurityNotificationDialogFragment;
import com.whatsapp.group.GroupSettingsActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.settings.SettingsChatHistoryFragment;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0110000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public boolean A01;
    public final int A02;

    public RunnableBRunnable0Shape1S0110000_I1(Object obj, int i, boolean z) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = z;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Runnable
    public final void run() {
        String str;
        String str2;
        Set entrySet;
        int i;
        FrameLayout.LayoutParams layoutParams;
        switch (this.A02) {
            case 0:
                ((C92474Wb) this.A00).A01.AW9(this.A01);
                return;
            case 1:
                AvatarHomeActivity avatarHomeActivity = (AvatarHomeActivity) this.A00;
                boolean z = this.A01;
                C16700pc.A0E(avatarHomeActivity, 0);
                MainChildCoordinatorLayout mainChildCoordinatorLayout = avatarHomeActivity.A0I;
                if (mainChildCoordinatorLayout == null) {
                    throw C16700pc.A06("coordinatorLayout");
                }
                int height = mainChildCoordinatorLayout.getHeight();
                int dimensionPixelSize = avatarHomeActivity.getResources().getDimensionPixelSize(R.dimen.avatar_preview_height);
                LinearLayout linearLayout = avatarHomeActivity.A04;
                if (linearLayout == null) {
                    throw C16700pc.A06("containerNewUser");
                }
                int measuredHeight = linearLayout.getMeasuredHeight();
                if (measuredHeight == 0) {
                    linearLayout.measure(0, 0);
                    measuredHeight = linearLayout.getMeasuredHeight();
                }
                if (z && measuredHeight > 0) {
                    dimensionPixelSize = measuredHeight;
                }
                int i2 = height - dimensionPixelSize;
                LockableBottomSheetBehavior lockableBottomSheetBehavior = avatarHomeActivity.A0G;
                if (lockableBottomSheetBehavior == null) {
                    throw C16700pc.A06("bottomSheetBehavior");
                }
                lockableBottomSheetBehavior.A0L(i2);
                LinearLayout linearLayout2 = avatarHomeActivity.A03;
                if (linearLayout2 == null) {
                    throw C16700pc.A06("containerAvatarSheet");
                }
                int height2 = height - linearLayout2.getHeight();
                View view = avatarHomeActivity.A01;
                if (view == null) {
                    throw C16700pc.A06("sheetPaddingView");
                }
                ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
                if (layoutParams2 == null) {
                    i = 0;
                } else {
                    i = layoutParams2.height;
                }
                if (height2 > i) {
                    View view2 = avatarHomeActivity.A01;
                    if (view2 == null) {
                        throw C16700pc.A06("sheetPaddingView");
                    }
                    ViewGroup.LayoutParams layoutParams3 = view2.getLayoutParams();
                    if (layoutParams3 != null) {
                        layoutParams3.height = height2;
                    }
                }
                CircularProgressBar circularProgressBar = avatarHomeActivity.A09;
                if (circularProgressBar == null) {
                    throw C16700pc.A06("avatarLoadingProgressBar");
                }
                if (circularProgressBar.getMeasuredHeight() == 0) {
                    CircularProgressBar circularProgressBar2 = avatarHomeActivity.A09;
                    if (circularProgressBar2 == null) {
                        throw C16700pc.A06("avatarLoadingProgressBar");
                    }
                    circularProgressBar2.measure(0, 0);
                }
                CircularProgressBar circularProgressBar3 = avatarHomeActivity.A09;
                if (circularProgressBar3 == null) {
                    throw C16700pc.A06("avatarLoadingProgressBar");
                }
                int measuredHeight2 = circularProgressBar3.getMeasuredHeight();
                CircularProgressBar circularProgressBar4 = avatarHomeActivity.A09;
                if (circularProgressBar4 == null) {
                    throw C16700pc.A06("avatarLoadingProgressBar");
                }
                ViewGroup.LayoutParams layoutParams4 = circularProgressBar4.getLayoutParams();
                if ((layoutParams4 instanceof FrameLayout.LayoutParams) && (layoutParams = (FrameLayout.LayoutParams) layoutParams4) != null) {
                    layoutParams.topMargin = (dimensionPixelSize >> 1) - measuredHeight2;
                    return;
                }
                return;
            case 2:
                boolean z2 = this.A01;
                RestoreFromBackupActivity restoreFromBackupActivity = ((C58902rx) this.A00).A01;
                if (!restoreFromBackupActivity.A33()) {
                    restoreFromBackupActivity.A05.setProgress(100);
                    restoreFromBackupActivity.A05.setIndeterminate(true);
                    restoreFromBackupActivity.A08.setText(R.string.settings_gdrive_backup_msgstore_restore_message);
                    restoreFromBackupActivity.A2x(z2);
                    return;
                }
                Log.i("gdrive-activity-observer/msgstore-download-finished/activity-already-exited");
                return;
            case 3:
                boolean z3 = this.A01;
                AddGroupsToCommunityActivity addGroupsToCommunityActivity = (AddGroupsToCommunityActivity) ((ViewOnClickCListenerShape13S0100000_I0) this.A00).A00;
                int size = ((Set) addGroupsToCommunityActivity.A01.A09.A01()).size();
                int A02 = addGroupsToCommunityActivity.A02.A0G.A02(1238);
                int i3 = A02 - size;
                if (i3 <= 0) {
                    Resources resources = addGroupsToCommunityActivity.getResources();
                    Object[] A1b = C12970iu.A1b();
                    C12960it.A1P(A1b, A02, 0);
                    addGroupsToCommunityActivity.A2Q("", resources.getQuantityString(R.plurals.link_group_max_limit, A02, A1b));
                    return;
                } else if (z3) {
                    Collection transform = C50912Rv.transform((Collection) addGroupsToCommunityActivity.A01.A08.A01(), new AbstractC469028d() { // from class: X.51I
                        @Override // X.AbstractC469028d
                        public final Object apply(Object obj) {
                            C15370n3 r2 = (C15370n3) obj;
                            AnonymousClass009.A05(r2);
                            Jid A0A = r2.A0A();
                            AnonymousClass009.A05(A0A);
                            return A0A.getRawString();
                        }
                    });
                    Intent A0A = C12970iu.A0A();
                    A0A.setClassName(addGroupsToCommunityActivity.getPackageName(), "com.whatsapp.community.LinkExistingGroups");
                    A0A.putExtra("max_groups_allowed_to_link", i3);
                    if (!transform.isEmpty()) {
                        A0A.putExtra("selected", C12980iv.A0x(transform));
                    }
                    addGroupsToCommunityActivity.A2E(A0A, 10);
                    return;
                } else {
                    addGroupsToCommunityActivity.A2K(new AnonymousClass2GV() { // from class: X.52b
                        @Override // X.AnonymousClass2GV
                        public final void AO1() {
                            AddGroupsToCommunityActivity.this.A2f();
                        }
                    }, 0, R.string.no_groups_to_link_error, R.string.create_group, R.string.cancel);
                    return;
                }
            case 4:
                C53892fb r1 = (C53892fb) this.A00;
                if (this.A01) {
                    r1.A07.A00();
                    r1.A05.A00();
                    return;
                }
                r1.A05.A02(null);
                return;
            case 5:
                C53892fb r3 = (C53892fb) this.A00;
                boolean z4 = this.A01;
                r3.A02.A0B(null);
                C12990iw.A1J(r3.A03, z4);
                return;
            case 6:
                Hilt_IdentityChangeDialogFragment hilt_IdentityChangeDialogFragment = (Hilt_IdentityChangeDialogFragment) this.A00;
                boolean z5 = this.A01;
                C252018m r12 = ((SecurityNotificationDialogFragment) hilt_IdentityChangeDialogFragment).A07;
                if (z5) {
                    str = "26000361";
                } else {
                    str = "28030014";
                }
                ((SecurityNotificationDialogFragment) hilt_IdentityChangeDialogFragment).A00.A06(hilt_IdentityChangeDialogFragment.A0p(), C12970iu.A0B(r12.A03(str)));
                return;
            case 7:
                boolean z6 = this.A01;
                AnonymousClass3ZC r8 = ((GroupSettingsActivity) this.A00).A0B;
                C17220qS r7 = r8.A02;
                String A01 = r7.A01();
                AnonymousClass1W9[] r32 = new AnonymousClass1W9[1];
                if (z6) {
                    str2 = "on";
                } else {
                    str2 = "off";
                }
                boolean A1a = C12990iw.A1a("state", str2, r32);
                AnonymousClass1V8 r4 = new AnonymousClass1V8(new AnonymousClass1V8("group_join", r32), "membership_approval_mode", (AnonymousClass1W9[]) null);
                AnonymousClass1W9[] r33 = new AnonymousClass1W9[4];
                C12960it.A1M("xmlns", "w:g2", r33, A1a ? 1 : 0);
                C12960it.A1M("id", A01, r33, 1);
                C12960it.A1M("type", "set", r33, 2);
                C12960it.A1M("to", r8.A01.getRawString(), r33, 3);
                r7.A0D(r8, new AnonymousClass1V8(r4, "iq", r33), A01, 337, 20000);
                return;
            case 8:
                boolean z7 = this.A01;
                AnonymousClass17Z r5 = ((C69173Yf) this.A00).A00.A02;
                if (z7) {
                    ArrayList A0l = C12960it.A0l();
                    Iterator it = r5.A03.A0E().iterator();
                    while (it.hasNext()) {
                        C15370n3 A0a = C12970iu.A0a(it);
                        if (A0a.A0I() && !C42391v8.A04(A0a.A0D)) {
                            A0l.add(A0a.A0B(UserJid.class));
                        }
                    }
                    boolean A00 = r5.A04.A00(AnonymousClass1JB.A0G, AnonymousClass1JA.A0B, A0l).A00();
                    if (A00) {
                        Iterator it2 = A0l.iterator();
                        while (it2.hasNext()) {
                            C17070qD r0 = r5.A0E;
                            r0.A03();
                            r0.A09.A0H((UserJid) it2.next());
                        }
                        try {
                            C50942Ry A022 = r5.A02();
                            C50952Rz A012 = r5.A01();
                            if (A022 != null && A012 != null) {
                                A022.A02 = A00;
                                r5.A0F.A02(A022, A012.A01);
                                return;
                            }
                            return;
                        } catch (Exception e) {
                            Log.e("PAY: PaymentIncentiveManager/processUpdateSyncFlag : Error while parsing ", e);
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 9:
                boolean z8 = this.A01;
                SettingsChatHistoryFragment settingsChatHistoryFragment = ((C1114859o) this.A00).A00;
                C16170oZ r52 = settingsChatHistoryFragment.A03;
                List A04 = r52.A0e.A04();
                ArrayList A0l2 = C12960it.A0l();
                HashSet A12 = C12970iu.A12();
                Iterator it3 = A04.iterator();
                while (it3.hasNext()) {
                    AbstractC14640lm A0b = C12990iw.A0b(it3);
                    if (r52.A0b.A00(A0b) > 0) {
                        r52.A1D.A09(A0b, null);
                        r52.A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(r52, 31, A0b));
                        r52.A1C.A06(A0b, true);
                    }
                    C15370n3 A013 = r52.A0c.A01(A0b);
                    if (!A013.A0K() && A013.A0C == null) {
                        A0l2.add(A013);
                    }
                    if (!A013.A0K()) {
                        r52.A0A(A0b);
                    }
                    A12.addAll(r52.A0J.A07(A0b, z8));
                }
                r52.A0K.A0Y(A0l2);
                C15650ng r6 = r52.A0f;
                Log.i("msgstore/deleteallmsgs");
                C28181Ma r9 = new C28181Ma("msgstore/deleteallmsgs");
                r6.A1i.clear();
                C16310on A023 = r6.A0t.A02();
                try {
                    AnonymousClass1Lx A002 = A023.A00();
                    A023.A03.A0B("DELETE FROM chat WHERE _id IN  (SELECT c._id FROM chat AS c LEFT JOIN jid AS j ON c.jid_row_id=j._id WHERE j.type != 1)");
                    r6.A0f(r9);
                    C19990v2 r13 = r6.A0O;
                    synchronized (r13) {
                        entrySet = r13.A0B().entrySet();
                    }
                    Iterator it4 = entrySet.iterator();
                    while (it4.hasNext()) {
                        Map.Entry A15 = C12970iu.A15(it4);
                        AbstractC14640lm r2 = (AbstractC14640lm) A15.getKey();
                        AnonymousClass1PE r14 = (AnonymousClass1PE) A15.getValue();
                        r14.A08();
                        if (C15380n4.A0K(r2) && r14.A00 == 1) {
                            r6.A0n(r2, null);
                        }
                    }
                    A002.A00();
                    A002.close();
                    A023.close();
                    StringBuilder A0k = C12960it.A0k("msgstore/deleteallmsgs time spent:");
                    A0k.append(r9.A01());
                    C12960it.A1F(A0k);
                    C14350lI.A0N(r6.A03.A04().A0O);
                    if (z8) {
                        r6.A0F();
                    }
                    Message.obtain(r6.A0e.A01, 9).sendToTarget();
                    C18780t0 r22 = r52.A0M;
                    C16310on A024 = r22.A03.A00.A02();
                    try {
                        AbstractC21570xd.A02(A024, "wa_trusted_contacts", null, null);
                        AbstractC21570xd.A02(A024, "wa_trusted_contacts_send", null, null);
                        A024.close();
                        r22.A06().clear();
                        r52.A0J.A0O(A12);
                        r52.A10.A0D();
                        r52.A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(r52.A1D, 26));
                        r52.A1C.A04(null, 2, 0, 0);
                        r52.A0E.A01();
                        settingsChatHistoryFragment.A00.A0H(new RunnableBRunnable0Shape16S0100000_I1_2(settingsChatHistoryFragment, 10));
                        return;
                    } finally {
                        try {
                            A024.close();
                        } catch (Throwable unused) {
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            default:
                return;
        }
    }
}
