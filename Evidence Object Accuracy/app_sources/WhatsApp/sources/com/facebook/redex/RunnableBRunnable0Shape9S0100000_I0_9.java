package com.facebook.redex;

import X.C15230mm;
import com.whatsapp.notification.PopupNotification;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape9S0100000_I0_9 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape9S0100000_I0_9(C15230mm r1, int i) {
        this.A01 = i;
        if (24 - i != 0) {
            this.A00 = r1;
        } else {
            this.A00 = r1;
        }
    }

    public RunnableBRunnable0Shape9S0100000_I0_9(PopupNotification popupNotification, int i) {
        this.A01 = i;
        if (6 - i != 0) {
            this.A00 = popupNotification;
        } else {
            this.A00 = popupNotification;
        }
    }

    public RunnableBRunnable0Shape9S0100000_I0_9(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a9, code lost:
        if (r2.A07 != false) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0179, code lost:
        if (r5.A06 != null) goto L_0x017b;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 2066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9.run():void");
    }
}
