package com.facebook.redex;

import android.content.DialogInterface;

/* loaded from: classes4.dex */
public class IDxCListenerShape5S0000000_3_I1 implements DialogInterface.OnClickListener {
    public final int A00;

    public IDxCListenerShape5S0000000_3_I1(int i) {
        this.A00 = i;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (this.A00) {
            case 0:
            case 2:
            case 3:
            case 4:
            case 5:
            case 7:
            case 8:
                dialogInterface.dismiss();
                return;
            case 1:
            case 6:
            default:
                return;
        }
    }
}
