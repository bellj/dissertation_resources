package com.facebook.redex;

import X.AbstractC15710nm;
import X.AbstractC47642Bu;
import X.AnonymousClass009;
import X.AnonymousClass13T;
import X.AnonymousClass1LC;
import X.AnonymousClass2C0;
import X.AnonymousClass2C1;
import X.AnonymousClass2C2;
import X.AnonymousClass2C4;
import X.AnonymousClass2C5;
import X.AnonymousClass3ZX;
import X.C12960it;
import X.C12970iu;
import X.C14850m9;
import X.C14900mE;
import X.C15580nU;
import X.C16240og;
import X.C17030q9;
import X.C17220qS;
import X.C18640sm;
import X.C20670w8;
import X.C20780wJ;
import X.C22330yu;
import X.C22640zP;
import X.C22700zV;
import X.C47622Bs;
import X.C47652Bv;
import X.C47662Bw;
import X.C47672Bx;
import X.C47692Bz;
import X.C63793Cy;
import android.content.Context;
import com.whatsapp.group.GroupChatInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0700000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public Object A06;
    public final int A07;

    public RunnableBRunnable0Shape1S0700000_I1(AbstractC15710nm r2, C14900mE r3, C22330yu r4, C22640zP r5, GroupChatInfo groupChatInfo, C15580nU r7, C17220qS r8) {
        this.A07 = 0;
        this.A07 = 0;
        this.A03 = r7;
        this.A04 = r3;
        this.A02 = r2;
        this.A06 = r8;
        this.A00 = r4;
        this.A01 = r5;
        this.A05 = C12970iu.A10(groupChatInfo);
    }

    public RunnableBRunnable0Shape1S0700000_I1(C20670w8 r2, C16240og r3, AnonymousClass13T r4, C17030q9 r5, C22700zV r6, C18640sm r7, C20780wJ r8) {
        this.A07 = 1;
        this.A00 = r2;
        this.A01 = r7;
        this.A02 = r8;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r4;
        this.A06 = r3;
    }

    @Override // java.lang.Runnable
    public void run() {
        int i = 0;
        int i2 = 0;
        switch (this.A07) {
            case 0:
                C15580nU r5 = (C15580nU) this.A03;
                C15580nU A00 = ((C22640zP) this.A01).A0D.A00(r5);
                AnonymousClass009.A05(A00);
                new C63793Cy((AbstractC15710nm) this.A02, A00, (C17220qS) this.A06, new AnonymousClass3ZX(this)).A00(Collections.singletonList(r5));
                return;
            case 1:
                C20670w8 r1 = (C20670w8) this.A00;
                AnonymousClass13T r4 = (AnonymousClass13T) this.A05;
                C16240og r3 = (C16240og) this.A06;
                ArrayList A0l = C12960it.A0l();
                A0l.add(new C47622Bs(r4));
                A0l.add(new C47652Bv(r4));
                A0l.add(new C47662Bw(r4));
                A0l.add(new C47672Bx((C22700zV) this.A04));
                A0l.add(new C47692Bz((C17030q9) this.A03));
                A0l.add(new AnonymousClass2C0(r3));
                A0l.add(new AnonymousClass2C1((C18640sm) this.A01));
                A0l.add(new AnonymousClass2C2(r3));
                Context context = r1.A02.A00;
                boolean z = false;
                int i3 = 15;
                List asList = Arrays.asList(A0l.toArray(new AbstractC47642Bu[0]));
                AnonymousClass2C4 r52 = new AnonymousClass2C4(r1.A01);
                C14850m9 r2 = r1.A03;
                boolean A07 = r2.A07(476);
                AnonymousClass2C5 r42 = new AnonymousClass2C5(r1);
                if (r2.A07(418)) {
                    z = true;
                    i3 = 1;
                    i = r2.A02(419);
                    i2 = r2.A02(420);
                }
                if (asList == null) {
                    asList = new LinkedList();
                }
                r1.A00 = new AnonymousClass1LC(context, r42, r52, asList, i3, i, i2, z, A07);
                return;
            default:
                return;
        }
    }
}
