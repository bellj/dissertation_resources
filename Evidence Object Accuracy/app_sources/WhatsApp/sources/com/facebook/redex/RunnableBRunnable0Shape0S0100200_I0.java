package com.facebook.redex;

import X.C14960mK;
import X.C36021jC;
import android.content.Intent;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;
import com.whatsapp.registration.VerifyPhoneNumber;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0100200_I0 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public long A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0100200_I0(Object obj, int i, long j, long j2) {
        this.A03 = i;
        this.A02 = obj;
        this.A00 = j;
        this.A01 = j2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Intent A08;
        switch (this.A03) {
            case 0:
                ((ExportMigrationActivity) this.A02).A2f(this.A00 - this.A01);
                return;
            case 1:
                VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) this.A02;
                long j = this.A00;
                long j2 = this.A01;
                C36021jC.A00(verifyPhoneNumber, 40);
                verifyPhoneNumber.A0m.A0A(verifyPhoneNumber.A2e());
                if (verifyPhoneNumber.A02 == 0) {
                    A08 = C14960mK.A0X(verifyPhoneNumber, null, -1, j, j2, verifyPhoneNumber.A1A, false, false, false);
                } else {
                    A08 = C14960mK.A08(verifyPhoneNumber, 3, j, j2, false, verifyPhoneNumber.A1A);
                }
                verifyPhoneNumber.finish();
                verifyPhoneNumber.startActivity(A08);
                return;
            case 2:
                VerifyPhoneNumber verifyPhoneNumber2 = (VerifyPhoneNumber) this.A02;
                long j3 = this.A00;
                long j4 = this.A01;
                C36021jC.A00(verifyPhoneNumber2, 41);
                Intent A082 = C14960mK.A08(verifyPhoneNumber2, 1, j3, j4, verifyPhoneNumber2.A10, verifyPhoneNumber2.A1A);
                verifyPhoneNumber2.finish();
                verifyPhoneNumber2.startActivity(A082);
                verifyPhoneNumber2.overridePendingTransition(0, 0);
                return;
            default:
                return;
        }
    }
}
