package com.facebook.redex;

import X.C25991Bp;
import android.content.BroadcastReceiver;
import com.whatsapp.Conversation;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0100000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape0S0100000_I0(BroadcastReceiver.PendingResult pendingResult) {
        this.A01 = 3;
        this.A00 = pendingResult;
    }

    public RunnableBRunnable0Shape0S0100000_I0(Conversation conversation, int i) {
        this.A01 = i;
        switch (i) {
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case C25991Bp.A0S:
            case 31:
            case 32:
                this.A00 = conversation;
                return;
            default:
                this.A00 = conversation;
                return;
        }
    }

    public RunnableBRunnable0Shape0S0100000_I0(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    public RunnableBRunnable0Shape0S0100000_I0(Runnable runnable) {
        this.A01 = 5;
        this.A00 = runnable;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:197:0x044c, code lost:
        if (r0 == false) goto L_0x044e;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 2042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0.run():void");
    }
}
