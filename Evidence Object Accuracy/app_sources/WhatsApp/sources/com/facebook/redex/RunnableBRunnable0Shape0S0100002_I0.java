package com.facebook.redex;

import X.AnonymousClass1s8;
import X.AnonymousClass27X;
import X.AnonymousClass2JX;
import X.AnonymousClass2P8;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import com.whatsapp.R;
import com.whatsapp.camera.overlays.AutofocusOverlay;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0100002_I0 extends EmptyBaseRunnable0 implements Runnable {
    public float A00;
    public float A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0100002_I0(Object obj, float f, float f2, int i) {
        this.A03 = i;
        this.A02 = obj;
        this.A00 = f;
        this.A01 = f2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A03) {
            case 0:
                float f = this.A00;
                float f2 = this.A01;
                AnonymousClass1s8 r2 = ((AnonymousClass2JX) this.A02).A00;
                AnonymousClass2P8 r1 = r2.A0E;
                float left = ((float) r2.A06.getLeft()) + f;
                float top = ((float) r2.A06.getTop()) + f2;
                AutofocusOverlay autofocusOverlay = r1.A02;
                float f3 = autofocusOverlay.A00 / 2.0f;
                autofocusOverlay.A01 = new RectF(left - f3, top - f3, left + f3, top + f3);
                autofocusOverlay.A03 = null;
                autofocusOverlay.setVisibility(0);
                autofocusOverlay.invalidate();
                autofocusOverlay.removeCallbacks(autofocusOverlay.A06);
                return;
            case 1:
                AnonymousClass27X r3 = (AnonymousClass27X) this.A02;
                float f4 = this.A00;
                float f5 = this.A01;
                synchronized (r3) {
                    Camera camera = r3.A07;
                    if (camera != null && r3.A0M) {
                        camera.cancelAutoFocus();
                        Camera.Parameters parameters = r3.A07.getParameters();
                        if (parameters.getMaxNumFocusAreas() > 0) {
                            float dimension = r3.getContext().getResources().getDimension(R.dimen.autofocus_box_size) / 2.0f;
                            RectF rectF = new RectF(f4 - dimension, f5 - dimension, f4 + dimension, dimension + f5);
                            Matrix matrix = new Matrix();
                            float f6 = 1.0f;
                            if (r3.A0N) {
                                f6 = -1.0f;
                            }
                            matrix.setScale(f6, 1.0f);
                            matrix.postRotate((float) r3.A01);
                            float width = (float) r3.getWidth();
                            float height = (float) r3.getHeight();
                            matrix.postScale(width / 2000.0f, height / 2000.0f);
                            matrix.postTranslate(width / 2.0f, height / 2.0f);
                            matrix.invert(matrix);
                            matrix.mapRect(rectF);
                            Rect rect = new Rect();
                            int A00 = AnonymousClass27X.A00(rectF.left);
                            rect.left = A00;
                            int A002 = AnonymousClass27X.A00(rectF.top);
                            rect.top = A002;
                            int A003 = AnonymousClass27X.A00(rectF.right);
                            rect.right = A003;
                            int A004 = AnonymousClass27X.A00(rectF.bottom);
                            rect.bottom = A004;
                            if (Math.abs(A002 - A004) < 10) {
                                rect.top = A002 - 5;
                                rect.bottom = A004 + 5;
                            }
                            if (Math.abs(A00 - A003) < 10) {
                                rect.left = A00 - 5;
                                rect.right = A003 + 5;
                            }
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(new Camera.Area(rect, 1000));
                            parameters.setFocusAreas(arrayList);
                            List<String> supportedFocusModes = parameters.getSupportedFocusModes();
                            if (supportedFocusModes != null && supportedFocusModes.contains("auto")) {
                                parameters.setFocusMode("auto");
                            }
                            r3.A07.setParameters(parameters);
                            r3.A0F.AMh(f4, f5);
                        }
                        r3.A07.autoFocus(new Camera.AutoFocusCallback() { // from class: X.4hl
                            @Override // android.hardware.Camera.AutoFocusCallback
                            public final void onAutoFocus(boolean z, Camera camera2) {
                                AnonymousClass27X.this.A0F.AMi(z);
                            }
                        });
                    }
                }
                return;
            default:
                return;
        }
    }
}
