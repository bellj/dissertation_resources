package com.facebook.redex;

import X.AbstractC15340mz;
import X.AbstractC15590nW;
import X.AbstractC16130oV;
import X.AnonymousClass009;
import X.AnonymousClass1CH;
import X.AnonymousClass1OT;
import X.AnonymousClass22Y;
import X.C15940oB;
import X.C15980oF;
import X.C16150oX;
import X.C20710wC;
import X.C22370yy;
import X.C22420z3;
import X.C22460z7;
import X.C22530zE;
import X.C28781Oz;
import X.C30921Zi;
import android.os.ConditionVariable;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0410000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public boolean A04;
    public final int A05;

    public RunnableBRunnable0Shape0S0410000_I0(Object obj, Object obj2, Object obj3, Object obj4, int i, boolean z) {
        this.A05 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A04 = z;
        this.A03 = obj4;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A05) {
            case 0:
                C20710wC r6 = (C20710wC) this.A00;
                AbstractC15590nW r5 = (AbstractC15590nW) this.A01;
                boolean z = this.A04;
                AnonymousClass1OT r3 = (AnonymousClass1OT) this.A03;
                if (r6.A0L.A0j(new C15980oF(C15940oB.A02((DeviceJid) this.A02), r5.getRawString()))) {
                    r6.A07.A0E(r5, 1);
                }
                if (z) {
                    r6.A0n.A0E(r3);
                    return;
                }
                return;
            case 1:
                C22370yy r52 = (C22370yy) this.A00;
                C28781Oz r4 = (C28781Oz) this.A01;
                AbstractC16130oV r32 = (AbstractC16130oV) this.A02;
                boolean z2 = this.A04;
                C22370yy.A00(r4, r32, false);
                AnonymousClass1CH r1 = r52.A0V;
                C16150oX r0 = r32.A02;
                HashMap hashMap = r1.A00;
                synchronized (hashMap) {
                    hashMap.remove(r0);
                }
                AnonymousClass009.A05(r4.A01());
                r52.A0B(r32, r4.A01().A00, z2);
                return;
            case 2:
                C22420z3 r33 = (C22420z3) this.A00;
                AbstractMap abstractMap = (AbstractMap) this.A01;
                boolean z3 = this.A04;
                ConditionVariable conditionVariable = (ConditionVariable) this.A02;
                ConditionVariable conditionVariable2 = (ConditionVariable) this.A03;
                ArrayList arrayList = new ArrayList();
                try {
                    for (AbstractC15340mz r02 : abstractMap.values()) {
                        if (r02 != null) {
                            arrayList.add(r02);
                        }
                    }
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder("app/xmpp/send/qr_send_conv preempt:");
                    sb.append(z3);
                    sb.append(" recents error ");
                    sb.append(e.toString());
                    Log.e(sb.toString());
                }
                r33.A00(conditionVariable, conditionVariable2, null, arrayList, 0, true, z3);
                return;
            case 3:
                C22460z7 r42 = (C22460z7) this.A00;
                AnonymousClass22Y r34 = (AnonymousClass22Y) this.A01;
                C30921Zi r2 = (C30921Zi) this.A02;
                File file = (File) this.A03;
                boolean z4 = this.A04;
                if (r34 != null) {
                    r34.AMI(r2, file);
                }
                if (!z4) {
                    C22530zE r03 = r42.A0A;
                    if (file != null) {
                        r03.A05();
                        return;
                    }
                    Iterator it = r03.A01().iterator();
                    while (it.hasNext()) {
                        it.next();
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
