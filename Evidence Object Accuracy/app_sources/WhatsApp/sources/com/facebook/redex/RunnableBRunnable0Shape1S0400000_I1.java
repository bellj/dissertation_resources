package com.facebook.redex;

import X.AbstractC15460nI;
import X.AbstractC18500sY;
import X.AbstractC28981Pu;
import X.AnonymousClass1J7;
import X.AnonymousClass28G;
import X.AnonymousClass28H;
import X.AnonymousClass28I;
import X.AnonymousClass28J;
import X.AnonymousClass28K;
import X.AnonymousClass28L;
import X.AnonymousClass28N;
import X.AnonymousClass28O;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C14850m9;
import X.C14900mE;
import X.C15450nH;
import X.C15570nT;
import X.C16170oZ;
import X.C16490p7;
import X.C16700pc;
import X.C18290sD;
import X.C19890uq;
import X.C19990v2;
import X.C20850wQ;
import X.C21630xj;
import X.C21950yF;
import X.C249317l;
import X.C28991Pv;
import X.C29001Pw;
import X.C44721zR;
import android.content.Context;
import android.content.res.Resources;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0400000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public RunnableBRunnable0Shape1S0400000_I1(Object obj, Object obj2, Object obj3, Object obj4, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AbstractC18500sY A01;
        switch (this.A04) {
            case 0:
                AnonymousClass28G r1 = (AnonymousClass28G) this.A00;
                AnonymousClass28H r0 = (AnonymousClass28H) this.A01;
                AnonymousClass1J7 r3 = (AnonymousClass1J7) this.A02;
                C44721zR r5 = (C44721zR) this.A03;
                C16700pc.A0F(r1, r0);
                C16700pc.A0H(r3, r5);
                AnonymousClass28I r6 = r1.A02;
                UserJid userJid = r0.A05;
                C16700pc.A0B(userJid);
                AnonymousClass28J r12 = r0.A00;
                if (r12 != null) {
                    Object obj = ((AtomicReference) r6.A01.getValue()).get();
                    String str = r12.A02;
                    C16700pc.A0B(str);
                    if (obj.equals(new AnonymousClass28K(userJid, str))) {
                        r6.A00 = r5.A00;
                        if (!r5.A01.isEmpty()) {
                            r3.AJ4(new AnonymousClass28L(r5));
                            return;
                        } else {
                            r3.AJ4(AnonymousClass28N.A00);
                            return;
                        }
                    }
                }
                r3.AJ4(AnonymousClass28O.A00);
                return;
            case 1:
                Collection collection = (Collection) this.A01;
                C14900mE r62 = (C14900mE) this.A02;
                Context context = (Context) this.A03;
                ((C16170oZ) this.A00).A0T(collection, false);
                if (collection.size() == 1) {
                    r62.A08(R.string.message_deleted, 0);
                    return;
                }
                Resources resources = context.getResources();
                int size = collection.size();
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, collection.size(), 0);
                r62.A0F(resources.getQuantityString(R.plurals.messages_deleted, size, objArr), 0);
                return;
            case 2:
                C16490p7 r02 = (C16490p7) this.A00;
                C18290sD r4 = (C18290sD) this.A01;
                C14850m9 r52 = (C14850m9) this.A02;
                C21950yF r7 = (C21950yF) this.A03;
                r02.A04();
                if (r02.A01) {
                    C29001Pw r8 = new C29001Pw(new AbstractC28981Pu[0]);
                    Log.i("DatabaseMigrationManager/processAllRollbacks");
                    HashSet A12 = C12970iu.A12();
                    for (AbstractC18500sY r13 : C12980iv.A0x(r4.A0A.A00().A00.values())) {
                        if (((r13.A0N() || r13.A0O()) && r13.A03() == 1) || r13.A0R()) {
                            A12.add(r13.A0C);
                        }
                    }
                    r4.A03(r8, A12, 1, 1);
                    if (r52.A07(425)) {
                        r4.A02(1);
                    }
                    C15450nH r42 = r7.A02;
                    boolean A05 = r42.A05(AbstractC15460nI.A0K);
                    C16490p7 r03 = r7.A05;
                    r03.A04();
                    if (r03.A01) {
                        C21630xj r32 = r7.A07;
                        AbstractC18500sY A012 = r32.A01("call_log");
                        if (A012 != null && !A012.A0O()) {
                            HashSet A122 = C12970iu.A12();
                            A122.add("call_log");
                            r7.A06.A03(new C29001Pw(new AbstractC28981Pu[0]), A122, 7, 1);
                        }
                        if (A05) {
                            C15570nT r04 = r7.A01;
                            r04.A08();
                            if (r04.A05 != null) {
                                HashSet A123 = C12970iu.A12();
                                AbstractC18500sY A013 = r32.A01("migration_jid_store");
                                if (A013 != null && !A013.A0O()) {
                                    r7.A00.AaV("jid-store-migration-pending", String.valueOf(A013.A04()), false);
                                    A123.add("migration_jid_store");
                                }
                                AbstractC18500sY A014 = r32.A01("migration_chat_store");
                                if (A014 != null && !A014.A0O()) {
                                    r7.A00.AaV("chat-store-migration-pending", String.valueOf(A014.A04()), false);
                                    A123.add("migration_chat_store");
                                }
                                AbstractC18500sY A015 = r32.A01("blank_me_jid");
                                if (A015 != null && !A015.A0O()) {
                                    r7.A00.AaV("md-blank-me-jid-migration-pending", String.valueOf(A015.A04()), false);
                                    A123.add("blank_me_jid");
                                }
                                AbstractC18500sY A016 = r32.A01("participant_user");
                                if (A016 != null && !A016.A0O()) {
                                    r7.A00.AaV("md-participant-user-migration-pending", String.valueOf(A016.A04()), false);
                                    A123.add("participant_user");
                                }
                                AbstractC18500sY A017 = r32.A01("broadcast_me_jid");
                                if (A017 != null && !A017.A0O()) {
                                    r7.A00.AaV("md-broadcast-me-jid-migration-pending", String.valueOf(A017.A04()), false);
                                    A123.add("broadcast_me_jid");
                                }
                                r7.A06.A03(new C29001Pw(new AbstractC28981Pu[0]), A123, 7, 1);
                            }
                        }
                        if (r42.A05(AbstractC15460nI.A0J) && (A01 = r32.A01("message_fts")) != null && A01.A0N()) {
                            HashSet A124 = C12970iu.A12();
                            A124.add("message_fts");
                            r7.A06.A03(new C29001Pw(new C28991Pv(r7.A03, r7.A04)), A124, 7, 1);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case 3:
                C19890uq r33 = (C19890uq) this.A02;
                C19990v2 r2 = (C19990v2) this.A03;
                if (((C20850wQ) this.A00).A02.A07((C249317l) this.A01, false)) {
                    Log.i("app-init/main/msgstoredb/healthy");
                    r33.A04();
                }
                r2.A0B();
                return;
            default:
                return;
        }
    }
}
