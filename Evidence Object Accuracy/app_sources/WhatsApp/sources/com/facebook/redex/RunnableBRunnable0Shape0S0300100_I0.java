package com.facebook.redex;

import X.C13030j1;
import X.C13710kC;
import android.content.Context;
import android.os.PowerManager;
import com.google.firebase.iid.FirebaseInstanceId;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0300100_I0 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0300100_I0(FirebaseInstanceId firebaseInstanceId, C13710kC r6, long j) {
        this.A04 = 0;
        this.A02 = firebaseInstanceId;
        this.A03 = r6;
        this.A00 = j;
        PowerManager.WakeLock newWakeLock = ((PowerManager) A00().getSystemService("power")).newWakeLock(1, "fiid-sync");
        this.A01 = newWakeLock;
        newWakeLock.setReferenceCounted(false);
    }

    public RunnableBRunnable0Shape0S0300100_I0(Object obj, Object obj2, Object obj3, int i, long j) {
        this.A04 = i;
        this.A01 = obj;
        this.A02 = obj2;
        this.A03 = obj3;
        this.A00 = j;
    }

    public final Context A00() {
        C13030j1 r0 = ((FirebaseInstanceId) this.A02).A01;
        r0.A02();
        return r0.A00;
    }

    /* JADX WARNING: Removed duplicated region for block: B:154:0x039e A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:324:? A[RETURN, SYNTHETIC] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1858
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0300100_I0.run():void");
    }
}
