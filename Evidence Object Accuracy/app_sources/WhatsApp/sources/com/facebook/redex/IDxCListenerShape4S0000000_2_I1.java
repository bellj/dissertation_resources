package com.facebook.redex;

import android.content.DialogInterface;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxCListenerShape4S0000000_2_I1 implements DialogInterface.OnClickListener {
    public final int A00;

    public IDxCListenerShape4S0000000_2_I1(int i) {
        this.A00 = i;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        switch (this.A00) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 19:
                dialogInterface.dismiss();
                return;
            case 4:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case 17:
            case 18:
            default:
                return;
        }
    }
}
