package com.facebook.redex;

import com.facebook.msys.mci.Execution;
import com.whatsapp.voipcalling.Voip;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0001000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public final int A01;

    public RunnableBRunnable0Shape0S0001000_I0(int i, int i2) {
        this.A01 = i2;
        if (i2 != 0) {
            this.A00 = i;
        } else {
            this.A00 = i;
        }
    }

    @Override // java.lang.Runnable
    public void run() {
        switch (this.A01) {
            case 0:
                Execution.nativeStartExecutor(this.A00);
                return;
            case 1:
                Voip.notifyAudioRouteChange(this.A00);
                return;
            case 2:
                Voip.rejectVideoUpgrade(this.A00);
                return;
            case 3:
                Voip.cancelVideoUpgrade(this.A00);
                return;
            default:
                return;
        }
    }
}
