package com.facebook.redex;

import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape1S0200000_I0_1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public final int A02;

    public ViewOnClickCListenerShape1S0200000_I0_1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = obj2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0014, code lost:
        if (r1 == false) goto L_0x0016;
     */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r10) {
        /*
            r9 = this;
            int r0 = r9.A02
            switch(r0) {
                case 0: goto L_0x00a5;
                case 1: goto L_0x002f;
                case 2: goto L_0x0020;
                case 3: goto L_0x0006;
                default: goto L_0x0005;
            }
        L_0x0005:
            return
        L_0x0006:
            java.lang.Object r2 = r9.A00
            X.0kN r2 = (X.ActivityC13810kN) r2
            java.lang.Object r0 = r9.A01
            com.whatsapp.voipcalling.CallInfo r0 = (com.whatsapp.voipcalling.CallInfo) r0
            if (r0 == 0) goto L_0x0016
            boolean r1 = r0.videoEnabled
            r0 = 11
            if (r1 != 0) goto L_0x0018
        L_0x0016:
            r0 = 10
        L_0x0018:
            com.whatsapp.voipcalling.VoipActivityV2$E2EEInfoDialogFragment r0 = com.whatsapp.voipcalling.VoipActivityV2.E2EEInfoDialogFragment.A00(r0)
            r2.Adm(r0)
            return
        L_0x0020:
            java.lang.Object r1 = r9.A00
            com.whatsapp.voipcalling.VoipActivityV2 r1 = (com.whatsapp.voipcalling.VoipActivityV2) r1
            java.lang.Object r0 = r9.A01
            android.view.View$OnClickListener r0 = (android.view.View.OnClickListener) r0
            r0.onClick(r10)
            r1.A2p()
            return
        L_0x002f:
            java.lang.Object r3 = r9.A00
            X.2V6 r3 = (X.AnonymousClass2V6) r3
            java.lang.Object r5 = r9.A01
            X.1Yo r5 = (X.C30721Yo) r5
            byte[] r2 = r5.A09
            r8 = 0
            if (r2 == 0) goto L_0x0077
            int r1 = r2.length
            if (r1 <= 0) goto L_0x0077
            r0 = 0
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeByteArray(r2, r0, r1)
        L_0x0044:
            com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity r3 = r3.A01
            r3.A0K = r5
            java.util.List r7 = r3.A0Q
            r7.clear()
            java.util.ArrayList r6 = r3.A0P
            r6.clear()
            java.util.List r0 = r5.A05
            if (r0 == 0) goto L_0x0079
            java.util.Iterator r2 = r0.iterator()
        L_0x005a:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r1 = r2.next()
            X.1Yq r1 = (X.C30741Yq) r1
            java.lang.String r0 = r1.A02
            r6.add(r0)
            com.whatsapp.jid.UserJid r0 = r1.A01
            if (r0 == 0) goto L_0x0073
            r7.add(r0)
            goto L_0x005a
        L_0x0073:
            r7.add(r8)
            goto L_0x005a
        L_0x0077:
            r4 = r8
            goto L_0x0044
        L_0x0079:
            r5.toString()
            X.02e r2 = new X.02e
            r2.<init>(r3)
            r0 = 2131886220(0x7f12008c, float:1.9407013E38)
            r2.A06(r0)
            r1 = 2131889560(0x7f120d98, float:1.9413787E38)
            X.3Kl r0 = new X.3Kl
            r0.<init>(r4, r3, r5)
            r2.setPositiveButton(r1, r0)
            r1 = 2131888120(0x7f1207f8, float:1.9410866E38)
            X.3Km r0 = new X.3Km
            r0.<init>(r4, r3, r5)
            r2.setNegativeButton(r1, r0)
            X.04S r0 = r2.create()
            r0.show()
            return
        L_0x00a5:
            java.lang.Object r0 = r9.A00
            X.2V6 r0 = (X.AnonymousClass2V6) r0
            java.lang.Object r4 = r9.A01
            X.0n3 r4 = (X.C15370n3) r4
            com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity r3 = r0.A01
            X.19Z r2 = r3.A0H
            r1 = 15
            r0 = 1
            r2.A01(r3, r4, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape1S0200000_I0_1.onClick(android.view.View):void");
    }
}
