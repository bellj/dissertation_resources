package com.facebook.redex;

import X.AbstractC136196Lo;
import X.C12970iu;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class IDxAListenerShape1S0200000_3_I1 implements AbstractC136196Lo {
    public Object A00;
    public Object A01;
    public final int A02;

    public IDxAListenerShape1S0200000_3_I1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj2;
        this.A01 = obj;
    }

    public static void A00(JSONArray jSONArray, JSONObject jSONObject) {
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            Object obj = jSONArray.get(i);
            if (obj instanceof String) {
                Object opt = jSONObject.opt((String) obj);
                if (opt != null) {
                    jSONArray.put(i, opt);
                }
            } else if (obj instanceof JSONArray) {
                A00((JSONArray) obj, jSONObject);
            } else if (obj instanceof JSONObject) {
                A01((JSONObject) obj, jSONObject);
            }
        }
    }

    public static void A01(JSONObject jSONObject, JSONObject jSONObject2) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String A0x = C12970iu.A0x(keys);
            Object obj = jSONObject.get(A0x);
            if (obj instanceof String) {
                Object opt = jSONObject2.opt((String) obj);
                if (opt != null) {
                    jSONObject.put(A0x, opt);
                }
            } else if (obj instanceof JSONArray) {
                A00((JSONArray) obj, jSONObject2);
            } else if (obj instanceof JSONObject) {
                A01((JSONObject) obj, jSONObject2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:204:0x04a1, code lost:
        if (r28.A01 != null) goto L_0x04a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x0670, code lost:
        if (r1 != false) goto L_0x0672;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:630:0x102f, code lost:
        if (r28.A01 != null) goto L_0x1031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:633:0x1040, code lost:
        if (r28.A06() != false) goto L_0x1042;
     */
    /* JADX WARNING: Removed duplicated region for block: B:594:0x0eee A[Catch: JSONException -> 0x0f5d, TryCatch #6 {JSONException -> 0x0f5d, blocks: (B:552:0x0d25, B:554:0x0d2b, B:556:0x0d2f, B:558:0x0d52, B:561:0x0d82, B:563:0x0dad, B:566:0x0dca, B:569:0x0df8, B:572:0x0e10, B:577:0x0e79, B:578:0x0e86, B:581:0x0e9f, B:583:0x0ea8, B:585:0x0eb6, B:591:0x0ecf, B:592:0x0ee0, B:594:0x0eee, B:595:0x0ef6, B:597:0x0efe, B:598:0x0f3a, B:599:0x0f49), top: B:650:0x0d25 }] */
    /* JADX WARNING: Removed duplicated region for block: B:597:0x0efe A[Catch: JSONException -> 0x0f5d, TryCatch #6 {JSONException -> 0x0f5d, blocks: (B:552:0x0d25, B:554:0x0d2b, B:556:0x0d2f, B:558:0x0d52, B:561:0x0d82, B:563:0x0dad, B:566:0x0dca, B:569:0x0df8, B:572:0x0e10, B:577:0x0e79, B:578:0x0e86, B:581:0x0e9f, B:583:0x0ea8, B:585:0x0eb6, B:591:0x0ecf, B:592:0x0ee0, B:594:0x0eee, B:595:0x0ef6, B:597:0x0efe, B:598:0x0f3a, B:599:0x0f49), top: B:650:0x0d25 }] */
    @Override // X.AbstractC136196Lo
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AV8(X.C130785zy r28) {
        /*
        // Method dump skipped, instructions count: 4316
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxAListenerShape1S0200000_3_I1.AV8(X.5zy):void");
    }
}
