package com.facebook.redex;

import android.view.View;
import com.whatsapp.group.view.custom.GroupDetailsCard;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape2S0100000_I0_2 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape2S0100000_I0_2(GroupDetailsCard groupDetailsCard, int i) {
        this.A01 = i;
        switch (i) {
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
                this.A00 = groupDetailsCard;
                return;
            default:
                this.A00 = groupDetailsCard;
                return;
        }
    }

    public ViewOnClickCListenerShape2S0100000_I0_2(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0239, code lost:
        if (r2.A01() == false) goto L_0x023b;
     */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r12) {
        /*
        // Method dump skipped, instructions count: 1788
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape2S0100000_I0_2.onClick(android.view.View):void");
    }
}
