package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC16230of;
import X.AnonymousClass1QF;
import X.AnonymousClass1QJ;
import X.AnonymousClass1QS;
import X.AnonymousClass1QX;
import X.AnonymousClass1QY;
import X.AnonymousClass1XV;
import X.AnonymousClass3G2;
import X.C16150oX;
import X.C16170oZ;
import X.C39351pj;
import X.C44691zO;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import com.whatsapp.R;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.jid.UserJid;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0600000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public final int A06;

    public RunnableBRunnable0Shape0S0600000_I0(Uri uri, C16170oZ r3, C44691zO r4, UserJid userJid, AbstractC15340mz r6, List list) {
        this.A06 = 1;
        this.A00 = r3;
        this.A01 = uri;
        this.A02 = list;
        this.A03 = r6;
        this.A05 = r4;
        this.A04 = userJid;
    }

    public RunnableBRunnable0Shape0S0600000_I0(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, int i) {
        this.A06 = i;
        this.A00 = obj;
        this.A02 = obj2;
        this.A01 = obj3;
        this.A04 = obj4;
        this.A05 = obj5;
        this.A03 = obj6;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Runnable
    public void run() {
        switch (this.A06) {
            case 0:
                try {
                    Log.v("fb-UnpackingSoSource", "starting syncer worker");
                    RandomAccessFile randomAccessFile = new RandomAccessFile((File) this.A02, "rw");
                    try {
                        randomAccessFile.write((byte[]) this.A01);
                        randomAccessFile.setLength(randomAccessFile.getFilePointer());
                        randomAccessFile.close();
                        File file = ((AnonymousClass1QF) ((AnonymousClass1QJ) this.A00)).A01;
                        randomAccessFile = new RandomAccessFile(new File(file, "dso_manifest"), "rw");
                        try {
                            randomAccessFile.writeByte(1);
                            AnonymousClass1QX[] r7 = ((AnonymousClass1QY) this.A04).A00;
                            int length = r7.length;
                            randomAccessFile.writeInt(length);
                            for (int i = 0; i < length; i++) {
                                randomAccessFile.writeUTF(r7[i].A01);
                                randomAccessFile.writeUTF(r7[i].A00);
                            }
                            randomAccessFile.close();
                            AnonymousClass3G2.A00(file);
                            AnonymousClass1QJ.A03((File) this.A05, (byte) 1);
                            StringBuilder sb = new StringBuilder();
                            sb.append("releasing dso store lock for ");
                            sb.append(file);
                            sb.append(" (from syncer thread)");
                            Log.v("fb-UnpackingSoSource", sb.toString());
                            ((AnonymousClass1QS) this.A03).close();
                            return;
                        } finally {
                            try {
                                throw th;
                            } catch (Throwable th) {
                            }
                        }
                    } finally {
                        try {
                            throw th;
                        } catch (Throwable th) {
                            try {
                                randomAccessFile.close();
                            } catch (Throwable unused) {
                            }
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            case 1:
                C16170oZ r2 = (C16170oZ) this.A00;
                Uri uri = (Uri) this.A01;
                List<AbstractC14640lm> list = (List) this.A02;
                AbstractC15340mz r13 = (AbstractC15340mz) this.A03;
                C44691zO r1 = (C44691zO) this.A05;
                UserJid userJid = (UserJid) this.A04;
                try {
                    Bitmap A07 = r2.A1P.A07(uri, 100, 100);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    try {
                        A07.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        byteArrayOutputStream.close();
                        A07.recycle();
                        ArrayList arrayList = new ArrayList(list.size());
                        for (AbstractC14640lm r12 : list) {
                            C16150oX r10 = new C16150oX();
                            AnonymousClass1XV r0 = (AnonymousClass1XV) r2.A11.A03(uri, r10, null, r12, r13, null, null, null, null, (byte) 23, 0, 0, false);
                            r1.A00(r0);
                            r0.A01 = userJid;
                            arrayList.add(r0);
                        }
                        r2.A05.A0H(new RunnableBRunnable0Shape0S0300000_I0(r2, arrayList, byteArray, 9));
                        return;
                    } catch (Throwable th2) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Throwable unused2) {
                        }
                        throw th2;
                    }
                } catch (C39351pj | IOException | OutOfMemoryError e2) {
                    r2.A05.A05(R.string.catalog_product_send_message_failed, 0);
                    com.whatsapp.util.Log.e("userActionSendProductMessages/product thumbnail load failed", e2);
                    return;
                }
            case 2:
                CommunityTabViewModel communityTabViewModel = (CommunityTabViewModel) this.A00;
                ((AbstractC16230of) this.A01).A03(communityTabViewModel.A09);
                ((AbstractC16230of) this.A02).A03(communityTabViewModel.A0C);
                ((AbstractC16230of) this.A03).A03(communityTabViewModel.A03);
                ((AbstractC16230of) this.A04).A03(communityTabViewModel.A02);
                ((AbstractC16230of) this.A05).A03(communityTabViewModel.A01);
                return;
            default:
                return;
        }
    }
}
