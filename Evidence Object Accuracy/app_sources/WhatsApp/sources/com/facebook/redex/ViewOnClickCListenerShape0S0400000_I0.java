package com.facebook.redex;

import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0400000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public ViewOnClickCListenerShape0S0400000_I0(Object obj, Object obj2, Object obj3, Object obj4, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:70:0x029f, code lost:
        if (r1.A00(new X.C102534pI(r5), r4, r12) == false) goto L_0x02a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x02bd, code lost:
        if (r0 == false) goto L_0x02bf;
     */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x02b0  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02b7  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02c4  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x02f2  */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r19) {
        /*
        // Method dump skipped, instructions count: 766
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape0S0400000_I0.onClick(android.view.View):void");
    }
}
