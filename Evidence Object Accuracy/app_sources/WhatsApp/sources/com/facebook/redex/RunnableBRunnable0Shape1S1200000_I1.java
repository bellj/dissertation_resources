package com.facebook.redex;

import X.FragmentC51722Yg;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.common.api.internal.zzd;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S1200000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public String A02;
    public final int A03;

    public RunnableBRunnable0Shape1S1200000_I1(LifecycleCallback lifecycleCallback, FragmentC51722Yg r3) {
        this.A03 = 0;
        this.A01 = r3;
        this.A00 = lifecycleCallback;
        this.A02 = "ConnectionlessLifecycleHelper";
    }

    public RunnableBRunnable0Shape1S1200000_I1(LifecycleCallback lifecycleCallback, zzd zzd) {
        this.A03 = 1;
        this.A01 = zzd;
        this.A00 = lifecycleCallback;
        this.A02 = "ConnectionlessLifecycleHelper";
    }

    public RunnableBRunnable0Shape1S1200000_I1(Object obj, Object obj2, String str, int i) {
        this.A03 = i;
        this.A01 = obj;
        this.A02 = str;
        this.A00 = obj2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:195:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x023f A[Catch: SQLiteException -> 0x0282, all -> 0x028e, TryCatch #7 {SQLiteException -> 0x0282, blocks: (B:74:0x01e7, B:76:0x01f1, B:78:0x0202, B:80:0x0212, B:81:0x0217, B:83:0x021d, B:85:0x0223, B:86:0x0233, B:88:0x0239, B:90:0x023f, B:91:0x0245, B:92:0x024b, B:94:0x0260), top: B:186:0x01e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0278 A[LOOP:2: B:96:0x0272->B:98:0x0278, LOOP_END] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1288
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1.run():void");
    }
}
