package com.facebook.redex;

import X.C18240s8;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0210000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public boolean A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0210000_I0(C18240s8 r2, Runnable runnable) {
        this.A03 = 7;
        this.A00 = r2;
        this.A02 = true;
        this.A01 = runnable;
    }

    public RunnableBRunnable0Shape0S0210000_I0(Object obj, Object obj2, int i, boolean z) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = z;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:230|117|(4:119|(1:(3:122|347|128))|130|198)|224|131|365|198) */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0355, code lost:
        if (r0 == false) goto L_0x0357;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x04f7, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x04f8, code lost:
        com.whatsapp.util.Log.e("LightSharedPreferencesImpl/writeToFile: Got exception:", r1);
        r5.A04 = false;
        r5.A03.countDown();
     */
    /* JADX WARNING: Removed duplicated region for block: B:256:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0103  */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1362
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0.run():void");
    }
}
