package com.facebook.redex;

import X.AnonymousClass10C;
import X.AnonymousClass1IS;
import X.C15650ng;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0201100_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public long A01;
    public Object A02;
    public Object A03;
    public final int A04 = 0;

    public RunnableBRunnable0Shape0S0201100_I0(C15650ng r2, AnonymousClass1IS r3, int i, long j) {
        this.A02 = r2;
        this.A03 = r3;
        this.A00 = i;
        this.A01 = j;
    }

    public RunnableBRunnable0Shape0S0201100_I0(AnonymousClass10C r2, List list, long j) {
        this.A02 = r2;
        this.A03 = list;
        this.A00 = 4;
        this.A01 = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:150:0x05af, code lost:
        if (r1.getRawString().compareTo(r0.getRawString()) < 0) goto L_0x05b1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x04a1 A[Catch: all -> 0x0651, TryCatch #7 {all -> 0x0656, blocks: (B:95:0x0440, B:108:0x04d3, B:96:0x0444, B:99:0x0457, B:102:0x048e, B:103:0x049b, B:105:0x04a1, B:106:0x04cc), top: B:197:0x0440 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x04dc A[Catch: all -> 0x065b, TRY_LEAVE, TryCatch #18 {all -> 0x0660, blocks: (B:45:0x019a, B:156:0x05d0, B:46:0x019e, B:49:0x01a6, B:51:0x01ae, B:53:0x01b4, B:55:0x01bc, B:56:0x01fa, B:58:0x0200, B:59:0x0216, B:60:0x0255, B:61:0x0262, B:62:0x0264, B:64:0x026f, B:66:0x0275, B:68:0x027d, B:69:0x02f0, B:70:0x02fd, B:72:0x0300, B:91:0x03bb, B:92:0x03bc, B:93:0x03bf, B:94:0x0438, B:109:0x04d6, B:111:0x04dc, B:119:0x051c, B:120:0x051d, B:121:0x0520, B:123:0x0528, B:125:0x0532, B:128:0x0542, B:130:0x054f, B:132:0x0559, B:137:0x0566, B:139:0x056a, B:141:0x0572, B:143:0x058a, B:146:0x0592, B:147:0x0593, B:149:0x05a3, B:151:0x05b1, B:152:0x05b5, B:154:0x05bd, B:155:0x05c5, B:185:0x065a), top: B:213:0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0528 A[Catch: all -> 0x065b, TryCatch #18 {all -> 0x0660, blocks: (B:45:0x019a, B:156:0x05d0, B:46:0x019e, B:49:0x01a6, B:51:0x01ae, B:53:0x01b4, B:55:0x01bc, B:56:0x01fa, B:58:0x0200, B:59:0x0216, B:60:0x0255, B:61:0x0262, B:62:0x0264, B:64:0x026f, B:66:0x0275, B:68:0x027d, B:69:0x02f0, B:70:0x02fd, B:72:0x0300, B:91:0x03bb, B:92:0x03bc, B:93:0x03bf, B:94:0x0438, B:109:0x04d6, B:111:0x04dc, B:119:0x051c, B:120:0x051d, B:121:0x0520, B:123:0x0528, B:125:0x0532, B:128:0x0542, B:130:0x054f, B:132:0x0559, B:137:0x0566, B:139:0x056a, B:141:0x0572, B:143:0x058a, B:146:0x0592, B:147:0x0593, B:149:0x05a3, B:151:0x05b1, B:152:0x05b5, B:154:0x05bd, B:155:0x05c5, B:185:0x065a), top: B:213:0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0532 A[Catch: all -> 0x065b, TryCatch #18 {all -> 0x0660, blocks: (B:45:0x019a, B:156:0x05d0, B:46:0x019e, B:49:0x01a6, B:51:0x01ae, B:53:0x01b4, B:55:0x01bc, B:56:0x01fa, B:58:0x0200, B:59:0x0216, B:60:0x0255, B:61:0x0262, B:62:0x0264, B:64:0x026f, B:66:0x0275, B:68:0x027d, B:69:0x02f0, B:70:0x02fd, B:72:0x0300, B:91:0x03bb, B:92:0x03bc, B:93:0x03bf, B:94:0x0438, B:109:0x04d6, B:111:0x04dc, B:119:0x051c, B:120:0x051d, B:121:0x0520, B:123:0x0528, B:125:0x0532, B:128:0x0542, B:130:0x054f, B:132:0x0559, B:137:0x0566, B:139:0x056a, B:141:0x0572, B:143:0x058a, B:146:0x0592, B:147:0x0593, B:149:0x05a3, B:151:0x05b1, B:152:0x05b5, B:154:0x05bd, B:155:0x05c5, B:185:0x065a), top: B:213:0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0542 A[Catch: all -> 0x065b, TryCatch #18 {all -> 0x0660, blocks: (B:45:0x019a, B:156:0x05d0, B:46:0x019e, B:49:0x01a6, B:51:0x01ae, B:53:0x01b4, B:55:0x01bc, B:56:0x01fa, B:58:0x0200, B:59:0x0216, B:60:0x0255, B:61:0x0262, B:62:0x0264, B:64:0x026f, B:66:0x0275, B:68:0x027d, B:69:0x02f0, B:70:0x02fd, B:72:0x0300, B:91:0x03bb, B:92:0x03bc, B:93:0x03bf, B:94:0x0438, B:109:0x04d6, B:111:0x04dc, B:119:0x051c, B:120:0x051d, B:121:0x0520, B:123:0x0528, B:125:0x0532, B:128:0x0542, B:130:0x054f, B:132:0x0559, B:137:0x0566, B:139:0x056a, B:141:0x0572, B:143:0x058a, B:146:0x0592, B:147:0x0593, B:149:0x05a3, B:151:0x05b1, B:152:0x05b5, B:154:0x05bd, B:155:0x05c5, B:185:0x065a), top: B:213:0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0566 A[Catch: all -> 0x065b, TryCatch #18 {all -> 0x0660, blocks: (B:45:0x019a, B:156:0x05d0, B:46:0x019e, B:49:0x01a6, B:51:0x01ae, B:53:0x01b4, B:55:0x01bc, B:56:0x01fa, B:58:0x0200, B:59:0x0216, B:60:0x0255, B:61:0x0262, B:62:0x0264, B:64:0x026f, B:66:0x0275, B:68:0x027d, B:69:0x02f0, B:70:0x02fd, B:72:0x0300, B:91:0x03bb, B:92:0x03bc, B:93:0x03bf, B:94:0x0438, B:109:0x04d6, B:111:0x04dc, B:119:0x051c, B:120:0x051d, B:121:0x0520, B:123:0x0528, B:125:0x0532, B:128:0x0542, B:130:0x054f, B:132:0x0559, B:137:0x0566, B:139:0x056a, B:141:0x0572, B:143:0x058a, B:146:0x0592, B:147:0x0593, B:149:0x05a3, B:151:0x05b1, B:152:0x05b5, B:154:0x05bd, B:155:0x05c5, B:185:0x065a), top: B:213:0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x05ee  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0457 A[Catch: all -> 0x0651, TRY_ENTER, TryCatch #7 {all -> 0x0656, blocks: (B:95:0x0440, B:108:0x04d3, B:96:0x0444, B:99:0x0457, B:102:0x048e, B:103:0x049b, B:105:0x04a1, B:106:0x04cc), top: B:197:0x0440 }] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1646
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0201100_I0.run():void");
    }
}
