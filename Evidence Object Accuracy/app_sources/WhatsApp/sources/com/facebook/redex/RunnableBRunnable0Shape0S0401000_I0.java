package com.facebook.redex;

import X.AbstractC15340mz;
import X.AbstractC16010oI;
import X.AbstractC16130oV;
import X.AnonymousClass1FY;
import X.AnonymousClass1JS;
import X.AnonymousClass1JV;
import X.AnonymousClass1OT;
import X.AnonymousClass1P4;
import X.AnonymousClass1RY;
import X.AnonymousClass1X7;
import X.AnonymousClass2QE;
import X.AnonymousClass3ZP;
import X.C14830m7;
import X.C14860mA;
import X.C15650ng;
import X.C15930o9;
import X.C15940oB;
import X.C15950oC;
import X.C15990oG;
import X.C16020oJ;
import X.C16050oM;
import X.C16150oX;
import X.C20710wC;
import X.C22130yZ;
import X.C22890zo;
import X.C26651Eh;
import X.C30421Xi;
import X.C43521xA;
import X.C617432a;
import X.C63323Bd;
import X.C68633Wd;
import com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0401000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public final int A05;

    public RunnableBRunnable0Shape0S0401000_I0(Object obj, Object obj2, Object obj3, Object obj4, int i, int i2) {
        this.A05 = i2;
        this.A01 = obj;
        this.A02 = obj2;
        this.A00 = i;
        this.A03 = obj3;
        this.A04 = obj4;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C15990oG r3;
        C16020oJ A09;
        switch (this.A05) {
            case 0:
                C22890zo r7 = (C22890zo) this.A01;
                AbstractC16130oV r6 = (AbstractC16130oV) this.A02;
                int i = this.A00;
                AbstractC15340mz r4 = (AbstractC15340mz) this.A03;
                C16150oX r32 = (C16150oX) this.A04;
                boolean z = false;
                if (i == 1 || i == 2 || (r6 instanceof AnonymousClass1X7) || ((r6 instanceof C30421Xi) && ((AbstractC15340mz) r6).A08 == 1)) {
                    z = true;
                }
                if (r32 != null) {
                    r32.A0P = false;
                }
                r6.A0S();
                if (z) {
                    r7.A01.A05(r6, false, false);
                    return;
                } else {
                    r7.A06.A0a(r4, -1);
                    return;
                }
            case 1:
                VoiceService$VoiceServiceEventCallback.m2$r8$lambda$1o_3JGdLUSkoA2RmlNGKgOFkTM((VoiceService$VoiceServiceEventCallback) this.A01, (Voip.RecordingInfo[]) this.A02, (Voip.DebugTapType) this.A03, (byte[]) this.A04, this.A00);
                return;
            case 2:
                C43521xA r42 = (C43521xA) this.A01;
                DeviceJid deviceJid = (DeviceJid) this.A02;
                byte[] bArr = (byte[]) this.A03;
                byte[] bArr2 = (byte[]) this.A04;
                byte b = (byte) this.A00;
                boolean z2 = false;
                if (deviceJid.device == 0) {
                    z2 = true;
                }
                if (!z2) {
                    C22130yZ r5 = r42.A0I;
                    if (!r5.A0D(deviceJid, bArr, bArr2, b, 5)) {
                        Log.e("recvmessagelistener/on-get-identity-success/invalid device identity");
                        r5.A09(deviceJid, true);
                        return;
                    }
                }
                try {
                    r42.A0C.A0U(new AnonymousClass1JS(C15940oB.A01(C16050oM.A05(new byte[]{b}, bArr2))), C15940oB.A02(deviceJid));
                    r42.A01.A0H(new RunnableBRunnable0Shape6S0200000_I0_6(r42, 13, deviceJid));
                    return;
                } catch (AnonymousClass1RY e) {
                    Log.e("recvmessagelistener/on-get-identity-success/invalidkey/", e);
                    return;
                }
            case 3:
                AnonymousClass2QE r1 = (AnonymousClass2QE) this.A04;
                C63323Bd r8 = new C63323Bd(null, (AnonymousClass1JV) this.A03, (AnonymousClass1P4) this.A02, r1.A08, null, r1.A0E, this.A00, false);
                C26651Eh r72 = (C26651Eh) this.A01;
                C14830m7 r62 = r72.A0O;
                C14860mA r52 = r72.A1W;
                C15650ng r43 = r72.A0b;
                C20710wC r33 = r72.A0s;
                C617432a r9 = new C617432a(this, r62, r72.A0Y, r43, r33, r8, r72.A1H, r52);
                new AnonymousClass3ZP(r72.A02, r72.A05, r62, r72.A0r, r33, r9, r9.A04, r72.A10).A00();
                return;
            case 4:
                AnonymousClass1FY r63 = (AnonymousClass1FY) this.A01;
                DeviceJid deviceJid2 = (DeviceJid) this.A02;
                int i2 = this.A00;
                AnonymousClass1OT r82 = (AnonymousClass1OT) this.A03;
                C15930o9 r53 = (C15930o9) this.A04;
                StringBuilder sb = new StringBuilder("axolotl received a location notification; jid=");
                sb.append(deviceJid2);
                sb.append("; retryCount=");
                sb.append(i2);
                Log.i(sb.toString());
                C15950oC A02 = C15940oB.A02(deviceJid2);
                C68633Wd r12 = new AbstractC16010oI(A02, r63, r82) { // from class: X.3Wd
                    public final /* synthetic */ C15950oC A00;
                    public final /* synthetic */ AnonymousClass1FY A01;
                    public final /* synthetic */ AnonymousClass1OT A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC16010oI
                    public final void AI3(byte[] bArr3) {
                        StringBuilder A0h;
                        String str;
                        char c;
                        AnonymousClass1FY r73 = this.A01;
                        AnonymousClass1OT r2 = this.A02;
                        C15950oC r64 = this.A00;
                        byte[] A0N = C32401c6.A0N(null, null, bArr3);
                        if (A0N == null) {
                            A0h = C12960it.A0h();
                            str = "axolotl derived invalid plaintext; stanzaKey=";
                        } else {
                            try {
                                C27081Fy A01 = C27081Fy.A01(A0N);
                                List A0A = C32401c6.A0A(A01);
                                if (A0A.size() != 0) {
                                    A0h = C12960it.A0k("axolotl received an invalid protobuf; stanzaKey=");
                                    A0h.append(r2);
                                    A0h.append("; messageTypes=");
                                    A0h.append(A0A);
                                    Log.w(A0h.toString());
                                } else if ((A01.A00 & 16384) == 16384) {
                                    Log.i(C12960it.A0b("axolotl received sender key distribution message; stanzaKey=", r2));
                                    C57242mi r54 = A01.A0a;
                                    if (r54 == null) {
                                        r54 = C57242mi.A03;
                                    }
                                    int i3 = r54.A00;
                                    if ((i3 & 1) == 1 && (i3 & 2) == 2) {
                                        C15980oF r44 = new C15980oF(r64, C15960oD.A00.getRawString());
                                        C15990oG r34 = r73.A03;
                                        byte[] A04 = r54.A01.A04();
                                        Lock A012 = r34.A0J.A01(r44);
                                        try {
                                            if (A012 == null) {
                                                r34.A0I.A00();
                                            } else {
                                                A012.lock();
                                            }
                                            C31201aA r0 = r34.A00;
                                            try {
                                                try {
                                                    C31821bA r35 = new C31821bA(A04);
                                                    C31811b9 r02 = new C31811b9(r0.A00.A01);
                                                    C31351aP A022 = C29201Rg.A02(r44);
                                                    synchronized (C31411aV.A02) {
                                                        C31401aU r55 = r02.A00;
                                                        C29271Rn A00 = r55.A00(A022);
                                                        int i4 = r35.A00;
                                                        int i5 = r35.A01;
                                                        byte[][] bArr4 = r35.A04;
                                                        C31491ad r11 = r35.A02;
                                                        LinkedList linkedList = A00.A00;
                                                        linkedList.addFirst(new C31451aZ(r11, C31671av.A00, bArr4, i4, i5));
                                                        if (linkedList.size() > 5) {
                                                            linkedList.removeLast();
                                                        }
                                                        r55.A01(A022, A00);
                                                    }
                                                    c = 0;
                                                } catch (C31571al e2) {
                                                    Log.w("axolotl", e2);
                                                    c = 64529;
                                                }
                                            } catch (C31521ag e3) {
                                                Log.w("axolotl", e3);
                                                c = 64531;
                                            }
                                            if (c == 64531) {
                                                A0h = C12960it.A0h();
                                                str = "axolotl received invalid sender key distribution message; stanzaKey=";
                                            } else if (c == 64529) {
                                                A0h = C12960it.A0h();
                                                str = "axolotl received legacy sender key distribution message; stanzaKey=";
                                            } else {
                                                return;
                                            }
                                        } finally {
                                            if (A012 != null) {
                                                A012.unlock();
                                            }
                                        }
                                    } else {
                                        A0h = C12960it.A0h();
                                        str = "axolotl received incomplete sender key distribution message; stanzaKey=";
                                    }
                                } else {
                                    return;
                                }
                            } catch (C28971Pt e4) {
                                Log.w(C12960it.A0b("axolotl derived plaintext does not represent valid protocol buffer; stanzaKey=", r2), e4);
                                return;
                            }
                        }
                        A0h.append(str);
                        A0h.append(r2);
                        Log.w(A0h.toString());
                    }
                };
                int i3 = r53.A00;
                if (i3 == 0) {
                    r3 = r63.A03;
                    A09 = r3.A08(r12, A02, r53.A02);
                } else if (i3 == 1) {
                    r3 = r63.A03;
                    A09 = r3.A09(r12, A02, r53.A02);
                } else {
                    StringBuilder sb2 = new StringBuilder("axolotl unrecognized ciphertext type; stanzaKey=");
                    sb2.append(r82);
                    sb2.append("; type=");
                    sb2.append(i3);
                    Log.w(sb2.toString());
                    return;
                }
                int i4 = A09.A00;
                if (i4 != 0) {
                    StringBuilder sb3 = new StringBuilder("axolotl error; status=");
                    sb3.append(i4);
                    Log.w(sb3.toString());
                    if (i4 == -1005 || i4 == -1008 || i4 == -1003 || i4 == -1002) {
                        r63.A00.A0H(new RunnableBRunnable0Shape0S0202000_I0(r63, r3.A07.A01(), r82, i2, 4));
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
