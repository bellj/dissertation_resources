package com.facebook.redex;

import com.whatsapp.registration.VerifyTwoFactorAuth;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape11S0100000_I0_11 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape11S0100000_I0_11(VerifyTwoFactorAuth verifyTwoFactorAuth, int i) {
        this.A01 = i;
        switch (i) {
            case 0:
            case 1:
                this.A00 = verifyTwoFactorAuth;
                return;
            default:
                this.A00 = verifyTwoFactorAuth;
                return;
        }
    }

    public RunnableBRunnable0Shape11S0100000_I0_11(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:232:0x058f, code lost:
        if (r1 == 0) goto L_0x0591;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 2406
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11.run():void");
    }
}
