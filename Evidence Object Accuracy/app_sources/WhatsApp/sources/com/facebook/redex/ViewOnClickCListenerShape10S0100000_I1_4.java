package com.facebook.redex;

import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape10S0100000_I1_4 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape10S0100000_I1_4(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:186:0x058d, code lost:
        if (r1 == false) goto L_0x058f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x06de  */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0732  */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r17) {
        /*
        // Method dump skipped, instructions count: 2396
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape10S0100000_I1_4.onClick(android.view.View):void");
    }
}
