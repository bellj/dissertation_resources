package com.facebook.redex;

import X.AbstractC14440lR;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.settings.SettingsDataUsageActivity;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S1100000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public String A01;
    public final int A02;

    public ViewOnClickCListenerShape0S1100000_I0(int i, String str, Object obj) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = str;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        String str;
        AbstractC14440lR r3;
        int i;
        ActivityC13830kP r1;
        switch (this.A02) {
            case 0:
                r1 = (ActivityC13830kP) this.A00;
                str = this.A01;
                r3 = r1.A05;
                i = 8;
                break;
            case 1:
                r1 = (ActivityC13830kP) this.A00;
                str = this.A01;
                r3 = r1.A05;
                i = 7;
                break;
            case 2:
                ActivityC13830kP r5 = (ActivityC13830kP) this.A00;
                String str2 = this.A01;
                r5.A05.Ab2(new RunnableBRunnable0Shape0S1200000_I0(r5, new SettingsGoogleDrive.AuthRequestDialogFragment(), str2, 6));
                return;
            case 3:
                ((BusinessDirectorySearchQueryViewModel) this.A00).A0R(this.A01);
                return;
            case 4:
                ActivityC13810kN r4 = (ActivityC13810kN) this.A00;
                String str3 = this.A01;
                Uri parse = Uri.parse(str3);
                if (parse.getScheme() == null) {
                    StringBuilder sb = new StringBuilder("http://");
                    sb.append(str3);
                    parse = Uri.parse(sb.toString());
                }
                try {
                    r4.startActivity(new Intent("android.intent.action.VIEW", parse));
                    return;
                } catch (ActivityNotFoundException unused) {
                    r4.A05.A07(R.string.activity_not_found, 0);
                    return;
                }
            case 5:
                SettingsDataUsageActivity.A02((SettingsDataUsageActivity) this.A00, this.A01);
                return;
            default:
                return;
        }
        r3.Ab2(new RunnableBRunnable0Shape0S1100000_I0(i, str, r1));
    }
}
