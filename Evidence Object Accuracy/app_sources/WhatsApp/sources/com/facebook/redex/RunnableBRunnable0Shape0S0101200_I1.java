package com.facebook.redex;

import X.ActivityC13830kP;
import X.AnonymousClass3HD;
import X.AnonymousClass3JP;
import X.AnonymousClass4PD;
import X.AnonymousClass4XT;
import X.C106424vg;
import X.C12960it;
import X.C12970iu;
import X.C28741Ov;
import X.C44891zj;
import X.C58902rx;
import X.C92474Wb;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0101200_I1 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public long A01;
    public long A02;
    public Object A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0101200_I1(Object obj, int i, int i2, long j, long j2) {
        this.A04 = i2;
        this.A03 = obj;
        this.A00 = i;
        this.A01 = j;
        this.A02 = j2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C28741Ov r0;
        switch (this.A04) {
            case 0:
                ((C92474Wb) this.A03).A01.AMZ(this.A00, this.A01, this.A02);
                return;
            case 1:
                C106424vg r3 = (C106424vg) ((AnonymousClass4PD) this.A03).A02;
                AnonymousClass3HD r1 = r3.A06;
                if (r1.A03.isEmpty()) {
                    r0 = null;
                } else {
                    r0 = (C28741Ov) AnonymousClass3JP.getLast(r1.A03);
                }
                AnonymousClass4XT A03 = r3.A03(r0);
                r3.A05(A03, new IDxEventShape18S0100000_2_I1(A03, 15), 1006);
                return;
            case 2:
                int i = this.A00;
                long j = this.A01;
                long j2 = this.A02;
                RestoreFromBackupActivity restoreFromBackupActivity = ((C58902rx) this.A03).A01;
                if (!restoreFromBackupActivity.A33()) {
                    restoreFromBackupActivity.A05.setIndeterminate(false);
                    restoreFromBackupActivity.A05.setProgress(i);
                    TextView textView = restoreFromBackupActivity.A08;
                    Object[] objArr = new Object[3];
                    objArr[0] = C44891zj.A03(((ActivityC13830kP) restoreFromBackupActivity).A01, j);
                    objArr[1] = C44891zj.A03(((ActivityC13830kP) restoreFromBackupActivity).A01, j2);
                    textView.setText(C12960it.A0X(restoreFromBackupActivity, C12970iu.A0r(((ActivityC13830kP) restoreFromBackupActivity).A01, i), objArr, 2, R.string.settings_gdrive_restore_progress_message_with_percentage));
                    return;
                }
                Log.i("gdrive-activity-observer/msgstore-download-progress/activity-already-exited");
                return;
            default:
                return;
        }
    }
}
