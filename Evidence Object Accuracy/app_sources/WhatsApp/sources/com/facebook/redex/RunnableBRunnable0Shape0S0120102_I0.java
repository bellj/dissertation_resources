package com.facebook.redex;

import com.whatsapp.mediaview.PhotoView;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0120102_I0 extends EmptyBaseRunnable0 implements Runnable {
    public float A00;
    public float A01;
    public long A02 = -1;
    public Object A03;
    public boolean A04;
    public boolean A05;
    public final int A06;

    public RunnableBRunnable0Shape0S0120102_I0(PhotoView photoView, int i) {
        this.A06 = i;
        this.A03 = photoView;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b8, code lost:
        if (r2 < 0.0f) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ba, code lost:
        r10.A00 = 0.0f;
        r2 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00bd, code lost:
        r1 = r10.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c1, code lost:
        if (r1 <= 0.0f) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c3, code lost:
        r1 = r1 - r6;
        r10.A01 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c8, code lost:
        if (r1 >= 0.0f) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ca, code lost:
        r10.A01 = 0.0f;
        r1 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00cf, code lost:
        if (r2 != 0.0f) goto L_0x00d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d3, code lost:
        if (r1 == 0.0f) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d5, code lost:
        if (r4 != false) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d7, code lost:
        r10.A04 = false;
        r10.A05 = true;
        r3.A09(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e3, code lost:
        if (r10.A05 != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e5, code lost:
        r3.post(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e8, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00e9, code lost:
        r1 = r1 + r6;
        r10.A01 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00ee, code lost:
        if (r1 <= 0.0f) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f6, code lost:
        if (r2 > 0.0f) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 260
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0120102_I0.run():void");
    }
}
