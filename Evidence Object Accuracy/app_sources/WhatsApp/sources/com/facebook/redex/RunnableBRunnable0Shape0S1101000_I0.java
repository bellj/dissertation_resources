package com.facebook.redex;

import X.ActivityC13810kN;
import X.AnonymousClass3JL;
import X.AnonymousClass4CA;
import X.C16430p0;
import X.C28531Ny;
import X.C44771zW;
import X.C59582uy;
import X.C77443nI;
import X.C77453nJ;
import android.accounts.Account;
import android.content.Intent;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.service.MDSyncService;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1101000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public String A02;
    public final int A03;

    public RunnableBRunnable0Shape0S1101000_I0(Object obj, String str, int i, int i2) {
        this.A03 = i2;
        this.A01 = obj;
        this.A02 = str;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ArrayList arrayList;
        int i;
        switch (this.A03) {
            case 0:
                RestoreFromBackupActivity restoreFromBackupActivity = (RestoreFromBackupActivity) this.A01;
                String str = this.A02;
                int i2 = this.A00;
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("gdrive-activity/auth-request asking GoogleAuthUtil for token for ");
                    sb.append(C44771zW.A0B(str));
                    Log.i(sb.toString());
                    restoreFromBackupActivity.A0e = AnonymousClass3JL.A01(new Account(str, "com.google"), restoreFromBackupActivity);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("gdrive-activity/auth-request for account ");
                    sb2.append(C44771zW.A0B(str));
                    sb2.append(", token has been received.");
                    Log.i(sb2.toString());
                    Intent intent = new Intent();
                    intent.putExtra("authtoken", restoreFromBackupActivity.A0e);
                    intent.putExtra("authAccount", str);
                    restoreFromBackupActivity.onActivityResult(i2, -1, intent);
                    restoreFromBackupActivity.A0m.open();
                    return;
                } catch (C77443nI e) {
                    ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(restoreFromBackupActivity, 38));
                    Log.e("gdrive-activity/gps-unavailable", e);
                    restoreFromBackupActivity.A0e = null;
                    return;
                } catch (C77453nJ e2) {
                    restoreFromBackupActivity.A0m.close();
                    restoreFromBackupActivity.A0e = null;
                    restoreFromBackupActivity.A2w(null, 25);
                    ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape0S0201000_I0(restoreFromBackupActivity, e2, i2, 7));
                    return;
                } catch (AnonymousClass4CA | SecurityException e3) {
                    Log.e("gdrive-activity/auth-request", e3);
                    restoreFromBackupActivity.A0e = null;
                    restoreFromBackupActivity.A0m.open();
                    ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape0S1100000_I0(9, str, restoreFromBackupActivity));
                    return;
                } catch (IOException e4) {
                    Log.e("gdrive-activity/auth-request", e4);
                    restoreFromBackupActivity.A0e = null;
                    restoreFromBackupActivity.A0m.open();
                    ((ActivityC13810kN) restoreFromBackupActivity).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(restoreFromBackupActivity, 34));
                    return;
                }
            case 1:
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = (BusinessDirectorySearchQueryViewModel) this.A01;
                int i3 = this.A00;
                String str2 = this.A02;
                businessDirectorySearchQueryViewModel.A0C.removeCallbacks(businessDirectorySearchQueryViewModel.A06);
                if (i3 == -1) {
                    businessDirectorySearchQueryViewModel.A0P.A04();
                    arrayList = new ArrayList(businessDirectorySearchQueryViewModel.A06());
                    i = 1;
                } else if (i3 == 1 || i3 == 2 || i3 == 3) {
                    businessDirectorySearchQueryViewModel.A0P.A04();
                    arrayList = new ArrayList(businessDirectorySearchQueryViewModel.A06());
                    i = 2;
                } else if (i3 == 4) {
                    C16430p0 r4 = businessDirectorySearchQueryViewModel.A0L;
                    C28531Ny r2 = new C28531Ny();
                    r2.A06 = 46;
                    r2.A0I = 1L;
                    r2.A05 = 6;
                    r4.A03(r2);
                    businessDirectorySearchQueryViewModel.A0Z.A0A(2);
                    return;
                } else {
                    return;
                }
                arrayList.add(new C59582uy(businessDirectorySearchQueryViewModel, str2, i));
                C16430p0 r42 = businessDirectorySearchQueryViewModel.A0L;
                Integer valueOf = Integer.valueOf(i);
                C28531Ny r22 = new C28531Ny();
                r22.A06 = 46;
                r22.A0I = 1L;
                r22.A05 = valueOf;
                r42.A03(r22);
                businessDirectorySearchQueryViewModel.A0G.A0A(arrayList);
                return;
            case 2:
                MDSyncService mDSyncService = (MDSyncService) this.A01;
                String str3 = this.A02;
                int i4 = this.A00;
                mDSyncService.A0C = str3;
                mDSyncService.A03(i4);
                return;
            default:
                return;
        }
    }
}
