package com.facebook.redex;

import X.AnonymousClass1S7;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S2000000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public String A00;
    public String A01;
    public final int A02;

    public RunnableBRunnable0Shape0S2000000_I0(String str, String str2, int i) {
        this.A02 = i;
        if (i != 0) {
            this.A00 = str;
            this.A01 = "close_call_link_lobby";
            return;
        }
        this.A00 = str;
        this.A01 = str2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A02) {
            case 0:
                String str = this.A00;
                String str2 = this.A01;
                CallInfo callInfo = Voip.getCallInfo();
                if (callInfo != null) {
                    AnonymousClass1S7 r1 = callInfo.callWaitingInfo;
                    if (r1.A01 != 0 && r1.A04.equals(str)) {
                        Voip.rejectPendingCall(str);
                        return;
                    }
                }
                Voip.rejectCall(str, str2);
                return;
            case 1:
                Voip.rejectCall(this.A00, this.A01);
                return;
            default:
                return;
        }
    }
}
