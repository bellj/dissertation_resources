package com.facebook.redex;

import X.AbstractC14670lq;
import X.AbstractC15340mz;
import X.AnonymousClass018;
import X.AnonymousClass19O;
import X.AnonymousClass19V;
import X.AnonymousClass1IS;
import X.AnonymousClass1SF;
import X.AnonymousClass1V8;
import X.AnonymousClass1VY;
import X.AnonymousClass1W9;
import X.AnonymousClass23a;
import X.AnonymousClass3XY;
import X.AnonymousClass3ZJ;
import X.AnonymousClass4OY;
import X.AnonymousClass5V6;
import X.C14350lI;
import X.C14690ls;
import X.C14860mA;
import X.C14880mC;
import X.C16310on;
import X.C17150qL;
import X.C17220qS;
import X.C18340sI;
import X.C21300xC;
import X.C23000zz;
import X.C245816b;
import X.C38241nl;
import X.C41561tj;
import X.C41611to;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.view.View;
import com.whatsapp.fieldstats.events.WamCall;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Exchanger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape2S0300000_I0_2 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape2S0300000_I0_2(Location location, C14860mA r4, C14880mC r5) {
        this.A03 = 7;
        this.A02 = r4;
        this.A01 = r5;
        this.A00 = location;
        r5.A01 = location.getLatitude();
        r5.A02 = location.getLongitude();
        r5.A00 = (double) location.getAccuracy();
    }

    public RunnableBRunnable0Shape2S0300000_I0_2(Object obj, Object obj2, Object obj3, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        String str;
        String str2;
        switch (this.A03) {
            case 0:
                C23000zz r5 = (C23000zz) this.A00;
                AnonymousClass3ZJ r10 = new AnonymousClass3ZJ(r5.A03, r5.A06, new AnonymousClass4OY(r5, (List) this.A01), (List) this.A02);
                if (!r10.A00.A0B()) {
                    r10.A02.A00.A0A.A01();
                    return;
                }
                C17220qS r9 = r10.A01;
                String A01 = r9.A01();
                ArrayList arrayList = new ArrayList();
                Iterator it = r10.A03.iterator();
                while (it.hasNext()) {
                    arrayList.add(new AnonymousClass1V8("notice", new AnonymousClass1W9[]{new AnonymousClass1W9("id", (String) it.next())}));
                }
                r9.A0A(r10, new AnonymousClass1V8(new AnonymousClass1V8("request", new AnonymousClass1W9[]{new AnonymousClass1W9("type", "session_update")}, (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0])), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "tos"), new AnonymousClass1W9("type", "set")}), A01, 290, 32000);
                return;
            case 1:
                C18340sI r3 = (C18340sI) this.A00;
                AnonymousClass19V r2 = (AnonymousClass19V) this.A01;
                AnonymousClass23a r1 = (AnonymousClass23a) this.A02;
                if (r3.A02.A0B()) {
                    r2.AZO(new AnonymousClass3XY(r1, r3));
                    return;
                } else if (r1 != null) {
                    r1.AQB(1);
                    return;
                } else {
                    return;
                }
            case 2:
                C21300xC r22 = (C21300xC) this.A00;
                Object obj = this.A01;
                Object obj2 = this.A02;
                if (!r22.A05.A0B() || !C38241nl.A08()) {
                    r22.A01.submit(new RunnableBRunnable0Shape0S0301000_I0(r22, obj, obj2, 5, 7));
                    return;
                }
                return;
            case 3:
                C41561tj r52 = (C41561tj) this.A01;
                Bitmap bitmap = (Bitmap) this.A02;
                ((C41611to) this.A00).A04.A0B.decrementAndGet();
                AnonymousClass19O r23 = r52.A03;
                AbstractC15340mz r32 = r52.A01;
                synchronized (r23) {
                    AnonymousClass1IS r0 = r32.A0z;
                    if (r0 == null || (str = r0.A01) == null) {
                        str = "null";
                    }
                    r23.A05.remove(str);
                    if (bitmap != null) {
                        r23.A02.A03(str, bitmap);
                    }
                }
                Object obj3 = r52.A04;
                View view = r52.A00;
                if (obj3.equals(view.getTag())) {
                    r52.A02.Adg(bitmap, view, r32);
                    return;
                }
                return;
            case 4:
                C14690ls r12 = (C14690ls) this.A01;
                AnonymousClass5V6 r02 = (AnonymousClass5V6) this.A02;
                ((AbstractC14670lq) this.A00).A0P = r12;
                if (r02 != null) {
                    r02.A6p();
                    return;
                }
                return;
            case 5:
                C17150qL r8 = (C17150qL) this.A00;
                File file = (File) this.A01;
                WamCall wamCall = (WamCall) this.A02;
                if (!file.exists() || !file.isDirectory()) {
                    str2 = "app/VoipTimeSeriesLogger: uploadTimeSeries received bad directory path, skipping upload.";
                } else {
                    if (wamCall == null) {
                        Log.i("app/VoipTimeSeriesLogger: injectAdditionalDataToLogs received null fieldStat, skipping injection");
                    } else if (!file.exists() || !file.isDirectory()) {
                        Log.w("app/VoipTimeSeriesLogger: injectAdditionalDataToLogs received bad directory path, skipping injection.");
                    } else {
                        File[] listFiles = file.listFiles();
                        if (listFiles != null) {
                            for (File file2 : listFiles) {
                                try {
                                    FileOutputStream fileOutputStream = new FileOutputStream(file2, true);
                                    C17150qL.A01(fileOutputStream, wamCall.callT, "call_t");
                                    C17150qL.A01(fileOutputStream, wamCall.userRating, "user_rating");
                                    C17150qL.A01(fileOutputStream, wamCall.videoRenderFreezeT, "freeze_t");
                                    C17150qL.A01(fileOutputStream, wamCall.videoRenderFreeze8xT, "freeze_8x");
                                    C17150qL.A01(fileOutputStream, wamCall.videoRenderNumFreezes, "num_freezes");
                                    C17150qL.A00(fileOutputStream, wamCall.jbLost, "jb_lost");
                                    C17150qL.A00(fileOutputStream, wamCall.jbEmpties, "jb_empties");
                                    C17150qL.A00(fileOutputStream, wamCall.jbGets, "jb_gets");
                                    C17150qL.A01(fileOutputStream, wamCall.callAvgRtt, "rtt");
                                    C17150qL.A00(fileOutputStream, wamCall.jbAvgDelay, "jb_delay");
                                    C17150qL.A01(fileOutputStream, wamCall.maxConnectedParticipants, "max_participants");
                                    C17150qL.A01(fileOutputStream, wamCall.numConnectedParticipants, "num_participants");
                                    C17150qL.A00(fileOutputStream, wamCall.videoRxBitrate, "vid_rx_bps");
                                    C17150qL.A01(fileOutputStream, wamCall.renderFreezeHighPeerBweT, "freeze_high_bwe_t");
                                    C17150qL.A01(fileOutputStream, wamCall.highPeerBweT, "high_bwe_t");
                                    C17150qL.A01(fileOutputStream, wamCall.renderFreezeLowToHighPeerBweT, "freeze_mid_bwe_t");
                                    C17150qL.A01(fileOutputStream, wamCall.lowToHighPeerBweT, "mid_bwe_t");
                                    C17150qL.A01(fileOutputStream, wamCall.renderFreezeLowPeerBweT, "freeze_low_bwe_t");
                                    C17150qL.A01(fileOutputStream, wamCall.lowPeerBweT, "low_bwe_t");
                                    C17150qL.A00(fileOutputStream, wamCall.jbLostEmptyLowPeerBwePerSec, "jb_lostempty_pct_low_bwe");
                                    C17150qL.A00(fileOutputStream, wamCall.jbLostEmptyLowToHighPeerBwePerSec, "jb_lostempty_pct_mid_bwe");
                                    C17150qL.A00(fileOutputStream, wamCall.jbLostEmptyHighPeerBwePerSec, "jb_lostempty_pct_high_bwe");
                                    fileOutputStream.close();
                                } catch (IOException e) {
                                    Log.w("app/VoipTimeSeriesLogger: injectAdditionalDataToLogs could not inject into time series file", e);
                                }
                            }
                        }
                    }
                    File A07 = AnonymousClass1SF.A07(r8.A02.A00);
                    StringBuilder sb = new StringBuilder();
                    sb.append(file.getName());
                    sb.append(".zip");
                    File file3 = new File(A07, sb.toString());
                    try {
                        FileOutputStream fileOutputStream2 = new FileOutputStream(file3);
                        ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream2);
                        try {
                            File[] listFiles2 = file.listFiles();
                            if (listFiles2 != null) {
                                for (File file4 : listFiles2) {
                                    FileInputStream fileInputStream = new FileInputStream(file4);
                                    zipOutputStream.putNextEntry(new ZipEntry(file4.getName()));
                                    C14350lI.A0G(fileInputStream, zipOutputStream);
                                    zipOutputStream.closeEntry();
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("app/VoipTimeSeriesLogger: compressed file ");
                                    sb2.append(file4.getName());
                                    sb2.append(" with init size ");
                                    sb2.append(file4.length());
                                    Log.i(sb2.toString());
                                }
                            }
                            zipOutputStream.close();
                            fileOutputStream2.close();
                            if (file3.length() < 5242880) {
                                r8.A03(file3, wamCall.callReplayerId);
                            } else {
                                file3.delete();
                                r8.A00.AaV("voip-time-series-upload-aborted", String.valueOf(file3.length()), false);
                                StringBuilder sb3 = new StringBuilder("app/VoipTimeSeriesLogger: aborting upload because file ");
                                sb3.append(file3.getName());
                                sb3.append(" has size ");
                                sb3.append(file3.length());
                                sb3.append(" which exceeds the threshold ");
                                sb3.append(5242880);
                                Log.w(sb3.toString());
                            }
                        } catch (Throwable th) {
                            try {
                                zipOutputStream.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } catch (IOException e2) {
                        Log.w("app/VoipTimeSeriesLogger: could not create compressed time series file", e2);
                        file3.delete();
                    }
                    if (!C14350lI.A0N(file)) {
                        StringBuilder sb4 = new StringBuilder("app/VoipTimeSeriesLogger: time series data directory ");
                        sb4.append(file.getAbsolutePath());
                        sb4.append(" could not be deleted");
                        str2 = sb4.toString();
                    } else {
                        return;
                    }
                }
                Log.w(str2);
                return;
            case 6:
                VoipPhysicalCamera.$r8$lambda$_CnR6mdFp5FMdyFQSTj2kjOqrv8((VoipPhysicalCamera) this.A00, (Exchanger) this.A01, (Callable) this.A02);
                return;
            case 7:
                C14860mA r4 = (C14860mA) this.A02;
                Geocoder geocoder = new Geocoder(r4.A0I.A00, AnonymousClass018.A00(r4.A0K.A00));
                try {
                    Location location = (Location) this.A00;
                    List<Address> fromLocation = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    if (fromLocation != null && !fromLocation.isEmpty()) {
                        ((C14880mC) this.A01).A09 = fromLocation.get(0).getLocality();
                    }
                } catch (Exception unused2) {
                }
                C245816b r92 = r4.A0P;
                C14880mC r7 = (C14880mC) this.A01;
                String str3 = r7.A0F;
                double d = r7.A01;
                double d2 = r7.A02;
                double d3 = r7.A00;
                String str4 = r7.A09;
                ContentValues contentValues = new ContentValues();
                contentValues.put("lat", Double.valueOf(d));
                contentValues.put("lon", Double.valueOf(d2));
                contentValues.put("accuracy", Double.valueOf(d3));
                contentValues.put("place_name", str4);
                C16310on A02 = r92.A01.A02();
                try {
                    A02.A03.A00("sessions", contentValues, "browser_id = ?", new String[]{str3});
                    A02.close();
                    r4.A0B.post(new RunnableBRunnable0Shape16S0100000_I1_2(r4, 19));
                    return;
                } catch (Throwable th2) {
                    try {
                        A02.close();
                    } catch (Throwable unused3) {
                    }
                    throw th2;
                }
            default:
                return;
        }
    }
}
