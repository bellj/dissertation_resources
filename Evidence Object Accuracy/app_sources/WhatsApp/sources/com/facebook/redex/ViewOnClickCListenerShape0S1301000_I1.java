package com.facebook.redex;

import X.AnonymousClass017;
import X.AnonymousClass1M9;
import X.AnonymousClass310;
import X.AnonymousClass3GM;
import X.C14960mK;
import X.C16120oU;
import X.C28021Kd;
import X.C55052hi;
import X.C75633kA;
import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S1301000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public String A04;
    public final int A05;

    public ViewOnClickCListenerShape0S1301000_I1(Object obj, Object obj2, Object obj3, String str, int i, int i2) {
        this.A05 = i2;
        this.A01 = obj;
        this.A04 = str;
        this.A00 = i;
        this.A02 = obj2;
        this.A03 = obj3;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A05) {
            case 0:
                C55052hi r5 = (C55052hi) this.A01;
                String str = this.A04;
                int i = this.A00;
                C28021Kd r4 = (C28021Kd) this.A02;
                C16120oU r2 = r5.A07;
                AnonymousClass310 r1 = new AnonymousClass310();
                AnonymousClass3GM.A01(r1, str, 3, i);
                r2.A07(r1);
                ((AnonymousClass017) this.A03).A0B(C14960mK.A0G(r5.A0H.getContext(), r4.A01(), str, 0, i, r4.A00.A0G));
                return;
            case 1:
                C75633kA r52 = (C75633kA) this.A01;
                String str2 = this.A04;
                int i2 = this.A00;
                C16120oU r22 = r52.A06;
                AnonymousClass310 r12 = new AnonymousClass310();
                AnonymousClass3GM.A01(r12, str2, 5, i2);
                r22.A07(r12);
                ((AnonymousClass017) this.A03).A0B(C14960mK.A0G(r52.A0H.getContext(), null, str2, 1, i2, ((AnonymousClass1M9) this.A02).A01));
                return;
            case 2:
                C75633kA r53 = (C75633kA) this.A01;
                String str3 = this.A04;
                int i3 = this.A00;
                C16120oU r23 = r53.A06;
                AnonymousClass310 r13 = new AnonymousClass310();
                AnonymousClass3GM.A01(r13, str3, 4, i3);
                r23.A07(r13);
                ((AnonymousClass017) this.A03).A0B(C14960mK.A0G(r53.A0H.getContext(), null, str3, 2, i3, ((AnonymousClass1M9) this.A02).A01));
                return;
            default:
                return;
        }
    }
}
