package com.facebook.redex;

import X.C238713i;
import X.C238813j;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0300000_I0_1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape1S0300000_I0_1(C238813j r2, C238713i r3) {
        this.A03 = 44;
        this.A00 = new AtomicBoolean();
        this.A01 = r3;
        this.A02 = r2;
    }

    public RunnableBRunnable0Shape1S0300000_I0_1(Object obj, Object obj2, Object obj3, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // java.lang.Runnable
    public final void run() {
        /*
        // Method dump skipped, instructions count: 6336
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1.run():void");
    }
}
