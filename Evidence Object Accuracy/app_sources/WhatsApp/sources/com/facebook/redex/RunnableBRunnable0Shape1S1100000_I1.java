package com.facebook.redex;

import X.ActivityC13790kL;
import X.AnonymousClass009;
import X.AnonymousClass1MK;
import X.AnonymousClass1US;
import X.AnonymousClass2B8;
import X.AnonymousClass3FK;
import X.AnonymousClass3V0;
import X.AnonymousClass4ME;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C25771At;
import X.C38241nl;
import X.C92474Wb;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.biz.product.viewmodel.ComplianceInfoViewModel;
import com.whatsapp.deeplink.DeepLinkActivity;
import com.whatsapp.inappsupport.ui.SupportTopicsActivity;
import com.whatsapp.support.faq.FaqItemActivity;
import com.whatsapp.videoplayback.ExoPlaybackControlView;
import com.whatsapp.videoplayback.ExoPlayerErrorFrame;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S1100000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public String A01;
    public final int A02;

    public RunnableBRunnable0Shape1S1100000_I1(int i, String str, Object obj) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Uri parse;
        ArrayList parcelableArrayListExtra;
        switch (this.A02) {
            case 0:
                ((C92474Wb) this.A00).A01.AMS(this.A01);
                return;
            case 1:
                ((AnonymousClass4ME) this.A00).A01.AYF(this.A01);
                return;
            case 2:
                ComplianceInfoViewModel complianceInfoViewModel = (ComplianceInfoViewModel) this.A00;
                String str = this.A01;
                complianceInfoViewModel.A04.A0M.add(new AnonymousClass3V0(complianceInfoViewModel, str));
                return;
            case 3:
                AnonymousClass2B8 r4 = (AnonymousClass2B8) this.A00;
                String str2 = this.A01;
                if (AnonymousClass1US.A0C(str2)) {
                    parse = null;
                } else {
                    parse = Uri.parse(str2);
                }
                DeepLinkActivity deepLinkActivity = (DeepLinkActivity) r4;
                deepLinkActivity.A00.removeMessages(1);
                deepLinkActivity.AaN();
                if (parse == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(deepLinkActivity);
                    boolean A03 = C38241nl.A03();
                    int i = R.string.futureproof_deep_link;
                    if (A03) {
                        i = R.string.futureproof_deep_link_no_update;
                    }
                    AlertDialog.Builder negativeButton = builder.setMessage(i).setNegativeButton(R.string.cancel, new IDxCListenerShape9S0100000_2_I1(deepLinkActivity, 35));
                    boolean A032 = C38241nl.A03();
                    int i2 = R.string.update_whatsapp;
                    if (A032) {
                        i2 = R.string.learn_more;
                    }
                    AlertDialog create = negativeButton.setPositiveButton(i2, new IDxCListenerShape8S0100000_1_I1(deepLinkActivity, 22)).create();
                    create.setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: X.4hE
                        @Override // android.content.DialogInterface.OnDismissListener
                        public final void onDismiss(DialogInterface dialogInterface) {
                            DeepLinkActivity deepLinkActivity2 = DeepLinkActivity.this;
                            deepLinkActivity2.finish();
                            deepLinkActivity2.overridePendingTransition(0, 0);
                        }
                    });
                    create.show();
                    return;
                }
                Intent A0B = C12970iu.A0B(parse);
                A0B.putExtra("com.android.browser.application_id", deepLinkActivity.getPackageName());
                A0B.putExtra("create_new_tab", true);
                ((ActivityC13790kL) deepLinkActivity).A00.A06(deepLinkActivity, A0B);
                deepLinkActivity.finish();
                deepLinkActivity.overridePendingTransition(0, 0);
                return;
            case 4:
                FaqItemActivity faqItemActivity = (FaqItemActivity) this.A00;
                String str3 = this.A01;
                if (!AnonymousClass1MK.A00(str3) || (parcelableArrayListExtra = faqItemActivity.getIntent().getParcelableArrayListExtra("payments_support_topics")) == null) {
                    C25771At r3 = faqItemActivity.A03;
                    if (str3 == null) {
                        str3 = "FaqItemActivity";
                    }
                    r3.A00(faqItemActivity.getIntent().getBundleExtra("describe_problem_fields"), faqItemActivity, str3, true);
                    return;
                }
                faqItemActivity.startActivity(SupportTopicsActivity.A02(faqItemActivity, faqItemActivity.getIntent().getBundleExtra("describe_problem_fields"), parcelableArrayListExtra));
                return;
            case 5:
                AnonymousClass3FK r0 = (AnonymousClass3FK) this.A00;
                ExoPlayerErrorFrame exoPlayerErrorFrame = r0.A03;
                exoPlayerErrorFrame.setLoadingViewVisibility(8);
                ExoPlaybackControlView exoPlaybackControlView = r0.A02;
                if (exoPlaybackControlView != null) {
                    exoPlaybackControlView.setPlayControlVisibility(8);
                }
                String str4 = this.A01;
                if (str4 == null) {
                    str4 = exoPlayerErrorFrame.getContext().getString(R.string.unable_to_finish_download);
                }
                if (exoPlayerErrorFrame.A02 == null) {
                    FrameLayout frameLayout = (FrameLayout) C12980iv.A0O(C12960it.A0E(exoPlayerErrorFrame), R.layout.wa_exoplayer_error_screen);
                    exoPlayerErrorFrame.A02 = frameLayout;
                    exoPlayerErrorFrame.A06.addView(frameLayout);
                    exoPlayerErrorFrame.A03 = C12960it.A0J(exoPlayerErrorFrame, R.id.error_text);
                    View findViewById = exoPlayerErrorFrame.findViewById(R.id.retry_button);
                    exoPlayerErrorFrame.A01 = findViewById;
                    findViewById.setOnClickListener(exoPlayerErrorFrame.A00);
                }
                TextView textView = exoPlayerErrorFrame.A03;
                AnonymousClass009.A03(textView);
                textView.setText(str4);
                FrameLayout frameLayout2 = exoPlayerErrorFrame.A02;
                AnonymousClass009.A03(frameLayout2);
                frameLayout2.setVisibility(0);
                return;
            default:
                return;
        }
    }
}
