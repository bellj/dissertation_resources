package com.facebook.redex;

import X.AnonymousClass12P;
import X.AnonymousClass2VB;
import X.C12970iu;
import X.C25781Au;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S1630000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public final int A0A;

    public ViewOnClickCListenerShape0S1630000_I1(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, String str, int i, boolean z, boolean z2, boolean z3) {
        this.A0A = i;
        this.A00 = obj;
        this.A06 = str;
        this.A01 = obj2;
        this.A07 = z;
        this.A08 = z2;
        this.A09 = z3;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A04 = obj5;
        this.A05 = obj6;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A0A) {
            case 0:
                C25781Au r5 = (C25781Au) this.A00;
                String str = this.A06;
                boolean z = this.A07;
                boolean z2 = this.A08;
                boolean z3 = this.A09;
                AnonymousClass2VB r4 = (AnonymousClass2VB) this.A02;
                AnonymousClass12P r3 = (AnonymousClass12P) this.A03;
                View view2 = (View) this.A04;
                Intent intent = (Intent) this.A05;
                r5.A05(null, (Integer) this.A01, str, 3, z, z2);
                if (z3) {
                    r5.A01(r4, 8);
                }
                r3.A06(view2.getContext(), intent);
                return;
            case 1:
                C25781Au r52 = (C25781Au) this.A00;
                String str2 = this.A06;
                boolean z4 = this.A07;
                boolean z5 = this.A08;
                boolean z6 = this.A09;
                AnonymousClass2VB r42 = (AnonymousClass2VB) this.A02;
                AnonymousClass12P r32 = (AnonymousClass12P) this.A03;
                View view3 = (View) this.A04;
                Uri uri = (Uri) this.A05;
                r52.A05(null, (Integer) this.A01, str2, 6, z4, z5);
                if (z6) {
                    r52.A01(r42, 9);
                }
                r32.A06(view3.getContext(), C12970iu.A0B(uri));
                return;
            default:
                return;
        }
    }
}
