package com.facebook.redex;

import X.ActivityC000900k;
import X.AnonymousClass018;
import X.AnonymousClass01T;
import X.AnonymousClass141;
import X.AnonymousClass144;
import X.AnonymousClass147;
import X.AnonymousClass180;
import X.AnonymousClass1KB;
import X.AnonymousClass1KM;
import X.AnonymousClass1KN;
import X.AnonymousClass1KS;
import X.AnonymousClass1KZ;
import X.AnonymousClass1SP;
import X.AnonymousClass1SQ;
import X.AnonymousClass1SR;
import X.AnonymousClass29U;
import X.AnonymousClass35f;
import X.C16310on;
import X.C20390vg;
import X.C21390xL;
import X.C235512c;
import X.C26951Fl;
import X.C30931Zj;
import X.C33501eB;
import X.C37471mS;
import X.C40971sf;
import X.C42591vT;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.anr.SigquitBasedANRDetector;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.companiondevice.LinkedDevicesSharedViewModel;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.stickers.AddThirdPartyStickerPackActivity;
import com.whatsapp.stickers.StickerStoreMyTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S2100000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public String A01;
    public String A02;
    public final int A03;

    public RunnableBRunnable0Shape0S2100000_I0(Object obj, String str, String str2, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = str;
        this.A02 = str2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass1KB r0;
        C37471mS[] r02;
        String str;
        String str2;
        C42591vT r2;
        int i;
        switch (this.A03) {
            case 0:
                SigquitBasedANRDetector sigquitBasedANRDetector = (SigquitBasedANRDetector) this.A00;
                String str3 = this.A01;
                String str4 = this.A02;
                Log.i("SigquitBasedANRDetector/processing ANR start");
                Log.i("SigquitBasedANRDetector/persisting ANR report start");
                if (str4 != null) {
                    StringBuilder sb = new StringBuilder("  | detected using Sigquit based detector\n");
                    sb.append(str4);
                    str3 = str3.replace(str4, sb.toString());
                }
                StackTraceElement[] stackTrace = Looper.getMainLooper().getThread().getStackTrace();
                Exception exc = new Exception("ANR detected");
                exc.setStackTrace(stackTrace);
                Log.e("SigquitBasedANRDetector/Generating ANR Report", exc);
                try {
                    File file = new File(sigquitBasedANRDetector.A05.A00.A00.getCacheDir(), "traces");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("2.22.17.70");
                    sb2.append("_");
                    sb2.append(AnonymousClass180.A08);
                    String obj = sb2.toString();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(obj);
                    sb3.append(".stacktrace");
                    File file2 = new File(file, sb3.toString());
                    FileOutputStream fileOutputStream = new FileOutputStream(file2);
                    PrintWriter printWriter = new PrintWriter(fileOutputStream);
                    printWriter.write(str3);
                    printWriter.flush();
                    fileOutputStream.close();
                    StringBuilder sb4 = new StringBuilder("anr-helper/stored anr report: ");
                    sb4.append(file2.getName());
                    Log.i(sb4.toString());
                    synchronized (sigquitBasedANRDetector.A08) {
                        AnonymousClass1SP r6 = sigquitBasedANRDetector.A06;
                        synchronized (r6) {
                            i = r6.A00;
                        }
                        if (i != 0) {
                            synchronized (r6) {
                                if (r6.A00 != 0) {
                                    AnonymousClass1SR r3 = r6.A02;
                                    Object obj2 = r3.A04;
                                    synchronized (obj2) {
                                        r3.A01 = true;
                                        obj2.notifyAll();
                                    }
                                }
                            }
                        }
                        Log.i("SigquitBasedANRDetector/About to start process anr error monitor");
                        AnonymousClass1SQ r7 = new AnonymousClass1SQ(sigquitBasedANRDetector, file2);
                        synchronized (r6) {
                            AnonymousClass1SR r32 = r6.A02;
                            if (!(r32 == null || r6.A00 == 0)) {
                                Object obj3 = r32.A04;
                                synchronized (obj3) {
                                    r32.A01 = true;
                                    obj3.notifyAll();
                                }
                            }
                            long j = r6.A01 + 1;
                            r6.A01 = j;
                            AnonymousClass1SR r5 = new AnonymousClass1SR(r6, r7, r6.A04, j);
                            r6.A02 = r5;
                            r6.A00 = 1;
                            r5.start();
                        }
                    }
                } catch (IOException e) {
                    Log.e("SigquitBasedANRDetector/Error saving ANR report", e);
                    Log.e("SigquitBasedANRDetector/couldn't write ANR to file, aborting");
                    Log.i("SigquitBasedANRDetector/abortANR");
                    sigquitBasedANRDetector.A0A = false;
                }
                Log.i("SigquitBasedANRDetector/processing ANR finish");
                return;
            case 1:
                ((AnonymousClass29U) this.A00).A2l(this.A01, this.A02);
                return;
            case 2:
                ((SettingsGoogleDrive) this.A00).A2o(this.A01, this.A02);
                return;
            case 3:
                String str5 = this.A01;
                String str6 = this.A02;
                LinkedDevicesSharedViewModel linkedDevicesSharedViewModel = ((C40971sf) this.A00).A00;
                if (linkedDevicesSharedViewModel.A02) {
                    linkedDevicesSharedViewModel.A02 = false;
                    linkedDevicesSharedViewModel.A04.A0B(Boolean.FALSE);
                    linkedDevicesSharedViewModel.A0L.A0B(new AnonymousClass01T(str5, str6));
                    return;
                }
                return;
            case 4:
                r2 = (C42591vT) this.A00;
                str = this.A01;
                str2 = this.A02;
                break;
            case 5:
                r2 = (C42591vT) this.A00;
                str = this.A01;
                str2 = this.A02;
                StringBuilder sb5 = new StringBuilder("conversations-gdrive-observer/set-message ");
                sb5.append(str);
                Log.i(sb5.toString());
                break;
            case 6:
                C20390vg r52 = (C20390vg) this.A00;
                AnonymousClass01T r9 = new AnonymousClass01T(this.A01, this.A02);
                synchronized (r52) {
                    Object obj4 = r9.A00;
                    if (TextUtils.isEmpty((CharSequence) obj4)) {
                        r52.A08.A05("addUnreadPaymentMethodUpdate empty credentialId");
                    } else {
                        C21390xL r62 = r52.A04;
                        String A02 = r62.A02("unread_payment_method_credential_ids");
                        if (A02 == null) {
                            A02 = "";
                        }
                        HashSet hashSet = new HashSet(Arrays.asList(TextUtils.split(A02, ";")));
                        hashSet.add(TextUtils.join(":", new String[]{(String) obj4, (String) r9.A01}));
                        String join = TextUtils.join(";", hashSet);
                        C30931Zj r22 = r52.A08;
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("addUnreadPaymentMethodUpdate/unreadCredential:");
                        sb6.append(join);
                        r22.A06(sb6.toString());
                        r62.A06("unread_payment_method_credential_ids", join);
                    }
                }
                r52.A02();
                return;
            case 7:
                ((C26951Fl) this.A00).A03.A02(this.A01, this.A02);
                return;
            case 8:
                RegisterName registerName = (RegisterName) this.A00;
                ((TextView) registerName.A0q.findViewById(R.id.restore_info)).setText(registerName.getString(R.string.local_restore_info, null, this.A01, this.A02));
                return;
            case 9:
                C235512c r53 = (C235512c) this.A00;
                String str7 = this.A01;
                String str8 = this.A02;
                try {
                    AnonymousClass147 r92 = r53.A0T;
                    AnonymousClass1KZ A03 = r92.A04.A03(str7, str8);
                    r92.A07.A00(A03, AnonymousClass144.A02(r92.A01.A00, A03));
                    if (!A03.A0L) {
                        r92.A06.A02(str7, str8, A03.A04);
                        for (AnonymousClass1KS r1 : A03.A04) {
                            String str9 = r1.A0C;
                            if (!(str9 == null || (r0 = r1.A04) == null || (r02 = r0.A08) == null)) {
                                AnonymousClass141 r63 = r92.A02;
                                String A00 = AnonymousClass1KS.A00(r02);
                                String str10 = r1.A09;
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("plaintext_hash", str9);
                                contentValues.put("authority", str7);
                                contentValues.put("sticker_pack_id", str8);
                                contentValues.put("emojis", A00);
                                contentValues.put("hash_of_image_part", str10);
                                C16310on A022 = r63.A00.A02();
                                A022.A03.A06(contentValues, "third_party_sticker_emoji_mapping", 5);
                                A022.close();
                            }
                        }
                    }
                    r92.A03.A01(A03, str7, str8);
                    r53.A0D.A01(A03.A0D);
                    r53.A0K.A0H(r53.A09(r53.A0C()), "add");
                } catch (Exception unused) {
                    Log.e("StickerRepository/InstallThirdPartyStickerPackAsyncTask failed to install third party pack");
                }
                r53.A04.A0H(new RunnableBRunnable0Shape0S2100000_I0(r53, str7, str8, 10));
                return;
            case 10:
                String str11 = this.A01;
                String str12 = this.A02;
                for (AnonymousClass1KM r12 : ((C235512c) this.A00).A0P.A01()) {
                    if (r12 instanceof AnonymousClass1KN) {
                        ((AnonymousClass1KN) r12).A00.A02();
                    } else if (r12 instanceof C33501eB) {
                        StickerStoreTabFragment stickerStoreTabFragment = ((C33501eB) r12).A00;
                        if (stickerStoreTabFragment instanceof StickerStoreMyTabFragment) {
                            ((StickerStoreMyTabFragment) stickerStoreTabFragment).A1D();
                        }
                    } else if (r12 instanceof AnonymousClass35f) {
                        AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment addStickerPackDialogFragment = ((AnonymousClass35f) r12).A00;
                        if (str11.equals(addStickerPackDialogFragment.A03) && str12.equals(addStickerPackDialogFragment.A04)) {
                            AnonymousClass018 r8 = addStickerPackDialogFragment.A01;
                            addStickerPackDialogFragment.A00.A0L(r8.A0B(R.string.sticker_third_party_pack_add_success_message_with_app, addStickerPackDialogFragment.A05, r8.A09(R.string.localized_app_name)), 1);
                            ActivityC000900k A0B = addStickerPackDialogFragment.A0B();
                            if (A0B != null) {
                                Intent intent = new Intent();
                                intent.putExtra("add_successful", true);
                                A0B.setResult(-1, intent);
                                addStickerPackDialogFragment.A1C();
                            }
                        }
                    }
                }
                return;
            default:
                return;
        }
        ConversationsFragment conversationsFragment = r2.A04;
        conversationsFragment.A0B.setText(str2);
        conversationsFragment.A0A.setText(str);
    }
}
