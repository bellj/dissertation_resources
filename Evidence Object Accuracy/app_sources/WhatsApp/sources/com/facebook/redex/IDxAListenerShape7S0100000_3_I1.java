package com.facebook.redex;

import X.AbstractActivityC117815ak;
import X.AbstractC009204q;
import X.AnonymousClass01J;
import X.AnonymousClass2FJ;
import X.AnonymousClass2FL;
import X.C117295Zj;
import X.C12960it;
import X.C130165yu;
import X.C25991Bp;
import X.C43951xu;
import android.content.Context;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;
import org.npci.commonlibrary.GetCredential;

/* loaded from: classes4.dex */
public class IDxAListenerShape7S0100000_3_I1 implements AbstractC009204q {
    public Object A00;
    public final int A01;

    public IDxAListenerShape7S0100000_3_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        switch (this.A01) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
            case 18:
            case 19:
            case C43951xu.A01:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case C25991Bp.A0S:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
            case 47:
            case 48:
            case 49:
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case C43951xu.A02:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case C43951xu.A03:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
                C117295Zj.A0t(this);
                return;
            case 122:
                AbstractActivityC117815ak r2 = (AbstractActivityC117815ak) this.A00;
                if (!r2.A00) {
                    r2.A00 = true;
                    GetCredential getCredential = (GetCredential) r2;
                    AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) r2.generatedComponent())).A1E;
                    getCredential.A05 = C12960it.A0R(r1);
                    getCredential.A0B = (C130165yu) r1.A2Z.get();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
