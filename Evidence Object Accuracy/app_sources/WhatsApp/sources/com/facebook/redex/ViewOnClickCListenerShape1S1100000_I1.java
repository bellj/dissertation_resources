package com.facebook.redex;

import X.AbstractC47392An;
import X.ActivityC13790kL;
import X.AnonymousClass009;
import X.AnonymousClass2xV;
import X.C12970iu;
import X.C14960mK;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListFragment;
import com.whatsapp.conversation.ChangeNumberNotificationDialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.spamwarning.SpamWarningActivity;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape1S1100000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public String A01;
    public final int A02;

    public ViewOnClickCListenerShape1S1100000_I1(int i, String str, Object obj) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = str;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        SpamWarningActivity spamWarningActivity;
        Intent intent;
        Uri A03;
        switch (this.A02) {
            case 0:
                BlockReasonListFragment.A01((BlockReasonListFragment) this.A00, this.A01);
                return;
            case 1:
                Activity activity = (Activity) this.A00;
                String str = this.A01;
                activity.finish();
                intent = C12970iu.A0B(Uri.parse(str));
                spamWarningActivity = activity;
                break;
            case 2:
                AnonymousClass2xV r3 = (AnonymousClass2xV) this.A00;
                String str2 = this.A01;
                r3.A05.A08();
                UserJid userJid = r3.A0A;
                AnonymousClass009.A05(userJid);
                UserJid userJid2 = r3.A02;
                AnonymousClass009.A05(userJid2);
                ((AbstractC47392An) r3).A01.Adm(ChangeNumberNotificationDialogFragment.A00(userJid, userJid2, str2));
                return;
            case 3:
                ActivityC13790kL r32 = (ActivityC13790kL) this.A00;
                r32.A00.A06(r32, C14960mK.A0W(r32, this.A01, 6));
                return;
            case 4:
                SpamWarningActivity spamWarningActivity2 = (SpamWarningActivity) this.A00;
                String str3 = this.A01;
                if (str3 == null || str3.isEmpty()) {
                    A03 = spamWarningActivity2.A03.A03(null);
                } else {
                    A03 = Uri.parse(str3);
                }
                intent = new Intent("android.intent.action.VIEW", A03);
                spamWarningActivity = spamWarningActivity2;
                break;
            default:
                return;
        }
        spamWarningActivity.startActivity(intent);
    }
}
