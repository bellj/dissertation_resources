package com.facebook.redex;

import X.AnonymousClass51A;
import X.AnonymousClass5R4;
import X.AnonymousClass5X1;
import X.C06260Su;
import android.view.View;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.whatsapp.gesture.VerticalSwipeDismissBehavior;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0210000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public boolean A02;
    public final int A03;

    public RunnableBRunnable0Shape1S0210000_I1(Object obj, Object obj2, int i, boolean z) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = z;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass5R4 r2;
        switch (this.A03) {
            case 0:
                SwipeDismissBehavior swipeDismissBehavior = (SwipeDismissBehavior) this.A00;
                C06260Su r0 = swipeDismissBehavior.A03;
                if (r0 == null || !r0.A0A()) {
                    if (this.A02 && (r2 = swipeDismissBehavior.A04) != null) {
                        ((View) this.A01).setVisibility(8);
                        ((AnonymousClass51A) r2).A00.A04(0);
                        return;
                    }
                    return;
                }
                break;
            case 1:
                VerticalSwipeDismissBehavior verticalSwipeDismissBehavior = (VerticalSwipeDismissBehavior) this.A00;
                C06260Su r02 = verticalSwipeDismissBehavior.A04;
                if (r02 == null || !r02.A0A()) {
                    AnonymousClass5X1 r22 = verticalSwipeDismissBehavior.A05;
                    if (r22 != null) {
                        boolean z = this.A02;
                        View view = (View) this.A01;
                        if (z) {
                            r22.APG(view);
                            return;
                        } else {
                            r22.AVv(view);
                            return;
                        }
                    } else {
                        return;
                    }
                }
                break;
            default:
                return;
        }
        ((View) this.A01).postOnAnimation(this);
    }
}
