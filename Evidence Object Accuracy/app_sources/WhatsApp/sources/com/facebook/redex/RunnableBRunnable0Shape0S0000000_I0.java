package com.facebook.redex;

import android.os.Process;
import com.whatsapp.aborthooks.AbortHooks;
import com.whatsapp.settings.SettingsPrivacy;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0000000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public final int A00;

    public RunnableBRunnable0Shape0S0000000_I0(int i) {
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A00) {
            case 0:
                AbortHooks.init();
                return;
            case 1:
                Log.rotate();
                return;
            case 2:
            case 8:
            default:
                return;
            case 3:
                Voip.acceptVideoUpgrade();
                return;
            case 4:
                Voip.joinCallLink();
                return;
            case 5:
                Voip.requestVideoUpgrade();
                return;
            case 6:
                Voip.turnCameraOff();
                return;
            case 7:
                Voip.turnCameraOn();
                return;
            case 9:
                SettingsPrivacy settingsPrivacy = SettingsPrivacy.A0p;
                if (settingsPrivacy != null) {
                    settingsPrivacy.A2j();
                    return;
                }
                return;
            case 10:
                Process.killProcess(Process.myPid());
                return;
        }
    }
}
