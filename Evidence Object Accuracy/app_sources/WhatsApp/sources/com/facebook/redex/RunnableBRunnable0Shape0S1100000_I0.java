package com.facebook.redex;

import X.C22940zt;
import X.C25841Ba;
import com.whatsapp.companiondevice.LinkedDevicesSharedViewModel;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1100000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public String A01;
    public final int A02;

    public RunnableBRunnable0Shape0S1100000_I0(int i, String str, Object obj) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = str;
    }

    public RunnableBRunnable0Shape0S1100000_I0(C22940zt r2) {
        this.A02 = 32;
        this.A00 = r2;
        this.A01 = "message-utils-build-fmessage";
    }

    public RunnableBRunnable0Shape0S1100000_I0(C25841Ba r2) {
        this.A02 = 14;
        this.A00 = r2;
        this.A01 = "daily_cron_job";
    }

    public RunnableBRunnable0Shape0S1100000_I0(LinkedDevicesSharedViewModel linkedDevicesSharedViewModel, String str) {
        this.A02 = 20;
        this.A00 = linkedDevicesSharedViewModel;
        this.A01 = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:116:0x0344  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0366  */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 2130
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0.run():void");
    }
}
