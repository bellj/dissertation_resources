package com.facebook.redex;

import X.AbstractC16230of;
import X.AbstractC35731ia;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass12O;
import X.AnonymousClass12S;
import X.C14830m7;
import X.C14960mK;
import X.C23000zz;
import X.C33431e2;
import X.C43831xf;
import X.C43901xo;
import X.C60482xw;
import android.app.Activity;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ChatWithBusinessInDirectoryDialogFragment;
import com.whatsapp.greenalert.GreenAlertActivity;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape1S0110000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public boolean A01;
    public final int A02;

    public ViewOnClickCListenerShape1S0110000_I1(C60482xw r2) {
        this.A02 = 1;
        this.A00 = r2;
        this.A01 = true;
    }

    public ViewOnClickCListenerShape1S0110000_I1(Object obj, int i, boolean z) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = z;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GreenAlertActivity greenAlertActivity;
        View findViewById;
        switch (this.A02) {
            case 0:
                boolean z = this.A01;
                ActivityC13810kN r2 = (ActivityC13810kN) AbstractC35731ia.A01(((View) this.A00).getContext(), ActivityC13810kN.class);
                if (r2 != null) {
                    r2.Adl(ChatWithBusinessInDirectoryDialogFragment.A00(z), null);
                    return;
                }
                return;
            case 1:
                boolean z2 = this.A01;
                Activity A01 = AbstractC35731ia.A01(((View) this.A00).getContext(), ActivityC13810kN.class);
                if (A01 != null && z2) {
                    A01.startActivity(C14960mK.A06(A01, 2));
                    return;
                }
                return;
            case 2:
                greenAlertActivity = (GreenAlertActivity) this.A00;
                boolean z3 = this.A01;
                AnonymousClass12S r1 = greenAlertActivity.A0D;
                int i = 15;
                if (z3) {
                    i = 14;
                }
                r1.A01(Integer.valueOf(i));
                int max = Math.max(-1, greenAlertActivity.A06.getCurrentLogicalItem() - 1);
                if (max >= 0) {
                    greenAlertActivity.A06.setCurrentLogicalItem(max);
                    greenAlertActivity.A2g(max);
                    greenAlertActivity.A2h(max);
                    break;
                } else {
                    greenAlertActivity.A2e();
                    break;
                }
            case 3:
                greenAlertActivity = (GreenAlertActivity) this.A00;
                boolean z4 = this.A01;
                int min = Math.min(greenAlertActivity.A06.getCurrentLogicalItem() + 1, 2);
                if (min != 2) {
                    greenAlertActivity.A0D.A01(13);
                    greenAlertActivity.A06.setCurrentLogicalItem(min);
                    greenAlertActivity.A2g(min);
                    greenAlertActivity.A2h(min);
                    break;
                } else {
                    C14830m7 r7 = ((ActivityC13790kL) greenAlertActivity).A05;
                    AnonymousClass12S r22 = greenAlertActivity.A0D;
                    AnonymousClass12O r12 = greenAlertActivity.A0E;
                    C23000zz r5 = greenAlertActivity.A0C;
                    C43831xf A012 = r12.A08.A01();
                    if (A012 == null || !String.valueOf(A012.A00).startsWith("202102")) {
                        A012 = new C43831xf(20210210, 1, 1, r7.A00());
                        Log.i("UserNoticeManager/agreePhonyUserNotice");
                        r12.A07(20210210, 5, 1);
                    } else {
                        r12.A02();
                    }
                    r5.A08.A01("20210210", 1);
                    Iterator A00 = AbstractC16230of.A00(r5.A07);
                    while (A00.hasNext()) {
                        C33431e2 r13 = (C33431e2) A00.next();
                        if ("20210210".equals("20210210")) {
                            r13.A00.A01.A0B(null);
                        }
                    }
                    int i2 = 9;
                    if (z4) {
                        i2 = 6;
                    }
                    r22.A00(A012, Integer.valueOf(i2));
                    greenAlertActivity.finish();
                    break;
                }
                break;
            default:
                return;
        }
        int currentLogicalItem = greenAlertActivity.A06.getCurrentLogicalItem();
        View findViewWithTag = greenAlertActivity.A06.findViewWithTag(Integer.valueOf(currentLogicalItem));
        if (currentLogicalItem != 0) {
            if (currentLogicalItem == 1) {
                greenAlertActivity.A07.performAccessibilityAction(128, null);
                findViewById = greenAlertActivity.A02;
            } else {
                return;
            }
        } else if (C43901xo.A02(greenAlertActivity.A0E)) {
            findViewById = greenAlertActivity.A03;
        } else if (findViewWithTag != null) {
            findViewById = findViewWithTag.findViewById(R.id.green_alert_education_title);
        } else {
            return;
        }
        findViewById.sendAccessibilityEvent(8);
    }
}
