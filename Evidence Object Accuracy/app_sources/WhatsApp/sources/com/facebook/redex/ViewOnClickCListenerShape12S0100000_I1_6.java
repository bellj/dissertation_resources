package com.facebook.redex;

import X.ActivityC000900k;
import X.AnonymousClass39B;
import X.AnonymousClass5VC;
import X.C16700pc;
import X.C34271fr;
import android.view.View;
import com.whatsapp.wabloks.commerce.ui.view.GalaxyBottomsheetBaseContainer;
import com.whatsapp.wabloks.commerce.ui.view.WaBkGalaxyScreenFragment;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape12S0100000_I1_6 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape12S0100000_I1_6(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A01) {
            case 0:
                AnonymousClass39B.A07((AnonymousClass39B) this.A00);
                return;
            case 1:
                AnonymousClass39B.A08((AnonymousClass39B) this.A00);
                return;
            case 2:
                GalaxyBottomsheetBaseContainer galaxyBottomsheetBaseContainer = (GalaxyBottomsheetBaseContainer) this.A00;
                C16700pc.A0E(galaxyBottomsheetBaseContainer, 0);
                WaGalaxyNavBarViewModel waGalaxyNavBarViewModel = galaxyBottomsheetBaseContainer.A03;
                if (waGalaxyNavBarViewModel != null) {
                    boolean A0O = C16700pc.A0O(waGalaxyNavBarViewModel.A02.A01(), Boolean.TRUE);
                    ActivityC000900k A0C = galaxyBottomsheetBaseContainer.A0C();
                    if (A0O) {
                        A0C.onBackPressed();
                        return;
                    } else {
                        A0C.finish();
                        return;
                    }
                } else {
                    throw C16700pc.A06("waGalaxyNavBarViewModel");
                }
            case 3:
                WaBkGalaxyScreenFragment waBkGalaxyScreenFragment = (WaBkGalaxyScreenFragment) this.A00;
                C16700pc.A0E(waBkGalaxyScreenFragment, 0);
                C34271fr r1 = waBkGalaxyScreenFragment.A03;
                if (r1 == null) {
                    throw C16700pc.A06("snackbar");
                }
                r1.A04(3);
                return;
            case 4:
                ((AnonymousClass5VC) this.A00).AMo();
                return;
            default:
                return;
        }
    }
}
