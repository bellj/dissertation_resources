package com.facebook.redex;

import X.AbstractC14710lv;
import X.AbstractC14770m1;
import X.AbstractC15340mz;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass12P;
import X.AnonymousClass1FO;
import X.AnonymousClass1s8;
import X.AnonymousClass283;
import X.AnonymousClass29K;
import X.AnonymousClass29U;
import X.AnonymousClass2GL;
import X.AnonymousClass2VA;
import X.AnonymousClass3E8;
import X.AnonymousClass3LU;
import X.AnonymousClass4NB;
import X.AnonymousClass4XG;
import X.C14960mK;
import X.C15350n0;
import X.C15360n1;
import X.C22730zY;
import X.C25991Bp;
import X.C37161lb;
import X.C43951xu;
import X.C44771zW;
import X.C48172Ep;
import X.C48232Fc;
import X.C89324Jn;
import X.C90794Pg;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.Conversation;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.WaImageView;
import com.whatsapp.account.delete.DeleteAccountFeedback;
import com.whatsapp.authentication.AppAuthenticationActivity;
import com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.businessdirectory.view.activity.DirectorySetLocationMapActivity;
import com.whatsapp.businessdirectory.viewmodel.DirectorySearchHistoryViewModel;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0100000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape0S0100000_I0(C48232Fc r1, int i) {
        this.A01 = i;
        if (12 - i != 0) {
            this.A00 = r1;
        } else {
            this.A00 = r1;
        }
    }

    public ViewOnClickCListenerShape0S0100000_I0(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        SearchView searchView;
        AnonymousClass1s8 ABD;
        switch (this.A01) {
            case 0:
                EditText editText = (EditText) this.A00;
                editText.setSelection(editText.getText().length());
                return;
            case 1:
                Conversation conversation = (Conversation) this.A00;
                conversation.A1g.onWindowFocusChanged(false);
                Stack stack = conversation.A55;
                if (stack.empty()) {
                    conversation.A1g.A04();
                    return;
                }
                C90794Pg r0 = (C90794Pg) stack.pop();
                conversation.A3I(r0.A02, r0.A00);
                return;
            case 2:
                Conversation conversation2 = (Conversation) this.A00;
                conversation2.A1g.onWindowFocusChanged(false);
                C15350n0 conversationCursorAdapter = conversation2.A1g.getConversationCursorAdapter();
                if (!conversation2.A20.A0b.isEmpty()) {
                    conversationCursorAdapter.A0V.clear();
                    int firstVisiblePosition = conversation2.A1g.getFirstVisiblePosition();
                    AbstractC15340mz A05 = conversationCursorAdapter.getItem(firstVisiblePosition);
                    if (A05 != null && A05.A0z.A00 == null) {
                        A05 = conversationCursorAdapter.getItem(firstVisiblePosition + 1);
                    }
                    C15360n1 r11 = conversation2.A20;
                    int dimensionPixelSize = conversation2.getResources().getDimensionPixelSize(R.dimen.conversation_row_min_height);
                    int A01 = conversationCursorAdapter.A01();
                    long j = conversation2.A03;
                    int firstVisiblePosition2 = conversation2.A1g.getFirstVisiblePosition();
                    int count = conversationCursorAdapter.getCount();
                    ArrayList arrayList = r11.A0b;
                    if (!arrayList.isEmpty()) {
                        ArrayList arrayList2 = new ArrayList();
                        if (A05 != null) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                AbstractC15340mz r8 = (AbstractC15340mz) it.next();
                                if (r8.A12 > A05.A12) {
                                    arrayList2.add(r8.A0z);
                                }
                            }
                        }
                        r11.A0C((AbstractC15340mz) arrayList.get(0), arrayList2, dimensionPixelSize, A01, firstVisiblePosition2, count, j, true);
                        return;
                    }
                    return;
                }
                return;
            case 3:
                Conversation conversation3 = (Conversation) this.A00;
                conversation3.A3l.A00(2);
                conversation3.A1e.A08(2);
                return;
            case 4:
            case 6:
                ((Conversation) this.A00).A3J(false);
                return;
            case 5:
                Conversation conversation4 = (Conversation) this.A00;
                conversation4.A3l.A00(8);
                if (conversation4.A1g.getLastVisiblePosition() >= (conversation4.A1g.getCount() - conversation4.A1g.getFooterViewsCount()) - 1) {
                    conversation4.A1g.setTranscriptMode(2);
                    conversation4.A1g.A04();
                }
                conversation4.A30();
                return;
            case 7:
            case 8:
            case 15:
                ((Activity) this.A00).finish();
                return;
            case 9:
                ((HomeActivity) this.A00).A2z(true);
                return;
            case 10:
                HomeActivity homeActivity = (HomeActivity) this.A00;
                AbstractC14770m1 A2i = homeActivity.A2i(HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, homeActivity.A03));
                if (A2i != null) {
                    A2i.Acn(true);
                    return;
                }
                return;
            case 11:
                ((AnonymousClass2GL) this.A00).A2e();
                return;
            case 12:
                ((C48232Fc) this.A00).A04(true);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C48232Fc r3 = (C48232Fc) this.A00;
                int inputType = r3.A02.A0k.getInputType();
                int i = 3;
                WaImageView waImageView = r3.A03;
                if (inputType == 3) {
                    waImageView.setImageResource(R.drawable.ic_action_dialpad);
                    searchView = r3.A02;
                    i = 1;
                } else {
                    waImageView.setImageResource(R.drawable.ic_action_keypad);
                    searchView = r3.A02;
                }
                searchView.setInputType(i);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                Log.i("acceptlink/confirmation/ok");
                ((Activity) this.A00).finish();
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                DeleteAccountFeedback deleteAccountFeedback = (DeleteAccountFeedback) this.A00;
                deleteAccountFeedback.A03.clearFocus();
                if (deleteAccountFeedback.getCurrentFocus() != null) {
                    ((ActivityC13790kL) deleteAccountFeedback).A0D.A01(deleteAccountFeedback.getCurrentFocus());
                }
                deleteAccountFeedback.A07 = true;
                deleteAccountFeedback.A05.A00();
                return;
            case 17:
                DeleteAccountFeedback deleteAccountFeedback2 = (DeleteAccountFeedback) this.A00;
                if (deleteAccountFeedback2.A03.getText().length() <= 0 || deleteAccountFeedback2.A03.getText().length() >= 5) {
                    int i2 = deleteAccountFeedback2.A01;
                    Editable text = deleteAccountFeedback2.A03.getText();
                    if (i2 == 1) {
                        DialogFragment A00 = DeleteAccountFeedback.ChangeNumberMessageDialogFragment.A00(text.toString());
                        deleteAccountFeedback2.A06 = A00;
                        A00.A1F(deleteAccountFeedback2.A0V(), null);
                        return;
                    }
                    String obj = text.toString();
                    Intent intent = new Intent();
                    intent.setClassName(deleteAccountFeedback2.getPackageName(), "com.whatsapp.account.delete.DeleteAccountConfirmation");
                    intent.putExtra("deleteReason", i2);
                    intent.putExtra("additionalComments", obj);
                    deleteAccountFeedback2.startActivity(intent);
                    return;
                }
                ((ActivityC13810kN) deleteAccountFeedback2).A05.A07(R.string.describe_problem_description_further, 0);
                return;
            case 18:
                AppAuthenticationActivity.A03((AppAuthenticationActivity) this.A00);
                return;
            case 19:
                AnonymousClass29U.A03(view, (AnonymousClass29U) this.A00);
                return;
            case C43951xu.A01:
                GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity = (GoogleDriveNewUserSetupActivity) this.A00;
                String A09 = ((ActivityC13810kN) googleDriveNewUserSetupActivity).A09.A09();
                StringBuilder sb = new StringBuilder("gdrive-new-user-setup/done-clicked account is ");
                sb.append(C44771zW.A0B(A09));
                sb.append(" and backup frequency is ");
                sb.append(googleDriveNewUserSetupActivity.A00);
                Log.i(sb.toString());
                int i3 = googleDriveNewUserSetupActivity.A00;
                if (i3 == -1) {
                    Log.i(String.format("gdrive-new-user-setup/done-clicked/show-toast \"%s\"", googleDriveNewUserSetupActivity.getString(R.string.gdrive_new_user_setup_button_toast_no_freq_selected)));
                    ((ActivityC13810kN) googleDriveNewUserSetupActivity).A05.A07(R.string.gdrive_new_user_setup_button_toast_no_freq_selected, 1);
                    return;
                } else if (i3 == 0 || A09 != null) {
                    Log.i("gdrive-new-user-setup/done-clicked/setup-finished");
                    ((ActivityC13810kN) googleDriveNewUserSetupActivity).A09.A1H(googleDriveNewUserSetupActivity.A00);
                    ((ActivityC13830kP) googleDriveNewUserSetupActivity).A05.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(googleDriveNewUserSetupActivity, 27));
                    googleDriveNewUserSetupActivity.setResult(-1);
                    googleDriveNewUserSetupActivity.finish();
                    return;
                } else {
                    Log.i("gdrive-new-user-setup/done-clicked/show-account-selector");
                    googleDriveNewUserSetupActivity.A2f();
                    return;
                }
            case 21:
                RestoreFromBackupActivity restoreFromBackupActivity = (RestoreFromBackupActivity) this.A00;
                restoreFromBackupActivity.startActivity(new Intent("android.intent.action.VIEW", AnonymousClass1FO.A00(restoreFromBackupActivity.A0A)));
                return;
            case 22:
            case 23:
                Log.i("gdrive-activity/show-skip-gdrive-restore-dialog");
                ((RestoreFromBackupActivity) this.A00).A2s(11);
                return;
            case 24:
                RestoreFromBackupActivity restoreFromBackupActivity2 = (RestoreFromBackupActivity) this.A00;
                restoreFromBackupActivity2.A0H.A05(10);
                restoreFromBackupActivity2.A2p();
                return;
            case 25:
                RestoreFromBackupActivity.A0L((RestoreFromBackupActivity) this.A00);
                return;
            case 26:
                Log.i("gdrive-activity/show-local-restore-skip-dialog");
                ((RestoreFromBackupActivity) this.A00).A2s(10);
                return;
            case 27:
                SettingsGoogleDrive.A0B((SettingsGoogleDrive) this.A00);
                return;
            case 28:
                SettingsGoogleDrive settingsGoogleDrive = (SettingsGoogleDrive) this.A00;
                if (settingsGoogleDrive.A0e.A0f.get()) {
                    if (settingsGoogleDrive.A0e.A0g.get()) {
                        settingsGoogleDrive.A0b.A03();
                    }
                    C22730zY r32 = settingsGoogleDrive.A0Y;
                    r32.A08 = true;
                    r32.A07(r32.A0O.A00);
                    r32.A0Y.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(r32, 21));
                    return;
                }
                settingsGoogleDrive.onNewIntent(new Intent("action_perform_media_restore_over_cellular"));
                return;
            case 29:
                SettingsGoogleDrive.A0K((SettingsGoogleDrive) this.A00);
                return;
            case C25991Bp.A0S:
                ((SettingsGoogleDrive) this.A00).A2f();
                return;
            case 31:
                ((SettingsGoogleDrive) this.A00).A2i();
                return;
            case 32:
                SettingsGoogleDrive settingsGoogleDrive2 = (SettingsGoogleDrive) this.A00;
                if (((ActivityC13810kN) settingsGoogleDrive2).A09.A01() == 0 || !AnonymousClass29K.A0A(((ActivityC13810kN) settingsGoogleDrive2).A09)) {
                    settingsGoogleDrive2.A2f();
                    return;
                } else {
                    settingsGoogleDrive2.A2g();
                    return;
                }
            case 33:
                SettingsGoogleDrive.A09(view, (SettingsGoogleDrive) this.A00);
                return;
            case 34:
                ActivityC13830kP r4 = (ActivityC13830kP) this.A00;
                r4.A05.Ab2(new RunnableBRunnable0Shape0S0200000_I0(r4, 48, new SettingsGoogleDrive.AuthRequestDialogFragment()));
                return;
            case 35:
                ((SettingsGoogleDrive) this.A00).A2g();
                return;
            case 36:
                SettingsGoogleDrive.A0D((SettingsGoogleDrive) this.A00);
                return;
            case 37:
                ActivityC13830kP r33 = (ActivityC13830kP) this.A00;
                r33.A05.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(r33, 42));
                return;
            case 38:
                ((AnonymousClass2VA) this.A00).A04(5);
                return;
            case 39:
                AnonymousClass2VA r02 = (AnonymousClass2VA) this.A00;
                AnonymousClass12P r42 = r02.A0O;
                ContactInfoActivity contactInfoActivity = r02.A0Y;
                r42.A08(contactInfoActivity, new C14960mK().A0i(contactInfoActivity, r02.A03()).putExtra("args_conversation_screen_entry_point", 1).putExtra("extra_show_search_on_create", true), "ContactInfoActivity");
                return;
            case 40:
                ((DialogFragment) this.A00).A1B();
                return;
            case 41:
                Context context = (Context) this.A00;
                Intent intent2 = new Intent();
                intent2.setClassName(context.getPackageName(), "com.whatsapp.report.ReportActivity");
                context.startActivity(intent2);
                return;
            case 42:
                AnonymousClass283 r34 = (AnonymousClass283) this.A00;
                r34.Adm(CartFragment.A00(r34.A0h, r34.A0l, 1));
                return;
            case 43:
                AnonymousClass283 r43 = (AnonymousClass283) this.A00;
                r43.A0i.A01(774774794, "cart_add_tag", "ProductBaseActivity");
                r43.A0i.A05("cart_add_tag", "IsConsumer", !((ActivityC13790kL) r43).A01.A0F(r43.A0h));
                r43.A0i.A04("cart_add_tag", "EntryPoint", "Product");
                r43.A0a.A04(r43.A0Q, r43.A0h, r43.A0k, r43.A0m, 1);
                return;
            case 44:
                CompoundButton compoundButton = (CompoundButton) this.A00;
                compoundButton.setChecked(!compoundButton.isChecked());
                return;
            case 45:
                DirectorySetLocationMapActivity directorySetLocationMapActivity = (DirectorySetLocationMapActivity) this.A00;
                if (RequestPermissionActivity.A0U(directorySetLocationMapActivity, directorySetLocationMapActivity.A09, R.string.permission_location_info_on_searching_businesses, R.string.permission_location_access_on_searching_businesses, 34)) {
                    AnonymousClass3LU r1 = directorySetLocationMapActivity.A08;
                    if (r1.A0D) {
                        r1.A00();
                        View view2 = directorySetLocationMapActivity.A08.A03;
                        if (view2 != null) {
                            view2.setVisibility(0);
                        }
                        directorySetLocationMapActivity.A0B.A09();
                        return;
                    }
                }
                if (directorySetLocationMapActivity.A09.A03()) {
                    AnonymousClass3LU r12 = directorySetLocationMapActivity.A08;
                    if (!r12.A0D) {
                        r12.A01(new C89324Jn(directorySetLocationMapActivity));
                        return;
                    }
                }
                AnonymousClass3LU r03 = directorySetLocationMapActivity.A08;
                r03.A0E = true;
                r03.A0J.A02.A00().edit().putBoolean("DIRECTORY_LOCATION_INFO_SHOWN", true).apply();
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                AnonymousClass4NB r04 = ((C37161lb) this.A00).A00;
                DirectorySearchHistoryViewModel directorySearchHistoryViewModel = r04.A01;
                C48172Ep r44 = r04.A00;
                DirectorySearchHistoryViewModel.A00(r44, directorySearchHistoryViewModel, 59);
                AnonymousClass3E8 r35 = directorySearchHistoryViewModel.A05;
                AnonymousClass4XG r2 = r35.A01;
                List A002 = r2.A00();
                A002.remove(r44);
                r2.A01.A01(A002);
                r35.A00.A0B(r2.A00());
                return;
            case 47:
                CallsHistoryFragment callsHistoryFragment = (CallsHistoryFragment) this.A00;
                callsHistoryFragment.A06.A01(callsHistoryFragment.A0B(), 1);
                return;
            case 48:
                ((MaximizedParticipantVideoDialogFragment) this.A00).A1L(true);
                return;
            case 49:
                ActivityC000900k A0B = ((AnonymousClass01E) this.A00).A0B();
                if ((A0B instanceof AbstractC14710lv) && (ABD = ((AbstractC14710lv) A0B).ABD()) != null) {
                    ABD.A0P();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
