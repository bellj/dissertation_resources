package com.facebook.redex;

import X.AnonymousClass1m0;
import X.C15370n3;
import X.C236612n;
import android.content.Intent;
import com.whatsapp.notification.DirectReplyService;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1400000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public String A04;
    public final int A05;

    public RunnableBRunnable0Shape0S1400000_I0(Object obj, Object obj2, Object obj3, Object obj4, String str, int i) {
        this.A05 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A04 = str;
        this.A03 = obj4;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A05) {
            case 0:
                ((C236612n) this.A00).A00.A00();
                return;
            case 1:
                AnonymousClass1m0 r3 = (AnonymousClass1m0) this.A01;
                String str = this.A04;
                C15370n3 r1 = (C15370n3) this.A02;
                DirectReplyService.A01((Intent) this.A03, r1, r3, (DirectReplyService) this.A00, str);
                return;
            default:
                return;
        }
    }
}
