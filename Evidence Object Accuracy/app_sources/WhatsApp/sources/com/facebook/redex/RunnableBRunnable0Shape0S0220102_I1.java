package com.facebook.redex;

import X.AnonymousClass3MP;
import android.view.View;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0220102_I1 extends EmptyBaseRunnable0 implements Runnable {
    public float A00;
    public float A01;
    public long A02 = -1;
    public Object A03;
    public Object A04;
    public boolean A05;
    public boolean A06;
    public final int A07;

    public RunnableBRunnable0Shape0S0220102_I1(View view, AnonymousClass3MP r4, int i) {
        this.A07 = i;
        this.A04 = view;
        this.A03 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00af, code lost:
        if (r2 < 0.0f) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b1, code lost:
        r10.A00 = 0.0f;
        r2 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b4, code lost:
        r1 = r10.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b8, code lost:
        if (r1 <= 0.0f) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ba, code lost:
        r1 = r1 - r6;
        r10.A01 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00bf, code lost:
        if (r1 >= 0.0f) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c1, code lost:
        r10.A01 = 0.0f;
        r1 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c6, code lost:
        if (r2 != 0.0f) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ca, code lost:
        if (r1 == 0.0f) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00cc, code lost:
        if (r3 != false) goto L_0x00d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ce, code lost:
        X.C12990iw.A1L(r10);
        r4.A02(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e1, code lost:
        r1 = r1 + r6;
        r10.A01 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e6, code lost:
        if (r1 <= 0.0f) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00ee, code lost:
        if (r2 > 0.0f) goto L_0x00b1;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 252
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0220102_I1.run():void");
    }
}
