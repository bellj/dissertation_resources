package com.facebook.redex;

import X.AbstractC15340mz;
import X.AbstractC16130oV;
import X.AnonymousClass23B;
import X.AnonymousClass32T;
import X.C14860mA;
import X.C15370n3;
import X.C15580nU;
import X.C16150oX;
import X.C16170oZ;
import X.C16440p1;
import X.C20660w7;
import X.C254119h;
import X.C38421o4;
import android.app.Activity;
import android.os.SystemClock;
import android.widget.CompoundButton;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1411000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public String A05;
    public boolean A06;
    public final int A07;

    public RunnableBRunnable0Shape0S1411000_I0(Object obj, Object obj2, Object obj3, Object obj4, String str, int i, int i2, boolean z) {
        this.A07 = i2;
        this.A01 = obj;
        this.A02 = obj2;
        this.A03 = obj3;
        this.A05 = str;
        this.A00 = i;
        this.A04 = obj4;
        this.A06 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A07) {
            case 0:
                C16170oZ r4 = (C16170oZ) this.A01;
                String str = this.A05;
                int i = this.A00;
                AbstractC15340mz r10 = (AbstractC15340mz) this.A04;
                boolean z = this.A06;
                C16150oX r8 = new C16150oX();
                r8.A0F = (File) this.A03;
                r8.A01 = i;
                C38421o4 A00 = r4.A11.A00(null, r8, null, r10, str, (List) this.A02, null, null, (byte) 9, 7, 0, z);
                for (AbstractC16130oV r1 : Collections.unmodifiableList(A00.A01)) {
                    if (r1 instanceof C16440p1) {
                        C16440p1 r12 = (C16440p1) r1;
                        r12.A00 = i;
                        ((AbstractC16130oV) r12).A06 = "text/vcard";
                        ((AbstractC16130oV) r12).A07 = str;
                    }
                }
                r4.A05(A00, null, false, false);
                return;
            case 1:
                C16170oZ r42 = (C16170oZ) this.A01;
                Object obj = this.A02;
                String str2 = this.A05;
                byte[] bArr = (byte[]) this.A03;
                int i2 = this.A00;
                Object obj2 = this.A04;
                boolean z2 = this.A06;
                File A0B = r42.A1P.A0B(str2, (long) bArr.length);
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(A0B);
                    fileOutputStream.write(bArr);
                    fileOutputStream.close();
                } catch (IOException e) {
                    Log.e("UserActions/prepareVCardDocument IO Exception when writing vcard document", e);
                }
                if (!A0B.exists()) {
                    Log.e("UserActions/prepareVCardDocument Error writing vcard document file");
                    return;
                } else {
                    r42.A05.A0I(new RunnableBRunnable0Shape0S1411000_I0(r42, obj, A0B, obj2, str2, i2, 0, z2));
                    return;
                }
            case 2:
                LeaveGroupsDialogFragment leaveGroupsDialogFragment = (LeaveGroupsDialogFragment) this.A01;
                int i3 = this.A00;
                boolean z3 = this.A06;
                CompoundButton compoundButton = (CompoundButton) this.A02;
                C15580nU r11 = (C15580nU) this.A03;
                Activity activity = (Activity) this.A04;
                String str3 = this.A05;
                if (i3 == 1) {
                    SystemClock.sleep(300);
                }
                if (!z3 || !compoundButton.isChecked()) {
                    C20660w7 r0 = leaveGroupsDialogFragment.A0E;
                    C14860mA r122 = leaveGroupsDialogFragment.A0H;
                    r0.A06(new AnonymousClass32T(leaveGroupsDialogFragment, leaveGroupsDialogFragment.A09, leaveGroupsDialogFragment.A0D, r11, r122, i3));
                    return;
                }
                C15370n3 A0B2 = leaveGroupsDialogFragment.A02.A0B(r11);
                C254119h r3 = leaveGroupsDialogFragment.A04;
                AnonymousClass23B r5 = new AnonymousClass23B(leaveGroupsDialogFragment.A00, i3);
                r3.A01(A0B2, str3, null);
                r3.A00(activity, r5, A0B2, null, null, str3, true);
                return;
            default:
                return;
        }
    }
}
