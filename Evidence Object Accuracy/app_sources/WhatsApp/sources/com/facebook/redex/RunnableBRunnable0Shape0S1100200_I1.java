package com.facebook.redex;

import X.AnonymousClass4ME;
import X.C92474Wb;

/* loaded from: classes3.dex */
public class RunnableBRunnable0Shape0S1100200_I1 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public long A01;
    public Object A02;
    public String A03;
    public final int A04;

    public RunnableBRunnable0Shape0S1100200_I1(Object obj, String str, int i, long j, long j2) {
        this.A04 = i;
        this.A02 = obj;
        this.A03 = str;
        this.A00 = j;
        this.A01 = j2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A04) {
            case 0:
                ((C92474Wb) this.A02).A01.AMR(this.A03, this.A00, this.A01);
                return;
            case 1:
                ((AnonymousClass4ME) this.A02).A01.AYE(this.A03, this.A00, this.A01);
                return;
            default:
                return;
        }
    }
}
