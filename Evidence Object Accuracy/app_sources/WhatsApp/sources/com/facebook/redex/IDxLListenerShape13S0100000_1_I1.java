package com.facebook.redex;

import X.AbstractC69213Yj;
import X.AnonymousClass009;
import X.AnonymousClass07N;
import X.AnonymousClass21U;
import X.AnonymousClass21W;
import X.AnonymousClass242;
import X.AnonymousClass2Zf;
import X.AnonymousClass2Zg;
import X.AnonymousClass2bf;
import X.AnonymousClass2xg;
import X.AnonymousClass341;
import X.AnonymousClass3IZ;
import X.C12960it;
import X.C12980iv;
import X.C12990iw;
import X.C15260mp;
import X.C28141Kv;
import X.C54392ge;
import X.C58062o5;
import X.C58092o8;
import X.C61162zV;
import X.C63443Bp;
import X.C69233Yl;
import X.View$OnClickListenerC55932ju;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.chatinfo.view.custom.CollapsingProfilePhotoView;
import com.whatsapp.group.GroupProfileEmojiEditor;
import com.whatsapp.mentions.MentionPickerView;
import com.whatsapp.migration.android.view.GoogleMigrateImporterActivity;
import com.whatsapp.registration.EULA;
import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;

/* loaded from: classes2.dex */
public class IDxLListenerShape13S0100000_1_I1 implements ViewTreeObserver.OnGlobalLayoutListener {
    public Object A00;
    public final int A01;

    public IDxLListenerShape13S0100000_1_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        View view;
        switch (this.A01) {
            case 0:
                C58092o8 r4 = (C58092o8) this.A00;
                Conversation conversation = r4.A03;
                C12980iv.A1F(conversation.A0A, this);
                Drawable background = conversation.A0A.getBackground();
                if (background instanceof AnonymousClass2Zg) {
                    AnonymousClass2Zg.A00(((AnonymousClass2Zg) background).A01, conversation.A0A);
                }
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, 0.0f);
                translateAnimation.setDuration(0);
                r4.A01.startAnimation(translateAnimation);
                if (r4.A02 == conversation.A0S && conversation.A0P.getVisibility() == 0) {
                    conversation.A0P.startAnimation(translateAnimation);
                }
                if (conversation.A2w.A0H) {
                    conversation.A0T.startAnimation(translateAnimation);
                }
                if (r4.A04) {
                    conversation.A1g.startAnimation(translateAnimation);
                }
                conversation.A02 = 0;
                return;
            case 1:
                AnonymousClass3IZ r3 = (AnonymousClass3IZ) this.A00;
                ViewPager viewPager = r3.A0L;
                int width = viewPager.getWidth() / C12960it.A09(viewPager).getDimensionPixelSize(R.dimen.emoji_picker_item);
                if (r3.A01 != width) {
                    r3.A01 = width;
                    AnonymousClass2bf[] r32 = r3.A0S;
                    for (AnonymousClass2bf r0 : r32) {
                        if (r0 != null) {
                            r0.notifyDataSetChanged();
                        }
                    }
                    return;
                }
                return;
            case 2:
                CollapsingProfilePhotoView collapsingProfilePhotoView = (CollapsingProfilePhotoView) this.A00;
                C12980iv.A1F(collapsingProfilePhotoView.A08, this);
                collapsingProfilePhotoView.A0B = false;
                collapsingProfilePhotoView.A02 = collapsingProfilePhotoView.getWidth();
                int left = collapsingProfilePhotoView.A08.getLeft();
                collapsingProfilePhotoView.A05 = left;
                collapsingProfilePhotoView.A03 = left - collapsingProfilePhotoView.A0E;
                collapsingProfilePhotoView.A01(collapsingProfilePhotoView.A06, collapsingProfilePhotoView.A01);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(collapsingProfilePhotoView.A08.getWidth(), collapsingProfilePhotoView.A08.getHeight());
                layoutParams.gravity = 17;
                collapsingProfilePhotoView.A07.setLayoutParams(layoutParams);
                collapsingProfilePhotoView.setAnimationValue(0.0f);
                return;
            case 3:
                AnonymousClass2xg r42 = (AnonymousClass2xg) this.A00;
                r42.A03();
                MentionPickerView mentionPickerView = (MentionPickerView) r42;
                mentionPickerView.A05(mentionPickerView.A0C.A07.size(), mentionPickerView.getResources().getDimensionPixelSize(R.dimen.mention_picker_row_height));
                C12980iv.A1E(r42, this);
                return;
            case 4:
                C58062o5 r1 = (C58062o5) this.A00;
                C12980iv.A1F(r1.A00, this);
                AnonymousClass2Zf r12 = r1.A01;
                r12.A00 = -1;
                r12.invalidateSelf();
                return;
            case 5:
                C61162zV r43 = (C61162zV) this.A00;
                LinearLayout linearLayout = r43.A01;
                C12980iv.A1E(linearLayout, this);
                linearLayout.setOrientation(C12990iw.A1X(r43.A03.getMeasuredWidth() + r43.A04.getMeasuredWidth(), linearLayout.getMeasuredWidth()) ? 1 : 0);
                return;
            case 6:
                GroupProfileEmojiEditor groupProfileEmojiEditor = (GroupProfileEmojiEditor) this.A00;
                C12980iv.A1E(groupProfileEmojiEditor.A03, this);
                C15260mp r02 = groupProfileEmojiEditor.A07.A02;
                AnonymousClass009.A05(r02);
                r02.A06();
                return;
            case 7:
                AnonymousClass21U r13 = (AnonymousClass21U) this.A00;
                C12980iv.A1F(r13.A0N, this);
                AnonymousClass21U.A00(r13);
                AnonymousClass21W r03 = r13.A0A;
                if (r03 != null) {
                    r03.A02();
                    return;
                }
                return;
            case 8:
                GoogleMigrateImporterActivity googleMigrateImporterActivity = (GoogleMigrateImporterActivity) this.A00;
                WaImageView waImageView = googleMigrateImporterActivity.A01;
                if (waImageView != null && googleMigrateImporterActivity.A02 != null) {
                    C12980iv.A1E(waImageView, this);
                    C12980iv.A1E(googleMigrateImporterActivity.A03, this);
                    if (googleMigrateImporterActivity.A01.getHeight() < googleMigrateImporterActivity.getResources().getDimensionPixelSize(R.dimen.xp_migrate_importer_hero_image_min_height)) {
                        view = googleMigrateImporterActivity.A01;
                        break;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
                break;
            case 9:
                C63443Bp r33 = (C63443Bp) this.A00;
                RecyclerView recyclerView = r33.A05;
                int height = recyclerView.getHeight();
                if (height != r33.A03) {
                    r33.A03 = height;
                    int i = r33.A01;
                    int i2 = (i * 3) >> 2;
                    int i3 = height % i;
                    if (i3 >= (i >> 2) && i3 <= i2) {
                        i2 = i3;
                    }
                    int max = Math.max(0, height - i2);
                    r33.A02 = (max % i) / ((max / i) + 1);
                }
                int width2 = recyclerView.getWidth();
                if (r33.A04 != width2) {
                    r33.A04 = width2;
                    int i4 = width2 / r33.A01;
                    if (r33.A00 != i4) {
                        r33.A00 = i4;
                        r33.A08.A1h(i4);
                        C54392ge r04 = r33.A06;
                        if (r04 != null) {
                            r04.A02();
                        }
                    }
                    C54392ge r05 = r33.A06;
                    if (r05 != null) {
                        r05.A02();
                        return;
                    }
                    return;
                }
                return;
            case 10:
                EULA eula = (EULA) this.A00;
                View view2 = eula.A01;
                if (view2 != null) {
                    C12980iv.A1F(view2, this);
                    if (eula.A01.getHeight() < eula.getResources().getDimensionPixelSize(R.dimen.registration_eula_logo_min_height)) {
                        view = eula.A01;
                        break;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 11:
                View$OnClickListenerC55932ju r7 = (View$OnClickListenerC55932ju) this.A00;
                View view3 = r7.A01;
                int[] iArr = r7.A05;
                view3.getLocationOnScreen(iArr);
                int[] iArr2 = r7.A06;
                int i5 = iArr2[0];
                int i6 = iArr[0];
                if (!(i5 == i6 && iArr2[1] == iArr[1])) {
                    iArr2[0] = i6;
                    iArr2[1] = iArr[1];
                    AnonymousClass07N r14 = r7.A03;
                    r14.A05.A01();
                    r14.A00();
                    if (r7.A00 == null) {
                        ViewTreeObserver viewTreeObserver = view3.getViewTreeObserver();
                        AnonymousClass009.A05(viewTreeObserver);
                        r7.A00 = viewTreeObserver;
                        viewTreeObserver.addOnGlobalLayoutListener(r7.A02);
                    }
                    StatusPlaybackBaseFragment statusPlaybackBaseFragment = r7.A04;
                    statusPlaybackBaseFragment.A07 = true;
                    statusPlaybackBaseFragment.A19();
                    return;
                }
                return;
            case 12:
                AnonymousClass341 r5 = (AnonymousClass341) this.A00;
                ViewPager viewPager2 = ((AnonymousClass242) r5).A0A;
                int height2 = viewPager2.getHeight();
                int width3 = viewPager2.getWidth();
                if (!(height2 == 0 || width3 == 0 || (r5.A01 == width3 && r5.A00 == height2))) {
                    r5.A01 = width3;
                    r5.A00 = height2;
                    for (AbstractC69213Yj r2 : r5.A0I) {
                        if (r2 != null) {
                            r2.A02(r5.A01, r5.A00);
                        }
                    }
                }
                int width4 = ((View) viewPager2.getParent()).getWidth();
                if (width4 != r5.A02) {
                    r5.A02 = width4;
                    C69233Yl r6 = r5.A0c;
                    int dimensionPixelSize = C12960it.A09(r6.A08).getDimensionPixelSize(R.dimen.sticker_picker_header_item);
                    boolean A01 = C28141Kv.A01(r6.A0B);
                    RecyclerView recyclerView2 = r6.A0A;
                    int i7 = ((width4 / 9) - dimensionPixelSize) >> 1;
                    if (A01) {
                        recyclerView2.setPadding(i7, 0, 0, 0);
                        return;
                    } else {
                        recyclerView2.setPadding(0, 0, i7, 0);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
        view.setVisibility(8);
    }
}
