package com.facebook.redex;

import X.AnonymousClass02B;

/* loaded from: classes4.dex */
public class IDxObserverShape5S0200000_3_I1 implements AnonymousClass02B {
    public Object A00;
    public Object A01;
    public final int A02;

    public IDxObserverShape5S0200000_3_I1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj2;
        this.A01 = obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:113:0x0237  */
    @Override // X.AnonymousClass02B
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ANq(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 1074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxObserverShape5S0200000_3_I1.ANq(java.lang.Object):void");
    }
}
