package com.facebook.redex;

import X.AbstractActivityC28171Kz;
import X.AbstractC14640lm;
import X.AbstractC14670lq;
import X.AbstractC15460nI;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass12S;
import X.AnonymousClass1S6;
import X.AnonymousClass1SF;
import X.AnonymousClass23N;
import X.AnonymousClass24D;
import X.AnonymousClass2I3;
import X.AnonymousClass2Nu;
import X.AnonymousClass2SU;
import X.AnonymousClass2SV;
import X.AnonymousClass2SW;
import X.AnonymousClass2SX;
import X.AnonymousClass2SY;
import X.AnonymousClass2SZ;
import X.C14900mE;
import X.C15380n4;
import X.C25991Bp;
import X.C29631Ua;
import X.C43831xf;
import X.C43951xu;
import X.C51392Um;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import com.whatsapp.R;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.jid.UserJid;
import com.whatsapp.storage.StorageUsageGalleryActivity;
import com.whatsapp.storage.StorageUsageGallerySortBottomSheet;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;
import com.whatsapp.text.ReadMoreTextView;
import com.whatsapp.textstatuscomposer.TextStatusComposerActivity;
import com.whatsapp.twofactor.SettingsTwoFactorAuthActivity;
import com.whatsapp.usernotice.UserNoticeBottomSheetDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.CallLinkInfo;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape5S0100000_I0_5 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape5S0100000_I0_5(TextStatusComposerActivity textStatusComposerActivity, int i) {
        this.A01 = i;
        switch (i) {
            case 3:
            case 4:
            case 5:
                this.A00 = textStatusComposerActivity;
                return;
            default:
                this.A00 = textStatusComposerActivity;
                return;
        }
    }

    public ViewOnClickCListenerShape5S0100000_I0_5(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        int length;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor edit;
        String str;
        CallInfo callInfo;
        AnonymousClass1S6 r0;
        int i;
        C29631Ua r02;
        AnonymousClass2SU r4;
        switch (this.A01) {
            case 0:
                ((StorageUsageGalleryActivity) this.A00).onBackPressed();
                return;
            case 1:
                StorageUsageGalleryActivity storageUsageGalleryActivity = (StorageUsageGalleryActivity) this.A00;
                StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = storageUsageGalleryActivity.A0j;
                if (storageUsageMediaGalleryFragment != null) {
                    int i2 = ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A02;
                    boolean z = true;
                    if (!((ActivityC13810kN) storageUsageGalleryActivity).A06.A05(AbstractC15460nI.A0P) || storageUsageGalleryActivity.A01 != 1) {
                        z = false;
                    }
                    StorageUsageGallerySortBottomSheet A00 = StorageUsageGallerySortBottomSheet.A00(i2, z);
                    A00.A00 = new C51392Um(storageUsageGalleryActivity);
                    storageUsageGalleryActivity.Adm(A00);
                    return;
                }
                return;
            case 2:
                ((Runnable) this.A00).run();
                return;
            case 3:
                TextStatusComposerActivity textStatusComposerActivity = (TextStatusComposerActivity) this.A00;
                if (textStatusComposerActivity.A0I.A03) {
                    textStatusComposerActivity.A0W.get();
                    throw new NullPointerException("maybeShowCrosspostNuxAndExecute");
                } else {
                    textStatusComposerActivity.A2h();
                    return;
                }
            case 4:
                TextStatusComposerActivity textStatusComposerActivity2 = (TextStatusComposerActivity) this.A00;
                textStatusComposerActivity2.A2g();
                int[] iArr = TextStatusComposerActivity.A0k;
                int i3 = textStatusComposerActivity2.A00;
                int[] iArr2 = AnonymousClass24D.A01;
                int i4 = 0;
                while (true) {
                    if (i4 >= iArr2.length) {
                        i4 = -1;
                    } else if (i3 != iArr2[i4]) {
                        i4++;
                    }
                }
                AnonymousClass23N.A00(textStatusComposerActivity2, ((ActivityC13810kN) textStatusComposerActivity2).A08, textStatusComposerActivity2.getString(iArr[i4]));
                return;
            case 5:
                TextStatusComposerActivity textStatusComposerActivity3 = (TextStatusComposerActivity) this.A00;
                int i5 = textStatusComposerActivity3.A01;
                int[] iArr3 = AnonymousClass24D.A02;
                int i6 = 0;
                while (true) {
                    length = iArr3.length;
                    if (i6 >= length) {
                        i6 = -1;
                    } else if (i5 != iArr3[i6]) {
                        i6++;
                    }
                }
                int i7 = iArr3[(i6 + 1) % length];
                textStatusComposerActivity3.A01 = i7;
                Typeface A03 = AnonymousClass24D.A03(textStatusComposerActivity3, i7);
                textStatusComposerActivity3.A08.setTypeface(A03);
                textStatusComposerActivity3.A0T.setTypeface(A03);
                int[] iArr4 = TextStatusComposerActivity.A0l;
                int i8 = textStatusComposerActivity3.A01;
                int i9 = 0;
                while (true) {
                    if (i9 >= length) {
                        i9 = -1;
                    } else if (i8 != iArr3[i9]) {
                        i9++;
                    }
                }
                AnonymousClass23N.A00(textStatusComposerActivity3, ((ActivityC13810kN) textStatusComposerActivity3).A08, textStatusComposerActivity3.getString(iArr4[i9]));
                return;
            case 6:
                ((TextStatusComposerActivity) this.A00).A2e();
                return;
            case 7:
                ((AnonymousClass01E) this.A00).A0B().finish();
                return;
            case 8:
                ((SettingsTwoFactorAuthActivity) this.A00).A2g(1, 2);
                return;
            case 9:
                ((ActivityC13810kN) this.A00).Adl(new SettingsTwoFactorAuthActivity.ConfirmDisableDialog(), null);
                return;
            case 10:
                ((SettingsTwoFactorAuthActivity) this.A00).A2g(2);
                return;
            case 11:
                ((SettingsTwoFactorAuthActivity) this.A00).A2g(1);
                return;
            case 12:
                ReadMoreTextView readMoreTextView = (ReadMoreTextView) this.A00;
                readMoreTextView.setExpanded(!readMoreTextView.A0I());
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment = (UserNoticeBottomSheetDialogFragment) this.A00;
                AnonymousClass12S r3 = userNoticeBottomSheetDialogFragment.A0C;
                boolean z2 = !TextUtils.isEmpty(userNoticeBottomSheetDialogFragment.A0E.A03);
                C43831xf A01 = r3.A01.A01();
                int i10 = 9;
                if (z2) {
                    i10 = 6;
                }
                r3.A00(A01, Integer.valueOf(i10));
                userNoticeBottomSheetDialogFragment.A0D.A02();
                userNoticeBottomSheetDialogFragment.A1B();
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                UserNoticeBottomSheetDialogFragment userNoticeBottomSheetDialogFragment2 = (UserNoticeBottomSheetDialogFragment) this.A00;
                AnonymousClass12S r1 = userNoticeBottomSheetDialogFragment2.A0C;
                if (!TextUtils.isEmpty(userNoticeBottomSheetDialogFragment2.A0E.A03)) {
                    r1.A01(4);
                }
                userNoticeBottomSheetDialogFragment2.A1B();
                return;
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
            case 18:
                CompoundButton compoundButton = (CompoundButton) this.A00;
                compoundButton.setChecked(!compoundButton.isChecked());
                return;
            case 19:
                View view2 = (View) this.A00;
                if (view2.isSelected()) {
                    view2.setSelected(false);
                }
                view2.setSelected(true);
                return;
            case C43951xu.A01:
                AnonymousClass2I3.A00((AnonymousClass2I3) this.A00);
                return;
            case 21:
                ((ViewSharedContactArrayActivity) this.A00).toggleCheckBox(view);
                return;
            case 22:
                ((AnonymousClass2SZ) this.A00).APD();
                return;
            case 23:
                AbstractC14670lq r2 = ((AnonymousClass2SW) this.A00).A00;
                if (r2.A0N.A0D()) {
                    r2.A04();
                } else {
                    AnonymousClass2SX r32 = r2.A1J;
                    r32.A01++;
                    AbstractC14640lm r12 = r32.A06.A00.A0J;
                    if (r12 != null) {
                        if (C15380n4.A0F(r12)) {
                            sharedPreferences = r32.A04.A00;
                            edit = sharedPreferences.edit();
                            str = "ptt_playback_broadcast";
                        } else {
                            boolean A0J = C15380n4.A0J(r12);
                            sharedPreferences = r32.A04.A00;
                            edit = sharedPreferences.edit();
                            if (A0J) {
                                str = "ptt_playback_group";
                            } else {
                                str = "ptt_playback_individual";
                            }
                        }
                        edit.putLong(str, sharedPreferences.getLong(str, 0) + 1).apply();
                    }
                    try {
                        r2.A19.A02();
                        if (r2.A0N.A02() == r2.A0N.A03()) {
                            r2.A0N.A0A(0);
                        }
                        if (r2.A0N.A02() == 0) {
                            r2.A0N.A08();
                        } else {
                            r2.A0N.A07();
                        }
                        r2.A0a.post(r2.A1Q);
                        for (AnonymousClass2SY r03 : r2.A1H.A01()) {
                            r03.A00();
                        }
                    } catch (IOException e) {
                        Log.e("voicenoterecordingui/showvoicenotepreview/ error playing voice note preview ", e);
                    }
                }
                r2.A0B();
                return;
            case 24:
                ((AnonymousClass2SV) this.A00).AYR();
                return;
            case 25:
                VoipActivityV2 voipActivityV2 = (VoipActivityV2) this.A00;
                if (!(voipActivityV2.A0v == null || (callInfo = Voip.getCallInfo()) == null || (r0 = callInfo.self) == null)) {
                    Voip.muteCall(!r0.A0C);
                }
                voipActivityV2.A2z();
                return;
            case 26:
                VoipActivityV2 voipActivityV22 = (VoipActivityV2) this.A00;
                i = 0;
                voipActivityV22.A1p = false;
                CallInfo A2i = voipActivityV22.A2i();
                if (A2i != null && A2i.callState == Voip.CallState.ACTIVE && !A2i.callEnding && voipActivityV22.A0v != null) {
                    Log.i("voip/VoipActivityV2/toggleVideoBtn/clicked");
                    AnonymousClass1S6 r6 = A2i.self;
                    AnonymousClass1S6 defaultPeerInfo = A2i.getDefaultPeerInfo();
                    if (defaultPeerInfo == null || defaultPeerInfo.A07) {
                        int i11 = r6.A04;
                        if (i11 == 6) {
                            view.setSelected(false);
                            voipActivityV22.A3H(voipActivityV22.A10, r6);
                            voipActivityV22.A0v.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(7));
                            return;
                        } else if (i11 == 0) {
                            if (A2i.isGroupCall()) {
                                ((ActivityC13810kN) voipActivityV22).A05.A0E(voipActivityV22.getString(R.string.voip_not_support_switch_voice_and_video_call_in_group_call), 0);
                                return;
                            }
                            UserJid peerJid = A2i.getPeerJid();
                            AnonymousClass009.A05(peerJid);
                            if (((ActivityC13810kN) voipActivityV22).A09.A00.getInt("switch_to_video_call_confirmation_dialog_count", 0) < 5) {
                                voipActivityV22.Adm(new VoipActivityV2.SwitchConfirmationFragment());
                                return;
                            } else if (voipActivityV22.A3U(peerJid, 1, true)) {
                                C29631Ua r04 = voipActivityV22.A0v;
                                AnonymousClass009.A05(r04);
                                r04.A0M();
                                return;
                            } else {
                                return;
                            }
                        } else if (i11 == 1) {
                            voipActivityV22.A0v.A0x.execute(new RunnableBRunnable0Shape0S0000000_I0(6));
                            view.setSelected(true);
                            return;
                        } else if (i11 == 3) {
                            r02 = voipActivityV22.A0v;
                            break;
                        } else {
                            return;
                        }
                    } else {
                        String A0A = voipActivityV22.A14.A0A(((AbstractActivityC28171Kz) voipActivityV22).A03.A0B(defaultPeerInfo.A06), -1);
                        boolean z3 = defaultPeerInfo.A08;
                        C14900mE r22 = ((ActivityC13810kN) voipActivityV22).A05;
                        int i12 = R.string.voip_not_support_switch_voice_and_video_call;
                        if (z3) {
                            i12 = R.string.voip_not_enable_switch_voice_and_video_call;
                        }
                        r22.A0E(voipActivityV22.getString(i12, A0A), 0);
                        return;
                    }
                } else {
                    return;
                }
                break;
            case 27:
                VoipActivityV2 voipActivityV23 = (VoipActivityV2) this.A00;
                if (voipActivityV23.A0v != null) {
                    Log.i("voip/VoipActivityV2/centerScreenCallStatusButton/resumeWhatsAppCallListener");
                    voipActivityV23.A0v.A0w(Voip.getCurrentCallId());
                    return;
                }
                return;
            case 28:
                VoipActivityV2 voipActivityV24 = (VoipActivityV2) this.A00;
                CallInfo A2i2 = voipActivityV24.A2i();
                if (A2i2 != null && A2i2.self.A04 == 3 && voipActivityV24.A0v != null) {
                    Log.i("voip/VoipActivityV2/centerScreenCallStatusButton/cancelSwitchToVideoCallListener");
                    r02 = voipActivityV24.A0v;
                    i = 0;
                    break;
                } else {
                    return;
                }
            case 29:
                VoipActivityV2 voipActivityV25 = (VoipActivityV2) this.A00;
                CallInfo A2i3 = voipActivityV25.A2i();
                if (!(A2i3 == null || A2i3.callState == Voip.CallState.NONE)) {
                    if (A2i3.videoEnabled) {
                        AnonymousClass1S6 r23 = A2i3.self;
                        if (r23.A04 != 6) {
                            voipActivityV25.A05 = 0;
                            voipActivityV25.A0E.removeMessages(3);
                            Voip.switchCamera();
                            voipActivityV25.A3H(voipActivityV25.A10, r23);
                        }
                    } else {
                        C29631Ua r05 = voipActivityV25.A0v;
                        if (r05 != null) {
                            AnonymousClass2Nu r5 = r05.A2P;
                            boolean z4 = true;
                            if (r5.A00 == 1) {
                                z4 = false;
                            }
                            CallInfo callInfo2 = Voip.getCallInfo();
                            if (z4 && r5.A00 == 3 && !r5.A06) {
                                r5.A09(callInfo2, false);
                            }
                            r5.A0A(callInfo2, z4);
                        }
                    }
                    voipActivityV25.A2z();
                    return;
                }
                return;
            case C25991Bp.A0S:
                VoipActivityV2.A0B((VoipActivityV2) this.A00);
                return;
            case 31:
                VoipActivityV2 voipActivityV26 = (VoipActivityV2) this.A00;
                C29631Ua r06 = voipActivityV26.A0v;
                if (r06 != null) {
                    AnonymousClass2Nu r33 = r06.A2P;
                    boolean z5 = false;
                    if (r33.A00 == 3) {
                        z5 = true;
                    }
                    r33.A09(Voip.getCallInfo(), !z5);
                }
                voipActivityV26.A2z();
                return;
            case 32:
            default:
                return;
            case 33:
                VoipActivityV2 voipActivityV27 = (VoipActivityV2) this.A00;
                Log.i("voip end call button pressed");
                Voip.CallState currentCallState = Voip.getCurrentCallState();
                if (currentCallState == Voip.CallState.NONE) {
                    Log.e("voip end call button pressed in NONE state");
                } else if (Voip.A0A(currentCallState)) {
                    voipActivityV27.A3B(2);
                    return;
                } else if (!AnonymousClass1SF.A0O(((ActivityC13810kN) voipActivityV27).A0C) || currentCallState != Voip.CallState.LINK) {
                    Log.i("voip/VoipActivityV2/call/end");
                    C29631Ua r24 = voipActivityV27.A0v;
                    if (r24 != null) {
                        r24.A0m(null, null, 1);
                    }
                    voipActivityV27.A1a = true;
                    return;
                } else {
                    C29631Ua r13 = voipActivityV27.A0v;
                    if (r13 != null) {
                        r13.A0v(CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID);
                    }
                }
                voipActivityV27.finish();
                return;
            case 34:
                Log.i("VoipActivityV2 vm cancel onClick");
                ((Activity) this.A00).finish();
                return;
            case 35:
                VoipActivityV2 voipActivityV28 = (VoipActivityV2) this.A00;
                Log.i("voip/VoipActivityV2/VideoCallParticipantView/cancelButton/onClick");
                if (view.getTag() != null) {
                    Object tag = view.getTag();
                    C29631Ua r07 = voipActivityV28.A0v;
                    if (r07 != null) {
                        r07.A0x.execute(new RunnableBRunnable0Shape3S0100000_I0_3(tag, 25));
                        return;
                    }
                    return;
                }
                return;
            case 36:
                ((VoipActivityV2) this.A00).A2y();
                return;
            case 37:
                VoipActivityV2 voipActivityV29 = (VoipActivityV2) this.A00;
                Log.i("voip/VoipActivityV2/videoPipParticipantView/onClick");
                if (!Build.DEVICE.equalsIgnoreCase("j7elte")) {
                    CallInfo A2i4 = voipActivityV29.A2i();
                    if (A2i4 == null || !A2i4.videoEnabled) {
                        Log.i("voip/VoipActivityV2/switchVideoSurface. ignore switch when it's not a video call");
                        return;
                    }
                    if (voipActivityV29.A1U.size() == 2) {
                        boolean z6 = true;
                        if (voipActivityV29.A0y.A03 == 1) {
                            boolean z7 = false;
                            if (voipActivityV29.A1U.size() == 2) {
                                z7 = true;
                            }
                            AnonymousClass009.A0A("This function can only be called when there are exactly two participants", z7);
                            Iterator it = voipActivityV29.A1U.entrySet().iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    Map.Entry entry = (Map.Entry) it.next();
                                    if (entry.getValue() != voipActivityV29.A10) {
                                        r4 = (AnonymousClass2SU) entry.getValue();
                                    }
                                } else {
                                    AnonymousClass009.A0A("Can not be here", false);
                                    r4 = null;
                                }
                            }
                            AnonymousClass009.A05(r4);
                            VideoCallParticipantView videoCallParticipantView = voipActivityV29.A10.A00;
                            AnonymousClass009.A03(videoCallParticipantView);
                            VideoCallParticipantView videoCallParticipantView2 = r4.A00;
                            AnonymousClass009.A03(videoCallParticipantView2);
                            StringBuilder sb = new StringBuilder("voip/VoipActivityV2/switchVideoSurface. show preview on full screen = ");
                            if (videoCallParticipantView2.A03 != 0) {
                                z6 = false;
                            }
                            sb.append(z6);
                            Log.i(sb.toString());
                            r4.A04();
                            voipActivityV29.A10.A04();
                            voipActivityV29.A10.A06(videoCallParticipantView2);
                            r4.A06(videoCallParticipantView);
                            voipActivityV29.A3J(A2i4);
                            return;
                        }
                    }
                    StringBuilder sb2 = new StringBuilder("voip/VoipActivityV2/switchVideoSurface. switch is allowed only for two participants, # of participants = ");
                    sb2.append(voipActivityV29.A1U.size());
                    Log.w(sb2.toString());
                    return;
                }
                return;
            case 38:
                ((VoipCallControlBottomSheetV2) this.A00).A1O(1);
                return;
        }
        r02.A0x.execute(new RunnableBRunnable0Shape0S0001000_I0(i, 3));
    }
}
