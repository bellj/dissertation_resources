package com.facebook.redex;

import X.C28001Kb;
import com.whatsapp.status.StatusesFragment;
import java.lang.ref.WeakReference;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape12S0100000_I0_12 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape12S0100000_I0_12(C28001Kb r2) {
        this.A01 = 26;
        this.A00 = new WeakReference(r2);
    }

    public RunnableBRunnable0Shape12S0100000_I0_12(StatusesFragment statusesFragment, int i) {
        this.A01 = i;
        switch (i) {
            case 4:
            case 5:
                this.A00 = statusesFragment;
                return;
            default:
                this.A00 = statusesFragment;
                return;
        }
    }

    public RunnableBRunnable0Shape12S0100000_I0_12(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:271:0x02e1 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v5, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v6, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v7, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r4v10, types: [java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1902
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12.run():void");
    }
}
