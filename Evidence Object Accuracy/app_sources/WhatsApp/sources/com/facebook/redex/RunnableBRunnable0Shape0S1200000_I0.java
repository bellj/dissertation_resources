package com.facebook.redex;

import X.AbstractC13940ka;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC16130oV;
import X.AbstractC28901Pl;
import X.AbstractC34011fR;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass01J;
import X.AnonymousClass109;
import X.AnonymousClass18L;
import X.AnonymousClass19X;
import X.AnonymousClass1CL;
import X.AnonymousClass1FM;
import X.AnonymousClass1IM;
import X.AnonymousClass1K9;
import X.AnonymousClass1KC;
import X.AnonymousClass1KS;
import X.AnonymousClass1MU;
import X.AnonymousClass1OT;
import X.AnonymousClass1PE;
import X.AnonymousClass1R9;
import X.AnonymousClass1V8;
import X.AnonymousClass1VC;
import X.AnonymousClass1WA;
import X.AnonymousClass29U;
import X.AnonymousClass2EM;
import X.AnonymousClass2O5;
import X.AnonymousClass2O6;
import X.AnonymousClass32J;
import X.AnonymousClass3JL;
import X.AnonymousClass3XZ;
import X.AnonymousClass45A;
import X.AnonymousClass4CA;
import X.AnonymousClass4T8;
import X.AnonymousClass5WG;
import X.C14270l8;
import X.C14300lD;
import X.C14310lE;
import X.C14430lQ;
import X.C14450lS;
import X.C14480lV;
import X.C14580lf;
import X.C14820m6;
import X.C14850m9;
import X.C14860mA;
import X.C14890mD;
import X.C15370n3;
import X.C15650ng;
import X.C16170oZ;
import X.C16310on;
import X.C16700pc;
import X.C18000rk;
import X.C18790t3;
import X.C19990v2;
import X.C21010wg;
import X.C21320xE;
import X.C21390xL;
import X.C22230yk;
import X.C22410z2;
import X.C22520zD;
import X.C242714w;
import X.C25621Ac;
import X.C25991Bp;
import X.C26651Eh;
import X.C26691El;
import X.C26741Eq;
import X.C27151Gf;
import X.C28111Kr;
import X.C29631Ua;
import X.C30101Wc;
import X.C34021fS;
import X.C35871iu;
import X.C35881iv;
import X.C35891iw;
import X.C36201jU;
import X.C37071lG;
import X.C38051nR;
import X.C38421o4;
import X.C43731xV;
import X.C43951xu;
import X.C44771zW;
import X.C459523w;
import X.C47732Ce;
import X.C49862Nb;
import X.C50032Ns;
import X.C63363Bh;
import X.C68563Vw;
import X.C68863Xa;
import X.C77443nI;
import X.C77453nJ;
import X.C866248e;
import X.C866548h;
import X.C90924Pt;
import android.accounts.Account;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.ConditionVariable;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.backup.google.BaseNewUserSetupActivity$AuthRequestDialogFragment;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.businessdirectory.util.LocationUpdateListener;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendWebForwardJob;
import com.whatsapp.notification.AndroidWear;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.Voip;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1200000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public String A02;
    public final int A03;

    public RunnableBRunnable0Shape0S1200000_I0(AnonymousClass016 r2, C21390xL r3) {
        this.A03 = 19;
        this.A00 = r3;
        this.A02 = "status_distribution";
        this.A01 = r2;
    }

    public RunnableBRunnable0Shape0S1200000_I0(Object obj, Object obj2, String str, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A02 = str;
        this.A01 = obj2;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Runnable
    public void run() {
        C16310on A01;
        C22230yk r1;
        int i;
        Pair A00;
        int i2;
        String str;
        String str2;
        C77443nI e;
        ConditionVariable conditionVariable;
        ConditionVariable conditionVariable2;
        String str3;
        switch (this.A03) {
            case 0:
                ((C14270l8) this.A00).A09(this.A02, this.A01);
                return;
            case 1:
                ((C22520zD) this.A00).A0P.A00((AnonymousClass1MU) ((UserJid) this.A01), this.A02);
                return;
            case 2:
                AnonymousClass29U r4 = (AnonymousClass29U) this.A00;
                String str4 = this.A02;
                Object obj = this.A01;
                str3 = "settings-gdrive/auth-request/user-cancelled";
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append("settings-gdrive/auth-request asking GoogleAuthUtil for token for ");
                    sb.append(C44771zW.A0B(str4));
                    Log.i(sb.toString());
                    String A012 = AnonymousClass3JL.A01(new Account(str4, "com.google"), r4);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("settings-gdrive/auth-request for account ");
                    sb2.append(C44771zW.A0B(str4));
                    sb2.append(", token has been received.");
                    Log.i(sb2.toString());
                    if (!r4.A0a) {
                        ((ActivityC13810kN) r4).A05.A0H(new RunnableBRunnable0Shape0S2100000_I0(r4, A012, str4, 1));
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("settings-gdrive/auth-request/received-token-but-user-cancelled-the-request/");
                        sb3.append(C44771zW.A0B(str4));
                        Log.i(sb3.toString());
                    }
                    r4.A0Y.open();
                    return;
                } catch (C77443nI e2) {
                    e = e2;
                    if (r4.A0a) {
                        conditionVariable = r4.A0Y;
                        conditionVariable.open();
                        Log.e("settings-gdrive/gps-unavailable", e);
                        return;
                    }
                    ((ActivityC13810kN) r4).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(r4, 39, obj));
                    Log.e("settings-gdrive/gps-unavailable", e);
                    return;
                } catch (C77453nJ e3) {
                    boolean z = r4.A0a;
                    conditionVariable2 = r4.A0Y;
                    if (!z) {
                        conditionVariable2.close();
                        ((ActivityC13810kN) r4).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(r4, 41, e3));
                        return;
                    }
                    conditionVariable2.open();
                    return;
                } catch (AnonymousClass4CA | SecurityException e4) {
                    Log.e("settings-gdrive/auth-request", e4);
                    r4.A0Y.open();
                    if (!r4.A0a) {
                        ((ActivityC13810kN) r4).A05.A0H(new RunnableBRunnable0Shape0S1100000_I0(5, str4, r4));
                        return;
                    }
                    Log.i(str3);
                    return;
                } catch (IOException e5) {
                    Log.e("settings-gdrive/auth-request", e5);
                    r4.A0Y.open();
                    if (!r4.A0a) {
                        ((ActivityC13810kN) r4).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(r4, 19));
                        return;
                    }
                    Log.i(str3);
                    return;
                }
            case 3:
                ((AnonymousClass29U) this.A00).A2j((BaseNewUserSetupActivity$AuthRequestDialogFragment) this.A01, this.A02);
                return;
            case 4:
            case 5:
                ((SettingsGoogleDrive) this.A00).A2m((SettingsGoogleDrive.AuthRequestDialogFragment) this.A01, this.A02);
                return;
            case 6:
                SettingsGoogleDrive.A0A((SettingsGoogleDrive.AuthRequestDialogFragment) this.A01, (SettingsGoogleDrive) this.A00, this.A02);
                return;
            case 7:
                SettingsGoogleDrive settingsGoogleDrive = (SettingsGoogleDrive) this.A00;
                String str5 = this.A02;
                Object obj2 = this.A01;
                str3 = "settings-gdrive/auth-request/user-cancelled";
                try {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("settings-gdrive/auth-request asking GoogleAuthUtil for token for ");
                    sb4.append(C44771zW.A0B(str5));
                    Log.i(sb4.toString());
                    String A013 = AnonymousClass3JL.A01(new Account(str5, "com.google"), settingsGoogleDrive);
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("settings-gdrive/auth-request for account ");
                    sb5.append(C44771zW.A0B(str5));
                    sb5.append(", token has been received.");
                    Log.i(sb5.toString());
                    if (!settingsGoogleDrive.A0t) {
                        ((ActivityC13810kN) settingsGoogleDrive).A05.A0H(new RunnableBRunnable0Shape0S2100000_I0(settingsGoogleDrive, A013, str5, 2));
                    } else {
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("settings-gdrive/auth-request/received-token-but-user-cancelled-the-request/");
                        sb6.append(C44771zW.A0B(str5));
                        Log.i(sb6.toString());
                    }
                    settingsGoogleDrive.A0r.open();
                    return;
                } catch (C77443nI e6) {
                    e = e6;
                    if (settingsGoogleDrive.A0t) {
                        conditionVariable = settingsGoogleDrive.A0r;
                        conditionVariable.open();
                        Log.e("settings-gdrive/gps-unavailable", e);
                        return;
                    }
                    ((ActivityC13810kN) settingsGoogleDrive).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(settingsGoogleDrive, 49, obj2));
                    Log.e("settings-gdrive/gps-unavailable", e);
                    return;
                } catch (C77453nJ e7) {
                    boolean z2 = settingsGoogleDrive.A0t;
                    conditionVariable2 = settingsGoogleDrive.A0r;
                    if (!z2) {
                        conditionVariable2.close();
                        ((ActivityC13810kN) settingsGoogleDrive).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(settingsGoogleDrive, 47, e7));
                        return;
                    }
                    conditionVariable2.open();
                    return;
                } catch (AnonymousClass4CA | SecurityException e8) {
                    Log.e("settings-gdrive/auth-request", e8);
                    settingsGoogleDrive.A0r.open();
                    if (!settingsGoogleDrive.A0t) {
                        ((ActivityC13810kN) settingsGoogleDrive).A05.A0H(new RunnableBRunnable0Shape0S1100000_I0(11, str5, settingsGoogleDrive));
                        return;
                    }
                    Log.i(str3);
                    return;
                } catch (IOException e9) {
                    Log.e("settings-gdrive/auth-request", e9);
                    settingsGoogleDrive.A0r.open();
                    if (!settingsGoogleDrive.A0t) {
                        ((ActivityC13810kN) settingsGoogleDrive).A05.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(settingsGoogleDrive, 44));
                        return;
                    }
                    Log.i(str3);
                    return;
                }
            case 8:
                C30101Wc r12 = (C30101Wc) this.A00;
                String str6 = this.A02;
                AnonymousClass1V8 r2 = (AnonymousClass1V8) this.A01;
                AbstractC13940ka r0 = r12.A00;
                if (r0 != null) {
                    r0.AR0();
                }
                AnonymousClass1VC r13 = r12.A01;
                if (r13 != null) {
                    r13.A00(new C47732Ce(r2, str6));
                    return;
                }
                return;
            case 9:
                AnonymousClass2EM r3 = (AnonymousClass2EM) this.A00;
                Jid jid = (Jid) this.A01;
                String str7 = this.A02;
                try {
                    r3.A0C.A07(jid, str7);
                    AnonymousClass016 r02 = r3.A02;
                    if (r02 != null) {
                        r02.A0A(str7);
                        return;
                    }
                    return;
                } catch (Exception unused) {
                    AnonymousClass016 r14 = r3.A05;
                    if (r14 != null) {
                        r14.A0A(Boolean.TRUE);
                        return;
                    }
                    return;
                }
            case 10:
                AnonymousClass19X r42 = (AnonymousClass19X) this.A00;
                r42.A09.AZO(new C68863Xa(r42, (AnonymousClass5WG) this.A01, this.A02));
                return;
            case 11:
                AnonymousClass1CL r5 = (AnonymousClass1CL) this.A00;
                Future future = (Future) this.A01;
                String str8 = this.A02;
                try {
                    future.get(32000, TimeUnit.MILLISECONDS);
                } catch (Exception unused2) {
                } catch (Throwable th) {
                    r5.A00.remove(str8);
                    throw th;
                }
                r5.A00.remove(str8);
                return;
            case 12:
                Location location = (Location) this.A01;
                ((LocationUpdateListener) this.A00).A01.A0B(new C90924Pt(this.A02, location.getLatitude(), location.getLongitude(), location.getAccuracy()));
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = (BusinessDirectorySearchQueryViewModel) this.A00;
                String str9 = this.A02;
                AnonymousClass4T8 r43 = (AnonymousClass4T8) this.A01;
                LinkedList linkedList = businessDirectorySearchQueryViewModel.A0a;
                synchronized (linkedList) {
                    if (linkedList.isEmpty()) {
                        C68563Vw r22 = businessDirectorySearchQueryViewModel.A0P;
                        C63363Bh r03 = (C63363Bh) r22.A04.A01();
                        if (r03 != null) {
                            str2 = r03.A06;
                        } else {
                            str2 = null;
                        }
                        if (str9.equals(str2)) {
                            businessDirectorySearchQueryViewModel.A0C.removeCallbacks(businessDirectorySearchQueryViewModel.A06);
                            r22.ANS(r43);
                        }
                    }
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                C29631Ua r32 = (C29631Ua) this.A00;
                String str10 = this.A02;
                List<UserJid> list = (List) this.A01;
                StringBuilder sb7 = new StringBuilder("voip/actionStartNewOutgoingCall async start for callId ");
                sb7.append(str10);
                Log.i(sb7.toString());
                long elapsedRealtime = SystemClock.elapsedRealtime();
                CallInfo callInfo = Voip.getCallInfo();
                if (!Voip.A08(callInfo) || !TextUtils.equals(str10, callInfo.callId)) {
                    str = "voip/actionStartNewOutgoingCall async operation canceled";
                } else {
                    r32.A1Y.A06(true);
                    if (r32.A20.A07(1971)) {
                        for (UserJid userJid : list) {
                            r32.A29.A00(userJid);
                        }
                    }
                    StringBuilder sb8 = new StringBuilder("voip/actionStartNewOutgoingCall async operation elapsed ");
                    sb8.append(SystemClock.elapsedRealtime() - elapsedRealtime);
                    sb8.append(" ms");
                    str = sb8.toString();
                }
                Log.i(str);
                return;
            case 15:
                ((C14310lE) this.A00).A06((C37071lG) this.A01, this.A02);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                C25621Ac r52 = (C25621Ac) this.A00;
                AbstractC14640lm r44 = (AbstractC14640lm) this.A01;
                String str11 = this.A02;
                C19990v2 r15 = r52.A02;
                AnonymousClass1PE A06 = r15.A06(r44);
                if (A06 == null) {
                    A06 = new AnonymousClass1PE(r44);
                    A06.A0e = str11;
                    r15.A0C(A06, r44);
                }
                A06.A0e = str11;
                r52.A02(A06, r44, null);
                return;
            case 17:
                AbstractC15340mz r16 = (AbstractC15340mz) this.A01;
                String str12 = this.A02;
                A01 = ((C26691El) this.A00).A01.A02();
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("message_row_id", Long.valueOf(r16.A11));
                    contentValues.put("message_template_id", str12);
                    A01.A03.A06(contentValues, "messages_hydrated_four_row_template", 5);
                    return;
                } catch (Throwable th2) {
                    try {
                        A01.close();
                    } catch (Throwable unused3) {
                    }
                    throw th2;
                }
            case 18:
                String str13 = this.A02;
                C14580lf r23 = (C14580lf) this.A01;
                AbstractC28901Pl A08 = ((C38051nR) this.A00).A01.A08(str13);
                if (A08 != null) {
                    r23.A02(A08);
                    return;
                }
                StringBuilder sb9 = new StringBuilder("Payments: failed to read payment method from credId: ");
                sb9.append(str13);
                r23.A03(new RuntimeException(sb9.toString()));
                return;
            case 19:
                ((AnonymousClass017) this.A01).A0A(((C21390xL) this.A00).A02(this.A02));
                return;
            case C43951xu.A01:
                String str14 = this.A02;
                GroupJid groupJid = (GroupJid) this.A01;
                C35871iu r6 = ((C36201jU) this.A00).A01;
                StringBuilder sb10 = new StringBuilder("ChatSupportTicketManager/contactSupport/onSuccess called, ticketId=");
                sb10.append(str14);
                Log.i(sb10.toString());
                C35881iv r7 = r6.A01;
                C35891iw r33 = r7.A00;
                if (r33.A02 != null) {
                    Log.i("ContactUsActivity/createTicketIq/onSuccess, removing spinner and finishing activity");
                    boolean z3 = false;
                    int max = Math.max(0, r33.A0A.A02(974));
                    r33.A03 = groupJid;
                    r33.A04 = false;
                    C21320xE r24 = r33.A09;
                    C27151Gf r17 = r33.A08;
                    r24.A03(r17);
                    if (max <= 0) {
                        z3 = true;
                    }
                    if (!r33.A04(z3)) {
                        r33.A06.A0J(new RunnableBRunnable0Shape7S0100000_I0_7(r7, 16), (long) max);
                    } else {
                        r24.A04(r17);
                    }
                }
                if (r6.A02) {
                    r6.A00.A03.Ab2(new RunnableBRunnable0Shape0S1100000_I0(25, str14, r6));
                    return;
                }
                return;
            case 21:
                AnonymousClass109 r34 = (AnonymousClass109) this.A00;
                AbstractC16130oV r18 = (AbstractC16130oV) this.A01;
                String str15 = this.A02;
                StringBuilder sb11 = new StringBuilder("app/mediajobmanager/enqueueingwebmediareupload enqueuing message: ");
                sb11.append(r18.A0z);
                Log.i(sb11.toString());
                C38421o4 r122 = new C38421o4(Collections.singletonList(r18));
                AnonymousClass1K9 A014 = AnonymousClass1K9.A01(r34.A04, r122, r34.A0I, r34.A0J, new C14480lV(true, r122.A04(), r122.A03()), false);
                C14300lD r72 = r34.A0E;
                AnonymousClass1KC A05 = r72.A05(A014, false);
                C14450lS r04 = A05.A0K;
                AnonymousClass009.A05(r04);
                r04.A05(1);
                C14430lQ r8 = A05.A0J;
                AnonymousClass009.A05(r8);
                long j = r18.A01;
                C14850m9 r11 = r34.A0C;
                if (j <= ((long) r11.A02(1098)) * 1048576) {
                    if (r11.A07(475)) {
                        C26741Eq r112 = r34.A0H;
                        if (System.currentTimeMillis() - r8.A05 < 1800000 || !r112.A00(r8)) {
                            return;
                        }
                    }
                    r122.A01();
                    r34.A02(r122, A05);
                    A05.A0U = "mms";
                    A05.A03(new C459523w(r122, r34, A05, false, false), r34.A0U);
                    r72.A09(A05, new AnonymousClass45A(A05, A05.A00(), str15));
                    return;
                }
                return;
            case 22:
                C26651Eh r53 = (C26651Eh) this.A00;
                AnonymousClass2O5 r35 = (AnonymousClass2O5) this.A01;
                String str16 = this.A02;
                r35.A00 = 400;
                r35.A01 = null;
                try {
                    A00 = Voip.A01.A00(r35.A02);
                } catch (UnsatisfiedLinkError e10) {
                    Log.e("app/xmpp/recv/call_offer_web_query/unable to query for current call offer", e10);
                }
                if (A00 == null) {
                    i2 = 410;
                } else {
                    i2 = 409;
                    if (!((Boolean) A00.first).booleanValue()) {
                        r35.A00 = 200;
                        Object obj3 = A00.second;
                        AnonymousClass009.A05(obj3);
                        r35.A01 = (AnonymousClass2O6) obj3;
                        r53.A17.A00(r35, str16, 34);
                        return;
                    }
                }
                r35.A00 = i2;
                r53.A17.A00(r35, str16, 34);
                return;
            case 23:
                C26651Eh r54 = (C26651Eh) this.A00;
                String str17 = this.A02;
                C866248e r36 = (C866248e) this.A01;
                if (str17 == null) {
                    r1 = r54.A18;
                    i = 400;
                } else if (r54.A0N.A05(true) == 3) {
                    r1 = r54.A18;
                    i = 412;
                } else {
                    List A0D = r54.A1O.A0D(0);
                    int i3 = r36.A00;
                    ArrayList arrayList = new ArrayList(i3);
                    int i4 = r36.A01;
                    int i5 = (i4 - 1) * i3;
                    while (i5 < A0D.size() && i5 < i4 * i3) {
                        arrayList.add(C28111Kr.A00(r54.A0E, r54.A0y, (AnonymousClass1KS) A0D.get(i5)));
                        i5++;
                    }
                    r54.A17.A00(new C50032Ns(r54.A02, r54.A08, C28111Kr.A02(A0D), arrayList, A0D.size()), str17, r36.A05);
                    return;
                }
                r1.A0F(str17, i);
                return;
            case 24:
                C22410z2 r45 = (C22410z2) this.A00;
                List list2 = (List) this.A01;
                String str18 = this.A02;
                List A015 = r45.A01(list2);
                if (A015.size() > 0) {
                    if (str18 == null) {
                        str18 = r45.A00();
                    }
                    C866548h r37 = new C866548h(r45, str18, list2);
                    C14890mD r25 = r45.A0D;
                    ((AbstractC34011fR) r37).A00 = r25.A00().A03;
                    C14860mA r05 = r45.A0E;
                    C34021fS r19 = new C34021fS(r37, r05);
                    String A02 = r05.A02();
                    r45.A02.A00(new SendWebForwardJob(Message.obtain(null, 0, 50, 0, new C49862Nb(r19, A02, str18, A015)), A02, r25.A00().A03));
                    return;
                }
                return;
            case 25:
                AndroidWear androidWear = (AndroidWear) this.A00;
                C15370n3 r26 = (C15370n3) this.A01;
                String str19 = this.A02;
                C16170oZ r46 = androidWear.A01;
                Jid A0B = r26.A0B(AbstractC14640lm.class);
                AnonymousClass009.A05(A0B);
                r46.A08(null, null, null, str19, Collections.singletonList(A0B), null, false, false);
                androidWear.A03.A00((AbstractC14640lm) r26.A0B(AbstractC14640lm.class), null, null, true, true, true);
                androidWear.A05.A0B(null, true, true, true, false, false);
                return;
            case 26:
                ((SharedPreferences.OnSharedPreferenceChangeListener) this.A01).onSharedPreferenceChanged(((AnonymousClass1IM) this.A00).A03, this.A02);
                return;
            case 27:
                AnonymousClass1FM r62 = (AnonymousClass1FM) this.A00;
                AnonymousClass1OT r55 = (AnonymousClass1OT) this.A01;
                String str20 = this.A02;
                StringBuilder sb12 = new StringBuilder("recvmessagelistener/on-revoke-psa stanzaKey=");
                sb12.append(r55);
                sb12.append("; campaignId=");
                sb12.append(str20);
                Log.i(sb12.toString());
                C242714w r06 = r62.A0C;
                ArrayList arrayList2 = new ArrayList();
                A01 = r06.A00.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT message_row_id FROM message_status_psa_campaign WHERE campaign_id = ?", new String[]{str20});
                    if (A09 == null) {
                        arrayList2 = null;
                    } else {
                        while (A09.moveToNext()) {
                            arrayList2.add(Long.valueOf(A09.getLong(A09.getColumnIndexOrThrow("message_row_id"))));
                        }
                        A09.close();
                    }
                    A01.close();
                    ArrayList arrayList3 = new ArrayList();
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        C15650ng r38 = r62.A09;
                        AbstractC15340mz A002 = r38.A0K.A00(((Number) it.next()).longValue());
                        if (A002 != null) {
                            arrayList3.add(A002);
                            r38.A06(A002, 1, true);
                        }
                    }
                    if (arrayList3.size() > 0) {
                        r62.A0A.A01.post(new RunnableBRunnable0Shape7S0200000_I0_7(r62, 5, arrayList3));
                    }
                    r62.A02(r55);
                    return;
                } finally {
                    try {
                        A01.close();
                    } catch (Throwable unused4) {
                    }
                }
            case 28:
                ((C43731xV) this.A00).A02.A0G(this.A02, (Set) this.A01);
                return;
            case 29:
                VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) this.A00;
                String str21 = this.A02;
                AnonymousClass1R9 r27 = (AnonymousClass1R9) this.A01;
                if (str21.equals("sms")) {
                    verifyPhoneNumber.A0e.A02(true);
                } else if (str21.equals("flash")) {
                    verifyPhoneNumber.A34();
                }
                verifyPhoneNumber.A3j(verifyPhoneNumber.A0x, verifyPhoneNumber.A0y, r27.A08, r27.A07, r27.A01, r27.A0C);
                return;
            case C25991Bp.A0S:
                AnonymousClass1WA r28 = (AnonymousClass1WA) this.A00;
                UserJid userJid2 = (UserJid) this.A01;
                String str22 = this.A02;
                C16700pc.A0E(r28, 0);
                C16700pc.A0E(userJid2, 1);
                C16700pc.A0E(str22, 2);
                C21010wg r07 = r28.A01;
                String str23 = r28.A08;
                AnonymousClass01J r39 = r07.A00.A01;
                new AnonymousClass32J((C18790t3) r39.AJw.get(), (C14820m6) r39.AN3.get(), (C14850m9) r39.A04.get(), (AnonymousClass18L) r39.A89.get(), C18000rk.A00(r39.AMu), str23, str22, r39.A8O, r39.A8N).AZO(new AnonymousClass3XZ(userJid2, r28));
                return;
            default:
                return;
        }
    }
}
