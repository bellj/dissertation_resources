package com.facebook.redex;

import X.AbstractC116095Uc;
import X.AbstractC15710nm;
import X.AnonymousClass1V8;
import X.AnonymousClass3Dy;
import X.AnonymousClass3E0;
import X.AnonymousClass3E1;
import X.AnonymousClass3E2;
import X.AnonymousClass3E3;
import X.AnonymousClass3E4;
import X.AnonymousClass3E5;
import X.AnonymousClass3E7;
import X.AnonymousClass3EF;
import X.AnonymousClass3EH;
import X.AnonymousClass3EK;
import X.AnonymousClass3EW;
import X.AnonymousClass3HE;
import X.AnonymousClass3HF;
import X.AnonymousClass3HJ;
import X.AnonymousClass4Wy;
import X.AnonymousClass4Wz;
import X.C25991Bp;
import X.C43951xu;
import X.C63983Dt;
import X.C63993Du;
import X.C64003Dv;
import X.C64023Dx;
import X.C64033Dz;
import X.C64083Ee;
import X.C71013cF;
import X.C71023cG;
import X.C71033cH;
import X.C71043cI;
import X.C71053cJ;
import X.C71063cK;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxNFunctionShape17S0100000_2_I1 implements AbstractC116095Uc {
    public Object A00;
    public final int A01;

    public IDxNFunctionShape17S0100000_2_I1(AbstractC15710nm r1, int i) {
        this.A01 = i;
        this.A00 = r1;
    }

    @Override // X.AbstractC116095Uc
    public final Object A63(AnonymousClass1V8 r3) {
        switch (this.A01) {
            case 0:
                return new AnonymousClass3HJ((AbstractC15710nm) this.A00, r3);
            case 1:
            case 6:
            case 10:
            case 27:
                return new C64083Ee(r3);
            case 2:
                return new C64003Dv((AbstractC15710nm) this.A00, r3);
            case 3:
                return new C63993Du(r3);
            case 4:
            case 21:
                return new AnonymousClass3EW(r3);
            case 5:
                return new AnonymousClass3Dy(r3);
            case 7:
                return new AnonymousClass3EH((AbstractC15710nm) this.A00, r3);
            case 8:
                return new AnonymousClass3E0((AbstractC15710nm) this.A00, r3);
            case 9:
                return new C63983Dt((AbstractC15710nm) this.A00, r3);
            case 11:
                return new AnonymousClass3HE((AbstractC15710nm) this.A00, r3);
            case 12:
                return new AnonymousClass3E4((AbstractC15710nm) this.A00, r3);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new AnonymousClass3EF(r3);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new AnonymousClass3E2((AbstractC15710nm) this.A00, r3);
            case 15:
                return new AnonymousClass3E1((AbstractC15710nm) this.A00, r3);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new AnonymousClass3E3((AbstractC15710nm) this.A00, r3);
            case 17:
                return new C64023Dx((AbstractC15710nm) this.A00, r3);
            case 18:
                return new C64033Dz((AbstractC15710nm) this.A00, r3);
            case 19:
                return new AnonymousClass3E5(r3);
            case C43951xu.A01:
                return new AnonymousClass3EK((AbstractC15710nm) this.A00, r3);
            case 22:
                return new AnonymousClass4Wy((AbstractC15710nm) this.A00, r3);
            case 23:
                return new C71013cF((AbstractC15710nm) this.A00, r3);
            case 24:
                return new C71033cH((AbstractC15710nm) this.A00, r3);
            case 25:
                return new C71043cI((AbstractC15710nm) this.A00, r3);
            case 26:
                return new C71023cG((AbstractC15710nm) this.A00, r3);
            case 28:
                return new AnonymousClass4Wz((AbstractC15710nm) this.A00, r3);
            case 29:
            case 32:
            case 33:
                return new AnonymousClass3HF(r3);
            case C25991Bp.A0S:
                return new C71063cK((AbstractC15710nm) this.A00, r3);
            case 31:
                return new C71053cJ((AbstractC15710nm) this.A00, r3);
            case 34:
                return new AnonymousClass3E7(r3);
            default:
                return null;
        }
    }
}
