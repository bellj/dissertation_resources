package com.facebook.redex;

import X.AbstractC33111dN;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass1OU;
import X.AnonymousClass2DI;
import X.AnonymousClass394;
import X.AnonymousClass3UW;
import X.AnonymousClass4NN;
import X.C12960it;
import X.C15220ml;
import X.C36021jC;
import X.C53892fb;
import X.C627238i;
import android.app.Activity;
import android.content.DialogInterface;
import android.net.wifi.WifiManager;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.community.ManageGroupsInCommunityActivity;
import com.whatsapp.communitysuspend.CommunitySuspendDialogFragment;
import com.whatsapp.companiondevice.optin.ui.ForcedOptInActivity;
import com.whatsapp.companiondevice.optin.ui.OptInActivity;
import com.whatsapp.conversation.conversationrow.IdentityChangeDialogFragment;
import com.whatsapp.growthlock.InviteLinkUnavailableDialogFragment;
import com.whatsapp.invites.RevokeInviteDialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.BusinessHubActivity;
import com.whatsapp.phonematching.ConnectionUnavailableDialogFragment;
import com.whatsapp.status.StatusConfirmMuteDialogFragment;
import com.whatsapp.status.StatusConfirmUnmuteDialogFragment;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxCListenerShape4S0200000_2_I1 implements DialogInterface.OnClickListener {
    public Object A00;
    public Object A01;
    public final int A02;

    public IDxCListenerShape4S0200000_2_I1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj2;
        this.A01 = obj;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        C15220ml r2;
        String str;
        ActivityC000900k r0;
        C53892fb r1;
        AnonymousClass4NN r02;
        AbstractC33111dN r03;
        switch (this.A02) {
            case 0:
                Activity activity = (Activity) this.A01;
                if (((UnblockDialogFragment) this.A00).A02) {
                    activity.finish();
                    return;
                }
                return;
            case 1:
                ((ManageGroupsInCommunityActivity) this.A00).A2e((AnonymousClass1OU) this.A01);
                return;
            case 2:
                r0 = (ActivityC000900k) this.A01;
                r2 = ((CommunitySuspendDialogFragment) this.A00).A00;
                str = "community-no-longer-available";
                r2.A00(r0, str);
                return;
            case 3:
                r02 = (AnonymousClass4NN) this.A01;
                r1 = ((ForcedOptInActivity) this.A00).A05;
                r1.A04(C12960it.A1T(r02.A01));
                return;
            case 4:
                r02 = (AnonymousClass4NN) this.A01;
                r1 = ((OptInActivity) this.A00).A09;
                r1.A04(C12960it.A1T(r02.A01));
                return;
            case 5:
                AnonymousClass3UW r3 = (AnonymousClass3UW) this.A00;
                C36021jC.A00(r3.A01, 1);
                r3.A00((ArrayList) this.A01);
                return;
            case 6:
                IdentityChangeDialogFragment identityChangeDialogFragment = (IdentityChangeDialogFragment) this.A00;
                identityChangeDialogFragment.A02.Ab2(new RunnableBRunnable0Shape11S0200000_I1_1(identityChangeDialogFragment, 22, this.A01));
                return;
            case 7:
                r0 = (ActivityC000900k) this.A01;
                r2 = ((InviteLinkUnavailableDialogFragment) this.A00).A00;
                str = "invite-via-link-unavailable";
                r2.A00(r0, str);
                return;
            case 8:
                RevokeInviteDialogFragment revokeInviteDialogFragment = (RevokeInviteDialogFragment) this.A00;
                UserJid userJid = (UserJid) this.A01;
                if (i == -1 && (r03 = revokeInviteDialogFragment.A02) != null) {
                    r03.AVU(userJid);
                    return;
                }
                return;
            case 9:
                Activity activity2 = (Activity) this.A00;
                WifiManager wifiManager = (WifiManager) this.A01;
                Log.i("disable wifi radio");
                if (wifiManager != null) {
                    wifiManager.setWifiEnabled(false);
                }
                activity2.finish();
                return;
            case 10:
                BusinessHubActivity.A03((AnonymousClass2DI) this.A01, (BusinessHubActivity) this.A00);
                return;
            case 11:
                ConnectionUnavailableDialogFragment connectionUnavailableDialogFragment = (ConnectionUnavailableDialogFragment) this.A00;
                connectionUnavailableDialogFragment.A1B();
                connectionUnavailableDialogFragment.A06.Aaz(new C627238i(null, (ActivityC13810kN) this.A01, connectionUnavailableDialogFragment.A00, connectionUnavailableDialogFragment.A01, connectionUnavailableDialogFragment.A02, connectionUnavailableDialogFragment.A03, null, connectionUnavailableDialogFragment.A04, connectionUnavailableDialogFragment.A05, "", true, true, false), new String[0]);
                return;
            case 12:
                AnonymousClass394.A01((Activity) this.A01, (AnonymousClass394) this.A00);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                AnonymousClass394.A02((Activity) this.A01, (AnonymousClass394) this.A00);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                StatusConfirmMuteDialogFragment.A01((UserJid) this.A01, (StatusConfirmMuteDialogFragment) this.A00);
                return;
            case 15:
                StatusConfirmUnmuteDialogFragment.A01((UserJid) this.A01, (StatusConfirmUnmuteDialogFragment) this.A00);
                return;
            default:
                return;
        }
    }
}
