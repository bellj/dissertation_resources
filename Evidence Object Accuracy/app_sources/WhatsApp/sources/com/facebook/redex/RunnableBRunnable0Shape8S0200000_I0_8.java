package com.facebook.redex;

import X.AnonymousClass1KK;
import X.AnonymousClass1KL;
import X.C22000yK;
import android.os.Handler;
import com.whatsapp.storage.StorageUsageActivity;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape8S0200000_I0_8 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape8S0200000_I0_8(AnonymousClass1KK r2, Object obj) {
        this.A02 = 32;
        this.A01 = r2;
        this.A00 = new AnonymousClass1KL(obj);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public RunnableBRunnable0Shape8S0200000_I0_8(Handler handler, C22000yK r3) {
        this(handler, 34, r3);
        this.A02 = 34;
    }

    public RunnableBRunnable0Shape8S0200000_I0_8(StorageUsageActivity storageUsageActivity) {
        this.A02 = 19;
        this.A02 = 19;
        this.A01 = storageUsageActivity;
        this.A00 = new AtomicBoolean(false);
    }

    public RunnableBRunnable0Shape8S0200000_I0_8(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = obj2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:142:0x02f6, code lost:
        if (r1 != false) goto L_0x02f8;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 2854
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8.run():void");
    }
}
