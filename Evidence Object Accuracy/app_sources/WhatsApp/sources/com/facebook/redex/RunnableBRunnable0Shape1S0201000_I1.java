package com.facebook.redex;

import X.AbstractC116475Vp;
import X.AbstractC14640lm;
import X.AnonymousClass0PK;
import X.AnonymousClass19T;
import X.AnonymousClass285;
import X.AnonymousClass28H;
import X.AnonymousClass3VE;
import X.AnonymousClass3VF;
import X.AnonymousClass3WV;
import X.AnonymousClass4AE;
import X.AnonymousClass4T7;
import X.AnonymousClass4TA;
import X.AnonymousClass5SU;
import X.C12980iv;
import X.C13000ix;
import X.C15580nU;
import X.C16430p0;
import X.C22640zP;
import X.C28531Ny;
import X.C48122Ek;
import X.C55112ho;
import X.C58902rx;
import X.C80113rj;
import X.C92164Uu;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.google.android.gms.vision.clearcut.DynamiteClearcutLogger;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.businessdirectory.viewmodel.DirectorySetNeighborhoodViewModel;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;
import java.util.AbstractCollection;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0201000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public RunnableBRunnable0Shape1S0201000_I1(LinearLayout linearLayout, Runnable runnable) {
        this.A03 = 10;
        this.A01 = linearLayout;
        this.A00 = -1;
        this.A02 = runnable;
    }

    public RunnableBRunnable0Shape1S0201000_I1(Object obj, Object obj2, int i, int i2) {
        this.A03 = i2;
        this.A02 = obj;
        this.A00 = i;
        this.A01 = obj2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        int i;
        AnonymousClass28H r1;
        AnonymousClass19T r0;
        switch (this.A03) {
            case 0:
                int i2 = this.A00;
                AnonymousClass5SU r4 = (AnonymousClass5SU) this.A02;
                Iterator it = ((AbstractCollection) this.A01).iterator();
                while (it.hasNext()) {
                    AnonymousClass0PK r2 = (AnonymousClass0PK) it.next();
                    if (!r2.A02) {
                        if (i2 != -1) {
                            r2.A00.A00.append(i2, true);
                        }
                        r2.A01 = true;
                        r4.AJ7(r2.A03);
                    }
                }
                return;
            case 1:
                ((DynamiteClearcutLogger) this.A02).zzc.zza(this.A00, (C80113rj) this.A01);
                return;
            case 2:
                RestoreFromBackupActivity.A0A((Bundle) this.A02, ((C58902rx) this.A01).A01, this.A00);
                return;
            case 3:
                AnonymousClass4TA r42 = (AnonymousClass4TA) this.A02;
                int i3 = this.A00;
                Iterator it2 = C12980iv.A0x(((C92164Uu) this.A01).A00.A0M).iterator();
                while (it2.hasNext()) {
                    ((AnonymousClass285) it2.next()).AQP(r42.A03, i3);
                }
                return;
            case 4:
                r1 = (AnonymousClass28H) this.A02;
                i = this.A00;
                r0 = ((AnonymousClass3VE) this.A01).A00;
                break;
            case 5:
                r1 = (AnonymousClass28H) this.A02;
                i = this.A00;
                r0 = ((AnonymousClass3VF) this.A01).A00;
                break;
            case 6:
                DirectorySetNeighborhoodViewModel directorySetNeighborhoodViewModel = (DirectorySetNeighborhoodViewModel) this.A01;
                AnonymousClass4T7 r5 = (AnonymousClass4T7) this.A02;
                int i4 = this.A00;
                try {
                    directorySetNeighborhoodViewModel.A04.A01(new C48122Ek(Double.valueOf(r5.A03), Double.valueOf(r5.A01), Double.valueOf(r5.A02), null, null, null, r5.A04, "manual"));
                    directorySetNeighborhoodViewModel.A01.A0A(AnonymousClass4AE.FINISH_WITH_LOCATION_UPDATE);
                    C16430p0 r52 = directorySetNeighborhoodViewModel.A02;
                    Integer A01 = directorySetNeighborhoodViewModel.A05.A01();
                    C28531Ny r12 = new C28531Ny();
                    C13000ix.A06(r12, 10);
                    r12.A0P = Long.valueOf((long) i4);
                    r12.A03 = A01;
                    r52.A03(r12);
                    return;
                } catch (Exception e) {
                    Log.e("DirectorySetLocationViewModel/updateNeighbourhoodListItems Failed to store the search location", e);
                    return;
                }
            case 7:
                AnonymousClass3WV r3 = (AnonymousClass3WV) this.A01;
                int i5 = this.A00;
                AbstractC14640lm r13 = (AbstractC14640lm) this.A02;
                if (i5 == 3) {
                    C22640zP r22 = r3.A02;
                    C15580nU A00 = r22.A0D.A00((C15580nU) r13);
                    if (A00 != null) {
                        r22.A04(A00, 1);
                        return;
                    }
                    return;
                } else if (i5 == 4) {
                    r3.A02.A02(2);
                    return;
                } else {
                    return;
                }
            case 8:
                if (this.A00 > 0) {
                    obj = this.A01;
                } else {
                    obj = this.A02;
                }
                ((Runnable) obj).run();
                return;
            case 9:
                int i6 = this.A00;
                ((C55112ho) this.A01).A07.A04((GroupJid) this.A02, i6);
                return;
            case 10:
                ((View) this.A01).setLayoutParams(new FrameLayout.LayoutParams(this.A00, -1));
                ((Runnable) this.A02).run();
                return;
            default:
                return;
        }
        AbstractC116475Vp r02 = (AbstractC116475Vp) r0.A0O.remove(r1);
        if (r02 == null) {
            Log.e("The response handler must not be null");
        } else {
            r02.AQN(r1, i);
        }
    }
}
