package com.facebook.redex;

import X.AbstractActivityC33001d7;
import X.AbstractC16350or;
import X.AbstractC20610w2;
import X.AbstractC58392on;
import X.ActivityC13790kL;
import X.AnonymousClass028;
import X.AnonymousClass14X;
import X.AnonymousClass2BN;
import X.AnonymousClass2KO;
import X.AnonymousClass2KP;
import X.C006202y;
import X.C14820m6;
import X.C14830m7;
import X.C20620w3;
import X.C41121sw;
import X.C44101yE;
import X.C44241ya;
import X.C49222Ju;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.ListItemWithLeftIcon;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.migration.export.ui.ExportMigrationActivity;
import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0100100_I0 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape0S0100100_I0(Object obj, long j, int i) {
        this.A02 = i;
        this.A01 = obj;
        this.A00 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C14820m6 r3;
        long j;
        long j2;
        switch (this.A02) {
            case 0:
                AnonymousClass2BN r4 = (AnonymousClass2BN) this.A01;
                long j3 = this.A00;
                AbstractActivityC33001d7 r1 = (AbstractActivityC33001d7) r4.A0A.get();
                if (r1 != null && !((AbstractC16350or) r4).A02.isCancelled()) {
                    r1.A2o(j3);
                    return;
                }
                return;
            case 1:
                AnonymousClass2BN r32 = (AnonymousClass2BN) this.A01;
                long j4 = this.A00;
                AbstractActivityC33001d7 r6 = (AbstractActivityC33001d7) r32.A0A.get();
                if (r6 != null && !((AbstractC16350or) r32).A02.isCancelled()) {
                    View findViewById = r6.findViewById(R.id.payment_transactions_layout);
                    View findViewById2 = r6.findViewById(R.id.payment_transactions_separator);
                    if (j4 != 0) {
                        ((ActivityC13790kL) r6).A01.A08();
                        findViewById.setVisibility(0);
                        if (findViewById2 != null) {
                            findViewById2.setVisibility(0);
                        }
                        TextView textView = (TextView) r6.findViewById(R.id.payment_transactions_count);
                        if (findViewById instanceof ListItemWithLeftIcon) {
                            AbstractC58392on r5 = (AbstractC58392on) findViewById;
                            if (textView == null) {
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                                textView = new WaTextView(r6);
                                textView.setId(R.id.payment_transactions_count);
                                textView.setLayoutParams(layoutParams);
                                ((ViewGroup) AnonymousClass028.A0D(r5, R.id.right_view_container)).addView(textView);
                            }
                            r5.setIcon(AnonymousClass14X.A00(r6.A0I.A01()));
                        } else if (textView == null) {
                            return;
                        }
                        textView.setText(r6.A08.A0J().format(j4));
                        return;
                    }
                    findViewById.setVisibility(8);
                    if (findViewById2 != null) {
                        findViewById2.setVisibility(8);
                        return;
                    }
                    return;
                }
                return;
            case 2:
                AnonymousClass2BN r42 = (AnonymousClass2BN) this.A01;
                long j5 = this.A00;
                AbstractActivityC33001d7 r12 = (AbstractActivityC33001d7) r42.A0A.get();
                if (r12 != null && !((AbstractC16350or) r42).A02.isCancelled()) {
                    r12.A00 = j5;
                    View findViewById3 = r12.findViewById(R.id.kept_messages_layout);
                    View findViewById4 = r12.findViewById(R.id.kept_messages_separator);
                    findViewById3.setVisibility(8);
                    if (findViewById4 != null) {
                        findViewById4.setVisibility(8);
                        return;
                    }
                    return;
                }
                return;
            case 3:
                C49222Ju r13 = (C49222Ju) this.A01;
                long j6 = this.A00;
                if (r13.A02) {
                    AnonymousClass2KO r8 = r13.A06;
                    AnonymousClass2KP r9 = r13.A00;
                    StringBuilder sb = new StringBuilder("CompanionDeviceQrHandler/onRetry retryTs=");
                    sb.append(j6);
                    Log.i(sb.toString());
                    C41121sw r52 = r8.A00;
                    C49222Ju r14 = r52.A01;
                    if (r14 != null) {
                        r14.A02 = false;
                    }
                    C14830m7 r33 = r52.A04.A00;
                    if (r33.A01 != 0) {
                        j2 = r33.A01 + SystemClock.elapsedRealtime();
                    } else {
                        j2 = 0;
                    }
                    long j7 = j2 / 1000;
                    if (j7 == 0) {
                        j7 = System.currentTimeMillis() / 1000;
                    }
                    if (j6 <= 86400 + j7) {
                        r52.A00();
                        r52.A0H.Ab2(new RunnableBRunnable0Shape0S0200100_I0(r8, r9, 1, j6));
                        return;
                    }
                    StringBuilder sb2 = new StringBuilder("CompanionDeviceAdvUtil/isRetryTimestampValid retryTs=");
                    sb2.append(j6);
                    sb2.append("; ntpTs=");
                    sb2.append(j7);
                    Log.e(sb2.toString());
                    Log.e("CompanionDeviceQrHandler/onRetry invalid local ts");
                    r52.A0F.ARS();
                    return;
                }
                return;
            case 4:
                ((AbstractC20610w2) this.A01).A00.A07(Long.valueOf(this.A00));
                return;
            case 5:
                long j8 = this.A00;
                C006202y r15 = ((C20620w3) this.A01).A00;
                synchronized (r15) {
                    r15.A07(Long.valueOf(j8));
                }
                return;
            case 6:
                ExportMigrationActivity exportMigrationActivity = (ExportMigrationActivity) this.A01;
                long j9 = this.A00;
                exportMigrationActivity.A0E.A0C(exportMigrationActivity.A0L, 9);
                exportMigrationActivity.A2f(j9);
                return;
            case 7:
                j = this.A00;
                r3 = ((C44101yE) this.A01).A00.A00.A04;
                r3.A0O(1);
                break;
            case 8:
                j = this.A00;
                BusinessActivityReportViewModel businessActivityReportViewModel = ((C44241ya) this.A01).A00.A00;
                businessActivityReportViewModel.A01.A0B(0);
                businessActivityReportViewModel.A02.A0B(1);
                r3 = businessActivityReportViewModel.A04;
                r3.A0O(1);
                break;
            default:
                return;
        }
        r3.A0q("business_activity_report_timestamp", j);
    }
}
