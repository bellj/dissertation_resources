package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AnonymousClass13L;
import X.AnonymousClass13U;
import X.AnonymousClass1JV;
import X.AnonymousClass1RM;
import X.AnonymousClass1RW;
import X.AnonymousClass2D2;
import X.AnonymousClass3ZP;
import X.C14830m7;
import X.C15380n4;
import X.C15990oG;
import X.C16050oM;
import X.C16210od;
import X.C16310on;
import X.C20710wC;
import X.C21820y2;
import X.C22920zr;
import X.C29211Rh;
import X.C617532b;
import X.C63323Bd;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.SystemClock;
import android.util.SparseArray;
import com.whatsapp.jid.Jid;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.locks.Lock;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0501000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public final int A06;

    public RunnableBRunnable0Shape0S0501000_I0(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i, int i2) {
        this.A06 = i2;
        this.A02 = obj;
        this.A05 = obj2;
        this.A03 = obj3;
        this.A01 = obj4;
        this.A00 = i;
        this.A04 = obj5;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C29211Rh[] r6;
        boolean z;
        AnonymousClass13L r2;
        int i;
        switch (this.A06) {
            case 0:
                C20710wC r62 = (C20710wC) this.A01;
                AnonymousClass1JV r15 = (AnonymousClass1JV) this.A02;
                AbstractC15340mz r3 = (AbstractC15340mz) this.A03;
                int i2 = this.A00;
                SystemClock.sleep(300);
                StringBuilder sb = new StringBuilder("group/create again, jid:");
                sb.append(r15);
                sb.append(" subject:");
                sb.append(r3.A0I());
                Log.w(sb.toString());
                C63323Bd r8 = new C63323Bd(null, r15, null, r3.A0I(), null, r3.A0R(), i2, false);
                C14830m7 r32 = r62.A0I;
                C617532b r22 = new C617532b(r32, r62.A0Q, r62.A0S, r62, r62, r8, r62.A0s, r62.A0w, (File) this.A04, (File) this.A05);
                new AnonymousClass3ZP(r62.A04, r62.A06, r32, r62.A0c, r62, r22, r22.A04, r62.A0l).A00();
                return;
            case 1:
                AnonymousClass13U r33 = (AnonymousClass13U) this.A01;
                byte b = (byte) this.A00;
                byte[] bArr = (byte[]) this.A02;
                byte[] bArr2 = (byte[]) this.A03;
                byte[][] bArr3 = (byte[][]) this.A04;
                byte[] bArr4 = (byte[]) this.A05;
                if (b == 5) {
                    int A00 = C16050oM.A00(bArr);
                    C15990oG r11 = r33.A07;
                    if (A00 == r11.A07.A01()) {
                        byte[] A0k = r11.A0k();
                        C29211Rh A0M = r11.A0M();
                        if (Arrays.equals(A0M.A01, bArr2)) {
                            int length = bArr3.length;
                            int[] iArr = new int[length];
                            for (int i3 = 0; i3 < length; i3++) {
                                iArr[i3] = C16050oM.A01(bArr3[i3]);
                            }
                            Lock A002 = r11.A0J.A00(r11.A0I());
                            try {
                                if (A002 == null) {
                                    r11.A0I.A00();
                                } else {
                                    A002.lock();
                                }
                                ArrayList arrayList = new ArrayList(length);
                                SparseArray sparseArray = new SparseArray(length);
                                AnonymousClass1RM r0 = r11.A09;
                                ArrayList arrayList2 = new ArrayList();
                                C16310on A01 = r0.A02.get();
                                Cursor A08 = A01.A03.A08("prekeys", null, null, null, new String[]{"prekey_id", "record"}, null);
                                while (A08.moveToNext()) {
                                    arrayList2.add(new AnonymousClass1RW(A08.getInt(0), A08.getBlob(1)));
                                }
                                A08.close();
                                A01.close();
                                Iterator it = arrayList2.iterator();
                                while (it.hasNext()) {
                                    AnonymousClass1RW r12 = (AnonymousClass1RW) it.next();
                                    try {
                                        int i4 = r12.A00;
                                        sparseArray.put(i4, C15990oG.A01(r12.A01, i4));
                                    } catch (IOException e) {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("error reading prekey ");
                                        sb2.append(r12.A00);
                                        Log.e(sb2.toString(), e);
                                    }
                                }
                                int i5 = 0;
                                while (true) {
                                    if (i5 >= length) {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("axolotl reporting back ");
                                        sb3.append(arrayList.size());
                                        sb3.append(" sequenced prekeys");
                                        Log.i(sb3.toString());
                                        r6 = (C29211Rh[]) arrayList.toArray(new C29211Rh[0]);
                                    } else {
                                        C29211Rh r02 = (C29211Rh) sparseArray.get(iArr[i5]);
                                        if (r02 == null) {
                                            r6 = null;
                                        } else {
                                            arrayList.add(r02);
                                            i5++;
                                        }
                                    }
                                }
                                if (r6 != null && (r5 = r6.length) == length) {
                                    try {
                                        MessageDigest instance = MessageDigest.getInstance("SHA1");
                                        instance.update(A0k);
                                        instance.update(A0M.A00);
                                        instance.update(A0M.A02);
                                        for (C29211Rh r03 : r6) {
                                            instance.update(r03.A00);
                                        }
                                        if (!Arrays.equals(instance.digest(), bArr4)) {
                                            Log.w("prekey digest check failed");
                                        } else {
                                            Log.i("prekey digest check passed");
                                            r33.A06.A1A(false);
                                            return;
                                        }
                                    } catch (NoSuchAlgorithmException e2) {
                                        Log.w("prekey digest SHA1 algorithm unknown", e2);
                                        C22920zr r1 = r33.A02;
                                        r1.A0B.A0T();
                                        r1.A02();
                                        r33.A06.A1A(false);
                                        return;
                                    }
                                }
                            } finally {
                                if (A002 != null) {
                                    A002.unlock();
                                }
                            }
                        }
                    }
                }
                C22920zr r13 = r33.A02;
                r13.A0B.A0T();
                r13.A02();
                r33.A06.A1A(false);
                return;
            case 2:
                C16210od r34 = (C16210od) this.A01;
                if (!r34.A00 || ((C21820y2) this.A05).A00) {
                    Log.i("messagenotification/popupnotification/background");
                    if ((!((C21820y2) this.A05).A00 || !((i = this.A00) == 2 || i == 3)) && (r34.A00 || this.A00 == 3)) {
                        z = false;
                    } else {
                        z = true;
                    }
                    r2 = (AnonymousClass13L) this.A03;
                    AnonymousClass2D2 r04 = r2.A00;
                    if (r04 != null) {
                        PopupNotification popupNotification = r04.A00;
                        if (popupNotification.A1O) {
                            popupNotification.A2b((AbstractC14640lm) this.A04);
                        }
                    }
                    if (z) {
                        Context context = (Context) this.A02;
                        Intent intent = new Intent();
                        intent.setClassName(context.getPackageName(), "com.whatsapp.notification.PopupNotification");
                        intent.putExtra("popup_notification_extra_quick_reply_jid", C15380n4.A03((Jid) this.A04));
                        intent.putExtra("popup_notification_extra_dismiss_notification", false);
                        intent.setFlags(268697600);
                        context.startActivity(intent);
                        return;
                    }
                    return;
                }
                r2 = (AnonymousClass13L) this.A03;
                AnonymousClass2D2 r05 = r2.A00;
                if (r05 != null && r05.A00.A1O) {
                    Log.i("messagenotification/popupnotification/foreground");
                    AbstractC14640lm r14 = (AbstractC14640lm) this.A04;
                    AnonymousClass2D2 r06 = r2.A00;
                    if (r06 != null) {
                        r06.A00.A2b(r14);
                    }
                } else {
                    return;
                }
                AnonymousClass2D2 r07 = r2.A00;
                if (r07 != null) {
                    r07.A00.A2T();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
