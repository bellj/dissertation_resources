package com.facebook.redex;

import X.AnonymousClass02N;
import X.C91024Qd;
import X.C92474Wb;
import X.C92614Wq;
import android.os.Handler;
import android.os.Looper;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0100100_I1 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape1S0100100_I1(Object obj, long j, int i) {
        this.A02 = i;
        this.A01 = obj;
        this.A00 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A02) {
            case 0:
                ((C92474Wb) this.A01).A01.AMX(this.A00);
                return;
            case 1:
                C92614Wq r3 = (C92614Wq) this.A01;
                long j = this.A00;
                synchronized (r3) {
                    Runtime.getRuntime().gc();
                    r3.A01(j);
                }
                return;
            case 2:
                C91024Qd r2 = (C91024Qd) this.A01;
                long j2 = this.A00;
                synchronized (r2) {
                    r2.A00 = j2;
                }
                return;
            case 3:
                AnonymousClass02N r5 = (AnonymousClass02N) this.A01;
                long j3 = this.A00;
                if (Looper.myLooper() != null) {
                    new Handler(Looper.myLooper()).postDelayed(new RunnableBRunnable0Shape16S0100000_I1_2(r5, 16), j3);
                    return;
                }
                try {
                    Thread.sleep(j3);
                } catch (InterruptedException e) {
                    Log.e(e);
                    Thread.currentThread().interrupt();
                }
                r5.A01();
                return;
            default:
                return;
        }
    }
}
