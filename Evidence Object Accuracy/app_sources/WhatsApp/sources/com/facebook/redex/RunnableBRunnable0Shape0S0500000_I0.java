package com.facebook.redex;

import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC41521tf;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass1BI;
import X.AnonymousClass1JS;
import X.AnonymousClass1M9;
import X.AnonymousClass1MA;
import X.AnonymousClass1MB;
import X.AnonymousClass1RV;
import X.AnonymousClass1RX;
import X.AnonymousClass21U;
import X.AnonymousClass21W;
import X.AnonymousClass28E;
import X.AnonymousClass28F;
import X.AnonymousClass4QB;
import X.C14820m6;
import X.C15950oC;
import X.C15990oG;
import X.C18240s8;
import X.C28181Ma;
import X.C33601ee;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.ConditionVariable;
import android.view.View;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.filter.SmoothScrollLinearLayoutManager;
import com.whatsapp.storage.StorageUsageActivity;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0500000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public final int A05;

    public RunnableBRunnable0Shape0S0500000_I0(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i) {
        this.A05 = i;
        this.A01 = obj;
        this.A03 = obj2;
        this.A02 = obj3;
        this.A04 = obj4;
        this.A00 = obj5;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Runnable
    public void run() {
        boolean z;
        byte[] bArr;
        switch (this.A05) {
            case 0:
                if (!((AnonymousClass28E) this.A04).A05) {
                    ImageView imageView = (ImageView) this.A01;
                    if (imageView.getTag() != null && imageView.getTag().equals(this.A03)) {
                        ((AnonymousClass28F) this.A02).Adh((Bitmap) this.A00, imageView, false);
                        return;
                    }
                    return;
                }
                return;
            case 1:
                try {
                    Log.i("SessionVerifier/verify/begin");
                    SharedPreferences sharedPreferences = ((C14820m6) this.A04).A00;
                    int i = sharedPreferences.getInt("session_verification_status", 0);
                    StringBuilder sb = new StringBuilder();
                    sb.append("SessionVerifier/verify/status=");
                    sb.append(i);
                    Log.i(sb.toString());
                    if (i != 1) {
                        C28181Ma r22 = new C28181Ma("SessionVerifier/verify");
                        r22.A03();
                        do {
                            try {
                                C28181Ma r21 = new C28181Ma("SessionVerifier/verify/processBatch");
                                r21.A03();
                                try {
                                    int i2 = sharedPreferences.getInt("last_read_session_row_id", 0);
                                    C15990oG r0 = (C15990oG) this.A02;
                                    ArrayList arrayList = new ArrayList(100);
                                    AnonymousClass1RX r02 = r0.A0B;
                                    AnonymousClass009.A0E(true);
                                    Cursor A09 = r02.A01.AHr().A09("SELECT _id, record, recipient_id, recipient_type, device_id FROM sessions WHERE _id > ?  ORDER BY _id ASC  LIMIT ? ", new String[]{String.valueOf(i2), String.valueOf(100)});
                                    int columnIndex = A09.getColumnIndex("_id");
                                    int columnIndex2 = A09.getColumnIndex("record");
                                    int columnIndex3 = A09.getColumnIndex("recipient_id");
                                    int columnIndex4 = A09.getColumnIndex("device_id");
                                    while (A09.moveToNext()) {
                                        int i3 = A09.getInt(columnIndex);
                                        byte[] blob = A09.getBlob(columnIndex2);
                                        C15950oC r6 = new C15950oC(0, String.valueOf(A09.getLong(columnIndex3)), A09.getInt(columnIndex4));
                                        try {
                                            AnonymousClass1RV r03 = new AnonymousClass1RV(blob);
                                            C15990oG.A05(r03);
                                            arrayList.add(new AnonymousClass4QB(r03, r6, i3));
                                        } catch (IOException unused) {
                                            r0.A0H(r6);
                                        }
                                    }
                                    A09.close();
                                    HashSet hashSet = new HashSet(arrayList.size());
                                    Iterator it = arrayList.iterator();
                                    while (it.hasNext()) {
                                        hashSet.add(((AnonymousClass4QB) it.next()).A02);
                                    }
                                    Map A0P = r0.A0P(hashSet);
                                    HashSet hashSet2 = new HashSet();
                                    Iterator it2 = arrayList.iterator();
                                    while (it2.hasNext()) {
                                        AnonymousClass4QB r10 = (AnonymousClass4QB) it2.next();
                                        byte[] A04 = r10.A01.A01.A00.A07.A04();
                                        C15950oC r12 = r10.A02;
                                        AnonymousClass1JS r04 = (AnonymousClass1JS) A0P.get(r12);
                                        if (r04 == null) {
                                            bArr = null;
                                        } else {
                                            bArr = r04.A00.A00();
                                        }
                                        if (!Arrays.equals(A04, bArr)) {
                                            hashSet2.add(r10);
                                            StringBuilder sb2 = new StringBuilder("SessionVerifier/verifyInSmallBatch/bad session: ");
                                            sb2.append(r12);
                                            Log.i(sb2.toString());
                                        }
                                        i2 = r10.A00;
                                    }
                                    z = false;
                                    if (!hashSet2.isEmpty()) {
                                        ((C18240s8) this.A03).A00.submit(new RunnableBRunnable0Shape11S0200000_I1_1(this, 24, hashSet2)).get();
                                        ((AbstractC15710nm) this.A01).AaV("session-verifier-delete-sessions", String.valueOf(hashSet2.size()), false);
                                    }
                                    sharedPreferences.edit().putInt("last_read_session_row_id", i2).apply();
                                    if (arrayList.size() == 100) {
                                        z = true;
                                    }
                                    r21.A01();
                                } catch (Throwable th) {
                                    r21.A01();
                                    throw th;
                                }
                            } catch (Exception e) {
                                Log.e("SessionVerifier/verify/error", e);
                                ((AbstractC15710nm) this.A01).AaV("session-verifier-failed", e.getMessage(), false);
                            }
                        } while (z);
                        sharedPreferences.edit().putInt("session_verification_status", 1).apply();
                        r22.A01();
                        ((AbstractC15710nm) this.A01).AaV("session-verifier-finish", null, false);
                        Log.i("SessionVerifier/verify/end");
                    }
                    return;
                } finally {
                    ((ConditionVariable) this.A00).open();
                }
            case 2:
                AnonymousClass21U r5 = (AnonymousClass21U) this.A00;
                Context context = (Context) this.A01;
                AnonymousClass1BI r3 = (AnonymousClass1BI) this.A02;
                AnonymousClass018 r1 = (AnonymousClass018) this.A03;
                View view = (View) this.A04;
                if (r5.A0A == null) {
                    AnonymousClass21W r05 = new AnonymousClass21W(context, r1, r3, r5);
                    r5.A0A = r05;
                    RecyclerView recyclerView = r5.A0Q;
                    recyclerView.setAdapter(r05);
                    view.getContext();
                    recyclerView.setLayoutManager(new SmoothScrollLinearLayoutManager());
                    return;
                }
                return;
            case 3:
                StorageUsageActivity storageUsageActivity = (StorageUsageActivity) this.A00;
                AnonymousClass1MB r62 = (AnonymousClass1MB) this.A01;
                AnonymousClass1M9 r52 = (AnonymousClass1M9) this.A02;
                AnonymousClass1M9 r4 = (AnonymousClass1M9) this.A03;
                C33601ee r2 = (C33601ee) this.A04;
                AnonymousClass1MA r13 = storageUsageActivity.A0F;
                if (r62 != null && r13.A02 == null) {
                    r13.A02 = r62;
                    r13.A03(r13.A0G(1));
                }
                if (r52 != null && r13.A01 == null) {
                    r13.A01 = r52;
                    r13.A0H();
                }
                if (r4 != null && r13.A00 == null) {
                    r13.A00 = r4;
                    r13.A0H();
                }
                if (r2.A04 != null) {
                    Log.i("storage-usage-activity/fetch cache/fetched media size");
                    storageUsageActivity.A2f(0);
                }
                if (r52 != null) {
                    Log.i("storage-usage-activity/fetch cache/fetched large files");
                    storageUsageActivity.A2f(1);
                }
                if (r4 != null) {
                    Log.i("storage-usage-activity/fetch cache/fetched forwarded files");
                    storageUsageActivity.A2f(2);
                    return;
                }
                return;
            case 4:
                Object obj = this.A00;
                View view2 = (View) this.A01;
                AbstractC41521tf r42 = (AbstractC41521tf) this.A02;
                Bitmap bitmap = (Bitmap) this.A03;
                AbstractC15340mz r14 = (AbstractC15340mz) this.A04;
                if (obj.equals(view2.getTag())) {
                    r42.Adg(bitmap, view2, r14);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
