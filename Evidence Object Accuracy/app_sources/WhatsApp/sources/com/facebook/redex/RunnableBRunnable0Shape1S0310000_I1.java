package com.facebook.redex;

import X.AbstractC42721vi;
import X.AnonymousClass18P;
import X.AnonymousClass1IR;
import X.AnonymousClass1J7;
import X.AnonymousClass3BT;
import X.AnonymousClass3DE;
import X.AnonymousClass3XV;
import X.AnonymousClass4OA;
import X.C16700pc;
import X.C72233eE;
import X.C72303eL;
import android.widget.TextView;
import com.whatsapp.R;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0310000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public boolean A03;
    public final int A04;

    public RunnableBRunnable0Shape1S0310000_I1(Object obj, Object obj2, Object obj3, int i, boolean z) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A04) {
            case 0:
                boolean z = this.A03;
                AnonymousClass18P r0 = ((AnonymousClass3DE) this.A00).A04;
                r0.A05((TextView) this.A01, (AnonymousClass1IR) this.A02, false, z);
                r0.A00.A07(R.string.payments_request_canceling_failure_message, 0);
                return;
            case 1:
                AnonymousClass3BT r7 = (AnonymousClass3BT) this.A00;
                boolean z2 = this.A03;
                AnonymousClass1J7 r5 = (AnonymousClass1J7) this.A01;
                C16700pc.A0E(r7, 0);
                AnonymousClass4OA r2 = r7.A02;
                r2.A01.A7z(new AbstractC42721vi() { // from class: X.3Yr
                    @Override // X.AbstractC42721vi
                    public final AnonymousClass19W A7y(String str, String str2) {
                        C44341yl r22 = AnonymousClass4OA.this.A00.A00;
                        AnonymousClass01J r1 = r22.A03;
                        C14850m9 A0S = C12960it.A0S(r1);
                        C18790t3 A0U = C12990iw.A0U(r1);
                        C14820m6 A0Z = C12970iu.A0Z(r1);
                        AnonymousClass01H A00 = C18000rk.A00(r1.AMu);
                        C44351ym r02 = r22.A02;
                        return new AnonymousClass32D(A0U, A0Z, A0S, (AnonymousClass18L) r1.A89.get(), A00, str, r02.A05, r02.A04);
                    }
                }).AZO(new AnonymousClass3XV(new C72233eE(r7, r5), new C72303eL(r7, r5, (AnonymousClass1J7) this.A02, z2)));
                return;
            default:
                return;
        }
    }
}
