package com.facebook.redex;

import X.AbstractActivityC41101su;
import X.AbstractC14740ly;
import X.AbstractC21180x0;
import X.AbstractC33621eg;
import X.AbstractC33631eh;
import X.ActivityC13830kP;
import X.ActivityC44201yU;
import X.AnonymousClass018;
import X.AnonymousClass02N;
import X.AnonymousClass1In;
import X.AnonymousClass1J7;
import X.AnonymousClass1US;
import X.AnonymousClass2G6;
import X.AnonymousClass2G8;
import X.AnonymousClass2G9;
import X.C12960it;
import X.C12980iv;
import X.C14330lG;
import X.C14860mA;
import X.C16700pc;
import X.C36021jC;
import X.C44891zj;
import X.C48232Fc;
import X.C48242Fd;
import X.C48252Fe;
import X.C63473Bs;
import android.app.Activity;
import android.graphics.Rect;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.CheckBox;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.Statistics$Data;
import com.whatsapp.WaPreferenceFragment;
import com.whatsapp.search.SearchFragment;
import com.whatsapp.settings.SettingsDataUsageActivity;
import com.whatsapp.settings.SettingsNetworkUsage;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.storage.StorageUsageActivity;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape16S0100000_I1_2 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape16S0100000_I1_2(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass1J7 r1;
        String str;
        switch (this.A01) {
            case 0:
                ((AnonymousClass1In) this.A00).ATd();
                return;
            case 1:
                C63473Bs r0 = (C63473Bs) this.A00;
                Rect A0J = C12980iv.A0J();
                CheckBox checkBox = r0.A04;
                checkBox.getHitRect(A0J);
                int i = A0J.right;
                View view = r0.A03;
                A0J.right = i + view.getWidth();
                view.setTouchDelegate(new TouchDelegate(A0J, checkBox));
                return;
            case 2:
                r1 = (AnonymousClass1J7) this.A00;
                C16700pc.A0E(r1, 0);
                str = "Profile image is not received.";
                break;
            case 3:
                r1 = (AnonymousClass1J7) this.A00;
                C16700pc.A0E(r1, 0);
                str = "Empty Poses list received.";
                break;
            case 4:
                r1 = (AnonymousClass1J7) this.A00;
                C16700pc.A0E(r1, 0);
                str = "Empty Sticker list received.";
                break;
            case 5:
                ((AbstractActivityC41101su) this.A00).A03.Aab();
                return;
            case 6:
                ((AbstractC21180x0) this.A00).AgE();
                return;
            case 7:
                Log.i("AccountDefenceVerificationHelper/learn-more tapped");
                C36021jC.A01((Activity) this.A00, 604);
                return;
            case 8:
                SearchFragment searchFragment = (SearchFragment) this.A00;
                AbstractC14740ly r2 = (AbstractC14740ly) searchFragment.A0B();
                searchFragment.A1G.A0P(2);
                if (r2 != null && !r2.isFinishing()) {
                    HomeActivity homeActivity = (HomeActivity) r2;
                    C48232Fc.A00(homeActivity.A0F);
                    homeActivity.A08.setVisibility(4);
                    return;
                }
                return;
            case 9:
                C14330lG r22 = (C14330lG) this.A00;
                r22.A0Q(r22.A04().A01);
                r22.A0Q(r22.A04().A00);
                r22.A0Q(r22.A04().A0N);
                r22.A0Q(r22.A04().A05);
                File file = r22.A04().A02;
                C14330lG.A03(file, false);
                r22.A0Q(file);
                File file2 = r22.A04().A0A;
                C14330lG.A03(file2, false);
                r22.A0Q(file2);
                return;
            case 10:
                ActivityC44201yU r02 = ((WaPreferenceFragment) this.A00).A00;
                if (r02 != null) {
                    r02.AaN();
                    return;
                }
                return;
            case 11:
                SettingsDataUsageActivity settingsDataUsageActivity = (SettingsDataUsageActivity) this.A00;
                Statistics$Data A00 = settingsDataUsageActivity.A0G.A00();
                AnonymousClass018 r7 = ((ActivityC13830kP) settingsDataUsageActivity).A01;
                String A0X = C12960it.A0X(settingsDataUsageActivity, r7.A0F(C44891zj.A04(r7, A00.A0E + A00.A0G + A00.A0M + A00.A0D + A00.A0J)), new Object[1], 0, R.string.settings_data_network_usage_amount_sent);
                AnonymousClass018 r4 = ((ActivityC13830kP) settingsDataUsageActivity).A01;
                settingsDataUsageActivity.A06.setText(AnonymousClass1US.A06(((ActivityC13830kP) settingsDataUsageActivity).A01, A0X, C12960it.A0X(settingsDataUsageActivity, r4.A0F(C44891zj.A04(r4, A00.A01 + A00.A03 + A00.A0B + A00.A00 + A00.A08)), new Object[1], 0, R.string.settings_data_network_usage_amount_received)));
                return;
            case 12:
                ((SettingsNetworkUsage) this.A00).A2f(false);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                AbstractC33621eg r12 = ((C48242Fd) this.A00).A00;
                if (((AbstractC33631eh) r12).A03) {
                    r12.A05 = false;
                    C48252Fe r3 = r12.A0L;
                    StringBuilder A0k = C12960it.A0k("playbackFragment/onPlaybackFinished ");
                    A0k.append(r3.A01);
                    C12960it.A1F(A0k);
                    StatusPlaybackContactFragment.A02(r3.A02, 4, 6);
                    return;
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                ((StorageUsageActivity) ((RunnableBRunnable0Shape8S0200000_I0_8) this.A00).A01).A2f(3);
                return;
            case 15:
                AnonymousClass2G6 r23 = (AnonymousClass2G6) this.A00;
                r23.A03.setSelected(true);
                r23.A01 = null;
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((AnonymousClass02N) this.A00).A01();
                return;
            case 17:
                ((AnonymousClass2G8) this.A00).A00();
                return;
            case 18:
                ((AnonymousClass2G9) this.A00).A0C();
                return;
            case 19:
                ((C14860mA) this.A00).A0D();
                return;
            default:
                return;
        }
        r1.AJ4(C12960it.A0U(str));
    }
}
