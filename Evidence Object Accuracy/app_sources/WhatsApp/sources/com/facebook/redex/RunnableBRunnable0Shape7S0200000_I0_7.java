package com.facebook.redex;

import X.AbstractActivityC41101su;
import X.AbstractC13860kS;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC16350or;
import X.AbstractC18870tC;
import X.AbstractC22250ym;
import X.AbstractC49392Ko;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass10R;
import X.AnonymousClass10W;
import X.AnonymousClass10X;
import X.AnonymousClass13Z;
import X.AnonymousClass1FM;
import X.AnonymousClass1FN;
import X.AnonymousClass1FV;
import X.AnonymousClass1IS;
import X.AnonymousClass1JC;
import X.AnonymousClass1JD;
import X.AnonymousClass1JE;
import X.AnonymousClass1JF;
import X.AnonymousClass1JG;
import X.AnonymousClass1JM;
import X.AnonymousClass1JQ;
import X.AnonymousClass1KS;
import X.AnonymousClass1OT;
import X.AnonymousClass1R5;
import X.AnonymousClass1UY;
import X.AnonymousClass1V8;
import X.AnonymousClass1VY;
import X.AnonymousClass1W9;
import X.AnonymousClass23M;
import X.AnonymousClass27M;
import X.AnonymousClass2DM;
import X.AnonymousClass2KL;
import X.AnonymousClass2KT;
import X.AnonymousClass2KV;
import X.AnonymousClass2KX;
import X.AnonymousClass2KZ;
import X.C005602s;
import X.C14310lE;
import X.C14320lF;
import X.C14820m6;
import X.C14900mE;
import X.C14960mK;
import X.C15370n3;
import X.C15380n4;
import X.C15570nT;
import X.C15580nU;
import X.C15860o1;
import X.C15990oG;
import X.C17220qS;
import X.C18350sJ;
import X.C18360sK;
import X.C18850tA;
import X.C18910tG;
import X.C20710wC;
import X.C22100yW;
import X.C22210yi;
import X.C22230yk;
import X.C22280yp;
import X.C22630zO;
import X.C237112s;
import X.C238413f;
import X.C239513q;
import X.C248116y;
import X.C25991Bp;
import X.C26531Dv;
import X.C26871Fd;
import X.C26881Fe;
import X.C26891Ff;
import X.C26951Fl;
import X.C28111Kr;
import X.C28391Mz;
import X.C28601Of;
import X.C28941Pp;
import X.C30331Wz;
import X.C32901cv;
import X.C33911fH;
import X.C38861op;
import X.C41091ss;
import X.C41141sy;
import X.C43951xu;
import X.C44101yE;
import X.C44281ye;
import X.C467427a;
import X.C47872Db;
import X.C47882Dc;
import X.C47902De;
import X.C48182Et;
import X.C49262Kb;
import X.C49282Kd;
import X.C49292Ke;
import X.C49302Kf;
import X.C49312Kg;
import X.C49322Kh;
import X.C49332Ki;
import X.C49342Kj;
import X.C49352Kk;
import X.C49362Kl;
import X.C49372Km;
import X.RunnableC29011Px;
import X.ServiceConnectionC49272Kc;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.ConditionVariable;
import android.os.Message;
import android.view.SurfaceHolder;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.qrcode.DevicePairQrScannerActivity;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.search.SearchFragment;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.service.RestoreChatConnectionWorker;
import com.whatsapp.shareinvitelink.ShareInviteLinkActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape7S0200000_I0_7 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape7S0200000_I0_7(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = obj2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        String str;
        AnonymousClass1JM ACX;
        AnonymousClass1JM ACX2;
        AnonymousClass1JM ACX3;
        AnonymousClass1JM ACX4;
        Runnable runnable;
        String str2;
        switch (this.A02) {
            case 0:
                ((AnonymousClass10X) this.A00).A06.A05((AbstractC14640lm) this.A01);
                return;
            case 1:
                C47872Db r3 = (C47872Db) this.A00;
                Object obj = this.A01;
                r3.A01.A0M(Collections.singleton(obj), false);
                AnonymousClass10W r32 = r3.A04;
                r32.A00(Collections.singleton(obj));
                r32.A00.post(new RunnableBRunnable0Shape2S0200000_I0_2(r32, 32, obj));
                return;
            case 2:
                AnonymousClass13Z r7 = (AnonymousClass13Z) this.A00;
                C47882Dc r6 = (C47882Dc) this.A01;
                C18850tA r5 = r7.A00;
                if (r5.A0Q()) {
                    for (String str3 : AnonymousClass1JQ.A08) {
                        r5.A0X.A04(str3, 0);
                    }
                    r5.A0B.A03(new C47902De(r5, r5.A0X.A01()));
                    r5.A0G();
                }
                r7.A03.A0H("syncd_app_state", r6.A00);
                return;
            case 3:
                AnonymousClass1FV r33 = (AnonymousClass1FV) this.A00;
                AnonymousClass1OT r2 = (AnonymousClass1OT) this.A01;
                Log.i("appending additional prekeys");
                C15990oG r1 = r33.A03;
                if (!r1.A0f()) {
                    Log.i("no unsent prekeys; generating some new ones");
                    r1.A0S();
                }
                r33.A00.A02();
                r33.A02(r2);
                return;
            case 4:
                ((AnonymousClass1FN) this.A00).A02.A08((AbstractC15340mz) this.A01, -1);
                return;
            case 5:
                ((AnonymousClass1FM) this.A00).A0B.A0A((Collection) this.A01, null, null);
                return;
            case 6:
                ((C26881Fe) this.A00).A05.A05((UserJid) this.A01);
                return;
            case 7:
                C26871Fd r12 = ((C26881Fe) this.A00).A0K;
                Log.i("business activity report/notify-report-available");
                r12.A03.A03((C44281ye) this.A01);
                C49292Ke r0 = r12.A00;
                if (r0 != null) {
                    r0.A00.A02.A0B(2);
                    return;
                }
                C18360sK r34 = r12.A02;
                Context context = r12.A01.A00;
                String string = context.getString(R.string.p2b_report_available);
                C005602s A00 = C22630zO.A00(context);
                A00.A0J = "other_notifications@1";
                A00.A0B(string);
                A00.A05(System.currentTimeMillis());
                A00.A02(3);
                A00.A0D(true);
                A00.A0A(context.getString(R.string.app_name));
                A00.A09(string);
                Intent intent = new Intent();
                intent.setClassName(context.getPackageName(), "com.whatsapp.report.ReportActivity");
                A00.A09 = AnonymousClass1UY.A00(context, 0, intent, 0);
                C18360sK.A01(A00, R.drawable.notifybar);
                r34.A03(32, A00.A01());
                return;
            case 8:
                AnonymousClass1JF r62 = (AnonymousClass1JF) this.A01;
                AnonymousClass1JC r4 = ((AnonymousClass1JG) this.A00).A05;
                C14310lE r52 = r4.A03;
                C14320lF r22 = r52.A01;
                if (r22 instanceof AnonymousClass2KT) {
                    ((AnonymousClass2KT) r22).A0E(r52.A0N.A01(r4.A01, r62));
                    r4.A00.A0A(new AnonymousClass1JE(r62, r4.A02, true));
                    r52.A0A.A0A(r52.A01);
                    r52.A0S.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(r4, 15));
                    return;
                }
                return;
            case 9:
                AnonymousClass1JG r35 = (AnonymousClass1JG) this.A00;
                AnonymousClass1JD r23 = (AnonymousClass1JD) this.A01;
                AbstractC18870tC r13 = r35.A00;
                if (r13 != null) {
                    r35.A04.A04(r13);
                }
                C49302Kf r14 = new C49302Kf(r23, r35);
                r35.A00 = r14;
                r35.A04.A05(r14);
                return;
            case 10:
                AnonymousClass1JG r72 = (AnonymousClass1JG) this.A00;
                AnonymousClass1JD r36 = (AnonymousClass1JD) this.A01;
                if (!r72.A01) {
                    C17220qS r63 = r72.A06;
                    String A01 = r63.A01();
                    C41141sy r53 = new C41141sy("iq");
                    r53.A04(new AnonymousClass1W9("id", A01));
                    r53.A04(new AnonymousClass1W9("type", "get"));
                    r53.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
                    r53.A04(new AnonymousClass1W9("smax_id", "4"));
                    r53.A04(new AnonymousClass1W9("xmlns", "fb:thrift_iq"));
                    r53.A05(new AnonymousClass1V8("account_number", r36.A01, (AnonymousClass1W9[]) null));
                    r53.A05(new AnonymousClass1V8("code", r36.A00, (AnonymousClass1W9[]) null));
                    r53.A05(new AnonymousClass1V8("expected_source_url", r36.A02, (AnonymousClass1W9[]) null));
                    r53.A05(new AnonymousClass1V8("feature", new AnonymousClass1W9[]{new AnonymousClass1W9("support_icebreakers", "true")}));
                    if (r63.A0D(r72, r53.A03(), A01, 246, 0)) {
                        return;
                    }
                    if (r72.A02) {
                        r72.A02 = false;
                        r72.A03.A0H(new RunnableBRunnable0Shape7S0200000_I0_7(r72, 9, r36));
                        return;
                    }
                    r72.AP1(A01);
                    return;
                }
                return;
            case 11:
                C49312Kg r02 = (C49312Kg) this.A00;
                C20710wC r42 = r02.A01;
                C15580nU r37 = r02.A02;
                Log.i("groupChatManger/onSubGroupsChanged");
                r42.A0a.A03(r37, (Collection) this.A01);
                r42.A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(r42, 16, r37));
                return;
            case 12:
                C26951Fl r43 = (C26951Fl) this.A00;
                r43.A0G.A00.execute(new RunnableBRunnable0Shape7S0200000_I0_7(r43, 13, this.A01));
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C26951Fl r03 = (C26951Fl) this.A00;
                C28601Of r38 = (C28601Of) this.A01;
                C22100yW r24 = r03.A0A;
                boolean z = false;
                if (r03.A0B.A0D.A05()) {
                    z = true;
                }
                r24.A0F(r38, z, true);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                RunnableC29011Px r44 = (RunnableC29011Px) this.A00;
                Object obj2 = this.A01;
                r44.A08.A0M(Collections.singleton(obj2), true);
                r44.A09.A00(Collections.singleton(obj2));
                return;
            case 15:
                RunnableC29011Px r39 = (RunnableC29011Px) this.A00;
                C28941Pp r25 = (C28941Pp) this.A01;
                AnonymousClass1IS r45 = r25.A0C;
                if (r45 == null) {
                    r45 = r25.A0i;
                }
                if (r39.A0E.A0K.A03(r45) == null) {
                    StringBuilder sb = new StringBuilder("Dropping message ");
                    sb.append(r25);
                    sb.append(" due to message with old counter");
                    Log.e(sb.toString());
                    AbstractC15710nm r310 = r39.A01;
                    StringBuilder sb2 = new StringBuilder("received-with-old-counter-");
                    AbstractC14640lm r15 = r45.A00;
                    if (C15380n4.A0J(r15)) {
                        str2 = "group";
                    } else if (C15380n4.A0N(r15)) {
                        str2 = "status";
                    } else {
                        str2 = C15380n4.A0G(r15) ? "list" : "individual";
                    }
                    sb2.append(str2);
                    r310.AaV("incoming-message-dropped", sb2.toString(), false);
                    return;
                }
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((RunnableC29011Px) this.A00).A0T.Aaz((AbstractC16350or) this.A01, new Void[0]);
                return;
            case 17:
                ((C239513q) this.A00).A0N.A0p(((C28941Pp) this.A01).A02((byte) 31));
                return;
            case 18:
                ((C239513q) this.A00).A0a.A0C((DeviceJid) this.A01, "unknown_companion", false, false);
                return;
            case 19:
                C238413f r46 = (C238413f) this.A00;
                Object obj3 = this.A01;
                r46.A04.A02.A01(new AnonymousClass2DM(Collections.singletonList(obj3)));
                C237112s r04 = r46.A05;
                List singletonList = Collections.singletonList(obj3);
                for (AbstractC22250ym r05 : r04.A01()) {
                    r05.AMm(singletonList);
                }
                return;
            case C43951xu.A01:
                AbstractC14640lm r47 = (AbstractC14640lm) this.A01;
                C22280yp r311 = ((AnonymousClass10R) this.A00).A03;
                HashMap hashMap = r311.A06;
                C33911fH r06 = (C33911fH) hashMap.get(r47);
                if (!(r06 == null || r06.A02 == 0)) {
                    C33911fH r26 = (C33911fH) hashMap.get(r47);
                    if (r26 == null) {
                        r26 = new C33911fH();
                        hashMap.put(r47, r26);
                    }
                    r26.A02 = 0;
                    r26.A04 = 0;
                    r311.A04(r47);
                    return;
                }
                return;
            case 21:
                Object obj4 = this.A01;
                DevicePairQrScannerActivity devicePairQrScannerActivity = ((C41091ss) this.A00).A00;
                if (!(devicePairQrScannerActivity.AJN() || (ACX4 = devicePairQrScannerActivity.A0H.A00().ACX()) == null || !ACX4.equals(obj4) || (runnable = devicePairQrScannerActivity.A0M) == null)) {
                    ((ActivityC13810kN) devicePairQrScannerActivity).A00.removeCallbacks(runnable);
                    return;
                }
                return;
            case 22:
                Object obj5 = this.A01;
                DevicePairQrScannerActivity devicePairQrScannerActivity2 = ((C41091ss) this.A00).A00;
                if (!(devicePairQrScannerActivity2.AJN() || (ACX3 = devicePairQrScannerActivity2.A0H.A00().ACX()) == null || !ACX3.equals(obj5))) {
                    devicePairQrScannerActivity2.Ado(R.string.error_log_in_device);
                    devicePairQrScannerActivity2.A2i();
                    ((AbstractActivityC41101su) devicePairQrScannerActivity2).A05 = null;
                    return;
                }
                return;
            case 23:
                Object obj6 = this.A01;
                DevicePairQrScannerActivity devicePairQrScannerActivity3 = ((C41091ss) this.A00).A00;
                if (!(devicePairQrScannerActivity3.AJN() || (ACX2 = devicePairQrScannerActivity3.A0H.A00().ACX()) == null || !ACX2.equals(obj6))) {
                    devicePairQrScannerActivity3.A05.AKn();
                    return;
                }
                return;
            case 24:
                Object obj7 = this.A01;
                DevicePairQrScannerActivity devicePairQrScannerActivity4 = ((C41091ss) this.A00).A00;
                if (!(devicePairQrScannerActivity4.AJN() || (ACX = devicePairQrScannerActivity4.A0H.A00().ACX()) == null || !ACX.equals(obj7))) {
                    ((ActivityC13810kN) devicePairQrScannerActivity4).A05.A0G(devicePairQrScannerActivity4.A0Q);
                    devicePairQrScannerActivity4.A2j();
                    return;
                }
                return;
            case 25:
                AnonymousClass27M r48 = (AnonymousClass27M) this.A00;
                SurfaceHolder surfaceHolder = (SurfaceHolder) this.A01;
                try {
                    if (!surfaceHolder.isCreating()) {
                        r48.A03.stopPreview();
                    }
                    r48.A03.setPreviewDisplay(surfaceHolder);
                    r48.A04.post(new RunnableBRunnable0Shape10S0100000_I0_10(r48, 9));
                    return;
                } catch (IOException e) {
                    r48.A03.release();
                    r48.A03 = null;
                    Log.e("qrview/surfacechanged: error setting preview display", e);
                    r48.A00();
                    return;
                } catch (RuntimeException e2) {
                    r48.A03.release();
                    r48.A03 = null;
                    Log.e("qrview/surfacechanged ", e2);
                    r48.A00();
                    return;
                }
            case 26:
                ((AnonymousClass27M) this.A00).A08.AUS((C49262Kb) this.A01);
                return;
            case 27:
                AnonymousClass27M r312 = (AnonymousClass27M) this.A00;
                byte[] bArr = (byte[]) this.A01;
                Log.i("QrScannerView/decodeQrCode");
                Camera.Size size = r312.A02;
                int i = size.width;
                int i2 = (i * 3) >> 2;
                if (i2 < 320) {
                    i2 = i;
                }
                int i3 = size.height;
                int i4 = (i3 * 3) >> 2;
                if (i4 < 320) {
                    i4 = i3;
                }
                try {
                    C49262Kb A002 = r312.A0K.A00(new AnonymousClass2KZ(new AnonymousClass2KX(new AnonymousClass2KV(bArr, i, i3, (i - i2) >> 1, (i3 - i4) >> 1, i2, i4))), r312.A0B);
                    if (A002.A02 != null) {
                        Log.i("QrScannerView/notifyQrCodeDetected");
                        if (r312.A08 != null) {
                            r312.A0H.post(new RunnableBRunnable0Shape7S0200000_I0_7(r312, 26, A002));
                            return;
                        }
                        return;
                    }
                } catch (AbstractC49392Ko e3) {
                    Log.e("QrScannerView/decodeQrCode/failed to decode", e3);
                }
                r312.Aab();
                return;
            case 28:
                ((C467427a) this.A00).A00.A05.AUS((C49262Kb) this.A01);
                return;
            case 29:
                C49322Kh r27 = (C49322Kh) this.A00;
                C15370n3 r16 = (C15370n3) this.A01;
                r27.A01.A0K(r16);
                r27.A04.A02(r16);
                return;
            case C25991Bp.A0S:
                ((C49332Ki) this.A00).A02.A02((C48182Et) this.A01);
                return;
            case 31:
                C18350sJ r54 = (C18350sJ) this.A00;
                Context context2 = (Context) this.A01;
                SharedPreferences sharedPreferences = r54.A0N.A00;
                boolean z2 = false;
                if (sharedPreferences.getInt("migration_state_on_provider_side", 0) != 0) {
                    z2 = true;
                }
                AnonymousClass009.A00();
                if (sharedPreferences.getBoolean("registration_biz_registered_on_device", false)) {
                    try {
                        context2.getPackageManager().getPackageInfo("com.whatsapp.w4b", 0);
                    } catch (PackageManager.NameNotFoundException unused) {
                        sharedPreferences.edit().putBoolean("registration_biz_registered_on_device", false).apply();
                    }
                    Log.i("app/loginfailed/notification was suppressed by smb registration");
                    return;
                }
                if (!z2) {
                    if (!r54.A01.A00) {
                        r54.A0M.A03(20, r54.A02(context2.getString(R.string.notification_post_registration_logout_title), context2.getString(R.string.notification_post_registration_logout_message), context2.getString(R.string.notification_post_registration_logout_headline_app_name, context2.getString(R.string.localized_app_name))).A01());
                        return;
                    }
                    return;
                }
                Log.i("app/loginfailed/notification was suppressed by smb registration");
                return;
            case 32:
                C14900mE r28 = ((C18350sJ) this.A00).A04;
                r28.A0D(r28.A00, ((Context) this.A01).getString(R.string.change_number_same_number));
                return;
            case 33:
                C14900mE r29 = ((C18350sJ) this.A00).A04;
                r29.A0D(r29.A00, ((Context) this.A01).getString(R.string.change_number_generic_fail_message));
                return;
            case 34:
                C18350sJ r10 = (C18350sJ) this.A00;
                Me me = (Me) this.A01;
                C15570nT r07 = r10.A05;
                r07.A08();
                Me me2 = r07.A00;
                C14900mE r64 = r10.A04;
                AbstractC13860kS r55 = r64.A00;
                Context context3 = r10.A0L.A00;
                AnonymousClass018 r8 = r10.A0O;
                r64.A0D(r55, context3.getString(R.string.change_number_success, r8.A0G(AnonymousClass23M.A0E(me.cc, me.number)), r8.A0G(AnonymousClass23M.A0E(me2.cc, me2.number))));
                if (r10.A0B.A09() && r10.A0N.A09() != null) {
                    String str4 = me.jabber_id;
                    ConditionVariable conditionVariable = new ConditionVariable(false);
                    ServiceConnectionC49272Kc r122 = new ServiceConnectionC49272Kc(conditionVariable, r10);
                    context3.bindService(C14960mK.A0U(context3, null), r122, 1);
                    r10.A0m.Ab2(new RunnableBRunnable0Shape0S1300000_I0(r10, conditionVariable, r122, str4, 7));
                    return;
                }
                return;
            case 35:
                C49342Kj r17 = (C49342Kj) this.A00;
                VerifyTwoFactorAuth verifyTwoFactorAuth = (VerifyTwoFactorAuth) this.A01;
                Log.i("verifytwofactorauth/verifycodetask/dismiss-verification-complete-dialog");
                AnonymousClass1R5 r210 = r17.A00;
                if (r210.A0A) {
                    str = r17.A07;
                } else {
                    str = null;
                }
                verifyTwoFactorAuth.A2i(str, r210.A06);
                return;
            case 36:
                Context context4 = (Context) this.A00;
                C14820m6 r56 = (C14820m6) this.A01;
                synchronized (C49352Kk.A00) {
                    new File(context4.getFilesDir(), "backup_token").delete();
                }
                SharedPreferences sharedPreferences2 = r56.A00;
                sharedPreferences2.edit().remove("backup_token_file_timestamp").apply();
                sharedPreferences2.edit().remove("backup_token_source").apply();
                if (C28391Mz.A02()) {
                    C49352Kk.A01(context4, new byte[0]);
                    return;
                } else {
                    new BackupManager(context4).dataChanged();
                    return;
                }
            case 37:
                ((C26531Dv) this.A00).A0H.A08((AbstractC15340mz) this.A01, -1);
                return;
            case 38:
                C44281ye r49 = (C44281ye) this.A01;
                BusinessActivityReportViewModel businessActivityReportViewModel = ((C44101yE) this.A00).A00.A00;
                C26891Ff r211 = businessActivityReportViewModel.A05;
                if (r211.A00() != 4) {
                    r211.A03(r49);
                    businessActivityReportViewModel.A02.A0B(2);
                    return;
                }
                BusinessActivityReportViewModel.A00(businessActivityReportViewModel);
                return;
            case 39:
                C18910tG r410 = (C18910tG) this.A00;
                C22100yW r18 = (C22100yW) this.A01;
                synchronized (r410) {
                    if (r410.A01) {
                        Log.i("SeamlessManager/Aborting Seamless Migration.");
                        r18.A0D("account_sync_timeout");
                        r410.A02(new AnonymousClass2KL(14, 101L, null, null));
                        r410.A00();
                        r410.A01 = false;
                    }
                }
                return;
            case 40:
                SearchFragment searchFragment = (SearchFragment) this.A00;
                searchFragment.A03.A0Y(((Number) this.A01).intValue());
                searchFragment.A1A();
                return;
            case 41:
                SearchFragment searchFragment2 = ((C32901cv) this.A00).A00;
                SearchFragment.A01((AbstractC14640lm) this.A01, searchFragment2);
                SearchViewModel searchViewModel = searchFragment2.A1G;
                if (searchViewModel.A0Z()) {
                    searchViewModel.A0G();
                    return;
                }
                return;
            case 42:
                ((RestoreChatConnectionWorker) this.A00).A03.A04(this.A01);
                return;
            case 43:
                ((RestoreChatConnectionWorker) this.A00).A00.removeCallbacks((Runnable) this.A01);
                return;
            case 44:
                ((C15860o1) this.A00).A0M.A07((AbstractC14640lm) this.A01);
                return;
            case 45:
                ShareInviteLinkActivity shareInviteLinkActivity = (ShareInviteLinkActivity) this.A00;
                C49362Kl r212 = new C49362Kl();
                r212.A00 = (Integer) this.A01;
                r212.A01 = Integer.valueOf(C49372Km.A02(shareInviteLinkActivity.A08, shareInviteLinkActivity.A09));
                shareInviteLinkActivity.A07.A07(r212);
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                C248116y r411 = (C248116y) this.A00;
                AbstractC15340mz r313 = (AbstractC15340mz) this.A01;
                if (!r411.A02.A01()) {
                    return;
                }
                if (!(r313 instanceof C30331Wz)) {
                    r411.A00.post(new RunnableBRunnable0Shape7S0200000_I0_7(r411, 47, r313));
                    return;
                }
                String str5 = ((C30331Wz) r313).A01;
                if (str5 != null) {
                    r411.A00.post(new RunnableBRunnable0Shape0S1100000_I0(36, str5, r411));
                    return;
                }
                return;
            case 47:
                C248116y r412 = (C248116y) this.A00;
                AbstractC15340mz r314 = (AbstractC15340mz) this.A01;
                Map map = r412.A03;
                String str6 = r314.A0z.A01;
                if (!map.containsKey(str6)) {
                    map.put(str6, r314);
                    r412.A03();
                    return;
                }
                return;
            case 48:
                ((StatusPlaybackContactFragment) this.A00).A0A.A0J((C15370n3) this.A01);
                return;
            case 49:
                C22210yi r57 = (C22210yi) this.A00;
                AnonymousClass1KS r413 = (AnonymousClass1KS) this.A01;
                String str7 = r413.A0C;
                if (str7 != null) {
                    r57.A06(new C38861op(r413, str7, r57.A06.A00(str7)));
                    r57.A01.A0H(new RunnableBRunnable0Shape12S0100000_I0_12(r57.A07, 18));
                    C22230yk r315 = r57.A05;
                    String A03 = C28111Kr.A03(r57.A0B(false));
                    String str8 = r413.A0C;
                    if (r315.A0G.A02()) {
                        r315.A0C.A08(Message.obtain(null, 0, 242, 0, new C49282Kd(A03, str8)), false);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
