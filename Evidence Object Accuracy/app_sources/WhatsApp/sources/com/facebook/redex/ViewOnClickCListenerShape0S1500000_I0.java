package com.facebook.redex;

import X.AbstractC15340mz;
import X.AbstractC16870pt;
import X.AnonymousClass18P;
import X.AnonymousClass1IR;
import X.AnonymousClass1In;
import X.C69163Ye;
import android.content.Context;
import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S1500000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public String A05;
    public final int A06;

    public ViewOnClickCListenerShape0S1500000_I0(Context context, AnonymousClass1IR r2, AnonymousClass1In r3, AnonymousClass18P r4, AbstractC15340mz r5, String str, int i) {
        this.A06 = i;
        this.A00 = r4;
        this.A05 = str;
        this.A01 = context;
        this.A02 = r2;
        this.A03 = r5;
        this.A04 = r3;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        boolean z;
        String str;
        AbstractC15340mz r8;
        AnonymousClass18P r7;
        AnonymousClass1In r6;
        AnonymousClass1IR r5;
        Context context;
        AbstractC16870pt ACx;
        AbstractC16870pt ACx2;
        switch (this.A06) {
            case 0:
                r7 = (AnonymousClass18P) this.A00;
                str = this.A05;
                context = (Context) this.A01;
                r5 = (AnonymousClass1IR) this.A02;
                r8 = (AbstractC15340mz) this.A03;
                r6 = (AnonymousClass1In) this.A04;
                if (!(str == null || (ACx = r7.A0B.A02().ACx()) == null)) {
                    ACx.AKg(1, 41, str, null);
                }
                z = false;
                break;
            case 1:
                r7 = (AnonymousClass18P) this.A00;
                str = this.A05;
                context = (Context) this.A01;
                r5 = (AnonymousClass1IR) this.A02;
                r8 = (AbstractC15340mz) this.A03;
                r6 = (AnonymousClass1In) this.A04;
                if (!(str == null || (ACx2 = r7.A0B.A02().ACx()) == null)) {
                    ACx2.AKg(1, 42, str, null);
                }
                z = true;
                break;
            default:
                return;
        }
        r7.A01(context, r5, new C69163Ye(context, r5, r6, r7, r8, str, z), z);
    }
}
