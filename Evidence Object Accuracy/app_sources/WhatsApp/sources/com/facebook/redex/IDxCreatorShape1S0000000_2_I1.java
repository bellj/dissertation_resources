package com.facebook.redex;

import X.AnonymousClass009;
import X.AnonymousClass2x6;
import X.AnonymousClass34J;
import X.AnonymousClass3M0;
import X.AnonymousClass3M1;
import X.AnonymousClass3M2;
import X.AnonymousClass3M3;
import X.AnonymousClass3M4;
import X.AnonymousClass3M5;
import X.AnonymousClass3M6;
import X.AnonymousClass3M7;
import X.AnonymousClass3M8;
import X.AnonymousClass46U;
import X.C100504m1;
import X.C100514m2;
import X.C100534m4;
import X.C100544m5;
import X.C100554m6;
import X.C100564m7;
import X.C100574m8;
import X.C100584m9;
import X.C100594mA;
import X.C100604mB;
import X.C100614mC;
import X.C100624mD;
import X.C107414xI;
import X.C107424xJ;
import X.C107434xK;
import X.C107444xL;
import X.C107454xM;
import X.C107464xN;
import X.C107474xO;
import X.C107484xP;
import X.C107494xQ;
import X.C107504xR;
import X.C112295Cv;
import X.C12960it;
import X.C16700pc;
import X.C25991Bp;
import X.C43951xu;
import X.C52422am;
import X.C52432an;
import X.C52442ao;
import X.C52452ap;
import X.C52462aq;
import X.C52472ar;
import X.C65923Lp;
import X.C65953Ls;
import X.C65963Lt;
import X.C65973Lu;
import X.C65983Lv;
import X.C65993Lw;
import X.C66003Lx;
import X.C66013Ly;
import X.C66023Lz;
import X.C73683gd;
import X.C73693ge;
import X.C77003ma;
import X.C77013mb;
import X.C77023mc;
import X.C77033md;
import X.C77043me;
import X.C77053mf;
import X.C77063mg;
import X.C77073mh;
import X.C77083mi;
import X.C77093mj;
import X.C77103mk;
import X.C77113ml;
import X.C77123mm;
import X.C77133mn;
import X.C77143mo;
import X.C77153mp;
import X.C77343n8;
import X.EnumC87334Bc;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxCreatorShape1S0000000_2_I1 implements Parcelable.Creator {
    public final int A00;

    public IDxCreatorShape1S0000000_2_I1(int i) {
        this.A00 = i;
    }

    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        switch (this.A00) {
            case 0:
                return new C100614mC(parcel);
            case 1:
                return new C112295Cv(parcel);
            case 2:
                return new C100574m8(parcel);
            case 3:
                return new C100624mD(parcel);
            case 4:
                return new C107414xI(parcel.readInt(), parcel.readString());
            case 5:
                return new C107494xQ(parcel);
            case 6:
                return new C107504xR(parcel);
            case 7:
                return new C107434xK(parcel);
            case 8:
                return new C107484xP(parcel);
            case 9:
                return new C107454xM(parcel);
            case 10:
                return new C77073mh(parcel);
            case 11:
                return new C77003ma(parcel);
            case 12:
                return new C77103mk(parcel);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new C77013mb(parcel);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new C77053mf(parcel);
            case 15:
                return new C77083mi(parcel);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new C77063mg(parcel);
            case 17:
                return new C77093mj(parcel);
            case 18:
                return new C77023mc(parcel);
            case 19:
                return new C77033md(parcel);
            case C43951xu.A01:
                return new C77043me(parcel);
            case 21:
                return new C107464xN(parcel);
            case 22:
                return new C107474xO(parcel);
            case 23:
                ArrayList A0l = C12960it.A0l();
                parcel.readList(A0l, C100504m1.class.getClassLoader());
                return new C107424xJ(A0l);
            case 24:
                return new C100504m1(parcel.readInt(), parcel.readLong(), parcel.readLong());
            case 25:
                return new C107444xL(parcel);
            case 26:
                return new C77143mo(parcel);
            case 27:
                return new C77153mp(parcel);
            case 28:
                return new C77113ml();
            case 29:
                return new C77123mm(parcel);
            case C25991Bp.A0S:
                return new C77133mn(parcel.readLong(), parcel.readLong());
            case 31:
                return new C100554m6(parcel);
            case 32:
                return new C100564m7(parcel);
            case 33:
                return new C77343n8(parcel);
            case 34:
                return new C100584m9(parcel);
            case 35:
                return new C100594mA(parcel);
            case 36:
                return new C100604mB(parcel);
            case 37:
                return new C65993Lw(parcel);
            case 38:
                return new C73683gd(parcel);
            case 39:
                return new AnonymousClass3M1(parcel);
            case 40:
                return new AnonymousClass3M4(parcel);
            case 41:
                return new AnonymousClass3M7(parcel);
            case 42:
                return new C52452ap(parcel);
            case 43:
                return new AnonymousClass3M5(parcel);
            case 44:
                return new AnonymousClass3M0(parcel);
            case 45:
                return new AnonymousClass2x6(parcel);
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                return new AnonymousClass3M8(parcel);
            case 47:
                return new C65973Lu(parcel);
            case 48:
                return new C100544m5(parcel);
            case 49:
                return new C52472ar(parcel);
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                return new C52422am(parcel);
            case 51:
                return new C65983Lv(parcel);
            case 52:
                return new C73693ge(parcel);
            case 53:
                return EnumC87334Bc.values()[parcel.readInt()];
            case 54:
                return new C66013Ly(parcel);
            case 55:
                return new C66003Lx(parcel);
            case 56:
                return new C100514m2(parcel);
            case 57:
                return new AnonymousClass3M6(parcel);
            case 58:
                return new C52432an(parcel);
            case 59:
                return new C52462aq(parcel);
            case 60:
                return new C52442ao(parcel);
            case 61:
                return new C100534m4(parcel);
            case 62:
                String readString = parcel.readString();
                AnonymousClass009.A05(readString);
                return new AnonymousClass46U(readString);
            case 63:
                C16700pc.A0E(parcel, 0);
                return new C65923Lp(parcel.createStringArrayList());
            case 64:
                return new AnonymousClass34J(parcel);
            case 65:
                return new C66023Lz(parcel);
            case 66:
                return new AnonymousClass3M2(parcel);
            case 67:
                return new AnonymousClass3M3(parcel);
            case 68:
                return new C65953Ls(parcel);
            case 69:
                return new C65963Lt(parcel);
            default:
                return null;
        }
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        switch (this.A00) {
            case 0:
                return new C100614mC[i];
            case 1:
                return new C112295Cv[i];
            case 2:
                return new C100574m8[i];
            case 3:
                return new C100624mD[i];
            case 4:
                return new C107414xI[i];
            case 5:
                return new C107494xQ[i];
            case 6:
                return new C107504xR[i];
            case 7:
                return new C107434xK[i];
            case 8:
                return new C107484xP[i];
            case 9:
                return new C107454xM[i];
            case 10:
                return new C77073mh[i];
            case 11:
                return new C77003ma[i];
            case 12:
                return new C77103mk[i];
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new C77013mb[i];
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new C77053mf[i];
            case 15:
                return new C77083mi[i];
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new C77063mg[i];
            case 17:
                return new C77093mj[i];
            case 18:
                return new C77023mc[i];
            case 19:
                return new C77033md[i];
            case C43951xu.A01:
                return new C77043me[i];
            case 21:
                return new C107464xN[i];
            case 22:
                return new C107474xO[i];
            case 23:
                return new C107424xJ[i];
            case 24:
                return new C100504m1[i];
            case 25:
                return new C107444xL[i];
            case 26:
                return new C77143mo[i];
            case 27:
                return new C77153mp[i];
            case 28:
                return new C77113ml[i];
            case 29:
                return new C77123mm[i];
            case C25991Bp.A0S:
                return new C77133mn[i];
            case 31:
                return new C100554m6[i];
            case 32:
                return new C100564m7[i];
            case 33:
                return new C77343n8[i];
            case 34:
                return new C100584m9[i];
            case 35:
                return new C100594mA[i];
            case 36:
                return new C100604mB[i];
            case 37:
                return new C65993Lw[i];
            case 38:
                return new C73683gd[i];
            case 39:
                return new AnonymousClass3M1[i];
            case 40:
                return new AnonymousClass3M4[i];
            case 41:
                return new AnonymousClass3M7[i];
            case 42:
                return new C52452ap[i];
            case 43:
                return new AnonymousClass3M5[i];
            case 44:
                return new AnonymousClass3M0[i];
            case 45:
                return new AnonymousClass2x6[i];
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                return new AnonymousClass3M8[i];
            case 47:
                return new C65973Lu[i];
            case 48:
                return new C100544m5[i];
            case 49:
                return new C52472ar[i];
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                return new C52422am[i];
            case 51:
                return new C65983Lv[i];
            case 52:
                return new C73693ge[i];
            case 53:
                return new EnumC87334Bc[i];
            case 54:
                return new C66013Ly[i];
            case 55:
                return new C66003Lx[i];
            case 56:
                return new C100514m2[i];
            case 57:
                return new AnonymousClass3M6[i];
            case 58:
                return new C52432an[i];
            case 59:
                return new C52462aq[i];
            case 60:
                return new C52442ao[i];
            case 61:
                return new C100534m4[i];
            case 62:
                return new AnonymousClass46U[i];
            case 63:
                return new C65923Lp[i];
            case 64:
                return new AnonymousClass34J[i];
            case 65:
                return new C66023Lz[i];
            case 66:
                return new AnonymousClass3M2[i];
            case 67:
                return new AnonymousClass3M3[i];
            case 68:
                return new C65953Ls[i];
            case 69:
                return new C65963Lt[i];
            default:
                return new Object[0];
        }
    }
}
