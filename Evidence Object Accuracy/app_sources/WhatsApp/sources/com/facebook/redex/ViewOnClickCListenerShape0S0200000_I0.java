package com.facebook.redex;

import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0200000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public final int A02;

    public ViewOnClickCListenerShape0S0200000_I0(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = obj2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:357:? A[RETURN, SYNTHETIC] */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r34) {
        /*
        // Method dump skipped, instructions count: 2918
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0.onClick(android.view.View):void");
    }
}
