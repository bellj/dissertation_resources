package com.facebook.redex;

import X.AnonymousClass1KZ;
import X.AnonymousClass2TW;
import X.C38671oW;
import X.C54912hU;
import android.view.View;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0201000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public int A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public ViewOnClickCListenerShape0S0201000_I0(Object obj, Object obj2, int i, int i2) {
        this.A03 = i2;
        this.A01 = obj;
        this.A02 = obj2;
        this.A00 = i;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A03) {
            case 0:
                AnonymousClass2TW r3 = (AnonymousClass2TW) this.A01;
                r3.A0E((C54912hU) this.A02, (StorageUsageMediaGalleryFragment) r3.A05, this.A00);
                return;
            case 1:
                int i = this.A00;
                ((C38671oW) this.A01).A01.A1C((AnonymousClass1KZ) this.A02, i);
                return;
            default:
                return;
        }
    }
}
