package com.facebook.redex;

import X.AbstractC14200l1;
import X.AbstractC14640lm;
import X.AbstractC33641ei;
import X.AbstractC75703kH;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass018;
import X.AnonymousClass028;
import X.AnonymousClass12P;
import X.AnonymousClass19M;
import X.AnonymousClass1OY;
import X.AnonymousClass28D;
import X.AnonymousClass2Jw;
import X.AnonymousClass2K2;
import X.AnonymousClass35V;
import X.AnonymousClass3DG;
import X.AnonymousClass3HU;
import X.AnonymousClass3IJ;
import X.AnonymousClass3IZ;
import X.AnonymousClass3JF;
import X.AnonymousClass3JU;
import X.AnonymousClass3LU;
import X.AnonymousClass4KO;
import X.AnonymousClass4UW;
import X.AnonymousClass4Wo;
import X.AnonymousClass56Z;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C12990iw;
import X.C13000ix;
import X.C14210l2;
import X.C14220l3;
import X.C14260l7;
import X.C14820m6;
import X.C14960mK;
import X.C15370n3;
import X.C16430p0;
import X.C16630pM;
import X.C28141Kv;
import X.C28531Ny;
import X.C28701Oq;
import X.C28851Pg;
import X.C30211Wn;
import X.C30761Ys;
import X.C37471mS;
import X.C38311ns;
import X.C38491oB;
import X.C39511q1;
import X.C52282aY;
import X.C52502ax;
import X.C53132cx;
import X.C53252dZ;
import X.C53262da;
import X.C53862fQ;
import X.C54482gn;
import X.C54982hb;
import X.C59782vN;
import X.C63363Bh;
import X.C64343Fe;
import X.C65093Ic;
import X.C68433Vj;
import X.C83413xC;
import X.C89324Jn;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.calling.callhistory.group.GroupCallLogActivity;
import com.whatsapp.conversation.conversationrow.TemplateRowContentLayout;
import com.whatsapp.emoji.EmojiDescriptor;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.Set;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape1S0300000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public ViewOnClickCListenerShape1S0300000_I1(C14260l7 r1, AnonymousClass28D r2, AbstractC14200l1 r3, int i) {
        this.A03 = i;
        if (i != 0) {
            this.A00 = r2;
            this.A01 = r3;
            this.A02 = r1;
            return;
        }
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public ViewOnClickCListenerShape1S0300000_I1(Object obj, Object obj2, Object obj3, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        AnonymousClass28D r4;
        AbstractC14200l1 r3;
        C14260l7 r2;
        C14210l2 r1;
        int length;
        C52502ax r5;
        int[] iArr;
        Intent A0A;
        switch (this.A03) {
            case 0:
                r4 = (AnonymousClass28D) this.A01;
                r3 = (AbstractC14200l1) this.A02;
                r1 = C14210l2.A00(r4);
                r2 = (C14260l7) this.A00;
                r1.A05(r2, 1);
                break;
            case 1:
                AnonymousClass3IZ r42 = (AnonymousClass3IZ) this.A00;
                AnonymousClass3HU r32 = (AnonymousClass3HU) this.A02;
                if (C28141Kv.A01((AnonymousClass018) this.A01)) {
                    length = r32.A04;
                } else {
                    length = (r42.A0S.length - 1) - r32.A04;
                }
                r42.A0L.A0F(length, true);
                return;
            case 2:
                AnonymousClass3IZ r43 = (AnonymousClass3IZ) this.A00;
                C16630pM r7 = (C16630pM) this.A01;
                C14820m6 r12 = (C14820m6) this.A02;
                C53252dZ r0 = r43.A04;
                if (r0 == null || !r0.isShowing()) {
                    C53262da r02 = r43.A03;
                    if ((r02 == null || !r02.isShowing()) && (iArr = (r5 = (C52502ax) view).A07) != null) {
                        if (AnonymousClass3JU.A02(iArr)) {
                            if (!r7.A01("emoji_modifiers").contains(C12960it.A0d(new C37471mS(AnonymousClass3JU.A07(iArr)).toString(), C12960it.A0k("multi_skin_")))) {
                                r43.A01(r5);
                                return;
                            }
                            AnonymousClass3JF.A01(r7, iArr);
                        } else if (AnonymousClass3JU.A03(iArr)) {
                            SharedPreferences sharedPreferences = r12.A00;
                            int A01 = C12970iu.A01(sharedPreferences, "skin_emoji_tip");
                            if (A01 < 1) {
                                C12960it.A0u(sharedPreferences, "skin_emoji_tip", A01 + 1);
                                int[] iArr2 = r5.A07;
                                View view2 = r43.A0E;
                                ViewGroup A0P = C12980iv.A0P(view2, R.id.all_samples);
                                A0P.removeAllViews();
                                Context context = r43.A0A;
                                int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.emoji_picker_item);
                                int dimensionPixelSize2 = (dimensionPixelSize - context.getResources().getDimensionPixelSize(R.dimen.emoji_picker_icon)) >> 1;
                                int[][] A0A2 = AnonymousClass3JU.A0A(iArr2);
                                for (int[] iArr3 : A0A2) {
                                    ImageView imageView = new ImageView(view2.getContext());
                                    imageView.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
                                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    imageView.setLayoutParams(new LinearLayout.LayoutParams(dimensionPixelSize, dimensionPixelSize));
                                    AnonymousClass19M.A02(context.getResources(), imageView, r43.A0O, iArr3);
                                    C12990iw.A1C(imageView, r43, iArr3, r5, 3);
                                    A0P.addView(imageView);
                                }
                                ImageView A0L = C12970iu.A0L(view2, R.id.sample_current);
                                A0L.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
                                A0L.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                AnonymousClass19M.A02(context.getResources(), A0L, r43.A0O, iArr2);
                                view2.setVisibility(0);
                                return;
                            } else if (!r7.A01("emoji_modifiers").contains(AnonymousClass3JF.A00(iArr))) {
                                C53252dZ r13 = new C53252dZ(r5, r43.A0O, new AnonymousClass56Z(r5, r43), r5.A07);
                                r43.A04 = r13;
                                C38491oB.A01(r5, r43.A0F, r13);
                                return;
                            } else {
                                AnonymousClass3JF.A02(r7, iArr);
                            }
                        }
                        r43.A02(iArr);
                        return;
                    }
                    return;
                }
                return;
            case 3:
                AnonymousClass3IZ r52 = (AnonymousClass3IZ) this.A00;
                int[] iArr4 = (int[]) this.A01;
                C52502ax r44 = (C52502ax) this.A02;
                r52.A02(iArr4);
                AnonymousClass3JF.A02(r52.A0Q, iArr4);
                r44.setEmoji(iArr4);
                long A00 = EmojiDescriptor.A00(new C39511q1(iArr4), false);
                Drawable A04 = r52.A0O.A04(r52.A0A.getResources(), new C39511q1(iArr4), 0.75f, A00);
                if (r44.A01 == A00) {
                    r44.A03 = A04;
                    r44.invalidate();
                }
                r52.A0E.setVisibility(8);
                return;
            case 4:
                CompoundButton compoundButton = (CompoundButton) this.A00;
                AnonymousClass28D r6 = (AnonymousClass28D) this.A01;
                C14260l7 r53 = (C14260l7) this.A02;
                boolean z = !compoundButton.isChecked();
                compoundButton.setChecked(z);
                AbstractC14200l1 A0G = r6.A0G(40);
                if (A0G != null) {
                    C65093Ic.A01(r53.A02.A01);
                    C28701Oq.A01(r53, r6, C14210l2.A01(new C14210l2(), Boolean.valueOf(z), 0), A0G);
                    return;
                }
                return;
            case 5:
                r4 = (AnonymousClass28D) this.A00;
                r3 = (AbstractC14200l1) this.A01;
                r2 = (C14260l7) this.A02;
                r1 = C14210l2.A00(r4);
                break;
            case 6:
                AnonymousClass3LU r14 = (AnonymousClass3LU) this.A00;
                ((C89324Jn) this.A01).A00.A2f();
                r14.A0D = true;
                C12960it.A0t(r14.A0J.A02.A00().edit(), "DIRECTORY_LOCATION_INFO_ACCEPTED", true);
                ((Dialog) this.A02).dismiss();
                return;
            case 7:
                C30211Wn r22 = (C30211Wn) this.A02;
                boolean A002 = ((AnonymousClass4UW) this.A01).A00();
                AnonymousClass2Jw r03 = ((AbstractC75703kH) this.A00).A00;
                if (A002) {
                    r03.AVl(r22);
                    return;
                } else {
                    r03.AY1(r22);
                    return;
                }
            case 8:
                C59782vN r45 = (C59782vN) this.A00;
                Object obj = this.A02;
                AppCompatCheckBox appCompatCheckBox = r45.A00;
                ((AnonymousClass4Wo) this.A01).A00 = appCompatCheckBox.isChecked();
                boolean isChecked = appCompatCheckBox.isChecked();
                Set set = r45.A01.A03.A03;
                if (isChecked) {
                    set.add(obj);
                    return;
                } else {
                    set.remove(obj);
                    return;
                }
            case 9:
                C53862fQ r23 = (C53862fQ) this.A00;
                C30211Wn r15 = (C30211Wn) this.A02;
                AnonymousClass2K2.A00(r23);
                r23.A0D();
                C16430p0 r10 = r23.A0E;
                long size = (long) ((C63363Bh) this.A01).A08.size();
                long size2 = (long) r23.A0S.size();
                AnonymousClass009.A05(r15);
                String str = r15.A00;
                String A05 = r23.A05();
                C68433Vj r04 = r23.A0L;
                int A02 = r04.A02();
                int i = r23.A00;
                int A012 = r04.A01();
                C28531Ny r16 = new C28531Ny();
                C13000ix.A06(r16, 16);
                r16.A0K = Long.valueOf(size);
                r16.A0X = str;
                r16.A0Z = A05;
                r16.A0N = Long.valueOf(size2);
                r16.A07 = Integer.valueOf(C12970iu.A06(r16, A02, i, A012));
                r10.A03(r16);
                return;
            case 10:
                AbstractC14640lm A022 = C15370n3.A02((C15370n3) this.A01);
                GroupCallLogActivity groupCallLogActivity = ((C54482gn) this.A00).A01;
                AnonymousClass3DG r24 = new AnonymousClass3DG(((ActivityC13810kN) groupCallLogActivity).A0C, A022, null);
                ImageView imageView2 = ((C54982hb) this.A02).A02;
                r24.A02 = AnonymousClass028.A0J(imageView2);
                r24.A00(groupCallLogActivity, imageView2);
                return;
            case 11:
                ((ActivityC13810kN) AnonymousClass12P.A00(((C64343Fe) this.A00).A03)).Adm(SuspiciousLinkWarningDialogFragment.A00(((AnonymousClass3IJ) this.A01).A03, (Set) this.A02));
                return;
            case 12:
                TemplateRowContentLayout templateRowContentLayout = (TemplateRowContentLayout) this.A00;
                C30761Ys r72 = (C30761Ys) this.A01;
                AnonymousClass4KO r62 = (AnonymousClass4KO) this.A02;
                int i2 = r72.A03;
                if (i2 != 1) {
                    if (i2 == 2) {
                        if (templateRowContentLayout.A05.A06(r72)) {
                            templateRowContentLayout.A05.A05((C28851Pg) templateRowContentLayout.A04.getFMessage(), 1);
                        } else if (templateRowContentLayout.A05.A07(r72)) {
                            templateRowContentLayout.A05.A03(templateRowContentLayout.getContext(), (C28851Pg) templateRowContentLayout.A04.getFMessage(), 2);
                        } else {
                            String str2 = r72.A05;
                            AnonymousClass1OY r05 = r62.A00;
                            Conversation A0o = r05.A0o();
                            if (A0o != null) {
                                Set A013 = r05.A0t.A01(r05.getFMessage().A0C(), str2);
                                if (A013 != null) {
                                    A0o.Adm(SuspiciousLinkWarningDialogFragment.A00(str2, A013));
                                    return;
                                }
                            } else {
                                Log.e("ConversationRow/needHandleSuspiciousUrl/error: not click in Conversation");
                            }
                            if (C38311ns.A01(templateRowContentLayout.A07, r72)) {
                                A0A = C14960mK.A0a(templateRowContentLayout.getContext(), str2, "research-survey", true, true);
                            } else {
                                try {
                                    if (templateRowContentLayout.A06.A05(Uri.parse(str2)) != 1) {
                                        Context context2 = templateRowContentLayout.getContext();
                                        Uri parse = Uri.parse(str2);
                                        A0A = C12970iu.A0A();
                                        A0A.setClassName(context2.getPackageName(), "com.whatsapp.deeplink.DeepLinkActivity");
                                        A0A.setData(parse);
                                        A0A.putExtra("source", 2);
                                    }
                                } catch (Exception e) {
                                    StringBuilder A0k = C12960it.A0k("TemplateRowContentLayout/isDeepLinkUri/<");
                                    A0k.append(str2);
                                    Log.e(C12960it.A0Z(e, "> is not a valid URL. Error=", A0k));
                                }
                                templateRowContentLayout.A08.A01(templateRowContentLayout.getContext(), Uri.parse(str2));
                                return;
                            }
                        }
                        Conversation A0o2 = templateRowContentLayout.A04.A0o();
                        if (A0o2 != null) {
                            A0o2.A33();
                            return;
                        }
                        return;
                    } else if (i2 == 3) {
                        A0A = C12970iu.A0B(Uri.parse(C12960it.A0d(r72.A05, C12960it.A0k("tel:"))));
                    } else {
                        return;
                    }
                    templateRowContentLayout.A01.A06(templateRowContentLayout.getContext(), A0A);
                    return;
                }
                Log.e("TemplateRowContentLayout/fillButton/the button is in wrong type.");
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                Context context3 = (Context) this.A01;
                AnonymousClass12P r33 = ((C53132cx) this.A00).A00;
                Intent A0A3 = C12970iu.A0A();
                A0A3.setClassName(context3.getPackageName(), "com.whatsapp.community.deactivate.DeactivateCommunityDisclaimerActivity");
                A0A3.putExtra("parent_group_jid", ((Jid) this.A02).getRawString());
                r33.A06(context3, A0A3);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                AnonymousClass35V r25 = (AnonymousClass35V) this.A00;
                Rect bounds = ((C52282aY) this.A01).A03.getBounds();
                int centerX = bounds.centerX();
                TextEmojiLabel textEmojiLabel = r25.A06;
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 0, (float) ((centerX + textEmojiLabel.getTotalPaddingLeft()) - textEmojiLabel.getScrollX()), 0, (float) ((bounds.centerY() + textEmojiLabel.getTotalPaddingTop()) - textEmojiLabel.getScrollY()));
                scaleAnimation.setDuration(160);
                scaleAnimation.setInterpolator(new DecelerateInterpolator());
                scaleAnimation.setAnimationListener(new C83413xC((ViewGroup) this.A02, r25));
                View view3 = r25.A00;
                if (view3 != null) {
                    view3.startAnimation(scaleAnimation);
                    r25.A00.setVisibility(8);
                    ((AbstractC33641ei) r25).A05.A00.A0E();
                    return;
                }
                return;
            default:
                return;
        }
        C28701Oq.A01(r2, r4, new C14220l3(r1.A00), r3);
    }
}
