package com.facebook.redex;

import X.AbstractC16130oV;
import X.AnonymousClass1MS;
import X.AnonymousClass4QQ;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.mediaview.PhotoView;
import java.util.AbstractList;
import java.util.Stack;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0310000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public boolean A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0310000_I0(MediaViewFragment mediaViewFragment) {
        this.A04 = 8;
        this.A04 = 8;
        this.A01 = mediaViewFragment;
        this.A00 = new Stack();
        this.A02 = new AnonymousClass1MS(this, "PhotoLoader");
    }

    public RunnableBRunnable0Shape0S0310000_I0(Object obj, Object obj2, Object obj3, int i, boolean z) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = z;
    }

    public void A00(PhotoView photoView, AbstractC16130oV r5) {
        AnonymousClass4QQ r2 = new AnonymousClass4QQ(this, photoView, r5);
        AbstractList abstractList = (AbstractList) this.A00;
        synchronized (abstractList) {
            abstractList.add(0, r2);
            abstractList.notifyAll();
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // java.lang.Runnable
    public final void run() {
        /*
        // Method dump skipped, instructions count: 2190
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0.run():void");
    }
}
