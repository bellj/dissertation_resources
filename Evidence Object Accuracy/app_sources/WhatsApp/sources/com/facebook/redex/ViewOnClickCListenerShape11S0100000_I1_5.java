package com.facebook.redex;

import android.view.View;
import com.whatsapp.stickers.AddThirdPartyStickerPackActivity;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape11S0100000_I1_5 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape11S0100000_I1_5(AddThirdPartyStickerPackActivity.AddStickerPackDialogFragment addStickerPackDialogFragment, int i) {
        this.A01 = i;
        switch (i) {
            case 24:
            case 25:
                this.A00 = addStickerPackDialogFragment;
                return;
            default:
                this.A00 = addStickerPackDialogFragment;
                return;
        }
    }

    public ViewOnClickCListenerShape11S0100000_I1_5(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:136:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0045  */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r13) {
        /*
        // Method dump skipped, instructions count: 1082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape11S0100000_I1_5.onClick(android.view.View):void");
    }
}
