package com.facebook.redex;

import X.AbstractC15340mz;
import X.AnonymousClass2KT;
import X.C13000ix;
import X.C14900mE;
import X.C16170oZ;
import X.C16310on;
import X.C16330op;
import X.C242814x;
import X.C32361c2;
import X.C63313Bc;
import X.C91564Sf;
import X.C92644Wt;
import android.content.ContentValues;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0620000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public boolean A06;
    public boolean A07;
    public final int A08;

    public RunnableBRunnable0Shape0S0620000_I1(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, int i, boolean z, boolean z2) {
        this.A08 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A06 = z;
        this.A04 = obj5;
        this.A07 = z2;
        this.A05 = obj6;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A08) {
            case 0:
                boolean z = this.A06;
                Object obj = this.A00;
                Set set = (Set) this.A01;
                C14900mE r7 = (C14900mE) this.A02;
                CharSequence charSequence = (CharSequence) this.A03;
                C16170oZ r5 = (C16170oZ) this.A04;
                boolean z2 = this.A07;
                Object obj2 = this.A05;
                if (!z || obj == null) {
                    r7.A0F(charSequence, 0);
                } else if (set.size() > 1000) {
                    r7.A08(R.string.singular_delete_message_limit, 1);
                    return;
                } else {
                    r7.A0H(new RunnableBRunnable0Shape10S0200000_I1(obj, 31, charSequence));
                }
                r5.A0T(set, z2);
                C14900mE.A00(r7, obj2, 13);
                return;
            case 1:
                C91564Sf r3 = (C91564Sf) this.A00;
                WebPagePreviewView webPagePreviewView = (WebPagePreviewView) this.A01;
                AnonymousClass2KT r6 = (AnonymousClass2KT) this.A02;
                AbstractC15340mz r52 = (AbstractC15340mz) this.A03;
                boolean z3 = this.A06;
                List list = (List) this.A04;
                boolean z4 = this.A07;
                C63313Bc r72 = (C63313Bc) this.A05;
                C92644Wt r2 = (C92644Wt) webPagePreviewView.getTag();
                try {
                    AnonymousClass2KT r4 = r2.A00;
                    if (!r4.A0F.isEmpty()) {
                        r4.A08((String) r4.A0F.toArray()[0]);
                    }
                    AbstractC15340mz r42 = r2.A01;
                    C32361c2 r1 = r42.A0N;
                    if (r1 != null) {
                        byte[] bArr = r6.A0I;
                        if (bArr == null) {
                            bArr = r6.A0H;
                        }
                        r1.A00 = bArr;
                    }
                    if (r42.A0z.A01.equals(r52.A0z.A01)) {
                        r3.A00.A0H(new Runnable(r6, r72, webPagePreviewView, list, z3, z4) { // from class: X.2j3
                            public final /* synthetic */ AnonymousClass2KT A00;
                            public final /* synthetic */ C63313Bc A01;
                            public final /* synthetic */ WebPagePreviewView A02;
                            public final /* synthetic */ List A03;
                            public final /* synthetic */ boolean A04;
                            public final /* synthetic */ boolean A05;

                            {
                                this.A02 = r3;
                                this.A00 = r1;
                                this.A04 = r5;
                                this.A03 = r4;
                                this.A05 = r6;
                                this.A01 = r2;
                            }

                            @Override // java.lang.Runnable
                            public final void run() {
                                WebPagePreviewView webPagePreviewView2 = this.A02;
                                AnonymousClass2KT r43 = this.A00;
                                boolean z5 = this.A04;
                                List list2 = this.A03;
                                boolean z6 = this.A05;
                                C63313Bc r0 = this.A01;
                                webPagePreviewView2.A0A(r43, list2, z5, z6);
                                C64343Fe r12 = r0.A01;
                                AbstractC15340mz r32 = r0.A02;
                                boolean z7 = r0.A05;
                                r12.A01(r0.A00, r32, r0.A03, r0.A04, z7, r0.A06, r0.A07);
                            }
                        });
                    }
                    byte[] bArr2 = r6.A0I;
                    if (!(bArr2 == null && (bArr2 = r6.A0H) == null)) {
                        C242814x r0 = r3.A02;
                        long j = r42.A11;
                        C16310on A02 = r0.A00.A02();
                        try {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("full_thumbnail", bArr2);
                            C16330op r9 = A02.A03;
                            String[] A08 = C13000ix.A08();
                            A08[0] = String.valueOf(j);
                            if (r9.A00("message_external_ad_content", contentValues, "message_row_id=?", A08) == 0) {
                                Log.e("ExternalAdContentInfoStore/updateFullThumbnail/full thumbnail wasn't updated");
                            }
                            A02.close();
                        } catch (Throwable th) {
                            try {
                                A02.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    }
                } catch (IOException | URISyntaxException e) {
                    Log.w("ConversationPageInfoLoader/load/failed to load thumb", e);
                }
                r3.A04.remove(r2.A01.A0z.A01);
                return;
            default:
                return;
        }
    }
}
