package com.facebook.redex;

import X.AbstractC130695zp;
import X.AnonymousClass616;
import X.AnonymousClass61B;
import X.AnonymousClass61Q;
import X.AnonymousClass662;
import X.AnonymousClass66M;
import X.AnonymousClass66P;
import X.AnonymousClass6L0;
import X.AnonymousClass6L1;
import X.C119085cr;
import X.C125445rG;
import X.C126635tC;
import X.C127245uB;
import X.C127255uC;
import X.C128445w7;
import X.C12960it;
import X.C130175yv;
import X.C1310360y;
import X.C136076Kx;
import X.CallableC135856Kb;
import X.CallableC135936Kj;
import java.util.List;
import java.util.concurrent.Callable;

/* loaded from: classes4.dex */
public class IDxCallableShape15S0100000_3_I1 implements Callable {
    public Object A00;
    public final int A01;

    public IDxCallableShape15S0100000_3_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    public static /* bridge */ /* synthetic */ Object A00(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        ((C128445w7) iDxCallableShape15S0100000_3_I1.A00).A00.A0Z.A02();
        return null;
    }

    public static /* bridge */ /* synthetic */ Object A01(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        try {
            AnonymousClass616.A00();
            AnonymousClass662 r1 = (AnonymousClass662) iDxCallableShape15S0100000_3_I1.A00;
            r1.A0C();
            if (r1.A0i != null) {
                r1.A0i.A01();
                r1.A0i = null;
                r1.A0A = null;
            }
            r1.A07 = null;
            r1.A0B = null;
            return null;
        } catch (Exception e) {
            throw e;
        } finally {
            AnonymousClass616.A00();
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:36:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.facebook.redex.IDxCallableShape15S0100000_3_I1 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: X.662 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v3, types: [X.662] */
    public static /* bridge */ /* synthetic */ Object A02(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        C119085cr r1;
        try {
            iDxCallableShape15S0100000_3_I1 = (AnonymousClass662) iDxCallableShape15S0100000_3_I1.A00;
            AnonymousClass616.A00();
            try {
                if (AnonymousClass662.A09(iDxCallableShape15S0100000_3_I1)) {
                    boolean A1T = C12960it.A1T(iDxCallableShape15S0100000_3_I1.A00);
                    C130175yv r12 = iDxCallableShape15S0100000_3_I1.A0U;
                    int i = 1;
                    if (A1T) {
                        i = 0;
                    }
                    if (!r12.A09(Integer.valueOf(i))) {
                        StringBuilder A0h = C12960it.A0h();
                        A0h.append("Cannot switch to ");
                        A0h.append(A1T ? "FRONT" : "BACK");
                        throw new AnonymousClass6L1(C12960it.A0d(", camera is not present", A0h));
                    }
                    iDxCallableShape15S0100000_3_I1.A0p = true;
                    String A06 = r12.A06(A1T ? 1 : 0);
                    AnonymousClass662.A06(iDxCallableShape15S0100000_3_I1, A06);
                    AnonymousClass662.A07(iDxCallableShape15S0100000_3_I1, A06);
                    AnonymousClass662.A05(iDxCallableShape15S0100000_3_I1);
                    AnonymousClass662.A08(iDxCallableShape15S0100000_3_I1, A06);
                    int i2 = iDxCallableShape15S0100000_3_I1.A00;
                    AbstractC130695zp ABG = iDxCallableShape15S0100000_3_I1.ABG();
                    if (!iDxCallableShape15S0100000_3_I1.isConnected() || (r1 = iDxCallableShape15S0100000_3_I1.A0C) == null) {
                        throw new C136076Kx("Cannot get camera settings");
                    }
                    C127255uC r13 = new C127255uC(new C127245uB(ABG, r1, i2));
                    AnonymousClass616.A00();
                    return r13;
                }
                throw new AnonymousClass6L0("Cannot switch camera, no cameras open.");
            } catch (Exception e) {
                AnonymousClass616.A00();
                throw e;
            }
        } finally {
            iDxCallableShape15S0100000_3_I1.A0p = false;
        }
    }

    public static /* bridge */ /* synthetic */ Object A03(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        C119085cr r0;
        AnonymousClass662 r4 = (AnonymousClass662) iDxCallableShape15S0100000_3_I1.A00;
        if (r4.isConnected()) {
            r4.A0D();
            if (r4.A0i != null) {
                AnonymousClass66P r3 = r4.A0i;
                int i = r4.A01;
                int i2 = 90;
                if (i != 1) {
                    i2 = 180;
                    if (i != 2) {
                        i2 = 270;
                        if (i != 3) {
                            i2 = 0;
                        }
                    }
                }
                r3.A02(i2);
            }
            int i3 = r4.A00;
            AbstractC130695zp ABG = r4.ABG();
            if (r4.isConnected() && (r0 = r4.A0C) != null) {
                return new C127255uC(new C127245uB(ABG, r0, i3));
            }
            throw new C136076Kx("Cannot get camera settings");
        }
        throw new C136076Kx("Can not update preview display rotation");
    }

    public static /* bridge */ /* synthetic */ Object A04(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        AnonymousClass662 r1 = (AnonymousClass662) iDxCallableShape15S0100000_3_I1.A00;
        if (r1.A0F()) {
            return null;
        }
        AnonymousClass61Q r3 = r1.A0Y;
        if (!r3.A0Q) {
            return null;
        }
        r3.A0N.A07("restart_preview_on_background_thread", new CallableC135936Kj(r3, false, false));
        return null;
    }

    public static /* bridge */ /* synthetic */ Object A05(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        ((C130175yv) iDxCallableShape15S0100000_3_I1.A00).A08();
        return null;
    }

    public static /* bridge */ /* synthetic */ Object A06(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        AnonymousClass61B.A02((AnonymousClass61B) iDxCallableShape15S0100000_3_I1.A00);
        return null;
    }

    public static /* bridge */ /* synthetic */ Object A07(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        AnonymousClass61Q r3 = ((C125445rG) iDxCallableShape15S0100000_3_I1.A00).A00;
        r3.A0H.A01("Method onCameraSessionActive must be called on Optic Thread.");
        r3.A0N.A04("camera_session_active_on_camera_handler_thread", new CallableC135856Kb(r3, new AnonymousClass66M()));
        return null;
    }

    public static /* bridge */ /* synthetic */ Object A08(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        C126635tC r0;
        AnonymousClass61Q r5 = (AnonymousClass61Q) iDxCallableShape15S0100000_3_I1.A00;
        if (r5.A0Q) {
            List list = r5.A0O;
            if (!list.isEmpty() && (r0 = (C126635tC) list.remove(0)) != null) {
                r5.A0N.A07("restart_preview_on_background_thread", new CallableC135936Kj(r5, r0.A00, r0.A01));
            }
        }
        return null;
    }

    public static /* bridge */ /* synthetic */ Object A09(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        AnonymousClass61Q r1;
        try {
            r1 = (AnonymousClass61Q) iDxCallableShape15S0100000_3_I1.A00;
            C1310360y r0 = r1.A09;
            if (r0 != null) {
                r0.A02();
            } else {
                r1.A0K.A02.A01();
            }
        } catch (Exception unused) {
            r1 = (AnonymousClass61Q) iDxCallableShape15S0100000_3_I1.A00;
            r1.A0K.A02.A01();
        }
        return r1.A0K;
    }

    public static /* bridge */ /* synthetic */ Object A0A(IDxCallableShape15S0100000_3_I1 iDxCallableShape15S0100000_3_I1) {
        AnonymousClass61Q r1;
        try {
            r1 = (AnonymousClass61Q) iDxCallableShape15S0100000_3_I1.A00;
            C1310360y r0 = r1.A09;
            if (r0 != null) {
                r0.A03();
                r1.A09 = null;
            } else {
                r1.A0K.A02.A01();
            }
        } catch (Exception unused) {
            r1 = (AnonymousClass61Q) iDxCallableShape15S0100000_3_I1.A00;
            r1.A0K.A02.A01();
        }
        return r1.A0K;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // java.util.concurrent.Callable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object call() {
        /*
        // Method dump skipped, instructions count: 454
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxCallableShape15S0100000_3_I1.call():java.lang.Object");
    }
}
