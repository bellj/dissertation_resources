package com.facebook.redex;

import X.AnonymousClass01J;
import X.AnonymousClass01M;
import X.AnonymousClass01N;
import X.AnonymousClass519;
import X.AnonymousClass5EX;
import X.C53042cM;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.migration.android.view.GoogleMigrateImporterActivity;

/* loaded from: classes3.dex */
public class IDxProviderShape16S0100000_2_I1 implements AnonymousClass01N {
    public Object A00;
    public final int A01;

    public IDxProviderShape16S0100000_2_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public final Object get() {
        float f;
        AnonymousClass5EX r0;
        switch (this.A01) {
            case 0:
                return ((C53042cM) this.A00).A09;
            case 1:
            case 2:
                View view = (View) this.A00;
                return new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            case 3:
                AnonymousClass519 r2 = (AnonymousClass519) this.A00;
                return Float.valueOf(1.0f - (AnonymousClass5EX.A00(r2.A0H) / ((float) ((Rect) r2.A0M.get()).height())));
            case 4:
                return Float.valueOf(AnonymousClass5EX.A00(((AnonymousClass519) this.A00).A0C) * 0.45f);
            case 5:
                AnonymousClass519 r22 = (AnonymousClass519) this.A00;
                return Float.valueOf(1.0f - (AnonymousClass5EX.A00(r22.A0F) / ((float) ((Rect) r22.A0L.get()).height())));
            case 6:
                AnonymousClass519 r23 = (AnonymousClass519) this.A00;
                f = (float) r23.A04.getDimensionPixelSize(R.dimen.space_base);
                r0 = r23.A0L;
                break;
            case 7:
                AnonymousClass519 r24 = (AnonymousClass519) this.A00;
                f = (float) r24.A04.getDimensionPixelSize(R.dimen.space_base);
                r0 = r24.A0M;
                break;
            case 8:
                return Float.valueOf(AnonymousClass5EX.A00(((AnonymousClass519) this.A00).A0C) * 0.55f);
            case 9:
                return ((GoogleMigrateImporterActivity) this.A00).A0A(R.drawable.ios_to_android_finished);
            case 10:
                return ((GoogleMigrateImporterActivity) this.A00).A0A(R.drawable.ios_to_android_error);
            case 11:
                return ((GoogleMigrateImporterActivity) this.A00).A0A(R.drawable.ios_to_android_in_progress);
            case 12:
                return ((AnonymousClass01J) AnonymousClass01M.A00(((Context) this.A00).getApplicationContext(), AnonymousClass01J.class)).A9Q.get();
            default:
                return null;
        }
        return Float.valueOf(f - ((float) ((Rect) r0.get()).top));
    }
}
