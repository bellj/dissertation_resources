package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AnonymousClass12H;
import X.AnonymousClass1PE;
import X.AnonymousClass1V0;
import X.C19990v2;
import X.C25621Ac;
import X.C26651Eh;
import X.EnumC35661iT;
import com.whatsapp.jid.Jid;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1301100_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public long A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public String A05;
    public final int A06;

    public RunnableBRunnable0Shape0S1301100_I0(Object obj, Object obj2, Object obj3, String str, int i, int i2, long j) {
        this.A06 = i2;
        this.A02 = obj;
        this.A03 = obj2;
        this.A05 = str;
        this.A01 = j;
        this.A04 = obj3;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass12H r1;
        int i;
        switch (this.A06) {
            case 0:
                C25621Ac r7 = (C25621Ac) this.A02;
                AbstractC14640lm r6 = (AbstractC14640lm) this.A03;
                String str = this.A05;
                long j = this.A01;
                AnonymousClass1V0 r4 = (AnonymousClass1V0) this.A04;
                int i2 = this.A00;
                Long valueOf = Long.valueOf(j);
                C19990v2 r12 = r7.A02;
                AnonymousClass1PE A06 = r12.A06(r6);
                if (A06 == null) {
                    A06 = new AnonymousClass1PE(r6);
                    A06.A0e = str;
                    r12.A0C(A06, r6);
                }
                A06.A0e = str;
                A06.A01 = i2;
                if (r4 != null) {
                    A06.A0b = r4;
                }
                r7.A02(A06, r6, valueOf);
                return;
            case 1:
                C26651Eh r62 = (C26651Eh) this.A02;
                String str2 = this.A05;
                long j2 = this.A01;
                r62.A18.A0F(str2, 501);
                r62.A11.A02((Jid) this.A04, str2, j2);
                return;
            case 2:
                C26651Eh r72 = (C26651Eh) this.A02;
                AbstractC15340mz r63 = (AbstractC15340mz) this.A03;
                int i3 = this.A00;
                Jid jid = (Jid) this.A04;
                String str3 = this.A05;
                long j3 = this.A01;
                if (r63 != null) {
                    if (i3 == 9) {
                        r63.A0O = EnumC35661iT.RELAY;
                        r1 = r72.A0i;
                        i = 25;
                    } else {
                        r1 = r72.A0i;
                        i = 26;
                    }
                    r1.A08(r63, i);
                }
                r72.A05(jid, str3, j3);
                return;
            default:
                return;
        }
    }
}
