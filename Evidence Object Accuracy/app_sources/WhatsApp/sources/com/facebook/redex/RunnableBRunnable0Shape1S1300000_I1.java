package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC28871Pi;
import X.AbstractC52172aN;
import X.AnonymousClass12X;
import X.AnonymousClass12Z;
import X.AnonymousClass19X;
import X.AnonymousClass1IS;
import X.AnonymousClass23Z;
import X.AnonymousClass2ET;
import X.AnonymousClass2EV;
import X.AnonymousClass3H5;
import X.AnonymousClass3J3;
import X.AnonymousClass3M3;
import X.AnonymousClass4A0;
import X.AnonymousClass5WG;
import X.C12960it;
import X.C12970iu;
import X.C15380n4;
import X.C15650ng;
import X.C16700pc;
import X.C26241Co;
import X.C58272oQ;
import X.C58432p3;
import X.C615530u;
import X.C64573Gb;
import X.C68863Xa;
import X.C72263eH;
import X.EnumC87334Bc;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.net.Uri;
import android.util.Pair;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.catalogcategory.view.viewmodel.CatalogAllCategoryViewModel;
import com.whatsapp.conversation.conversationrow.messagerating.MessageRatingViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S1300000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public String A03;
    public final int A04;

    public RunnableBRunnable0Shape1S1300000_I1(Object obj, Object obj2, Object obj3, String str, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A03 = str;
        this.A02 = obj3;
    }

    @Override // java.lang.Runnable
    public void run() {
        String str;
        int i;
        boolean z;
        String str2;
        int i2;
        String str3;
        C26241Co r4;
        Integer num;
        switch (this.A04) {
            case 0:
                try {
                    RunnableBRunnable0Shape10S0200000_I1 runnableBRunnable0Shape10S0200000_I1 = new RunnableBRunnable0Shape10S0200000_I1(this, 28, C64573Gb.A00(this.A03));
                    if (AnonymousClass3J3.A03()) {
                        runnableBRunnable0Shape10S0200000_I1.run();
                        return;
                    } else {
                        AnonymousClass3J3.A00().post(runnableBRunnable0Shape10S0200000_I1);
                        return;
                    }
                } catch (IllegalArgumentException unused) {
                    RunnableBRunnable0Shape14S0100000_I1 runnableBRunnable0Shape14S0100000_I1 = new RunnableBRunnable0Shape14S0100000_I1(this, 10);
                    if (AnonymousClass3J3.A03()) {
                        runnableBRunnable0Shape14S0100000_I1.run();
                        return;
                    } else {
                        AnonymousClass3J3.A00().post(runnableBRunnable0Shape14S0100000_I1);
                        return;
                    }
                }
            case 1:
                C58272oQ r5 = (C58272oQ) this.A00;
                Uri uri = (Uri) this.A01;
                String str4 = this.A03;
                View view = (View) this.A02;
                ClipboardManager A0B = r5.A08.A0B();
                if (A0B != null) {
                    try {
                        if ("wapay".equals(str4)) {
                            str = uri.getLastPathSegment();
                            i = R.string.payment_id_copied;
                        } else if ("tel".equals(str4)) {
                            str = uri.getSchemeSpecificPart();
                            i = R.string.phone_copied;
                            if (r5 instanceof C58432p3) {
                                C58432p3 r1 = (C58432p3) r5;
                                r1.A00.A0c.A00(Boolean.valueOf(r1.A01.A0z.A02), null, 2);
                            }
                        } else {
                            str = r5.A09;
                            i = R.string.link_copied;
                        }
                        Pair A0D = C12960it.A0D(str, i);
                        CharSequence charSequence = (CharSequence) A0D.first;
                        A0B.setPrimaryClip(ClipData.newPlainText(charSequence, charSequence));
                        ((AbstractC52172aN) r5).A04 = false;
                        view.invalidate();
                        r5.A07.A07(C12960it.A05(A0D.second), 0);
                        return;
                    } catch (NullPointerException | SecurityException e) {
                        Log.e("linktouchablespan/copy/", e);
                        return;
                    }
                } else {
                    return;
                }
            case 2:
                C68863Xa r52 = (C68863Xa) this.A00;
                AnonymousClass23Z r12 = (AnonymousClass23Z) this.A01;
                String str5 = this.A03;
                AnonymousClass5WG r3 = (AnonymousClass5WG) this.A02;
                AnonymousClass12X r2 = r12.A03;
                AnonymousClass12Z r0 = r12.A02;
                int i3 = r12.A00;
                if (i3 == 0) {
                    if (r0 != null) {
                        AnonymousClass3M3 r13 = (AnonymousClass3M3) r0.A00;
                        if (r13 != null) {
                            r13.A00 = str5;
                            r3.AQM(r13);
                            r52.A00.A01.A08(str5, r13);
                            return;
                        }
                    } else {
                        return;
                    }
                } else if (i3 != 1) {
                    return;
                }
                if (r2 != null) {
                    AnonymousClass19X.A00(r52.A00, r3, str5, r2.A00, false);
                    return;
                }
                return;
            case 3:
                Throwable th = (Throwable) this.A01;
                String str6 = this.A03;
                Log.e(th);
                HashMap A11 = C12970iu.A11();
                A11.put(-1, new AnonymousClass3H5(C12960it.A0d(th.getLocalizedMessage(), C12960it.A0k("Failed to fetch metadata: "))));
                AnonymousClass19X.A00(((C68863Xa) this.A00).A00, (AnonymousClass5WG) this.A02, str6, A11, true);
                return;
            case 4:
                CatalogAllCategoryViewModel catalogAllCategoryViewModel = (CatalogAllCategoryViewModel) this.A00;
                String str7 = this.A03;
                UserJid userJid = (UserJid) this.A01;
                AnonymousClass4A0 r02 = (AnonymousClass4A0) this.A02;
                C16700pc.A0F(catalogAllCategoryViewModel, str7);
                C16700pc.A0H(userJid, r02);
                AnonymousClass2ET r32 = catalogAllCategoryViewModel.A04;
                C72263eH r22 = new C72263eH(r02, catalogAllCategoryViewModel, userJid);
                Set singleton = Collections.singleton(str7);
                C16700pc.A0B(singleton);
                r32.A00(userJid, singleton, new AnonymousClass2EV(str7, r22));
                return;
            case 5:
                MessageRatingViewModel messageRatingViewModel = (MessageRatingViewModel) this.A00;
                String str8 = this.A03;
                Object obj = this.A02;
                C15650ng r23 = messageRatingViewModel.A03;
                AbstractC15340mz A03 = r23.A0K.A03(new AnonymousClass1IS((AbstractC14640lm) this.A01, str8, false));
                if (A03 != null) {
                    str3 = C15380n4.A03(A03.A0C());
                    r4 = messageRatingViewModel.A02;
                    if (A03 instanceof AbstractC28871Pi) {
                        str2 = ((AbstractC28871Pi) A03).AH7().A03;
                    } else {
                        str2 = null;
                    }
                    z = C12960it.A1W(messageRatingViewModel.A04.A00(A03.A11));
                    int i4 = 1;
                    i2 = 1;
                    if (obj != EnumC87334Bc.LONG_PRESS) {
                        i4 = 2;
                    }
                    num = Integer.valueOf(i4);
                    break;
                } else {
                    return;
                }
            case 6:
                MessageRatingViewModel messageRatingViewModel2 = (MessageRatingViewModel) this.A00;
                String str9 = this.A03;
                Object obj2 = this.A02;
                C15650ng r24 = messageRatingViewModel2.A03;
                AbstractC15340mz A032 = r24.A0K.A03(new AnonymousClass1IS((AbstractC14640lm) this.A01, str9, false));
                if (A032 != null) {
                    str3 = C15380n4.A03(A032.A0C());
                    r4 = messageRatingViewModel2.A02;
                    if (A032 instanceof AbstractC28871Pi) {
                        str2 = ((AbstractC28871Pi) A032).AH7().A03;
                    } else {
                        str2 = null;
                    }
                    z = C12960it.A1W(messageRatingViewModel2.A04.A00(A032.A11));
                    i2 = C12970iu.A0h();
                    int i5 = 2;
                    if (obj2 == EnumC87334Bc.LONG_PRESS) {
                        i5 = 1;
                    }
                    num = Integer.valueOf(i5);
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        C615530u r14 = new C615530u();
        r14.A05 = str3;
        r14.A02 = i2;
        r14.A03 = num;
        r14.A01 = null;
        r14.A06 = str2;
        r14.A00 = Boolean.valueOf(z);
        r4.A00.A05(r14);
    }
}
