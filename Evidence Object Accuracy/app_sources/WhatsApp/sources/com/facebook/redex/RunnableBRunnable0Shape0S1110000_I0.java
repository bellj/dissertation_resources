package com.facebook.redex;

import X.AbstractC21570xd;
import X.AbstractC49122Jj;
import X.AnonymousClass009;
import X.AnonymousClass19Z;
import X.AnonymousClass1VW;
import X.C14320lF;
import X.C15370n3;
import X.C15380n4;
import X.C15570nT;
import X.C16310on;
import X.C21580xe;
import X.C22410z2;
import X.C244515o;
import X.C26391De;
import X.C28181Ma;
import X.C29631Ua;
import X.C49132Jk;
import android.database.Cursor;
import android.os.Message;
import com.whatsapp.calling.service.VoiceService$VoiceServiceEventCallback;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.Voip;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1110000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public String A01;
    public boolean A02;
    public final int A03;

    public RunnableBRunnable0Shape0S1110000_I0(Object obj, String str, int i, boolean z) {
        this.A03 = i;
        this.A00 = obj;
        this.A01 = str;
        this.A02 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        switch (this.A03) {
            case 0:
                boolean z = this.A02;
                String str = this.A01;
                C14320lF r2 = (C14320lF) this.A00;
                HashMap hashMap = C49132Jk.A01;
                if (z) {
                    obj = hashMap.remove(str);
                } else {
                    obj = hashMap.get(str);
                }
                AbstractCollection abstractCollection = (AbstractCollection) obj;
                if (abstractCollection != null) {
                    Iterator it = abstractCollection.iterator();
                    while (it.hasNext()) {
                        AbstractC49122Jj r0 = (AbstractC49122Jj) it.next();
                        if (r0 != null) {
                            r0.ATN(r2, z);
                        }
                    }
                }
                C49132Jk.A00.put(str, r2);
                return;
            case 1:
                C29631Ua r02 = (C29631Ua) this.A00;
                boolean z2 = this.A02;
                String str2 = this.A01;
                if (z2) {
                    r02.A0X();
                    r02.A2T.setRequestedCamera2SupportLevel(r02.A2R.A02());
                }
                int previewCallLink = Voip.previewCallLink(str2, z2);
                if (previewCallLink != 0) {
                    StringBuilder sb = new StringBuilder("voip/actionPreviewCallLink failed error: ");
                    sb.append(previewCallLink);
                    Log.e(sb.toString());
                    return;
                }
                return;
            case 2:
                boolean z3 = this.A02;
                String str3 = this.A01;
                C15570nT r03 = ((C29631Ua) this.A00).A1W;
                if (r03 != null) {
                    r03.A08();
                    if (r03.A01 == null) {
                        return;
                    }
                    if (z3) {
                        Voip.endCallAndAcceptPendingCall(str3);
                        return;
                    } else {
                        Voip.acceptCall();
                        return;
                    }
                } else {
                    return;
                }
            case 3:
                VoiceService$VoiceServiceEventCallback.$r8$lambda$4TVtommTsTQRVm16iyPmWUvvTqU((VoiceService$VoiceServiceEventCallback) this.A00, this.A01, this.A02);
                return;
            case 4:
                C22410z2 r4 = (C22410z2) this.A00;
                String str4 = this.A01;
                boolean z4 = this.A02;
                ArrayList arrayList = new ArrayList();
                C21580xe r7 = r4.A04.A06;
                r7.A0S(arrayList, 0, false, false);
                ArrayList arrayList2 = new ArrayList();
                Iterator it2 = r7.A0B().iterator();
                while (it2.hasNext()) {
                    C15370n3 r1 = (C15370n3) it2.next();
                    if (C15380n4.A0K(r1.A0D)) {
                        arrayList2.add(r1);
                    }
                }
                arrayList2.removeAll(arrayList);
                arrayList.addAll(arrayList2);
                C28181Ma r6 = new C28181Ma(true);
                r6.A03();
                arrayList.size();
                C16310on A01 = ((AbstractC21570xd) r7).A00.get();
                try {
                    Cursor A03 = AbstractC21570xd.A03(A01, "wa_contacts LEFT JOIN wa_vnames ON (wa_contacts.jid = wa_vnames.jid) LEFT JOIN wa_group_descriptions ON (wa_contacts.jid = wa_group_descriptions.jid) LEFT JOIN wa_group_admin_settings ON (wa_contacts.jid = wa_group_admin_settings.jid)", "wa_contacts.jid LIKE '%broadcast'", null, "CONTACTS", C21580xe.A08, null);
                    if (A03 == null) {
                        AnonymousClass009.A07("contact-mgr-db/unable to get all broadcastlist chats");
                        A01.close();
                    } else {
                        while (A03.moveToNext()) {
                            C15370n3 A00 = AnonymousClass1VW.A00(A03);
                            if (A00.A0D != null) {
                                arrayList.add(A00);
                            }
                        }
                        A03.close();
                        A01.close();
                        arrayList.size();
                        r6.A00();
                    }
                    List A012 = r4.A01(arrayList);
                    String A002 = r4.A00();
                    C244515o r13 = r4.A0A;
                    int i = 2;
                    if (z4) {
                        i = 8;
                    }
                    r13.A01(str4, A002, A012, i, z4);
                    return;
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            case 5:
                boolean z5 = this.A02;
                String str5 = this.A01;
                Message obtain = Message.obtain();
                obtain.arg1 = z5 ? 1 : 0;
                obtain.obj = str5;
                ((AnonymousClass19Z) this.A00).A08.A00(new C26391De(obtain, "preview_call_link"));
                return;
            default:
                return;
        }
    }
}
