package com.facebook.redex;

import X.AnonymousClass01d;
import X.AnonymousClass1J7;
import X.AnonymousClass21U;
import X.AnonymousClass21W;
import X.AnonymousClass23N;
import X.AnonymousClass2B8;
import X.AnonymousClass367;
import X.AnonymousClass384;
import X.AnonymousClass3FG;
import X.AnonymousClass3ZS;
import X.AnonymousClass3ZT;
import X.AnonymousClass417;
import X.AnonymousClass418;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C16700pc;
import X.C470128o;
import X.C628838y;
import X.C64523Fw;
import X.C97994hy;
import X.View$OnClickListenerC55222hz;
import android.content.Context;
import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.whatsapp.R;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.deeplink.DeepLinkActivity;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0101000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape1S0101000_I1(C628838y r2, int i, int i2) {
        this.A02 = i2;
        if (11 - i2 != 0) {
            this.A01 = r2;
            this.A00 = i;
            return;
        }
        this.A01 = r2;
        this.A00 = R.string.share_failed;
    }

    public RunnableBRunnable0Shape1S0101000_I1(Object obj, int i, int i2) {
        this.A02 = i2;
        this.A00 = i;
        this.A01 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AcceptInviteLinkActivity acceptInviteLinkActivity;
        int i;
        int i2;
        switch (this.A02) {
            case 0:
                int i3 = this.A00;
                AnonymousClass3FG r2 = ((C97994hy) this.A01).A01;
                if (i3 == -3) {
                    i2 = 3;
                } else if (i3 == -2) {
                    r2.A01(0);
                    i2 = 2;
                } else if (i3 == -1) {
                    r2.A01(-1);
                    r2.A00();
                    return;
                } else if (i3 != 1) {
                    Log.w("AudioFocusManager", C12960it.A0W(i3, "Unknown focus change type: "));
                    return;
                } else {
                    r2.A02(1);
                    r2.A01(1);
                    return;
                }
                r2.A02(i2);
                return;
            case 1:
                AnonymousClass3ZT r22 = (AnonymousClass3ZT) this.A01;
                int i4 = this.A00;
                if (i4 != 500) {
                    switch (i4) {
                        case 403:
                            acceptInviteLinkActivity = r22.A00;
                            i = R.string.subgroup_info_error_not_part_of_parent_group;
                            break;
                        case 404:
                            acceptInviteLinkActivity = r22.A00;
                            i = R.string.subgroup_info_error_parent_group_does_not_exist;
                            break;
                        case 405:
                            acceptInviteLinkActivity = r22.A00;
                            i = R.string.subgroup_info_error_subgroup_does_not_exist;
                            break;
                        default:
                            return;
                    }
                } else {
                    acceptInviteLinkActivity = r22.A00;
                    i = R.string.subgroup_info_error_internal_server;
                }
                acceptInviteLinkActivity.A2f(i);
                return;
            case 2:
                ((StaggeredGridLayoutManager) this.A01).A1K(this.A00);
                return;
            case 3:
                C64523Fw r4 = (C64523Fw) this.A01;
                int i5 = this.A00;
                if (i5 == 5 && !r4.A0F.A0J) {
                    return;
                }
                if (!r4.A08) {
                    r4.A02 = i5;
                    r4.A05.A01(r4.A0C, i5);
                    r4.A07(C12980iv.A1V(i5, 5));
                    return;
                }
                com.whatsapp.util.Log.i(C12960it.A0W(i5, "CallControlBottomSheetBehaviorController setBottomSheetState "));
                r4.A0F.A0M(i5);
                return;
            case 4:
                int i6 = this.A00;
                AnonymousClass1J7 r1 = (AnonymousClass1J7) this.A01;
                C16700pc.A0E(r1, 1);
                if (i6 == -1) {
                    r1.AJ4(AnonymousClass418.A00);
                    return;
                } else {
                    r1.AJ4(AnonymousClass417.A00);
                    return;
                }
            case 5:
            case 6:
            case 7:
                DeepLinkActivity deepLinkActivity = (DeepLinkActivity) ((AnonymousClass2B8) this.A01);
                deepLinkActivity.AaN();
                deepLinkActivity.Ado(R.string.invalid_deep_link);
                deepLinkActivity.A00.removeMessages(1);
                return;
            case 8:
                int i7 = this.A00;
                AnonymousClass384 r12 = ((AnonymousClass3ZS) this.A01).A00;
                r12.A08(i7, r12.A06);
                return;
            case 9:
                View$OnClickListenerC55222hz r8 = (View$OnClickListenerC55222hz) this.A01;
                int i8 = this.A00;
                AnonymousClass21W r42 = r8.A05;
                AnonymousClass21U r13 = r42.A0A;
                int i9 = r13.A01;
                RecyclerView recyclerView = r13.A0Q;
                View$OnClickListenerC55222hz r10 = (View$OnClickListenerC55222hz) recyclerView.A0C(i9);
                if (r10 != null) {
                    r10.A04.A04(false, true);
                    r10.A00.animate().scaleX(1.0f).scaleY(1.0f).setDuration(100).start();
                } else {
                    r42.A03(i9);
                }
                int size = C470128o.A00.size();
                if (i8 > 0 && i8 < (size >> 1)) {
                    i8--;
                } else if (i8 > (size >> 1) && i8 < size - 1) {
                    i8++;
                }
                recyclerView.A0Z(i8);
                SelectionCheckView selectionCheckView = r8.A04;
                selectionCheckView.setScaleX(1.0f);
                selectionCheckView.setScaleY(1.0f);
                selectionCheckView.A04(true, true);
                r8.A00.animate().scaleX(r42.A05).scaleY(r42.A04).setDuration(100).start();
                return;
            case 10:
                int i10 = this.A00;
                AnonymousClass21W r3 = ((View$OnClickListenerC55222hz) this.A01).A05;
                r3.A02[i10 - 1] = false;
                r3.A03(i10);
                return;
            case 11:
                ((C628838y) this.A01).A05.A07(this.A00, 0);
                return;
            case 12:
                ((C628838y) this.A01).A05.A05(this.A00, 0);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                AnonymousClass367 r14 = (AnonymousClass367) this.A01;
                int i11 = this.A00;
                Context context = r14.A05.getContext();
                AnonymousClass01d r5 = r14.A06;
                Object[] A1b = C12970iu.A1b();
                C12960it.A1O(A1b, i11);
                AnonymousClass23N.A00(context, r5, r14.A07.A0I(A1b, R.plurals.text_limit_characters_remaining_description, (long) i11));
                return;
            default:
                return;
        }
    }
}
