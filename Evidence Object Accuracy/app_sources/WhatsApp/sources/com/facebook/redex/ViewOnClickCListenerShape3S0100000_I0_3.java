package com.facebook.redex;

import X.AbstractC116115Ue;
import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC44071y9;
import X.AbstractC51412Uo;
import X.ActivityC000800j;
import X.ActivityC000900k;
import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass01I;
import X.AnonymousClass1SF;
import X.AnonymousClass1V8;
import X.AnonymousClass1VY;
import X.AnonymousClass1W9;
import X.AnonymousClass1YT;
import X.AnonymousClass27T;
import X.AnonymousClass34P;
import X.AnonymousClass3J8;
import X.AnonymousClass4HD;
import X.C14960mK;
import X.C15370n3;
import X.C15550nR;
import X.C15570nT;
import X.C15580nU;
import X.C16030oK;
import X.C17220qS;
import X.C248917h;
import X.C254219i;
import X.C25991Bp;
import X.C27631Ih;
import X.C30751Yr;
import X.C35221hT;
import X.C36021jC;
import X.C38491oB;
import X.C43951xu;
import X.C44061y8;
import X.C44241ya;
import X.C51122Sx;
import X.C51402Un;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.Conversation;
import com.whatsapp.PushnameEmojiBlacklistDialogFragment;
import com.whatsapp.R;
import com.whatsapp.emoji.EmojiEditTextBottomSheetDialogFragment;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.notification.PopupNotificationViewPager;
import com.whatsapp.profile.ProfileInfoActivity;
import com.whatsapp.profile.ProfilePhotoReminder;
import com.whatsapp.qrcode.WaQrScannerView;
import com.whatsapp.qrcode.contactqr.QrScanCodeFragment;
import com.whatsapp.qrcode.contactqr.ScannedCodeDialogFragment;
import com.whatsapp.quickcontact.QuickContactActivity;
import com.whatsapp.registration.RegisterName;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.report.DeleteReportConfirmationDialogFragment;
import com.whatsapp.report.ReportActivity;
import com.whatsapp.search.SearchFragment;
import com.whatsapp.search.SearchViewModel;
import com.whatsapp.search.views.TokenizedSearchInput;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape3S0100000_I0_3 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape3S0100000_I0_3(TokenizedSearchInput tokenizedSearchInput, int i) {
        this.A01 = i;
        switch (i) {
            case 43:
            case 44:
            case 45:
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
            case 47:
                this.A00 = tokenizedSearchInput;
                return;
            default:
                this.A00 = tokenizedSearchInput;
                return;
        }
    }

    public ViewOnClickCListenerShape3S0100000_I0_3(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        PopupNotification popupNotification;
        C30751Yr r0;
        C35221hT r7;
        boolean z;
        switch (this.A01) {
            case 0:
                Activity activity = (Activity) this.A00;
                Log.i("ExportMigrationDataExportedActivity/dataExportReadyFinishActivity");
                activity.setResult(100);
                popupNotification = activity;
                break;
            case 1:
                ((PopupNotification) this.A00).A2V();
                return;
            case 2:
                PopupNotification popupNotification2 = (PopupNotification) this.A00;
                popupNotification2.A1D.A00();
                popupNotification2.A2X();
                popupNotification = popupNotification2;
                break;
            case 3:
                PopupNotification popupNotification3 = (PopupNotification) this.A00;
                if (!popupNotification3.A2d()) {
                    popupNotification3.A13.A01(true);
                    AbstractC15340mz r02 = popupNotification3.A16;
                    if (r02 != null) {
                        popupNotification3.A1L.add(r02.A0z);
                    }
                    C15370n3 r2 = popupNotification3.A0p;
                    if (r2 != null) {
                        popupNotification3.A1J.add(r2.A0B(AbstractC14640lm.class));
                    }
                    popupNotification3.A1P = true;
                    if (popupNotification3.A1M.size() > 1) {
                        PopupNotificationViewPager popupNotificationViewPager = popupNotification3.A10;
                        popupNotificationViewPager.A0F(popupNotificationViewPager.getCurrentItem() + 1, true);
                        int currentItem = popupNotification3.A10.getCurrentItem();
                        if (currentItem >= popupNotification3.A1M.size()) {
                            currentItem = 0;
                        }
                        popupNotification3.A2Z(currentItem);
                        return;
                    }
                    return;
                }
                return;
            case 4:
                PopupNotification popupNotification4 = (PopupNotification) this.A00;
                if (!popupNotification4.A2d()) {
                    popupNotification4.A13.A01(true);
                    AbstractC15340mz r03 = popupNotification4.A16;
                    if (r03 != null) {
                        popupNotification4.A1L.add(r03.A0z);
                    }
                    C15370n3 r22 = popupNotification4.A0p;
                    if (r22 != null) {
                        popupNotification4.A1J.add(r22.A0B(AbstractC14640lm.class));
                    }
                    popupNotification4.A1P = true;
                    if (popupNotification4.A1M.size() > 1) {
                        PopupNotificationViewPager popupNotificationViewPager2 = popupNotification4.A10;
                        popupNotificationViewPager2.A0F(popupNotificationViewPager2.getCurrentItem() - 1, true);
                        int currentItem2 = popupNotification4.A10.getCurrentItem();
                        if (currentItem2 < 0) {
                            currentItem2 = popupNotification4.A1M.size() - 1;
                        }
                        popupNotification4.A2Z(currentItem2);
                        return;
                    }
                    return;
                }
                return;
            case 5:
                PopupNotification popupNotification5 = (PopupNotification) this.A00;
                popupNotification5.A1D.A00();
                String trim = popupNotification5.A0c.getText().toString().trim();
                C15370n3 r23 = popupNotification5.A0p;
                if (r23 != null && trim.length() > 0) {
                    Conversation.A5E.put(r23.A0B(AbstractC14640lm.class), trim);
                }
                popupNotification5.A0G.A07(popupNotification5, new C14960mK().A0g(popupNotification5, popupNotification5.A0k.A01(popupNotification5.A16.A0z.A00)));
                popupNotification5.A2X();
                popupNotification5.finish();
                return;
            case 6:
                PopupNotification popupNotification6 = (PopupNotification) this.A00;
                popupNotification6.A13.A01(true);
                AbstractC15340mz r04 = popupNotification6.A16;
                if (r04 != null) {
                    popupNotification6.A1L.add(r04.A0z);
                }
                C15370n3 r24 = popupNotification6.A0p;
                if (r24 != null) {
                    popupNotification6.A1J.add(r24.A0B(AbstractC14640lm.class));
                    return;
                }
                return;
            case 7:
                ProfileInfoActivity profileInfoActivity = (ProfileInfoActivity) this.A00;
                profileInfoActivity.A0D.A06(profileInfoActivity, profileInfoActivity.A09, 12);
                return;
            case 8:
                ActivityC13790kL r1 = (ActivityC13790kL) this.A00;
                r1.Adm(EmojiEditTextBottomSheetDialogFragment.A00(r1.A01.A05(), AnonymousClass4HD.A01, 0, R.string.settings_dialog_title, R.string.no_empty_name, 25, 8193));
                return;
            case 9:
                ProfileInfoActivity.A03((ProfileInfoActivity) this.A00);
                return;
            case 10:
                ProfilePhotoReminder profilePhotoReminder = (ProfilePhotoReminder) this.A00;
                profilePhotoReminder.A0G.A06(profilePhotoReminder, profilePhotoReminder.A09, 12);
                return;
            case 11:
                ProfilePhotoReminder profilePhotoReminder2 = (ProfilePhotoReminder) this.A00;
                String trim2 = profilePhotoReminder2.A05.getText().toString().trim();
                if (C38491oB.A03(trim2, AnonymousClass4HD.A01)) {
                    Log.w("registername/checkmarks in pushname");
                    profilePhotoReminder2.Adm(PushnameEmojiBlacklistDialogFragment.A00(trim2));
                } else if (trim2.length() == 0) {
                    Log.w("registername/no-pushname");
                    ((ActivityC13810kN) profilePhotoReminder2).A05.A07(R.string.register_failure_noname, 0);
                } else if (!trim2.equals(((ActivityC13790kL) profilePhotoReminder2).A01.A05())) {
                    ((ActivityC13830kP) profilePhotoReminder2).A05.Ab2(new RunnableBRunnable0Shape0S1100000_I0(31, trim2, profilePhotoReminder2));
                    return;
                }
                profilePhotoReminder2.finish();
                return;
            case 12:
            case 19:
            case 37:
                ((DialogFragment) this.A00).A1B();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                QrScanCodeFragment qrScanCodeFragment = (QrScanCodeFragment) this.A00;
                qrScanCodeFragment.A08 = null;
                qrScanCodeFragment.A06.Aao();
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                QrScanCodeFragment qrScanCodeFragment2 = (QrScanCodeFragment) this.A00;
                WaQrScannerView waQrScannerView = qrScanCodeFragment2.A06;
                if (waQrScannerView.A01.Aee()) {
                    waQrScannerView.Af2();
                    qrScanCodeFragment2.A1B();
                    return;
                }
                return;
            case 15:
                ActivityC000900k A0B = ((AnonymousClass01E) this.A00).A0B();
                if (A0B instanceof AnonymousClass34P) {
                    AnonymousClass34P r3 = (AnonymousClass34P) A0B;
                    Intent intent = new Intent();
                    intent.setClassName(r3.getPackageName(), "com.whatsapp.gallerypicker.GalleryPickerLauncher");
                    r3.A0X = true;
                    r3.startActivityForResult(intent, 2);
                    return;
                }
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 18:
                ScannedCodeDialogFragment scannedCodeDialogFragment = (ScannedCodeDialogFragment) this.A00;
                ActivityC000900k A0B2 = scannedCodeDialogFragment.A0B();
                A0B2.startActivity(new C14960mK().A0f(A0B2, scannedCodeDialogFragment.A0I));
                return;
            case 17:
                ScannedCodeDialogFragment scannedCodeDialogFragment2 = (ScannedCodeDialogFragment) this.A00;
                int i = scannedCodeDialogFragment2.A00;
                if (i != 0) {
                    if (i != 1) {
                        if (i == 2) {
                            Context A01 = scannedCodeDialogFragment2.A01();
                            boolean isEmpty = TextUtils.isEmpty(scannedCodeDialogFragment2.A0S);
                            C14960mK r25 = new C14960mK();
                            UserJid userJid = scannedCodeDialogFragment2.A0L;
                            if (!isEmpty) {
                                scannedCodeDialogFragment2.A0v(r25.A0k(A01, userJid, scannedCodeDialogFragment2.A0S));
                            } else {
                                C51122Sx.A00(r25.A0i(A01, userJid), scannedCodeDialogFragment2);
                            }
                            scannedCodeDialogFragment2.A0Q.Ab2(new RunnableBRunnable0Shape10S0100000_I0_10(scannedCodeDialogFragment2, 18));
                            scannedCodeDialogFragment2.A1B();
                        } else {
                            throw new IllegalArgumentException("Unhandled type");
                        }
                    }
                } else if (scannedCodeDialogFragment2.A0I.A0C != null) {
                    scannedCodeDialogFragment2.A0v(C14960mK.A02(scannedCodeDialogFragment2.A0C()).addFlags(603979776));
                    C51122Sx.A00(new C14960mK().A0i(scannedCodeDialogFragment2.A01(), scannedCodeDialogFragment2.A0L), scannedCodeDialogFragment2);
                } else {
                    C254219i r32 = scannedCodeDialogFragment2.A0P;
                    String A04 = C248917h.A04(scannedCodeDialogFragment2.A0L);
                    AnonymousClass009.A05(A04);
                    Intent A012 = r32.A01(A04, scannedCodeDialogFragment2.A0I.A0U, true, false);
                    A012.putExtra("finishActivityOnSaveCompleted", true);
                    scannedCodeDialogFragment2.startActivityForResult(A012, 1);
                    scannedCodeDialogFragment2.A0O.A02(true, 11);
                    return;
                }
                scannedCodeDialogFragment2.A1B();
                return;
            case C43951xu.A01:
                QuickContactActivity quickContactActivity = (QuickContactActivity) this.A00;
                double doubleExtra = quickContactActivity.getIntent().getDoubleExtra("location_latitude", 0.0d);
                double doubleExtra2 = quickContactActivity.getIntent().getDoubleExtra("location_longitude", 0.0d);
                if (doubleExtra == 0.0d && doubleExtra2 == 0.0d) {
                    C16030oK r8 = quickContactActivity.A0S;
                    AbstractC14640lm A013 = AbstractC14640lm.A01(quickContactActivity.getIntent().getStringExtra("gjid"));
                    AnonymousClass009.A05(A013);
                    Jid A0B3 = quickContactActivity.A0M.A0B(UserJid.class);
                    AnonymousClass009.A05(A0B3);
                    synchronized (r8.A0W) {
                        Map map = (Map) r8.A0A().get(A013);
                        r0 = (map == null || (r7 = (C35221hT) map.get(A0B3)) == null || !C16030oK.A01(r7.A00, r8.A0K.A00())) ? null : (C30751Yr) r8.A0h.get(r7.A01);
                    }
                    if (r0 != null) {
                        doubleExtra = r0.A00;
                        doubleExtra2 = r0.A01;
                    }
                }
                if (!(doubleExtra == 0.0d || doubleExtra2 == 0.0d)) {
                    quickContactActivity.A0T.A09(quickContactActivity, quickContactActivity.A0D.A04(quickContactActivity.A0M), null, doubleExtra, doubleExtra2);
                }
                quickContactActivity.A2g(false);
                return;
            case 21:
                QuickContactActivity.A09((QuickContactActivity) this.A00);
                return;
            case 22:
                ((AbstractC51412Uo) this.A00).A01();
                return;
            case 23:
                QuickContactActivity quickContactActivity2 = ((C51402Un) this.A00).A04;
                C15370n3 r33 = quickContactActivity2.A0M;
                if (r33 == null) {
                    ((ActivityC13810kN) quickContactActivity2).A05.A07(R.string.group_add_contact_failed, 0);
                } else {
                    C254219i r12 = quickContactActivity2.A0c;
                    Jid A0B4 = r33.A0B(AbstractC14640lm.class);
                    AnonymousClass009.A05(A0B4);
                    Intent A00 = r12.A00(r33, (AbstractC14640lm) A0B4, true);
                    A00.setFlags(524288);
                    try {
                        quickContactActivity2.startActivityForResult(A00, 1);
                        quickContactActivity2.A0b.A02(true, 9);
                    } catch (ActivityNotFoundException unused) {
                        C36021jC.A01(quickContactActivity2, 1);
                    }
                }
                quickContactActivity2.finish();
                return;
            case 24:
                QuickContactActivity quickContactActivity3 = ((AbstractC51412Uo) this.A00).A00;
                ((ActivityC13790kL) quickContactActivity3).A00.A08(quickContactActivity3, new C14960mK().A0g(quickContactActivity3, quickContactActivity3.A0M).putExtra("args_conversation_screen_entry_point", 1).putExtra("extra_show_search_on_create", true), "QuickContactActivity");
                quickContactActivity3.A2g(false);
                return;
            case 25:
                QuickContactActivity quickContactActivity4 = ((AbstractC51412Uo) this.A00).A00;
                ((ActivityC13790kL) quickContactActivity4).A00.A07(quickContactActivity4, new C14960mK().A0g(quickContactActivity4, quickContactActivity4.A0M));
                quickContactActivity4.A2g(false);
                return;
            case 26:
                QuickContactActivity quickContactActivity5 = ((AbstractC51412Uo) this.A00).A00;
                AnonymousClass1YT r26 = quickContactActivity5.A0e;
                if (r26 != null) {
                    quickContactActivity5.A0h.A06(quickContactActivity5, r26, 10);
                    return;
                } else {
                    ((ActivityC13810kN) quickContactActivity5).A03.AaV("LinkedCallLogPrefetchNotCompletedOnTime", "quickContactDialog ", false);
                    return;
                }
            case 27:
                QuickContactActivity quickContactActivity6 = ((AbstractC51412Uo) this.A00).A00;
                if (quickContactActivity6.A0R != null) {
                    ((ActivityC13790kL) quickContactActivity6).A00.A07(quickContactActivity6, new C14960mK().A0g(quickContactActivity6, quickContactActivity6.A0M));
                    C15550nR r4 = quickContactActivity6.A0B;
                    C15580nU r5 = quickContactActivity6.A0R;
                    AnonymousClass1SF.A0E(quickContactActivity6, r4, r5, AnonymousClass1SF.A0D(((ActivityC13790kL) quickContactActivity6).A01, quickContactActivity6.A0K, r5), null, 7, true);
                    quickContactActivity6.A2g(false);
                    return;
                }
                return;
            case 28:
                Activity activity2 = (Activity) this.A00;
                Log.i("changenumberoverview/next");
                Intent intent2 = new Intent();
                intent2.setClassName(activity2.getPackageName(), "com.whatsapp.registration.ChangeNumber");
                activity2.startActivity(intent2);
                activity2.finish();
                return;
            case 29:
                ((CompoundButton) ((ActivityC000800j) this.A00).findViewById(R.id.cbx_app_shortcut)).toggle();
                return;
            case C25991Bp.A0S:
                RegisterName registerName = (RegisterName) this.A00;
                registerName.A1J = true;
                registerName.A0z.A02("profile_photo", "tapped");
                registerName.A0m.A07(registerName, registerName.A0V, 12, 1, true, true);
                return;
            case 31:
                Log.i("registername/clicked");
                ((RegisterName) this.A00).A2i();
                return;
            case 32:
                Log.i("registername/init/stack ");
                AnonymousClass01I.A00();
                RegisterName registerName2 = ((AnonymousClass27T) this.A00).A01;
                registerName2.A0b.A00(null, registerName2, "regname-init", false);
                return;
            case 33:
                Activity activity3 = (Activity) this.A00;
                C36021jC.A00(activity3, 130);
                C36021jC.A01(activity3, 126);
                return;
            case 34:
                VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) this.A00;
                if (AnonymousClass3J8.A01(verifyPhoneNumber.A2e())) {
                    verifyPhoneNumber.A3A(2);
                }
                verifyPhoneNumber.A3m(true);
                return;
            case 35:
                VerifyPhoneNumber.A03((VerifyPhoneNumber) this.A00);
                return;
            case 36:
                VerifyPhoneNumber verifyPhoneNumber2 = (VerifyPhoneNumber) this.A00;
                Log.i("verifyvoice/retryverify");
                String code = verifyPhoneNumber2.A0M.getCode();
                verifyPhoneNumber2.A0H.setVisibility(4);
                verifyPhoneNumber2.A3Y(code);
                return;
            case 38:
                ((VerifyTwoFactorAuth) ((AnonymousClass01E) this.A00).A0B()).A2f(1, null, false);
                return;
            case 39:
                VerifyTwoFactorAuth verifyTwoFactorAuth = (VerifyTwoFactorAuth) ((AnonymousClass01E) this.A00).A0B();
                verifyTwoFactorAuth.Adl(VerifyTwoFactorAuth.ConfirmWipe.A00(verifyTwoFactorAuth.A2e()), null);
                return;
            case 40:
                ReportActivity reportActivity = (ReportActivity) this.A00;
                if (((ActivityC13810kN) reportActivity).A05.A0M()) {
                    DeleteReportConfirmationDialogFragment deleteReportConfirmationDialogFragment = new DeleteReportConfirmationDialogFragment();
                    deleteReportConfirmationDialogFragment.A01 = new AbstractC116115Ue() { // from class: X.596
                        @Override // X.AbstractC116115Ue
                        public final void A8m() {
                            ReportActivity reportActivity2 = ReportActivity.this;
                            if (((ActivityC13810kN) reportActivity2).A05.A0M()) {
                                if (reportActivity2.A0N != null) {
                                    reportActivity2.A0N = null;
                                }
                                C48322Fo r27 = new C48322Fo(reportActivity2, ((ActivityC13810kN) reportActivity2).A05, reportActivity2);
                                reportActivity2.A0N = r27;
                                ((ActivityC13830kP) reportActivity2).A05.Aaz(r27, new Void[0]);
                            }
                        }
                    };
                    reportActivity.Adl(deleteReportConfirmationDialogFragment, null);
                    return;
                }
                return;
            case 41:
                BusinessActivityReportViewModel businessActivityReportViewModel = ((ReportActivity) this.A00).A0L;
                if (businessActivityReportViewModel.A03.A0M()) {
                    businessActivityReportViewModel.A01.A0B(1);
                    C44241ya r9 = businessActivityReportViewModel.A0D;
                    if (r9.A04.A0B()) {
                        C17220qS r82 = r9.A06;
                        String A014 = r82.A01();
                        AnonymousClass1V8 r42 = new AnonymousClass1V8("p2b", new AnonymousClass1W9[]{new AnonymousClass1W9("action", "request"), new AnonymousClass1W9("lang", r9.A05.A06())});
                        C15570nT r05 = r9.A03;
                        r05.A08();
                        C27631Ih r06 = r05.A05;
                        AnonymousClass009.A05(r06);
                        r82.A0A(r9, new AnonymousClass1V8(r42, "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("from", r06.getRawString()), new AnonymousClass1W9("xmlns", "w:biz:p2b_report"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("smax_id", "33"), new AnonymousClass1W9("id", A014)}), A014, 267, 32000);
                        z = true;
                    } else {
                        z = false;
                    }
                    StringBuilder sb = new StringBuilder("app/sendRequestReport success:");
                    sb.append(z);
                    Log.i(sb.toString());
                    return;
                }
                return;
            case 42:
                ((SearchFragment) this.A00).A1G.A0W(true);
                return;
            case 43:
                TokenizedSearchInput.A01((TokenizedSearchInput) this.A00);
                return;
            case 44:
                TokenizedSearchInput.A04((TokenizedSearchInput) this.A00);
                return;
            case 45:
                SearchViewModel searchViewModel = ((TokenizedSearchInput) this.A00).A0C;
                if (searchViewModel != null) {
                    searchViewModel.A0g.A04("user_grid_view_choice", Boolean.valueOf(!searchViewModel.A0a()));
                    return;
                }
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                TokenizedSearchInput.A02((TokenizedSearchInput) this.A00);
                return;
            case 47:
                TokenizedSearchInput.A03((TokenizedSearchInput) this.A00);
                return;
            case 48:
                SearchViewModel searchViewModel2 = ((TokenizedSearchInput) this.A00).A0C;
                searchViewModel2.A0F();
                searchViewModel2.A0X(true);
                return;
            case 49:
                C44061y8 r07 = (C44061y8) this.A00;
                r07.A07.A00(((AbstractC44071y9) r07).A09);
                return;
            default:
                return;
        }
        popupNotification.finish();
    }
}
