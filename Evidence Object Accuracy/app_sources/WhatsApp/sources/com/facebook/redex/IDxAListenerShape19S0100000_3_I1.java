package com.facebook.redex;

import X.AbstractActivityC121685jC;
import X.AbstractActivityC123635nW;
import X.AbstractC130285z6;
import X.AbstractC136196Lo;
import X.AnonymousClass009;
import X.AnonymousClass017;
import X.AnonymousClass1V8;
import X.AnonymousClass1V9;
import X.AnonymousClass1ZO;
import X.AnonymousClass602;
import X.AnonymousClass610;
import X.AnonymousClass619;
import X.AnonymousClass68J;
import X.C117295Zj;
import X.C117305Zk;
import X.C118125bJ;
import X.C118145bL;
import X.C118185bP;
import X.C120955h1;
import X.C120965h2;
import X.C120975h3;
import X.C121335hg;
import X.C121395hm;
import X.C121405hn;
import X.C123395n5;
import X.C123525nI;
import X.C123565nM;
import X.C124755q3;
import X.C125715rh;
import X.C125725ri;
import X.C125755rl;
import X.C126765tP;
import X.C127045tr;
import X.C127375uO;
import X.C127455uW;
import X.C127645up;
import X.C127655uq;
import X.C127665ur;
import X.C127765v1;
import X.C127995vO;
import X.C128185vh;
import X.C128885wp;
import X.C129085x9;
import X.C12960it;
import X.C129685y8;
import X.C12970iu;
import X.C12980iv;
import X.C130105yo;
import X.C130785zy;
import X.C1309260n;
import X.C1316663q;
import X.C27691It;
import X.C37891nB;
import X.C452120p;
import android.content.Intent;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.service.NoviVideoSelfieFgService;
import com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.chromium.net.UrlRequest;
import org.json.JSONException;

/* loaded from: classes4.dex */
public class IDxAListenerShape19S0100000_3_I1 implements AbstractC136196Lo {
    public Object A00;
    public final int A01;

    public IDxAListenerShape19S0100000_3_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC136196Lo
    public final void AV8(C130785zy r27) {
        AbstractC136196Lo r3;
        C130785zy r1;
        Object obj;
        Object obj2;
        String str;
        Object obj3;
        Object obj4;
        C128185vh r0;
        List list;
        Object obj5;
        String str2;
        Object obj6;
        Object obj7;
        Object obj8;
        int i;
        String str3;
        Object obj9;
        Object obj10;
        C27691It r12;
        Object obj11;
        C127765v1 r32;
        C27691It r02;
        List<C127455uW> list2;
        switch (this.A01) {
            case 0:
                r3 = (AbstractC136196Lo) this.A00;
                if (r27.A06() && (obj = r27.A02) != null) {
                    try {
                        C121335hg r4 = (C121335hg) AbstractC130285z6.A01(null, ((AnonymousClass1V8) obj).A0F("step_up_challenge"), "MANUAL_REVIEW__AUTO_TRIGGERED");
                        if (r4 != null) {
                            C452120p r13 = new C452120p(456);
                            r13.A08 = r4.A01.A00;
                            r13.A07 = r4.A00.A00;
                            C130785zy.A03(r13, r3);
                            return;
                        }
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("PAY: NoviActionManager failed to parse start step-up response");
                    }
                }
                r1 = new C130785zy(r27.A00, null);
                r1.A01 = null;
                r3.AV8(r1);
                return;
            case 1:
                r3 = (AbstractC136196Lo) this.A00;
                obj2 = null;
                if (r27.A06() && (obj3 = r27.A02) != null) {
                    try {
                        AnonymousClass1V8 r10 = (AnonymousClass1V8) obj3;
                        ArrayList A0l = C12960it.A0l();
                        AnonymousClass1V8[] r122 = r10.A0F("subdivisions").A03;
                        if (r122 != null) {
                            for (AnonymousClass1V8 r14 : r122) {
                                A0l.add(new C127375uO(r14.A0H("name"), r14.A0H("type"), "true".equals(r14.A0I("is_supported", null))));
                            }
                        }
                        ArrayList A0l2 = C12960it.A0l();
                        AnonymousClass1V8[] r7 = r10.A0F("name").A03;
                        AnonymousClass009.A06(r7, "[PAY] Novi/CountryConfig/fromProtocolTreeNode/expected children for name config");
                        for (AnonymousClass1V8 r15 : r7) {
                            A0l2.add(new C120965h2(r15));
                        }
                        ArrayList A0l3 = C12960it.A0l();
                        AnonymousClass1V8[] r6 = r10.A0F("address").A03;
                        AnonymousClass009.A06(r6, "[PAY] Novi/CountryConfig/fromProtocolTreeNode/expected children for address config");
                        for (AnonymousClass1V8 r16 : r6) {
                            A0l3.add(new C120975h3(r16));
                        }
                        C130785zy r03 = new C130785zy(null, new C127665ur(AnonymousClass619.A01(r10.A0F("id"), "text_entity"), A0l, A0l2, A0l3));
                        r03.A01 = null;
                        r3.AV8(r03);
                        return;
                    } catch (AnonymousClass1V9 unused2) {
                        str = "PAY: NoviCommonAction/getCountryConfig can't construct object";
                        Log.e(str);
                        C452120p r42 = r27.A00;
                        C1316663q r04 = r27.A01;
                        r1 = new C130785zy(r42, obj2);
                        r1.A01 = r04;
                        r3.AV8(r1);
                        return;
                    }
                }
                C452120p r42 = r27.A00;
                C1316663q r04 = r27.A01;
                r1 = new C130785zy(r42, obj2);
                r1.A01 = r04;
                r3.AV8(r1);
                return;
            case 2:
                AbstractC136196Lo r8 = (AbstractC136196Lo) this.A00;
                if (r27.A06() && (obj4 = r27.A02) != null) {
                    try {
                        ArrayList A0l4 = C12960it.A0l();
                        AnonymousClass1V8[] r5 = ((AnonymousClass1V8) obj4).A03;
                        HashSet A12 = C12970iu.A12();
                        if (r5 != null) {
                            for (AnonymousClass1V8 r05 : r5) {
                                if (r05.A00.equals("field")) {
                                    String A0H = r05.A0H("type");
                                    String A0H2 = r05.A0H("title");
                                    String A0H3 = r05.A0H("typename");
                                    String A0H4 = r05.A0H("is_sensitive");
                                    if (A0H.equals("FIELD")) {
                                        r0 = new C128185vh(r05.A0H("input_type"), A0H2, A0H3, r05.A0I("typename_to_confirm", null), "FIELD", A0H4, r05.A0H("error_message"), r05.A0I("validation_regex", null), null);
                                    } else if (A0H.equals("DROPDOWN")) {
                                        AnonymousClass1V8[] r123 = r05.A03;
                                        ArrayList A0l5 = C12960it.A0l();
                                        AnonymousClass009.A05(r123);
                                        for (AnonymousClass1V8 r9 : r123) {
                                            if (r9.A00.equals("option")) {
                                                A0l5.add(new C125715rh(r9.A0H("value")));
                                            }
                                        }
                                        if (A0l5.isEmpty()) {
                                            Log.e("PAY: bankField/getDropdownField/empty dropdown option list");
                                        }
                                        r0 = new C128185vh(null, A0H2, A0H3, null, "DROPDOWN", A0H4, null, null, A0l5);
                                    } else {
                                        String A0d = C12960it.A0d(A0H, C12960it.A0k("Invalid value for bank field type="));
                                        Log.e(C12960it.A0d(A0d, C12960it.A0k("[PAY] BankField/fromProtocolTreeNode/Illegal argument exception: ")));
                                        throw C12970iu.A0f(A0d);
                                    }
                                    String str4 = r0.A06;
                                    if (TextUtils.isEmpty(str4)) {
                                        A12.add(r0.A05);
                                    } else if (!A12.contains(str4)) {
                                        throw C12970iu.A0f(C12960it.A0d(str4, C12960it.A0k("PAY: BankSchema/fromProtocolTreeNode/invalid bank schema/field with typename to confirm missing earlier in form: typename=")));
                                    }
                                    A0l4.add(r0);
                                }
                            }
                        }
                        C130785zy.A04(null, r8, new C125725ri(A0l4));
                        return;
                    } catch (AnonymousClass1V9 unused3) {
                        Log.e("PAY: NoviCommonAction/getBankSchema can't parse response");
                    }
                }
                C130785zy.A05(r8, r27);
                return;
            case 3:
                AbstractC136196Lo r92 = (AbstractC136196Lo) this.A00;
                if (r27.A06()) {
                    try {
                        AnonymousClass602 r82 = new AnonymousClass602();
                        AnonymousClass1V8[] r11 = ((AnonymousClass1V8) r27.A02).A03;
                        AnonymousClass009.A05(r11);
                        for (AnonymousClass1V8 r06 : r11) {
                            String str5 = r06.A00;
                            AnonymousClass1V8[] r43 = r06.A03;
                            AnonymousClass009.A05(r43);
                            for (AnonymousClass1V8 r124 : r43) {
                                String A0H5 = r124.A0H("type");
                                String str6 = "BANK";
                                if (!A0H5.equals(str6)) {
                                    str6 = "DEBIT";
                                    if (!A0H5.equals(str6)) {
                                        str6 = "CASH";
                                        if (!A0H5.equals(str6)) {
                                            Log.e("[PAY] fundingSourceViewConfig/fundingOptionFromNode/invalid funding option type");
                                            str6 = null;
                                        }
                                    }
                                }
                                if (!TextUtils.isEmpty(str6)) {
                                    switch (str5.hashCode()) {
                                        case -1629586251:
                                            if (str5.equals("withdrawal")) {
                                                list = r82.A03;
                                                list.add(str6);
                                                break;
                                            }
                                            Log.e(C12960it.A0d(str5, C12960it.A0k("[PAY] fundingSourceViewConfig/fromProtocolTreeNode/unknown child node tag: ")));
                                            break;
                                        case 454861372:
                                            if (str5.equals("payment_settings")) {
                                                list = r82.A02;
                                                list.add(str6);
                                                break;
                                            }
                                            Log.e(C12960it.A0d(str5, C12960it.A0k("[PAY] fundingSourceViewConfig/fromProtocolTreeNode/unknown child node tag: ")));
                                            break;
                                        case 934614152:
                                            if (str5.equals("balance_top_up")) {
                                                list = r82.A00;
                                                list.add(str6);
                                                break;
                                            }
                                            Log.e(C12960it.A0d(str5, C12960it.A0k("[PAY] fundingSourceViewConfig/fromProtocolTreeNode/unknown child node tag: ")));
                                            break;
                                        case 1554454174:
                                            if (str5.equals("deposit")) {
                                                list = r82.A01;
                                                list.add(str6);
                                                break;
                                            }
                                            Log.e(C12960it.A0d(str5, C12960it.A0k("[PAY] fundingSourceViewConfig/fromProtocolTreeNode/unknown child node tag: ")));
                                            break;
                                        default:
                                            Log.e(C12960it.A0d(str5, C12960it.A0k("[PAY] fundingSourceViewConfig/fromProtocolTreeNode/unknown child node tag: ")));
                                            break;
                                    }
                                }
                            }
                        }
                        C130785zy r07 = new C130785zy(null, r82);
                        r07.A01 = null;
                        r92.AV8(r07);
                        return;
                    } catch (AnonymousClass1V9 unused4) {
                        Log.e("PAY: NoviCommonAction/getFundingSourceViewConfig can't construct object");
                    }
                }
                C130785zy.A05(r92, r27);
                return;
            case 4:
                C130785zy.A04(r27.A00, (AbstractC136196Lo) this.A00, Boolean.valueOf(r27.A06()));
                return;
            case 5:
                r3 = (AbstractC136196Lo) this.A00;
                obj2 = null;
                if (r27.A06() && (obj5 = r27.A02) != null) {
                    try {
                        AnonymousClass1V8 r44 = (AnonymousClass1V8) obj5;
                        List A0J = r44.A0J("card_property");
                        ArrayList A0l6 = C12960it.A0l();
                        Iterator it = A0J.iterator();
                        while (it.hasNext()) {
                            AnonymousClass1V8 A0d2 = C117305Zk.A0d(it);
                            A0l6.add(new C127655uq(A0d2.A0I("card_network", null), A0d2.A0H("detection_regex"), A0d2.A04("cvv_length"), A0d2.A04("card_number_length")));
                        }
                        AnonymousClass1V8 A0E = r44.A0E("card_postal_code");
                        C130785zy r08 = new C130785zy(null, new C129085x9(new C128885wp(r44.A0F("card_number")), new C128885wp(r44.A0F("card_expiry")), new C128885wp(r44.A0F("card_cvv")), A0E != null ? new C120955h1(A0E) : null, A0l6));
                        r08.A01 = null;
                        r3.AV8(r08);
                        return;
                    } catch (AnonymousClass1V9 unused5) {
                        str = "PAY: NoviCommonAction/getCardSchema can't construct object";
                        Log.e(str);
                        C452120p r42 = r27.A00;
                        C1316663q r04 = r27.A01;
                        r1 = new C130785zy(r42, obj2);
                        r1.A01 = r04;
                        r3.AV8(r1);
                        return;
                    }
                }
                C452120p r42 = r27.A00;
                C1316663q r04 = r27.A01;
                r1 = new C130785zy(r42, obj2);
                r1.A01 = r04;
                r3.AV8(r1);
                return;
            case 6:
                C129685y8 r33 = (C129685y8) this.A00;
                if (!r27.A06() || (obj7 = r27.A02) == null) {
                    StringBuilder A0k = C12960it.A0k("PAY: NoviSyncRepository/fetchUserPreferenceOtpLogin: Error code ");
                    C452120p r09 = r27.A00;
                    if (r09 != null) {
                        obj6 = Integer.valueOf(r09.A00);
                    } else {
                        obj6 = "none";
                    }
                    str2 = C12970iu.A0s(obj6, A0k);
                    Log.e(str2);
                    return;
                }
                C12960it.A0t(C130105yo.A01(r33.A04), "payment_login_require_otp", !"NONE".equals(C117295Zj.A0W((AnonymousClass1V8) obj7, "preferred-two-factor-method")));
                return;
            case 7:
                AnonymousClass017 r34 = (AnonymousClass017) this.A00;
                if (r27.A06() && (obj8 = r27.A02) != null) {
                    try {
                        r34.A0A(Integer.valueOf(AnonymousClass1ZO.A01(((AnonymousClass1V8) obj8).A0H("status"))));
                        return;
                    } catch (AnonymousClass1V9 unused6) {
                        Log.e("PAY: NoviSyncRepository/fetchConsumerStatus can't parse consumer status");
                    }
                }
                r34.A0A(C12980iv.A0i());
                return;
            case 8:
                NoviVideoSelfieFgService noviVideoSelfieFgService = (NoviVideoSelfieFgService) this.A00;
                if (!r27.A06() || r27.A02 == null) {
                    C1316663q r17 = r27.A01;
                    if (r17 != null) {
                        Intent intent = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
                        intent.putExtra("step_up", r17);
                        intent.putExtra("step_up_origin_action", noviVideoSelfieFgService.A01);
                        intent.putExtra("extra_event_type", "extra_event_answer_selfie_stepup");
                        C117305Zk.A0y(noviVideoSelfieFgService, intent);
                    } else {
                        Intent intent2 = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
                        intent2.putExtra("extra_event_type", "extra_event_answer_selfie_failed");
                        C452120p r2 = r27.A00;
                        if (r2 != null) {
                            intent2.putExtra("error_code", r2.A00);
                            intent2.putExtra("error_display_title", r2.A08);
                            intent2.putExtra("error_display_text", r2.A07);
                        }
                        C117305Zk.A0y(noviVideoSelfieFgService, intent2);
                        if (r2.A00 == 456) {
                            str3 = r2.A08;
                            noviVideoSelfieFgService.A04(str3);
                            return;
                        }
                        i = R.string.novi_selfie_verify_failed;
                        str3 = noviVideoSelfieFgService.getString(i);
                        noviVideoSelfieFgService.A04(str3);
                        return;
                    }
                } else {
                    Intent intent3 = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
                    intent3.putExtra("extra_event_type", "extra_event_answer_selfie_success");
                    C117305Zk.A0y(noviVideoSelfieFgService, intent3);
                }
                i = R.string.novi_selfie_verify_success;
                str3 = noviVideoSelfieFgService.getString(i);
                noviVideoSelfieFgService.A04(str3);
                return;
            case 9:
                AbstractActivityC123635nW r010 = (AbstractActivityC123635nW) this.A00;
                C452120p r22 = r27.A00;
                if (r22 != null) {
                    r010.A05.A02(r22, null, null);
                    return;
                }
                return;
            case 10:
                NoviPayHubAddPaymentMethodActivity noviPayHubAddPaymentMethodActivity = (NoviPayHubAddPaymentMethodActivity) this.A00;
                if (!r27.A06() || (obj9 = r27.A02) == null) {
                    C452120p r011 = r27.A00;
                    if (r011 != null && r011.A00 == 542720003) {
                        noviPayHubAddPaymentMethodActivity.finish();
                    }
                } else {
                    try {
                        noviPayHubAddPaymentMethodActivity.A07.A00((C125725ri) obj9);
                        return;
                    } catch (C124755q3 | JSONException e) {
                        Log.e(C12960it.A0b("[PAY] NoviPayHubAddPaymentMethodActivity/getBankSchema/exception: ", e));
                    }
                }
                str2 = "[PAY] NoviPayHubAddPaymentMethodActivity/getBankSchema/networkError";
                Log.e(str2);
                return;
            case 11:
                AbstractActivityC121685jC r18 = (AbstractActivityC121685jC) this.A00;
                if (r27 != null && r27.A06()) {
                    r18.A05.A0D(new AnonymousClass68J((C37891nB) r27.A02), "payment_view");
                    return;
                }
                return;
            case 12:
                C123395n5 r19 = (C123395n5) this.A00;
                if (r27.A06()) {
                    r19.A03 = (C127645up) r27.A02;
                    return;
                }
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C123565nM r35 = (C123565nM) this.A00;
                r35.A00 = 3;
                r35.A01 = 3;
                if (!r27.A06() || (obj10 = r27.A02) == null) {
                    C452120p r23 = r27.A00;
                    if (!(r23 == null || r23.A00 == 542720003)) {
                        C123525nI r110 = new C123525nI(506);
                        r110.A0F = r23.A08;
                        r110.A0B = r23.A07;
                        C118185bP.A01(r35, r110);
                    }
                } else {
                    AnonymousClass1V8 A0E2 = ((AnonymousClass1V8) obj10).A0E("cash_withdrawal_code");
                    if (A0E2 != null) {
                        try {
                            r35.A02 = new C126765tP(C1309260n.A01(A0E2.A0E("instructions")), C117295Zj.A0W(A0E2, "code"));
                        } catch (AnonymousClass1V9 unused7) {
                            C118185bP.A01(r35, new C123525nI(506));
                            Log.e("PAY: NoviPaymentTransactionDetailsViewModel/getCashWithdrawalCode can't parse response");
                        }
                    }
                }
                r35.A09();
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                C118145bL r111 = (C118145bL) this.A00;
                if (r27.A06()) {
                    r12 = r111.A0y;
                    obj11 = r27.A02;
                } else {
                    r12 = r111.A10;
                    obj11 = r27.A00;
                }
                r12.A0B(obj11);
                return;
            case 15:
                r32 = new C127765v1(5);
                r32.A02 = r27;
                r02 = ((C118125bJ) this.A00).A0B;
                r02.A0B(r32);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                C118125bJ r45 = (C118125bJ) this.A00;
                if (r27.A06()) {
                    Object obj12 = r27.A02;
                    if (obj12 instanceof C121395hm) {
                        C121395hm r36 = (C121395hm) obj12;
                        C125755rl r24 = r36.A00;
                        if (r24 == null && r36.A01.isEmpty()) {
                            r45.A0B.A0B(new C127765v1(0));
                        }
                        C127765v1 r83 = new C127765v1(2);
                        if (r24 != null) {
                            r83.A03 = true;
                            list2 = r45.A0C;
                            list2.clear();
                            List<C127995vO> list3 = r24.A00;
                            ArrayList A0l7 = C12960it.A0l();
                            for (C127995vO r25 : list3) {
                                A0l7.add(new C127455uW(r25, r45.A0A));
                            }
                            list2.addAll(A0l7);
                            r45.A02.A0B(list2);
                        } else {
                            r83.A03 = false;
                            list2 = r45.A0C;
                            list2.clear();
                            List list4 = r45.A0D;
                            list4.clear();
                            for (C121405hn r62 : r36.A01) {
                                List<C127995vO> list5 = ((C125755rl) r62).A00;
                                ArrayList A0l8 = C12960it.A0l();
                                for (C127995vO r26 : list5) {
                                    A0l8.add(new C127455uW(r26, r45.A0A));
                                }
                                list2.addAll(A0l8);
                                list4.add(new C127045tr(r62.A00, A0l8));
                            }
                            r45.A03.A0B(list4);
                        }
                        r45.A0B.A0B(r83);
                        ArrayList A0w = C12980iv.A0w(list2.size());
                        for (C127455uW r012 : list2) {
                            A0w.add(r012.A00.A03);
                        }
                        r45.A00 = TextUtils.join(",", A0w);
                        r45.A04(new AnonymousClass610("STEP_UP_PRESENTED_VPV", "TEXT_INPUT", "BUTTON"));
                        return;
                    }
                    r02 = r45.A0B;
                    r32 = new C127765v1(0);
                } else {
                    r32 = new C127765v1(0);
                    r32.A01 = r27.A00;
                    r02 = r45.A0B;
                }
                r02.A0B(r32);
                return;
            case 17:
                AbstractActivityC123635nW r112 = (AbstractActivityC123635nW) this.A00;
                if (r27.A06()) {
                    r112.A09.A01((AnonymousClass602) r27.A02);
                    return;
                }
                str2 = "[PAY] noviPayBloksActivity/fetchAndCacheFundingSourceViewConfig/ error fetching funding source view config";
                Log.e(str2);
                return;
            case 18:
                AbstractActivityC123635nW.A1V(r27, (AbstractActivityC123635nW) this.A00);
                return;
            default:
                return;
        }
    }
}
