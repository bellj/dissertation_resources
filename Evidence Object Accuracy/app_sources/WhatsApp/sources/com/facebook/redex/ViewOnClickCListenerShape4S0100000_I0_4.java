package com.facebook.redex;

import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape4S0100000_I0_4 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape4S0100000_I0_4(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:138:0x044e  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x046b  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x027a  */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r13) {
        /*
        // Method dump skipped, instructions count: 1690
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape4S0100000_I0_4.onClick(android.view.View):void");
    }
}
