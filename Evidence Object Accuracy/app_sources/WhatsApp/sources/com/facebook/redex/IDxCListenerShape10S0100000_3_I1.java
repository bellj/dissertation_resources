package com.facebook.redex;

import android.content.DialogInterface;

/* loaded from: classes4.dex */
public class IDxCListenerShape10S0100000_3_I1 implements DialogInterface.OnClickListener {
    public Object A00;
    public final int A01;

    public IDxCListenerShape10S0100000_3_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:175:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0016  */
    @Override // android.content.DialogInterface.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.content.DialogInterface r14, int r15) {
        /*
        // Method dump skipped, instructions count: 1410
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxCListenerShape10S0100000_3_I1.onClick(android.content.DialogInterface, int):void");
    }
}
