package com.facebook.redex;

import X.AnonymousClass0Az;
import X.AnonymousClass0B0;
import X.AnonymousClass0B1;
import X.AnonymousClass0Ec;
import X.AnonymousClass0Ed;
import X.AnonymousClass0Ee;
import X.AnonymousClass0VU;
import X.C02280As;
import X.C02800Ea;
import X.C02810Eb;
import X.C02820Ef;
import X.C06790Vc;
import X.C06800Vd;
import X.C06810Ve;
import X.C06820Vf;
import X.C06830Vg;
import X.C06840Vh;
import X.C06850Vi;
import X.C06860Vj;
import X.C06870Vk;
import X.C06880Vl;
import X.C06890Vm;
import X.C25991Bp;
import X.C43951xu;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.MediaBrowserCompat$MediaItem;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.RatingCompat;
import android.support.v4.media.session.MediaSessionCompat$QueueItem;
import android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper;
import android.support.v4.media.session.MediaSessionCompat$Token;
import android.support.v4.media.session.ParcelableVolumeInfo;
import android.support.v4.media.session.PlaybackStateCompat;
import androidx.versionedparcelable.ParcelImpl;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* loaded from: classes.dex */
public class IDxCreatorShape0S0000000_I1 implements Parcelable.Creator {
    public final int A00;

    public IDxCreatorShape0S0000000_I1(int i) {
        this.A00 = i;
    }

    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        Object readStrongBinder;
        switch (this.A00) {
            case 0:
                return new MediaBrowserCompat$MediaItem(parcel);
            case 1:
                return new MediaMetadataCompat(parcel);
            case 2:
                return new RatingCompat(parcel.readInt(), parcel.readFloat());
            case 3:
                return new MediaSessionCompat$QueueItem(parcel);
            case 4:
                return new MediaSessionCompat$ResultReceiverWrapper(parcel);
            case 5:
                if (Build.VERSION.SDK_INT >= 21) {
                    readStrongBinder = parcel.readParcelable(null);
                } else {
                    readStrongBinder = parcel.readStrongBinder();
                }
                return new MediaSessionCompat$Token(readStrongBinder);
            case 6:
                return new ParcelableVolumeInfo(parcel);
            case 7:
                return new PlaybackStateCompat(parcel);
            case 8:
                return new PlaybackStateCompat.CustomAction(parcel);
            case 9:
                return new AnonymousClass0VU(parcel);
            case 10:
                return new C06830Vg(parcel);
            case 11:
                return new C06800Vd(parcel);
            case 12:
                return new AnonymousClass0Az(parcel);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new AnonymousClass0B1(parcel);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new C06820Vf(parcel);
            case 15:
                return new C06790Vc(parcel);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new C06810Ve(parcel);
            case 17:
                return new C06850Vi(parcel);
            case 18:
                return new C02800Ea(parcel);
            case 19:
                return new C02810Eb(parcel);
            case C43951xu.A01:
                return new AnonymousClass0Ec(parcel);
            case 21:
                return new C02280As(parcel);
            case 22:
                return new AnonymousClass0Ed(parcel);
            case 23:
                return new C02820Ef(parcel);
            case 24:
                return new AnonymousClass0Ee(parcel);
            case 25:
                return new C06880Vl(parcel);
            case 26:
                return new C06840Vh(parcel);
            case 27:
                return new C06890Vm(parcel);
            case 28:
                return new ParcelImpl(parcel);
            case 29:
                return new AnonymousClass0B0(parcel);
            case C25991Bp.A0S:
                return new C06860Vj(parcel);
            case 31:
                return new C06870Vk(parcel);
            default:
                return null;
        }
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        switch (this.A00) {
            case 0:
                return new MediaBrowserCompat$MediaItem[i];
            case 1:
                return new MediaMetadataCompat[i];
            case 2:
                return new RatingCompat[i];
            case 3:
                return new MediaSessionCompat$QueueItem[i];
            case 4:
                return new MediaSessionCompat$ResultReceiverWrapper[i];
            case 5:
                return new MediaSessionCompat$Token[i];
            case 6:
                return new ParcelableVolumeInfo[i];
            case 7:
                return new PlaybackStateCompat[i];
            case 8:
                return new PlaybackStateCompat.CustomAction[i];
            case 9:
                return new AnonymousClass0VU[i];
            case 10:
                return new C06830Vg[i];
            case 11:
                return new C06800Vd[i];
            case 12:
                return new AnonymousClass0Az[i];
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new AnonymousClass0B1[i];
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return new C06820Vf[i];
            case 15:
                return new C06790Vc[i];
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return new C06810Ve[i];
            case 17:
                return new C06850Vi[i];
            case 18:
                return new C02800Ea[i];
            case 19:
                return new C02810Eb[i];
            case C43951xu.A01:
                return new AnonymousClass0Ec[i];
            case 21:
                return new C02280As[i];
            case 22:
                return new AnonymousClass0Ed[i];
            case 23:
                return new C02820Ef[i];
            case 24:
                return new AnonymousClass0Ee[i];
            case 25:
                return new C06880Vl[i];
            case 26:
                return new C06840Vh[i];
            case 27:
                return new C06890Vm[i];
            case 28:
                return new ParcelImpl[i];
            case 29:
                return new AnonymousClass0B0[i];
            case C25991Bp.A0S:
                return new C06860Vj[i];
            case 31:
                return new C06870Vk[i];
            default:
                return new Object[0];
        }
    }
}
