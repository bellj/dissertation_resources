package com.facebook.redex;

import X.AnonymousClass3EX;
import X.C12990iw;
import X.C21770xx;
import X.C44691zO;
import X.C68263Us;
import X.C92584Wm;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S2300100_I1 extends EmptyBaseRunnable0 implements Runnable {
    public long A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public String A04;
    public String A05;
    public final int A06;

    public RunnableBRunnable0Shape0S2300100_I1(Object obj, Object obj2, Object obj3, String str, String str2, int i, long j) {
        this.A06 = i;
        this.A01 = obj;
        this.A00 = j;
        this.A02 = obj2;
        this.A03 = obj3;
        this.A04 = str;
        this.A05 = str2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A06) {
            case 0:
                AnonymousClass3EX r4 = (AnonymousClass3EX) this.A01;
                long j = this.A00;
                UserJid userJid = (UserJid) this.A02;
                C44691zO r10 = (C44691zO) this.A03;
                String str = this.A04;
                String str2 = this.A05;
                if (j == 0) {
                    C21770xx r0 = r4.A01;
                    String str3 = r10.A0D;
                    r0.A07(userJid, str3);
                    r4.A02.A03(userJid, 54, str3, 30);
                } else {
                    C21770xx r3 = r4.A01;
                    String str4 = r10.A0D;
                    C92584Wm A03 = r3.A03(userJid, str4);
                    if (A03 == null) {
                        A03 = new C92584Wm(r10, 1);
                        r4.A02.A01(userJid, null, null, 49, null, Long.valueOf(A03.A00), str4, null, str, str2, 28);
                    } else {
                        A03.A00 = j;
                        r4.A02.A02(userJid, 53, Long.valueOf(j), str4, 29);
                    }
                    r3.A05(A03, userJid);
                }
                r4.A00();
                return;
            case 1:
                C68263Us r42 = (C68263Us) this.A01;
                long j2 = this.A00;
                UserJid userJid2 = (UserJid) this.A02;
                C44691zO r102 = (C44691zO) this.A03;
                String str5 = this.A04;
                String str6 = this.A05;
                if (j2 == 0) {
                    C21770xx r02 = r42.A07;
                    String str7 = r102.A0D;
                    r02.A07(userJid2, str7);
                    r42.A08.A03(userJid2, 54, str7, 30);
                } else {
                    C21770xx r32 = r42.A07;
                    String str8 = r102.A0D;
                    C92584Wm A032 = r32.A03(userJid2, str8);
                    if (A032 == null) {
                        A032 = new C92584Wm(r102, 1);
                        r42.A08.A01(userJid2, null, null, 49, null, Long.valueOf(A032.A00), str8, null, str5, str6, 28);
                    } else {
                        A032.A00 = j2;
                        r42.A08.A02(userJid2, 53, Long.valueOf(j2), str8, 29);
                    }
                    r32.A05(A032, userJid2);
                }
                C12990iw.A1O(r42.A0A, r42, 36);
                return;
            default:
                return;
        }
    }
}
