package com.facebook.redex;

import X.AbstractActivityC59392ue;
import X.AbstractC115845Td;
import X.AbstractC116645Wg;
import X.AbstractC49232Jx;
import X.AbstractC75703kH;
import X.AbstractC75743kL;
import X.AbstractC92184Uw;
import X.AbstractC92594Wn;
import X.ActivityC000800j;
import X.ActivityC60092vu;
import X.AnonymousClass016;
import X.AnonymousClass1B0;
import X.AnonymousClass1B9;
import X.AnonymousClass1WK;
import X.AnonymousClass2CT;
import X.AnonymousClass3E8;
import X.AnonymousClass405;
import X.AnonymousClass4N8;
import X.AnonymousClass4NA;
import X.AnonymousClass4RT;
import X.AnonymousClass4XG;
import X.AnonymousClass5TT;
import X.AnonymousClass5TU;
import X.C12960it;
import X.C12970iu;
import X.C12980iv;
import X.C15370n3;
import X.C15550nR;
import X.C16700pc;
import X.C25991Bp;
import X.C43951xu;
import X.C48142Em;
import X.C48152En;
import X.C53842fM;
import X.C59402uf;
import X.C59552uv;
import X.C59582uy;
import X.C59622v2;
import X.C59632v3;
import X.C59642v4;
import X.C59652v5;
import X.C60172w8;
import X.C63363Bh;
import X.C68553Vv;
import X.C68563Vw;
import X.C84553zV;
import X.C91524Sb;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.View;
import android.widget.CompoundButton;
import androidx.fragment.app.DialogFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.biz.cart.view.fragment.CartFragment;
import com.whatsapp.biz.catalog.view.widgets.QuantitySelector;
import com.whatsapp.businessdirectory.view.activity.BusinessDirectoryActivity;
import com.whatsapp.businessdirectory.view.activity.DirectorySetNeighborhoodActivity;
import com.whatsapp.businessdirectory.view.custom.ClearLocationDialogFragment;
import com.whatsapp.businessdirectory.view.custom.FilterBottomSheetDialogFragment;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.businessdirectory.viewmodel.DirectorySearchHistoryViewModel;
import com.whatsapp.businessupsell.BusinessAppEducation;
import com.whatsapp.businessupsell.BusinessProfileEducation;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPickerSheet;
import com.whatsapp.calling.callrating.CallRatingActivity;
import com.whatsapp.calling.callrating.CallRatingBottomSheet;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.Voip;
import java.util.List;
import java.util.TimerTask;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape7S0100000_I1_1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape7S0100000_I1_1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AnonymousClass1WK r0;
        C91524Sb r2;
        int i;
        C68553Vv r02;
        int i2;
        int i3;
        boolean z;
        ActivityC60092vu r03;
        BusinessAppEducation businessAppEducation;
        BusinessProfileEducation businessProfileEducation;
        int i4;
        AnonymousClass5TU r22;
        long j;
        Voip.CallState callState;
        switch (this.A01) {
            case 0:
                C53842fM r23 = (C53842fM) this.A00;
                AnonymousClass016 r1 = r23.A06;
                UserJid userJid = r23.A0M;
                r1.A0B(new C84553zV(userJid));
                r23.A0E.A00(userJid, null, null, C12980iv.A0i(), null, null, 2, 1);
                return;
            case 1:
                QuantitySelector quantitySelector = (QuantitySelector) this.A00;
                if (!quantitySelector.A0A) {
                    long j2 = quantitySelector.A01;
                    long j3 = quantitySelector.A00;
                    if (j2 < j3) {
                        if (quantitySelector.A0B) {
                            int i5 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                            quantitySelector.A01 = j2 + 1;
                            if (i5 == 0) {
                                quantitySelector.A00();
                            } else {
                                quantitySelector.A03();
                            }
                            Handler handler = quantitySelector.A0D;
                            handler.removeCallbacksAndMessages(null);
                            handler.postDelayed(new RunnableBRunnable0Shape14S0100000_I1(quantitySelector, 35), 2000);
                        } else {
                            quantitySelector.A01 = j2 + 1;
                            quantitySelector.A03();
                        }
                        r22 = quantitySelector.A05;
                        if (r22 != null) {
                            j = quantitySelector.A01;
                            r22.AUU(j);
                            return;
                        }
                        return;
                    }
                    AnonymousClass5TT r04 = quantitySelector.A04;
                    if (r04 != null) {
                        r04.ARm(j3);
                        return;
                    }
                    return;
                }
                return;
            case 2:
                QuantitySelector quantitySelector2 = (QuantitySelector) this.A00;
                if (!quantitySelector2.A0A) {
                    quantitySelector2.A00();
                    Handler handler2 = quantitySelector2.A0D;
                    handler2.removeCallbacksAndMessages(null);
                    handler2.postDelayed(new RunnableBRunnable0Shape14S0100000_I1(quantitySelector2, 35), 2000);
                    return;
                }
                return;
            case 3:
                QuantitySelector quantitySelector3 = (QuantitySelector) this.A00;
                if (!quantitySelector3.A0A) {
                    quantitySelector3.A01--;
                    quantitySelector3.A03();
                    if (quantitySelector3.A0B) {
                        int i6 = (quantitySelector3.A01 > 0 ? 1 : (quantitySelector3.A01 == 0 ? 0 : -1));
                        Handler handler3 = quantitySelector3.A0D;
                        if (i6 > 0) {
                            handler3.removeCallbacksAndMessages(null);
                            handler3.postDelayed(new RunnableBRunnable0Shape14S0100000_I1(quantitySelector3, 35), 2000);
                        } else {
                            handler3.removeCallbacksAndMessages(null);
                            quantitySelector3.A04(quantitySelector3.A01, quantitySelector3.A00);
                        }
                    }
                    r22 = quantitySelector3.A05;
                    if (r22 != null) {
                        j = quantitySelector3.A01;
                        r22.AUU(j);
                        return;
                    }
                    return;
                }
                return;
            case 4:
            case 5:
                r0 = (AnonymousClass1WK) this.A00;
                r0.AJ3();
                return;
            case 6:
                AbstractActivityC59392ue r3 = (AbstractActivityC59392ue) this.A00;
                r3.Adm(CartFragment.A00(r3.A0K, null, 2));
                return;
            case 7:
                AbstractC92594Wn r24 = (AbstractC92594Wn) this.A00;
                if (r24 instanceof C59402uf) {
                    r24.A00.ARZ(((C59402uf) r24).A00);
                    return;
                } else {
                    r24.A00.AVV();
                    return;
                }
            case 8:
            case 41:
                ((Activity) this.A00).finish();
                return;
            case 9:
                BusinessDirectoryActivity businessDirectoryActivity = (BusinessDirectoryActivity) this.A00;
                TimerTask timerTask = businessDirectoryActivity.A0C;
                if (timerTask != null) {
                    timerTask.cancel();
                }
                businessDirectoryActivity.A01.A04(true);
                businessDirectoryActivity.onBackPressed();
                return;
            case 10:
                DirectorySetNeighborhoodActivity directorySetNeighborhoodActivity = (DirectorySetNeighborhoodActivity) this.A00;
                directorySetNeighborhoodActivity.A01.A04(true);
                directorySetNeighborhoodActivity.onBackPressed();
                return;
            case 11:
                ClearLocationDialogFragment clearLocationDialogFragment = (ClearLocationDialogFragment) this.A00;
                AnonymousClass1B9 r25 = clearLocationDialogFragment.A00;
                synchronized (r25) {
                    AnonymousClass1B0 r12 = r25.A00;
                    r12.A00();
                    r12.A02(false);
                    for (AbstractC115845Td r05 : r25.A01) {
                        r05.AVe();
                    }
                }
                clearLocationDialogFragment.A1B();
                return;
            case 12:
            case 42:
                ((DialogFragment) this.A00).A1B();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                ((Dialog) this.A00).dismiss();
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                FilterBottomSheetDialogFragment filterBottomSheetDialogFragment = (FilterBottomSheetDialogFragment) this.A00;
                filterBottomSheetDialogFragment.A1B();
                AbstractC116645Wg r06 = filterBottomSheetDialogFragment.A02;
                if (r06 != null) {
                    r06.ANL();
                    return;
                }
                return;
            case 15:
                r02 = (C68553Vv) this.A00;
                r2 = r02.A05;
                i = 5;
                r2.A01 = i;
                r02.A01();
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                AnonymousClass4N8 r26 = (AnonymousClass4N8) this.A00;
                AnonymousClass4RT r13 = r26.A01;
                r13.A01 = 5;
                r26.A00.A0A(r13);
                return;
            case 17:
                AbstractC75703kH r14 = (AbstractC75703kH) this.A00;
                C16700pc.A0E(r14, 0);
                r14.A00.ANz();
                return;
            case 18:
            case 19:
                ((AbstractC75703kH) this.A00).A00.ANn();
                return;
            case C43951xu.A01:
                ((AbstractC49232Jx) this.A00).ASG();
                return;
            case 21:
                ((AbstractC49232Jx) this.A00).AVi();
                return;
            case 22:
                ((AbstractC49232Jx) this.A00).AO0();
                return;
            case 23:
                ((AbstractC49232Jx) this.A00).AVS();
                return;
            case 24:
                ((AbstractC49232Jx) this.A00).AVT();
                return;
            case 25:
                ((AbstractC75703kH) this.A00).A00.AQY();
                return;
            case 26:
                ((AbstractC75703kH) this.A00).A00.ASq();
                return;
            case 27:
                C59652v5 r15 = (C59652v5) this.A00;
                C16700pc.A0E(r15, 0);
                r02 = r15.A01.A00;
                r2 = r02.A05;
                i = 7;
                r2.A01 = i;
                r02.A01();
                return;
            case 28:
                C59622v2 r16 = (C59622v2) this.A00;
                C16700pc.A0E(r16, 0);
                r02 = r16.A00.A00;
                r2 = r02.A05;
                i = 8;
                r2.A01 = i;
                r02.A01();
                return;
            case 29:
                C59622v2 r17 = (C59622v2) this.A00;
                C16700pc.A0E(r17, 0);
                r02 = r17.A00.A00;
                r2 = r02.A05;
                i = 9;
                r2.A01 = i;
                r02.A01();
                return;
            case C25991Bp.A0S:
                C59632v3 r18 = (C59632v3) this.A00;
                C16700pc.A0E(r18, 0);
                r0 = r18.A00;
                r0.AJ3();
                return;
            case 31:
                C59642v4 r19 = (C59642v4) this.A00;
                C16700pc.A0E(r19, 0);
                r0 = r19.A01;
                r0.AJ3();
                return;
            case 32:
                AnonymousClass4NA r07 = ((C59552uv) this.A00).A00;
                DirectorySearchHistoryViewModel directorySearchHistoryViewModel = r07.A01;
                C48152En r27 = r07.A00;
                DirectorySearchHistoryViewModel.A00(r27, directorySearchHistoryViewModel, 48);
                ((C48142Em) r27).A00 = System.currentTimeMillis();
                directorySearchHistoryViewModel.A05.A00(r27);
                directorySearchHistoryViewModel.A02.A0B(r27);
                return;
            case 33:
                AnonymousClass4NA r08 = ((C59552uv) this.A00).A00;
                DirectorySearchHistoryViewModel directorySearchHistoryViewModel2 = r08.A01;
                C48152En r4 = r08.A00;
                DirectorySearchHistoryViewModel.A00(r4, directorySearchHistoryViewModel2, 49);
                AnonymousClass3E8 r32 = directorySearchHistoryViewModel2.A05;
                AnonymousClass4XG r28 = r32.A01;
                List A00 = r28.A00();
                A00.remove(r4);
                r28.A01.A01(A00);
                r32.A00.A0B(r28.A00());
                return;
            case 34:
            case 36:
                C59582uy r09 = (C59582uy) this.A00;
                BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel = r09.A01;
                String str = r09.A02;
                if (businessDirectorySearchQueryViewModel.A04 == 0 || !businessDirectorySearchQueryViewModel.A0V()) {
                    businessDirectorySearchQueryViewModel.A0P(str);
                    return;
                }
                C68563Vw r42 = businessDirectorySearchQueryViewModel.A0P;
                C63363Bh r33 = r42.A05;
                List list = r33.A08;
                if (!list.isEmpty()) {
                    list.add(new AnonymousClass405(0));
                    r33.A02 = 2;
                    r42.A03();
                }
                businessDirectorySearchQueryViewModel.A0R(str);
                return;
            case 35:
                ((C59582uy) this.A00).A01.AVi();
                return;
            case 37:
                ActivityC60092vu r010 = (ActivityC60092vu) this.A00;
                r010.onBackPressed();
                i2 = 3;
                businessAppEducation = r010;
                i3 = 12;
                z = false;
                r03 = businessAppEducation;
                r03.A2e(i2, i3, z);
                return;
            case 38:
                BusinessAppEducation businessAppEducation2 = (BusinessAppEducation) this.A00;
                businessAppEducation2.startActivity(businessAppEducation2.A00.A00("smb_cs_chats_banner"));
                i2 = 2;
                businessAppEducation = businessAppEducation2;
                i3 = 12;
                z = false;
                r03 = businessAppEducation;
                r03.A2e(i2, i3, z);
                return;
            case 39:
                ActivityC60092vu r011 = (ActivityC60092vu) this.A00;
                r011.onBackPressed();
                i2 = 3;
                businessProfileEducation = r011;
                i3 = 11;
                z = true;
                r03 = businessProfileEducation;
                r03.A2e(i2, i3, z);
                return;
            case 40:
                BusinessProfileEducation businessProfileEducation2 = (BusinessProfileEducation) this.A00;
                businessProfileEducation2.startActivity(businessProfileEducation2.A02.A00("smb_cs_profile"));
                i2 = 2;
                businessProfileEducation = businessProfileEducation2;
                i3 = 11;
                z = true;
                r03 = businessProfileEducation;
                r03.A2e(i2, i3, z);
                return;
            case 43:
                C60172w8 r29 = (C60172w8) this.A00;
                r29.A0A.A1H(((AbstractC92184Uw) r29).A00, r29);
                return;
            case 44:
                ((GroupCallParticipantPickerSheet) this.A00).A3C();
                return;
            case 45:
                ((ActivityC000800j) this.A00).findViewById(C12960it.A05(view.getTag())).performClick();
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                CallRatingActivity callRatingActivity = (CallRatingActivity) this.A00;
                CompoundButton compoundButton = (CompoundButton) view;
                Number number = (Number) compoundButton.getTag();
                if (number != null) {
                    boolean isChecked = compoundButton.isChecked();
                    int intValue = callRatingActivity.A0F.intValue();
                    int intValue2 = 1 << number.intValue();
                    if (isChecked) {
                        i4 = intValue | intValue2;
                    } else {
                        i4 = intValue & (intValue2 ^ -1);
                    }
                    callRatingActivity.A0F = Integer.valueOf(i4);
                }
                Log.i(C12960it.A0d(Integer.toBinaryString(callRatingActivity.A0F.intValue()), C12960it.A0k("callratingactivity/problems ")));
                return;
            case 47:
            case 48:
                CallRatingBottomSheet callRatingBottomSheet = (CallRatingBottomSheet) this.A00;
                C16700pc.A0E(callRatingBottomSheet, 0);
                BottomSheetBehavior bottomSheetBehavior = callRatingBottomSheet.A00;
                if (bottomSheetBehavior != null && 5 == bottomSheetBehavior.A0B) {
                    bottomSheetBehavior.A0M(5);
                }
                r0 = callRatingBottomSheet.A01;
                r0.AJ3();
                return;
            case 49:
                ParticipantsListViewModel participantsListViewModel = ((AbstractC75743kL) this.A00).A00;
                if (participantsListViewModel != null) {
                    Context context = view.getContext();
                    AnonymousClass2CT r012 = participantsListViewModel.A01;
                    if (!(r012 == null || context == null)) {
                        CallInfo A2i = r012.A00.A2i();
                        if (A2i == null || (callState = A2i.callState) == Voip.CallState.ACTIVE || callState == Voip.CallState.CONNECTED_LONELY) {
                            participantsListViewModel.A01.A00.A2y();
                            return;
                        }
                        C15370n3 A002 = C15550nR.A00(participantsListViewModel.A08, A2i.getPeerJid());
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        boolean isGroupCall = A2i.isGroupCall();
                        int i7 = R.string.voip_joinable_can_not_add_before_connected_one_on_one;
                        if (isGroupCall) {
                            i7 = R.string.voip_joinable_can_not_add_before_connected_group;
                        }
                        builder.setMessage(C12960it.A0X(context, participantsListViewModel.A09.A04(A002), C12970iu.A1b(), 0, i7)).setPositiveButton(R.string.ok, (DialogInterface.OnClickListener) null).create().show();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
