package com.facebook.redex;

import X.C14850m9;
import X.C18290sD;
import X.C18480sW;
import android.os.ConditionVariable;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0400000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0400000_I0(ConditionVariable conditionVariable, C18480sW r3, C18290sD r4, C14850m9 r5) {
        this.A04 = 22;
        this.A00 = r5;
        this.A03 = r4;
        this.A01 = r3;
        this.A02 = conditionVariable;
    }

    public RunnableBRunnable0Shape0S0400000_I0(Object obj, Object obj2, Object obj3, Object obj4, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:467:0x10fa, code lost:
        if (r9.A0S.A0K(r9.A0F) != false) goto L_0x10fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:600:0x1736, code lost:
        if (r12 != null) goto L_0x1738;
     */
    /* JADX WARNING: Removed duplicated region for block: B:478:0x1181  */
    /* JADX WARNING: Removed duplicated region for block: B:660:0x1861 A[Catch: all -> 0x1900, TryCatch #20 {all -> 0x1900, blocks: (B:642:0x1809, B:644:0x181a, B:646:0x1820, B:647:0x182c, B:649:0x1836, B:651:0x183c, B:653:0x1844, B:656:0x184e, B:657:0x1854, B:658:0x1858, B:660:0x1861, B:662:0x1867, B:664:0x186f, B:665:0x1876, B:667:0x18c4, B:668:0x18c8), top: B:699:0x1809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:724:0x164d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 6506
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0.run():void");
    }
}
