package com.facebook.redex;

import X.AbstractActivityC119225dN;
import X.AbstractActivityC123635nW;
import X.AbstractC130285z6;
import X.AbstractC136196Lo;
import X.AnonymousClass017;
import X.AnonymousClass102;
import X.AnonymousClass1V8;
import X.AnonymousClass1V9;
import X.AnonymousClass3FE;
import X.AnonymousClass610;
import X.AnonymousClass619;
import X.AnonymousClass61E;
import X.AnonymousClass61F;
import X.AnonymousClass63Y;
import X.C117295Zj;
import X.C117305Zk;
import X.C117315Zl;
import X.C121385hl;
import X.C125705rg;
import X.C127645up;
import X.C127985vN;
import X.C128365vz;
import X.C128735wa;
import X.C12960it;
import X.C129665y6;
import X.C129685y8;
import X.C12970iu;
import X.C129865yQ;
import X.C130025yg;
import X.C130105yo;
import X.C130645zk;
import X.C130665zm;
import X.C130785zy;
import X.C1315463e;
import X.C18600si;
import X.C452120p;
import android.util.Pair;
import android.widget.CompoundButton;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.payments.ui.NoviPayHubSecurityActivity;
import com.whatsapp.util.Log;
import java.security.PublicKey;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes4.dex */
public class IDxAListenerShape0S0300000_3_I1 implements AbstractC136196Lo {
    public Object A00;
    public Object A01;
    public Object A02;
    public final int A03;

    public IDxAListenerShape0S0300000_3_I1(Object obj, Object obj2, Object obj3, int i) {
        this.A03 = i;
        this.A00 = obj3;
        this.A01 = obj;
        this.A02 = obj2;
    }

    @Override // X.AbstractC136196Lo
    public final void AV8(C130785zy r19) {
        AnonymousClass3FE r3;
        C129865yQ r1;
        C127985vN r11;
        Object obj;
        String str;
        C130785zy r12;
        AnonymousClass3FE r7;
        String str2;
        Object obj2;
        AnonymousClass619 r0;
        switch (this.A03) {
            case 0:
                AnonymousClass61E r72 = (AnonymousClass61E) this.A00;
                Pair pair = (Pair) this.A01;
                AbstractC136196Lo r8 = (AbstractC136196Lo) this.A02;
                if (r19.A06() && r19.A02 != null) {
                    r72.A06((String) pair.first);
                    PublicKey publicKey = (PublicKey) pair.second;
                    synchronized (r72) {
                        try {
                            C18600si r6 = r72.A01;
                            JSONObject A0b = C117295Zj.A0b(r6);
                            JSONObject optJSONObject = A0b.optJSONObject("bio");
                            if (optJSONObject == null) {
                                optJSONObject = C117295Zj.A0a();
                            }
                            optJSONObject.put("bioPublicKey", C117305Zk.A0n(publicKey.getEncoded()));
                            A0b.put("bio", optJSONObject);
                            C117295Zj.A1E(r6, A0b);
                        } catch (JSONException e) {
                            r72.A02.A0A("setPublicKey failed", e);
                        }
                    }
                }
                r8.AV8(r19);
                return;
            case 1:
                AnonymousClass61E r32 = (AnonymousClass61E) this.A00;
                C130105yo r13 = (C130105yo) this.A01;
                AbstractC136196Lo r2 = (AbstractC136196Lo) this.A02;
                if (r19.A06()) {
                    r32.A05();
                    C130105yo.A01(r13).remove("wavi_bio_skip_counter").apply();
                }
                r2.AV8(r19);
                return;
            case 2:
                AnonymousClass102 r82 = (AnonymousClass102) this.A00;
                AnonymousClass61F r73 = (AnonymousClass61F) this.A01;
                AbstractC136196Lo r62 = (AbstractC136196Lo) this.A02;
                if (r19.A06()) {
                    try {
                        AnonymousClass1V8 r14 = (AnonymousClass1V8) r19.A02;
                        AnonymousClass1V8 A0E = r14.A0E("name");
                        AnonymousClass1V8 A0E2 = r14.A0E("address");
                        AnonymousClass1V8 A0E3 = r14.A0E("saved_onboarding");
                        AnonymousClass1V8 A0E4 = r14.A0E("home_country");
                        C130645zk r33 = null;
                        String A0H = A0E != null ? A0E.A0H("legal_name") : null;
                        if (A0E2 != null) {
                            r11 = new C127985vN(A0E2.A0H("line_one"), A0E2.A0I("line_two", null), A0E2.A0H("city"), A0E2.A0H("state"), A0E2.A0H("zip"), A0E2.A0H("country_alpha2"));
                        } else {
                            r11 = null;
                        }
                        C130665zm A00 = A0E3 != null ? C130665zm.A00(A0E3) : null;
                        if (A0E4 != null) {
                            r33 = C130645zk.A00(r82, A0E4);
                        }
                        C127645up r02 = new C127645up(r33, r11, A00, A0H);
                        r73.A02 = r02;
                        C130785zy.A04(null, r62, r02);
                        return;
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("PAY: NoviCommonAction/getAccountInfo can't construct object");
                    }
                }
                C130785zy.A05(r62, r19);
                return;
            case 3:
                C129685y8 r83 = (C129685y8) this.A00;
                UserJid userJid = (UserJid) this.A01;
                AnonymousClass017 r63 = (AnonymousClass017) this.A02;
                if (!r19.A06() || (obj = r19.A02) == null) {
                    C130785zy.A02(r63, r19.A00, null);
                    return;
                }
                try {
                    AnonymousClass102 r9 = r83.A01;
                    AnonymousClass1V8 A0E5 = ((AnonymousClass1V8) obj).A0E("receiver");
                    String str3 = "";
                    if (A0E5 != null) {
                        str3 = A0E5.A0F("local").A0H("iso_code");
                        str = A0E5.A0F("primary").A0H("iso_code");
                    } else {
                        str = str3;
                    }
                    C128735wa r22 = new C128735wa(userJid, new AnonymousClass63Y(r9.A02(str3), r9.A02(str), str));
                    if (r22.A00()) {
                        r83.A00.A08(userJid, r22);
                        r12 = new C130785zy(null, r22);
                        r12.A01 = null;
                    } else {
                        r12 = new C130785zy(new C452120p(542720028), null);
                    }
                    r63.A0A(r12);
                    return;
                } catch (AnonymousClass1V9 unused2) {
                    Log.e("PAY: NoviSyncRepository/fetchReceiverPreferences can't parse receiver preferences");
                    C130785zy.A02(r63, r19.A00, null);
                    return;
                }
            case 4:
                r3 = (AnonymousClass3FE) this.A00;
                Runnable runnable = (Runnable) this.A01;
                r1 = (C129865yQ) this.A02;
                if (r19.A06()) {
                    C117315Zl.A0T(r3);
                    runnable.run();
                    return;
                }
                C129865yQ.A00(r1, r19);
                C117305Zk.A1E(r3);
                return;
            case 5:
                C129665y6 r23 = (C129665y6) this.A00;
                r3 = (AnonymousClass3FE) this.A01;
                r1 = (C129865yQ) this.A02;
                if (r19.A06()) {
                    AbstractC130285z6 r15 = (AbstractC130285z6) r19.A02;
                    if (r15 instanceof C121385hl) {
                        C121385hl r16 = (C121385hl) r15;
                        r23.A04.A03.A00.push(r16);
                        C117305Zk.A1G(r3, "stepUpType", r16.A01, C12970iu.A11());
                        return;
                    }
                    return;
                }
                if (r19.A01 != null) {
                    Log.e("PAY: KycStepUpManager/startKycStepUp expected TKYC step got step up instead.");
                    return;
                }
                C129865yQ.A00(r1, r19);
                C117305Zk.A1E(r3);
                return;
            case 6:
                C129665y6 r84 = (C129665y6) this.A00;
                r7 = (AnonymousClass3FE) this.A01;
                C129865yQ r64 = (C129865yQ) this.A02;
                if (r19.A06() && (obj2 = r19.A02) != null) {
                    try {
                        AnonymousClass1V8 r17 = (AnonymousClass1V8) obj2;
                        AnonymousClass1V8 A0F = r17.A0F("novi-account");
                        AnonymousClass1V8 A0F2 = r17.A0F("home_country");
                        AnonymousClass1V8 A0F3 = A0F.A0F("welcome_info");
                        AnonymousClass102 r03 = r84.A01;
                        C1315463e A002 = C1315463e.A00(r03, A0F);
                        C130645zk A003 = C130645zk.A00(r03, A0F2);
                        AnonymousClass1V8 A0E6 = A0F3.A0E("description");
                        if (A0E6 != null) {
                            r0 = AnonymousClass619.A00(A0E6);
                        } else {
                            r0 = null;
                        }
                        C130025yg r18 = r84.A03;
                        r18.A02 = A003;
                        r18.A00 = r0;
                        r18.A01 = A002.A00;
                        C117305Zk.A1G(r7, "decision", "APPROVE", C12970iu.A11());
                        return;
                    } catch (AnonymousClass1V9 unused3) {
                        Log.e("PAY: KycStepUpManager/getAccountStatus could not parse account status response");
                    }
                }
                r64.A02(r19.A00, null, null);
                str2 = "on_failure_handled_natively";
                r7.A00(str2);
                return;
            case 7:
                AbstractActivityC123635nW r24 = (AbstractActivityC123635nW) this.A00;
                AnonymousClass610 r110 = (AnonymousClass610) this.A01;
                r7 = (AnonymousClass3FE) this.A02;
                if (r19.A06()) {
                    C128365vz r111 = r110.A00;
                    r111.A0Q = "SUCCESSFUL";
                    r24.A07.A05(r111);
                    str2 = "on_success";
                    r7.A00(str2);
                    return;
                }
                C128365vz r112 = r110.A00;
                r112.A0Q = "FAILED";
                r24.A07.A05(r112);
                r24.A05.A02(r19.A00, new Runnable() { // from class: X.6GO
                    @Override // java.lang.Runnable
                    public final void run() {
                        C117305Zk.A1E(AnonymousClass3FE.this);
                    }
                }, null);
                return;
            case 8:
                NoviPayBloksActivity noviPayBloksActivity = (NoviPayBloksActivity) this.A00;
                Map map = (Map) this.A01;
                AnonymousClass3FE r85 = (AnonymousClass3FE) this.A02;
                if (r19.A06()) {
                    Iterator A0n = C12960it.A0n(map);
                    while (A0n.hasNext()) {
                        Map.Entry A15 = C12970iu.A15(A0n);
                        C130025yg r65 = noviPayBloksActivity.A0B;
                        Object key = A15.getKey();
                        Object value = A15.getValue();
                        synchronized (r65) {
                            C130665zm A01 = r65.A01();
                            C125705rg r113 = A01.A03;
                            if (r113 == null) {
                                r113 = new C125705rg(C12970iu.A11());
                                A01.A03 = r113;
                            }
                            r113.A00.put(key, value);
                        }
                    }
                    AbstractActivityC119225dN.A0L(r85, r19);
                    return;
                }
                r85.A00(C130785zy.A00(r19, noviPayBloksActivity));
                return;
            case 9:
                NoviPayHubSecurityActivity noviPayHubSecurityActivity = (NoviPayHubSecurityActivity) this.A00;
                CompoundButton compoundButton = (CompoundButton) this.A01;
                DialogFragment dialogFragment = (DialogFragment) this.A02;
                if (r19.A06()) {
                    compoundButton.setChecked(true);
                } else {
                    C129865yQ.A00(noviPayHubSecurityActivity.A04, r19);
                }
                if (dialogFragment != null) {
                    dialogFragment.A1B();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
