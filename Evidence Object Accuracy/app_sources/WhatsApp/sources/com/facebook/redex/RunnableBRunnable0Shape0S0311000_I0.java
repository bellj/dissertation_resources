package com.facebook.redex;

import X.AbstractC14640lm;
import X.AnonymousClass10Z;
import X.AnonymousClass2L7;
import X.C15370n3;
import X.C49402Kp;
import java.net.InetSocketAddress;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0311000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public boolean A04;
    public final int A05;

    public RunnableBRunnable0Shape0S0311000_I0(C15370n3 r2, AbstractC14640lm r3, AnonymousClass10Z r4, int i) {
        this.A05 = 4;
        this.A01 = r4;
        this.A02 = r2;
        this.A00 = i;
        this.A04 = true;
        this.A03 = r3;
    }

    public RunnableBRunnable0Shape0S0311000_I0(C49402Kp r2, AnonymousClass2L7 r3, InetSocketAddress inetSocketAddress, int i, boolean z) {
        this.A05 = i;
        this.A01 = r2;
        this.A02 = inetSocketAddress;
        this.A00 = 30000;
        this.A04 = z;
        this.A03 = r3;
    }

    public RunnableBRunnable0Shape0S0311000_I0(Object obj, Object obj2, Object obj3, int i, int i2, boolean z) {
        this.A05 = i2;
        this.A01 = obj;
        this.A02 = obj2;
        this.A04 = z;
        this.A00 = i;
        this.A03 = obj3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005f, code lost:
        if (r5.A05 == r2) goto L_0x0061;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 518
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S0311000_I0.run():void");
    }
}
