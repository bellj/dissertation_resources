package com.facebook.redex;

import X.AnonymousClass5SU;

/* loaded from: classes3.dex */
public class IDxEventShape18S0100000_2_I1 implements AnonymousClass5SU {
    public Object A00;
    public final int A01;

    public IDxEventShape18S0100000_2_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r2.A01 != 0) goto L_0x003e;
     */
    @Override // X.AnonymousClass5SU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AJ7(java.lang.Object r4) {
        /*
            r3 = this;
            int r0 = r3.A01
            switch(r0) {
                case 0: goto L_0x0073;
                case 1: goto L_0x0067;
                case 2: goto L_0x005b;
                case 3: goto L_0x004f;
                case 4: goto L_0x0043;
                case 5: goto L_0x0005;
                case 6: goto L_0x002a;
                case 7: goto L_0x001e;
                case 8: goto L_0x0012;
                case 9: goto L_0x0006;
                default: goto L_0x0005;
            }
        L_0x0005:
            return
        L_0x0006:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            X.3A1 r0 = r0.A03
            r4.ATs(r0)
            return
        L_0x0012:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            int r0 = r0.A00
            r4.ATq(r0)
            return
        L_0x001e:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            java.util.List r0 = r0.A0A
            r4.AWU(r0)
            return
        L_0x002a:
            java.lang.Object r2 = r3.A00
            X.4cy r2 = (X.C95034cy) r2
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            int r1 = r2.A00
            r0 = 3
            if (r1 != r0) goto L_0x003e
            boolean r0 = r2.A0D
            if (r0 == 0) goto L_0x003e
            int r1 = r2.A01
            r0 = 1
            if (r1 == 0) goto L_0x003f
        L_0x003e:
            r0 = 0
        L_0x003f:
            r4.ARY(r0)
            return
        L_0x0043:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            boolean r0 = r0.A0B
            r4.ARX(r0)
            return
        L_0x004f:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            X.4be r0 = r0.A04
            r4.ATo(r0)
            return
        L_0x005b:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            boolean r0 = r0.A0E
            r4.AQ4(r0)
            return
        L_0x0067:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            int r0 = r0.A01
            r4.ATr(r0)
            return
        L_0x0073:
            java.lang.Object r0 = r3.A00
            X.4cy r0 = (X.C95034cy) r0
            X.5XZ r4 = (X.AnonymousClass5XZ) r4
            boolean r1 = r0.A0D
            int r0 = r0.A00
            r4.ATt(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxEventShape18S0100000_2_I1.AJ7(java.lang.Object):void");
    }
}
