package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC28901Pl;
import X.AnonymousClass01J;
import X.AnonymousClass10D;
import X.AnonymousClass10G;
import X.AnonymousClass18L;
import X.AnonymousClass1IS;
import X.AnonymousClass23a;
import X.AnonymousClass2JY;
import X.AnonymousClass32I;
import X.AnonymousClass3XX;
import X.C1105055u;
import X.C14820m6;
import X.C14850m9;
import X.C14950mJ;
import X.C16700pc;
import X.C17910rb;
import X.C18000rk;
import X.C18340sI;
import X.C18790t3;
import X.C30041Vv;
import X.C30761Ys;
import X.C43661xO;
import X.C48182Et;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import com.whatsapp.notification.MessageOTPNotificationBroadcastReceiver;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S2200000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public String A02;
    public String A03;
    public final int A04;

    public RunnableBRunnable0Shape0S2200000_I0(Object obj, Object obj2, String str, String str2, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A02 = str;
        this.A03 = str2;
        this.A01 = obj2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A04) {
            case 0:
                MessageOTPNotificationBroadcastReceiver messageOTPNotificationBroadcastReceiver = (MessageOTPNotificationBroadcastReceiver) this.A00;
                String str = this.A02;
                String str2 = this.A03;
                Object obj = this.A01;
                AbstractC15340mz A03 = messageOTPNotificationBroadcastReceiver.A03.A0K.A03(new AnonymousClass1IS(AbstractC14640lm.A01(str), str2, false));
                if (A03 != null && C30041Vv.A0V(messageOTPNotificationBroadcastReceiver.A02.A04, A03)) {
                    C30761Ys A00 = messageOTPNotificationBroadcastReceiver.A02.A00(A03);
                    if (messageOTPNotificationBroadcastReceiver.A02.A06(A00)) {
                        messageOTPNotificationBroadcastReceiver.A00.A0H(new RunnableBRunnable0Shape6S0200000_I0_6(messageOTPNotificationBroadcastReceiver, 29, A03));
                    } else if (messageOTPNotificationBroadcastReceiver.A02.A07(A00)) {
                        messageOTPNotificationBroadcastReceiver.A00.A0H(new RunnableBRunnable0Shape1S0300000_I0_1(messageOTPNotificationBroadcastReceiver, A03, obj, 16));
                    }
                    messageOTPNotificationBroadcastReceiver.A01.A02(A03.A0C(), true, true);
                    return;
                }
                return;
            case 1:
                C17910rb r7 = (C17910rb) this.A00;
                AbstractC28901Pl r6 = (AbstractC28901Pl) this.A01;
                String str3 = this.A02;
                String str4 = this.A03;
                C16700pc.A0E(r7, 0);
                C16700pc.A0E(r6, 1);
                C16700pc.A0E(str3, 2);
                C16700pc.A0E(str4, 3);
                Context context = r7.A03.A00;
                Intent intent = new Intent();
                intent.setClassName(context.getPackageName(), "com.whatsapp.payments.phoenix.flowconfigurationservice.npci.IndiaUpiFcsPinHandlerActivity");
                intent.putExtra("extra_bank_account", r6);
                intent.putExtra("extra_india_upi_pin_op", str3);
                intent.putExtra("extra_fds_manager_id", str4);
                String str5 = r7.A01;
                if (str5 == null) {
                    C16700pc.A0K("observerId");
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
                intent.putExtra("extra_fcs_observer_id", str5);
                intent.setFlags(268435456);
                context.startActivity(intent);
                return;
            case 2:
                ((AnonymousClass2JY) this.A00).A0F.A01(new C43661xO(((C48182Et) this.A01).A02, this.A02, this.A03));
                return;
            case 3:
                C18340sI r2 = (C18340sI) this.A00;
                String str6 = this.A02;
                String str7 = this.A03;
                AnonymousClass23a r0 = (AnonymousClass23a) this.A01;
                AnonymousClass10D r72 = r2.A05;
                Context context2 = r2.A03.A00;
                C14950mJ r8 = r72.A02;
                long A02 = r8.A02();
                long[] jArr = new long[1];
                if (r72.A01.A03(new C1105055u(r72, jArr))) {
                    jArr[0] = r8.A01();
                }
                String externalStorageState = Environment.getExternalStorageState();
                AnonymousClass10G r4 = r72.A00;
                String A04 = r4.A00.A04(context2, null, "blocked_ban_appeals", null, null, externalStorageState, null, null, jArr[0], A02, true, false);
                AnonymousClass01J r5 = r2.A00.A00.A01;
                AnonymousClass32I r42 = new AnonymousClass32I((C18790t3) r5.AJw.get(), (C14820m6) r5.AN3.get(), (C14850m9) r5.A04.get(), (AnonymousClass18L) r5.A89.get(), C18000rk.A00(r5.AMu), str6, str7, A04, r5.AKl, r5.A1J);
                if (r2.A02.A0B()) {
                    r42.AZO(new AnonymousClass3XX(r0, r2));
                    return;
                } else if (r0 != null) {
                    r0.AQB(1);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
