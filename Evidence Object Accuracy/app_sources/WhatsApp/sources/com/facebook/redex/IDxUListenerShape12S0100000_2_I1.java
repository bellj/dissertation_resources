package com.facebook.redex;

import X.AbstractC92674Wx;
import X.AnonymousClass23G;
import X.AnonymousClass39B;
import X.AnonymousClass3DO;
import X.C12960it;
import X.C64523Fw;
import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.whatsapp.calling.callgrid.view.VoiceParticipantAudioWave;
import com.whatsapp.components.RoundCornerProgressBar;
import com.whatsapp.voipcalling.VoipActivityV2;

/* loaded from: classes3.dex */
public class IDxUListenerShape12S0100000_2_I1 implements ValueAnimator.AnimatorUpdateListener {
    public Object A00;
    public final int A01;

    public IDxUListenerShape12S0100000_2_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        switch (this.A01) {
            case 0:
                ((Drawable) this.A00).invalidateSelf();
                return;
            case 1:
                ((CollapsingToolbarLayout) this.A00).setScrimAlpha(C12960it.A05(valueAnimator.getAnimatedValue()));
                return;
            case 2:
                ((AnonymousClass23G) this.A00).A01.setAlpha(C12960it.A00(valueAnimator));
                return;
            case 3:
                ((View) this.A00).setX(C12960it.A00(valueAnimator));
                return;
            case 4:
                VoiceParticipantAudioWave voiceParticipantAudioWave = (VoiceParticipantAudioWave) this.A00;
                voiceParticipantAudioWave.A00 = C12960it.A00(valueAnimator);
                voiceParticipantAudioWave.invalidate();
                return;
            case 5:
                ((C64523Fw) this.A00).A0C.setTranslationY(C12960it.A00(valueAnimator));
                return;
            case 6:
                RoundCornerProgressBar roundCornerProgressBar = (RoundCornerProgressBar) this.A00;
                roundCornerProgressBar.A00 = C12960it.A00(valueAnimator);
                roundCornerProgressBar.postInvalidate();
                return;
            case 7:
                AbstractC92674Wx r2 = (AbstractC92674Wx) this.A00;
                float A00 = C12960it.A00(valueAnimator);
                valueAnimator.getCurrentPlayTime();
                r2.A01 = true;
                r2.A00 = A00;
                return;
            case 8:
                View view = (View) this.A00;
                int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.height = A05;
                    view.setLayoutParams(layoutParams);
                    return;
                }
                return;
            case 9:
                ((View) this.A00).setBackgroundColor(C12960it.A05(valueAnimator.getAnimatedValue()));
                return;
            case 10:
                AnonymousClass39B.A00(valueAnimator, (AnonymousClass39B) this.A00);
                return;
            case 11:
                AnonymousClass3DO r1 = ((VoipActivityV2) this.A00).A0n;
                r1.A00 = C12960it.A00(valueAnimator);
                r1.A00();
                return;
            default:
                return;
        }
    }
}
