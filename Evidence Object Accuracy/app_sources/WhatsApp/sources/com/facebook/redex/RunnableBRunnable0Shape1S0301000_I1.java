package com.facebook.redex;

import X.AbstractC15710nm;
import X.AnonymousClass0RD;
import X.AnonymousClass3IV;
import X.AnonymousClass3J1;
import X.C14330lG;
import X.C20870wS;
import X.C22590zK;
import X.C26511Dt;
import X.C26521Du;
import X.C28781Oz;
import X.C28921Pn;
import X.C37891nB;
import X.C41471ta;
import X.C54062g7;
import X.C71203cY;
import X.C91574Sg;
import java.io.File;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0301000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public RunnableBRunnable0Shape1S0301000_I1(Object obj, Object obj2, Object obj3, int i, int i2) {
        this.A04 = i2;
        this.A01 = obj;
        this.A03 = obj2;
        this.A02 = obj3;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        switch (this.A04) {
            case 0:
                C91574Sg r4 = (C91574Sg) this.A01;
                r4.A04.execute(new RunnableBRunnable0Shape11S0200000_I1_1(this, 23, AnonymousClass0RD.A00(new C54062g7((C71203cY) this.A03, (C71203cY) this.A02, r4))));
                return;
            case 1:
                AnonymousClass3IV r0 = (AnonymousClass3IV) this.A01;
                int i = this.A00;
                C37891nB r42 = (C37891nB) this.A03;
                AbstractC15710nm r1 = r0.A01;
                C14330lG r2 = r0.A02;
                C26511Dt r9 = r0.A0H;
                C20870wS r3 = r0.A03;
                C26521Du r10 = r0.A0I;
                C22590zK r8 = r0.A0G;
                C41471ta r7 = r0.A0D;
                C28781Oz r6 = r0.A0C;
                C28921Pn r5 = r0.A0B;
                AnonymousClass3J1.A02(r1, r2, r3, r42, r5, r6, r7, r8, r9, r10, (File) this.A02, r5.A0s, i);
                return;
            default:
                return;
        }
    }
}
