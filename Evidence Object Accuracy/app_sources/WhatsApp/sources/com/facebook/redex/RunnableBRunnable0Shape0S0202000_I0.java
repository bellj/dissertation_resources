package com.facebook.redex;

import X.AbstractC15340mz;
import X.AnonymousClass009;
import X.AnonymousClass15Y;
import X.AnonymousClass1BD;
import X.AnonymousClass1FY;
import X.AnonymousClass1OT;
import X.C15370n3;
import X.C15380n4;
import X.C15940oB;
import X.C15950oC;
import X.C15990oG;
import X.C16030oK;
import X.C16050oM;
import X.C16120oU;
import X.C31311aL;
import X.C458223h;
import X.C471629h;
import X.C471829j;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendLiveLocationKeyJob;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0202000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public int A01;
    public Object A02;
    public Object A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0202000_I0(AnonymousClass15Y r2, Object obj, int i, int i2, int i3) {
        this.A04 = i3;
        if (i3 != 0) {
            this.A02 = r2;
            this.A00 = i;
        } else {
            this.A02 = r2;
            this.A00 = 2;
        }
        this.A01 = i2;
        this.A03 = obj;
    }

    public RunnableBRunnable0Shape0S0202000_I0(Object obj, int i, Object obj2, int i2, int i3) {
        this.A04 = i3;
        this.A02 = obj;
        this.A00 = i;
        this.A03 = obj2;
        this.A01 = i2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        long j;
        UserJid A01;
        C15370n3 A0A;
        switch (this.A04) {
            case 0:
            case 1:
                AnonymousClass15Y r3 = (AnonymousClass15Y) this.A02;
                int i = this.A00;
                int i2 = this.A01;
                Object obj = this.A03;
                r3.A00(i);
                r3.A01(obj, i2, i);
                return;
            case 2:
                C16120oU r6 = (C16120oU) this.A02;
                int i3 = this.A00;
                byte[] bArr = (byte[]) this.A03;
                int i4 = this.A01;
                if (r6.A0I()) {
                    if (r6.A0O && !r6.A0M) {
                        r6.A0E.A06(null, C471629h.A00());
                        r6.A0M = true;
                    }
                    r6.A03.A04(bArr, i3, i4);
                    r6.A03.A01();
                    r6.A02();
                    return;
                }
                return;
            case 3:
                AnonymousClass1FY r7 = (AnonymousClass1FY) this.A02;
                DeviceJid deviceJid = (DeviceJid) this.A03;
                int i5 = this.A00;
                int i6 = this.A01;
                C15950oC A02 = C15940oB.A02(deviceJid);
                StringBuilder sb = new StringBuilder("axolotl checking sessions for ");
                sb.append(A02);
                sb.append(" due to retry receipt for ");
                sb.append(deviceJid);
                Log.i(sb.toString());
                C15990oG r32 = r7.A03;
                byte[] bArr2 = null;
                if (r32.A0g(A02)) {
                    C31311aL r9 = r32.A0G(A02).A01;
                    if (r9.A00.A03 != i5) {
                        StringBuilder sb2 = new StringBuilder("axolotl deleting session due to registration id change for ");
                        sb2.append(deviceJid);
                        sb2.append(" stop retrying");
                        Log.i(sb2.toString());
                        r32.A0I.A00();
                        r32.A0H(A02);
                        r32.A0W(A02);
                        return;
                    }
                    if (i6 >= 2) {
                        StringBuilder sb3 = new StringBuilder("axolotl requiring new session before resending for ");
                        sb3.append(deviceJid);
                        Log.i(sb3.toString());
                        bArr2 = r9.A00.A05.A04();
                    }
                    if (i6 > 2 && r32.A0h(A02, deviceJid.getUserJid())) {
                        Log.i("axolotl will wait to send notification until a new prekey has been fetched");
                        return;
                    } else if (i6 == 2) {
                        Log.i("axolotl will record the base key used to send ");
                        r32.A0X(A02, deviceJid.getUserJid(), bArr2);
                    }
                }
                r7.A02.A00(new SendLiveLocationKeyJob(deviceJid, bArr2, i6));
                return;
            case 4:
                AnonymousClass1FY r72 = (AnonymousClass1FY) this.A02;
                AnonymousClass1OT r62 = (AnonymousClass1OT) this.A03;
                int i7 = this.A00;
                int i8 = this.A01;
                StringBuilder sb4 = new StringBuilder("need to send retry receipt; stanzaKey=");
                sb4.append(r62);
                Log.i(sb4.toString());
                byte[] A03 = C16050oM.A03(i7);
                if (i8 > 1) {
                    r72.A01.A01();
                }
                StringBuilder sb5 = new StringBuilder("axolotl sending retry receipt; stanzaKey=");
                sb5.append(r62);
                sb5.append("; localRegistrationId=");
                sb5.append(i7);
                Log.i(sb5.toString());
                C16030oK r1 = r72.A05;
                UserJid of = UserJid.of(C15380n4.A00(r62.A02));
                AnonymousClass009.A05(of);
                r1.A0U(of, A03, i8 + 1);
                return;
            case 5:
                AnonymousClass1BD r33 = (AnonymousClass1BD) this.A02;
                int i9 = this.A00;
                int i10 = this.A01;
                AbstractC15340mz r4 = (AbstractC15340mz) this.A03;
                C471829j r2 = new C471829j();
                C458223h r0 = r33.A00;
                if (r0 == null) {
                    j = 0;
                } else {
                    j = r0.A05;
                }
                r2.A04 = Long.valueOf(j);
                r2.A03 = Integer.valueOf(i9);
                r2.A02 = Integer.valueOf(i10);
                if (!(!r33.A09.A07(1254) || (A01 = AnonymousClass1BD.A01(r4)) == null || (A0A = r33.A03.A0A(A01)) == null)) {
                    r2.A00 = Boolean.valueOf(A0A.A0J());
                    r2.A01 = Boolean.valueOf(A0A.A0I());
                }
                r33.A0A.A06(r2);
                return;
            default:
                return;
        }
    }
}
