package com.facebook.redex;

import android.view.View;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape8S0100000_I1_2 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape8S0100000_I1_2(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:127:0x0374  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0381  */
    /* JADX WARNING: Removed duplicated region for block: B:317:0x088e  */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x0893  */
    /* JADX WARNING: Removed duplicated region for block: B:322:0x08a4  */
    /* JADX WARNING: Removed duplicated region for block: B:325:0x08ab  */
    /* JADX WARNING: Removed duplicated region for block: B:328:0x08c6  */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r25) {
        /*
        // Method dump skipped, instructions count: 2394
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2.onClick(android.view.View):void");
    }
}
