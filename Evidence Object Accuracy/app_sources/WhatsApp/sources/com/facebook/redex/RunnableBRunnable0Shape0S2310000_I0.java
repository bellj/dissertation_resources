package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AnonymousClass1B4;
import X.AnonymousClass1IZ;
import X.C63933Dm;
import X.C90934Pu;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S2310000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public String A03;
    public String A04;
    public boolean A05;
    public final int A06;

    public RunnableBRunnable0Shape0S2310000_I0(Object obj, Object obj2, Object obj3, String str, String str2, int i, boolean z) {
        this.A06 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = str;
        this.A04 = str2;
        this.A05 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C90934Pu r1;
        int i;
        switch (this.A06) {
            case 0:
                AnonymousClass1B4 r5 = (AnonymousClass1B4) this.A00;
                C63933Dm r4 = (C63933Dm) this.A01;
                try {
                    r4.A00(r5.A00(this.A03, this.A04, (JSONObject) this.A02, this.A05));
                    return;
                } catch (Exception e) {
                    if (e instanceof JSONException) {
                        r4.A00.A02.AaV("BusinessDirectoryNetworkRequest/createResponseCallback/onError: Error while parsing the JSON: ", e.getMessage(), true);
                        r1 = r4.A01;
                        i = 2;
                    } else if (e instanceof IOException) {
                        Log.e("BusinessDirectoryNetworkRequest/createResponseCallback/onError", e);
                        r1 = r4.A01;
                        r1.A00 = 1;
                        r4.A00.A06(r1);
                        return;
                    } else {
                        Log.e("BusinessDirectoryNetworkRequest/createResponseCallback/onError: generic error - ", e);
                        r1 = r4.A01;
                        i = 3;
                    }
                    r1.A00 = i;
                    r4.A00.A06(r1);
                    return;
                }
            case 1:
                ((AnonymousClass1IZ) this.A00).A01((AbstractC14640lm) this.A01, (AbstractC15340mz) this.A02, this.A03, this.A04, this.A05);
                return;
            default:
                return;
        }
    }
}
