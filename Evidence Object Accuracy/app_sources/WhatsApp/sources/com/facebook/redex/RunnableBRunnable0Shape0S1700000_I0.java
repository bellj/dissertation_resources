package com.facebook.redex;

import X.AbstractC14440lR;
import X.AbstractC16390ow;
import X.AbstractC20460vn;
import X.AbstractC21180x0;
import X.AnonymousClass15S;
import X.AnonymousClass15V;
import X.AnonymousClass15Z;
import X.AnonymousClass1A7;
import X.AnonymousClass1QA;
import X.C14830m7;
import X.C14900mE;
import X.C15650ng;
import X.C20370ve;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S1700000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public Object A06;
    public String A07;
    public final int A08 = 1;

    public RunnableBRunnable0Shape0S1700000_I0(C14830m7 r2, AnonymousClass15S r3, AnonymousClass15Z r4, AbstractC20460vn r5, AnonymousClass15V r6, AbstractC21180x0 r7, AbstractC14440lR r8) {
        this.A05 = r2;
        this.A06 = r8;
        this.A00 = r7;
        this.A03 = r5;
        this.A01 = r3;
        this.A04 = r6;
        this.A02 = r4;
        this.A07 = "2.22.17.70";
    }

    public RunnableBRunnable0Shape0S1700000_I0(C14900mE r2, C15650ng r3, C20370ve r4, AnonymousClass1QA r5, AnonymousClass1A7 r6, AbstractC16390ow r7, AbstractC14440lR r8, String str) {
        this.A00 = r4;
        this.A07 = str;
        this.A01 = r8;
        this.A02 = r3;
        this.A03 = r7;
        this.A04 = r2;
        this.A05 = r5;
        this.A06 = r6;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01e7, code lost:
        ((X.AbstractC20460vn) r23.A03).Af3(r2.AAh());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01f2, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x024a, code lost:
        if (X.AnonymousClass1QC.A00.contains(java.lang.Integer.valueOf(r2)) == false) goto L_0x024c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0269, code lost:
        if (0 == 0) goto L_0x0271;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006d, code lost:
        r1.AbK(r2, r0, 50);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0070, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00bd, code lost:
        if (r5.length < r9) goto L_0x00bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01db, code lost:
        if (r1.A03() == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01dd, code lost:
        r2 = (X.AbstractC21180x0) r23.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01e5, code lost:
        if (r2.AID() == false) goto L_?;
     */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x01c2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x018c  */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 640
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape0S1700000_I0.run():void");
    }
}
