package com.facebook.redex;

import X.AnonymousClass1US;
import X.AnonymousClass5WJ;
import X.C83613xW;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.widget.EditText;
import com.whatsapp.status.playback.widget.StatusEditText;
import com.whatsapp.text.ReadMoreTextView;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0102000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public int A01;
    public Object A02;
    public final int A03 = 0;

    public RunnableBRunnable0Shape0S0102000_I0(StatusEditText statusEditText, int i, int i2) {
        this.A02 = statusEditText;
        this.A00 = i;
        this.A01 = i2;
    }

    public RunnableBRunnable0Shape0S0102000_I0(ReadMoreTextView readMoreTextView) {
        this.A02 = readMoreTextView;
    }

    @Override // java.lang.Runnable
    public final void run() {
        int i;
        switch (this.A03) {
            case 0:
                EditText editText = (EditText) this.A02;
                int i2 = this.A00;
                int i3 = this.A01;
                if (editText.getText() != null) {
                    i = editText.getText().length();
                } else {
                    i = 0;
                }
                if (i2 <= i && i3 <= i) {
                    editText.setSelection(i2, i3);
                    return;
                }
                return;
            case 1:
                ReadMoreTextView readMoreTextView = (ReadMoreTextView) this.A02;
                if (readMoreTextView.A00 != 0 && !readMoreTextView.A0I()) {
                    int width = (readMoreTextView.getWidth() - readMoreTextView.getPaddingLeft()) - readMoreTextView.getPaddingRight();
                    int height = (readMoreTextView.getHeight() - readMoreTextView.getPaddingTop()) - readMoreTextView.getPaddingBottom();
                    if ((this.A01 != width || this.A00 != height) && readMoreTextView.getLayout() != null) {
                        this.A01 = width;
                        this.A00 = height;
                        CharSequence charSequence = readMoreTextView.A04;
                        AnonymousClass5WJ r7 = ReadMoreTextView.A0B;
                        Layout A8L = r7.A8L(readMoreTextView, AnonymousClass1US.A01(charSequence), width);
                        boolean z = false;
                        if (A8L.getLineCount() > readMoreTextView.A00) {
                            z = true;
                        }
                        readMoreTextView.A05 = z;
                        if (z) {
                            CharSequence charSequence2 = readMoreTextView.A03;
                            if (charSequence2 != null) {
                                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence2);
                                spannableStringBuilder.setSpan(new C83613xW(readMoreTextView.getContext(), this, readMoreTextView.A01), 0, spannableStringBuilder.length(), 18);
                                if (readMoreTextView.A06) {
                                    spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 18);
                                }
                                int lineCount = A8L.getLineCount();
                                SpannableStringBuilder spannableStringBuilder2 = null;
                                for (int lineEnd = A8L.getLineEnd(readMoreTextView.A00 - 1); lineEnd > 0 && lineCount > readMoreTextView.A00; lineEnd--) {
                                    spannableStringBuilder2 = new SpannableStringBuilder(readMoreTextView.A04.subSequence(0, lineEnd));
                                    spannableStringBuilder2.append((CharSequence) "... ").append((CharSequence) spannableStringBuilder);
                                    lineCount = r7.A8L(readMoreTextView, AnonymousClass1US.A01(spannableStringBuilder2), width).getLineCount();
                                }
                                if (!TextUtils.equals(readMoreTextView.getText(), spannableStringBuilder2)) {
                                    readMoreTextView.setVisibleText(spannableStringBuilder2);
                                    return;
                                }
                                return;
                            }
                            throw new NullPointerException("You must specify an rmtvText attribute");
                        } else if (!TextUtils.equals(readMoreTextView.getText(), readMoreTextView.A04)) {
                            readMoreTextView.setVisibleText(readMoreTextView.A04);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
