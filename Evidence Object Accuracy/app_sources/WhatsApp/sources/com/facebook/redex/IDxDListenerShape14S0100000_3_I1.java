package com.facebook.redex;

import X.AbstractActivityC121655j9;
import X.AbstractActivityC121665jA;
import X.AbstractActivityC121685jC;
import X.AbstractC118045bB;
import X.AnonymousClass3FE;
import X.C118135bK;
import X.C118145bL;
import X.C127055ts;
import X.C127945vJ;
import X.C12960it;
import X.C12970iu;
import X.C1329168s;
import X.C43951xu;
import android.app.Activity;
import android.content.DialogInterface;
import androidx.fragment.app.DialogFragment;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiQrCodeUrlValidationActivity;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* loaded from: classes4.dex */
public class IDxDListenerShape14S0100000_3_I1 implements DialogInterface.OnDismissListener {
    public Object A00;
    public final int A01;

    public IDxDListenerShape14S0100000_3_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public final void onDismiss(DialogInterface dialogInterface) {
        switch (this.A01) {
            case 0:
                ((AnonymousClass3FE) this.A00).A00("on_dismiss");
                return;
            case 1:
                Runnable runnable = (Runnable) this.A00;
                if (runnable != null) {
                    runnable.run();
                    return;
                }
                return;
            case 2:
            case 10:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 18:
            case 19:
                ((Activity) this.A00).finish();
                return;
            case 3:
            case 4:
            case 6:
            case 23:
                ((DialogFragment) this.A00).A1C();
                return;
            case 5:
            case 24:
                ((PinBottomSheetDialogFragment) this.A00).A05.setText((CharSequence) null);
                return;
            case 7:
                DialogFragment dialogFragment = (DialogFragment) this.A00;
                if (dialogFragment.A0c()) {
                    dialogFragment.A1C();
                    return;
                }
                return;
            case 8:
            case 9:
                AbstractActivityC121655j9 r1 = (AbstractActivityC121655j9) this.A00;
                r1.A3L(r1.A08);
                return;
            case 11:
                IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity = (IndiaUpiMandatePaymentActivity) this.A00;
                indiaUpiMandatePaymentActivity.finish();
                ((AbstractActivityC121665jA) indiaUpiMandatePaymentActivity).A0D.AKh(C12960it.A0V(), C12970iu.A0h(), "approve_mandate_prompt", indiaUpiMandatePaymentActivity.A04, true);
                return;
            case 12:
                IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity2 = (IndiaUpiMandatePaymentActivity) this.A00;
                indiaUpiMandatePaymentActivity2.A01.A04();
                ((AbstractActivityC121665jA) indiaUpiMandatePaymentActivity2).A0D.AKh(C12960it.A0V(), C12970iu.A0h(), "decline_mandate_dialogue", indiaUpiMandatePaymentActivity2.A04, true);
                return;
            case 15:
                IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity3 = (IndiaUpiMandatePaymentActivity) this.A00;
                indiaUpiMandatePaymentActivity3.A01.A04();
                ((AbstractActivityC121665jA) indiaUpiMandatePaymentActivity3).A0D.AKh(C12960it.A0V(), C12970iu.A0h(), "approve_mandate_update_request_prompt", "payment_transaction_details", true);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((IndiaUpiMandatePaymentActivity) this.A00).A01.A04();
                return;
            case 17:
                AbstractActivityC121685jC r12 = (AbstractActivityC121685jC) this.A00;
                r12.A01 = 7;
                r12.A2m(null);
                return;
            case C43951xu.A01:
                C118135bK r2 = ((IndiaUpiQrCodeUrlValidationActivity) this.A00).A05;
                if (!r2.A03) {
                    C127945vJ.A00(r2.A01, 0);
                    return;
                } else {
                    r2.A03 = false;
                    return;
                }
            case 21:
                Activity activity = (Activity) this.A00;
                activity.finish();
                activity.overridePendingTransition(0, 0);
                return;
            case 22:
                ((C1329168s) this.A00).A00.A1B();
                return;
            case 25:
                C127055ts.A00(((AbstractC118045bB) this.A00).A01, 502);
                return;
            case 26:
            case 27:
            case 28:
                ((C118145bL) this.A00).A0x.A0B(null);
                return;
            default:
                return;
        }
    }
}
