package com.facebook.redex;

import android.view.View;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0000000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public final int A00;

    public ViewOnClickCListenerShape0S0000000_I0(int i) {
        this.A00 = i;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        if (this.A00 == 0) {
            Log.i("gdrive-activity/show-import-skip-dialog");
        }
    }
}
