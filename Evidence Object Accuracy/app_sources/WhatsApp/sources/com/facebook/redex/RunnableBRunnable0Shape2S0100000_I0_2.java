package com.facebook.redex;

import X.ActivityC13790kL;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass00T;
import X.AnonymousClass016;
import X.AnonymousClass01E;
import X.AnonymousClass04S;
import X.AnonymousClass10K;
import X.AnonymousClass1KZ;
import X.AnonymousClass1OE;
import X.AnonymousClass1OF;
import X.AnonymousClass1ON;
import X.AnonymousClass1Tv;
import X.AnonymousClass1UB;
import X.AnonymousClass1US;
import X.AnonymousClass1VO;
import X.AnonymousClass1WF;
import X.AnonymousClass29E;
import X.AnonymousClass29J;
import X.AnonymousClass29K;
import X.AnonymousClass29M;
import X.AnonymousClass29R;
import X.AnonymousClass29S;
import X.AnonymousClass29T;
import X.AnonymousClass29U;
import X.AnonymousClass29Y;
import X.C003501n;
import X.C004802e;
import X.C14330lG;
import X.C14350lI;
import X.C14820m6;
import X.C14900mE;
import X.C14960mK;
import X.C15820nx;
import X.C15900o5;
import X.C16310on;
import X.C16560pF;
import X.C16700pc;
import X.C17460qq;
import X.C22210yi;
import X.C22730zY;
import X.C235412b;
import X.C235512c;
import X.C247816v;
import X.C25721Am;
import X.C25991Bp;
import X.C26551Dx;
import X.C32781cj;
import X.C32811cm;
import X.C43951xu;
import X.C44771zW;
import X.C471229a;
import X.C471329b;
import X.EnumC16570pG;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Process;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.authentication.AppAuthenticationActivity;
import com.whatsapp.authentication.FingerprintView;
import com.whatsapp.authentication.VerifyTwoFactorAuthCodeDialogFragment;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity;
import com.whatsapp.backup.google.GoogleDriveRestoreAnimationView;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.backup.google.viewmodel.RestoreFromBackupViewModel;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape2S0100000_I0_2 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape2S0100000_I0_2(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        File file;
        long j;
        C22730zY r0;
        String str;
        AnonymousClass1OF r02;
        AnonymousClass016 r1;
        int i;
        String str2;
        C14900mE r2;
        int i2;
        switch (this.A01) {
            case 0:
                AppAuthenticationActivity.A02((AppAuthenticationActivity) this.A00);
                return;
            case 1:
                FingerprintView.A00((FingerprintView) this.A00);
                return;
            case 2:
                ((VerifyTwoFactorAuthCodeDialogFragment) this.A00).AXu();
                return;
            case 3:
                AnonymousClass01E r4 = (AnonymousClass01E) this.A00;
                C004802e r12 = new C004802e(r4.A0p());
                r12.A06(R.string.settings_two_factor_auth_disable_confirm);
                AnonymousClass04S create = r12.create();
                create.A03(-1, r4.A0I(R.string.settings_two_factor_auth_disable), new IDxCListenerShape9S0100000_2_I1(r4, 9));
                create.A03(-2, r4.A0I(R.string.cancel), new IDxCListenerShape4S0000000_2_I1(4));
                create.show();
                return;
            case 4:
                VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment = (VerifyTwoFactorAuthCodeDialogFragment) this.A00;
                verifyTwoFactorAuthCodeDialogFragment.A04.A03();
                C004802e r13 = new C004802e(verifyTwoFactorAuthCodeDialogFragment.A0p());
                r13.A06(R.string.two_factor_auth_save_error);
                AnonymousClass04S create2 = r13.create();
                create2.A03(-1, verifyTwoFactorAuthCodeDialogFragment.A0I(R.string.retry), new IDxCListenerShape9S0100000_2_I1(verifyTwoFactorAuthCodeDialogFragment, 8));
                create2.A03(-2, verifyTwoFactorAuthCodeDialogFragment.A0I(R.string.cancel), new IDxCListenerShape8S0100000_1_I1(verifyTwoFactorAuthCodeDialogFragment, 6));
                create2.show();
                return;
            case 5:
                VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment2 = (VerifyTwoFactorAuthCodeDialogFragment) this.A00;
                verifyTwoFactorAuthCodeDialogFragment2.A03.requestFocus();
                verifyTwoFactorAuthCodeDialogFragment2.A03.A04(false);
                return;
            case 6:
                VerifyTwoFactorAuthCodeDialogFragment verifyTwoFactorAuthCodeDialogFragment3 = (VerifyTwoFactorAuthCodeDialogFragment) this.A00;
                verifyTwoFactorAuthCodeDialogFragment3.A04.A03();
                verifyTwoFactorAuthCodeDialogFragment3.A04.A07(R.string.two_factor_auth_disabled, 0);
                verifyTwoFactorAuthCodeDialogFragment3.A1L();
                return;
            case 7:
                C17460qq r5 = (C17460qq) this.A00;
                C16700pc.A0E(r5, 0);
                AnonymousClass1KZ A00 = r5.A00.A00(AnonymousClass1WF.A00);
                C235512c r42 = r5.A04;
                String str3 = A00.A0D;
                if (r42.A03(str3) != null) {
                    r42.A0Y.Aaz(new AnonymousClass29R(r42.A0P, r5.A03, r42), str3);
                }
                C22210yi r3 = r5.A01;
                r3.A09.execute(new RunnableBRunnable0Shape12S0100000_I0_12(r3, 16));
                C235412b r52 = r5.A05;
                AnonymousClass009.A00();
                r52.A00();
                C16310on A02 = r52.A01.A01.A02();
                try {
                    A02.A03.A01("starred_stickers", "is_avatar = ?", new String[]{"1"});
                    A02.close();
                    r52.A00();
                    return;
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            case 8:
                C32781cj.A08(((C247816v) this.A00).A00.A00);
                return;
            case 9:
                ((C15820nx) this.A00).A02();
                return;
            case 10:
                EncBackupViewModel encBackupViewModel = (EncBackupViewModel) this.A00;
                try {
                    encBackupViewModel.A0G.A05(32000);
                } catch (AnonymousClass1VO e) {
                    Log.w("Failed to connect to chatd", e);
                    Log.e("EncBackupViewModel/failed to retrieve and save backup key due to a server error");
                    encBackupViewModel.A04.A0A(4);
                }
                C15820nx r22 = encBackupViewModel.A0B;
                Object A01 = encBackupViewModel.A05.A01();
                AnonymousClass009.A05(A01);
                r02 = new AnonymousClass1OE(new AnonymousClass29S(encBackupViewModel), r22, r22.A05, r22.A06, r22.A07, r22.A08, (String) A01);
                r02.A00();
                return;
            case 11:
                EncBackupViewModel encBackupViewModel2 = (EncBackupViewModel) this.A00;
                C15820nx r14 = encBackupViewModel2.A0B;
                Object A012 = encBackupViewModel2.A05.A01();
                AnonymousClass009.A05(A012);
                AnonymousClass29T r43 = new Object() { // from class: X.29T
                };
                byte[] bytes = ((String) A012).getBytes();
                C16560pF A002 = r14.A01.A00();
                AnonymousClass009.A05(A002);
                boolean isEqual = MessageDigest.isEqual(C003501n.A05(bytes, A002.A02, A002.A00).getEncoded(), A002.A01);
                EncBackupViewModel encBackupViewModel3 = EncBackupViewModel.this;
                int i3 = 5;
                if (isEqual) {
                    Log.i("EncBackupViewModel/successfully verified password");
                    encBackupViewModel3.A0A.A0A(Boolean.TRUE);
                    encBackupViewModel3.A0D.A00.edit().putInt("encrypted_backup_num_attempts_remaining", 5).apply();
                    int A04 = encBackupViewModel3.A04();
                    if (A04 == 4) {
                        encBackupViewModel3.A04.A0A(3);
                        encBackupViewModel3.A09(4);
                        r1 = encBackupViewModel3.A03;
                        i3 = 302;
                    } else if (A04 != 5) {
                        encBackupViewModel3.A06();
                        return;
                    } else {
                        encBackupViewModel3.A04.A0A(3);
                        encBackupViewModel3.A09(4);
                        r1 = encBackupViewModel3.A03;
                        i3 = 300;
                    }
                } else {
                    Log.i("EncBackupViewModel/invalid password");
                    SharedPreferences sharedPreferences = encBackupViewModel3.A0D.A00;
                    int i4 = sharedPreferences.getInt("encrypted_backup_num_attempts_remaining", 0) - 1;
                    if (i4 <= 0) {
                        Context context = encBackupViewModel3.A0B.A01.A00.A00;
                        C14350lI.A0M(new File(context.getFilesDir(), "password_hash.key"));
                        C14350lI.A0M(new File(context.getFilesDir(), "password_hash_salt.key"));
                        C14350lI.A0M(new File(context.getFilesDir(), "password_data.key"));
                    }
                    sharedPreferences.edit().putInt("encrypted_backup_num_attempts_remaining", i4).apply();
                    encBackupViewModel3.A06.A0A(Integer.valueOf(i4));
                    r1 = encBackupViewModel3.A04;
                }
                i = Integer.valueOf(i3);
                r1.A0A(i);
                return;
            case 12:
                EncBackupViewModel encBackupViewModel4 = (EncBackupViewModel) this.A00;
                AnonymousClass016 r44 = encBackupViewModel4.A02;
                try {
                    C15900o5 r15 = encBackupViewModel4.A0B.A01;
                    byte[] A03 = r15.A03();
                    if (A03 == null) {
                        A03 = C003501n.A0D(32);
                        r15.A02(A03);
                    }
                    str2 = AnonymousClass1US.A0B(A03);
                    boolean z = false;
                    if (str2.length() == 64) {
                        z = true;
                    }
                    AnonymousClass009.A0E(z);
                } catch (IOException e2) {
                    Log.e("EncBackupManager/getAndSaveRootKey/failed to get and save root key", e2);
                    str2 = null;
                }
                r44.A0A(str2);
                r1 = encBackupViewModel4.A03;
                i = 301;
                r1.A0A(i);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                r02 = (AnonymousClass1OF) this.A00;
                r02.A00();
                return;
            case 15:
                AnonymousClass1ON r23 = (AnonymousClass1ON) this.A00;
                r23.A07.A03(r23.A0D);
                Log.i("EncBackupManager/encrypted backup enabled");
                EncBackupViewModel.A00(r23.A08.A00, 0);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                Log.i("gdrive-activity/one-time-setup/taking-too-long");
                RestoreFromBackupActivity restoreFromBackupActivity = (RestoreFromBackupActivity) ((AnonymousClass29E) this.A00).A00.A08.get();
                if (restoreFromBackupActivity != null) {
                    SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(restoreFromBackupActivity.getString(R.string.gdrive_message_taking_longer_than_expected)));
                    URLSpan[] uRLSpanArr = (URLSpan[]) spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), URLSpan.class);
                    if (uRLSpanArr != null) {
                        for (URLSpan uRLSpan : uRLSpanArr) {
                            if ("skip-looking-for-backups".equals(uRLSpan.getURL())) {
                                Log.i("gdrive-activity/one-time-setup/taking-too-long/allow-user-to-skip-looking-for-backups");
                                int spanStart = spannableStringBuilder.getSpanStart(uRLSpan);
                                int spanEnd = spannableStringBuilder.getSpanEnd(uRLSpan);
                                int spanFlags = spannableStringBuilder.getSpanFlags(uRLSpan);
                                spannableStringBuilder.removeSpan(uRLSpan);
                                spannableStringBuilder.setSpan(new AnonymousClass29J(restoreFromBackupActivity), spanStart, spanEnd, spanFlags);
                            }
                        }
                    }
                    TextView textView = (TextView) AnonymousClass00T.A05(restoreFromBackupActivity, R.id.gdrive_lookup_for_backups_view);
                    textView.setText(spannableStringBuilder);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    return;
                }
                return;
            case 17:
                ((C25721Am) this.A00).A06.A08(Environment.getExternalStorageState());
                return;
            case 18:
                AnonymousClass29U r32 = (AnonymousClass29U) this.A00;
                r32.A0X.block();
                if (AnonymousClass29K.A0A(((ActivityC13810kN) r32).A09) || ((ActivityC13810kN) r32).A09.A03() == 12) {
                    r32.A0G.A05(10);
                    if (!C44771zW.A0H(((ActivityC13810kN) r32).A09)) {
                        r32.A0G.A05(10);
                        Intent A0U = C14960mK.A0U(r32, "action_backup");
                        A0U.putExtra("backup_mode", "user_initiated");
                        AnonymousClass1Tv.A00(r32, A0U);
                        return;
                    }
                }
                r0 = r32.A0F;
                r0.A03();
                return;
            case 19:
            case 34:
            case 44:
                ((ActivityC13810kN) this.A00).Ado(R.string.settings_gdrive_error_data_network_not_available_message);
                return;
            case C43951xu.A01:
                Process.setThreadPriority(10);
                ((Runnable) this.A00).run();
                return;
            case 21:
                r0 = (C22730zY) this.A00;
                r0.A03();
                return;
            case 22:
                ((C22730zY) this.A00).A06();
                return;
            case 23:
                r0 = (C22730zY) this.A00;
                r0.A06();
                r0.A03();
                return;
            case 24:
                C22730zY r16 = (C22730zY) this.A00;
                if (!r16.A09()) {
                    return;
                }
                if (r16.A0c.get() || r16.A0f.get() || r16.A0e.get() || r16.A0d.get()) {
                    r16.A06();
                    return;
                } else {
                    r16.A03();
                    return;
                }
            case 25:
                C26551Dx r24 = (C26551Dx) this.A00;
                C22730zY r17 = r24.A07;
                r17.A08(Environment.getExternalStorageState());
                r17.A04();
                r17.A06();
                r24.A08.A05();
                return;
            case 26:
                C26551Dx r25 = (C26551Dx) this.A00;
                C22730zY r18 = r25.A07;
                r18.A08(Environment.getExternalStorageState());
                r18.A04();
                r18.A06();
                r25.A08.A06();
                r25.A0J.A0V(0);
                return;
            case 27:
                GoogleDriveNewUserSetupActivity googleDriveNewUserSetupActivity = (GoogleDriveNewUserSetupActivity) this.A00;
                int i5 = googleDriveNewUserSetupActivity.A00;
                Log.i("gdrive-new-user-setup/next-setup-prompt-timestamp");
                C14820m6 r19 = ((ActivityC13810kN) googleDriveNewUserSetupActivity).A09;
                if (i5 != 0) {
                    Log.i("wa-shared-prefs/reset-gdrive-prompt-shown-count");
                    str = "gdrive_setup_user_prompted_count";
                    r19.A00.edit().remove(str).apply();
                } else {
                    Log.i("wa-shared-prefs/increment-gdrive-prompt-shown-count");
                    SharedPreferences sharedPreferences2 = r19.A00;
                    str = "gdrive_setup_user_prompted_count";
                    int i6 = sharedPreferences2.getInt(str, 0);
                    if (i6 < 0) {
                        i6 = 0;
                    }
                    int i7 = i6 + 1;
                    sharedPreferences2.edit().putInt(str, i7).apply();
                    StringBuilder sb = new StringBuilder("wa-shared-prefs/increment-gdriveprompt-shown-count/new-count/");
                    sb.append(i7);
                    Log.i(sb.toString());
                }
                int min = Math.min(4, ((ActivityC13810kN) googleDriveNewUserSetupActivity).A09.A00.getInt(str, 0)) * 30;
                Log.i(String.format(Locale.ENGLISH, "gdrive-new-user-setup/next-setup-prompt-timestamp/increment-%d-days", Integer.valueOf(min)));
                ((ActivityC13810kN) googleDriveNewUserSetupActivity).A09.A0Y(System.currentTimeMillis() + (((long) min) * 86400000));
                return;
            case 28:
                AnonymousClass10K r110 = (AnonymousClass10K) this.A00;
                r110.A0I.A03(r110);
                return;
            case 29:
                AnonymousClass10K r111 = (AnonymousClass10K) this.A00;
                r111.A0I.A04(r111);
                return;
            case C25991Bp.A0S:
                AnonymousClass10K r6 = (AnonymousClass10K) this.A00;
                AtomicReference atomicReference = r6.A0Q;
                int intValue = ((Number) atomicReference.get()).intValue();
                int A05 = r6.A0I.A05(true);
                if (intValue == 28) {
                    if (A05 == 2 || A05 == 0) {
                        atomicReference.set(10);
                        r6.ASa((long) r6.A02, 100);
                        return;
                    }
                    return;
                } else if (intValue == 16) {
                    if (A05 == 2 || A05 == 0) {
                        atomicReference.set(10);
                        r6.AN3((long) r6.A02, 100);
                        return;
                    }
                    return;
                } else {
                    return;
                }
            case 31:
                RestoreFromBackupActivity restoreFromBackupActivity2 = (RestoreFromBackupActivity) this.A00;
                if (restoreFromBackupActivity2.A0J == null) {
                    restoreFromBackupActivity2.A0J = (GoogleDriveRestoreAnimationView) restoreFromBackupActivity2.findViewById(R.id.google_drive_restore_animation_view);
                }
                AnonymousClass00T.A05(restoreFromBackupActivity2, R.id.google_drive_backup_error_info_view).setVisibility(8);
                AnonymousClass00T.A05(restoreFromBackupActivity2, R.id.restore_actions_view).setVisibility(8);
                restoreFromBackupActivity2.A0J.setVisibility(0);
                restoreFromBackupActivity2.A05.setVisibility(0);
                restoreFromBackupActivity2.A08.setVisibility(0);
                restoreFromBackupActivity2.A0J.A01();
                restoreFromBackupActivity2.A08.setText(R.string.activity_gdrive_restore_messages_preparation_message);
                return;
            case 32:
                RestoreFromBackupActivity restoreFromBackupActivity3 = (RestoreFromBackupActivity) this.A00;
                restoreFromBackupActivity3.A0R.A0M(null, null);
                restoreFromBackupActivity3.A0R.A0G();
                restoreFromBackupActivity3.A0R.A0H();
                return;
            case 33:
                ((RestoreFromBackupActivity) this.A00).A0G.A0C(0);
                return;
            case 35:
                ((RestoreFromBackupActivity) this.A00).A2y(true);
                return;
            case 36:
                RestoreFromBackupActivity restoreFromBackupActivity4 = (RestoreFromBackupActivity) this.A00;
                restoreFromBackupActivity4.A0k.block();
                if (AnonymousClass29K.A0A(((ActivityC13810kN) restoreFromBackupActivity4).A09) || ((ActivityC13810kN) restoreFromBackupActivity4).A09.A03() == 12) {
                    restoreFromBackupActivity4.A0H.A05(10);
                    restoreFromBackupActivity4.A2p();
                    return;
                }
                return;
            case 37:
                RestoreFromBackupActivity restoreFromBackupActivity5 = (RestoreFromBackupActivity) this.A00;
                if (!restoreFromBackupActivity5.A33()) {
                    AnonymousClass00T.A05(restoreFromBackupActivity5, R.id.google_drive_backup_error_info_view).setVisibility(8);
                    return;
                }
                return;
            case 38:
                RestoreFromBackupActivity restoreFromBackupActivity6 = (RestoreFromBackupActivity) this.A00;
                Dialog A022 = C44771zW.A02(restoreFromBackupActivity6, new DialogInterface.OnCancelListener() { // from class: X.29L
                    @Override // android.content.DialogInterface.OnCancelListener
                    public final void onCancel(DialogInterface dialogInterface) {
                        RestoreFromBackupActivity restoreFromBackupActivity7 = RestoreFromBackupActivity.this;
                        Log.i("gdrive-activity/gps-unavailable user declined to install Google Play Services.");
                        restoreFromBackupActivity7.A0m.open();
                    }
                }, AnonymousClass1UB.A00(restoreFromBackupActivity6.A0O.A00), 0, true);
                if (A022 == null) {
                    Log.e("gdrive-activity/gps-unavailable no way to install.");
                    return;
                } else if (!restoreFromBackupActivity6.A33()) {
                    Log.i("gdrive-activity/gps-unavailable/prompting-user-to-fix");
                    A022.show();
                    return;
                } else {
                    return;
                }
            case 39:
                RestoreFromBackupActivity restoreFromBackupActivity7 = ((AnonymousClass29M) this.A00).A00;
                ((TextView) restoreFromBackupActivity7.findViewById(R.id.activity_gdrive_backup_found_category)).setText(R.string.import_complete_title);
                restoreFromBackupActivity7.A0F.A01 = true;
                boolean z2 = false;
                try {
                    File A09 = ((ActivityC13790kL) restoreFromBackupActivity7).A07.A09();
                    if (A09 != null) {
                        if (C32811cm.A00(A09.getName()) == EnumC16570pG.A07) {
                            z2 = true;
                        }
                    }
                } catch (IOException unused2) {
                    Log.e("Cannot determine whether local backup is encrypted");
                }
                if (restoreFromBackupActivity7.A35()) {
                    return;
                }
                if (z2) {
                    restoreFromBackupActivity7.A2q(2);
                    restoreFromBackupActivity7.startActivityForResult(C14960mK.A07(restoreFromBackupActivity7, 2), 0);
                    return;
                }
                restoreFromBackupActivity7.A2w(null, 27);
                restoreFromBackupActivity7.A2j();
                restoreFromBackupActivity7.A2x(true);
                return;
            case 40:
                RestoreFromBackupActivity restoreFromBackupActivity8 = ((AnonymousClass29M) this.A00).A00;
                if (!restoreFromBackupActivity8.A33()) {
                    restoreFromBackupActivity8.A2w(null, 29);
                    restoreFromBackupActivity8.A2i();
                    return;
                }
                Log.i("gdrive-activity-observer/backup-import-start/activity-already-exited");
                return;
            case 41:
                ActivityC13790kL r45 = (ActivityC13790kL) this.A00;
                boolean A0I = r45.A07.A0I();
                boolean z3 = ((ActivityC13810kN) r45).A09.A00.getBoolean("encrypted_backup_enabled", false);
                if (A0I) {
                    if (!z3) {
                        r45.A07.A0F();
                        r2 = ((ActivityC13810kN) r45).A05;
                        i2 = 43;
                    } else {
                        return;
                    }
                } else if (z3) {
                    r2 = ((ActivityC13810kN) r45).A05;
                    i2 = 45;
                } else {
                    return;
                }
                r2.A0H(new RunnableBRunnable0Shape2S0100000_I0_2(r45, i2));
                return;
            case 42:
                SettingsGoogleDrive settingsGoogleDrive = (SettingsGoogleDrive) this.A00;
                settingsGoogleDrive.A0Z.A05(10);
                r0 = settingsGoogleDrive.A0Y;
                r0.A03();
                return;
            case 43:
                ((SettingsGoogleDrive) this.A00).A0e.A05();
                return;
            case 45:
                ((SettingsGoogleDrive) this.A00).A2f();
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                SettingsGoogleDrive settingsGoogleDrive2 = (SettingsGoogleDrive) this.A00;
                settingsGoogleDrive2.A0e.A01.block();
                if (AnonymousClass29K.A0A(((ActivityC13810kN) settingsGoogleDrive2).A09) || ((ActivityC13810kN) settingsGoogleDrive2).A09.A03() == 12) {
                    settingsGoogleDrive2.A0Z.A05(10);
                    if (!C44771zW.A0H(((ActivityC13810kN) settingsGoogleDrive2).A09)) {
                        settingsGoogleDrive2.A2k();
                        return;
                    }
                }
                r0 = settingsGoogleDrive2.A0Y;
                r0.A03();
                return;
            case 47:
                SettingsGoogleDrive settingsGoogleDrive3 = (SettingsGoogleDrive) this.A00;
                settingsGoogleDrive3.A0e.A05();
                SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = settingsGoogleDrive3.A0e;
                AnonymousClass29Y r03 = new AnonymousClass29Y(settingsGoogleDriveViewModel);
                C25721Am r33 = settingsGoogleDriveViewModel.A0R;
                C471229a r112 = r33.A03;
                r112.A03(r03);
                r33.A0H.Aaz(new C471329b(r112, r33, r33.A08), new Void[0]);
                return;
            case 48:
                RestoreFromBackupViewModel restoreFromBackupViewModel = (RestoreFromBackupViewModel) this.A00;
                restoreFromBackupViewModel.A02.A0A(Long.valueOf(restoreFromBackupViewModel.A06.A06()));
                return;
            case 49:
                RestoreFromBackupViewModel restoreFromBackupViewModel2 = (RestoreFromBackupViewModel) this.A00;
                try {
                    file = restoreFromBackupViewModel2.A06.A09();
                } catch (IOException e3) {
                    Log.e("gdrive-activity/show-restore-panel-for-local-backup", e3);
                    file = null;
                }
                AnonymousClass016 r46 = restoreFromBackupViewModel2.A01;
                File file2 = restoreFromBackupViewModel2.A03.A04().A0A;
                C14330lG.A03(file2, false);
                long A003 = C14350lI.A00(null, file2);
                if (file != null) {
                    j = file.length();
                } else {
                    j = 0;
                }
                r46.A0A(Long.valueOf(A003 + j));
                return;
            default:
                return;
        }
    }
}
