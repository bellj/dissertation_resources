package com.facebook.redex;

import X.AbstractC116035Tw;
import X.AnonymousClass398;
import X.AnonymousClass3DI;
import android.text.Spannable;
import android.widget.TextView;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape1S0600000_I1 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public Object A05;
    public final int A06;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public RunnableBRunnable0Shape1S0600000_I1(Spannable spannable, TextView textView, AbstractC116035Tw r11, AnonymousClass398 r12, AnonymousClass3DI r13, Object obj) {
        this(r11, obj, spannable, r13, r12, textView, 3);
        this.A06 = 3;
    }

    public RunnableBRunnable0Shape1S0600000_I1(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, int i) {
        this.A06 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A04 = obj5;
        this.A05 = obj6;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:22|(1:24)(1:40)|25|(5:27|(1:29)(3:31|(1:33)(3:(1:36)|37|(1:39)(6:41|(1:47)|48|(11:50|186|51|192|52|196|53|54|55|56|57)|76|(2:78|(4:80|(2:83|81)|202|210)(1:206))(1:205)))|34)|30|76|(0)(0))|194|65|(1:67)|76|(0)(0)) */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0461  */
    /* JADX WARNING: Removed duplicated region for block: B:205:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:209:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01c9  */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape1S0600000_I1.run():void");
    }
}
