package com.facebook.redex;

import X.AbstractActivityC13750kH;
import X.AbstractC15340mz;
import X.AbstractC16130oV;
import X.AbstractC28711Os;
import X.AbstractC28771Oy;
import X.AnonymousClass009;
import X.AnonymousClass1CH;
import X.AnonymousClass1Or;
import X.AnonymousClass1RN;
import X.AnonymousClass2KP;
import X.AnonymousClass2KR;
import X.AnonymousClass2NE;
import X.C16150oX;
import X.C17220qS;
import X.C22370yy;
import X.C22920zr;
import X.C28721Ot;
import X.C28731Ou;
import X.C28781Oz;
import X.C29211Rh;
import X.C30421Xi;
import X.C35191hP;
import X.C41121sw;
import X.C41911uM;
import X.C41921uN;
import X.C49222Ju;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import com.whatsapp.Conversation;
import com.whatsapp.conversation.ConversationListView;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.HashMap;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0510000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public boolean A05;
    public final int A06;

    public RunnableBRunnable0Shape0S0510000_I0(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i, boolean z) {
        this.A06 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A04 = obj5;
        this.A05 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        boolean z;
        switch (this.A06) {
            case 0:
                AnonymousClass1Or r0 = (AnonymousClass1Or) this.A00;
                boolean z2 = this.A05;
                int i = r0.A00;
                ((AbstractC28711Os) this.A01).ARy((C28721Ot) this.A02, (C28731Ou) this.A03, r0.A01, (IOException) this.A04, i, z2);
                return;
            case 1:
                Conversation conversation = (Conversation) this.A00;
                C35191hP r6 = (C35191hP) this.A01;
                AbstractC15340mz r7 = (AbstractC15340mz) this.A02;
                AbstractC15340mz r5 = (AbstractC15340mz) this.A03;
                boolean z3 = this.A05;
                C30421Xi r2 = (C30421Xi) this.A04;
                if (!((AbstractActivityC13750kH) conversation).A0I.A0B() && conversation.A4J) {
                    if (conversation.A4a) {
                        boolean z4 = true;
                        r6.A0Q = true;
                        PowerManager.WakeLock wakeLock = r6.A0b;
                        if (wakeLock == null || !wakeLock.isHeld()) {
                            z4 = false;
                        }
                        r6.A0Z = z4;
                        r6.A08();
                    }
                    r6.A0A(C35191hP.A0x, true, false);
                    conversation.A3L = r7;
                    if (conversation.A4K) {
                        conversation.A3H(r5);
                    }
                    if (z3) {
                        conversation.Afc(r2, 0, false);
                    }
                    ConversationListView conversationListView = conversation.A1g;
                    RunnableBRunnable0Shape0S0211000_I0 runnableBRunnable0Shape0S0211000_I0 = new RunnableBRunnable0Shape0S0211000_I0(conversationListView, r7, -1, 0, true);
                    conversationListView.A09 = runnableBRunnable0Shape0S0211000_I0;
                    conversationListView.post(runnableBRunnable0Shape0S0211000_I0);
                    return;
                }
                return;
            case 2:
                C22920zr r4 = (C22920zr) this.A00;
                byte[] bArr = (byte[]) this.A01;
                byte[] bArr2 = (byte[]) this.A02;
                C29211Rh[] r9 = (C29211Rh[]) this.A03;
                C29211Rh r8 = (C29211Rh) this.A04;
                boolean z5 = this.A05;
                AnonymousClass009.A01();
                if (z5) {
                    r4.A03();
                }
                synchronized (r4) {
                    r4.A02 = true;
                    r4.A04 = r9;
                }
                C17220qS r52 = r4.A0D;
                Bundle bundle = new Bundle();
                bundle.putByteArray("identity", bArr);
                bundle.putByteArray("registration", bArr2);
                bundle.putByte("type", (byte) 5);
                int length = r9.length;
                AnonymousClass2NE[] r3 = new AnonymousClass2NE[length];
                for (int i2 = 0; i2 < length; i2++) {
                    r3[i2] = new AnonymousClass2NE(r9[i2]);
                }
                bundle.putParcelableArray("preKeys", r3);
                bundle.putParcelable("signedPreKey", new AnonymousClass2NE(r8));
                bundle.putByteArray("vname", null);
                r52.A08(Message.obtain(null, 0, 85, 0, bundle), false);
                return;
            case 3:
                C41121sw r1 = (C41121sw) this.A00;
                AnonymousClass2KP r22 = (AnonymousClass2KP) this.A01;
                C41911uM r32 = (C41911uM) this.A02;
                AnonymousClass2KR r42 = (AnonymousClass2KR) this.A03;
                C41921uN r53 = (C41921uN) this.A04;
                boolean z6 = this.A05;
                C49222Ju r02 = r1.A01;
                if (r02 == null || !r02.A02) {
                    Log.i("CompanionDeviceQrHandler/request aborted, stopping");
                    return;
                } else {
                    r1.A02(r22, r32, r42, r53, z6);
                    return;
                }
            case 4:
                C22370yy r62 = (C22370yy) this.A00;
                C28781Oz r72 = (C28781Oz) this.A01;
                AbstractC16130oV r54 = (AbstractC16130oV) this.A02;
                AbstractC28771Oy r43 = (AbstractC28771Oy) this.A04;
                boolean z7 = this.A05;
                C22370yy.A00(r72, r54, true);
                AnonymousClass1CH r12 = r62.A0V;
                C16150oX r03 = r54.A02;
                HashMap hashMap = r12.A00;
                synchronized (hashMap) {
                    hashMap.remove(r03);
                }
                AnonymousClass1RN A01 = r72.A01();
                AnonymousClass009.A05(A01);
                if (r43 != null) {
                    if (A01.A00 == 13) {
                        if (r72.A04() != null) {
                            z = r72.A04().booleanValue();
                        } else {
                            z = false;
                        }
                        r43.APP(z);
                    } else {
                        r43.APQ(A01, r72);
                    }
                }
                r62.A0B(r54, A01.A00, z7);
                return;
            default:
                return;
        }
    }
}
