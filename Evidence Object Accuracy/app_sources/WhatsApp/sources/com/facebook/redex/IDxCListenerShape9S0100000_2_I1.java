package com.facebook.redex;

import X.AbstractC116115Ue;
import X.AbstractC116125Uf;
import X.AbstractC116445Vl;
import X.AbstractC13970kd;
import X.AbstractC42911w6;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.ActivityC13830kP;
import X.AnonymousClass01E;
import X.AnonymousClass1KS;
import X.AnonymousClass2GV;
import X.AnonymousClass35V;
import X.AnonymousClass3UW;
import X.AnonymousClass4U1;
import X.AnonymousClass5TF;
import X.AnonymousClass5UK;
import X.C003501n;
import X.C004802e;
import X.C12970iu;
import X.C15370n3;
import X.C15580nU;
import X.C16700pc;
import X.C20660w7;
import X.C25991Bp;
import X.C36021jC;
import X.C38211ni;
import X.C43951xu;
import X.C63453Bq;
import X.C72463ee;
import X.RunnableC32531cJ;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.SuspiciousLinkWarningDialogFragment;
import com.whatsapp.audiopicker.AudioPickerActivity;
import com.whatsapp.authentication.VerifyTwoFactorAuthCodeDialogFragment;
import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.backup.encryptedbackup.PasswordInputFragment;
import com.whatsapp.biz.product.view.activity.ProductDetailActivity;
import com.whatsapp.biz.product.view.fragment.ProductReportReasonDialogFragment;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.companiondevice.LinkedDevicesActivity;
import com.whatsapp.companiondevice.WifiSpeedBumpDialogFragment;
import com.whatsapp.conversation.CapturePictureOrVideoDialogFragment;
import com.whatsapp.conversation.ChatMediaVisibilityDialog;
import com.whatsapp.conversation.dialog.UpdateAppDialogFragment;
import com.whatsapp.conversationslist.SmsDefaultAppWarning;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.group.GroupSettingsActivity;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.payments.phoenix.flowconfigurationservice.npci.IndiaUpiFcsPinHandlerActivity;
import com.whatsapp.payments.ui.PaymentsUnavailableDialogFragment;
import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperLinkActivity;
import com.whatsapp.registration.EULA;
import com.whatsapp.registration.VerifyTwoFactorAuth;
import com.whatsapp.report.DeleteReportConfirmationDialogFragment;
import com.whatsapp.report.ShareReportConfirmationDialogFragment;
import com.whatsapp.settings.SettingsNetworkUsage;
import com.whatsapp.status.playback.fragment.OpenLinkConfirmationDialogFragment;
import com.whatsapp.stickers.StickerInfoDialogFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxCListenerShape9S0100000_2_I1 implements DialogInterface.OnClickListener {
    public Object A00;
    public final int A01;

    public IDxCListenerShape9S0100000_2_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        SuspiciousLinkWarningDialogFragment suspiciousLinkWarningDialogFragment;
        int i2;
        Activity activity;
        int i3;
        SmsDefaultAppWarning smsDefaultAppWarning;
        String str;
        int i4;
        EULA eula;
        AnonymousClass1KS r0;
        switch (this.A01) {
            case 0:
                ActivityC000900k A0B = ((AnonymousClass01E) this.A00).A0B();
                if (A0B != null) {
                    A0B.finish();
                    return;
                }
                return;
            case 1:
                ((AbstractC116445Vl) this.A00).AUr();
                return;
            case 2:
                Log.i("invitelink/revoke/confirmation/ok");
                AbstractC42911w6 r02 = (AbstractC42911w6) ((AnonymousClass01E) this.A00).A0B();
                if (r02 != null) {
                    r02.Aat();
                    return;
                }
                return;
            case 3:
                ((AnonymousClass5TF) this.A00).AUr();
                return;
            case 4:
                SuspiciousLinkWarningDialogFragment suspiciousLinkWarningDialogFragment2 = (SuspiciousLinkWarningDialogFragment) this.A00;
                AnonymousClass35V r03 = suspiciousLinkWarningDialogFragment2.A05;
                suspiciousLinkWarningDialogFragment = suspiciousLinkWarningDialogFragment2;
                if (r03 != null) {
                    r03.A05();
                    suspiciousLinkWarningDialogFragment = suspiciousLinkWarningDialogFragment2;
                }
                suspiciousLinkWarningDialogFragment.A1B();
                return;
            case 5:
            case 56:
                activity = (Activity) this.A00;
                i2 = 2;
                C36021jC.A00(activity, i2);
                return;
            case 6:
                activity = (Activity) this.A00;
                i2 = 3;
                C36021jC.A00(activity, i2);
                return;
            case 7:
                AudioPickerActivity.A02((AudioPickerActivity) this.A00);
                return;
            case 8:
            case 9:
                ((VerifyTwoFactorAuthCodeDialogFragment) this.A00).A1K();
                return;
            case 10:
                ((EncBackupViewModel) this.A00).A05();
                return;
            case 11:
                ((PasswordInputFragment) this.A00).A07.A07.A0B(0);
                return;
            case 12:
                ((AnonymousClass2GV) this.A00).AO1();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                activity = (Activity) this.A00;
                i2 = 106;
                C36021jC.A00(activity, i2);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case 41:
            case 51:
                ((Activity) this.A00).finish();
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((ProductReportReasonDialogFragment) this.A00).A00 = i;
                return;
            case 17:
            case 49:
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
            case C43951xu.A02:
            case 75:
            case 76:
            case 77:
                ((DialogFragment) this.A00).A1C();
                return;
            case 18:
                DialogFragment dialogFragment = (DialogFragment) this.A00;
                ((ProductDetailActivity) dialogFragment.A0C()).A2h(null);
                dialogFragment.A1C();
                return;
            case 19:
                ((UnblockDialogFragment) this.A00).A00.AfB();
                return;
            case C43951xu.A01:
                DialogFragment dialogFragment2 = (DialogFragment) this.A00;
                boolean A0c = dialogFragment2.A0c();
                suspiciousLinkWarningDialogFragment = dialogFragment2;
                if (!A0c) {
                    return;
                }
                suspiciousLinkWarningDialogFragment.A1B();
                return;
            case 21:
                C38211ni.A05(((AnonymousClass01E) this.A00).A0C());
                return;
            case 22:
            case C25991Bp.A0S:
            case 36:
            case 48:
            case 66:
            case 67:
            case 69:
            case 71:
                ((DialogFragment) this.A00).A1B();
                return;
            case 23:
                DialogFragment dialogFragment3 = (DialogFragment) this.A00;
                C16700pc.A0E(dialogFragment3, 0);
                dialogFragment3.A1B();
                return;
            case 24:
                ((LinkedDevicesActivity) this.A00).A2e();
                return;
            case 25:
                Activity activity2 = (Activity) this.A00;
                activity2.startActivityForResult(new Intent().setClassName(activity2.getPackageName(), "com.whatsapp.companiondevice.optin.ui.ForcedOptInActivity"), 100);
                return;
            case 26:
                C63453Bq r04 = ((WifiSpeedBumpDialogFragment) this.A00).A00.A00;
                r04.A05.A04(r04.A08.A01());
                return;
            case 27:
                AbstractC13970kd r1 = ((CapturePictureOrVideoDialogFragment) this.A00).A00;
                if (r1 == null) {
                    return;
                }
                if (i == 0) {
                    r1.AXQ();
                    return;
                } else if (i == 1) {
                    r1.AUl();
                    return;
                } else {
                    return;
                }
            case 28:
                ChatMediaVisibilityDialog chatMediaVisibilityDialog = (ChatMediaVisibilityDialog) this.A00;
                int i5 = 2;
                if (i != 1) {
                    if (i != 2) {
                        i5 = 0;
                    } else {
                        chatMediaVisibilityDialog.A01 = 1;
                        return;
                    }
                }
                chatMediaVisibilityDialog.A01 = i5;
                return;
            case 29:
                activity = ((AnonymousClass3UW) this.A00).A01;
                i2 = 1;
                C36021jC.A00(activity, i2);
                return;
            case 31:
                UpdateAppDialogFragment updateAppDialogFragment = (UpdateAppDialogFragment) this.A00;
                updateAppDialogFragment.A00.A06(updateAppDialogFragment.A0p(), new Intent("android.intent.action.VIEW").setData(updateAppDialogFragment.A01.A01()));
                return;
            case 32:
                smsDefaultAppWarning = (SmsDefaultAppWarning) this.A00;
                i3 = 1;
                C36021jC.A00(smsDefaultAppWarning, i3);
                smsDefaultAppWarning.A2e();
                smsDefaultAppWarning.finish();
                return;
            case 33:
                Activity activity3 = (Activity) this.A00;
                Log.i("smsdefaultappwarning/reset");
                activity3.getPackageManager().clearPackagePreferredActivities("com.whatsapp");
                activity3.finish();
                return;
            case 34:
                smsDefaultAppWarning = (SmsDefaultAppWarning) this.A00;
                i3 = 0;
                C36021jC.A00(smsDefaultAppWarning, i3);
                smsDefaultAppWarning.A2e();
                smsDefaultAppWarning.finish();
                return;
            case 35:
                Activity activity4 = (Activity) this.A00;
                activity4.finish();
                activity4.overridePendingTransition(0, 0);
                return;
            case 37:
                GroupChatInfo.DescriptionConflictDialogFragment.A01((GroupChatInfo.DescriptionConflictDialogFragment) this.A00);
                return;
            case 38:
                GroupSettingsActivity.AdminSettingsDialogFragment adminSettingsDialogFragment = (GroupSettingsActivity.AdminSettingsDialogFragment) this.A00;
                if (adminSettingsDialogFragment.A03.A0B()) {
                    boolean z = adminSettingsDialogFragment.A0F[0];
                    if (!(adminSettingsDialogFragment instanceof GroupSettingsActivity.SendMessagesDialogFragment)) {
                        boolean z2 = adminSettingsDialogFragment instanceof GroupSettingsActivity.RestrictFrequentlyForwardedDialogFragment;
                        C15370n3 r2 = adminSettingsDialogFragment.A09;
                        if (!z2) {
                            if (r2.A0i != z) {
                                C20660w7 r22 = adminSettingsDialogFragment.A0C;
                                C15580nU r6 = adminSettingsDialogFragment.A0B;
                                r22.A0A(new RunnableC32531cJ(adminSettingsDialogFragment.A06, adminSettingsDialogFragment.A0A, r6, null, adminSettingsDialogFragment.A0E, null, null, 159), r6, z);
                            } else {
                                str = "EditGroupInfoDialogFragment/onPositiveButtonClick: skip request, values are equal";
                                Log.i(str);
                            }
                        } else if (r2.A0g != z) {
                            C20660w7 r23 = adminSettingsDialogFragment.A0C;
                            C15580nU r62 = adminSettingsDialogFragment.A0B;
                            r23.A09(new RunnableC32531cJ(adminSettingsDialogFragment.A06, adminSettingsDialogFragment.A0A, r62, null, adminSettingsDialogFragment.A0E, null, null, 213), r62, z);
                        } else {
                            str = "RestrictFrequentlyForwardedDialogFragment/onPositiveButtonClick: skip request, values are equal";
                            Log.i(str);
                        }
                    } else if (adminSettingsDialogFragment.A09.A0W != z) {
                        C20660w7 r24 = adminSettingsDialogFragment.A0C;
                        C15580nU r63 = adminSettingsDialogFragment.A0B;
                        r24.A08(new RunnableC32531cJ(adminSettingsDialogFragment.A06, adminSettingsDialogFragment.A0A, r63, null, adminSettingsDialogFragment.A0E, null, null, 161), r63, z);
                    } else {
                        str = "SendMessagesDialogFragment/onPositiveButtonClick: skip request, values are equal";
                        Log.i(str);
                    }
                } else {
                    adminSettingsDialogFragment.A00.A07(R.string.coldsync_no_network, 0);
                }
                adminSettingsDialogFragment.A1B();
                return;
            case 39:
                ActivityC000900k A0B2 = ((AnonymousClass01E) this.A00).A0B();
                if (A0B2 instanceof AnonymousClass5UK) {
                    ((AnonymousClass5UK) A0B2).A7f();
                    return;
                }
                return;
            case 40:
                ((MediaViewBaseFragment) this.A00).A1E();
                return;
            case 42:
                Activity activity5 = (Activity) this.A00;
                Log.i("ExportMigrationActivity/cancelMigrationAndReturn/unknown/resultCode: 0");
                activity5.setResult(0);
                activity5.finish();
                return;
            case 43:
                IndiaUpiFcsPinHandlerActivity.A03((IndiaUpiFcsPinHandlerActivity) this.A00);
                return;
            case 44:
                PaymentsUnavailableDialogFragment paymentsUnavailableDialogFragment = (PaymentsUnavailableDialogFragment) this.A00;
                ActivityC000900k A0B3 = paymentsUnavailableDialogFragment.A0B();
                if (A0B3 != null) {
                    A0B3.startActivity(paymentsUnavailableDialogFragment.A00.A00(A0B3, null, null, null, "payments-blocked", null, null, null, paymentsUnavailableDialogFragment.A01.A00()));
                    return;
                }
                return;
            case 45:
                IndiaUpiMapperLinkActivity.A02((IndiaUpiMapperLinkActivity) this.A00);
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                IndiaUpiMapperLinkActivity.A03((IndiaUpiMapperLinkActivity) this.A00);
                return;
            case 47:
                IndiaUpiMapperLinkActivity.A09((IndiaUpiMapperLinkActivity) this.A00);
                return;
            case 52:
                EULA eula2 = (EULA) this.A00;
                C36021jC.A00(eula2, 8);
                eula2.A00 = 0;
                return;
            case 53:
                i4 = 7;
                eula = (Activity) this.A00;
                C36021jC.A01(eula, i4);
                return;
            case 54:
                activity = (Activity) this.A00;
                i2 = 1;
                C36021jC.A00(activity, i2);
                return;
            case 55:
                EULA eula3 = (EULA) this.A00;
                C36021jC.A00(eula3, 6);
                if (C003501n.A09()) {
                    i4 = 8;
                    eula = eula3;
                    C36021jC.A01(eula, i4);
                    return;
                }
                eula3.A00 = 0;
                return;
            case 57:
                i4 = 5;
                eula = (Activity) this.A00;
                C36021jC.A01(eula, i4);
                return;
            case 58:
                activity = (Activity) this.A00;
                i2 = 9;
                C36021jC.A00(activity, i2);
                return;
            case 59:
                Context context = (Context) this.A00;
                C004802e r25 = new C004802e(context);
                r25.A06(R.string.two_factor_auth_reset_wipe_secondary_confirmation);
                C12970iu.A1L(r25, context, 60, R.string.two_factor_auth_reset_account_label);
                C72463ee.A0S(r25);
                r25.A05();
                return;
            case 60:
                ((VerifyTwoFactorAuth) this.A00).A2f(2, null, false);
                return;
            case 61:
                AbstractC116115Ue r05 = ((DeleteReportConfirmationDialogFragment) this.A00).A01;
                if (r05 != null) {
                    r05.A8m();
                    return;
                }
                return;
            case 62:
                AbstractC116125Uf r06 = ((ShareReportConfirmationDialogFragment) this.A00).A01;
                if (r06 != null) {
                    r06.AdH();
                    return;
                }
                return;
            case 63:
                ActivityC13810kN r3 = (ActivityC13810kN) this.A00;
                ((ActivityC13830kP) r3).A05.Ab6(new RunnableBRunnable0Shape11S0100000_I0_11(r3, 38));
                r3.A2C(R.string.logging_out_device);
                return;
            case 64:
                ((Context) this.A00).startActivity(new Intent("android.settings.INTERNAL_STORAGE_SETTINGS"));
                return;
            case 65:
                SettingsNetworkUsage settingsNetworkUsage = (SettingsNetworkUsage) ((AnonymousClass01E) this.A00).A0B();
                if (settingsNetworkUsage != null) {
                    settingsNetworkUsage.A2f(true);
                    return;
                }
                return;
            case 68:
                OpenLinkConfirmationDialogFragment openLinkConfirmationDialogFragment = (OpenLinkConfirmationDialogFragment) this.A00;
                openLinkConfirmationDialogFragment.A03.A05();
                openLinkConfirmationDialogFragment.A1C();
                return;
            case 70:
                StickerInfoDialogFragment stickerInfoDialogFragment = (StickerInfoDialogFragment) this.A00;
                AnonymousClass4U1 r12 = stickerInfoDialogFragment.A0G;
                if (r12 != null && (r0 = stickerInfoDialogFragment.A0E) != null) {
                    stickerInfoDialogFragment.A1K(r0, r12);
                    return;
                }
                return;
            case 73:
                DialogFragment dialogFragment4 = (DialogFragment) this.A00;
                ActivityC000900k A0C = dialogFragment4.A0C();
                dialogFragment4.A1C();
                A0C.finish();
                return;
            case 74:
                DialogFragment dialogFragment5 = (DialogFragment) this.A00;
                if (dialogFragment5.A0c()) {
                    dialogFragment5.A1C();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
