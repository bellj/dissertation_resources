package com.facebook.redex;

import android.view.View;
import com.whatsapp.backup.google.RestoreFromBackupActivity;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0101000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public int A00;
    public Object A01;
    public final int A02;

    public ViewOnClickCListenerShape0S0101000_I0(RestoreFromBackupActivity restoreFromBackupActivity) {
        this.A02 = 2;
        this.A01 = restoreFromBackupActivity;
        this.A00 = 15;
    }

    public ViewOnClickCListenerShape0S0101000_I0(Object obj, int i, int i2) {
        this.A02 = i2;
        this.A01 = obj;
        this.A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0043, code lost:
        if (r4 == 1) goto L_0x0045;
     */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r13) {
        /*
        // Method dump skipped, instructions count: 580
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0.onClick(android.view.View):void");
    }
}
