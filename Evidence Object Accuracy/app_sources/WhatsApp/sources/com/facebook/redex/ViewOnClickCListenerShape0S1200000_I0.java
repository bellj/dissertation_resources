package com.facebook.redex;

import X.AbstractC15340mz;
import X.AnonymousClass1BD;
import X.AnonymousClass29N;
import android.net.Uri;
import android.view.View;
import android.widget.RadioButton;
import com.whatsapp.backup.google.GoogleDriveNewUserSetupActivity;
import com.whatsapp.backup.google.RestoreFromBackupActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S1200000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public String A02;
    public final int A03;

    public ViewOnClickCListenerShape0S1200000_I0(Object obj, Object obj2, String str, int i) {
        this.A03 = i;
        this.A00 = obj;
        this.A02 = str;
        this.A01 = obj2;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A03) {
            case 0:
                ((GoogleDriveNewUserSetupActivity) this.A00).A2o((RadioButton) this.A01, this.A02);
                return;
            case 1:
                String str = this.A02;
                RestoreFromBackupActivity.A0B((AnonymousClass29N) this.A01, (RestoreFromBackupActivity) this.A00, str);
                return;
            case 2:
                StatusPlaybackContactFragment statusPlaybackContactFragment = (StatusPlaybackContactFragment) this.A00;
                String str2 = this.A02;
                AnonymousClass1BD r3 = statusPlaybackContactFragment.A0U;
                r3.A0G.put(((AbstractC15340mz) this.A01).A0z.A01, Boolean.TRUE);
                statusPlaybackContactFragment.A03.Ab9(statusPlaybackContactFragment.A0p(), Uri.parse(str2));
                return;
            default:
                return;
        }
    }
}
