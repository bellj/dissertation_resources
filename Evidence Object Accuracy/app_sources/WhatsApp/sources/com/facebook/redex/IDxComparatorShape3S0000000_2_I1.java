package com.facebook.redex;

import X.AbstractC15340mz;
import X.AbstractC28901Pl;
import X.AnonymousClass009;
import X.AnonymousClass048;
import X.AnonymousClass1KS;
import X.AnonymousClass1V2;
import X.AnonymousClass3M8;
import X.AnonymousClass4PC;
import X.AnonymousClass4PE;
import X.AnonymousClass4XI;
import X.C100614mC;
import X.C14880mC;
import X.C15380n4;
import X.C30191Wl;
import X.C43951xu;
import X.C48142Em;
import X.C90314Nk;
import X.C90324Nl;
import X.C90354No;
import X.C93384a5;
import X.C93394a6;
import X.C93674aY;
import android.util.Pair;
import com.whatsapp.location.PlaceInfo;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.util.Comparator;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxComparatorShape3S0000000_2_I1 implements Comparator {
    public final int A00;

    public IDxComparatorShape3S0000000_2_I1(int i) {
        this.A00 = i;
    }

    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        switch (this.A00) {
            case 0:
                return AnonymousClass048.A00(((C93384a5) obj2).A00, ((C93384a5) obj).A00);
            case 1:
                return AnonymousClass048.A00(((C93394a6) obj).A01.A00, ((C93394a6) obj2).A01.A00);
            case 2:
                return (((AnonymousClass4PC) obj).A01 > ((AnonymousClass4PC) obj2).A01 ? 1 : (((AnonymousClass4PC) obj).A01 == ((AnonymousClass4PC) obj2).A01 ? 0 : -1));
            case 3:
                return ((C100614mC) obj2).A05 - ((C100614mC) obj).A05;
            case 5:
                Number number = (Number) obj2;
                int intValue = ((Number) obj).intValue();
                if (intValue != -1) {
                    int intValue2 = number.intValue();
                    int i = intValue - intValue2;
                    if (intValue2 == -1) {
                        return 1;
                    }
                    return i;
                } else if (number.intValue() != -1) {
                    return -1;
                }
                break;
            case 6:
                C93674aY r6 = (C93674aY) obj;
                C93674aY r7 = (C93674aY) obj2;
                int A00 = AnonymousClass048.A00(r7.A00, r6.A00);
                if (A00 != 0) {
                    return A00;
                }
                int compareTo = r6.A03.compareTo(r7.A03);
                if (compareTo == 0) {
                    return r6.A02.compareTo(r7.A02);
                }
                return compareTo;
            case 7:
                C93674aY r62 = (C93674aY) obj;
                C93674aY r72 = (C93674aY) obj2;
                int A002 = AnonymousClass048.A00(r72.A01, r62.A01);
                if (A002 != 0) {
                    return A002;
                }
                int compareTo2 = r72.A03.compareTo(r62.A03);
                if (compareTo2 == 0) {
                    return r72.A02.compareTo(r62.A02);
                }
                return compareTo2;
            case 8:
                return ((AnonymousClass4PE) obj).A01 - ((AnonymousClass4PE) obj2).A01;
            case 9:
                return Float.compare(((AnonymousClass4PE) obj).A00, ((AnonymousClass4PE) obj2).A00);
            case 10:
                Integer num = ((C30191Wl) obj).A03;
                AnonymousClass009.A05(num);
                int intValue3 = num.intValue();
                Integer num2 = ((C30191Wl) obj2).A03;
                AnonymousClass009.A05(num2);
                return intValue3 - num2.intValue();
            case 11:
                return Double.compare(((AnonymousClass3M8) obj).A01, ((AnonymousClass3M8) obj2).A01);
            case 12:
                return (((C48142Em) obj2).A00 > ((C48142Em) obj).A00 ? 1 : (((C48142Em) obj2).A00 == ((C48142Em) obj).A00 ? 0 : -1));
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return (((C14880mC) obj2).A04 > ((C14880mC) obj).A04 ? 1 : (((C14880mC) obj2).A04 == ((C14880mC) obj).A04 ? 0 : -1));
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return ((AbstractC28901Pl) obj).A0A.compareTo(((AbstractC28901Pl) obj2).A0A);
            case 15:
                return (((AnonymousClass4XI) obj2).A00 > ((AnonymousClass4XI) obj).A00 ? 1 : (((AnonymousClass4XI) obj2).A00 == ((AnonymousClass4XI) obj).A00 ? 0 : -1));
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return (((AbstractC15340mz) obj).A11 > ((AbstractC15340mz) obj2).A11 ? 1 : (((AbstractC15340mz) obj).A11 == ((AbstractC15340mz) obj2).A11 ? 0 : -1));
            case 17:
                return Double.compare(((PlaceInfo) obj).A00, ((PlaceInfo) obj2).A00);
            case 18:
                return ((C90314Nk) obj).A01.getName().compareTo(((C90314Nk) obj2).A01.getName());
            case 19:
                return ((File) ((C90324Nl) obj).A01.get(0)).getName().compareTo(((File) ((C90324Nl) obj2).A01.get(0)).getName());
            case C43951xu.A01:
                return (((C90354No) obj).A00 > ((C90354No) obj2).A00 ? 1 : (((C90354No) obj).A00 == ((C90354No) obj2).A00 ? 0 : -1));
            case 21:
                return Long.valueOf(((File) obj).lastModified()).compareTo(Long.valueOf(((File) obj2).lastModified()));
            case 22:
                return ((Integer) ((Pair) obj2).second).compareTo((Integer) ((Pair) obj).second);
            case 23:
                AnonymousClass1V2 r63 = (AnonymousClass1V2) obj;
                AnonymousClass1V2 r73 = (AnonymousClass1V2) obj2;
                if (r63.A0C()) {
                    return -1;
                }
                if (r73.A0C()) {
                    return 1;
                }
                if (r63.A02() > 0 && r73.A02() == 0) {
                    return -1;
                }
                if (r63.A02() == 0 && r73.A02() > 0) {
                    return 1;
                }
                if (C15380n4.A0M(r63.A0A)) {
                    return -1;
                }
                if (!C15380n4.A0M(r73.A0A)) {
                    return -(r63.A04() > r73.A04() ? 1 : (r63.A04() == r73.A04() ? 0 : -1));
                }
                return 1;
            case 24:
            case 25:
                AnonymousClass1KS r64 = (AnonymousClass1KS) obj;
                AnonymousClass1KS r74 = (AnonymousClass1KS) obj2;
                if (r64 != r74) {
                    if (r64 == null) {
                        return -1;
                    }
                    if (r74 == null) {
                        return 1;
                    }
                    String str = r64.A0D;
                    if (str == null) {
                        str = "";
                    }
                    String str2 = r74.A0D;
                    if (str2 == null) {
                        str2 = "";
                    }
                    return str.compareTo(str2);
                }
                break;
        }
        return 0;
    }
}
