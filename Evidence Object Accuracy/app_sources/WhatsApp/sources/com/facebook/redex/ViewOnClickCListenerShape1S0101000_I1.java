package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC49232Jx;
import X.AnonymousClass009;
import X.C12980iv;
import X.C15370n3;
import X.C15580nU;
import X.C49662Lr;
import X.C61162zV;
import X.C64433Fn;
import X.C91274Rc;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.calling.callgrid.view.MenuBottomSheet;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.search.SearchViewModel;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape1S0101000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public int A00;
    public Object A01;
    public final int A02;

    public ViewOnClickCListenerShape1S0101000_I1(Object obj, int i, int i2) {
        this.A02 = i2;
        this.A00 = i;
        this.A01 = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A02) {
            case 0:
                ((AbstractC49232Jx) this.A01).ANy(this.A00);
                return;
            case 1:
                MenuBottomSheet menuBottomSheet = (MenuBottomSheet) this.A01;
                menuBottomSheet.A00 = this.A00;
                menuBottomSheet.A1C();
                return;
            case 2:
                ((C91274Rc) this.A01).A01.ANT(this.A00);
                return;
            case 3:
                C61162zV r2 = (C61162zV) this.A01;
                int i = this.A00;
                if (i == 0) {
                    C64433Fn r4 = r2.A05;
                    AbstractC14640lm A01 = C15370n3.A01(r4.A00);
                    r4.A0B.A02(A01, C12980iv.A0k(), r4.A01);
                    r4.A0E.run();
                    return;
                } else if (i == 1) {
                    r2.A05.A02();
                    return;
                } else {
                    return;
                }
            case 4:
                C61162zV r3 = (C61162zV) this.A01;
                int i2 = this.A00;
                if (i2 == 0) {
                    r3.A05.A04(1);
                    return;
                } else if (i2 == 1) {
                    r3.A05.A03(1);
                    return;
                } else if (i2 == 2) {
                    C64433Fn r22 = r3.A05;
                    C15580nU A02 = C15580nU.A02(C15370n3.A03(r22.A00, AbstractC14640lm.class));
                    AnonymousClass009.A05(A02);
                    String str = "group_spam_banner_exit";
                    if (r22.A01) {
                        str = "triggered_block";
                    }
                    r22.A05.Adm(LeaveGroupsDialogFragment.A00(A02, str, 0, 2, true, false));
                    return;
                } else if (i2 == 3) {
                    r3.AIS();
                    return;
                } else {
                    return;
                }
            case 5:
                int i3 = this.A00;
                SearchViewModel searchViewModel = (SearchViewModel) this.A01;
                if (117 == i3) {
                    searchViewModel.A0z.A00(2, 117);
                    searchViewModel.A0U(new C49662Lr(2, R.id.search_unread_filter, R.string.filter_unread, R.drawable.smart_filter_unread));
                    return;
                }
                searchViewModel.A0Q(i3);
                return;
            default:
                return;
        }
    }
}
