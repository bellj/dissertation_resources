package com.facebook.redex;

import X.AnonymousClass5SU;
import X.AnonymousClass5XZ;
import X.C92524Wg;
import X.C95034cy;

/* loaded from: classes3.dex */
public class IDxEventShape6S0200000_2_I1 implements AnonymousClass5SU {
    public Object A00;
    public Object A01;
    public final int A02;

    public IDxEventShape6S0200000_2_I1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj2;
        this.A01 = obj;
    }

    @Override // X.AnonymousClass5SU
    public final void AJ7(Object obj) {
        if (this.A02 == 0) {
            ((AnonymousClass5XZ) obj).AXl(((C95034cy) this.A00).A08, (C92524Wg) this.A01);
        }
    }
}
