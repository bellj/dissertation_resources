package com.facebook.redex;

import X.AbstractC15710nm;
import X.AnonymousClass01V;
import X.AnonymousClass029;
import X.AnonymousClass1LI;
import X.C14920mG;
import X.C31091Zz;
import X.C42591vT;
import android.content.SharedPreferences;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.util.Log;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S2101000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public String A02;
    public String A03;
    public final int A04;

    public RunnableBRunnable0Shape0S2101000_I0(Object obj, String str, String str2, int i, int i2) {
        this.A04 = i2;
        this.A01 = obj;
        this.A02 = str;
        this.A00 = i;
        this.A03 = str2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A04) {
            case 0:
                String str = this.A02;
                int i = this.A00;
                String str2 = this.A03;
                ConversationsFragment conversationsFragment = ((C42591vT) this.A01).A04;
                conversationsFragment.A08.setProgress(i);
                conversationsFragment.A0B.setText(str2);
                conversationsFragment.A0A.setText(str);
                return;
            case 1:
                C14920mG r3 = (C14920mG) this.A01;
                String str3 = this.A02;
                String str4 = this.A03;
                int i2 = this.A00;
                synchronized (r3) {
                    Log.i("twofactorauthmanager/store-new-auth-settings");
                    if (str3 == null || str3.isEmpty()) {
                        r3.A0C = "";
                        r3.A00().edit().remove("two_factor_auth_code").remove("two_factor_auth_using_encryption").remove("two_factor_auth_email_set").remove("two_factor_auth_nag_time").remove("two_factor_auth_nag_interval").remove("two_factor_auth_last_code_correctness").apply();
                    } else {
                        r3.A0C = str3;
                        C31091Zz A00 = r3.A07.A00(AnonymousClass029.A0M, str3.getBytes(AnonymousClass01V.A0A));
                        String str5 = null;
                        if (A00 != null) {
                            String A002 = A00.A00();
                            if (A002 == null || !str3.equals(r3.A02(A002))) {
                                AbstractC15710nm r5 = r3.A01;
                                boolean z = false;
                                if (A002 == null) {
                                    z = true;
                                }
                                r5.AaV("TwoFactorAuthManager/encryptCode/EncryptedCodeFailure", String.valueOf(z), false);
                            } else {
                                str5 = A002;
                            }
                        }
                        boolean z2 = false;
                        if (str5 != null) {
                            z2 = true;
                            str3 = str5;
                        }
                        StringBuilder sb = new StringBuilder("TwoFactorAuthManager/storeNewAuthSettings/isUsingEncryption: ");
                        sb.append(z2);
                        Log.i(sb.toString());
                        SharedPreferences.Editor putBoolean = r3.A00().edit().putString("two_factor_auth_code", str3).putBoolean("two_factor_auth_using_encryption", z2).putLong("two_factor_auth_nag_time", System.currentTimeMillis()).putInt("two_factor_auth_nag_interval", i2).putBoolean("two_factor_auth_last_code_correctness", false);
                        if (str4 != null) {
                            int i3 = 2;
                            if (str4.length() > 0) {
                                i3 = 1;
                            }
                            putBoolean.putInt("two_factor_auth_email_set", i3);
                        } else if (r3.A00().getInt("two_factor_auth_email_set", 0) == 0) {
                            r3.A03.A00(new AnonymousClass1LI());
                        }
                        putBoolean.apply();
                    }
                    r3.A02.A0H(new RunnableBRunnable0Shape13S0100000_I0_13(r3, 5));
                }
                return;
            default:
                return;
        }
    }
}
