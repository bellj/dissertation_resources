package com.facebook.redex;

import X.C16120oU;
import X.C19380u1;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0111000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public boolean A02;
    public final int A03;

    public RunnableBRunnable0Shape0S0111000_I0(Object obj, int i, int i2, boolean z) {
        this.A03 = i2;
        this.A01 = obj;
        this.A00 = i;
        this.A02 = z;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A03) {
            case 0:
                C19380u1 r3 = (C19380u1) this.A01;
                int i = this.A00;
                boolean z = this.A02;
                if (r3.A03) {
                    r3.A05[i] = z;
                    if (z) {
                        r3.A04[i] = true;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                C16120oU r1 = (C16120oU) this.A01;
                boolean z2 = this.A02;
                int i2 = this.A00;
                if (r1.A0I()) {
                    r1.A06.AbZ(r1.A01, i2, z2, false);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
