package com.facebook.redex;

import com.whatsapp.chatinfo.ContactInfoActivity;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape3S0100000_I0_3 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public final int A01;

    public RunnableBRunnable0Shape3S0100000_I0_3(ContactInfoActivity contactInfoActivity, int i) {
        this.A01 = i;
        switch (i) {
            case 36:
            case 37:
            case 38:
            case 40:
                this.A00 = contactInfoActivity;
                return;
            case 39:
            default:
                this.A00 = contactInfoActivity;
                return;
        }
    }

    public RunnableBRunnable0Shape3S0100000_I0_3(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d5, code lost:
        if (86400000 >= (r5 - r0.longValue())) goto L_0x0112;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 1634
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3.run():void");
    }
}
