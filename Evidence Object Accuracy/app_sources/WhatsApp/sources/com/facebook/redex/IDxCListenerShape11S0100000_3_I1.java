package com.facebook.redex;

import android.view.View;

/* loaded from: classes4.dex */
public class IDxCListenerShape11S0100000_3_I1 implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public IDxCListenerShape11S0100000_3_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Removed duplicated region for block: B:130:0x02be  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x0517  */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x055a  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x0576  */
    /* JADX WARNING: Removed duplicated region for block: B:740:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:761:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:764:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:766:? A[RETURN, SYNTHETIC] */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r15) {
        /*
        // Method dump skipped, instructions count: 5736
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.IDxCListenerShape11S0100000_3_I1.onClick(android.view.View):void");
    }
}
