package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC15710nm;
import X.AbstractC16130oV;
import X.AbstractC28551Oa;
import X.AbstractC47972Dm;
import X.ActivityC000900k;
import X.ActivityC13770kJ;
import X.ActivityC13810kN;
import X.AnonymousClass016;
import X.AnonymousClass017;
import X.AnonymousClass01E;
import X.AnonymousClass024;
import X.AnonymousClass15L;
import X.AnonymousClass180;
import X.AnonymousClass1IS;
import X.AnonymousClass1Iv;
import X.AnonymousClass1MD;
import X.AnonymousClass1OY;
import X.AnonymousClass1PE;
import X.AnonymousClass1PZ;
import X.AnonymousClass1Q5;
import X.AnonymousClass1VC;
import X.AnonymousClass1XF;
import X.AnonymousClass1YT;
import X.AnonymousClass3GN;
import X.C14310lE;
import X.C14320lF;
import X.C14650lo;
import X.C14820m6;
import X.C15360n1;
import X.C15380n4;
import X.C15580nU;
import X.C15650ng;
import X.C16310on;
import X.C16460p3;
import X.C16510p9;
import X.C17650rA;
import X.C18750sx;
import X.C18850tA;
import X.C19490uC;
import X.C19990v2;
import X.C20020v5;
import X.C20140vH;
import X.C20650w6;
import X.C20850wQ;
import X.C236612n;
import X.C255419u;
import X.C25991Bp;
import X.C27631Ih;
import X.C28181Ma;
import X.C30041Vv;
import X.C30401Xg;
import X.C32761ch;
import X.C32941cz;
import X.C35371hi;
import X.C38311ns;
import X.C38411o3;
import X.C40581rp;
import X.C42411vA;
import X.C42441vD;
import X.C42571vR;
import X.C42581vS;
import X.C42591vT;
import X.C42611vV;
import X.C43951xu;
import X.C47892Dd;
import X.C47932Di;
import X.C47942Dj;
import X.C47952Dk;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.community.CommunitySubgroupsBottomSheet;
import com.whatsapp.conversation.conversationrow.E2EEDescriptionBottomSheet;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.conversationslist.LeaveGroupsDialogFragment;
import com.whatsapp.crop.CropImageView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape3S0200000_I0_3 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape3S0200000_I0_3() {
        this.A02 = 10;
        this.A00 = new AnonymousClass016();
    }

    public RunnableBRunnable0Shape3S0200000_I0_3(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj;
        this.A01 = obj2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C17650rA r0;
        C20850wQ r02;
        ContentValues contentValues;
        Object obj;
        Handler handler;
        int i;
        C18750sx r1;
        AnonymousClass1VC r2;
        Boolean bool;
        int i2;
        File file;
        Object obj2;
        AnonymousClass017 r03;
        AnonymousClass1XF r04;
        C16460p3 A0G;
        byte[] A07;
        Bitmap decodeByteArray;
        long j;
        long j2;
        String obj3;
        C47942Dj r22;
        switch (this.A02) {
            case 0:
                ((AnonymousClass024) this.A01).accept(((C42411vA) this.A00).A01);
                return;
            case 1:
                C42411vA r4 = (C42411vA) this.A00;
                AnonymousClass024 r3 = (AnonymousClass024) this.A01;
                C15580nU A00 = r4.A03.A0D.A00(r4.A0E);
                if (A00 == null) {
                    r3.accept(null);
                    return;
                }
                r4.A01 = r4.A04.A0A(A00);
                r4.A0J.A0I(new RunnableBRunnable0Shape3S0200000_I0_3(r4, 0, r3));
                return;
            case 2:
                Number number = (Number) this.A01;
                TextView textView = ((AnonymousClass1PZ) this.A00).A08;
                textView.setText(textView.getResources().getQuantityString(R.plurals.participants_title, number.intValue(), number));
                return;
            case 3:
                AbstractC15340mz r8 = (AbstractC15340mz) this.A01;
                C20020v5 r7 = ((C35371hi) this.A00).A00;
                synchronized (r7) {
                    SharedPreferences A04 = r7.A04();
                    long j3 = A04.getLong("start_time_ms", 0);
                    AbstractC14640lm r32 = r8.A0z.A00;
                    if (r8.A0I >= j3 && r32 != null) {
                        String rawString = r32.getRawString();
                        C40581rp A002 = C40581rp.A00(A04.getString(rawString, "0,0,0,0,0,0,0,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null"));
                        A002.A01(6);
                        C20020v5.A01(A04, A002, rawString);
                    }
                }
                r7.A07();
                return;
            case 4:
                AnonymousClass1OY r42 = (AnonymousClass1OY) this.A00;
                AbstractC15340mz r33 = (AbstractC15340mz) this.A01;
                if (r33.A0C == 7) {
                    r33.A0S();
                    if (r33 instanceof AnonymousClass1Iv) {
                        r42.A0y.A0I.A06(Collections.singleton(r33.A0z), 0);
                    } else {
                        r42.A0p.A0W(r33);
                    }
                }
                r42.A0J.A0H(new RunnableBRunnable0Shape3S0200000_I0_3(r42, 5, r33));
                return;
            case 5:
                AnonymousClass1OY r34 = (AnonymousClass1OY) this.A00;
                AbstractC15340mz r23 = (AbstractC15340mz) this.A01;
                byte b = r23.A0y;
                if (C30041Vv.A0G(b) || C30041Vv.A0I(b)) {
                    r34.A0M.A04((AbstractC16130oV) r23);
                    return;
                } else {
                    r34.A0R.A0N(r23);
                    return;
                }
            case 6:
                C42441vD r5 = (C42441vD) this.A00;
                AbstractC14640lm r43 = ((AbstractC15340mz) this.A01).A0z.A00;
                if (r43 != null) {
                    boolean z = false;
                    if (r43.equals(C27631Ih.A00)) {
                        z = true;
                    }
                    boolean A003 = C38311ns.A00(((AbstractC28551Oa) r5).A0L, r43);
                    boolean A01 = r5.A05.A01(r43);
                    boolean A004 = AnonymousClass3GN.A00(((AbstractC28551Oa) r5).A0L, r43);
                    if (!(z || A003 || A01 || A004)) {
                        int type = r43.getType();
                        int i3 = 1;
                        if (type == 3) {
                            i3 = 2;
                        } else if (type != 1) {
                            i3 = 0;
                        }
                        r5.A07.A00(i3, 0);
                        return;
                    }
                    return;
                }
                return;
            case 7:
                AnonymousClass1MD r6 = (AnonymousClass1MD) this.A00;
                Map map = (Map) this.A01;
                C255419u r24 = r6.A0V;
                r24.A00(0, map.size());
                AnonymousClass1Q5 r44 = r24.A00;
                r44.A08("update_star_message_store");
                r6.A0M.A0U(map.values(), true, true);
                r44.A07("update_star_message_store");
                r44.A08("sync");
                C18850tA r12 = r6.A0P;
                r12.A0O(r12.A0B(map.values(), true));
                r44.A07("sync");
                r44.A0C(2);
                return;
            case 8:
                AnonymousClass1MD r35 = (AnonymousClass1MD) this.A00;
                Map map2 = (Map) this.A01;
                C18850tA r72 = r35.A0P;
                Set A0B = r72.A0B(map2.values(), false);
                if (!r35.A0c.A03(map2.values(), true)) {
                    r35.A0J.A0F(r35.A0Z.A0D((long) map2.values().size(), R.plurals.unstar_while_clearing_error), 0);
                    r72.A0N(A0B);
                    return;
                }
                r72.A0O(A0B);
                return;
            case 9:
                AbstractC15340mz r36 = (AbstractC15340mz) this.A01;
                ListView A2e = ((ActivityC13770kJ) this.A00).A2e();
                AnonymousClass1IS r25 = r36.A0z;
                View findViewWithTag = A2e.findViewWithTag(r25);
                if (findViewWithTag instanceof AnonymousClass1OY) {
                    AnonymousClass1OY r13 = (AnonymousClass1OY) findViewWithTag;
                    if (r13.A1L(r25)) {
                        r13.A1D(r36, true);
                        return;
                    }
                    throw new IllegalStateException();
                }
                return;
            case 10:
                synchronized (this) {
                    r04 = (AnonymousClass1XF) this.A01;
                }
                if (r04 == null || (A0G = r04.A0G()) == null || (A07 = A0G.A07()) == null || (decodeByteArray = BitmapFactory.decodeByteArray(A07, 0, A07.length)) == null || decodeByteArray.getHeight() == 0 || decodeByteArray.getWidth() == 0) {
                    ((AnonymousClass017) this.A00).A0A(null);
                    return;
                } else {
                    ((AnonymousClass017) this.A00).A0A(decodeByteArray);
                    return;
                }
            case 11:
                obj2 = this.A01;
                r03 = ((C15360n1) this.A00).A0V;
                r03.A0B(obj2);
                return;
            case 12:
                C15360n1 r45 = (C15360n1) this.A01;
                if (r45 != null) {
                    C15650ng r26 = r45.A0O;
                    AbstractC14640lm r10 = r45.A0R;
                    C19990v2 r14 = r26.A0O;
                    AnonymousClass1PE r05 = (AnonymousClass1PE) r14.A0B().get(r10);
                    if (r05 == null) {
                        j = 1;
                    } else {
                        j = r05.A0P;
                    }
                    AnonymousClass1PE r06 = (AnonymousClass1PE) r14.A0B().get(r10);
                    if (r06 == null) {
                        j2 = 1;
                    } else {
                        j2 = r06.A0K;
                    }
                    ArrayList arrayList = new ArrayList();
                    if (j2 == 1) {
                        obj3 = "msgstore/get-important-messages empty";
                    } else {
                        C28181Ma r52 = new C28181Ma("msgstore/get-important-messages");
                        String[] strArr = {String.valueOf(r26.A0N.A02(r10)), String.valueOf(j), String.valueOf(r26.A1C.A03(j2))};
                        try {
                            C16310on A012 = r26.A0t.get();
                            Cursor A09 = A012.A03.A09(C38411o3.A02, strArr);
                            if (A09 != null) {
                                while (A09.moveToNext()) {
                                    AbstractC15340mz A02 = r26.A0K.A02(A09, r10, false, true);
                                    if (A02 != null && C30041Vv.A0K(r26.A05, A02)) {
                                        arrayList.add(A02);
                                    }
                                }
                                A09.close();
                            }
                            A012.close();
                        } catch (SQLiteDatabaseCorruptException e) {
                            Log.e(e);
                            r26.A0s.A02();
                        }
                        StringBuilder sb = new StringBuilder("msgstore/get-important-messages time spent:");
                        sb.append(r52.A01());
                        sb.append(" found:");
                        sb.append(arrayList.size());
                        obj3 = sb.toString();
                    }
                    Log.i(obj3);
                    ArrayList arrayList2 = r45.A0b;
                    arrayList2.clear();
                    arrayList2.addAll(arrayList);
                    if (arrayList2.isEmpty()) {
                        r22 = new C47942Dj(8, null);
                    } else {
                        r22 = new C47942Dj(0, r45.A0L.A0J().format((long) arrayList2.size()));
                    }
                    C47952Dk r07 = (C47952Dk) this.A00;
                    if (r07 != null) {
                        r07.A00.A0A(r22);
                        this.A00 = null;
                    }
                    this.A01 = null;
                    return;
                }
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C14310lE r27 = (C14310lE) this.A00;
                Bitmap bitmap = (Bitmap) this.A01;
                C14320lF r15 = r27.A01;
                if (r15 != null) {
                    r15.A0H = r15.A0C(bitmap);
                    r27.A0A.A0A(r27.A01);
                    return;
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                obj2 = this.A01;
                r03 = ((C14310lE) this.A00).A0A;
                r03.A0B(obj2);
                return;
            case 15:
                C14310lE r46 = (C14310lE) this.A00;
                UserJid userJid = (UserJid) this.A01;
                if (r46.A01 != null) {
                    C14650lo r28 = r46.A0H;
                    String A013 = r28.A01(userJid);
                    if (A013 != null) {
                        r46.A01.A0B = A013;
                        r46.A07(userJid);
                        return;
                    }
                    r28.A04(r46.A0I, userJid, null);
                    return;
                }
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((C14820m6) this.A00).A00.edit().putLong("last_message_row_id_since_archive_open", ((C20140vH) this.A01).A01()).apply();
                return;
            case 17:
                ConversationsFragment conversationsFragment = (ConversationsFragment) this.A00;
                C15580nU A005 = conversationsFragment.A0e.A0D.A00((C15580nU) this.A01);
                if (A005 != null) {
                    conversationsFragment.A0L.A0I(new RunnableBRunnable0Shape3S0200000_I0_3(conversationsFragment, 18, A005));
                    return;
                }
                Log.e("conversations/subgroupBottomSheet/parentGroupJid is null");
                conversationsFragment.A0H.AaV("wa-com-event/subgroupBottomSheet", "null_parent_group_jid", true);
                return;
            case 18:
                CommunitySubgroupsBottomSheet.A00((C15580nU) this.A01).A1F(((AnonymousClass01E) this.A00).A0C().A0V(), "SUBGROUP_PICKER_TAG");
                return;
            case 19:
                ConversationsFragment.A04(((C32941cz) this.A00).A00, (AbstractC14640lm) this.A01);
                return;
            case C43951xu.A01:
                C42611vV r37 = (C42611vV) this.A00;
                AbstractCollection abstractCollection = (AbstractCollection) this.A01;
                if (!abstractCollection.isEmpty()) {
                    Iterator it = abstractCollection.iterator();
                    while (it.hasNext()) {
                        AbstractC14640lm r53 = (AbstractC14640lm) it.next();
                        ConversationsFragment conversationsFragment2 = r37.A0F;
                        conversationsFragment2.A1F.A05(r53, false);
                        conversationsFragment2.A1j.A04(r53, 4, 0, 0);
                    }
                    return;
                }
                return;
            case 21:
                C42611vV r29 = (C42611vV) this.A00;
                ArrayList arrayList3 = (ArrayList) this.A01;
                ConversationsFragment conversationsFragment3 = r29.A0F;
                conversationsFragment3.A0L.A0H(new RunnableBRunnable0Shape0S0301000_I0(r29, arrayList3, conversationsFragment3.A1F.A01(arrayList3), arrayList3.size(), 2));
                return;
            case 22:
                C42611vV r47 = (C42611vV) this.A00;
                Iterator it2 = ((AbstractCollection) this.A01).iterator();
                while (it2.hasNext()) {
                    AbstractC14640lm r210 = (AbstractC14640lm) it2.next();
                    if (!C15380n4.A0O(r210)) {
                        r47.A0F.A0Q.A0I(r210, true);
                    }
                }
                return;
            case 23:
                C42611vV r48 = (C42611vV) this.A00;
                AbstractCollection abstractCollection2 = (AbstractCollection) this.A01;
                int size = abstractCollection2.size();
                Iterator it3 = abstractCollection2.iterator();
                while (it3.hasNext()) {
                    r48.A0F.A1F.A00((AbstractC14640lm) it3.next());
                }
                ConversationsFragment conversationsFragment4 = r48.A0F;
                conversationsFragment4.A0L.A0F(conversationsFragment4.A02().getQuantityString(R.plurals.unpin_toast, size), 0);
                return;
            case 24:
            case 25:
                C42591vT r16 = (C42591vT) this.A00;
                if (!((Activity) this.A01).isFinishing()) {
                    ConversationsFragment conversationsFragment5 = r16.A04;
                    conversationsFragment5.A07.removeHeaderView(conversationsFragment5.A03);
                    conversationsFragment5.A03 = null;
                    return;
                }
                return;
            case 26:
                C42581vS r54 = (C42581vS) this.A00;
                AbstractC14640lm r49 = (AbstractC14640lm) this.A01;
                r54.A01.A0H(new RunnableBRunnable0Shape0S0300000_I0(r54, r49, r54.A07.A00(r49), 36));
                return;
            case 27:
                C42571vR r38 = (C42571vR) this.A00;
                for (AbstractC14640lm r17 : (Set) this.A01) {
                    r38.A0E.A00(r17);
                }
                return;
            case 28:
                AnonymousClass01E r39 = (AnonymousClass01E) this.A01;
                ActivityC000900k A0B2 = r39.A0B();
                if (A0B2 instanceof ActivityC13810kN) {
                    ActivityC13810kN r211 = (ActivityC13810kN) A0B2;
                    int i4 = 7;
                    if (r39 instanceof StatusesFragment) {
                        i4 = 8;
                    } else if (r39 instanceof CallsHistoryFragment) {
                        i4 = 6;
                    }
                    r211.Adm(E2EEDescriptionBottomSheet.A00(i4));
                    return;
                }
                return;
            case 29:
                ((ActivityC13810kN) this.A01).Adm(E2EEDescriptionBottomSheet.A00(9));
                return;
            case C25991Bp.A0S:
                ((LeaveGroupsDialogFragment) this.A00).A07.A01((List) this.A01);
                return;
            case 31:
                ((AbstractC15710nm) this.A00).A03(new HashSet(Collections.singletonList("log_files_upload")), (Map) this.A01, false, true, true, true);
                return;
            case 32:
                AnonymousClass180 r410 = (AnonymousClass180) this.A00;
                r410.A04 = new File(((Context) this.A01).getFilesDir(), "app_state");
                if (!r410.A04.exists() || !r410.A04.isDirectory()) {
                    r410.A04.delete();
                    if (!r410.A04.mkdir()) {
                        r410.A06 = false;
                        return;
                    }
                }
                int i5 = 0;
                while (true) {
                    File file2 = r410.A04;
                    String valueOf = String.valueOf(i5);
                    file = new File(file2, valueOf);
                    if (file.exists()) {
                        i5++;
                        if (i5 >= 5) {
                            r410.A05 = String.valueOf(System.currentTimeMillis() % 5);
                            file = new File(r410.A04, r410.A05);
                        }
                    } else {
                        r410.A05 = valueOf;
                    }
                }
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.setLength(1024);
                    MappedByteBuffer map3 = randomAccessFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 1024);
                    r410.A01 = map3;
                    map3.position(0);
                    MappedByteBuffer mappedByteBuffer = r410.A01;
                    byte[] bArr = AnonymousClass180.A09;
                    mappedByteBuffer.put(bArr);
                    MappedByteBuffer mappedByteBuffer2 = r410.A01;
                    int i6 = AnonymousClass180.A07;
                    mappedByteBuffer2.position(i6 - 1);
                    r410.A01.put((byte) 10);
                    r410.A01.put(bArr);
                    r410.A01.position(0);
                    r410.A01.put(AnonymousClass180.A08.getBytes());
                    r410.A01.position(i6 + 2);
                    r410.A00 = new C47892Dd();
                    r410.A06 = true;
                    randomAccessFile.close();
                    return;
                } catch (IOException unused) {
                    r410.A06 = false;
                    return;
                }
            case 33:
                ((CropImageView) this.A00).A05((C47932Di) this.A01);
                return;
            case 34:
                C236612n r18 = (C236612n) this.A00;
                if (((AnonymousClass1YT) this.A01).A0B.A03) {
                    r0 = r18.A00;
                    r0.A00();
                    return;
                }
                return;
            case 35:
                r0 = ((C236612n) this.A00).A00;
                r0.A00();
                return;
            case 36:
                r2 = (AnonymousClass1VC) this.A01;
                for (C32761ch r08 : new HashMap(((C19490uC) this.A00).A01.A00).values()) {
                    i2 = r08.A00;
                    if (i2 >= 500) {
                        StringBuilder sb2 = new StringBuilder("getKeyForEncryptedBackupWithFuture/received/received/error ");
                        sb2.append(i2);
                        Log.w(sb2.toString());
                        bool = Boolean.FALSE;
                        r2.A01(bool);
                        return;
                    }
                }
                bool = Boolean.TRUE;
                r2.A01(bool);
                return;
            case 37:
                r2 = (AnonymousClass1VC) this.A01;
                for (C32761ch r09 : new HashMap(((C19490uC) this.A00).A01.A00).values()) {
                    i2 = r09.A00;
                    if (i2 >= 500) {
                        StringBuilder sb2 = new StringBuilder("getKeyForEncryptedBackupWithFuture/received/received/error ");
                        sb2.append(i2);
                        Log.w(sb2.toString());
                        bool = Boolean.FALSE;
                        r2.A01(bool);
                        return;
                    }
                }
                bool = Boolean.TRUE;
                r2.A01(bool);
                return;
            case 38:
                Runnable runnable = (Runnable) this.A01;
                if (((C19490uC) this.A00).A01.A00.size() == 1) {
                    runnable.run();
                    return;
                }
                return;
            case 39:
            case 41:
            case 45:
                AnonymousClass1YT r212 = (AnonymousClass1YT) this.A01;
                for (AbstractC47972Dm r010 : ((C18750sx) this.A00).A0K.A01()) {
                    r010.ANX(r212);
                }
                return;
            case 40:
                r1 = (C18750sx) this.A00;
                obj = this.A01;
                handler = r1.A0B.A02;
                i = 41;
                handler.post(new RunnableBRunnable0Shape3S0200000_I0_3(r1, i, obj));
                return;
            case 42:
                r1 = (C18750sx) this.A00;
                obj = this.A01;
                handler = r1.A0B.A02;
                i = 39;
                handler.post(new RunnableBRunnable0Shape3S0200000_I0_3(r1, i, obj));
                return;
            case 43:
                ((C18750sx) this.A00).A0A((AnonymousClass1YT) this.A01);
                return;
            case 44:
                ((C18750sx) this.A00).A0B((AnonymousClass1YT) this.A01);
                return;
            case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                C18750sx r62 = (C18750sx) this.A00;
                Collection<AnonymousClass1YT> collection = (Collection) this.A01;
                HashMap hashMap = new HashMap();
                ArrayList arrayList4 = new ArrayList();
                ReentrantReadWriteLock reentrantReadWriteLock = r62.A0L;
                reentrantReadWriteLock.writeLock().lock();
                try {
                    for (AnonymousClass1YT r213 : collection) {
                        if (!r213.A0D) {
                            if (r213.A05 instanceof C30401Xg) {
                                r62.A07.A05(C18750sx.A00(r213));
                            } else {
                                r62.A07.A05(r213);
                            }
                        }
                        r62.A00.A01(r213);
                    }
                    r62.A0B.A01.post(new RunnableBRunnable0Shape0S0300000_I0(r62, arrayList4, hashMap, 40));
                    return;
                } finally {
                    reentrantReadWriteLock.writeLock().unlock();
                }
            case 47:
                AnonymousClass15L r214 = (AnonymousClass15L) this.A00;
                try {
                    r214.A02.A08((AnonymousClass1PE) this.A01);
                    return;
                } catch (SQLiteDatabaseCorruptException e2) {
                    Log.e(e2);
                    r02 = r214.A04;
                    r02.A02();
                    return;
                } catch (Error | RuntimeException e3) {
                    Log.e(e3);
                    throw e3;
                }
            case 48:
                AnonymousClass1PE r73 = (AnonymousClass1PE) this.A01;
                C16510p9 r55 = ((C20650w6) this.A00).A08;
                StringBuilder sb3 = new StringBuilder("msgstore/reset-show-group-description ");
                AbstractC14640lm r63 = r73.A0i;
                sb3.append(r63);
                Log.i(sb3.toString());
                try {
                    try {
                        C16310on A022 = r55.A04.A02();
                        try {
                            synchronized (r73) {
                                contentValues = new ContentValues(2);
                                int i7 = 0;
                                if (r73.A0h) {
                                    i7 = 1;
                                }
                                contentValues.put("show_group_description", Integer.valueOf(i7));
                            }
                            if (r55.A00(contentValues, r63) == 0) {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("msgstore/reset-show-group-description/did not update ");
                                sb4.append(r63);
                                Log.e(sb4.toString());
                            }
                            A022.close();
                            return;
                        } catch (Throwable th) {
                            try {
                                A022.close();
                            } catch (Throwable unused2) {
                            }
                            throw th;
                        }
                    } catch (SQLiteDatabaseCorruptException e4) {
                        Log.e(e4);
                        r02 = r55.A03;
                        r02.A02();
                        return;
                    }
                } catch (Error | RuntimeException e5) {
                    Log.e(e5);
                    throw e5;
                }
            case 49:
                ((C20650w6) this.A00).A08.A0C((AnonymousClass1PE) this.A01);
                return;
            default:
                return;
        }
    }
}
