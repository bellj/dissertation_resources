package com.facebook.redex;

import X.AbstractC13860kS;
import X.AbstractC14440lR;
import X.AnonymousClass009;
import X.C14960mK;
import X.C15370n3;
import X.C238013b;
import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;
import com.whatsapp.jid.UserJid;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape1S0500000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public final int A05;

    public ViewOnClickCListenerShape1S0500000_I1(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i) {
        this.A05 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A04 = obj5;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A05) {
            case 0:
                View view2 = (View) this.A00;
                C15370n3 r4 = (C15370n3) this.A01;
                C238013b r3 = (C238013b) this.A02;
                AbstractC13860kS r2 = (AbstractC13860kS) this.A03;
                Activity activity = (Activity) this.A04;
                AnonymousClass009.A05(r4);
                UserJid A05 = C15370n3.A05(r4);
                AnonymousClass009.A05(A05);
                if (r3.A0I(A05)) {
                    r3.A0B(activity, r4, false);
                    return;
                } else if (r4.A0J()) {
                    view2.getContext().startActivity(C14960mK.A0S(view2.getContext(), A05, "chat", false, false, false));
                    return;
                } else {
                    r2.Adm(BlockConfirmationDialogFragment.A00(A05, "block_header_chat", false, false, true));
                    return;
                }
            case 1:
                ((AbstractC14440lR) this.A01).Ab2(new RunnableBRunnable0Shape11S0200000_I1_1(this.A02, 26, this.A03));
                ((Dialog) this.A04).dismiss();
                return;
            default:
                return;
        }
    }
}
