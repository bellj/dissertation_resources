package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC15340mz;
import X.AbstractC16390ow;
import X.AbstractC28901Pl;
import X.AnonymousClass1IS;
import X.AnonymousClass1Z7;
import X.AnonymousClass1Z9;
import X.C15650ng;
import X.C16470p4;
import X.C16700pc;
import X.C17070qD;
import X.C17610r6;
import X.C17910rb;
import X.C241414j;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S3100000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public String A01;
    public String A02;
    public String A03;
    public final int A04;

    public RunnableBRunnable0Shape0S3100000_I0(Object obj, String str, String str2, String str3, int i) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = str;
        this.A02 = str2;
        this.A03 = str3;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AbstractC16390ow r0;
        C16470p4 ABf;
        AnonymousClass1Z7 r02;
        switch (this.A04) {
            case 0:
                C17910rb r2 = (C17910rb) this.A00;
                String str = this.A01;
                String str2 = this.A02;
                String str3 = this.A03;
                C16700pc.A0E(r2, 0);
                C16700pc.A0E(str, 1);
                C16700pc.A0E(str2, 2);
                C16700pc.A0E(str3, 3);
                C17070qD r03 = r2.A04;
                r03.A03();
                AbstractC28901Pl A00 = C241414j.A00(str, r03.A09.A0B());
                if (A00 != null) {
                    r2.A02.A0H(new RunnableBRunnable0Shape0S2200000_I0(r2, A00, str2, str3, 1));
                    return;
                }
                return;
            case 1:
                String str4 = this.A01;
                String str5 = this.A02;
                String str6 = this.A03;
                C16700pc.A0C(str5);
                AnonymousClass1IS r1 = new AnonymousClass1IS(AbstractC14640lm.A01(str5), str4, false);
                C15650ng r6 = ((C17610r6) this.A00).A02;
                AbstractC15340mz A03 = r6.A0K.A03(r1);
                Object obj = null;
                if ((A03 instanceof AbstractC16390ow) && (r0 = (AbstractC16390ow) A03) != null && (ABf = r0.ABf()) != null && ABf.A00 == 5 && (r02 = ABf.A03) != null) {
                    List list = r02.A00;
                    if (list != null) {
                        Iterator it = list.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                Object next = it.next();
                                if (C16700pc.A0O(((AnonymousClass1Z9) next).A01.A00, str6)) {
                                    obj = next;
                                }
                            }
                        }
                        AnonymousClass1Z9 r4 = (AnonymousClass1Z9) obj;
                        if (r4 != null) {
                            r4.A00 = true;
                        }
                    }
                    r6.A0W(A03);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
