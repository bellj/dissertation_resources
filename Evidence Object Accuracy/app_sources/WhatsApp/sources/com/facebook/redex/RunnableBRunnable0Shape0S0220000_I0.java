package com.facebook.redex;

import X.AbstractC15340mz;
import X.AnonymousClass1IS;
import X.AnonymousClass1JO;
import X.C14480lV;
import X.C22100yW;
import X.C239613r;
import X.C38421o4;
import X.C42981wD;
import java.util.Collections;
import java.util.HashSet;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0220000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public Object A00;
    public Object A01;
    public boolean A02;
    public boolean A03;
    public final int A04;

    public RunnableBRunnable0Shape0S0220000_I0(AnonymousClass1JO r2, C22100yW r3, boolean z) {
        this.A04 = 1;
        this.A00 = r3;
        this.A01 = r2;
        this.A02 = z;
        this.A03 = false;
    }

    public RunnableBRunnable0Shape0S0220000_I0(Object obj, Object obj2, int i, boolean z, boolean z2) {
        this.A04 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = z;
        this.A03 = z2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        switch (this.A04) {
            case 0:
                C239613r r4 = (C239613r) this.A00;
                C38421o4 r5 = (C38421o4) this.A01;
                boolean z = this.A02;
                boolean z2 = this.A03;
                byte[] bArr = null;
                for (AbstractC15340mz r1 : Collections.unmodifiableList(r5.A01)) {
                    r4.A0G.A00(r1);
                    if (bArr == null && r1.A0G() != null) {
                        bArr = r1.A0G().A07();
                    }
                }
                r4.A0D.A04(r5, null, new C14480lV(z, r5.A04(), r5.A03()), null, bArr, z2, false, false, false);
                return;
            case 1:
                C22100yW r42 = (C22100yW) this.A00;
                AnonymousClass1JO r3 = (AnonymousClass1JO) this.A01;
                r42.A0I.A06(r3, this.A02);
                r42.A0P.removeAll(new HashSet(r3.A00));
                r42.A0B(r3);
                return;
            case 2:
                boolean z3 = this.A02;
                boolean z4 = this.A03;
                ((C42981wD) this.A00).A00.A0C((AnonymousClass1IS) this.A01, z3, z4);
                return;
            default:
                return;
        }
    }
}
