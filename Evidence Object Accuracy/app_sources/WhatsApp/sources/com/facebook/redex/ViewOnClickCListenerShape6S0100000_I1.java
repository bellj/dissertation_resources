package com.facebook.redex;

import X.C68003Ts;
import android.view.View;
import com.google.android.exoplayer2.ui.TrackSelectionView;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape6S0100000_I1 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public final int A01;

    public ViewOnClickCListenerShape6S0100000_I1(C68003Ts r1, int i) {
        this.A01 = i;
        this.A00 = r1;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ViewOnClickCListenerShape6S0100000_I1(TrackSelectionView trackSelectionView) {
        this(trackSelectionView, 0);
        this.A01 = 0;
    }

    public ViewOnClickCListenerShape6S0100000_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0555, code lost:
        if (r8.size() != 0) goto L_0x0557;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x05bb, code lost:
        if (r10.A00.A01 <= 1) goto L_0x05bd;
     */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0550  */
    @Override // android.view.View.OnClickListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r12) {
        /*
        // Method dump skipped, instructions count: 1612
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.redex.ViewOnClickCListenerShape6S0100000_I1.onClick(android.view.View):void");
    }
}
