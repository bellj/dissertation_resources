package com.facebook.redex;

import X.AbstractC115445Ro;
import X.AnonymousClass2I3;
import X.AnonymousClass2YW;
import X.AnonymousClass2Yb;
import X.AnonymousClass38V;
import X.AnonymousClass5AS;
import X.AnonymousClass5RO;
import X.C1101254i;
import X.C12980iv;
import X.C12990iw;
import X.C64153El;
import X.C69653a1;
import X.ScaleGestureDetector$OnScaleGestureListenerC52942c4;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.view.View;
import com.whatsapp.calling.callgrid.view.VoipInCallNotifBanner;
import com.whatsapp.calling.callgrid.viewmodel.InCallBannerViewModel;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout;
import com.whatsapp.collections.MarginCorrectedViewPager;
import com.whatsapp.phonematching.CountryPicker;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;
import com.whatsapp.status.StatusesFragment;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipActivityV2;
import java.util.List;

/* loaded from: classes2.dex */
public class IDxLAdapterShape0S0100000_1_I1 extends AnimatorListenerAdapter {
    public Object A00;
    public final int A01;

    public IDxLAdapterShape0S0100000_1_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    public static void A00(IDxLAdapterShape0S0100000_1_I1 iDxLAdapterShape0S0100000_1_I1) {
        ScaleGestureDetector$OnScaleGestureListenerC52942c4 r2 = (ScaleGestureDetector$OnScaleGestureListenerC52942c4) iDxLAdapterShape0S0100000_1_I1.A00;
        View view = r2.A0C;
        if (view != null) {
            r2.A0K = false;
            view.setPivotX((float) (view.getMeasuredWidth() >> 1));
            View view2 = r2.A0C;
            view2.setPivotY((float) (view2.getMeasuredHeight() >> 1));
            AbstractC115445Ro r0 = r2.A0H;
            if (r0 != null) {
                AnonymousClass2I3 r4 = ((AnonymousClass5AS) r0).A00;
                if (r4.A0S.A07(1052)) {
                    Rect A0J = C12980iv.A0J();
                    Point point = new Point();
                    r4.A08.getGlobalVisibleRect(A0J, point);
                    A0J.offset(point.x - A0J.left, point.y - A0J.top);
                    r4.A0J.set(A0J);
                    if (r4.A0H) {
                        r4.A9m(false);
                    } else {
                        r4.A9X();
                    }
                }
            }
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        MarginCorrectedViewPager marginCorrectedViewPager;
        switch (this.A01) {
            case 2:
                Log.i("VideoCallParticipantViewLayout/animatePiPView onAnimationCancel");
                AnonymousClass5RO r0 = ((VideoCallParticipantViewLayout) this.A00).A0F;
                if (r0 != null) {
                    VoipActivityV2 voipActivityV2 = ((C1101254i) r0).A00;
                    voipActivityV2.A1m = false;
                    voipActivityV2.A37();
                    return;
                }
                return;
            case 3:
            default:
                super.onAnimationCancel(animator);
                return;
            case 4:
                super.onAnimationCancel(animator);
                SolidColorWallpaperPreview solidColorWallpaperPreview = ((AnonymousClass2Yb) this.A00).A03.A04;
                solidColorWallpaperPreview.A0C = false;
                marginCorrectedViewPager = solidColorWallpaperPreview.A09;
                break;
            case 5:
                super.onAnimationCancel(animator);
                WallpaperPreview wallpaperPreview = ((AnonymousClass2YW) this.A00).A02.A04;
                wallpaperPreview.A0D = false;
                marginCorrectedViewPager = wallpaperPreview.A09;
                break;
            case 6:
                super.onAnimationCancel(animator);
                ((AnonymousClass38V) this.A00).A05.setImageDrawable(null);
                return;
            case 7:
                animator.removeListener(this);
                return;
        }
        marginCorrectedViewPager.setScrollEnabled(true);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        MarginCorrectedViewPager marginCorrectedViewPager;
        switch (this.A01) {
            case 0:
                super.onAnimationEnd(animator);
                Handler handler = ((VoipInCallNotifBanner) this.A00).A0D;
                handler.removeMessages(0);
                handler.sendEmptyMessageDelayed(0, 3000);
                return;
            case 1:
                super.onAnimationEnd(animator);
                View view = (View) this.A00;
                view.setVisibility(8);
                view.setTranslationY(0.0f);
                return;
            case 2:
                Log.i("VideoCallParticipantViewLayout/animatePiPView onAnimationEnd");
                AnonymousClass5RO r0 = ((VideoCallParticipantViewLayout) this.A00).A0F;
                if (r0 != null) {
                    VoipActivityV2 voipActivityV2 = ((C1101254i) r0).A00;
                    voipActivityV2.A1m = false;
                    voipActivityV2.A37();
                    return;
                }
                return;
            case 3:
                super.onAnimationEnd(animator);
                CountryPicker countryPicker = (CountryPicker) this.A00;
                countryPicker.A01.setIconified(true);
                countryPicker.A00.setVisibility(8);
                countryPicker.A02.setVisibility(0);
                return;
            case 4:
                super.onAnimationEnd(animator);
                SolidColorWallpaperPreview solidColorWallpaperPreview = ((AnonymousClass2Yb) this.A00).A03.A04;
                solidColorWallpaperPreview.A0C = false;
                marginCorrectedViewPager = solidColorWallpaperPreview.A09;
                break;
            case 5:
                super.onAnimationEnd(animator);
                WallpaperPreview wallpaperPreview = ((AnonymousClass2YW) this.A00).A02.A04;
                wallpaperPreview.A0D = false;
                marginCorrectedViewPager = wallpaperPreview.A09;
                break;
            case 6:
                super.onAnimationEnd(animator);
                ((AnonymousClass38V) this.A00).A05.setImageDrawable(null);
                return;
            case 7:
                StatusesFragment statusesFragment = (StatusesFragment) this.A00;
                statusesFragment.A0v.removeAll(statusesFragment.A0w);
                statusesFragment.A0g.notifyDataSetChanged();
                statusesFragment.A00 = null;
                return;
            case 8:
                C64153El r2 = ((C69653a1) this.A00).A02.A0h;
                r2.A04.A01 = false;
                r2.A00();
                return;
            case 9:
                A00(this);
                return;
            default:
                super.onAnimationEnd(animator);
                return;
        }
        marginCorrectedViewPager.setScrollEnabled(true);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
        if (2 - this.A01 != 0) {
            super.onAnimationRepeat(animator);
        } else {
            Log.i("VideoCallParticipantViewLayout/animatePiPView onAnimationRepeat");
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        switch (this.A01) {
            case 0:
                super.onAnimationStart(animator);
                InCallBannerViewModel inCallBannerViewModel = ((VoipInCallNotifBanner) this.A00).A04;
                if (inCallBannerViewModel != null) {
                    C12990iw.A1J(inCallBannerViewModel.A07, true);
                    return;
                }
                return;
            case 1:
                super.onAnimationStart(animator);
                InCallBannerViewModel inCallBannerViewModel2 = ((VoipInCallNotifBanner) this.A00).A04;
                if (inCallBannerViewModel2 != null) {
                    C12990iw.A1J(inCallBannerViewModel2.A07, false);
                    List list = inCallBannerViewModel2.A08;
                    if (list.size() <= 1) {
                        list.clear();
                        return;
                    }
                    list.remove(0);
                    inCallBannerViewModel2.A06.A0B(list.get(0));
                    return;
                }
                return;
            case 2:
                Log.i("VideoCallParticipantViewLayout/animatePiPView onAnimationStart");
                AnonymousClass5RO r0 = ((VideoCallParticipantViewLayout) this.A00).A0F;
                if (r0 != null) {
                    VoipActivityV2 voipActivityV2 = ((C1101254i) r0).A00;
                    voipActivityV2.A1m = true;
                    if (voipActivityV2.A1N != null) {
                        voipActivityV2.A2z();
                        return;
                    }
                    return;
                }
                return;
            default:
                super.onAnimationStart(animator);
                return;
        }
    }
}
