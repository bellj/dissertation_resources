package com.facebook.redex;

import X.AbstractC14640lm;
import X.AbstractC14670lq;
import X.AbstractC15340mz;
import X.AbstractC15460nI;
import X.AbstractC16130oV;
import X.AnonymousClass016;
import X.AnonymousClass01H;
import X.AnonymousClass01J;
import X.AnonymousClass03U;
import X.AnonymousClass1JG;
import X.AnonymousClass1s8;
import X.AnonymousClass27M;
import X.AnonymousClass27Z;
import X.AnonymousClass29U;
import X.AnonymousClass2CG;
import X.AnonymousClass2JX;
import X.AnonymousClass2LE;
import X.AnonymousClass2QF;
import X.AnonymousClass3XW;
import X.AnonymousClass4JN;
import X.C14820m6;
import X.C14850m9;
import X.C14900mE;
import X.C14960mK;
import X.C14970mL;
import X.C15660nh;
import X.C16120oU;
import X.C16150oX;
import X.C16310on;
import X.C18000rk;
import X.C18790t3;
import X.C19930uu;
import X.C20400vh;
import X.C20660w7;
import X.C21280xA;
import X.C21880y8;
import X.C22400z1;
import X.C22730zY;
import X.C239513q;
import X.C25991Bp;
import X.C27611If;
import X.C28181Ma;
import X.C29631Ua;
import X.C32331bz;
import X.C36021jC;
import X.C36181jS;
import X.C36191jT;
import X.C36201jU;
import X.C43951xu;
import X.C44101yE;
import X.C44241ya;
import X.C44341yl;
import X.C44351ym;
import X.C467427a;
import X.C49222Ju;
import X.C64523Fw;
import X.C863146r;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDiskIOException;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.SystemClock;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.backup.google.SettingsGoogleDrive;
import com.whatsapp.jid.UserJid;
import com.whatsapp.migration.android.integration.service.GoogleMigrateService;
import com.whatsapp.registration.report.BanReportViewModel;
import com.whatsapp.report.BusinessActivityReportViewModel;
import com.whatsapp.search.views.TokenizedSearchInput;
import com.whatsapp.text.AutoSizeTextView;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class RunnableBRunnable0Shape0S0101000_I0 extends EmptyBaseRunnable0 implements Runnable {
    public int A00;
    public Object A01;
    public final int A02;

    public RunnableBRunnable0Shape0S0101000_I0(C27611If r2, int i, int i2) {
        this.A02 = i2;
        if (14 - i2 != 0) {
            this.A01 = r2;
            this.A00 = 401;
            return;
        }
        this.A01 = r2;
        this.A00 = i;
    }

    public RunnableBRunnable0Shape0S0101000_I0(Object obj, int i, int i2) {
        this.A02 = i2;
        this.A01 = obj;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        int i;
        C14900mE r1;
        int i2;
        int i3;
        C22730zY r0;
        C14900mE r2;
        int i4;
        int i5;
        AnonymousClass27Z r02;
        AnonymousClass016 r12;
        int i6;
        int i7;
        int i8;
        C21880y8 r03;
        Object A0A;
        boolean z;
        C20660w7 r04;
        String string;
        String str;
        AbstractC16130oV r10;
        C16150oX r13;
        switch (this.A02) {
            case 0:
                ((C14970mL) this.A01).A05(this.A00);
                return;
            case 1:
                ((C14900mE) this.A01).A05(this.A00, 1);
                return;
            case 2:
                ((AnonymousClass2LE) this.A01).A01.A0e.A02(this.A00);
                return;
            case 3:
                i3 = this.A00;
                r0 = ((AnonymousClass29U) this.A01).A0F;
                r0.A0C(i3);
                return;
            case 4:
                i3 = this.A00;
                r0 = ((SettingsGoogleDrive) this.A01).A0Y;
                r0.A0C(i3);
                return;
            case 5:
                C29631Ua r6 = (C29631Ua) this.A01;
                int i9 = this.A00;
                CallInfo callInfo = Voip.getCallInfo();
                if (callInfo != null) {
                    StringBuilder sb = new StringBuilder("voip/service/signal_thread/end_call/");
                    sb.append(r6.A2e);
                    Log.i(sb.toString());
                    if (i9 == 1) {
                        Integer num = r6.A0i;
                        if (num != null) {
                            long longValue = num.longValue();
                            if (longValue <= 10000 && callInfo.isCaller && callInfo.callState == Voip.CallState.ACTIVE && callInfo.callDuration <= longValue && SystemClock.elapsedRealtime() - r6.A09 >= 8000) {
                                r6.A0l = 1;
                                Context context = r6.A1P;
                                Object[] objArr = new Object[1];
                                UserJid peerJid = Voip.getPeerJid();
                                if (peerJid == null) {
                                    Log.w("voip/getPeerDisplayNameShort/peer_jid_is_null call must have been finished");
                                    A0A = null;
                                } else {
                                    A0A = r6.A1j.A0A(r6.A1i.A0B(peerJid), -1);
                                }
                                objArr[0] = A0A;
                                String string2 = context.getString(R.string.voip_call_end_call_confirmation, objArr);
                                Intent A0T = C14960mK.A0T(context, Boolean.valueOf(!r6.A1R.A00), null, true, null, null);
                                A0T.setAction("com.whatsapp.intent.action.SHOW_END_CALL_CONFIRMATION");
                                A0T.putExtra("confirmationString", string2);
                                context.startActivity(A0T);
                                return;
                            }
                        }
                    } else if (i9 == 2) {
                        r6.A0j = r6.A0l;
                    }
                    Voip.endCall(true);
                    return;
                }
                return;
            case 6:
                int i10 = this.A00;
                StringBuilder sb2 = new StringBuilder("cameraui/error ");
                sb2.append(i10);
                Log.w(sb2.toString());
                AnonymousClass1s8 r4 = ((AnonymousClass2JX) this.A01).A00;
                if (r4.A0s.A03()) {
                    r4.A0b.A07(R.string.error_camera_disabled_during_video_call, 1);
                } else if (i10 != 2) {
                    if (r4.A0i.A02("android.permission.CAMERA") != 0) {
                        Log.w("cameraui/no-camera-permission");
                        r2 = r4.A0b;
                        i4 = R.string.cannot_start_camera_no_permission;
                    } else if (C21280xA.A00()) {
                        r2 = r4.A0b;
                        i4 = R.string.error_video_messages_disabled_during_call;
                    } else {
                        r2 = r4.A0b;
                        if (i10 == 3) {
                            i4 = R.string.photo_capture_failed;
                        } else {
                            i4 = R.string.cannot_start_camera;
                            if (i10 == 4) {
                                i4 = R.string.video_capture_failed;
                            }
                        }
                    }
                    r2.A07(i4, 1);
                }
                r4.A0x.ANa();
                return;
            case 7:
                C49222Ju r22 = (C49222Ju) this.A01;
                int i11 = this.A00;
                if (r22.A02) {
                    r22.A06.A00(i11);
                    return;
                }
                return;
            case 8:
                C16120oU r05 = (C16120oU) this.A01;
                r05.A06.AbZ(r05.A01, this.A00, false, true);
                return;
            case 9:
                int i12 = this.A00;
                C36191jT r8 = ((C36181jS) this.A01).A00;
                i = 0;
                z = false;
                if (i12 == 400 || i12 == 401 || i12 == 404) {
                    r1 = r8.A02;
                    i2 = R.string.group_error_subject;
                    r1.A07(i2, i);
                    return;
                } else if (i12 == 406) {
                    int A02 = r8.A03.A02(AbstractC15460nI.A2A);
                    r8.A02.A0E(r8.A07.A0I(new Object[]{Integer.valueOf(A02)}, R.plurals.subject_reach_limit, (long) A02), 0);
                    r04 = r8.A0C;
                    r04.A0M(z);
                    return;
                } else {
                    return;
                }
            case 10:
                ((C36201jU) this.A01).A01.A00(this.A00);
                return;
            case 11:
                AnonymousClass2QF r82 = (AnonymousClass2QF) this.A01;
                int i13 = this.A00;
                i = 0;
                i = 0;
                z = false;
                if (i13 == 403) {
                    r1 = r82.A00;
                    i2 = R.string.group_error_description_not_allowed;
                } else if (i13 == 406) {
                    int A022 = r82.A01.A02(AbstractC15460nI.A1P);
                    r82.A00.A0E(r82.A02.A0I(new Object[]{Integer.valueOf(A022)}, R.plurals.description_reach_limit, (long) A022), 0);
                    r04 = r82.A05;
                    r04.A0M(z);
                    return;
                } else if (i13 != 409) {
                    r1 = r82.A00;
                    i2 = R.string.group_error_description;
                } else {
                    r82.A05.A0C(r82.A04);
                    return;
                }
                r1.A07(i2, i);
                return;
            case 12:
                ((GoogleMigrateService) this.A01).A02.A0F(this.A00);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                C20400vh r5 = (C20400vh) this.A01;
                int i14 = this.A00;
                if (r5.A05.A03()) {
                    Context context2 = r5.A00.A00;
                    String string3 = context2.getString(R.string.notification_payment_cs_step_up_required_title);
                    if (i14 == 37) {
                        string = context2.getString(R.string.notification_payment_document_verification_success_message);
                        str = "DOC_VERIF_SUCCESS";
                    } else {
                        string = context2.getString(R.string.notification_payment_document_verification_failure_message);
                        str = "DOC_VERIF_FAILURE";
                    }
                    r5.A04(string3, string, str, null);
                    return;
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                C27611If r23 = (C27611If) this.A01;
                r23.A03(r23.A0I, this.A00);
                return;
            case 15:
                C27611If r3 = (C27611If) this.A01;
                int i15 = this.A00;
                AbstractC14640lm r14 = r3.A0I;
                r3.A02(r14);
                r3.A0J.A0K.A00(r14, i15);
                r3.A07.A07(R.string.failed_update_photo_not_authorized, 0);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                ((AnonymousClass1JG) this.A01).A05.A00(this.A00);
                return;
            case 17:
                C239513q r52 = (C239513q) this.A01;
                int i16 = this.A00;
                C22400z1 r7 = r52.A0l;
                if (r7.A08.compareAndSet(false, true)) {
                    C16310on A01 = r7.A03.A01.get();
                    try {
                        Cursor A09 = A01.A03.A09("SELECT message_row_id FROM mms_thumbnail_metadata WHERE transferred = 0 AND direct_path IS NOT NULL AND enc_thumb_hash IS NOT NULL AND media_key IS NOT NULL AND enc_thumb_hash IS NOT NULL AND media_key IS NOT NULL  ORDER BY message_row_id DESC  LIMIT ? ", new String[]{String.valueOf(100)});
                        LinkedList linkedList = new LinkedList();
                        while (A09.moveToNext()) {
                            linkedList.add(Long.valueOf(A09.getLong(0)));
                        }
                        A09.close();
                        A01.close();
                        linkedList.size();
                        r7.A00(linkedList, new Random());
                    } finally {
                    }
                }
                if (i16 != 0) {
                    C15660nh r24 = r52.A0S;
                    C28181Ma r32 = new C28181Ma(false);
                    r32.A04("msgstore/getRetryAutodownloadMessages");
                    ArrayList arrayList = new ArrayList();
                    long A04 = r24.A0E.A04(r52.A0H.A00() - 86400000);
                    try {
                        C16310on A012 = r24.A0C.get();
                        try {
                            Cursor A092 = A012.A03.A09(C32331bz.A0C, new String[]{Long.toString(A04)});
                            if (A092 != null) {
                                while (A092.moveToNext()) {
                                    AbstractC15340mz A013 = r24.A05.A01(A092);
                                    if ((A013 instanceof AbstractC16130oV) && (r13 = (r10 = (AbstractC16130oV) A013).A02) != null && !r13.A0P && !r13.A0a && r13.A0L) {
                                        arrayList.add(r10);
                                    }
                                    if (arrayList.size() >= 32) {
                                        A092.close();
                                    }
                                }
                                A092.close();
                            }
                            A012.close();
                        } finally {
                        }
                    } catch (SQLiteDatabaseCorruptException e) {
                        Log.e(e);
                        r24.A0B.A02();
                    } catch (SQLiteDiskIOException e2) {
                        r24.A0A.A00(1);
                        throw e2;
                    } catch (IllegalStateException e3) {
                        Log.i("msgstore/getRetryAutodownloadMessages/IllegalStateException ", e3);
                    }
                    StringBuilder sb3 = new StringBuilder("msgstore/getRetryAutodownloadMessages ");
                    sb3.append(arrayList.size());
                    sb3.append(" | time spent:");
                    sb3.append(r32.A01());
                    Log.i(sb3.toString());
                    int A05 = r52.A0G.A05(true);
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        r52.A0k.A01((AbstractC16130oV) it.next(), A05, true);
                    }
                    return;
                }
                return;
            case 18:
                i5 = this.A00;
                r02 = ((AnonymousClass27M) this.A01).A08;
                r02.ANb(i5);
                return;
            case 19:
                i5 = this.A00;
                r02 = ((C467427a) this.A01).A00.A05;
                r02.ANb(i5);
                return;
            case C43951xu.A01:
                BanReportViewModel banReportViewModel = (BanReportViewModel) this.A01;
                int i17 = this.A00;
                if (i17 > 5) {
                    r12 = banReportViewModel.A02;
                    i6 = 1;
                    r12.A0B(Integer.valueOf(i6));
                    return;
                }
                banReportViewModel.A08.Ab2(new RunnableBRunnable0Shape0S0101000_I0(banReportViewModel, i17, 21));
                return;
            case 21:
                BanReportViewModel banReportViewModel2 = (BanReportViewModel) this.A01;
                int i18 = this.A00;
                AnonymousClass4JN r06 = banReportViewModel2.A03;
                String str2 = banReportViewModel2.A00;
                C44341yl r42 = r06.A00;
                AnonymousClass01J r33 = r42.A03;
                AnonymousClass01H A00 = C18000rk.A00(r33.AMu);
                C14850m9 r72 = (C14850m9) r33.A04.get();
                C44351ym r07 = r42.A02;
                new C863146r((C18790t3) r33.AJw.get(), (C14820m6) r33.AN3.get(), r72, (C19930uu) r33.AM5.get(), A00, str2, r07.A0r, r07.A09).AZO(new AnonymousClass3XW(banReportViewModel2, i18));
                return;
            case 22:
                BusinessActivityReportViewModel businessActivityReportViewModel = ((C44101yE) this.A01).A00.A00;
                if (businessActivityReportViewModel.A05.A00() == 1) {
                    BusinessActivityReportViewModel.A00(businessActivityReportViewModel);
                    return;
                }
                return;
            case 23:
                BusinessActivityReportViewModel businessActivityReportViewModel2 = ((C44241ya) this.A01).A00.A00;
                businessActivityReportViewModel2.A01.A0B(2);
                r12 = businessActivityReportViewModel2.A02;
                i6 = 0;
                r12.A0B(Integer.valueOf(i6));
                return;
            case 24:
                TokenizedSearchInput tokenizedSearchInput = (TokenizedSearchInput) this.A01;
                tokenizedSearchInput.A0S.setVisibility(this.A00);
                tokenizedSearchInput.A0B();
                return;
            case 25:
                AutoSizeTextView.A02((AutoSizeTextView) this.A01, this.A00);
                return;
            case 26:
                r03 = (C21880y8) this.A01;
                i7 = this.A00;
                i8 = 4;
                r03.A03(i7, i8);
                return;
            case 27:
                r03 = (C21880y8) this.A01;
                i7 = this.A00;
                i8 = 3;
                r03.A03(i7, i8);
                return;
            case 28:
                C36021jC.A01(((AbstractC14670lq) this.A01).A0j, this.A00);
                return;
            case 29:
                VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = (VoipCallControlBottomSheetV2) this.A01;
                AnonymousClass03U A0C = voipCallControlBottomSheetV2.A0C.A0C(this.A00);
                if (A0C != null) {
                    int dimensionPixelOffset = voipCallControlBottomSheetV2.A0p().getResources().getDimensionPixelOffset(R.dimen.call_control_participant_list_scrolling_bottom_offset);
                    View view = A0C.A0H;
                    voipCallControlBottomSheetV2.A0B.requestChildRectangleOnScreen(view, new Rect(0, 0, view.getWidth(), view.getHeight() + dimensionPixelOffset), false);
                    view.setBackground(new ColorDrawable(voipCallControlBottomSheetV2.A05));
                    ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(view.getBackground(), PropertyValuesHolder.ofInt("alpha", 255, 0));
                    ofPropertyValuesHolder.setTarget(view.getBackground());
                    ofPropertyValuesHolder.setDuration(3000L);
                    ofPropertyValuesHolder.start();
                    C64523Fw r15 = voipCallControlBottomSheetV2.A0F;
                    if (r15 != null && !r15.A08()) {
                        r15.A02(3);
                        r15.A07 = true;
                        return;
                    }
                    return;
                }
                return;
            case C25991Bp.A0S:
                ((AnonymousClass2CG) this.A01).A00.A05.APl(this.A00);
                return;
            default:
                return;
        }
    }
}
