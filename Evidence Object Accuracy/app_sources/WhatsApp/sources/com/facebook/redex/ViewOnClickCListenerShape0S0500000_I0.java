package com.facebook.redex;

import X.ActivityC13810kN;
import X.AnonymousClass018;
import X.AnonymousClass12P;
import X.AnonymousClass18P;
import X.AnonymousClass1IR;
import X.AnonymousClass1In;
import X.AnonymousClass1R1;
import X.AnonymousClass1V8;
import X.AnonymousClass1W9;
import X.AnonymousClass3DE;
import X.C14900mE;
import X.C18610sj;
import X.C26061Bw;
import X.C619733i;
import X.C95734eG;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.MessageReplyActivity;
import java.util.ArrayList;

/* loaded from: classes2.dex */
public class ViewOnClickCListenerShape0S0500000_I0 extends EmptyBaseViewOnClick0CListener implements View.OnClickListener {
    public Object A00;
    public Object A01;
    public Object A02;
    public Object A03;
    public Object A04;
    public final int A05;

    public ViewOnClickCListenerShape0S0500000_I0(Object obj, Object obj2, Object obj3, Object obj4, Object obj5, int i) {
        this.A05 = i;
        this.A00 = obj;
        this.A01 = obj2;
        this.A02 = obj3;
        this.A03 = obj4;
        this.A04 = obj5;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        switch (this.A05) {
            case 0:
                AnonymousClass18P r11 = (AnonymousClass18P) this.A00;
                TextView textView = (TextView) this.A01;
                AnonymousClass1In r10 = (AnonymousClass1In) this.A02;
                AnonymousClass1IR r8 = (AnonymousClass1IR) this.A03;
                UserJid userJid = (UserJid) this.A04;
                if (r11.A00.A0M()) {
                    r11.A05(textView, r8, true, true);
                    C18610sj r12 = r11.A0A;
                    String str = r8.A0K;
                    r12.A0F(new C619733i(r12.A05.A00, r12.A01, r12.A0B, new AnonymousClass3DE(textView, r8, userJid, r10, r11), r12), new AnonymousClass1V8("account", new AnonymousClass1W9[]{new AnonymousClass1W9("action", "cancel-payment-request"), new AnonymousClass1W9("request-id", str)}), "set", C26061Bw.A0L);
                    return;
                }
                return;
            case 1:
                AnonymousClass1R1 r0 = (AnonymousClass1R1) this.A00;
                AnonymousClass018 r1 = (AnonymousClass018) this.A01;
                AnonymousClass12P r5 = (AnonymousClass12P) this.A02;
                Context context = (Context) this.A03;
                C14900mE r3 = (C14900mE) this.A04;
                if (r0 != null) {
                    String str2 = r0.A01;
                    if (!TextUtils.isEmpty(str2)) {
                        r5.A06(context, new Intent("android.intent.action.VIEW", Uri.parse(str2.replace("%@", r1.A06()))));
                        return;
                    }
                }
                r3.A07(R.string.something_went_wrong_network_required, 0);
                return;
            case 2:
                MessageReplyActivity messageReplyActivity = (MessageReplyActivity) this.A00;
                View view2 = (View) this.A01;
                ViewGroup viewGroup = (ViewGroup) this.A02;
                Object obj = this.A03;
                int[] iArr = (int[]) this.A04;
                view2.performHapticFeedback(1, 2);
                if (((ActivityC13810kN) messageReplyActivity).A09.A00.getInt("status_reactions_nux_shown_count", 0) <= 2) {
                    SharedPreferences sharedPreferences = ((ActivityC13810kN) messageReplyActivity).A09.A00;
                    sharedPreferences.edit().putInt("status_reactions_nux_shown_count", sharedPreferences.getInt("status_reactions_nux_shown_count", 0) + 1).apply();
                }
                AnimatorSet animatorSet = new AnimatorSet();
                AnimatorSet animatorSet2 = new AnimatorSet();
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view2, View.SCALE_X, 1.5f);
                ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view2, View.SCALE_Y, 1.5f);
                ofFloat.setDuration(500L);
                ofFloat2.setDuration(500L);
                Interpolator interpolator = MessageReplyActivity.A1E;
                ofFloat.setInterpolator(interpolator);
                ofFloat2.setInterpolator(interpolator);
                animatorSet2.playTogether(ofFloat, ofFloat2);
                AnimatorSet animatorSet3 = new AnimatorSet();
                ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(view2, View.SCALE_X, 0.0f);
                ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(view2, View.SCALE_Y, 0.0f);
                ObjectAnimator ofFloat5 = ObjectAnimator.ofFloat(view2, View.ALPHA, 0.0f);
                ofFloat3.setDuration(200L);
                ofFloat4.setDuration(200L);
                ofFloat5.setDuration(200L);
                Interpolator interpolator2 = MessageReplyActivity.A1D;
                ofFloat3.setInterpolator(interpolator2);
                ofFloat4.setInterpolator(interpolator2);
                ofFloat5.setInterpolator(interpolator2);
                animatorSet3.playTogether(ofFloat3, ofFloat4, ofFloat5);
                AnimatorSet animatorSet4 = new AnimatorSet();
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    if (viewGroup.getChildAt(i).getId() != view2.getId()) {
                        ObjectAnimator ofFloat6 = ObjectAnimator.ofFloat(viewGroup.getChildAt(i), View.ALPHA, 0.0f);
                        ofFloat6.setDuration(250L);
                        ofFloat6.setInterpolator(interpolator);
                        arrayList.add(ofFloat6);
                    }
                }
                ObjectAnimator ofFloat7 = ObjectAnimator.ofFloat(obj, View.ALPHA, 0.0f);
                ofFloat7.setDuration(250L);
                ofFloat7.setInterpolator(interpolator);
                arrayList.add(ofFloat7);
                animatorSet4.playTogether(arrayList);
                animatorSet.playTogether(animatorSet2, animatorSet4);
                animatorSet.playSequentially(animatorSet2, animatorSet3);
                animatorSet.addListener(new C95734eG(animatorSet, messageReplyActivity, iArr));
                animatorSet.start();
                return;
            default:
                return;
        }
    }
}
