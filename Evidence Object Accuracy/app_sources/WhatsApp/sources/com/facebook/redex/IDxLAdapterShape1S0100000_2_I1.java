package com.facebook.redex;

import X.AbstractC55202hx;
import X.AnonymousClass2YT;
import X.AnonymousClass33Z;
import X.C60142w0;
import X.View$OnLayoutChangeListenerC66123Mj;
import X.ViewTreeObserver$OnPreDrawListenerC66413Nm;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.google.android.material.transformation.ExpandableTransformationBehavior;
import com.whatsapp.RollingCounterView;
import com.whatsapp.calling.callgrid.view.FocusViewContainer;
import com.whatsapp.calling.callgrid.view.PipViewContainer;
import com.whatsapp.components.AnimatingArrowsLayout;
import com.whatsapp.components.CircularRevealView;
import com.whatsapp.components.RoundCornerProgressBar;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaperPreview;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.GlVideoRenderer;
import com.whatsapp.voipcalling.VoipActivityV2;
import org.chromium.net.UrlRequest;

/* loaded from: classes3.dex */
public class IDxLAdapterShape1S0100000_2_I1 extends AnimatorListenerAdapter {
    public Object A00;
    public final int A01;

    public IDxLAdapterShape1S0100000_2_I1(Object obj, int i) {
        this.A01 = i;
        this.A00 = obj;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        switch (this.A01) {
            case 3:
                super.onAnimationCancel(animator);
                View view = (View) this.A00;
                view.setAlpha(1.0f);
                view.setVisibility(8);
                return;
            case 4:
                super.onAnimationCancel(animator);
                FocusViewContainer.A00(((View$OnLayoutChangeListenerC66123Mj) this.A00).A01);
                return;
            case 5:
                Log.i("PipViewContainer/animatePiPView onAnimationCancel");
                PipViewContainer pipViewContainer = (PipViewContainer) this.A00;
                pipViewContainer.A03();
                PipViewContainer.A00(pipViewContainer, false);
                return;
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case 17:
            default:
                super.onAnimationCancel(animator);
                return;
            case 9:
                ((CircularRevealView) this.A00).A0C = true;
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                super.onAnimationCancel(animator);
                ((SolidColorWallpaperPreview) this.A00).finish();
                return;
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                super.onAnimationCancel(animator);
                ((AnonymousClass2YT) this.A00).A01.finish();
                return;
            case 18:
                super.onAnimationCancel(animator);
                ((VoipActivityV2) this.A00).A38();
                return;
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        View view;
        switch (this.A01) {
            case 0:
                ((HideBottomViewOnScrollBehavior) this.A00).A02 = null;
                return;
            case 1:
                ((ExpandableTransformationBehavior) this.A00).A00 = null;
                return;
            case 2:
                ((RollingCounterView) this.A00).A01();
                return;
            case 3:
                super.onAnimationEnd(animator);
                view = (View) this.A00;
                view.setAlpha(1.0f);
                break;
            case 4:
                super.onAnimationEnd(animator);
                FocusViewContainer.A00(((View$OnLayoutChangeListenerC66123Mj) this.A00).A01);
                return;
            case 5:
                Log.i("PipViewContainer/animatePiPView onAnimationEnd");
                PipViewContainer pipViewContainer = (PipViewContainer) this.A00;
                pipViewContainer.A03();
                PipViewContainer.A00(pipViewContainer, false);
                return;
            case 6:
            case 11:
            default:
                super.onAnimationEnd(animator);
                return;
            case 7:
                View view2 = (View) this.A00;
                view2.setVisibility(8);
                view2.animate().setListener(null);
                return;
            case 8:
                AnimatingArrowsLayout animatingArrowsLayout = (AnimatingArrowsLayout) this.A00;
                animatingArrowsLayout.post(new RunnableBRunnable0Shape15S0100000_I1_1(animatingArrowsLayout.A03, 20));
                return;
            case 9:
                ((CircularRevealView) this.A00).A0C = false;
                return;
            case 10:
                super.onAnimationEnd(animator);
                view = (View) this.A00;
                break;
            case 12:
                ((AnonymousClass33Z) this.A00).A06.A1E();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                Runnable runnable = (Runnable) this.A00;
                if (runnable != null) {
                    runnable.run();
                    return;
                }
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                super.onAnimationEnd(animator);
                ((SolidColorWallpaperPreview) this.A00).finish();
                return;
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                super.onAnimationEnd(animator);
                ((AnonymousClass2YT) this.A00).A01.finish();
                return;
            case 17:
                ((ViewTreeObserver$OnPreDrawListenerC66413Nm) this.A00).A00.A00 = null;
                return;
            case 18:
                super.onAnimationEnd(animator);
                ((VoipActivityV2) this.A00).A38();
                return;
        }
        view.setVisibility(8);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
        switch (this.A01) {
            case 5:
                Log.i("PipViewContainer/animatePiPView onAnimationRepeat");
                return;
            case 6:
                super.onAnimationRepeat(animator);
                C60142w0 r1 = (C60142w0) this.A00;
                if (!((AbstractC55202hx) r1).A05.A0D) {
                    r1.A0H();
                    return;
                }
                return;
            default:
                super.onAnimationRepeat(animator);
                return;
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        switch (this.A01) {
            case 2:
                ((View) this.A00).invalidate();
                return;
            case 5:
                Log.i("PipViewContainer/animatePiPView onAnimationStart");
                PipViewContainer.A00((PipViewContainer) this.A00, true);
                return;
            case 9:
                ((CircularRevealView) this.A00).A0C = true;
                return;
            case 11:
                ((RoundCornerProgressBar) this.A00).A06 = false;
                return;
            default:
                super.onAnimationStart(animator);
                return;
        }
    }
}
