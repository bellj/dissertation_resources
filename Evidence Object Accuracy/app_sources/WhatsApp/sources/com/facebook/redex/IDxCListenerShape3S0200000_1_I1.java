package com.facebook.redex;

import X.AbstractC14440lR;
import X.AbstractC15710nm;
import X.ActivityC000900k;
import X.ActivityC13810kN;
import X.AnonymousClass009;
import X.AnonymousClass01E;
import X.AnonymousClass2KJ;
import X.AnonymousClass2y0;
import X.AnonymousClass30Z;
import X.AnonymousClass37P;
import X.AnonymousClass37T;
import X.AnonymousClass4VJ;
import X.C12960it;
import X.C12970iu;
import X.C14580lf;
import X.C14900mE;
import X.C14960mK;
import X.C15370n3;
import X.C15550nR;
import X.C15580nU;
import X.C17220qS;
import X.C35741ib;
import X.C36021jC;
import X.C51122Sx;
import X.C626638c;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.os.SystemClock;
import com.whatsapp.PhoneHyperLinkDialogFragment;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.activity.CatalogListActivity;
import com.whatsapp.community.CommunityDeleteDialogFragment;
import com.whatsapp.contact.picker.ContactPickerFragment;
import com.whatsapp.conversation.conversationrow.EncryptionChangeDialogFragment;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.groupsuspend.CreateGroupSuspendDialog;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import com.whatsapp.voipcalling.VoipActivityV2;
import java.util.ArrayList;
import java.util.List;
import org.chromium.net.UrlRequest;

/* loaded from: classes2.dex */
public class IDxCListenerShape3S0200000_1_I1 implements DialogInterface.OnClickListener {
    public Object A00;
    public Object A01;
    public final int A02;

    public IDxCListenerShape3S0200000_1_I1(Object obj, int i, Object obj2) {
        this.A02 = i;
        this.A00 = obj2;
        this.A01 = obj;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (this.A02) {
            case 0:
                List list = (List) this.A01;
                if (((AnonymousClass4VJ) list.get(i)).A00 == 1) {
                    PhoneHyperLinkDialogFragment phoneHyperLinkDialogFragment = (PhoneHyperLinkDialogFragment) this.A00;
                    phoneHyperLinkDialogFragment.A04.A00(Boolean.valueOf(phoneHyperLinkDialogFragment.A0A), Boolean.valueOf(phoneHyperLinkDialogFragment.A0C), 6);
                    Intent A0g = new C14960mK().A0g(phoneHyperLinkDialogFragment.A01(), C15550nR.A00(phoneHyperLinkDialogFragment.A02, phoneHyperLinkDialogFragment.A06));
                    A0g.putExtra("args_conversation_screen_entry_point", 7);
                    A0g.putExtra("isWAAccount", phoneHyperLinkDialogFragment.A0C);
                    A0g.putExtra("isPhoneNumberOwner", phoneHyperLinkDialogFragment.A0A);
                    phoneHyperLinkDialogFragment.A00.A08(phoneHyperLinkDialogFragment.A01(), A0g, "PhoneHyperLinkDialogFragment");
                    return;
                } else if (((AnonymousClass4VJ) list.get(i)).A00 == 2) {
                    PhoneHyperLinkDialogFragment phoneHyperLinkDialogFragment2 = (PhoneHyperLinkDialogFragment) this.A00;
                    phoneHyperLinkDialogFragment2.A04.A00(Boolean.valueOf(phoneHyperLinkDialogFragment2.A0A), Boolean.valueOf(phoneHyperLinkDialogFragment2.A0C), 4);
                    phoneHyperLinkDialogFragment2.A01.Ab9(phoneHyperLinkDialogFragment2.A01(), Uri.parse(phoneHyperLinkDialogFragment2.A09));
                    return;
                } else if (((AnonymousClass4VJ) list.get(i)).A00 == 3) {
                    PhoneHyperLinkDialogFragment phoneHyperLinkDialogFragment3 = (PhoneHyperLinkDialogFragment) this.A00;
                    phoneHyperLinkDialogFragment3.A04.A00(Boolean.valueOf(phoneHyperLinkDialogFragment3.A0A), Boolean.valueOf(phoneHyperLinkDialogFragment3.A0C), 5);
                    Intent A01 = phoneHyperLinkDialogFragment3.A07.A01(phoneHyperLinkDialogFragment3.A08, null, true, false);
                    A01.putExtra("finishActivityOnSaveCompleted", true);
                    phoneHyperLinkDialogFragment3.startActivityForResult(A01, 1000);
                    return;
                } else {
                    return;
                }
            case 1:
                CatalogListActivity catalogListActivity = (CatalogListActivity) this.A00;
                catalogListActivity.A07.A0C(catalogListActivity, C15370n3.A04((C15370n3) this.A01));
                C36021jC.A00(catalogListActivity, 106);
                return;
            case 2:
                AnonymousClass01E r2 = (AnonymousClass01E) this.A00;
                Context A0p = r2.A0p();
                Jid jid = ((C15370n3) this.A01).A0D;
                AnonymousClass009.A05(jid);
                r2.A0v(C14960mK.A0V(A0p, jid.getRawString()));
                return;
            case 3:
                CommunityDeleteDialogFragment communityDeleteDialogFragment = (CommunityDeleteDialogFragment) this.A00;
                ActivityC13810kN r3 = (ActivityC13810kN) communityDeleteDialogFragment.A0C();
                r3.A2C(R.string.register_wait_message);
                long elapsedRealtime = SystemClock.elapsedRealtime();
                communityDeleteDialogFragment.A05.Aaz(new AnonymousClass37T(r3, communityDeleteDialogFragment, (C15370n3) this.A01, C12970iu.A10(r3), elapsedRealtime), new Object[0]);
                return;
            case 4:
                Jid jid2 = (Jid) this.A01;
                AnonymousClass01E r32 = ((AnonymousClass01E) this.A00).A0D;
                if (r32 != null) {
                    ContactPickerFragment contactPickerFragment = (ContactPickerFragment) r32;
                    Intent A0A = C12970iu.A0A();
                    A0A.putExtra("contact", jid2.getRawString());
                    contactPickerFragment.A0m.A01(A0A);
                    contactPickerFragment.A0m.A00();
                    return;
                }
                return;
            case 5:
                AnonymousClass01E r33 = (AnonymousClass01E) this.A00;
                C51122Sx.A00(new C14960mK().A0g(r33.A0B(), (C15370n3) this.A01), r33);
                return;
            case 6:
                ViewOnClickCListenerShape18S0100000_I1_1 viewOnClickCListenerShape18S0100000_I1_1 = (ViewOnClickCListenerShape18S0100000_I1_1) this.A00;
                int size = ((AnonymousClass2y0) viewOnClickCListenerShape18S0100000_I1_1.A00).A0S.size();
                String A0g2 = C12960it.A0g((List) this.A01, i);
                if (i < size) {
                    viewOnClickCListenerShape18S0100000_I1_1.A06(A0g2);
                    return;
                } else {
                    viewOnClickCListenerShape18S0100000_I1_1.A05(A0g2);
                    return;
                }
            case 7:
                EncryptionChangeDialogFragment encryptionChangeDialogFragment = (EncryptionChangeDialogFragment) this.A00;
                Uri uri = (Uri) this.A01;
                AnonymousClass30Z r1 = encryptionChangeDialogFragment.A0A;
                if (r1 != null) {
                    r1.A01 = 2;
                    encryptionChangeDialogFragment.A09.A07(r1);
                }
                encryptionChangeDialogFragment.A00.A06(encryptionChangeDialogFragment.A0p(), C12970iu.A0B(uri));
                encryptionChangeDialogFragment.A1C();
                return;
            case 8:
                ArrayList<? extends Parcelable> arrayList = (ArrayList) this.A01;
                ActivityC000900k A0B = ((AnonymousClass01E) this.A00).A0B();
                if (A0B != null) {
                    Intent A0A2 = C12970iu.A0A();
                    A0A2.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
                    C12960it.A0q(A0B, A0A2);
                    return;
                }
                return;
            case 9:
                C626638c r34 = (C626638c) this.A00;
                GroupChatInfo groupChatInfo = (GroupChatInfo) this.A01;
                groupChatInfo.A2C(R.string.community_remove_group_progress_dialog_title);
                AbstractC14440lR r22 = r34.A07;
                C14900mE r5 = r34.A01;
                AbstractC15710nm r4 = r34.A00;
                C17220qS r10 = r34.A06;
                r22.Ab2(new RunnableBRunnable0Shape1S0700000_I1(r4, r5, r34.A02, r34.A03, groupChatInfo, (C15580nU) C15370n3.A03(r34.A05, C15580nU.class), r10));
                return;
            case 10:
                CreateGroupSuspendDialog createGroupSuspendDialog = (CreateGroupSuspendDialog) this.A00;
                createGroupSuspendDialog.A01.A00((ActivityC000900k) this.A01, "group-no-longer-available");
                createGroupSuspendDialog.A19().dismiss();
                return;
            case 11:
                CreateGroupSuspendDialog createGroupSuspendDialog2 = (CreateGroupSuspendDialog) this.A00;
                Activity activity = (Activity) this.A01;
                activity.startActivity(createGroupSuspendDialog2.A00.A00(activity, null, null, null, "group-suspend-appeal", null, null, null, createGroupSuspendDialog2.A02.A00()));
                return;
            case 12:
                C12960it.A0t(C12960it.A08(((AnonymousClass37P) this.A00).A02.A02), "delete_chat_clear_chat_nux_accepted", true);
                ((C14580lf) this.A01).A02(Boolean.TRUE);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                VoipActivityV2.ReplyWithMessageDialogFragment replyWithMessageDialogFragment = (VoipActivityV2.ReplyWithMessageDialogFragment) this.A00;
                String[] strArr = (String[]) this.A01;
                VoipActivityV2 voipActivityV2 = (VoipActivityV2) replyWithMessageDialogFragment.A0C();
                Intent A0i = new C14960mK().A0i(voipActivityV2, replyWithMessageDialogFragment.A02);
                if (i != strArr.length - 1) {
                    A0i.putExtra("wa_type", (byte) 0);
                    A0i.putExtra("share_msg", strArr[i]);
                    A0i.putExtra("has_share", true);
                    AnonymousClass2KJ.A00(replyWithMessageDialogFragment.A0p(), A0i);
                } else {
                    A0i.putExtra("show_keyboard", true);
                }
                C35741ib.A00(A0i, "VoipActivityV2");
                voipActivityV2.startActivity(A0i);
                voipActivityV2.A3B(2);
                return;
            default:
                return;
        }
    }
}
