package com.facebook.superpack;

/* loaded from: classes2.dex */
public class AssetDecompressionException extends Exception {
    public AssetDecompressionException(String str) {
        super(str);
    }
}
