package com.facebook.profilo.mmapbuf.core;

import X.C64733Gr;
import android.util.Log;
import com.facebook.jni.HybridData;
import com.facebook.soloader.SoLoader;
import java.io.File;
import java.util.UUID;

/* loaded from: classes2.dex */
public class Buffer {
    public static final String LOG_TAG = "Prflo/Buffer";
    public final HybridData mHybridData;

    private native void nativeUpdateId(String str);

    public native synchronized String getFilePath();

    public native synchronized String getMemoryMappingFilePath();

    public native synchronized void updateFilePath(String str);

    public native synchronized void updateHeader(int i, long j, long j2, long j3);

    public native synchronized void updateMemoryMappingFilePath(String str);

    static {
        SoLoader.A04("profilo_mmapbuf");
    }

    public Buffer(HybridData hybridData) {
        this.mHybridData = hybridData;
    }

    public synchronized String generateMemoryMappingFilePath() {
        String str;
        if (!isFileBacked()) {
            str = null;
        } else {
            str = getMemoryMappingFilePath();
            if (str == null) {
                C64733Gr r2 = new C64733Gr(getBufferContainingFolder());
                String obj = UUID.randomUUID().toString();
                StringBuilder sb = new StringBuilder();
                sb.append(C64733Gr.A00(obj));
                sb.append(".maps");
                str = r2.A01(sb.toString());
                if (str != null) {
                    updateMemoryMappingFilePath(str);
                }
            }
        }
        return str;
    }

    private File getBufferContainingFolder() {
        return new File(getFilePath()).getParentFile();
    }

    public boolean isFileBacked() {
        return getFilePath() != null;
    }

    public synchronized void updateId(String str) {
        if (isFileBacked()) {
            StringBuilder sb = new StringBuilder();
            sb.append(C64733Gr.A00(str));
            sb.append(".buff");
            String A01 = new C64733Gr(getBufferContainingFolder()).A01(sb.toString());
            if (A01 != null) {
                try {
                    nativeUpdateId(str);
                    updateFilePath(A01);
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Id update failed", e);
                }
            }
        }
    }
}
