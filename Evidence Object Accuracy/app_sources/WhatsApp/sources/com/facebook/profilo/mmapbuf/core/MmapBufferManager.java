package com.facebook.profilo.mmapbuf.core;

import X.C64733Gr;
import com.facebook.jni.HybridData;
import com.facebook.soloader.SoLoader;
import java.io.File;
import java.util.UUID;

/* loaded from: classes2.dex */
public class MmapBufferManager {
    public final C64733Gr mFileHelper;
    public final HybridData mHybridData = initHybrid();

    public static native HybridData initHybrid();

    private native Buffer nativeAllocateBuffer(int i);

    private native Buffer nativeAllocateBuffer(int i, String str);

    private native boolean nativeDeallocateBuffer(Buffer buffer);

    static {
        SoLoader.A04("profilo_mmapbuf");
    }

    public MmapBufferManager(File file) {
        this.mFileHelper = new C64733Gr(file);
    }

    public Buffer allocateBuffer(int i, boolean z) {
        if (!z) {
            return nativeAllocateBuffer(i);
        }
        String obj = UUID.randomUUID().toString();
        StringBuilder sb = new StringBuilder();
        sb.append(C64733Gr.A00(obj));
        sb.append(".buff");
        String A01 = this.mFileHelper.A01(sb.toString());
        if (A01 == null) {
            return null;
        }
        return nativeAllocateBuffer(i, A01);
    }

    public synchronized boolean deallocateBuffer(Buffer buffer) {
        return nativeDeallocateBuffer(buffer);
    }
}
