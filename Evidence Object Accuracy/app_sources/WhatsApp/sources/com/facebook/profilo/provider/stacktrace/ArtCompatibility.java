package com.facebook.profilo.provider.stacktrace;

import com.facebook.soloader.SoLoader;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes2.dex */
public class ArtCompatibility {
    public static final AtomicReference sIsCompatible = new AtomicReference(null);

    public static native boolean nativeCheck(int i);

    static {
        SoLoader.A04("profilo_stacktrace");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x009e, code lost:
        if (r1.equals("5.0.2") != false) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b7, code lost:
        if (r1.equals("5.1.1") != false) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c6, code lost:
        if (r1.equals("6.0.1") != false) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d3, code lost:
        if (r1.equals("7.0.0") != false) goto L_0x00d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e2, code lost:
        if (r1.equals("7.1.0") != false) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x012d, code lost:
        if (r1.equals("9.0.0") != false) goto L_0x012f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isCompatible(android.content.Context r7) {
        /*
        // Method dump skipped, instructions count: 442
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.ArtCompatibility.isCompatible(android.content.Context):boolean");
    }
}
