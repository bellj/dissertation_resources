package com.facebook.profilo.provider.threadmetadata;

import X.AnonymousClass1SY;
import X.AnonymousClass1Se;
import X.C29441Sr;
import com.facebook.profilo.mmapbuf.core.Buffer;

/* loaded from: classes2.dex */
public final class ThreadMetadataProvider extends AnonymousClass1SY {
    public static native void nativeLogThreadMetadata(Buffer buffer);

    @Override // X.AnonymousClass1SY
    public void disable() {
    }

    @Override // X.AnonymousClass1SY
    public void enable() {
    }

    @Override // X.AnonymousClass1SY
    public int getSupportedProviders() {
        return -1;
    }

    @Override // X.AnonymousClass1SY
    public int getTracingProviders() {
        return 0;
    }

    public ThreadMetadataProvider() {
        super("profilo_threadmetadata");
    }

    public void logOnTraceEnd(C29441Sr r2, AnonymousClass1Se r3) {
        nativeLogThreadMetadata(r2.A09);
    }

    @Override // X.AnonymousClass1SY
    public void onTraceEnded(C29441Sr r3, AnonymousClass1Se r4) {
        if (r3.A00 != 2) {
            logOnTraceEnd(r3, r4);
        }
    }
}
