package com.facebook.profilo.provider.atrace;

import X.AnonymousClass1Sc;
import X.C12960it;
import X.C88724Gu;
import com.facebook.profilo.logger.MultiBufferLogger;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* loaded from: classes3.dex */
public final class Atrace {
    public static boolean sHasHook;
    public static boolean sHookFailed;

    public static native void enableSystraceNative();

    public static native boolean installSystraceHook(MultiBufferLogger multiBufferLogger, int i);

    public static native boolean isEnabled();

    public static native void restoreSystraceNative();

    public static void enableSystrace(MultiBufferLogger multiBufferLogger) {
        Method method;
        if (hasHacks(multiBufferLogger)) {
            enableSystraceNative();
            Field field = C88724Gu.A00;
            if (field != null && (method = C88724Gu.A01) != null) {
                try {
                    field.set(null, method.invoke(null, new Object[0]));
                } catch (IllegalAccessException | InvocationTargetException unused) {
                }
            }
        }
    }

    public static synchronized boolean hasHacks(MultiBufferLogger multiBufferLogger) {
        boolean z;
        synchronized (Atrace.class) {
            z = sHasHook;
            if (!z && !sHookFailed) {
                z = installSystraceHook(multiBufferLogger, AnonymousClass1Sc.A00);
                sHasHook = z;
                int i = z ? 1 : 0;
                int i2 = z ? 1 : 0;
                sHookFailed = C12960it.A1T(i);
            }
        }
        return z;
    }

    public static void restoreSystrace(MultiBufferLogger multiBufferLogger) {
        Method method;
        if (hasHacks(multiBufferLogger)) {
            restoreSystraceNative();
            Field field = C88724Gu.A00;
            if (field != null && (method = C88724Gu.A01) != null) {
                try {
                    field.set(null, method.invoke(null, new Object[0]));
                } catch (IllegalAccessException | InvocationTargetException unused) {
                }
            }
        }
    }
}
