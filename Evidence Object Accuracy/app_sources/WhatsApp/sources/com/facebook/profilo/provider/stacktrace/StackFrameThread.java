package com.facebook.profilo.provider.stacktrace;

import X.AnonymousClass1SY;
import X.AnonymousClass1Se;
import X.C29441Sr;
import X.C29451St;
import android.app.Application;
import android.content.Context;
import com.facebook.profilo.core.ProvidersRegistry;
import com.facebook.profilo.logger.MultiBufferLogger;

/* loaded from: classes2.dex */
public final class StackFrameThread extends AnonymousClass1SY {
    public static final int PROVIDER_NATIVE_STACK_TRACE;
    public static final int PROVIDER_STACK_FRAME;
    public static final int PROVIDER_WALL_TIME_STACK_TRACE;
    public final Context mContext;
    public volatile boolean mEnabled;
    public Thread mProfilerThread;
    public C29441Sr mSavedTraceContext;
    public int mSystemClockTimeIntervalMs = -1;

    public static native int nativeCpuClockResolutionMicros();

    static {
        C29451St r1 = ProvidersRegistry.A00;
        PROVIDER_STACK_FRAME = r1.A02("stack_trace");
        PROVIDER_WALL_TIME_STACK_TRACE = r1.A02("wall_time_stack_trace");
        PROVIDER_NATIVE_STACK_TRACE = r1.A02("native_stack_trace");
    }

    public StackFrameThread(Context context) {
        super("profilo_stacktrace");
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null || !(context instanceof Application)) {
            this.mContext = applicationContext;
        } else {
            this.mContext = context;
        }
    }

    @Override // X.AnonymousClass1SY
    public void disable() {
        if (this.mEnabled) {
            this.mSavedTraceContext = null;
            this.mEnabled = false;
            synchronized (CPUProfiler.class) {
                if (CPUProfiler.sInitialized) {
                    CPUProfiler.nativeStopProfiling();
                }
            }
            Thread thread = this.mProfilerThread;
            if (thread != null) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } else {
                return;
            }
        }
        this.mProfilerThread = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00d7 A[Catch: all -> 0x0135, TryCatch #2 {, blocks: (B:41:0x00b1, B:43:0x00b8, B:44:0x00bc, B:52:0x00c9, B:55:0x00d2, B:57:0x00d7, B:59:0x00db, B:69:0x00f4, B:71:0x00f7, B:34:0x009c, B:77:0x0128, B:64:0x00e2, B:66:0x00ed), top: B:82:0x009b }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00dc  */
    @Override // X.AnonymousClass1SY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void enable() {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.StackFrameThread.enable():void");
    }

    @Override // X.AnonymousClass1SY
    public int getSupportedProviders() {
        return PROVIDER_NATIVE_STACK_TRACE | PROVIDER_STACK_FRAME | PROVIDER_WALL_TIME_STACK_TRACE;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if ((r2 & r1) != 0) goto L_0x0017;
     */
    @Override // X.AnonymousClass1SY
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getTracingProviders() {
        /*
            r4 = this;
            X.1Sr r1 = r4.mSavedTraceContext
            boolean r0 = r4.mEnabled
            r3 = 0
            if (r0 == 0) goto L_0x001d
            if (r1 == 0) goto L_0x001d
            int r2 = r1.A02
            int r1 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_WALL_TIME_STACK_TRACE
            r0 = r2 & r1
            if (r0 != 0) goto L_0x0017
            int r1 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_STACK_FRAME
            r0 = r2 & r1
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            r3 = r3 | r1
        L_0x0018:
            int r0 = com.facebook.profilo.provider.stacktrace.StackFrameThread.PROVIDER_NATIVE_STACK_TRACE
            r2 = r2 & r0
            r2 = r2 | r3
            return r2
        L_0x001d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.profilo.provider.stacktrace.StackFrameThread.getTracingProviders():int");
    }

    public final void logAnnotation(String str, String str2) {
        MultiBufferLogger A00 = A00();
        A00.writeBytesEntry(0, 57, A00.writeBytesEntry(0, 56, A00.writeStandardEntry(6, 52, 0, 0, 0, 0, 0), str), str2);
    }

    @Override // X.AnonymousClass1SY
    public void onTraceEnded(C29441Sr r5, AnonymousClass1Se r6) {
        int i;
        int i2 = r5.A02;
        int i3 = PROVIDER_STACK_FRAME;
        int i4 = PROVIDER_WALL_TIME_STACK_TRACE;
        if (((i3 | i4) & i2) != 0) {
            logAnnotation("provider.stack_trace.art_compatibility", Boolean.toString(ArtCompatibility.isCompatible(this.mContext)));
            int i5 = (PROVIDER_STACK_FRAME | i4) & i2;
            int i6 = 0;
            if (i5 != 0) {
                i6 = 32753;
            }
            if ((PROVIDER_NATIVE_STACK_TRACE & i2) != 0) {
                i6 |= 4;
            }
            synchronized (CPUProfiler.class) {
                i = CPUProfiler.sAvailableTracers;
            }
            logAnnotation("provider.stack_trace.tracers", Integer.toBinaryString(i6 & i));
        }
        if ((i2 & getSupportedProviders()) != 0) {
            logAnnotation("provider.stack_trace.cpu_timer_res_us", Integer.toString(nativeCpuClockResolutionMicros()));
        }
    }

    @Override // X.AnonymousClass1SY
    public void onTraceStarted(C29441Sr r2, AnonymousClass1Se r3) {
        if (CPUProfiler.sInitialized) {
            CPUProfiler.nativeResetFrameworkNamesSet();
        }
    }
}
