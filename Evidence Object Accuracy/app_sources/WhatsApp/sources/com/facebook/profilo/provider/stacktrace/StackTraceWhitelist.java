package com.facebook.profilo.provider.stacktrace;

import com.facebook.soloader.SoLoader;

/* loaded from: classes3.dex */
public class StackTraceWhitelist {
    public static native void nativeAddToWhitelist(int i);

    public static native void nativeRemoveFromWhitelist(int i);

    static {
        SoLoader.A04("profilo_stacktrace");
    }
}
