package com.facebook.soloader;

import X.AnonymousClass49V;
import android.os.Build;
import android.os.Process;
import java.util.ArrayList;
import java.util.TreeSet;

/* loaded from: classes2.dex */
public final class SysUtil$MarshmallowSysdeps {
    public static String[] getSupportedAbis() {
        AnonymousClass49V r0;
        String[] strArr = Build.SUPPORTED_ABIS;
        TreeSet treeSet = new TreeSet();
        if (Process.is64Bit()) {
            treeSet.add(AnonymousClass49V.A00.toString());
            r0 = AnonymousClass49V.A03;
        } else {
            treeSet.add(AnonymousClass49V.A01.toString());
            r0 = AnonymousClass49V.A02;
        }
        treeSet.add(r0.toString());
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (treeSet.contains(str)) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static boolean is64Bit() {
        return Process.is64Bit();
    }
}
