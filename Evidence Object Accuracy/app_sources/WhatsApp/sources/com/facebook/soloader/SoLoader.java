package com.facebook.soloader;

import X.AnonymousClass12B;
import X.AnonymousClass1QF;
import X.AnonymousClass1QG;
import X.AnonymousClass1QH;
import X.AnonymousClass1QJ;
import X.AnonymousClass1QK;
import X.AnonymousClass1QL;
import X.AnonymousClass1QM;
import X.AnonymousClass1QO;
import X.AnonymousClass1QP;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import dalvik.system.BaseDexClassLoader;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* loaded from: classes2.dex */
public class SoLoader {
    public static int A00;
    public static AnonymousClass1QP A01;
    public static AnonymousClass12B A02;
    public static boolean A03;
    public static AnonymousClass1QG[] A04;
    public static AnonymousClass1QJ[] A05;
    public static final HashSet A06 = new HashSet();
    public static final Map A07 = new HashMap();
    public static final Set A08 = Collections.newSetFromMap(new ConcurrentHashMap());
    public static final ReentrantReadWriteLock A09 = new ReentrantReadWriteLock();
    public static final boolean A0A;
    public static volatile int A0B;

    /* loaded from: classes3.dex */
    public class Api14Utils {
    }

    static {
        boolean z = false;
        try {
            if (Build.VERSION.SDK_INT >= 18) {
                z = true;
            }
        } catch (NoClassDefFoundError | UnsatisfiedLinkError unused) {
        }
        A0A = z;
    }

    public static int A00() {
        ReentrantReadWriteLock reentrantReadWriteLock = A09;
        reentrantReadWriteLock.writeLock().lock();
        try {
            int i = 0;
            if ((A00 & 2) != 0) {
                i = 1;
            }
            return i;
        } finally {
            reentrantReadWriteLock.writeLock().unlock();
        }
    }

    public static void A01() {
        ReentrantReadWriteLock reentrantReadWriteLock = A09;
        reentrantReadWriteLock.readLock().lock();
        try {
            boolean z = false;
            if (A04 != null) {
                z = true;
            }
            if (!z) {
                throw new RuntimeException("SoLoader.init() not yet called");
            }
        } finally {
            reentrantReadWriteLock.readLock().unlock();
        }
    }

    public static void A02(Context context, AnonymousClass12B r17, int i) {
        boolean z;
        int i2;
        boolean z2;
        boolean z3;
        String str;
        String str2;
        ClassLoader classLoader;
        AnonymousClass12B r9 = r17;
        StrictMode.ThreadPolicy allowThreadDiskWrites = StrictMode.allowThreadDiskWrites();
        boolean z4 = false;
        if ((i & 32) == 0 && context != null) {
            try {
                if ((context.getApplicationInfo().flags & 129) != 0) {
                    z4 = true;
                }
            } finally {
                StrictMode.setThreadPolicy(allowThreadDiskWrites);
            }
        }
        A03 = z4;
        synchronized (SoLoader.class) {
            if (r9 == null) {
                Runtime runtime = Runtime.getRuntime();
                int i3 = Build.VERSION.SDK_INT;
                Method method = null;
                if (i3 >= 23 && i3 <= 27) {
                    try {
                        Method declaredMethod = Runtime.class.getDeclaredMethod("nativeLoad", String.class, ClassLoader.class, String.class);
                        declaredMethod.setAccessible(true);
                        method = declaredMethod;
                        z3 = true;
                        classLoader = SoLoader.class.getClassLoader();
                    } catch (NoSuchMethodException | SecurityException e) {
                        Log.w("SoLoader", "Cannot get nativeLoad method", e);
                    }
                    if (classLoader == null || (classLoader instanceof BaseDexClassLoader)) {
                        try {
                            str2 = (String) BaseDexClassLoader.class.getMethod("getLdLibraryPath", new Class[0]).invoke((BaseDexClassLoader) classLoader, new Object[0]);
                            if (str2 != null) {
                                String[] split = str2.split(":");
                                int length = split.length;
                                ArrayList arrayList = new ArrayList(length);
                                for (String str3 : split) {
                                    if (!str3.contains("!")) {
                                        arrayList.add(str3);
                                    }
                                }
                                str = TextUtils.join(":", arrayList);
                                r9 = new AnonymousClass1QH(runtime, str2, str, method, z3);
                            }
                            str = null;
                            r9 = new AnonymousClass1QH(runtime, str2, str, method, z3);
                        } catch (Exception e2) {
                            throw new RuntimeException("Cannot call getLdLibraryPath", e2);
                        }
                    } else {
                        StringBuilder sb = new StringBuilder("ClassLoader ");
                        sb.append(classLoader.getClass().getName());
                        sb.append(" should be of type BaseDexClassLoader");
                        throw new IllegalStateException(sb.toString());
                    }
                }
                z3 = false;
                str2 = null;
                str = null;
                r9 = new AnonymousClass1QH(runtime, str2, str, method, z3);
            }
            A02 = r9;
        }
        ReentrantReadWriteLock reentrantReadWriteLock = A09;
        reentrantReadWriteLock.writeLock().lock();
        if (A04 == null) {
            Log.d("SoLoader", "init start");
            A00 = i;
            ArrayList arrayList2 = new ArrayList();
            String str4 = System.getenv("LD_LIBRARY_PATH");
            if (str4 == null) {
                int i4 = Build.VERSION.SDK_INT;
                if (i4 >= 23) {
                    z2 = SysUtil$MarshmallowSysdeps.is64Bit();
                } else {
                    if (i4 >= 21) {
                        try {
                            z2 = SysUtil$LollipopSysdeps.is64Bit();
                        } catch (Exception e3) {
                            Log.e("SysUtil", String.format("Could not read /proc/self/exe. Err msg: %s", e3.getMessage()));
                        }
                    }
                    str4 = "/vendor/lib:/system/lib";
                }
                if (z2) {
                    str4 = "/vendor/lib64:/system/lib64";
                }
                str4 = "/vendor/lib:/system/lib";
            }
            String[] split2 = str4.split(":");
            for (String str5 : split2) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("adding system library source: ");
                sb2.append(str5);
                Log.d("SoLoader", sb2.toString());
                arrayList2.add(new AnonymousClass1QF(new File(str5), 2));
            }
            if (context != null) {
                if ((i & 1) != 0) {
                    A05 = null;
                    Log.d("SoLoader", "adding exo package source: lib-main");
                    arrayList2.add(0, new AnonymousClass1QK(context));
                } else {
                    if (A03) {
                        i2 = 0;
                    } else {
                        int i5 = 0;
                        if (Build.VERSION.SDK_INT <= 17) {
                            i5 = 1;
                        }
                        AnonymousClass1QP r4 = new AnonymousClass1QP(context, i5);
                        A01 = r4;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("adding application source: ");
                        sb3.append(r4.toString());
                        Log.d("SoLoader", sb3.toString());
                        arrayList2.add(0, A01);
                        i2 = 1;
                    }
                    if ((A00 & 8) != 0) {
                        A05 = null;
                    } else {
                        File file = new File(context.getApplicationInfo().sourceDir);
                        ArrayList arrayList3 = new ArrayList();
                        AnonymousClass1QO r42 = new AnonymousClass1QO(context, file, "lib-main", i2);
                        arrayList3.add(r42);
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append("adding backup source from : ");
                        sb4.append(r42.toString());
                        Log.d("SoLoader", sb4.toString());
                        if (Build.VERSION.SDK_INT >= 21 && context.getApplicationInfo().splitSourceDirs != null) {
                            Log.d("SoLoader", "adding backup sources from split apks");
                            String[] strArr = context.getApplicationInfo().splitSourceDirs;
                            int length2 = strArr.length;
                            int i6 = 0;
                            int i7 = 0;
                            while (i6 < length2) {
                                File file2 = new File(strArr[i6]);
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("lib-");
                                int i8 = i7 + 1;
                                sb5.append(i7);
                                AnonymousClass1QO r43 = new AnonymousClass1QO(context, file2, sb5.toString(), i2);
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append("adding backup source: ");
                                sb6.append(r43.toString());
                                Log.d("SoLoader", sb6.toString());
                                arrayList3.add(r43);
                                i6++;
                                i7 = i8;
                            }
                        }
                        A05 = (AnonymousClass1QJ[]) arrayList3.toArray(new AnonymousClass1QJ[arrayList3.size()]);
                        arrayList2.addAll(0, arrayList3);
                    }
                }
            }
            AnonymousClass1QG[] r6 = (AnonymousClass1QG[]) arrayList2.toArray(new AnonymousClass1QG[arrayList2.size()]);
            int A002 = A00();
            int length3 = r6.length;
            while (true) {
                int i9 = length3 - 1;
                if (length3 <= 0) {
                    break;
                }
                StringBuilder sb7 = new StringBuilder();
                sb7.append("Preparing SO source: ");
                sb7.append(r6[i9]);
                Log.d("SoLoader", sb7.toString());
                r6[i9].A04(A002);
                length3 = i9;
            }
            A04 = r6;
            A0B++;
            StringBuilder sb8 = new StringBuilder();
            sb8.append("init finish: ");
            sb8.append(A04.length);
            sb8.append(" SO sources prepared");
            Log.d("SoLoader", sb8.toString());
        }
        Log.d("SoLoader", "init exiting");
        reentrantReadWriteLock.writeLock().unlock();
        synchronized (AnonymousClass1QL.class) {
            z = false;
            if (AnonymousClass1QL.A00 != null) {
                z = true;
            }
        }
        if (!z) {
            AnonymousClass1QM r1 = new AnonymousClass1QM();
            synchronized (AnonymousClass1QL.class) {
                if (AnonymousClass1QL.A00 == null) {
                    AnonymousClass1QL.A00 = r1;
                } else {
                    throw new IllegalStateException("Cannot re-initialize NativeLoader.");
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:142:0x008c */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:93:0x01a2 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:99:0x01af */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r20v0, types: [android.os.StrictMode$ThreadPolicy] */
    /* JADX WARN: Type inference failed for: r20v1 */
    /* JADX WARN: Type inference failed for: r0v22 */
    /* JADX WARN: Type inference failed for: r20v2 */
    /* JADX WARN: Type inference failed for: r20v3 */
    /* JADX WARN: Type inference failed for: r20v4, types: [int] */
    /* JADX WARN: Type inference failed for: r13v5, types: [X.1QJ[]] */
    /* JADX WARN: Type inference failed for: r1v21, types: [X.1QJ, X.1QG] */
    /* JADX WARN: Type inference failed for: r20v5 */
    /* JADX WARN: Type inference failed for: r20v6 */
    /* JADX WARN: Type inference failed for: r20v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01e1, code lost:
        r2 = new java.lang.StringBuilder();
        r2.append("couldn't find DSO to load: ");
        r2.append(r21);
        r1 = r13.getMessage();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01f0, code lost:
        if (r1 == null) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01f2, code lost:
        r1 = r13.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01f6, code lost:
        r2.append(" caused by: ");
        r2.append(r1);
        r13.printStackTrace();
        r2.append(" result: ");
        r2.append(r7);
        r1 = r2.toString();
        android.util.Log.e("SoLoader", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0213, code lost:
        throw new java.lang.UnsatisfiedLinkError(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a8, code lost:
        if (com.facebook.soloader.SoLoader.A05 == null) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00aa, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append("Trying backup SoSource for ");
        r1.append(r21);
        android.util.Log.d("SoLoader", r1.toString());
        r13 = com.facebook.soloader.SoLoader.A05;
        r0 = r13.length;
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00cc, code lost:
        if (r14 >= r0) goto L_0x0103;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ce, code lost:
        r1 = r13[r14];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d0, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d1, code lost:
        r15 = r1.A03;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d3, code lost:
        monitor-enter(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00d4, code lost:
        r0 = r15.get(r21);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d8, code lost:
        if (r0 != null) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00da, code lost:
        r0 = new java.lang.Object();
        r15.put(r21, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e2, code lost:
        monitor-exit(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e3, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e4, code lost:
        r1.A00 = r21;
        r1.A04(2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00ea, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00eb, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00f2, code lost:
        if (r1.A00(r10, r21, r20) != 1) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00f5, code lost:
        r14 = r14 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00f8, code lost:
        r12 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0103, code lost:
        r18.readLock().unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x010a, code lost:
        if (r16 == false) goto L_0x010f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x010c, code lost:
        com.facebook.soloader.Api18TraceUtils.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x010f, code lost:
        if (r17 == false) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0111, code lost:
        android.os.StrictMode.setThreadPolicy(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0114, code lost:
        if (r12 == 0) goto L_0x0118;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0116, code lost:
        if (r12 != 3) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0118, code lost:
        r2 = new java.lang.StringBuilder();
        r2.append("couldn't find DSO to load: ");
        r2.append(r21);
        r18.readLock().lock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x012a, code lost:
        r1 = com.facebook.soloader.SoLoader.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x012d, code lost:
        if (r7 >= r1.length) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x012f, code lost:
        r2.append("\n\tSoSource ");
        r2.append(r7);
        r2.append(": ");
        r2.append(r1[r7].toString());
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0148, code lost:
        r0 = com.facebook.soloader.SoLoader.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x014a, code lost:
        if (r0 == null) goto L_0x017b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x014c, code lost:
        r5 = r0.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0158, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x015e, code lost:
        throw new java.lang.RuntimeException(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x015f, code lost:
        r1 = new java.io.File(r5.createPackageContext(r5.getPackageName(), 0).getApplicationInfo().nativeLibraryDir);
        r2.append("\n\tNative lib dir: ");
        r2.append(r1.getAbsolutePath());
        r2.append("\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x017b, code lost:
        r18.readLock().unlock();
        r2.append(" result: ");
        r2.append(r12);
        r1 = r2.toString();
        android.util.Log.e("SoLoader", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0194, code lost:
        throw new java.lang.UnsatisfiedLinkError(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0195, code lost:
        r13 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0196, code lost:
        r7 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01a3, code lost:
        if (r16 != false) goto L_0x01a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01a5, code lost:
        com.facebook.soloader.Api18TraceUtils.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01a8, code lost:
        if (r17 != false) goto L_0x01aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01aa, code lost:
        android.os.StrictMode.setThreadPolicy(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01ad, code lost:
        if (r7 != 0) goto L_0x01af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01af, code lost:
        if (r7 == 3) goto L_0x01b1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01b3 A[Catch: all -> 0x01de, TRY_ENTER, TryCatch #10 {, blocks: (B:19:0x003b, B:25:0x0046, B:29:0x006d, B:33:0x007c, B:34:0x0082, B:36:0x0086, B:73:0x010c, B:75:0x0111, B:78:0x0118, B:79:0x012a, B:81:0x012f, B:82:0x0148, B:84:0x014c, B:86:0x0159, B:87:0x015e, B:88:0x015f, B:89:0x017b, B:90:0x0194, B:95:0x01a5, B:97:0x01aa, B:101:0x01b2, B:106:0x01cf, B:108:0x01d5, B:109:0x01dc, B:111:0x01e1, B:113:0x01f2, B:114:0x01f6, B:115:0x0213, B:119:0x0243, B:120:0x024a, B:122:0x024c, B:124:0x0252, B:126:0x025b, B:127:0x0268, B:128:0x0269, B:20:0x003c, B:22:0x0042, B:24:0x0045, B:102:0x01b3, B:103:0x01ca, B:104:0x01cb), top: B:146:0x003b }] */
    /* JADX WARNING: Unknown variable types count: 5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A03(android.os.StrictMode.ThreadPolicy r20, java.lang.String r21, java.lang.String r22, int r23) {
        /*
        // Method dump skipped, instructions count: 627
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.SoLoader.A03(android.os.StrictMode$ThreadPolicy, java.lang.String, java.lang.String, int):boolean");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d3 A[LOOP:0: B:49:0x0049->B:31:0x00d3, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00dd A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(java.lang.String r14) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.soloader.SoLoader.A04(java.lang.String):boolean");
    }

    public static void init(Context context, int i) {
        A02(context, null, i);
    }
}
