package com.facebook.soloader;

import android.os.Trace;

/* loaded from: classes2.dex */
public class Api18TraceUtils {
    public static void A00() {
        Trace.endSection();
    }

    public static void A01(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        sb.append("]");
        String obj = sb.toString();
        if (obj.length() > 127 && str2 != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(str2.substring(0, (127 - str.length()) - "]".length()));
            sb2.append("]");
            obj = sb2.toString();
        }
        Trace.beginSection(obj);
    }
}
