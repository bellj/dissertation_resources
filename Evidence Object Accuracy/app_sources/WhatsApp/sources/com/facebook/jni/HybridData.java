package com.facebook.jni;

import X.AnonymousClass1QL;
import X.AnonymousClass5HR;

/* loaded from: classes2.dex */
public class HybridData {
    public Destructor mDestructor = new Destructor(this);

    static {
        AnonymousClass1QL.A00("fbjni");
    }

    public boolean isValid() {
        return this.mDestructor.mNativePointer != 0;
    }

    public synchronized void resetNative() {
        this.mDestructor.destruct();
    }

    /* loaded from: classes3.dex */
    public class Destructor extends AnonymousClass5HR {
        public volatile long mNativePointer;

        public static native void deleteNative(long j);

        public Destructor(Object obj) {
            super(obj);
        }

        @Override // X.AnonymousClass5HR
        public final void destruct() {
            deleteNative(this.mNativePointer);
            this.mNativePointer = 0;
        }
    }
}
