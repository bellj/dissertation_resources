package com.facebook.imagepipeline.platform;

import X.C12970iu;
import android.graphics.Bitmap;

/* loaded from: classes3.dex */
public class PreverificationHelper {
    public boolean shouldUseHardwareBitmapConfig(Bitmap.Config config) {
        return C12970iu.A1Z(config, Bitmap.Config.HARDWARE);
    }
}
