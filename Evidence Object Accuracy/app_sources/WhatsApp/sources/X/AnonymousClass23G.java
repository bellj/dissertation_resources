package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.23G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23G {
    public int A00 = -1;
    public final View A01;
    public final View A02;
    public final View A03;
    public final ViewTreeObserver.OnPreDrawListener A04 = new ViewTreeObserver$OnPreDrawListenerC66373Ni(this);
    public final ViewTreeObserver.OnPreDrawListener A05 = new ViewTreeObserver$OnPreDrawListenerC101844oB(this);

    public AnonymousClass23G(View view, View view2, View view3) {
        this.A02 = view;
        this.A03 = view2;
        this.A01 = view3;
        view.getViewTreeObserver().addOnPreDrawListener(this.A05);
        this.A01.setVisibility(8);
        this.A03.setVisibility(0);
    }
}
