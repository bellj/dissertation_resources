package X;

import android.view.View;

/* renamed from: X.0WL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WL implements View.OnClickListener {
    public final /* synthetic */ C04550Md A00;
    public final /* synthetic */ C05150Ol A01;

    public AnonymousClass0WL(C04550Md r1, C05150Ol r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        C05150Ol r2 = this.A01;
        r2.A02.post(new RunnableC09380co(r2));
        AnonymousClass0OS r0 = (AnonymousClass0OS) this.A00.A00.A0B.peek();
        if (r0 != null) {
            C06420Tn r02 = r0.A00.A00;
            if (r02 != null) {
                r02.A01();
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
            throw new IllegalStateException("RequestData does not exist in BloksSurfaceController.");
        }
    }
}
