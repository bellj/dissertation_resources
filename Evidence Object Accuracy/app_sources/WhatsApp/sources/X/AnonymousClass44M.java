package X;

import org.json.JSONObject;

/* renamed from: X.44M  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44M extends AnonymousClass12Z {
    @Override // X.AnonymousClass12Z
    public void A00(JSONObject jSONObject, long j) {
        this.A00 = jSONObject.getJSONObject("whatsapp_biz_integrity_delete_p2b_report").getString("message");
    }
}
