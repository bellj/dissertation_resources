package X;

/* renamed from: X.43l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856043l extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public String A03;
    public String A04;

    public C856043l() {
        super(2166, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A00);
        r3.Abe(3, this.A03);
        r3.Abe(1, this.A04);
        r3.Abe(4, this.A01);
        r3.Abe(5, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPushNotificationReceived {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "deliveredPriority", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pushNotificationEventId", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pushNotificationId", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pushReceivedT", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pushSentT", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
