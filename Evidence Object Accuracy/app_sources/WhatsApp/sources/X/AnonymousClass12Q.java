package X;

import android.content.Context;
import android.net.Uri;

/* renamed from: X.12Q  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass12Q {
    void Ab9(Context context, Uri uri);

    void AbA(Context context, Uri uri, int i);

    void AbB(Context context, Uri uri, int i, int i2);
}
