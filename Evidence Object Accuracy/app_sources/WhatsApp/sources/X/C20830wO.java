package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.0wO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20830wO {
    public List A00;
    public Map A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final C15680nj A04;
    public final C20660w7 A05;
    public final Object A06 = new Object();

    public C20830wO(C15550nR r2, C15610nY r3, C15680nj r4, C20660w7 r5) {
        this.A05 = r5;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
    }

    public static boolean A00(AbstractC14640lm r2, List list) {
        if (list == null || list.isEmpty()) {
            return true;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (!((AbstractC33171dZ) it.next()).A9v(r2)) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0019, code lost:
        if (r4.A06.A0C(r1) == false) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C15370n3 A01(X.AbstractC14640lm r6) {
        /*
            r5 = this;
            X.0nR r0 = r5.A02
            X.0n3 r3 = r0.A0B(r6)
            X.0nY r4 = r5.A03
            java.lang.Class<X.0nU> r0 = X.C15580nU.class
            com.whatsapp.jid.Jid r1 = r3.A0B(r0)
            com.whatsapp.jid.GroupJid r1 = (com.whatsapp.jid.GroupJid) r1
            if (r1 == 0) goto L_0x001b
            X.0nX r0 = r4.A06
            boolean r0 = r0.A0C(r1)
            r2 = 1
            if (r0 != 0) goto L_0x001c
        L_0x001b:
            r2 = 0
        L_0x001c:
            com.whatsapp.jid.Jid r1 = r3.A0D
            boolean r0 = X.C15380n4.A0J(r1)
            if (r0 == 0) goto L_0x0044
            boolean r0 = X.C15380n4.A0O(r1)
            if (r0 != 0) goto L_0x0044
            r0 = -1
            boolean r0 = r4.A0L(r3, r0)
            if (r0 != 0) goto L_0x0039
            java.lang.String r0 = r3.A0P
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0044
        L_0x0039:
            if (r2 == 0) goto L_0x0044
            X.0w7 r2 = r5.A05
            X.0nU r6 = (X.C15580nU) r6
            r1 = 0
            r0 = 3
            r2.A0D(r6, r1, r0)
        L_0x0044:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20830wO.A01(X.0lm):X.0n3");
    }

    public List A02() {
        List list;
        synchronized (this.A06) {
            if (this.A00 == null) {
                CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
                this.A00 = copyOnWriteArrayList;
                this.A02.A06.A0S(copyOnWriteArrayList, 0, false, false);
            }
            list = this.A00;
        }
        return list;
    }

    public List A03(int i) {
        List A04 = this.A04.A04();
        ArrayList arrayList = new ArrayList(Math.min(A04.size(), i));
        for (int i2 = 0; i2 < A04.size() && arrayList.size() < i; i2++) {
            A04.get(i2);
            C15370n3 A01 = A01((AbstractC14640lm) A04.get(i2));
            if (!TextUtils.isEmpty(A01.A0K)) {
                arrayList.add(A01);
            }
        }
        return arrayList;
    }

    public Map A04() {
        Map map;
        synchronized (this.A06) {
            if (this.A01 == null) {
                List<C15370n3> A02 = A02();
                this.A01 = new HashMap(A02.size(), 1.0f);
                for (C15370n3 r7 : A02) {
                    C15370n3 r0 = (C15370n3) this.A01.get(r7.A0B(AbstractC14640lm.class));
                    if (r0 == null || r0.A08() > r7.A08()) {
                        AbstractC14640lm r1 = (AbstractC14640lm) r7.A0B(AbstractC14640lm.class);
                        if (r1 != null) {
                            this.A01.put(r1, r7);
                        }
                    }
                }
                ArrayList arrayList = new ArrayList();
                for (AbstractC14640lm r2 : this.A04.A04()) {
                    if (this.A01.get(r2) == null) {
                        C15370n3 A01 = A01(r2);
                        arrayList.add(A01);
                        this.A01.put(r2, A01);
                    }
                }
                List list = this.A00;
                AnonymousClass009.A05(list);
                list.addAll(arrayList);
            }
            map = this.A01;
        }
        return map;
    }
}
