package X;

import android.view.LayoutInflater;
import com.whatsapp.gallery.GalleryTabHostFragment;

/* renamed from: X.2Sf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50972Sf extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ GalleryTabHostFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C50972Sf(GalleryTabHostFragment galleryTabHostFragment) {
        super(0);
        this.this$0 = galleryTabHostFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        LayoutInflater from = LayoutInflater.from(this.this$0.A01());
        C16700pc.A0B(from);
        return new C50992Sh(from, (C457522x) this.this$0.A0C.getValue());
    }
}
