package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130755zv {
    public AnonymousClass1ZR A00;
    public String A01;

    public C130755zv(AnonymousClass1V8 r6) {
        this.A00 = C117305Zk.A0I(C117305Zk.A0J(), String.class, r6.A0I("id", null), "upiSequenceNumber");
        this.A01 = r6.A0I("status", null);
    }

    public C130755zv(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                this.A00 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A05.optString("id"), "upiSequenceNumber");
                this.A01 = A05.optString("status");
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiMandateMetadata:InstanceTransaction threw: ", e);
            }
        }
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ id: ");
        A0k.append(this.A00);
        A0k.append(" status: ");
        C1309060l.A03(A0k, this.A01);
        return C12960it.A0d("]", A0k);
    }
}
