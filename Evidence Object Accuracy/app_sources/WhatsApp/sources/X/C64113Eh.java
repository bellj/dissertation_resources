package X;

import java.util.Map;

/* renamed from: X.3Eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64113Eh {
    public C49262Kb A00;
    public AnonymousClass397 A01;
    public Map A02;
    public final int A03;
    public final AnonymousClass2Ka A04 = new AnonymousClass2Ka();
    public final AbstractC115965Tp A05;
    public final AbstractC115975Tq A06;
    public volatile boolean A07 = false;

    public C64113Eh(AbstractC115965Tp r2, AbstractC115975Tq r3, int i) {
        this.A03 = i;
        this.A06 = r3;
        this.A05 = r2;
    }

    public synchronized void A00() {
        if (this.A07) {
            this.A07 = false;
            AnonymousClass397 r0 = this.A01;
            AnonymousClass009.A05(r0);
            r0.interrupt();
            this.A01 = null;
        }
    }

    public synchronized void A01() {
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass397 r0 = new AnonymousClass397(this);
            this.A01 = r0;
            r0.start();
        }
    }
}
