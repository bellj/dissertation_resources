package X;

import android.util.Property;
import android.view.View;

/* renamed from: X.0Am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02220Am extends Property {
    public C02220Am() {
        super(Float.class, "translationAlpha");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return Float.valueOf(AnonymousClass0U3.A04.A00((View) obj));
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        float floatValue = ((Number) obj2).floatValue();
        AnonymousClass0U3.A04.A05((View) obj, floatValue);
    }
}
