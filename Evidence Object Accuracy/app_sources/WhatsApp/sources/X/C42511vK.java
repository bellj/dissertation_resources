package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.WaMapView;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.1vK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42511vK extends AbstractC42521vL {
    public AnonymousClass130 A00;
    public C16030oK A01;
    public final View A02;
    public final View A03 = findViewById(R.id.control_frame);
    public final View A04 = findViewById(R.id.live_location_label_holder);
    public final View A05;
    public final View A06 = findViewById(R.id.progress_bar);
    public final View A07;
    public final View A08;
    public final View A09 = findViewById(R.id.thumb_button);
    public final FrameLayout A0A;
    public final ImageView A0B;
    public final ImageView A0C;
    public final ImageView A0D;
    public final ImageView A0E = ((ImageView) findViewById(R.id.thumb));
    public final TextView A0F = ((TextView) findViewById(R.id.control_btn));
    public final TextView A0G = ((TextView) findViewById(R.id.live_location_label));
    public final TextEmojiLabel A0H;
    public final TextEmojiLabel A0I;
    public final ThumbnailButton A0J;
    public final AnonymousClass1J1 A0K;
    public final WaMapView A0L;

    public C42511vK(Context context, AnonymousClass1J1 r5, AbstractC13890kV r6, C30341Xa r7) {
        super(context, r6, r7);
        this.A0K = r5;
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.map_frame);
        this.A0A = frameLayout;
        this.A0J = (ThumbnailButton) findViewById(R.id.contact_thumbnail);
        this.A02 = findViewById(R.id.contact_thumbnail_overlay);
        this.A05 = findViewById(R.id.message_info_holder);
        this.A08 = findViewById(R.id.text_and_date);
        this.A07 = findViewById(R.id.btn_divider);
        this.A0I = (TextEmojiLabel) findViewById(R.id.stop_share_btn);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) findViewById(R.id.live_location_caption);
        this.A0H = textEmojiLabel;
        this.A0B = (ImageView) findViewById(R.id.live_location_icon_1);
        this.A0C = (ImageView) findViewById(R.id.live_location_icon_2);
        this.A0D = (ImageView) findViewById(R.id.live_location_icon_3);
        this.A0L = (WaMapView) findViewById(R.id.map_holder);
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setFocusable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
        if (frameLayout != null) {
            frameLayout.setForeground(getLiveLocationFrameForegroundDrawable());
        }
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != ((AbstractC28551Oa) this).A0O) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A1M();
        }
    }

    public final void A1M() {
        long A04;
        ImageView imageView;
        ImageView imageView2;
        int dimensionPixelSize;
        int dimensionPixelSize2;
        int dimensionPixelSize3;
        Resources resources;
        int i;
        int dimensionPixelSize4;
        C15370n3 A01;
        C30341Xa r10 = (C30341Xa) ((AbstractC28551Oa) this).A0O;
        View view = this.A09;
        View.OnLongClickListener onLongClickListener = this.A1a;
        view.setOnLongClickListener(onLongClickListener);
        TextEmojiLabel textEmojiLabel = this.A0I;
        textEmojiLabel.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(r10, 17, this));
        textEmojiLabel.setOnLongClickListener(onLongClickListener);
        View view2 = this.A03;
        if (view2 != null) {
            view2.setVisibility(8);
        }
        View view3 = this.A08;
        if (view3 != null) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view3.getLayoutParams();
            marginLayoutParams.topMargin = 0;
            marginLayoutParams.bottomMargin = 0;
        }
        this.A0A.setVisibility(0);
        long A00 = this.A0k.A00();
        C16030oK r1 = this.A01;
        AnonymousClass009.A05(r1);
        boolean z = r10.A0z.A02;
        if (z) {
            A04 = r1.A05(r10);
        } else {
            A04 = r1.A04(r10);
        }
        boolean A02 = C65173Ik.A02(this.A0k, r10, A04);
        ((AnonymousClass1OY) this).A0L.A08();
        View view4 = this.A05;
        if (view4 != null) {
            view4.setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.media_message_thumb));
        }
        ImageView imageView3 = this.A0B;
        if (A02) {
            imageView3.setVisibility(0);
            imageView = this.A0C;
            imageView.setVisibility(0);
            imageView2 = this.A0D;
            imageView2.setVisibility(0);
        } else {
            imageView3.setVisibility(8);
            imageView = this.A0C;
            imageView.setVisibility(8);
            imageView2 = this.A0D;
            imageView2.setVisibility(8);
        }
        imageView.clearAnimation();
        imageView2.clearAnimation();
        if (A02 && A04 > A00) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(1000);
            alphaAnimation.setInterpolator(new DecelerateInterpolator());
            alphaAnimation.setRepeatCount(-1);
            alphaAnimation.setRepeatMode(2);
            alphaAnimation.setAnimationListener(new C83233wu(this));
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation2.setDuration(1000);
            alphaAnimation2.setStartOffset(300);
            alphaAnimation2.setInterpolator(new DecelerateInterpolator());
            alphaAnimation2.setRepeatCount(-1);
            alphaAnimation2.setRepeatMode(2);
            imageView.startAnimation(alphaAnimation);
            imageView2.startAnimation(alphaAnimation2);
        }
        this.A04.setVisibility(0);
        Context context = getContext();
        C15570nT r2 = ((AnonymousClass1OY) this).A0L;
        C244415n r12 = ((AbstractC28551Oa) this).A0N;
        AnonymousClass009.A05(r12);
        View.OnClickListener A002 = C65173Ik.A00(context, r2, r12, r10, A02);
        View view5 = this.A07;
        if (A02) {
            view5.setVisibility(0);
            textEmojiLabel.setVisibility(0);
        } else {
            view5.setVisibility(8);
            textEmojiLabel.setVisibility(8);
        }
        view.setOnClickListener(A002);
        String A012 = C65173Ik.A01(getContext(), ((AnonymousClass1OY) this).A0L, this.A0k, ((AbstractC28551Oa) this).A0K, this.A01, r10, A02);
        TextView textView = this.A0G;
        textView.setText(A012);
        textView.setTextColor(getSecondaryTextColor());
        View view6 = this.A02;
        if (view6 != null) {
            view6.setVisibility(8);
        }
        WaMapView waMapView = this.A0L;
        C244415n r122 = ((AbstractC28551Oa) this).A0N;
        AnonymousClass009.A05(r122);
        waMapView.A02(r122, r10, A02);
        if (waMapView.getVisibility() == 0) {
            ThumbnailButton thumbnailButton = this.A0J;
            C15570nT r13 = ((AnonymousClass1OY) this).A0L;
            AnonymousClass130 r14 = this.A00;
            AnonymousClass009.A05(r14);
            AnonymousClass1J1 r123 = this.A0K;
            C20830wO r15 = this.A0o;
            if (z) {
                r13.A08();
                A01 = r13.A01;
                AnonymousClass009.A05(A01);
            } else {
                UserJid A0C = r10.A0C();
                if (A0C != null) {
                    A01 = r15.A01(A0C);
                } else {
                    r14.A05(thumbnailButton, R.drawable.avatar_contact);
                }
            }
            r123.A06(thumbnailButton, A01);
        }
        if (!TextUtils.isEmpty(r10.A03)) {
            setMessageText(r10.A03, this.A0H, r10);
            int i2 = 8;
            if (A02) {
                i2 = 0;
            }
            view5.setVisibility(i2);
            Resources resources2 = getResources();
            i = R.dimen.conversation_live_location_button_padding;
            dimensionPixelSize = resources2.getDimensionPixelSize(R.dimen.conversation_live_location_button_padding);
            dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.conversation_live_location_button_padding_top);
            dimensionPixelSize3 = getResources().getDimensionPixelSize(R.dimen.conversation_live_location_button_padding);
            resources = getResources();
        } else {
            setMessageText("", this.A0H, r10);
            view5.setVisibility(8);
            dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.conversation_live_location_button_padding);
            dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.conversation_live_location_button_padding_top_no_comment);
            dimensionPixelSize3 = getResources().getDimensionPixelSize(R.dimen.conversation_live_location_button_padding);
            resources = getResources();
            i = R.dimen.conversation_live_location_button_padding_bottom_no_comment;
        }
        textEmojiLabel.setPadding(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize3, resources.getDimensionPixelSize(i));
        if (view3 != null) {
            boolean isEmpty = TextUtils.isEmpty(r10.A03);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            if (isEmpty) {
                layoutParams.addRule(11);
                layoutParams.addRule(8, R.id.live_location_info_holder);
                view3.setLayoutParams(layoutParams);
                ViewGroup viewGroup = ((AnonymousClass1OY) this).A05;
                viewGroup.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                dimensionPixelSize4 = viewGroup.getMeasuredWidth() + getResources().getDimensionPixelSize(R.dimen.conversation_live_location_label_padding_right);
            } else {
                layoutParams.addRule(11);
                layoutParams.addRule(3, R.id.live_location_info_holder);
                view3.setLayoutParams(layoutParams);
                dimensionPixelSize4 = getResources().getDimensionPixelSize(R.dimen.conversation_live_location_label_padding_right);
            }
            boolean z2 = !((AbstractC28551Oa) this).A0K.A04().A06;
            ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
            if (z2) {
                marginLayoutParams2.rightMargin = dimensionPixelSize4;
            } else {
                marginLayoutParams2.leftMargin = dimensionPixelSize4;
            }
        }
        TextView textView2 = this.A0F;
        if (textView2 != null) {
            textView2.setVisibility(8);
        }
        int i3 = ((AnonymousClass1XP) r10).A02;
        if (i3 == 1) {
            View view7 = this.A06;
            if (z) {
                view7.setVisibility(0);
                if (view2 != null) {
                    view2.setVisibility(0);
                }
                view.setOnClickListener(null);
            } else {
                view7.setVisibility(0);
            }
        } else if (!z || i3 == 2 || !A02) {
            View view8 = this.A06;
            if (view8 != null) {
                view8.setVisibility(8);
            }
        } else {
            View view9 = this.A06;
            if (view9 != null) {
                view9.setVisibility(8);
            }
            if (textView2 != null) {
                ((AnonymousClass1OY) this).A0L.A08();
                textView2.setVisibility(0);
                textView2.setText(R.string.retry);
                textView2.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this));
            }
            if (view2 != null) {
                view2.setVisibility(0);
            }
            view5.setVisibility(8);
            textEmojiLabel.setVisibility(8);
            ((AnonymousClass1OY) this).A0L.A08();
            view.setOnClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this));
        }
        if (waMapView.getVisibility() == 8) {
            this.A1O.A08(this.A0E, r10, new C70223aw(this));
        }
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void dispatchSetPressed(boolean z) {
        super.dispatchSetPressed(z);
        FrameLayout frameLayout = this.A0A;
        if (frameLayout != null) {
            frameLayout.setForeground(getLiveLocationFrameForegroundDrawable());
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_live_location_left_large;
    }

    @Override // X.AbstractC28551Oa
    public C30341Xa getFMessage() {
        return (C30341Xa) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_live_location_left_large;
    }

    public Drawable getLiveLocationFrameForegroundDrawable() {
        if (isPressed()) {
            Context context = getContext();
            boolean z = ((AbstractC28551Oa) this).A0O.A0z.A02;
            int i = R.color.bubble_color_incoming_pressed;
            if (z) {
                i = R.color.bubble_color_outgoing_pressed;
            }
            return AnonymousClass2GE.A01(context, R.drawable.balloon_live_location_incoming_frame, i);
        }
        boolean z2 = ((AbstractC28551Oa) this).A0O.A0z.A02;
        int i2 = R.drawable.balloon_live_location_incoming_frame;
        if (z2) {
            i2 = R.drawable.balloon_live_location_outgoing_frame;
        }
        Context context2 = getContext();
        boolean z3 = ((AbstractC28551Oa) this).A0O.A0z.A02;
        int i3 = R.color.bubble_color_incoming;
        if (z3) {
            i3 = R.color.bubble_color_outgoing;
        }
        return AnonymousClass2GE.A01(context2, i2, i3);
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_live_location_right_large;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C30341Xa);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
