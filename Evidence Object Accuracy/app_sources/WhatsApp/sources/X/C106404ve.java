package X;

/* renamed from: X.4ve  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106404ve implements AnonymousClass5Pv {
    public int A00;
    public boolean A01;
    public final int A02;
    public final long A03;
    public final long A04;
    public final long A05;
    public final long A06;
    public final long A07;
    public final C107834y1 A08;

    public C106404ve() {
        this(new C107834y1(65536), 50000, 50000, 2500, 5000);
    }

    public C106404ve(C107834y1 r7, int i, int i2, int i3, int i4) {
        A00("bufferForPlaybackMs", "0", i3, 0);
        A00("bufferForPlaybackAfterRebufferMs", "0", i4, 0);
        A00("minBufferMs", "bufferForPlaybackMs", i, i3);
        A00("minBufferMs", "bufferForPlaybackAfterRebufferMs", i, i4);
        A00("maxBufferMs", "minBufferMs", i2, i);
        A00("backBufferDurationMs", "0", 0, 0);
        this.A08 = r7;
        this.A07 = C95214dK.A01((long) i);
        this.A06 = C95214dK.A01((long) i2);
        this.A05 = C95214dK.A01((long) i3);
        this.A04 = C95214dK.A01((long) i4);
        this.A02 = -1;
        this.A00 = 13107200;
        this.A03 = C95214dK.A01((long) 0);
    }

    public static void A00(String str, String str2, int i, int i2) {
        boolean A1X = C12990iw.A1X(i, i2);
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append(" cannot be less than ");
        String A0d = C12960it.A0d(str2, A0j);
        if (!A1X) {
            throw C12970iu.A0f(String.valueOf(A0d));
        }
    }

    public final void A01(boolean z) {
        int i = this.A02;
        if (i == -1) {
            i = 13107200;
        }
        this.A00 = i;
        this.A01 = false;
        if (z) {
            C107834y1 r1 = this.A08;
            synchronized (r1) {
                if (r1.A05) {
                    r1.A00(0);
                }
            }
        }
    }
}
