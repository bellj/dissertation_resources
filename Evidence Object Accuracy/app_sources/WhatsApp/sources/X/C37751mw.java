package X;

import android.net.Uri;

/* renamed from: X.1mw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37751mw implements AbstractC37701mr {
    public final Uri.Builder A00;
    public final String A01;
    public final String A02;

    public C37751mw(String str, String str2) {
        Uri parse = Uri.parse(str);
        this.A00 = parse.buildUpon();
        this.A01 = parse.getAuthority();
        this.A02 = str2;
    }

    @Override // X.AbstractC37701mr
    public String AAJ(C15450nH r3, C28481Nj r4, boolean z) {
        return this.A00.encodedAuthority(r4.A02).build().toString();
    }
}
