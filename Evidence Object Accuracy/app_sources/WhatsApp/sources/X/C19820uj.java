package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.0uj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19820uj {
    public final C14650lo A00;

    public C19820uj(C14650lo r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    public final void A00(UserJid userJid, AnonymousClass1WA r12, String str, String str2, String str3, boolean z) {
        C16700pc.A0E(userJid, 0);
        C16700pc.A0E(str, 2);
        C16700pc.A0E(str2, 4);
        C16700pc.A0E(str3, 5);
        C14650lo r1 = this.A00;
        C247016n r2 = r1.A08;
        if (r2.A02(userJid)) {
            r2.A01(userJid, r12, str, str2, str3, z);
        } else {
            r1.A03(new AbstractC30111Wd(userJid, r12, this, str, str2, str3, z) { // from class: X.53S
                public final /* synthetic */ UserJid A00;
                public final /* synthetic */ AnonymousClass1WA A01;
                public final /* synthetic */ C19820uj A02;
                public final /* synthetic */ String A03;
                public final /* synthetic */ String A04;
                public final /* synthetic */ String A05;
                public final /* synthetic */ boolean A06;

                {
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A06 = r7;
                    this.A03 = r4;
                    this.A01 = r2;
                    this.A04 = r5;
                    this.A05 = r6;
                }

                @Override // X.AbstractC30111Wd
                public final void ANP(C30141Wg r9) {
                    C19820uj r13 = this.A02;
                    UserJid userJid2 = this.A00;
                    boolean z2 = this.A06;
                    String str4 = this.A03;
                    AnonymousClass1WA r3 = this.A01;
                    String str5 = this.A04;
                    String str6 = this.A05;
                    C16700pc.A0F(r13, userJid2);
                    C16700pc.A0E(str4, 3);
                    C16700pc.A0E(str5, 5);
                    C16700pc.A0E(str6, 6);
                    if (r9 == null || !r9.A0J) {
                        r3.A00();
                    } else {
                        r13.A00.A08.A01(userJid2, r3, str4, str5, str6, z2);
                    }
                }
            }, userJid);
        }
    }
}
