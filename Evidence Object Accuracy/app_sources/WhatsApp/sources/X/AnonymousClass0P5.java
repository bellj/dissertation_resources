package X;

import java.util.Arrays;

/* renamed from: X.0P5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0P5 {
    public final int A00;
    public final CharSequence A01;

    public AnonymousClass0P5(int i, CharSequence charSequence) {
        this.A00 = i;
        this.A01 = charSequence;
    }

    public boolean equals(Object obj) {
        String str;
        String str2;
        if (obj instanceof AnonymousClass0P5) {
            AnonymousClass0P5 r5 = (AnonymousClass0P5) obj;
            if (this.A00 == r5.A00) {
                CharSequence charSequence = r5.A01;
                CharSequence charSequence2 = this.A01;
                if (charSequence2 != null) {
                    str = charSequence2.toString();
                } else {
                    str = null;
                }
                if (charSequence != null) {
                    str2 = charSequence.toString();
                } else {
                    str2 = null;
                }
                if (str == null) {
                    if (str2 == null) {
                        return true;
                    }
                } else if (str.equals(str2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int hashCode() {
        String str;
        Object[] objArr = new Object[2];
        objArr[0] = Integer.valueOf(this.A00);
        CharSequence charSequence = this.A01;
        if (charSequence != null) {
            str = charSequence.toString();
        } else {
            str = null;
        }
        objArr[1] = str;
        return Arrays.hashCode(objArr);
    }
}
