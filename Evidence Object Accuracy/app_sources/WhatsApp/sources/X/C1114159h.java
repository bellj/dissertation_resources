package X;

import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

/* renamed from: X.59h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1114159h implements AbstractC116265Ut {
    @Override // X.AbstractC116265Ut
    public Layout A8M(TextPaint textPaint, CharSequence charSequence, int i) {
        return new StaticLayout(charSequence, textPaint, i, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, true);
    }
}
