package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.4vW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106324vW implements AnonymousClass5WW {
    public final C89054Im A00;

    public /* synthetic */ C106324vW(C89054Im r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void A6O(Context context, Object obj, Object obj2, Object obj3) {
        this.A00.A00 = (View) obj;
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return false;
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
        this.A00.A00 = null;
    }
}
