package X;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0500000_I1;
import com.whatsapp.R;

/* renamed from: X.2cB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C52992cB extends FrameLayout implements AnonymousClass004 {
    public View A00;
    public View A01;
    public TextView A02;
    public TextView A03;
    public C238013b A04;
    public C19990v2 A05;
    public AnonymousClass2P7 A06;
    public boolean A07;

    public C52992cB(Context context) {
        super(context);
        if (!this.A07) {
            this.A07 = true;
            generatedComponent();
        }
        FrameLayout.inflate(context, R.layout.conversation_block_add_header, this);
        this.A00 = findViewById(R.id.content);
        this.A01 = findViewById(R.id.divider);
        this.A02 = C12960it.A0J(this, R.id.add_btn);
        this.A03 = C12960it.A0J(this, R.id.block_btn);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public void setup(ActivityC000800j r9, C19990v2 r10, AbstractC13860kS r11, C15450nH r12, C238013b r13, Runnable runnable, C15370n3 r15) {
        this.A05 = r10;
        this.A04 = r13;
        this.A03.setOnClickListener(new ViewOnClickCListenerShape1S0500000_I1(this, r15, r13, r11, r9, 0));
        C12960it.A0y(this.A02, runnable, 43);
    }
}
