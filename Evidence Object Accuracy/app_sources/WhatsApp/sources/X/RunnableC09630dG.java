package X;

/* renamed from: X.0dG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09630dG implements Runnable {
    public final /* synthetic */ C05930Rn A00;
    public final /* synthetic */ C004401z A01;

    public RunnableC09630dG(C05930Rn r1, C004401z r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        C06390Tk A00 = C06390Tk.A00();
        String str = C05930Rn.A03;
        C004401z r3 = this.A01;
        A00.A02(str, String.format("Scheduling work %s", r3.A0E), new Throwable[0]);
        this.A00.A01.AbJ(r3);
    }
}
