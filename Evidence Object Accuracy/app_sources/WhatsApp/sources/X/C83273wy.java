package X;

import android.view.animation.Animation;

/* renamed from: X.3wy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83273wy extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    public C83273wy(AbstractView$OnCreateContextMenuListenerC35851ir r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A0T.setVisibility(8);
    }
}
