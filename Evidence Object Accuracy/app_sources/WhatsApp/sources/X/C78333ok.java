package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78333ok extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99324k7();
    public final int A00;
    public final boolean A01;
    public final boolean A02;

    public C78333ok(int i, boolean z, boolean z2) {
        this.A00 = i;
        this.A01 = z;
        this.A02 = z2;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A09(parcel, 3, this.A01);
        C95654e8.A09(parcel, 4, this.A02);
        C95654e8.A06(parcel, A00);
    }
}
