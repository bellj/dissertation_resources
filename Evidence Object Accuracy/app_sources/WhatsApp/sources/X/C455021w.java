package X;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.PhotoViewPager;
import java.util.HashSet;

/* renamed from: X.21w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C455021w implements AnonymousClass21x {
    public float A00;
    public float A01;
    public C51432Us A02;
    public AnonymousClass21x A03;
    public AbstractC453121b A04;
    public final Context A05;
    public final RecyclerView A06;
    public final AnonymousClass018 A07;
    public final PhotoViewPager A08;
    public final AnonymousClass2BB A09;

    public C455021w(C006202y r19, RecyclerView recyclerView, AnonymousClass018 r21, AnonymousClass19M r22, C453421e r23, C457522x r24, PhotoViewPager photoViewPager, C453321d r26, AnonymousClass2BB r27, AnonymousClass1AB r28, C22190yg r29, HashSet hashSet, boolean z) {
        this.A06 = recyclerView;
        this.A08 = photoViewPager;
        this.A09 = r27;
        this.A07 = r21;
        Context context = recyclerView.getContext();
        this.A05 = context;
        this.A02 = new C51432Us(context, r19, r21, r22, r23, r24, r26, r27, this, r28, r29, hashSet, context.getResources().getDimensionPixelSize(z ? R.dimen.new_media_sharing_selector_gallery_picker_preview_thumb_size : R.dimen.gallery_picker_preview_thumb_size), z);
        recyclerView.setItemAnimator(null);
        recyclerView.A0h = true;
    }

    @Override // X.AnonymousClass21x
    public void AXU(int i) {
        AnonymousClass21x r0 = this.A03;
        if (r0 != null) {
            r0.AXU(i);
        }
    }
}
