package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2TQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2TQ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2TQ A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public int A01;
    public boolean A02;

    static {
        AnonymousClass2TQ r0 = new AnonymousClass2TQ();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        switch (r8.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass2TQ r10 = (AnonymousClass2TQ) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A01;
                int i3 = r10.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A01 = r9.Afp(i2, r10.A01, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                boolean z4 = this.A02;
                boolean z5 = false;
                if ((i3 & 2) == 2) {
                    z5 = true;
                }
                this.A02 = r9.Afl(z3, z4, z5, r10.A02);
                if (r9 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r92.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 120) {
                            int A02 = r92.A02();
                            if (A02 == 0 || A02 == 1 || A02 == 2 || A02 == 3 || A02 == 4) {
                                this.A00 = 1 | this.A00;
                                this.A01 = A02;
                            } else {
                                super.A0X(15, A02);
                            }
                        } else if (A032 == 128) {
                            this.A00 |= 2;
                            this.A02 = r92.A0F();
                        } else if (!A0a(r92, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AnonymousClass2TQ();
            case 5:
                return new AnonymousClass2TR();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (AnonymousClass2TQ.class) {
                        if (A04 == null) {
                            A04 = new AnonymousClass255(A03);
                        }
                    }
                }
                return A04;
            default:
                throw new UnsupportedOperationException();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(15, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A00(16);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(15, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0J(16, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
