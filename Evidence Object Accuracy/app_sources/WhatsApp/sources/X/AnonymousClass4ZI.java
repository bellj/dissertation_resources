package X;

/* renamed from: X.4ZI  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ZI {
    public static final AbstractC11610gZ A00;

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:15:0x000d */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0003 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: boolean */
    /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.String] */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        if (r0 != 0) goto L_0x0014;
     */
    static {
        /*
            java.lang.String r1 = "kotlinx.coroutines.main.delay"
            r0 = 1
            java.lang.String r0 = java.lang.System.getProperty(r1)     // Catch: SecurityException -> 0x000d
            if (r0 == 0) goto L_0x0014
            boolean r0 = java.lang.Boolean.parseBoolean(r0)
        L_0x000d:
            if (r0 != 0) goto L_0x0014
        L_0x000f:
            X.5Ks r1 = X.RunnableC114235Ks.A01
        L_0x0011:
            X.AnonymousClass4ZI.A00 = r1
            return
        L_0x0014:
            X.5Kk r1 = X.C88584Gf.A00
            X.5Kk r0 = r1.A05()
            boolean r0 = r0 instanceof X.AnonymousClass5L4
            if (r0 != 0) goto L_0x000f
            boolean r0 = r1 instanceof X.AbstractC11610gZ
            if (r0 == 0) goto L_0x000f
            X.0gZ r1 = (X.AbstractC11610gZ) r1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4ZI.<clinit>():void");
    }

    public static final AbstractC11610gZ A00() {
        return A00;
    }
}
