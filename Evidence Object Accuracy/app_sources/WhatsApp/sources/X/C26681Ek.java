package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;

/* renamed from: X.1Ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26681Ek {
    public final C22260yn A00;
    public final C18850tA A01;
    public final C15550nR A02;
    public final C19990v2 A03;
    public final C15650ng A04;
    public final AnonymousClass15K A05;
    public final C22230yk A06;

    public C26681Ek(C22260yn r1, C18850tA r2, C15550nR r3, C19990v2 r4, C15650ng r5, AnonymousClass15K r6, C22230yk r7) {
        this.A03 = r4;
        this.A01 = r2;
        this.A05 = r6;
        this.A02 = r3;
        this.A06 = r7;
        this.A00 = r1;
        this.A04 = r5;
    }

    public void A00(C15370n3 r7, AbstractC14640lm r8, boolean z, boolean z2) {
        if (r7.A0C == null || r7.A0K() || C15380n4.A0F(r7.A0D)) {
            this.A00.A01(new RunnableBRunnable0Shape0S0210000_I0(this, r7, 1, z));
        }
        if (z2) {
            this.A06.A04(r8, 2, 0, 0);
        }
    }
}
