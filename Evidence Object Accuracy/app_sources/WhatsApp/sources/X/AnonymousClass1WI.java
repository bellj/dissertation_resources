package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import java.io.Serializable;

/* renamed from: X.1WI  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1WI implements AnonymousClass1WJ, Serializable {
    public final int arity;

    public AnonymousClass1WI(int i) {
        this.arity = i;
    }

    public static void A00(C14900mE r1, Object obj, Object obj2, int i) {
        r1.A0H(new RunnableBRunnable0Shape11S0200000_I1_1(obj, i, obj2));
    }

    @Override // X.AnonymousClass1WJ
    public int AAk() {
        return this.arity;
    }

    @Override // java.lang.Object
    public String toString() {
        String obj = getClass().getGenericInterfaces()[0].toString();
        if (obj.startsWith("kotlin.jvm.functions.")) {
            obj = obj.substring(21);
        }
        C16700pc.A0B(obj);
        return obj;
    }
}
