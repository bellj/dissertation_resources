package X;

import java.util.Map;

/* renamed from: X.3nF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77413nF extends AbstractC64703Go {
    public final Map A00 = C12970iu.A11();

    @Override // X.AbstractC64703Go
    public final /* bridge */ /* synthetic */ void A02(AbstractC64703Go r3) {
        C77413nF r32 = (C77413nF) r3;
        C13020j0.A01(r32);
        r32.A00.putAll(this.A00);
    }

    public final String toString() {
        return AbstractC64703Go.A00(this.A00, 0);
    }
}
