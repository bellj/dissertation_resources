package X;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;

/* renamed from: X.0Fz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03070Fz extends AnonymousClass072 {
    public static final String[] A00 = {"android:clipBounds:clip"};

    public static final void A00(C05350Pf r4) {
        View view = r4.A00;
        if (view.getVisibility() != 8) {
            Rect A0A = AnonymousClass028.A0A(view);
            Map map = r4.A02;
            map.put("android:clipBounds:clip", A0A);
            if (A0A == null) {
                map.put("android:clipBounds:bounds", new Rect(0, 0, view.getWidth(), view.getHeight()));
            }
        }
    }

    @Override // X.AnonymousClass072
    public Animator A0Q(ViewGroup viewGroup, C05350Pf r12, C05350Pf r13) {
        ObjectAnimator objectAnimator = null;
        if (!(r12 == null || r13 == null)) {
            Map map = r12.A02;
            if (map.containsKey("android:clipBounds:clip")) {
                Map map2 = r13.A02;
                if (map2.containsKey("android:clipBounds:clip")) {
                    Rect rect = (Rect) map.get("android:clipBounds:clip");
                    Object obj = map2.get("android:clipBounds:clip");
                    boolean z = false;
                    if (obj == null) {
                        z = true;
                    }
                    if (!(rect == null && obj == null)) {
                        if (rect == null) {
                            rect = (Rect) map.get("android:clipBounds:bounds");
                        } else if (obj == null) {
                            obj = map2.get("android:clipBounds:bounds");
                        }
                        if (!rect.equals(obj)) {
                            AnonymousClass028.A0f(r13.A00, rect);
                            objectAnimator = ObjectAnimator.ofObject(r13.A00, AnonymousClass0U3.A02, new C06640Ul(new Rect()), rect, obj);
                            if (z) {
                                objectAnimator.addListener(new AnonymousClass096(r13.A00, this));
                                return objectAnimator;
                            }
                        }
                    }
                }
            }
        }
        return objectAnimator;
    }

    @Override // X.AnonymousClass072
    public String[] A0S() {
        return A00;
    }

    @Override // X.AnonymousClass072
    public void A0T(C05350Pf r1) {
        A00(r1);
    }

    @Override // X.AnonymousClass072
    public void A0U(C05350Pf r1) {
        A00(r1);
    }
}
