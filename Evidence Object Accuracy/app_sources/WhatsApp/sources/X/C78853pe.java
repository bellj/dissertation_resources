package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3pe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78853pe extends C98374ia implements IInterface {
    public C78853pe(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }
}
