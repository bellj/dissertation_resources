package X;

import android.content.Context;
import com.whatsapp.shareinvitelink.ShareInviteLinkActivity;

/* renamed from: X.4qs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103514qs implements AbstractC009204q {
    public final /* synthetic */ ShareInviteLinkActivity A00;

    public C103514qs(ShareInviteLinkActivity shareInviteLinkActivity) {
        this.A00 = shareInviteLinkActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
