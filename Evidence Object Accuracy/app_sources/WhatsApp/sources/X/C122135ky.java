package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.5ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122135ky extends AbstractC118815cQ {
    public final WaTextView A00;
    public final WaTextView A01;

    public C122135ky(View view) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.status_text);
        this.A00 = C12960it.A0N(view, R.id.order_description);
    }
}
