package X;

import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.RelativeSizeSpan;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;

/* renamed from: X.3MH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3MH implements TextWatcher {
    public String A00 = "";
    public boolean A01;
    public boolean A02;
    public final AbstractC116435Vk A03;
    public final AnonymousClass5TB A04;
    public final CodeInputField A05;

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public /* synthetic */ AnonymousClass3MH(AbstractC116435Vk r2, AnonymousClass5TB r3, CodeInputField codeInputField) {
        this.A03 = r2;
        this.A05 = codeInputField;
        this.A04 = r3;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        int length;
        String obj;
        int length2;
        SpannableStringBuilder spannableStringBuilder;
        AbstractC116435Vk r3;
        C52272aX r1;
        int i;
        CodeInputField codeInputField = this.A05;
        int selectionStart = codeInputField.getSelectionStart();
        String replace = editable.toString().replace(Character.toString(codeInputField.A01), "");
        if (!replace.isEmpty() && replace.charAt(0) != 160) {
            codeInputField.A05 = false;
        }
        int i2 = codeInputField.A02 >> 1;
        int length3 = replace.length();
        if (length3 > 0 && this.A00.startsWith(replace.substring(0, 1)) && this.A00.indexOf(160) >= 0 && replace.indexOf(160) < 0 && selectionStart == i2) {
            StringBuilder A0h = C12960it.A0h();
            A0h.append(replace.substring(0, i2 - 1));
            replace = C12960it.A0d(replace.substring(i2), A0h);
            selectionStart--;
        } else if (length3 > selectionStart && replace.indexOf(160) == selectionStart && selectionStart == i2 + 1) {
            selectionStart++;
        }
        String replace2 = replace.replace(Character.toString(160), "");
        int length4 = replace2.length();
        if (length4 > i2) {
            length4++;
        }
        while (true) {
            length = replace2.length();
            if (length >= i2) {
                break;
            }
            StringBuilder A0j = C12960it.A0j(replace2);
            A0j.append(codeInputField.A01);
            replace2 = A0j.toString();
        }
        StringBuilder A0h2 = C12960it.A0h();
        A0h2.append(replace2.substring(0, i2));
        A0h2.append((char) 160);
        A0h2.append(replace2.substring(i2, Math.min(codeInputField.A02, length)));
        while (true) {
            obj = A0h2.toString();
            length2 = obj.length();
            if (length2 >= codeInputField.A02 + 1) {
                break;
            }
            A0h2 = C12960it.A0j(obj);
            A0h2.append(codeInputField.A01);
        }
        if (codeInputField.A05) {
            spannableStringBuilder = C12990iw.A0J(obj);
            for (int i3 = 0; i3 < spannableStringBuilder.length(); i3++) {
                if (spannableStringBuilder.charAt(i3) == codeInputField.A01) {
                    i = i3 + 1;
                    spannableStringBuilder.setSpan(new RelativeSizeSpan(0.9f), i3, i, 33);
                    r1 = new C52272aX(codeInputField.getContext(), AnonymousClass00T.A00(codeInputField.getContext(), R.color.code_input_error));
                } else if (spannableStringBuilder.charAt(i3) != 160) {
                    r1 = new C52272aX(codeInputField.getContext(), AnonymousClass00T.A00(codeInputField.getContext(), R.color.code_input_text));
                    i = i3 + 1;
                }
                spannableStringBuilder.setSpan(r1, i3, i, 33);
            }
        } else {
            spannableStringBuilder = this.A04.AGq(obj);
        }
        if (spannableStringBuilder.length() > 0) {
            InputFilter[] filters = editable.getFilters();
            codeInputField.removeTextChangedListener(this);
            editable.setFilters(new InputFilter[0]);
            for (Object obj2 : editable.getSpans(0, editable.length(), CharacterStyle.class)) {
                editable.removeSpan(obj2);
            }
            editable.replace(0, editable.length(), spannableStringBuilder.toString());
            Object[] spans = spannableStringBuilder.getSpans(0, spannableStringBuilder.length(), CharacterStyle.class);
            for (Object obj3 : spans) {
                editable.setSpan(obj3, spannableStringBuilder.getSpanStart(obj3), spannableStringBuilder.getSpanEnd(obj3), 18);
            }
            editable.setFilters(filters);
            codeInputField.addTextChangedListener(this);
        }
        codeInputField.setSelection(Math.min(selectionStart, Math.min(length4, length2)));
        if (!this.A01 && (r3 = this.A03) != null) {
            String replaceAll = obj.toString().replaceAll("[^0-9]", "");
            if (replaceAll.length() != codeInputField.A02) {
                this.A02 = false;
                r3.AT5(replaceAll);
            } else if (!this.A02) {
                this.A02 = true;
                r3.AOH(replaceAll);
            }
        }
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (charSequence != null) {
            this.A00 = charSequence.toString();
        }
    }
}
