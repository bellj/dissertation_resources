package X;

import android.app.ActivityManager;
import java.util.concurrent.TimeUnit;

/* renamed from: X.4uo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105904uo implements AbstractC12220hZ {
    public static final long A01 = TimeUnit.MINUTES.toMillis(5);
    public final ActivityManager A00;

    public C105904uo(ActivityManager activityManager) {
        this.A00 = activityManager;
    }

    @Override // X.AbstractC12220hZ
    public /* bridge */ /* synthetic */ Object get() {
        int i;
        int min = Math.min(this.A00.getMemoryClass() * 1048576, Integer.MAX_VALUE);
        if (min < 33554432) {
            i = 4194304;
        } else {
            i = min >> 2;
            if (min < 67108864) {
                i = 6291456;
            }
        }
        return new C91754Sz(i, A01);
    }
}
