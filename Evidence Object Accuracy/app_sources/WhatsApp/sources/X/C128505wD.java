package X;

import java.util.Locale;

/* renamed from: X.5wD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128505wD {
    public final Boolean A00;

    public C128505wD(String str) {
        Boolean valueOf;
        String upperCase = str.toUpperCase(Locale.US);
        if ("UNKNOWN".equals(upperCase)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf("YES".equals(upperCase));
        }
        this.A00 = valueOf;
    }

    public String toString() {
        Boolean bool = this.A00;
        if (bool == null) {
            return "UNKNOWN";
        }
        return bool.booleanValue() ? "YES" : "NO";
    }
}
