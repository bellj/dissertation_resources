package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/* renamed from: X.0bQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08570bQ implements AbstractC12210hY {
    public final /* synthetic */ float A00 = 240.0f;

    @Override // X.AbstractC12210hY
    public Bitmap A89() {
        float f = this.A00;
        Paint paint = new Paint(1);
        float f2 = AnonymousClass03Q.A00 * 10.0f;
        int round = Math.round(2.0f * f2);
        int round2 = Math.round(3.0f * f2);
        Bitmap createBitmap = Bitmap.createBitmap(round + 10, round2 + 10, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(createBitmap);
        float[] fArr = new float[3];
        fArr[0] = f;
        fArr[1] = 1.0f;
        fArr[2] = 1.0f;
        int HSVToColor = Color.HSVToColor(fArr);
        fArr[2] = 0.8f;
        int HSVToColor2 = Color.HSVToColor(fArr);
        fArr[2] = 0.5f;
        int HSVToColor3 = Color.HSVToColor(fArr);
        paint.setColor(HSVToColor);
        float f3 = 5.0f + f2;
        float f4 = (float) (round2 + 5);
        AnonymousClass03Q.A02(canvas, paint, f3, f4, f2);
        paint.setColor(HSVToColor3);
        canvas.drawCircle(f3, f3, f2 / 2.5f, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(AnonymousClass03Q.A00);
        paint.setColor(HSVToColor2);
        AnonymousClass03Q.A02(canvas, paint, f3, f4, f2);
        return createBitmap;
    }
}
