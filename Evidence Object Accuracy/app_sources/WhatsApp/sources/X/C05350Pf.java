package X;

import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0Pf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05350Pf {
    public View A00;
    public final ArrayList A01 = new ArrayList();
    public final Map A02 = new HashMap();

    public boolean equals(Object obj) {
        if (obj instanceof C05350Pf) {
            C05350Pf r3 = (C05350Pf) obj;
            if (this.A00 == r3.A00 && this.A02.equals(r3.A02)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.A00.hashCode() * 31) + this.A02.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("TransitionValues@");
        sb.append(Integer.toHexString(hashCode()));
        sb.append(":\n");
        String obj = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(obj);
        sb2.append("    view = ");
        sb2.append(this.A00);
        sb2.append("\n");
        String obj2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(obj2);
        sb3.append("    values:");
        String obj3 = sb3.toString();
        Map map = this.A02;
        for (String str : map.keySet()) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(obj3);
            sb4.append("    ");
            sb4.append(str);
            sb4.append(": ");
            sb4.append(map.get(str));
            sb4.append("\n");
            obj3 = sb4.toString();
        }
        return obj3;
    }
}
