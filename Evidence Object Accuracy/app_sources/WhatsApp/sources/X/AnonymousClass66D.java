package X;

import com.facebook.redex.IDxCallableShape15S0100000_3_I1;

/* renamed from: X.66D  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66D implements AbstractC136186Li {
    public final /* synthetic */ AnonymousClass61Q A00;

    public AnonymousClass66D(AnonymousClass61Q r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136186Li
    public void AUG() {
        AnonymousClass61Q r3 = this.A00;
        r3.A0N.A07("handle_preview_started", new IDxCallableShape15S0100000_3_I1(r3, 16));
    }
}
