package X;

import java.util.Map;

/* renamed from: X.5KT  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KT extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public final /* synthetic */ C27661Io this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KT(C27661Io r2) {
        super(2);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass5ZQ
    public /* bridge */ /* synthetic */ Object AJ5(Object obj, Object obj2) {
        Map map = (Map) obj2;
        C16700pc.A0E(map, 1);
        C27661Io.A01(this.this$0, (String) obj, map, 2);
        return AnonymousClass1WZ.A00;
    }
}
