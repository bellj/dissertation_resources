package X;

import android.view.ViewTreeObserver;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;

/* renamed from: X.1vf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC42691vf implements ViewTreeObserver.OnGlobalLayoutListener {
    public int A00;
    public final Runnable A01 = new RunnableBRunnable0Shape5S0100000_I0_5(this, 1);
    public final /* synthetic */ AnonymousClass1PZ A02;

    public ViewTreeObserver$OnGlobalLayoutListenerC42691vf(AnonymousClass1PZ r3) {
        this.A02 = r3;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass1PZ r2 = this.A02;
        int width = r2.A01.getWidth();
        if (width != 0 && width != this.A00) {
            this.A00 = width;
            C14900mE r1 = r2.A0J;
            Runnable runnable = this.A01;
            r1.A0G(runnable);
            r1.A0H(runnable);
        }
    }
}
