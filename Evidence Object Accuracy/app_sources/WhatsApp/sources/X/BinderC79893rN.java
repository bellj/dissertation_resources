package X;

import android.os.IInterface;

/* renamed from: X.3rN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class BinderC79893rN extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115725Sr A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC79893rN(AbstractC115725Sr r2) {
        super("com.google.android.gms.maps.internal.IOnMapClickListener");
        this.A00 = r2;
    }
}
