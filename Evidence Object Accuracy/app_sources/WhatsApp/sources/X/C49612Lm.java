package X;

import android.view.animation.Animation;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Lm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49612Lm extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ HomeActivity A00;

    public C49612Lm(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        HomeActivity homeActivity = this.A00;
        homeActivity.A0H.setIconified(true);
        homeActivity.A0D.setVisibility(4);
    }
}
