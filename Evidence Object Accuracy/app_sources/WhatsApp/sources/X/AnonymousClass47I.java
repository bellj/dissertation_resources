package X;

/* renamed from: X.47I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47I extends AnonymousClass4OU {
    public final String A00;

    public AnonymousClass47I(int i, String str, String str2) {
        super(i, str2);
        this.A00 = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((AnonymousClass47I) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
