package X;

/* renamed from: X.4ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93774ai {
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final float A04;
    public final float A05;
    public final float A06;
    public final float A07;
    public final float A08;

    public C93774ai(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        this.A00 = f;
        this.A01 = f4;
        this.A02 = f7;
        this.A03 = f2;
        this.A04 = f5;
        this.A05 = f8;
        this.A06 = f3;
        this.A07 = f6;
        this.A08 = f9;
    }

    public static C93774ai A00(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        float f9;
        float f10;
        float f11;
        float f12;
        float f13;
        float f14;
        float f15 = ((f - f3) + f5) - f7;
        float f16 = ((f2 - f4) + f6) - f8;
        float f17 = 0.0f;
        if (f15 == 0.0f && f16 == 0.0f) {
            f10 = f3 - f;
            f11 = f5 - f3;
            f12 = f4 - f2;
            f13 = f6 - f4;
            f14 = 1.0f;
            f9 = 0.0f;
        } else {
            float f18 = f3 - f5;
            float f19 = f7 - f5;
            float f20 = f4 - f6;
            float f21 = f8 - f6;
            float f22 = (f18 * f21) - (f19 * f20);
            f17 = ((f21 * f15) - (f19 * f16)) / f22;
            f9 = ((f18 * f16) - (f15 * f20)) / f22;
            f10 = (f17 * f3) + (f3 - f);
            f11 = (f9 * f7) + (f7 - f);
            f12 = (f4 - f2) + (f17 * f4);
            f13 = (f8 - f2) + (f9 * f8);
            f14 = 1.0f;
        }
        return new C93774ai(f10, f11, f, f12, f13, f2, f17, f9, f14);
    }
}
