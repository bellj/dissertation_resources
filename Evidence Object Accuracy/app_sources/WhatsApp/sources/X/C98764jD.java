package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98764jD implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        C56412kq r3 = null;
        int[] iArr = null;
        int[] iArr2 = null;
        boolean z = false;
        boolean z2 = false;
        int i = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    r3 = (C56412kq) C95664e9.A07(parcel, C56412kq.CREATOR, readInt);
                    break;
                case 2:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 3:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 4:
                    iArr = C95664e9.A0J(parcel, readInt);
                    break;
                case 5:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 6:
                    iArr2 = C95664e9.A0J(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C56422kr(r3, iArr, iArr2, i, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C56422kr[i];
    }
}
