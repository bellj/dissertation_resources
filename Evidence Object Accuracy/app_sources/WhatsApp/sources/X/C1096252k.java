package X;

import android.app.Activity;

/* renamed from: X.52k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C1096252k implements AnonymousClass2GV {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Activity A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ C1096252k(Activity activity, String str, int i) {
        this.A01 = activity;
        this.A02 = str;
        this.A00 = i;
    }

    @Override // X.AnonymousClass2GV
    public final void AO1() {
        Activity activity = this.A01;
        activity.startActivity(C14960mK.A0W(activity.getApplicationContext(), this.A02, this.A00));
    }
}
