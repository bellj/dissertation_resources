package X;

import java.text.BreakIterator;
import java.util.Locale;

/* renamed from: X.5HI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HI extends ThreadLocal {
    @Override // java.lang.ThreadLocal
    public Object initialValue() {
        return BreakIterator.getWordInstance((Locale) C32751cg.A01.get());
    }
}
