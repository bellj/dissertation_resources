package X;

import android.util.SparseBooleanArray;

/* renamed from: X.59i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1114259i implements AnonymousClass5WK {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C54472gm A01;

    @Override // X.AnonymousClass5WK
    public void ARw() {
    }

    public C1114259i(C54472gm r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass5WK
    public void AXT() {
        C91014Qc r0 = this.A01.A04;
        int i = this.A00;
        SparseBooleanArray sparseBooleanArray = r0.A00;
        if (sparseBooleanArray != null) {
            sparseBooleanArray.put(i, true);
        }
    }
}
