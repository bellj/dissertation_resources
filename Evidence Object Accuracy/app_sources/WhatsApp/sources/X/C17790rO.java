package X;

import java.util.Map;

/* renamed from: X.0rO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17790rO extends AbstractC17440qo {
    public final C17780rN A00;

    public C17790rO(C17780rN r4) {
        super("wa.action.GalaxyInit");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r6, C1093651k r7, C14240l5 r8) {
        C14230l4 r82 = (C14230l4) r8;
        if (r7.A00.equals("wa.action.GalaxyInit")) {
            this.A00.A00(AnonymousClass1AI.A00(r82), new C90904Pr(r82, r6, this), (Map) r6.A00.get(0));
        }
        return null;
    }
}
