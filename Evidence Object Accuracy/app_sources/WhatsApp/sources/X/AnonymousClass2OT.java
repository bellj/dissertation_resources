package X;

/* renamed from: X.2OT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OT extends AnonymousClass1JW {
    public final String A00;
    public final String A01;
    public final String A02;

    public AnonymousClass2OT(AbstractC15710nm r2, C15450nH r3, int i) {
        super(r2, r3);
        this.A05 = 5;
        ((AnonymousClass1JX) this).A00 = i;
        this.A00 = null;
        this.A01 = null;
        this.A02 = null;
    }

    public AnonymousClass2OT(AbstractC15710nm r2, C15450nH r3, String str, String str2, String str3, byte[] bArr) {
        super(r2, r3);
        this.A05 = 5;
        ((AnonymousClass1JX) this).A00 = 200;
        this.A02 = str;
        this.A0U = bArr;
        this.A00 = str2;
        this.A01 = str3;
    }
}
