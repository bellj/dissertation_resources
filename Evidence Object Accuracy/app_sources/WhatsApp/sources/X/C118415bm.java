package X;

import com.whatsapp.payments.ui.MerchantPayoutTransactionHistoryActivity;

/* renamed from: X.5bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118415bm extends AnonymousClass0Yo {
    public final /* synthetic */ MerchantPayoutTransactionHistoryActivity A00;
    public final /* synthetic */ C129395xe A01;

    public C118415bm(MerchantPayoutTransactionHistoryActivity merchantPayoutTransactionHistoryActivity, C129395xe r2) {
        this.A01 = r2;
        this.A00 = merchantPayoutTransactionHistoryActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117935b0.class)) {
            MerchantPayoutTransactionHistoryActivity merchantPayoutTransactionHistoryActivity = this.A00;
            C129395xe r0 = this.A01;
            C14830m7 r2 = r0.A07;
            AbstractC14440lR r7 = r0.A0Q;
            return new C117935b0(merchantPayoutTransactionHistoryActivity, r2, r0.A09, r0.A0C, r0.A0O, r0.A0P, r7);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
