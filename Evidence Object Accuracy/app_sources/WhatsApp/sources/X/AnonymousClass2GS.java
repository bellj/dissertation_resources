package X;

import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ScrollView;
import com.whatsapp.status.playback.widget.StatusEditText;
import com.whatsapp.util.Log;

/* renamed from: X.2GS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GS {
    public int A00;
    public C37071lG A01;
    public AnonymousClass2GR A02 = null;
    public String A03;
    public boolean A04;
    public final ViewGroup A05;
    public final ImageButton A06;
    public final ScrollView A07;
    public final AnonymousClass016 A08;
    public final C252918v A09;
    public final C14310lE A0A;
    public final AnonymousClass01d A0B;
    public final AnonymousClass19M A0C;
    public final C14850m9 A0D;
    public final C245115u A0E;
    public final C16630pM A0F;
    public final StatusEditText A0G;
    public final C63893Di A0H;
    public final boolean A0I;

    public AnonymousClass2GS(ViewGroup viewGroup, ImageButton imageButton, ScrollView scrollView, AnonymousClass016 r5, C252918v r6, C14310lE r7, AnonymousClass01d r8, AnonymousClass19M r9, C14850m9 r10, C245115u r11, C16630pM r12, StatusEditText statusEditText, C63893Di r14, boolean z) {
        this.A0D = r10;
        this.A0I = z;
        this.A05 = viewGroup;
        this.A0G = statusEditText;
        this.A0H = r14;
        this.A0E = r11;
        this.A0A = r7;
        this.A06 = imageButton;
        this.A08 = r5;
        this.A0C = r9;
        this.A0B = r8;
        this.A0F = r12;
        this.A09 = r6;
        this.A07 = scrollView;
    }

    public static final int A00(CharSequence charSequence, int i, int i2) {
        int i3 = 0;
        if (charSequence == null) {
            Log.e("textstatus/linecount/str-null");
        } else {
            int length = charSequence.length();
            if (i < 0 || i2 > length || i > i2) {
                throw new IndexOutOfBoundsException();
            }
            while (i < i2) {
                if (charSequence.charAt(i) == '\n') {
                    i3++;
                }
                i++;
            }
        }
        return i3;
    }
}
