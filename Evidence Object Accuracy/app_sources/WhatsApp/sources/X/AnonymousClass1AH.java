package X;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Intent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1AH  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1AH {
    void A6B(AbstractC28681Oo v, AbstractC28681Oo v2, Object obj, String str, HashMap hashMap);

    void A6C(AbstractC28681Oo v, AbstractC28681Oo v2, Object obj, String str, HashMap hashMap);

    void A8q(HashMap hashMap);

    void A90(Activity activity, AbstractC28681Oo v);

    String AAU(Activity activity, String str, int i);

    Map AAx();

    ClipboardManager ABR();

    long ACC();

    File ACq(String str);

    void AKO(Activity activity, String str, String str2);

    void AYc(Activity activity, AbstractC115815Ta v);

    void AYd(Activity activity, String str);

    void AYe(Activity activity, String str);

    void AYf(Activity activity, String str, String str2);

    void AYg(Activity activity, String str);

    void AYi(Activity activity, String str, String str2, List list);

    void AYj(Activity activity, String str, String str2);

    void AYm(Activity activity, AbstractC115815Ta v, String str, String str2);

    void AZL(Activity activity);

    void AZf(Activity activity, AbstractC115815Ta v, boolean z);

    void Aa0(C90184Mx v, HashMap hashMap);

    void Aaa(Activity activity, AbstractC115825Tb v, String[] strArr);

    void Abc(String str, ArrayList arrayList, HashMap hashMap, int i, int i2);

    void AcL(Activity activity, AbstractC115815Ta v);

    void AdI(Activity activity, String str);

    void Adx(Activity activity, ProgressDialog progressDialog, String str, boolean z);

    void Ae8(Activity activity, Intent intent, AnonymousClass3CY v, int i);

    void Aey(String str);
}
