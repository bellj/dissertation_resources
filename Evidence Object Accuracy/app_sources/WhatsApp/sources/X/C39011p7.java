package X;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Build;
import android.text.TextUtils;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.Mp4Ops;
import com.whatsapp.VideoFrameConverter;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.Locale;

/* renamed from: X.1p7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39011p7 implements AbstractC39001p6 {
    public static int A0L;
    public static String A0M;
    public static final byte[] A0N = {102, 116, 121, 112};
    public static final int[] A0O = {19, 20, 21, 39, 2141391872, 2130706688, 25, 2141391876, 2130706433, 2141391875, 2141391873, 11, 2130706944};
    public float A00 = 3.0f;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public C64653Gj A05;
    public AnonymousClass5UN A06;
    public C93804al A07;
    public AnonymousClass3JD A08;
    public File A09;
    public File A0A;
    public File A0B;
    public byte[] A0C;
    public final int A0D;
    public final int A0E;
    public final AbstractC15710nm A0F;
    public final C16590pI A0G;
    public final C14850m9 A0H;
    public final C239713s A0I;
    public final File A0J;
    public volatile boolean A0K;

    public static int A00(int i) {
        switch (i) {
            case 11:
                return 7;
            case 21:
            case 39:
            case 2130706688:
            case 2141391873:
            case 2141391876:
                return 3;
            case 2130706433:
                return 6;
            case 2130706944:
                return 2;
            case 2141391872:
                return 4;
            case 2141391875:
                return 5;
            default:
                return 1;
        }
    }

    public C39011p7(AbstractC15710nm r4, C16590pI r5, C14850m9 r6, C239713s r7, File file, File file2, int i, int i2, long j, long j2) {
        this.A0G = r5;
        this.A0H = r6;
        this.A0F = r4;
        this.A0I = r7;
        this.A0A = file;
        this.A0J = file2;
        this.A01 = j;
        this.A02 = j2;
        this.A0E = i;
        this.A0D = i2;
        if (j2 > 0 && j == j2) {
            StringBuilder sb = new StringBuilder("timeFrom:");
            sb.append(j);
            sb.append(" timeTo:");
            sb.append(j2);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public static int A01(MediaCodecInfo mediaCodecInfo) {
        StringBuilder sb;
        MediaCodecInfo.CodecCapabilities capabilitiesForType = mediaCodecInfo.getCapabilitiesForType("video/avc");
        StringBuilder sb2 = new StringBuilder("videotranscoder/transcode/color formats: ");
        sb2.append(capabilitiesForType.colorFormats.length);
        Log.i(sb2.toString());
        int i = 0;
        int i2 = 0;
        while (true) {
            int[] iArr = capabilitiesForType.colorFormats;
            if (i < iArr.length && i2 == 0) {
                int i3 = iArr[i];
                if (!(i3 == 39 || i3 == 2130706688)) {
                    switch (i3) {
                        case 19:
                        case C43951xu.A01 /* 20 */:
                        case 21:
                            break;
                        default:
                            sb = new StringBuilder("videotranscoder/transcode/skipping unsupported color format ");
                            sb.append(i3);
                            Log.i(sb.toString());
                            break;
                    }
                    i++;
                }
                if (!"OMX.SEC.avc.enc".equals(mediaCodecInfo.getName()) || i3 != 19) {
                    i2 = i3;
                    i++;
                } else {
                    sb = new StringBuilder("videotranscoder/transcode/skipping ");
                    sb.append(i3);
                    sb.append(" for OMX.SEC.avc.enc");
                    Log.i(sb.toString());
                    i++;
                }
            }
        }
        return i2;
    }

    public static synchronized int A02(boolean z) {
        int i;
        String str;
        synchronized (C39011p7.class) {
            i = A0L;
            if (i == 0) {
                i = 1;
                String str2 = null;
                if (!A08()) {
                    i = 2;
                    StringBuilder sb = new StringBuilder();
                    sb.append("videotranscoder/istranscodesupported/unsupported model ");
                    sb.append(Build.MANUFACTURER);
                    sb.append("-");
                    sb.append(Build.MODEL);
                    str = sb.toString();
                } else {
                    int codecCount = MediaCodecList.getCodecCount();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("videotranscoder/istranscodesupported/number of codecs: ");
                    sb2.append(codecCount);
                    Log.i(sb2.toString());
                    int i2 = 0;
                    boolean z2 = false;
                    while (true) {
                        if (i2 < codecCount) {
                            if (z2) {
                                break;
                            }
                            MediaCodecInfo codecInfoAt = MediaCodecList.getCodecInfoAt(i2);
                            if (codecInfoAt.isEncoder() && A0A(codecInfoAt.getName(), z)) {
                                String[] supportedTypes = codecInfoAt.getSupportedTypes();
                                int i3 = 0;
                                while (true) {
                                    if (i3 < supportedTypes.length) {
                                        if (z2) {
                                            break;
                                        }
                                        if (supportedTypes[i3].equals("video/avc")) {
                                            z2 = true;
                                        }
                                        i3++;
                                    } else if (!z2) {
                                    }
                                }
                                str2 = codecInfoAt.getName();
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("videotranscoder/istranscodesupported/found ");
                                sb3.append(codecInfoAt.getName());
                                Log.i(sb3.toString());
                            }
                            i2++;
                        } else if (!z2) {
                            i = 3;
                            str = "videotranscoder/istranscodesupported/no encoder found";
                        }
                    }
                    A07(str2);
                    A0L = i;
                }
                Log.w(str);
                A07(str2);
                A0L = i;
            }
        }
        return i;
    }

    public static MediaCodecInfo A03(boolean z) {
        String name;
        int codecCount = MediaCodecList.getCodecCount();
        StringBuilder sb = new StringBuilder("videotranscoder/transcode/number of codecs: ");
        sb.append(codecCount);
        Log.i(sb.toString());
        MediaCodecInfo mediaCodecInfo = null;
        MediaCodecInfo mediaCodecInfo2 = null;
        int i = 0;
        while (true) {
            if (i < codecCount) {
                if (mediaCodecInfo != null) {
                    break;
                }
                MediaCodecInfo codecInfoAt = MediaCodecList.getCodecInfoAt(i);
                if (codecInfoAt.isEncoder()) {
                    if (A0A(codecInfoAt.getName(), false)) {
                        String[] supportedTypes = codecInfoAt.getSupportedTypes();
                        int i2 = 0;
                        boolean z2 = false;
                        while (true) {
                            if (i2 < supportedTypes.length) {
                                if (z2) {
                                    break;
                                }
                                if (supportedTypes[i2].equals("video/avc")) {
                                    z2 = true;
                                }
                                i2++;
                            } else if (!z2) {
                            }
                        }
                        mediaCodecInfo = codecInfoAt;
                    } else if (z && (name = codecInfoAt.getName()) != null && name.equals("OMX.google.h264.encoder")) {
                        String[] supportedTypes2 = codecInfoAt.getSupportedTypes();
                        int length = supportedTypes2.length;
                        int i3 = 0;
                        while (true) {
                            if (i3 >= length) {
                                break;
                            } else if (supportedTypes2[i3].equals("video/avc")) {
                                mediaCodecInfo2 = codecInfoAt;
                                break;
                            } else {
                                i3++;
                            }
                        }
                    }
                }
                i++;
            } else if (mediaCodecInfo == null) {
                return mediaCodecInfo2;
            }
        }
        return mediaCodecInfo;
    }

    public static C93804al A04(MediaFormat mediaFormat, C64653Gj r4, String str) {
        int i;
        String str2;
        String str3;
        int i2;
        StringBuilder sb = new StringBuilder("videotranscoder/transcode/getDecoderFormat output format has changed to ");
        sb.append(mediaFormat);
        Log.i(sb.toString());
        C93804al r2 = new C93804al();
        r2.A0A = str;
        r2.A00 = mediaFormat.getInteger("color-format");
        r2.A09 = mediaFormat.getInteger("width");
        r2.A06 = mediaFormat.getInteger("height");
        try {
            r2.A02 = mediaFormat.getInteger("crop-left");
        } catch (Exception unused) {
        }
        try {
            r2.A03 = mediaFormat.getInteger("crop-right");
        } catch (Exception unused2) {
        }
        try {
            r2.A04 = mediaFormat.getInteger("crop-top");
        } catch (Exception unused3) {
        }
        try {
            r2.A01 = mediaFormat.getInteger("crop-bottom");
        } catch (Exception unused4) {
        }
        try {
            r2.A07 = mediaFormat.getInteger("slice-height");
        } catch (Exception unused5) {
        }
        if (str.startsWith("OMX.Nvidia.")) {
            r2.A07 = ((r2.A06 + 16) - 1) & -16;
        } else if (str.equalsIgnoreCase("OMX.SEC.avc.dec")) {
            r2.A07 = r2.A06;
            r2.A08 = r2.A09;
        }
        try {
            r2.A08 = mediaFormat.getInteger("stride");
        } catch (Exception unused6) {
        }
        if (Build.VERSION.SDK_INT < 21 && r2.A01 == 1079 && r2.A06 == 1088 && A09(str)) {
            Log.i("videotranscoder/transcode/decoder workaround samsung incorrect height");
            r2.A06 = 1080;
        }
        A07(str);
        if (r4 != null) {
            int i3 = r2.A00;
            String str4 = r4.A04;
            if (str4 != null && i3 > 0 && (i2 = r4.A00) > 0 && str4.equals(str) && i2 == i3) {
                StringBuilder sb2 = new StringBuilder("videotranscoder/parseDecoderFormat/forcing frame convert color id=");
                i = r4.A02;
                sb2.append(i);
                str3 = sb2.toString();
                Log.i(str3);
                r2.A05 = i;
                return r2;
            }
        }
        int i4 = r2.A00;
        r2.A05 = A00(i4);
        i = 3;
        if (i4 != 25) {
            if (i4 != 2141391876) {
                if (i4 == 2130706433 && ((str2 = A0M) == null || !str2.toLowerCase(Locale.US).startsWith("mt6589"))) {
                    r2.A05 = 1;
                    return r2;
                }
            }
            r2.A05 = i;
        } else if ("OMX.k3.video.encoder.avc".equals(str) || "OMX.k3.video.decoder.avc".equals(str)) {
            str3 = "videotranscoder/transcode/decoder color format for Huaiwei is VideoFrameConverter.FRAMECONV_COLOR_FORMAT_NV12";
            Log.i(str3);
            r2.A05 = i;
        }
        return r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0179, code lost:
        if (r3 != 17) goto L_0x0193;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C93804al A05(X.C64653Gj r11, java.lang.String r12, int r13, int r14, int r15, int r16, int r17, int r18) {
        /*
        // Method dump skipped, instructions count: 486
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39011p7.A05(X.3Gj, java.lang.String, int, int, int, int, int, int):X.4al");
    }

    public static String A06(String str) {
        String str2 = "";
        try {
            Process start = new ProcessBuilder("/system/bin/getprop", str).redirectErrorStream(true).start();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(start.getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    str2 = readLine;
                } else {
                    bufferedReader.close();
                    start.destroy();
                    return str2;
                }
            }
        } catch (IOException e) {
            Log.w("getsystemproperty/", e);
            return str2;
        }
    }

    public static void A07(String str) {
        if (A0M == null && "OMX.MTK.VIDEO.ENCODER.AVC".equals(str)) {
            String A06 = A06("ro.board.platform");
            A0M = A06;
            if (TextUtils.isEmpty(A06)) {
                A0M = A06("ro.mediatek.platform");
            }
            StringBuilder sb = new StringBuilder("videotranscoder/setHwBoardPlatform/board/");
            sb.append(A0M);
            Log.i(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0081, code lost:
        if (r0 != false) goto L_0x0083;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A08() {
        /*
        // Method dump skipped, instructions count: 245
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39011p7.A08():boolean");
    }

    public static boolean A09(String str) {
        return "OMX.SEC.avc.enc".equals(str) || "OMX.SEC.avc.dec".equals(str) || "OMX.Exynos.AVC.Encoder".equals(str) || "OMX.Exynos.AVC.Decoder".equals(str);
    }

    public static boolean A0A(String str, boolean z) {
        String obj;
        if (str == null || ((!z && str.equals("OMX.google.h264.encoder")) || str.equals("OMX.ST.VFM.H264Enc") || str.equals("OMX.Exynos.avc.enc") || ((str.equals("OMX.MARVELL.VIDEO.HW.CODA7542ENCODER") && Build.VERSION.SDK_INT < 21) || str.equals("OMX.MARVELL.VIDEO.H264ENCODER")))) {
            return false;
        }
        if (str.equals("OMX.MTK.VIDEO.ENCODER.AVC") && "QMobile".equalsIgnoreCase(Build.MANUFACTURER) && Build.VERSION.SDK_INT < 23) {
            obj = "videotranscoder/ OMX.MTK.VIDEO.ENCODER.AVC on QMobile is not supported";
        } else if (!str.equals("OMX.allwinner.video.encoder.avc") && !str.equals("AVCEncoder")) {
            return true;
        } else {
            StringBuilder sb = new StringBuilder("videotranscoder/ ");
            sb.append(str);
            sb.append(" not supported");
            obj = sb.toString();
        }
        Log.i(obj);
        return false;
    }

    public static boolean A0B(String str, String[] strArr) {
        if (strArr != null) {
            Locale locale = Locale.US;
            String lowerCase = str.toLowerCase(locale);
            for (String str2 : strArr) {
                if (lowerCase.startsWith(str2.toLowerCase(locale))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int[] A0C(String str, int i) {
        if (i <= 0 && (str.equals("OMX.MTK.VIDEO.ENCODER.AVC") || str.equals("OMX.MTK.VIDEO.DECODER.AVC"))) {
            i = 2130706944;
        }
        int[] iArr = A0O;
        if (i <= 0) {
            return iArr;
        }
        int length = iArr.length;
        int[] iArr2 = new int[length];
        iArr2[0] = i;
        for (int i2 = 1; i2 < length; i2++) {
            int i3 = iArr[i2 - 1];
            iArr2[i2] = i3;
            if (i3 == i) {
                iArr2[i2] = 0;
            }
        }
        return iArr2;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:415:0x1015 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r45v0, types: [android.media.MediaCodec] */
    /* JADX WARN: Type inference failed for: r8v7 */
    /* JADX WARN: Type inference failed for: r8v8, types: [boolean] */
    /* JADX WARN: Type inference failed for: r8v9 */
    /* JADX WARN: Type inference failed for: r8v10, types: [boolean] */
    /* JADX WARN: Type inference failed for: r8v11 */
    /* JADX WARN: Type inference failed for: r8v29 */
    /* JADX WARN: Type inference failed for: r8v30 */
    /* JADX WARN: Type inference failed for: r8v31 */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A0D() {
        /*
        // Method dump skipped, instructions count: 5373
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39011p7.A0D():void");
    }

    public void A0E() {
        try {
            File file = this.A0A;
            File file2 = this.A0J;
            long j = this.A01;
            long j2 = this.A02;
            StringBuilder sb = new StringBuilder("mp4ops/trim/start from ");
            sb.append(j);
            sb.append(" to ");
            sb.append(j2);
            sb.append(" size:");
            sb.append(file.length());
            Log.i(sb.toString());
            if ((j > 0 || j2 > 0) && j != j2) {
                float f = ((float) j) / 1000.0f;
                try {
                    Mp4Ops.LibMp4OperationResult mp4mux = Mp4Ops.mp4mux(file.getAbsolutePath(), file.getAbsolutePath(), file2.getAbsolutePath(), -1.0f, f, ((float) (j2 - j)) / 1000.0f, -1.0f, -1, file.getAbsolutePath(), f);
                    StringBuilder sb2 = new StringBuilder("mp4ops/trim/result: ");
                    sb2.append(mp4mux.success);
                    Log.i(sb2.toString());
                    if (!mp4mux.success) {
                        StringBuilder sb3 = new StringBuilder("mp4ops/trim/error_message/");
                        sb3.append(mp4mux.errorMessage);
                        Log.e(sb3.toString());
                        if (mp4mux.ioException) {
                            throw new IOException("No space");
                        }
                        StringBuilder sb4 = new StringBuilder("invalid result, error_code: ");
                        int i = mp4mux.errorCode;
                        sb4.append(i);
                        throw new C39361pl(i, sb4.toString());
                    }
                    Log.i("mp4ops/trim/finished: size");
                    Mp4Ops.A04(file2, true);
                } catch (Error e) {
                    Log.e("mp4ops/trim/failed: mp4mux error, exiting", e);
                    throw new C39361pl(0, e.getMessage());
                }
            } else {
                StringBuilder sb5 = new StringBuilder("timeFrom:");
                sb5.append(j);
                sb5.append(" timeTo:");
                sb5.append(j2);
                throw new IllegalArgumentException(sb5.toString());
            }
        } catch (C39361pl e2) {
            Log.e("videotranscodequeue/libmp4muxexception", e2);
            Mp4Ops.A00(this.A0G.A00, this.A0F, this.A0A, e2, "trim");
            throw e2;
        }
    }

    public void A0F(MediaCodec mediaCodec, VideoFrameConverter videoFrameConverter, C38911ou r14, ByteBuffer byteBuffer, ByteBuffer[] byteBufferArr, int i, int i2, long j) {
        StringBuilder sb = new StringBuilder("videotranscoder/handleLastFrame/");
        sb.append(i);
        Log.i(sb.toString());
        int dequeueInputBuffer = mediaCodec.dequeueInputBuffer(SearchActionVerificationClientService.MS_TO_NS);
        ByteBuffer byteBuffer2 = byteBufferArr[dequeueInputBuffer];
        byteBuffer2.clear();
        VideoFrameConverter.convertFrame(videoFrameConverter.A00, byteBuffer, byteBuffer2);
        int i3 = r14.A01.getFrameDurations()[i];
        if (i3 < 70) {
            i3 = 70;
        }
        long j2 = j + ((long) (i3 * 1000));
        mediaCodec.queueInputBuffer(dequeueInputBuffer, 0, i2, j2, 4);
        this.A03++;
        this.A04 = j2 - (this.A01 * 1000);
    }

    @Override // X.AbstractC39001p6
    public boolean AII() {
        return this.A0B != null;
    }

    @Override // X.AbstractC39001p6
    public void cancel() {
        this.A0K = true;
    }
}
