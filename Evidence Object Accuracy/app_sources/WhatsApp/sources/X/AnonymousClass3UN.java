package X;

import android.app.Activity;
import android.content.SharedPreferences;

/* renamed from: X.3UN  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3UN implements AnonymousClass5TF {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Activity A01;
    public final /* synthetic */ C14820m6 A02;
    public final /* synthetic */ boolean A03;

    public /* synthetic */ AnonymousClass3UN(Activity activity, C14820m6 r2, int i, boolean z) {
        this.A01 = activity;
        this.A00 = i;
        this.A02 = r2;
        this.A03 = z;
    }

    @Override // X.AnonymousClass5TF
    public final void AUr() {
        String str;
        Activity activity = this.A01;
        int i = this.A00;
        C14820m6 r1 = this.A02;
        boolean z = this.A03;
        C36021jC.A00(activity, i);
        SharedPreferences sharedPreferences = r1.A00;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (z) {
            str = "pref_revoke_sender_nux_v2";
        } else {
            str = "pref_revoke_sender_nux";
        }
        C12960it.A0t(edit, str, false);
        C12960it.A0t(sharedPreferences.edit(), "pref_revoke_admin_nux", false);
    }
}
