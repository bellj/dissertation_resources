package X;

import com.whatsapp.authentication.AppAuthSettingsActivity;
import com.whatsapp.util.Log;
import java.security.Signature;

/* renamed from: X.2rA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC58672rA extends AnonymousClass4UT {
    public void A01() {
    }

    public void A02() {
    }

    public void A05(Signature signature) {
    }

    public void A03(int i) {
        if (this instanceof C58662r9) {
            Log.i("AppAuthSettingsActivity/fingerprint-error");
            ((C58662r9) this).A00.A2e();
        }
    }

    public void A04(AnonymousClass02N r3, AnonymousClass21K r4) {
        Log.i("AppAuthSettingsActivity/authenticate");
        C22670zS r1 = ((ActivityC13790kL) ((C58662r9) this).A00).A03;
        AnonymousClass009.A0F(r1.A04());
        r1.A01.A6I(r3, r4);
    }

    public void A06(byte[] bArr) {
        if (this instanceof C58662r9) {
            Log.i("AppAuthSettingsActivity/fingerprint-success");
            AppAuthSettingsActivity appAuthSettingsActivity = ((C58662r9) this).A00;
            ((ActivityC13810kN) appAuthSettingsActivity).A09.A17(true);
            ((ActivityC13790kL) appAuthSettingsActivity).A03.A01(false);
            appAuthSettingsActivity.A0C.A07();
            appAuthSettingsActivity.A09.A01();
        }
    }
}
