package X;

import android.view.View;

/* renamed from: X.0Qp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05700Qp {
    public static int A00(View view) {
        return view.getImportantForAutofill();
    }

    public static void A01(View view, int i) {
        view.setImportantForAutofill(i);
    }
}
