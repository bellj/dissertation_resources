package X;

import android.app.ActivityManager;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* renamed from: X.16A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16A extends AbstractC15710nm {
    public AbstractC20260vT A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final AnonymousClass125 A02;
    public final C15570nT A03;
    public final C18790t3 A04;
    public final C18640sm A05;
    public final AnonymousClass01d A06;
    public final C16590pI A07;
    public final C14820m6 A08;
    public final C16120oU A09;
    public final C21780xy A0A;
    public final C18800t4 A0B;
    public final C22650zQ A0C;
    public final C19930uu A0D;
    public final AbstractC14440lR A0E;

    public AnonymousClass16A(AnonymousClass125 r3, C15570nT r4, C18790t3 r5, C18640sm r6, AnonymousClass01d r7, C16590pI r8, C14820m6 r9, C16120oU r10, C21780xy r11, C18800t4 r12, C22650zQ r13, C19930uu r14, AbstractC14440lR r15) {
        this.A07 = r8;
        this.A03 = r4;
        this.A0D = r14;
        this.A0E = r15;
        this.A04 = r5;
        this.A09 = r10;
        this.A0C = r13;
        this.A06 = r7;
        this.A0B = r12;
        this.A08 = r9;
        this.A05 = r6;
        this.A02 = r3;
        this.A0A = r11;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 5, insn: 0x0056: IF  (r5 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:42:0x0060, block:B:38:0x0056
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static java.io.File A00(
/*
[107] Method generation error in method: X.16A.A00(java.io.File, java.io.File):java.io.File, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r7v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public static String A01(HashSet hashSet) {
        StringBuilder sb = new StringBuilder();
        if (!hashSet.isEmpty()) {
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                sb.append((String) it.next());
                sb.append(',');
            }
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    public final C28401Na A04(String str) {
        int memoryClass;
        ActivityManager A03 = this.A06.A03();
        if (A03 == null) {
            Log.w("memorydumpuploadservice/get-upload-params am=null");
            memoryClass = 16;
        } else {
            memoryClass = A03.getMemoryClass();
        }
        this.A0D.A02(this.A07, AnonymousClass00C.A01());
        return new C28401Na(str, memoryClass);
    }

    public final File A05() {
        File file = new File(this.A07.A00.getCacheDir(), "Crashes");
        file.mkdirs();
        return file;
    }

    public final File A06(File file, long j) {
        File A05 = A05();
        StringBuilder sb = new StringBuilder();
        sb.append(j);
        sb.append(".log");
        return A00(file, new File(A05, sb.toString()));
    }

    public final Map A07(Map map) {
        File file;
        int i;
        if (map.isEmpty()) {
            return map;
        }
        HashMap hashMap = new HashMap();
        try {
            file = new File(A05(), UUID.randomUUID().toString().substring(24));
            int i2 = 0;
            while (true) {
                i = i2 + 1;
                if (i2 >= 10 || file.mkdirs()) {
                    break;
                }
                file = new File(A05(), UUID.randomUUID().toString().substring(24));
                i2 = i;
            }
        } catch (IOException unused) {
        }
        if (i <= 10) {
            for (Map.Entry entry : map.entrySet()) {
                Object key = entry.getKey();
                File file2 = new File((String) entry.getValue());
                File A00 = A00(file2, new File(file, file2.getName()));
                if (A00 != null) {
                    hashMap.put(key, A00.getAbsolutePath());
                }
            }
            return hashMap;
        }
        throw new IOException("max retries reached while creating attachment temp directory");
    }

    public final void A08(C28411Nc r19, String str, String str2, Map map, boolean z) {
        Set<String> set;
        Set<C28431Ne> set2;
        AnonymousClass1Nb r6 = new AnonymousClass1Nb();
        r6.A02 = str;
        r6.A01 = str2;
        if (z) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (StackTraceElement stackTraceElement : r19.getStackTrace()) {
                sb.append(stackTraceElement.toString());
                sb.append("\n");
                i++;
                if (i == 15) {
                    break;
                }
            }
            r6.A00 = sb.toString();
        }
        this.A09.A05(r6);
        AnonymousClass125 r8 = this.A02;
        if (!"AppMessagingXmppHandler/onLogNotification".equals(str)) {
            synchronized (r8) {
                SharedPreferences sharedPreferences = r8.A00;
                if (sharedPreferences == null) {
                    sharedPreferences = r8.A05.A01("critical_event_client_prefs");
                    r8.A00 = sharedPreferences;
                }
                if (r8.A02 == null) {
                    AnonymousClass009.A05(sharedPreferences);
                    if (221770000 != sharedPreferences.getInt("build_version", -1)) {
                        boolean commit = r8.A00.edit().remove("critical_event_client_config").putInt("build_version", 221770000).commit();
                        StringBuilder sb2 = new StringBuilder("CriticalEventConfigManager/updateConfigFromAbProp/clearing client config due to app upgrade ");
                        sb2.append(commit);
                        Log.i(sb2.toString());
                    }
                    String A03 = r8.A04.A03(414);
                    String string = r8.A00.getString("critical_event_client_config", "");
                    HashSet hashSet = new HashSet();
                    String[] split = A03.split(";");
                    int length = split.length;
                    if (length > 0) {
                        long currentTimeMillis = System.currentTimeMillis() + 3888000000L;
                        int i2 = 0;
                        while (true) {
                            String[] split2 = split[i2].split(":");
                            if (split2.length == 2 && !TextUtils.isEmpty(split2[0]) && !TextUtils.isEmpty(split2[1])) {
                                String str3 = split2[0];
                                int A00 = C28421Nd.A00(split2[1], -1);
                                if (A00 >= 1) {
                                    hashSet.add(new C28431Ne(A00, str3, currentTimeMillis));
                                }
                            }
                            i2++;
                            if (i2 >= length) {
                                break;
                            }
                        }
                    }
                    HashSet hashSet2 = new HashSet();
                    String[] split3 = string.split(";");
                    int length2 = split3.length;
                    if (length2 > 0) {
                        int i3 = 0;
                        while (true) {
                            String[] split4 = split3[i3].split(":");
                            if (split4.length == 3 && !TextUtils.isEmpty(split4[0]) && !TextUtils.isEmpty(split4[1]) && !TextUtils.isEmpty(split4[2])) {
                                String str4 = split4[0];
                                int A002 = C28421Nd.A00(split4[1], -1);
                                if (A002 >= 1) {
                                    long A01 = C28421Nd.A01(split4[2], -1);
                                    if (A01 >= 1) {
                                        hashSet2.add(new C28431Ne(A002, str4, A01));
                                    }
                                }
                            }
                            i3++;
                            if (i3 >= length2) {
                                break;
                            }
                        }
                    }
                    HashSet hashSet3 = new HashSet();
                    r8.A02 = hashSet3;
                    HashSet hashSet4 = new HashSet();
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        hashSet4.add(((C28431Ne) it.next()).A02);
                    }
                    HashSet hashSet5 = new HashSet();
                    HashSet hashSet6 = new HashSet();
                    Iterator it2 = hashSet2.iterator();
                    while (it2.hasNext()) {
                        C28431Ne r2 = (C28431Ne) it2.next();
                        String str5 = r2.A02;
                        if (hashSet4.contains(str5)) {
                            hashSet6.add(r2);
                            hashSet5.add(str5);
                        }
                    }
                    Iterator it3 = hashSet.iterator();
                    while (it3.hasNext()) {
                        C28431Ne r22 = (C28431Ne) it3.next();
                        String str6 = r22.A02;
                        if (!hashSet5.contains(str6)) {
                            hashSet6.add(r22);
                            hashSet5.add(str6);
                        }
                    }
                    hashSet3.addAll(hashSet6);
                    StringBuilder sb3 = new StringBuilder();
                    for (Object obj : r8.A02) {
                        sb3.append(obj);
                    }
                    r8.A00.edit().putString("critical_event_client_config", sb3.toString()).apply();
                }
                set2 = r8.A02;
                AnonymousClass009.A05(set2);
            }
            for (C28431Ne r3 : set2) {
                if (str.contains(r3.A02) && System.currentTimeMillis() < r3.A01) {
                    if (r8.A06.nextInt(r3.A00) != 0) {
                        return;
                    }
                }
            }
            return;
        }
        AnonymousClass01I.A00();
        Log.e("UNCAUGHT EXCEPTION", r19);
        String message = r19.getMessage();
        synchronized (r8) {
            Set set3 = r8.A01;
            set = set3;
            if (set3 == null) {
                String A032 = r8.A04.A03(426);
                HashSet hashSet7 = new HashSet();
                String[] split5 = A032.split(";");
                for (String str7 : split5) {
                    if (str7.length() > 0) {
                        hashSet7.add(str7);
                    }
                }
                r8.A01 = hashSet7;
                set = hashSet7;
            }
            AnonymousClass009.A05(set);
        }
        for (String str8 : set) {
            if (message.contains(str8)) {
                return;
            }
        }
        try {
            this.A0E.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 31, map));
        } catch (Exception e) {
            Log.e("crashlogs/upload/failed", e);
            StringBuilder sb4 = new StringBuilder("wa-worker-error-");
            sb4.append(e.getMessage());
            A0A(sb4.toString(), true);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 5, insn: 0x00ba: INVOKE  (r5 I:java.lang.String), (r0 I:java.lang.Throwable) type: STATIC call: com.whatsapp.util.Log.w(java.lang.String, java.lang.Throwable):void, block:B:21:0x00ba
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final void A09(
/*
[202] Method generation error in method: X.16A.A09(X.1Nf):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r9v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public final void A0A(String str, boolean z) {
        if (z) {
            AnonymousClass1Nb r1 = new AnonymousClass1Nb();
            r1.A02 = "crash-log-upload-failure";
            r1.A01 = str;
            this.A09.A05(r1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003b, code lost:
        if ("IN".equals(X.C22650zQ.A01(r2.cc, r2.number)) != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005a, code lost:
        if ("log_files_upload".equals(r27) != false) goto L_0x005c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0B(java.lang.String r25, java.lang.String r26, java.lang.String r27, java.util.Map r28, boolean r29, boolean r30) {
        /*
        // Method dump skipped, instructions count: 583
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass16A.A0B(java.lang.String, java.lang.String, java.lang.String, java.util.Map, boolean, boolean):boolean");
    }
}
