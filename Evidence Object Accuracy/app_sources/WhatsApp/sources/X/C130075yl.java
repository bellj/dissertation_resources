package X;

import com.whatsapp.util.Log;

/* renamed from: X.5yl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130075yl {
    public final C006202y A00 = new C006202y(10);
    public final C15570nT A01;
    public final C14830m7 A02;
    public final AnonymousClass102 A03;
    public final C17070qD A04;
    public final C130155yt A05;
    public final C130125yq A06;
    public final C22980zx A07;

    public C130075yl(C15570nT r3, C14830m7 r4, AnonymousClass102 r5, C17070qD r6, C130155yt r7, C130125yq r8, C22980zx r9) {
        this.A02 = r4;
        this.A01 = r3;
        this.A04 = r6;
        this.A05 = r7;
        this.A07 = r9;
        this.A06 = r8;
        this.A03 = r5;
    }

    public AnonymousClass017 A00(AbstractC28901Pl r8, AnonymousClass6F2 r9) {
        AnonymousClass016 A0T = C12980iv.A0T();
        C006202y r2 = this.A00;
        C1309460p r1 = (C1309460p) r2.A04(r9);
        if (r1 != null) {
            if (r1.A02.A01()) {
                C130785zy.A01(A0T, null, r1);
                return A0T;
            }
            r2.A07(r9);
        }
        C130155yt r4 = this.A05;
        AnonymousClass61S[] r6 = new AnonymousClass61S[2];
        AnonymousClass61S.A05("action", "novi-get-fi-withdrawal-quote", r6, 0);
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("credential_id", r8.A0A), r6, 1);
        A0F.A02.add(C130325zE.A02(r9, "amount"));
        r4.A0B(new AbstractC136196Lo(A0T, this) { // from class: X.69v
            public final /* synthetic */ AnonymousClass016 A00;
            public final /* synthetic */ C130075yl A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r3) {
                this.A01.A02(this.A00, r3);
            }
        }, A0F, "get", 5);
        return A0T;
    }

    public AnonymousClass017 A01(AnonymousClass6F2 r8, String str) {
        AnonymousClass016 A0T = C12980iv.A0T();
        C006202y r2 = this.A00;
        C1309460p r1 = (C1309460p) r2.A04(r8);
        if (r1 != null) {
            if (r1.A02.A01()) {
                C130785zy.A01(A0T, null, r1);
                return A0T;
            }
            r2.A07(r8);
        }
        C130155yt r4 = this.A05;
        AnonymousClass61S[] r6 = new AnonymousClass61S[2];
        AnonymousClass61S.A05("action", "novi-get-cash-withdrawal-quote", r6, 0);
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("store_id", str), r6, 1);
        A0F.A02.add(C130325zE.A02(r8, "amount"));
        r4.A0B(new AbstractC136196Lo(A0T, this) { // from class: X.69w
            public final /* synthetic */ AnonymousClass016 A00;
            public final /* synthetic */ C130075yl A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r3) {
                this.A01.A02(this.A00, r3);
            }
        }, A0F, "get", 5);
        return A0T;
    }

    public final void A02(AnonymousClass016 r5, C130785zy r6) {
        Object obj;
        if (r6.A06() && (obj = r6.A02) != null) {
            try {
                C1309460p A00 = C1309460p.A00(this.A03, ((AnonymousClass1V8) obj).A0F("withdraw_amount"));
                C130785zy.A01(r5, null, A00);
                this.A00.A08(A00.A00, A00);
                return;
            } catch (AnonymousClass1V9 unused) {
                Log.e("PAY: NoviWithdrawRepository/handleWithdrawalQuoteResponse can't parse result");
            }
        }
        C130785zy.A01(r5, r6.A00, null);
    }
}
