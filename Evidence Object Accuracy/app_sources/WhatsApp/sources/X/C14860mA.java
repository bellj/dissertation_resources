package X;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.TriggerEventListener;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;
import com.whatsapp.web.WebSessionVerificationReceiver;
import java.io.File;
import java.io.FileInputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0mA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14860mA {
    public Sensor A00;
    public SensorManager A01;
    public TriggerEventListener A02;
    public AnonymousClass1UV A03;
    public AnonymousClass1UO A04;
    public Boolean A05;
    public String A06;
    public SecureRandom A07;
    public Map A08;
    public boolean A09;
    public boolean A0A;
    public final Handler A0B = new Handler(Looper.getMainLooper());
    public final C16210od A0C;
    public final C244615p A0D;
    public final C15450nH A0E;
    public final C18230s7 A0F;
    public final AnonymousClass01d A0G;
    public final C14830m7 A0H;
    public final C16590pI A0I;
    public final C14820m6 A0J;
    public final AnonymousClass018 A0K;
    public final C22900zp A0L;
    public final AbstractC14440lR A0M;
    public final C14890mD A0N;
    public final AnonymousClass1UN A0O;
    public final C245816b A0P;
    public final List A0Q;
    public final List A0R;
    public final AtomicInteger A0S;
    public final AtomicReference A0T = new AtomicReference();
    public volatile Map A0U;

    public C14860mA(C16210od r3, C244615p r4, C15450nH r5, C18230s7 r6, AnonymousClass01d r7, C14830m7 r8, C16590pI r9, C14820m6 r10, AnonymousClass018 r11, C22900zp r12, AbstractC14440lR r13, C14890mD r14, C245816b r15) {
        this.A0I = r9;
        this.A0H = r8;
        this.A0F = r6;
        this.A0M = r13;
        this.A0N = r14;
        this.A0E = r5;
        this.A0G = r7;
        this.A0K = r11;
        this.A0L = r12;
        this.A0P = r15;
        this.A0J = r10;
        this.A0D = r4;
        this.A0C = r3;
        this.A0O = new AnonymousClass1UN(this);
        this.A0R = new CopyOnWriteArrayList();
        this.A0Q = Collections.synchronizedList(new ArrayList());
        this.A0S = new AtomicInteger(1);
    }

    public final synchronized AnonymousClass1UO A00() {
        AnonymousClass1UO r1;
        r1 = this.A04;
        if (r1 == null) {
            HandlerThread handlerThread = new HandlerThread("web-session-disk-cache-handler", 10);
            handlerThread.start();
            r1 = new AnonymousClass1UO(handlerThread.getLooper(), this);
            this.A04 = r1;
        }
        return r1;
    }

    public final C14880mC A01(C14880mC r7) {
        if (r7 != null) {
            if (r7.A0D) {
                long j = r7.A03;
                if (j != 0 && j < System.currentTimeMillis()) {
                    StringBuilder sb = new StringBuilder("webSession/getUnexpiredSession browser timed out ");
                    String str = r7.A0F;
                    sb.append(str);
                    Log.i(sb.toString());
                    A0K(str, false);
                }
            }
            return r7;
        }
        return null;
    }

    public String A02() {
        String str;
        StringBuilder sb = new StringBuilder();
        synchronized (this) {
            str = this.A06;
            if (str == null) {
                byte[] bArr = new byte[8];
                A03().nextBytes(bArr);
                str = AnonymousClass1US.A0B(bArr);
                this.A06 = str;
            }
        }
        sb.append(str);
        sb.append(".--");
        sb.append(Integer.toHexString(this.A0S.getAndIncrement()));
        return sb.toString();
    }

    public final synchronized SecureRandom A03() {
        SecureRandom secureRandom;
        secureRandom = this.A07;
        if (secureRandom == null) {
            secureRandom = new SecureRandom();
            this.A07 = secureRandom;
        }
        return secureRandom;
    }

    public List A04() {
        Iterator it = new ArrayList(A05().values()).iterator();
        while (it.hasNext()) {
            A01((C14880mC) it.next());
        }
        return new ArrayList(A05().values());
    }

    public final Map A05() {
        if (this.A08 == null) {
            this.A08 = new LinkedHashMap();
            C245816b r7 = this.A0P;
            ArrayList arrayList = new ArrayList();
            C16310on A01 = r7.A01.get();
            try {
                Cursor A08 = A01.A03.A08("sessions", null, null, null, new String[]{"browser_id", "secret", "token", "os", "browser_type", "lat", "lon", "accuracy", "place_name", "last_active", "timeout", "expiration", "last_push_sent", "login_time", "syncd_release"}, null);
                while (A08 != null && A08.moveToNext()) {
                    C15450nH r12 = r7.A00;
                    String string = A08.getString(0);
                    String string2 = A08.getString(1);
                    String string3 = A08.getString(2);
                    String string4 = A08.getString(3);
                    String string5 = A08.getString(4);
                    boolean z = false;
                    if (A08.getInt(10) > 0) {
                        z = true;
                    }
                    C14880mC r11 = new C14880mC(r12, string, string2, string3, string4, string5, z);
                    r11.A01 = A08.getDouble(5);
                    r11.A02 = A08.getDouble(6);
                    r11.A00 = A08.getDouble(7);
                    r11.A09 = A08.getString(8);
                    r11.A04 = A08.getLong(9);
                    r11.A03 = A08.getLong(11);
                    r11.A05 = A08.getLong(12);
                    r11.A06 = A08.getLong(13);
                    arrayList.add(r11);
                }
                if (A08 != null) {
                    A08.close();
                }
                A01.close();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    C14880mC r2 = (C14880mC) it.next();
                    this.A08.put(r2.A0F, r2);
                }
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return this.A08;
    }

    public Map A06(boolean z) {
        if (this.A0U == null) {
            synchronized (this) {
                if (this.A0U == null) {
                    this.A0U = Collections.synchronizedMap(new AnonymousClass1UQ());
                    if (z) {
                        try {
                            synchronized (this.A0U) {
                                FileInputStream fileInputStream = new FileInputStream(new File(this.A0I.A00.getCacheDir(), "WebActionIdCache"));
                                AnonymousClass1UR r5 = new AnonymousClass1UR(this, fileInputStream);
                                for (Map.Entry entry : ((Map) r5.readObject()).entrySet()) {
                                    if (((Integer) entry.getValue()).intValue() > 0) {
                                        this.A0U.put((String) entry.getKey(), (Integer) entry.getValue());
                                    }
                                }
                                r5.close();
                                fileInputStream.close();
                            }
                        } catch (Exception e) {
                            Log.e(e);
                        }
                    }
                }
            }
        }
        return this.A0U;
    }

    public void A07() {
        Log.i("qrsession/deleteAllSessions");
        AnonymousClass1UP A00 = this.A0N.A00();
        A00.A08.edit().clear().apply();
        A00.A07 = null;
        A00.A05 = null;
        A00.A06 = null;
        A00.A02 = null;
        A00.A00 = null;
        A05().clear();
        C16310on A02 = this.A0P.A01.A02();
        try {
            A02.A03.A01("sessions", null, null);
            A02.close();
            A06(false).clear();
            A00().sendEmptyMessage(2);
            A0D();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A08() {
        if (this.A0Q.size() > 0) {
            this.A0M.Ab2(new RunnableBRunnable0Shape13S0100000_I0_13(this, 47));
        }
    }

    public void A09() {
        if (this.A0N.A02()) {
            this.A0A = true;
            A0F();
        }
    }

    public void A0A() {
        this.A09 = true;
        if (Build.VERSION.SDK_INT >= 23) {
            AnonymousClass1UN r1 = this.A0O;
            r1.removeMessages(2);
            r1.removeMessages(3);
            Message.obtain(r1, 1, 0, 0).sendToTarget();
        }
        A0D();
    }

    public void A0B() {
        if (Build.VERSION.SDK_INT >= 23) {
            this.A0O.sendEmptyMessage(2);
        }
    }

    public final void A0C() {
        if (A0O()) {
            SensorManager sensorManager = this.A01;
            AnonymousClass009.A05(sensorManager);
            sensorManager.cancelTriggerSensor(this.A02, this.A00);
        }
    }

    public final void A0D() {
        for (AnonymousClass1UU r0 : this.A0R) {
            r0.AYU();
        }
    }

    public final void A0E() {
        if (Build.VERSION.SDK_INT >= 23) {
            AnonymousClass1UN r6 = this.A0O;
            if (!r6.hasMessages(2) && !r6.hasMessages(3)) {
                long min = Math.min(r6.A00 + C26061Bw.A0L, ((long) r6.A01.A0E.A02(AbstractC15460nI.A2I)) * 1000);
                r6.A00 = min;
                boolean sendEmptyMessageDelayed = r6.sendEmptyMessageDelayed(3, min);
                StringBuilder sb = new StringBuilder("qrsession/fservice/delayed timeout=");
                sb.append(r6.A00);
                sb.append(" success:");
                sb.append(sendEmptyMessageDelayed);
                sb.append(" uptime:");
                sb.append(SystemClock.uptimeMillis());
                Log.i(sb.toString());
            }
        }
    }

    public final void A0F() {
        if (A0O()) {
            SensorManager sensorManager = this.A01;
            AnonymousClass009.A05(sensorManager);
            sensorManager.requestTriggerSensor(this.A02, this.A00);
        }
    }

    public void A0G(Context context, long j) {
        if (!this.A0F.A01(AnonymousClass1UY.A01(context, 0, new Intent(context, WebSessionVerificationReceiver.class), 134217728), 0, j)) {
            Log.w("WebSession/scheduleWebSessionVerificationAlarm AlarmManager is null");
        }
    }

    public void A0H(String str, int i) {
        A06(true).put(str, Integer.valueOf(i));
        A00().sendEmptyMessageDelayed(1, 2000);
        this.A0N.A00();
    }

    /* JADX INFO: finally extract failed */
    public void A0I(String str, long j) {
        C14880mC r8;
        if (str != null && j > 0 && (r8 = (C14880mC) A05().get(str)) != null) {
            r8.A04 = j;
            r8.A05 = j;
            C245816b r2 = this.A0P;
            ContentValues contentValues = new ContentValues();
            contentValues.put("last_active", Long.valueOf(j));
            C29611Tw r7 = r2.A01;
            C16310on A02 = r7.A02();
            try {
                A02.A03.A00("sessions", contentValues, "browser_id = ?", new String[]{str});
                A02.close();
                long j2 = r8.A05;
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("last_push_sent", Long.valueOf(j2));
                A02 = r7.A02();
                try {
                    A02.A03.A00("sessions", contentValues2, "browser_id = ?", new String[]{str});
                    A02.close();
                    A0D();
                } finally {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    public void A0J(String str, String str2, String str3, String str4, String str5, String str6, boolean z) {
        boolean z2;
        this.A03 = null;
        C14890mD r4 = this.A0N;
        if (!r4.A03(str3)) {
            Log.i("qrsession/onQrSync/clear epoch idcache");
            r4.A00().A01 = null;
            A06(false).clear();
            A00().sendEmptyMessage(2);
        }
        boolean z3 = !r4.A03(str3);
        AnonymousClass1UP A00 = r4.A00();
        A00.A04 = z3;
        A00.A08.edit().putBoolean("browser_changed", z3).commit();
        AnonymousClass1UP A002 = r4.A00();
        A002.A03 = str;
        A002.A08.edit().putString("ref", str).commit();
        AnonymousClass1UP A003 = r4.A00();
        A003.A02 = str4;
        A003.A08.edit().putString("token", str4).commit();
        if (str2 != null) {
            r4.A01(str2, true);
        }
        if (str3 != null) {
            AnonymousClass1UP A004 = r4.A00();
            A004.A00 = str3;
            A004.A08.edit().putString("browser", str3).commit();
            StringBuilder sb = new StringBuilder("qrsession/set_browser ");
            sb.append(str3);
            sb.append(' ');
            sb.append(str4);
            sb.append(' ');
            sb.append(str5);
            sb.append(' ');
            sb.append(str6);
            Log.i(sb.toString());
            C14880mC r9 = (C14880mC) A05().get(str3);
            boolean z4 = false;
            if (r9 == null) {
                z2 = true;
                r9 = new C14880mC(this.A0E, str3, str2, str4, str5, str6, z);
                r9.A06 = this.A0H.A00();
                A05().put(str3, r9);
                z4 = this.A0C.A00;
            } else {
                z2 = false;
                r9.A0A = str2;
                r9.A0B = str4;
                r9.A08 = str5;
                r9.A07 = str6;
                r9.A0D = z;
                r9.A03 = 0;
                r9.A0C = "Portal".equals(str5);
            }
            r9.A04 = this.A0H.A00();
            C245816b r7 = this.A0P;
            ContentValues contentValues = new ContentValues();
            contentValues.put("browser_id", r9.A0F);
            contentValues.put("secret", r9.A0A);
            contentValues.put("token", r9.A0B);
            contentValues.put("os", r9.A08);
            contentValues.put("browser_type", r9.A07);
            contentValues.put("login_time", Long.valueOf(r9.A06));
            contentValues.put("last_active", Long.valueOf(r9.A04));
            contentValues.put("timeout", Boolean.valueOf(r9.A0D));
            C16310on A02 = r7.A01.A02();
            try {
                A02.A03.A05(contentValues, "sessions");
                A02.close();
                if (z2) {
                    Iterator it = this.A0R.iterator();
                    while (it.hasNext()) {
                        it.next();
                    }
                }
                A0D();
                if (z4) {
                    C244615p r10 = this.A0D;
                    Location A022 = r10.A02("web-session", 2);
                    if (A022 == null || A022.getTime() + 120000 <= System.currentTimeMillis()) {
                        AnonymousClass1UW r11 = new AnonymousClass1UW(this, r9);
                        r10.A05(r11, "web-session", 0.0f, 2, 0, 0);
                        this.A0B.postDelayed(new RunnableBRunnable0Shape8S0200000_I0_8(this, 47, r11), 60000);
                    } else {
                        StringBuilder sb2 = new StringBuilder("qrsession/location/last ");
                        sb2.append(A022.getTime());
                        Log.i(sb2.toString());
                        this.A0M.Ab2(new RunnableBRunnable0Shape2S0300000_I0_2(A022, this, r9));
                    }
                }
                boolean z5 = r9.A0C;
                if (Build.VERSION.SDK_INT >= 23) {
                    AnonymousClass1UN r2 = this.A0O;
                    r2.removeMessages(2);
                    r2.removeMessages(3);
                    Message.obtain(r2, 1, z5 ? 1 : 0, 0).sendToTarget();
                }
                A0E();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public void A0K(String str, boolean z) {
        StringBuilder sb = new StringBuilder("qrsession/deleteSession bid=");
        sb.append(str);
        Log.i(sb.toString());
        C14890mD r2 = this.A0N;
        if (r2.A03(str)) {
            A06(false).clear();
            A00().sendEmptyMessage(2);
            AnonymousClass1UP A00 = r2.A00();
            A00.A07 = null;
            A00.A05 = null;
            A00.A06 = null;
            A00.A02 = null;
            A00.A00 = null;
        }
        A05().remove(str);
        C16310on A02 = this.A0P.A01.A02();
        try {
            A02.A03.A01("sessions", "browser_id = ?", new String[]{str});
            A02.close();
            if (z) {
                A0D();
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0L(boolean z) {
        C14890mD r5 = this.A0N;
        r5.A00().A03 = null;
        r5.A00().A00("ref");
        if (z) {
            A0K(r5.A00().A00, false);
            AnonymousClass1UP A00 = r5.A00();
            A00.A07 = null;
            A00.A05 = null;
            A00.A06 = null;
            A00.A02 = null;
            A00.A00 = null;
            AnonymousClass1UP A002 = r5.A00();
            A002.A00("key");
            A002.A00("token");
            A002.A00("browser");
            A0D();
            this.A0O.sendEmptyMessage(2);
        } else {
            C14880mC r4 = (C14880mC) A05().get(r5.A00().A00);
            if (r4 != null && r4.A0D) {
                r4.A03 = System.currentTimeMillis() + 600000;
                C245816b r2 = this.A0P;
                String str = r5.A00().A00;
                long j = r4.A03;
                ContentValues contentValues = new ContentValues();
                contentValues.put("expiration", Long.valueOf(j));
                C16310on A02 = r2.A01.A02();
                try {
                    A02.A03.A00("sessions", contentValues, "browser_id = ?", new String[]{str});
                    A02.close();
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
        this.A0Q.clear();
        this.A0A = false;
        A0C();
    }

    public boolean A0M() {
        for (C14880mC r0 : A04()) {
            if (r0 != null && r0.A0C) {
                return true;
            }
        }
        return false;
    }

    public boolean A0N() {
        return this.A0N.A02() && this.A09;
    }

    public final synchronized boolean A0O() {
        Boolean bool;
        boolean z;
        bool = this.A05;
        if (bool == null) {
            AnonymousClass01d r4 = this.A0G;
            boolean z2 = true;
            try {
                Class.forName("android.hardware.TriggerEventListener");
                z = true;
            } catch (Exception unused) {
                z = false;
            }
            if (Build.VERSION.SDK_INT >= 18 && z) {
                SensorManager A0D = r4.A0D();
                this.A01 = A0D;
                Sensor sensor = null;
                if (A0D != null) {
                    sensor = A0D.getDefaultSensor(17);
                }
                this.A00 = sensor;
                this.A02 = new AnonymousClass1UT(this);
            }
            if (this.A01 == null || this.A00 == null) {
                z2 = false;
            }
            bool = Boolean.valueOf(z2);
            this.A05 = bool;
        }
        return bool.booleanValue();
    }
}
