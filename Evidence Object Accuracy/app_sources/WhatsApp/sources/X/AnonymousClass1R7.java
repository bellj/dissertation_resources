package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.1R7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1R7 extends AbstractC29111Qx {
    public final /* synthetic */ C251818k A00;
    public final /* synthetic */ C251918l A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ List A07;
    public final /* synthetic */ Map A08;
    public final /* synthetic */ byte[] A09;
    public final /* synthetic */ byte[] A0A;
    public final /* synthetic */ byte[] A0B;

    public AnonymousClass1R7(C251818k r1, C251918l r2, String str, String str2, String str3, String str4, String str5, List list, Map map, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A06 = str;
        this.A04 = str2;
        this.A05 = str3;
        this.A0B = bArr;
        this.A0A = bArr2;
        this.A09 = bArr3;
        this.A02 = str4;
        this.A03 = str5;
        this.A08 = map;
        this.A07 = list;
    }
}
