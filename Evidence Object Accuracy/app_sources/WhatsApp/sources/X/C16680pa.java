package X;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: X.0pa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16680pa implements AnonymousClass6MD {
    public Map A00;
    public final C14900mE A01;
    public final AnonymousClass0t9 A02;
    public final AnonymousClass018 A03;
    public final AbstractC14440lR A04;
    public final C18840t8 A05;
    public final C16660pY A06;
    public final C18820t6 A07;

    public C16680pa(C14900mE r2, AnonymousClass0t9 r3, AnonymousClass018 r4, AbstractC14440lR r5, C18840t8 r6, C16660pY r7, C18820t6 r8) {
        C16700pc.A0E(r6, 1);
        C16700pc.A0E(r2, 2);
        C16700pc.A0E(r5, 3);
        C16700pc.A0E(r4, 4);
        C16700pc.A0E(r3, 5);
        C16700pc.A0E(r7, 6);
        this.A05 = r6;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
        this.A06 = r7;
        this.A07 = r8;
    }

    public static /* synthetic */ void A00(AnonymousClass1WE r6, C65963Lt r7, C16680pa r8, C129625y2 r9, String str) {
        String str2;
        Object next;
        C16700pc.A0E(r8, 0);
        C16700pc.A0E(r6, 1);
        C16700pc.A0E(str, 2);
        C16700pc.A0E(r9, 3);
        C18820t6 r2 = r8.A07;
        String str3 = r6.A08;
        if (!(r8 instanceof C16670pZ)) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry entry : ((Map) r8.A06.A01.getValue()).entrySet()) {
                if (C16700pc.A0O(((C91284Rd) entry.getValue()).A01, str)) {
                    linkedHashMap.put(entry.getKey(), entry.getValue());
                }
            }
            Set keySet = linkedHashMap.keySet();
            C16700pc.A0E(keySet, 0);
            if (keySet instanceof List) {
                List list = (List) keySet;
                C16700pc.A0E(list, 0);
                if (!list.isEmpty()) {
                    next = list.get(0);
                } else {
                    throw new NoSuchElementException("List is empty.");
                }
            } else {
                Iterator it = keySet.iterator();
                if (it.hasNext()) {
                    next = it.next();
                } else {
                    throw new NoSuchElementException("Collection is empty.");
                }
            }
            str2 = (String) next;
            C16700pc.A0B(str2);
        } else {
            Map map = r8.A00;
            if (map != null) {
                Object obj = map.get("flow_version_id");
                if (obj != null) {
                    str2 = (String) obj;
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
                }
            } else {
                C16700pc.A0K("paramsMap");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        }
        r2.A0A(new C68743Wo(r7, r8, r9, str), str3, str2);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:32:0x005f */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r7v0, types: [X.0t9] */
    /* JADX WARN: Type inference failed for: r6v0, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r6v1, types: [X.1WF] */
    /* JADX WARN: Type inference failed for: r6v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1WE A01(java.lang.String r11) {
        /*
            r10 = this;
            boolean r0 = r10 instanceof X.C16670pZ
            if (r0 != 0) goto L_0x000f
            r0 = 0
            X.C16700pc.A0E(r11, r0)
            X.0t9 r0 = r10.A02
            X.1WE r0 = r0.A00(r11)
            return r0
        L_0x000f:
            java.util.Map r1 = r10.A00
            if (r1 == 0) goto L_0x006c
            java.lang.String r0 = "flow_version_id"
            java.lang.Object r0 = r1.get(r0)
            if (r0 == 0) goto L_0x0064
            java.lang.String r0 = (java.lang.String) r0
            long r8 = java.lang.Long.parseLong(r0)
            X.0t9 r7 = r10.A02
            X.1WG r0 = r7.A02()
            if (r0 == 0) goto L_0x005d
            java.util.List r0 = r0.A00
            if (r0 == 0) goto L_0x005d
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.util.Iterator r5 = r0.iterator()
        L_0x0036:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x005f
            java.lang.Object r4 = r5.next()
            r3 = r4
            X.1WE r3 = (X.AnonymousClass1WE) r3
            java.lang.Long r0 = r3.A02
            if (r0 == 0) goto L_0x0036
            long r1 = r0.longValue()
            int r0 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r0 != 0) goto L_0x0036
            java.lang.String r1 = r3.A06
            java.lang.String r0 = "android"
            boolean r0 = X.C16700pc.A0O(r1, r0)
            if (r0 == 0) goto L_0x0036
            r6.add(r4)
            goto L_0x0036
        L_0x005d:
            X.1WF r6 = X.AnonymousClass1WF.A00
        L_0x005f:
            X.1WE r0 = r7.A01(r6)
            return r0
        L_0x0064:
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.String"
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        L_0x006c:
            java.lang.String r0 = "paramsMap"
            X.C16700pc.A0K(r0)
            java.lang.String r1 = "Redex: Unreachable code after no-return invoke"
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16680pa.A01(java.lang.String):X.1WE");
    }

    public String A02(String str) {
        StringBuilder sb;
        if (!(this instanceof C16670pZ)) {
            sb = new StringBuilder();
            sb.append(str);
        } else {
            Map map = this.A00;
            if (map != null) {
                Object obj = map.get("flow_version_id");
                if (obj != null) {
                    long parseLong = Long.parseLong((String) obj);
                    sb = new StringBuilder();
                    sb.append(parseLong);
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
                }
            } else {
                C16700pc.A0K("paramsMap");
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        }
        sb.append(":8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666:");
        sb.append(AnonymousClass018.A00(this.A03.A00));
        return sb.toString();
    }

    @Override // X.AnonymousClass6MD
    public void AZP(C65963Lt r8, C129625y2 r9, String str, String str2) {
        String str3;
        C16700pc.A0E(str, 0);
        this.A00 = C65053Hy.A01(str2);
        if (r8 == null || (str3 = (String) this.A05.A01(r8.A01, A02(str))) == null) {
            AnonymousClass1WE A01 = A01(str);
            if (A01 != null) {
                this.A01.A0I(new RunnableC76283lO(A01, r8, this, r9, str));
                return;
            }
            AnonymousClass0t9 r1 = this.A02;
            r1.A01 = new AnonymousClass1WD(r8, this, r9, str);
            r1.A03();
            return;
        }
        r9.A01(str3);
    }
}
