package X;

import com.whatsapp.status.StatusRecipientsActivity;

/* renamed from: X.2oS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58282oS extends AbstractActivityC36551k4 {
    public boolean A00 = false;

    public AbstractActivityC58282oS() {
        ActivityC13830kP.A1P(this, 125);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            StatusRecipientsActivity statusRecipientsActivity = (StatusRecipientsActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, statusRecipientsActivity);
            ActivityC13810kN.A10(A1M, statusRecipientsActivity);
            ActivityC13770kJ.A0M(statusRecipientsActivity, A1M, ActivityC13790kL.A0S(r3, A1M, statusRecipientsActivity, ActivityC13790kL.A0Y(A1M, statusRecipientsActivity)));
            statusRecipientsActivity.A01 = (C18470sV) A1M.AK8.get();
            statusRecipientsActivity.A00 = (C20670w8) A1M.AMw.get();
            statusRecipientsActivity.A02 = (AnonymousClass1BD) A1M.AKA.get();
        }
    }
}
