package X;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.os.Build;
import com.whatsapp.service.RestoreChatConnectionWorker;
import com.whatsapp.service.UnsentMessagesNetworkAvailableJob;
import com.whatsapp.util.Log;

/* renamed from: X.10l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C231210l {
    public final AnonymousClass01d A00;
    public final C16590pI A01;
    public final C21710xr A02;

    public C231210l(AnonymousClass01d r1, C16590pI r2, C21710xr r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public void A00() {
        Log.i("Scheduling job to restore chat connection");
        AnonymousClass023 r4 = AnonymousClass023.KEEP;
        C004201x r3 = new C004201x(RestoreChatConnectionWorker.class);
        C003901s r2 = new C003901s();
        r2.A01 = EnumC004001t.CONNECTED;
        r3.A00.A09 = new C004101u(r2);
        ((AnonymousClass022) this.A02.get()).A05(r4, (AnonymousClass021) r3.A00(), "com.whatsapp.service.restoreChatConnection");
    }

    public void A01() {
        if (Build.VERSION.SDK_INT >= 21) {
            Log.i("Scheduling job for unsent messages");
            this.A00.A09().schedule(new JobInfo.Builder(6, new ComponentName(this.A01.A00, UnsentMessagesNetworkAvailableJob.class)).setRequiredNetworkType(1).setPersisted(true).build());
        }
    }
}
