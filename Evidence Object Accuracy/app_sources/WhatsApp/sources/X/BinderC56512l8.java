package X;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IInterface;
import android.os.Parcel;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.2l8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56512l8 extends AbstractBinderC73253fv implements IInterface {
    public final Context A00;

    public BinderC56512l8(Context context) {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
        this.A00 = context;
    }

    @Override // X.AbstractBinderC73253fv
    public final boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        BasePendingResult A06;
        if (i == 1) {
            A01();
            Context context = this.A00;
            C65263Iv A00 = C65263Iv.A00(context);
            GoogleSignInAccount A02 = A00.A02();
            GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.A0D;
            if (A02 != null) {
                googleSignInOptions = A00.A03();
            }
            C13020j0.A01(googleSignInOptions);
            C56262kb r0 = new C56262kb(context, googleSignInOptions);
            AnonymousClass1U8 r3 = r0.A05;
            Context context2 = r0.A01;
            boolean A1V = C12960it.A1V(r0.A03(), 3);
            C63703Cp r2 = AnonymousClass3GP.A00;
            Object[] objArr = new Object[0];
            if (A02 != null) {
                r2.A00("Revoking access", objArr);
                String A04 = C65263Iv.A00(context2).A04("refreshToken");
                AnonymousClass3GP.A00(context2);
                if (!A1V) {
                    A06 = r3.A06(new C77533nR(r3));
                } else if (A04 == null) {
                    AnonymousClass5SX status = new Status(4, null);
                    A06 = new C77783nq(status);
                    A06.A05(status);
                } else {
                    RunnableC55832jR r1 = new RunnableC55832jR(A04);
                    new Thread(r1).start();
                    A06 = r1.A00;
                }
            } else {
                r2.A00("Signing out", objArr);
                AnonymousClass3GP.A00(context2);
                if (A1V) {
                    AnonymousClass5SX r12 = Status.A09;
                    C13020j0.A02(r12, "Result must not be null");
                    A06 = new C77773np(r3);
                    A06.A05(r12);
                } else {
                    A06 = r3.A06(new C77523nQ(r3));
                }
            }
            A06.A00(new AnonymousClass2RZ(A06, new C108424z2(), AnonymousClass2RX.A00, new C13690kA()));
            return true;
        } else if (i != 2) {
            return false;
        } else {
            A01();
            C64813Gz.A00(this.A00).A01();
            return true;
        }
    }

    public final void A01() {
        Context context = this.A00;
        if (AnonymousClass4DQ.A00(context, Binder.getCallingUid())) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.google.android.gms", 64);
                C472929z A00 = C472929z.A00(context);
                if (packageInfo != null) {
                    if (C472929z.A01(packageInfo, false)) {
                        return;
                    }
                    if (C472929z.A01(packageInfo, true)) {
                        if (!C472329r.A02(A00.A00)) {
                            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
                        } else {
                            return;
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException unused) {
                if (Log.isLoggable("UidVerifier", 3)) {
                    Log.d("UidVerifier", "Package manager can't find google play services package, defaulting to false");
                }
            }
        }
        int callingUid = Binder.getCallingUid();
        StringBuilder A0t = C12980iv.A0t(52);
        A0t.append("Calling UID ");
        A0t.append(callingUid);
        throw new SecurityException(C12960it.A0d(" is not Google Play services.", A0t));
    }
}
