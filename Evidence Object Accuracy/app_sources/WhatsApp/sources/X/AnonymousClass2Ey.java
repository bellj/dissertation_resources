package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.2Ey  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ey {
    public final Set A00;

    public AnonymousClass2Ey() {
        HashSet hashSet = new HashSet();
        hashSet.add("migration_jid_store");
        hashSet.add("migration_chat_store");
        hashSet.add("call_log");
        hashSet.add("blank_me_jid");
        hashSet.add("participant_user");
        this.A00 = Collections.unmodifiableSet(hashSet);
    }
}
