package X;

import java.util.Arrays;
import java.util.Set;

/* renamed from: X.0qo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17440qo implements AbstractC17450qp {
    public final Set A00;

    public AbstractC17440qo(String... strArr) {
        AnonymousClass01b r1 = new AnonymousClass01b(strArr.length);
        this.A00 = r1;
        r1.addAll(Arrays.asList(strArr));
    }
}
