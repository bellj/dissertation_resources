package X;

import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.6BF  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BF implements AbstractC38041nQ {
    public final String A00;
    public final List A01 = C12960it.A0l();

    public AnonymousClass6BF(String str) {
        AnonymousClass009.A04(str);
        this.A00 = str;
    }

    @Override // X.AbstractC38041nQ
    public AbstractC16830pp AFX(String str) {
        AbstractC16830pp r0;
        if (str == null || C117305Zk.A1V(C30771Yt.A06, str)) {
            for (AbstractC129495xo r02 : this.A01) {
                Iterator it = C12980iv.A0x(r02.A00.values()).iterator();
                while (it.hasNext()) {
                    AbstractC16830pp r1 = (AbstractC16830pp) it.next();
                    if (r1.AJH()) {
                        return r1;
                    }
                }
            }
            return null;
        }
        for (AbstractC129495xo r12 : this.A01) {
            if (!TextUtils.isEmpty(str)) {
                String upperCase = str.toUpperCase(Locale.US);
                Map map = r12.A00;
                if (map.containsKey(upperCase) && (r0 = (AbstractC16830pp) map.get(upperCase)) != null) {
                    return r0;
                }
            }
        }
        return null;
    }
}
