package X;

import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.search.views.itemviews.AudioPlayerView;

/* renamed from: X.2z1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60922z1 extends AnonymousClass3WO {
    public final /* synthetic */ AnonymousClass5U1 A00;
    public final /* synthetic */ AnonymousClass34W A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60922z1(ConversationRowAudioPreview conversationRowAudioPreview, AnonymousClass5U0 r2, AnonymousClass5U1 r3, AnonymousClass5U1 r4, AnonymousClass34W r5, AudioPlayerView audioPlayerView) {
        super(conversationRowAudioPreview, r2, r3, audioPlayerView);
        this.A01 = r5;
        this.A00 = r4;
    }

    @Override // X.AnonymousClass2MF
    public C30421Xi ACr() {
        return this.A01.A09;
    }
}
