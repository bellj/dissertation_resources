package X;

import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.WaInAppBrowsingActivity;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.BrazilPaymentTransactionDetailActivity;
import com.whatsapp.util.Log;

/* renamed from: X.5zO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130425zO {
    public final C17070qD A00;

    public C130425zO(C17070qD r1) {
        this.A00 = r1;
    }

    public static Class A00(Bundle bundle) {
        String A0d;
        String string = bundle.getString("nfm_action");
        if (TextUtils.isEmpty(string)) {
            A0d = "[PAY]: BrazilPayNFMController -- NFM action not passed";
        } else {
            switch (string.hashCode()) {
                case -229223458:
                    if (string.equals("wa_payment_learn_more")) {
                        return WaInAppBrowsingActivity.class;
                    }
                    break;
                case 127237947:
                    if (string.equals("wa_payment_fbpin_reset")) {
                        return BrazilPayBloksActivity.class;
                    }
                    break;
                case 540952115:
                    if (string.equals("wa_payment_transaction_details")) {
                        return BrazilPaymentTransactionDetailActivity.class;
                    }
                    break;
            }
            A0d = C12960it.A0d(string, C12960it.A0k("[PAY]: BrazilPayNFMController -- Unsupported NFM action: "));
        }
        Log.e(A0d);
        return null;
    }
}
