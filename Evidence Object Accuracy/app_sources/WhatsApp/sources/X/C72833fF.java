package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3fF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72833fF extends AnimatorListenerAdapter {
    public final /* synthetic */ AbstractC14670lq A00;
    public final /* synthetic */ boolean A01;
    public final /* synthetic */ boolean A02;

    public C72833fF(AbstractC14670lq r1, boolean z, boolean z2) {
        this.A00 = r1;
        this.A02 = z;
        this.A01 = z2;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.A0U(this.A02, this.A01, false, false);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        AnonymousClass2CF r0 = this.A00.A0O;
        if (r0 != null) {
            r0.A03();
        }
    }
}
