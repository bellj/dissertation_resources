package X;

import android.content.Context;

/* renamed from: X.05n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C011405n implements AbstractC009204q {
    public final /* synthetic */ ActivityC000800j A00;

    public C011405n(ActivityC000800j r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        ActivityC000800j r0 = this.A00;
        AnonymousClass025 A1V = r0.A1V();
        A1V.A06();
        A1V.A0D(r0.A07.A00.A00(ActivityC000800j.A02));
    }
}
