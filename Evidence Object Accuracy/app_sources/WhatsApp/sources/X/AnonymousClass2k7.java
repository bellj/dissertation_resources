package X;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.CodeInputField;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.2k7  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2k7 extends AbstractC65073Ia implements AnonymousClass5SC {
    public AnonymousClass28D A00;
    public final long A01;

    public AnonymousClass2k7(C14260l7 r5, AnonymousClass28D r6) {
        super(EnumC869849t.VIEW);
        this.A01 = (long) r6.A00;
        this.A00 = r6;
        C93304Zx[] r2 = {new C93304Zx(new C106354vZ(r5, this), this), new C93304Zx(new C106344vY(r5, this), this)};
        int i = 0;
        do {
            A04(r2[i]);
            i++;
        } while (i < 2);
    }

    public void A05(View view, C14260l7 r5) {
        Activity activity;
        if (this instanceof C57902no) {
            Context context = r5.A00;
            CodeInputField codeInputField = (CodeInputField) AnonymousClass028.A0D(view, R.id.code);
            codeInputField.removeTextChangedListener(codeInputField.A04);
            if (codeInputField.getTag() != null) {
                codeInputField.removeCallbacks((Runnable) codeInputField.getTag());
            }
            ((ViewGroup) view).removeAllViews();
            activity = AnonymousClass12P.A00(context);
        } else if (this instanceof C57892nn) {
            ((ViewGroup) AnonymousClass028.A0D(view, R.id.container)).removeAllViews();
            activity = AnonymousClass12P.A00(r5.A00);
        } else if (this instanceof C57882nm) {
            AnonymousClass0BD r4 = (AnonymousClass0BD) view;
            for (int i = 0; i < r4.getChildCount(); i++) {
                View childAt = r4.getChildAt(i);
                if (childAt instanceof C55982k5) {
                    ((C55982k5) childAt).setRenderTree(null);
                    r4.setRefreshing(false);
                    return;
                }
            }
            throw C12960it.A0U("SwipeRefreshLayout does not contain RenderTreeHostView child");
        } else {
            return;
        }
        activity.getWindow().clearFlags(DefaultCrypto.BUFFER_SIZE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:203:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x018e  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x01e8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.view.View r21, X.C14260l7 r22, X.AnonymousClass28D r23, java.lang.Object r24) {
        /*
        // Method dump skipped, instructions count: 1252
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2k7.A06(android.view.View, X.0l7, X.28D, java.lang.Object):void");
    }

    public void A07(View view, C14260l7 r8, AnonymousClass28D r9, Object obj) {
        if (this instanceof C57872nl) {
            VideoSurfaceView videoSurfaceView = (VideoSurfaceView) AnonymousClass028.A0D(view, R.id.video_view);
            videoSurfaceView.A00();
            videoSurfaceView.A04(false);
        } else if (this instanceof C57862nk) {
            ImageView imageView = (ImageView) view;
            imageView.setImageBitmap(null);
            imageView.setImageDrawable(null);
            imageView.setColorFilter((ColorFilter) null);
        } else if (this instanceof C57852nj) {
            ImageView imageView2 = (ImageView) view;
            imageView2.setImageDrawable(null);
            imageView2.setTag(null);
            C12990iw.A1E(imageView2);
            imageView2.setColorFilter((ColorFilter) null);
            imageView2.setScaleX(1.0f);
        } else if (this instanceof C57902no) {
        } else {
            if (this instanceof C57842ni) {
                C57842ni r2 = (C57842ni) this;
                r2.A01.Af9((AnonymousClass5RJ) AnonymousClass3JV.A04(r8, r2.A00));
            } else if (this instanceof C57832nh) {
                C12990iw.A1G((TextView) view);
            } else if (this instanceof C57792nd) {
                C004602b r7 = (C004602b) view;
                C12990iw.A1G(r7);
                r7.setGravity(8388659);
                r7.setMovementMethod(null);
                ((TextEmojiLabel) r7).A07 = null;
            } else if (!(this instanceof C57822ng) && !(this instanceof C57782nc)) {
                if (this instanceof C57772nb) {
                    C12990iw.A0R(view, R.id.recycler_view).setAdapter(null);
                } else if (this instanceof C57892nn) {
                    C67823Ta r5 = (C67823Ta) AnonymousClass3JV.A04(r8, r9);
                    r5.A04 = null;
                    if (((ViewGroup) AnonymousClass028.A0D(view, R.id.container)).getChildCount() != 0) {
                        TextInputLayout textInputLayout = (TextInputLayout) AnonymousClass028.A0D(view, R.id.default_text_input_layout);
                        EditText editText = (EditText) AnonymousClass028.A0D(textInputLayout, R.id.text_view);
                        r5.A00 = editText.getSelectionEnd();
                        r5.A01 = editText.getSelectionStart();
                        r5.A05 = C12970iu.A0p(editText);
                        editText.removeTextChangedListener(r5.A03);
                        editText.setText("");
                        TextWatcher textWatcher = r5.A02;
                        if (textWatcher != null) {
                            editText.removeTextChangedListener(textWatcher);
                        }
                        editText.setFilters(AnonymousClass3I5.A00);
                        editText.setTypeface(Typeface.DEFAULT);
                        editText.setEnabled(true);
                        editText.setFocusableInTouchMode(true);
                        editText.setFocusable(true);
                        editText.setCursorVisible(true);
                        textInputLayout.setPasswordVisibilityToggleEnabled(true);
                        textInputLayout.setError(null);
                        textInputLayout.setErrorEnabled(false);
                        textInputLayout.setHint("");
                    }
                } else if (this instanceof C57812nf) {
                    TextView A0I = C12960it.A0I(view, R.id.hintOrDate);
                    A0I.setOnClickListener(null);
                    A0I.setText("");
                    A0I.setEnabled(false);
                    A0I.setClickable(false);
                    ((TextInputLayout) AnonymousClass028.A0D(view, R.id.inputView)).setHint("");
                } else if (!(this instanceof C57802ne)) {
                    ((AnonymousClass0BD) view).A0N = null;
                } else {
                    C57802ne r22 = (C57802ne) this;
                    C89274Ji r1 = (C89274Ji) AnonymousClass3JV.A04(r8, r22.A00);
                    CountDownTimer countDownTimer = r1.A00;
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                        r1.A00 = null;
                    }
                    r22.A01.AfA(view);
                }
            }
        }
    }

    @Override // X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        int i;
        if (this instanceof C57872nl) {
            i = R.layout.bloks_video_view;
        } else if ((this instanceof C57862nk) || (this instanceof C57852nj)) {
            return new WaImageView(context);
        } else {
            if (this instanceof C57902no) {
                i = R.layout.wa_bloks_code_input;
            } else if (this instanceof C57842ni) {
                i = R.layout.bloks_camera_view;
            } else if ((this instanceof C57832nh) || (this instanceof C57792nd)) {
                return new TextEmojiLabel(context);
            } else {
                if (this instanceof C57822ng) {
                    i = R.layout.range_slider;
                } else if (this instanceof C57782nc) {
                    i = R.layout.wa_bloks_progress_bar;
                } else if (this instanceof C57772nb) {
                    i = R.layout.wa_list_view;
                } else if (this instanceof C57892nn) {
                    i = R.layout.wa_bloks_text_input;
                } else if (this instanceof C57812nf) {
                    i = R.layout.date_picker;
                } else if (this instanceof C57802ne) {
                    i = R.layout.count_down_timer;
                } else if (!(this instanceof C57882nm)) {
                    C15190mi r1 = new C15190mi(context);
                    r1.setBackgroundResource(17170445);
                    return r1;
                } else {
                    AnonymousClass0BD r4 = new AnonymousClass0BD(context);
                    r4.addView(new C55982k5(context), C12990iw.A0M());
                    TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(C88484Fv.A00);
                    ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(1);
                    if (colorStateList != null) {
                        r4.setColorSchemeColors(colorStateList.getDefaultColor());
                    }
                    ColorStateList colorStateList2 = obtainStyledAttributes.getColorStateList(0);
                    if (colorStateList2 != null) {
                        r4.setProgressBackgroundColorSchemeColor(colorStateList2.getDefaultColor());
                    }
                    return r4;
                }
            }
        }
        return C12980iv.A0N(context, i);
    }
}
