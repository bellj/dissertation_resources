package X;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2yR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60692yR extends C60862yl {
    public boolean A00;
    public final View.OnClickListener A01;

    public C60692yR(Context context, View.OnClickListener onClickListener, AbstractC16130oV r4) {
        super(context, null, r4);
        A0Z();
        this.A01 = onClickListener;
    }

    @Override // X.AbstractC60702yS, X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            AnonymousClass1OY.A0O(A08, this);
        }
    }

    @Override // X.C60862yl, X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        this.A01.onClick(this);
    }

    @Override // X.AbstractC28551Oa, android.view.View
    public void onDraw(Canvas canvas) {
        View view = ((C60862yl) this).A00;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = C12960it.A09(this).getDimensionPixelSize(R.dimen.popup_notification_audio_width);
            view.setLayoutParams(layoutParams);
        }
        view.setBackground(C12970iu.A0C(view.getContext(), R.drawable.balloon_centered_normal));
        ((AbstractC28551Oa) this).A0Q = false;
        view.setPadding(getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_horizontal), getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_top), C12990iw.A07(this, R.dimen.date_balloon_alt_padding_horizontal), getResources().getDimensionPixelSize(R.dimen.date_balloon_alt_padding_bottom));
        super.onDraw(canvas);
    }
}
