package X;

import com.google.android.exoplayer2.Timeline;
import com.whatsapp.videoplayback.ExoPlaybackControlView;
import com.whatsapp.videoplayback.ExoPlayerErrorFrame;
import java.util.List;

/* renamed from: X.3Si  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67653Si implements AnonymousClass5XZ, AnonymousClass5SS, AnonymousClass5QR {
    public final /* synthetic */ C47482Aw A00;

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AQ4(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARX(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARY(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ASS(AnonymousClass4XL r1, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATm(boolean z, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public void ATo(C94344be r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATq(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATr(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public void ATs(AnonymousClass3A1 r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATx(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AVk() {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AWU(List list) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXV(Timeline timeline, int i) {
        AnonymousClass4DD.A00(this, timeline, i);
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXW(Timeline timeline, Object obj, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public void AXl(C100564m7 r1, C92524Wg r2) {
    }

    public /* synthetic */ C67653Si(C47482Aw r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5SS
    public void AOn(List list) {
        this.A00.A0C.AOn(list);
    }

    @Override // X.AnonymousClass5XZ
    public void ATt(boolean z, int i) {
        AnonymousClass3FK r1;
        C47482Aw r2 = this.A00;
        r2.A00 = i;
        if (i == 3) {
            r1 = r2.A03;
        } else {
            ExoPlaybackControlView exoPlaybackControlView = r2.A02;
            if (exoPlaybackControlView == null || i != 4) {
                r1 = r2.A03;
                if (i == 2) {
                    if (r1 == null) {
                        return;
                    }
                    if (r2.A07) {
                        r1.A01(r2.A06);
                        return;
                    }
                    ExoPlayerErrorFrame exoPlayerErrorFrame = r1.A03;
                    exoPlayerErrorFrame.setLoadingViewVisibility(0);
                    C12970iu.A1G(exoPlayerErrorFrame.A02);
                    return;
                }
            } else {
                if (!exoPlaybackControlView.A07()) {
                    r2.A02.A01();
                }
                AnonymousClass3FK r0 = r2.A03;
                if (r0 != null) {
                    r0.A00();
                }
                C47492Ax r12 = r2.A01;
                AnonymousClass009.A05(r12);
                r12.AcW(false);
                C47492Ax r3 = r2.A01;
                r3.AbS(r3.ACF(), 0);
                return;
            }
        }
        if (r1 != null) {
            r1.A00();
        }
    }
}
