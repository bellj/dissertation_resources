package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0HG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HG extends AbstractC08070aX {
    public Paint A00 = new Paint();
    public AnonymousClass0QR A01;
    public final RectF A02 = new RectF();
    public final RectF A03 = new RectF();
    public final List A04 = new ArrayList();

    public AnonymousClass0HG(C05540Py r10, AnonymousClass0AA r11, AnonymousClass0PU r12, List list) {
        super(r11, r12);
        int i;
        AbstractC08070aX r0;
        AbstractC08070aX r2;
        AnonymousClass0H9 r02 = r12.A0A;
        if (r02 != null) {
            AnonymousClass0H1 r03 = new AnonymousClass0H1(r02.A00);
            this.A01 = r03;
            A03(r03);
            this.A01.A07.add(this);
        } else {
            this.A01 = null;
        }
        AnonymousClass036 r4 = new AnonymousClass036(r10.A07.size());
        int size = list.size() - 1;
        AbstractC08070aX r6 = null;
        while (true) {
            if (size >= 0) {
                AnonymousClass0PU r8 = (AnonymousClass0PU) list.get(size);
                AnonymousClass0JI r22 = r8.A0E;
                switch (r22.ordinal()) {
                    case 0:
                        r2 = new AnonymousClass0HG(r10, r11, r8, (List) r10.A0B.get(r8.A0H));
                        break;
                    case 1:
                        r2 = new AnonymousClass0HE(r11, r8);
                        break;
                    case 2:
                        r2 = new AnonymousClass0HF(r11, r8);
                        break;
                    case 3:
                        r2 = new AnonymousClass0HC(r11, r8);
                        break;
                    case 4:
                        r2 = new AnonymousClass0HD(r11, r8);
                        break;
                    case 5:
                        r2 = new AnonymousClass0HH(r11, r8);
                        break;
                    default:
                        StringBuilder sb = new StringBuilder("Unknown layer type ");
                        sb.append(r22);
                        AnonymousClass0R5.A00(sb.toString());
                        continue;
                        size--;
                }
                r4.A09(r2.A0M.A07, r2);
                if (r6 != null) {
                    r6.A03 = r2;
                    r6 = null;
                } else {
                    this.A04.add(0, r2);
                    switch (r8.A0F.ordinal()) {
                        case 1:
                        case 2:
                            r6 = r2;
                            continue;
                    }
                }
                size--;
            }
        }
        for (i = 0; i < r4.A00(); i++) {
            AbstractC08070aX r23 = (AbstractC08070aX) r4.A04(r4.A01(i), null);
            if (!(r23 == null || (r0 = (AbstractC08070aX) r4.A04(r23.A0M.A08, null)) == null)) {
                r23.A04 = r0;
            }
        }
    }

    @Override // X.AbstractC08070aX
    public void A01(float f) {
        super.A01(f);
        AnonymousClass0QR r4 = this.A01;
        if (r4 != null) {
            C05540Py r0 = this.A0K.A04;
            C05540Py r2 = this.A0M.A09;
            f = ((((Number) r4.A03()).floatValue() * r2.A01) - r2.A02) / ((r0.A00 - r0.A02) + 0.01f);
        }
        if (this.A01 == null) {
            AnonymousClass0PU r02 = this.A0M;
            float f2 = r02.A00;
            C05540Py r03 = r02.A09;
            f -= f2 / (r03.A00 - r03.A02);
        }
        float f3 = this.A0M.A01;
        if (f3 != 0.0f) {
            f /= f3;
        }
        List list = this.A04;
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                ((AbstractC08070aX) list.get(size)).A01(f);
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC08070aX
    public void A04(C06430To r4, C06430To r5, List list, int i) {
        int i2 = 0;
        while (true) {
            List list2 = this.A04;
            if (i2 < list2.size()) {
                ((AbstractC08070aX) list2.get(i2)).Aan(r4, r5, list, i);
                i2++;
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC08070aX
    public void A05(boolean z) {
        super.A05(z);
        for (AbstractC08070aX r0 : this.A04) {
            r0.A05(z);
        }
    }

    @Override // X.AbstractC08070aX
    public void A06(Canvas canvas, Matrix matrix, int i) {
        RectF rectF = this.A02;
        AnonymousClass0PU r1 = this.A0M;
        rectF.set(0.0f, 0.0f, (float) r1.A03, (float) r1.A02);
        matrix.mapRect(rectF);
        if (!this.A0K.A0C || this.A04.size() <= 1 || i == 255) {
            canvas.save();
        } else {
            Paint paint = this.A00;
            paint.setAlpha(i);
            AnonymousClass0UV.A03(canvas, paint, rectF, 31);
            i = 255;
        }
        List list = this.A04;
        for (int size = list.size() - 1; size >= 0; size--) {
            if (rectF.isEmpty() || canvas.clipRect(rectF)) {
                ((AbstractC08070aX) list.get(size)).A9C(canvas, matrix, i);
            }
        }
        canvas.restore();
        AnonymousClass0MI.A00();
    }

    @Override // X.AbstractC08070aX, X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        super.A5q(r3, obj);
        if (obj != AbstractC12810iX.A0J) {
            return;
        }
        if (r3 == null) {
            AnonymousClass0QR r1 = this.A01;
            if (r1 != null) {
                r1.A08(null);
                return;
            }
            return;
        }
        AnonymousClass0Gu r0 = new AnonymousClass0Gu(r3, null);
        this.A01 = r0;
        r0.A07.add(this);
        A03(this.A01);
    }

    @Override // X.AbstractC08070aX, X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        super.AAy(matrix, rectF, z);
        List list = this.A04;
        for (int size = list.size() - 1; size >= 0; size--) {
            RectF rectF2 = this.A03;
            rectF2.set(0.0f, 0.0f, 0.0f, 0.0f);
            ((AbstractC08070aX) list.get(size)).AAy(this.A08, rectF2, true);
            rectF.union(rectF2);
        }
    }
}
