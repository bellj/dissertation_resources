package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57422n1 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57422n1 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;

    static {
        C57422n1 r0 = new C57422n1();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57422n1 r9 = (C57422n1) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A01;
                int i3 = r9.A00;
                this.A01 = r8.Afp(i2, r9.A01, A1R, C12960it.A1R(i3));
                this.A04 = r8.Afp(this.A04, r9.A04, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A02 = r8.Afp(this.A02, r9.A02, C12960it.A1V(i & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A03 = r8.Afp(this.A03, r9.A03, C12960it.A1V(i & 8, 8), C12960it.A1V(i3 & 8, 8));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 8) {
                            this.A00 |= 1;
                            this.A01 = r82.A02();
                        } else if (A03 == 16) {
                            this.A00 |= 2;
                            this.A04 = r82.A02();
                        } else if (A03 == 24) {
                            this.A00 |= 4;
                            this.A02 = r82.A02();
                        } else if (A03 == 32) {
                            this.A00 |= 8;
                            this.A03 = r82.A02();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57422n1();
            case 5:
                return new C81483u5();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C57422n1.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A02(1, this.A01, 0);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A02(2, this.A04, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A02(3, this.A02, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A02(4, this.A03, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A02);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
