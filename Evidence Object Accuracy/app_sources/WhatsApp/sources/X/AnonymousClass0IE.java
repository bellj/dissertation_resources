package X;

import android.view.View;
import java.lang.ref.Reference;
import java.util.ListIterator;

/* renamed from: X.0IE  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IE extends AnonymousClass0eL {
    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        ListIterator listIterator = AnonymousClass03N.A08.listIterator();
        while (listIterator.hasNext()) {
            AnonymousClass04P r0 = (AnonymousClass04P) ((Reference) listIterator.next()).get();
            if (r0 != null) {
                ((View) r0).invalidate();
            } else {
                listIterator.remove();
            }
        }
    }
}
