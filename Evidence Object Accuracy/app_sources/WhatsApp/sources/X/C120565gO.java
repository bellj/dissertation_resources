package X;

import android.content.Context;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5gO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120565gO extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C18650sn A02;
    public final C120475gF A03;
    public final C18590sh A04;

    public C120565gO(Context context, C14900mE r2, C18650sn r3, C64513Fv r4, C18610sj r5, C120475gF r6, C18590sh r7) {
        super(r4, r5);
        this.A00 = context;
        this.A01 = r2;
        this.A04 = r7;
        this.A03 = r6;
        this.A02 = r3;
    }

    public static final void A00(AbstractC28901Pl r2, HashMap hashMap, List list) {
        String A00;
        if (!(hashMap == null || (A00 = C1308460e.A00("MPIN", hashMap)) == null)) {
            C117295Zj.A1O("mpin", A00, list);
        }
        if (r2 != null) {
            C117295Zj.A1O("credential-id", r2.A0A, list);
            C119755f3 r0 = (C119755f3) r2.A08;
            if (r0 != null) {
                AnonymousClass1ZR r1 = r0.A06;
                if (!AnonymousClass1ZS.A03(r1)) {
                    C117295Zj.A1O("upi-bank-info", (String) C117295Zj.A0R(r1), list);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A01(X.AnonymousClass60O r9, X.C119835fB r10, java.lang.String r11, java.util.List r12, boolean r13) {
        /*
            X.60R r2 = r10.A0B
            if (r2 == 0) goto L_0x00bc
            long r0 = r2.A02
            r7 = 1000(0x3e8, double:4.94E-321)
            r5 = 0
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0019
            long r0 = r0 / r7
            java.lang.String r4 = "start-ts"
            X.1W9 r3 = new X.1W9
            r3.<init>(r4, r0)
            r12.add(r3)
        L_0x0019:
            long r0 = r2.A01
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x002a
            long r0 = r0 / r7
            java.lang.String r4 = "end-ts"
            X.1W9 r3 = new X.1W9
            r3.<init>(r4, r0)
            r12.add(r3)
        L_0x002a:
            java.lang.String r1 = r2.A0F
            if (r1 == 0) goto L_0x0033
            java.lang.String r0 = "amount-rule"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x0033:
            java.lang.String r1 = r2.A0E
            if (r1 == 0) goto L_0x003c
            java.lang.String r0 = "frequency-rule"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x003c:
            X.1ZR r1 = r2.A07
            boolean r0 = X.AnonymousClass1ZS.A03(r1)
            if (r0 != 0) goto L_0x004f
            java.lang.Object r1 = X.C117295Zj.A0R(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r0 = "mandate-name"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x004f:
            boolean r0 = r2.A0L
            if (r0 == 0) goto L_0x00c7
            java.lang.String r1 = "1"
        L_0x0055:
            java.lang.String r0 = "is-revocable"
            X.C117295Zj.A1O(r0, r1, r12)
            java.lang.String r3 = "seq-no"
            if (r11 == 0) goto L_0x00bd
            X.1W9 r1 = new X.1W9
            r1.<init>(r3, r11)
        L_0x0063:
            r12.add(r1)
        L_0x0066:
            if (r9 == 0) goto L_0x0081
            boolean r0 = r9.A01()
            if (r0 == 0) goto L_0x0081
            X.1ZR r1 = r9.A02
            boolean r0 = X.AnonymousClass1ZS.A02(r1)
            if (r0 != 0) goto L_0x0081
            java.lang.Object r1 = X.C117295Zj.A0R(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r0 = "mandate-update-info"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x0081:
            if (r13 == 0) goto L_0x00bc
            java.lang.String r0 = r2.A0J
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0092
            java.lang.String r1 = r2.A0J
            java.lang.String r0 = "recurrence-rule"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x0092:
            java.lang.String r0 = r2.A0I
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00a1
            java.lang.String r1 = r2.A0I
            java.lang.String r0 = "recurrence-day"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x00a1:
            X.1ZR r0 = r2.A0A
            if (r0 == 0) goto L_0x00bc
            java.lang.Object r0 = r0.A00
            java.lang.CharSequence r0 = (java.lang.CharSequence) r0
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00bc
            X.1ZR r0 = r2.A0A
            java.lang.Object r1 = X.C117295Zj.A0R(r0)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r0 = "purpose-code"
            X.C117295Zj.A1O(r0, r1, r12)
        L_0x00bc:
            return
        L_0x00bd:
            java.lang.String r0 = r10.A0N
            if (r0 == 0) goto L_0x0066
            X.1W9 r1 = new X.1W9
            r1.<init>(r3, r0)
            goto L_0x0063
        L_0x00c7:
            java.lang.String r1 = "0"
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120565gO.A01(X.60O, X.5fB, java.lang.String, java.util.List, boolean):void");
    }

    public final void A02(AnonymousClass1IR r5, List list) {
        C117295Zj.A1O("id", r5.A0K, list);
        C117295Zj.A1O("device-id", this.A04.A01(), list);
        AbstractC30891Zf r3 = r5.A0A;
        AnonymousClass009.A05(r3);
        C119835fB r32 = (C119835fB) r3;
        AnonymousClass60R r2 = r32.A0B;
        AnonymousClass009.A05(r2);
        C117295Zj.A1O("sender-vpa", r32.A0L, list);
        if (!TextUtils.isEmpty(r32.A0M)) {
            C117295Zj.A1O("sender-vpa-id", r32.A0M, list);
        }
        C117295Zj.A1O("receiver-vpa", r32.A0J, list);
        if (!TextUtils.isEmpty(r32.A0K)) {
            C117295Zj.A1O("receiver-vpa-id", r32.A0K, list);
        }
        if (!AnonymousClass1ZS.A02(r2.A08)) {
            C117295Zj.A1O("mandate-no", (String) C117295Zj.A0R(r2.A08), list);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1V8[] A03(X.AnonymousClass1IR r10) {
        /*
            r9 = this;
            java.util.ArrayList r1 = X.C12960it.A0l()
            X.1Zf r0 = r10.A0A
            if (r0 == 0) goto L_0x0070
            X.20C r2 = r0.A01
            if (r2 == 0) goto L_0x0070
            X.0sj r3 = r9.A01
            int r0 = r2.A01()
            long r7 = (long) r0
            int r6 = r2.A00
            X.1Yv r4 = r2.A01
            java.lang.String r5 = "amount"
            X.1V8 r0 = r3.A05(r4, r5, r6, r7)
        L_0x001d:
            r1.add(r0)
        L_0x0020:
            X.1Zf r0 = r10.A0A
            X.5fB r0 = (X.C119835fB) r0
            X.60R r2 = r0.A0B
            X.AnonymousClass009.A05(r2)
            X.20C r0 = r2.A05
            if (r0 == 0) goto L_0x0050
            X.0sj r3 = r9.A01
            int r0 = r0.A01()
            long r7 = (long) r0
            X.20C r0 = r2.A05
            int r6 = r0.A00
            X.1Yv r4 = r0.A01
            java.lang.String r5 = "original-amount"
            X.1V8 r0 = r3.A05(r4, r5, r6, r7)
        L_0x0040:
            r1.add(r0)
        L_0x0043:
            int r0 = r1.size()
            X.1V8[] r0 = new X.AnonymousClass1V8[r0]
            java.lang.Object[] r0 = r1.toArray(r0)
            X.1V8[] r0 = (X.AnonymousClass1V8[]) r0
            return r0
        L_0x0050:
            X.1ZR r2 = r2.A09
            boolean r0 = X.AnonymousClass1ZS.A03(r2)
            if (r0 != 0) goto L_0x0043
            java.lang.Object r2 = r2.A00
            java.lang.String r2 = (java.lang.String) r2
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r0.<init>(r2)
            X.1Yv r4 = X.C30771Yt.A05
            X.1Yy r3 = X.C117295Zj.A0D(r4, r0)
            X.0sj r2 = r9.A01
            java.lang.String r0 = "original-amount"
            X.1V8 r0 = r2.A04(r4, r3, r0)
            goto L_0x0040
        L_0x0070:
            X.1Yy r4 = r10.A08
            if (r4 == 0) goto L_0x0020
            X.0sj r3 = r9.A01
            X.1Yv r2 = X.C30771Yt.A05
            java.lang.String r0 = "amount"
            X.1V8 r0 = r3.A04(r2, r4, r0)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120565gO.A03(X.1IR):X.1V8[]");
    }
}
