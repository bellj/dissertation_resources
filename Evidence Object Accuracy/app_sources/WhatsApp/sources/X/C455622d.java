package X;

import com.whatsapp.util.Log;

/* renamed from: X.22d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C455622d {
    public final AbstractC15710nm A00;
    public final C16630pM A01;
    public final String A02 = "ctwa_ads_conversions_for_sending";

    public C455622d(AbstractC15710nm r2, C16630pM r3) {
        this.A00 = r2;
        this.A01 = r3;
    }

    public final void A00(String str, Exception exc) {
        AbstractC15710nm r2 = this.A00;
        StringBuilder sb = new StringBuilder();
        sb.append(exc.getClass().getSimpleName());
        sb.append(" : ");
        sb.append(exc.getMessage());
        r2.AaV("ClickToWhatsAppAdsConversionStore/getConversionFromJson", sb.toString(), false);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(exc);
        Log.e(sb2.toString());
    }
}
