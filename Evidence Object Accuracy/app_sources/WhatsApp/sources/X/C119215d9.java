package X;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.components.Button;
import com.whatsapp.components.FloatingActionButton;
import com.whatsapp.payments.ui.widget.PaymentAmountInputField;
import java.util.List;

/* renamed from: X.5d9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119215d9 extends AbstractC15280mr implements AnonymousClass6M4 {
    public int A00;
    public Handler A01;
    public View A02;
    public TextView A03;
    public ConstraintLayout A04;
    public RecyclerView A05;
    public ShimmerFrameLayout A06;
    public Button A07;
    public C30921Zi A08;
    public C118615c6 A09;
    public final ImageView A0A;
    public final TextView A0B;
    public final TextView A0C;
    public final TextView A0D;
    public final FloatingActionButton A0E;
    public final C22540zF A0F;
    public final C22460z7 A0G;
    public final PaymentAmountInputField A0H;
    public final C134146Dm A0I;
    public final C128295vs A0J;
    public final List A0K = C12960it.A0l();

    public C119215d9(Activity activity, ImageView imageView, TextView textView, TextView textView2, TextView textView3, AbstractC15710nm r14, AbstractC49822Mw r15, FloatingActionButton floatingActionButton, AnonymousClass01d r17, C14820m6 r18, C22540zF r19, C22460z7 r20, PaymentAmountInputField paymentAmountInputField, C134146Dm r22, C128295vs r23, C252718t r24) {
        super(activity, r14, r15, r17, r18, r24);
        this.A0G = r20;
        this.A0F = r19;
        this.A0E = floatingActionButton;
        this.A0I = r22;
        this.A0H = paymentAmountInputField;
        this.A0B = textView;
        this.A0C = textView2;
        this.A0J = r23;
        this.A0D = textView3;
        this.A0A = imageView;
    }

    public static /* synthetic */ void A00(C119215d9 r2) {
        r2.A06.setVisibility(8);
        r2.A05.setVisibility(8);
        r2.A03.setText(R.string.loading_failed_expressive_background_themes);
        r2.A04.setVisibility(0);
        C117295Zj.A0n(r2.A07, r2, 119);
    }

    @Override // X.AbstractC15280mr
    public int A03(int i) {
        return this.A00;
    }

    @Override // X.AbstractC15280mr
    public void A06() {
        if (!isShowing()) {
            super.A06();
            C134146Dm r1 = this.A0I;
            if (C252718t.A00(r1.A09)) {
                AbstractC49822Mw r8 = super.A05;
                KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) r8;
                keyboardPopupLayout.A06 = true;
                InputMethodManager A0Q = super.A06.A0Q();
                AnonymousClass009.A05(A0Q);
                if (!A0Q.hideSoftInputFromWindow(r1.A09.getWindowToken(), 0, new ResultReceiverC73413gC(C12970iu.A0E(), new Runnable() { // from class: X.6Gx
                    @Override // java.lang.Runnable
                    public final void run() {
                        C119215d9.this.A0A();
                    }
                }, super.A0A))) {
                    keyboardPopupLayout.A06 = false;
                    ((View) r8).requestLayout();
                    return;
                }
                return;
            }
            A0A();
        }
    }

    public final void A0A() {
        A05();
        if (this.A02 == null) {
            Activity activity = super.A03;
            LinearLayout linearLayout = new LinearLayout(activity);
            View inflate = activity.getLayoutInflater().inflate(R.layout.expressive_background_selection_fragment, (ViewGroup) linearLayout, true);
            this.A02 = inflate;
            C117295Zj.A0n(AnonymousClass028.A0D(inflate, R.id.close_button), this, 120);
            this.A03 = C12960it.A0I(this.A02, R.id.themes_title);
            this.A05 = (RecyclerView) AnonymousClass028.A0D(this.A02, R.id.expressive_themes_list);
            C118615c6 r1 = new C118615c6(this.A0F, this.A0G, this);
            this.A09 = r1;
            this.A05.setAdapter(r1);
            this.A06 = (ShimmerFrameLayout) AnonymousClass028.A0D(this.A02, R.id.expressive_bg_loading_shimmer);
            ConstraintLayout constraintLayout = (ConstraintLayout) AnonymousClass028.A0D(this.A02, R.id.backgrounds_download_error_layout);
            this.A04 = constraintLayout;
            this.A07 = (Button) AnonymousClass028.A0D(constraintLayout, R.id.retry_backgrounds_download);
            setContentView(linearLayout);
            setTouchable(true);
            setOutsideTouchable(true);
            setInputMethodMode(2);
            setAnimationStyle(0);
            setBackgroundDrawable(new ColorDrawable(activity.getResources().getColor(R.color.primary_surface)));
            this.A02.measure(View.MeasureSpec.makeMeasureSpec(activity.getWindowManager().getDefaultDisplay().getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
            this.A00 = this.A02.getMeasuredHeight();
            this.A01 = new Handler();
            setTouchInterceptor(new View.OnTouchListener() { // from class: X.64u
                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == 2 || motionEvent.getActionMasked() == 1 || motionEvent.getY() >= 0.0f) {
                        return false;
                    }
                    return true;
                }
            });
        }
        setHeight(this.A00);
        setWidth(-1);
        AbstractC49822Mw r4 = super.A05;
        r4.setKeyboardPopup(this);
        KeyboardPopupLayout keyboardPopupLayout = (KeyboardPopupLayout) r4;
        if (keyboardPopupLayout.A06) {
            View view = (View) r4;
            view.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass650(this));
            keyboardPopupLayout.A06 = false;
            view.requestLayout();
        } else if (!isShowing()) {
            showAtLocation((View) r4, 48, 0, 1000000);
            C134146Dm r2 = this.A0I;
            r2.A02.setVisibility(8);
            View view2 = r2.A01;
            if (view2 != null) {
                view2.setVisibility(8);
            }
        }
        this.A0E.A03(true);
        A0B(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0044, code lost:
        if (java.lang.System.currentTimeMillis() < X.C12980iv.A0E(r8, "payment_backgrounds_backoff_timestamp")) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B(boolean r12) {
        /*
            r11 = this;
            androidx.constraintlayout.widget.ConstraintLayout r1 = r11.A04
            r0 = 8
            r1.setVisibility(r0)
            com.facebook.shimmer.ShimmerFrameLayout r1 = r11.A06
            r0 = 0
            r1.setVisibility(r0)
            android.widget.TextView r1 = r11.A03
            r0 = 2131889192(0x7f120c28, float:1.941304E38)
            r1.setText(r0)
            X.0z7 r4 = r11.A0G
            X.6B1 r3 = new X.6B1
            r3.<init>(r11)
            X.0zJ r7 = r4.A0B
            if (r12 != 0) goto L_0x0053
            X.0m6 r10 = r7.A01
            java.lang.String r9 = "payment_backgrounds_last_fetch_timestamp"
            android.content.SharedPreferences r8 = r10.A00
            long r5 = X.C12980iv.A0E(r8, r9)
            r1 = -1
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0053
            long r0 = X.C22580zJ.A02
            boolean r0 = r10.A1J(r9, r0)
            if (r0 == 0) goto L_0x0046
            java.lang.String r0 = "payment_backgrounds_backoff_timestamp"
            long r5 = java.lang.System.currentTimeMillis()
            long r1 = X.C12980iv.A0E(r8, r0)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0053
        L_0x0046:
            X.0lR r2 = r4.A0D
            r1 = 40
            com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6 r0 = new com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6
            r0.<init>(r4, r1, r3)
            r2.Ab2(r0)
            return
        L_0x0053:
            X.0sm r0 = r7.A00
            boolean r0 = r0.A0B()
            if (r0 == 0) goto L_0x0046
            X.0m6 r9 = r7.A01
            android.content.SharedPreferences r10 = r9.A00
            java.lang.String r8 = "payment_background_backoff_attempt"
            int r0 = X.C12970iu.A01(r10, r8)
            int r7 = r0 + 1
            r5 = 720(0x2d0, double:3.557E-321)
            r0 = 1
            X.1VN r2 = new X.1VN
            r2.<init>(r0, r5)
            long r0 = (long) r7
            r2.A03(r0)
            long r1 = r2.A01()
            r5 = 60000(0xea60, double:2.9644E-319)
            long r1 = r1 * r5
            long r5 = java.lang.System.currentTimeMillis()
            long r1 = r1 + r5
            android.content.SharedPreferences$Editor r0 = r10.edit()
            X.C12970iu.A1B(r0, r8, r7)
            java.lang.String r0 = "payment_backgrounds_backoff_timestamp"
            r9.A0q(r0, r1)
            X.0zI r2 = r4.A09
            r1 = 0
            X.2DP r0 = new X.2DP
            r0.<init>(r3, r4)
            r2.A00(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119215d9.A0B(boolean):void");
    }

    @Override // X.AnonymousClass6M4
    public void AXS(C30921Zi r12) {
        this.A08 = r12;
        ImageView imageView = this.A0A;
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        C30921Zi r2 = this.A08;
        if (r2 != null) {
            layoutParams.height = (int) (((float) layoutParams.width) / (((float) r2.A0D) / ((float) r2.A09)));
            String str = r2.A01;
            if (!TextUtils.isEmpty(str)) {
                imageView.setContentDescription(str);
            }
            this.A0F.A00(imageView, this.A08, layoutParams.width, layoutParams.height);
            PaymentAmountInputField paymentAmountInputField = this.A0H;
            paymentAmountInputField.setTextColor(this.A08.A0C);
            int i = this.A08.A0C;
            paymentAmountInputField.setHintTextColor(Color.argb((int) (((float) Color.alpha(i)) * 0.3f), (int) ((float) Color.red(i)), (int) ((float) Color.green(i)), (int) ((float) Color.blue(i))));
            this.A0B.setTextColor(this.A08.A0C);
            this.A0C.setTextColor(this.A08.A0C);
            TextView textView = this.A0D;
            textView.setTextColor(this.A08.A0B);
            textView.setBackgroundColor(this.A08.A0A);
        } else {
            imageView.setImageResource(R.drawable.payment_default_background);
            PaymentAmountInputField paymentAmountInputField2 = this.A0H;
            C128295vs r10 = this.A0J;
            AnonymousClass04D.A08(paymentAmountInputField2, r10.A00);
            TextView textView2 = this.A0B;
            Pair pair = r10.A02;
            AnonymousClass04D.A08(textView2, C12960it.A05(pair.first));
            TextView textView3 = this.A0C;
            int[] iArr = (int[]) pair.second;
            textView3.setPadding(iArr[0], iArr[1], iArr[2], iArr[3]);
            Pair pair2 = r10.A01;
            AnonymousClass04D.A08(textView3, C12960it.A05(pair2.first));
            int[] iArr2 = (int[]) pair2.second;
            textView3.setPadding(iArr2[0], iArr2[1], iArr2[2], iArr2[3]);
            TextView textView4 = this.A0D;
            C12980iv.A14(super.A03.getResources(), textView4, R.color.payments_currency_amount_text_color);
            textView4.setBackgroundColor(0);
        }
        imageView.setTag(R.id.selected_expressive_background_theme, this.A08);
    }

    @Override // X.AbstractC15280mr, android.widget.PopupWindow
    public void dismiss() {
        this.A0E.A04(true);
        C134146Dm r2 = this.A0I;
        r2.A02.setVisibility(0);
        View view = r2.A01;
        if (view != null) {
            view.setVisibility(0);
        }
        super.dismiss();
    }
}
