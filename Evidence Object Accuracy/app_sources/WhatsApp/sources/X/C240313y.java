package X;

import com.whatsapp.jid.DeviceJid;

/* renamed from: X.13y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C240313y {
    public String A00;
    public final C15570nT A01;
    public final C233411h A02;
    public final C14830m7 A03;
    public final C14950mJ A04;
    public final C15990oG A05;
    public final C16120oU A06;

    public C240313y(C15570nT r1, C233411h r2, C14830m7 r3, C14950mJ r4, C15990oG r5, C16120oU r6) {
        this.A03 = r3;
        this.A01 = r1;
        this.A06 = r6;
        this.A04 = r4;
        this.A02 = r2;
        this.A05 = r5;
    }

    public final String A00() {
        String str = this.A00;
        if (str != null) {
            return str;
        }
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        DeviceJid primaryDevice = r02.getPrimaryDevice();
        C233411h r2 = this.A02;
        String A04 = C233411h.A04(r2.A03.A00.A04().A01, this.A05.A0E(C15940oB.A02(primaryDevice)));
        this.A00 = A04;
        return A04;
    }
}
