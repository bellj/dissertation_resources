package X;

import android.text.TextUtils;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

/* renamed from: X.6Dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134046Dc implements AnonymousClass5WK {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ AbstractC11740gm A01;
    public final /* synthetic */ AbstractC11740gm A02;
    public final /* synthetic */ C130795zz A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ WeakReference A06;
    public final /* synthetic */ WeakReference A07;

    public C134046Dc(ImageView imageView, AbstractC11740gm r2, AbstractC11740gm r3, C130795zz r4, String str, String str2, WeakReference weakReference, WeakReference weakReference2) {
        this.A07 = weakReference;
        this.A06 = weakReference2;
        this.A02 = r2;
        this.A05 = str;
        this.A00 = imageView;
        this.A04 = str2;
        this.A03 = r4;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass5WK
    public void ARw() {
        String str = this.A05;
        if (!TextUtils.isEmpty(str)) {
            ImageView imageView = this.A00;
            String str2 = this.A04;
            C130795zz r2 = this.A03;
            AbstractC11740gm r0 = this.A01;
            AbstractC11740gm r1 = this.A02;
            if (str2 != null && C12970iu.A1Y(r0.get())) {
                str = str2;
            }
            r2.A00(new C1324466x(imageView, r1), str);
        }
    }

    @Override // X.AnonymousClass5WK
    public void AXT() {
        ImageView imageView = (ImageView) this.A07.get();
        Object obj = this.A06.get();
        Number number = (Number) this.A02.get();
        if (imageView != null && obj != null && number != null) {
            imageView.setColorFilter(number.intValue());
        }
    }
}
