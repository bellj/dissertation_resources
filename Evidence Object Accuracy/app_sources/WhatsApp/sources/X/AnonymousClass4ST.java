package X;

import android.content.Context;
import java.util.Map;

/* renamed from: X.4ST  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ST {
    public final Context A00;
    public final AbstractC115225Qr A01;
    public final Map A02 = C12970iu.A11();
    public final Map A03 = C12970iu.A11();
    public final Map A04 = C12970iu.A11();

    public AnonymousClass4ST(Context context, AbstractC115225Qr r3) {
        this.A00 = context;
        this.A01 = r3;
    }
}
