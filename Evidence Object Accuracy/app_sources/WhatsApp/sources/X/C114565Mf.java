package X;

/* renamed from: X.5Mf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114565Mf extends AnonymousClass1TM {
    public AbstractC114775Na A00;
    public AnonymousClass5MA A01;
    public C114725Mv A02;
    public C114575Mg A03;

    public static C114565Mf A00(Object obj) {
        if (obj instanceof C114565Mf) {
            return (C114565Mf) obj;
        }
        if (obj != null) {
            return new C114565Mf(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public C114565Mf(AbstractC114775Na r3) {
        C114575Mg r1;
        this.A00 = r3;
        if (r3.A0B() == 3) {
            AnonymousClass1TN A00 = AbstractC114775Na.A00(r3);
            if (A00 instanceof C114575Mg) {
                r1 = (C114575Mg) A00;
            } else {
                r1 = A00 != null ? new C114575Mg(AbstractC114775Na.A04(A00)) : null;
            }
            this.A03 = r1;
            this.A02 = C114725Mv.A00(AbstractC114775Na.A01(r3));
            this.A01 = AnonymousClass5MA.A00(r3.A0D(2));
            return;
        }
        throw C12970iu.A0f("sequence wrong size for a certificate");
    }
}
