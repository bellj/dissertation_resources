package X;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.DeadObjectException;
import android.os.DeadSystemException;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.core.NetworkStateManager$SubscriptionManagerBasedRoamingDetector;
import com.whatsapp.util.Log;

/* renamed from: X.16u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C247716u {
    public final AbstractC15710nm A00;
    public final AnonymousClass01d A01;

    public C247716u(AbstractC15710nm r1, AnonymousClass01d r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public int A00(boolean z) {
        AnonymousClass01d r1 = this.A01;
        TelephonyManager A0N = r1.A0N();
        ConnectivityManager A0H = r1.A0H();
        int i = 0;
        if (!(A0H == null || A0N == null)) {
            try {
                NetworkInfo activeNetworkInfo = A0H.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    i = 1;
                    if (activeNetworkInfo.getType() != 1) {
                        if (Build.VERSION.SDK_INT >= 24) {
                            Pair determineNetworkStateUsingSubscriptionManager = NetworkStateManager$SubscriptionManagerBasedRoamingDetector.determineNetworkStateUsingSubscriptionManager(r1, z);
                            if (((Boolean) determineNetworkStateUsingSubscriptionManager.first).booleanValue()) {
                                return ((Number) determineNetworkStateUsingSubscriptionManager.second).intValue();
                            }
                        }
                        i = 3;
                        if (!activeNetworkInfo.isRoaming() && !A0N.isNetworkRoaming()) {
                            String simCountryIso = A0N.getSimCountryIso();
                            if (!TextUtils.isEmpty(simCountryIso)) {
                                String simOperator = A0N.getSimOperator();
                                if (!TextUtils.isEmpty(simOperator)) {
                                    if (A0N.getPhoneType() != 2) {
                                        String networkCountryIso = A0N.getNetworkCountryIso();
                                        if (!TextUtils.isEmpty(networkCountryIso) && simCountryIso.equals(networkCountryIso)) {
                                            String networkOperator = A0N.getNetworkOperator();
                                            if (!TextUtils.isEmpty(networkOperator)) {
                                                if (!networkOperator.equals(simOperator) && !C43291wl.A00.contains(new AnonymousClass01T(networkOperator, simOperator))) {
                                                    return 3;
                                                }
                                            }
                                        }
                                    }
                                    return 2;
                                }
                            }
                        }
                    }
                }
            } catch (RuntimeException e) {
                if (!(e.getCause() instanceof DeadObjectException)) {
                    if (Build.VERSION.SDK_INT >= 24 && (e.getCause() instanceof DeadSystemException)) {
                        return 0;
                    }
                    throw e;
                }
            }
        }
        return i;
    }

    public NetworkInfo A01() {
        ConnectivityManager A0H = this.A01.A0H();
        if (A0H != null) {
            return A0H.getActiveNetworkInfo();
        }
        Log.w("NetworkStateManager/getActiveNetworkInfo cm=null");
        return null;
    }

    public boolean A02() {
        if (Build.VERSION.SDK_INT >= 24) {
            ConnectivityManager A0H = this.A01.A0H();
            if (A0H == null) {
                Log.w("NetworkStateManager/isDataSaverOn cm=null");
            } else if (A0H.isActiveNetworkMetered()) {
                A0H.getRestrictBackgroundStatus();
                if (A0H.getRestrictBackgroundStatus() == 3) {
                    return true;
                }
            }
        }
        return false;
    }
}
