package X;

import android.content.Context;

/* renamed from: X.2S6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2S6 extends AbstractC451020e {
    public final /* synthetic */ AnonymousClass2S5 A00;
    public final /* synthetic */ AnonymousClass2S3 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2S6(Context context, C14900mE r2, C18650sn r3, AnonymousClass2S5 r4, AnonymousClass2S3 r5) {
        super(context, r2, r3);
        this.A01 = r5;
        this.A00 = r4;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        this.A00.APo(r2);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        this.A00.APo(r2);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r2) {
        this.A00.AX2(r2);
    }
}
