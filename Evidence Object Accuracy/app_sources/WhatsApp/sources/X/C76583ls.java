package X;

import android.net.Uri;

/* renamed from: X.3ls  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76583ls extends AnonymousClass496 {
    public final Uri uri;

    public C76583ls(String str, Uri uri) {
        super(str);
        this.uri = uri;
    }
}
