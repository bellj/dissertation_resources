package X;

/* renamed from: X.4B6  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4B6 {
    A03(0),
    A02(1),
    A01(2);
    
    public final int value;

    AnonymousClass4B6(int i) {
        this.value = i;
    }
}
