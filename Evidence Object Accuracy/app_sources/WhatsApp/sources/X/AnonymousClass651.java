package X;

import android.view.ViewTreeObserver;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.651  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass651 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ PaymentView A00;

    public AnonymousClass651(PaymentView paymentView) {
        this.A00 = paymentView;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        PaymentView paymentView = this.A00;
        paymentView.A0t.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        paymentView.A0z.A01(1);
    }
}
