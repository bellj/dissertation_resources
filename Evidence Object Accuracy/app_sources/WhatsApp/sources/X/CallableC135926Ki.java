package X;

import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135926Ki implements Callable {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ AnonymousClass60X A01;
    public final /* synthetic */ AnonymousClass66I A02;

    public CallableC135926Ki(CaptureRequest.Builder builder, AnonymousClass60X r2, AnonymousClass66I r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = builder;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C1310360y r4;
        AnonymousClass60X r1 = this.A01;
        AnonymousClass61Q r0 = r1.A02;
        if (r0 == null || !r0.A0Q || (r4 = r1.A02.A09) == null) {
            return this.A02;
        }
        CaptureRequest.Builder builder = this.A00;
        CaptureRequest.Key key = CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER;
        Integer A0i = C12980iv.A0i();
        builder.set(key, A0i);
        CaptureRequest build = builder.build();
        AnonymousClass66I r5 = this.A02;
        r4.A04(build, r5);
        builder.set(key, C12960it.A0V());
        r4.A04(builder.build(), r5);
        builder.set(key, A0i);
        r4.A05(builder.build(), r5);
        return r5;
    }
}
