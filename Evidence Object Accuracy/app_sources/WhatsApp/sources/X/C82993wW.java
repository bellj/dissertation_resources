package X;

import java.util.regex.Pattern;

/* renamed from: X.3wW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82993wW extends AbstractC94534c0 {
    public final String A00;
    public final String A01;
    public final Pattern A02;

    public C82993wW(CharSequence charSequence) {
        String str;
        int i;
        String charSequence2 = charSequence.toString();
        int indexOf = charSequence2.indexOf(47);
        int lastIndexOf = charSequence2.lastIndexOf(47);
        String substring = charSequence2.substring(indexOf + 1, lastIndexOf);
        this.A01 = substring;
        int i2 = lastIndexOf + 1;
        if (charSequence2.length() > i2) {
            str = charSequence2.substring(i2);
        } else {
            str = "";
        }
        this.A00 = str;
        char[] charArray = str.toCharArray();
        int i3 = 0;
        for (char c : charArray) {
            AnonymousClass4BJ[] values = AnonymousClass4BJ.values();
            int length = values.length;
            int i4 = 0;
            while (true) {
                if (i4 >= length) {
                    i = 0;
                    break;
                }
                AnonymousClass4BJ r1 = values[i4];
                if (r1.flag == c) {
                    i = r1.code;
                    break;
                }
                i4++;
            }
            i3 |= i;
        }
        this.A02 = Pattern.compile(substring, i3);
    }

    public C82993wW(Pattern pattern) {
        this.A01 = pattern.pattern();
        this.A02 = pattern;
        int flags = pattern.flags();
        StringBuilder A0h = C12960it.A0h();
        AnonymousClass4BJ[] values = AnonymousClass4BJ.values();
        for (AnonymousClass4BJ r2 : values) {
            int i = r2.code;
            if ((i & flags) == i) {
                A0h.append(r2.flag);
            }
        }
        this.A00 = A0h.toString();
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C82993wW)) {
                return false;
            }
            Pattern pattern = this.A02;
            Pattern pattern2 = ((C82993wW) obj).A02;
            if (pattern != null) {
                if (!pattern.equals(pattern2)) {
                    return false;
                }
            } else if (pattern2 != null) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        String str = this.A01;
        if (str.startsWith("/")) {
            return str;
        }
        StringBuilder A0j = C12960it.A0j("/");
        A0j.append(str);
        A0j.append("/");
        return C12960it.A0d(this.A00, A0j);
    }
}
