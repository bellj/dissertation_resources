package X;

/* renamed from: X.2fB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53762fB extends AnonymousClass07A {
    public final /* synthetic */ AnonymousClass2IQ A00;
    public final /* synthetic */ AbstractC14640lm A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53762fB(AbstractC001500q r2, AnonymousClass2IQ r3, AbstractC14640lm r4) {
        super(null, r2);
        this.A00 = r3;
        this.A01 = r4;
    }

    @Override // X.AnonymousClass07A
    public AnonymousClass015 A02(AnonymousClass07E r21, Class cls, String str) {
        AnonymousClass2IQ r0 = this.A00;
        AbstractC14640lm r14 = this.A01;
        C48302Fl r1 = r0.A00;
        AnonymousClass01J r2 = r1.A03;
        C14850m9 A0S = C12960it.A0S(r2);
        C15650ng r11 = (C15650ng) r2.A4m.get();
        AbstractC14440lR A0T = C12960it.A0T(r2);
        C19990v2 A0c = C12980iv.A0c(r2);
        AnonymousClass018 A0R = C12960it.A0R(r2);
        C241814n r9 = (C241814n) r2.A3D.get();
        C15570nT A0S2 = C12970iu.A0S(r2);
        AbstractC35401hl A0M = r1.A01.A0M();
        C21830y3 r6 = (C21830y3) r2.A4f.get();
        AnonymousClass1AP r5 = (AnonymousClass1AP) r2.A4Z.get();
        AnonymousClass15O r15 = (AnonymousClass15O) r2.A7C.get();
        return new C15360n1(r21, A0S2, r5, r6, C12980iv.A0b(r2), A0R, r9, A0c, r11, (AnonymousClass134) r2.AJg.get(), A0S, r14, r15, (C20320vZ) r2.A7A.get(), (C22190yg) r2.AB6.get(), A0T, A0M);
    }
}
