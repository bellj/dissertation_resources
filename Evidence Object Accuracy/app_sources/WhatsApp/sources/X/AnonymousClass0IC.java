package X;

import android.content.Context;

/* renamed from: X.0IC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IC extends AnonymousClass0IU {
    public String A00;
    public final int A01;

    public AnonymousClass0IC(Context context, int i) {
        super(context, i);
        int i2 = context.getResources().getDisplayMetrics().densityDpi;
        int i3 = 320;
        if (i2 <= 320) {
            i3 = 250;
            if (i2 <= 250) {
                i3 = 72;
            }
        }
        this.A01 = i3;
    }
}
