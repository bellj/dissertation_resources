package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1fR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC34011fR {
    public String A00;

    public String A00() {
        StringBuilder sb;
        String str;
        int i;
        String obj;
        if (this instanceof C34181fi) {
            return "web-status-seen";
        }
        if (this instanceof AnonymousClass2CW) {
            AnonymousClass2CW r1 = (AnonymousClass2CW) this;
            sb = new StringBuilder("qr_response type: ");
            sb.append(r1.A00);
            sb.append(" id: ");
            sb.append(r1.A03);
            sb.append(" #details: ");
            List list = r1.A04;
            str = list != null ? Integer.valueOf(list.size()) : "-";
        } else if (this instanceof C34161fg) {
            sb = new StringBuilder("qr_msg_recv ");
            str = ((C34161fg) this).A02;
        } else if (this instanceof AnonymousClass2D9) {
            sb = new StringBuilder("web_identity_changed/");
            str = ((AnonymousClass2D9) this).A00;
        } else if (this instanceof C49092Jc) {
            return "web-frequent-contacts";
        } else {
            if (this instanceof C34111fb) {
                C34111fb r4 = (C34111fb) this;
                StringBuilder sb2 = new StringBuilder();
                for (AbstractC15340mz r0 : r4.A03) {
                    sb2.append(" ");
                    sb2.append(r0.A0z);
                }
                sb = new StringBuilder("qr_del_msgs ");
                sb.append(r4.A01);
                sb.append((Object) sb2);
                sb.append(" ");
                i = r4.A00;
            } else if (this instanceof C34091fZ) {
                return ((C34091fZ) this).A01.toString();
            } else {
                if (this instanceof C34211fl) {
                    C34211fl r12 = (C34211fl) this;
                    sb = new StringBuilder("qr_chat_seen/");
                    sb.append(r12.A00);
                    sb.append("/");
                    sb.append(r12.A02);
                    return sb.toString();
                } else if (this instanceof C34001fQ) {
                    sb = new StringBuilder("qr_bclist_recipients ");
                    str = ((C34001fQ) this).A00;
                } else if (!(this instanceof AnonymousClass22L)) {
                    AnonymousClass2CX r3 = (AnonymousClass2CX) this;
                    String str2 = r3.A04;
                    if (str2 == null) {
                        obj = "most recent msgs";
                    } else {
                        StringBuilder sb3 = new StringBuilder("msg query id: ");
                        sb3.append(str2);
                        obj = sb3.toString();
                    }
                    sb = new StringBuilder("qr_msgs/");
                    sb.append(obj);
                    sb.append("/fwdType:");
                    sb.append(r3.A00);
                    sb.append("/qryType:");
                    sb.append(r3.A01);
                    sb.append("/firstUnread:");
                    sb.append(r3.A03);
                    sb.append("/#msgs:");
                    i = r3.A05.size();
                } else {
                    AnonymousClass22L r2 = (AnonymousClass22L) this;
                    StringBuilder sb4 = new StringBuilder("qr_star_msgs ");
                    sb4.append(r2.A01);
                    sb4.append(' ');
                    sb4.append(r2.A04);
                    sb4.append(' ');
                    sb4.append(r2.A00);
                    for (AbstractC15340mz r13 : r2.A03) {
                        sb4.append(" ");
                        sb4.append(r13.A0z);
                    }
                    return sb4.toString();
                }
            }
            sb.append(i);
            return sb.toString();
        }
        sb.append(str);
        return sb.toString();
    }

    public void A01() {
        if (this instanceof C34181fi) {
            C34181fi r0 = (C34181fi) this;
            r0.A00.A0A(r0.A01, r0.A02);
        } else if (this instanceof AnonymousClass2CW) {
            AnonymousClass2CW r02 = (AnonymousClass2CW) this;
            r02.A01.A01(r02.A03, r02.A02, r02.A04, r02.A00, r02.A05);
        } else if (this instanceof C34161fg) {
            C34161fg r03 = (C34161fg) this;
            r03.A01.A0B(r03.A02, r03.A00);
        } else if (this instanceof AnonymousClass2D9) {
            AnonymousClass2D9 r04 = (AnonymousClass2D9) this;
            r04.A01.A08(r04.A00);
        } else if (this instanceof C49092Jc) {
            C49092Jc r05 = (C49092Jc) this;
            C22230yk r4 = r05.A00;
            r4.A0F.Ab2(new RunnableBRunnable0Shape0S1100000_I0(28, r05.A01, r4));
        } else if (this instanceof C34111fb) {
            C34111fb r06 = (C34111fb) this;
            r06.A02.A05(r06.A01, r06.A03, r06.A00);
        } else if (this instanceof C34091fZ) {
            C34091fZ r3 = (C34091fZ) this;
            List<C34081fY> list = r3.A01;
            ArrayList arrayList = new ArrayList(list.size());
            for (C34081fY r07 : list) {
                arrayList.add(r07.A00());
            }
            r3.A00.A0E(0, arrayList);
        } else if (this instanceof C34211fl) {
            C34211fl r08 = (C34211fl) this;
            r08.A01.A06(r08.A00, r08.A02);
        } else if (this instanceof C34001fQ) {
            C34001fQ r09 = (C34001fQ) this;
            r09.A01.A03(r09.A00, r09.A02);
        } else if (!(this instanceof AnonymousClass22L)) {
            AnonymousClass2CX r010 = (AnonymousClass2CX) this;
            C22470z8 r1 = r010.A02;
            String str = r010.A04;
            List list2 = r010.A05;
            int i = r010.A00;
            boolean z = r010.A06;
            r1.A00(null, null, r010.A03, str, list2, null, i, r010.A01, false, z);
        } else {
            AnonymousClass22L r011 = (AnonymousClass22L) this;
            r011.A02.A01(r011.A01, r011.A03, r011.A00, r011.A04);
        }
    }

    public String toString() {
        return A00();
    }
}
