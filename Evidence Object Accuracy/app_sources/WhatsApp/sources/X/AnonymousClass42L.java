package X;

import android.graphics.RectF;

/* renamed from: X.42L  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42L extends C61112zQ {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass42L(int r3, boolean r4) {
        /*
            r2 = this;
            if (r4 == 0) goto L_0x000a
            X.3Cj r1 = X.C61112zQ.A04
        L_0x0004:
            X.3Cj r0 = X.C61112zQ.A02
            r2.<init>(r1, r0, r3)
            return
        L_0x000a:
            X.3Cj r1 = X.C61112zQ.A03
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass42L.<init>(int, boolean):void");
    }

    @Override // X.AbstractC65123If
    public RectF A05(int i, int i2) {
        throw C12970iu.A0z();
    }
}
