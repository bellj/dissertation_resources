package X;

/* renamed from: X.5ss  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126435ss {
    public final AnonymousClass1V8 A00;

    public C126435ss(AnonymousClass3CT r10, String str, String str2, String str3, String str4, String str5) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-generate-otp");
        if (str != null && C117295Zj.A1U(str)) {
            C41141sy.A01(A0N, "vpa", str);
        }
        if (str2 != null && C117295Zj.A1U(str2)) {
            C41141sy.A01(A0N, "vpa-id", str2);
        }
        if (C117295Zj.A1V(str3, 1, false)) {
            C41141sy.A01(A0N, "device-id", str3);
        }
        if (C117305Zk.A1X(str4, 1, false)) {
            C41141sy.A01(A0N, "upi-bank-info", str4);
        }
        if (str5 != null && AnonymousClass3JT.A0E(str5, 1, 10, true)) {
            C41141sy.A01(A0N, "provider-type", str5);
        }
        C117295Zj.A1H(A0N, A0M);
        r10.A00(A0M, C117305Zk.A0q(r10.A00, A0M, C12960it.A0l()));
        this.A00 = A0M.A03();
    }
}
