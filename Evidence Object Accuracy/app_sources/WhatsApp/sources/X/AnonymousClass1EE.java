package X;

/* renamed from: X.1EE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EE {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C20020v5 A02;
    public final C19990v2 A03;
    public final C15650ng A04;
    public final C21410xN A05;
    public final C240213x A06;
    public final C18460sU A07;
    public final C16490p7 A08;
    public final AnonymousClass1EA A09;
    public final AnonymousClass1ED A0A;
    public final C16120oU A0B;

    public AnonymousClass1EE(AbstractC15710nm r1, C15570nT r2, C20020v5 r3, C19990v2 r4, C15650ng r5, C21410xN r6, C240213x r7, C18460sU r8, C16490p7 r9, AnonymousClass1EA r10, AnonymousClass1ED r11, C16120oU r12) {
        this.A07 = r8;
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A0B = r12;
        this.A04 = r5;
        this.A02 = r3;
        this.A0A = r11;
        this.A08 = r9;
        this.A05 = r6;
        this.A09 = r10;
        this.A06 = r7;
    }

    public void A00(AbstractC15340mz r5, C30311Wx r6, Integer num, Integer num2) {
        C615830x r3 = new C615830x();
        int i = 1;
        if (r6.A01 != 1) {
            i = 2;
        }
        r3.A01 = Integer.valueOf(i);
        r3.A02 = num;
        r3.A00 = num2;
        r3.A05 = Long.valueOf(r6.A0I);
        if (r5 != null) {
            r3.A04 = Long.valueOf((long) r5.A04);
            Integer A02 = this.A06.A02(r5);
            if (A02 != null) {
                r3.A04 = Long.valueOf((long) A02.intValue());
            }
        }
        AbstractC14640lm r2 = r6.A0z.A00;
        if (r2 != null) {
            r3.A06 = this.A02.A06(r2.getRawString());
            r3.A03 = Long.valueOf((long) this.A03.A01(r2));
        }
        this.A0B.A07(r3);
    }
}
