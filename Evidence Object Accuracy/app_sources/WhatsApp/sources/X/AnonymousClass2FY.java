package X;

import com.whatsapp.accountsync.ProfileActivity;
import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.2FY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2FY extends AbstractActivityC28171Kz {
    public boolean A00 = false;

    public AnonymousClass2FY() {
        A0R(new C102784ph(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ProfileActivity profileActivity = (ProfileActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) profileActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) profileActivity).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) profileActivity).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) profileActivity).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) profileActivity).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) profileActivity).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) profileActivity).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) profileActivity).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) profileActivity).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) profileActivity).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) profileActivity).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) profileActivity).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) profileActivity).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) profileActivity).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) profileActivity).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) profileActivity).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) profileActivity).A09 = r3.A06();
            ((ActivityC13790kL) profileActivity).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) profileActivity).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) profileActivity).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) profileActivity).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) profileActivity).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) profileActivity).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) profileActivity).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) profileActivity).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) profileActivity).A08 = (C249317l) r2.A8B.get();
            ((AbstractActivityC28171Kz) profileActivity).A05 = (C20650w6) r2.A3A.get();
            ((AbstractActivityC28171Kz) profileActivity).A09 = (C18470sV) r2.AK8.get();
            ((AbstractActivityC28171Kz) profileActivity).A02 = (C20670w8) r2.AMw.get();
            ((AbstractActivityC28171Kz) profileActivity).A03 = (C15550nR) r2.A45.get();
            ((AbstractActivityC28171Kz) profileActivity).A0B = (C19890uq) r2.ABw.get();
            ((AbstractActivityC28171Kz) profileActivity).A0A = (C20710wC) r2.A8m.get();
            ((AbstractActivityC28171Kz) profileActivity).A0E = (AbstractC15850o0) r2.ANA.get();
            ((AbstractActivityC28171Kz) profileActivity).A04 = (C17050qB) r2.ABL.get();
            ((AbstractActivityC28171Kz) profileActivity).A0C = (C20740wF) r2.AIA.get();
            ((AbstractActivityC28171Kz) profileActivity).A0D = (C18350sJ) r2.AHX.get();
            ((AbstractActivityC28171Kz) profileActivity).A06 = (C20820wN) r2.ACG.get();
            ((AbstractActivityC28171Kz) profileActivity).A08 = (C26041Bu) r2.ACL.get();
            ((AbstractActivityC28171Kz) profileActivity).A00 = (AnonymousClass2FR) r3.A0P.get();
            ((AbstractActivityC28171Kz) profileActivity).A07 = (C20850wQ) r2.ACI.get();
            profileActivity.A00 = (C15570nT) r2.AAr.get();
            profileActivity.A06 = (AbstractC14440lR) r2.ANe.get();
            profileActivity.A05 = (AnonymousClass12U) r2.AJd.get();
            profileActivity.A04 = (WhatsAppLibLoader) r2.ANa.get();
            profileActivity.A02 = (AnonymousClass116) r2.A3z.get();
            profileActivity.A03 = (C16490p7) r2.ACJ.get();
        }
    }
}
