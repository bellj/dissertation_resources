package X;

import java.util.Comparator;

/* renamed from: X.5Cq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112245Cq implements Comparator {
    public final C19990v2 A00;

    public C112245Cq(C19990v2 r1) {
        this.A00 = r1;
    }

    /* renamed from: A00 */
    public int compare(AnonymousClass1OU r8, AnonymousClass1OU r9) {
        int i = r8.A00;
        int i2 = r9.A00;
        if (i != 3 || i == i2) {
            if (i2 == 3 && i != i2) {
                return 1;
            }
            C19990v2 r1 = this.A00;
            long A05 = r1.A05(r8.A02);
            long A052 = r1.A05(r9.A02);
            if (A05 == A052) {
                return r8.A03.compareTo(r9.A03);
            }
            if (A05 < A052) {
                return 1;
            }
        }
        return -1;
    }
}
