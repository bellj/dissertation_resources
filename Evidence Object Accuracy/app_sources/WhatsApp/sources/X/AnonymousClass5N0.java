package X;

/* renamed from: X.5N0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N0 extends AnonymousClass1TM implements AbstractC115545Ry {
    public int A00;
    public AnonymousClass1TN A01;

    public AnonymousClass5N0(AnonymousClass1TN r2) {
        this.A00 = 0;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return new C114835Ng(this.A01, this.A00, false);
    }

    public AnonymousClass5N0(AnonymousClass5NU r3) {
        int i = r3.A00;
        this.A00 = i;
        this.A01 = i == 0 ? new C114705Mt(AbstractC114775Na.A05(r3, false)) : AnonymousClass5NV.A01(r3);
    }

    public String toString() {
        String str;
        String str2 = AnonymousClass1T7.A00;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("DistributionPointName: [");
        stringBuffer.append(str2);
        int i = this.A00;
        String obj = this.A01.toString();
        if (i == 0) {
            str = "fullName";
        } else {
            str = "nameRelativeToCRLIssuer";
        }
        C72453ed.A1N(str, str2, obj, stringBuffer);
        stringBuffer.append("]");
        stringBuffer.append(str2);
        return stringBuffer.toString();
    }
}
