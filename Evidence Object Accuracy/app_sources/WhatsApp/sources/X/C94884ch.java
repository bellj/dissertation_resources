package X;

/* renamed from: X.4ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94884ch {
    public static final C94884ch A04 = new C94884ch(2);
    public static final C94884ch A05 = new C94884ch(-1);
    public static final C94884ch A06 = new C94884ch(0);
    public AnonymousClass5VL A00;
    public AnonymousClass5VL A01;
    public AnonymousClass5VM A02;
    public boolean A03;

    public C94884ch() {
        this(0);
    }

    public C94884ch(int i) {
        AnonymousClass5VL r1;
        boolean z = false;
        boolean A1T = C12960it.A1T(i & 1);
        boolean A1T2 = C12960it.A1T(i & 4);
        boolean A1T3 = C12960it.A1T(i & 2);
        this.A03 = (i & 16) > 0 ? true : z;
        if ((i & 8) > 0) {
            r1 = C95224dL.A02;
        } else {
            r1 = C95224dL.A03;
        }
        if (A1T2) {
            this.A01 = C95224dL.A04;
        } else {
            this.A01 = r1;
        }
        this.A00 = A1T ? C95224dL.A04 : r1;
        if (A1T3) {
            this.A02 = C95224dL.A00;
        } else {
            this.A02 = C95224dL.A01;
        }
    }

    public void A00(Appendable appendable, String str) {
        if (!this.A01.ALX(str)) {
            appendable.append(str);
            return;
        }
        appendable.append('\"');
        if (str != null) {
            this.A02.A9f(appendable, str);
        }
        appendable.append('\"');
    }
}
