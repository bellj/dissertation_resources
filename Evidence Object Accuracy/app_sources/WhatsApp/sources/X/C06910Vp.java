package X;

import android.os.IBinder;
import android.support.v4.os.IResultReceiver;

/* renamed from: X.0Vp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06910Vp implements IResultReceiver {
    public IBinder A00;

    public C06910Vp(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
