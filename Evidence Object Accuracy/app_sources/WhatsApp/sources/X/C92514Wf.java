package X;

/* renamed from: X.4Wf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92514Wf {
    public final long A00;
    public final long A01;

    public C92514Wf(long j, long j2) {
        this.A01 = j;
        this.A00 = j2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C92514Wf)) {
            return false;
        }
        C92514Wf r7 = (C92514Wf) obj;
        if (this.A01 == r7.A01 && this.A00 == r7.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((int) this.A01) * 31) + ((int) this.A00);
    }
}
