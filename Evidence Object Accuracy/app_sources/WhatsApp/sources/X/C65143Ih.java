package X;

import com.whatsapp.util.Log;
import java.util.HashSet;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: X.3Ih  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65143Ih {
    public static final HashSet A0J;
    public static final HashSet A0K;
    public static final HashSet A0L;
    public static final HashSet A0M;
    public static final Pattern A0N = Pattern.compile("type=(.*?)[:;]");
    public static final Pattern A0O = Pattern.compile("waid=(.*?)[:;]");
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public long A0A;
    public long A0B;
    public String A0C = null;
    public String A0D;
    public String A0E;
    public HashSet A0F = C12970iu.A12();
    public C41401tT A0G = null;
    public AnonymousClass5VU A0H;
    public boolean A0I = false;

    static {
        String[] strArr = new String[50];
        strArr[0] = "DOM";
        strArr[1] = "INTL";
        strArr[2] = "POSTAL";
        strArr[3] = "PARCEL";
        strArr[4] = "HOME";
        strArr[5] = "WORK";
        strArr[6] = "PREF";
        strArr[7] = "VOICE";
        strArr[8] = "FAX";
        strArr[9] = "MSG";
        strArr[10] = "CELL";
        strArr[11] = "PAGER";
        strArr[12] = "BBS";
        strArr[13] = "MODEM";
        strArr[14] = "CAR";
        strArr[15] = "ISDN";
        strArr[16] = "VIDEO";
        strArr[17] = "AOL";
        strArr[18] = "APPLELINK";
        strArr[19] = "ATTMAIL";
        strArr[20] = "CIS";
        strArr[21] = "EWORLD";
        strArr[22] = "INTERNET";
        strArr[23] = "IBMMAIL";
        strArr[24] = "MCIMAIL";
        strArr[25] = "POWERSHARE";
        strArr[26] = "PRODIGY";
        strArr[27] = "TLX";
        strArr[28] = "X400";
        strArr[29] = "GIF";
        strArr[30] = "CGM";
        strArr[31] = "WMF";
        strArr[32] = "BMP";
        strArr[33] = "MET";
        strArr[34] = "PMB";
        strArr[35] = "DIB";
        strArr[36] = "PICT";
        strArr[37] = "TIFF";
        strArr[38] = "PDF";
        strArr[39] = "PS";
        strArr[40] = "JPEG";
        strArr[41] = "QTIME";
        strArr[42] = "MPEG";
        strArr[43] = "MPEG2";
        strArr[44] = "AVI";
        strArr[45] = "WAVE";
        strArr[46] = "AIFF";
        strArr[47] = "PCM";
        strArr[48] = "X509";
        A0L = C12970iu.A13("PGP", strArr, 49);
        String[] strArr2 = new String[4];
        strArr2[0] = "INLINE";
        strArr2[1] = "URL";
        strArr2[2] = "CONTENT-ID";
        A0M = C12970iu.A13("CID", strArr2, 3);
        String[] strArr3 = new String[20];
        strArr3[0] = "BEGIN";
        strArr3[1] = "LOGO";
        strArr3[2] = "PHOTO";
        strArr3[3] = "LABEL";
        strArr3[4] = "FN";
        strArr3[5] = "TITLE";
        strArr3[6] = "SOUND";
        strArr3[7] = "VERSION";
        strArr3[8] = "TEL";
        strArr3[9] = "EMAIL";
        strArr3[10] = "TZ";
        strArr3[11] = "GEO";
        strArr3[12] = "NOTE";
        strArr3[13] = "URL";
        strArr3[14] = "BDAY";
        strArr3[15] = "ROLE";
        strArr3[16] = "REV";
        strArr3[17] = "UID";
        strArr3[18] = "KEY";
        A0K = C12970iu.A13("MAILER", strArr3, 19);
        String[] strArr4 = new String[5];
        strArr4[0] = "7BIT";
        strArr4[1] = "8BIT";
        strArr4[2] = "QUOTED-PRINTABLE";
        strArr4[3] = "BASE64";
        A0J = C12970iu.A13("B", strArr4, 4);
    }

    public String A00() {
        if (!(this instanceof C72363eT)) {
            return this.A0H.readLine();
        }
        C72363eT r2 = (C72363eT) this;
        String str = r2.A00;
        if (str == null) {
            return r2.A0H.readLine();
        }
        r2.A00 = null;
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0086, code lost:
        r6.A00 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0088, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A01() {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.C72363eT
            if (r0 != 0) goto L_0x0034
            boolean r0 = r8.A0I
            if (r0 == 0) goto L_0x000e
            r0 = 0
            r8.A0I = r0
            java.lang.String r2 = r8.A0D
            return r2
        L_0x000e:
            java.lang.String r2 = r8.A00()
            if (r2 == 0) goto L_0x002c
            int r1 = r2.length()
            r0 = 16384(0x4000, float:2.2959E-41)
            if (r1 >= r0) goto L_0x0027
            java.lang.String r0 = r2.trim()
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0027
            return r2
        L_0x0027:
            java.lang.String r2 = r8.A01()
            return r2
        L_0x002c:
            java.lang.String r1 = "Reached end of buffer."
            X.1tN r0 = new X.1tN
            r0.<init>(r1)
            throw r0
        L_0x0034:
            r6 = r8
            X.3eT r6 = (X.C72363eT) r6
            boolean r0 = r6.A0I
            r5 = 0
            if (r0 == 0) goto L_0x0041
            r6.A0I = r5
            java.lang.String r0 = r6.A0D
        L_0x0040:
            return r0
        L_0x0041:
            r4 = 0
        L_0x0042:
            r7 = r4
        L_0x0043:
            X.5VU r0 = r6.A0H
            java.lang.String r3 = r0.readLine()
            if (r3 != 0) goto L_0x0059
            if (r7 != 0) goto L_0x007b
            java.lang.String r0 = r6.A00
            if (r0 != 0) goto L_0x0086
            java.lang.String r1 = "Reached end of buffer."
            X.1tN r0 = new X.1tN
            r0.<init>(r1)
            throw r0
        L_0x0059:
            int r0 = r3.length()
            if (r0 == 0) goto L_0x0080
            r2 = 16384(0x4000, float:2.2959E-41)
            if (r0 > r2) goto L_0x0080
            char r1 = r3.charAt(r5)
            r0 = 32
            if (r1 == r0) goto L_0x0089
            char r1 = r3.charAt(r5)
            r0 = 9
            if (r1 == r0) goto L_0x0089
            java.lang.String r0 = r6.A00
            r6.A00 = r3
            if (r0 != 0) goto L_0x0040
            if (r7 == 0) goto L_0x0043
        L_0x007b:
            java.lang.String r0 = r7.toString()
            return r0
        L_0x0080:
            if (r7 != 0) goto L_0x007b
            java.lang.String r0 = r6.A00
            if (r0 == 0) goto L_0x0043
        L_0x0086:
            r6.A00 = r4
            return r0
        L_0x0089:
            r1 = 1
            if (r7 != 0) goto L_0x0096
            java.lang.String r0 = r6.A00
            if (r0 == 0) goto L_0x00a4
            java.lang.StringBuilder r7 = X.C12960it.A0j(r0)
            r6.A00 = r4
        L_0x0096:
            java.lang.String r0 = r3.substring(r1)
            r7.append(r0)
            int r0 = r7.length()
            if (r0 <= r2) goto L_0x0043
            goto L_0x0042
        L_0x00a4:
            java.lang.String r1 = "Space exists at the beginning of the line"
            X.1tN r0 = new X.1tN
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65143Ih.A01():java.lang.String");
    }

    public String A02(String str) {
        if (!str.trim().endsWith("=")) {
            return str;
        }
        StringBuilder A0h = C12960it.A0h();
        A0h.append(str.substring(0, (str.length() - 1) + 1));
        while (true) {
            A0h.append("\r\n");
            String A00 = A00();
            if (A00 == null) {
                throw new C41341tN("File ended during parsing quoted-printable String");
            } else if (!A00.trim().endsWith("=")) {
                return C12960it.A0d(A00, A0h);
            } else {
                A0h.append(A00.substring(0, (A00.length() - 1) + 1));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        if (r3 == null) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A03(java.lang.String r8, java.lang.String r9) {
        /*
            r7 = this;
            java.lang.String r0 = "."
            int r4 = r8.indexOf(r0)
            r5 = 1
            int r4 = r4 + r5
            r6 = 0
            if (r4 <= 0) goto L_0x0013
            boolean r0 = r7.A0I
            if (r0 == 0) goto L_0x0014
            java.lang.String r3 = r7.A0D
            if (r3 != 0) goto L_0x0028
        L_0x0013:
            return r6
        L_0x0014:
            java.lang.String r3 = r7.A01()
            r7.A0D = r3
            if (r3 == 0) goto L_0x0013
            java.lang.String r0 = r3.trim()
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0013
            r7.A0I = r5
        L_0x0028:
            int r0 = r3.length()
            if (r0 <= r4) goto L_0x0013
            r2 = 0
            java.lang.String r1 = r8.substring(r2, r4)
            java.lang.String r0 = r3.substring(r2, r4)
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0013
            if (r9 == 0) goto L_0x0060
            int r0 = r4 + -1
            char r1 = r3.charAt(r0)
            r0 = 46
            if (r1 == r0) goto L_0x004a
            r5 = 0
        L_0x004a:
            X.AnonymousClass009.A0F(r5)
            java.lang.String r0 = ":"
            int r0 = r3.lastIndexOf(r0)
            if (r0 < r4) goto L_0x0013
            java.lang.String r0 = r3.substring(r4, r0)
            boolean r0 = r9.equals(r0)
            if (r0 != 0) goto L_0x0060
            return r6
        L_0x0060:
            r7.A0I = r2
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65143Ih.A03(java.lang.String, java.lang.String):java.lang.String");
    }

    public void A04(String str) {
        String str2;
        C41401tT r0;
        HashSet hashSet;
        String[] split = str.split("=", 2);
        if (split.length == 2) {
            String trim = split[0].trim();
            str2 = split[1].trim();
            if (!trim.equalsIgnoreCase("TYPE")) {
                if (trim.equals("VALUE")) {
                    if (A0M.contains(str2.toUpperCase(Locale.US)) || str2.startsWith("X-")) {
                        r0 = this.A0G;
                        if (r0 != null) {
                            r0.A01 = "VALUE";
                        } else {
                            return;
                        }
                    } else {
                        StringBuilder A0k = C12960it.A0k("Unknown value \"");
                        A0k.append(str2);
                        throw new C41341tN(C12960it.A0d("\"", A0k));
                    }
                } else if (trim.equals("ENCODING")) {
                    if (!(this instanceof C72363eT)) {
                        hashSet = A0J;
                    } else {
                        hashSet = C72363eT.A01;
                    }
                    if (hashSet.contains(str2.toUpperCase(Locale.US)) || str2.startsWith("X-")) {
                        C41401tT r02 = this.A0G;
                        if (r02 != null) {
                            r02.A01 = "ENCODING";
                            r02.A00(str2);
                        }
                        this.A0C = str2;
                        return;
                    }
                    StringBuilder A0k2 = C12960it.A0k("Unknown encoding \"");
                    A0k2.append(str2);
                    throw new C41341tN(C12960it.A0d("\"", A0k2));
                } else if (trim.equals("CHARSET")) {
                    r0 = this.A0G;
                    if (r0 != null) {
                        r0.A01 = "CHARSET";
                    } else {
                        return;
                    }
                } else if (trim.equals("LANGUAGE")) {
                    String[] split2 = str2.split("-");
                    int length = split2.length;
                    if (length <= 2) {
                        String str3 = split2[0];
                        int length2 = str3.length();
                        for (int i = 0; i < length2; i++) {
                            char charAt = str3.charAt(i);
                            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                                StringBuilder A0j = C12960it.A0j("Invalid Language: \"");
                                A0j.append(str2);
                                throw new C41341tN(C12960it.A0d("\"", A0j));
                            }
                        }
                        if (length > 1) {
                            String str4 = split2[1];
                            int length3 = str4.length();
                            for (int i2 = 0; i2 < length3; i2++) {
                                char charAt2 = str4.charAt(i2);
                                if ((charAt2 < 'a' || charAt2 > 'z') && (charAt2 < 'A' || charAt2 > 'Z')) {
                                    StringBuilder A0j2 = C12960it.A0j("Invalid Language: \"");
                                    A0j2.append(str2);
                                    throw new C41341tN(C12960it.A0d("\"", A0j2));
                                }
                            }
                        }
                        r0 = this.A0G;
                        if (r0 != null) {
                            r0.A01 = "LANGUAGE";
                        } else {
                            return;
                        }
                    } else {
                        StringBuilder A0j3 = C12960it.A0j("Invalid Language: \"");
                        A0j3.append(str2);
                        throw new C41341tN(C12960it.A0d("\"", A0j3));
                    }
                } else if (trim.startsWith("X-")) {
                    r0 = this.A0G;
                    if (r0 != null) {
                        r0.A01 = trim;
                    } else {
                        return;
                    }
                } else if (trim.equalsIgnoreCase("WAID")) {
                    C41401tT r1 = this.A0G;
                    if (r1 != null) {
                        r1.A01 = "waId";
                        r1.A00(str2);
                        return;
                    }
                    return;
                } else {
                    StringBuilder A0k3 = C12960it.A0k("Unknown type \"");
                    A0k3.append(trim);
                    throw new C41341tN(C12960it.A0d("\"", A0k3));
                }
                r0.A00(str2);
                return;
            }
        } else {
            str2 = split[0];
        }
        A05(str2);
    }

    public void A05(String str) {
        C41401tT r2;
        String replaceAll;
        if (!(this instanceof C72363eT)) {
            if (!A0L.contains(str) && !str.startsWith("X-")) {
                HashSet hashSet = this.A0F;
                if (!hashSet.contains(str)) {
                    hashSet.add(str);
                    Log.w(C12960it.A0d(str, C12960it.A0k("Type unsupported by vCard 2.1: ")));
                }
            }
            C41401tT r1 = this.A0G;
            if (r1 != null) {
                r1.A01 = "TYPE";
                r1.A00(str);
                return;
            }
            return;
        }
        String[] split = str.split(",");
        this.A0G.A01 = "TYPE";
        for (String str2 : split) {
            if (str2.length() < 2 || !str2.startsWith("\"") || !str2.endsWith("\"")) {
                r2 = this.A0G;
                replaceAll = str2.replaceAll("[_$!<|>!$_]", "");
            } else {
                r2 = this.A0G;
                replaceAll = str2.substring(1, str2.length() - 1);
            }
            r2.A00(replaceAll);
        }
    }
}
