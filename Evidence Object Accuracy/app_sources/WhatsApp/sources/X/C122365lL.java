package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.5lL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122365lL extends AbstractC118825cR {
    public final TextView A00;
    public final WaImageView A01;

    public C122365lL(View view) {
        super(view);
        this.A01 = C12980iv.A0X(view, R.id.icon);
        this.A00 = C12960it.A0I(view, R.id.text);
    }
}
