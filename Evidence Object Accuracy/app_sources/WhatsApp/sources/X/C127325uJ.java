package X;

import java.math.BigDecimal;

/* renamed from: X.5uJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127325uJ {
    public final AbstractC30791Yv A00;
    public final AbstractC30791Yv A01;
    public final BigDecimal A02;

    public C127325uJ(AbstractC30791Yv r1, AbstractC30791Yv r2, BigDecimal bigDecimal) {
        this.A02 = bigDecimal;
        this.A00 = r1;
        this.A01 = r2;
    }
}
