package X;

import android.view.ViewGroup;
import java.util.Iterator;

/* renamed from: X.0f3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10700f3 implements AnonymousClass1WO {
    public final /* synthetic */ ViewGroup A00;

    public C10700f3(ViewGroup viewGroup) {
        this.A00 = viewGroup;
    }

    public static final Iterator A00(ViewGroup viewGroup) {
        C16700pc.A0E(viewGroup, 0);
        return new C10400eZ(viewGroup);
    }

    @Override // X.AnonymousClass1WO
    public Iterator iterator() {
        return A00(this.A00);
    }
}
