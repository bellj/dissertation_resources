package X;

import android.view.View;

/* renamed from: X.5o9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123915o9 extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ C121455i8 A00;

    public C123915o9(C121455i8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        this.A00.A0E.Ab2(new Runnable() { // from class: X.6Gs
            @Override // java.lang.Runnable
            public final void run() {
                C123915o9 r3 = C123915o9.this;
                C121455i8 r1 = r3.A00;
                C17070qD r0 = ((C130035yh) r1).A04.A0D;
                r0.A03();
                r1.A0A.A0H(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0018: INVOKE  
                      (wrap: X.0mE : 0x0011: IGET  (r1v1 X.0mE A[REMOVE]) = (r1v0 'r1' X.5i8) X.5i8.A0A X.0mE)
                      (wrap: X.6IQ : 0x0015: CONSTRUCTOR  (r0v3 X.6IQ A[REMOVE]) = 
                      (wrap: X.1Pl : 0x000d: INVOKE  (r2v0 X.1Pl A[REMOVE]) = (wrap: X.14j : 0x000b: IGET  (r0v2 X.14j A[REMOVE]) = (r0v1 'r0' X.0qD) X.0qD.A09 X.14j) type: VIRTUAL call: X.14j.A06():X.1Pl)
                      (r3v0 'r3' X.5o9)
                     call: X.6IQ.<init>(X.1Pl, X.5o9):void type: CONSTRUCTOR)
                     type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6Gs.run():void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0015: CONSTRUCTOR  (r0v3 X.6IQ A[REMOVE]) = 
                      (wrap: X.1Pl : 0x000d: INVOKE  (r2v0 X.1Pl A[REMOVE]) = (wrap: X.14j : 0x000b: IGET  (r0v2 X.14j A[REMOVE]) = (r0v1 'r0' X.0qD) X.0qD.A09 X.14j) type: VIRTUAL call: X.14j.A06():X.1Pl)
                      (r3v0 'r3' X.5o9)
                     call: X.6IQ.<init>(X.1Pl, X.5o9):void type: CONSTRUCTOR in method: X.6Gs.run():void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IQ, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    X.5o9 r3 = X.C123915o9.this
                    X.5i8 r1 = r3.A00
                    X.5jx r0 = r1.A04
                    X.0qD r0 = r0.A0D
                    r0.A03()
                    X.14j r0 = r0.A09
                    X.1Pl r2 = r0.A06()
                    X.0mE r1 = r1.A0A
                    X.6IQ r0 = new X.6IQ
                    r0.<init>(r2, r3)
                    r1.A0H(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.RunnableC134986Gs.run():void");
            }
        });
    }
}
