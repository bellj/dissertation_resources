package X;

import android.net.Uri;
import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3lx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76633lx extends Timeline {
    public static final AnonymousClass4XL A08;
    public static final Object A09 = C12970iu.A0l();
    public final long A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;
    public final AnonymousClass4XK A05;
    public final AnonymousClass4XL A06;
    public final boolean A07;

    static {
        AnonymousClass3DU r1 = new AnonymousClass3DU();
        r1.A07 = "SinglePeriodTimeline";
        r1.A06 = Uri.EMPTY;
        A08 = r1.A00();
    }

    public C76633lx(AnonymousClass4XL r4, long j, boolean z, boolean z2) {
        AnonymousClass4XK r2;
        if (z2) {
            r2 = r4.A01;
        } else {
            r2 = null;
        }
        this.A02 = -9223372036854775807L;
        this.A04 = -9223372036854775807L;
        this.A00 = -9223372036854775807L;
        this.A01 = j;
        this.A03 = j;
        this.A07 = z;
        this.A06 = r4;
        this.A05 = r2;
    }

    @Override // com.google.android.exoplayer2.Timeline
    public C94404bl A0B(C94404bl r15, int i, long j) {
        C95314dV.A00(i, 1);
        Object obj = C94404bl.A0F;
        r15.A00(this.A05, this.A06, obj, this.A02, this.A04, this.A00, this.A03, this.A07, false);
        return r15;
    }
}
