package X;

import android.content.Context;
import java.lang.Thread;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.0ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14170ky {
    public static volatile C14170ky A06;
    public Thread.UncaughtExceptionHandler A00;
    public final Context A01;
    public final C67813Sz A02 = new C67813Sz();
    public final C14180kz A03 = new C14180kz(this);
    public final List A04 = new CopyOnWriteArrayList();
    public volatile C56252ka A05;

    public C14170ky(Context context) {
        Context applicationContext = context.getApplicationContext();
        C13020j0.A01(applicationContext);
        this.A01 = applicationContext;
    }

    public static void A00() {
        if (!(Thread.currentThread() instanceof AnonymousClass5HA)) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }
}
