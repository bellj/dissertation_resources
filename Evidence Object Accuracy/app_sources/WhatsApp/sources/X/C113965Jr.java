package X;

import android.graphics.Paint;

/* renamed from: X.5Jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C113965Jr extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ C73063fc this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C113965Jr(C73063fc r2) {
        super(0);
        this.this$0 = r2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        Paint paint = new Paint(1);
        C73063fc r2 = this.this$0;
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setDither(true);
        paint.setColor(AnonymousClass00T.A00(r2.A03, r2.A02.A01.A00));
        return paint;
    }
}
