package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DA  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5DA implements Iterator {
    public AnonymousClass4AO A00 = AnonymousClass4AO.A03;
    public Object A01;

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        if (r5 >= r3) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006b, code lost:
        if (r4.A02.A00(r2.charAt(r5)) == false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006d, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0070, code lost:
        if (r3 <= r5) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007e, code lost:
        if (r4.A02.A00(r2.charAt(r3 - 1)) == false) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0080, code lost:
        r3 = r3 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0083, code lost:
        r0 = r4.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0085, code lost:
        if (r0 != 1) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0087, code lost:
        r3 = r2.length();
        r4.A01 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008d, code lost:
        if (r3 <= r5) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009b, code lost:
        if (r4.A02.A00(r2.charAt(r3 - 1)) == false) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009d, code lost:
        r3 = r3 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a0, code lost:
        r4.A00 = r0 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a3, code lost:
        r0 = X.C72463ee.A0E(r2, r5, r3);
     */
    @Override // java.util.Iterator
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean hasNext() {
        /*
            r9 = this;
            X.4AO r0 = r9.A00
            X.4AO r2 = X.AnonymousClass4AO.A02
            r1 = 0
            r7 = 1
            if (r0 == r2) goto L_0x00bf
            int r0 = r0.ordinal()
            switch(r0) {
                case 0: goto L_0x00be;
                case 1: goto L_0x000f;
                case 2: goto L_0x00bd;
                default: goto L_0x000f;
            }
        L_0x000f:
            r9.A00 = r2
            r4 = r9
            X.3st r4 = (X.C80743st) r4
            int r5 = r4.A01
        L_0x0016:
            int r3 = r4.A01
            r6 = -1
            if (r3 == r6) goto L_0x00a8
            X.4Ie r0 = r4.A04
            X.51N r8 = r0.A00
            java.lang.CharSequence r2 = r4.A03
            boolean r0 = r8 instanceof X.C80763sv
            if (r0 != 0) goto L_0x0050
            int r1 = r2.length()
            X.C28291Mn.A02(r3, r1)
        L_0x002c:
            if (r3 >= r1) goto L_0x0057
            char r0 = r2.charAt(r3)
            boolean r0 = r8.A00(r0)
            if (r0 == 0) goto L_0x004d
            if (r3 == r6) goto L_0x0057
            int r0 = r3 + 1
            r4.A01 = r0
        L_0x003e:
            if (r0 != r5) goto L_0x005f
            int r1 = r0 + 1
            r4.A01 = r1
            int r0 = r2.length()
            if (r1 <= r0) goto L_0x0016
            r4.A01 = r6
            goto L_0x0016
        L_0x004d:
            int r3 = r3 + 1
            goto L_0x002c
        L_0x0050:
            int r0 = r2.length()
            X.C28291Mn.A02(r3, r0)
        L_0x0057:
            int r3 = r2.length()
            r4.A01 = r6
            r0 = -1
            goto L_0x003e
        L_0x005f:
            if (r5 >= r3) goto L_0x0070
            X.51N r1 = r4.A02
            char r0 = r2.charAt(r5)
            boolean r0 = r1.A00(r0)
            if (r0 == 0) goto L_0x0070
            int r5 = r5 + 1
            goto L_0x005f
        L_0x0070:
            if (r3 <= r5) goto L_0x0083
            X.51N r1 = r4.A02
            int r0 = r3 + -1
            char r0 = r2.charAt(r0)
            boolean r0 = r1.A00(r0)
            if (r0 == 0) goto L_0x0083
            int r3 = r3 + -1
            goto L_0x0070
        L_0x0083:
            int r0 = r4.A00
            if (r0 != r7) goto L_0x00a0
            int r3 = r2.length()
            r4.A01 = r6
        L_0x008d:
            if (r3 <= r5) goto L_0x00a3
            X.51N r1 = r4.A02
            int r0 = r3 + -1
            char r0 = r2.charAt(r0)
            boolean r0 = r1.A00(r0)
            if (r0 == 0) goto L_0x00a3
            int r3 = r3 + -1
            goto L_0x008d
        L_0x00a0:
            int r0 = r0 - r7
            r4.A00 = r0
        L_0x00a3:
            java.lang.String r0 = X.C72463ee.A0E(r2, r5, r3)
            goto L_0x00ad
        L_0x00a8:
            X.4AO r0 = X.AnonymousClass4AO.A01
            r4.A00 = r0
            r0 = 0
        L_0x00ad:
            r9.A01 = r0
            X.4AO r1 = r9.A00
            X.4AO r0 = X.AnonymousClass4AO.A01
            if (r1 == r0) goto L_0x00bb
            X.4AO r0 = X.AnonymousClass4AO.A04
            r9.A00 = r0
            r0 = 1
            return r0
        L_0x00bb:
            r0 = 0
            return r0
        L_0x00bd:
            return r1
        L_0x00be:
            return r7
        L_0x00bf:
            java.lang.IllegalStateException r0 = X.C72463ee.A0D()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5DA.hasNext():boolean");
    }

    @Override // java.util.Iterator
    public final Object next() {
        if (hasNext()) {
            this.A00 = AnonymousClass4AO.A03;
            Object obj = this.A01;
            this.A01 = null;
            return obj;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw C12970iu.A0z();
    }
}
