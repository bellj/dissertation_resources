package X;

import android.media.ImageReader;

/* renamed from: X.63L  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63L implements ImageReader.OnImageAvailableListener {
    public final /* synthetic */ AnonymousClass66L A00;

    public AnonymousClass63L(AnonymousClass66L r1) {
        this.A00 = r1;
    }

    @Override // android.media.ImageReader.OnImageAvailableListener
    public void onImageAvailable(ImageReader imageReader) {
        AnonymousClass66L.A00(imageReader, this.A00);
    }
}
