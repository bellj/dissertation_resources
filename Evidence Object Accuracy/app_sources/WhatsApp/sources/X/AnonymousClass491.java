package X;

import java.io.IOException;

/* renamed from: X.491  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass491 extends IOException {
    public AnonymousClass491(String str) {
        super(str);
    }

    public AnonymousClass491(String str, Throwable th) {
        super(str, th);
    }
}
