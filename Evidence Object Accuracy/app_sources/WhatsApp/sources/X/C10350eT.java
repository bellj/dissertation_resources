package X;

import java.util.Comparator;

/* renamed from: X.0eT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10350eT implements Comparator {
    public final /* synthetic */ AnonymousClass0Cq A00;

    public C10350eT(AnonymousClass0Cq r1) {
        this.A00 = r1;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((AnonymousClass0QC) obj).A02 - ((AnonymousClass0QC) obj2).A02;
    }
}
