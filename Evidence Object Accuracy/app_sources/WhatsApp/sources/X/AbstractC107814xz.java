package X;

import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import java.util.Arrays;

/* renamed from: X.4xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC107814xz implements AbstractC117085Ye {
    public int A00;
    public final int A01;
    public final C100554m6 A02;
    public final int[] A03;
    public final C100614mC[] A04;

    public AbstractC107814xz(C100554m6 r8, int... iArr) {
        int length = iArr.length;
        C95314dV.A04(C12960it.A1U(length));
        this.A02 = r8;
        this.A01 = length;
        C100614mC[] r3 = new C100614mC[length];
        this.A04 = r3;
        for (int i = 0; i < length; i++) {
            r3[i] = r8.A02[iArr[i]];
        }
        Arrays.sort(r3, new IDxComparatorShape3S0000000_2_I1(3));
        int i2 = this.A01;
        int[] iArr2 = new int[i2];
        this.A03 = iArr2;
        for (int i3 = 0; i3 < i2; i3++) {
            C100614mC r32 = this.A04[i3];
            int i4 = 0;
            while (true) {
                C100614mC[] r1 = r8.A02;
                if (i4 >= r1.length) {
                    i4 = -1;
                    break;
                }
                i4 = r32 != r1[i4] ? i4 + 1 : i4;
            }
            iArr2[i3] = i4;
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AbstractC107814xz r5 = (AbstractC107814xz) obj;
            if (this.A02 != r5.A02 || !Arrays.equals(this.A03, r5.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int identityHashCode = (System.identityHashCode(this.A02) * 31) + Arrays.hashCode(this.A03);
        this.A00 = identityHashCode;
        return identityHashCode;
    }
}
