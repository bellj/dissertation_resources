package X;

/* renamed from: X.1fJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33931fJ {
    public byte A00;
    public AbstractC14640lm A01;

    public C33931fJ(AbstractC14640lm r1, byte b) {
        this.A01 = r1;
        this.A00 = b;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C33931fJ r5 = (C33931fJ) obj;
            if (this.A00 != r5.A00 || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append(" ");
        sb.append((int) this.A00);
        return sb.toString();
    }
}
