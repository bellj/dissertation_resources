package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: X.012  reason: invalid class name */
/* loaded from: classes.dex */
public interface AnonymousClass012 {
    ColorStateList getSupportBackgroundTintList();

    PorterDuff.Mode getSupportBackgroundTintMode();

    void setSupportBackgroundTintList(ColorStateList colorStateList);

    void setSupportBackgroundTintMode(PorterDuff.Mode mode);
}
