package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1aD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC31231aD {
    public static AbstractC31231aD A00(int i) {
        if (i == 2) {
            return new AnonymousClass5Pg();
        }
        if (i == 3) {
            return new C31221aC();
        }
        StringBuilder sb = new StringBuilder("Unknown version: ");
        sb.append(i);
        throw new AssertionError(sb.toString());
    }

    public int A01() {
        return 1;
    }

    public byte[] A02(byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        try {
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance.init(new SecretKeySpec(bArr2, DefaultCrypto.HMAC_SHA256));
            byte[] doFinal = instance.doFinal(bArr);
            try {
                int ceil = (int) Math.ceil(((double) i) / 32.0d);
                byte[] bArr4 = new byte[0];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                int A01 = A01();
                while (A01 < A01 + ceil) {
                    Mac instance2 = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
                    instance2.init(new SecretKeySpec(doFinal, DefaultCrypto.HMAC_SHA256));
                    instance2.update(bArr4);
                    if (bArr3 != null) {
                        instance2.update(bArr3);
                    }
                    instance2.update((byte) A01);
                    bArr4 = instance2.doFinal();
                    int min = Math.min(i, bArr4.length);
                    byteArrayOutputStream.write(bArr4, 0, min);
                    i -= min;
                    A01++;
                }
                return byteArrayOutputStream.toByteArray();
            } catch (InvalidKeyException | NoSuchAlgorithmException e) {
                throw new AssertionError(e);
            }
        } catch (InvalidKeyException | NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }
}
