package X;

import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.5Fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112885Fe implements AbstractC117265Ze {
    public C92754Xh A00;

    public C112885Fe(C92754Xh r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC117265Ze
    public InputStream AEi() {
        return new AnonymousClass49C(this.A00);
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return new AnonymousClass5N6(AnonymousClass4F6.A00(new AnonymousClass49C(this.A00)));
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw new AnonymousClass4CU(C12960it.A0d(e.getMessage(), C12960it.A0k("IOException converting stream to byte array: ")), e);
        }
    }
}
