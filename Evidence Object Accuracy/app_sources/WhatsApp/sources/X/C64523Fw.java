package X;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* renamed from: X.3Fw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64523Fw {
    public float A00;
    public int A01;
    public int A02 = 0;
    public ValueAnimator A03;
    public ValueAnimator A04;
    public AnonymousClass2UH A05 = new C56722lY(this);
    public boolean A06 = true;
    public boolean A07;
    public boolean A08 = true;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public final View A0C;
    public final View A0D;
    public final View A0E;
    public final BottomSheetBehavior A0F;
    public final VoipCallControlBottomSheetV2 A0G;

    public C64523Fw(View view, View view2, VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2) {
        this.A0C = view;
        this.A0D = view2;
        this.A0F = BottomSheetBehavior.A00(view);
        this.A0G = voipCallControlBottomSheetV2;
        this.A0E = AnonymousClass028.A0D(view, R.id.bottom_sheet);
        this.A0F.A0E = this.A05;
    }

    public void A00() {
        int i;
        if (this.A08) {
            Activity A02 = AnonymousClass12P.A02(this.A0C);
            if (Build.VERSION.SDK_INT <= 24 || !A02.isInPictureInPictureMode()) {
                Point point = new Point();
                Rect A0J = C12980iv.A0J();
                C12970iu.A17(A02, point);
                C12970iu.A0G(A02).getWindowVisibleDisplayFrame(A0J);
                float f = (float) (point.y - A0J.top);
                this.A00 = (float) ((int) (0.75f * f));
                int dimensionPixelSize = A02.getResources().getDimensionPixelSize(R.dimen.call_control_bottom_sheet_btn_stub_height);
                if (!this.A0A || !this.A09) {
                    if (this.A0B) {
                        dimensionPixelSize += this.A0D.getMeasuredHeight();
                    }
                    BottomSheetBehavior bottomSheetBehavior = this.A0F;
                    if (bottomSheetBehavior.A0M) {
                        i = -1;
                    } else {
                        i = bottomSheetBehavior.A09;
                    }
                    if (dimensionPixelSize != i) {
                        A03(500);
                        bottomSheetBehavior.A0L(dimensionPixelSize);
                        return;
                    }
                    return;
                }
                int i2 = (int) (f * 0.6f);
                int dimensionPixelSize2 = A02.getResources().getDimensionPixelSize(R.dimen.contact_picker_row_height);
                this.A0F.A0L(i2 + ((dimensionPixelSize2 >> 1) - (i2 % dimensionPixelSize2)));
            }
        }
    }

    public void A01() {
        int i;
        if (this.A08) {
            View view = this.A0C;
            view.measure(C12980iv.A04(view.getWidth()), View.MeasureSpec.makeMeasureSpec(0, 0));
            if ((((float) view.getMeasuredHeight()) <= this.A00 && view.getHeight() != view.getMeasuredHeight()) || (((float) view.getMeasuredHeight()) > this.A00 && C12990iw.A03(view) != this.A00)) {
                AnonymousClass0B5 r2 = (AnonymousClass0B5) view.getLayoutParams();
                ((ViewGroup.MarginLayoutParams) r2).height = Math.min(view.getMeasuredHeight(), (int) this.A00);
                r2.A02 = 0;
                view.setLayoutParams(r2);
            }
            if (this.A0A) {
                BottomSheetBehavior bottomSheetBehavior = this.A0F;
                if (bottomSheetBehavior.A0M) {
                    i = -1;
                } else {
                    i = bottomSheetBehavior.A09;
                }
                if (i >= view.getMeasuredHeight()) {
                    bottomSheetBehavior.A0L(view.getMeasuredHeight());
                    this.A06 = false;
                    return;
                }
            }
            this.A06 = !this.A0B;
        }
    }

    public final void A02(int i) {
        RunnableBRunnable0Shape1S0101000_I1 runnableBRunnable0Shape1S0101000_I1 = new RunnableBRunnable0Shape1S0101000_I1(this, i, 3);
        View view = this.A0C;
        ViewParent parent = view.getParent();
        if (parent == null || !parent.isLayoutRequested() || !AnonymousClass028.A0q(view)) {
            runnableBRunnable0Shape1S0101000_I1.run();
        } else {
            view.post(runnableBRunnable0Shape1S0101000_I1);
        }
    }

    public final void A03(long j) {
        BottomSheetBehavior bottomSheetBehavior = this.A0F;
        if (!bottomSheetBehavior.A0M && bottomSheetBehavior.A09 > 0) {
            AnonymousClass071 r1 = new AnonymousClass071();
            r1.A04(j);
            r1.A08(new C75763kQ(this));
            AnonymousClass073.A01((ViewGroup) this.A0C, r1);
        }
    }

    public void A04(TimeInterpolator timeInterpolator, int i, int i2, int i3, int i4) {
        if (this.A08) {
            if (this.A0F.A0B != 4) {
                i = 0;
                i2 = 0;
            }
            ValueAnimator ofFloat = ValueAnimator.ofFloat((float) i3, (float) i4);
            this.A04 = ofFloat;
            ofFloat.setInterpolator(timeInterpolator);
            this.A04.setDuration((long) i);
            this.A04.setStartDelay((long) i2);
            C12980iv.A11(this.A04, this, 5);
            this.A04.start();
        }
    }

    public final void A05(AbstractC009304r r5) {
        int i;
        boolean A1W = C12960it.A1W(r5);
        this.A08 = A1W;
        StringBuilder A0k = C12960it.A0k("CallControlBottomSheetBehaviorController setBehavior ");
        A0k.append(A1W);
        C12960it.A1F(A0k);
        View view = this.A0C;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof AnonymousClass0B5) {
            AnonymousClass0B5 r1 = (AnonymousClass0B5) layoutParams;
            if (r1.A0A != r5) {
                r1.A00(r5);
                view.setLayoutParams(layoutParams);
                AnonymousClass2UH r12 = this.A05;
                if (this.A08 || (i = this.A02) == 0) {
                    i = this.A0F.A0B;
                }
                r12.A01(view, i);
                return;
            }
            return;
        }
        throw C12970iu.A0f("The view is not a child of CoordinatorLayout");
    }

    public void A06(boolean z) {
        int i;
        if (this.A08 && !this.A0A) {
            BottomSheetBehavior bottomSheetBehavior = this.A0F;
            if (bottomSheetBehavior.A0M) {
                i = -1;
            } else {
                i = bottomSheetBehavior.A09;
            }
            float f = ((float) i) * 0.07f;
            View view = this.A0C;
            if (z) {
                f = -f;
            }
            view.setTranslationY(f);
        }
    }

    public final void A07(boolean z) {
        ValueAnimator valueAnimator = this.A03;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        float f = 0.0f;
        if (z) {
            f = 1.0f;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(this.A0C.getAlpha(), f);
        this.A03 = ofFloat;
        ofFloat.setDuration(200L);
        this.A03.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.3Je
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                C64523Fw r1 = C64523Fw.this;
                float A00 = C12960it.A00(valueAnimator2);
                r1.A0C.setAlpha(A00);
                r1.A0G.A1P(0, A00 - 1.0f);
            }
        });
        this.A03.addListener(new AnonymousClass2Ya(this, f));
        this.A03.start();
    }

    public boolean A08() {
        return this.A08 && this.A0F.A0B == 3;
    }

    public boolean A09() {
        int i;
        if (this.A08 || (i = this.A02) == 0) {
            i = this.A0F.A0B;
        }
        return C12960it.A1V(i, 5);
    }
}
