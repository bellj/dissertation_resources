package X;

import X.AbstractC11940h7;
import X.AnonymousClass0LL;
import X.AnonymousClass0PZ;
import X.AnonymousClass0UP;
import X.AnonymousClass0ZY;
import X.C16700pc;
import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.os.IBinder;
import android.util.Log;
import androidx.window.sidecar.SidecarDeviceState;
import androidx.window.sidecar.SidecarInterface;
import androidx.window.sidecar.SidecarProvider;
import androidx.window.sidecar.SidecarWindowLayoutInfo;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0ZY  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0ZY implements AbstractC12430hu {
    public AbstractC11940h7 A00;
    public final AnonymousClass0UP A01;
    public final SidecarInterface A02;
    public final Map A03 = new LinkedHashMap();
    public final Map A04 = new LinkedHashMap();

    public AnonymousClass0ZY(Context context) {
        SidecarInterface sidecarImpl = SidecarProvider.getSidecarImpl(context.getApplicationContext());
        AnonymousClass0UP r0 = new AnonymousClass0UP(AnonymousClass0JH.QUIET);
        this.A02 = sidecarImpl;
        this.A01 = r0;
    }

    public final AnonymousClass0PZ A00(Activity activity) {
        SidecarDeviceState sidecarDeviceState;
        C16700pc.A0E(activity, 0);
        IBinder A00 = AnonymousClass0LL.A00(activity);
        if (A00 == null) {
            return new AnonymousClass0PZ(C16770pj.A0G());
        }
        SidecarInterface sidecarInterface = this.A02;
        SidecarWindowLayoutInfo sidecarWindowLayoutInfo = null;
        if (sidecarInterface != null) {
            sidecarWindowLayoutInfo = sidecarInterface.getWindowLayoutInfo(A00);
        }
        AnonymousClass0UP r1 = this.A01;
        if (sidecarInterface == null || (sidecarDeviceState = sidecarInterface.getDeviceState()) == null) {
            sidecarDeviceState = new SidecarDeviceState();
        }
        return r1.A04(sidecarDeviceState, sidecarWindowLayoutInfo);
    }

    public final SidecarInterface A01() {
        return this.A02;
    }

    public final void A02(Activity activity, IBinder iBinder) {
        Map map = this.A04;
        map.put(iBinder, activity);
        SidecarInterface sidecarInterface = this.A02;
        if (sidecarInterface != null) {
            sidecarInterface.onWindowLayoutChangeListenerAdded(iBinder);
        }
        if (map.size() == 1 && sidecarInterface != null) {
            sidecarInterface.onDeviceStateListenersChanged(false);
        }
        AbstractC11940h7 r1 = this.A00;
        if (r1 != null) {
            r1.AYW(activity, A00(activity));
        }
        Map map2 = this.A03;
        if (map2.get(activity) == null) {
            AnonymousClass0V2 r0 = new AnonymousClass0V2(activity, this);
            map2.put(activity, r0);
            activity.registerComponentCallbacks(r0);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0119 A[Catch: all -> 0x0131, TryCatch #0 {all -> 0x0131, blocks: (B:3:0x0002, B:5:0x0007, B:7:0x000d, B:9:0x001b, B:10:0x001f, B:17:0x002f, B:19:0x003b, B:21:0x0049, B:22:0x004d, B:27:0x005a, B:29:0x0060, B:31:0x006e, B:32:0x0072, B:35:0x007c, B:37:0x0082, B:39:0x0090, B:40:0x0094, B:42:0x009c, B:43:0x00a2, B:44:0x00a5, B:46:0x00ce, B:48:0x00d6, B:49:0x00dd, B:50:0x00de, B:52:0x00f9, B:53:0x0100, B:54:0x0101, B:55:0x010c, B:56:0x010d, B:57:0x0118, B:58:0x0119, B:59:0x0124, B:60:0x0125, B:61:0x0130), top: B:63:0x0002, inners: #1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03() {
        /*
        // Method dump skipped, instructions count: 307
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0ZY.A03():boolean");
    }

    @Override // X.AbstractC12430hu
    public void AYV(Activity activity) {
        C16700pc.A0E(activity, 0);
        IBinder A00 = AnonymousClass0LL.A00(activity);
        if (A00 != null) {
            SidecarInterface sidecarInterface = this.A02;
            if (sidecarInterface != null) {
                sidecarInterface.onWindowLayoutChangeListenerRemoved(A00);
            }
            Map map = this.A03;
            activity.unregisterComponentCallbacks((ComponentCallbacks) map.get(activity));
            map.remove(activity);
            Map map2 = this.A04;
            boolean z = false;
            if (map2.size() == 1) {
                z = true;
            }
            map2.remove(A00);
            if (z && sidecarInterface != null) {
                sidecarInterface.onDeviceStateListenersChanged(true);
            }
        }
    }

    @Override // X.AbstractC12430hu
    public void Ac7(AbstractC11940h7 r5) {
        this.A00 = new AnonymousClass0ZX(r5);
        SidecarInterface sidecarInterface = this.A02;
        if (sidecarInterface != null) {
            sidecarInterface.setSidecarCallback(new SidecarInterface.SidecarCallback(this.A01, new SidecarInterface.SidecarCallback() { // from class: androidx.window.layout.SidecarCompat$TranslatingCallback
                public void onDeviceStateChanged(SidecarDeviceState sidecarDeviceState) {
                    SidecarInterface A01;
                    C16700pc.A0E(sidecarDeviceState, 0);
                    AnonymousClass0ZY r52 = AnonymousClass0ZY.this;
                    for (Activity activity : r52.A04.values()) {
                        IBinder A00 = AnonymousClass0LL.A00(activity);
                        SidecarWindowLayoutInfo sidecarWindowLayoutInfo = null;
                        if (!(A00 == null || (A01 = r52.A01()) == null)) {
                            sidecarWindowLayoutInfo = A01.getWindowLayoutInfo(A00);
                        }
                        AbstractC11940h7 r1 = r52.A00;
                        if (r1 != null) {
                            r1.AYW(activity, r52.A01.A04(sidecarDeviceState, sidecarWindowLayoutInfo));
                        }
                    }
                }

                public void onWindowLayoutChanged(IBinder iBinder, SidecarWindowLayoutInfo sidecarWindowLayoutInfo) {
                    SidecarDeviceState sidecarDeviceState;
                    C16700pc.A0E(iBinder, 0);
                    C16700pc.A0E(sidecarWindowLayoutInfo, 1);
                    AnonymousClass0ZY r3 = AnonymousClass0ZY.this;
                    Activity activity = (Activity) r3.A04.get(iBinder);
                    if (activity == null) {
                        Log.w("SidecarCompat", "Unable to resolve activity from window token. Missing a call to #onWindowLayoutChangeListenerAdded()?");
                        return;
                    }
                    AnonymousClass0UP r1 = r3.A01;
                    SidecarInterface A01 = r3.A01();
                    if (A01 == null || (sidecarDeviceState = A01.getDeviceState()) == null) {
                        sidecarDeviceState = new SidecarDeviceState();
                    }
                    AnonymousClass0PZ A04 = r1.A04(sidecarDeviceState, sidecarWindowLayoutInfo);
                    AbstractC11940h7 r0 = r3.A00;
                    if (r0 != null) {
                        r0.AYW(activity, A04);
                    }
                }
            }) { // from class: androidx.window.layout.SidecarCompat$DistinctSidecarElementCallback
                public SidecarDeviceState A00;
                public final AnonymousClass0UP A01;
                public final SidecarInterface.SidecarCallback A02;
                public final WeakHashMap A03 = new WeakHashMap();
                public final ReentrantLock A04 = new ReentrantLock();

                {
                    C16700pc.A0E(r2, 1);
                    this.A01 = r2;
                    this.A02 = r3;
                }

                public void onDeviceStateChanged(SidecarDeviceState sidecarDeviceState) {
                    C16700pc.A0E(sidecarDeviceState, 0);
                    ReentrantLock reentrantLock = this.A04;
                    reentrantLock.lock();
                    try {
                        if (!AnonymousClass0UP.A01(this.A00, sidecarDeviceState)) {
                            this.A00 = sidecarDeviceState;
                            this.A02.onDeviceStateChanged(sidecarDeviceState);
                        }
                    } finally {
                        reentrantLock.unlock();
                    }
                }

                public void onWindowLayoutChanged(IBinder iBinder, SidecarWindowLayoutInfo sidecarWindowLayoutInfo) {
                    C16700pc.A0E(iBinder, 0);
                    C16700pc.A0E(sidecarWindowLayoutInfo, 1);
                    synchronized (this.A04) {
                        WeakHashMap weakHashMap = this.A03;
                        if (!this.A01.A06((SidecarWindowLayoutInfo) weakHashMap.get(iBinder), sidecarWindowLayoutInfo)) {
                            weakHashMap.put(iBinder, sidecarWindowLayoutInfo);
                            this.A02.onWindowLayoutChanged(iBinder, sidecarWindowLayoutInfo);
                        }
                    }
                }
            });
        }
    }
}
