package X;

/* renamed from: X.1Xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30421Xi extends AbstractC16130oV implements AbstractC16400ox, AbstractC16420oz {
    public C38111nX A00;

    public C30421Xi(C16150oX r11, AnonymousClass1IS r12, C30421Xi r13, long j, boolean z) {
        super(r11, r12, r13, r13.A0y, j, z);
        C38111nX r0 = r13.A00;
        if (r0 != null) {
            this.A00 = new C38111nX(r0.A01);
            A0T(32768);
        }
    }

    public C30421Xi(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 2, j);
    }

    public boolean A1B() {
        if (((AbstractC15340mz) this).A08 == 1 || !"audio/ogg; codecs=opus".equals(((AbstractC16130oV) this).A06) || !C30051Vw.A19(this)) {
            return false;
        }
        return true;
    }

    public boolean A1C() {
        int i = ((AbstractC15340mz) this).A08;
        if (i == 1 || ((i != 1 && "audio/ogg; codecs=opus".equals(((AbstractC16130oV) this).A06) && !C30051Vw.A19(this)) || A1B())) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006b, code lost:
        if (r8 != false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0073, code lost:
        if (android.text.TextUtils.isEmpty(((X.AbstractC16130oV) r16).A05) != false) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0075, code lost:
        r1 = android.util.Base64.decode(((X.AbstractC16130oV) r16).A05, 0);
        r9 = X.AbstractC27881Jp.A01(r1, 0, r1.length);
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 4;
        r1.A05 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0095, code lost:
        if (android.text.TextUtils.isEmpty(((X.AbstractC16130oV) r16).A04) != false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0097, code lost:
        r1 = android.util.Base64.decode(((X.AbstractC16130oV) r16).A04, 0);
        r9 = X.AbstractC27881Jp.A01(r1, 0, r1.length);
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 128;
        r1.A04 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b3, code lost:
        if (r8 == false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b9, code lost:
        if (((X.AbstractC16130oV) r16).A01 <= 0) goto L_0x01d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00bb, code lost:
        r0 = ((X.AbstractC16130oV) r16).A01;
        r2.A03();
        r10 = (X.C40841sQ) r2.A00;
        r10.A00 |= 8;
        r10.A02 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00cc, code lost:
        if (r8 != false) goto L_0x01d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ce, code lost:
        r0 = ((X.AbstractC16130oV) r16).A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d2, code lost:
        if (r0 > 0) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d4, code lost:
        r9 = new java.lang.StringBuilder("FMessageAudio/buildE2eMessage/sending audio with media size not set, size=");
        r9.append(r0);
        r9.append("; message.key=");
        r9.append(r16.A0z);
        com.whatsapp.util.Log.w(r9.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ef, code lost:
        r9 = ((X.AbstractC16130oV) r16).A00;
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 16;
        r1.A01 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0102, code lost:
        if (((X.AbstractC15340mz) r16).A08 != 1) goto L_0x0105;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0104, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0105, code lost:
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 32;
        r1.A0D = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0114, code lost:
        if (r8 == false) goto L_0x011a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0118, code lost:
        if (r5.A0U == null) goto L_0x0131;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x011a, code lost:
        r6 = r5.A0U;
        r6 = X.AbstractC27881Jp.A01(r6, 0, r6.length);
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 64;
        r1.A06 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0131, code lost:
        r0 = r16.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0133, code lost:
        if (r0 == null) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0135, code lost:
        r6 = r0.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0137, code lost:
        if (r6 == null) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0139, code lost:
        r1 = r6.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x013a, code lost:
        if (r1 <= 0) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x013e, code lost:
        if (r1 > 192) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0140, code lost:
        r6 = X.AbstractC27881Jp.A01(r6, 0, r1);
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 4096;
        r1.A08 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0154, code lost:
        r0 = r5.A0B;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0158, code lost:
        if (r0 <= 0) goto L_0x016c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x015a, code lost:
        r2.A03();
        r7 = (X.C40841sQ) r2.A00;
        r7.A00 |= 512;
        r7.A03 = r0 / 1000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x016c, code lost:
        r12 = r17.A04;
        r14 = r17.A09;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0174, code lost:
        if (X.C32411c7.A0U(r12, r16, r14) == false) goto L_0x018f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0176, code lost:
        r0 = X.C32411c7.A0P(r17.A00, r17.A02, r12, r16, r14, r17.A06);
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A09 = r0;
        r1.A00 |= androidx.core.view.inputmethod.EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0195, code lost:
        if (android.text.TextUtils.isEmpty(r5.A0G) != false) goto L_0x01be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0197, code lost:
        r4 = r5.A0G;
        r2.A03();
        r1 = (X.C40841sQ) r2.A00;
        r1.A00 |= 256;
        r1.A0A = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01be, code lost:
        r1 = new java.lang.StringBuilder("FMessageAudio/buildE2eMessage/sending audio with directPath not set; message.key=");
        r1.append(r16.A0z);
        com.whatsapp.util.Log.w(r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01d4, code lost:
        if (((X.AbstractC16130oV) r16).A00 <= 0) goto L_0x0100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01de, code lost:
        if (android.text.TextUtils.isEmpty(((X.AbstractC16130oV) r16).A06) == false) goto L_0x005a;
     */
    @Override // X.AbstractC16420oz
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A6k(X.C39971qq r17) {
        /*
        // Method dump skipped, instructions count: 608
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C30421Xi.A6k(X.1qq):void");
    }

    @Override // X.AbstractC16400ox
    public /* bridge */ /* synthetic */ AbstractC15340mz A7M(AnonymousClass1IS r8) {
        return new C30421Xi(((AbstractC16130oV) this).A02, r8, this, this.A0I, true);
    }
}
