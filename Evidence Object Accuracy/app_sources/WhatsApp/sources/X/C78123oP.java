package X;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;

/* renamed from: X.3oP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78123oP extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99294k4();
    public final int A00;
    public final ParcelFileDescriptor A01;

    public C78123oP(ParcelFileDescriptor parcelFileDescriptor, int i) {
        this.A00 = i;
        this.A01 = parcelFileDescriptor;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A0A(parcel, this.A01, i, A00);
    }
}
