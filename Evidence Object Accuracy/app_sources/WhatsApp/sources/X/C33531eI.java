package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1eI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33531eI extends AnonymousClass02M {
    public List A00 = new ArrayList();
    public final /* synthetic */ C33521eH A01;

    public /* synthetic */ C33531eI(C33521eH r2) {
        this.A01 = r2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    public void A0E(AbstractC14640lm r3) {
        if (r3 != null) {
            for (AnonymousClass4OQ r0 : this.A00) {
                if (r3.equals(r0.A01)) {
                    A02();
                }
            }
            return;
        }
        A02();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r8, int i) {
        C75573k4 r82 = (C75573k4) r8;
        AnonymousClass4OQ r6 = (AnonymousClass4OQ) this.A00.get(i);
        UserJid userJid = r6.A01;
        r82.A00 = userJid;
        C33521eH r5 = this.A01;
        C15370n3 A0B = r5.A08.A0B(userJid);
        r5.A0C.A07(r82.A01, A0B, false);
        r82.A03.A0G(null, r5.A0B.A07(A0B));
        r82.A02.setText(C38131nZ.A01(r5.A0G, r5.A0E.A02(r6.A00)));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75573k4(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.status_details_row, viewGroup, false));
    }
}
