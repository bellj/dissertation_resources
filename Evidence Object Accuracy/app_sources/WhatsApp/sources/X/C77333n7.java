package X;

import android.app.UiModeManager;
import android.content.Context;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.Display;
import android.view.WindowManager;

/* renamed from: X.3n7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77333n7 extends AnonymousClass4X6 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public AnonymousClass1Mr A08;
    public AnonymousClass1Mr A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public final SparseArray A0G;
    public final SparseBooleanArray A0H;

    @Deprecated
    public C77333n7() {
        A02();
        this.A0G = new SparseArray();
        this.A0H = new SparseBooleanArray();
    }

    public C77333n7(Context context) {
        Point point;
        UiModeManager uiModeManager;
        String str;
        String str2;
        int i = AnonymousClass3JZ.A01;
        if (i >= 19) {
            A00(context);
        }
        A02();
        this.A0G = new SparseArray();
        this.A0H = new SparseBooleanArray();
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (i <= 29 && defaultDisplay.getDisplayId() == 0 && (uiModeManager = (UiModeManager) context.getApplicationContext().getSystemService("uimode")) != null && uiModeManager.getCurrentModeType() == 4) {
            if (!"Sony".equals(AnonymousClass3JZ.A04) || !AnonymousClass3JZ.A05.startsWith("BRAVIA") || !context.getPackageManager().hasSystemFeature("com.sony.dtv.hardware.panel.qfhd")) {
                if (i < 28) {
                    str = "sys.display-size";
                } else {
                    str = "vendor.display-size";
                }
                try {
                    Class<?> cls = Class.forName("android.os.SystemProperties");
                    str2 = (String) cls.getMethod("get", String.class).invoke(cls, str);
                } catch (Exception e) {
                    C64923Hl.A01("Util", C12960it.A0d(str, C12960it.A0k("Failed to read system property ")), e);
                    str2 = null;
                }
                if (!TextUtils.isEmpty(str2)) {
                    try {
                        String[] split = str2.trim().split("x", -1);
                        if (split.length == 2) {
                            int parseInt = Integer.parseInt(split[0]);
                            int parseInt2 = Integer.parseInt(split[1]);
                            if (parseInt > 0 && parseInt2 > 0) {
                                point = new Point(parseInt, parseInt2);
                            }
                        }
                    } catch (NumberFormatException unused) {
                    }
                    Log.e("Util", C12960it.A0d(str2, C12960it.A0k("Invalid display size: ")));
                }
            } else {
                point = new Point(3840, 2160);
            }
            int i2 = point.x;
            int i3 = point.y;
            this.A07 = i2;
            this.A06 = i3;
            this.A0F = true;
        }
        point = new Point();
        if (i >= 23) {
            AnonymousClass3JZ.A0D(point, defaultDisplay);
        } else if (i >= 17) {
            AnonymousClass3JZ.A0C(point, defaultDisplay);
        } else {
            defaultDisplay.getSize(point);
        }
        int i2 = point.x;
        int i3 = point.y;
        this.A07 = i2;
        this.A06 = i3;
        this.A0F = true;
    }

    public C77343n8 A01() {
        int i = this.A05;
        int i2 = this.A04;
        int i3 = this.A03;
        int i4 = this.A02;
        boolean z = this.A0E;
        boolean z2 = this.A0B;
        int i5 = this.A07;
        int i6 = this.A06;
        boolean z3 = this.A0F;
        AnonymousClass1Mr r11 = this.A09;
        AnonymousClass1Mr r10 = super.A01;
        int i7 = this.A01;
        int i8 = this.A00;
        boolean z4 = this.A0C;
        return new C77343n8(this.A0G, this.A0H, r11, r10, this.A08, super.A02, i, i2, i3, i4, i5, i6, i7, i8, super.A00, z, z2, z3, z4, this.A0D, this.A0A);
    }

    public final void A02() {
        this.A05 = Integer.MAX_VALUE;
        this.A04 = Integer.MAX_VALUE;
        this.A03 = Integer.MAX_VALUE;
        this.A02 = Integer.MAX_VALUE;
        this.A0E = true;
        this.A0B = true;
        this.A07 = Integer.MAX_VALUE;
        this.A06 = Integer.MAX_VALUE;
        this.A0F = true;
        AnonymousClass1Mr of = AnonymousClass1Mr.of();
        this.A09 = of;
        this.A01 = Integer.MAX_VALUE;
        this.A00 = Integer.MAX_VALUE;
        this.A0C = true;
        this.A08 = of;
        this.A0D = true;
        this.A0A = true;
    }
}
