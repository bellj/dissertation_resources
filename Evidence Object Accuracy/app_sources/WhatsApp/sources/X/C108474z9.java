package X;

import android.content.Context;

/* renamed from: X.4z9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108474z9 implements AbstractC115575Sc {
    @Override // X.AbstractC115575Sc
    public final AnonymousClass4PK AbV(Context context, AbstractC116365Vd r5, String str) {
        AnonymousClass4PK r2 = new AnonymousClass4PK();
        int Ah1 = r5.Ah1(context, str, true);
        r2.A01 = Ah1;
        if (Ah1 != 0) {
            r2.A02 = 1;
        } else {
            int Agj = r5.Agj(context, str);
            r2.A00 = Agj;
            if (Agj != 0) {
                r2.A02 = -1;
                return r2;
            }
        }
        return r2;
    }
}
