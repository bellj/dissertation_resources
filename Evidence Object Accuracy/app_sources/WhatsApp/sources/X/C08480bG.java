package X;

/* renamed from: X.0bG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08480bG implements AbstractC12120hP {
    public String A00;
    public boolean A01;

    public C08480bG(String str, boolean z) {
        this.A01 = z;
        this.A00 = str;
    }

    @Override // X.AbstractC12120hP
    public boolean ALK(AnonymousClass0K2 r7, AnonymousClass0I1 r8) {
        String str;
        if (!this.A01 || this.A00 != null) {
            str = this.A00;
        } else {
            str = r8.A00();
        }
        AbstractC12490i0 r0 = ((AnonymousClass0OO) r8).A00;
        if (r0 != null) {
            int i = 0;
            for (AnonymousClass0OO r02 : r0.ABO()) {
                if (str == null || r02.A00().equals(str)) {
                    i++;
                }
            }
            if (i != 1) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        Object[] objArr;
        String str;
        if (this.A01) {
            objArr = new Object[]{this.A00};
            str = "only-of-type <%s>";
        } else {
            objArr = new Object[0];
            str = "only-child";
        }
        return String.format(str, objArr);
    }
}
