package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49Z  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49Z extends Enum {
    public static final /* synthetic */ AnonymousClass49Z[] A00;

    static {
        AnonymousClass49Z r14 = new AnonymousClass49Z("DIRECTION", 0);
        AnonymousClass49Z r12 = new AnonymousClass49Z("FLEX_DIRECTION", 1);
        AnonymousClass49Z r10 = new AnonymousClass49Z("JUSTIFY_CONTENT", 2);
        AnonymousClass49Z r0 = new AnonymousClass49Z("ALIGN_CONTENT", 3);
        AnonymousClass49Z r8 = new AnonymousClass49Z("ALIGN_ITEMS", 4);
        AnonymousClass49Z r7 = new AnonymousClass49Z("FLEX_WRAP", 5);
        AnonymousClass49Z r6 = new AnonymousClass49Z("OVERFLOW", 6);
        AnonymousClass49Z r5 = new AnonymousClass49Z("PADDING", 7);
        AnonymousClass49Z r4 = new AnonymousClass49Z("PADDING_PERCENT", 8);
        AnonymousClass49Z r2 = new AnonymousClass49Z("BORDER", 9);
        AnonymousClass49Z[] r1 = new AnonymousClass49Z[10];
        r1[0] = r14;
        r1[1] = r12;
        r1[2] = r10;
        r1[3] = r0;
        r1[4] = r8;
        C12970iu.A1R(r7, r6, r5, r4, r1);
        r1[9] = r2;
        A00 = r1;
    }

    public AnonymousClass49Z(String str, int i) {
    }

    public static AnonymousClass49Z[] values() {
        return (AnonymousClass49Z[]) A00.clone();
    }
}
