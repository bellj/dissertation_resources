package X;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.facebook.redex.ViewOnClickCListenerShape1S0300000_I1;
import com.facebook.redex.ViewOnClickCListenerShape1S0400000_I1;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.conversationslist.ViewHolder;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.chromium.net.UrlRequest;

/* renamed from: X.1dE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33061dE extends AnonymousClass02M implements AbstractC33071dF {
    public List A00 = new CopyOnWriteArrayList();
    public final int A01;
    public final Activity A02;
    public final Context A03;
    public final LayoutInflater A04;
    public final AnonymousClass12P A05;
    public final C253519b A06;
    public final C14900mE A07;
    public final C15570nT A08;
    public final C15450nH A09;
    public final AnonymousClass11L A0A;
    public final AnonymousClass2TT A0B;
    public final C14650lo A0C;
    public final C238013b A0D;
    public final C22640zP A0E;
    public final AnonymousClass1BF A0F;
    public final AnonymousClass130 A0G;
    public final C15550nR A0H;
    public final C15610nY A0I;
    public final AnonymousClass1J1 A0J;
    public final C63563Cb A0K;
    public final AbstractC33091dK A0L;
    public final C63543Bz A0M;
    public final C14830m7 A0N;
    public final C16590pI A0O;
    public final C14820m6 A0P;
    public final AnonymousClass018 A0Q;
    public final C19990v2 A0R;
    public final C241714m A0S;
    public final C15600nX A0T;
    public final C236812p A0U;
    public final AnonymousClass10Y A0V;
    public final C21400xM A0W;
    public final AnonymousClass11F A0X;
    public final C14850m9 A0Y;
    public final C22050yP A0Z;
    public final C20710wC A0a;
    public final AbstractC37301ly A0b;
    public final AbstractC37291lx A0c;
    public final AbstractC33041dC A0d;
    public final AbstractC37281lw A0e;
    public final C15580nU A0f;
    public final AnonymousClass13H A0g;
    public final C22710zW A0h;
    public final C17070qD A0i;
    public final AnonymousClass14X A0j;
    public final C15860o1 A0k;
    public final AnonymousClass12F A0l;
    public final AbstractC14440lR A0m;

    @Override // X.AbstractC33071dF
    public boolean AdR() {
        return true;
    }

    public C33061dE(Activity activity, AnonymousClass12P r5, C253519b r6, C14900mE r7, C15570nT r8, C15450nH r9, AnonymousClass11L r10, AnonymousClass2TT r11, C14650lo r12, C238013b r13, C22640zP r14, AnonymousClass1BF r15, AnonymousClass130 r16, C15550nR r17, C15610nY r18, AnonymousClass1J1 r19, AbstractC33091dK r20, C63543Bz r21, C14830m7 r22, C16590pI r23, C14820m6 r24, AnonymousClass018 r25, C19990v2 r26, C241714m r27, C15600nX r28, C236812p r29, AnonymousClass10Y r30, C21400xM r31, AnonymousClass11F r32, C14850m9 r33, C22050yP r34, C20710wC r35, AbstractC37301ly r36, AbstractC37291lx r37, AbstractC33041dC r38, AbstractC37281lw r39, C15580nU r40, AnonymousClass13H r41, C22710zW r42, C17070qD r43, AnonymousClass14X r44, C15860o1 r45, AnonymousClass12F r46, AbstractC14440lR r47, int i) {
        this.A0d = r38;
        this.A03 = activity;
        this.A04 = LayoutInflater.from(activity);
        this.A0N = r22;
        this.A0Y = r33;
        this.A07 = r7;
        this.A0c = r37;
        this.A02 = activity;
        this.A0g = r41;
        this.A08 = r8;
        this.A0O = r23;
        this.A0m = r47;
        this.A0R = r26;
        this.A09 = r9;
        this.A0M = r21;
        this.A0j = r44;
        this.A05 = r5;
        this.A0G = r16;
        this.A06 = r6;
        this.A0H = r17;
        this.A0S = r27;
        this.A0Q = r25;
        this.A0I = r18;
        this.A0i = r43;
        this.A0B = r11;
        this.A0D = r13;
        this.A0a = r35;
        this.A0V = r30;
        this.A0l = r46;
        this.A0k = r45;
        this.A0Z = r34;
        this.A0J = r19;
        this.A0e = r39;
        this.A0b = r36;
        this.A0X = r32;
        this.A0W = r31;
        this.A0P = r24;
        this.A0E = r14;
        this.A0F = r15;
        this.A0U = r29;
        this.A0h = r42;
        this.A0C = r12;
        this.A0T = r28;
        this.A0A = r10;
        this.A0L = r20;
        this.A0K = new C63563Cb(new ExecutorC27271Gr(r47, true));
        this.A0f = r40;
        this.A01 = i;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    public void A0E() {
        this.A07.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(this, 2));
    }

    @Override // X.AbstractC33071dF
    public int ADJ(int i) {
        while (i >= 0) {
            if (AJU(i)) {
                return i;
            }
            i--;
        }
        return -1;
    }

    @Override // X.AbstractC33071dF
    public boolean AJU(int i) {
        return getItemViewType(i) == 3;
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r18, int i) {
        String str;
        Resources resources;
        int i2;
        Object[] objArr;
        if (r18 instanceof C75303jd) {
            GroupJid groupJid = (GroupJid) ((AnonymousClass4Ww) this.A00.get(i)).A01;
            C53032cF r2 = ((C75303jd) r18).A00;
            r2.A05 = groupJid;
            r2.A02 = r2.A00.A0B(groupJid);
            r2.A00();
        }
        if (r18 instanceof C75323jf) {
            ((C75323jf) r18).A00.A03 = (GroupJid) ((AnonymousClass4Ww) this.A00.get(i)).A01;
        }
        if (r18 instanceof C55112ho) {
            C55112ho r7 = (C55112ho) r18;
            AnonymousClass1OU r10 = (AnonymousClass1OU) ((AnonymousClass4Ww) this.A00.get(i)).A01;
            C15580nU r9 = this.A0f;
            GroupJid groupJid2 = r10.A02;
            C28801Pb r1 = r7.A03;
            r1.A08(r10.A03);
            Context context = r7.A04;
            r1.A04(AnonymousClass00T.A00(context, R.color.list_item_title));
            ImageView imageView = r7.A00;
            StringBuilder sb = new StringBuilder();
            sb.append(r7.A06.A00(R.string.transition_avatar));
            sb.append(C15380n4.A03(groupJid2));
            AnonymousClass028.A0k(imageView, sb.toString());
            C15370n3 A0B = r7.A08.A0B(groupJid2);
            if (r10.A00 == 3) {
                imageView.setImageDrawable(r7.A0C.A00(imageView.getContext().getTheme(), imageView.getResources(), AnonymousClass51L.A00, R.drawable.avatar_announcement));
                TextEmojiLabel textEmojiLabel = r7.A02;
                textEmojiLabel.setVisibility(0);
                r7.A01.setVisibility(0);
                textEmojiLabel.setText(context.getString(R.string.community_home_announcement_group));
            } else {
                r7.A09.A06(imageView, A0B);
                r7.A02.setVisibility(8);
                r7.A01.setVisibility(8);
            }
            r7.A0H.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(r7, groupJid2, r9, r10, 5));
        } else if (r18 instanceof C75553k2) {
            C75553k2 r72 = (C75553k2) r18;
            C90264Nf r4 = (C90264Nf) ((AnonymousClass4Ww) this.A00.get(i)).A01;
            ImageView imageView2 = r72.A01;
            Context context2 = r72.A00;
            boolean z = r4.A01;
            int i3 = R.drawable.ic_chevron_down;
            if (z) {
                i3 = R.drawable.ic_chevron_up;
            }
            imageView2.setImageDrawable(AnonymousClass00T.A04(context2, i3));
            TextView textView = r72.A02;
            int i4 = R.string.subgroup_view_more;
            if (z) {
                i4 = R.string.subgroup_view_less;
            }
            textView.setText(i4);
            r72.A0H.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(r72, 10, r4));
        } else if (r18 instanceof C75313je) {
            int intValue = ((Number) ((AnonymousClass4Ww) this.A00.get(i)).A01).intValue();
            C52912bt r22 = ((C75313je) r18).A00;
            r22.A01.setText(r22.getContext().getString(intValue));
        } else if (r18 instanceof C75273ja) {
            ((C75273ja) r18).A00 = this.A0f;
        } else if (r18 instanceof C54892hS) {
        } else {
            if (r18 instanceof C36381jn) {
                C36381jn r73 = (C36381jn) r18;
                AnonymousClass1PE r3 = (AnonymousClass1PE) ((AnonymousClass4Ww) this.A00.get(i)).A01;
                long j = this.A0P.A00.getLong("previous_last_seen_community_activity", 0);
                r73.A04.setText(r3.A06());
                C15370n3 A0A = r73.A06.A0A(r3.A05());
                if (A0A != null) {
                    r73.A07.A06(r73.A01, A0A);
                }
                int i5 = (r3.A0H > j ? 1 : (r3.A0H == j ? 0 : -1));
                WaTextView waTextView = r73.A03;
                int i6 = 8;
                if (i5 > 0) {
                    i6 = 0;
                }
                waTextView.setVisibility(i6);
                r73.A0H.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(r73, 33, r3));
            } else if (r18 instanceof C54922hV) {
                r18.A0H.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(r18, 11, ((AnonymousClass4Ww) this.A00.get(i)).A01));
            } else if (r18 instanceof ViewHolder) {
                ((ViewHolder) r18).A0G(this.A02, this.A03, (AnonymousClass2WJ) ((AnonymousClass4Ww) this.A00.get(i)).A01, null, this.A0M, null, this.A01, i, false);
            } else if (r18 instanceof C75293jc) {
                C15580nU r42 = (C15580nU) ((AnonymousClass4Ww) this.A00.get(i)).A01;
                C53132cx r5 = ((C75293jc) r18).A00;
                r5.A05 = r42;
                C15370n3 A0B2 = r5.A01.A0B(r42);
                r5.A03 = A0B2;
                AnonymousClass1E5 r32 = r5.A04;
                boolean A00 = r32.A00(A0B2);
                View view = r5.A08;
                if (A00) {
                    view.setVisibility(0);
                    r5.A0B.setVisibility(8);
                    r5.A09.setVisibility(0);
                } else {
                    view.setVisibility(8);
                    r5.A0B.setVisibility(0);
                    r5.A09.setVisibility(8);
                }
                if (r5.A02.A0G(r42) && !r32.A00(r5.A03)) {
                    View view2 = r5.A0A;
                    Context context3 = view2.getContext();
                    View A0D = AnonymousClass028.A0D(view2, R.id.deactivate_community_btn);
                    A0D.setVisibility(0);
                    A0D.setOnClickListener(new ViewOnClickCListenerShape1S0300000_I1(r5, context3, r42, 13));
                }
            } else if (r18 instanceof C75283jb) {
                AbstractC15340mz r52 = (AbstractC15340mz) ((AnonymousClass4Ww) this.A00.get(i)).A01;
                C53122cw r6 = ((C75283jb) r18).A00;
                TextEmojiLabel textEmojiLabel2 = r6.A04;
                if (C30041Vv.A0O(r6.A00, r52)) {
                    AnonymousClass1Y5 r0 = (AnonymousClass1Y5) r52;
                    AnonymousClass11L r102 = r6.A01;
                    int i7 = r0.A00;
                    List A01 = AnonymousClass11L.A01(r0.A03, 3);
                    Pair A002 = AnonymousClass11L.A00(A01, i7);
                    int intValue2 = ((Number) A002.first).intValue();
                    if (intValue2 == 1) {
                        resources = r102.A06.A00.getResources();
                        i2 = AnonymousClass11L.A0F[1];
                        objArr = new Object[]{A01.get(0), A01.get(1), A002.second};
                    } else if (intValue2 == 2 || intValue2 == 3) {
                        str = r102.A06.A00.getString(AnonymousClass11L.A0F[intValue2], A01.get(0), A01.get(1));
                    } else {
                        resources = r102.A06.A00.getResources();
                        i2 = AnonymousClass11L.A0F[0];
                        objArr = new Object[]{Integer.valueOf(i7)};
                    }
                    str = resources.getQuantityString(i2, i7, objArr);
                } else {
                    AnonymousClass009.A07("CommunityActivityView/unexpected community activity");
                    str = null;
                }
                textEmojiLabel2.A0G(null, str);
                r6.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(r6, 41, r52));
            }
        }
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (i) {
            case -1:
                return new AnonymousClass445(new View(viewGroup.getContext()));
            case 0:
            default:
                return new AnonymousClass445(new View(viewGroup.getContext()));
            case 1:
                return new C75303jd(new C53032cF(viewGroup.getContext()));
            case 2:
                return new C75323jf(new C53172d2(viewGroup.getContext(), this.A0e));
            case 3:
                return new C75313je(new C52912bt(viewGroup.getContext()));
            case 4:
                AnonymousClass3J9 r49 = AnonymousClass3J9.A01;
                C14830m7 r1 = this.A0N;
                C14850m9 r12 = this.A0Y;
                AnonymousClass13H r13 = this.A0g;
                C15570nT r14 = this.A08;
                C16590pI r15 = this.A0O;
                AbstractC14440lR r16 = this.A0m;
                C19990v2 r17 = this.A0R;
                C15450nH r18 = this.A09;
                AnonymousClass14X r19 = this.A0j;
                AnonymousClass130 r110 = this.A0G;
                C15550nR r111 = this.A0H;
                C253519b r112 = this.A06;
                C241714m r113 = this.A0S;
                C15610nY r114 = this.A0I;
                AnonymousClass018 r115 = this.A0Q;
                C17070qD r116 = this.A0i;
                C238013b r117 = this.A0D;
                C20710wC r118 = this.A0a;
                AnonymousClass10Y r119 = this.A0V;
                AnonymousClass12F r152 = this.A0l;
                C15860o1 r142 = this.A0k;
                AnonymousClass1J1 r10 = this.A0J;
                C21400xM r9 = this.A0W;
                C14820m6 r8 = this.A0P;
                C236812p r7 = this.A0U;
                C22710zW r6 = this.A0h;
                C14650lo r5 = this.A0C;
                C15600nX r4 = this.A0T;
                AnonymousClass11L r3 = this.A0A;
                AbstractC33091dK r2 = this.A0L;
                return new ViewHolder(viewGroup.getContext(), LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversations_row, viewGroup, false), r112, r14, r18, r3, r5, r117, r110, r111, r114, r10, this.A0K, r2, r1, r15, r8, r115, r17, r113, r4, r7, r119, r9, r12, r118, r13, r6, r116, r19, r142, r152, r49, r16);
            case 5:
                View inflate = this.A04.inflate(R.layout.group_chat_info_row_v2, viewGroup, false);
                Activity activity = this.A02;
                AbstractC14440lR r132 = this.A0m;
                C19990v2 r92 = this.A0R;
                AnonymousClass12P r32 = this.A05;
                C15550nR r62 = this.A0H;
                AnonymousClass2TT r42 = this.A0B;
                C15610nY r72 = this.A0I;
                AnonymousClass12F r122 = this.A0l;
                AnonymousClass11F r11 = this.A0X;
                return new C55112ho(activity, inflate, r32, r42, this.A0E, r62, r72, this.A0J, r92, this.A0T, r11, r122, r132);
            case 6:
                return new C75553k2(this.A03, this.A04.inflate(R.layout.expand_row, viewGroup, false), this.A0c);
            case 7:
                View inflate2 = this.A04.inflate(R.layout.parent_row, viewGroup, false);
                Activity activity2 = this.A02;
                C22050yP r63 = this.A0Z;
                AnonymousClass11F r52 = this.A0X;
                return new C54892hS(activity2, inflate2, this.A0F, this.A0P, r52, r63);
            case 8:
                View inflate3 = this.A04.inflate(R.layout.parent_row, viewGroup, false);
                Activity activity3 = this.A02;
                AbstractC14440lR r73 = this.A0m;
                return new C36381jn(activity3, inflate3, this.A05, this.A0E, this.A0H, this.A0J, r73);
            case 9:
                return new C54922hV(this.A02, this.A04.inflate(R.layout.see_all_row, viewGroup, false), this.A05, this.A0Q);
            case 10:
                View inflate4 = this.A04.inflate(R.layout.empty_community_row, viewGroup, false);
                Activity activity4 = this.A02;
                C22050yP r53 = this.A0Z;
                return new C617131v(activity4, inflate4, this.A0F, this.A0P, r53);
            case 11:
                return new C75293jc(new C53132cx(viewGroup.getContext()));
            case 12:
                return new C75273ja(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.community_home_new_group, viewGroup, false), this.A0T, this.A0f);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return new C75283jb(new C53122cw(viewGroup.getContext(), this.A0d));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        if (i < 0) {
            return -1;
        }
        List list = this.A00;
        if (i < list.size()) {
            return ((AnonymousClass4Ww) list.get(i)).A00;
        }
        return -1;
    }
}
