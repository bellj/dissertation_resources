package X;

import android.graphics.Bitmap;
import android.util.Pair;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Fg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64363Fg {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public Bitmap A07;
    public Pair A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public boolean A0M;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;
    public boolean A0Q;
    public final C15370n3 A0R;
    public final UserJid A0S;

    public /* synthetic */ C64363Fg(C15370n3 r1, UserJid userJid) {
        this.A0S = userJid;
        this.A0R = r1;
    }

    public boolean A00() {
        Pair pair = this.A08;
        return C12960it.A05(pair.first) == -1 && C12960it.A05(pair.second) != -1;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C64363Fg)) {
            return false;
        }
        C64363Fg r4 = (C64363Fg) obj;
        if (this.A0S.equals(r4.A0S) && r4.A0R.equals(this.A0R) && C29941Vi.A00(r4.A08, this.A08) && r4.A0G == this.A0G && r4.A0E == this.A0E && r4.A0A == this.A0A && r4.A0H == this.A0H && r4.A0F == this.A0F && r4.A01 == this.A01 && r4.A0B == this.A0B && r4.A00 == this.A00 && r4.A0N == this.A0N && r4.A0K == this.A0K && r4.A0J == this.A0J && r4.A05 == this.A05 && r4.A0M == this.A0M && r4.A0O == this.A0O && r4.A09 == this.A09 && r4.A03 == this.A03 && C29941Vi.A00(r4.A07, this.A07) && r4.A0L == this.A0L && r4.A0D == this.A0D && r4.A0P == this.A0P && r4.A02 == this.A02 && r4.A0I == this.A0I && r4.A04 == this.A04 && r4.A0Q == this.A0Q && r4.A06 == this.A06 && r4.A0C == this.A0C) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int hashCode;
        int A08 = ((((((((((((((((((((((((((((((((C12990iw.A08(this.A08, C12990iw.A08(this.A0R, (217 + this.A0S.hashCode()) * 31) * 31) * 31) + (this.A0G ? 1 : 0)) * 31) + (this.A0E ? 1 : 0)) * 31) + (this.A0A ? 1 : 0)) * 31) + (this.A0H ? 1 : 0)) * 31) + (this.A0F ? 1 : 0)) * 31) + this.A01) * 31) + (this.A0B ? 1 : 0)) * 31) + this.A00) * 31) + (this.A0N ? 1 : 0)) * 31) + (this.A0K ? 1 : 0)) * 31) + (this.A0J ? 1 : 0)) * 31) + this.A05) * 31) + (this.A0M ? 1 : 0)) * 31) + (this.A0O ? 1 : 0)) * 31) + (this.A09 ? 1 : 0)) * 31) + this.A03) * 31;
        Bitmap bitmap = this.A07;
        if (bitmap == null) {
            hashCode = 0;
        } else {
            hashCode = bitmap.hashCode();
        }
        return ((((((((((((((((((A08 + hashCode) * 31) + (this.A0L ? 1 : 0)) * 31) + (this.A0D ? 1 : 0)) * 31) + (this.A0P ? 1 : 0)) * 31) + this.A02) * 31) + (this.A0I ? 1 : 0)) * 31) + (this.A0Q ? 1 : 0)) * 31) + this.A04) * 31) + this.A06) * 31) + (this.A0C ? 1 : 0);
    }

    public final String toString() {
        return this.A0S.toString();
    }
}
