package X;

import com.whatsapp.R;
import com.whatsapp.identity.IdentityVerificationActivity;

/* renamed from: X.32f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617732f extends AnonymousClass4V2 {
    public final /* synthetic */ IdentityVerificationActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C617732f(IdentityVerificationActivity identityVerificationActivity) {
        super(identityVerificationActivity);
        this.A00 = identityVerificationActivity;
    }

    @Override // X.AnonymousClass4V2
    public void A00() {
        String A04;
        IdentityVerificationActivity identityVerificationActivity = this.A00;
        C15370n3 r1 = identityVerificationActivity.A0J;
        String str = null;
        if (!(r1 == null || (A04 = identityVerificationActivity.A0E.A04(r1)) == null)) {
            str = ((ActivityC13830kP) identityVerificationActivity).A01.A0F(A04);
        }
        String A0o = C12990iw.A0o(identityVerificationActivity.getResources(), str, new Object[1], 0, R.string.security_code_changed_dialog_title);
        AnonymousClass2AC r12 = new AnonymousClass2AC(C12990iw.A0o(identityVerificationActivity.getResources(), str, new Object[1], 0, R.string.security_code_changed_dialog_message));
        r12.A09 = A0o;
        r12.A00 = 101;
        identityVerificationActivity.Adl(r12.A01(), null);
        identityVerificationActivity.A0U = true;
    }
}
