package X;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.registration.directmigration.MigrationProviderOrderedBroadcastReceiver;
import com.whatsapp.util.Log;

/* renamed from: X.1Fr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27011Fr {
    public final C16590pI A00;
    public final C14820m6 A01;
    public final C20850wQ A02;
    public final C16490p7 A03;
    public final C15830ny A04;
    public final AbstractC14440lR A05;

    public C27011Fr(C16590pI r1, C14820m6 r2, C20850wQ r3, C16490p7 r4, C15830ny r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A05 = r6;
        this.A04 = r5;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    public void A00() {
        String string = this.A01.A00.getString("registration_sibling_app_country_code", null);
        StringBuilder sb = new StringBuilder("InterAppCommunicationManager/migrateFromConsumerAppFlowEnabled/sibling-country-code = ");
        sb.append(string);
        Log.i(sb.toString());
        StringBuilder sb2 = new StringBuilder("InterAppCommunicationManager/smbIsCapableOfMigratingFromConsumer=");
        sb2.append(false);
        Log.i(sb2.toString());
        StringBuilder sb3 = new StringBuilder("InterAppCommunicationManager/migrateFromConsumerAppFlowEnabled = ");
        sb3.append(false);
        Log.i(sb3.toString());
    }

    public void A01() {
        Bundle bundle = new Bundle();
        StringBuilder sb = new StringBuilder("InterAppCommunicationManager/smbIsCapableOfMigratingFromConsumer=");
        sb.append(false);
        Log.i(sb.toString());
        bundle.putBoolean("database_migration_is_enabled_on_requester_side", false);
        Log.i("InterAppCommunicationManager/sendInitialMigrationInfoNeededBroadcast/sendInitialMigrationInfoNeededBroadcast");
        A03("com.whatsapp.registration.directmigration.initialMigrationInfoAction", bundle);
    }

    public void A02() {
        Bundle bundle = new Bundle();
        bundle.putInt("migration_state_on_provider_side", 2);
        Log.i("InterAppCommunicationManager/setDirectMigrationStateOnProviderSide");
        A03("com.whatsapp.registration.directmigration.setMigrationStateOnProviderSide", bundle);
    }

    public final void A03(String str, Bundle bundle) {
        StringBuilder sb = new StringBuilder("InterAppCommunicationManager/sendRequesterToProviderOrderedBroadcast/action = ");
        sb.append(str);
        Log.i(sb.toString());
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.whatsapp.w4b", MigrationProviderOrderedBroadcastReceiver.class.getName()));
        intent.setAction(str);
        intent.addFlags(32);
        new RunnableBRunnable0Shape1S0300000_I0_1(this, intent, bundle, 43).run();
    }
}
