package X;

import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.registration.RegisterPhone;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.2x3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2x3 extends AnonymousClass4WI {
    public final /* synthetic */ RegisterPhone A00;

    public AnonymousClass2x3(RegisterPhone registerPhone) {
        this.A00 = registerPhone;
    }

    @Override // X.AnonymousClass4WI
    public void A00() {
        this.A00.A2i();
    }

    @Override // X.AnonymousClass4WI
    public void A01(String str, String str2) {
        RegisterPhone registerPhone = this.A00;
        ((AbstractActivityC452520u) registerPhone).A09.A04.setContentDescription(null);
        int i = 0;
        if ("".equals(str)) {
            ((AbstractActivityC452520u) registerPhone).A09.A04.setText(R.string.register_choose_country);
        } else if (str2 == null) {
            ((AbstractActivityC452520u) registerPhone).A09.A04.setText(R.string.register_invalid_cc);
            registerPhone.A2h();
        } else {
            String A02 = ((AbstractActivityC452520u) registerPhone).A0I.A02(((ActivityC13830kP) registerPhone).A01, str2);
            ((AbstractActivityC452520u) registerPhone).A09.A04.setText(A02);
            ((AbstractActivityC452520u) registerPhone).A09.A04.setContentDescription(C12960it.A0X(registerPhone, A02, C12970iu.A1b(), 0, R.string.register_selected_country_content_description));
            registerPhone.A2i();
        }
        try {
            TextEmojiLabel textEmojiLabel = registerPhone.A09;
            if (!"eu".equals(registerPhone.A07.A03(str))) {
                i = 8;
            }
            textEmojiLabel.setVisibility(i);
        } catch (IOException e) {
            Log.e("register/phone/countrywatcher/aftertextchanged getTosRegion failed", e);
            registerPhone.A09.setVisibility(8);
        }
    }
}
