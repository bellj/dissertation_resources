package X;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;

/* renamed from: X.3DF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3DF {
    public final Activity A00;
    public final View A01;
    public final AnonymousClass12V A02;
    public final C18160s0 A03;
    public final C18120rw A04;
    public final C235812f A05;

    public AnonymousClass3DF(Activity activity, View view, AnonymousClass12V r4, C18160s0 r5, C18120rw r6, C235812f r7) {
        C16700pc.A0E(view, 1);
        this.A01 = view;
        this.A00 = activity;
        this.A05 = r7;
        this.A02 = r4;
        this.A03 = r5;
        this.A04 = r6;
    }

    public final void A00() {
        C12960it.A0t(((SharedPreferences) C16700pc.A05(this.A03.A01)).edit(), "pref_has_dismissed_sticker_upsell", true);
        this.A01.setVisibility(8);
    }
}
