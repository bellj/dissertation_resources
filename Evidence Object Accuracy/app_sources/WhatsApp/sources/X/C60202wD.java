package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;

/* renamed from: X.2wD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60202wD extends AbstractC75743kL {
    public final TextEmojiLabel A00;
    public final WaTextView A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final C21250x7 A04;
    public final C20710wC A05;

    public C60202wD(View view, ParticipantsListViewModel participantsListViewModel, C15550nR r4, C15610nY r5, C21250x7 r6, C20710wC r7) {
        super(view, participantsListViewModel);
        this.A00 = C12970iu.A0T(view, R.id.group_name);
        this.A01 = C12960it.A0N(view, R.id.participant_count);
        this.A04 = r6;
        this.A02 = r4;
        this.A03 = r5;
        this.A05 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        if (r0 == false) goto L_0x003a;
     */
    @Override // X.AbstractC75743kL
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.C92194Ux r10) {
        /*
            r9 = this;
            com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel r2 = r9.A00
            if (r2 == 0) goto L_0x006e
            com.whatsapp.jid.GroupJid r0 = r2.A05()
            if (r0 == 0) goto L_0x006e
            android.view.View r4 = r9.A0H
            android.content.res.Resources r6 = r4.getResources()
            com.whatsapp.jid.GroupJid r7 = r2.A05()
            X.0nR r5 = r9.A02
            X.0nY r3 = r9.A03
            X.0x7 r1 = r9.A04
            X.0wC r0 = r9.A05
            java.lang.String r8 = X.AnonymousClass1SF.A09(r5, r3, r1, r0, r7)
            r5 = 0
            r7 = 1
            if (r8 == 0) goto L_0x0046
            com.whatsapp.TextEmojiLabel r3 = r9.A00
            r3.setText(r8)
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x0080
            X.2Ht r0 = r2.A07
            X.3HK r0 = r0.A05()
            boolean r0 = r0.A0D
        L_0x0035:
            r1 = 2131892737(0x7f121a01, float:1.942023E38)
            if (r0 != 0) goto L_0x003d
        L_0x003a:
            r1 = 2131892842(0x7f121a6a, float:1.9420444E38)
        L_0x003d:
            java.lang.Object[] r0 = new java.lang.Object[r7]
            java.lang.String r0 = X.C12990iw.A0o(r6, r8, r0, r5, r1)
            r3.setContentDescription(r0)
        L_0x0046:
            boolean r0 = r2.A03
            if (r0 == 0) goto L_0x006f
            X.2Ht r0 = r2.A07
            X.3HK r0 = r0.A05()
            X.0qP r0 = r0.A00
        L_0x0052:
            int r3 = r0.size()
        L_0x0056:
            int r3 = r3 - r7
            com.whatsapp.WaTextView r2 = r9.A01
            r1 = 2131755164(0x7f10009c, float:1.91412E38)
            java.lang.Object[] r0 = new java.lang.Object[r7]
            X.C12960it.A1P(r0, r3, r5)
            X.C12980iv.A15(r6, r2, r0, r1, r3)
            r1 = 48
            com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1 r0 = new com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1
            r0.<init>(r9, r1)
            r4.post(r0)
        L_0x006e:
            return
        L_0x006f:
            X.2CT r0 = r2.A01
            if (r0 == 0) goto L_0x007e
            com.whatsapp.voipcalling.VoipActivityV2 r0 = r0.A00
            com.whatsapp.voipcalling.CallInfo r0 = r0.A2i()
            if (r0 == 0) goto L_0x007e
            java.util.Map r0 = r0.participants
            goto L_0x0052
        L_0x007e:
            r3 = 0
            goto L_0x0056
        L_0x0080:
            X.2CT r0 = r2.A01
            if (r0 == 0) goto L_0x003a
            com.whatsapp.voipcalling.VoipActivityV2 r0 = r0.A00
            com.whatsapp.voipcalling.CallInfo r0 = r0.A2i()
            if (r0 == 0) goto L_0x003a
            boolean r0 = r0.videoEnabled
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60202wD.A09(X.4Ux):void");
    }
}
