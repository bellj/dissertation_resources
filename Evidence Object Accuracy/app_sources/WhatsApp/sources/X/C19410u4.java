package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/* renamed from: X.0u4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19410u4 extends AbstractC18220s6 {
    public final Context A00;
    public final C18250s9 A01;
    public final C19380u1 A02;
    public final C18230s7 A03;
    public final AnonymousClass01d A04;
    public final C14820m6 A05;
    public final C19400u3 A06;
    public final C19390u2 A07;
    public final C19370u0 A08;
    public final AnonymousClass01H A09;

    public C19410u4(Context context, C18250s9 r2, C19380u1 r3, C18230s7 r4, AnonymousClass01d r5, C14820m6 r6, C19400u3 r7, C19390u2 r8, C19370u0 r9, AnonymousClass01H r10) {
        super(context);
        this.A00 = context;
        this.A03 = r4;
        this.A08 = r9;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = r8;
        this.A06 = r7;
        this.A05 = r6;
        this.A09 = r10;
        this.A01 = r2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 7, insn: 0x0509: IGET  (r0 I:X.0u1) = (r7 I:X.0u4) X.0u4.A02 X.0u1, block:B:62:0x0509
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    @Override // X.AbstractC18220s6
    public void A01(
/*
[1295] Method generation error in method: X.0u4.A01(android.content.Intent):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r40v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public final void A02() {
        AlarmManager A04 = this.A04.A04();
        if (A04 == null) {
            Log.w("DailyCronAction/dailyCatchupCron; AlarmManager is null");
            return;
        }
        PendingIntent A00 = A00("com.whatsapp.action.DAILY_CATCHUP_CRON", 536870912);
        if (!A04()) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences sharedPreferences = this.A05.A00;
            long j = sharedPreferences.getLong("next_daily_cron_catchup", 0);
            long j2 = j - currentTimeMillis;
            if (A00 == null || j2 <= 0 || j2 >= 900000) {
                long j3 = currentTimeMillis + 900000;
                this.A03.A02(A00("com.whatsapp.action.DAILY_CATCHUP_CRON", 0), 1, j3);
                sharedPreferences.edit().putLong("next_daily_cron_catchup", j3).apply();
                C38121nY.A02(j3);
                C38121nY.A02(sharedPreferences.getLong("last_daily_cron", 0));
                return;
            }
            C38121nY.A02(j);
        } else if (A00 != null) {
            A04.cancel(A00);
            A00.cancel();
        }
    }

    public final void A03() {
        long nextInt;
        Calendar instance = Calendar.getInstance();
        instance.add(5, 1);
        instance.set(14, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, 0);
        long timeInMillis = instance.getTimeInMillis();
        C18250s9 r0 = this.A01;
        C15450nH r2 = r0.A00;
        Random random = r0.A01;
        int A02 = r2.A02(AbstractC15460nI.A1t);
        if (A02 <= 0) {
            nextInt = 0;
        } else {
            nextInt = ((long) random.nextInt(A02 << 1)) * 1000;
        }
        long j = timeInMillis + nextInt;
        StringBuilder sb = new StringBuilder("DailyCronAction/setupDailyCronAlarm; alarmTimeMillis=");
        sb.append(new Date(j));
        Log.i(sb.toString());
        if (!this.A03.A02(A00("com.whatsapp.action.DAILY_CRON", 134217728), 0, j)) {
            Log.w("DailyCronAction/setupDailyCronAlarm AlarmManager is null");
        }
    }

    public final boolean A04() {
        long j = this.A05.A00.getLong("last_daily_cron", 0);
        Calendar instance = Calendar.getInstance();
        instance.set(14, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, 0);
        long timeInMillis = instance.getTimeInMillis();
        long j2 = 86400000 + timeInMillis;
        if (j >= timeInMillis && j < j2) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (currentTimeMillis <= 0 || currentTimeMillis >= 21600000) {
            return false;
        }
        return true;
    }
}
