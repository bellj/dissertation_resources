package X;

import android.os.SystemClock;

/* renamed from: X.1Pv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28991Pv implements AbstractC28981Pu {
    public long A00 = 0;
    public boolean A01 = false;
    public final long A02;
    public final C18280sC A03;
    public final C14830m7 A04;

    public C28991Pv(C18280sC r5, C14830m7 r6) {
        this.A04 = r6;
        this.A02 = 5000;
        this.A03 = r5;
    }

    @Override // X.AbstractC28981Pu
    public Integer A8s() {
        return 1;
    }

    @Override // X.AbstractC28981Pu
    public synchronized boolean isValid() {
        boolean z;
        long uptimeMillis = SystemClock.uptimeMillis();
        if (uptimeMillis - this.A00 < this.A02) {
            z = this.A01;
        } else {
            this.A00 = uptimeMillis;
            try {
                C32441cA r3 = this.A03.A00;
                int i = r3.A00;
                if ((i != 1 || r3.A01 > 0 || r3.A03 > 0 || r3.A02 != Integer.MIN_VALUE) && (!r3.A01() || i != 2)) {
                    z = false;
                    this.A01 = z;
                }
                z = true;
                this.A01 = z;
            } catch (Exception unused) {
                z = false;
                this.A01 = false;
            }
        }
        return z;
    }
}
