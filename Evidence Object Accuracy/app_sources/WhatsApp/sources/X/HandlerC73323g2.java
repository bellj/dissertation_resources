package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.3g2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class HandlerC73323g2 extends Handler {
    public final /* synthetic */ AnonymousClass3IZ A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC73323g2(Looper looper, AnonymousClass3IZ r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AbstractC116455Vm r0 = this.A00.A02;
        if (r0 != null) {
            r0.AMr();
            sendEmptyMessageDelayed(0, (long) AnonymousClass3IZ.A0X);
        }
    }
}
