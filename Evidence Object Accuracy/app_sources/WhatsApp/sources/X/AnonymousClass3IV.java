package X;

import android.text.TextUtils;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.FutureTask;

/* renamed from: X.3IV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IV {
    public boolean A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C20870wS A03;
    public final C18790t3 A04;
    public final C14950mJ A05;
    public final C14850m9 A06;
    public final C20110vE A07;
    public final C19940uv A08;
    public final C14430lQ A09;
    public final AnonymousClass12J A0A;
    public final C28921Pn A0B;
    public final C28781Oz A0C;
    public final C41471ta A0D;
    public final C43161wW A0E;
    public final C22600zL A0F;
    public final C22590zK A0G;
    public final C26511Dt A0H;
    public final C26521Du A0I;
    public final AbstractC14440lR A0J;
    public final C21710xr A0K;
    public final URL A0L;

    public AnonymousClass3IV(AbstractC15710nm r2, C14330lG r3, C20870wS r4, C18790t3 r5, C14950mJ r6, C14850m9 r7, C20110vE r8, C19940uv r9, C14430lQ r10, AnonymousClass12J r11, C28921Pn r12, C28781Oz r13, C41471ta r14, C43161wW r15, C22600zL r16, C22590zK r17, C26511Dt r18, C26521Du r19, AbstractC14440lR r20, C21710xr r21, URL url) {
        this.A06 = r7;
        this.A01 = r2;
        this.A0J = r20;
        this.A02 = r3;
        this.A05 = r6;
        this.A0F = r16;
        this.A0H = r18;
        this.A03 = r4;
        this.A0I = r19;
        this.A0G = r17;
        this.A0K = r21;
        this.A0A = r11;
        this.A08 = r9;
        this.A07 = r8;
        this.A0L = url;
        this.A0D = r14;
        this.A0E = r15;
        this.A04 = r5;
        this.A09 = r10;
        this.A0B = r12;
        this.A0C = r13;
    }

    public static AnonymousClass1RN A00(C28921Pn r8, AnonymousClass4QM r9, C41471ta r10, File file, File file2, String str, String str2, URL url) {
        int A00 = AnonymousClass3J1.A00(r9, r10, file2, str, url);
        FutureTask futureTask = ((AbstractRunnableC14570le) r8).A02;
        if (!futureTask.isCancelled()) {
            if (A00 != 0) {
                return new AnonymousClass1RN(A00, null, true);
            }
            if (file.equals(file2)) {
                return new AnonymousClass1RN(null, r9.A02, 0, true);
            }
            int A01 = AnonymousClass3J1.A01(r10, str2, url);
            if (!futureTask.isCancelled()) {
                if (A01 == 0) {
                    String str3 = r10.A0H;
                    String str4 = "enc";
                    if (str3 != null) {
                        String A002 = AnonymousClass14P.A00(str3);
                        if (!TextUtils.isEmpty(A002)) {
                            str4 = A002;
                        }
                    }
                    return new AnonymousClass1RN(null, str4, 0, true);
                } else if (A01 == 1) {
                    return new AnonymousClass1RN(1, null, true);
                } else {
                    if (A01 == 2) {
                        return new AnonymousClass1RN(7, null, true);
                    }
                    throw new AssertionError("unknown result encountered in switch");
                }
            }
        }
        return new AnonymousClass1RN(13, null, false);
    }

    public static final void A01(C41471ta r8, File file, File file2) {
        AnonymousClass2QH r1 = new AnonymousClass2QH(r8.A09);
        byte[] bArr = r8.A0Y;
        AnonymousClass009.A05(bArr);
        C37891nB A8r = r1.A8r(bArr);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            try {
                C629539h r5 = new C629539h(A8r, fileOutputStream, file.length());
                byte[] bArr2 = new byte[DefaultCrypto.BUFFER_SIZE];
                for (int read = fileInputStream.read(bArr2, 0, DefaultCrypto.BUFFER_SIZE); read > 0; read = fileInputStream.read(bArr2, 0, DefaultCrypto.BUFFER_SIZE)) {
                    r5.write(bArr2);
                }
                if (!r5.A01) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("normaldownloadhandler/decryptFile Decryption failure in express path download for file ");
                    A0h.append(file2.getCanonicalPath());
                    A0h.append(", enc hash: ");
                    A0h.append(r8.A0E);
                    A0h.append(", plain text hash: ");
                    Log.e(C12960it.A0d(r8.A0F, A0h));
                } else {
                    file2.getCanonicalPath();
                    file2.length();
                }
                r5.close();
                fileOutputStream.close();
                fileInputStream.close();
            } catch (Throwable th) {
                try {
                    fileOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException unused2) {
            StringBuilder A0k = C12960it.A0k("normaldownloadhandler/decryptFile Decryption failure in express path download, enc hash: ");
            A0k.append(r8.A0E);
            A0k.append(", plain text hash: ");
            Log.e(C12960it.A0d(r8.A0F, A0k));
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:327:0x0165 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:324:0x02fe */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:243:0x05ad */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:233:0x059b */
    /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: X.0vE */
    /* JADX DEBUG: Multi-variable search result rejected for r27v5, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r27v8, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r26v0, types: [long] */
    /* JADX WARN: Type inference failed for: r3v9, types: [X.2QH] */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARN: Type inference failed for: r27v4 */
    /* JADX WARN: Type inference failed for: r26v4 */
    /* JADX WARN: Type inference failed for: r26v5, types: [java.lang.String] */
    /* JADX WARNING: Can't wrap try/catch for region: R(21:10|(1:12)|346|13|329|14|317|15|(2:19|(2:21|(16:23|(1:25)|26|321|27|(3:29|266|(2:268|269)(1:356))|349|33|(1:35)|(1:39)(1:38)|40|336|41|(2:43|(2:45|(4:47|327|48|(9:50|(1:52)|53|54|55|(3:57|266|(0)(0))|263|266|(0)(0)))))|60|(7:62|63|64|(3:66|266|(0)(0))|263|266|(0)(0))(12:67|68|(4:72|(1:74)|76|77)|79|(21:81|(1:85)|86|333|87|323|88|344|89|(6:345|93|315|94|(2:95|(1:97)(1:350))|98)|324|105|343|106|(1:108)|109|339|110|111|(1:(10:113|(1:115)|116|(1:118)|119|120|121|(2:123|(1:130))|131|(1:138)(3:351|137|142))(2:352|139))|(14:3ed|146|(1:148)|149|(1:151)|152|153|154|155|156|(3:158|266|(0)(0))|263|266|(0)(0))(1:(12:160|(1:162)|163|164|165|166|270|(4:272|297|(1:299)|300)|296|297|(0)|300)(17:167|(1:169)(1:171)|170|(1:173)|174|175|176|177|(1:181)|182|(3:(1:187)|188|(1:196))|197|198|(3:200|266|(0)(0))|263|266|(0)(0))))|251|252|253|(3:255|266|(0)(0))|263|266|(0)(0)))))|30|349|33|(0)|(0)|39|40|336|41|(0)|60|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x05f2, code lost:
        r11 = new X.AnonymousClass1RN(1, null, r46.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x05ff, code lost:
        if (r0.A0A != null) goto L_0x0601;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x063f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x0640, code lost:
        r1 = X.C12960it.A0h();
        r1.append("MediaDownload/MMS download failed with IOException while retrieving response code; mediaHash=");
        r1.append(r4.A0F);
        com.whatsapp.util.Log.w(X.C12960it.A0Z(r49, "; url=", r1), r2);
        X.C28471Ni.A01(r0, r2, r49);
        r1 = new X.AnonymousClass1RN(X.AnonymousClass4EO.A00(r2), null, r46.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x066a, code lost:
        if (r0.A0A != null) goto L_0x066c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x066d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:282:0x066e, code lost:
        r0.A04();
        X.C28471Ni.A01(r0, r3, r49);
        r2 = r3.responseCode;
        r0.A0H = X.C12980iv.A0l(r2);
        r1 = r46.A0F;
        com.whatsapp.util.Log.i(X.C12960it.A0W(r2, "routeselector/onmediatransfererrororresponsecode/code "));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x068b, code lost:
        if (r2 == 401) goto L_0x0691;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x0691, code lost:
        r1.A0A();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x0694, code lost:
        r1 = new X.AnonymousClass1RN(r3.downloadStatus, null, r46.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x06a2, code lost:
        if (r0.A0A != null) goto L_0x06a4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x06a5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x06a6, code lost:
        X.C28471Ni.A01(r0, r1, r49);
        r1 = new X.AnonymousClass1RN(r1.downloadStatus, null, r46.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:294:0x06b9, code lost:
        if (r0.A0A != null) goto L_0x06bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x023f, code lost:
        if (r0 > 0) goto L_0x0241;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0259, code lost:
        if (r2.A02(r2, r4.A04, r2, false, r40, r4.A0U, r4.A0T) != false) goto L_0x025b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x05d9  */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x062b A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x06c5  */
    /* JADX WARNING: Removed duplicated region for block: B:356:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0107 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x014d A[Catch: all -> 0x05ed, TryCatch #14 {all -> 0x05ed, blocks: (B:41:0x011d, B:43:0x014d, B:45:0x015b, B:48:0x0165, B:50:0x0177, B:52:0x0186, B:53:0x018c, B:59:0x01c2, B:60:0x01dc, B:62:0x01f9, B:68:0x0222, B:70:0x0228, B:72:0x022d, B:77:0x0243, B:79:0x025b, B:83:0x0267, B:85:0x026f, B:86:0x0275, B:87:0x0287, B:154:0x0432, B:165:0x0454, B:176:0x04a1, B:177:0x04a4, B:179:0x04ae, B:181:0x04b4, B:182:0x04b9, B:185:0x04d6, B:187:0x04dc, B:188:0x04df, B:190:0x04e5, B:192:0x04e9, B:194:0x04ef, B:196:0x04f5, B:211:0x053d, B:219:0x055c, B:228:0x058d, B:242:0x05ac, B:246:0x05b2, B:251:0x05da), top: B:336:0x011d, inners: #7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01f9 A[Catch: all -> 0x05ed, TRY_LEAVE, TryCatch #14 {all -> 0x05ed, blocks: (B:41:0x011d, B:43:0x014d, B:45:0x015b, B:48:0x0165, B:50:0x0177, B:52:0x0186, B:53:0x018c, B:59:0x01c2, B:60:0x01dc, B:62:0x01f9, B:68:0x0222, B:70:0x0228, B:72:0x022d, B:77:0x0243, B:79:0x025b, B:83:0x0267, B:85:0x026f, B:86:0x0275, B:87:0x0287, B:154:0x0432, B:165:0x0454, B:176:0x04a1, B:177:0x04a4, B:179:0x04ae, B:181:0x04b4, B:182:0x04b9, B:185:0x04d6, B:187:0x04dc, B:188:0x04df, B:190:0x04e5, B:192:0x04e9, B:194:0x04ef, B:196:0x04f5, B:211:0x053d, B:219:0x055c, B:228:0x058d, B:242:0x05ac, B:246:0x05b2, B:251:0x05da), top: B:336:0x011d, inners: #7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0221  */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1RN A02(X.C28481Nj r47, java.io.File r48, java.net.URL r49, boolean r50) {
        /*
        // Method dump skipped, instructions count: 1779
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3IV.A02(X.1Nj, java.io.File, java.net.URL, boolean):X.1RN");
    }

    public final void A03(String str) {
        if (str == null) {
            Log.e("normalDownloadHandler/cancelExpressPathFileCleanUp cancel work with empty enc hash");
        } else {
            ((AnonymousClass022) this.A0K.get()).A08(str);
        }
    }
}
