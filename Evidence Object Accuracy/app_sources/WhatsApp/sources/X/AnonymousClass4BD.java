package X;

/* renamed from: X.4BD  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BD {
    A04(0),
    A02(1),
    A03(2),
    A01(3);
    
    public final int value;

    AnonymousClass4BD(int i) {
        this.value = i;
    }
}
