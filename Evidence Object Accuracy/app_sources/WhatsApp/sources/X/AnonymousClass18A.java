package X;

import org.json.JSONObject;

/* renamed from: X.18A  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass18A extends AnonymousClass12Z {
    public C457723b A01(JSONObject jSONObject) {
        String str;
        String str2;
        String str3;
        String string;
        if (jSONObject != null) {
            String string2 = jSONObject.getString("status");
            String A00 = C88104Eh.A00(string2);
            if (!A00.equals("UNKNOWN_IN_CLIENT")) {
                String str4 = null;
                if (jSONObject.isNull("reason") || (string = jSONObject.getString("reason")) == null) {
                    str3 = null;
                } else {
                    str3 = "OOPS";
                    if (!str3.equals(string)) {
                        str3 = "OTHER";
                    }
                }
                if (!jSONObject.isNull("reason_url")) {
                    str4 = jSONObject.getString("reason_url");
                }
                return new C457723b(A00, str3, str4);
            }
            StringBuilder sb = new StringBuilder("State is invalid in ban appeal status: ");
            sb.append(string2);
            sb.append(", ");
            if (!(this instanceof AnonymousClass18C)) {
                str2 = "whatsapp_support_ban_appeal_status";
            } else {
                str2 = "whatsapp_support_process_ban_appeal_request";
            }
            sb.append(str2);
            throw new Exception(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder("Value of ");
        if (!(this instanceof AnonymousClass18C)) {
            str = "whatsapp_support_ban_appeal_status";
        } else {
            str = "whatsapp_support_process_ban_appeal_request";
        }
        sb2.append(str);
        sb2.append(" is null while submitting ban appeal");
        throw new Exception(sb2.toString());
    }
}
