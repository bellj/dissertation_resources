package X;

import android.os.Bundle;
import android.view.View;
import com.whatsapp.payments.ui.NoviWithdrawLocationDetailsSheet;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.fragment.NoviWithdrawCashReviewSheet;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.6CY  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CY implements AbstractC136466Mq {
    public final /* synthetic */ PaymentBottomSheet A00;
    public final /* synthetic */ AbstractC136466Mq A01;
    public final /* synthetic */ NoviWithdrawCashReviewSheet A02;
    public final /* synthetic */ AbstractC118095bG A03;

    public AnonymousClass6CY(PaymentBottomSheet paymentBottomSheet, AbstractC136466Mq r2, NoviWithdrawCashReviewSheet noviWithdrawCashReviewSheet, AbstractC118095bG r4) {
        this.A03 = r4;
        this.A02 = noviWithdrawCashReviewSheet;
        this.A01 = r2;
        this.A00 = paymentBottomSheet;
    }

    @Override // X.AbstractC136466Mq
    public void AOC() {
        AbstractC136466Mq r0 = this.A01;
        if (r0 != null) {
            r0.AOC();
        }
        this.A00.A1B();
    }

    @Override // X.AbstractC136466Mq
    public void AOV(View view) {
        NoviWithdrawCashReviewSheet noviWithdrawCashReviewSheet = this.A02;
        noviWithdrawCashReviewSheet.A04.A6Q(new AnonymousClass4OZ(1, noviWithdrawCashReviewSheet.A07));
        AbstractC136466Mq r0 = this.A01;
        if (r0 != null) {
            r0.AOV(view);
        }
    }

    @Override // X.AbstractC136466Mq
    public void AYM(View view) {
        AbstractC118095bG r3 = this.A03;
        PaymentBottomSheet paymentBottomSheet = this.A00;
        if (!(r3 instanceof C123465nC)) {
            C123475nD r32 = (C123475nD) r3;
            C128365vz r1 = new AnonymousClass610("REVIEW_FI_DETAILS", "ADD_MONEY", "REVIEW_TRANSACTION", "BODY").A00;
            r1.A0T = "DEBIT";
            C30881Ze r0 = r32.A00;
            if (r0 != null) {
                r1.A0S = r0.A0A;
            }
            r32.A0J.A05(r1);
            C117315Zl.A0Q(r32.A09);
            ((AbstractC118095bG) r32).A00.A00(new AbstractC14590lg(paymentBottomSheet, r32) { // from class: X.6Ea
                public final /* synthetic */ PaymentBottomSheet A00;
                public final /* synthetic */ C123475nD A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    C1315463e A02;
                    C123475nD r9 = this.A01;
                    PaymentBottomSheet paymentBottomSheet2 = this.A00;
                    List list = (List) obj;
                    r9.A09.A0B(Boolean.FALSE);
                    ArrayList A0l = C12960it.A0l();
                    AnonymousClass61F r8 = r9.A0K;
                    AbstractC28901Pl A00 = AnonymousClass61F.A00(list);
                    if (!(A00 == null || (A02 = AnonymousClass61F.A02(A00)) == null)) {
                        list = r8.A05(A02, list);
                    }
                    for (Object obj2 : list) {
                        if (obj2 instanceof C30881Ze) {
                            A0l.add(obj2);
                        }
                    }
                    PaymentMethodsListPickerFragment A002 = PaymentMethodsListPickerFragment.A00(A0l);
                    A002.A08 = new C121905kS(r9.A0F.A00, r9.A0G, r9.A0H, r8, r9);
                    A002.A06 = 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0051: IPUT  
                          (wrap: X.6CR : 0x004e: CONSTRUCTOR  (r0v4 X.6CR A[REMOVE]) = (r3v0 'paymentBottomSheet2' com.whatsapp.payments.ui.PaymentBottomSheet), (r9v0 'r9' X.5nD) call: X.6CR.<init>(com.whatsapp.payments.ui.PaymentBottomSheet, X.5nD):void type: CONSTRUCTOR)
                          (r1v1 'A002' com.whatsapp.payments.ui.PaymentMethodsListPickerFragment)
                         com.whatsapp.payments.ui.PaymentMethodsListPickerFragment.A06 X.6M1 in method: X.6Ea.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x004e: CONSTRUCTOR  (r0v4 X.6CR A[REMOVE]) = (r3v0 'paymentBottomSheet2' com.whatsapp.payments.ui.PaymentBottomSheet), (r9v0 'r9' X.5nD) call: X.6CR.<init>(com.whatsapp.payments.ui.PaymentBottomSheet, X.5nD):void type: CONSTRUCTOR in method: X.6Ea.accept(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:459)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6CR, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 19 more
                        */
                    /*
                        this = this;
                        X.5nD r9 = r10.A01
                        com.whatsapp.payments.ui.PaymentBottomSheet r3 = r10.A00
                        java.util.List r11 = (java.util.List) r11
                        X.1It r1 = r9.A09
                        java.lang.Boolean r0 = java.lang.Boolean.FALSE
                        r1.A0B(r0)
                        java.util.ArrayList r4 = X.C12960it.A0l()
                        X.61F r8 = r9.A0K
                        X.1Pl r0 = X.AnonymousClass61F.A00(r11)
                        if (r0 == 0) goto L_0x0023
                        X.63e r0 = X.AnonymousClass61F.A02(r0)
                        if (r0 == 0) goto L_0x0023
                        java.util.List r11 = r8.A05(r0, r11)
                    L_0x0023:
                        java.util.Iterator r2 = r11.iterator()
                    L_0x0027:
                        boolean r0 = r2.hasNext()
                        if (r0 == 0) goto L_0x0039
                        java.lang.Object r1 = r2.next()
                        boolean r0 = r1 instanceof X.C30881Ze
                        if (r0 == 0) goto L_0x0027
                        r4.add(r1)
                        goto L_0x0027
                    L_0x0039:
                        com.whatsapp.payments.ui.PaymentMethodsListPickerFragment r1 = com.whatsapp.payments.ui.PaymentMethodsListPickerFragment.A00(r4)
                        X.0pI r0 = r9.A0F
                        android.content.Context r5 = r0.A00
                        X.018 r6 = r9.A0G
                        X.0qD r7 = r9.A0H
                        X.5kS r4 = new X.5kS
                        r4.<init>(r5, r6, r7, r8, r9)
                        r1.A08 = r4
                        X.6CR r0 = new X.6CR
                        r0.<init>(r3, r9)
                        r1.A06 = r0
                        r3.A1L(r1)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C134286Ea.accept(java.lang.Object):void");
                }
            });
        } else {
            C123465nC r33 = (C123465nC) r3;
            if (r33.A00 == 1) {
                C1316163l r02 = r33.A02;
                AnonymousClass009.A05(r02);
                C1315663g r34 = new C1315663g(r02.A03, r02.A01, r02.A02, -1, true, false);
                Bundle A0D = C12970iu.A0D();
                A0D.putParcelable("withdraw-location-data", r34);
                NoviWithdrawLocationDetailsSheet noviWithdrawLocationDetailsSheet = new NoviWithdrawLocationDetailsSheet();
                noviWithdrawLocationDetailsSheet.A0U(A0D);
                paymentBottomSheet.A1L(noviWithdrawLocationDetailsSheet);
            }
        }
        AbstractC136466Mq r03 = this.A01;
        if (r03 != null) {
            r03.AYM(view);
        }
    }
}
