package X;

import android.util.Log;
import com.facebook.rendercore.RootHostView;

/* renamed from: X.547  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass547 implements AbstractC116535Vv {
    public final /* synthetic */ C73633gY A00;

    public AnonymousClass547(C73633gY r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116535Vv
    public void AOU(AnonymousClass3JI r4) {
        C64173En r1;
        C73633gY r2 = this.A00;
        RootHostView rootHostView = r2.A03;
        if (rootHostView != null) {
            rootHostView.setVisibility(0);
        }
        C64893Hi r0 = r2.A04;
        if (r0 != null) {
            r0.A04();
        }
        AbstractC116545Vw r12 = r2.A08;
        if (r12 != null) {
            r12.Abo(new C89294Jk(r4));
        }
        AnonymousClass01E r02 = r2.A02;
        if (!(r02 == null || (r1 = r2.A06) == null)) {
            r2.A04 = C64893Hi.A00(r02.A0C(), r4, r1).A00();
        }
        r2.A00();
        AbstractC115835Tc r03 = r2.A07;
        if (r03 != null) {
            r03.AaN();
        }
    }

    @Override // X.AbstractC116535Vv
    public void APq(String str) {
        Log.e("Whatsapp", str);
        AbstractC115835Tc r0 = this.A00.A07;
        if (r0 != null) {
            r0.AaN();
        }
    }
}
