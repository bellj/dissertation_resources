package X;

import androidx.constraintlayout.widget.ConstraintLayout;

/* renamed from: X.0Xh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07260Xh implements AbstractC11710gj {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public ConstraintLayout A06;
    public final /* synthetic */ ConstraintLayout A07;

    public C07260Xh(ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2) {
        this.A07 = constraintLayout;
        this.A06 = constraintLayout2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
        if (r7 == X.AnonymousClass0JR.FIXED) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0059, code lost:
        if (r8 == X.AnonymousClass0JR.FIXED) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0065, code lost:
        if (r20.A01 <= 0.0f) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006f, code lost:
        if (r20.A01 <= 0.0f) goto L_0x0071;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0093, code lost:
        if (r7 != r21.A04) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00b0, code lost:
        if (r3 != false) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0123, code lost:
        if (r2 == -1) goto L_0x008c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x013d  */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x007c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00f8 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0102 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0129 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0136  */
    @Override // X.AbstractC11710gj
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ALU(X.AnonymousClass0QV r20, X.AnonymousClass0O5 r21) {
        /*
        // Method dump skipped, instructions count: 546
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07260Xh.ALU(X.0QV, X.0O5):void");
    }
}
