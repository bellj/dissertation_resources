package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1N8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1N8 {
    public AnonymousClass1N9 A00;
    public boolean A01;
    public boolean A02;
    public final int A03;
    public final int A04;
    public final AnonymousClass2BO A05;
    public final AnonymousClass23W A06;
    public final File A07;
    public final RandomAccessFile A08;
    public final boolean A09;

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00fa, code lost:
        if (r6.A0A != false) goto L_0x00fc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1N8(X.C14820m6 r21, X.C14850m9 r22, X.AnonymousClass16D r23, X.AnonymousClass16F r24, java.io.File r25, int r26, int r27, int r28, int r29, boolean r30, boolean r31) {
        /*
        // Method dump skipped, instructions count: 408
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1N8.<init>(X.0m6, X.0m9, X.16D, X.16F, java.io.File, int, int, int, int, boolean, boolean):void");
    }

    public AnonymousClass1N8(AnonymousClass16F r13, File file, int i, int i2, int i3, boolean z) {
        this(null, null, null, r13, file, AnonymousClass2Bc.A00.length - 1, i, i2, i3, z, false);
    }

    public final int A00() {
        return this.A00.A8e().A04.A05.capacity() - C47592Bp.A00(C47592Bp.A00.length - 1).length;
    }

    public final void A01() {
        int i;
        if (!this.A02) {
            return;
        }
        if (!this.A00.AA2()) {
            Log.e("wambuffer: PERSISTENCE TURNED OFF");
            this.A02 = false;
            return;
        }
        try {
            AnonymousClass2BO r2 = this.A05;
            int i2 = r2.A06;
            if (i2 == 0) {
                i = 2;
            } else {
                i = r2.A08.A05.length;
            }
            r2.A01(i2, i);
            r2.A07.A03();
            this.A01 = false;
        } catch (IOException unused) {
            throw new NullPointerException("setWamErrorWriteHeader");
        }
    }

    public final void A02() {
        AnonymousClass1N9 r4 = this.A00;
        if (r4.A8e().A05()) {
            throw new Error("Rotation failed since the current event buffer is empty");
        } else if (r4.A6w()) {
            Locale locale = Locale.US;
            Object[] objArr = new Object[4];
            AnonymousClass23W r3 = this.A06;
            objArr[0] = Integer.valueOf(r3.A01);
            objArr[1] = Integer.valueOf(r4.A8e().A04.A05.position());
            AnonymousClass1NA A8e = r4.A8e();
            if (A8e.A04()) {
                objArr[2] = Integer.valueOf(A8e.A00);
                AnonymousClass1NA A8e2 = r4.A8e();
                objArr[3] = Long.valueOf(A8e2.A05.A05[A8e2.A02].A04);
                Log.i(String.format(locale, "wambuffer/rotate: rotated event buffer %d: size = %d, event count = %d, timestamp = %d", objArr));
                if (!this.A09 || this.A04 != 2) {
                    r3.A01 = r4.AEc(r3.A01);
                    r4.AKX();
                    this.A01 = true;
                    return;
                }
                r4.Aau();
                return;
            }
            throw new UnsupportedOperationException("No event count available for rotated buffers");
        } else {
            throw new Error("Rotation failed since there is no empty buffer");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        if (r8 != 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        r7 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0035, code lost:
        r4.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        r7 = r4.getInt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        r6 = r9.A08;
        r5 = r6.A05;
        r4 = r5.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0043, code lost:
        if (r7 > r4) goto L_0x0270;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        r9.A01(r8, r7);
        r0 = r3.A05;
        r14 = r0.position();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
        r3.A04(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0053, code lost:
        r12 = r3.A01();
        r0 = new byte[X.AnonymousClass2Bc.A00(r8).length];
        r9.A03 = r0;
        r12.get(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0064, code lost:
        if (r8 != 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0066, code lost:
        r10 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0068, code lost:
        r12.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        r10 = r12.getInt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0071, code lost:
        r3 = java.nio.ByteOrder.LITTLE_ENDIAN;
        r12.order(r3);
        r9.A01 = r12.getInt();
        r12.order(r3);
        r9.A00 = r12.getInt();
        r12.order(r3);
        r0 = r12.getInt();
        r6.A01 = r0;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x008f, code lost:
        if (r0 < r10) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0091, code lost:
        r6.A01 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0093, code lost:
        if (r8 < 2) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0095, code lost:
        r12.order(r3);
        r6.A00 = r12.getInt();
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a3, code lost:
        if (r12.get() == 1) goto L_0x00a6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a5, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a6, code lost:
        r6.A04 = r0;
        r6.A03 = X.AnonymousClass2BO.A00(r12);
        r12.order(r3);
        r6.A02 = r12.getInt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b7, code lost:
        if (r2 >= r10) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b9, code lost:
        if (r2 >= r4) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00bb, code lost:
        r13 = r5[r2];
        r12.order(r3);
        r13.A01 = r12.getInt();
        r13.A04 = X.AnonymousClass2BO.A00(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00cc, code lost:
        if (r8 < 2) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ce, code lost:
        r12.order(r3);
        r13.A02 = r12.getInt();
        r12.order(r3);
        r13.A00 = r12.getInt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e0, code lost:
        r13.A03 = X.AnonymousClass2BO.A00(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e7, code lost:
        r12.order(r3);
        r12.getInt();
        X.AnonymousClass2BO.A00(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f0, code lost:
        if (r8 < 2) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f2, code lost:
        r12.order(r3);
        r12.getInt();
        r12.order(r3);
        r12.getInt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fe, code lost:
        X.AnonymousClass2BO.A00(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0101, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0104, code lost:
        r9.A02 = X.AnonymousClass2BO.A00(r12);
        r3 = r14 - 4;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0113, code lost:
        if ((0 + r3) > r0.position()) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0115, code lost:
        r1 = new java.util.zip.Adler32();
        r1.update(r0.array(), 0, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0129, code lost:
        if (r9.A02 != r1.getValue()) goto L_0x024c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0135, code lost:
        if (java.util.Arrays.equals(r9.A03, X.AnonymousClass2Bc.A00(r8)) == false) goto L_0x0239;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0137, code lost:
        r11 = r9.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x013b, code lost:
        if (r11 != 65536) goto L_0x0231;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x013d, code lost:
        r10 = r9.A00;
        r1 = r9.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0141, code lost:
        if (r1 == 0) goto L_0x0150;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0144, code lost:
        if (r1 != 1) goto L_0x0149;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0146, code lost:
        r1 = 16384;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0149, code lost:
        r1 = 32768;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x014e, code lost:
        if (r9.A0A == false) goto L_0x0152;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0150, code lost:
        r1 = 65536;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0152, code lost:
        if (r10 != r1) goto L_0x0229;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0154, code lost:
        r1 = r6.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0156, code lost:
        if (r1 < 0) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x015a, code lost:
        if (r1 >= r9.A05) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x015c, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x015d, code lost:
        if (r3 >= r4) goto L_0x0170;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0163, code lost:
        if (r5[r3].A01 > r10) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0165, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x016f, code lost:
        throw new X.C47532Bh("Invalid event buffer size");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0170, code lost:
        r3 = new java.lang.StringBuilder("wambuffer/header/init: header=");
        r3.append(r8);
        r3.append(" bufferCount=");
        r3.append(r7);
        r3.append(" maxMetadataSize=");
        r3.append(r11);
        r3.append(" maxEventBufferSize=");
        r3.append(r10);
        r3.append(" currentEventBufferIndex=");
        r3.append(r1);
        r3.append(" currentEventBufferIndex=");
        r3.append(r1);
        r3.append(" currentBufferSequenceNumber=");
        r3.append(r6.A00);
        r3.append(" isEventBeaconingEnabled=");
        r3.append(r6.A04);
        r3.append(" dayOfLastBeaconingDecision=");
        r3.append(r6.A03);
        r3.append(" currentEventSequenceNumber=");
        r3.append(r6.A02);
        com.whatsapp.util.Log.i(r3.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01d0, code lost:
        if (r2 >= r4) goto L_0x021b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01d2, code lost:
        r6 = r5[r2];
        r3 = new java.lang.StringBuilder("wambuffer/header/init/eventBufferMetadata/");
        r3.append(r2);
        r3.append(": size=");
        r3.append(r6.A01);
        r3.append(" timestamp=");
        r3.append(r6.A04);
        r3.append(" streamId=");
        r3.append(r6.A02);
        r3.append(" bufferSequenceNumber=");
        r3.append(r6.A00);
        r3.append(" checksum=");
        r3.append(r6.A03);
        com.whatsapp.util.Log.i(r3.toString());
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x021b, code lost:
        r17.A00.AIm();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0220, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0228, code lost:
        throw new X.C47532Bh("Invalid current event buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0230, code lost:
        throw new X.C47532Bh("Invalid max event buffer size");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0238, code lost:
        throw new X.C47532Bh("Invalid max metadata size");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0240, code lost:
        throw new X.C47532Bh("Invalid WAM file magic or version");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0241, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0242, code lost:
        r1 = r9.A09;
        r1.A03 = java.lang.Boolean.TRUE;
        r1.A05();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x024b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x024c, code lost:
        r1 = r9.A09;
        r1.A05 = java.lang.Boolean.TRUE;
        r1.A05();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x025c, code lost:
        throw new X.C47532Bh("Invalid checksum");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0264, code lost:
        throw new java.lang.IllegalArgumentException("Given range contains invalid bytes");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0265, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x026f, code lost:
        throw new X.C47532Bh(r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0277, code lost:
        throw new X.C47532Bh("Event buffer downgrade not allowed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0278, code lost:
        r0 = new java.lang.StringBuilder("Invalid value: ");
        r0.append(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x028b, code lost:
        throw new java.lang.RuntimeException(r0.toString());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(int r18) {
        /*
        // Method dump skipped, instructions count: 685
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1N8.A03(int):void");
    }

    public final void A04(AnonymousClass02W r11, AnonymousClass02Y r12) {
        AnonymousClass1NA A8e = this.A00.A8e();
        int length = C47592Bp.A00.length - 1;
        if (A8e.A05()) {
            AnonymousClass02V r1 = A8e.A04;
            byte[] A00 = C47592Bp.A00(length);
            ByteBuffer byteBuffer = r1.A05;
            byteBuffer.put(A00);
            AnonymousClass23W r7 = A8e.A05;
            AnonymousClass23X[] r5 = r7.A05;
            int i = A8e.A02;
            AnonymousClass23X r3 = r5[i];
            int i2 = r7.A00 + 1;
            r7.A00 = i2;
            if (i2 > 65535) {
                r7.A00 = 1;
                i2 = 1;
            }
            r3.A00 = i2;
            byteBuffer.put(A8e.A06(length));
            r5[i].A04 = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        }
        C47602Bq r4 = r11.A02;
        int size = r4.size();
        C47602Bq r2 = r12.A02;
        int size2 = size + r2.size();
        ByteBuffer byteBuffer2 = A8e.A04.A05;
        if (size2 <= byteBuffer2.remaining()) {
            byteBuffer2.put(r4.A00());
            byteBuffer2.put(r2.A00());
            int i3 = A8e.A01 + r11.A01;
            A8e.A01 = i3;
            A8e.A01 = i3 + r12.A01;
            A8e.A00++;
            Map map = r11.A00;
            for (Number number : Collections.unmodifiableCollection(map.keySet())) {
                C28211Md r32 = A8e.A03;
                int intValue = number.intValue();
                Integer valueOf = Integer.valueOf(intValue);
                if (map.containsKey(valueOf)) {
                    r32.A00(intValue, ((C28221Me) map.get(valueOf)).A00);
                } else {
                    throw new Error("The buffer does not contain the given attribute");
                }
            }
            return;
        }
        throw new Error("Not enough space in the buffer");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r3.A09 != false) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(java.io.RandomAccessFile r4, int r5) {
        /*
            r3 = this;
            r2 = 65536(0x10000, float:9.18355E-41)
            int r1 = r3.A04     // Catch: IOException -> 0x001c
            if (r1 == 0) goto L_0x0013
            r0 = 1
            if (r1 != r0) goto L_0x000c
            r1 = 16384(0x4000, float:2.2959E-41)
            goto L_0x0015
        L_0x000c:
            boolean r0 = r3.A09     // Catch: IOException -> 0x001c
            r1 = 32768(0x8000, float:4.5918E-41)
            if (r0 == 0) goto L_0x0015
        L_0x0013:
            r1 = 65536(0x10000, float:9.18355E-41)
        L_0x0015:
            int r1 = r1 * r5
            int r1 = r1 + r2
            long r0 = (long) r1     // Catch: IOException -> 0x001c
            r4.setLength(r0)     // Catch: IOException -> 0x001c
            return
        L_0x001c:
            java.lang.String r1 = "setWamErrorWriteFile"
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1N8.A05(java.io.RandomAccessFile, int):void");
    }
}
