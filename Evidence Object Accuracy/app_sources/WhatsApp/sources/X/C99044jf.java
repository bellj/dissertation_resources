package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99044jf implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78643pF[i];
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        byte[] bArr = null;
        byte[][] bArr2 = null;
        byte[][] bArr3 = null;
        byte[][] bArr4 = null;
        byte[][] bArr5 = null;
        int[] iArr = null;
        byte[][] bArr6 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 3:
                    bArr = C95664e9.A0I(parcel, readInt);
                    break;
                case 4:
                    bArr2 = C95664e9.A0M(parcel, readInt);
                    break;
                case 5:
                    bArr3 = C95664e9.A0M(parcel, readInt);
                    break;
                case 6:
                    bArr4 = C95664e9.A0M(parcel, readInt);
                    break;
                case 7:
                    bArr5 = C95664e9.A0M(parcel, readInt);
                    break;
                case '\b':
                    iArr = C95664e9.A0J(parcel, readInt);
                    break;
                case '\t':
                    bArr6 = C95664e9.A0M(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78643pF(str, bArr, iArr, bArr2, bArr3, bArr4, bArr5, bArr6);
    }
}
