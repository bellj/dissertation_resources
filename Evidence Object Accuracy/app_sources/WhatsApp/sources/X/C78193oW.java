package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.3oW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78193oW extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99384kD();
    public final int A00;
    public final List A01;

    public C78193oW(List list, int i) {
        this.A00 = i;
        this.A01 = list;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A0F(parcel, this.A01, 3, false);
        C95654e8.A06(parcel, A00);
    }
}
