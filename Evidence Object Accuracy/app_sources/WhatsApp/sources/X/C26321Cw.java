package X;

/* renamed from: X.1Cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26321Cw implements AbstractC16990q5 {
    public final C26311Cv A00;
    public final C18780t0 A01;
    public final C22700zV A02;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public C26321Cw(C26311Cv r1, C18780t0 r2, C22700zV r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC16990q5
    public void AOo() {
        C16310on A02;
        C22700zV r0 = this.A02;
        AnonymousClass009.A00();
        synchronized (r0.A0C) {
            C21580xe r2 = r0.A05;
            int A022 = r0.A02.A02(AbstractC15460nI.A2G);
            try {
                A02 = ((AbstractC21570xd) r2).A00.A02();
            } catch (IllegalArgumentException e) {
                AnonymousClass009.A08("contact-mgr-db/unable to delete stale vnames", e);
            }
            try {
                AbstractC21570xd.A02(A02, "wa_vnames", "identity_unconfirmed_since > ? AND identity_unconfirmed_since < ?", new String[]{"0", String.valueOf((System.currentTimeMillis() / 1000) - ((long) A022))});
                A02.close();
            } catch (Throwable th) {
                throw th;
            }
        }
        C43651xN r1 = this.A00.A00;
        C28181Ma r9 = new C28181Ma(true);
        r9.A03();
        try {
            C16310on A023 = r1.A00.A02();
            AbstractC21570xd.A02(A023, "wa_last_entry_point", "entry_point_time <= ?", new String[]{String.valueOf(System.currentTimeMillis() - 604800000)});
            A023.close();
            r9.A00();
        } catch (IllegalArgumentException e2) {
            AnonymousClass009.A08("deleteOldChatEntryPointLogs/unable to delete old chat entry points ", e2);
        }
        C18780t0 r6 = this.A01;
        long A01 = r6.A01();
        long A024 = r6.A02();
        C232010t r7 = r6.A03.A00;
        A02 = r7.A02();
        try {
            AbstractC21570xd.A02(A02, "wa_trusted_contacts", "incoming_tc_token_timestamp< ?", new String[]{String.valueOf(A01)});
            A02.close();
            C16310on A025 = r7.A02();
            try {
                long A026 = AbstractC21570xd.A02(A025, "wa_trusted_contacts_send", "sent_tc_token_timestamp< ?", new String[]{String.valueOf(A024)});
                A025.close();
                if (A026 > 0) {
                    r6.A06().clear();
                }
            } finally {
                try {
                    A025.close();
                } catch (Throwable unused) {
                }
            }
        } finally {
            try {
                A02.close();
            } catch (Throwable unused2) {
            }
        }
    }
}
