package X;

import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;

/* renamed from: X.68E  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68E implements AnonymousClass1QA {
    public final /* synthetic */ C30821Yy A00;
    public final /* synthetic */ C133976Cv A01;
    public final /* synthetic */ AbstractC16390ow A02;
    public final /* synthetic */ String A03;

    public AnonymousClass68E(C30821Yy r1, C133976Cv r2, AbstractC16390ow r3, String str) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = str;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1QA
    public void AWX() {
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A01.A00;
        indiaUpiCheckOrderDetailsActivity.AaN();
        String str = this.A03;
        if (str == null) {
            indiaUpiCheckOrderDetailsActivity.A3b(this.A00);
        } else {
            indiaUpiCheckOrderDetailsActivity.A06.A00(new Runnable(this.A00, this) { // from class: X.6IA
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ AnonymousClass68E A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AnonymousClass68E r0 = this.A01;
                    r0.A01.A00.A3b(this.A00);
                }
            }, str);
        }
    }

    @Override // X.AnonymousClass1QA
    public void AWZ() {
        AnonymousClass1ZD r0;
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = this.A01.A00;
        indiaUpiCheckOrderDetailsActivity.AaN();
        AbstractC16390ow r1 = this.A02;
        C16470p4 ABf = r1.ABf();
        if (ABf != null && (r0 = ABf.A01) != null && r0.A04.A02 != null) {
            C117295Zj.A0f(indiaUpiCheckOrderDetailsActivity, indiaUpiCheckOrderDetailsActivity.getResources(), C117295Zj.A0V(indiaUpiCheckOrderDetailsActivity.A01, "HH:mm", r1.ABf().A01.A04.A02.A00), C12970iu.A1b());
        }
    }
}
