package X;

import android.content.DialogInterface;
import androidx.preference.ListPreferenceDialogFragmentCompat;

/* renamed from: X.0V6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V6 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ListPreferenceDialogFragmentCompat A00;

    public AnonymousClass0V6(ListPreferenceDialogFragmentCompat listPreferenceDialogFragmentCompat) {
        this.A00 = listPreferenceDialogFragmentCompat;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        ListPreferenceDialogFragmentCompat listPreferenceDialogFragmentCompat = this.A00;
        listPreferenceDialogFragmentCompat.A00 = i;
        listPreferenceDialogFragmentCompat.onClick(dialogInterface, -1);
        dialogInterface.dismiss();
    }
}
