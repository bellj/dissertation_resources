package X;

import java.util.List;

/* renamed from: X.68a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327368a implements AnonymousClass1FK {
    public final /* synthetic */ AbstractActivityC121505iP A00;

    public C1327368a(AbstractActivityC121505iP r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        ((ActivityC13810kN) this.A00).A05.A0H(new Runnable() { // from class: X.6Fl
            @Override // java.lang.Runnable
            public final void run() {
                C1327368a.this.A00.A31();
            }
        });
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        ((ActivityC13810kN) this.A00).A05.A0H(new Runnable() { // from class: X.6Fn
            @Override // java.lang.Runnable
            public final void run() {
                C1327368a.this.A00.A31();
            }
        });
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        C119755f3 r1;
        AbstractActivityC121505iP r2 = this.A00;
        r2.AaN();
        List list = ((AnonymousClass46O) r4).A00;
        if (list != null && !list.isEmpty()) {
            AnonymousClass1ZY r12 = C117315Zl.A08(list, C1311161i.A01(list)).A08;
            if ((r12 instanceof C119755f3) && (r1 = (C119755f3) r12) != null) {
                ((AbstractActivityC121665jA) r2).A0B.AfV(r1);
                r2.A32(r1);
                return;
            }
        }
        ((ActivityC13810kN) r2).A05.A0H(new Runnable() { // from class: X.6Fm
            @Override // java.lang.Runnable
            public final void run() {
                C1327368a.this.A00.A31();
            }
        });
    }
}
