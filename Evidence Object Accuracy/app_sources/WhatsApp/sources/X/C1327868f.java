package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.68f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327868f implements AnonymousClass1FK {
    public final /* synthetic */ C118035bA A00;

    public C1327868f(C118035bA r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r5) {
        C27691It A00 = C118035bA.A00(this.A00);
        Log.w(C12960it.A0b("PAY: BrazilMerchantDetailsViewModel removePayment/onRequestError. paymentNetworkError: ", r5));
        C128215vk r0 = new C128215vk(6);
        r0.A00 = R.string.seller_account_cannot_be_removed;
        A00.A0B(r0);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r5) {
        C27691It A00 = C118035bA.A00(this.A00);
        Log.i(C12960it.A0b("PAY: BrazilMerchantDetailsViewModel removePayment/onResponseError. paymentNetworkError: ", r5));
        C128215vk r0 = new C128215vk(6);
        r0.A00 = R.string.seller_account_cannot_be_removed;
        A00.A0B(r0);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        C27691It A00 = C118035bA.A00(this.A00);
        Log.i("PAY: BrazilMerchantDetailsViewModel removePayment Success");
        C128215vk r0 = new C128215vk(6);
        r0.A00 = R.string.seller_account_is_removed;
        A00.A0B(r0);
    }
}
