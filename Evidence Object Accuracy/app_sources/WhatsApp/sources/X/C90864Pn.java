package X;

import com.whatsapp.jid.UserJid;
import java.util.Collection;
import java.util.Date;

/* renamed from: X.4Pn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90864Pn {
    public final UserJid A00;
    public final Collection A01;
    public final Date A02;

    public C90864Pn(UserJid userJid, Collection collection, Date date) {
        this.A00 = userJid;
        this.A01 = collection;
        this.A02 = date;
    }
}
