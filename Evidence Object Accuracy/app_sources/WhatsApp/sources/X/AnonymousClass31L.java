package X;

/* renamed from: X.31L  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31L extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Double A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Integer A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;

    public AnonymousClass31L() {
        super(2044, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(12, this.A08);
        r3.Abe(15, this.A09);
        r3.Abe(16, this.A00);
        r3.Abe(17, this.A0A);
        r3.Abe(8, this.A01);
        r3.Abe(10, this.A04);
        r3.Abe(11, this.A0B);
        r3.Abe(18, this.A02);
        r3.Abe(14, this.A03);
        r3.Abe(9, this.A05);
        r3.Abe(13, this.A0C);
        r3.Abe(5, this.A06);
        r3.Abe(6, this.A07);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPttPlayback {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttDuration", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttMiniPlayerClick", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttMiniPlayerClose", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttMiniPlayerPauseCnt", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlaybackFailed", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlaybackSpeed", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlaybackSpeedCnt", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlayedOutOfChat", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlayedPct", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttPlayer", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttSeekCnt", this.A0C);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttTrigger", C12960it.A0Y(this.A06));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "pttType", C12960it.A0Y(this.A07));
        return C12960it.A0d("}", A0k);
    }
}
