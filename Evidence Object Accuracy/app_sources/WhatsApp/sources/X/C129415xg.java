package X;

import android.hardware.camera2.CaptureFailure;

/* renamed from: X.5xg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129415xg {
    public CaptureFailure A00;

    public int A00() {
        CaptureFailure captureFailure = this.A00;
        if (captureFailure != null) {
            return captureFailure.getReason();
        }
        return -1;
    }

    public void A01(CaptureFailure captureFailure) {
        this.A00 = captureFailure;
    }
}
