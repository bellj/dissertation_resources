package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0ok  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16280ok extends SQLiteOpenHelper implements AnonymousClass01N, AbstractC16300om {
    public static volatile AnonymousClass1Tz A05;
    public C16330op A00;
    public final AbstractC15710nm A01;
    public final AnonymousClass1Tz A02;
    public final C29621Ty A03;
    public final ReentrantReadWriteLock A04;

    public abstract C16330op A03();

    public AbstractC16280ok(Context context, AbstractC15710nm r4, String str, ReentrantReadWriteLock reentrantReadWriteLock, int i, boolean z) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
        this.A01 = r4;
        if (A05 == null) {
            synchronized (AbstractC16280ok.class) {
                if (A05 == null) {
                    A05 = new AnonymousClass1Tz(r4);
                }
            }
        }
        this.A02 = A05;
        this.A04 = reentrantReadWriteLock;
        this.A03 = new C29621Ty();
        if (z) {
            setWriteAheadLoggingEnabled(true);
        }
    }

    public SQLiteDatabase A00() {
        return super.getWritableDatabase();
    }

    /* renamed from: A01 */
    public C16310on get() {
        ReentrantReadWriteLock reentrantReadWriteLock = this.A04;
        return new C16310on(this.A01, this, reentrantReadWriteLock != null ? reentrantReadWriteLock.readLock() : null, false);
    }

    public C16310on A02() {
        ReentrantReadWriteLock reentrantReadWriteLock = this.A04;
        return new C16310on(this.A01, this, reentrantReadWriteLock != null ? reentrantReadWriteLock.readLock() : null, true);
    }

    @Override // X.AbstractC16300om
    public C29621Ty AEm() {
        return this.A03;
    }

    @Override // X.AbstractC16300om
    public C16330op AG6() {
        return AHr();
    }

    @Override // X.AbstractC16300om
    public synchronized C16330op AHr() {
        C16330op r0 = this.A00;
        if (r0 == null || !r0.A00.isOpen()) {
            this.A00 = A03();
        }
        return this.A00;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper, java.lang.AutoCloseable
    public void close() {
        super.close();
        AnonymousClass1Tz r0 = this.A02;
        r0.A01.remove(getDatabaseName());
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    @Deprecated
    public synchronized SQLiteDatabase getReadableDatabase() {
        AnonymousClass009.A07("Use getReadableLoggableDatabase instead");
        return AHr().A00;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    @Deprecated
    public synchronized SQLiteDatabase getWritableDatabase() {
        AnonymousClass009.A07("Use getWritableLoggableDatabase instead");
        return AHr().A00;
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        AnonymousClass1Tz r1 = this.A02;
        String databaseName = getDatabaseName();
        if (!r1.A01.add(databaseName)) {
            r1.A00.A02("db-already-created", databaseName, new Throwable());
        }
    }
}
