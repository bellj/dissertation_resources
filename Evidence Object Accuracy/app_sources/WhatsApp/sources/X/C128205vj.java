package X;

/* renamed from: X.5vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128205vj {
    public final C15550nR A00;
    public final C15610nY A01;
    public final C14830m7 A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass1IR A04;
    public final C17070qD A05;
    public final AnonymousClass6M8 A06;
    public final AnonymousClass14X A07;
    public final boolean A08;

    public C128205vj(C15550nR r1, C15610nY r2, C14830m7 r3, AnonymousClass018 r4, AnonymousClass1IR r5, C17070qD r6, AnonymousClass6M8 r7, AnonymousClass14X r8, boolean z) {
        this.A04 = r5;
        this.A02 = r3;
        this.A07 = r8;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A06 = r7;
        this.A08 = z;
    }
}
