package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DO  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DO implements Iterator, AbstractC16910px {
    public int A00;
    public boolean A01;
    public final int A02;
    public final int A03;

    public AnonymousClass5DO(int i, int i2, int i3) {
        this.A03 = i3;
        this.A02 = i2;
        boolean z = true;
        if (i3 <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.A01 = z;
        this.A00 = !z ? i2 : i;
    }

    public int A00() {
        int i = this.A00;
        if (i != this.A02) {
            this.A00 = this.A03 + i;
            return i;
        } else if (this.A01) {
            this.A01 = false;
            return i;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A01;
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        return Integer.valueOf(A00());
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }
}
