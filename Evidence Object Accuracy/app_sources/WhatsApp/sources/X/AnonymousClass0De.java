package X;

import android.view.View;

/* renamed from: X.0De  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0De extends AnonymousClass0Y9 {
    public final /* synthetic */ AnonymousClass0C4 A00;

    public AnonymousClass0De(AnonymousClass0C4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        AnonymousClass0C4 r1 = this.A00;
        r1.A08 = null;
        r1.A09.requestLayout();
    }
}
