package X;

import java.util.zip.ZipEntry;

/* renamed from: X.1QW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1QW extends AnonymousClass1QX implements Comparable {
    public final int A00;
    public final ZipEntry A01;

    public AnonymousClass1QW(String str, ZipEntry zipEntry, int i) {
        super(str, String.format("pseudo-zip-hash-1-%s-%s-%s-%s", zipEntry.getName(), Long.valueOf(zipEntry.getSize()), Long.valueOf(zipEntry.getCompressedSize()), Long.valueOf(zipEntry.getCrc())));
        this.A01 = zipEntry;
        this.A00 = i;
    }

    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        return super.A01.compareTo(((AnonymousClass1QX) obj).A01);
    }
}
