package X;

import android.graphics.Bitmap;
import com.whatsapp.settings.chat.wallpaper.WallpaperDownloadFailedDialogFragment;
import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPreviewActivity;

/* renamed from: X.3Cm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C63673Cm {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass35E A01;
    public final /* synthetic */ AnonymousClass35K A02;

    public /* synthetic */ C63673Cm(AnonymousClass35E r1, AnonymousClass35K r2, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    public final void A00(AnonymousClass4OI r6) {
        AnonymousClass35K r1 = this.A02;
        AnonymousClass35E r3 = this.A01;
        int i = this.A00;
        Bitmap bitmap = r6.A01;
        if (bitmap != null) {
            r3.setWallpaper(bitmap);
        }
        int i2 = r6.A00;
        if (i2 == 2) {
            DownloadableWallpaperPreviewActivity downloadableWallpaperPreviewActivity = r1.A02.A01;
            downloadableWallpaperPreviewActivity.A08.add(Integer.valueOf(i));
            if (downloadableWallpaperPreviewActivity.A01.getCurrentItem() == i) {
                ((AnonymousClass35C) downloadableWallpaperPreviewActivity).A00.setEnabled(true);
            }
            r3.A02.setVisibility(8);
        } else if (i2 == 1) {
            r3.A02.setVisibility(0);
            r3.A03.setVisibility(0);
            r3.A01.setVisibility(8);
        } else {
            AnonymousClass4OK r12 = r1.A02;
            if (!r12.A00) {
                r12.A00 = true;
                r12.A01.Adm(WallpaperDownloadFailedDialogFragment.A00(i2));
            }
            r3.A02.setVisibility(0);
            r3.A03.setVisibility(8);
            r3.A01.setVisibility(0);
        }
    }
}
