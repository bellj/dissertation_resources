package X;

import android.content.Context;
import com.whatsapp.companionmode.registration.RegisterAsCompanionActivity;

/* renamed from: X.4pz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102964pz implements AbstractC009204q {
    public final /* synthetic */ RegisterAsCompanionActivity A00;

    public C102964pz(RegisterAsCompanionActivity registerAsCompanionActivity) {
        this.A00 = registerAsCompanionActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
