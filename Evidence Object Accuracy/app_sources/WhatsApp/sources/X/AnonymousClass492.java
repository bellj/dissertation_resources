package X;

import java.io.IOException;

/* renamed from: X.492  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass492 extends IOException {
    public Throwable cause;

    public AnonymousClass492(String str) {
        super(str);
    }

    public AnonymousClass492(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
