package X;

import com.whatsapp.R;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.2v0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59602v0 extends C37171lc {
    public final int A00;
    public final int A01;
    public final AbstractView$OnClickListenerC34281fs A02;
    public final String A03;
    public final String A04;

    public C59602v0(AbstractView$OnClickListenerC34281fs r6, String str, String str2) {
        super(AnonymousClass39o.A0W);
        AnonymousClass4BM r1;
        int i;
        AnonymousClass4BM[] values = AnonymousClass4BM.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                r1 = AnonymousClass4BM.A03;
                break;
            }
            r1 = values[i2];
            if (r1.id.equals(str)) {
                break;
            }
            i2++;
        }
        switch (r1.ordinal()) {
            case 0:
                i = R.color.category_apparel_clothing;
                break;
            case 1:
                i = R.color.category_shopping_retail;
                break;
            case 2:
                i = R.color.category_automotive;
                break;
            case 3:
                i = R.color.category_restaurant;
                break;
            case 4:
                i = R.color.category_grocery_store;
                break;
            case 5:
                i = R.color.category_pizza_place;
                break;
            case 6:
                i = R.color.category_food_beverage;
                break;
            case 7:
                i = R.color.category_education;
                break;
            case 8:
                i = R.color.category_sports_recreation;
                break;
            case 9:
                i = R.color.category_local_service;
                break;
            case 10:
                i = R.color.category_ads_marketing;
                break;
            case 11:
                i = R.color.category_agriculture;
                break;
            case 12:
                i = R.color.category_arts_entertainment;
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                i = R.color.category_beauty_cosmetics_care;
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                i = R.color.category_commercial_industrial;
                break;
            case 15:
                i = R.color.category_community_org;
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                i = R.color.category_finance;
                break;
            case 17:
                i = R.color.category_hotel_lodging;
                break;
            case 18:
                i = R.color.category_interest;
                break;
            case 19:
                i = R.color.category_legal;
                break;
            case C43951xu.A01:
                i = R.color.category_media;
                break;
            case 21:
                i = R.color.category_media_news_company;
                break;
            case 22:
                i = R.color.category_medical_health;
                break;
            case 23:
                i = R.color.category_non_gov_org;
                break;
            case 24:
                i = R.color.category_non_profit_org;
                break;
            case 25:
                i = R.color.category_public_gov_service;
                break;
            case 26:
                i = R.color.category_real_state;
                break;
            case 27:
                i = R.color.category_science_technology_eng;
                break;
            case 28:
                i = R.color.category_travel_transportation;
                break;
            case 29:
                i = R.color.category_vehicle_aircraft_boat;
                break;
            default:
                i = R.color.category_fallback;
                break;
        }
        this.A00 = i;
        this.A01 = AnonymousClass4E7.A00(str);
        this.A04 = str;
        this.A03 = str2;
        this.A02 = r6;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A04.equals(((C59602v0) obj).A04);
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A04.hashCode();
    }
}
