package X;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import java.util.Map;

/* renamed from: X.60m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309160m {
    public static final Map A00 = C12970iu.A11();

    public static float A00(CameraCharacteristics.Key key, CameraManager cameraManager, String str) {
        if (str != null) {
            return C72453ed.A02(A01(cameraManager, str).get(key));
        }
        throw new AnonymousClass6L0("Camera ID must be provided to check supported modes.");
    }

    public static CameraCharacteristics A01(CameraManager cameraManager, String str) {
        Map map = A00;
        CameraCharacteristics cameraCharacteristics = (CameraCharacteristics) map.get(str);
        if (cameraCharacteristics != null) {
            return cameraCharacteristics;
        }
        try {
            CameraCharacteristics cameraCharacteristics2 = cameraManager.getCameraCharacteristics(str);
            map.put(str, cameraCharacteristics2);
            return cameraCharacteristics2;
        } catch (CameraAccessException e) {
            throw new AnonymousClass6L0(C12960it.A0d(str, C12960it.A0k("Could not get Camera Characteristics for Camera ID: ")), e);
        }
    }
}
