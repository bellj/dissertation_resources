package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.contact.picker.PhoneContactsSelector;

/* renamed from: X.3iy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74903iy extends AbstractC018308n {
    public final /* synthetic */ int A00;
    public final /* synthetic */ PhoneContactsSelector A01;

    public C74903iy(PhoneContactsSelector phoneContactsSelector, int i) {
        this.A01 = phoneContactsSelector;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        int i = this.A00;
        rect.set(0, i, 0, i);
    }
}
