package X;

import com.whatsapp.payments.ui.mapper.register.IndiaUpiMapperValuePropsActivity;

/* renamed from: X.5db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119365db extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119365db() {
        C117295Zj.A0p(this, 113);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiMapperValuePropsActivity indiaUpiMapperValuePropsActivity = (IndiaUpiMapperValuePropsActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiMapperValuePropsActivity);
            ActivityC13810kN.A10(A1M, indiaUpiMapperValuePropsActivity);
            ((ActivityC13790kL) indiaUpiMapperValuePropsActivity).A08 = ActivityC13790kL.A0S(r3, A1M, indiaUpiMapperValuePropsActivity, ActivityC13790kL.A0Y(A1M, indiaUpiMapperValuePropsActivity));
            indiaUpiMapperValuePropsActivity.A02 = C117315Zl.A0H(A1M);
            indiaUpiMapperValuePropsActivity.A01 = C117305Zk.A0T(A1M);
        }
    }
}
