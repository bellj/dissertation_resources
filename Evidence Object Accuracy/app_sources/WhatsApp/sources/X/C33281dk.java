package X;

/* renamed from: X.1dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33281dk {
    public Long A00;
    public final AbstractC14640lm A01;
    public final Long A02;

    public C33281dk(AbstractC14640lm r1, Long l) {
        this.A01 = r1;
        this.A02 = l;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C33281dk r4 = (C33281dk) obj;
            if (this.A01.equals(r4.A01)) {
                return this.A02.equals(r4.A02);
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.A01.hashCode() * 31) + this.A02.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("MutedChat{chatJid=");
        sb.append(this.A01);
        sb.append(", muteEndTimestampMs=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }
}
