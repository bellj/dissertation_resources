package X;

import android.graphics.Bitmap;
import java.util.Arrays;

/* renamed from: X.2Vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51492Vb {
    public Bitmap A00;
    public C15370n3 A01;
    public String A02;
    public boolean A03;
    public final long A04;
    public final String A05;
    public final String A06;

    public C51492Vb(String str, String str2, long j) {
        this.A06 = str;
        this.A04 = j;
        this.A05 = str2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof C51492Vb) && this.A04 == ((C51492Vb) obj).A04) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.A04)});
    }
}
