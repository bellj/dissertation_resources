package X;

/* renamed from: X.39T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39T extends AnonymousClass2ON {
    public final /* synthetic */ C29631Ua A00;

    public AnonymousClass39T(C29631Ua r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002b, code lost:
        if (r16.equals(r3.callWaitingInfo.A04) == false) goto L_0x002d;
     */
    @Override // X.AnonymousClass2ON
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(java.lang.String r16) {
        /*
            r15 = this;
            X.AnonymousClass009.A01()
            X.1Ua r8 = r15.A00
            android.os.Handler r0 = r8.A0L
            r4 = 28
            r0.removeMessages(r4)
            java.lang.String r0 = "voip/service/selfManagedConnectionListener/onShowIncomingCallUi "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            r11 = r16
            java.lang.String r0 = X.C12960it.A0d(r11, r0)
            com.whatsapp.util.Log.i(r0)
            com.whatsapp.voipcalling.CallInfo r3 = com.whatsapp.voipcalling.Voip.getCallInfo()
            if (r3 == 0) goto L_0x002d
            X.1S7 r0 = r3.callWaitingInfo
            java.lang.String r0 = r0.A04
            boolean r0 = r11.equals(r0)
            r2 = 1
            if (r0 != 0) goto L_0x002e
        L_0x002d:
            r2 = 0
        L_0x002e:
            boolean r0 = com.whatsapp.voipcalling.Voip.A09(r3)
            if (r0 == 0) goto L_0x0075
            boolean r0 = r3.callEnding
            if (r0 != 0) goto L_0x0075
            java.lang.String r0 = r3.callId
            boolean r0 = r11.equals(r0)
            if (r0 != 0) goto L_0x0042
            if (r2 == 0) goto L_0x0075
        L_0x0042:
            long r0 = r8.A0D
            r6 = 0
            int r5 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r5 <= 0) goto L_0x0076
            long r5 = android.os.SystemClock.elapsedRealtime()
            long r5 = r5 - r0
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            r8.A0o = r0
        L_0x0055:
            if (r2 != 0) goto L_0x006a
            com.whatsapp.jid.UserJid r10 = r3.getPeerJid()
            X.AnonymousClass009.A05(r10)
            boolean r13 = r3.videoEnabled
            boolean r14 = r3.isGroupCall()
            com.whatsapp.jid.GroupJid r9 = r3.groupJid
            r12 = 0
            r8.A0d(r9, r10, r11, r12, r13, r14)
        L_0x006a:
            int r0 = android.os.Build.VERSION.SDK_INT
            if (r0 < r4) goto L_0x0072
            r0 = 1
            r8.A0l(r3, r0, r2)
        L_0x0072:
            r0 = 1
            r8.A17 = r0
        L_0x0075:
            return
        L_0x0076:
            java.lang.String r0 = "selfManagedConnectionNewCallTs is not set"
            X.AnonymousClass009.A07(r0)
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass39T.A01(java.lang.String):void");
    }
}
