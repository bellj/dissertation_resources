package X;

import java.util.LinkedHashSet;

/* renamed from: X.1ZY  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1ZY extends AnonymousClass1ZP {
    public abstract AbstractC28901Pl A05();

    public abstract LinkedHashSet A09();

    public AnonymousClass1ZR A06() {
        return null;
    }

    public AnonymousClass1ZR A07() {
        if (this instanceof AnonymousClass1ZX) {
            return null;
        }
        if (!(this instanceof AbstractC30871Zd)) {
            return ((AbstractC30851Zb) this).A01;
        }
        return ((AbstractC30871Zd) this).A08;
    }

    public String A08() {
        if (this instanceof AnonymousClass1ZX) {
            return null;
        }
        if (!(this instanceof AbstractC30871Zd)) {
            return ((AbstractC30851Zb) this).A04;
        }
        return ((AbstractC30871Zd) this).A0B;
    }

    public boolean A0A() {
        if (!(this instanceof AbstractC30871Zd)) {
            return true;
        }
        return ((AbstractC30871Zd) this).A0a;
    }
}
