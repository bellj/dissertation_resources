package X;

/* renamed from: X.40D  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass40D extends AnonymousClass4UW {
    public final boolean A00;

    public AnonymousClass40D() {
        this(false);
    }

    public AnonymousClass40D(boolean z) {
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass40D) && this.A00 == ((AnonymousClass40D) obj).A00);
    }

    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("OpenNowChip(isSelected=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
