package X;

/* renamed from: X.5xm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129475xm {
    public int A00;
    public final AnonymousClass60H[] A01 = new AnonymousClass60H[3];

    public AnonymousClass60H A00(long j) {
        int i = 0;
        do {
            AnonymousClass60H r3 = this.A01[i];
            if (r3 != null) {
                C125495rL r1 = AnonymousClass60H.A0P;
                if (r3.A00(r1) != null && C12980iv.A0G(r3.A00(r1)) == j) {
                    return r3;
                }
            }
            i++;
        } while (i < 3);
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00c3, code lost:
        if (r5.intValue() != 3) goto L_0x00c5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.C129425xh r8) {
        /*
            r7 = this;
            X.60H[] r1 = r7.A01
            int r0 = r7.A00
            r6 = r1[r0]
            if (r6 != 0) goto L_0x000f
            X.60H r6 = new X.60H
            r6.<init>()
            r1[r0] = r6
        L_0x000f:
            X.5rL r1 = X.AnonymousClass60H.A0Q
            int r2 = android.os.Build.VERSION.SDK_INT
            r0 = 23
            r3 = 0
            if (r2 < r0) goto L_0x00d9
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.LENS_INTRINSIC_CALIBRATION
            java.lang.Object r0 = r8.A00(r0)
        L_0x001e:
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0N
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.LENS_FOCUS_RANGE
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0O
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.SENSOR_ROLLING_SHUTTER_SKEW
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0M
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.LENS_FOCAL_LENGTH
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0K
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.SENSOR_EXPOSURE_TIME
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0P
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.SENSOR_TIMESTAMP
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            r0 = 24
            if (r2 < r0) goto L_0x0064
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.CONTROL_POST_RAW_SENSITIVITY_BOOST
            java.lang.Object r3 = r8.A00(r0)
            java.lang.Number r3 = (java.lang.Number) r3
        L_0x0064:
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.SENSOR_SENSITIVITY
            java.lang.Object r0 = r8.A00(r0)
            java.lang.Number r0 = (java.lang.Number) r0
            if (r0 == 0) goto L_0x0084
            X.5rL r2 = X.AnonymousClass60H.A0R
            if (r3 == 0) goto L_0x0081
            int r1 = r0.intValue()
            int r0 = r3.intValue()
            int r1 = r1 * r0
            int r0 = r1 / 100
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
        L_0x0081:
            r6.A01(r2, r0)
        L_0x0084:
            X.5rL r1 = X.AnonymousClass60H.A0F
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.LENS_APERTURE
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0G
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.CONTROL_AWB_MODE
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0T
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.JPEG_ORIENTATION
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            X.5rL r1 = X.AnonymousClass60H.A0S
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.LENS_FOCUS_DISTANCE
            java.lang.Object r0 = r8.A00(r0)
            r6.A01(r1, r0)
            android.hardware.camera2.CaptureResult$Key r0 = android.hardware.camera2.CaptureResult.FLASH_STATE
            java.lang.Object r5 = r8.A00(r0)
            java.lang.Number r5 = (java.lang.Number) r5
            X.5rL r4 = X.AnonymousClass60H.A0J
            r3 = 1
            r2 = 3
            if (r5 == 0) goto L_0x00c5
            int r1 = r5.intValue()
            r0 = 1
            if (r1 == r2) goto L_0x00c6
        L_0x00c5:
            r0 = 0
        L_0x00c6:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6.A01(r4, r0)
            X.5rL r0 = X.AnonymousClass60H.A0L
            r6.A01(r0, r5)
            int r0 = r7.A00
            int r0 = r0 + r3
            int r0 = r0 % r2
            r7.A00 = r0
            return
        L_0x00d9:
            r0 = r3
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C129475xm.A01(X.5xh):void");
    }
}
