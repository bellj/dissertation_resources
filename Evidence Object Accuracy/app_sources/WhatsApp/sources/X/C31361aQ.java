package X;

/* renamed from: X.1aQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31361aQ implements AbstractC31371aR {
    public final int A00;
    public final int A01;
    public final C31491ad A02;
    public final byte[] A03;
    public final byte[] A04;

    @Override // X.AbstractC31371aR
    public int getType() {
        return 5;
    }

    public C31361aQ(C31491ad r8, byte[] bArr, int i, int i2) {
        AnonymousClass1G4 A0T = AnonymousClass25H.A05.A0T();
        A0T.A03();
        AnonymousClass25H r1 = (AnonymousClass25H) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = i;
        A0T.A03();
        AnonymousClass25H r12 = (AnonymousClass25H) A0T.A00;
        r12.A00 |= 2;
        r12.A02 = i2;
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        AnonymousClass25H r13 = (AnonymousClass25H) A0T.A00;
        r13.A00 |= 4;
        r13.A03 = A01;
        byte[] A00 = r8.A00();
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        AnonymousClass25H r14 = (AnonymousClass25H) A0T.A00;
        r14.A00 |= 8;
        r14.A04 = A012;
        byte[] A02 = A0T.A02().A02();
        this.A00 = i;
        this.A01 = i2;
        this.A03 = bArr;
        this.A02 = r8;
        this.A04 = C31241aE.A00(new byte[]{(byte) 51}, A02);
    }

    public C31361aQ(byte[] bArr) {
        try {
            byte[][] A01 = C31241aE.A01(bArr, 1, bArr.length - 1);
            byte b = A01[0][0];
            byte[] bArr2 = A01[1];
            int i = (b & 255) >> 4;
            if (i < 3) {
                StringBuilder sb = new StringBuilder();
                sb.append("Legacy message: ");
                sb.append(i);
                throw new C31571al(sb.toString());
            } else if (i <= 3) {
                AnonymousClass25H r3 = (AnonymousClass25H) AbstractC27091Fz.A0E(AnonymousClass25H.A05, bArr2);
                int i2 = r3.A00;
                if ((i2 & 1) == 1 && (i2 & 2) == 2 && (i2 & 4) == 4 && (i2 & 8) == 8) {
                    this.A04 = bArr;
                    this.A00 = r3.A01;
                    this.A01 = r3.A02;
                    this.A03 = r3.A03.A04();
                    this.A02 = C31481ac.A00(r3.A04.A04());
                    return;
                }
                throw new C31521ag("Incomplete message.");
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unknown version: ");
                sb2.append(i);
                throw new C31521ag(sb2.toString());
            }
        } catch (C28971Pt | C31561ak e) {
            throw new C31521ag(e);
        }
    }

    @Override // X.AbstractC31371aR
    public byte[] Abf() {
        return this.A04;
    }
}
