package X;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.jid.UserJid;
import com.whatsapp.mediacomposer.bottombar.recipients.RecipientsView;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape5S0200000_I1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.2GL  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2GL extends AnonymousClass2GN implements AbstractC33441e5, AnonymousClass2GO {
    public View A00;
    public FrameLayout A01;
    public ImageView A02;
    public C15550nR A03;
    public C15610nY A04;
    public C32731ce A05;
    public C231510o A06;
    public AnonymousClass193 A07;
    public C63763Cv A08;
    public AnonymousClass3F8 A09;
    public C16630pM A0A;
    public C33421e0 A0B;
    public AnonymousClass01H A0C;
    public String A0D;
    public List A0E;
    public List A0F;
    public boolean A0G;
    public boolean A0H;

    public void A2e() {
        if (this.A0F.size() == 0) {
            A2g(false);
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("uri", getIntent().getParcelableExtra("uri"));
        intent.putExtra("caption", this.A0B.A05.getStringText());
        intent.putStringArrayListExtra("mentions", C15380n4.A06(this.A0B.A05.getMentions()));
        intent.putStringArrayListExtra("jids", C15380n4.A06(this.A0F));
        setResult(-1, intent);
        finish();
    }

    public void A2f() {
        View A05 = AnonymousClass00T.A05(this, R.id.input_container);
        boolean z = false;
        if (this.A0F.size() > 0) {
            z = true;
        }
        C63763Cv r3 = this.A08;
        List list = this.A0F;
        r3.A00(this.A04, this.A05, list, C15380n4.A0P(list), true);
        AnonymousClass018 r0 = ((ActivityC13830kP) this).A01;
        if (z) {
            C92974Yj.A00(A05, r0);
        } else {
            C92974Yj.A01(A05, r0);
        }
        this.A09.A01(z);
    }

    public void A2g(boolean z) {
        C42641vY r3 = new C42641vY(this);
        r3.A0D = true;
        r3.A0F = true;
        r3.A0S = this.A0F;
        Byte b = (byte) 0;
        r3.A0R = new ArrayList(Collections.singleton(Integer.valueOf(b.intValue())));
        r3.A0G = Boolean.valueOf(z);
        r3.A01 = this.A05;
        startActivityForResult(r3.A00(), 1);
    }

    @Override // X.AbstractC33441e5
    public void AUk(boolean z) {
        this.A0G = true;
        A2g(z);
    }

    @Override // X.AnonymousClass2GO
    public void AVo() {
        this.A0C.get();
        A2e();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            this.A0F = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
            Parcelable parcelableExtra = intent.getParcelableExtra("status_distribution");
            AnonymousClass009.A05(parcelableExtra);
            this.A05 = (C32731ce) parcelableExtra;
            A2f();
            if (i2 == -1) {
                A2e();
            }
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        List A07;
        String quantityString;
        super.onCreate(bundle);
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            A1U.A0M(true);
            A1U.A0O(false);
        }
        this.A0H = ((ActivityC13810kN) this).A0C.A07(815);
        LayoutInflater layoutInflater = getLayoutInflater();
        boolean z = this.A0H;
        int i = R.layout.media_preview;
        if (z) {
            i = R.layout.new_media_preview;
        }
        C15370n3 r11 = null;
        View inflate = layoutInflater.inflate(i, (ViewGroup) null, false);
        setContentView(inflate);
        this.A01 = (FrameLayout) AnonymousClass028.A0D(inflate, R.id.preview_holder);
        this.A0D = getIntent().getStringExtra("file_path");
        AbstractC14640lm A01 = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
        if (A01 != null) {
            A07 = Collections.singletonList(A01);
        } else {
            A07 = C15380n4.A07(AbstractC14640lm.class, getIntent().getStringArrayListExtra("jids"));
        }
        this.A0E = A07;
        this.A0F = A07;
        if (this.A0H) {
            this.A08 = new C63763Cv(((ActivityC13830kP) this).A01, (RecipientsView) AnonymousClass00T.A05(this, R.id.media_recipients), this.A0H);
            this.A09 = new AnonymousClass3F8((WaImageButton) AnonymousClass00T.A05(this, R.id.send), ((ActivityC13830kP) this).A01);
            boolean booleanExtra = getIntent().getBooleanExtra("usage_quote", false);
            C63763Cv r0 = this.A08;
            if (!booleanExtra) {
                r0.A02.setRecipientsListener(this);
            } else {
                RecipientsView recipientsView = r0.A02;
                recipientsView.A04 = false;
                recipientsView.A00 = R.color.audience_selector_disabled_chip;
            }
            AnonymousClass3F8 r3 = this.A09;
            r3.A01.setOnClickListener(new ViewOnClickCListenerShape5S0200000_I1(r3, 46, this));
            this.A05 = new C32731ce(((ActivityC13810kN) this).A0A.A06(), ((ActivityC13810kN) this).A0A.A07(), ((ActivityC13810kN) this).A0A.A03.A00("status_distribution", 0), false);
            A2f();
        } else {
            if (!A07.isEmpty()) {
                if (this.A0F.size() == 1) {
                    quantityString = this.A04.A04(this.A03.A0B((AbstractC14640lm) this.A0F.get(0)));
                } else {
                    quantityString = Resources.getSystem().getQuantityString(R.plurals.broadcast_n_recipients, this.A0F.size(), Integer.valueOf(this.A0F.size()));
                }
                A2N(quantityString);
            }
            ImageView imageView = (ImageView) AnonymousClass00T.A05(this, R.id.send);
            imageView.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(this, R.drawable.input_send), ((ActivityC13830kP) this).A01));
            imageView.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 11));
        }
        this.A00 = AnonymousClass00T.A05(this, R.id.loading_progress);
        this.A02 = (ImageView) AnonymousClass00T.A05(this, R.id.thumb_view);
        if (TextUtils.isEmpty(this.A0D)) {
            if (TextUtils.isEmpty(getIntent().getStringExtra("media_url"))) {
                Log.e("neither file path nor media url provided");
                finish();
                return;
            }
            this.A00.setVisibility(0);
            this.A02.setVisibility(0);
        }
        C14850m9 r15 = ((ActivityC13810kN) this).A0C;
        C252718t r9 = ((ActivityC13790kL) this).A0D;
        AbstractC15710nm r8 = ((ActivityC13810kN) this).A03;
        AnonymousClass19M r7 = ((ActivityC13810kN) this).A0B;
        C231510o r6 = this.A06;
        AnonymousClass01d r5 = ((ActivityC13810kN) this).A08;
        AnonymousClass018 r4 = ((ActivityC13830kP) this).A01;
        AnonymousClass193 r32 = this.A07;
        C14820m6 r2 = ((ActivityC13810kN) this).A09;
        C16630pM r1 = this.A0A;
        if (A01 != null) {
            r11 = this.A03.A0B(A01);
        }
        this.A0B = new C33421e0(this, inflate, r8, r5, r2, r4, r11, r7, r6, r32, r15, r1, r9, getIntent().getStringExtra("caption"), C15380n4.A07(UserJid.class, getIntent().getStringArrayListExtra("mentions")));
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onStart() {
        super.onStart();
        if (!TextUtils.isEmpty(this.A0D)) {
            this.A00.setVisibility(8);
            this.A02.setVisibility(8);
        }
    }
}
