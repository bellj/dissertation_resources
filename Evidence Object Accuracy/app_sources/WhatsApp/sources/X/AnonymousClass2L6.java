package X;

/* renamed from: X.2L6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2L6 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;

    public AnonymousClass2L6() {
        super(460, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(10, this.A02);
        r3.Abe(6, this.A03);
        r3.Abe(5, this.A05);
        r3.Abe(1, this.A04);
        r3.Abe(3, this.A06);
        r3.Abe(4, this.A00);
        r3.Abe(8, this.A01);
        r3.Abe(2, this.A07);
        r3.Abe(7, this.A08);
        r3.Abe(9, this.A09);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamLogin {");
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "androidKeystoreState", obj);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "connectionOrigin", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "connectionT", this.A05);
        Integer num3 = this.A04;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "loginResult", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "loginT", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "longConnect", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "passive", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryCount", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "sequenceStep", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "serverErrorCode", this.A09);
        sb.append("}");
        return sb.toString();
    }
}
