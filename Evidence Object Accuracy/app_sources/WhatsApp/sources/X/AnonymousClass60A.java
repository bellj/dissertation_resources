package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.60A  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass60A {
    public static final ArrayList A04;
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EM A01;
    public final String A02;
    public final String A03;

    static {
        String[] strArr = new String[2];
        strArr[0] = "ACTIVE";
        A04 = C12960it.A0m("INACTIVE", strArr, 1);
    }

    public AnonymousClass60A(AbstractC15710nm r22, AnonymousClass1V8 r23, C126375sm r24) {
        AnonymousClass1V8.A01(r23, "iq");
        AnonymousClass1V8 r6 = r24.A00;
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        this.A02 = (String) AnonymousClass3JT.A04(null, r23, String.class, A0j, A0k, AnonymousClass3JT.A04(null, r6, String.class, A0j, A0k, null, new String[]{"account", "action"}, false), new String[]{"account", "action"}, true);
        this.A03 = AnonymousClass3JT.A09(r23, A04, new String[]{"account", "status"});
        this.A01 = (AnonymousClass3EM) AnonymousClass3JT.A05(r23, new AbstractC116095Uc(r22, r6) { // from class: X.6DV
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r4) {
                return new AnonymousClass3EM(this.A00, r4, this.A01);
            }
        }, new String[0]);
        this.A00 = r23;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass60A.class != obj.getClass()) {
                return false;
            }
            AnonymousClass60A r5 = (AnonymousClass60A) obj;
            if (!this.A03.equals(r5.A03) || !this.A02.equals(r5.A02) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A03, this.A02, this.A01});
    }
}
