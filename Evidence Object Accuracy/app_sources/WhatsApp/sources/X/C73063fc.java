package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.3fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C73063fc extends Drawable {
    public Drawable A00;
    public AnonymousClass4V8 A01;
    public AbstractC93424a9 A02;
    public final Context A03;
    public final Path A04 = new Path();
    public final Path A05 = new Path();
    public final RectF A06 = new RectF();
    public final RectF A07 = new RectF();
    public final AbstractC16710pd A08;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public C73063fc(Context context, AnonymousClass4V8 r4, AbstractC93424a9 r5) {
        int i;
        Drawable mutate;
        this.A03 = context;
        if (r5 instanceof AnonymousClass48N) {
            i = R.drawable.ic_action_add;
        } else if (r5 instanceof AnonymousClass48K) {
            i = R.drawable.ic_camera;
        } else if (r5 instanceof AnonymousClass48O) {
            i = ((AnonymousClass48O) r5).A00.drawableRes;
        } else if (r5 instanceof AnonymousClass48M) {
            i = R.drawable.ic_group_ephemeral_v2;
        } else if (r5 instanceof AnonymousClass48L) {
            i = R.drawable.ic_checkmark_selected;
        } else {
            throw new C113285Gx();
        }
        Drawable A04 = AnonymousClass00T.A04(context, i);
        Drawable drawable = null;
        if (!(A04 == null || (mutate = A04.mutate()) == null)) {
            drawable = C015607k.A03(mutate);
            C16700pc.A0B(drawable);
            C015607k.A0A(drawable, AnonymousClass00T.A00(context, r5.A01.A01));
        }
        this.A00 = drawable;
        this.A08 = new AnonymousClass1WL(new C113965Jr(this));
        this.A01 = r4;
        this.A02 = r5;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        C16700pc.A0E(canvas, 0);
        AbstractC90544Oh r1 = this.A02.A01;
        if ((r1 instanceof AnonymousClass48H) || (r1 instanceof AnonymousClass48J)) {
            canvas.drawPath(this.A04, (Paint) this.A08.getValue());
        }
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.draw(canvas);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        AnonymousClass4V8 r3 = this.A01;
        Context context = this.A03;
        C16700pc.A0E(context, 0);
        return (int) (context.getResources().getDimension(r3.A00) + (r3.A00() * ((float) 2)));
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        AnonymousClass4V8 r3 = this.A01;
        Context context = this.A03;
        C16700pc.A0E(context, 0);
        return (int) (context.getResources().getDimension(r3.A00) + (r3.A00() * ((float) 2)));
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        ((Paint) this.A08.getValue()).setAlpha(i);
        Drawable drawable = this.A00;
        if (drawable != null) {
            drawable.setAlpha(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setBounds(int i, int i2, int i3, int i4) {
        int i5;
        super.setBounds(i, i2, i3, i4);
        RectF rectF = this.A06;
        rectF.set((float) i, (float) i2, (float) i3, (float) i4);
        RectF rectF2 = this.A07;
        rectF2.set(rectF);
        Path path = this.A05;
        path.reset();
        float f = (float) 2;
        Path.Direction direction = Path.Direction.CW;
        path.addCircle(rectF2.centerX(), rectF2.centerY(), rectF2.width() / f, direction);
        AnonymousClass4V8 r5 = this.A01;
        float A00 = r5.A00();
        rectF2.inset(A00, A00);
        Path path2 = this.A04;
        path2.reset();
        path2.addCircle(rectF2.centerX(), rectF2.centerY(), rectF2.width() / f, direction);
        Resources resources = this.A03.getResources();
        C92794Xl r1 = this.A02.A00;
        C16700pc.A0E(r1, 1);
        if (r5 instanceof AnonymousClass48D) {
            i5 = r1.A00;
        } else if (r5 instanceof AnonymousClass48G) {
            i5 = r1.A03;
        } else if (r5 instanceof AnonymousClass48F) {
            i5 = r1.A02;
        } else if (r5 instanceof AnonymousClass48E) {
            i5 = r1.A01;
        } else {
            throw new C113285Gx();
        }
        float dimension = resources.getDimension(i5) / f;
        Drawable drawable = this.A00;
        if (drawable != null) {
            RectF rectF3 = new RectF(rectF2.centerX() - dimension, rectF2.centerY() - dimension, rectF2.centerX() + dimension, rectF2.centerY() + dimension);
            Rect rect = new Rect();
            rectF3.roundOut(rect);
            drawable.setBounds(rect);
        }
        invalidateSelf();
    }
}
