package X;

/* renamed from: X.5Mk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114615Mk extends AnonymousClass1TM {
    public AbstractC114775Na A00;

    public C114615Mk(AbstractC114775Na r1) {
        this.A00 = r1;
    }

    public static C114615Mk A00(Object obj) {
        if (obj instanceof C114615Mk) {
            return (C114615Mk) obj;
        }
        if (obj != null) {
            return new C114615Mk(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: X.5Mr[] */
    /* JADX WARN: Multi-variable type inference failed */
    public C114685Mr[] A03() {
        AbstractC114775Na r4 = this.A00;
        C114685Mr[] r3 = new C114685Mr[r4.A0B()];
        for (int i = 0; i != r4.A0B(); i++) {
            AnonymousClass1TN A0D = r4.A0D(i);
            if (A0D != null && !(A0D instanceof C114685Mr)) {
                if (A0D instanceof AbstractC114775Na) {
                    A0D = new C114685Mr((AbstractC114775Na) A0D);
                } else {
                    throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(A0D), C12960it.A0k("Invalid DistributionPoint: ")));
                }
            }
            r3[i] = A0D;
        }
        return r3;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        String str = AnonymousClass1T7.A00;
        stringBuffer.append("CRLDistPoint:");
        stringBuffer.append(str);
        C114685Mr[] A03 = A03();
        for (int i = 0; i != A03.length; i++) {
            stringBuffer.append("    ");
            stringBuffer.append(A03[i]);
            stringBuffer.append(str);
        }
        return stringBuffer.toString();
    }
}
