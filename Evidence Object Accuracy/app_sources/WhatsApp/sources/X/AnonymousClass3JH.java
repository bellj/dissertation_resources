package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.3JH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JH {
    public static final String[] A03 = {"facebook.com", "www.facebook.com", "m.facebook.com"};
    public static final String[] A04 = {"fbwat.ch", "www.fbwat.ch", "fb.watch", "www.fb.watch"};
    public static final String[] A05 = {"instagram.com", "www.instagram.com"};
    public static final String[] A06 = {"lassovideos.com", "www.lassovideos.com"};
    public static final String[] A07 = {"netflix.com", "www.netflix.com"};
    public static final String[] A08 = {"sharechat.com", "www.sharechat.com"};
    public static final String[] A09 = {"streamable.com", "www.streamable.com"};
    public final int A00;
    public final int A01;
    public final String A02;

    public AnonymousClass3JH(int i, String str, int i2) {
        this.A02 = str;
        this.A00 = i2;
        this.A01 = i;
    }

    public static int A00(int i) {
        if (i == 2) {
            return R.drawable.ic_pip_facebook;
        }
        if (i == 3) {
            return R.drawable.ic_pip_instagram;
        }
        if (i == 4) {
            return R.drawable.ic_pip_youtube;
        }
        if (i == 5) {
            return R.drawable.ic_pip_fb_watch;
        }
        if (i == 6) {
            return R.drawable.ic_pip_lasso;
        }
        if (i != 8) {
            return -1;
        }
        return R.drawable.ic_pip_sharechat;
    }

    public static int A01(String str) {
        Uri parse = Uri.parse(C33771f3.A03(str, C33771f3.A03));
        if (A03(parse, A04)) {
            return 5;
        }
        String[] strArr = A03;
        if (A03(parse, strArr) && "1".equalsIgnoreCase(parse.getQueryParameter("fw"))) {
            return 5;
        }
        if (A03(parse, strArr)) {
            return 2;
        }
        if (A03(parse, A05)) {
            return 3;
        }
        if (A03(parse, A09)) {
            return 1;
        }
        if (!TextUtils.isEmpty(A02(parse))) {
            return 4;
        }
        if (A03(parse, A06)) {
            return 6;
        }
        if (A03(parse, A07)) {
            return 7;
        }
        return C13010iy.A00(A03(parse, A08) ? 1 : 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if ("youtu.be".equalsIgnoreCase(r1) != false) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A02(android.net.Uri r3) {
        /*
            r2 = 0
            if (r3 == 0) goto L_0x004c
            java.lang.String r1 = r3.getHost()
            if (r1 == 0) goto L_0x004c
            java.lang.String r0 = "m.youtube.com"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0031
            java.lang.String r0 = "www.youtube.com"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0031
            java.lang.String r0 = "youtube.com"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0031
            java.lang.String r0 = "youtu.be"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x004c
        L_0x002c:
            java.lang.String r0 = r3.getLastPathSegment()
            return r0
        L_0x0031:
            java.lang.String r1 = r3.getPath()
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r1.toLowerCase(r0)
            java.lang.String r0 = "shorts"
            boolean r0 = r1.contains(r0)
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = "v"
            java.lang.String r0 = r3.getQueryParameter(r0)
            return r0
        L_0x004c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JH.A02(android.net.Uri):java.lang.String");
    }

    public static boolean A03(Uri uri, String[] strArr) {
        String host;
        if (!(uri == null || (host = uri.getHost()) == null)) {
            for (String str : strArr) {
                if (host.equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }
}
