package X;

import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Kh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135916Kh implements Callable {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ AnonymousClass60X A01;
    public final /* synthetic */ AnonymousClass66H A02;

    public CallableC135916Kh(CaptureRequest.Builder builder, AnonymousClass60X r2, AnonymousClass66H r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = builder;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass61Q r0 = this.A01.A02;
        if (r0 != null) {
            C1310360y r2 = r0.A09;
            if (r2 != null) {
                CaptureRequest build = this.A00.build();
                AnonymousClass66H r02 = this.A02;
                r2.A04(build, r02);
                return r02;
            }
            throw new AnonymousClass6L0("Session closed while capturing photo.");
        }
        throw new AnonymousClass6L0("Preview closed while capturing photo.");
    }
}
