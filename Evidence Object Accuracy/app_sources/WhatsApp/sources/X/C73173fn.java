package X;

import android.media.AudioTrack;

/* renamed from: X.3fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73173fn extends AudioTrack.StreamEventCallback {
    public final /* synthetic */ AnonymousClass4X4 A00;
    public final /* synthetic */ C106534vr A01;

    public C73173fn(AnonymousClass4X4 r1, C106534vr r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.media.AudioTrack.StreamEventCallback
    public void onDataRequest(AudioTrack audioTrack, int i) {
        C106534vr r2 = this.A00.A02;
        C95314dV.A04(C12970iu.A1Z(audioTrack, r2.A0D));
        AnonymousClass5XL r1 = r2.A0G;
        if (r1 != null && r2.A0T) {
            r1.ATA();
        }
    }

    @Override // android.media.AudioTrack.StreamEventCallback
    public void onTearDown(AudioTrack audioTrack) {
        C106534vr r2 = this.A00.A02;
        C95314dV.A04(C12970iu.A1Z(audioTrack, r2.A0D));
        AnonymousClass5XL r1 = r2.A0G;
        if (r1 != null && r2.A0T) {
            r1.ATA();
        }
    }
}
