package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.util.Log;

/* renamed from: X.11C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11C {
    public final C14900mE A00;
    public final C19990v2 A01;
    public final C15650ng A02;
    public final C15600nX A03;
    public final C16490p7 A04;
    public final AnonymousClass11B A05;
    public final AnonymousClass119 A06;
    public final AnonymousClass11A A07;

    public AnonymousClass11C(C14900mE r1, C19990v2 r2, C15650ng r3, C15600nX r4, C16490p7 r5, AnonymousClass11B r6, AnonymousClass119 r7, AnonymousClass11A r8) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A04 = r5;
        this.A07 = r8;
        this.A03 = r4;
        this.A05 = r6;
    }

    public void A00(AbstractC15340mz r7, int i) {
        String str;
        if (i != 1) {
            if (i == 2) {
                str = "groupactionhandler/handleGroupAction/handle_add_groupchat_msg";
            } else if (i != 4) {
                boolean z = false;
                if (i == 7) {
                    Log.i("groupactionhandler/handle_user_remove");
                    AnonymousClass1XB r5 = (AnonymousClass1XB) r7;
                    GroupJid of = GroupJid.of(r5.A0z.A00);
                    AnonymousClass009.A05(of);
                    int i2 = r5.A00;
                    if (i2 == 5 || i2 == 13) {
                        z = true;
                    }
                    C14850m9 r1 = this.A05.A02;
                    if (r1.A07(1613) && r1.A07(1527) && z) {
                        C15600nX r3 = this.A03;
                        if (!r3.A0D(of)) {
                            C16310on A02 = this.A04.A02();
                            try {
                                AnonymousClass1Lx A00 = A02.A00();
                                r3.A0B(r5);
                                this.A06.A00(A02, of);
                                A00.A00();
                                A00.close();
                                A02.close();
                            } catch (Throwable th) {
                                try {
                                    A02.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        }
                    }
                    this.A02.A0S(r7);
                } else if (i != 8) {
                    switch (i) {
                        case 3009:
                            str = "groupactionhandler/handleGroupAction/handle groupchat announcements only change";
                            break;
                        case 3010:
                            Log.i("groupactionhandler/community_link_change");
                            this.A02.A0S(r7);
                            this.A00.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(this, 3, r7.A0z.A00));
                            if (r7 instanceof AnonymousClass1Y5) {
                                AnonymousClass1OV.A01(((AnonymousClass1Y5) r7).A02, "groupactionhandler/community_link_change", "scheduled adding system message and notified observers", new Object[0]);
                                return;
                            }
                            return;
                        case 3011:
                            str = "groupactionhandler/sibling_link";
                            break;
                        case 3012:
                            break;
                        case 3013:
                            str = "groupmgr/handle group linked with membership mode disabled";
                            break;
                        default:
                            StringBuilder sb = new StringBuilder("Unhandled action ");
                            sb.append(i);
                            throw new RuntimeException(sb.toString());
                    }
                } else {
                    str = "groupactionhandler/handle_growth_lock_change";
                }
                this.A00.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(this, 3, r7.A0z.A00));
                return;
            } else {
                str = "groupactionhandler/handleGroupAction/handle_groupchat_subject_change";
            }
            Log.i(str);
            this.A02.A0S(r7);
            this.A00.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(this, 3, r7.A0z.A00));
            return;
        }
        Log.i("groupactionhandler/handleGroupAction/handle-init-group-chat");
        this.A02.A0S(r7);
    }
}
