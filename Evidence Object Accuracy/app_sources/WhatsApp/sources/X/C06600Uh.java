package X;

import android.animation.TimeInterpolator;
import android.view.animation.Interpolator;

/* renamed from: X.0Uh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06600Uh implements TimeInterpolator {
    public final Interpolator A00 = AnonymousClass0L1.A00(0.5f, 0.0f, 0.5f, 1.0f);

    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        if (f < 0.5f) {
            return this.A00.getInterpolation(f * 2.0f);
        }
        return 1.0f - this.A00.getInterpolation((f - 0.5f) * 2.0f);
    }
}
