package X;

import android.text.SpannableStringBuilder;

/* renamed from: X.5m8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122805m8 extends C122965mO {
    public String A00;
    public boolean A01;
    public final SpannableStringBuilder A02;

    public C122805m8(SpannableStringBuilder spannableStringBuilder, boolean z) {
        super(1004);
        this.A02 = spannableStringBuilder;
        this.A01 = z;
    }
}
