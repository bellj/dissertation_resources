package X;

import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0jv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13560jv implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99544kT();
    public Messenger A00;
    public C98314iU A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C13560jv(IBinder iBinder) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.A00 = new Messenger(iBinder);
        } else {
            this.A01 = new C98314iU(iBinder);
        }
    }

    public final void A00(Message message) {
        Messenger messenger = this.A00;
        if (messenger != null) {
            messenger.send(message);
            return;
        }
        C98314iU r1 = this.A01;
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
        obtain.writeInt(1);
        message.writeToParcel(obtain, 0);
        try {
            r1.A00.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        IBinder asBinder;
        IBinder asBinder2;
        if (obj == null) {
            return false;
        }
        try {
            Messenger messenger = this.A00;
            if (messenger != null) {
                asBinder = messenger.getBinder();
            } else {
                asBinder = this.A01.asBinder();
            }
            C13560jv r4 = (C13560jv) obj;
            Messenger messenger2 = r4.A00;
            if (messenger2 != null) {
                asBinder2 = messenger2.getBinder();
            } else {
                asBinder2 = r4.A01.asBinder();
            }
            return asBinder.equals(asBinder2);
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @Override // java.lang.Object
    public int hashCode() {
        IBinder asBinder;
        Messenger messenger = this.A00;
        if (messenger != null) {
            asBinder = messenger.getBinder();
        } else {
            asBinder = this.A01.asBinder();
        }
        return asBinder.hashCode();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        IBinder asBinder;
        Messenger messenger = this.A00;
        if (messenger != null) {
            asBinder = messenger.getBinder();
        } else {
            asBinder = this.A01.asBinder();
        }
        parcel.writeStrongBinder(asBinder);
    }
}
