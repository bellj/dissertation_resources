package X;

import java.util.Random;

/* renamed from: X.00E  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00E {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final Random A04;
    public final boolean A05;

    public AnonymousClass00E(int i, int i2) {
        this(true, 1, i, i, i2);
    }

    public AnonymousClass00E(int i, int i2, int i3) {
        this(false, i, i2, i2, i3);
    }

    public AnonymousClass00E(boolean z, int i, int i2, int i3, int i4) {
        this.A02 = i;
        this.A00 = i2;
        this.A01 = i3;
        this.A03 = i4;
        this.A05 = z;
        this.A04 = new Random();
    }

    public boolean A00() {
        return this.A04.nextInt(this.A03 * 1) == 0;
    }

    public boolean A01(Object obj) {
        if (obj == null) {
            return A00();
        }
        return obj.hashCode() % (this.A03 * 1) == 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass00E r5 = (AnonymousClass00E) obj;
            if (this.A02 == r5.A02 && this.A00 == r5.A00 && this.A01 == r5.A01 && this.A03 == r5.A03 && this.A05 == r5.A05) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.A02 * 31) + this.A00) * 31) + this.A01) * 31) + this.A03) * 31) + (this.A05 ? 1 : 0);
    }
}
