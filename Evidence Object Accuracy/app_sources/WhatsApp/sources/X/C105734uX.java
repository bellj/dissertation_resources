package X;

import com.whatsapp.mediaview.MediaViewBaseFragment;

/* renamed from: X.4uX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105734uX implements AnonymousClass070 {
    public final /* synthetic */ AnonymousClass443 A00;
    public final /* synthetic */ MediaViewBaseFragment A01;

    @Override // X.AnonymousClass070
    public void ATO(int i) {
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
    }

    public C105734uX(AnonymousClass443 r1, MediaViewBaseFragment mediaViewBaseFragment) {
        this.A00 = r1;
        this.A01 = mediaViewBaseFragment;
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        this.A00.A00.A1J(i);
    }
}
