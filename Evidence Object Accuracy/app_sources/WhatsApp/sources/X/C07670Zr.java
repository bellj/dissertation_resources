package X;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* renamed from: X.0Zr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07670Zr implements AbstractC12570i8 {
    public static final String A04 = C06390Tk.A01("SystemJobScheduler");
    public final JobScheduler A00;
    public final Context A01;
    public final AnonymousClass022 A02;
    public final AnonymousClass0Tc A03;

    @Override // X.AbstractC12570i8
    public boolean AIE() {
        return true;
    }

    public C07670Zr(Context context, AnonymousClass022 r4) {
        AnonymousClass0Tc r0 = new AnonymousClass0Tc(context);
        this.A01 = context;
        this.A02 = r4;
        this.A00 = (JobScheduler) context.getSystemService("jobscheduler");
        this.A03 = r0;
    }

    public static List A00(JobScheduler jobScheduler, Context context) {
        List<JobInfo> list;
        ArrayList arrayList = null;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            C06390Tk.A00().A03(A04, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list != null) {
            arrayList = new ArrayList(list.size());
            ComponentName componentName = new ComponentName(context, SystemJobService.class);
            for (JobInfo jobInfo : list) {
                if (componentName.equals(jobInfo.getService())) {
                    arrayList.add(jobInfo);
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0012 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A01(android.app.job.JobScheduler r4, android.content.Context r5, java.lang.String r6) {
        /*
            java.util.List r1 = A00(r4, r5)
            if (r1 != 0) goto L_0x0008
            r0 = 0
            return r0
        L_0x0008:
            r0 = 2
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>(r0)
            java.util.Iterator r4 = r1.iterator()
        L_0x0012:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0044
            java.lang.Object r3 = r4.next()
            android.app.job.JobInfo r3 = (android.app.job.JobInfo) r3
            java.lang.String r2 = "EXTRA_WORK_SPEC_ID"
            android.os.PersistableBundle r1 = r3.getExtras()
            if (r1 == 0) goto L_0x0031
            boolean r0 = r1.containsKey(r2)     // Catch: NullPointerException -> 0x0031
            if (r0 == 0) goto L_0x0031
            java.lang.String r0 = r1.getString(r2)     // Catch: NullPointerException -> 0x0031
            goto L_0x0032
        L_0x0031:
            r0 = 0
        L_0x0032:
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0012
            int r0 = r3.getId()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r5.add(r0)
            goto L_0x0012
        L_0x0044:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07670Zr.A01(android.app.job.JobScheduler, android.content.Context, java.lang.String):java.util.List");
    }

    public static void A02(JobScheduler jobScheduler, int i) {
        try {
            jobScheduler.cancel(i);
        } catch (Throwable th) {
            C06390Tk.A00().A03(A04, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", Integer.valueOf(i)), th);
        }
    }

    public static void A03(Context context) {
        List<JobInfo> A00;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (!(jobScheduler == null || (A00 = A00(jobScheduler, context)) == null || A00.isEmpty())) {
            for (JobInfo jobInfo : A00) {
                A02(jobScheduler, jobInfo.getId());
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0083 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x007f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(android.content.Context r10, X.AnonymousClass022 r11) {
        /*
            java.lang.String r0 = "jobscheduler"
            java.lang.Object r9 = r10.getSystemService(r0)
            android.app.job.JobScheduler r9 = (android.app.job.JobScheduler) r9
            java.util.List r3 = A00(r9, r10)
            androidx.work.impl.WorkDatabase r5 = r11.A04
            X.0i9 r1 = r5.A08()
            X.0Zx r1 = (X.C07710Zx) r1
            java.lang.String r0 = "SELECT DISTINCT work_spec_id FROM SystemIdInfo"
            r4 = 0
            X.0ZJ r2 = X.AnonymousClass0ZJ.A00(r0, r4)
            X.0QN r0 = r1.A01
            r0.A02()
            android.database.Cursor r1 = X.AnonymousClass0LC.A00(r0, r2, r4)
            int r0 = r1.getCount()     // Catch: all -> 0x00d9
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch: all -> 0x00d9
            r6.<init>(r0)     // Catch: all -> 0x00d9
        L_0x002d:
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x00d9
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = r1.getString(r4)     // Catch: all -> 0x00d9
            r6.add(r0)     // Catch: all -> 0x00d9
            goto L_0x002d
        L_0x003b:
            r1.close()
            r2.A01()
            r10 = 0
            if (r3 == 0) goto L_0x008b
            int r0 = r3.size()
        L_0x0048:
            java.util.HashSet r7 = new java.util.HashSet
            r7.<init>(r0)
            if (r3 == 0) goto L_0x008d
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x008d
            java.util.Iterator r8 = r3.iterator()
        L_0x0059:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x008d
            java.lang.Object r3 = r8.next()
            android.app.job.JobInfo r3 = (android.app.job.JobInfo) r3
            java.lang.String r2 = "EXTRA_WORK_SPEC_ID"
            android.os.PersistableBundle r1 = r3.getExtras()
            if (r1 == 0) goto L_0x0078
            boolean r0 = r1.containsKey(r2)     // Catch: NullPointerException -> 0x0078
            if (r0 == 0) goto L_0x0078
            java.lang.String r1 = r1.getString(r2)     // Catch: NullPointerException -> 0x0078
            goto L_0x0079
        L_0x0078:
            r1 = 0
        L_0x0079:
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0083
            r7.add(r1)
            goto L_0x0059
        L_0x0083:
            int r0 = r3.getId()
            A02(r9, r0)
            goto L_0x0059
        L_0x008b:
            r0 = 0
            goto L_0x0048
        L_0x008d:
            java.util.Iterator r1 = r6.iterator()
        L_0x0091:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00d8
            java.lang.Object r0 = r1.next()
            boolean r0 = r7.contains(r0)
            if (r0 != 0) goto L_0x0091
            X.0Tk r3 = X.C06390Tk.A00()
            java.lang.String r2 = X.C07670Zr.A04
            java.lang.Throwable[] r1 = new java.lang.Throwable[r4]
            java.lang.String r0 = "Reconciling jobs"
            r3.A02(r2, r0, r1)
            r10 = 1
            r5.A03()
            X.0iM r4 = r5.A0B()     // Catch: all -> 0x00d0
            java.util.Iterator r3 = r6.iterator()     // Catch: all -> 0x00d0
        L_0x00ba:
            boolean r0 = r3.hasNext()     // Catch: all -> 0x00d0
            if (r0 == 0) goto L_0x00cc
            java.lang.Object r2 = r3.next()     // Catch: all -> 0x00d0
            java.lang.String r2 = (java.lang.String) r2     // Catch: all -> 0x00d0
            r0 = -1
            r4.AKt(r2, r0)     // Catch: all -> 0x00d0
            goto L_0x00ba
        L_0x00cc:
            r5.A05()     // Catch: all -> 0x00d0
            goto L_0x00d5
        L_0x00d0:
            r0 = move-exception
            r5.A04()
            throw r0
        L_0x00d5:
            r5.A04()
        L_0x00d8:
            return r10
        L_0x00d9:
            r0 = move-exception
            r1.close()
            r2.A01()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C07670Zr.A04(android.content.Context, X.022):boolean");
    }

    public void A05(C004401z r10, int i) {
        int i2;
        JobInfo A01 = this.A03.A01(r10, i);
        C06390Tk A00 = C06390Tk.A00();
        String str = A04;
        A00.A02(str, String.format("Scheduling work ID %s Job ID %s", r10.A0E, Integer.valueOf(i)), new Throwable[0]);
        try {
            if (this.A00.schedule(A01) == 0) {
                C06390Tk.A00().A05(str, String.format("Unable to schedule work ID %s", r10.A0E), new Throwable[0]);
                if (r10.A0H && r10.A0C == EnumC006603c.RUN_AS_NON_EXPEDITED_WORK_REQUEST) {
                    r10.A0H = false;
                    C06390Tk.A00().A02(str, String.format("Scheduling a non-expedited job (work ID %s)", r10.A0E), new Throwable[0]);
                    A05(r10, i);
                }
            }
        } catch (IllegalStateException e) {
            List A002 = A00(this.A00, this.A01);
            if (A002 != null) {
                i2 = A002.size();
            } else {
                i2 = 0;
            }
            Locale locale = Locale.getDefault();
            AnonymousClass022 r7 = this.A02;
            String format = String.format(locale, "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", Integer.valueOf(i2), Integer.valueOf(r7.A04.A0B().AGQ().size()), Integer.valueOf(r7.A02.A00()));
            C06390Tk.A00().A03(str, format, new Throwable[0]);
            throw new IllegalStateException(format, e);
        } catch (Throwable th) {
            C06390Tk.A00().A03(str, String.format("Unable to schedule %s", r10), th);
        }
    }

    @Override // X.AbstractC12570i8
    public void A73(String str) {
        Context context = this.A01;
        JobScheduler jobScheduler = this.A00;
        List<Number> A01 = A01(jobScheduler, context, str);
        if (!(A01 == null || A01.isEmpty())) {
            for (Number number : A01) {
                A02(jobScheduler, number.intValue());
            }
            this.A02.A04.A08().AaR(str);
        }
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC12570i8
    public void AbJ(C004401z... r13) {
        int A00;
        List A01;
        int A002;
        AnonymousClass022 r8 = this.A02;
        WorkDatabase workDatabase = r8.A04;
        AnonymousClass0P1 r6 = new AnonymousClass0P1(workDatabase);
        for (C004401z r2 : r13) {
            workDatabase.A03();
            try {
                C004401z AHn = workDatabase.A0B().AHn(r2.A0E);
                if (AHn == null) {
                    C06390Tk A003 = C06390Tk.A00();
                    String str = A04;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Skipping scheduling ");
                    sb.append(r2.A0E);
                    sb.append(" because it's no longer in the DB");
                    A003.A05(str, sb.toString(), new Throwable[0]);
                } else if (AHn.A0D != EnumC03840Ji.ENQUEUED) {
                    C06390Tk A004 = C06390Tk.A00();
                    String str2 = A04;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Skipping scheduling ");
                    sb2.append(r2.A0E);
                    sb2.append(" because it is no longer enqueued");
                    A004.A05(str2, sb2.toString(), new Throwable[0]);
                } else {
                    AnonymousClass0PC AH2 = workDatabase.A08().AH2(r2.A0E);
                    if (AH2 != null) {
                        A00 = AH2.A00;
                    } else {
                        C05180Oo r0 = r8.A02;
                        A00 = r6.A00(r0.A02, r0.A01);
                        workDatabase.A08().AJ1(new AnonymousClass0PC(r2.A0E, A00));
                    }
                    A05(r2, A00);
                    if (Build.VERSION.SDK_INT == 23 && (A01 = A01(this.A00, this.A01, r2.A0E)) != null) {
                        int indexOf = A01.indexOf(Integer.valueOf(A00));
                        if (indexOf >= 0) {
                            A01.remove(indexOf);
                        }
                        if (!A01.isEmpty()) {
                            A002 = ((Integer) A01.get(0)).intValue();
                        } else {
                            C05180Oo r02 = r8.A02;
                            A002 = r6.A00(r02.A02, r02.A01);
                        }
                        A05(r2, A002);
                    }
                }
                workDatabase.A05();
                workDatabase.A04();
            } catch (Throwable th) {
                workDatabase.A04();
                throw th;
            }
        }
    }
}
