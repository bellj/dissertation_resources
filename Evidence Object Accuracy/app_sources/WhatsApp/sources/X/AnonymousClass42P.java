package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.42P  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42P extends AbstractC75243jX {
    public final TextEmojiLabel A00;
    public final /* synthetic */ C54352ga A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass42P(View view, C54352ga r3) {
        super(view, r3);
        this.A01 = r3;
        this.A00 = (TextEmojiLabel) view.findViewById(R.id.select_list_section_title);
    }
}
