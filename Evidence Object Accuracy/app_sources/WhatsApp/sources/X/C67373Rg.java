package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3Rg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67373Rg implements AbstractC009404s {
    public final AnonymousClass4JH A00;
    public final UserJid A01;
    public final String A02;
    public final String A03;

    public C67373Rg(AnonymousClass4JH r1, UserJid userJid, String str, String str2) {
        this.A03 = str;
        this.A02 = str2;
        this.A01 = userJid;
        this.A00 = r1;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        AnonymousClass4JH r0 = this.A00;
        String str = this.A03;
        String str2 = this.A02;
        UserJid userJid = this.A01;
        C71573d9 r02 = r0.A00;
        AnonymousClass01J r1 = r02.A04;
        C14830m7 A0b = C12980iv.A0b(r1);
        return new C53882fY(C12970iu.A0S(r1), r02.A03.A01(), A0b, C12970iu.A0X(r1), C12960it.A0R(r1), userJid, str, str2);
    }
}
