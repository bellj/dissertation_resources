package X;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import androidx.core.app.NotificationCompat$BigTextStyle;
import androidx.core.graphics.drawable.IconCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.notification.AndroidWear;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.0zO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C22630zO {
    public static final HashMap A0L = new HashMap();
    public static final String[] A0M = {"_id"};
    public C007303s A00;
    public final C14330lG A01;
    public final C15570nT A02;
    public final AnonymousClass11L A03;
    public final C14650lo A04;
    public final AnonymousClass130 A05;
    public final C15550nR A06;
    public final C15610nY A07;
    public final C21270x9 A08;
    public final AnonymousClass01d A09;
    public final C16590pI A0A;
    public final AnonymousClass018 A0B;
    public final C15650ng A0C;
    public final C15600nX A0D;
    public final AnonymousClass134 A0E;
    public final AnonymousClass13H A0F;
    public final AnonymousClass14X A0G;
    public final C16630pM A0H;
    public final C15860o1 A0I;
    public final C22590zK A0J;
    public final HashMap A0K = new HashMap();

    public C22630zO(C14330lG r2, C15570nT r3, AnonymousClass11L r4, C14650lo r5, AnonymousClass130 r6, C15550nR r7, C15610nY r8, C21270x9 r9, AnonymousClass01d r10, C16590pI r11, AnonymousClass018 r12, C15650ng r13, C15600nX r14, AnonymousClass134 r15, AnonymousClass13H r16, AnonymousClass14X r17, C16630pM r18, C15860o1 r19, C22590zK r20) {
        this.A0A = r11;
        this.A0F = r16;
        this.A02 = r3;
        this.A01 = r2;
        this.A0E = r15;
        this.A08 = r9;
        this.A0G = r17;
        this.A05 = r6;
        this.A06 = r7;
        this.A09 = r10;
        this.A07 = r8;
        this.A0B = r12;
        this.A0J = r20;
        this.A0C = r13;
        this.A0I = r19;
        this.A04 = r5;
        this.A0D = r14;
        this.A0H = r18;
        this.A03 = r4;
    }

    public static C005602s A00(Context context) {
        C005602s r1 = new C005602s(context, null);
        r1.A00 = AnonymousClass00T.A00(context, R.color.primary_notification);
        return r1;
    }

    public static CharSequence A01(CharSequence charSequence) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        spannableStringBuilder.setSpan(new StyleSpan(1), 0, charSequence.length(), 0);
        return spannableStringBuilder;
    }

    public static CharSequence A02(CharSequence charSequence, boolean z) {
        if (!z) {
            return charSequence;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        spannableStringBuilder.setSpan(new StyleSpan(2), 0, charSequence.length(), 0);
        return spannableStringBuilder;
    }

    public static Integer A03(String str) {
        int i;
        if (str == null) {
            return null;
        }
        try {
            i = Integer.parseInt(str, 16);
        } catch (NumberFormatException unused) {
            i = 16777215;
        }
        int i2 = i | -16777216;
        if (i2 != -16777216) {
            return Integer.valueOf(i2);
        }
        return null;
    }

    public static String A04(Context context, AnonymousClass018 r8, AnonymousClass1XF r9) {
        int i = r9.A00;
        String A0I = r8.A0I(new Object[]{Integer.valueOf(i)}, R.plurals.total_items, (long) i);
        if (TextUtils.isEmpty(r9.A05)) {
            return A0I;
        }
        return context.getString(R.string.message_preview_order, A0I, r9.A05);
    }

    public static String A05(Context context, AbstractC15340mz r4) {
        AbstractC16130oV r42;
        if (r4 instanceof AbstractC16390ow) {
            AbstractC16390ow r43 = (AbstractC16390ow) r4;
            if (r43.ACL() != null) {
                return r43.ACL().A06(context);
            }
            return null;
        } else if ((r4 instanceof AnonymousClass1XI) || (r4 instanceof AnonymousClass1XG) || (r4 instanceof AbstractC28871Pi)) {
            return null;
        } else {
            if (r4 instanceof AnonymousClass1XV) {
                AnonymousClass1XV r44 = (AnonymousClass1XV) r4;
                String str = r44.A09;
                String str2 = r44.A02;
                if (!TextUtils.isEmpty(str2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(" ");
                    sb.append(str2);
                    str = sb.toString();
                }
                String str3 = r44.A05;
                if (TextUtils.isEmpty(str3)) {
                    return str;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" ");
                sb2.append(str3);
                return sb2.toString();
            } else if (r4 instanceof AnonymousClass1XF) {
                return null;
            } else {
                if (r4 instanceof AnonymousClass1X7) {
                    AbstractC16130oV r1 = (AbstractC16130oV) r4;
                    boolean A04 = C35011h5.A04(r4);
                    r42 = r4;
                    if (!A04) {
                        return r1.A15();
                    }
                } else if (r4 instanceof C30421Xi) {
                    return null;
                } else {
                    if (r4 instanceof AnonymousClass1XR) {
                        return ((AbstractC16130oV) r4).A15();
                    }
                    if (r4 instanceof AnonymousClass1X2) {
                        AbstractC16130oV r45 = (AbstractC16130oV) r4;
                        boolean A042 = C35011h5.A04(r45);
                        r42 = r45;
                        if (!A042) {
                            return r45.A15();
                        }
                    } else if (r4 instanceof C16440p1) {
                        boolean A0r = C30041Vv.A0r(r4);
                        r42 = r4;
                        if (A0r) {
                            return null;
                        }
                    } else if ((r4 instanceof C30061Vy) || (r4 instanceof C28581Od) || (r4 instanceof C30351Xb) || (r4 instanceof C30411Xh)) {
                        return null;
                    } else {
                        boolean z = r4 instanceof AnonymousClass1XO;
                        r42 = r4;
                        if (!z) {
                            if (r4 instanceof C30341Xa) {
                                return ((C30341Xa) r4).A03;
                            }
                            return null;
                        }
                    }
                }
                return C35011h5.A00(r42);
            }
        }
    }

    public static String A06(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return context.getString(R.string.settings_sound_silent);
        }
        Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(str));
        String str2 = null;
        if (ringtone == null) {
            return null;
        }
        try {
            str2 = ringtone.getTitle(context);
            return str2;
        } catch (Exception unused) {
            return str2;
        }
    }

    public static String A07(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append(" ");
        if (!TextUtils.isEmpty(str)) {
            if (((long) str.length()) > 1024) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append((Object) str.subSequence(0, 1020));
                sb2.append("…");
                str = sb2.toString();
            }
            str3 = str;
        }
        sb.append(str3);
        return sb.toString();
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public static void A08(android.content.Context r10, android.net.Uri r11, X.C005602s r12, X.AnonymousClass01d r13, X.C21310xD r14, X.C21300xC r15) {
        /*
            r5 = r11
            int r3 = android.os.Build.VERSION.SDK_INT
            r0 = 22
            if (r3 >= r0) goto L_0x0023
            r0 = 21
            if (r3 != r0) goto L_0x0055
            android.net.Uri r0 = android.provider.Settings.System.DEFAULT_NOTIFICATION_URI
            boolean r0 = r0.equals(r11)
            if (r0 != 0) goto L_0x0023
            java.util.HashMap r2 = X.C22630zO.A0L
            java.lang.Object r0 = r2.get(r11)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x0045
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0055
        L_0x0023:
            boolean r0 = r14.A00
            if (r0 != 0) goto L_0x0058
            r0 = 26
            if (r3 >= r0) goto L_0x0058
            java.io.File r1 = X.C14350lI.A03(r11)
            if (r1 == 0) goto L_0x003f
            r0 = 24
            if (r3 < r0) goto L_0x003f
            android.net.Uri r5 = X.C14350lI.A01(r10, r1)     // Catch: IllegalArgumentException -> 0x007f
            java.lang.String r1 = "com.android.systemui"
            r0 = 1
            r10.grantUriPermission(r1, r5, r0)     // Catch: IllegalArgumentException -> 0x007f
        L_0x003f:
            if (r5 == 0) goto L_0x0058
            r12.A07(r5)
            return
        L_0x0045:
            android.content.ContentResolver r4 = r13.A0C()
            if (r4 != 0) goto L_0x0059
            java.lang.String r0 = "messagenotification/is-notification-tone cr=null"
            com.whatsapp.util.Log.w(r0)
        L_0x0050:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r2.put(r5, r0)
        L_0x0055:
            r15.A01(r5)
        L_0x0058:
            return
        L_0x0059:
            java.lang.String[] r6 = X.C22630zO.A0M     // Catch: Exception -> 0x0050
            java.lang.String r7 = "is_notification=1"
            r8 = 0
            java.lang.String r9 = "title_key"
            android.database.Cursor r1 = r4.query(r5, r6, r7, r8, r9)     // Catch: Exception -> 0x0050
            if (r1 == 0) goto L_0x0050
            int r0 = r1.getCount()     // Catch: all -> 0x007a
            if (r0 <= 0) goto L_0x0076
            java.lang.Boolean r0 = java.lang.Boolean.TRUE     // Catch: all -> 0x007a
            r2.put(r11, r0)     // Catch: all -> 0x007a
            r1.close()     // Catch: Exception -> 0x0050
            goto L_0x0023
        L_0x0076:
            r1.close()     // Catch: Exception -> 0x0050
            goto L_0x0050
        L_0x007a:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x007e
        L_0x007e:
            throw r0     // Catch: Exception -> 0x0050
        L_0x007f:
            r1 = move-exception
            java.lang.String r0 = "notification/"
            com.whatsapp.util.Log.w(r0, r1)
            r15.A01(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22630zO.A08(android.content.Context, android.net.Uri, X.02s, X.01d, X.0xD, X.0xC):void");
    }

    public static long[] A09(String str) {
        char c;
        if (str == null) {
            return null;
        }
        switch (str.hashCode()) {
            case 49:
                if (str.equals("1")) {
                    c = 0;
                    break;
                } else {
                    return null;
                }
            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                if (str.equals("2")) {
                    c = 1;
                    break;
                } else {
                    return null;
                }
            case 51:
                if (str.equals("3")) {
                    c = 2;
                    break;
                } else {
                    return null;
                }
            default:
                return null;
        }
        switch (c) {
            case 0:
            case 1:
                return new long[]{0, 300, 200, 300, 200};
            case 2:
                return new long[]{0, 750, 250, 750, 250};
            default:
                return null;
        }
    }

    public int A0A(int i, int i2) {
        Point point = new Point();
        this.A09.A0O().getDefaultDisplay().getSize(point);
        int i3 = point.x;
        int i4 = point.y / 3;
        int i5 = 1;
        if (i != 0 && i2 != 0) {
            while (true) {
                if (i2 <= i4 && i <= i3) {
                    break;
                }
                i5 <<= 1;
                i2 = (i2 + 1) >> 1;
                i = (i + 1) >> 1;
            }
        }
        return i5;
    }

    public Bitmap A0B(C15370n3 r5) {
        Context context = this.A0A.A00;
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(17104901);
        int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(17104902);
        Bitmap A02 = this.A08.A02(context, r5, dimensionPixelSize, dimensionPixelSize2);
        if (A02 != null) {
            return A02;
        }
        AnonymousClass130 r3 = this.A05;
        return r3.A04(r5, r3.A01.A00.getResources().getDimension(R.dimen.small_avatar_radius), Math.min(dimensionPixelSize, dimensionPixelSize2));
    }

    public C007303s A0C() {
        C007303s r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        C007403t r3 = new C007403t();
        r3.A01 = this.A0A.A00.getString(R.string.group_subject_changed_you_pronoun);
        C15570nT r02 = this.A02;
        r02.A08();
        C27621Ig r03 = r02.A01;
        AnonymousClass009.A05(r03);
        Bitmap A0B = A0B(r03);
        IconCompat iconCompat = new IconCompat(1);
        iconCompat.A06 = A0B;
        r3.A00 = iconCompat;
        C007303s r04 = new C007303s(r3);
        this.A00 = r04;
        return r04;
    }

    public CharSequence A0D(C15370n3 r13, AbstractC15340mz r14, boolean z, boolean z2) {
        CharSequence obj;
        CharSequence[] charSequenceArr;
        CharSequence[] charSequenceArr2;
        AnonymousClass1IS r9 = r14.A0z;
        int i = 2;
        if (C15380n4.A0J(r9.A00)) {
            i = 1;
        }
        byte b = r14.A0y;
        if (b != 0) {
            CharSequence A0E = A0E(r14);
            boolean z3 = false;
            if (b == 12) {
                z3 = true;
            }
            if (!r13.A0K()) {
                if (!z) {
                    if (z2) {
                        charSequenceArr = new CharSequence[2];
                        if (!r9.A02) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(this.A07.A0B(r13, i, false));
                            sb.append(" ");
                            charSequenceArr[0] = A01(AbstractC32741cf.A02(sb.toString()));
                            charSequenceArr[1] = A02(AbstractC32741cf.A02(A0E), z3);
                            obj = TextUtils.concat(charSequenceArr);
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(this.A0A.A00.getString(R.string.you));
                        sb2.append(" ");
                        charSequenceArr[0] = A01(sb2.toString());
                        charSequenceArr[1] = A02(AbstractC32741cf.A02(A0E), z3);
                        obj = TextUtils.concat(charSequenceArr);
                    } else {
                        obj = A02(A0E, z3);
                    }
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.A07.A0B(r13, i, false));
                sb3.append(": ");
                sb3.append((Object) A02(A0E, z3));
                obj = sb3.toString();
            } else if (z) {
                if (!r9.A02) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(A0H(r14.A0B(), i));
                    sb4.append(" @ ");
                    sb4.append(this.A07.A0B(r13, i, false));
                    sb4.append(": ");
                    sb4.append((Object) A02(A0E, z3));
                    obj = sb4.toString();
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.A07.A0B(r13, i, false));
                sb3.append(": ");
                sb3.append((Object) A02(A0E, z3));
                obj = sb3.toString();
            } else if (z2) {
                charSequenceArr = new CharSequence[2];
                if (!r9.A02) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(A0H(r14.A0B(), i));
                    sb5.append(" ");
                    charSequenceArr[0] = A01(AbstractC32741cf.A02(sb5.toString()));
                    charSequenceArr[1] = A02(AbstractC32741cf.A02(A0E), z3);
                    obj = TextUtils.concat(charSequenceArr);
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.A0A.A00.getString(R.string.you));
                sb2.append(" ");
                charSequenceArr[0] = A01(sb2.toString());
                charSequenceArr[1] = A02(AbstractC32741cf.A02(A0E), z3);
                obj = TextUtils.concat(charSequenceArr);
            } else {
                StringBuilder sb6 = new StringBuilder();
                sb6.append(A0H(r14.A0B(), i));
                sb6.append(": ");
                obj = TextUtils.concat(A01(sb6.toString()), A02(A0E, z3));
            }
        } else if (!(r14 instanceof AnonymousClass1XB)) {
            AnonymousClass1IR r0 = r14.A0L;
            if (r0 == null) {
                obj = A0G(r14.A0I());
            } else if (r0.A03 != 5) {
                obj = this.A0G.A0U(r14, false);
            } else {
                obj = this.A0A.A00.getString(R.string.notification_future_payment);
            }
            if (r13.A0K()) {
                boolean z4 = r9.A02;
                if (z) {
                    if (!z4) {
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append(A0H(r14.A0B(), i));
                        sb7.append(" @ ");
                        sb7.append(this.A07.A0B(r13, i, false));
                        sb7.append(": ");
                        sb7.append((Object) obj);
                        obj = sb7.toString();
                    }
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(this.A07.A0B(r13, i, false));
                    sb8.append(": ");
                    sb8.append((Object) obj);
                    obj = sb8.toString();
                } else if (z2) {
                    charSequenceArr2 = new CharSequence[2];
                    if (!z4) {
                        StringBuilder sb9 = new StringBuilder();
                        sb9.append(A0H(r14.A0B(), i));
                        sb9.append(" ");
                        charSequenceArr2[0] = A01(AbstractC32741cf.A02(sb9.toString()));
                        charSequenceArr2[1] = obj;
                        obj = TextUtils.concat(charSequenceArr2);
                    }
                    StringBuilder sb10 = new StringBuilder();
                    sb10.append(this.A0A.A00.getString(R.string.you));
                    sb10.append(" ");
                    charSequenceArr2[0] = A01(sb10.toString());
                    charSequenceArr2[1] = obj;
                    obj = TextUtils.concat(charSequenceArr2);
                } else if (!z4) {
                    StringBuilder sb11 = new StringBuilder();
                    sb11.append(A0H(r14.A0B(), i));
                    sb11.append(": ");
                    obj = TextUtils.concat(A01(sb11.toString()), obj);
                } else {
                    StringBuilder sb12 = new StringBuilder("messagePreview/missing_rmt_src:");
                    sb12.append(C30041Vv.A0C(r14));
                    Log.e(sb12.toString());
                    StringBuilder sb13 = new StringBuilder();
                    sb13.append(this.A0A.A00.getString(R.string.contact));
                    sb13.append(": ");
                    sb13.append((Object) obj);
                    obj = sb13.toString();
                }
            } else {
                if (!z) {
                    if (z2) {
                        charSequenceArr2 = new CharSequence[2];
                        if (!r9.A02) {
                            StringBuilder sb14 = new StringBuilder();
                            sb14.append(AbstractC32741cf.A02(this.A07.A0B(r13, i, false)));
                            sb14.append(" ");
                            charSequenceArr2[0] = A01(sb14.toString());
                            charSequenceArr2[1] = obj;
                            obj = TextUtils.concat(charSequenceArr2);
                        }
                        StringBuilder sb10 = new StringBuilder();
                        sb10.append(this.A0A.A00.getString(R.string.you));
                        sb10.append(" ");
                        charSequenceArr2[0] = A01(sb10.toString());
                        charSequenceArr2[1] = obj;
                        obj = TextUtils.concat(charSequenceArr2);
                    }
                }
                StringBuilder sb8 = new StringBuilder();
                sb8.append(this.A07.A0B(r13, i, false));
                sb8.append(": ");
                sb8.append((Object) obj);
                obj = sb8.toString();
            }
        } else if (z) {
            StringBuilder sb15 = new StringBuilder();
            sb15.append(this.A07.A0B(r13, i, false));
            sb15.append(": ");
            sb15.append(this.A03.A0A((AnonymousClass1XB) r14, false));
            obj = sb15.toString();
        } else {
            obj = this.A03.A0A((AnonymousClass1XB) r14, false);
        }
        return this.A0F.A00(obj, r14.A0o);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01ea, code lost:
        if (r0 == 1) goto L_0x01ec;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.CharSequence A0E(X.AbstractC15340mz r11) {
        /*
        // Method dump skipped, instructions count: 800
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22630zO.A0E(X.0mz):java.lang.CharSequence");
    }

    public CharSequence A0F(AbstractC15340mz r4) {
        CharSequence A00;
        byte b = r4.A0y;
        if (b == 0 || (r4 instanceof AnonymousClass1XE) || ((r4 instanceof C30061Vy) && r4.A0L != null)) {
            AnonymousClass1IR r0 = r4.A0L;
            if (r0 != null) {
                if (r0.A03 != 5) {
                    A00 = this.A0G.A0U(r4, true);
                } else {
                    A00 = this.A0A.A00.getString(R.string.notification_future_payment);
                }
            } else if (C35011h5.A04(r4)) {
                String A002 = C35011h5.A00(r4);
                if (TextUtils.isEmpty(A002)) {
                    A002 = "";
                }
                if (!TextUtils.isEmpty(r4.A0I())) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(r4.A0I());
                    sb.append("\n");
                    sb.append(A002);
                    A002 = sb.toString();
                }
                A00 = A0G(A002);
            } else {
                A00 = this.A0F.A00(A0G(r4.A0I()), r4.A0o);
            }
            CharSequence A01 = AnonymousClass1US.A01(A00);
            return (!TextUtils.isEmpty(A01) || !(r4 instanceof AnonymousClass1XB)) ? A01 : this.A03.A0A((AnonymousClass1XB) r4, false);
        }
        CharSequence A0E = A0E(r4);
        if (b == 27) {
            A0E = C42971wC.A03(this.A09, this.A0H, A0E);
        }
        return this.A0F.A00(A0E, r4.A0o);
    }

    public final CharSequence A0G(String str) {
        if (str == null) {
            return "";
        }
        AnonymousClass01d r6 = this.A09;
        C16630pM r5 = this.A0H;
        if (((long) str.length()) > 1024) {
            StringBuilder sb = new StringBuilder();
            sb.append((Object) str.subSequence(0, 1020));
            sb.append("…");
            str = sb.toString();
        }
        return C42971wC.A03(r6, r5, AbstractC32741cf.A02(str));
    }

    public String A0H(AbstractC14640lm r4, int i) {
        if (r4 != null) {
            return this.A07.A0B(this.A06.A0B(r4), i, false);
        }
        Log.w("notification/messagepreview/getname remote_resource null");
        return "";
    }

    public void A0I(C005602s r5, C15370n3 r6) {
        Context context = this.A0A.A00;
        Intent A03 = C14960mK.A03(context);
        A03.putExtra("show_mute", true);
        A03.putExtra("mute_jid", C15380n4.A03(r6.A0D));
        r5.A0N.add(new AnonymousClass03l(R.drawable.ic_notif_mute, this.A0B.A09(R.string.mute_status), AnonymousClass1UY.A00(context, 4, A03, 134217728)));
    }

    public void A0J(C005602s r26, C15370n3 r27, C42951wA r28, boolean z, boolean z2, boolean z3, boolean z4) {
        Bitmap bitmap;
        CharSequence[] charSequenceArr;
        CharSequence charSequence;
        Context context = this.A0A.A00;
        AnonymousClass134 r10 = this.A0E;
        C15610nY r15 = this.A07;
        AnonymousClass018 r1 = this.A0B;
        C15650ng r9 = this.A0C;
        if (!z3 || !z2) {
            bitmap = null;
        } else {
            bitmap = this.A08.A02(context, r27, 400, 400);
        }
        C006903m r12 = new C006903m();
        if (z) {
            AbstractC15340mz r3 = r28.A00;
            if ((r3 instanceof AnonymousClass1X7) && ((AbstractC16130oV) r3).A02 != null) {
                C006903m r7 = new C006903m();
                r7.A05 = 4 | r7.A05;
                C005602s r2 = new C005602s(context, null);
                r7.A01(r2);
                r12.A0D.add(r2.A01());
            }
        }
        if (z3) {
            C30031Vu A0A = r9.A0A((AbstractC14640lm) r27.A0B(AbstractC14640lm.class), 20, 1, -1, true);
            Cursor cursor = A0A.A00;
            CharSequence charSequence2 = "";
            if (cursor != null) {
                try {
                    if (cursor.moveToLast()) {
                        if (r10.A07((AbstractC14640lm) r27.A0B(AbstractC14640lm.class), A0A.A02)) {
                            charSequence = TextUtils.concat(charSequence2, "…");
                        } else {
                            charSequence = charSequence2;
                        }
                        do {
                            AbstractC14640lm r32 = (AbstractC14640lm) r27.A0B(AbstractC14640lm.class);
                            AnonymousClass009.A05(r32);
                            AbstractC15340mz A02 = r9.A0K.A02(cursor, r32, false, true);
                            CharSequence A0D = A02 != null ? A0D(r27, A02, false, true) : charSequence2;
                            if (A0D != charSequence2) {
                                if (charSequence != charSequence2) {
                                    charSequence = TextUtils.concat(charSequence, "\n\n");
                                }
                                charSequence = TextUtils.concat(charSequence, A0D);
                            }
                        } while (cursor.moveToPrevious());
                        charSequence2 = charSequence;
                    }
                } finally {
                    cursor.close();
                }
            }
            C005602s r72 = new C005602s(context, null);
            NotificationCompat$BigTextStyle notificationCompat$BigTextStyle = new NotificationCompat$BigTextStyle();
            notificationCompat$BigTextStyle.A09(charSequence2);
            r72.A08(notificationCompat$BigTextStyle);
            C006903m r5 = new C006903m();
            r5.A05 = 8 | r5.A05;
            r5.A01(r72);
            r12.A0D.add(r72.A01());
        }
        if (z4) {
            String string = context.getString(R.string.reply_to_label, r15.A04(r27));
            String[] A0S = r1.A0S(AndroidWear.A0B);
            C007003n r22 = new C007003n("android_wear_voice_input");
            r22.A00 = string;
            r22.A01 = A0S;
            C007103o r52 = new C007103o(r22.A02, string, "android_wear_voice_input", r22.A03, A0S);
            Intent intent = new Intent(AndroidWear.A0A, ContentUris.withAppendedId(C42961wB.A00, r27.A08()), context, AndroidWear.class);
            int i = 134217728;
            if (AnonymousClass1UY.A01) {
                i = 167772160;
            }
            PendingIntent service = PendingIntent.getService(context, 0, intent, i);
            CharSequence charSequence3 = r52.A01;
            C007103o[] r14 = null;
            IconCompat A022 = IconCompat.A02(null, "", R.drawable.ic_full_reply);
            Bundle bundle = new Bundle();
            CharSequence A00 = C005602s.A00(charSequence3);
            ArrayList arrayList = new ArrayList();
            arrayList.add(r52);
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                C007103o r73 = (C007103o) it.next();
                if (r73.A04 || (!((charSequenceArr = r73.A05) == null || charSequenceArr.length == 0) || r73.A03.isEmpty())) {
                    arrayList3.add(r73);
                } else {
                    arrayList2.add(r73);
                }
            }
            C007103o[] r13 = null;
            if (!arrayList2.isEmpty()) {
                r14 = (C007103o[]) arrayList2.toArray(new C007103o[arrayList2.size()]);
            }
            if (!arrayList3.isEmpty()) {
                r13 = (C007103o[]) arrayList3.toArray(new C007103o[arrayList3.size()]);
            }
            r12.A0C.add(new AnonymousClass03l(service, bundle, A022, A00, r13, r14, 0, true, true));
        }
        r12.A0C.add(AndroidWear.A00(context, r27));
        if (bitmap != null) {
            r12.A09 = bitmap;
        }
        r12.A01(r26);
    }

    public boolean A0K(UserJid userJid) {
        int currentInterruptionFilter;
        NotificationManager.Policy notificationPolicy;
        Cursor query;
        AnonymousClass009.A00();
        C15550nR r4 = this.A06;
        C15370n3 A0B = r4.A0B(userJid);
        NotificationManager A08 = this.A09.A08();
        if (Build.VERSION.SDK_INT < 28 || (currentInterruptionFilter = A08.getCurrentInterruptionFilter()) == 1 || currentInterruptionFilter == 0 || (notificationPolicy = A08.getNotificationPolicy()) == null) {
            return false;
        }
        StringBuilder sb = new StringBuilder("NotificationUtils/isDNDTurnedOn NotificationManager policy ");
        sb.append(notificationPolicy.toString());
        Log.i(sb.toString());
        if ((notificationPolicy.priorityCategories & 8) == 0) {
            Log.i("VNotificationUtils/isDNDTurnedOn Calls not allowed in DND");
            return true;
        }
        int i = notificationPolicy.priorityCallSenders;
        if (i == 1) {
            if (A0B.A0C == null) {
                return true;
            }
            return false;
        } else if (i != 2) {
            return false;
        } else {
            ContentResolver contentResolver = this.A0A.A00.getContentResolver();
            AnonymousClass009.A00();
            Uri A05 = r4.A05(contentResolver, A0B);
            boolean z = false;
            if (!(A05 == null || (query = contentResolver.query(A05, null, "starred==1", null, null)) == null)) {
                try {
                    if (query.moveToNext()) {
                        z = true;
                    }
                    query.close();
                    if (z) {
                        return false;
                    }
                } catch (Throwable th) {
                    try {
                        query.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            return true;
        }
    }

    public StatusBarNotification[] A0L() {
        NotificationManager A08 = this.A09.A08();
        if (A08 != null) {
            try {
                return A08.getActiveNotifications();
            } catch (Exception e) {
                Log.w("notification-utils/failed to get active notifications: ", e);
            }
        }
        return new StatusBarNotification[0];
    }
}
