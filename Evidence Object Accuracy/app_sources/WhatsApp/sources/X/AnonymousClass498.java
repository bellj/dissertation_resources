package X;

import java.io.IOException;

/* renamed from: X.498  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass498 extends IOException implements AnonymousClass2GC {
    public final int errorCode = 201;

    public AnonymousClass498(String str) {
        super(str);
    }

    @Override // X.AnonymousClass2GC
    public int AER() {
        return this.errorCode;
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.getMessage());
        A0h.append(" (error_code=");
        A0h.append(this.errorCode);
        return C12960it.A0d(")", A0h);
    }
}
