package X;

import android.graphics.Point;
import com.whatsapp.voipcalling.VideoPort;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.3c9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70953c9 implements AnonymousClass5X3 {
    public final /* synthetic */ AnonymousClass2SU A00;

    @Override // X.AnonymousClass5X3
    public void AUu(VideoPort videoPort) {
    }

    public C70953c9(AnonymousClass2SU r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5X3
    public void AOY(VideoPort videoPort) {
        StringBuilder A0h = C12960it.A0h();
        AnonymousClass2SU r1 = this.A00;
        A0h.append(r1.A07);
        C12960it.A1N(A0h, "onConnected ", videoPort);
        A0h.append(r1.A04);
        C12960it.A1F(A0h);
        r1.A02 = true;
        r1.A05();
    }

    @Override // X.AnonymousClass5X3
    public void APE(VideoPort videoPort) {
        StringBuilder A0h = C12960it.A0h();
        AnonymousClass2SU r1 = this.A00;
        A0h.append(r1.A07);
        C12960it.A1N(A0h, "onDisconnecting ", videoPort);
        A0h.append(r1.A04);
        C12960it.A1F(A0h);
        r1.A03();
        r1.A02 = false;
    }

    @Override // X.AnonymousClass5X3
    public void ATu(VideoPort videoPort) {
        StringBuilder A0h = C12960it.A0h();
        AnonymousClass2SU r2 = this.A00;
        A0h.append(r2.A07);
        C12960it.A1N(A0h, "onPortWindowSizeChanged ", videoPort);
        A0h.append(r2.A04);
        C12960it.A1F(A0h);
        if (!(r2 instanceof C60232wG)) {
            C60242wH r22 = (C60242wH) r2;
            if (!r22.A0A()) {
                Voip.setVideoDisplayPort(r22.A04.getRawString(), r22.A01);
                return;
            }
            return;
        }
        VideoPort videoPort2 = r2.A01;
        Point point = new Point(0, 0);
        if (videoPort2 != null) {
            point = videoPort2.getWindowSize();
        }
        Voip.setVideoPreviewSize(point.x, point.y);
    }
}
