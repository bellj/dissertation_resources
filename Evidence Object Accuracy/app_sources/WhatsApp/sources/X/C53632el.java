package X;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerView;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.2el  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53632el extends AnonymousClass0BR {
    public final Context A00;
    public final C15570nT A01;
    public final C15550nR A02;
    public final AnonymousClass1J1 A03;
    public final AnonymousClass1OX A04;
    public final AbstractC13890kV A05;
    public final C15650ng A06;
    public final ViewOnClickCListenerShape4S0200000_I0 A07;

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 95;
    }

    public C53632el(Context context, C15570nT r2, C15550nR r3, AnonymousClass1J1 r4, AnonymousClass1OX r5, AbstractC13890kV r6, C15650ng r7, ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0) {
        super(context);
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
        this.A07 = viewOnClickCListenerShape4S0200000_I0;
    }

    @Override // X.AnonymousClass0BR
    public View A03(Context context, Cursor cursor, ViewGroup viewGroup) {
        throw new AssertionError();
    }

    @Override // X.AnonymousClass0BR
    public void A04(View view, Context context, Cursor cursor) {
        throw new AssertionError();
    }

    /* renamed from: A05 */
    public AbstractC15340mz getItem(int i) {
        Cursor cursor = super.A02;
        if (cursor == null) {
            return null;
        }
        cursor.moveToPosition(i);
        return this.A06.A0K.A01(cursor);
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        AnonymousClass1OX r1 = this.A04;
        AbstractC15340mz A05 = getItem(i);
        AnonymousClass009.A05(A05);
        return r1.A00(A05);
    }

    @Override // X.AnonymousClass0BR, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass1OY r6;
        C15370n3 A00;
        AbstractC15340mz A05 = getItem(i);
        AnonymousClass009.A05(A05);
        if (view == null) {
            r6 = this.A04.A02(viewGroup.getContext(), this.A05, A05);
        } else {
            r6 = (AnonymousClass1OY) view;
            r6.A1D(A05, true);
        }
        ImageView A0L = C12970iu.A0L(r6, R.id.profile_picture);
        AnonymousClass028.A0a(A0L, 2);
        if (A05.A0z.A02) {
            C15570nT r0 = this.A01;
            r0.A08();
            A00 = r0.A01;
            AnonymousClass009.A05(A00);
        } else {
            A00 = C15550nR.A00(this.A02, A05.A0C());
        }
        this.A03.A06(A0L, A00);
        r6.setOnClickListener(this.A07);
        if ((r6 instanceof C60822yh) && ((C30061Vy) r6.getFMessage()).A00) {
            C60822yh r02 = (C60822yh) r6;
            r02.A00 = true;
            StickerView stickerView = r02.A04.A0H;
            if (stickerView != null) {
                stickerView.A03 = true;
                stickerView.A02();
            }
        }
        return r6;
    }
}
