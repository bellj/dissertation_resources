package X;

import androidx.window.java.layout.WindowInfoTrackerCallbackAdapter$addListener$1$1;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.0Zd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07550Zd implements AbstractC12760iS {
    public final AbstractC12760iS A00;
    public final Map A01 = new LinkedHashMap();
    public final ReentrantLock A02 = new ReentrantLock();

    public C07550Zd(AbstractC12760iS r2) {
        this.A00 = r2;
    }

    public static final AnonymousClass5VF A00(AnonymousClass5X4 r1) {
        if (r1.get(AbstractC02760Dw.A00) == null) {
            r1 = r1.plus(new AnonymousClass5L3());
        }
        return new C112855Ez(r1);
    }

    public static /* synthetic */ AbstractC02760Dw A01(AnonymousClass5ZQ r4, AnonymousClass5VF r5) {
        C112775Er r3 = C112775Er.A00;
        AnonymousClass5X4 ABk = r5.ABk();
        if (((Boolean) ABk.fold(Boolean.FALSE, new AnonymousClass5KN())).booleanValue()) {
            ABk = (AnonymousClass5X4) ABk.fold(r3, new AnonymousClass5KM());
        }
        AnonymousClass5X4 plus = ABk.plus(r3);
        AbstractC10990fX r1 = AnonymousClass4ZY.A00;
        if (plus != r1 && plus.get(AnonymousClass5ZS.A00) == null) {
            plus = plus.plus(r1);
        }
        C114095Ke r0 = new C114095Ke(plus);
        r0.A0m(r0, r4);
        return r0;
    }

    public final void A02(AnonymousClass024 r5) {
        ReentrantLock reentrantLock = this.A02;
        reentrantLock.lock();
        try {
            Map map = this.A01;
            AbstractC02760Dw r1 = (AbstractC02760Dw) map.get(r5);
            if (r1 != null) {
                r1.A74(null);
            }
            map.remove(r5);
        } finally {
            reentrantLock.unlock();
        }
    }

    public final void A03(AnonymousClass024 r6, Executor executor, AnonymousClass5VH r8) {
        ReentrantLock reentrantLock = this.A02;
        reentrantLock.lock();
        try {
            Map map = this.A01;
            if (map.get(r6) == null) {
                map.put(r6, A01(new WindowInfoTrackerCallbackAdapter$addListener$1$1(r6, null, r8), A00(new C11130fn(executor))));
            }
        } finally {
            reentrantLock.unlock();
        }
    }
}
