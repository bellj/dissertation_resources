package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.PowerManager;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.redex.IDxComparatorShape3S0000000_2_I1;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.facebook.redex.IDxUListenerShape12S0100000_2_I1;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.File;
import java.math.BigDecimal;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0iv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C12980iv {
    public static float A00(C47742Cf r2) {
        TextPaint textPaint = r2.A0h;
        textPaint.setTextSize(r2.A05);
        textPaint.setTypeface(r2.A0T);
        return -textPaint.ascent();
    }

    public static float A01(float[] fArr, int i) {
        float f = fArr[i];
        if (Float.isNaN(f)) {
            return 0.0f;
        }
        return f;
    }

    public static float A02(int[] iArr, float f, int i) {
        return Math.abs(f - ((float) iArr[i]));
    }

    public static int A03(int i) {
        if (i != 0) {
            return 2;
        }
        return 1;
    }

    public static int A04(int i) {
        return View.MeasureSpec.makeMeasureSpec(i, 1073741824);
    }

    public static int A05(int i, int i2) {
        return Math.abs(i - i2);
    }

    public static int A06(View view) {
        return view.getResources().getDimensionPixelSize(R.dimen.search_attachment_margin);
    }

    public static int A07(View view) {
        return view.getHeight() - view.getPaddingBottom();
    }

    public static int A08(View view) {
        return view.getWidth() - view.getPaddingRight();
    }

    public static int A09(View view, int i, int i2) {
        return Math.max((i - view.getMeasuredWidth()) >> 1, i2);
    }

    public static int A0A(LinearLayoutManager linearLayoutManager) {
        return linearLayoutManager.A07() - (linearLayoutManager.A06() + linearLayoutManager.A1A());
    }

    public static int A0B(Object obj, Object[] objArr, int i) {
        objArr[i] = obj;
        return Arrays.hashCode(objArr);
    }

    public static int A0C(List list) {
        return list.size() - 1;
    }

    public static long A0D(long j) {
        return j / 1000;
    }

    public static long A0E(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getLong(str, -1);
    }

    public static long A0F(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getLong(str, 0);
    }

    public static long A0G(Object obj) {
        return ((Number) obj).longValue();
    }

    public static Configuration A0H(Context context) {
        return context.getResources().getConfiguration();
    }

    public static Resources.Theme A0I(View view) {
        return view.getContext().getTheme();
    }

    public static Rect A0J() {
        return new Rect();
    }

    public static RectF A0K() {
        return new RectF();
    }

    public static ColorDrawable A0L(Context context, int i) {
        return new ColorDrawable(AnonymousClass00T.A00(context, i));
    }

    public static ForegroundColorSpan A0M(Context context, int i) {
        return new ForegroundColorSpan(AnonymousClass00T.A00(context, i));
    }

    public static View A0N(Context context, int i) {
        return View.inflate(context, i, null);
    }

    public static View A0O(LayoutInflater layoutInflater, int i) {
        return layoutInflater.inflate(i, (ViewGroup) null);
    }

    public static ViewGroup A0P(View view, int i) {
        return (ViewGroup) view.findViewById(i);
    }

    public static ViewPropertyAnimator A0Q(View view, float f) {
        return view.animate().setDuration(250).alpha(f);
    }

    public static AlphaAnimation A0R() {
        return new AlphaAnimation(1.0f, 0.0f);
    }

    public static C004802e A0S(Context context) {
        return new C004802e(context);
    }

    public static AnonymousClass016 A0T() {
        return new AnonymousClass016();
    }

    public static AnonymousClass28D A0U(List list, int i) {
        return (AnonymousClass28D) list.get(i);
    }

    public static C1093751l A0V(String str) {
        return new C1093751l(new C90014Mg(str), null, 0);
    }

    public static AnonymousClass12P A0W(AnonymousClass01J r0) {
        return (AnonymousClass12P) r0.A0H.get();
    }

    public static WaImageView A0X(View view, int i) {
        return (WaImageView) AnonymousClass028.A0D(view, i);
    }

    public static C14650lo A0Y(AnonymousClass01J r0) {
        return (C14650lo) r0.A2V.get();
    }

    public static AnonymousClass19Q A0Z(AnonymousClass01J r0) {
        return (AnonymousClass19Q) r0.A2u.get();
    }

    public static C22700zV A0a(AnonymousClass01J r0) {
        return (C22700zV) r0.AMN.get();
    }

    public static C14830m7 A0b(AnonymousClass01J r0) {
        return (C14830m7) r0.ALb.get();
    }

    public static C19990v2 A0c(AnonymousClass01J r0) {
        return (C19990v2) r0.A3M.get();
    }

    public static C15600nX A0d(AnonymousClass01J r0) {
        return (C15600nX) r0.A8x.get();
    }

    public static C20710wC A0e(AnonymousClass01J r0) {
        return (C20710wC) r0.A8m.get();
    }

    public static AbstractC15340mz A0f(Iterator it) {
        return (AbstractC15340mz) it.next();
    }

    public static C252018m A0g(AnonymousClass01J r0) {
        return (C252018m) r0.A7g.get();
    }

    public static File A0h(File file) {
        return new File(Uri.fromFile(file).getPath());
    }

    public static Integer A0i() {
        return 0;
    }

    public static Integer A0j() {
        return 4;
    }

    public static Integer A0k() {
        return 5;
    }

    public static Long A0l(int i) {
        return Long.valueOf((long) i);
    }

    public static Long A0m(Number number) {
        return Long.valueOf(number.longValue() + 1);
    }

    public static NullPointerException A0n(String str) {
        return new NullPointerException(str);
    }

    public static Object A0o(List list) {
        return list.get(0);
    }

    public static String A0p(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getString(str, null);
    }

    public static String A0q(TextView textView) {
        return textView.getText().toString();
    }

    public static String A0r(Object obj) {
        return obj.getClass().getSimpleName();
    }

    public static String A0s(Object obj) {
        return obj.getClass().getName();
    }

    public static StringBuilder A0t(int i) {
        return new StringBuilder(i);
    }

    public static UnsupportedOperationException A0u(String str) {
        return new UnsupportedOperationException(str);
    }

    public static BigDecimal A0v(C30711Yn r2, AnonymousClass1V8 r3) {
        String A0G;
        if (r3 == null || (A0G = r3.A0G()) == null || r2 == null) {
            return null;
        }
        return C30701Ym.A00(r2, Long.parseLong(A0G));
    }

    public static ArrayList A0w(int i) {
        return new ArrayList(i);
    }

    public static ArrayList A0x(Collection collection) {
        return new ArrayList(collection);
    }

    public static ArrayList A0y(List list) {
        return new ArrayList(list.size());
    }

    public static List A0z(AnonymousClass017 r0) {
        return (List) r0.A01();
    }

    public static List A10(List list, int i) {
        return (List) list.get(i);
    }

    public static void A11(ValueAnimator valueAnimator, Object obj, int i) {
        valueAnimator.addUpdateListener(new IDxUListenerShape12S0100000_2_I1(obj, i));
    }

    public static void A12(Context context, Paint paint, int i) {
        paint.setColor(AnonymousClass00T.A00(context, i));
    }

    public static void A13(Intent intent, Jid jid) {
        intent.putExtra("jid", jid.getRawString());
    }

    public static void A14(Resources resources, TextView textView, int i) {
        textView.setTextColor(resources.getColor(i));
    }

    public static void A15(Resources resources, TextView textView, Object[] objArr, int i, int i2) {
        textView.setText(resources.getQuantityString(i, i2, objArr));
    }

    public static void A16(Drawable drawable, View view) {
        if (drawable.isStateful()) {
            drawable.setState(view.getDrawableState());
        }
    }

    public static void A17(Drawable drawable, View view) {
        drawable.setBounds(0, 0, view.getWidth(), view.getHeight());
    }

    public static void A18(Handler handler, Object obj, Object obj2, int i) {
        handler.post(new RunnableBRunnable0Shape10S0200000_I1(obj, i, obj2));
    }

    public static void A19(PowerManager.WakeLock wakeLock) {
        if (wakeLock != null && wakeLock.isHeld()) {
            Log.i("ExportFlowManager/onStartCommand/wakelock released");
            wakeLock.release();
        }
    }

    public static void A1A(View view, int i, int i2) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, 1073741824), View.MeasureSpec.makeMeasureSpec(i2, 1073741824));
    }

    public static void A1B(View view, int i, int i2) {
        view.findViewById(i).setVisibility(i2);
    }

    public static void A1C(View view, View view2, View view3, int i) {
        view.setVisibility(i);
        view2.setVisibility(i);
        view3.setVisibility(i);
    }

    public static void A1D(View view, ViewGroup viewGroup) {
        viewGroup.addView(view, new FrameLayout.LayoutParams(-1, -1, 17));
    }

    public static void A1E(View view, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        view.getViewTreeObserver().removeOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    public static void A1F(View view, ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener) {
        view.getViewTreeObserver().removeGlobalOnLayoutListener(onGlobalLayoutListener);
    }

    public static void A1G(View view, ViewTreeObserver.OnPreDrawListener onPreDrawListener) {
        view.getViewTreeObserver().removeOnPreDrawListener(onPreDrawListener);
    }

    public static void A1H(ViewTreeObserver viewTreeObserver, Object obj, int i) {
        viewTreeObserver.addOnGlobalLayoutListener(new IDxLListenerShape13S0100000_1_I1(obj, i));
    }

    public static void A1I(TextView textView, AnonymousClass01E r2, int i) {
        textView.setText(r2.A0H(i));
    }

    public static void A1J(TextView textView, CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            textView.setText(charSequence);
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(8);
    }

    public static void A1K(AnonymousClass04Y r1, Object obj, List list) {
        list.remove(obj);
        if (!r1.A0B()) {
            r1.A02();
        }
    }

    public static void A1L(ConversationRowAudioPreview conversationRowAudioPreview) {
        if (conversationRowAudioPreview != null) {
            conversationRowAudioPreview.A03.setVisibility(0);
            conversationRowAudioPreview.A00.setVisibility(8);
        }
    }

    public static void A1M(AbstractC16350or r1) {
        if (r1 != null) {
            r1.A03(true);
        }
    }

    public static void A1N(File file) {
        if (file.exists()) {
            file.delete();
        }
    }

    public static void A1O(Object obj, Object obj2, Object obj3, Object[] objArr) {
        objArr[36] = obj;
        objArr[37] = obj2;
        objArr[38] = obj3;
    }

    public static void A1P(Object obj, Object obj2, Object obj3, Object[] objArr) {
        objArr[2] = obj;
        objArr[3] = obj2;
        objArr[4] = obj3;
    }

    public static void A1Q(Object obj, String str, String str2, StringBuilder sb) {
        sb.append(str);
        sb.append(str2);
        sb.append(obj);
    }

    public static void A1R(AbstractCollection abstractCollection, int i) {
        abstractCollection.add(Integer.valueOf(i));
    }

    public static void A1S(List list, int i) {
        Collections.sort(list, new IDxComparatorShape3S0000000_2_I1(i));
    }

    public static void A1T(Object[] objArr, int i) {
        objArr[1] = Integer.valueOf(i);
    }

    public static void A1U(Object[] objArr, int i, long j) {
        objArr[i] = Long.valueOf(j);
    }

    public static boolean A1V(int i, int i2) {
        return i != i2;
    }

    public static boolean A1W(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getBoolean(str, false);
    }

    public static boolean A1X(Object obj) {
        return obj == null;
    }

    public static float[] A1Y(C47762Cm r1) {
        C47762Cm.A00(r1, 2);
        return r1.A01;
    }

    public static int[] A1Z(Object[] objArr, int i, int i2) {
        int[] iArr = new int[1];
        iArr[0] = 128557;
        objArr[i] = iArr;
        int[] iArr2 = new int[1];
        iArr2[0] = 128542;
        objArr[i2] = iArr2;
        return new int[1];
    }

    public static Object[] A1a() {
        return new Object[2];
    }

    public static Object[] A1b(Object obj, Object obj2) {
        Object[] objArr = new Object[4];
        objArr[0] = obj.toString();
        objArr[1] = obj2.toString();
        return objArr;
    }
}
