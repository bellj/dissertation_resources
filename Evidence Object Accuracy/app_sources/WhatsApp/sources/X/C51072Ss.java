package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/* renamed from: X.2Ss  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51072Ss {
    public static final String[] A0A = {"e1.whatsapp.net.", "e2.whatsapp.net.", "e3.whatsapp.net.", "e4.whatsapp.net.", "e5.whatsapp.net.", "e6.whatsapp.net.", "e7.whatsapp.net.", "e8.whatsapp.net.", "e9.whatsapp.net.", "e10.whatsapp.net.", "e11.whatsapp.net.", "e12.whatsapp.net.", "e13.whatsapp.net.", "e14.whatsapp.net.", "e15.whatsapp.net.", "e16.whatsapp.net."};
    public int A00 = -1;
    public int A01 = 0;
    public int A02;
    public final int A03;
    public final int A04;
    public final C20250vS A05;
    public final String A06;
    public final List A07;
    public final List A08;
    public final Random A09;

    public C51072Ss(C20250vS r4, String str, List list, Random random) {
        List list2;
        this.A05 = r4;
        this.A06 = str;
        this.A09 = random;
        this.A08 = new ArrayList();
        this.A07 = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C31041Zu r1 = (C31041Zu) it.next();
            if (r1.A04) {
                list2 = this.A08;
            } else {
                list2 = this.A07;
            }
            list2.add(r1);
        }
        boolean nextBoolean = random.nextBoolean();
        int i = 443;
        this.A03 = nextBoolean ? 443 : 5222;
        this.A04 = nextBoolean ? 5222 : i;
    }

    public final C29381Ry A00(int i, boolean z) {
        String str;
        if (z) {
            str = "g.whatsapp.net";
        } else {
            String[] strArr = A0A;
            str = strArr[this.A09.nextInt(strArr.length)];
        }
        C29381Ry A00 = this.A05.A00(str);
        A00.A00 = i;
        StringBuilder sb = new StringBuilder("ConnectionSequence/getInetSocketAddress; host=");
        sb.append(str);
        Log.i(sb.toString());
        return A00;
    }
}
