package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3lp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76553lp extends AbstractC106444vi implements Handler.Callback {
    public int A00;
    public int A01;
    public long A02;
    public C100614mC A03;
    public AbstractC117065Yc A04;
    public C76743m8 A05;
    public AbstractC76773mC A06;
    public AbstractC76773mC A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final Handler A0B;
    public final C89864Lr A0C;
    public final AbstractC116845Xc A0D;
    public final AnonymousClass5SS A0E;

    @Override // X.AbstractC117055Yb
    public boolean AJx() {
        return true;
    }

    @Override // X.AbstractC117055Yb, X.AnonymousClass5WX
    public String getName() {
        return "TextRenderer";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C76553lp(Looper looper, AnonymousClass5SS r4) {
        super(3);
        Handler handler;
        AbstractC116845Xc r1 = AbstractC116845Xc.A00;
        this.A0E = r4;
        if (looper == null) {
            handler = null;
        } else {
            handler = new Handler(looper, this);
        }
        this.A0B = handler;
        this.A0D = r1;
        this.A0C = new C89864Lr();
        this.A02 = -9223372036854775807L;
    }

    @Override // X.AbstractC106444vi
    public void A08() {
        this.A03 = null;
        this.A02 = -9223372036854775807L;
        A0C();
        A0E();
        this.A04.release();
        this.A04 = null;
        this.A00 = 0;
    }

    @Override // X.AbstractC106444vi
    public void A09(long j, boolean z) {
        A0C();
        this.A08 = false;
        this.A09 = false;
        this.A02 = -9223372036854775807L;
        if (this.A00 != 0) {
            A0F();
            return;
        }
        A0E();
        this.A04.flush();
    }

    public final long A0B() {
        int i = this.A01;
        if (i == -1 || i >= this.A07.A01.ACo()) {
            return Long.MAX_VALUE;
        }
        return this.A07.ACn(this.A01);
    }

    public final void A0C() {
        List emptyList = Collections.emptyList();
        Handler handler = this.A0B;
        if (handler != null) {
            handler.obtainMessage(0, emptyList).sendToTarget();
        } else {
            this.A0E.AOn(emptyList);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0085  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0D() {
        /*
            r3 = this;
            r0 = 1
            r3.A0A = r0
            X.4mC r2 = r3.A03
            java.lang.String r1 = r2.A0T
            if (r1 == 0) goto L_0x0010
            int r0 = r1.hashCode()
            switch(r0) {
                case -1351681404: goto L_0x001f;
                case -1248334819: goto L_0x0030;
                case -1026075066: goto L_0x003e;
                case -1004728940: goto L_0x004c;
                case 691401887: goto L_0x005a;
                case 822864842: goto L_0x006a;
                case 930165504: goto L_0x007a;
                case 1566015601: goto L_0x007d;
                case 1566016562: goto L_0x008d;
                case 1668750253: goto L_0x009f;
                case 1693976202: goto L_0x00ad;
                default: goto L_0x0010;
            }
        L_0x0010:
            java.lang.String r0 = "Attempted to create decoder for unsupported MIME type: "
            java.lang.StringBuilder r0 = X.C12960it.A0k(r0)
            java.lang.String r0 = X.C12960it.A0d(r1, r0)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x001f:
            java.lang.String r0 = "application/dvbsubs"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            java.util.List r0 = r2.A0U
            X.3ms r2 = new X.3ms
            r2.<init>(r0)
            goto L_0x00ba
        L_0x0030:
            java.lang.String r0 = "application/pgs"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            X.3mv r2 = new X.3mv
            r2.<init>()
            goto L_0x00ba
        L_0x003e:
            java.lang.String r0 = "application/x-mp4-vtt"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            X.3mt r2 = new X.3mt
            r2.<init>()
            goto L_0x00ba
        L_0x004c:
            java.lang.String r0 = "text/vtt"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            X.3mu r2 = new X.3mu
            r2.<init>()
            goto L_0x00ba
        L_0x005a:
            java.lang.String r0 = "application/x-quicktime-tx3g"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            java.util.List r0 = r2.A0U
            X.3mw r2 = new X.3mw
            r2.<init>(r0)
            goto L_0x00ba
        L_0x006a:
            java.lang.String r0 = "text/x-ssa"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            java.util.List r0 = r2.A0U
            X.3mz r2 = new X.3mz
            r2.<init>(r0)
            goto L_0x00ba
        L_0x007a:
            java.lang.String r0 = "application/x-mp4-cea-608"
            goto L_0x007f
        L_0x007d:
            java.lang.String r0 = "application/cea-608"
        L_0x007f:
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            int r0 = r2.A03
            X.3n4 r2 = new X.3n4
            r2.<init>(r0, r1)
            goto L_0x00ba
        L_0x008d:
            java.lang.String r0 = "application/cea-708"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            int r1 = r2.A03
            java.util.List r0 = r2.A0U
            X.3n3 r2 = new X.3n3
            r2.<init>(r0, r1)
            goto L_0x00ba
        L_0x009f:
            java.lang.String r0 = "application/x-subrip"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            X.3mx r2 = new X.3mx
            r2.<init>()
            goto L_0x00ba
        L_0x00ad:
            java.lang.String r0 = "application/ttml+xml"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0010
            X.3my r2 = new X.3my
            r2.<init>()
        L_0x00ba:
            r3.A04 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76553lp.A0D():void");
    }

    public final void A0E() {
        this.A05 = null;
        this.A01 = -1;
        AbstractC76773mC r0 = this.A07;
        if (r0 != null) {
            r0.release();
            this.A07 = null;
        }
        AbstractC76773mC r02 = this.A06;
        if (r02 != null) {
            r02.release();
            this.A06 = null;
        }
    }

    public final void A0F() {
        A0E();
        this.A04.release();
        this.A04 = null;
        this.A00 = 0;
        A0D();
    }

    @Override // X.AbstractC117055Yb
    public boolean AJN() {
        return this.A09;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ab, code lost:
        if (r8 != false) goto L_0x00ad;
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x008c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x00c5 A[EXC_TOP_SPLITTER, LOOP:1: B:84:0x00c5->B:96:0x00c5, LOOP_START, SYNTHETIC] */
    @Override // X.AbstractC117055Yb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AaS(long r11, long r13) {
        /*
        // Method dump skipped, instructions count: 321
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76553lp.AaS(long, long):void");
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1 */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3 */
    /* JADX WARN: Type inference failed for: r1v8 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass5WX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int Aeb(X.C100614mC r3) {
        /*
            r2 = this;
            java.lang.String r1 = r3.A0T
            java.lang.String r0 = "text/vtt"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "text/x-ssa"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/ttml+xml"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/x-mp4-vtt"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/x-subrip"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/x-quicktime-tx3g"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/cea-608"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/x-mp4-cea-608"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/cea-708"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/dvbsubs"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = "application/pgs"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0066
            boolean r0 = X.C95554dx.A04(r1)
            boolean r1 = X.C12960it.A1S(r0)
        L_0x0062:
            r0 = 0
            r1 = r1 | r0
            r1 = r1 | r0
            return r1
        L_0x0066:
            java.lang.Class r0 = r3.A0N
            r1 = 2
            if (r0 != 0) goto L_0x0062
            r1 = 4
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76553lp.Aeb(X.4mC):int");
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 0) {
            this.A0E.AOn((List) message.obj);
            return true;
        }
        throw C72463ee.A0D();
    }
}
