package X;

import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;

/* renamed from: X.6By  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133746By implements AnonymousClass6MY {
    public final /* synthetic */ int A00 = R.string.register_wait_message;
    public final /* synthetic */ ActivityC13790kL A01;
    public final /* synthetic */ FingerprintBottomSheet A02;
    public final /* synthetic */ PinBottomSheetDialogFragment A03;
    public final /* synthetic */ AbstractC118055bC A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ String A07;

    public C133746By(ActivityC13790kL r2, FingerprintBottomSheet fingerprintBottomSheet, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, AbstractC118055bC r5, String str, String str2, String str3) {
        this.A04 = r5;
        this.A06 = str;
        this.A05 = str2;
        this.A01 = r2;
        this.A07 = str3;
        this.A03 = pinBottomSheetDialogFragment;
        this.A02 = fingerprintBottomSheet;
    }

    @Override // X.AnonymousClass6MY
    public void AW2() {
        AbstractC118055bC r3 = this.A04;
        int i = this.A00;
        String str = this.A06;
        String str2 = this.A05;
        ActivityC13790kL r1 = this.A01;
        String str3 = this.A07;
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment = this.A03;
        pinBottomSheetDialogFragment.A0B = new AnonymousClass6CH(r1, pinBottomSheetDialogFragment, r3, str2, str3, str, i);
        r1.Adm(pinBottomSheetDialogFragment);
    }

    @Override // X.AnonymousClass6MY
    public void AX7(byte[] bArr) {
        AbstractC118055bC r7 = this.A04;
        AnonymousClass605 r2 = r7.A07;
        String str = this.A07;
        String str2 = this.A06;
        int i = this.A00;
        C129275xS r3 = new C129275xS(this.A01, this.A02, null, r7, str2, this.A05, str, 0, i);
        r2.A01(new AbstractC136296Lz(bArr) { // from class: X.6Bz
            public final /* synthetic */ byte[] A01;

            {
                this.A01 = r2;
            }

            @Override // X.AbstractC136296Lz
            public final void AVF(C128545wH r4) {
                C129275xS.this.A00(null, r4.A02(this.A01));
            }
        }, new AnonymousClass6M0() { // from class: X.6C6
            @Override // X.AnonymousClass6M0
            public final void AVD(C452120p r32) {
                C129275xS.this.A00(r32, null);
            }
        }, str);
    }
}
