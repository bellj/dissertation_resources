package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;

/* renamed from: X.4jX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98964jX implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        long j4 = 0;
        int i = 102;
        boolean z = false;
        int i2 = Integer.MAX_VALUE;
        float f = 0.0f;
        boolean z2 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    j = C95664e9.A04(parcel, readInt);
                    break;
                case 3:
                    j2 = C95664e9.A04(parcel, readInt);
                    break;
                case 4:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 5:
                    j3 = C95664e9.A04(parcel, readInt);
                    break;
                case 6:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 7:
                    f = C95664e9.A00(parcel, readInt);
                    break;
                case '\b':
                    j4 = C95664e9.A04(parcel, readInt);
                    break;
                case '\t':
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new LocationRequest(f, i, i2, j, j2, j3, j4, z, z2);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new LocationRequest[i];
    }
}
