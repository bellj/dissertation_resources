package X;

/* renamed from: X.1gd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34741gd extends AbstractC16110oT {
    public Boolean A00;
    public Long A01;

    public C34741gd() {
        super(2506, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamMdAppStateRegistrationDirtyState {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "dataDeletionResult", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "timeBetweenDataDeletionAndFirstCompanionRegistration", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
