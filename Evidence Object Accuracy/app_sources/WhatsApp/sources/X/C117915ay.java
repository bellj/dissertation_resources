package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* renamed from: X.5ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117915ay extends AnonymousClass015 {
    public C27691It A00 = C13000ix.A03();
    public C27691It A01 = C13000ix.A03();
    public C27691It A02 = C13000ix.A03();
    public final C17900ra A03;
    public final C17070qD A04;

    public C117915ay(C17900ra r4, C17070qD r5) {
        this.A04 = r5;
        this.A03 = r4;
        C17930rd A01 = r4.A01();
        AnonymousClass009.A05(A01);
        AbstractC38041nQ A012 = this.A04.A01(A01.A03);
        AnonymousClass009.A05(A012);
        ArrayList A0l = C12960it.A0l();
        for (AbstractC129495xo r0 : ((AnonymousClass6BF) A012).A01) {
            C127705uv A00 = r0.A00();
            if (A00 != null) {
                A0l.add(A00);
            }
        }
        Collections.sort(A0l, new Comparator() { // from class: X.6KI
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return 0;
            }
        });
        this.A01.A0B(A0l);
    }
}
