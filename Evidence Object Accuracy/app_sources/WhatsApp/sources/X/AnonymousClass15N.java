package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.15N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15N {
    public final AnonymousClass15M A00;

    public AnonymousClass15N(AnonymousClass15M r1) {
        this.A00 = r1;
    }

    public final List A00(long j, boolean z) {
        String str;
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList arrayList = new ArrayList();
        try {
            C16310on A01 = this.A00.get();
            C16330op r9 = A01.A03;
            String[] strArr = C35791ik.A00;
            String[] strArr2 = new String[2];
            if (z) {
                str = "1";
            } else {
                str = "0";
            }
            strArr2[0] = str;
            strArr2[1] = Long.toString(j);
            Cursor A08 = r9.A08("location_sharer", "from_me = ? AND expires >= ?", "_id DESC", null, strArr, strArr2);
            if (A08 == null) {
                Log.e("LocationSharingStore/getAllLocationSharers/unable to get location sharers");
                A01.close();
                return arrayList;
            }
            while (A08.moveToNext()) {
                AbstractC14640lm A012 = AbstractC14640lm.A01(A08.getString(0));
                C35801il r2 = null;
                if (A012 != null) {
                    r2 = new C35801il(A08, A012, UserJid.getNullable(A08.getString(2)));
                }
                if (r2 != null) {
                    arrayList.add(r2);
                }
            }
            A08.close();
            A01.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/getAllLocationSharers/returned ");
            sb.append(arrayList.size());
            sb.append(" location sharer; fromMe=");
            sb.append(z);
            sb.append(" | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
            return arrayList;
        } catch (Exception e) {
            Log.e("LocationSharingStore/getAllLocationSharers/error getting sharers", e);
            throw new RuntimeException(e);
        }
    }

    public final void A01(long j, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            C16330op r7 = A02.A03;
            String[] strArr = new String[3];
            strArr[0] = Long.toString(j);
            strArr[1] = Long.toString(0);
            strArr[2] = z ? "1" : "0";
            int A01 = r7.A01("location_sharer", "expires < ? AND expires > ? AND from_me = ?", strArr);
            A02.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/deleteOldLocationSharers/deleted ");
            sb.append(A01);
            sb.append(" location sharers | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
        } catch (Exception e) {
            Log.e("LocationSharingStore/deleteOldLocationSharers/delete failed", e);
            throw new RuntimeException(e);
        }
    }

    public final void A02(AbstractC14640lm r15, Collection collection, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            AnonymousClass1Lx A01 = A02.A01();
            Iterator it = collection.iterator();
            int i = 0;
            while (it.hasNext()) {
                C16330op r7 = A02.A03;
                String[] strArr = new String[3];
                strArr[0] = r15.getRawString();
                strArr[1] = ((UserJid) it.next()).getRawString();
                strArr[2] = z ? "1" : "0";
                i += r7.A01("location_sharer", "remote_jid = ? AND remote_resource = ? AND from_me = ?", strArr);
            }
            A01.A00();
            A01.close();
            A02.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/deleteLocationSharers/deleted ");
            sb.append(i);
            sb.append(" location sharers | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
        } catch (Exception e) {
            Log.e("LocationSharingStore/deleteLocationSharers/delete failed", e);
            throw new RuntimeException(e);
        }
    }

    public void A03(C30751Yr r7) {
        C16310on A02 = this.A00.A02();
        try {
            ContentValues contentValues = new ContentValues();
            UserJid userJid = r7.A06;
            contentValues.put("jid", userJid.getRawString());
            contentValues.put("latitude", Double.valueOf(r7.A00));
            contentValues.put("longitude", Double.valueOf(r7.A01));
            contentValues.put("accuracy", Integer.valueOf(r7.A03));
            contentValues.put("speed", Float.valueOf(r7.A02));
            contentValues.put("bearing", Integer.valueOf(r7.A04));
            contentValues.put("location_ts", Long.valueOf(r7.A05));
            A02.A03.A05(contentValues, "location_cache");
            StringBuilder sb = new StringBuilder();
            sb.append("LocationSharingStore/saveUserLocation/saved user location; jid=");
            sb.append(userJid);
            sb.append("; timestamp=");
            sb.append(r7.A05);
            Log.i(sb.toString());
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A04(Iterable iterable, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            AnonymousClass1Lx A00 = A02.A00();
            Iterator it = iterable.iterator();
            int i = 0;
            while (it.hasNext()) {
                C16330op r5 = A02.A03;
                String[] strArr = new String[2];
                strArr[0] = ((AbstractC14640lm) it.next()).getRawString();
                strArr[1] = z ? "1" : "0";
                i += r5.A01("location_sharer", "remote_jid = ? AND from_me = ?", strArr);
            }
            A00.A00();
            A00.close();
            A02.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/deleteLocationSharers/deleted ");
            sb.append(i);
            sb.append(" location sharers | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
        } catch (Exception e) {
            Log.e("LocationSharingStore/deleteLocationSharers/delete failed", e);
            throw new RuntimeException(e);
        }
    }

    public void A05(Collection collection) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            AnonymousClass1Lx A01 = A02.A01();
            Iterator it = collection.iterator();
            int i = 0;
            while (it.hasNext()) {
                i += A02.A03.A01("location_cache", "jid = ?", new String[]{((UserJid) it.next()).getRawString()});
            }
            A01.A00();
            A01.close();
            A02.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/deleteUserLocations/deleted ");
            sb.append(i);
            sb.append(" location sharers | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
        } catch (Exception e) {
            Log.e("LocationSharingStore/deleteUserLocations/delete failed", e);
            throw new RuntimeException(e);
        }
    }

    public void A06(Collection collection, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            AnonymousClass1Lx A01 = A02.A01();
            Iterator it = collection.iterator();
            int i = 0;
            while (it.hasNext()) {
                C35211hS r8 = (C35211hS) it.next();
                for (UserJid userJid : r8.A03) {
                    ContentValues contentValues = new ContentValues();
                    AnonymousClass1IS r9 = r8.A02;
                    AbstractC14640lm r0 = r9.A00;
                    AnonymousClass009.A05(r0);
                    contentValues.put("remote_jid", r0.getRawString());
                    contentValues.put("from_me", Boolean.TRUE);
                    contentValues.put("remote_resource", userJid.getRawString());
                    contentValues.put("expires", Long.valueOf(Math.min(r8.A01, j)));
                    contentValues.put("message_id", r9.A01);
                    int i2 = 0;
                    if (A02.A03.A05(contentValues, "location_sharer") >= 0) {
                        i2 = 1;
                    }
                    i += i2;
                }
            }
            A01.A00();
            A01.close();
            A02.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/updateSharingExpire/update ");
            sb.append(i);
            sb.append(" location sharers | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
        } catch (Exception e) {
            Log.e("LocationSharingStore/updateSharingExpire/save failed", e);
            throw new RuntimeException(e);
        }
    }

    public void A07(List list) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            AnonymousClass1Lx A01 = A02.A01();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                C35801il r6 = (C35801il) it.next();
                ContentValues contentValues = new ContentValues();
                contentValues.put("remote_jid", r6.A01.getRawString());
                UserJid userJid = r6.A02;
                String str = null;
                if (userJid != null) {
                    str = userJid.getRawString();
                }
                contentValues.put("remote_resource", str);
                AnonymousClass1IS r3 = r6.A03;
                contentValues.put("from_me", Boolean.valueOf(r3.A02));
                contentValues.put("expires", Long.valueOf(r6.A00));
                contentValues.put("message_id", r3.A01);
                A02.A03.A05(contentValues, "location_sharer");
            }
            A01.A00();
            A01.close();
            A02.close();
            StringBuilder sb = new StringBuilder("LocationSharingStore/saveLocationSharer/saved ");
            sb.append(list.size());
            sb.append(" location sharers | time: ");
            sb.append(System.currentTimeMillis() - currentTimeMillis);
            Log.i(sb.toString());
        } catch (Exception e) {
            Log.e("LocationSharingStore/saveLocationSharer/save failed", e);
            throw new RuntimeException(e);
        }
    }

    public void A08(List list, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            C16310on A02 = this.A00.A02();
            AnonymousClass1Lx A00 = A02.A00();
            try {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("jid", ((UserJid) it.next()).getRawString());
                    contentValues.put("sent_to_server", Boolean.valueOf(z));
                    A02.A03.A05(contentValues, "location_key_distribution");
                }
                A00.A00();
                A00.close();
                A02.close();
                StringBuilder sb = new StringBuilder("LocationSharingStore/storeLocationReceiverHasKey/saved ");
                sb.append(list.size());
                sb.append(" location receiver has key: ");
                sb.append(z);
                sb.append(" | time: ");
                sb.append(System.currentTimeMillis() - currentTimeMillis);
                Log.i(sb.toString());
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception e) {
            Log.e("LocationSharingStore/storeLocationReceiverHasKey/save failed", e);
            throw new RuntimeException(e);
        }
    }
}
