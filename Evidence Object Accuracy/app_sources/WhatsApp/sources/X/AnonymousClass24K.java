package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.24K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass24K {
    public final C43661xO A00;
    public final AbstractC14640lm A01;
    public final UserJid A02;
    public final AnonymousClass1P4 A03;
    public final String A04;
    public final String A05;
    public final ArrayList A06;
    public final ArrayList A07;

    public AnonymousClass24K(C43661xO r1, AbstractC14640lm r2, UserJid userJid, AnonymousClass1P4 r4, String str, String str2, ArrayList arrayList, ArrayList arrayList2) {
        this.A07 = arrayList;
        this.A06 = arrayList2;
        this.A01 = r2;
        this.A02 = userJid;
        this.A03 = r4;
        this.A05 = str2;
        this.A04 = str;
        this.A00 = r1;
    }
}
