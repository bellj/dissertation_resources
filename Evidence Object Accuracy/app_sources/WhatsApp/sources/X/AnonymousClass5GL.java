package X;

import java.security.Signature;

/* renamed from: X.5GL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GL implements AnonymousClass5VR {
    public final /* synthetic */ AbstractC113435Ho A00;

    public AnonymousClass5GL(AbstractC113435Ho r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VR
    public Signature A8Z(String str) {
        try {
            return Signature.getInstance(str, ((AnonymousClass5GT) this.A00.A02).A00);
        } catch (Exception unused) {
            return Signature.getInstance(str);
        }
    }
}
