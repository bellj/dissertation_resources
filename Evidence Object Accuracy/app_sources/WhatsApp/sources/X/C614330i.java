package X;

/* renamed from: X.30i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614330i extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public String A04;

    public C614330i() {
        super(2370, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(5, this.A01);
        r3.Abe(2, this.A03);
        r3.Abe(6, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamEphemeralSettingChange {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "chatEphemeralityDuration", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ephemeralSettingEntryPoint", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ephemeralSettingGroupSize", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "previousEphemeralityDuration", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "threadId", this.A04);
        return C12960it.A0d("}", A0k);
    }
}
