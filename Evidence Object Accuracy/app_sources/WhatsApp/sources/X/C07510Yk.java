package X;

/* renamed from: X.0Yk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07510Yk implements AnonymousClass02B {
    public final /* synthetic */ AnonymousClass02O A00;
    public final /* synthetic */ AnonymousClass02P A01;

    public C07510Yk(AnonymousClass02O r1, AnonymousClass02P r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass02B
    public void ANq(Object obj) {
        this.A01.A0B(this.A00.apply(obj));
    }
}
