package X;

import com.whatsapp.jid.DeviceJid;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: X.2Cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47832Cx extends FutureTask {
    public final DeviceJid A00;

    public C47832Cx(C15950oC r9, C15990oG r10, DeviceJid deviceJid, C29211Rh r12, C29211Rh r13, byte[] bArr, byte[] bArr2, byte b) {
        super(new Callable(r9, r10, r12, r13, bArr, bArr2, b) { // from class: X.5Dy
            public final /* synthetic */ byte A00;
            public final /* synthetic */ C15950oC A01;
            public final /* synthetic */ C15990oG A02;
            public final /* synthetic */ C29211Rh A03;
            public final /* synthetic */ C29211Rh A04;
            public final /* synthetic */ byte[] A05;
            public final /* synthetic */ byte[] A06;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A05 = r5;
                this.A00 = r7;
                this.A03 = r3;
                this.A04 = r4;
                this.A06 = r6;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return Integer.valueOf(this.A02.A07(this.A01, this.A03, this.A04, this.A05, this.A06, this.A00));
            }
        });
        this.A00 = deviceJid;
    }
}
