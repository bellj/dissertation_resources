package X;

import com.whatsapp.util.Log;
import java.util.HashMap;
import org.json.JSONException;

/* renamed from: X.5zH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130355zH {
    public static String A00(Integer num) {
        try {
            return C117305Zk.A0l(num, "fdsInstanceKey", C117295Zj.A0a());
        } catch (JSONException e) {
            Log.e("PAY: createQplPramsStringFromInstanceKey threw exception ", e);
            return null;
        }
    }

    public static void A01(C17120qI r4, String str, String str2, HashMap hashMap) {
        if (str != null) {
            try {
                String string = C13000ix.A05(str).getString("fdsInstanceKey");
                HashMap A11 = C12970iu.A11();
                A11.put("fdsInstanceKey", string);
                A11.put("event", str2);
                if (hashMap != null) {
                    for (Object obj : hashMap.keySet()) {
                        A11.put(obj, hashMap.get(obj));
                    }
                }
                r4.A00().A01(new AnonymousClass6E8(A11));
            } catch (JSONException unused) {
                Log.e("qpl params parsing failure");
            }
        }
    }
}
