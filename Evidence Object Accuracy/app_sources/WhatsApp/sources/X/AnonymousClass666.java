package X;

import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.666  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass666 implements AbstractC136166Lg {
    public final /* synthetic */ AnonymousClass661 A00;

    public AnonymousClass666(AnonymousClass661 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136166Lg
    public void AUE(C129975yb r6) {
        AnonymousClass661 r1 = this.A00;
        r1.AaL(r1.A0H);
        C128965wx r4 = r1.A0L;
        C129465xl r3 = r4.A00;
        ReentrantLock reentrantLock = r3.A01;
        reentrantLock.lock();
        try {
            reentrantLock.lock();
            boolean z = true;
            if ((r3.A00 & 1) != 1) {
                z = false;
            }
            reentrantLock.unlock();
            reentrantLock.lock();
            if (!r3.A01()) {
                r3.A00 = (r3.A00 | 2) & -2;
            }
            reentrantLock.unlock();
            if (z) {
                AnonymousClass616.A00();
                C129775yH r12 = r4.A01;
                if (!r12.A00.isEmpty()) {
                    AnonymousClass61K.A00(new RunnableC135136Hh(r4, r12.A00));
                }
            }
        } finally {
            reentrantLock.unlock();
        }
    }
}
