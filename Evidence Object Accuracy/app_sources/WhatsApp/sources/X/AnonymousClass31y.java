package X;

import com.whatsapp.Mp4Ops;

/* renamed from: X.31y  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31y extends AbstractC628938z {
    public final C14330lG A00;

    public AnonymousClass31y(AbstractC15710nm r11, C14330lG r12, Mp4Ops mp4Ops, C18790t3 r14, C17050qB r15, C14830m7 r16, AnonymousClass1O6 r17, AbstractC39381po r18, String str) {
        super(r11, mp4Ops, r14, r15, r16, r17, r18, str, true);
        this.A00 = r12;
    }
}
