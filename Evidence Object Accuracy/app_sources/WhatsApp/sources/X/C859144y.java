package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.community.CommunityTabViewModel;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.44y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C859144y extends AbstractC469528i {
    public final /* synthetic */ CommunityTabViewModel A00;

    public C859144y(CommunityTabViewModel communityTabViewModel) {
        this.A00 = communityTabViewModel;
    }

    @Override // X.AbstractC469528i
    public void A00(GroupJid groupJid) {
        this.A00.A0O.execute(new RunnableBRunnable0Shape11S0200000_I1_1(this, 8, groupJid));
    }
}
