package X;

/* renamed from: X.4wO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106854wO implements AbstractC14070ko {
    public final long A00;
    public final AbstractC14070ko A01;

    public C106854wO(AbstractC14070ko r1, long j) {
        this.A00 = j;
        this.A01 = r1;
    }

    @Override // X.AbstractC14070ko
    public void A9V() {
        this.A01.A9V();
    }

    @Override // X.AbstractC14070ko
    public void AbR(AnonymousClass5WY r3) {
        this.A01.AbR(new C106914wU(r3, this));
    }

    @Override // X.AbstractC14070ko
    public AnonymousClass5X6 Af4(int i, int i2) {
        return this.A01.Af4(i, i2);
    }
}
