package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import com.whatsapp.fieldstats.events.WamCall;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

/* renamed from: X.0qL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17150qL {
    public final AbstractC15710nm A00;
    public final C18790t3 A01;
    public final C16590pI A02;
    public final C18800t4 A03;
    public final C19930uu A04;
    public final AbstractC14440lR A05;

    public C17150qL(AbstractC15710nm r1, C18790t3 r2, C16590pI r3, C18800t4 r4, C19930uu r5, AbstractC14440lR r6) {
        this.A00 = r1;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
        this.A01 = r2;
        this.A03 = r4;
    }

    public static final void A00(FileOutputStream fileOutputStream, Double d, String str) {
        if (d != null) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(";");
                sb.append(str);
                sb.append(":");
                sb.append(String.format(Locale.US, "%.2f", d));
                fileOutputStream.write(sb.toString().getBytes());
            } catch (IOException e) {
                StringBuilder sb2 = new StringBuilder("app/VoipTimeSeriesLogger: couldn't inject FS ");
                sb2.append(str);
                Log.w(sb2.toString(), e);
            }
        }
    }

    public static final void A01(FileOutputStream fileOutputStream, Long l, String str) {
        if (l != null) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(";");
                sb.append(str);
                sb.append(":");
                sb.append(l);
                fileOutputStream.write(sb.toString().getBytes());
            } catch (IOException e) {
                StringBuilder sb2 = new StringBuilder("app/VoipTimeSeriesLogger: couldn't inject FS ");
                sb2.append(str);
                Log.w(sb2.toString(), e);
            }
        }
    }

    public void A02(WamCall wamCall, String str) {
        Object obj;
        if (wamCall == null || !C29941Vi.A00(wamCall.callResult, 1)) {
            StringBuilder sb = new StringBuilder("Skipping uploadTimeSeries. callResult: ");
            if (wamCall == null) {
                obj = "null FS";
            } else {
                obj = wamCall.callResult;
            }
            sb.append(obj);
            Log.i(sb.toString());
            return;
        }
        this.A05.Ab2(new RunnableBRunnable0Shape2S0300000_I0_2(this, new File(str), wamCall, 5));
    }

    public final void A03(File file, String str) {
        if (file.exists()) {
            AbstractC15710nm r0 = this.A00;
            String A00 = r0.A00();
            C28471Ni r7 = new C28471Ni(this.A01, new C49972Nm(this, file), this.A03, "https://crashlogs.whatsapp.net/wa_clb_data", this.A04.A00(), 16, false, false, false);
            r7.A06("access_token", "1063127757113399|745146ffa34413f9dbb5469f5370b7af");
            r7.A07("from_jid", A00);
            r7.A07("tags", "voip_time_series");
            if (str != null && str.length() > 0) {
                r7.A07("call_id", str);
            }
            try {
                try {
                    r7.A05(new FileInputStream(file), "file", file.getName(), 0, file.length());
                    r7.A02(null);
                } catch (IOException e) {
                    Log.w("app/VoiceService: could not upload dummy time series log data", e);
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.valueOf(file.length()));
                    sb.append(":uploadError:");
                    r0.AaV("voip-time-series-upload-fail", sb.toString(), true);
                }
            } finally {
                if (!file.delete()) {
                    Log.i("app/VoiceService: dummy time series log could not be deleted");
                }
            }
        }
    }
}
