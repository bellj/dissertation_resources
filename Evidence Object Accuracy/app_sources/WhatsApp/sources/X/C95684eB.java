package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.4eB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95684eB {
    public static final AnonymousClass4DX A00 = A0B(false);
    public static final AnonymousClass4DX A01 = A0B(true);
    public static final AnonymousClass4DX A02 = new AnonymousClass4DX();
    public static final Class A03;

    static {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            cls = null;
        }
        A03 = cls;
    }

    public static int A00(AnonymousClass5XU r4, Object obj, int i) {
        int i2;
        int i3;
        AbstractC80173rp r1;
        if (obj instanceof C94434bo) {
            C94434bo r5 = (C94434bo) obj;
            i2 = AnonymousClass4DU.A04(i);
            if (r5.A00 != null) {
                i3 = r5.A00.A02();
            } else if (r5.A01 != null) {
                r1 = (AbstractC80173rp) r5.A01;
                i3 = r1.zzc;
                if (i3 == -1) {
                    i3 = C72463ee.A0C(r1).Ah2(r1);
                    r1.zzc = i3;
                }
            } else {
                i3 = 0;
            }
        } else {
            i2 = AnonymousClass4DU.A04(i);
            AnonymousClass50T r52 = (AnonymousClass50T) ((AbstractC117135Yq) obj);
            r1 = (AbstractC80173rp) r52;
            i3 = r1.zzc;
            if (i3 == -1) {
                i3 = r4.Ah2(r52);
                r1.zzc = i3;
            }
        }
        return i2 + C72453ed.A07(i3) + i3;
    }

    public static int A01(AnonymousClass5XU r7, List list, int i) {
        int i2;
        AbstractC80173rp r1;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int A04 = AnonymousClass4DU.A04(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof C94434bo) {
                C94434bo r3 = (C94434bo) obj;
                if (r3.A00 != null) {
                    i2 = r3.A00.A02();
                } else if (r3.A01 != null) {
                    r1 = (AbstractC80173rp) r3.A01;
                    i2 = r1.zzc;
                    if (i2 == -1) {
                        i2 = C72463ee.A0C(r1).Ah2(r1);
                        r1.zzc = i2;
                    }
                } else {
                    i2 = 0;
                }
            } else {
                AnonymousClass50T r32 = (AnonymousClass50T) ((AbstractC117135Yq) obj);
                r1 = (AbstractC80173rp) r32;
                i2 = r1.zzc;
                if (i2 == -1) {
                    i2 = r7.Ah2(r32);
                    r1.zzc = i2;
                }
            }
            A04 += C72453ed.A07(i2) + i2;
        }
        return A04;
    }

    public static int A02(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80223ru) {
            C80223ru r5 = (C80223ru) list;
            i = 0;
            while (i2 < size) {
                r5.A03(i2);
                i += C80253rx.A01(r5.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C80253rx.A01(C72453ed.A0Z(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A03(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80223ru) {
            C80223ru r5 = (C80223ru) list;
            i = 0;
            while (i2 < size) {
                r5.A03(i2);
                i += C80253rx.A01(r5.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C80253rx.A01(C72453ed.A0Z(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A04(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80223ru) {
            C80223ru r5 = (C80223ru) list;
            i = 0;
            while (i2 < size) {
                r5.A03(i2);
                i += AnonymousClass4DU.A06(r5.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C80253rx.A01(C72453ed.A0Y(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A05(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80213rt) {
            C80213rt r4 = (C80213rt) list;
            i = 0;
            while (i2 < size) {
                r4.A04(i2);
                i += C72453ed.A06(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C72453ed.A06(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A06(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80213rt) {
            C80213rt r4 = (C80213rt) list;
            i = 0;
            while (i2 < size) {
                r4.A04(i2);
                i += C72453ed.A06(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C72453ed.A06(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A07(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80213rt) {
            C80213rt r4 = (C80213rt) list;
            i = 0;
            while (i2 < size) {
                r4.A04(i2);
                i += C72453ed.A07(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C72453ed.A07(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A08(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C80213rt) {
            C80213rt r4 = (C80213rt) list;
            i = 0;
            while (i2 < size) {
                r4.A04(i2);
                i += AnonymousClass4DU.A05(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += AnonymousClass4DU.A05(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:32:0x0023 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:25:0x002e */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:36:0x004b */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:27:0x0056 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: java.lang.String */
    /* JADX DEBUG: Multi-variable search result rejected for r1v10, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v4, types: [int] */
    /* JADX WARN: Type inference failed for: r1v7, types: [int] */
    /* JADX WARN: Type inference failed for: r1v9, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v12, types: [int] */
    /* JADX WARN: Type inference failed for: r1v15, types: [int] */
    public static int A09(List list, int i) {
        String str;
        String str2;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int A04 = AnonymousClass4DU.A04(i) * size;
        if (list instanceof AnonymousClass5Z4) {
            AnonymousClass5Z4 r5 = (AnonymousClass5Z4) list;
            while (i2 < size) {
                Object Ah7 = r5.Ah7(i2);
                if (Ah7 instanceof AbstractC111925Bi) {
                    str2 = ((AbstractC111925Bi) Ah7).A02();
                } else {
                    str2 = (String) Ah7;
                    try {
                        str2 = C94634cG.A00(str2);
                    } catch (AnonymousClass4CN unused) {
                        str2 = str2.getBytes(C93104Zc.A02).length;
                    }
                }
                A04 += C72453ed.A07(str2) + str2;
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                if (obj instanceof AbstractC111925Bi) {
                    str = ((AbstractC111925Bi) obj).A02();
                } else {
                    str = (String) obj;
                    try {
                        str = C94634cG.A00(str);
                    } catch (AnonymousClass4CN unused2) {
                        str = str.getBytes(C93104Zc.A02).length;
                    }
                }
                A04 += C72453ed.A07(str) + str;
                i2++;
            }
        }
        return A04;
    }

    public static int A0A(List list, int i) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int A04 = size * AnonymousClass4DU.A04(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            int A022 = ((AbstractC111925Bi) list.get(i2)).A02();
            A04 += C72453ed.A07(A022) + A022;
        }
        return A04;
    }

    public static AnonymousClass4DX A0B(boolean z) {
        try {
            Class<?> cls = Class.forName("com.google.protobuf.UnknownFieldSetSchema");
            if (cls != null) {
                return (AnonymousClass4DX) cls.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    public static void A0C(AbstractC115295Qy r5, List list, int i) {
        if (list != null && !list.isEmpty()) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (list instanceof AnonymousClass5Z4) {
                AnonymousClass5Z4 r3 = (AnonymousClass5Z4) list;
                while (i2 < list.size()) {
                    Object Ah7 = r3.Ah7(i2);
                    boolean z = Ah7 instanceof String;
                    C80253rx r0 = r52.A00;
                    if (z) {
                        r0.A08(i, (String) Ah7);
                    } else {
                        r0.A0B((AbstractC111925Bi) Ah7, i);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                r52.A00.A08(i, C12960it.A0g(list, i2));
                i2++;
            }
        }
    }

    public static void A0D(AbstractC115295Qy r3, List list, int i) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r32 = (C1090250c) r3;
            for (int i2 = 0; i2 < list.size(); i2++) {
                r32.A00.A0B((AbstractC111925Bi) list.get(i2), i);
            }
        }
    }

    public static void A0E(AbstractC115295Qy r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (z) {
                C80253rx r3 = r52.A00;
                AnonymousClass4DU.A07(r3, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 8;
                }
                r3.A05(i3);
                while (i2 < list.size()) {
                    r3.A0A(Double.doubleToRawLongBits(C72453ed.A00(list.get(i2))));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r32 = r52.A00;
                long doubleToRawLongBits = Double.doubleToRawLongBits(C72453ed.A00(list.get(i2)));
                AnonymousClass4DU.A07(r32, i, 1);
                r32.A0A(doubleToRawLongBits);
                i2++;
            }
        }
    }

    public static void A0F(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 4;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A06(Float.floatToRawIntBits(C72453ed.A02(list.get(i2))));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r2 = r42.A00;
                int floatToRawIntBits = Float.floatToRawIntBits(C72453ed.A02(list.get(i2)));
                AnonymousClass4DU.A07(r2, i, 5);
                r2.A06(floatToRawIntBits);
                i2++;
            }
        }
    }

    public static void A0G(AbstractC115295Qy r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (z) {
                C80253rx r53 = r52.A00;
                AnonymousClass4DU.A07(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C80253rx.A01(C72453ed.A0Z(list, i4));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    r53.A09(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r3 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4DU.A07(r3, i, 0);
                r3.A09(A0Z);
                i2++;
            }
        }
    }

    public static void A0H(AbstractC115295Qy r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (z) {
                C80253rx r53 = r52.A00;
                AnonymousClass4DU.A07(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C80253rx.A01(C72453ed.A0Z(list, i4));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    r53.A09(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r3 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4DU.A07(r3, i, 0);
                r3.A09(A0Z);
                i2++;
            }
        }
    }

    public static void A0I(AbstractC115295Qy r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (z) {
                C80253rx r53 = r52.A00;
                AnonymousClass4DU.A07(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C80253rx.A01(C72453ed.A0Y(list, i4));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    r53.A09(C72453ed.A0Y(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r3 = r52.A00;
                long A0Y = C72453ed.A0Y(list, i2);
                AnonymousClass4DU.A07(r3, i, 0);
                r3.A09(A0Y);
                i2++;
            }
        }
    }

    public static void A0J(AbstractC115295Qy r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (z) {
                C80253rx r3 = r52.A00;
                AnonymousClass4DU.A07(r3, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 8;
                }
                r3.A05(i3);
                while (i2 < list.size()) {
                    r3.A0A(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r32 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4DU.A07(r32, i, 1);
                r32.A0A(A0Z);
                i2++;
            }
        }
    }

    public static void A0K(AbstractC115295Qy r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r52 = (C1090250c) r5;
            int i2 = 0;
            if (z) {
                C80253rx r3 = r52.A00;
                AnonymousClass4DU.A07(r3, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 8;
                }
                r3.A05(i3);
                while (i2 < list.size()) {
                    r3.A0A(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r32 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4DU.A07(r32, i, 1);
                r32.A0A(A0Z);
                i2++;
            }
        }
    }

    public static void A0L(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C72453ed.A06(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    int A0G = C72453ed.A0G(list, i2);
                    if (A0G >= 0) {
                        r43.A05(A0G);
                    } else {
                        r43.A09((long) A0G);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                r42.A00.A07(i, C72453ed.A0G(list, i2));
                i2++;
            }
        }
    }

    public static void A0M(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C72453ed.A07(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A05(C72453ed.A0G(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r2 = r42.A00;
                int A0G = C72453ed.A0G(list, i2);
                AnonymousClass4DU.A07(r2, i, 0);
                r2.A05(A0G);
                i2++;
            }
        }
    }

    public static void A0N(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += AnonymousClass4DU.A05(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    int A0G = C72453ed.A0G(list, i2);
                    r43.A05((A0G >> 31) ^ (A0G << 1));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r2 = r42.A00;
                int A0G2 = C72453ed.A0G(list, i2);
                AnonymousClass4DU.A07(r2, i, 0);
                r2.A05((A0G2 >> 31) ^ (A0G2 << 1));
                i2++;
            }
        }
    }

    public static void A0O(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 4;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A06(C72453ed.A0G(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r2 = r42.A00;
                int A0G = C72453ed.A0G(list, i2);
                AnonymousClass4DU.A07(r2, i, 5);
                r2.A06(A0G);
                i2++;
            }
        }
    }

    public static void A0P(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 4;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A06(C72453ed.A0G(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r2 = r42.A00;
                int A0G = C72453ed.A0G(list, i2);
                AnonymousClass4DU.A07(r2, i, 5);
                r2.A06(A0G);
                i2++;
            }
        }
    }

    public static void A0Q(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C72453ed.A06(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    int A0G = C72453ed.A0G(list, i2);
                    if (A0G >= 0) {
                        r43.A05(A0G);
                    } else {
                        r43.A09((long) A0G);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                r42.A00.A07(i, C72453ed.A0G(list, i2));
                i2++;
            }
        }
    }

    public static void A0R(AbstractC115295Qy r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C1090250c r42 = (C1090250c) r4;
            int i2 = 0;
            if (z) {
                C80253rx r43 = r42.A00;
                AnonymousClass4DU.A07(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3++;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A04(C12970iu.A1Y(list.get(i2)) ? (byte) 1 : 0);
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                C80253rx r2 = r42.A00;
                boolean A1Y = C12970iu.A1Y(list.get(i2));
                AnonymousClass4DU.A07(r2, i, 0);
                r2.A04(A1Y ? (byte) 1 : 0);
                i2++;
            }
        }
    }

    public static void A0S(Object obj, Object obj2) {
        AbstractC80173rp r9 = (AbstractC80173rp) obj;
        C94994cs r7 = r9.zzb;
        C94994cs r8 = ((AbstractC80173rp) obj2).zzb;
        if (!r8.equals(C94994cs.A05)) {
            int i = r7.A00 + r8.A00;
            int[] copyOf = Arrays.copyOf(r7.A03, i);
            System.arraycopy(r8.A03, 0, copyOf, r7.A00, r8.A00);
            Object[] copyOf2 = Arrays.copyOf(r7.A04, i);
            System.arraycopy(r8.A04, 0, copyOf2, r7.A00, r8.A00);
            r7 = new C94994cs(copyOf, copyOf2, i, true);
        }
        r9.zzb = r7;
    }
}
