package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.37A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37A extends AbstractC16350or {
    public final ArrayList A00;
    public final List A01;
    public final /* synthetic */ AbstractActivityC36551k4 A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass37A(AbstractActivityC36551k4 r2, List list, List list2) {
        super(r2);
        ArrayList arrayList;
        this.A02 = r2;
        if (list != null) {
            arrayList = C12980iv.A0x(list);
        } else {
            arrayList = null;
        }
        this.A00 = arrayList;
        this.A01 = list2;
    }
}
