package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaperPreview;
import java.util.Map;

/* renamed from: X.35J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35J extends AbstractC55342iF {
    public boolean A00 = false;
    public final Map A01 = C12970iu.A11();
    public final /* synthetic */ SolidColorWallpaperPreview A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass35J(Context context, SolidColorWallpaperPreview solidColorWallpaperPreview) {
        super(context, null);
        this.A02 = solidColorWallpaperPreview;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A02.A0D.length;
    }

    @Override // X.AnonymousClass01A
    public int A02(Object obj) {
        if (obj instanceof AnonymousClass35E) {
            Object tag = ((View) obj).getTag();
            if (tag instanceof Integer) {
                int A05 = C12960it.A05(tag);
                Map map = this.A01;
                Integer valueOf = Integer.valueOf(A05);
                if (map.containsKey(valueOf) && !Boolean.valueOf(this.A00).equals(map.get(valueOf))) {
                    return -2;
                }
            }
        }
        return -1;
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }
}
