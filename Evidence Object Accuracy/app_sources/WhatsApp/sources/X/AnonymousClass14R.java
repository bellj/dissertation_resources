package X;

/* renamed from: X.14R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14R {
    public final C19950uw A00;
    public final C20110vE A01;
    public final C19940uv A02;
    public final AnonymousClass14Q A03;
    public final AnonymousClass14P A04;

    public AnonymousClass14R(C19950uw r1, C20110vE r2, C19940uv r3, AnonymousClass14Q r4, AnonymousClass14P r5) {
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }
}
