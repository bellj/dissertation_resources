package X;

/* renamed from: X.50W  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50W implements AbstractC116395Vg {
    public static final AnonymousClass50W A00 = new AnonymousClass50W();

    @Override // X.AbstractC116395Vg
    public final boolean Ags(Class cls) {
        return AbstractC80173rp.class.isAssignableFrom(cls);
    }

    @Override // X.AbstractC116395Vg
    public final AbstractC115245Qt Ah6(Class cls) {
        if (!AbstractC80173rp.class.isAssignableFrom(cls)) {
            throw C12970iu.A0f(C12960it.A0c(cls.getName(), "Unsupported message type: "));
        }
        try {
            Class asSubclass = cls.asSubclass(AbstractC80173rp.class);
            AbstractC80173rp r2 = (AbstractC80173rp) AbstractC80173rp.zzd.get(asSubclass);
            if (r2 == null) {
                try {
                    Class.forName(asSubclass.getName(), true, asSubclass.getClassLoader());
                    r2 = (AbstractC80173rp) AbstractC80173rp.zzd.get(asSubclass);
                    if (r2 == null) {
                        r2 = (AbstractC80173rp) ((AbstractC80173rp) C95634e6.A02(asSubclass)).A06(6);
                        if (r2 != null) {
                            AnonymousClass50T.A08(asSubclass, r2);
                        } else {
                            throw C72463ee.A0D();
                        }
                    }
                } catch (ClassNotFoundException e) {
                    throw new IllegalStateException("Class initialization cannot fail.", e);
                }
            }
            return (AbstractC115245Qt) r2.A06(3);
        } catch (Exception e2) {
            throw new RuntimeException(C12960it.A0c(cls.getName(), "Unable to get message info for "), e2);
        }
    }
}
