package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.24Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass24Z extends BroadcastReceiver {
    public C20390vg A00;
    public final Object A01 = new Object();
    public volatile boolean A02 = false;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A02) {
            synchronized (this.A01) {
                if (!this.A02) {
                    this.A00 = (C20390vg) ((AnonymousClass01J) AnonymousClass22D.A00(context)).AEi.get();
                    this.A02 = true;
                }
            }
        }
        Log.i("PaymentMethodUpdateNotification/dismiss");
        this.A00.A00();
    }
}
