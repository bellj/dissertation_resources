package X;

import java.util.AbstractCollection;
import java.util.TreeMap;

/* renamed from: X.1Sy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29491Sy {
    public C91184Qt A00;
    public C90664Ot A01;
    public boolean A02;
    public boolean A03;

    public C29491Sy(C29441Sr r8) {
        C90664Ot r0;
        int A00;
        AbstractCollection abstractCollection;
        int[] iArr;
        try {
            C29461Su r6 = r8.A08;
            TreeMap treeMap = r6.A02;
            if (treeMap == null || (iArr = (int[]) treeMap.get("trace_config.duration_condition")) == null || iArr.length <= 0) {
                r0 = null;
            } else {
                r0 = new C90664Ot(iArr);
            }
            this.A01 = r0;
            TreeMap treeMap2 = r6.A04;
            String[] strArr = null;
            if (!(treeMap2 == null || (abstractCollection = (AbstractCollection) treeMap2.get("trace_config.string_list_condition")) == null)) {
                strArr = (String[]) abstractCollection.toArray(new String[abstractCollection.size()]);
            }
            C91184Qt r3 = null;
            if (strArr != null && strArr.length > 0 && strArr[0].equals("annotation") && (A00 = r6.A00("trace_config.fallback_sampling_rate_for_string_list_condition", 0)) != 1) {
                r3 = new C91184Qt(strArr, A00);
            }
            this.A00 = r3;
        } catch (IllegalArgumentException unused) {
            this.A02 = true;
        }
        if (this.A01 != null || this.A00 != null) {
            this.A03 = true;
        }
    }
}
