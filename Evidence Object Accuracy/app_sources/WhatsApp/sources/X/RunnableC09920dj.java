package X;

import java.util.concurrent.ExecutionException;

/* renamed from: X.0dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09920dj implements Runnable {
    public AbstractC11960h9 A00;
    public AbstractFutureC44231yX A01;
    public String A02;

    public RunnableC09920dj(AbstractC11960h9 r1, AbstractFutureC44231yX r2, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean z;
        try {
            z = ((Boolean) this.A01.get()).booleanValue();
        } catch (InterruptedException | ExecutionException unused) {
            z = true;
        }
        this.A00.AQ1(this.A02, z);
    }
}
