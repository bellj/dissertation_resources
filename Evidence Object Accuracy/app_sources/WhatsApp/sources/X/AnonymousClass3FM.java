package X;

/* renamed from: X.3FM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FM {
    public int A00;
    public int A01;
    public AnonymousClass4AV A02;
    public String A03;
    public String A04;
    public String A05;

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass3FM)) {
            return false;
        }
        AnonymousClass3FM r4 = (AnonymousClass3FM) obj;
        if (this.A02 == r4.A02 && r4.A01 == this.A01 && r4.A00 == this.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        Object[] objArr = new Object[4];
        objArr[0] = this.A02;
        C12980iv.A1T(objArr, this.A01);
        C12990iw.A1V(objArr, this.A00);
        return C12980iv.A0B(C12980iv.A0i(), objArr, 3);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ResumeCheck.Result type=");
        A0k.append(this.A02);
        A0k.append(", resume=");
        A0k.append(this.A01);
        A0k.append(", error=");
        A0k.append(this.A00);
        A0k.append(", message=");
        A0k.append(this.A04);
        A0k.append(", backoff=");
        A0k.append(0);
        return C12960it.A0d("]", A0k);
    }
}
