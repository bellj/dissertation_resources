package X;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.UpdateAppearance;
import android.view.MotionEvent;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2aN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC52172aN extends CharacterStyle implements AbstractC116465Vn, UpdateAppearance {
    public int A00;
    public int A01;
    public int A02;
    public long A03;
    public boolean A04;

    public AbstractC52172aN(int i, int i2, int i3) {
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
    }

    public AbstractC52172aN(Context context) {
        this(context, R.color.link_color);
    }

    public AbstractC52172aN(Context context, int i) {
        this.A01 = AnonymousClass00T.A00(context, i);
        int A00 = AnonymousClass00T.A00(context, i);
        this.A02 = A00;
        this.A00 = C016907y.A06(A00, 72);
    }

    @Override // X.AbstractC116465Vn
    public void AXY(MotionEvent motionEvent, View view) {
        boolean z = true;
        if (motionEvent.getAction() == 1) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.A03 > 1000) {
                this.A03 = elapsedRealtime;
                if (this.A04) {
                    onClick(view);
                }
            }
        }
        if (motionEvent.getAction() != 0) {
            z = false;
        }
        this.A04 = z;
        view.invalidate();
    }

    @Override // android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        if (this.A04) {
            textPaint.setColor(this.A02);
            textPaint.bgColor = this.A00;
            textPaint.setUnderlineText(true);
            return;
        }
        int i = this.A01;
        if (i == 0) {
            i = textPaint.linkColor;
        }
        textPaint.setColor(i);
        textPaint.setUnderlineText(false);
        textPaint.bgColor = 0;
    }
}
