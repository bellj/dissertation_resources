package X;

/* renamed from: X.0df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09880df implements Runnable {
    public final /* synthetic */ RunnableC09890dg A00;
    public final /* synthetic */ AbstractFutureC44231yX A01;
    public final /* synthetic */ String A02;

    public RunnableC09880df(RunnableC09890dg r1, AbstractFutureC44231yX r2, String str) {
        this.A00 = r1;
        this.A02 = str;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        RunnableC09890dg r2 = this.A00;
        r2.A01.A03.remove(this.A02);
        AbstractFutureC44231yX r1 = this.A01;
        if (!r1.isCancelled()) {
            try {
                r1.get();
            } catch (Exception e) {
                r2.A00.A06(e);
            }
        }
    }
}
