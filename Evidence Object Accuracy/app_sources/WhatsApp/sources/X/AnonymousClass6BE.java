package X;

import org.json.JSONObject;

/* renamed from: X.6BE  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BE implements AbstractC16870pt {
    public C14850m9 A00;
    public Integer A01;
    public final C16120oU A02;
    public final C1329668y A03;
    public final AnonymousClass17V A04;

    public AnonymousClass6BE(C16120oU r1, C1329668y r2, AnonymousClass17V r3) {
        this.A02 = r1;
        this.A04 = r3;
        this.A03 = r2;
    }

    public static void A00(AnonymousClass2SP r3, AnonymousClass3FW r4, AnonymousClass6BE r5, boolean z) {
        r3.A06 = Boolean.valueOf(z);
        if (r4 != null) {
            JSONObject jSONObject = r4.A01;
            if (jSONObject.has("is_payment_account_setup")) {
                r3.A02 = Boolean.valueOf(jSONObject.optBoolean("is_payment_account_setup"));
                jSONObject.remove("is_payment_account_setup");
            }
            if (jSONObject.length() > 0) {
                r3.A0X = r4.toString();
            }
        }
        r5.A02.A07(r3);
    }

    public AnonymousClass2SP A01(C452120p r3, int i) {
        int i2;
        AnonymousClass2SP A8H = A8H();
        A8H.A0B = Integer.valueOf(i);
        if (r3 != null) {
            A8H.A0S = String.valueOf(r3.A00);
            A8H.A0T = r3.A09;
            i2 = 2;
        } else {
            i2 = 1;
        }
        A8H.A0C = Integer.valueOf(i2);
        A8H.A09 = C12970iu.A0h();
        return A8H;
    }

    public AnonymousClass2SP A02(Integer num, Integer num2, String str, String str2) {
        return A03(num, num2, str, str2, null, null, false);
    }

    public AnonymousClass2SP A03(Integer num, Integer num2, String str, String str2, String str3, String str4, boolean z) {
        AnonymousClass2SP A8H = A8H();
        A8H.A0Z = str;
        A8H.A07 = Boolean.valueOf(z);
        A8H.A09 = num;
        if (this.A00.A07(1330)) {
            A8H.A0V = str3;
            A8H.A0W = str4;
        }
        if (num2 != null) {
            A8H.A08 = num2;
        }
        if (str2 != null) {
            A8H.A0Y = str2;
        }
        return A8H;
    }

    public void A04(AbstractC28901Pl r3, C452120p r4, int i) {
        String str;
        AnonymousClass2SP A01 = A01(r4, i);
        AnonymousClass1ZY r0 = r3.A08;
        if (r0 == null) {
            str = "";
        } else {
            str = ((C119755f3) r0).A0C;
        }
        A01.A0O = str;
        this.A02.A07(A01);
    }

    public void A05(String str) {
        int i;
        int i2;
        Integer A0V = C12960it.A0V();
        if (str != null) {
            switch (str.hashCode()) {
                case 81882:
                    if (str.equals("SBI")) {
                        i2 = 4;
                        i = Integer.valueOf(i2);
                        this.A01 = i;
                        return;
                    }
                    break;
                case 2023329:
                    if (str.equals("AXIS")) {
                        i2 = 3;
                        i = Integer.valueOf(i2);
                        this.A01 = i;
                        return;
                    }
                    break;
                case 2212537:
                    if (str.equals("HDFC")) {
                        i = 2;
                        this.A01 = i;
                        return;
                    }
                    break;
            }
        }
        this.A01 = A0V;
    }

    @Override // X.AbstractC16870pt
    public AnonymousClass2SP A8H() {
        AnonymousClass2SP r1 = new AnonymousClass2SP();
        r1.A0U = this.A04.A00();
        r1.A0R = "IN";
        A05(this.A03.A07());
        r1.A0F = this.A01;
        return r1;
    }

    @Override // X.AbstractC16870pt
    public void AKa(C452120p r2, int i) {
        AKf(A01(r2, i));
    }

    @Override // X.AbstractC16870pt
    public void AKf(AnonymousClass2SP r2) {
        r2.A0U = this.A04.A00();
        r2.A0R = "IN";
        this.A02.A07(r2);
    }

    @Override // X.AbstractC16870pt
    public void AKg(Integer num, Integer num2, String str, String str2) {
        this.A02.A07(A02(num, num2, str, str2));
    }

    @Override // X.AbstractC16870pt
    public void AKh(Integer num, Integer num2, String str, String str2, boolean z) {
        AnonymousClass2SP A02 = A02(num, num2, str, str2);
        A02.A06 = Boolean.valueOf(z);
        this.A02.A07(A02);
    }

    @Override // X.AbstractC16870pt
    public void AKi(AnonymousClass3FW r11, Integer num, Integer num2, String str, String str2) {
        AKj(r11, num, num2, str, str2, null, null, false, false);
    }

    @Override // X.AbstractC16870pt
    public void AKj(AnonymousClass3FW r11, Integer num, Integer num2, String str, String str2, String str3, String str4, boolean z, boolean z2) {
        A00(A03(num, num2, str, str2, str3, str4, z2), r11, this, z);
    }

    @Override // X.AbstractC16870pt
    public void AeG() {
        this.A04.A01();
    }

    @Override // X.AbstractC16870pt
    public void reset() {
        C117305Zk.A1L(this.A04);
    }
}
