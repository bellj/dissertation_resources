package X;

import com.whatsapp.VideoTimelineView;
import java.io.File;
import java.lang.ref.WeakReference;

/* renamed from: X.38R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38R extends AbstractC16350or {
    public long A00;
    public final float A01;
    public final float A02;
    public final int A03;
    public final File A04;
    public final WeakReference A05;

    public AnonymousClass38R(VideoTimelineView videoTimelineView, File file, float f, float f2, int i) {
        this.A05 = C12970iu.A10(videoTimelineView);
        this.A04 = file;
        this.A03 = i;
        this.A02 = f;
        this.A01 = f2;
    }
}
