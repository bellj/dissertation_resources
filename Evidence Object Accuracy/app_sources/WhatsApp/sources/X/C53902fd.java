package X;

import android.os.Bundle;

/* renamed from: X.2fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53902fd extends AnonymousClass015 {
    public C15580nU A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C22640zP A02;

    public C53902fd(C22640zP r2) {
        this.A02 = r2;
    }

    public void A04() {
        this.A02.A05(this.A00, true);
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogAction", 2);
        A0D.putParcelable("parentGroupJid", this.A00);
        this.A01.A0A(A0D);
    }

    public void A05() {
        this.A02.A05(this.A00, true);
        Bundle A0D = C12970iu.A0D();
        A0D.putInt("dialogAction", 1);
        A0D.putParcelable("parentGroupJid", this.A00);
        this.A01.A0A(A0D);
    }
}
