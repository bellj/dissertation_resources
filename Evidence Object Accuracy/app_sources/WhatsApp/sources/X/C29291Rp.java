package X;

import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.1Rp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29291Rp {
    public final C14830m7 A00;
    public final AnonymousClass1RO A01;

    public C29291Rp(C14830m7 r1, AnonymousClass1RO r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static String A00(String str, boolean z) {
        String str2;
        StringBuilder sb = new StringBuilder("sender_id = ? AND sender_type = ? AND device_id = ? AND group_id");
        if (z) {
            str2 = " = '";
        } else {
            str2 = " != '";
        }
        sb.append(str2);
        sb.append(str);
        sb.append("' AND ");
        sb.append("timestamp");
        sb.append(" < ?");
        return sb.toString();
    }

    public C29301Rq A01(C15980oF r13) {
        StringBuilder sb = new StringBuilder("SenderKeyStore/getSenderKey/");
        sb.append(r13);
        Log.i(sb.toString());
        C16310on A01 = this.A01.get();
        try {
            Cursor A08 = A01.A03.A08("sender_keys", "group_id = ? AND sender_id = ? AND sender_type = ? AND device_id = ?", null, null, new String[]{"record", "timestamp"}, r13.A00());
            if (!A08.moveToNext()) {
                A08.close();
                A01.close();
                return null;
            }
            C29301Rq r0 = new C29301Rq(A08.getBlob(0), A08.getLong(1));
            A08.close();
            A01.close();
            return r0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
