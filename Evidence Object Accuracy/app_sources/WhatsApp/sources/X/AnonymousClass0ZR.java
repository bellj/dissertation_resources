package X;

import android.os.IBinder;

/* renamed from: X.0ZR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZR implements AbstractC11380gC {
    public final IBinder A00;

    public AnonymousClass0ZR(IBinder iBinder) {
        this.A00 = iBinder;
    }

    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass0ZR) && ((AnonymousClass0ZR) obj).A00.equals(this.A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
