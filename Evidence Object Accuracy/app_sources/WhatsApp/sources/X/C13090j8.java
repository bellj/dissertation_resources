package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0j8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13090j8 {
    public int A00;
    public int A01;
    public AbstractC13110jA A02;
    public Set A03;
    public final Set A04 = new HashSet();
    public final Set A05;

    public /* synthetic */ C13090j8(Class cls, Class[] clsArr) {
        HashSet hashSet = new HashSet();
        this.A05 = hashSet;
        this.A00 = 0;
        this.A01 = 0;
        this.A03 = new HashSet();
        hashSet.add(cls);
        for (Class cls2 : clsArr) {
            C13020j0.A02(cls2, "Null interface");
        }
        Collections.addAll(this.A05, clsArr);
    }

    public C13080j7 A00() {
        boolean z = false;
        if (this.A02 != null) {
            z = true;
        }
        C13020j0.A04("Missing required property: factory.", z);
        return new C13080j7(this.A02, new HashSet(this.A05), new HashSet(this.A04), this.A03, this.A00, this.A01);
    }

    public void A01(C13140jD r3) {
        C13020j0.A03("Components are not allowed to depend on interfaces they themselves provide.", !this.A05.contains(r3.A01));
        this.A04.add(r3);
    }
}
