package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.LongSparseArray;
import com.facebook.profilo.core.TraceEvents;
import com.facebook.profilo.logger.BufferLogger;
import com.facebook.profilo.mmapbuf.core.Buffer;
import com.facebook.profilo.writer.NativeTraceWriter;
import java.util.HashSet;
import java.util.Random;

/* renamed from: X.1Sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC29471Sw extends Handler {
    public final C29481Sx A00 = new C29481Sx();
    public final AnonymousClass1Se A01;
    public final AbstractC29391Sf A02;
    public final HashSet A03 = new HashSet();
    public final Random A04 = new Random();

    public HandlerC29471Sw(Looper looper, AnonymousClass1Se r3, AbstractC29391Sf r4) {
        super(looper);
        this.A01 = r3;
        this.A02 = r4;
    }

    public static final void A00(C29441Sr r9) {
        BufferLogger.writeStandardEntry(r9.A09, 6, 98, 0, 0, r9.A08.A00("trace_config.logger_priority", 5), 0, r9.A06);
    }

    public synchronized void A01(C29441Sr r4) {
        HashSet hashSet = this.A03;
        Long valueOf = Long.valueOf(r4.A06);
        if (hashSet.contains(valueOf)) {
            sendMessage(obtainMessage(3, r4));
            hashSet.remove(valueOf);
        }
    }

    public synchronized void A02(C29441Sr r4) {
        HashSet hashSet = this.A03;
        Long valueOf = Long.valueOf(r4.A06);
        if (hashSet.contains(valueOf)) {
            sendMessage(obtainMessage(2, r4));
            hashSet.remove(valueOf);
        }
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        LongSparseArray longSparseArray;
        int i;
        AnonymousClass1SY[] r4;
        AnonymousClass1SY[] r5;
        int tracingProviders;
        int tracingProviders2;
        C90664Ot r3;
        AnonymousClass1SY[] r7;
        AnonymousClass1SY[] r6;
        int i2;
        AnonymousClass1SY[] r62;
        int i3 = message.what;
        if (i3 != 7) {
            C29441Sr r0 = (C29441Sr) message.obj;
            switch (i3) {
                case 0:
                    long j = r0.A06;
                    AnonymousClass1ST r32 = AnonymousClass1ST.A0B;
                    C29441Sr A01 = r32.A01(j);
                    if (A01 != null) {
                        BufferLogger.writeStandardEntry(A01.A09, 6, 41, 0, 0, 0, 0, A01.A06);
                        r32.A04(j, 4);
                        return;
                    }
                    return;
                case 1:
                    if ((r0.A03 & 2) == 0) {
                        r0.A0G.start();
                        A00(r0);
                    }
                    AnonymousClass1Se r72 = this.A01;
                    if (r72 != null) {
                        synchronized (r72) {
                            r62 = r72.A03;
                        }
                        for (AnonymousClass1SY r33 : r62) {
                            if ((r0.A02 & r33.getSupportedProviders()) != 0) {
                                r33.A00().addBuffer(r0.A09);
                                r33.A01();
                                r33.A02(r0);
                                r33.onTraceStarted(r0, r72);
                            }
                        }
                        AnonymousClass1Sj r1 = r72.A05;
                        r1.AUP(r0);
                        r1.AXf(r0);
                        return;
                    }
                    return;
                case 2:
                    synchronized (this) {
                        removeMessages(0, r0);
                        int i4 = r0.A03;
                        if ((i4 & 2) != 0) {
                            if (r0.A0G != null) {
                                r0.A0G.start();
                                A00(r0);
                                NativeTraceWriter nativeTraceWriter = r0.A0G.A02;
                                Buffer buffer = r0.A09;
                                long j2 = r0.A06;
                                BufferLogger.writeAndWakeupTraceWriter(nativeTraceWriter, buffer, j2, 40, 0, i4, j2);
                            } else {
                                throw new IllegalStateException("Trace stopped but never started");
                            }
                        }
                        sendMessageDelayed(obtainMessage(4, r0), (long) r0.A08.A00("trace_config.post_trace_extension_ms", 0));
                    }
                    return;
                case 3:
                    synchronized (this) {
                        removeMessages(0, r0);
                    }
                    AnonymousClass1Se r8 = this.A01;
                    if (r8 != null) {
                        synchronized (r8) {
                            r7 = r8.A03;
                            r6 = r8.A04;
                        }
                        r8.A05.AXc(r0);
                        int i5 = r0.A02;
                        synchronized (TraceEvents.class) {
                            TraceEvents.sProviders = TraceEvents.nativeDisableProviders(i5);
                        }
                        synchronized (r8.A06) {
                            for (AnonymousClass1SY r12 : r6) {
                                r12.A03(r0, r8);
                            }
                        }
                        for (AnonymousClass1SY r13 : r7) {
                            r13.A03(r0, r8);
                        }
                        return;
                    }
                    return;
                case 4:
                    AnonymousClass1Se r34 = this.A01;
                    if (r34 != null) {
                        synchronized (r34) {
                            r4 = r34.A03;
                            r5 = r34.A04;
                        }
                        if (r34.A08) {
                            BufferLogger.writeStandardEntry(r0.A09, 6, 52, 0, 0, 8126470, 0, 0);
                        }
                        int i6 = 0;
                        for (AnonymousClass1SY r2 : r4) {
                            if (r2.A03 == null || r2.A04) {
                                tracingProviders2 = r2.getTracingProviders();
                            } else {
                                tracingProviders2 = 0;
                            }
                            i6 |= tracingProviders2;
                        }
                        for (AnonymousClass1SY r22 : r5) {
                            if (r22.A03 == null || r22.A04) {
                                tracingProviders = r22.getTracingProviders();
                            } else {
                                tracingProviders = 0;
                            }
                            i6 |= tracingProviders;
                        }
                        int i7 = r0.A02;
                        synchronized (TraceEvents.class) {
                            TraceEvents.sProviders = TraceEvents.nativeDisableProviders(i7);
                        }
                        synchronized (r34.A06) {
                            for (AnonymousClass1SY r14 : r5) {
                                r14.A03(r0, r34);
                            }
                        }
                        for (AnonymousClass1SY r15 : r4) {
                            r15.A03(r0, r34);
                        }
                        AnonymousClass1Sj r16 = r34.A05;
                        r16.AUQ(r0, i6);
                        r16.AXg(r0);
                    }
                    BufferLogger.writeStandardEntry(r0.A09, 6, 38, 0, 0, 0, 0, r0.A06);
                    return;
                case 5:
                    long j3 = (long) message.arg1;
                    C29481Sx r63 = this.A00;
                    long j4 = r0.A06;
                    synchronized (r63) {
                        C29491Sy r02 = (C29491Sy) r63.A00.get(j4);
                        if (!(r02 == null || (r3 = r02.A01) == null || j3 <= r3.A00)) {
                            r3.A00 = j3;
                        }
                    }
                    return;
                case 6:
                    C29481Sx r23 = this.A00;
                    long j5 = r0.A06;
                    synchronized (r23) {
                        longSparseArray = r23.A00;
                        C29491Sy r9 = (C29491Sy) longSparseArray.get(j5);
                        if (r9 != null) {
                            C91184Qt r82 = r9.A00;
                            if (r82 != null) {
                                HashSet hashSet = r82.A02;
                                synchronized (hashSet) {
                                    if (!r82.A01.A9h(hashSet, r82.A03)) {
                                        i = r82.A00;
                                        if (i != 1) {
                                        }
                                    }
                                }
                            }
                            C90664Ot r122 = r9.A01;
                            if (r122 != null) {
                                int i8 = 0;
                                i = 0;
                                int i9 = -1;
                                while (true) {
                                    int[] iArr = r122.A01;
                                    if (i8 < iArr.length) {
                                        if (r122.A00 > ((long) iArr[i8]) && iArr[i8] > i9) {
                                            i9 = iArr[i8];
                                            i = r122.A02[i8];
                                        }
                                        i8++;
                                    }
                                }
                            }
                        }
                        i = 1;
                    }
                    if (i == 0 || this.A04.nextInt(i) != 0) {
                        BufferLogger.writeStandardEntry(r0.A09, 6, 37, 0, 0, 0, 0, r0.A06);
                        A01(new C29441Sr(r0, 6));
                    } else {
                        Buffer buffer2 = r0.A09;
                        BufferLogger.writeStandardEntry(buffer2, 6, 99, 0, 0, 0, 0, (long) i);
                        BufferLogger.writeStandardEntry(buffer2, 6, 61, 0, 0, 0, 0, r0.A06);
                        A02(r0);
                    }
                    synchronized (r23) {
                        longSparseArray.delete(r0.A06);
                    }
                    return;
                case 7:
                    break;
                default:
                    return;
            }
        }
        throw new NullPointerException("mTraceContext");
    }
}
