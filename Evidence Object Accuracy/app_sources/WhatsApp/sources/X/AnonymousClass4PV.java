package X;

import android.content.Context;
import java.util.concurrent.Executor;

/* renamed from: X.4PV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4PV {
    public final Context A00;
    public final C64903Hj A01;
    public final Executor A02;

    public AnonymousClass4PV(Context context, C64903Hj r2, Executor executor) {
        this.A02 = executor;
        this.A00 = context;
        this.A01 = r2;
    }
}
