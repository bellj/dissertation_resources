package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AL  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AL extends Enum {
    public static final /* synthetic */ AnonymousClass4AL[] A00;
    public static final AnonymousClass4AL A01;
    public static final AnonymousClass4AL A02;
    public static final AnonymousClass4AL A03;

    public static AnonymousClass4AL valueOf(String str) {
        return (AnonymousClass4AL) Enum.valueOf(AnonymousClass4AL.class, str);
    }

    public static AnonymousClass4AL[] values() {
        return (AnonymousClass4AL[]) A00.clone();
    }

    static {
        AnonymousClass4AL r4 = new AnonymousClass4AL("NORMAL", 0);
        A03 = r4;
        AnonymousClass4AL r3 = new AnonymousClass4AL("DESTRUCTIVE", 1);
        A01 = r3;
        AnonymousClass4AL r1 = new AnonymousClass4AL("MEDIA", 2);
        A02 = r1;
        AnonymousClass4AL[] r0 = new AnonymousClass4AL[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4AL(String str, int i) {
    }
}
