package X;

import android.os.Handler;
import androidx.preference.Preference;
import androidx.preference.PreferenceGroup;

/* renamed from: X.0Yw  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yw implements AbstractC11830gw {
    public final /* synthetic */ PreferenceGroup A00;
    public final /* synthetic */ AnonymousClass0F1 A01;

    public AnonymousClass0Yw(PreferenceGroup preferenceGroup, AnonymousClass0F1 r2) {
        this.A01 = r2;
        this.A00 = preferenceGroup;
    }

    @Override // X.AbstractC11830gw
    public boolean AU5(Preference preference) {
        this.A00.A01 = Integer.MAX_VALUE;
        AnonymousClass0F1 r0 = this.A01;
        Handler handler = r0.A00;
        Runnable runnable = r0.A02;
        handler.removeCallbacks(runnable);
        handler.post(runnable);
        return true;
    }
}
