package X;

import android.util.Base64;

/* renamed from: X.1W0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1W0 {
    public final byte[] A00;
    public final byte[] A01;
    public final byte[] A02;

    public AnonymousClass1W0(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        this.A01 = bArr;
        this.A00 = bArr2;
        this.A02 = bArr3;
    }

    public String A00() {
        StringBuilder sb = new StringBuilder("AesKey=");
        sb.append(Base64.encodeToString(this.A01, 2));
        sb.append(";IV=");
        sb.append(Base64.encodeToString(this.A02, 2));
        sb.append(";Data=");
        sb.append(Base64.encodeToString(this.A00, 2));
        return sb.toString();
    }
}
