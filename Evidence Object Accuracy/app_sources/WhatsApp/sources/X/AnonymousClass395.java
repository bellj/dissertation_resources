package X;

import android.graphics.Bitmap;
import android.os.Process;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.395  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass395 extends AnonymousClass1MS {
    public final /* synthetic */ AbstractC38761of A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass395(AbstractC38761of r2, String str) {
        super(C12960it.A0d(str, C12960it.A0k("PhotosDisk-")));
        this.A00 = r2;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Process.setThreadPriority(10);
        do {
            try {
                AbstractC38761of r4 = this.A00;
                Stack stack = r4.A0A;
                synchronized (stack) {
                    if (stack.size() == 0) {
                        stack.wait();
                    }
                }
                if (stack.size() != 0) {
                    AnonymousClass3BX r8 = null;
                    Object obj = r4.A05;
                    synchronized (obj) {
                        if (stack.size() != 0) {
                            r8 = (AnonymousClass3BX) stack.pop();
                        }
                    }
                    if (r8 != null) {
                        ConcurrentMap concurrentMap = r8.A05;
                        if (concurrentMap.size() != 0) {
                            String str = r8.A03;
                            String A01 = C003501n.A01(str);
                            AnonymousClass009.A05(A01);
                            C64843Hc r2 = r4.A03;
                            Bitmap A00 = r2.A00(A01, r8.A01, r8.A00);
                            if (A00 == null) {
                                synchronized (obj) {
                                    for (AnonymousClass5XB r1 : concurrentMap.values()) {
                                        if (r1.A9o()) {
                                            if (r1.getId().equals(str)) {
                                                concurrentMap.remove(r1);
                                            }
                                            r4.A02.A0H(new RunnableBRunnable0Shape0S0300000_I0(null, Collections.singletonList(r1), r4, 15));
                                        }
                                    }
                                    if (!r8.A06.get() && concurrentMap.size() != 0) {
                                        Stack stack2 = r4.A0B;
                                        stack2.remove(r8);
                                        stack2.push(r8);
                                        r8.A02 = new C43161wW(C14370lK.A0B);
                                        synchronized (stack2) {
                                            stack2.notify();
                                        }
                                    }
                                }
                            } else {
                                C006202y r12 = r2.A02;
                                synchronized (r12) {
                                    r12.A08(str, A00);
                                    r12.A01();
                                    r12.A00();
                                }
                                synchronized (obj) {
                                    r4.A09.remove(str);
                                    if (concurrentMap.size() != 0) {
                                        ArrayList A0x = C12980iv.A0x(concurrentMap.values());
                                        concurrentMap.clear();
                                        r4.A02.A0H(new RunnableBRunnable0Shape0S0300000_I0(A00, A0x, r4, 15));
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (InterruptedException unused) {
                return;
            }
        } while (!Thread.interrupted());
    }
}
