package X;

/* renamed from: X.4cW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94774cW {
    public static final C94774cW A01 = new C94774cW(33023);
    public int A00;

    public C94774cW(int i) {
        this.A00 = i;
    }

    public C94774cW(C114845Nh r8) {
        byte[] bArr = r8.A01;
        int min = Math.min(4, bArr.length - 1);
        int i = 0;
        for (int i2 = 0; i2 < min; i2++) {
            i |= (255 & bArr[i2]) << (i2 << 3);
        }
        if (min >= 0 && min < 4) {
            i |= (((byte) (bArr[min] & (255 << r8.A00))) & 255) << (min << 3);
        }
        this.A00 = i;
    }
}
