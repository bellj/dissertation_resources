package X;

import com.whatsapp.jid.Jid;
import java.util.List;

/* renamed from: X.2PD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2PD extends AnonymousClass2PA {
    public final List A00;

    public AnonymousClass2PD(Jid jid, String str, List list, long j) {
        super(jid, str, j);
        this.A00 = list;
    }
}
