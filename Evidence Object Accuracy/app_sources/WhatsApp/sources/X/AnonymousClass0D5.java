package X;

import android.app.ActivityOptions;
import android.os.Bundle;

/* renamed from: X.0D5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0D5 extends C018108l {
    public final ActivityOptions A00;

    public AnonymousClass0D5(ActivityOptions activityOptions) {
        this.A00 = activityOptions;
    }

    @Override // X.C018108l
    public Bundle A03() {
        return this.A00.toBundle();
    }
}
