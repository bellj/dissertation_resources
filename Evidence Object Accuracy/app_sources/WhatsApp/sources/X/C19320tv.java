package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.0tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19320tv {
    public final Set A00;

    public C19320tv(C18760sy r3, C19150te r4, C19020tR r5, C19080tX r6, C19290ts r7, C19000tP r8, C19030tS r9, C19160tf r10, C19040tT r11, C19180th r12, C19190ti r13, C19130tc r14, C19110ta r15, C19090tY r16, C19270tq r17, C19260tp r18, C19280tr r19, C18690sr r20, C19170tg r21, C19220tl r22, C19100tZ r23, C19050tU r24, C19140td r25, C19200tj r26, C19120tb r27, C19060tV r28, C19210tk r29, C19070tW r30, C18490sX r31, AnonymousClass0t2 r32, C19310tu r33, C19230tm r34, C19300tt r35, C19240tn r36) {
        HashSet hashSet = new HashSet();
        hashSet.add(r8);
        hashSet.add(r5);
        hashSet.add(r9);
        hashSet.add(r11);
        hashSet.add(r24);
        hashSet.add(r28);
        hashSet.add(r30);
        hashSet.add(r5);
        hashSet.add(r6);
        hashSet.add(r16);
        hashSet.add(r23);
        hashSet.add(r15);
        hashSet.add(r27);
        hashSet.add(r14);
        hashSet.add(r25);
        hashSet.add(r4);
        hashSet.add(r10);
        hashSet.add(r21);
        hashSet.add(r20);
        hashSet.add(r12);
        hashSet.add(r13);
        hashSet.add(r26);
        hashSet.add(r29);
        hashSet.add(r22);
        hashSet.add(r34);
        hashSet.add(r32);
        hashSet.add(r31);
        hashSet.add(r3);
        hashSet.add(r36);
        hashSet.add(r18);
        hashSet.add(r17);
        hashSet.add(r19);
        hashSet.add(r7);
        hashSet.add(r35);
        hashSet.add(r33);
        this.A00 = Collections.unmodifiableSet(hashSet);
    }
}
