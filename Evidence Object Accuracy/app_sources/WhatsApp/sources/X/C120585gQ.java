package X;

import android.content.Context;

/* renamed from: X.5gQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120585gQ extends C120895gv {
    public final /* synthetic */ C120435gB A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120585gQ(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120435gB r11) {
        super(context, r8, r9, r10, "upi-get-psp-routing-and-list-keys");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        AbstractC136216Lr r0 = this.A00.A00;
        if (r0 != null) {
            r0.AUR(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        AbstractC136216Lr r0 = this.A00.A00;
        if (r0 != null) {
            r0.AUR(r2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002d, code lost:
        if (r2 == null) goto L_0x002f;
     */
    @Override // X.C120895gv, X.AbstractC451020e
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.AnonymousClass1V8 r9) {
        /*
            r8 = this;
            super.A04(r9)
            X.5gB r5 = r8.A00
            X.0qD r0 = r5.A08
            X.0pp r0 = r0.A02()
            X.1xB r1 = r0.AEz()
            X.AnonymousClass009.A05(r1)
            X.102 r0 = r5.A03
            java.util.ArrayList r0 = r1.AYv(r0, r9)
            X.60e r1 = r5.A05
            X.3Fv r4 = r5.A00
            X.5uG r0 = r1.A03(r4, r0)
            java.util.ArrayList r6 = r0.A02
            X.5ez r2 = r0.A00
            if (r6 == 0) goto L_0x002f
            int r0 = r6.size()
            if (r0 <= 0) goto L_0x002f
            r0 = 1
            if (r2 != 0) goto L_0x0030
        L_0x002f:
            r0 = 0
        L_0x0030:
            java.lang.String r3 = "upi-get-psp-routing-and-list-keys"
            if (r0 == 0) goto L_0x0093
            r1.A06 = r6
            r1.A03 = r2
            java.lang.String r0 = "PAY: IndiaUpiPaymentSetup setPspAndBanksList pspConfig: "
            java.lang.String r0 = X.C12960it.A0b(r0, r6)
            com.whatsapp.util.Log.i(r0)
            java.lang.String r0 = "PAY: IndiaUpiPaymentSetup setPspAndBanksList pspRouting: "
            java.lang.String r0 = X.C12960it.A0b(r0, r2)
            com.whatsapp.util.Log.i(r0)
            r4.A05(r3)
            X.6Lr r7 = r5.A00
            if (r7 == 0) goto L_0x0071
            X.5iP r7 = (X.AbstractActivityC121505iP) r7
            if (r6 == 0) goto L_0x007f
            int r0 = r6.size()
            if (r0 <= 0) goto L_0x007f
            if (r2 == 0) goto L_0x007f
            r6 = 0
            X.0lR r5 = r7.A05
            X.0qD r2 = r7.A0P
            X.5ru r0 = new X.5ru
            r0.<init>(r7)
            X.5oT r1 = new X.5oT
            r1.<init>(r7, r2, r0)
            X.00n[] r0 = new X.AbstractC001200n[r6]
            r5.Aaz(r1, r0)
        L_0x0071:
            java.util.ArrayList r0 = r4.A05
            boolean r0 = r0.contains(r3)
            if (r0 != 0) goto L_0x007e
            r0 = 500(0x1f4, float:7.0E-43)
            r4.A06(r3, r0)
        L_0x007e:
            return
        L_0x007f:
            X.1Zj r2 = r7.A0R
            java.lang.String r0 = "onPspRoutingAndListKeys error. showGenericError error: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            X.3Fv r0 = r7.A01
            X.C117315Zl.A0V(r0, r3, r1)
            X.C117295Zj.A1F(r2, r1)
            r7.A31()
            goto L_0x0071
        L_0x0093:
            java.lang.String r0 = "PAY: received invalid data from upi-get-psp-routing-and-list-keys: psps: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r6)
            java.lang.String r0 = " pspRouting: "
            java.lang.String r0 = X.C12960it.A0Z(r2, r0, r1)
            com.whatsapp.util.Log.w(r0)
            X.6Lr r1 = r5.A00
            if (r1 == 0) goto L_0x0071
            X.20p r0 = X.C117305Zk.A0L()
            r1.AUR(r0)
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120585gQ.A04(X.1V8):void");
    }
}
