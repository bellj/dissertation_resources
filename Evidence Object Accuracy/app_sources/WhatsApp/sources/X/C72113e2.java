package X;

import com.whatsapp.avatar.home.AvatarHomeViewModel;

/* renamed from: X.3e2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72113e2 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AvatarHomeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72113e2(AvatarHomeViewModel avatarHomeViewModel) {
        super(1);
        this.this$0 = avatarHomeViewModel;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C16700pc.A0E(obj, 0);
        this.this$0.A07.A0B(obj);
        return AnonymousClass1WZ.A00;
    }
}
