package X;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.Surface;
import android.view.TextureView;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.2CO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CO implements TextureView.SurfaceTextureListener {
    public final /* synthetic */ AnonymousClass2CP A00;

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public AnonymousClass2CO(AnonymousClass2CP r1) {
        this.A00 = r1;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        AnonymousClass2CP r3 = this.A00;
        if (r3.A09 != null) {
            Surface surface = new Surface(surfaceTexture);
            r3.A0A = surface;
            r3.A09.setSurface(surface);
            if (r3.A00 == 0) {
                try {
                    r3.A09.setDataSource(r3.A0B);
                    r3.A09.prepareAsync();
                    r3.A00 = 1;
                } catch (IOException e) {
                    r3.A00 = -1;
                    r3.A03 = -1;
                    if (r3.A07 != null) {
                        r3.post(new RunnableBRunnable0Shape13S0100000_I0_13(this, 22));
                    }
                    Log.e("mediaview/unable-to-play", e);
                }
            }
        }
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        AnonymousClass2CP r2 = this.A00;
        MediaPlayer mediaPlayer = r2.A09;
        if (mediaPlayer != null) {
            mediaPlayer.setSurface(null);
        }
        Surface surface = r2.A0A;
        if (surface != null) {
            surface.release();
            r2.A0A = null;
        }
        r2.A0H = false;
        return false;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        AnonymousClass2CP r6 = this.A00;
        if (!r6.A0H) {
            boolean z = false;
            if (surfaceTexture.getTimestamp() > 0) {
                z = true;
            }
            r6.A0H = z;
        }
    }
}
