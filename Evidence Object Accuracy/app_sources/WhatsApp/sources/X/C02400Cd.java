package X;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import androidx.appcompat.widget.AppCompatSpinner;

/* renamed from: X.0Cd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02400Cd extends AnonymousClass0XR implements AbstractC12740iQ {
    public int A00;
    public ListAdapter A01;
    public CharSequence A02;
    public final Rect A03 = new Rect();
    public final /* synthetic */ AppCompatSpinner A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C02400Cd(Context context, AttributeSet attributeSet, AppCompatSpinner appCompatSpinner, int i) {
        super(context, attributeSet, i, 0);
        this.A04 = appCompatSpinner;
        this.A0A = appCompatSpinner;
        this.A0H = true;
        this.A0D.setFocusable(true);
        this.A0B = new C07140Wv(this, appCompatSpinner);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02() {
        /*
            r9 = this;
            android.widget.PopupWindow r3 = r9.A0D
            android.graphics.drawable.Drawable r0 = r3.getBackground()
            r2 = 0
            androidx.appcompat.widget.AppCompatSpinner r4 = r9.A04
            android.graphics.Rect r6 = r4.A05
            if (r0 == 0) goto L_0x007d
            r0.getPadding(r6)
            int r1 = X.AnonymousClass028.A05(r4)
            r0 = 1
            if (r1 != r0) goto L_0x0079
            int r2 = r6.right
        L_0x0019:
            int r8 = r4.getPaddingLeft()
            int r7 = r4.getPaddingRight()
            int r5 = r4.getWidth()
            int r1 = r4.A00
            r0 = -2
            if (r1 != r0) goto L_0x006e
            android.widget.ListAdapter r1 = r9.A01
            android.widget.SpinnerAdapter r1 = (android.widget.SpinnerAdapter) r1
            android.graphics.drawable.Drawable r0 = r3.getBackground()
            int r3 = r4.A00(r0, r1)
            android.content.Context r0 = r4.getContext()
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r1 = r0.widthPixels
            int r0 = r6.left
            int r1 = r1 - r0
            int r0 = r6.right
            int r1 = r1 - r0
            if (r3 <= r1) goto L_0x004d
            r3 = r1
        L_0x004d:
            int r0 = r5 - r8
            int r0 = r0 - r7
            int r0 = java.lang.Math.max(r3, r0)
        L_0x0054:
            r9.A01(r0)
        L_0x0057:
            int r1 = X.AnonymousClass028.A05(r4)
            r0 = 1
            if (r1 != r0) goto L_0x0069
            int r5 = r5 - r7
            int r0 = r9.A04
            int r5 = r5 - r0
            int r0 = r9.A00
            int r5 = r5 - r0
            int r2 = r2 + r5
        L_0x0066:
            r9.A02 = r2
            return
        L_0x0069:
            int r0 = r9.A00
            int r8 = r8 + r0
            int r2 = r2 + r8
            goto L_0x0066
        L_0x006e:
            r0 = -1
            if (r1 != r0) goto L_0x0075
            int r0 = r5 - r8
            int r0 = r0 - r7
            goto L_0x0054
        L_0x0075:
            r9.A01(r1)
            goto L_0x0057
        L_0x0079:
            int r0 = r6.left
            int r2 = -r0
            goto L_0x0019
        L_0x007d:
            r6.right = r2
            r6.left = r2
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C02400Cd.A02():void");
    }

    @Override // X.AbstractC12740iQ
    public CharSequence ADM() {
        return this.A02;
    }

    @Override // X.AnonymousClass0XR, X.AbstractC12740iQ
    public void Abi(ListAdapter listAdapter) {
        super.Abi(listAdapter);
        this.A01 = listAdapter;
    }

    @Override // X.AbstractC12740iQ
    public void AcD(int i) {
        this.A00 = i;
    }

    @Override // X.AbstractC12740iQ
    public void Ace(CharSequence charSequence) {
        this.A02 = charSequence;
    }

    @Override // X.AbstractC12740iQ
    public void Adf(int i, int i2) {
        ViewTreeObserver viewTreeObserver;
        PopupWindow popupWindow = this.A0D;
        boolean isShowing = popupWindow.isShowing();
        A02();
        popupWindow.setInputMethodMode(2);
        super.Ade();
        C02360Bs r2 = this.A0E;
        r2.setChoiceMode(1);
        if (Build.VERSION.SDK_INT >= 17) {
            r2.setTextDirection(i);
            r2.setTextAlignment(i2);
        }
        AppCompatSpinner appCompatSpinner = this.A04;
        int selectedItemPosition = appCompatSpinner.getSelectedItemPosition();
        C02360Bs r1 = this.A0E;
        if (popupWindow.isShowing() && r1 != null) {
            r1.A0B = false;
            r1.setSelection(selectedItemPosition);
            if (r1.getChoiceMode() != 0) {
                r1.setItemChecked(selectedItemPosition, true);
            }
        }
        if (!isShowing && (viewTreeObserver = appCompatSpinner.getViewTreeObserver()) != null) {
            ViewTreeObserver$OnGlobalLayoutListenerC06940Wb r12 = new ViewTreeObserver$OnGlobalLayoutListenerC06940Wb(this);
            viewTreeObserver.addOnGlobalLayoutListener(r12);
            popupWindow.setOnDismissListener(new AnonymousClass0X7(r12, this));
        }
    }
}
