package X;

import android.net.Uri;
import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3UO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UO implements AnonymousClass24S {
    public boolean A00 = false;
    public final ArrayList A01 = C12960it.A0l();
    public final ArrayList A02 = C12960it.A0l();
    public final /* synthetic */ ContactPickerFragment A03;
    public final /* synthetic */ List A04;

    public AnonymousClass3UO(ContactPickerFragment contactPickerFragment, List list) {
        this.A03 = contactPickerFragment;
        this.A04 = list;
    }

    public final void A00() {
        ArrayList arrayList = this.A02;
        int size = arrayList.size() + this.A01.size();
        ContactPickerFragment contactPickerFragment = this.A03;
        if (size == contactPickerFragment.A20.size()) {
            if (!arrayList.isEmpty() && !this.A00) {
                AnonymousClass3UA r0 = contactPickerFragment.A0m;
                r0.A00.A2a(this.A04);
            }
            contactPickerFragment.A0m.A00();
        }
    }

    @Override // X.AnonymousClass24S
    public void AQ8() {
        this.A00 = true;
    }

    @Override // X.AnonymousClass24S
    public void AY7(Uri uri) {
        this.A01.add(uri);
        A00();
    }

    @Override // X.AnonymousClass24S
    public void AY8(Uri uri) {
        this.A02.add(uri);
        A00();
    }
}
