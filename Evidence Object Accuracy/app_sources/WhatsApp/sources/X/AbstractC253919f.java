package X;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.19f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC253919f {
    public long A00 = -1;
    public WeakReference A01;
    public final C18790t3 A02;
    public final C14830m7 A03;
    public final AnonymousClass018 A04;
    public final C20250vS A05;
    public final AnonymousClass197 A06;
    public final C16120oU A07;
    public final C253719d A08;
    public final C19930uu A09;
    public final AbstractC14440lR A0A;
    public final Random A0B;

    public AbstractC253919f(C18790t3 r3, C14830m7 r4, AnonymousClass018 r5, C20250vS r6, AnonymousClass197 r7, C16120oU r8, C253719d r9, C19930uu r10, AbstractC14440lR r11) {
        this.A03 = r4;
        this.A05 = r6;
        this.A08 = r9;
        this.A09 = r10;
        this.A0A = r11;
        this.A02 = r3;
        this.A07 = r8;
        this.A04 = r5;
        this.A06 = r7;
        this.A0B = new Random();
    }

    public final AnonymousClass3EY A00() {
        AnonymousClass3EY r3;
        AnonymousClass3EY r32;
        AnonymousClass009.A01();
        WeakReference weakReference = this.A01;
        if (weakReference != null && (r32 = (AnonymousClass3EY) weakReference.get()) != null && this.A03.A00() - this.A00 < TimeUnit.HOURS.toMillis(4) && !r32.A02) {
            return r32;
        }
        if (!(this instanceof C254019g)) {
            r3 = new AnonymousClass324((C253819e) this);
        } else {
            r3 = new AnonymousClass325((C254019g) this);
        }
        this.A01 = new WeakReference(r3);
        this.A00 = this.A03.A00();
        return r3;
    }

    public final HttpsURLConnection A01(String str) {
        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL(str).openConnection();
        httpsURLConnection.setRequestProperty("User-Agent", this.A09.A00());
        httpsURLConnection.setConnectTimeout(15000);
        httpsURLConnection.setReadTimeout(30000);
        httpsURLConnection.setRequestMethod("GET");
        httpsURLConnection.connect();
        return httpsURLConnection;
    }
}
