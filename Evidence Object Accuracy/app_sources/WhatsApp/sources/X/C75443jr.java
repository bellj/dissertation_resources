package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.3jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75443jr extends AnonymousClass03U {
    public TextView A00;
    public final /* synthetic */ AnonymousClass1KX A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C75443jr(View view, AnonymousClass1KX r3) {
        super(view);
        this.A01 = r3;
        this.A00 = C12960it.A0J(view, R.id.section_title);
    }
}
