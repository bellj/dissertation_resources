package X;

import android.os.Bundle;

/* renamed from: X.1At  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25771At {
    public final AnonymousClass19Y A00;
    public final C18640sm A01;
    public final AnonymousClass01d A02;
    public final C15890o4 A03;
    public final AnonymousClass11G A04;
    public final C20800wL A05;
    public final AbstractC14440lR A06;

    public C25771At(AnonymousClass19Y r1, C18640sm r2, AnonymousClass01d r3, C15890o4 r4, AnonymousClass11G r5, C20800wL r6, AbstractC14440lR r7) {
        this.A06 = r7;
        this.A00 = r1;
        this.A02 = r3;
        this.A04 = r5;
        this.A03 = r4;
        this.A05 = r6;
        this.A01 = r2;
    }

    public void A00(Bundle bundle, ActivityC13810kN r18, String str, boolean z) {
        AbstractC14440lR r1 = this.A06;
        AnonymousClass19Y r5 = this.A00;
        AnonymousClass01d r7 = this.A02;
        AnonymousClass11G r10 = this.A04;
        r1.Aaz(new C627238i(bundle, r18, r5, this.A01, r7, this.A03, null, r10, this.A05, str, false, false, z), new String[0]);
    }
}
