package X;

import android.content.Context;
import com.whatsapp.account.delete.DeleteAccountFeedback;

/* renamed from: X.4pg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102774pg implements AbstractC009204q {
    public final /* synthetic */ DeleteAccountFeedback A00;

    public C102774pg(DeleteAccountFeedback deleteAccountFeedback) {
        this.A00 = deleteAccountFeedback;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
