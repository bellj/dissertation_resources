package X;

/* renamed from: X.1sD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40721sD extends C32411c7 {
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (r7 != null) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C40711sC A00(X.C15570nT r6, X.AbstractC14640lm r7, X.AnonymousClass1G8 r8, X.AnonymousClass1IS r9) {
        /*
            java.lang.String r0 = r8.A02
            com.whatsapp.jid.UserJid r5 = com.whatsapp.jid.UserJid.getNullable(r0)
            boolean r0 = r9.A02
            if (r0 != 0) goto L_0x0035
            X.0lm r4 = r9.A00
            boolean r3 = X.C15380n4.A0J(r4)
            boolean r0 = r8.A04
            r2 = 0
            r1 = 0
            if (r0 == 0) goto L_0x0028
            if (r3 == 0) goto L_0x0033
            if (r7 == 0) goto L_0x0033
        L_0x001a:
            r5 = r7
        L_0x001b:
            java.lang.String r0 = r8.A01
            X.1IS r1 = new X.1IS
            r1.<init>(r4, r0, r2)
            X.1sC r0 = new X.1sC
            r0.<init>(r5, r1)
            return r0
        L_0x0028:
            boolean r0 = r6.A0F(r5)
            if (r0 != 0) goto L_0x0032
            if (r3 == 0) goto L_0x0032
            r7 = r5
            goto L_0x001a
        L_0x0032:
            r2 = 1
        L_0x0033:
            r7 = r1
            goto L_0x001a
        L_0x0035:
            boolean r2 = r8.A04
            X.0lm r4 = r9.A00
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C40721sD.A00(X.0nT, X.0lm, X.1G8, X.1IS):X.1sC");
    }

    public static C40711sC A01(AbstractC15340mz r5) {
        if (!r5.A0r) {
            return null;
        }
        AnonymousClass1IS r4 = r5.A0z;
        return new C40711sC(r4.A00, new AnonymousClass1IS(r5.A0B(), r4.A01, r4.A02));
    }

    public static void A02(AbstractC14640lm r2, AnonymousClass1G9 r3, AnonymousClass1IS r4) {
        AbstractC14640lm r1 = r4.A00;
        AnonymousClass009.A05(r1);
        r3.A07(r1.getRawString());
        r3.A05(r4.A01);
        boolean z = r4.A02;
        r3.A08(z);
        if (r2 == null) {
            return;
        }
        if ((!z && C15380n4.A0J(r1)) || C15380n4.A0F(r1)) {
            r3.A06(r2.getRawString());
        }
    }
}
