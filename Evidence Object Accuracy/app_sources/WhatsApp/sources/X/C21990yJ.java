package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/* renamed from: X.0yJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21990yJ {
    public final AnonymousClass016 A00 = new AnonymousClass016();
    public final C18640sm A01;
    public final C21980yI A02;
    public final C21970yH A03;
    public final ExecutorC27271Gr A04;

    public C21990yJ(C18640sm r3, C21980yI r4, C21970yH r5, AbstractC14440lR r6) {
        this.A04 = new ExecutorC27271Gr(r6, false);
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
    }

    public static Bitmap A00(Context context, File file) {
        Point A00 = AbstractC15850o0.A00(context);
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                Bitmap bitmap = C37501mV.A05(AbstractC15850o0.A01(A00, true), fileInputStream).A02;
                fileInputStream.close();
                return bitmap;
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IOException | OutOfMemoryError e) {
            Log.e("DownloadableWallpaperManager/error when loading wallpaper resource", e);
            return null;
        }
    }
}
