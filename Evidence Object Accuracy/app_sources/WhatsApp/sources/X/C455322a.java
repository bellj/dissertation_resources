package X;

import android.util.Pair;
import java.util.concurrent.Executor;

/* renamed from: X.22a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C455322a implements AbstractC28771Oy {
    public final C14620lj A00 = new C14620lj();
    public final C14620lj A01 = new C14620lj();
    public final C14620lj A02 = new C14620lj();
    public final Executor A03;

    public C455322a(Executor executor) {
        this.A03 = executor;
    }

    @Override // X.AbstractC28771Oy
    public void APN(long j) {
        this.A00.A04(Long.valueOf(j));
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        this.A01.A04(Boolean.valueOf(z));
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r3, C28781Oz r4) {
        this.A02.A04(Pair.create(r3, r4));
    }
}
