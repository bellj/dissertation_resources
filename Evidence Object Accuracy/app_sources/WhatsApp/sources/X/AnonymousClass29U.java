package X;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Dialog;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.ConditionVariable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.backup.google.BaseNewUserSetupActivity$AuthRequestDialogFragment;
import com.whatsapp.backup.google.PromptDialogFragment;
import com.whatsapp.backup.google.SingleChoiceListDialogFragment;
import com.whatsapp.backup.google.viewmodel.GoogleDriveNewUserSetupViewModel;
import com.whatsapp.settings.SettingsChat;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.chromium.net.UrlRequest;

/* renamed from: X.29U  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass29U extends AnonymousClass29V implements AnonymousClass29H, AnonymousClass29I {
    public View A00;
    public View A01;
    public View A02;
    public View A03;
    public View A04;
    public ImageView A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public SwitchCompat A09;
    public SwitchCompat A0A;
    public TextEmojiLabel A0B;
    public WaTextView A0C;
    public WaTextView A0D;
    public C15820nx A0E;
    public C22730zY A0F;
    public C26551Dx A0G;
    public AnonymousClass10J A0H;
    public AnonymousClass10K A0I;
    public AnonymousClass1D6 A0J;
    public GoogleDriveNewUserSetupViewModel A0K;
    public AnonymousClass116 A0L;
    public C17050qB A0M;
    public C16590pI A0N;
    public C15890o4 A0O;
    public C18260sA A0P;
    public C16120oU A0Q;
    public C20660w7 A0R;
    public AnonymousClass12U A0S;
    public C21710xr A0T;
    public boolean A0U;
    public String[] A0V;
    public final ServiceConnection A0W = new AnonymousClass3LF(this);
    public final ConditionVariable A0X = new ConditionVariable(false);
    public final ConditionVariable A0Y = new ConditionVariable(false);
    public final AtomicBoolean A0Z = new AtomicBoolean();
    public volatile boolean A0a;

    public static /* synthetic */ void A02(AccountManagerFuture accountManagerFuture, BaseNewUserSetupActivity$AuthRequestDialogFragment baseNewUserSetupActivity$AuthRequestDialogFragment, AnonymousClass29U r5) {
        try {
            Log.i("settings-gdrive/show-accounts/waiting-for-add-account-activity-to-return");
            Bundle bundle = (Bundle) accountManagerFuture.getResult();
            if (!bundle.containsKey("authAccount")) {
                Log.e("settings-gdrive/error-during-add-account/account-manager-returned-with-no-account-name");
            } else {
                r5.A2j(baseNewUserSetupActivity$AuthRequestDialogFragment, String.valueOf(bundle.get("authAccount")));
            }
        } catch (AuthenticatorException | IOException e) {
            Log.e("settings-gdrive/error-during-add-account", e);
        } catch (OperationCanceledException e2) {
            Log.i("settings-gdrive/user-canceled-add-account-operation", e2);
        }
    }

    public static /* synthetic */ void A03(View view, AnonymousClass29U r6) {
        if (view == r6.A00) {
            r6.A2f();
        } else if (view == r6.A04) {
            Log.i("settings-gdrive/toggle-network-pref");
            ((ActivityC13830kP) r6).A05.Ab2(new RunnableBRunnable0Shape0S0101000_I0(r6, r6.A0A.isChecked() ? 1 : 0, 3));
        } else if (view == r6.A02) {
            Log.i("settings-gdrive/show-freq-pref");
            SingleChoiceListDialogFragment singleChoiceListDialogFragment = new SingleChoiceListDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("dialog_id", 10);
            bundle.putString("title", r6.getString(R.string.settings_gdrive_backup_options_title));
            bundle.putStringArray("items", r6.A0V);
            int A01 = ((ActivityC13810kN) r6).A09.A01();
            int i = 0;
            while (true) {
                int[] iArr = GoogleDriveNewUserSetupViewModel.A07;
                if (i < iArr.length) {
                    if (iArr[i] == A01) {
                        break;
                    }
                    i++;
                } else {
                    StringBuilder sb = new StringBuilder("settings-gdrive/get-backup-freq-index/");
                    sb.append(A01);
                    Log.e(sb.toString());
                    i = 0;
                    break;
                }
            }
            bundle.putInt("selected_item_index", i);
            singleChoiceListDialogFragment.A0U(bundle);
            if (!C36021jC.A03(r6) && !r6.A0U) {
                C004902f r1 = new C004902f(r6.A0V());
                r1.A09(singleChoiceListDialogFragment, null);
                r1.A02();
            }
        } else if (view != r6.A03) {
            throw new IllegalArgumentException("Can't handle the click event for the pref view");
        } else if (C44771zW.A0G(((ActivityC13810kN) r6).A09)) {
            r6.Ado(R.string.settings_gdrive_please_wait_for_backup_to_finish_before_change);
        } else {
            if (C44771zW.A0H(((ActivityC13810kN) r6).A09)) {
                r6.Ado(R.string.settings_gdrive_please_wait_for_media_restore_to_finish_before_account_change);
            } else {
                r6.A09.toggle();
            }
            GoogleDriveNewUserSetupViewModel googleDriveNewUserSetupViewModel = r6.A0K;
            boolean isChecked = r6.A09.isChecked();
            googleDriveNewUserSetupViewModel.A02.A0B(Boolean.valueOf(isChecked));
            googleDriveNewUserSetupViewModel.A05.A11(isChecked);
        }
    }

    public void A2e() {
        this.A0K.A04(0);
    }

    public void A2f() {
        int i;
        AnonymousClass009.A01();
        if (!C36021jC.A03(this) && !this.A0U) {
            if (C44771zW.A0G(((ActivityC13810kN) this).A09)) {
                Log.i("settings-gdrive/account-selector/backup/running");
                i = R.string.settings_gdrive_please_wait_for_backup_to_finish_before_account_change;
            } else if (C44771zW.A0H(((ActivityC13810kN) this).A09)) {
                Log.i("settings-gdrive/account-selector/restore/running");
                i = R.string.settings_gdrive_please_wait_for_restore_to_finish_before_account_change;
            } else {
                int i2 = 0;
                if (this.A0O.A02("android.permission.GET_ACCOUNTS") != 0 || !this.A0L.A00()) {
                    C35751ig r3 = new C35751ig(this);
                    r3.A01 = R.drawable.permission_contacts_small;
                    r3.A0C = new String[]{"android.permission.GET_ACCOUNTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS"};
                    r3.A02 = R.string.permission_contacts_access_for_gdrive_backup_request;
                    r3.A03 = R.string.permission_contacts_access_for_gdrive_backup;
                    A2E(r3.A00(), 150);
                    return;
                }
                String A09 = ((ActivityC13810kN) this).A09.A09();
                Account[] accountsByType = AccountManager.get(this).getAccountsByType("com.google");
                int length = accountsByType.length;
                if (length > 0) {
                    StringBuilder sb = new StringBuilder("settings-gdrive/account-selector/starting-account-picker/num-accounts/");
                    sb.append(length);
                    Log.i(sb.toString());
                    int i3 = -1;
                    int i4 = length + 1;
                    String[] strArr = new String[i4];
                    do {
                        strArr[i2] = accountsByType[i2].name;
                        if (A09 != null && A09.equals(strArr[i2])) {
                            i3 = i2;
                        }
                        i2++;
                    } while (i2 < length);
                    strArr[i4 - 1] = getString(R.string.google_account_picker_add_account);
                    SingleChoiceListDialogFragment singleChoiceListDialogFragment = new SingleChoiceListDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("dialog_id", 17);
                    bundle.putString("title", getString(R.string.google_account_picker_title));
                    bundle.putInt("selected_item_index", i3);
                    bundle.putStringArray("multi_line_list_items_key", strArr);
                    singleChoiceListDialogFragment.A0U(bundle);
                    if (A0V().A0A("account-picker") == null) {
                        C004902f r0 = new C004902f(A0V());
                        r0.A09(singleChoiceListDialogFragment, "account-picker");
                        r0.A02();
                        return;
                    }
                    return;
                }
                Log.i("settings-gdrive/account-selector/no-account-found/start-add-account-activity");
                A2h();
                return;
            }
            Ado(i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a8, code lost:
        if (r0 == false) goto L_0x00aa;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A2g() {
        /*
            r7 = this;
            X.1D6 r0 = r7.A0J
            X.0m9 r1 = r0.A08
            r0 = 932(0x3a4, float:1.306E-42)
            boolean r0 = r1.A07(r0)
            r5 = 1
            r3 = 0
            com.whatsapp.TextEmojiLabel r1 = r7.A0B
            if (r0 == 0) goto L_0x006b
            r1.setVisibility(r3)
            r0 = 2131889088(0x7f120bc0, float:1.941283E38)
            java.lang.String r6 = r7.getString(r0)
            r2 = 2131891679(0x7f1215df, float:1.9418085E38)
            r0 = 2
            java.lang.Object[] r1 = new java.lang.Object[r0]
            X.1D6 r0 = r7.A0J
            java.lang.String r0 = r0.A03()
            r1[r3] = r0
            r1[r5] = r6
            java.lang.String r0 = r7.getString(r2, r1)
            android.text.SpannableStringBuilder r4 = new android.text.SpannableStringBuilder
            r4.<init>(r0)
            X.2aP r3 = new X.2aP
            r3.<init>(r7)
            int r2 = r0.length()
            int r0 = r6.length()
            int r1 = r2 - r0
            r0 = 33
            r4.setSpan(r3, r1, r2, r0)
            com.whatsapp.TextEmojiLabel r0 = r7.A0B
            r0.setText(r4)
            com.whatsapp.TextEmojiLabel r0 = r7.A0B
            r0.setLinksClickable(r5)
            com.whatsapp.TextEmojiLabel r1 = r7.A0B
            android.text.method.MovementMethod r0 = android.text.method.LinkMovementMethod.getInstance()
            r1.setMovementMethod(r0)
            X.0nx r0 = r7.A0E
            boolean r0 = r0.A04()
            if (r0 == 0) goto L_0x009d
            com.whatsapp.WaTextView r3 = r7.A0D
            r2 = 2131891668(0x7f1215d4, float:1.9418063E38)
        L_0x0067:
            r3.setText(r2)
            return
        L_0x006b:
            r0 = 8
            r1.setVisibility(r0)
            X.0nx r0 = r7.A0E
            boolean r0 = r0.A05()
            if (r0 == 0) goto L_0x009a
            X.0nx r0 = r7.A0E
            boolean r1 = r0.A04()
            X.0m6 r0 = r7.A09
            int r0 = r0.A01()
            com.whatsapp.WaTextView r3 = r7.A0D
            if (r0 == 0) goto L_0x0091
            r2 = 2131891666(0x7f1215d2, float:1.9418058E38)
            if (r1 == 0) goto L_0x0067
            r2 = 2131891667(0x7f1215d3, float:1.941806E38)
            goto L_0x0067
        L_0x0091:
            r2 = 2131891664(0x7f1215d0, float:1.9418054E38)
            if (r1 == 0) goto L_0x0067
            r2 = 2131891665(0x7f1215d1, float:1.9418056E38)
            goto L_0x0067
        L_0x009a:
            com.whatsapp.WaTextView r3 = r7.A0D
            goto L_0x00aa
        L_0x009d:
            X.0nx r0 = r7.A0E
            boolean r0 = r0.A05()
            com.whatsapp.WaTextView r3 = r7.A0D
            r2 = 2131891660(0x7f1215cc, float:1.9418046E38)
            if (r0 != 0) goto L_0x0067
        L_0x00aa:
            r2 = 2131891663(0x7f1215cf, float:1.9418052E38)
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass29U.A2g():void");
    }

    public final void A2h() {
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S0300000_I0(this, AccountManager.get(this).addAccount("com.google", null, null, null, this, null, null), new BaseNewUserSetupActivity$AuthRequestDialogFragment(), 13));
    }

    public final void A2i() {
        A2g();
        boolean A05 = this.A0E.A05();
        View view = this.A01;
        if (A05) {
            view.setVisibility(0);
            boolean A04 = this.A0E.A04();
            WaTextView waTextView = this.A0C;
            int i = R.string.settings_gdrive_backup_e2e_encrypted_value_disabled;
            if (A04) {
                i = R.string.settings_gdrive_backup_e2e_encrypted_value_enabled;
            }
            waTextView.setText(i);
            return;
        }
        view.setVisibility(8);
    }

    public final void A2j(BaseNewUserSetupActivity$AuthRequestDialogFragment baseNewUserSetupActivity$AuthRequestDialogFragment, String str) {
        AnonymousClass009.A00();
        StringBuilder sb = new StringBuilder("settings-gdrive/auth-request account being used is ");
        sb.append(C44771zW.A0B(str));
        Log.i(sb.toString());
        this.A0a = false;
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(this, 38, baseNewUserSetupActivity$AuthRequestDialogFragment));
        ConditionVariable conditionVariable = this.A0Y;
        conditionVariable.close();
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, baseNewUserSetupActivity$AuthRequestDialogFragment, str, 2));
        Log.i("settings-gdrive/auth-request blocking on tokenReceived");
        C28181Ma r3 = new C28181Ma("settings-gdrive/fetch-auth-token");
        conditionVariable.block(C26061Bw.A0L);
        ((ActivityC13810kN) this).A05.A0H(new RunnableBRunnable0Shape0S0200000_I0(this, 40, r3));
    }

    public final void A2k(String str) {
        StringBuilder sb = new StringBuilder("setting-gdrive/activity-result/account-picker accountName is ");
        sb.append(C44771zW.A0B(str));
        Log.i(sb.toString());
        if (str != null) {
            ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape0S1200000_I0(this, new BaseNewUserSetupActivity$AuthRequestDialogFragment(), str, 3));
        } else if (((ActivityC13810kN) this).A09.A09() == null) {
            Log.i("setting-gdrive/activity-result/account-picker accountName is null");
            A2e();
        }
    }

    public final void A2l(String str, String str2) {
        this.A0Y.open();
        DialogFragment dialogFragment = (DialogFragment) A0V().A0A("auth_request_dialog");
        if (dialogFragment != null) {
            dialogFragment.A1C();
        }
        if (str != null) {
            GoogleDriveNewUserSetupViewModel googleDriveNewUserSetupViewModel = this.A0K;
            C14820m6 r1 = googleDriveNewUserSetupViewModel.A05;
            if (!TextUtils.equals(r1.A09(), str2)) {
                r1.A0f(str2);
                C26551Dx r2 = googleDriveNewUserSetupViewModel.A03;
                synchronized (r2.A0N) {
                    r2.A00 = null;
                }
                StringBuilder sb = new StringBuilder("gdrive-new-user-view-model/update-account-name new accountName is ");
                sb.append(C44771zW.A0B(str2));
                Log.i(sb.toString());
                googleDriveNewUserSetupViewModel.A00.A0B(str2);
                Intent A0U = C14960mK.A0U(this, "action_fetch_backup_info");
                A0U.putExtra("account_name", str2);
                AnonymousClass1Tv.A00(this, A0U);
            } else {
                StringBuilder sb2 = new StringBuilder("gdrive-new-user-view-model/update-account-name account unchanged, token received for ");
                sb2.append(C44771zW.A0B(str2));
                Log.i(sb2.toString());
            }
        }
        ((ActivityC13830kP) this).A05.Ab2(new RunnableBRunnable0Shape2S0100000_I0_2(this, 18));
    }

    @Override // X.AnonymousClass29H
    public void AP8(int i) {
        String str;
        switch (i) {
            case 12:
                str = "settings-gdrive/cancel-media-restore-dialog/user-decided-not-to-cancel";
                break;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                str = "settings-gdrive/perform-backup user declined to perform Google Drive backup over cellular (when the settings say Wi-Fi only)";
                break;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 17:
            default:
                StringBuilder sb = new StringBuilder("unexpected dialog box: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            case 15:
                str = "settings-gdrive/user-declined-to-restore-media-over-cellular";
                break;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                str = "settings-gdrive/user-declined-to-backup-over-cellular";
                break;
            case 18:
                str = "settings-gdrive/user-declined-to-cancel-encrypted-backup";
                break;
        }
        Log.i(str);
    }

    @Override // X.AnonymousClass29H
    public void AP9(int i) {
        StringBuilder sb = new StringBuilder("unexpected dialog box: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }

    @Override // X.AnonymousClass29H
    public void APA(int i) {
        switch (i) {
            case 12:
                this.A0G.A03();
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                Log.i("settings-gdrive/perform-backup user decided to perform Google Drive backup over cellular (when the settings say Wi-Fi only)");
                this.A0F.A01();
                this.A0G.A05(10);
                Intent A0U = C14960mK.A0U(this, "action_backup");
                A0U.putExtra("backup_mode", "user_initiated");
                AnonymousClass1Tv.A00(this, A0U);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                Log.i("settings-gdrive/google-play-services-is-broken");
                this.A0K.A04(0);
                return;
            case 15:
                Log.i("settings-gdrive/user-confirmed-media-restore-over-cellular");
                this.A0F.A02();
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                Log.i("settings-gdrive/user-confirmed-backup-over-cellular");
                this.A0F.A01();
                return;
            case 17:
            default:
                StringBuilder sb = new StringBuilder("unexpected dialog box: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            case 18:
                Log.i("settings-gdrive/user-confirmed-cancel-encrypted-backup");
                Log.i("settings-gdrive/cancel-backup");
                this.A05.setVisibility(8);
                this.A0G.A03();
                if (C44771zW.A0M(((ActivityC13810kN) this).A0C)) {
                    try {
                        for (C05450Pp r0 : (List) ((AnonymousClass022) this.A0T.get()).A02().get()) {
                            if (!r0.A03.A00()) {
                                ((AnonymousClass022) this.A0T.get()).A08("com.whatsapp.backup.google.google-backup-worker");
                                return;
                            }
                        }
                        return;
                    } catch (InterruptedException | ExecutionException unused) {
                        Log.e("settings-gdrive/cancel-backup couldn't get work info for BackupWorker.");
                        return;
                    }
                } else {
                    return;
                }
        }
    }

    @Override // X.AnonymousClass29I
    public void APF(int i) {
        StringBuilder sb = new StringBuilder("settings-gdrive/dialogId-");
        sb.append(i);
        sb.append("-dismissed");
        Log.i(sb.toString());
    }

    @Override // X.AnonymousClass29I
    public void AW6(String[] strArr, int i, int i2) {
        String str;
        if (i == 10) {
            int[] iArr = GoogleDriveNewUserSetupViewModel.A07;
            if (i2 > iArr.length) {
                StringBuilder sb = new StringBuilder("settings-gdrive/change-freq/unexpected-choice/");
                sb.append(i2);
                str = sb.toString();
            } else {
                StringBuilder sb2 = new StringBuilder("settings-gdrive/change-freq/index:");
                sb2.append(i2);
                sb2.append("/value:");
                sb2.append(iArr[i2]);
                Log.i(sb2.toString());
                int A01 = ((ActivityC13810kN) this).A09.A01();
                int i3 = iArr[i2];
                if (!this.A0K.A04(i3)) {
                    str = "settings-gdrive/change-freq failed to set the new frequency.";
                } else {
                    if (i3 != 0) {
                        if (A01 == 0 && !C44771zW.A0G(((ActivityC13810kN) this).A09) && !C44771zW.A0H(((ActivityC13810kN) this).A09)) {
                            this.A00.performClick();
                        }
                    } else if (((ActivityC13810kN) this).A09.A00.getLong("gdrive_next_prompt_for_setup_timestamp", -1) < System.currentTimeMillis() + 2592000000L) {
                        ((ActivityC13810kN) this).A09.A0Y(System.currentTimeMillis() + 2592000000L);
                    }
                    A2g();
                    return;
                }
            }
            Log.e(str);
        } else if (i != 17) {
            StringBuilder sb3 = new StringBuilder("unexpected dialog box: ");
            sb3.append(i);
            throw new IllegalStateException(sb3.toString());
        } else if (strArr[i2].equals(getString(R.string.google_account_picker_add_account))) {
            A2h();
        } else {
            A2k(strArr[i2]);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        StringBuilder sb = new StringBuilder("settings-gdrive/activity-result request: ");
        sb.append(i);
        sb.append(" result: ");
        sb.append(i2);
        Log.i(sb.toString());
        super.onActivityResult(i, i2, intent);
        if (i != 0) {
            if (i != 1) {
                if (i == 2) {
                    String str2 = null;
                    if (intent != null) {
                        str = intent.getStringExtra("authAccount");
                    } else {
                        str = null;
                    }
                    if (i2 == -1) {
                        str2 = str;
                    }
                    A2k(str2);
                } else if (i == 150 && i2 == -1) {
                    A2f();
                }
            } else if (i2 == -1) {
                AnonymousClass009.A05(intent);
                A2l(intent.getStringExtra("authtoken"), intent.getStringExtra("authAccount"));
            } else {
                DialogFragment dialogFragment = (DialogFragment) A0V().A0A("auth_request_dialog");
                if (dialogFragment != null) {
                    dialogFragment.A1C();
                }
            }
        } else if (i2 == -1) {
            A2i();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        this.A0K = (GoogleDriveNewUserSetupViewModel) new AnonymousClass02A(this).A00(GoogleDriveNewUserSetupViewModel.class);
        setTitle(R.string.settings_backup);
        setContentView(R.layout.activity_settings_google_drive);
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        this.A00 = findViewById(R.id.settings_gdrive_change_account_view);
        this.A06 = (TextView) AnonymousClass00T.A05(this, R.id.settings_gdrive_account_name_summary);
        this.A08 = (TextView) AnonymousClass00T.A05(this, R.id.gdrive_backup_general_info);
        this.A05 = (ImageView) findViewById(R.id.cancel_download);
        this.A02 = findViewById(R.id.settings_gdrive_change_frequency_view);
        this.A07 = (TextView) findViewById(R.id.settings_gdrive_backup_options_summary);
        this.A04 = findViewById(R.id.settings_gdrive_network_settings_view);
        this.A0A = (SwitchCompat) findViewById(R.id.gdrive_network_setting);
        this.A03 = findViewById(R.id.settings_gdrive_backup_include_video);
        this.A01 = findViewById(R.id.settings_gdrive_password_protect_backups);
        this.A0C = (WaTextView) findViewById(R.id.settings_gdrive_password_protect_backups_value);
        this.A0D = (WaTextView) findViewById(R.id.settings_gdrive_backup_encryption_info);
        this.A0B = (TextEmojiLabel) findViewById(R.id.settings_gdrive_backup_quota_info);
        this.A09 = (SwitchCompat) findViewById(R.id.include_video_setting);
        int A00 = C41691tw.A00(this, R.attr.settingsIconColor, R.color.settings_icon);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.last_backup_icon), A00);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.gdrive_icon), A00);
        AnonymousClass2GE.A07((ImageView) findViewById(R.id.backup_settings_icon), A00);
        int[] iArr = GoogleDriveNewUserSetupViewModel.A06;
        int length = iArr.length;
        this.A0V = new String[length];
        for (int i = 0; i < length; i++) {
            int i2 = iArr[i];
            if (i2 == R.string.settings_gdrive_backup_frequency_option_manual) {
                this.A0V[i] = getString(R.string.settings_gdrive_backup_frequency_option_manual, getString(R.string.backup));
            } else {
                this.A0V[i] = getString(i2);
            }
        }
        this.A01.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 6));
        this.A0K.A02.A05(this, new AnonymousClass02B() { // from class: X.4sG
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass29U.this.A09.setChecked(C12970iu.A1Z(obj, Boolean.TRUE));
            }
        });
        this.A0K.A00.A05(this, new AnonymousClass02B() { // from class: X.4sH
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass29U r1 = AnonymousClass29U.this;
                String str = (String) obj;
                if (str == null) {
                    str = r1.getString(R.string.settings_gdrive_account_name_missing_value);
                }
                r1.A06.setText(str);
            }
        });
        this.A0K.A01.A05(this, new AnonymousClass02B() { // from class: X.3Pe
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                AnonymousClass29U r0 = AnonymousClass29U.this;
                TextView textView = r0.A07;
                String[] strArr = r0.A0V;
                int intValue = ((Number) obj).intValue();
                int i3 = 0;
                while (true) {
                    int[] iArr2 = GoogleDriveNewUserSetupViewModel.A07;
                    if (i3 < iArr2.length) {
                        if (iArr2[i3] == intValue) {
                            break;
                        }
                        i3++;
                    } else {
                        Log.e(C12960it.A0W(intValue, "settings-gdrive/get-backup-freq-index/"));
                        i3 = 0;
                        break;
                    }
                }
                textView.setText(strArr[i3]);
            }
        });
        TextView textView = this.A08;
        boolean A002 = C14950mJ.A00();
        int i3 = R.string.gdrive_new_user_setup_general_info_shared_storage_short;
        if (A002) {
            i3 = R.string.gdrive_new_user_setup_general_info_sdcard_short;
        }
        textView.setText(i3);
        A2i();
        ViewOnClickCListenerShape0S0100000_I0 viewOnClickCListenerShape0S0100000_I0 = new ViewOnClickCListenerShape0S0100000_I0(this, 19);
        this.A00.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A04.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A02.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        this.A03.setOnClickListener(viewOnClickCListenerShape0S0100000_I0);
        bindService(C14960mK.A0U(this, null), this.A0W, 1);
        if (!this.A0F.A09()) {
            Log.i("settings-gdrive/create google drive access not allowed.");
            finish();
        }
        if ((bundle == null || !bundle.getBoolean("intent_already_parsed", false)) && (intent = getIntent()) != null) {
            if (intent.getAction() != null) {
                onNewIntent(intent);
            }
            if (intent.getBooleanExtra("backup_quota_notification", false)) {
                AnonymousClass2KB r1 = new AnonymousClass2KB();
                r1.A04 = 3;
                this.A0Q.A07(r1);
            }
        }
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i) {
        if (i == 600) {
            return SettingsChat.A02(this);
        }
        if (i != 602) {
            return super.onCreateDialog(i);
        }
        return SettingsChat.A03(this);
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A0U = true;
        this.A0Z.set(false);
        unbindService(this.A0W);
        super.onDestroy();
    }

    @Override // X.ActivityC13790kL, X.ActivityC000800j, android.app.Activity, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        AnonymousClass3FU r3;
        int i;
        super.onNewIntent(intent);
        String action = intent.getAction();
        StringBuilder sb = new StringBuilder("settings-gdrive/new-intent/action/");
        sb.append(action);
        Log.i(sb.toString());
        if (action != null) {
            String str = "action_perform_backup_over_cellular";
            if (!action.equals(str)) {
                str = "action_perform_media_restore_over_cellular";
                if (!action.equals(str)) {
                    StringBuilder sb2 = new StringBuilder("settings-gdrive/new-intent/unexpected-action/");
                    sb2.append(intent.getAction());
                    Log.e(sb2.toString());
                    return;
                }
                r3 = new AnonymousClass3FU(15);
                i = R.string.google_drive_confirm_media_restore_over_cellular_message;
            } else {
                r3 = new AnonymousClass3FU(16);
                i = R.string.google_drive_confirm_backup_over_cellular_message;
            }
            String string = getString(i);
            Bundle bundle = r3.A00;
            bundle.putCharSequence("message", string);
            r3.A00();
            r3.A02(getString(R.string.google_drive_resume_button_label));
            r3.A01(getString(R.string.not_now));
            PromptDialogFragment promptDialogFragment = new PromptDialogFragment();
            promptDialogFragment.A0U(bundle);
            C004902f r0 = new C004902f(A0V());
            r0.A09(promptDialogFragment, str);
            r0.A02();
        }
    }

    @Override // X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        onBackPressed();
        return true;
    }

    @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("intent_already_parsed", true);
    }
}
