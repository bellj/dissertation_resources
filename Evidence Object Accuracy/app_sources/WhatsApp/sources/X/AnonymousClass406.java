package X;

/* renamed from: X.406  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass406 extends C37171lc {
    public final int A00;

    public AnonymousClass406(int i) {
        super(AnonymousClass39o.A0P);
        this.A00 = i;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj) || this.A00 != ((AnonymousClass406) obj).A00) {
            return false;
        }
        return true;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return super.A00.hashCode();
    }
}
