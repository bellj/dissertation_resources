package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34231fn extends AbstractC28131Kt {
    public final UserJid A00;
    public final AnonymousClass1IS A01;
    public final String A02;

    public C34231fn(UserJid userJid, AnonymousClass1IS r2, C34021fS r3, String str, String str2) {
        super(r3, str);
        this.A00 = userJid;
        this.A02 = str2;
        this.A01 = r2;
    }
}
