package X;

/* renamed from: X.1xe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC43821xe {
    A03(0),
    A02(1),
    A01(2),
    A04(3);
    
    public final int value;

    EnumC43821xe(int i) {
        this.value = i;
    }

    public static EnumC43821xe A00(int i) {
        if (i == 0) {
            return A03;
        }
        if (i == 1) {
            return A02;
        }
        if (i == 2) {
            return A01;
        }
        if (i != 3) {
            return null;
        }
        return A04;
    }
}
