package X;

import java.util.List;

/* renamed from: X.2us  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59522us extends C37171lc {
    public final AnonymousClass2Jw A00;
    public final List A01;

    public C59522us(AnonymousClass2Jw r2, List list) {
        super(AnonymousClass39o.A0A);
        this.A01 = list;
        this.A00 = r2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A01.equals(((C59522us) obj).A01);
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A01.hashCode();
    }
}
