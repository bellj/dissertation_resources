package X;

import android.app.Application;
import android.os.Bundle;
import com.whatsapp.jid.Jid;

/* renamed from: X.2fE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53792fE extends AnonymousClass07A {
    public final AnonymousClass4JK A00;
    public final C30211Wn A01;
    public final Jid A02;
    public final String A03;
    public final boolean A04;

    public C53792fE(Bundle bundle, AbstractC001500q r2, AnonymousClass4JK r3, C30211Wn r4, Jid jid, String str, boolean z) {
        super(bundle, r2);
        this.A01 = r4;
        this.A02 = jid;
        this.A04 = z;
        this.A00 = r3;
        this.A03 = str;
    }

    @Override // X.AnonymousClass07A
    public AnonymousClass015 A02(AnonymousClass07E r36, Class cls, String str) {
        AnonymousClass4JK r0 = this.A00;
        boolean z = this.A04;
        String str2 = this.A03;
        C30211Wn r4 = this.A01;
        Jid jid = this.A02;
        C71573d9 r9 = r0.A00;
        AnonymousClass01J r8 = r9.A04;
        Application A00 = AbstractC250617y.A00(r8.AO3);
        AnonymousClass018 A0R = C12960it.A0R(r8);
        C251118d A0V = C12970iu.A0V(r8);
        AnonymousClass2FL r02 = r9.A01;
        AnonymousClass01J r1 = r02.A1E;
        C15550nR A0O = C12960it.A0O(r1);
        AnonymousClass2J2 r6 = (AnonymousClass2J2) r02.A0j.get();
        AnonymousClass2J4 r5 = (AnonymousClass2J4) r02.A0k.get();
        AnonymousClass2K0 r18 = new AnonymousClass2K0(C12970iu.A0V(r1), (AnonymousClass2J6) r02.A0l.get(), r5, (AnonymousClass2J8) r02.A0m.get(), (AnonymousClass2JA) r02.A0n.get(), r6, (AnonymousClass1B5) r1.A5t.get(), A0O);
        C51112Sw r62 = r9.A03;
        C92174Uv r12 = new C92174Uv(C12970iu.A0V(r62.A0Y));
        C16430p0 A0W = C12990iw.A0W(r8);
        AnonymousClass4N8 r22 = new AnonymousClass4N8();
        AbstractC115855Te r03 = (AbstractC115855Te) r62.A0S.get();
        AbstractC17940re copyOf = AbstractC17940re.copyOf(C12970iu.A12());
        return new C53862fQ(A00, r36, (C89154Iw) r62.A0T.get(), A0V, A0W, r18, r12, r03, (AbstractC115915Tk) r62.A0Q.get(), r22, (AbstractC115925Tl) r62.A0R.get(), r4, A0R, jid, str2, copyOf, z);
    }
}
