package X;

/* renamed from: X.4Wc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92484Wc {
    public final int A00;
    public final int A01;

    public C92484Wc(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C92484Wc)) {
            return false;
        }
        C92484Wc r4 = (C92484Wc) obj;
        if (this.A01 == r4.A01 && this.A00 == r4.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((16337 + this.A01) * 31) + this.A00;
    }
}
