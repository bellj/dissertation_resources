package X;

import android.app.Application;
import android.content.Context;

/* renamed from: X.22D  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass22D {
    public static Object A00(Context context) {
        Application A00 = AnonymousClass1HD.A00(context.getApplicationContext());
        boolean z = A00 instanceof AnonymousClass005;
        Object[] objArr = {A00.getClass()};
        if (z) {
            return ((AnonymousClass005) A00).generatedComponent();
        }
        throw new IllegalArgumentException(String.format("Hilt BroadcastReceiver must be attached to an @AndroidEntryPoint Application. Found: %s", objArr));
    }
}
