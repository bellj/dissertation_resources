package X;

import android.os.Looper;

/* renamed from: X.4a7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93404a7 {
    public static final C93404a7 A02 = new C93404a7(Looper.getMainLooper(), new C108294yp());
    public final Looper A00;
    public final AnonymousClass5QZ A01;

    public /* synthetic */ C93404a7(Looper looper, AnonymousClass5QZ r2) {
        this.A01 = r2;
        this.A00 = looper;
    }
}
