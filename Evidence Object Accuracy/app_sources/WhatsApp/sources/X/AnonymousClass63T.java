package X;

import android.os.Handler;
import android.os.Message;
import java.util.List;

/* renamed from: X.63T  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63T implements Handler.Callback {
    public final /* synthetic */ C1308260c A00;

    public AnonymousClass63T(C1308260c r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        C1308260c r6 = this.A00;
        if (message.what != 1) {
            return false;
        }
        List list = r6.A09;
        List list2 = r6.A08;
        int i = message.arg1;
        r6.A01();
        if (list == null || list2 == null || i >= list.size()) {
            return true;
        }
        C129775yH r1 = r6.A0E;
        list2.get(list2.size() - 1);
        list.get(list.size() - 1);
        List list3 = r1.A00;
        if (0 >= list3.size()) {
            return true;
        }
        list3.get(0);
        throw C12980iv.A0n("onZoomChange");
    }
}
