package X;

/* renamed from: X.2uf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59402uf extends AbstractC92594Wn {
    public final AnonymousClass3F6 A00;

    public C59402uf(AbstractC116555Vx r1, AnonymousClass3F6 r2, boolean z) {
        super(r1, z);
        this.A00 = r2;
    }

    @Override // X.AbstractC92594Wn
    public String A00() {
        return this.A00.A01;
    }

    @Override // X.AbstractC92594Wn
    public String A01() {
        return this.A00.A02;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C59402uf r5 = (C59402uf) obj;
            if (!this.A00.equals(r5.A00) || r5.A01 != this.A01) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
