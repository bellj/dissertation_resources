package X;

import android.content.Context;
import android.graphics.Rect;
import com.facebook.rendercore.RenderTreeNode;
import java.util.ArrayList;

/* renamed from: X.3IP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IP {
    public final C89824Ln A00;
    public final AnonymousClass5SD A01;
    public final AnonymousClass3H1 A02;
    public final AnonymousClass28D A03;
    public final Object A04;

    public AnonymousClass3IP(C89824Ln r1, AnonymousClass5SD r2, AnonymousClass3H1 r3, AnonymousClass28D r4, Object obj) {
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = r1;
        this.A04 = obj;
    }

    public static AnonymousClass3IP A00(AbstractC72393eW r8, C92304Vj r9, AnonymousClass5SD r10, AnonymousClass28D r11, Object obj, int i, int i2) {
        Context context = r9.A02;
        C94614cC.A01("Reducer.reduceTree");
        ArrayList A0l = C12960it.A0l();
        RenderTreeNode A00 = AnonymousClass3I3.A00(new Rect(0, 0, r8.getWidth(), r8.getHeight()), r8, null, AnonymousClass3I3.A00);
        A0l.add(A00);
        AnonymousClass3I3.A01(context, r8, A00, A0l, 0, 0);
        C94614cC.A00();
        AnonymousClass3H1 r3 = new AnonymousClass3H1(A00, (RenderTreeNode[]) A0l.toArray(new RenderTreeNode[A0l.size()]), i, i2);
        C93294Zw r0 = r9.A00;
        if (r0 != null) {
            return new AnonymousClass3IP(r0.A01, r10, r3, r11, obj);
        }
        throw C12960it.A0U("Trying to access the LayoutCache from outside a layout call");
    }

    public AnonymousClass3H1 A01() {
        return this.A02;
    }
}
