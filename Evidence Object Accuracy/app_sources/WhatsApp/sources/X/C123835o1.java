package X;

import android.text.Editable;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.5o1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123835o1 extends C469928m {
    public final /* synthetic */ C117795ag A00;

    public C123835o1(C117795ag r1) {
        this.A00 = r1;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        String str;
        C117795ag r3 = this.A00;
        if (r3.A0C || r3.A0D) {
            str = editable.toString();
        } else if (editable.length() == 0) {
            r3.A09 = "";
            return;
        } else if (r3.A09.length() > editable.length()) {
            String str2 = r3.A09;
            str = str2.substring(0, str2.length() - 1);
        } else {
            char charAt = editable.toString().charAt(editable.length() - 1);
            if (charAt != 9679) {
                String str3 = r3.A09;
                StringBuilder A0j = C12960it.A0j("");
                A0j.append(charAt);
                String concat = str3.concat(A0j.toString());
                r3.A09 = concat;
                r3.A0A.setText(concat.replaceAll(".", "●"));
                return;
            }
            r3.A0A.setSelection(editable.length());
            return;
        }
        r3.A09 = str;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        C117795ag r4 = this.A00;
        if (r4.A0B != null) {
            FormItemEditText formItemEditText = r4.A0A;
            if (formItemEditText.getText() != null && formItemEditText.getText().length() >= r4.A00) {
                r4.A0B.AQm(r4.A01, C12970iu.A0p(formItemEditText));
            }
        }
    }
}
