package X;

import android.graphics.Bitmap;
import android.util.SparseArray;
import java.util.LinkedList;

/* renamed from: X.4v8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106104v8 implements AnonymousClass5YZ {
    public int A00;
    public int A01;
    public final C89764Lh A02 = new C89764Lh();
    public final AbstractC115035Pl A03;

    public C106104v8(AbstractC115035Pl r2, int i) {
        this.A01 = i;
        this.A03 = r2;
    }

    @Override // X.AbstractC12900ik, X.AbstractC12240hb
    public /* bridge */ /* synthetic */ void Aa4(Object obj) {
        boolean add;
        Object[] objArr;
        String str;
        Bitmap bitmap = (Bitmap) obj;
        C89764Lh r4 = this.A02;
        int A01 = C94594c9.A01(bitmap);
        if (A01 <= this.A01) {
            if (bitmap != null) {
                if (bitmap.isRecycled()) {
                    objArr = new Object[]{bitmap};
                    str = "Cannot reuse a recycled bitmap: %s";
                } else if (!bitmap.isMutable()) {
                    objArr = new Object[]{bitmap};
                    str = "Cannot reuse an immutable bitmap: %s";
                } else {
                    synchronized (r4) {
                        add = r4.A01.add(bitmap);
                    }
                    if (add) {
                        AnonymousClass4VV r42 = r4.A00;
                        int A012 = C94594c9.A01(bitmap);
                        synchronized (r42) {
                            SparseArray sparseArray = r42.A02;
                            C92294Vi r1 = (C92294Vi) sparseArray.get(A012);
                            if (r1 == null) {
                                r1 = new C92294Vi(new LinkedList(), A012);
                                sparseArray.put(A012, r1);
                            }
                            r1.A03.addLast(bitmap);
                            if (r42.A00 != r1) {
                                r42.A00(r1);
                                C92294Vi r0 = r42.A00;
                                if (r0 == null) {
                                    r42.A00 = r1;
                                    r42.A01 = r1;
                                } else {
                                    r1.A01 = r0;
                                    r0.A02 = r1;
                                    r42.A00 = r1;
                                }
                            }
                        }
                    }
                }
                AnonymousClass0UN.A04("BitmapPoolBackend", str, objArr);
            }
            synchronized (this) {
                this.A00 += A01;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b0 A[Catch: all -> 0x00cd, TryCatch #5 {, blocks: (B:4:0x0002, B:23:0x004c, B:24:0x0050, B:34:0x0079, B:36:0x007c, B:40:0x0087, B:42:0x008b, B:44:0x0094, B:45:0x009a, B:46:0x009e, B:48:0x00a4, B:52:0x00b0, B:55:0x00b7, B:56:0x00c1, B:37:0x007d, B:38:0x0082, B:25:0x0051, B:28:0x005d, B:30:0x0067, B:32:0x006e, B:33:0x0073, B:7:0x0009, B:8:0x000d, B:15:0x002c, B:17:0x002f, B:20:0x0036, B:22:0x003a, B:18:0x0030, B:19:0x0035, B:9:0x000e, B:12:0x0014, B:14:0x0022), top: B:61:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00b7 A[Catch: all -> 0x00cd, TryCatch #5 {, blocks: (B:4:0x0002, B:23:0x004c, B:24:0x0050, B:34:0x0079, B:36:0x007c, B:40:0x0087, B:42:0x008b, B:44:0x0094, B:45:0x009a, B:46:0x009e, B:48:0x00a4, B:52:0x00b0, B:55:0x00b7, B:56:0x00c1, B:37:0x007d, B:38:0x0082, B:25:0x0051, B:28:0x005d, B:30:0x0067, B:32:0x006e, B:33:0x0073, B:7:0x0009, B:8:0x000d, B:15:0x002c, B:17:0x002f, B:20:0x0036, B:22:0x003a, B:18:0x0030, B:19:0x0035, B:9:0x000e, B:12:0x0014, B:14:0x0022), top: B:61:0x0002 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00c1 A[Catch: all -> 0x00cd, TRY_LEAVE, TryCatch #5 {, blocks: (B:4:0x0002, B:23:0x004c, B:24:0x0050, B:34:0x0079, B:36:0x007c, B:40:0x0087, B:42:0x008b, B:44:0x0094, B:45:0x009a, B:46:0x009e, B:48:0x00a4, B:52:0x00b0, B:55:0x00b7, B:56:0x00c1, B:37:0x007d, B:38:0x0082, B:25:0x0051, B:28:0x005d, B:30:0x0067, B:32:0x006e, B:33:0x0073, B:7:0x0009, B:8:0x000d, B:15:0x002c, B:17:0x002f, B:20:0x0036, B:22:0x003a, B:18:0x0030, B:19:0x0035, B:9:0x000e, B:12:0x0014, B:14:0x0022), top: B:61:0x0002 }] */
    @Override // X.AbstractC12900ik
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object get(int r9) {
        /*
        // Method dump skipped, instructions count: 208
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106104v8.get(int):java.lang.Object");
    }
}
