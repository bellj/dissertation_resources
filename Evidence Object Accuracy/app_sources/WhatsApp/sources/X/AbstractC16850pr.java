package X;

import android.os.Handler;
import android.os.Looper;
import android.util.JsonReader;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.io.IOException;
import java.io.StringReader;
import java.util.Set;

/* renamed from: X.0pr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16850pr implements AbstractC16860ps {
    public final Set A00;

    public abstract void A01(C91064Qh v, AnonymousClass5WN v2, String str);

    public abstract void A02(C65963Lt v, AnonymousClass5WN v2, Boolean bool, String str, String str2, String str3, boolean z);

    public AbstractC16850pr(Set set) {
        this.A00 = set;
    }

    public void A00(C91064Qh r7, AnonymousClass5WN r8, String str, boolean z) {
        if (z) {
            C91094Qk r4 = new C91094Qk(r7, r8, this);
            try {
                JsonReader jsonReader = new JsonReader(new StringReader(str));
                C67883Tg r0 = new C67883Tg(jsonReader);
                r0.ALh();
                new Handler(Looper.getMainLooper()).post(new RunnableBRunnable0Shape10S0200000_I1(AnonymousClass3AF.A00(r0), 49, r4));
                jsonReader.close();
            } catch (IOException e) {
                r4.A02.A01(r4.A00, r4.A01, e.getMessage());
            }
        } else {
            AnonymousClass3AL.A00(new AnonymousClass548(r7, r8, this), str);
        }
    }
}
