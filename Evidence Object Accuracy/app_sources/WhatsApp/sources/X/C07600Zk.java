package X;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.0Zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07600Zk implements AbstractC11410gF {
    public final Handler A00;

    public C07600Zk() {
        Handler handler;
        Looper mainLooper = Looper.getMainLooper();
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            handler = C04110Kk.A00(mainLooper);
        } else {
            if (i >= 17) {
                try {
                    handler = (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(mainLooper, null, Boolean.TRUE);
                } catch (IllegalAccessException | InstantiationException | NoSuchMethodException e) {
                    Log.w("HandlerCompat", "Unable to invoke Handler(Looper, Callback, boolean) constructor", e);
                } catch (InvocationTargetException e2) {
                    Throwable cause = e2.getCause();
                    if (cause instanceof RuntimeException) {
                        throw cause;
                    } else if (cause instanceof Error) {
                        throw cause;
                    } else {
                        throw new RuntimeException(cause);
                    }
                }
            }
            handler = new Handler(mainLooper);
        }
        this.A00 = handler;
    }
}
