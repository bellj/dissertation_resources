package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78283of extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98574iu();
    public int A00;
    public Bundle A01;
    public final int A02;

    public C78283of(Bundle bundle, int i, int i2) {
        this.A02 = i;
        this.A00 = i2;
        this.A01 = bundle;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A02);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A03(this.A01, parcel, 3);
        C95654e8.A06(parcel, A00);
    }
}
