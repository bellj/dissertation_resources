package X;

import android.graphics.Bitmap;

/* renamed from: X.53q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1099453q implements AnonymousClass2E5 {
    public boolean A00;
    public final /* synthetic */ C68203Um A01;
    public final /* synthetic */ C68183Uk A02;

    public C1099453q(C68203Um r1, C68183Uk r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    @Override // X.AnonymousClass2E5
    public void AS6(Bitmap bitmap, C68203Um r4, boolean z) {
        if (this.A00) {
            return;
        }
        if (r4.A04 == 3) {
            this.A00 = true;
            C68203Um r1 = this.A01;
            r1.A08.AS6(bitmap, r1, z);
            return;
        }
        this.A01.A08.AS6(bitmap, r4, z);
    }
}
