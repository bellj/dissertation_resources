package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.2eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53532eb extends AnonymousClass04v {
    public final /* synthetic */ String A00;
    public final /* synthetic */ String A01;

    public C53532eb(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        r4.A0F("Button");
        AccessibilityNodeInfo accessibilityNodeInfo = r4.A02;
        accessibilityNodeInfo.setSelected(false);
        accessibilityNodeInfo.setContentDescription(this.A01);
        String str = this.A00;
        if (str != null) {
            C12970iu.A1O(r4, str);
        }
    }
}
