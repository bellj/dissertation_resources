package X;

import com.google.android.gms.common.api.Scope;

/* renamed from: X.4He  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88804He {
    public static final AbstractC77683ng A00;
    public static final AbstractC77683ng A01;
    public static final AnonymousClass4DN A02;
    public static final AnonymousClass4DN A03;
    public static final AnonymousClass1UE A04;
    public static final AnonymousClass1UE A05;

    static {
        AnonymousClass4DN r6 = new AnonymousClass4DN();
        A02 = r6;
        AnonymousClass4DN r5 = new AnonymousClass4DN();
        A03 = r5;
        C77653nd r4 = new C77653nd();
        A00 = r4;
        C77663ne r3 = new C77663ne();
        A01 = r3;
        new Scope(1, "profile");
        new Scope(1, "email");
        A04 = new AnonymousClass1UE(r4, r6, "SignIn.API");
        A05 = new AnonymousClass1UE(r3, r5, "SignIn.INTERNAL_API");
    }
}
