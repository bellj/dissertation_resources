package X;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.072  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass072 implements Cloneable {
    public static ThreadLocal A0L = new ThreadLocal();
    public static final AnonymousClass0LF A0M = new AnonymousClass0LF();
    public static final int[] A0N = {2, 1, 3, 4};
    public int A00 = 0;
    public long A01 = -1;
    public long A02 = -1;
    public TimeInterpolator A03 = null;
    public AnonymousClass0LF A04 = A0M;
    public AnonymousClass0OE A05;
    public AnonymousClass0LG A06;
    public C03060Fy A07 = null;
    public C04830Nf A08 = new C04830Nf();
    public C04830Nf A09 = new C04830Nf();
    public String A0A = getClass().getName();
    public ArrayList A0B = new ArrayList();
    public ArrayList A0C = new ArrayList();
    public ArrayList A0D;
    public ArrayList A0E = null;
    public ArrayList A0F;
    public ArrayList A0G = new ArrayList();
    public ArrayList A0H = new ArrayList();
    public boolean A0I = false;
    public boolean A0J = false;
    public int[] A0K = A0N;

    public Animator A0Q(ViewGroup viewGroup, C05350Pf r3, C05350Pf r4) {
        return null;
    }

    public String[] A0S() {
        return null;
    }

    public abstract void A0T(C05350Pf v);

    public abstract void A0U(C05350Pf v);

    public static AnonymousClass00N A01() {
        ThreadLocal threadLocal = A0L;
        AnonymousClass00N r0 = (AnonymousClass00N) threadLocal.get();
        if (r0 != null) {
            return r0;
        }
        AnonymousClass00N r02 = new AnonymousClass00N();
        threadLocal.set(r02);
        return r02;
    }

    public static void A02(View view, C05350Pf r7, C04830Nf r8) {
        r8.A02.put(view, r7);
        int id = view.getId();
        if (id >= 0) {
            SparseArray sparseArray = r8.A00;
            if (sparseArray.indexOfKey(id) >= 0) {
                sparseArray.put(id, null);
            } else {
                sparseArray.put(id, view);
            }
        }
        String A0J = AnonymousClass028.A0J(view);
        if (A0J != null) {
            AnonymousClass00N r1 = r8.A01;
            if (r1.containsKey(A0J)) {
                r1.put(A0J, null);
            } else {
                r1.put(A0J, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            ListView listView = (ListView) view.getParent();
            if (listView.getAdapter().hasStableIds()) {
                long itemIdAtPosition = listView.getItemIdAtPosition(listView.getPositionForView(view));
                AnonymousClass036 r4 = r8.A03;
                if (r4.A01) {
                    r4.A06();
                }
                if (AnonymousClass00R.A01(r4.A02, r4.A00, itemIdAtPosition) >= 0) {
                    View view2 = (View) r4.A04(itemIdAtPosition, null);
                    if (view2 != null) {
                        view2.setHasTransientState(false);
                        r4.A09(itemIdAtPosition, null);
                        return;
                    }
                    return;
                }
                view.setHasTransientState(true);
                r4.A09(itemIdAtPosition, view);
            }
        }
    }

    /* renamed from: A03 */
    public AnonymousClass072 clone() {
        try {
            AnonymousClass072 r1 = (AnonymousClass072) super.clone();
            r1.A0B = new ArrayList();
            r1.A09 = new C04830Nf();
            r1.A08 = new C04830Nf();
            r1.A0F = null;
            r1.A0D = null;
            return r1;
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }

    public AnonymousClass072 A04(long j) {
        this.A01 = j;
        return this;
    }

    public AnonymousClass072 A05(TimeInterpolator timeInterpolator) {
        this.A03 = timeInterpolator;
        return this;
    }

    public AnonymousClass072 A06(View view) {
        this.A0H.add(view);
        return this;
    }

    public AnonymousClass072 A07(View view) {
        this.A0H.remove(view);
        return this;
    }

    public AnonymousClass072 A08(AbstractC018608t r2) {
        ArrayList arrayList = this.A0E;
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.A0E = arrayList;
        }
        arrayList.add(r2);
        return this;
    }

    public AnonymousClass072 A09(AbstractC018608t r2) {
        ArrayList arrayList = this.A0E;
        if (arrayList != null) {
            arrayList.remove(r2);
            if (this.A0E.size() == 0) {
                this.A0E = null;
            }
        }
        return this;
    }

    public C05350Pf A0A(View view, boolean z) {
        ArrayList arrayList;
        ArrayList arrayList2;
        C03060Fy r0 = this.A07;
        if (r0 != null) {
            return r0.A0A(view, z);
        }
        if (z) {
            arrayList = this.A0F;
        } else {
            arrayList = this.A0D;
        }
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            C05350Pf r1 = (C05350Pf) arrayList.get(i);
            if (r1 == null) {
                return null;
            }
            if (r1.A00 == view) {
                if (i < 0) {
                    return null;
                } else {
                    if (z) {
                        arrayList2 = this.A0D;
                    } else {
                        arrayList2 = this.A0F;
                    }
                    return (C05350Pf) arrayList2.get(i);
                }
            }
        }
        return null;
    }

    public C05350Pf A0B(View view, boolean z) {
        C04830Nf r0;
        C03060Fy r02 = this.A07;
        if (r02 != null) {
            return r02.A0B(view, z);
        }
        if (z) {
            r0 = this.A09;
        } else {
            r0 = this.A08;
        }
        return (C05350Pf) r0.A02.get(view);
    }

    public String A0C(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(getClass().getSimpleName());
        sb.append("@");
        sb.append(Integer.toHexString(hashCode()));
        sb.append(": ");
        String obj = sb.toString();
        long j = this.A01;
        if (j != -1) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append("dur(");
            sb2.append(j);
            sb2.append(") ");
            obj = sb2.toString();
        }
        long j2 = this.A02;
        if (j2 != -1) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(obj);
            sb3.append("dly(");
            sb3.append(j2);
            sb3.append(") ");
            obj = sb3.toString();
        }
        TimeInterpolator timeInterpolator = this.A03;
        if (timeInterpolator != null) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(obj);
            sb4.append("interp(");
            sb4.append(timeInterpolator);
            sb4.append(") ");
            obj = sb4.toString();
        }
        ArrayList arrayList = this.A0G;
        if (arrayList.size() <= 0 && this.A0H.size() <= 0) {
            return obj;
        }
        StringBuilder sb5 = new StringBuilder();
        sb5.append(obj);
        sb5.append("tgts(");
        String obj2 = sb5.toString();
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (i > 0) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append(obj2);
                    sb6.append(", ");
                    obj2 = sb6.toString();
                }
                StringBuilder sb7 = new StringBuilder();
                sb7.append(obj2);
                sb7.append(arrayList.get(i));
                obj2 = sb7.toString();
            }
        }
        ArrayList arrayList2 = this.A0H;
        if (arrayList2.size() > 0) {
            for (int i2 = 0; i2 < arrayList2.size(); i2++) {
                if (i2 > 0) {
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(obj2);
                    sb8.append(", ");
                    obj2 = sb8.toString();
                }
                StringBuilder sb9 = new StringBuilder();
                sb9.append(obj2);
                sb9.append(arrayList2.get(i2));
                obj2 = sb9.toString();
            }
        }
        StringBuilder sb10 = new StringBuilder();
        sb10.append(obj2);
        sb10.append(")");
        return sb10.toString();
    }

    public void A0D() {
        int i = this.A00 - 1;
        this.A00 = i;
        if (i == 0) {
            ArrayList arrayList = this.A0E;
            if (arrayList != null && arrayList.size() > 0) {
                AbstractList abstractList = (AbstractList) arrayList.clone();
                int size = abstractList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ((AbstractC018608t) abstractList.get(i2)).AXq(this);
                }
            }
            int i3 = 0;
            while (true) {
                AnonymousClass036 r1 = this.A09.A03;
                if (i3 >= r1.A00()) {
                    break;
                }
                View view = (View) r1.A03(i3);
                if (view != null) {
                    view.setHasTransientState(false);
                }
                i3++;
            }
            int i4 = 0;
            while (true) {
                AnonymousClass036 r12 = this.A08.A03;
                if (i4 < r12.A00()) {
                    View view2 = (View) r12.A03(i4);
                    if (view2 != null) {
                        view2.setHasTransientState(false);
                    }
                    i4++;
                } else {
                    this.A0I = true;
                    return;
                }
            }
        }
    }

    public void A0E() {
        A0F();
        AnonymousClass00N A01 = A01();
        Iterator it = this.A0B.iterator();
        while (it.hasNext()) {
            Animator animator = (Animator) it.next();
            if (A01.containsKey(animator)) {
                A0F();
                if (animator != null) {
                    animator.addListener(new AnonymousClass09F(A01, this));
                    long j = this.A01;
                    if (j >= 0) {
                        animator.setDuration(j);
                    }
                    long j2 = this.A02;
                    if (j2 >= 0) {
                        animator.setStartDelay(j2);
                    }
                    TimeInterpolator timeInterpolator = this.A03;
                    if (timeInterpolator != null) {
                        animator.setInterpolator(timeInterpolator);
                    }
                    animator.addListener(new AnonymousClass094(this));
                    animator.start();
                }
            }
        }
        this.A0B.clear();
        A0D();
    }

    public void A0F() {
        if (this.A00 == 0) {
            ArrayList arrayList = this.A0E;
            if (arrayList != null && arrayList.size() > 0) {
                AbstractList abstractList = (AbstractList) arrayList.clone();
                int size = abstractList.size();
                for (int i = 0; i < size; i++) {
                    ((AbstractC018608t) abstractList.get(i)).AXt(this);
                }
            }
            this.A0I = false;
        }
        this.A00++;
    }

    public void A0G(View view) {
        if (!this.A0I) {
            AnonymousClass00N A01 = A01();
            int size = A01.size();
            AbstractC11380gC A00 = AnonymousClass0U3.A00(view);
            for (int i = size - 1; i >= 0; i--) {
                C04960Ns r1 = (C04960Ns) A01.A02[(i << 1) + 1];
                if (r1.A00 != null && A00.equals(r1.A03)) {
                    Animator animator = (Animator) A01.A02[i << 1];
                    if (Build.VERSION.SDK_INT >= 19) {
                        animator.pause();
                    } else {
                        ArrayList<Animator.AnimatorListener> listeners = animator.getListeners();
                        if (listeners != null) {
                            int size2 = listeners.size();
                            for (int i2 = 0; i2 < size2; i2++) {
                                Animator.AnimatorListener animatorListener = listeners.get(i2);
                                if (animatorListener instanceof AbstractC12400hr) {
                                    ((AbstractC12400hr) animatorListener).onAnimationPause(animator);
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = this.A0E;
            if (arrayList != null && arrayList.size() > 0) {
                AbstractList abstractList = (AbstractList) arrayList.clone();
                int size3 = abstractList.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    ((AbstractC018608t) abstractList.get(i3)).AXr(this);
                }
            }
            this.A0J = true;
        }
    }

    public void A0H(View view) {
        if (this.A0J) {
            if (!this.A0I) {
                AnonymousClass00N A01 = A01();
                int size = A01.size();
                AbstractC11380gC A00 = AnonymousClass0U3.A00(view);
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    int i = size << 1;
                    C04960Ns r1 = (C04960Ns) A01.A02[i + 1];
                    if (r1.A00 != null && A00.equals(r1.A03)) {
                        Animator animator = (Animator) A01.A02[i];
                        if (Build.VERSION.SDK_INT >= 19) {
                            animator.resume();
                        } else {
                            ArrayList<Animator.AnimatorListener> listeners = animator.getListeners();
                            if (listeners != null) {
                                int size2 = listeners.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    Animator.AnimatorListener animatorListener = listeners.get(i2);
                                    if (animatorListener instanceof AbstractC12400hr) {
                                        ((AbstractC12400hr) animatorListener).onAnimationResume(animator);
                                    }
                                }
                            }
                        }
                    }
                }
                ArrayList arrayList = this.A0E;
                if (arrayList != null && arrayList.size() > 0) {
                    AbstractList abstractList = (AbstractList) arrayList.clone();
                    int size3 = abstractList.size();
                    for (int i3 = 0; i3 < size3; i3++) {
                        ((AbstractC018608t) abstractList.get(i3)).AXs(this);
                    }
                }
            }
            this.A0J = false;
        }
    }

    public final void A0I(View view, boolean z) {
        C04830Nf r0;
        if (view != null) {
            view.getId();
            if (view.getParent() instanceof ViewGroup) {
                C05350Pf r1 = new C05350Pf();
                r1.A00 = view;
                if (z) {
                    A0U(r1);
                } else {
                    A0T(r1);
                }
                r1.A01.add(this);
                A0O(r1);
                if (z) {
                    r0 = this.A09;
                } else {
                    r0 = this.A08;
                }
                A02(view, r1, r0);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    A0I(viewGroup.getChildAt(i), z);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:0x01ff, code lost:
        if (r5 == null) goto L_0x0201;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x017e, code lost:
        if (X.AnonymousClass028.A05(r30) == 1) goto L_0x0180;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01c5, code lost:
        if (X.AnonymousClass028.A05(r30) == 1) goto L_0x01c7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x01f4  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0045 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01bc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0J(android.view.ViewGroup r30, X.C04830Nf r31, X.C04830Nf r32, java.util.ArrayList r33, java.util.ArrayList r34) {
        /*
        // Method dump skipped, instructions count: 574
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass072.A0J(android.view.ViewGroup, X.0Nf, X.0Nf, java.util.ArrayList, java.util.ArrayList):void");
    }

    public void A0K(ViewGroup viewGroup, boolean z) {
        C04830Nf r0;
        C04830Nf r02;
        C04830Nf r03;
        if (z) {
            this.A09.A02.clear();
            this.A09.A00.clear();
            r0 = this.A09;
        } else {
            this.A08.A02.clear();
            this.A08.A00.clear();
            r0 = this.A08;
        }
        r0.A03.A05();
        ArrayList arrayList = this.A0G;
        if (arrayList.size() > 0 || this.A0H.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                View findViewById = viewGroup.findViewById(((Number) arrayList.get(i)).intValue());
                if (findViewById != null) {
                    C05350Pf r1 = new C05350Pf();
                    r1.A00 = findViewById;
                    if (z) {
                        A0U(r1);
                    } else {
                        A0T(r1);
                    }
                    r1.A01.add(this);
                    A0O(r1);
                    if (z) {
                        r03 = this.A09;
                    } else {
                        r03 = this.A08;
                    }
                    A02(findViewById, r1, r03);
                }
            }
            int i2 = 0;
            while (true) {
                ArrayList arrayList2 = this.A0H;
                if (i2 < arrayList2.size()) {
                    View view = (View) arrayList2.get(i2);
                    C05350Pf r12 = new C05350Pf();
                    r12.A00 = view;
                    if (z) {
                        A0U(r12);
                    } else {
                        A0T(r12);
                    }
                    r12.A01.add(this);
                    A0O(r12);
                    if (z) {
                        r02 = this.A09;
                    } else {
                        r02 = this.A08;
                    }
                    A02(view, r12, r02);
                    i2++;
                } else {
                    return;
                }
            }
        } else {
            A0I(viewGroup, z);
        }
    }

    public void A0L(AnonymousClass0LF r1) {
        if (r1 == null) {
            r1 = A0M;
        }
        this.A04 = r1;
    }

    public void A0M(AnonymousClass0OE r1) {
        this.A05 = r1;
    }

    public void A0N(AnonymousClass0LG r1) {
        this.A06 = r1;
    }

    public void A0O(C05350Pf r8) {
        if (this.A06 != null) {
            Map map = r8.A02;
            if (!map.isEmpty()) {
                for (String str : AnonymousClass0G6.A01) {
                    if (!map.containsKey(str)) {
                        View view = r8.A00;
                        Object obj = map.get("android:visibility:visibility");
                        if (obj == null) {
                            obj = Integer.valueOf(view.getVisibility());
                        }
                        map.put("android:visibilityPropagation:visibility", obj);
                        int[] iArr = new int[2];
                        view.getLocationOnScreen(iArr);
                        int round = iArr[0] + Math.round(view.getTranslationX());
                        iArr[0] = round;
                        iArr[0] = round + (view.getWidth() / 2);
                        int round2 = iArr[1] + Math.round(view.getTranslationY());
                        iArr[1] = round2;
                        iArr[1] = round2 + (view.getHeight() / 2);
                        map.put("android:visibilityPropagation:center", iArr);
                        return;
                    }
                }
            }
        }
    }

    public boolean A0P(View view) {
        int id = view.getId();
        ArrayList arrayList = this.A0G;
        if ((arrayList.size() != 0 || this.A0H.size() != 0) && !arrayList.contains(Integer.valueOf(id)) && !this.A0H.contains(view)) {
            return false;
        }
        return true;
    }

    public boolean A0R(C05350Pf r8, C05350Pf r9) {
        if (r8 == null || r9 == null) {
            return false;
        }
        String[] A0S = A0S();
        if (A0S != null) {
            for (String str : A0S) {
                Object obj = r8.A02.get(str);
                Object obj2 = r9.A02.get(str);
                if (obj == null) {
                    if (obj2 == null) {
                    }
                } else if (obj2 != null && !(true ^ obj.equals(obj2))) {
                }
            }
            return false;
        }
        Map map = r8.A02;
        for (Object obj3 : map.keySet()) {
            Object obj4 = map.get(obj3);
            Object obj5 = r9.A02.get(obj3);
            if (obj4 == null) {
                if (obj5 == null) {
                }
            } else if (obj5 != null && !(true ^ obj4.equals(obj5))) {
            }
        }
        return false;
        return true;
    }

    @Override // java.lang.Object
    public String toString() {
        return A0C("");
    }
}
