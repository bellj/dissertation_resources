package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1gF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34501gF extends AnonymousClass1JQ {
    public final UserJid A00;
    public final String A01;
    public final String A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        if (r1[0].equals(r2) != false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C34501gF(X.C27791Jf r13, X.AnonymousClass1JR r14, com.whatsapp.jid.UserJid r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, long r19) {
        /*
            r12 = this;
            r2 = r17
            r1 = r18
            r8 = 2
            r11 = 0
            java.lang.String r7 = "critical_unblock_low"
            r3 = r12
            r6 = r16
            r4 = r13
            r9 = r19
            r5 = r14
            r3.<init>(r4, r5, r6, r7, r8, r9, r11)
            r12.A00 = r15
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 == 0) goto L_0x001c
            java.lang.String r1 = ""
        L_0x001c:
            r12.A01 = r1
            if (r17 == 0) goto L_0x0034
            java.lang.String r0 = " "
            java.lang.String[] r1 = r1.split(r0)
            int r0 = r1.length
            if (r0 == 0) goto L_0x0031
            r0 = r1[r11]
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0034
        L_0x0031:
            r12.A02 = r2
            return
        L_0x0034:
            java.lang.String r2 = ""
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34501gF.<init>(X.1Jf, X.1JR, com.whatsapp.jid.UserJid, java.lang.String, java.lang.String, java.lang.String, long):void");
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        if (this.A05.equals(C27791Jf.A02)) {
            return null;
        }
        AnonymousClass1G4 A0T = C34491gE.A03.A0T();
        String str = this.A01;
        A0T.A03();
        C34491gE r1 = (C34491gE) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = str;
        String str2 = this.A02;
        if (!TextUtils.isEmpty(str2)) {
            A0T.A03();
            C34491gE r12 = (C34491gE) A0T.A00;
            r12.A00 |= 2;
            r12.A01 = str2;
        }
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r13 = (C27831Jk) A01.A00;
        r13.A06 = (C34491gE) A0T.A02();
        r13.A00 |= 4;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("ContactMutation{rowId=");
        sb.append(this.A07);
        sb.append(", contactJid=");
        sb.append(this.A00);
        sb.append(", givenName=");
        sb.append(this.A02);
        sb.append(", displayName=");
        sb.append(this.A01);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
