package X;

import android.content.SharedPreferences;

/* renamed from: X.5xw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129575xw {
    public SharedPreferences A00;
    public final C30931Zj A01 = C117305Zk.A0V("PaymentProviderKeySharedPrefs", "infra");
    public final C16630pM A02;

    public C129575xw(C16630pM r3) {
        this.A02 = r3;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("com.whatsapp_payment_provider_key_preferences");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public void A01(String str, String str2) {
        SharedPreferences.Editor edit = A00().edit();
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append("::");
        edit.remove(C12960it.A0d(str2, A0j)).apply();
    }
}
