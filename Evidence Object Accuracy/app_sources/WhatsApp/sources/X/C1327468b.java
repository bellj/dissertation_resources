package X;

import com.whatsapp.R;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.68b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1327468b implements AnonymousClass1FK {
    public final /* synthetic */ AbstractActivityC121655j9 A00;

    public C1327468b(AbstractActivityC121655j9 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        AbstractActivityC121655j9 r1 = this.A00;
        r1.AaN();
        r1.A3L(r1.A08);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        AbstractActivityC121655j9 r1 = this.A00;
        r1.AaN();
        r1.A3L(r1.A08);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        if (!r5.A02) {
            AbstractActivityC121655j9 r2 = this.A00;
            C12970iu.A0M(r2, R.id.unlink_payment_accounts_title).setText(R.string.payment_account_not_unlinked);
            r2.findViewById(R.id.unlink_payment_accounts_desc).setVisibility(8);
            r2.Ado(R.string.payment_account_not_unlinked);
            return;
        }
        AbstractActivityC121655j9 r3 = this.A00;
        try {
            C18600si r22 = ((AbstractActivityC121665jA) r3).A0B.A03;
            JSONObject A0b = C117295Zj.A0b(r22);
            A0b.remove("smsVerifDataSentToPsp");
            A0b.remove("smsVerifData");
            C117295Zj.A1E(r22, A0b);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteSmsVerificationData threw: ", e);
        }
        r3.A2q();
        r3.AaN();
        r3.A2G(r3.A3H(), true);
    }
}
