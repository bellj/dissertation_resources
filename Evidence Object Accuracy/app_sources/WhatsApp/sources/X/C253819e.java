package X;

import android.util.JsonReader;

/* renamed from: X.19e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C253819e extends AbstractC253919f {
    public C253819e(C18790t3 r1, C14830m7 r2, AnonymousClass018 r3, C20250vS r4, AnonymousClass197 r5, C16120oU r6, C253719d r7, C19930uu r8, AbstractC14440lR r9) {
        super(r1, r2, r3, r4, r5, r6, r7, r8, r9);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 3, insn: 0x03c3: IPUT  (r0v2 ?? I:java.lang.Long), (r3 I:X.318) X.318.A05 java.lang.Long, block:B:172:0x03b7
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static /* synthetic */ X.AnonymousClass01T A00(
/*
[1042] Method generation error in method: X.19e.A00(X.318, X.19e, java.lang.String):X.01T, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r22v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public static final C66003Lx A01(JsonReader jsonReader) {
        jsonReader.beginObject();
        String str = null;
        int i = -1;
        int i2 = -1;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            switch (nextName.hashCode()) {
                case -1221029593:
                    if (!nextName.equals("height")) {
                        break;
                    } else {
                        i2 = Integer.parseInt(jsonReader.nextString());
                        continue;
                    }
                case 116079:
                    if (!nextName.equals("url")) {
                        break;
                    } else {
                        str = jsonReader.nextString();
                        continue;
                    }
                case 113126854:
                    if (!nextName.equals("width")) {
                        break;
                    } else {
                        i = Integer.parseInt(jsonReader.nextString());
                        continue;
                    }
            }
            jsonReader.skipValue();
        }
        jsonReader.endObject();
        if (str != null) {
            return new C66003Lx(i, str, i2);
        }
        return null;
    }
}
