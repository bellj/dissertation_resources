package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.17l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C249317l {
    public C20740wF A00;
    public final AnonymousClass016 A01 = new AnonymousClass016(0);
    public final AbstractC15710nm A02;
    public final C16510p9 A03;
    public final C19990v2 A04;
    public final C18460sU A05;
    public final C16490p7 A06;
    public final C18290sD A07;
    public final C21630xj A08;

    public C249317l(AbstractC15710nm r3, C16510p9 r4, C19990v2 r5, C18460sU r6, C16490p7 r7, C18290sD r8, C21630xj r9) {
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
        this.A08 = r9;
        this.A06 = r7;
        this.A07 = r8;
    }

    public synchronized int A00() {
        return ((Integer) this.A01.A01()).intValue();
    }

    public void A01() {
        C16490p7 r6 = this.A06;
        r6.A04();
        if (r6.A01) {
            HashSet hashSet = new HashSet();
            C21630xj r8 = this.A08;
            for (AbstractC18500sY r3 : new ArrayList(r8.A00().A00.values())) {
                if (r3.A0Q() && r3.A03() == 3 && !r3.A0O()) {
                    hashSet.add(r3.A0C);
                }
            }
            boolean z = !hashSet.isEmpty();
            if (!z) {
                for (AbstractC18500sY r32 : new ArrayList(r8.A00().A00.values())) {
                    if (r32.A0P() && r32.A03() == 3 && !r32.A0O()) {
                        hashSet.add(r32.A0C);
                    }
                }
            }
            if (!hashSet.isEmpty()) {
                C18290sD r82 = this.A07;
                if (!r82.A0C.get()) {
                    AnonymousClass016 r1 = this.A01;
                    if (((Number) r1.A01()).intValue() == 0) {
                        r1.A0A(1);
                        this.A00.A03(false);
                        r6.A04();
                        ReentrantReadWriteLock.WriteLock writeLock = r6.A08;
                        writeLock.lock();
                        this.A04.A0B();
                        StringBuilder sb = new StringBuilder("ForcedDBMigration/running forced migrations. blocking = ");
                        sb.append(z);
                        Log.i(sb.toString());
                        try {
                            C29001Pw r2 = new C29001Pw(new AbstractC28981Pu[0]);
                            int i = 2;
                            if (z) {
                                i = 3;
                            }
                            r82.A03(r2, hashSet, 7, i);
                        } finally {
                            if (A02(hashSet, z) || !z) {
                                r6.A04();
                                writeLock.unlock();
                                this.A00.A03(true);
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean A02(Set set, boolean z) {
        boolean z2;
        AnonymousClass016 r1;
        int i;
        if (!this.A05.A0C()) {
            Log.e("ForcedDBMigration/failed to migrate jidStore");
            z2 = false;
        } else {
            z2 = true;
        }
        if (!this.A03.A0G()) {
            Log.e("ForcedDBMigration/failed to migrate chatStore");
            z2 = false;
        }
        Iterator it = set.iterator();
        boolean z3 = false;
        while (it.hasNext()) {
            String str = (String) it.next();
            AbstractC18500sY A01 = this.A08.A01(str);
            if (A01 != null && !A01.A0O()) {
                StringBuilder sb = new StringBuilder("ForcedDBMigration/failed to migrate ");
                sb.append(str);
                Log.e(sb.toString());
                if (z3 || A01.A0M()) {
                    z2 = false;
                } else {
                    z2 = false;
                    z3 = true;
                }
            }
        }
        if (z2) {
            Log.i("ForcedDBMigration/successfully migrated all forced migration");
            r1 = this.A01;
            i = 5;
        } else {
            StringBuilder sb2 = new StringBuilder("ForcedDBMigration/failed to migrate all forced migration. blocking = ");
            sb2.append(z);
            Log.i(sb2.toString());
            this.A02.AaV("ForcedDatabaseMigrationManager/failedToMigrate", "failedToMigrate", true);
            r1 = this.A01;
            if (!z) {
                i = 2;
            } else {
                i = 4;
                if (z3) {
                    i = 3;
                }
            }
        }
        r1.A0A(Integer.valueOf(i));
        return z2;
    }
}
