package X;

import android.content.Context;

/* renamed from: X.5gh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120755gh extends C120895gv {
    public final /* synthetic */ AbstractC136246Lu A00;
    public final /* synthetic */ C120505gI A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120755gh(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, AbstractC136246Lu r11, C120505gI r12) {
        super(context, r8, r9, r10, "upi-generate-otp");
        this.A01 = r12;
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r2) {
        super.A02(r2);
        AbstractC136246Lu r0 = this.A00;
        if (r0 != null) {
            r0.AV4(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r2) {
        super.A03(r2);
        AbstractC136246Lu r0 = this.A00;
        if (r0 != null) {
            r0.AV4(r2);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r3) {
        super.A04(r3);
        AbstractC136246Lu r1 = this.A00;
        if (r1 != null) {
            r1.AV4(null);
        }
    }
}
