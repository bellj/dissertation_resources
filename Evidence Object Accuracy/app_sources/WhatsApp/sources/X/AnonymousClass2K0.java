package X;

import com.facebook.redex.ViewOnClickCListenerShape7S0100000_I1_1;
import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;
import com.whatsapp.util.ViewOnClickCListenerShape2S0201000_I1;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2K0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2K0 implements AnonymousClass2K1 {
    public AbstractC16320oo A00;
    public AnonymousClass54N A01;
    public C68473Vn A02;
    public C68463Vm A03;
    public C68483Vo A04;
    public AbstractC116565Vy A05;
    public C68553Vv A06;
    public AnonymousClass4N8 A07;
    public AnonymousClass4RU A08;
    public BusinessDirectorySearchQueryViewModel A09;
    public final C251118d A0A;
    public final AnonymousClass2J6 A0B;
    public final AnonymousClass2J4 A0C;
    public final AnonymousClass2J8 A0D;
    public final AnonymousClass2JA A0E;
    public final AnonymousClass2J2 A0F;
    public final AnonymousClass1B5 A0G;
    public final C15550nR A0H;

    public AnonymousClass2K0(C251118d r1, AnonymousClass2J6 r2, AnonymousClass2J4 r3, AnonymousClass2J8 r4, AnonymousClass2JA r5, AnonymousClass2J2 r6, AnonymousClass1B5 r7, C15550nR r8) {
        this.A0H = r8;
        this.A0A = r1;
        this.A0G = r7;
        this.A0D = r4;
        this.A0F = r6;
        this.A0C = r3;
        this.A0B = r2;
        this.A0E = r5;
    }

    public final void A00() {
        AbstractC16320oo r0 = this.A00;
        if (r0 != null && r0.A0A.A00() != 2 && !this.A00.A0A.A02.isCancelled()) {
            this.A00.A0A.A03(true);
            this.A00 = null;
        }
    }

    public void A01(AnonymousClass2K4 r12, AnonymousClass2K3 r13, C48122Ek r14) {
        if (!(this.A00 instanceof C59432ui)) {
            A00();
        }
        C68473Vn r5 = new C68473Vn(r14, this);
        this.A02 = r5;
        C59452uk A85 = this.A0B.A85(r12, r13, r14, r5, this.A0G.A00, null, "all_descendents", "all_descendents", "nearby");
        A85.A03();
        this.A00 = A85;
    }

    public void A02(AnonymousClass2K4 r12, AnonymousClass2K3 r13, C48122Ek r14, String str, boolean z, boolean z2) {
        AnonymousClass2J8 r1;
        AnonymousClass1B4 r6;
        String str2;
        boolean z3 = z2;
        C68503Vq r5 = new C68503Vq(r14, this, str, z3);
        C14850m9 r15 = this.A0A.A00;
        if (!r15.A07(450) || !r15.A07(1705) || z2) {
            r1 = this.A0D;
            r6 = this.A0G.A00;
            str2 = null;
        } else {
            r1 = this.A0D;
            r6 = this.A0G.A00;
            z3 = false;
            r1.A86(r12, r13, r14, r5, r6, str, "typeahead_business", z, false).A03();
            str2 = "typeahead_category";
        }
        r1.A86(r12, r13, r14, r5, r6, str, str2, z, z3).A03();
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        AnonymousClass4N8 r2 = this.A07;
        if (r2 != null) {
            AnonymousClass4RT r1 = r2.A01;
            r1.A01 = 4;
            r1.A00 = i;
            r2.A00.A0A(r1);
        }
    }

    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        AnonymousClass4RT r1;
        List list = (List) obj;
        AnonymousClass4N8 r5 = this.A07;
        if (r5 != null) {
            if (list.isEmpty()) {
                r1 = r5.A01;
                r1.A01 = 3;
            } else {
                ArrayList arrayList = new ArrayList();
                int i = 0;
                while (i < list.size()) {
                    C30211Wn r6 = (C30211Wn) list.get(i);
                    i++;
                    arrayList.add(new C59602v0(new ViewOnClickCListenerShape2S0201000_I1(r5, r6, i, 0), r6.A00, r6.A01));
                }
                arrayList.add(new C84753zr());
                arrayList.add(new C84733zp());
                arrayList.add(new C59492up(new ViewOnClickCListenerShape7S0100000_I1_1(r5, 16)));
                r1 = r5.A01;
                r1.A01 = 1;
                List list2 = r1.A03;
                list2.clear();
                list2.addAll(arrayList);
            }
            r5.A00.A0A(r1);
        }
    }
}
