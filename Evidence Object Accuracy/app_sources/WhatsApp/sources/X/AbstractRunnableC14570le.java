package X;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* renamed from: X.0le  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractRunnableC14570le extends C14580lf implements Runnable, AbstractC14600lh {
    public final C14620lj A00 = new C14620lj();
    public final CountDownLatch A01 = new CountDownLatch(1);
    public final FutureTask A02 = new AnonymousClass5IS(this, new Callable() { // from class: X.5DY
        @Override // java.util.concurrent.Callable
        public final Object call() {
            return AbstractRunnableC14570le.this.A05();
        }
    });

    @Override // X.C14580lf
    public void A04() {
        super.A04();
        this.A00.A01();
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public java.lang.Object A05() {
        /*
        // Method dump skipped, instructions count: 2930
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractRunnableC14570le.A05():java.lang.Object");
    }

    public void A06() {
        if (this.A02.isCancelled()) {
            throw new CancellationException();
        }
    }

    @Override // X.AbstractC14600lh
    public void cancel() {
        this.A02.cancel(true);
    }

    @Override // java.lang.Runnable
    public void run() {
        Throwable e;
        Throwable e2;
        try {
            FutureTask futureTask = this.A02;
            futureTask.run();
            try {
                boolean interrupted = Thread.interrupted();
                this.A01.await();
                if (interrupted) {
                    Thread.currentThread().interrupt();
                }
                Object obj = futureTask.get();
                AnonymousClass009.A05(obj);
                A02(obj);
            } catch (InterruptedException | CancellationException e3) {
                e2 = e3;
                A03(e2);
            } catch (ExecutionException e4) {
                e2 = e4.getCause();
                A03(e2);
            }
        } catch (Throwable th) {
            try {
                boolean interrupted2 = Thread.interrupted();
                this.A01.await();
                if (interrupted2) {
                    Thread.currentThread().interrupt();
                }
                Object obj2 = this.A02.get();
                AnonymousClass009.A05(obj2);
                A02(obj2);
                throw th;
            } catch (InterruptedException | CancellationException e5) {
                e = e5;
                A03(e);
                throw th;
            } catch (ExecutionException e6) {
                e = e6.getCause();
                A03(e);
                throw th;
            }
        }
    }
}
