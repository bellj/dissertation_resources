package X;

import android.view.View;
import android.widget.AbsListView;

/* renamed from: X.0Wr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07100Wr implements AbsListView.OnScrollListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass0U5 A02;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public C07100Wr(View view, View view2, AnonymousClass0U5 r3) {
        this.A02 = r3;
        this.A01 = view;
        this.A00 = view2;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        AnonymousClass0U5.A01(absListView, this.A01, this.A00);
    }
}
