package X;

import java.util.HashMap;

/* renamed from: X.4RG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4RG {
    public final C92364Vp A00;
    public final Object A01;
    public final Object A02;
    public final HashMap A03;

    public AnonymousClass4RG(C92364Vp r1, Object obj, Object obj2, HashMap hashMap) {
        this.A01 = obj;
        this.A02 = obj2;
        this.A00 = r1;
        this.A03 = hashMap;
    }
}
