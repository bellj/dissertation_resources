package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC869349n extends Enum {
    public static final /* synthetic */ EnumC869349n[] A00;
    public static final EnumC869349n A01;
    public static final EnumC869349n A02;
    public static final EnumC869349n A03;
    public static final EnumC869349n A04;
    public static final EnumC869349n A05;
    public static final EnumC869349n A06;
    public static final EnumC869349n A07;
    public static final EnumC869349n A08;
    public static final EnumC869349n A09;
    public final int id;
    public final EnumC869249m zzix;
    public final EnumC868949j zziy;
    public final Class zziz;
    public final boolean zzja;

    public static EnumC869349n[] values() {
        return (EnumC869349n[]) A00.clone();
    }

    static {
        EnumC868949j r6 = EnumC868949j.A01;
        EnumC869249m r3 = EnumC869249m.A05;
        EnumC869349n A002 = A00(r6, r3, "DOUBLE", 0);
        EnumC869249m r7 = EnumC869249m.A04;
        EnumC869349n A003 = A00(r6, r7, "FLOAT", 1);
        EnumC869249m r12 = EnumC869249m.A03;
        EnumC869349n A004 = A00(r6, r12, "INT64", 2);
        EnumC869349n A005 = A00(r6, r12, "UINT64", 3);
        EnumC869249m r13 = EnumC869249m.A02;
        EnumC869349n A006 = A00(r6, r13, "INT32", 4);
        EnumC869349n A007 = A00(r6, r12, "FIXED64", 5);
        EnumC869349n A008 = A00(r6, r13, "FIXED32", 6);
        EnumC869249m r15 = EnumC869249m.A06;
        EnumC869349n A009 = A00(r6, r15, "BOOL", 7);
        EnumC869249m r9 = EnumC869249m.A07;
        EnumC869349n A0010 = A00(r6, r9, "STRING", 8);
        EnumC869249m r5 = EnumC869249m.A0A;
        EnumC869349n A0011 = A00(r6, r5, "MESSAGE", 9);
        A01 = A0011;
        EnumC869249m r8 = EnumC869249m.A08;
        EnumC869349n A0012 = A00(r6, r8, "BYTES", 10);
        EnumC869349n A0013 = A00(r6, r13, "UINT32", 11);
        EnumC869249m r14 = EnumC869249m.A09;
        EnumC869349n A0014 = A00(r6, r14, "ENUM", 12);
        A02 = A0014;
        EnumC869349n A0015 = A00(r6, r13, "SFIXED32", 13);
        EnumC869349n A0016 = A00(r6, r12, "SFIXED64", 14);
        EnumC869349n A0017 = A00(r6, r13, "SINT32", 15);
        EnumC869349n A0018 = A00(r6, r12, "SINT64", 16);
        EnumC869349n A0019 = A00(r6, r5, "GROUP", 17);
        A03 = A0019;
        EnumC868949j r4 = EnumC868949j.A02;
        EnumC869349n A0020 = A00(r4, r3, "DOUBLE_LIST", 18);
        A04 = A0020;
        EnumC869349n A0021 = A00(r4, r7, "FLOAT_LIST", 19);
        EnumC869349n A0022 = A00(r4, r12, "INT64_LIST", 20);
        EnumC869349n A0023 = A00(r4, r12, "UINT64_LIST", 21);
        EnumC869349n A0024 = A00(r4, r13, "INT32_LIST", 22);
        EnumC869349n A0025 = A00(r4, r12, "FIXED64_LIST", 23);
        EnumC869349n A0026 = A00(r4, r13, "FIXED32_LIST", 24);
        EnumC869349n A0027 = A00(r4, r15, "BOOL_LIST", 25);
        EnumC869349n A0028 = A00(r4, r9, "STRING_LIST", 26);
        EnumC869349n A0029 = A00(r4, r5, "MESSAGE_LIST", 27);
        A05 = A0029;
        EnumC869349n A0030 = A00(r4, r8, "BYTES_LIST", 28);
        EnumC869349n A0031 = A00(r4, r13, "UINT32_LIST", 29);
        EnumC869349n A0032 = A00(r4, r14, "ENUM_LIST", 30);
        A06 = A0032;
        EnumC869349n A0033 = A00(r4, r13, "SFIXED32_LIST", 31);
        EnumC869349n A0034 = A00(r4, r12, "SFIXED64_LIST", 32);
        EnumC869349n A0035 = A00(r4, r13, "SINT32_LIST", 33);
        EnumC869349n A0036 = A00(r4, r12, "SINT64_LIST", 34);
        EnumC868949j r62 = EnumC868949j.A03;
        EnumC869349n A0037 = A00(r62, r3, "DOUBLE_LIST_PACKED", 35);
        EnumC869349n A0038 = A00(r62, r7, "FLOAT_LIST_PACKED", 36);
        EnumC869349n A0039 = A00(r62, r12, "INT64_LIST_PACKED", 37);
        EnumC869349n A0040 = A00(r62, r12, "UINT64_LIST_PACKED", 38);
        EnumC869349n A0041 = A00(r62, r13, "INT32_LIST_PACKED", 39);
        EnumC869349n A0042 = A00(r62, r12, "FIXED64_LIST_PACKED", 40);
        EnumC869349n A0043 = A00(r62, r13, "FIXED32_LIST_PACKED", 41);
        EnumC869349n A0044 = A00(r62, r15, "BOOL_LIST_PACKED", 42);
        EnumC869349n A0045 = A00(r62, r13, "UINT32_LIST_PACKED", 43);
        EnumC869349n A0046 = A00(r62, r14, "ENUM_LIST_PACKED", 44);
        A07 = A0046;
        EnumC869349n A0047 = A00(r62, r13, "SFIXED32_LIST_PACKED", 45);
        EnumC869349n A0048 = A00(r62, r12, "SFIXED64_LIST_PACKED", 46);
        EnumC869349n A0049 = A00(r62, r13, "SINT32_LIST_PACKED", 47);
        EnumC869349n A0050 = A00(r62, r12, "SINT64_LIST_PACKED", 48);
        EnumC869349n A0051 = A00(r4, r5, "GROUP_LIST", 49);
        A08 = A0051;
        EnumC869349n A0052 = A00(EnumC868949j.A04, EnumC869249m.A01, "MAP", 50);
        A09 = A0052;
        EnumC869349n[] r52 = new EnumC869349n[51];
        r52[0] = A002;
        r52[1] = A003;
        r52[2] = A004;
        r52[3] = A005;
        r52[4] = A006;
        r52[5] = A007;
        r52[6] = A008;
        r52[7] = A009;
        r52[8] = A0010;
        r52[9] = A0011;
        r52[10] = A0012;
        r52[11] = A0013;
        r52[12] = A0014;
        C72453ed.A1H(A0015, A0016, A0017, A0018, r52);
        r52[17] = A0019;
        r52[18] = A0020;
        r52[19] = A0021;
        C12960it.A1G(A0022, A0023, A0024, A0025, r52);
        r52[24] = A0026;
        C12960it.A1H(A0027, A0028, A0029, A0030, r52);
        C12970iu.A1S(A0031, A0032, A0033, r52);
        r52[32] = A0034;
        r52[33] = A0035;
        r52[34] = A0036;
        r52[35] = A0037;
        C12980iv.A1O(A0038, A0039, A0040, r52);
        r52[39] = A0041;
        r52[40] = A0042;
        r52[41] = A0043;
        r52[42] = A0044;
        r52[43] = A0045;
        r52[44] = A0046;
        r52[45] = A0047;
        r52[46] = A0048;
        r52[47] = A0049;
        r52[48] = A0050;
        r52[49] = A0051;
        r52[50] = A0052;
        A00 = r52;
        for (int i = 0; i < values().length; i++) {
        }
    }

    public EnumC869349n(EnumC868949j r5, EnumC869249m r6, String str, int i, int i2) {
        int i3;
        this.id = i2;
        this.zziy = r5;
        this.zzix = r6;
        int i4 = C88754Gz.A00[r5.ordinal()];
        boolean z = true;
        this.zziz = (i4 == 1 || i4 == 2) ? r6.zzli : null;
        this.zzja = (r5 != EnumC868949j.A01 || (i3 = C88754Gz.A01[r6.ordinal()]) == 1 || i3 == 2 || i3 == 3) ? false : z;
    }

    public static EnumC869349n A00(EnumC868949j r3, EnumC869249m r4, String str, int i) {
        return new EnumC869349n(r3, r4, str, i, i);
    }
}
