package X;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1HF  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1HF extends AnonymousClass1HG {
    public final AtomicBoolean A00 = new AtomicBoolean();

    public AnonymousClass1HF(AnonymousClass1HE r2, Executor executor) {
        super(r2, executor, 1);
    }
}
