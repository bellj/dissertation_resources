package X;

import android.view.KeyEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;

/* renamed from: X.3hB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74023hB extends InputConnectionWrapper {
    public final InputConnection A00;

    public C74023hB(InputConnection inputConnection, InputConnection inputConnection2) {
        super(inputConnection2, true);
        this.A00 = inputConnection;
    }

    @Override // android.view.inputmethod.InputConnectionWrapper, android.view.inputmethod.InputConnection
    public boolean deleteSurroundingText(int i, int i2) {
        if (i != 1 || i2 != 0) {
            return super.deleteSurroundingText(i, i2);
        }
        if (!sendKeyEvent(new KeyEvent(0, 67)) || !sendKeyEvent(new KeyEvent(1, 67))) {
            return false;
        }
        return true;
    }

    @Override // android.view.inputmethod.InputConnectionWrapper, android.view.inputmethod.InputConnection
    public boolean sendKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 67) {
            return this.A00.sendKeyEvent(keyEvent);
        }
        return super.sendKeyEvent(keyEvent);
    }
}
