package X;

import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.44z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C859244z extends AbstractC469528i {
    public final /* synthetic */ GroupChatInfo A00;

    public C859244z(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AbstractC469528i
    public void A00(GroupJid groupJid) {
        GroupChatInfo groupChatInfo = this.A00;
        if (groupJid.equals(groupChatInfo.A1D)) {
            groupChatInfo.A2z();
        }
    }
}
