package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.13P  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass13P {
    void AUg(C30751Yr v);

    void AUh(AbstractC14640lm v, UserJid userJid);

    void AUi(AbstractC14640lm v, UserJid userJid);
}
