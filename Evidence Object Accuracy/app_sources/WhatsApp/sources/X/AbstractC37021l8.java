package X;

import android.content.Context;
import android.util.AttributeSet;

/* renamed from: X.1l8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC37021l8 extends AnonymousClass1l9 {
    public AbstractC37021l8(Context context) {
        super(context);
        A02();
    }

    public AbstractC37021l8(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A02();
    }

    public AbstractC37021l8(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A02();
    }
}
