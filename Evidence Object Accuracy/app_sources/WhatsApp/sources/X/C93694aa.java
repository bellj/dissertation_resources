package X;

import android.graphics.Bitmap;

/* renamed from: X.4aa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93694aa {
    public int A00;
    public int A01;
    public Bitmap A02;
    public Bitmap A03;
    public final C89534Ki A04;

    public C93694aa(int i, int i2, Bitmap bitmap) {
        this.A03 = null;
        this.A02 = null;
        this.A04 = null;
        this.A03 = bitmap;
        this.A01 = i;
        this.A00 = i2;
    }

    public C93694aa(C89534Ki r2) {
        this.A03 = null;
        this.A02 = null;
        this.A04 = r2;
    }
}
