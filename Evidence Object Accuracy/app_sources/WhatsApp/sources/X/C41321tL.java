package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1tL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41321tL {
    public static final List A04 = new ArrayList();
    public final List A00 = new ArrayList();
    public final List A01 = new ArrayList();
    public final List A02 = new ArrayList();
    public final List A03 = new ArrayList();

    public C41321tL() {
    }

    public C41321tL(AnonymousClass1V8 r10) {
        C41301tJ r4;
        AnonymousClass1V8 A0E;
        AnonymousClass1V8[] r0;
        AnonymousClass1V8[] r02;
        AnonymousClass1V8 A0E2 = r10.A0E("sync");
        if (A0E2 != null) {
            for (AnonymousClass1V8 r5 : A0E2.A0J("collection")) {
                if ("error".equals(r5.A0I("type", null))) {
                    AnonymousClass1V8 A0E3 = r5.A0E("error");
                    AnonymousClass009.A05(A0E3);
                    try {
                        int A05 = A0E3.A05("code", -1);
                        String A0I = A0E3.A0I("text", null);
                        if (A05 != 409 || (A0E = r5.A0E("patches")) == null || (r0 = A0E.A03) == null || r0.length <= 0) {
                            r4 = new C41301tJ(null, A02(r5), A04, "true".equals(r5.A0I("has_more_patches", null)));
                        } else {
                            r4 = A01(r5);
                        }
                        this.A01.add(new C92374Vq(r4, A0I, A02(r5), A05));
                    } catch (AnonymousClass1V9 e) {
                        StringBuilder sb = new StringBuilder("Expected attribute code in ");
                        sb.append(A0E3);
                        sb.append(" exception");
                        sb.append(e.getMessage());
                        throw new AnonymousClass1t4(sb.toString(), true);
                    }
                } else if (r5.A0I("version", null) != null) {
                    try {
                        long A08 = r5.A08("version", -1);
                        if (A08 != -1) {
                            this.A03.add(new AnonymousClass4NM(A02(r5), A08));
                        } else {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Expected attribute version in ");
                            sb2.append(r5);
                            throw new AnonymousClass1t4(sb2.toString(), true);
                        }
                    } catch (AnonymousClass1V9 e2) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Expected attribute version in ");
                        sb3.append(r5);
                        sb3.append(" exception");
                        sb3.append(e2.getMessage());
                        throw new AnonymousClass1t4(sb3.toString(), true);
                    }
                } else {
                    AnonymousClass1V8 A0E4 = r5.A0E("patches");
                    if ((A0E4 == null || (r02 = A0E4.A03) == null || r02.length <= 0) && r5.A0E("snapshot") == null) {
                        this.A00.add(A02(r5));
                    } else {
                        this.A02.add(A01(r5));
                    }
                }
            }
            return;
        }
        throw new AnonymousClass1t4("Expected node sync in response, but not found", true);
    }

    public static final void A00(AnonymousClass1V8 r2, String str) {
        byte[] bArr = r2.A01;
        if (bArr == null || bArr.length == 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" body was empty.");
            throw new AnonymousClass1t4(sb.toString(), true);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:68:0x0025 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1, types: [X.1K3] */
    /* JADX WARN: Type inference failed for: r0v47, types: [X.1K3] */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e2, code lost:
        if (r5 != 101) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0010, code lost:
        if (r2 != null) goto L_0x0012;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C41301tJ A01(X.AnonymousClass1V8 r10) {
        /*
        // Method dump skipped, instructions count: 334
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41321tL.A01(X.1V8):X.1tJ");
    }

    public final String A02(AnonymousClass1V8 r4) {
        String A0I = r4.A0I("name", null);
        if (A0I != null) {
            return A0I;
        }
        StringBuilder sb = new StringBuilder("Expected attribute name in ");
        sb.append(r4);
        throw new AnonymousClass1t4(sb.toString(), true);
    }
}
