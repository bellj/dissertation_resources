package X;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import com.whatsapp.CircleWaImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/* renamed from: X.2ve  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59952ve extends AbstractC37191le {
    public final Handler A00 = new Handler();
    public final View A01;
    public final View A02;
    public final View A03;
    public final View A04;
    public final CircleWaImageView A05;
    public final C15570nT A06;
    public final WaImageButton A07;
    public final WaImageView A08;
    public final WaTextView A09;
    public final WaTextView A0A;
    public final WaTextView A0B;
    public final WaTextView A0C;
    public final WaTextView A0D;
    public final WaTextView A0E;
    public final WaTextView A0F;
    public final C251118d A0G;
    public final C48842Hz A0H;
    public final C48822Hx A0I;
    public final AnonymousClass1CN A0J;
    public final AnonymousClass131 A0K;
    public final AnonymousClass018 A0L;
    public final AbstractC14440lR A0M;

    public C59952ve(View view, C15570nT r3, C251118d r4, C48842Hz r5, C48822Hx r6, AnonymousClass1CN r7, AnonymousClass131 r8, AnonymousClass018 r9, AbstractC14440lR r10) {
        super(view);
        this.A06 = r3;
        this.A0M = r10;
        this.A0J = r7;
        this.A0L = r9;
        this.A0G = r4;
        this.A0I = r6;
        this.A0H = r5;
        this.A0K = r8;
        this.A05 = (CircleWaImageView) AnonymousClass028.A0D(view, R.id.business_avatar);
        this.A0A = C12960it.A0N(view, R.id.business_name);
        this.A0F = C12960it.A0N(view, R.id.open_status);
        this.A0B = C12960it.A0N(view, R.id.category);
        this.A09 = C12960it.A0N(view, R.id.address);
        this.A0C = C12960it.A0N(view, R.id.distance);
        this.A08 = C12980iv.A0X(view, R.id.dot_divider);
        this.A07 = (WaImageButton) AnonymousClass028.A0D(view, R.id.message_btn);
        this.A0D = C12960it.A0N(view, R.id.fb_likes);
        this.A0E = C12960it.A0N(view, R.id.ig_followers);
        this.A02 = AnonymousClass028.A0D(view, R.id.fb_layout);
        this.A03 = AnonymousClass028.A0D(view, R.id.ig_layout);
        this.A01 = AnonymousClass028.A0D(view, R.id.fb_ig_layout);
        this.A04 = AnonymousClass028.A0D(view, R.id.responiveness_signal);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ce, code lost:
        if (r12 == false) goto L_0x00d0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x021f  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x022e  */
    @Override // X.AbstractC37191le
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void A09(java.lang.Object r20) {
        /*
        // Method dump skipped, instructions count: 645
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C59952ve.A09(java.lang.Object):void");
    }

    public final void A0A(Context context, View view, WaTextView waTextView, Integer num, int i, int i2) {
        String string;
        String valueOf;
        if (num != null) {
            int i3 = 0;
            view.setVisibility(0);
            int intValue = num.intValue();
            if (intValue > 0) {
                AnonymousClass018 r1 = this.A0L;
                if (intValue < 10000) {
                    Locale A14 = C12970iu.A14(r1);
                    Object[] objArr = new Object[1];
                    C12960it.A1P(objArr, intValue, 0);
                    valueOf = String.format(A14, "%,d", objArr);
                } else {
                    double d = (double) intValue;
                    double pow = Math.pow(1000.0d, (double) ((int) (Math.log(d) / Math.log(1000.0d))));
                    double d2 = d / pow;
                    int i4 = intValue / ((int) pow);
                    if (d2 == ((double) i4) || ((int) (d2 * 10.0d)) % 10 == 0) {
                        valueOf = String.valueOf(i4);
                    } else {
                        NumberFormat instance = DecimalFormat.getInstance();
                        instance.setMinimumFractionDigits(1);
                        instance.setMaximumFractionDigits(1);
                        instance.setRoundingMode(RoundingMode.DOWN);
                        valueOf = instance.format(d2);
                    }
                }
                if (intValue >= 10000) {
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append("KMB".charAt(((int) (Math.log((double) intValue) / Math.log(1000.0d))) - 1));
                    String obj = A0h.toString();
                    if (obj != null) {
                        switch (obj.hashCode()) {
                            case 66:
                                if (obj.equals("B")) {
                                    i3 = R.string.consumer_short_format_number_trust_signal_b;
                                    break;
                                }
                                break;
                            case 75:
                                if (obj.equals("K")) {
                                    i3 = R.string.consumer_short_format_number_trust_signal_k;
                                    break;
                                }
                                break;
                            case 77:
                                if (obj.equals("M")) {
                                    i3 = R.string.consumer_short_format_number_trust_signal_m;
                                    break;
                                }
                                break;
                        }
                        valueOf = C12960it.A0d(context.getString(i3), C12960it.A0j(valueOf));
                    }
                }
                string = context.getResources().getQuantityString(i, num.intValue(), valueOf);
            } else {
                string = context.getResources().getString(i2);
            }
            waTextView.setText(string);
            return;
        }
        view.setVisibility(8);
    }
}
