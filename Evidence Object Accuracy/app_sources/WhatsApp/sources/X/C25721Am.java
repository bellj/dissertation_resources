package X;

/* renamed from: X.1Am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25721Am {
    public long A00;
    public long A01;
    public final C25701Ak A02;
    public final C471229a A03 = new C471229a();
    public final AnonymousClass10I A04;
    public final C25711Al A05;
    public final C22730zY A06;
    public final AnonymousClass10K A07;
    public final C19380u1 A08;
    public final C15810nw A09;
    public final C16590pI A0A;
    public final C15890o4 A0B;
    public final C14820m6 A0C;
    public final C14950mJ A0D;
    public final C15880o3 A0E;
    public final C14850m9 A0F;
    public final C22190yg A0G;
    public final AbstractC14440lR A0H;

    public C25721Am(C25701Ak r2, AnonymousClass10I r3, C25711Al r4, C22730zY r5, AnonymousClass10K r6, C19380u1 r7, C15810nw r8, C16590pI r9, C15890o4 r10, C14820m6 r11, C14950mJ r12, C15880o3 r13, C14850m9 r14, C22190yg r15, AbstractC14440lR r16) {
        this.A0A = r9;
        this.A0F = r14;
        this.A0H = r16;
        this.A09 = r8;
        this.A0D = r12;
        this.A08 = r7;
        this.A0G = r15;
        this.A0E = r13;
        this.A05 = r4;
        this.A0C = r11;
        this.A0B = r10;
        this.A04 = r3;
        this.A02 = r2;
        this.A06 = r5;
        this.A07 = r6;
    }
}
