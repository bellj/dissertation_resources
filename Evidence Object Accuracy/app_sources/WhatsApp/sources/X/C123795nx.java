package X;

/* renamed from: X.5nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123795nx extends C469928m {
    public final /* synthetic */ AbstractActivityC119325dX A00;

    public C123795nx(AbstractActivityC119325dX r1) {
        this.A00 = r1;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        AbstractC118075bE r1 = this.A00.A07;
        int length = charSequence.length();
        AnonymousClass016 r2 = r1.A01;
        int i4 = 1;
        if (length > 0) {
            i4 = 3;
        }
        r2.A0B(new C127065tt(i4));
    }
}
