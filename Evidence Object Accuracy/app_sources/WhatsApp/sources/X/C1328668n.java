package X;

import com.whatsapp.R;

/* renamed from: X.68n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328668n implements AnonymousClass1FK {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractC16870pt A01;
    public final /* synthetic */ AbstractView$OnClickListenerC121765jx A02;

    public C1328668n(AbstractC16870pt r1, AbstractView$OnClickListenerC121765jx r2, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        AbstractView$OnClickListenerC121765jx r2 = this.A02;
        r2.A0K.A04(C12960it.A0b("removePayment/onRequestError. paymentNetworkError: ", r4));
        AbstractC16870pt r1 = this.A01;
        if (r1 != null) {
            r1.AKa(r4, this.A00);
        }
        r2.AaN();
        r2.Ado(R.string.payment_method_cannot_be_removed);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        AbstractView$OnClickListenerC121765jx r2 = this.A02;
        r2.A0K.A06(C12960it.A0b("removePayment/onResponseError. paymentNetworkError: ", r4));
        AbstractC16870pt r1 = this.A01;
        if (r1 != null) {
            r1.AKa(r4, this.A00);
        }
        r2.AaN();
        r2.Ado(R.string.payment_method_cannot_be_removed);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r5) {
        AbstractView$OnClickListenerC121765jx r3 = this.A02;
        r3.A0K.A06("removePayment Success");
        AbstractC16870pt r2 = this.A01;
        if (r2 != null) {
            r2.AKa(null, this.A00);
        }
        r3.AaN();
        r3.Ado(R.string.payment_method_is_removed);
    }
}
