package X;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.conversation.conversationrow.components.ViewOnceDownloadProgressView;
import java.util.Arrays;

/* renamed from: X.1vc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC42661vc extends AbstractC42671vd {
    public int A00 = 0;
    public final View A01 = AnonymousClass028.A0D(this, R.id.view_once_media_container_small);
    public final WaTextView A02 = ((WaTextView) AnonymousClass028.A0D(this, R.id.view_once_media_type_small));
    public final ViewOnceDownloadProgressView A03 = ((ViewOnceDownloadProgressView) AnonymousClass028.A0D(this, R.id.view_once_download_small));

    public AbstractC42661vc(Context context, AbstractC13890kV r3, AbstractC16130oV r4) {
        super(context, r3, r4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0X(com.whatsapp.conversation.conversationrow.components.ViewOnceDownloadProgressView r3, X.AbstractC16130oV r4, int r5, boolean r6) {
        /*
            if (r5 == 0) goto L_0x0032
            r0 = 1
            if (r5 == r0) goto L_0x0032
            r0 = 2
            if (r5 != r0) goto L_0x0039
            r2 = 2131232216(0x7f0805d8, float:1.8080535E38)
        L_0x000b:
            if (r6 == 0) goto L_0x002b
            r2 = 2131232216(0x7f0805d8, float:1.8080535E38)
            r1 = 2131231866(0x7f08047a, float:1.8079825E38)
            r0 = 2131101046(0x7f060576, float:1.781449E38)
        L_0x0016:
            r3.A00(r2, r1, r0)
        L_0x0019:
            boolean r0 = r3.isInEditMode()
            if (r0 != 0) goto L_0x0026
            X.109 r1 = r3.A00
            com.whatsapp.CircularProgressBar r0 = r3.A03
            X.C65033Hw.A00(r0, r1, r4)
        L_0x0026:
            r0 = 0
            r3.setVisibility(r0)
            return
        L_0x002b:
            r1 = 2131231866(0x7f08047a, float:1.8079825E38)
            r0 = 2131101045(0x7f060575, float:1.7814489E38)
            goto L_0x0016
        L_0x0032:
            r2 = 2131232213(0x7f0805d5, float:1.8080529E38)
            r1 = 2131101046(0x7f060576, float:1.781449E38)
            goto L_0x0042
        L_0x0039:
            r0 = 3
            if (r5 != r0) goto L_0x0047
            r2 = 2131232217(0x7f0805d9, float:1.8080537E38)
            r1 = 2131101045(0x7f060575, float:1.7814489E38)
        L_0x0042:
            r0 = -1
            r3.A00(r2, r0, r1)
            goto L_0x0019
        L_0x0047:
            r2 = 2131232214(0x7f0805d6, float:1.808053E38)
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC42661vc.A0X(com.whatsapp.conversation.conversationrow.components.ViewOnceDownloadProgressView, X.0oV, int, boolean):void");
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1R();
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != getFMessage()) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A1R();
        }
    }

    public void A1P() {
        this.A03.A00(R.drawable.ic_ephemeral_ring, -1, R.color.view_once_viewed);
        WaTextView waTextView = this.A02;
        waTextView.setTextColor(getResources().getColor(R.color.view_once_viewed));
        waTextView.setTypeface(Typeface.DEFAULT, 2);
        A1S();
        this.A01.setVisibility(0);
        waTextView.setContentDescription(getContext().getString(getMediaTypeDescriptionString()));
    }

    public void A1Q() {
        A1P();
        WaTextView waTextView = this.A02;
        waTextView.setText(getContext().getString(R.string.view_once_opened));
        waTextView.setContentDescription(getContext().getString(getMediaTypeDescriptionString()));
    }

    public void A1R() {
        C42651vb r3 = (C42651vb) this;
        AbstractC16130oV fMessage = r3.getFMessage();
        int AHc = ((AnonymousClass1XH) fMessage).AHc();
        if (AHc != 0) {
            if (AHc == 1) {
                r3.A1Q();
            } else if (AHc == 2) {
                A0X(((AbstractC42661vc) r3).A03, fMessage, 2, true);
                r3.A1U(((AbstractC42661vc) r3).A01, 2, true);
                r3.A1S();
            } else {
                return;
            }
            View view = ((AbstractC42661vc) r3).A01;
            view.setOnClickListener(r3.A02);
            view.setOnLongClickListener(r3.A1a);
            return;
        }
        int A00 = C30041Vv.A00(fMessage);
        A0X(((AbstractC42661vc) r3).A03, fMessage, A00, true);
        View view2 = ((AbstractC42661vc) r3).A01;
        r3.A1U(view2, A00, true);
        if (A00 == 2) {
            view2.setOnClickListener(r3.A02);
            view2.setOnLongClickListener(r3.A1a);
        }
        r3.A1S();
    }

    public void A1S() {
        if (this.A00 == 0) {
            A1T();
        }
        this.A02.setWidth(this.A00);
    }

    public final void A1T() {
        for (Number number : Arrays.asList(Integer.valueOf((int) R.string.retry), Integer.valueOf((int) R.string.view_once_photo), Integer.valueOf((int) R.string.view_once_video), Integer.valueOf((int) R.string.view_once_expired), Integer.valueOf((int) R.string.view_once_opened))) {
            String string = getContext().getString(number.intValue());
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);
            spannableStringBuilder.setSpan(new C52292aZ(getContext()), 0, string.length(), 0);
            this.A00 = Math.max(this.A00, (int) Layout.getDesiredWidth(spannableStringBuilder, this.A02.getPaint())) + getResources().getDimensionPixelSize(R.dimen.conversationRowViewOnce_small_marginRight);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0084  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1U(android.view.View r7, int r8, boolean r9) {
        /*
        // Method dump skipped, instructions count: 306
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC42661vc.A1U(android.view.View, int, boolean):void");
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_view_once_media_incoming;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_view_once_media_incoming;
    }

    public int getMediaTypeDescriptionString() {
        AnonymousClass1XH r1 = (AnonymousClass1XH) getFMessage();
        boolean z = r1 instanceof AnonymousClass1XG;
        int AHc = r1.AHc();
        if (z) {
            if (AHc == 1) {
                return R.string.view_once_video_description_opened;
            }
            if (AHc != 2) {
                return R.string.view_once_video_description;
            }
            return R.string.view_once_video_description_expired;
        } else if (AHc == 1) {
            return R.string.view_once_photo_description_opened;
        } else {
            if (AHc != 2) {
                return R.string.view_once_photo_description;
            }
            return R.string.view_once_photo_description_expired;
        }
    }

    public int getMediaTypeString() {
        if (getFMessage() instanceof AnonymousClass1XG) {
            return R.string.view_once_video;
        }
        return R.string.view_once_photo;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_view_once_media_outgoing;
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        A1T();
        A1S();
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AbstractC16130oV);
        super.setFMessage(r2);
    }
}
