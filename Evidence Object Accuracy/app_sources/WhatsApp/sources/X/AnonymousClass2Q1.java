package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2Q1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Q1 extends AnonymousClass2PA {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;

    public AnonymousClass2Q1(Jid jid, String str, String str2, String str3, String str4, String str5, String str6, long j) {
        super(jid, str, j);
        this.A02 = str2;
        this.A00 = str3;
        this.A03 = str4;
        this.A04 = str5;
        this.A01 = str6;
    }
}
