package X;

/* renamed from: X.0ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15670ni {
    public final C16270oj A00;

    public C15670ni(C16270oj r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0038, code lost:
        if (r1 != null) goto L_0x003a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A00(java.lang.String r8) {
        /*
            r7 = this;
            X.0oj r0 = r7.A00
            X.0on r4 = r0.get()
            X.0op r6 = r4.A03     // Catch: all -> 0x0041
            java.lang.String r5 = "SELECT file_name FROM shared_media_ids WHERE item_uuid =?AND expiration_timestamp >?"
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch: all -> 0x0041
            r0 = 0
            r3[r0] = r8     // Catch: all -> 0x0041
            r2 = 1
            long r0 = java.lang.System.currentTimeMillis()     // Catch: all -> 0x0041
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x0041
            r3[r2] = r0     // Catch: all -> 0x0041
            android.database.Cursor r1 = r6.A09(r5, r3)     // Catch: all -> 0x0041
            if (r1 == 0) goto L_0x0037
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0032
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "file_name"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0032
            java.lang.String r0 = r1.getString(r0)     // Catch: all -> 0x0032
            goto L_0x003a
        L_0x0032:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x0036
        L_0x0036:
            throw r0     // Catch: all -> 0x0041
        L_0x0037:
            r0 = 0
            if (r1 == 0) goto L_0x003d
        L_0x003a:
            r1.close()     // Catch: all -> 0x0041
        L_0x003d:
            r4.close()
            return r0
        L_0x0041:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0045
        L_0x0045:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15670ni.A00(java.lang.String):java.lang.String");
    }

    public void A01(String str, String str2, String str3, String str4) {
        C16310on A02 = this.A00.A02();
        try {
            AnonymousClass1YE A0A = A02.A03.A0A("INSERT INTO shared_media_ids (item_uuid, file_name, mime_type, display_name, expiration_timestamp) VALUES (?, ?, ?, ?, ?)");
            A0A.A02(1, str);
            A0A.A02(2, str2);
            A0A.A02(3, str3);
            if (str4 == null) {
                A0A.A00.bindNull(4);
            } else {
                A0A.A02(4, str4);
            }
            A0A.A02(5, String.valueOf(System.currentTimeMillis() + 3600000));
            A0A.A00.executeInsert();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
