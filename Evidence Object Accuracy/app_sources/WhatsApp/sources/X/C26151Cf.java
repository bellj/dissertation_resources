package X;

import java.util.List;

/* renamed from: X.1Cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26151Cf {
    public final AnonymousClass018 A00;
    public final C22650zQ A01;

    public C26151Cf(AnonymousClass018 r1, C22650zQ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static boolean A00(AbstractC15340mz r4) {
        List<C30761Ys> list;
        if ((r4 instanceof C28851Pg) && (list = ((C28851Pg) r4).A00.A04) != null) {
            for (C30761Ys r0 : list) {
                if (r0.A06.get() == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
