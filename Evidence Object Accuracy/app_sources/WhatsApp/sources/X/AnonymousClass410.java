package X;

import java.util.List;

/* renamed from: X.410  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass410 extends AnonymousClass4K8 {
    public final List A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass410) && C16700pc.A0O(this.A00, ((AnonymousClass410) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public AnonymousClass410(List list) {
        super(list);
        this.A00 = list;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Loading(loadingItems=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
