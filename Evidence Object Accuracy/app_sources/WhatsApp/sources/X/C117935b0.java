package X;

/* renamed from: X.5b0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117935b0 extends AnonymousClass015 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public AnonymousClass016 A01 = C12980iv.A0T();
    public AnonymousClass016 A02 = C12980iv.A0T();
    public final AbstractC001200n A03;
    public final C14830m7 A04;
    public final AnonymousClass018 A05;
    public final C20370ve A06;
    public final C129795yJ A07;
    public final AnonymousClass14X A08;
    public final AbstractC14440lR A09;

    public C117935b0(AbstractC001200n r2, C14830m7 r3, AnonymousClass018 r4, C20370ve r5, C129795yJ r6, AnonymousClass14X r7, AbstractC14440lR r8) {
        this.A03 = r2;
        this.A04 = r3;
        this.A09 = r8;
        this.A08 = r7;
        this.A05 = r4;
        this.A07 = r6;
        this.A06 = r5;
    }
}
