package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1tA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41221tA extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C41221tA A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public AbstractC27881Jp A01 = AbstractC27881Jp.A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public C27921Jt A03;
    public AnonymousClass1tB A04;

    static {
        C41221tA r0 = new C41221tA();
        A05 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        if ((this.A00 & 1) == 1) {
            AnonymousClass1tB r0 = this.A04;
            if (r0 == null) {
                r0 = AnonymousClass1tB.A02;
            }
            i = CodedOutputStream.A0A(r0, 1) + 0;
        } else {
            i = 0;
        }
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A02.get(i3), 2);
        }
        int i4 = this.A00;
        if ((i4 & 2) == 2) {
            i += CodedOutputStream.A09(this.A01, 3);
        }
        if ((i4 & 4) == 4) {
            C27921Jt r02 = this.A03;
            if (r02 == null) {
                r02 = C27921Jt.A02;
            }
            i += CodedOutputStream.A0A(r02, 4);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass1tB r0 = this.A04;
            if (r0 == null) {
                r0 = AnonymousClass1tB.A02;
            }
            codedOutputStream.A0L(r0, 1);
        }
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A02.get(i), 2);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A01, 3);
        }
        if ((this.A00 & 4) == 4) {
            C27921Jt r02 = this.A03;
            if (r02 == null) {
                r02 = C27921Jt.A02;
            }
            codedOutputStream.A0L(r02, 4);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
