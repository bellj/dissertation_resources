package X;

import android.os.SystemClock;
import android.util.Pair;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: X.18z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C253318z {
    public C42341v3 A00;
    public final AbstractC15710nm A01;
    public final C246716k A02;
    public final C15550nR A03;
    public final AnonymousClass1E4 A04;
    public final C20840wP A05;
    public final C18640sm A06;
    public final C14830m7 A07;
    public final C22130yZ A08;
    public final C14850m9 A09;
    public final C17220qS A0A;
    public final ConcurrentHashMap A0B = new ConcurrentHashMap();
    public final ConcurrentHashMap A0C = new ConcurrentHashMap();
    public final ConcurrentHashMap A0D = new ConcurrentHashMap();

    public C253318z(AbstractC15710nm r2, C246716k r3, C15550nR r4, AnonymousClass1E4 r5, C20840wP r6, C18640sm r7, C14830m7 r8, C22130yZ r9, C14850m9 r10, C17220qS r11) {
        this.A07 = r8;
        this.A09 = r10;
        this.A01 = r2;
        this.A0A = r11;
        this.A03 = r4;
        this.A02 = r3;
        this.A08 = r9;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
    }

    public Pair A00(AnonymousClass1JA r14, String str) {
        Pair pair;
        Pair pair2;
        Integer num;
        C42351v4 r0;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        boolean z = true;
        if (r14 != AnonymousClass1JA.A0C) {
            z = false;
        }
        AnonymousClass009.A0F(z);
        if (!this.A06.A0B()) {
            Log.i("ContactQuerySyncManager/querySyncPhoneNumber: network_unavailable");
            r0 = C42351v4.A04;
        } else {
            ConcurrentHashMap concurrentHashMap = this.A0C;
            if (concurrentHashMap.putIfAbsent(str, str) != null) {
                r0 = C42351v4.A08;
            } else {
                String A00 = C42391v8.A00("sync_sid_query");
                try {
                    try {
                        try {
                            A02().A04(C50242Os.A00(r14, null, str, this.A02.A00(), this.A09.A07(1892)), A00, 32000).get(32000, TimeUnit.MILLISECONDS);
                            ConcurrentHashMap concurrentHashMap2 = this.A0D;
                            C42061ub r6 = (C42061ub) concurrentHashMap2.get(A00);
                            if (r6 == null) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("ContactQuerySyncManager/querySyncPhoneNumber: empty sync result for ");
                                sb.append(str);
                                sb.append(" (syncId is ");
                                sb.append(A00);
                                sb.append(")");
                                Log.e(sb.toString());
                                pair2 = Pair.create(C42351v4.A03, null);
                            } else {
                                C42051ua[] r8 = r6.A01;
                                if (r8.length == 0) {
                                    C42111ug r02 = r6.A00.A01;
                                    if (r02 == null || (num = r02.A00) == null || num.intValue() != 429) {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("ContactQuerySyncManager/querySyncPhoneNumber: no users for ");
                                        sb2.append(str);
                                        Log.e(sb2.toString());
                                        pair2 = Pair.create(C42351v4.A03, null);
                                    } else {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("ContactQuerySyncManager/querySyncPhoneNumber: rate-limit-error ");
                                        sb3.append(str);
                                        Log.e(sb3.toString());
                                        pair2 = Pair.create(C42351v4.A05, null);
                                    }
                                } else {
                                    C42051ua r82 = r8[0];
                                    if (r82.A04 == 1) {
                                        C15550nR r4 = this.A03;
                                        UserJid userJid = r82.A0C;
                                        AnonymousClass009.A05(userJid);
                                        this.A04.A01(r82, r6.A00, r4.A0B(userJid), elapsedRealtime);
                                    }
                                    List list = r82.A0G;
                                    if (list != null && list.size() > 0) {
                                        r82.A0G.get(0);
                                    }
                                    pair2 = Pair.create(C42351v4.A06, r82);
                                }
                            }
                            concurrentHashMap.remove(str);
                            concurrentHashMap2.remove(A00);
                            return pair2;
                        } catch (InterruptedException e) {
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("ContactQuerySync/querySyncPhoneNumber: exception during Query Sync ");
                            sb4.append(str);
                            Log.e(sb4.toString(), e);
                            pair = Pair.create(C42351v4.A02, null);
                            return pair;
                        }
                    } catch (ExecutionException e2) {
                        A03("querySyncPhoneNumber", e2);
                        pair = Pair.create(C42351v4.A03, null);
                        return pair;
                    } catch (TimeoutException unused) {
                        Log.e("ContactQuerySyncManager/querySyncPhoneNumber/timeout");
                        pair = Pair.create(C42351v4.A03, null);
                        return pair;
                    }
                } finally {
                    concurrentHashMap.remove(str);
                    this.A0D.remove(A00);
                }
            }
        }
        return Pair.create(r0, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r14 == X.AnonymousClass1JA.A01) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C42351v4 A01(X.AnonymousClass1JA r14, com.whatsapp.jid.UserJid r15) {
        /*
        // Method dump skipped, instructions count: 290
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C253318z.A01(X.1JA, com.whatsapp.jid.UserJid):X.1v4");
    }

    public final synchronized C42341v3 A02() {
        C42341v3 r4;
        r4 = this.A00;
        if (r4 == null) {
            AbstractC15710nm r3 = this.A01;
            C17220qS r2 = this.A0A;
            boolean z = false;
            if (this.A08.A0D.A05()) {
                z = true;
            }
            r4 = new C42341v3(r3, new AnonymousClass2Oq(this), r2, z);
            this.A00 = r4;
        }
        return r4;
    }

    public final void A03(String str, ExecutionException executionException) {
        if ((executionException.getCause() instanceof RuntimeException) || ((executionException.getCause() instanceof Error) && !(executionException.getCause() instanceof AssertionError) && !(executionException.getCause() instanceof OutOfMemoryError))) {
            AbstractC15710nm r3 = this.A01;
            StringBuilder sb = new StringBuilder("ContactQuerySync/");
            sb.append(str);
            r3.AaV(sb.toString(), executionException.getMessage(), true);
        }
    }
}
