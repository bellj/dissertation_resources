package X;

/* renamed from: X.0Yo  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yo implements AbstractC009404s {
    public static AnonymousClass0Yo A00;

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        try {
            return (AnonymousClass015) cls.newInstance();
        } catch (IllegalAccessException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot create an instance of ");
            sb.append(cls);
            throw new RuntimeException(sb.toString(), e);
        } catch (InstantiationException e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot create an instance of ");
            sb2.append(cls);
            throw new RuntimeException(sb2.toString(), e2);
        }
    }
}
