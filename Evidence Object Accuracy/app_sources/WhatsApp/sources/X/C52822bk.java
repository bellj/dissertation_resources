package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0201000_I1;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.2bk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52822bk extends BaseAdapter {
    public UserJid A00;
    public final int A01;
    public final /* synthetic */ MessageDetailsActivity A02;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 1;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public C52822bk(MessageDetailsActivity messageDetailsActivity) {
        this.A02 = messageDetailsActivity;
        this.A01 = messageDetailsActivity.getResources().getInteger(17694721);
    }

    public static void A00(View view, C52822bk r4, int i) {
        TranslateAnimation translateAnimation = new TranslateAnimation((float) i, 0.0f, 0.0f, 0.0f);
        translateAnimation.setDuration((long) r4.A01);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        view.startAnimation(translateAnimation);
    }

    public final void A01(View view, int i) {
        int A00;
        Context context;
        int i2;
        int i3;
        AnonymousClass49N A02;
        ViewOnClickCListenerShape1S0201000_I1 viewOnClickCListenerShape1S0201000_I1;
        MessageDetailsActivity messageDetailsActivity = this.A02;
        ArrayList arrayList = messageDetailsActivity.A0c;
        AnonymousClass1lQ r10 = (AnonymousClass1lQ) arrayList.get(i);
        View findViewById = view.findViewById(R.id.section_header);
        if (i == 0 || (A00 = r10.A00()) != ((AnonymousClass1lQ) arrayList.get(i - 1)).A00()) {
            findViewById.setVisibility(0);
            TextView A0J = C12960it.A0J(view, R.id.section_title);
            TextView A0J2 = C12960it.A0J(view, R.id.section_icon);
            A00 = r10.A00();
            if (A00 == 5) {
                boolean z = messageDetailsActivity.A0P instanceof C27671Iq;
                int i4 = R.string.message_delivered_to;
                if (z) {
                    i4 = R.string.poll_delivered_to;
                }
                A0J.setText(i4);
                context = view.getContext();
                i2 = R.drawable.msg_status_client;
                i3 = R.color.msgStatusTint;
            } else if (A00 != 8) {
                if (A00 == 13) {
                    A0J.setText(AnonymousClass4EG.A00(messageDetailsActivity.A0P, false));
                    context = view.getContext();
                    i2 = R.drawable.msg_status_client;
                    i3 = R.color.msgStatusReadTint;
                }
                A02 = r10.A02();
            } else {
                C93494aG A002 = C93494aG.A00(messageDetailsActivity.A0P, false);
                A0J.setText(A002.A02);
                context = view.getContext();
                i2 = A002.A01;
                i3 = A002.A00;
            }
            A0J2.setCompoundDrawablesWithIntrinsicBounds(AnonymousClass2GE.A01(context, i2, i3), (Drawable) null, (Drawable) null, (Drawable) null);
            A02 = r10.A02();
        } else {
            A02 = r10.A02();
            findViewById.setVisibility(8);
        }
        View findViewById2 = view.findViewById(R.id.divider);
        View findViewById3 = view.findViewById(R.id.content);
        if (i == arrayList.size() - 1 || A00 != ((AnonymousClass1lQ) arrayList.get(i + 1)).A00()) {
            findViewById3.setBackgroundResource(R.drawable.panel_bot);
            findViewById2.setVisibility(8);
        } else {
            findViewById3.setBackgroundResource(R.drawable.panel_mid);
            findViewById2.setVisibility(0);
        }
        ImageView A0L = C12970iu.A0L(view, R.id.contact_photo);
        C28801Pb r11 = new C28801Pb(view, messageDetailsActivity.A09, messageDetailsActivity.A0S, (int) R.id.contact_name);
        TextEmojiLabel A0U = C12970iu.A0U(view, R.id.push_name);
        TextView A0J3 = C12960it.A0J(view, R.id.remaining);
        View findViewById4 = view.findViewById(R.id.date_time_delivered_group);
        View findViewById5 = view.findViewById(R.id.date_time_read_group);
        View findViewById6 = view.findViewById(R.id.date_time_played_group);
        View A0D = AnonymousClass028.A0D(view, R.id.date_time_kept_group);
        TextView A0J4 = C12960it.A0J(view, R.id.date_time_delivered);
        TextView A0J5 = C12960it.A0J(view, R.id.date_time_read);
        TextView A0J6 = C12960it.A0J(view, R.id.date_time_played);
        AnonymousClass028.A0D(view, R.id.date_time_kept);
        View findViewById7 = view.findViewById(R.id.date_time_delivered_label);
        View findViewById8 = view.findViewById(R.id.date_time_read_label);
        View findViewById9 = view.findViewById(R.id.date_time_played_label);
        View A0D2 = AnonymousClass028.A0D(view, R.id.date_time_kept_label);
        C12980iv.A1C(findViewById4, findViewById5, findViewById6, 8);
        C12980iv.A1C(A0D, findViewById7, findViewById8, 8);
        findViewById9.setVisibility(8);
        A0D2.setVisibility(8);
        if (AnonymousClass49N.A01 == A02) {
            A0J3.setVisibility(0);
            A0L.setVisibility(8);
            r11.A01.setVisibility(8);
            A0U.setVisibility(8);
            AnonymousClass018 r8 = ((ActivityC13830kP) messageDetailsActivity).A01;
            int i5 = ((AnonymousClass42D) r10).A00;
            Object[] A1b = C12970iu.A1b();
            C12960it.A1O(A1b, i5);
            A0J3.setText(r8.A0I(A1b, R.plurals.participants_remaining, (long) i5));
            viewOnClickCListenerShape1S0201000_I1 = null;
            view.setTag(null);
        } else {
            C15370n3 A0B = messageDetailsActivity.A07.A0B(r10.A01);
            boolean A0J7 = C15380n4.A0J(messageDetailsActivity.A0P.A0z.A00);
            int i6 = 2;
            if (A0J7) {
                i6 = 1;
            }
            A0J3.setVisibility(8);
            A0L.setVisibility(0);
            messageDetailsActivity.A0B.A07(A0L, A0B, false);
            r11.A01.setVisibility(0);
            r11.A07(A0B, null, i6);
            if (!messageDetailsActivity.A09.A0L(A0B, i6) || ((A0B.A0J() || TextUtils.isEmpty(A0B.A0U)) && (!A0B.A0J() || TextUtils.isEmpty(A0B.A0D())))) {
                A0U.setVisibility(8);
            } else {
                A0U.setVisibility(0);
                A0U.A0G(null, messageDetailsActivity.A09.A09(A0B));
            }
            if (A0B.A0D.equals(this.A00)) {
                long A01 = r10.A01(5);
                if (A01 > 0) {
                    ActivityC13790kL.A0d(A0J4, messageDetailsActivity, A01);
                    findViewById4.setVisibility(0);
                    findViewById7.setVisibility(0);
                }
                long A012 = r10.A01(13);
                if (A012 > 0) {
                    ActivityC13790kL.A0d(A0J5, messageDetailsActivity, A012);
                    findViewById5.setVisibility(0);
                    findViewById8.setVisibility(0);
                }
                long A013 = r10.A01(8);
                if (A013 > 0) {
                    ActivityC13790kL.A0d(A0J6, messageDetailsActivity, A013);
                    findViewById6.setVisibility(0);
                    findViewById9.setVisibility(0);
                }
            } else if (A00 == 5) {
                ActivityC13790kL.A0d(A0J4, messageDetailsActivity, r10.A01(5));
                findViewById4.setVisibility(0);
            } else if (A00 == 8) {
                ActivityC13790kL.A0d(A0J6, messageDetailsActivity, r10.A01(8));
                findViewById6.setVisibility(0);
            } else if (A00 == 13) {
                ActivityC13790kL.A0d(A0J5, messageDetailsActivity, r10.A01(13));
                findViewById5.setVisibility(0);
            }
            view.setTag(A0B.A0D);
            viewOnClickCListenerShape1S0201000_I1 = new ViewOnClickCListenerShape1S0201000_I1(this, view, i, 2);
        }
        findViewById3.setOnClickListener(viewOnClickCListenerShape1S0201000_I1);
    }

    public final void A02(View view, int i, boolean z) {
        int width;
        AlphaAnimation alphaAnimation;
        int width2;
        int width3;
        MessageDetailsActivity messageDetailsActivity = this.A02;
        View findViewById = view.findViewById(R.id.date_time_delivered);
        View findViewById2 = view.findViewById(R.id.date_time_read);
        View findViewById3 = view.findViewById(R.id.date_time_played);
        View A0D = AnonymousClass028.A0D(view, R.id.date_time_delivered_label);
        TextView A0I = C12960it.A0I(view, R.id.date_time_read_label);
        TextView A0I2 = C12960it.A0I(view, R.id.date_time_played_label);
        View findViewById4 = view.findViewById(R.id.date_time_delivered_group);
        View findViewById5 = view.findViewById(R.id.date_time_read_group);
        View findViewById6 = view.findViewById(R.id.date_time_played_group);
        A0I.setText(AnonymousClass4EG.A00(messageDetailsActivity.A0P, true));
        A0I2.setText(C93494aG.A00(messageDetailsActivity.A0P, true).A02);
        ArrayList A0w = C12980iv.A0w(6);
        int A00 = ((AnonymousClass1lQ) messageDetailsActivity.A0c.get(i)).A00();
        if (A00 == 5) {
            A0w.add(A0D);
            if (C28141Kv.A01(((ActivityC13830kP) messageDetailsActivity).A01)) {
                if (z) {
                    width = findViewById.getWidth() - findViewById4.getWidth();
                } else {
                    width = A0D.getWidth();
                }
                A00(findViewById, this, width);
            }
        } else if (A00 == 8) {
            A0w.add(A0I2);
            A0w.add(findViewById5);
            A0w.add(A0I);
            A0w.add(findViewById4);
            A0w.add(A0D);
            if (C28141Kv.A01(((ActivityC13830kP) messageDetailsActivity).A01)) {
                if (z) {
                    width2 = findViewById3.getWidth() - findViewById6.getWidth();
                } else {
                    width2 = A0I2.getWidth();
                }
                A00(findViewById3, this, width2);
            }
        } else if (A00 == 13) {
            A0w.add(A0I);
            A0w.add(findViewById4);
            A0w.add(A0D);
            if (C28141Kv.A01(((ActivityC13830kP) messageDetailsActivity).A01)) {
                if (z) {
                    width3 = findViewById2.getWidth() - findViewById5.getWidth();
                } else {
                    width3 = A0I.getWidth();
                }
                A00(findViewById2, this, width3);
            }
        }
        Iterator it = A0w.iterator();
        while (it.hasNext()) {
            View view2 = (View) it.next();
            if (z) {
                alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            } else {
                alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            }
            int i2 = this.A01;
            alphaAnimation.setDuration((long) i2);
            alphaAnimation.setStartOffset((long) (((float) i2) * 0.0f));
            view2.startAnimation(alphaAnimation);
        }
        View findViewById7 = view.findViewById(R.id.timestamps);
        int height = findViewById7.getHeight();
        A01(view, i);
        findViewById7.measure(C12980iv.A04(findViewById7.getWidth()), View.MeasureSpec.makeMeasureSpec(0, 0));
        int measuredHeight = findViewById7.getMeasuredHeight();
        findViewById7.getLayoutParams().height = height;
        C73953h4 r2 = new C73953h4(findViewById7, this, height, measuredHeight);
        r2.setDuration((long) this.A01);
        findViewById7.startAnimation(r2);
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A02.A0c.size();
    }

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return this.A02.A0c.get(i);
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = C12960it.A0F(this.A02.getLayoutInflater(), viewGroup, R.layout.message_details_row);
        }
        A01(view, i);
        return view;
    }
}
