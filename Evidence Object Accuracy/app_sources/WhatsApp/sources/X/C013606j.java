package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;
import java.util.ArrayDeque;
import org.chromium.net.UrlRequest;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.06j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C013606j extends AbstractC013706k {
    public static final PorterDuff.Mode A09 = PorterDuff.Mode.SRC_IN;
    public ColorFilter A00;
    public PorterDuffColorFilter A01;
    public Drawable.ConstantState A02;
    public C013906m A03;
    public boolean A04;
    public boolean A05;
    public final Matrix A06;
    public final Rect A07;
    public final float[] A08;

    public C013606j() {
        this.A04 = true;
        this.A08 = new float[9];
        this.A06 = new Matrix();
        this.A07 = new Rect();
        this.A03 = new C013906m();
    }

    public C013606j(C013906m r3) {
        this.A04 = true;
        this.A08 = new float[9];
        this.A06 = new Matrix();
        this.A07 = new Rect();
        this.A03 = r3;
        this.A01 = A02(r3.A03, r3.A07);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0203, code lost:
        if (r11 == 1) goto L_0x0224;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0206, code lost:
        if (r11 == 2) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0208, code lost:
        r1 = r0.A01;
        r0 = r0.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x020c, code lost:
        if (r6 == 1) goto L_0x0213;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x020e, code lost:
        if (r6 == 2) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0210, code lost:
        r26 = android.graphics.Shader.TileMode.CLAMP;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0213, code lost:
        r26 = android.graphics.Shader.TileMode.REPEAT;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0216, code lost:
        r3 = new android.graphics.SweepGradient(r19, r18, r0.A01, r0.A00);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0226, code lost:
        if (r24 <= 0.0f) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0228, code lost:
        r2 = r0.A01;
        r1 = r0.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x022c, code lost:
        if (r6 == 1) goto L_0x0234;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x022f, code lost:
        if (r6 == 2) goto L_0x0237;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0231, code lost:
        r27 = android.graphics.Shader.TileMode.CLAMP;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0234, code lost:
        r27 = android.graphics.Shader.TileMode.REPEAT;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0237, code lost:
        r27 = android.graphics.Shader.TileMode.MIRROR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0239, code lost:
        r3 = new android.graphics.RadialGradient(r19, r18, r24, r2, r1, r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0249, code lost:
        r26 = android.graphics.Shader.TileMode.MIRROR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x024b, code lost:
        r3 = new android.graphics.LinearGradient(r20, r21, r22, r23, r1, r0, r26);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0264, code lost:
        throw new org.xmlpull.v1.XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x028f, code lost:
        return new X.C014606x(null, r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ee, code lost:
        if (r2.size() <= 0) goto L_0x01f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01f0, code lost:
        r0 = new X.AnonymousClass0TZ(r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01f6, code lost:
        if (r17 == false) goto L_0x01fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01f8, code lost:
        r0 = new X.AnonymousClass0TZ(r7, r15, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01fe, code lost:
        r0 = new X.AnonymousClass0TZ(r7, r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C014606x A00(android.content.res.Resources.Theme r23, android.content.res.TypedArray r24, java.lang.String r25, org.xmlpull.v1.XmlPullParser r26, int r27) {
        /*
        // Method dump skipped, instructions count: 656
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C013606j.A00(android.content.res.Resources$Theme, android.content.res.TypedArray, java.lang.String, org.xmlpull.v1.XmlPullParser, int):X.06x");
    }

    public static C013606j A01(Resources.Theme theme, Resources resources, int i) {
        int next;
        if (Build.VERSION.SDK_INT >= 24) {
            C013606j r2 = new C013606j();
            Drawable A04 = AnonymousClass00X.A04(theme, resources, i);
            ((AbstractC013706k) r2).A00 = A04;
            r2.A02 = new C020909y(A04.getConstantState());
            return r2;
        }
        try {
            XmlResourceParser xml = resources.getXml(i);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            do {
                next = xml.next();
                if (next == 2) {
                    C013606j r0 = new C013606j();
                    r0.inflate(resources, xml, asAttributeSet, theme);
                    return r0;
                }
            } while (next != 1);
            throw new XmlPullParserException("No start tag found");
        } catch (IOException | XmlPullParserException e) {
            Log.e("VectorDrawableCompat", "parser error", e);
            return null;
        }
    }

    public PorterDuffColorFilter A02(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean canApplyTheme() {
        Drawable drawable = super.A00;
        if (drawable == null) {
            return false;
        }
        C015607k.A0E(drawable);
        return false;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Paint paint;
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        Rect rect = this.A07;
        copyBounds(rect);
        if (rect.width() > 0 && rect.height() > 0) {
            ColorFilter colorFilter = this.A00;
            if (colorFilter == null) {
                colorFilter = this.A01;
            }
            Matrix matrix = this.A06;
            canvas.getMatrix(matrix);
            float[] fArr = this.A08;
            matrix.getValues(fArr);
            float abs = Math.abs(fArr[0]);
            float abs2 = Math.abs(fArr[4]);
            float abs3 = Math.abs(fArr[1]);
            float abs4 = Math.abs(fArr[3]);
            if (!(abs3 == 0.0f && abs4 == 0.0f)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min((int) EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, (int) (((float) rect.width()) * abs));
            int min2 = Math.min((int) EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, (int) (((float) rect.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                canvas.translate((float) rect.left, (float) rect.top);
                if (Build.VERSION.SDK_INT >= 17 && isAutoMirrored() && C015607k.A01(this) == 1) {
                    canvas.translate((float) rect.width(), 0.0f);
                    canvas.scale(-1.0f, 1.0f);
                }
                rect.offsetTo(0, 0);
                C013906m r1 = this.A03;
                Bitmap bitmap = r1.A04;
                if (!(bitmap != null && min == bitmap.getWidth() && min2 == r1.A04.getHeight())) {
                    r1.A04 = Bitmap.createBitmap(min, min2, Bitmap.Config.ARGB_8888);
                    r1.A0A = true;
                }
                boolean z = this.A04;
                C013906m r8 = this.A03;
                if (!z) {
                    r8.A00(min, min2);
                } else if (!(!r8.A0A && r8.A02 == r8.A03 && r8.A06 == r8.A07 && r8.A0B == r8.A09 && r8.A00 == r8.A08.A05)) {
                    r8.A00(min, min2);
                    C013906m r12 = this.A03;
                    r12.A02 = r12.A03;
                    r12.A06 = r12.A07;
                    r12.A00 = r12.A08.A05;
                    r12.A0B = r12.A09;
                    r12.A0A = false;
                }
                C013906m r6 = this.A03;
                if (r6.A08.A05 < 255 || colorFilter != null) {
                    if (r6.A05 == null) {
                        Paint paint2 = new Paint();
                        r6.A05 = paint2;
                        paint2.setFilterBitmap(true);
                    }
                    r6.A05.setAlpha(r6.A08.A05);
                    r6.A05.setColorFilter(colorFilter);
                    paint = r6.A05;
                } else {
                    paint = null;
                }
                canvas.drawBitmap(r6.A04, (Rect) null, rect, paint);
                canvas.restoreToCount(save);
            }
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A00(drawable);
        }
        return this.A03.A08.A05;
    }

    @Override // android.graphics.drawable.Drawable
    public int getChangingConfigurations() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.A03.A01;
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A02(drawable);
        }
        return this.A00;
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        Drawable drawable = super.A00;
        if (drawable != null && Build.VERSION.SDK_INT >= 24) {
            return new C020909y(drawable.getConstantState());
        }
        this.A03.A01 = getChangingConfigurations();
        return this.A03;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.A03.A08.A00;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.A03.A08.A01;
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, null);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        ColorStateList A01;
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A05(theme, resources, drawable, attributeSet, xmlPullParser);
            return;
        }
        C013906m r4 = this.A03;
        r4.A08 = new C014006n();
        TypedArray A012 = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A0B);
        C013906m r12 = this.A03;
        C014006n r11 = r12.A08;
        if (!AnonymousClass06r.A02("tintMode", xmlPullParser)) {
            i = -1;
        } else {
            i = A012.getInt(6, -1);
        }
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
        if (i == 3) {
            mode = PorterDuff.Mode.SRC_OVER;
        } else if (i != 5) {
            if (i != 9) {
                switch (i) {
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        mode = PorterDuff.Mode.MULTIPLY;
                        break;
                    case 15:
                        mode = PorterDuff.Mode.SCREEN;
                        break;
                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                        mode = PorterDuff.Mode.ADD;
                        break;
                }
            } else {
                mode = PorterDuff.Mode.SRC_ATOP;
            }
        }
        r12.A07 = mode;
        if (AnonymousClass06r.A02("tint", xmlPullParser)) {
            TypedValue typedValue = new TypedValue();
            A012.getValue(1, typedValue);
            int i6 = typedValue.type;
            if (i6 != 2) {
                if (i6 < 28 || i6 > 31) {
                    Resources resources2 = A012.getResources();
                    try {
                        A01 = C015207f.A01(theme, resources2, resources2.getXml(A012.getResourceId(1, 0)));
                    } catch (Exception e) {
                        Log.e("CSLCompat", "Failed to inflate ColorStateList.", e);
                    }
                } else {
                    A01 = ColorStateList.valueOf(typedValue.data);
                }
                if (A01 != null) {
                    r12.A03 = A01;
                }
            } else {
                StringBuilder sb = new StringBuilder("Failed to resolve attribute at index ");
                sb.append(1);
                sb.append(": ");
                sb.append(typedValue);
                throw new UnsupportedOperationException(sb.toString());
            }
        }
        boolean z = r12.A09;
        if (AnonymousClass06r.A02("autoMirrored", xmlPullParser)) {
            z = A012.getBoolean(5, z);
        }
        r12.A09 = z;
        float f = r11.A03;
        if (AnonymousClass06r.A02("viewportWidth", xmlPullParser)) {
            f = A012.getFloat(7, f);
        }
        r11.A03 = f;
        float f2 = r11.A02;
        if (AnonymousClass06r.A02("viewportHeight", xmlPullParser)) {
            f2 = A012.getFloat(8, f2);
        }
        r11.A02 = f2;
        if (r11.A03 <= 0.0f) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(A012.getPositionDescription());
            sb2.append("<vector> tag requires viewportWidth > 0");
            throw new XmlPullParserException(sb2.toString());
        } else if (f2 > 0.0f) {
            r11.A01 = A012.getDimension(3, r11.A01);
            float dimension = A012.getDimension(2, r11.A00);
            r11.A00 = dimension;
            if (r11.A01 <= 0.0f) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(A012.getPositionDescription());
                sb3.append("<vector> tag requires width > 0");
                throw new XmlPullParserException(sb3.toString());
            } else if (dimension > 0.0f) {
                float alpha = r11.getAlpha();
                if (AnonymousClass06r.A02("alpha", xmlPullParser)) {
                    alpha = A012.getFloat(4, alpha);
                }
                r11.setAlpha(alpha);
                String string = A012.getString(0);
                if (string != null) {
                    r11.A0A = string;
                    r11.A0E.put(string, r11);
                }
                A012.recycle();
                r4.A01 = getChangingConfigurations();
                r4.A0A = true;
                C013906m r3 = this.A03;
                C014006n r1 = r3.A08;
                ArrayDeque arrayDeque = new ArrayDeque();
                arrayDeque.push(r1.A0F);
                int eventType = xmlPullParser.getEventType();
                int depth = xmlPullParser.getDepth() + 1;
                boolean z2 = true;
                while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
                    if (eventType == 2) {
                        String name = xmlPullParser.getName();
                        AnonymousClass06o r122 = (AnonymousClass06o) arrayDeque.peek();
                        if ("path".equals(name)) {
                            AnonymousClass06s r13 = new AnonymousClass06s();
                            TypedArray A013 = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A0A);
                            r13.A0B = null;
                            if (AnonymousClass06r.A02("pathData", xmlPullParser)) {
                                String string2 = A013.getString(0);
                                if (string2 != null) {
                                    ((AbstractC014206t) r13).A02 = string2;
                                }
                                String string3 = A013.getString(2);
                                if (string3 != null) {
                                    ((AbstractC014206t) r13).A03 = C014306u.A02(string3);
                                }
                                r13.A09 = A00(theme, A013, "fillColor", xmlPullParser, 1);
                                float f3 = r13.A00;
                                if (AnonymousClass06r.A02("fillAlpha", xmlPullParser)) {
                                    f3 = A013.getFloat(12, f3);
                                }
                                r13.A00 = f3;
                                int i7 = -1;
                                if (!AnonymousClass06r.A02("strokeLineCap", xmlPullParser)) {
                                    i5 = -1;
                                } else {
                                    i5 = A013.getInt(8, -1);
                                }
                                Paint.Cap cap = r13.A07;
                                if (i5 == 0) {
                                    cap = Paint.Cap.BUTT;
                                } else if (i5 == 1) {
                                    cap = Paint.Cap.ROUND;
                                } else if (i5 == 2) {
                                    cap = Paint.Cap.SQUARE;
                                }
                                r13.A07 = cap;
                                if (AnonymousClass06r.A02("strokeLineJoin", xmlPullParser)) {
                                    i7 = A013.getInt(9, -1);
                                }
                                Paint.Join join = r13.A08;
                                if (i7 == 0) {
                                    join = Paint.Join.MITER;
                                } else if (i7 == 1) {
                                    join = Paint.Join.ROUND;
                                } else if (i7 == 2) {
                                    join = Paint.Join.BEVEL;
                                }
                                r13.A08 = join;
                                float f4 = r13.A02;
                                if (AnonymousClass06r.A02("strokeMiterLimit", xmlPullParser)) {
                                    f4 = A013.getFloat(10, f4);
                                }
                                r13.A02 = f4;
                                r13.A0A = A00(theme, A013, "strokeColor", xmlPullParser, 3);
                                float f5 = r13.A01;
                                if (AnonymousClass06r.A02("strokeAlpha", xmlPullParser)) {
                                    f5 = A013.getFloat(11, f5);
                                }
                                r13.A01 = f5;
                                float f6 = r13.A03;
                                if (AnonymousClass06r.A02("strokeWidth", xmlPullParser)) {
                                    f6 = A013.getFloat(4, f6);
                                }
                                r13.A03 = f6;
                                float f7 = r13.A04;
                                if (AnonymousClass06r.A02("trimPathEnd", xmlPullParser)) {
                                    f7 = A013.getFloat(6, f7);
                                }
                                r13.A04 = f7;
                                float f8 = r13.A05;
                                if (AnonymousClass06r.A02("trimPathOffset", xmlPullParser)) {
                                    f8 = A013.getFloat(7, f8);
                                }
                                r13.A05 = f8;
                                float f9 = r13.A06;
                                if (AnonymousClass06r.A02("trimPathStart", xmlPullParser)) {
                                    f9 = A013.getFloat(5, f9);
                                }
                                r13.A06 = f9;
                                int i8 = ((AbstractC014206t) r13).A01;
                                if (AnonymousClass06r.A02("fillType", xmlPullParser)) {
                                    i8 = A013.getInt(13, i8);
                                }
                                ((AbstractC014206t) r13).A01 = i8;
                            }
                            A013.recycle();
                            r122.A0C.add(r13);
                            String str = ((AbstractC014206t) r13).A02;
                            if (str != null) {
                                r1.A0E.put(str, r13);
                            }
                            z2 = false;
                            i2 = r3.A01;
                            i3 = ((AbstractC014206t) r13).A00;
                        } else if ("clip-path".equals(name)) {
                            AnonymousClass0GD r132 = new AnonymousClass0GD();
                            if (AnonymousClass06r.A02("pathData", xmlPullParser)) {
                                TypedArray A014 = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A08);
                                String string4 = A014.getString(0);
                                if (string4 != null) {
                                    r132.A02 = string4;
                                }
                                String string5 = A014.getString(1);
                                if (string5 != null) {
                                    r132.A03 = C014306u.A02(string5);
                                }
                                if (!AnonymousClass06r.A02("fillType", xmlPullParser)) {
                                    i4 = 0;
                                } else {
                                    i4 = A014.getInt(2, 0);
                                }
                                r132.A01 = i4;
                                A014.recycle();
                            }
                            r122.A0C.add(r132);
                            String str2 = r132.A02;
                            if (str2 != null) {
                                r1.A0E.put(str2, r132);
                            }
                            i2 = r3.A01;
                            i3 = r132.A00;
                        } else if ("group".equals(name)) {
                            AnonymousClass06o r112 = new AnonymousClass06o();
                            TypedArray A015 = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A09);
                            r112.A09 = null;
                            float f10 = r112.A02;
                            if (AnonymousClass06r.A02("rotation", xmlPullParser)) {
                                f10 = A015.getFloat(5, f10);
                            }
                            r112.A02 = f10;
                            r112.A00 = A015.getFloat(1, r112.A00);
                            r112.A01 = A015.getFloat(2, r112.A01);
                            float f11 = r112.A03;
                            if (AnonymousClass06r.A02("scaleX", xmlPullParser)) {
                                f11 = A015.getFloat(3, f11);
                            }
                            r112.A03 = f11;
                            float f12 = r112.A04;
                            if (AnonymousClass06r.A02("scaleY", xmlPullParser)) {
                                f12 = A015.getFloat(4, f12);
                            }
                            r112.A04 = f12;
                            float f13 = r112.A05;
                            if (AnonymousClass06r.A02("translateX", xmlPullParser)) {
                                f13 = A015.getFloat(6, f13);
                            }
                            r112.A05 = f13;
                            float f14 = r112.A06;
                            if (AnonymousClass06r.A02("translateY", xmlPullParser)) {
                                f14 = A015.getFloat(7, f14);
                            }
                            r112.A06 = f14;
                            String string6 = A015.getString(0);
                            if (string6 != null) {
                                r112.A08 = string6;
                            }
                            r112.A02();
                            A015.recycle();
                            r122.A0C.add(r112);
                            arrayDeque.push(r112);
                            String str3 = r112.A08;
                            if (str3 != null) {
                                r1.A0E.put(str3, r112);
                            }
                            i2 = r3.A01;
                            i3 = r112.A07;
                        }
                        r3.A01 = i3 | i2;
                    } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                        arrayDeque.pop();
                    }
                    eventType = xmlPullParser.next();
                }
                if (!z2) {
                    this.A01 = A02(r4.A03, r4.A07);
                    return;
                }
                throw new XmlPullParserException("no path defined");
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(A012.getPositionDescription());
                sb4.append("<vector> tag requires height > 0");
                throw new XmlPullParserException(sb4.toString());
            }
        } else {
            StringBuilder sb5 = new StringBuilder();
            sb5.append(A012.getPositionDescription());
            sb5.append("<vector> tag requires viewportHeight > 0");
            throw new XmlPullParserException(sb5.toString());
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void invalidateSelf() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isAutoMirrored() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A0F(drawable);
        }
        return this.A03.A09;
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.isStateful();
        }
        if (super.isStateful()) {
            return true;
        }
        C013906m r0 = this.A03;
        if (r0 == null) {
            return false;
        }
        C014006n r1 = r0.A08;
        Boolean bool = r1.A09;
        if (bool == null) {
            bool = Boolean.valueOf(r1.A0F.A00());
            r1.A09 = bool;
        }
        if (bool.booleanValue()) {
            return true;
        }
        ColorStateList colorStateList = this.A03.A03;
        return colorStateList != null && colorStateList.isStateful();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.mutate();
        } else if (!this.A05 && super.mutate() == this) {
            this.A03 = new C013906m(this.A03);
            this.A05 = true;
            return this;
        }
        return this;
    }

    @Override // X.AbstractC013706k, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        C013906m r3 = this.A03;
        ColorStateList colorStateList = r3.A03;
        if (!(colorStateList == null || (mode = r3.A07) == null)) {
            this.A01 = A02(colorStateList, mode);
            invalidateSelf();
            z = true;
        }
        C014006n r1 = r3.A08;
        Boolean bool = r1.A09;
        if (bool == null) {
            bool = Boolean.valueOf(r1.A0F.A00());
            r1.A09 = bool;
        }
        if (bool.booleanValue()) {
            boolean A01 = r3.A08.A0F.A01(iArr);
            r3.A0A |= A01;
            if (A01) {
                invalidateSelf();
                return true;
            }
        }
        return z;
    }

    @Override // android.graphics.drawable.Drawable
    public void scheduleSelf(Runnable runnable, long j) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j);
        } else {
            super.scheduleSelf(runnable, j);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.setAlpha(i);
            return;
        }
        C014006n r1 = this.A03.A08;
        if (r1.A05 != i) {
            r1.A05 = i;
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAutoMirrored(boolean z) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A0C(drawable, z);
        } else {
            this.A03.A09 = z;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.A00 = colorFilter;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTint(int i) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A0A(drawable, i);
        } else {
            setTintList(ColorStateList.valueOf(i));
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A04(colorStateList, drawable);
            return;
        }
        C013906m r1 = this.A03;
        if (r1.A03 != colorStateList) {
            r1.A03 = colorStateList;
            this.A01 = A02(colorStateList, r1.A07);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A07(mode, drawable);
            return;
        }
        C013906m r1 = this.A03;
        if (r1.A07 != mode) {
            r1.A07 = mode;
            this.A01 = A02(r1.A03, mode);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Drawable
    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }
}
