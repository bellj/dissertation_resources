package X;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1tc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41491tc {
    public C41611to A00;
    public final Handler A01 = new Handler(Looper.getMainLooper());
    public final C14330lG A02;
    public final C16590pI A03;
    public final AnonymousClass018 A04;
    public final AnonymousClass19M A05;
    public final C14410lO A06;
    public final AnonymousClass1EK A07;
    public final AnonymousClass1AB A08;
    public final C22590zK A09;
    public final C41571tk A0A = new C41571tk(this);
    public final AtomicInteger A0B = new AtomicInteger();

    public C41491tc(C14330lG r3, C16590pI r4, AnonymousClass018 r5, AnonymousClass19M r6, C14410lO r7, AnonymousClass1EK r8, AnonymousClass1AB r9, C22590zK r10) {
        this.A03 = r4;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A04 = r5;
        this.A09 = r10;
        this.A08 = r9;
        this.A07 = r8;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(16:2|(1:4)(1:22)|(1:6)(1:21)|7|(4:9|(1:20)|(4:14|(2:19|15)|23|(11:25|(1:27)|29|69|(1:31)(1:32)|67|33|(1:41)|(3:43|(3:45|(1:47)|48)|(1:51))|52|53))|61)|28|29|69|(0)(0)|67|33|(4:35|37|39|41)|(0)|52|53|(1:(0))) */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00f4, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00f9, code lost:
        if (X.AnonymousClass1O1.A00() == false) goto L_0x00fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00fb, code lost:
        com.whatsapp.util.Log.e("MessageThumbsThread/bitmap-decode/OutOfMemory avoided");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0101, code lost:
        throw r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007e A[Catch: IOException -> 0x0100, OutOfMemoryError -> 0x00f4, TRY_ENTER, TryCatch #4 {IOException -> 0x0100, OutOfMemoryError -> 0x00f4, blocks: (B:31:0x007e, B:32:0x0084, B:52:0x00eb, B:56:0x00f3), top: B:69:0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0084 A[Catch: IOException -> 0x0100, OutOfMemoryError -> 0x00f4, TRY_LEAVE, TryCatch #4 {IOException -> 0x0100, OutOfMemoryError -> 0x00f4, blocks: (B:31:0x007e, B:32:0x0084, B:52:0x00eb, B:56:0x00f3), top: B:69:0x007c }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bb A[Catch: all -> 0x00ef, TryCatch #3 {all -> 0x00ef, blocks: (B:33:0x009a, B:35:0x00aa, B:37:0x00ae, B:39:0x00b2, B:41:0x00b6, B:43:0x00bb, B:45:0x00cf, B:47:0x00e0, B:51:0x00e8), top: B:67:0x009a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ android.graphics.Bitmap A00(X.C16150oX r14, X.C41581tl r15, X.C41491tc r16, java.io.File r17, int[] r18) {
        /*
        // Method dump skipped, instructions count: 258
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41491tc.A00(X.0oX, X.1tl, X.1tc, java.io.File, int[]):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0052, code lost:
        if ((r14 instanceof X.AbstractC16130oV) == false) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0054, code lost:
        r6 = (X.AbstractC16130oV) r14;
        r4 = r6.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        if (r4 == null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005c, code lost:
        if (r4.A0P == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0060, code lost:
        if (r4.A0F == null) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        if (r4.A03() != false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006a, code lost:
        if (r4.A0X != false) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x006c, code lost:
        r4.A0X = true;
        r12.A01.post(new com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13(r15, 16));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007d, code lost:
        if (r4.A0F != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        if ((r6 instanceof X.C30061Vy) != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0085, code lost:
        if ((r6 instanceof X.AnonymousClass1X7) != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0089, code lost:
        if ((r6 instanceof X.AnonymousClass1X2) != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008c, code lost:
        r4.A0X = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0092, code lost:
        if ((r6 instanceof X.AnonymousClass1XH) != false) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0094, code lost:
        r4 = new X.C41581tl(r13, r6, r15, r16, r12, r17, r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x009c, code lost:
        if ((r14 instanceof X.C28861Ph) == false) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a0, code lost:
        if (r14.A0T == null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a2, code lost:
        r4 = new X.C41581tl(r13, r14, r15, r16, r12, r17, r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00a7, code lost:
        r1.offer(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ac, code lost:
        if (r12.A00 != null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ae, code lost:
        r4 = new X.C41611to(r12.A02, r12.A04, r12.A05, r12.A09, r12);
        r12.A00 = r4;
        r4.start();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A01(android.view.View r13, X.AbstractC15340mz r14, X.C41561tj r15, X.AbstractC41521tf r16, java.lang.Object r17, boolean r18) {
        /*
            r12 = this;
            r6 = r14
            r9 = r12
            monitor-enter(r9)
            X.1tk r0 = r12.A0A     // Catch: all -> 0x00c2
            java.util.concurrent.LinkedBlockingDeque r1 = r0.A00     // Catch: all -> 0x00c2
            java.util.Iterator r3 = r1.iterator()     // Catch: all -> 0x00c2
        L_0x000b:
            boolean r0 = r3.hasNext()     // Catch: all -> 0x00c2
            r5 = r13
            if (r0 == 0) goto L_0x0020
            java.lang.Object r2 = r3.next()     // Catch: all -> 0x00c2
            X.1tl r2 = (X.C41581tl) r2     // Catch: all -> 0x00c2
            android.view.View r0 = r2.A00     // Catch: all -> 0x00c2
            if (r0 != r13) goto L_0x000b
            r1.remove(r2)     // Catch: all -> 0x00c2
            goto L_0x000b
        L_0x0020:
            java.util.Iterator r4 = r1.iterator()     // Catch: all -> 0x00c2
        L_0x0024:
            boolean r0 = r4.hasNext()     // Catch: all -> 0x00c2
            r10 = r17
            if (r0 == 0) goto L_0x004b
            java.lang.Object r3 = r4.next()     // Catch: all -> 0x00c2
            X.1tl r3 = (X.C41581tl) r3     // Catch: all -> 0x00c2
            X.0mz r0 = r3.A01     // Catch: all -> 0x00c2
            X.1IS r2 = r0.A0z     // Catch: all -> 0x00c2
            X.1IS r0 = r14.A0z     // Catch: all -> 0x00c2
            boolean r0 = r2.equals(r0)     // Catch: all -> 0x00c2
            if (r0 == 0) goto L_0x0024
            java.lang.Object r0 = r3.A04     // Catch: all -> 0x00c2
            boolean r0 = r0.equals(r10)     // Catch: all -> 0x00c2
            if (r0 == 0) goto L_0x0024
            android.view.View r0 = r3.A00     // Catch: all -> 0x00c2
            if (r0 != r13) goto L_0x0024
            goto L_0x00c0
        L_0x004b:
            boolean r0 = r14 instanceof X.AbstractC16130oV     // Catch: all -> 0x00c2
            r11 = r18
            r7 = r15
            r8 = r16
            if (r0 == 0) goto L_0x009a
            X.0oV r6 = (X.AbstractC16130oV) r6     // Catch: all -> 0x00c2
            X.0oX r4 = r6.A02     // Catch: all -> 0x00c2
            if (r4 == 0) goto L_0x00c0
            boolean r0 = r4.A0P     // Catch: all -> 0x00c2
            if (r0 == 0) goto L_0x008c
            java.io.File r0 = r4.A0F     // Catch: all -> 0x00c2
            if (r0 == 0) goto L_0x008c
            boolean r0 = r4.A03()     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x008c
            boolean r0 = r4.A0X     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x007b
            r0 = 1
            r4.A0X = r0     // Catch: all -> 0x00c2
            android.os.Handler r3 = r12.A01     // Catch: all -> 0x00c2
            r2 = 16
            com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13 r0 = new com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13     // Catch: all -> 0x00c2
            r0.<init>(r15, r2)     // Catch: all -> 0x00c2
            r3.post(r0)     // Catch: all -> 0x00c2
        L_0x007b:
            java.io.File r0 = r4.A0F     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x0090
            boolean r0 = r6 instanceof X.C30061Vy     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x0090
            boolean r0 = r6 instanceof X.AnonymousClass1X7     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x0090
            boolean r0 = r6 instanceof X.AnonymousClass1X2     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x0090
            goto L_0x00c0
        L_0x008c:
            r0 = 0
            r4.A0X = r0     // Catch: all -> 0x00c2
            goto L_0x007b
        L_0x0090:
            boolean r0 = r6 instanceof X.AnonymousClass1XH     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x00c0
            X.1tl r4 = new X.1tl     // Catch: all -> 0x00c2
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)     // Catch: all -> 0x00c2
            goto L_0x00a7
        L_0x009a:
            boolean r0 = r14 instanceof X.C28861Ph     // Catch: all -> 0x00c2
            if (r0 == 0) goto L_0x00c0
            X.0lJ r0 = r14.A0T     // Catch: all -> 0x00c2
            if (r0 == 0) goto L_0x00c0
            X.1tl r4 = new X.1tl     // Catch: all -> 0x00c2
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)     // Catch: all -> 0x00c2
        L_0x00a7:
            r1.offer(r4)     // Catch: all -> 0x00c2
            X.1to r0 = r12.A00     // Catch: all -> 0x00c2
            if (r0 != 0) goto L_0x00c0
            X.0lG r5 = r12.A02     // Catch: all -> 0x00c2
            X.19M r7 = r12.A05     // Catch: all -> 0x00c2
            X.018 r6 = r12.A04     // Catch: all -> 0x00c2
            X.0zK r8 = r12.A09     // Catch: all -> 0x00c2
            X.1to r4 = new X.1to     // Catch: all -> 0x00c2
            r4.<init>(r5, r6, r7, r8, r9)     // Catch: all -> 0x00c2
            r12.A00 = r4     // Catch: all -> 0x00c2
            r4.start()     // Catch: all -> 0x00c2
        L_0x00c0:
            monitor-exit(r9)
            return
        L_0x00c2:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41491tc.A01(android.view.View, X.0mz, X.1tj, X.1tf, java.lang.Object, boolean):void");
    }
}
