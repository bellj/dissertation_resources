package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.facebook.animated.webp.WebPImage;
import com.whatsapp.R;
import com.whatsapp.stickers.WebpUtils;
import com.whatsapp.util.Log;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1AB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AB {
    public C39611qC A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C17170qN A03;
    public final C14850m9 A04;
    public final C39591qA A05 = new C39591qA();
    public final C22590zK A06;
    public final C39601qB A07;
    public final ConcurrentHashMap A08;
    public final ConcurrentHashMap A09;

    public AnonymousClass1AB(C14330lG r2, C14900mE r3, C17170qN r4, C14850m9 r5, C22590zK r6, AnonymousClass1EL r7) {
        this.A04 = r5;
        this.A02 = r3;
        this.A01 = r2;
        this.A06 = r6;
        this.A03 = r4;
        this.A07 = new C39601qB(r3, r5, r7);
        this.A09 = new ConcurrentHashMap();
        this.A08 = new ConcurrentHashMap();
    }

    public static String A00(AnonymousClass1KS r5, int i, int i2) {
        StringBuilder sb;
        String obj;
        String str = r5.A0C;
        if (str != null) {
            sb = new StringBuilder();
            obj = str.replace("/", "-");
        } else {
            sb = new StringBuilder();
            obj = r5.toString();
        }
        sb.append(obj);
        sb.append("_");
        sb.append(i);
        sb.append("_");
        sb.append(i2);
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x0115  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] A01(android.content.Context r10, X.C14330lG r11, X.AnonymousClass1KS r12) {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AB.A01(android.content.Context, X.0lG, X.1KS):byte[]");
    }

    public final Drawable A02(AnonymousClass1qE r20, String str, byte[] bArr) {
        Context context;
        int min;
        int i;
        AnonymousClass1KS r6;
        AnonymousClass1KB r0;
        String str2;
        ConcurrentHashMap concurrentHashMap = this.A08;
        String str3 = r20.A04;
        Reference reference = (Reference) concurrentHashMap.get(str3);
        if (reference != null) {
            C28001Kb r5 = (C28001Kb) reference.get();
            if (r5 != null) {
                return new C39631qF(r5);
            }
            concurrentHashMap.remove(str3);
        }
        WebPImage A00 = C22590zK.A00(bArr);
        if (A00 != null) {
            boolean z = false;
            C14850m9 r10 = this.A04;
            if (r10.A07(295) && !((r0 = (r6 = r20.A03).A04) == null && ((str2 = r6.A08) == null || (r0 = AnonymousClass1KB.A00(WebpUtils.fetchWebpMetadata(str2))) == null))) {
                z = !r0.A06;
            }
            if (A00.getFrameCount() == 1 || z) {
                Bitmap A05 = this.A06.A05(str, bArr, r20.A02, r20.A00);
                if (A05 != null) {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(A05);
                    this.A09.put(str3, new SoftReference(bitmapDrawable));
                    return bitmapDrawable;
                }
                StringBuilder sb = new StringBuilder("StickerImageLoader/loadAnimatedSticker failed to create drawable, hash: ");
                sb.append(str);
                Log.e(sb.toString());
            } else {
                if (r10.A07(276)) {
                    min = Math.min(512, r20.A02);
                    i = Math.min(512, r20.A00);
                    if (r20.A06) {
                        min = (int) (((float) min) / 2.0f);
                        i = (int) (((float) i) / 2.0f);
                    }
                } else {
                    if (!(r20 instanceof C39651qH)) {
                        context = ((AnonymousClass1qI) r20).A00;
                    } else {
                        context = ((C39651qH) r20).A00.getContext();
                    }
                    min = Math.min(512, context.getResources().getDimensionPixelSize(R.dimen.conversation_row_sticker_size));
                    i = min;
                }
                C22590zK r1 = this.A06;
                String A01 = C22590zK.A01(str3, min, i);
                Bitmap A04 = r1.A04(A01);
                if (!(A04 == null && (A04 = r1.A02(A00, A01, min, i)) == null)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str.replace('/', '-'));
                    sb2.append("_");
                    sb2.append(min);
                    sb2.append("_");
                    sb2.append(i);
                    C28001Kb r11 = new C28001Kb(A04, A00, this.A02, this.A07, sb2.toString(), min, i);
                    concurrentHashMap.put(str3, new WeakReference(r11));
                    return new C39631qF(r11);
                }
            }
        }
        return null;
    }

    public void A03() {
        C39611qC r1 = this.A00;
        if (r1 != null) {
            r1.A01 = true;
            r1.interrupt();
            this.A00 = null;
        }
        C39591qA r12 = this.A05;
        synchronized (r12) {
            r12.A00.clear();
        }
        C39601qB r13 = this.A07;
        C39621qD r0 = r13.A00;
        if (r0 != null) {
            r0.A00();
            r13.A00 = null;
        }
    }

    public void A04(ImageView imageView, AnonymousClass1KS r18, AbstractC39661qJ r19, int i, int i2, int i3, boolean z, boolean z2) {
        PriorityQueue priorityQueue;
        Drawable drawable;
        String A00 = A00(r18, i2, i3);
        imageView.setTag(A00);
        C39591qA r2 = this.A05;
        synchronized (r2) {
            priorityQueue = r2.A00;
            Iterator it = priorityQueue.iterator();
            while (it.hasNext()) {
                if (((C39651qH) it.next()).A00 == imageView) {
                    it.remove();
                }
            }
        }
        Drawable drawable2 = imageView.getDrawable();
        Reference reference = (Reference) this.A09.get(A00);
        if (reference == null || (drawable = (Drawable) reference.get()) == null) {
            drawable = null;
            if (r18.A0C != null) {
                ConcurrentHashMap concurrentHashMap = this.A08;
                Reference reference2 = (Reference) concurrentHashMap.get(A00);
                if (reference2 != null) {
                    C28001Kb r0 = (C28001Kb) reference2.get();
                    if (r0 != null) {
                        drawable = new C39631qF(r0);
                    } else {
                        concurrentHashMap.remove(A00);
                    }
                }
            }
        }
        if (drawable2 == null || !drawable2.equals(drawable)) {
            if (drawable != null) {
                imageView.setImageDrawable(drawable);
            } else {
                imageView.setImageResource(R.drawable.sticker_loading);
                C39651qH r6 = new C39651qH(imageView, r18, r19, A00, i2, i3, i, z, z2);
                synchronized (r2) {
                    priorityQueue.add(r6);
                    r2.notifyAll();
                }
                if (this.A00 == null) {
                    C39611qC r02 = new C39611qC(r2, this);
                    this.A00 = r02;
                    r02.start();
                    return;
                }
                return;
            }
        }
        if (r19 != null) {
            r19.AWe(true);
        }
    }

    public final void A05(C14900mE r6, AnonymousClass1qE r7, Map map) {
        Context context;
        Reference reference;
        boolean z = r7 instanceof C39651qH;
        if (z) {
            C39651qH r0 = (C39651qH) r7;
            if (!r0.A04.equals(r0.A00.getTag())) {
                return;
            }
        }
        Drawable drawable = null;
        if (map == null || (reference = (Reference) map.get(r7.A04)) == null || (drawable = (Drawable) reference.get()) == null) {
            if (!z) {
                context = ((AnonymousClass1qI) r7).A00;
            } else {
                context = ((C39651qH) r7).A00.getContext();
            }
            AnonymousClass1KS r2 = r7.A03;
            byte[] A01 = A01(context, this.A01, r2);
            if (A01 != null) {
                if (r7.A05) {
                    String str = r2.A0C;
                    AnonymousClass009.A05(str);
                    drawable = A02(r7, str, A01);
                } else {
                    Bitmap A05 = this.A06.A05(r7.A04, A01, r7.A02, r7.A00);
                    drawable = A05 != null ? new BitmapDrawable(A05) : null;
                }
            }
            if (map != null) {
                if (drawable != null) {
                    map.put(r7.A04, new SoftReference(drawable));
                } else {
                    StringBuilder sb = new StringBuilder("StickerImageLoader/loadSticker failed to create drawable: ");
                    sb.append(r7.A04);
                    Log.e(sb.toString());
                }
            }
        }
        r7.A00(drawable, r6);
    }
}
