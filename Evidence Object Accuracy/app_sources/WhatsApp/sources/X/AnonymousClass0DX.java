package X;

import android.content.Context;
import android.view.ActionProvider;

/* renamed from: X.0DX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DX extends AbstractC04730Mv {
    public final ActionProvider A00;
    public final /* synthetic */ AnonymousClass0CJ A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0DX(Context context, ActionProvider actionProvider, AnonymousClass0CJ r3) {
        super(context);
        this.A01 = r3;
        this.A00 = actionProvider;
    }
}
