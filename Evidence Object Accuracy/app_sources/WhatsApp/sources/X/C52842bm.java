package X;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.whatsapp.R;
import com.whatsapp.audiopicker.AudioPickerActivity;

/* renamed from: X.2bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52842bm extends CursorAdapter {
    public final ActivityC13810kN A00;
    public final /* synthetic */ AudioPickerActivity A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52842bm(ActivityC13810kN r3, AudioPickerActivity audioPickerActivity) {
        super(r3, (Cursor) null, 0);
        this.A01 = audioPickerActivity;
        this.A00 = r3;
    }

    public final C63353Bg A00(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        return new C63353Bg(this.A01, cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getInt(0), cursor.getInt(4), cursor.getInt(5));
    }

    @Override // android.widget.CursorAdapter
    public void bindView(View view, Context context, Cursor cursor) {
        new C64393Fj(view, this.A01, cursor.getPosition()).A03(this.A00, A00(cursor));
    }

    @Override // android.widget.CursorAdapter
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return C12960it.A0F(this.A01.getLayoutInflater(), viewGroup, R.layout.audio_file_row);
    }
}
