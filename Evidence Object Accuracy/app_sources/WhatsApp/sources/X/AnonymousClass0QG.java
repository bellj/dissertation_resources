package X;

/* renamed from: X.0QG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QG {
    public final AnonymousClass00N A00 = new AnonymousClass00N();
    public final AnonymousClass036 A01 = new AnonymousClass036();

    public final AnonymousClass0N0 A00(AnonymousClass03U r7, int i) {
        C05920Rm r3;
        AnonymousClass0N0 r1;
        AnonymousClass00N r4 = this.A00;
        int A05 = r4.A05(r7, r7.hashCode());
        if (A05 >= 0 && (r3 = (C05920Rm) r4.A02[(A05 << 1) + 1]) != null) {
            int i2 = r3.A00;
            if ((i2 & i) != 0) {
                int i3 = (i ^ -1) & i2;
                r3.A00 = i3;
                if (i == 4) {
                    r1 = r3.A02;
                } else if (i == 8) {
                    r1 = r3.A01;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((i3 & 12) == 0) {
                    r4.A06(A05);
                    r3.A00 = 0;
                    r3.A02 = null;
                    r3.A01 = null;
                    C05920Rm.A03.Aa6(r3);
                }
                return r1;
            }
        }
        return null;
    }

    public void A01(AnonymousClass0N0 r4, AnonymousClass03U r5) {
        AnonymousClass00N r2 = this.A00;
        C05920Rm r1 = (C05920Rm) r2.get(r5);
        if (r1 == null) {
            r1 = (C05920Rm) C05920Rm.A03.A5a();
            if (r1 == null) {
                r1 = new C05920Rm();
            }
            r2.put(r5, r1);
        }
        r1.A01 = r4;
        r1.A00 |= 8;
    }

    public void A02(AnonymousClass0N0 r4, AnonymousClass03U r5) {
        AnonymousClass00N r2 = this.A00;
        C05920Rm r1 = (C05920Rm) r2.get(r5);
        if (r1 == null) {
            r1 = (C05920Rm) C05920Rm.A03.A5a();
            if (r1 == null) {
                r1 = new C05920Rm();
            }
            r2.put(r5, r1);
        }
        r1.A02 = r4;
        r1.A00 |= 4;
    }

    public void A03(AnonymousClass03U r4) {
        AnonymousClass00N r2 = this.A00;
        C05920Rm r1 = (C05920Rm) r2.get(r4);
        if (r1 == null) {
            r1 = (C05920Rm) C05920Rm.A03.A5a();
            if (r1 == null) {
                r1 = new C05920Rm();
            }
            r2.put(r4, r1);
        }
        r1.A00 |= 1;
    }

    public void A04(AnonymousClass03U r3) {
        C05920Rm r1 = (C05920Rm) this.A00.get(r3);
        if (r1 != null) {
            r1.A00 &= -2;
        }
    }

    public void A05(AnonymousClass03U r6) {
        AnonymousClass036 r4 = this.A01;
        int A00 = r4.A00();
        while (true) {
            A00--;
            if (A00 < 0) {
                break;
            } else if (r6 == r4.A03(A00)) {
                Object[] objArr = r4.A03;
                Object obj = objArr[A00];
                Object obj2 = AnonymousClass036.A04;
                if (obj != obj2) {
                    objArr[A00] = obj2;
                    r4.A01 = true;
                }
            }
        }
        C05920Rm r1 = (C05920Rm) this.A00.remove(r6);
        if (r1 != null) {
            r1.A00 = 0;
            r1.A02 = null;
            r1.A01 = null;
            C05920Rm.A03.Aa6(r1);
        }
    }
}
