package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.WaBloksActivity;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5v4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127795v4 {
    public final AnonymousClass6MG A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;
    public final List A03;

    public C127795v4(AnonymousClass01H r5, AnonymousClass01H r6) {
        C134506Ew r3 = new AnonymousClass6MG() { // from class: X.6Ew
            @Override // X.AnonymousClass6MG
            public final Intent AYx(Context context, Uri uri) {
                C127795v4 r62 = C127795v4.this;
                C126185sT r4 = new C126185sT();
                String obj = uri.toString();
                AnonymousClass01H r32 = r62.A01;
                String A03 = ((C14850m9) r32.get()).A03(265);
                boolean z = false;
                if (((C14850m9) r32.get()).A07(267) && !TextUtils.isEmpty(A03)) {
                    r62.A02.get();
                    if (C130245z2.A00(r4, obj, A03) && r4.A00 != null) {
                        z = true;
                    }
                }
                if (!z) {
                    return null;
                }
                try {
                    JSONObject jSONObject = r4.A00;
                    JSONObject A0a = C117295Zj.A0a();
                    A0a.put("server_params", jSONObject);
                    return WaBloksActivity.A09(context, "com.bloks.www.minishops.storefront.wa", C117305Zk.A0l(A0a, "params", C117295Zj.A0a()));
                } catch (JSONException e) {
                    Log.e("ShopsLinks.handleStoreFrontLink: Failed to assemble JSON", e);
                    return null;
                }
            }
        };
        this.A00 = r3;
        AnonymousClass6MG[] r2 = new AnonymousClass6MG[2];
        r2[0] = new AnonymousClass6MG() { // from class: X.6Ex
            @Override // X.AnonymousClass6MG
            public final Intent AYx(Context context, Uri uri) {
                JSONObject jSONObject;
                C127795v4 r32 = C127795v4.this;
                String A03 = ((C14850m9) r32.A01.get()).A03(210);
                if (TextUtils.isEmpty(A03)) {
                    return null;
                }
                C126185sT r1 = new C126185sT();
                r32.A02.get();
                if (!C130245z2.A00(r1, uri.toString(), A03) || (jSONObject = r1.A00) == null) {
                    return null;
                }
                try {
                    JSONObject A0a = C117295Zj.A0a();
                    A0a.put("server_params", jSONObject);
                    return WaBloksActivity.A09(context, "com.bloks.www.minishops.whatsapp.pdp", C117305Zk.A0l(A0a, "params", C117295Zj.A0a()));
                } catch (JSONException e) {
                    Log.e("ShopsLinks.handleShopsPdpLink: Failed to assemble JSON", e);
                    return null;
                }
            }
        };
        this.A03 = C12960it.A0m(r3, r2, 1);
        this.A01 = r5;
        this.A02 = r6;
    }
}
