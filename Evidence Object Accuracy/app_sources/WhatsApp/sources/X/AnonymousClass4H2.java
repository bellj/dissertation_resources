package X;

/* renamed from: X.4H2  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4H2 {
    public static final String A00;
    public static final String A01;

    static {
        String replaceAll = "12451".replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");
        A00 = replaceAll;
        String valueOf = String.valueOf(replaceAll);
        A01 = C72453ed.A0s("ma", valueOf, valueOf.length());
    }
}
