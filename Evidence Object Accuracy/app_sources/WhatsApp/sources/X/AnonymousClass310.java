package X;

/* renamed from: X.310  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass310 extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public String A06;

    public AnonymousClass310() {
        super(2350, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A03);
        r3.Abe(5, this.A04);
        r3.Abe(3, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(4, this.A05);
        r3.Abe(1, this.A06);
        r3.Abe(7, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStorageManagement {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementDeletedNumFiles", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementDeletedSize", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementEntryPoint", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementEventType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementOverallSize", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementSessionId", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storageManagementVersion", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
