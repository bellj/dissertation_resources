package X;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.util.Log;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/* renamed from: X.4dy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95564dy {
    public static int A01 = -1;
    public static C79523qj A02;
    public static C79533qk A03;
    public static Boolean A04;
    public static String A05;
    public static boolean A06;
    public static final AbstractC116365Vd A07 = new AnonymousClass4z7();
    public static final AbstractC115575Sc A08 = new C108484zA();
    public static final AbstractC115575Sc A09 = new C108474z9();
    public static final ThreadLocal A0A = new ThreadLocal();
    public static final ThreadLocal A0B = new AnonymousClass5HG();
    public final Context A00;

    public C95564dy(Context context) {
        C13020j0.A01(context);
        this.A00 = context;
    }

    public static int A00(Context context, String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 61);
            A0t.append("com.google.android.gms.dynamite.descriptors.");
            A0t.append(str);
            A0t.append(".");
            Class<?> loadClass = classLoader.loadClass(C12960it.A0d("ModuleDescriptor", A0t));
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (C13300jT.A00(declaredField.get(null), str)) {
                return declaredField2.getInt(null);
            }
            String valueOf = String.valueOf(declaredField.get(null));
            StringBuilder A0t2 = C12980iv.A0t(valueOf.length() + 51 + C12970iu.A07(str));
            A0t2.append("Module descriptor id '");
            A0t2.append(valueOf);
            A0t2.append("' didn't match expected id '");
            A0t2.append(str);
            Log.e("DynamiteModule", C12960it.A0d("'", A0t2));
            return 0;
        } catch (ClassNotFoundException unused) {
            StringBuilder A0t3 = C12980iv.A0t(C12970iu.A07(str) + 45);
            A0t3.append("Local module descriptor class for ");
            A0t3.append(str);
            Log.w("DynamiteModule", C12960it.A0d(" not found.", A0t3));
            return 0;
        } catch (Exception e) {
            Log.e("DynamiteModule", C12960it.A0c(String.valueOf(e.getMessage()), "Failed to load module descriptor class: "));
            return 0;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r21v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00be, code lost:
        r9 = new X.AnonymousClass5HB(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c3, code lost:
        r9.setContextClassLoader(null);
        r9.start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ca, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ce, code lost:
        android.util.Log.w("DynamiteLoaderV2CL", X.C72453ed.A0r("Failed to enumerate thread/threadgroup ", r0.getMessage()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00e8, code lost:
        if (X.C87724Cq.A01 != null) goto L_0x00ea;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(android.content.Context r19, java.lang.String r20, boolean r21) {
        /*
        // Method dump skipped, instructions count: 656
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95564dy.A01(android.content.Context, java.lang.String, boolean):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 3, insn: 0x00c9: IF  (r3 I:??[int, boolean, OBJECT, ARRAY, byte, short, char]) == (0 ??[int, boolean, OBJECT, ARRAY, byte, short, char])  -> B:55:0x00ce, block:B:53:0x00c9
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public static int A02(
/*
[207] Method generation error in method: X.4dy.A02(android.content.Context, java.lang.String, boolean):int, file: classes3.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r9v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public static Parcel A03(C98394ic r4, Object obj, Object obj2, String str, int i) {
        BinderC56502l7 r3 = new BinderC56502l7(obj);
        BinderC56502l7 r2 = new BinderC56502l7(obj2);
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(r4.A01);
        obtain.writeStrongBinder(r3.asBinder());
        obtain.writeString(str);
        obtain.writeInt(i);
        obtain.writeStrongBinder(r2.asBinder());
        return obtain;
    }

    public static Parcel A04(BinderC73273fx r2, String str, String str2, int i) {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(str);
        obtain.writeStrongBinder(r2.asBinder());
        obtain.writeString(str2);
        obtain.writeInt(i);
        return obtain;
    }

    public static IObjectWrapper A05(Parcel parcel, C98394ic r1, int i) {
        Parcel A00 = r1.A00(i, parcel);
        IObjectWrapper A012 = AbstractBinderC79573qo.A01(A00.readStrongBinder());
        A00.recycle();
        return A012;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0275, code lost:
        if (r11.A00 != 0) goto L_0x0277;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C95564dy A06(android.content.Context r19, X.AbstractC115575Sc r20, java.lang.String r21) {
        /*
        // Method dump skipped, instructions count: 773
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95564dy.A06(android.content.Context, X.5Sc, java.lang.String):X.4dy");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r1 != null) goto L_0x002f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C79523qj A07(android.content.Context r5) {
        /*
            java.lang.Class<X.4dy> r4 = X.C95564dy.class
            monitor-enter(r4)
            X.3qj r3 = X.C95564dy.A02     // Catch: all -> 0x004c
            if (r3 != 0) goto L_0x004a
            r3 = 0
            java.lang.String r1 = "com.google.android.gms"
            r0 = 3
            android.content.Context r0 = r5.createPackageContext(r1, r0)     // Catch: Exception -> 0x003a, all -> 0x004c
            java.lang.ClassLoader r1 = r0.getClassLoader()     // Catch: Exception -> 0x003a, all -> 0x004c
            java.lang.String r0 = "com.google.android.gms.chimera.container.DynamiteLoaderImpl"
            java.lang.Class r0 = r1.loadClass(r0)     // Catch: Exception -> 0x003a, all -> 0x004c
            java.lang.Object r2 = r0.newInstance()     // Catch: Exception -> 0x003a, all -> 0x004c
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch: Exception -> 0x003a, all -> 0x004c
            if (r2 == 0) goto L_0x004a
            java.lang.String r0 = "com.google.android.gms.dynamite.IDynamiteLoader"
            android.os.IInterface r1 = r2.queryLocalInterface(r0)     // Catch: Exception -> 0x003a, all -> 0x004c
            boolean r0 = r1 instanceof X.C79523qj     // Catch: Exception -> 0x003a, all -> 0x004c
            if (r0 == 0) goto L_0x0032
            X.3qj r1 = (X.C79523qj) r1     // Catch: Exception -> 0x003a, all -> 0x004c
            if (r1 == 0) goto L_0x004a
        L_0x002f:
            X.C95564dy.A02 = r1     // Catch: Exception -> 0x003a, all -> 0x004c
            goto L_0x0038
        L_0x0032:
            X.3qj r1 = new X.3qj     // Catch: Exception -> 0x003a, all -> 0x004c
            r1.<init>(r2)     // Catch: Exception -> 0x003a, all -> 0x004c
            goto L_0x002f
        L_0x0038:
            monitor-exit(r4)     // Catch: all -> 0x004c
            return r1
        L_0x003a:
            r0 = move-exception
            java.lang.String r2 = "DynamiteModule"
            java.lang.String r1 = "Failed to load IDynamiteLoader from GmsCore: "
            java.lang.String r0 = r0.getMessage()     // Catch: all -> 0x004c
            java.lang.String r0 = X.C72453ed.A0r(r1, r0)     // Catch: all -> 0x004c
            android.util.Log.e(r2, r0)     // Catch: all -> 0x004c
        L_0x004a:
            monitor-exit(r4)     // Catch: all -> 0x004c
            return r3
        L_0x004c:
            r0 = move-exception
            monitor-exit(r4)     // Catch: all -> 0x004c
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95564dy.A07(android.content.Context):X.3qj");
    }

    public static void A08(ClassLoader classLoader) {
        C79533qk r3 = null;
        try {
            IBinder iBinder = (IBinder) classLoader.loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor(new Class[0]).newInstance(new Object[0]);
            if (iBinder != null) {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoaderV2");
                if (queryLocalInterface instanceof C79533qk) {
                    r3 = (C79533qk) queryLocalInterface;
                } else {
                    r3 = new C79533qk(iBinder);
                }
            }
            A03 = r3;
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new AnonymousClass4CB("Failed to instantiate dynamite loader", e);
        }
    }
}
