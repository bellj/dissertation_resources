package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1Fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C27081Fy extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C27081Fy A0i;
    public static volatile AnonymousClass255 A0j;
    public int A00;
    public int A01;
    public C40841sQ A02;
    public C57552nF A03;
    public C57502nA A04;
    public C456122i A05;
    public C56942mD A06;
    public C57132mX A07;
    public C57292mn A08;
    public C57302mo A09;
    public C56952mE A0A;
    public C57312mp A0B;
    public C40831sP A0C;
    public C57712nV A0D;
    public C56962mF A0E;
    public C56962mF A0F;
    public C56962mF A0G;
    public C57602nK A0H;
    public AnonymousClass229 A0I;
    public C40821sO A0J;
    public C57752nZ A0K;
    public C57452n4 A0L;
    public C47802Cs A0M;
    public C57742nY A0N;
    public C57512nB A0O;
    public C35771ii A0P;
    public C57672nR A0Q;
    public C57682nS A0R;
    public C57212mf A0S;
    public C57522nC A0T;
    public AnonymousClass2RK A0U;
    public C57542nE A0V;
    public C57692nT A0W;
    public AnonymousClass2RJ A0X;
    public C57572nH A0Y;
    public C57412mz A0Z;
    public C57242mi A0a;
    public C57242mi A0b;
    public C57702nU A0c;
    public C57462n5 A0d;
    public C49692Lu A0e;
    public C40851sR A0f;
    public AnonymousClass2n6 A0g;
    public String A0h = "";

    static {
        C27081Fy r0 = new C27081Fy();
        A0i = r0;
        r0.A0W();
    }

    public static AnonymousClass1G3 A00() {
        return (AnonymousClass1G3) A0i.A0T();
    }

    public static C27081Fy A01(byte[] bArr) {
        return (C27081Fy) AbstractC27091Fz.A0E(A0i, bArr);
    }

    public boolean A0b() {
        return (this.A00 & 64) == 64;
    }

    public boolean A0c() {
        return (this.A00 & Integer.MIN_VALUE) == Integer.MIN_VALUE;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r13, Object obj, Object obj2) {
        C56862m4 r1;
        int i;
        C82053v0 r12;
        C56872m5 r14;
        C82193vE r2;
        C56862m4 r15;
        int i2;
        C82153vA r16;
        C82183vD r17;
        C56862m4 r18;
        C82083v3 r19;
        C81863uh r110;
        C82253vK r111;
        C82353vU r112;
        C81893uk r113;
        C82343vT r114;
        AnonymousClass2Lw r115;
        C81813uc r116;
        C81853ug r117;
        C82323vR r118;
        C35761ih r119;
        C82333vS r120;
        C56892m7 r121;
        C467226y r122;
        C81843uf r123;
        C56882m6 r124;
        C81823ud r125;
        C455822f r126;
        C82373vW r127;
        int i3;
        C81763uX r128;
        C81873ui r129;
        C81883uj r130;
        C82173vC r131;
        C81833ue r132;
        C81963ur r22;
        C56892m7 r133;
        switch (r13.ordinal()) {
            case 0:
                return A0i;
            case 1:
                AbstractC462925h r142 = (AbstractC462925h) obj;
                C27081Fy r152 = (C27081Fy) obj2;
                boolean z = true;
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
                String str = this.A0h;
                boolean z2 = true;
                if ((r152.A00 & 1) != 1) {
                    z2 = false;
                }
                this.A0h = r142.Afy(str, r152.A0h, z, z2);
                this.A0b = (C57242mi) r142.Aft(this.A0b, r152.A0b);
                this.A0J = (C40821sO) r142.Aft(this.A0J, r152.A0J);
                this.A08 = (C57292mn) r142.Aft(this.A08, r152.A08);
                this.A0Q = (C57672nR) r142.Aft(this.A0Q, r152.A0Q);
                this.A0D = (C57712nV) r142.Aft(this.A0D, r152.A0D);
                this.A0C = (C40831sP) r142.Aft(this.A0C, r152.A0C);
                this.A02 = (C40841sQ) r142.Aft(this.A02, r152.A02);
                this.A0f = (C40851sR) r142.Aft(this.A0f, r152.A0f);
                this.A05 = (C456122i) r142.Aft(this.A05, r152.A05);
                this.A07 = (C57132mX) r142.Aft(this.A07, r152.A07);
                this.A0W = (C57692nT) r142.Aft(this.A0W, r152.A0W);
                this.A09 = (C57302mo) r142.Aft(this.A09, r152.A09);
                this.A0I = (AnonymousClass229) r142.Aft(this.A0I, r152.A0I);
                this.A0a = (C57242mi) r142.Aft(this.A0a, r152.A0a);
                this.A0Z = (C57412mz) r142.Aft(this.A0Z, r152.A0Z);
                this.A0P = (C35771ii) r142.Aft(this.A0P, r152.A0P);
                this.A0Y = (C57572nH) r142.Aft(this.A0Y, r152.A0Y);
                this.A0A = (C56952mE) r142.Aft(this.A0A, r152.A0A);
                this.A06 = (C56942mD) r142.Aft(this.A06, r152.A06);
                this.A0e = (C49692Lu) r142.Aft(this.A0e, r152.A0e);
                this.A0c = (C57702nU) r142.Aft(this.A0c, r152.A0c);
                this.A0H = (C57602nK) r142.Aft(this.A0H, r152.A0H);
                this.A0d = (C57462n5) r142.Aft(this.A0d, r152.A0d);
                this.A0V = (C57542nE) r142.Aft(this.A0V, r152.A0V);
                this.A0B = (C57312mp) r142.Aft(this.A0B, r152.A0B);
                this.A0g = (AnonymousClass2n6) r142.Aft(this.A0g, r152.A0g);
                this.A0N = (C57742nY) r142.Aft(this.A0N, r152.A0N);
                this.A0G = (C56962mF) r142.Aft(this.A0G, r152.A0G);
                this.A0R = (C57682nS) r142.Aft(this.A0R, r152.A0R);
                this.A0O = (C57512nB) r142.Aft(this.A0O, r152.A0O);
                this.A0F = (C56962mF) r142.Aft(this.A0F, r152.A0F);
                this.A03 = (C57552nF) r142.Aft(this.A03, r152.A03);
                this.A04 = (C57502nA) r142.Aft(this.A04, r152.A04);
                this.A0S = (C57212mf) r142.Aft(this.A0S, r152.A0S);
                this.A0K = (C57752nZ) r142.Aft(this.A0K, r152.A0K);
                this.A0X = (AnonymousClass2RJ) r142.Aft(this.A0X, r152.A0X);
                this.A0L = (C57452n4) r142.Aft(this.A0L, r152.A0L);
                this.A0T = (C57522nC) r142.Aft(this.A0T, r152.A0T);
                this.A0U = (AnonymousClass2RK) r142.Aft(this.A0U, r152.A0U);
                this.A0M = (C47802Cs) r142.Aft(this.A0M, r152.A0M);
                this.A0E = (C56962mF) r142.Aft(this.A0E, r152.A0E);
                if (r142 == C463025i.A00) {
                    this.A00 |= r152.A00;
                    this.A01 |= r152.A01;
                }
                return this;
            case 2:
                AnonymousClass253 r143 = (AnonymousClass253) obj;
                AnonymousClass254 r153 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r143.A03();
                        int i4 = 512;
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                String A0A = r143.A0A();
                                this.A00 |= 1;
                                this.A0h = A0A;
                                break;
                            case 18:
                                if ((this.A00 & 2) == 2) {
                                    r133 = (C56892m7) this.A0b.A0T();
                                } else {
                                    r133 = null;
                                }
                                C57242mi r0 = (C57242mi) r143.A09(r153, C57242mi.A03.A0U());
                                this.A0b = r0;
                                if (r133 != null) {
                                    r133.A04(r0);
                                    this.A0b = (C57242mi) r133.A01();
                                }
                                i3 = this.A00 | 2;
                                this.A00 = i3;
                                break;
                            case 26:
                                if ((this.A00 & 4) == 4) {
                                    r22 = (C81963ur) this.A0J.A0T();
                                } else {
                                    r22 = null;
                                }
                                C40821sO r02 = (C40821sO) r143.A09(r153, C40821sO.A0R.A0U());
                                this.A0J = r02;
                                if (r22 != null) {
                                    r22.A04(r02);
                                    this.A0J = (C40821sO) r22.A01();
                                }
                                i3 = this.A00 | 4;
                                this.A00 = i3;
                                break;
                            case 34:
                                if ((this.A00 & 8) == 8) {
                                    r132 = (C81833ue) this.A08.A0T();
                                } else {
                                    r132 = null;
                                }
                                C57292mn r03 = (C57292mn) r143.A09(r153, C57292mn.A04.A0U());
                                this.A08 = r03;
                                if (r132 != null) {
                                    r132.A04(r03);
                                    this.A08 = (C57292mn) r132.A01();
                                }
                                i3 = this.A00 | 8;
                                this.A00 = i3;
                                break;
                            case 42:
                                if ((this.A00 & 16) == 16) {
                                    r131 = (C82173vC) this.A0Q.A0T();
                                } else {
                                    r131 = null;
                                }
                                C57672nR r04 = (C57672nR) r143.A09(r153, C57672nR.A0D.A0U());
                                this.A0Q = r04;
                                if (r131 != null) {
                                    r131.A04(r04);
                                    this.A0Q = (C57672nR) r131.A01();
                                }
                                i3 = this.A00 | 16;
                                this.A00 = i3;
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                if ((this.A00 & 32) == 32) {
                                    r130 = (C81883uj) this.A0D.A0T();
                                } else {
                                    r130 = null;
                                }
                                C57712nV r05 = (C57712nV) r143.A09(r153, C57712nV.A0O.A0U());
                                this.A0D = r05;
                                if (r130 != null) {
                                    r130.A04(r05);
                                    this.A0D = (C57712nV) r130.A01();
                                }
                                i3 = this.A00 | 32;
                                this.A00 = i3;
                                break;
                            case 58:
                                if ((this.A00 & 64) == 64) {
                                    r129 = (C81873ui) this.A0C.A0T();
                                } else {
                                    r129 = null;
                                }
                                C40831sP r06 = (C40831sP) r143.A09(r153, C40831sP.A0L.A0U());
                                this.A0C = r06;
                                if (r129 != null) {
                                    r129.A04(r06);
                                    this.A0C = (C40831sP) r129.A01();
                                }
                                i3 = this.A00 | 64;
                                this.A00 = i3;
                                break;
                            case 66:
                                if ((this.A00 & 128) == 128) {
                                    r128 = (C81763uX) this.A02.A0T();
                                } else {
                                    r128 = null;
                                }
                                C40841sQ r07 = (C40841sQ) r143.A09(r153, C40841sQ.A0E.A0U());
                                this.A02 = r07;
                                if (r128 != null) {
                                    r128.A04(r07);
                                    this.A02 = (C40841sQ) r128.A01();
                                }
                                i3 = this.A00 | 128;
                                this.A00 = i3;
                                break;
                            case 74:
                                if ((this.A00 & 256) == 256) {
                                    r127 = (C82373vW) this.A0f.A0T();
                                } else {
                                    r127 = null;
                                }
                                C40851sR r08 = (C40851sR) r143.A09(r153, C40851sR.A0O.A0U());
                                this.A0f = r08;
                                if (r127 != null) {
                                    r127.A04(r08);
                                    this.A0f = (C40851sR) r127.A01();
                                }
                                i3 = this.A00 | 256;
                                this.A00 = i3;
                                break;
                            case 82:
                                if ((this.A00 & 512) == 512) {
                                    r126 = (C455822f) this.A05.A0T();
                                } else {
                                    r126 = null;
                                }
                                C456122i r09 = (C456122i) r143.A09(r153, C456122i.A05.A0U());
                                this.A05 = r09;
                                if (r126 != null) {
                                    r126.A04(r09);
                                    this.A05 = (C456122i) r126.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 90:
                                int i5 = this.A00;
                                i4 = EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                                    r125 = (C81823ud) this.A07.A0T();
                                } else {
                                    r125 = null;
                                }
                                C57132mX r010 = (C57132mX) r143.A09(r153, C57132mX.A03.A0U());
                                this.A07 = r010;
                                if (r125 != null) {
                                    r125.A04(r010);
                                    this.A07 = (C57132mX) r125.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 98:
                                int i6 = this.A00;
                                i4 = EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                if ((i6 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                                    r124 = (C56882m6) this.A0W.A0T();
                                } else {
                                    r124 = null;
                                }
                                C57692nT r011 = (C57692nT) r143.A09(r153, C57692nT.A0D.A0U());
                                this.A0W = r011;
                                if (r124 != null) {
                                    r124.A04(r011);
                                    this.A0W = (C57692nT) r124.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 106:
                                i4 = 4096;
                                if ((this.A00 & 4096) == 4096) {
                                    r123 = (C81843uf) this.A09.A0T();
                                } else {
                                    r123 = null;
                                }
                                C57302mo r012 = (C57302mo) r143.A09(r153, C57302mo.A04.A0U());
                                this.A09 = r012;
                                if (r123 != null) {
                                    r123.A04(r012);
                                    this.A09 = (C57302mo) r123.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 114:
                                int i7 = this.A00;
                                i4 = DefaultCrypto.BUFFER_SIZE;
                                if ((i7 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                                    r122 = (C467226y) this.A0I.A0T();
                                } else {
                                    r122 = null;
                                }
                                AnonymousClass229 r013 = (AnonymousClass229) r143.A09(r153, AnonymousClass229.A0A.A0U());
                                this.A0I = r013;
                                if (r122 != null) {
                                    r122.A04(r013);
                                    this.A0I = (AnonymousClass229) r122.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 122:
                                i4 = 16384;
                                if ((this.A00 & 16384) == 16384) {
                                    r121 = (C56892m7) this.A0a.A0T();
                                } else {
                                    r121 = null;
                                }
                                C57242mi r014 = (C57242mi) r143.A09(r153, C57242mi.A03.A0U());
                                this.A0a = r014;
                                if (r121 != null) {
                                    r121.A04(r014);
                                    this.A0a = (C57242mi) r121.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 130:
                                i4 = 32768;
                                if ((this.A00 & 32768) == 32768) {
                                    r120 = (C82333vS) this.A0Z.A0T();
                                } else {
                                    r120 = null;
                                }
                                C57412mz r015 = (C57412mz) r143.A09(r153, C57412mz.A04.A0U());
                                this.A0Z = r015;
                                if (r120 != null) {
                                    r120.A04(r015);
                                    this.A0Z = (C57412mz) r120.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 146:
                                i4 = 65536;
                                if ((this.A00 & 65536) == 65536) {
                                    r119 = (C35761ih) this.A0P.A0T();
                                } else {
                                    r119 = null;
                                }
                                C35771ii r016 = (C35771ii) r143.A09(r153, C35771ii.A0B.A0U());
                                this.A0P = r016;
                                if (r119 != null) {
                                    r119.A04(r016);
                                    this.A0P = (C35771ii) r119.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 178:
                                int i8 = this.A00;
                                i4 = C25981Bo.A0F;
                                if ((i8 & C25981Bo.A0F) == 131072) {
                                    r118 = (C82323vR) this.A0Y.A0T();
                                } else {
                                    r118 = null;
                                }
                                C57572nH r017 = (C57572nH) r143.A09(r153, C57572nH.A08.A0U());
                                this.A0Y = r017;
                                if (r118 != null) {
                                    r118.A04(r017);
                                    this.A0Y = (C57572nH) r118.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 186:
                                i4 = 262144;
                                if ((this.A00 & 262144) == 262144) {
                                    r117 = (C81853ug) this.A0A.A0T();
                                } else {
                                    r117 = null;
                                }
                                C56952mE r018 = (C56952mE) r143.A09(r153, C56952mE.A02.A0U());
                                this.A0A = r018;
                                if (r117 != null) {
                                    r117.A04(r018);
                                    this.A0A = (C56952mE) r117.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 194:
                                i4 = 524288;
                                if ((this.A00 & 524288) == 524288) {
                                    r116 = (C81813uc) this.A06.A0T();
                                } else {
                                    r116 = null;
                                }
                                C56942mD r019 = (C56942mD) r143.A09(r153, C56942mD.A02.A0U());
                                this.A06 = r019;
                                if (r116 != null) {
                                    r116.A04(r019);
                                    this.A06 = (C56942mD) r116.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 202:
                                i4 = 1048576;
                                if ((this.A00 & 1048576) == 1048576) {
                                    r115 = (AnonymousClass2Lw) this.A0e.A0T();
                                } else {
                                    r115 = null;
                                }
                                C49692Lu r020 = (C49692Lu) r143.A09(r153, C49692Lu.A05.A0U());
                                this.A0e = r020;
                                if (r115 != null) {
                                    r115.A04(r020);
                                    this.A0e = (C49692Lu) r115.A01();
                                }
                                i2 = this.A00;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 210:
                                if ((this.A00 & 2097152) == 2097152) {
                                    r114 = (C82343vT) this.A0c.A0T();
                                } else {
                                    r114 = null;
                                }
                                C57702nU r021 = (C57702nU) r143.A09(r153, C57702nU.A0G.A0U());
                                this.A0c = r021;
                                if (r114 != null) {
                                    r114.A04(r021);
                                    this.A0c = (C57702nU) r114.A01();
                                }
                                i2 = this.A00;
                                i4 = 2097152;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 226:
                                if ((this.A00 & 4194304) == 4194304) {
                                    r113 = (C81893uk) this.A0H.A0T();
                                } else {
                                    r113 = null;
                                }
                                C57602nK r022 = (C57602nK) r143.A09(r153, C57602nK.A09.A0U());
                                this.A0H = r022;
                                if (r113 != null) {
                                    r113.A04(r022);
                                    this.A0H = (C57602nK) r113.A01();
                                }
                                i2 = this.A00;
                                i4 = 4194304;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 234:
                                if ((this.A00 & 8388608) == 8388608) {
                                    r112 = (C82353vU) this.A0d.A0T();
                                } else {
                                    r112 = null;
                                }
                                C57462n5 r023 = (C57462n5) r143.A09(r153, C57462n5.A05.A0U());
                                this.A0d = r023;
                                if (r112 != null) {
                                    r112.A04(r023);
                                    this.A0d = (C57462n5) r112.A01();
                                }
                                i2 = this.A00;
                                i4 = 8388608;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 242:
                                if ((this.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                                    r111 = (C82253vK) this.A0V.A0T();
                                } else {
                                    r111 = null;
                                }
                                C57542nE r024 = (C57542nE) r143.A09(r153, C57542nE.A07.A0U());
                                this.A0V = r024;
                                if (r111 != null) {
                                    r111.A04(r024);
                                    this.A0V = (C57542nE) r111.A01();
                                }
                                i2 = this.A00;
                                i4 = EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 250:
                                if ((this.A00 & 33554432) == 33554432) {
                                    r110 = (C81863uh) this.A0B.A0T();
                                } else {
                                    r110 = null;
                                }
                                C57312mp r025 = (C57312mp) r143.A09(r153, C57312mp.A04.A0U());
                                this.A0B = r025;
                                if (r110 != null) {
                                    r110.A04(r025);
                                    this.A0B = (C57312mp) r110.A01();
                                }
                                i2 = this.A00;
                                i4 = 33554432;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 282:
                                C82383vX r134 = (this.A00 & 67108864) == 67108864 ? (C82383vX) this.A0g.A0T() : null;
                                AnonymousClass2n6 r026 = (AnonymousClass2n6) r143.A09(r153, AnonymousClass2n6.A05.A0U());
                                this.A0g = r026;
                                if (r134 != null) {
                                    r134.A04(r026);
                                    this.A0g = (AnonymousClass2n6) r134.A01();
                                }
                                i2 = this.A00;
                                i4 = 67108864;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 290:
                                if ((this.A00 & 134217728) == 134217728) {
                                    r19 = (C82083v3) this.A0N.A0T();
                                } else {
                                    r19 = null;
                                }
                                C57742nY r027 = (C57742nY) r143.A09(r153, C57742nY.A09.A0U());
                                this.A0N = r027;
                                if (r19 != null) {
                                    r19.A04(r027);
                                    this.A0N = (C57742nY) r19.A01();
                                }
                                i2 = this.A00;
                                i4 = 134217728;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 298:
                                if ((this.A00 & 268435456) == 268435456) {
                                    r18 = (C56862m4) this.A0G.A0T();
                                } else {
                                    r18 = null;
                                }
                                C56962mF r028 = (C56962mF) r143.A09(r153, C56962mF.A02.A0U());
                                this.A0G = r028;
                                if (r18 != null) {
                                    r18.A04(r028);
                                    this.A0G = (C56962mF) r18.A01();
                                }
                                i2 = this.A00;
                                i4 = 268435456;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 306:
                                if ((this.A00 & 536870912) == 536870912) {
                                    r17 = (C82183vD) this.A0R.A0T();
                                } else {
                                    r17 = null;
                                }
                                C57682nS r029 = (C57682nS) r143.A09(r153, C57682nS.A0D.A0U());
                                this.A0R = r029;
                                if (r17 != null) {
                                    r17.A04(r029);
                                    this.A0R = (C57682nS) r17.A01();
                                }
                                i2 = this.A00;
                                i4 = 536870912;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 314:
                                if ((this.A00 & 1073741824) == 1073741824) {
                                    r16 = (C82153vA) this.A0O.A0T();
                                } else {
                                    r16 = null;
                                }
                                C57512nB r030 = (C57512nB) r143.A09(r153, C57512nB.A06.A0U());
                                this.A0O = r030;
                                if (r16 != null) {
                                    r16.A04(r030);
                                    this.A0O = (C57512nB) r16.A01();
                                }
                                i2 = this.A00;
                                i4 = 1073741824;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 322:
                                if ((this.A00 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                                    r15 = (C56862m4) this.A0F.A0T();
                                } else {
                                    r15 = null;
                                }
                                C56962mF r031 = (C56962mF) r143.A09(r153, C56962mF.A02.A0U());
                                this.A0F = r031;
                                if (r15 != null) {
                                    r15.A04(r031);
                                    this.A0F = (C56962mF) r15.A01();
                                }
                                i2 = this.A00;
                                i4 = Integer.MIN_VALUE;
                                i3 = i2 | i4;
                                this.A00 = i3;
                                break;
                            case 338:
                                C56852m3 r135 = (this.A01 & 1) == 1 ? (C56852m3) this.A03.A0T() : null;
                                C57552nF r032 = (C57552nF) r143.A09(r153, C57552nF.A08.A0U());
                                this.A03 = r032;
                                if (r135 != null) {
                                    r135.A04(r032);
                                    this.A03 = (C57552nF) r135.A01();
                                }
                                i = this.A01 | 1;
                                this.A01 = i;
                                break;
                            case 346:
                                C81803ub r136 = (this.A01 & 2) == 2 ? (C81803ub) this.A04.A0T() : null;
                                C57502nA r033 = (C57502nA) r143.A09(r153, C57502nA.A06.A0U());
                                this.A04 = r033;
                                if (r136 != null) {
                                    r136.A04(r033);
                                    this.A04 = (C57502nA) r136.A01();
                                }
                                i = this.A01 | 2;
                                this.A01 = i;
                                break;
                            case 354:
                                if ((this.A01 & 4) == 4) {
                                    r2 = (C82193vE) this.A0S.A0T();
                                } else {
                                    r2 = null;
                                }
                                C57212mf r034 = (C57212mf) r143.A09(r153, C57212mf.A03.A0U());
                                this.A0S = r034;
                                if (r2 != null) {
                                    r2.A04(r034);
                                    this.A0S = (C57212mf) r2.A01();
                                }
                                i = this.A01 | 4;
                                this.A01 = i;
                                break;
                            case 362:
                                if ((this.A01 & 8) == 8) {
                                    r14 = (C56872m5) this.A0K.A0T();
                                } else {
                                    r14 = null;
                                }
                                C57752nZ r035 = (C57752nZ) r143.A09(r153, C57752nZ.A07.A0U());
                                this.A0K = r035;
                                if (r14 != null) {
                                    r14.A04(r035);
                                    this.A0K = (C57752nZ) r14.A01();
                                }
                                i = this.A01 | 8;
                                this.A01 = i;
                                break;
                            case 370:
                                C82283vN r137 = (this.A01 & 16) == 16 ? (C82283vN) this.A0X.A0T() : null;
                                AnonymousClass2RJ r036 = (AnonymousClass2RJ) r143.A09(r153, AnonymousClass2RJ.A05.A0U());
                                this.A0X = r036;
                                if (r137 != null) {
                                    r137.A04(r036);
                                    this.A0X = (AnonymousClass2RJ) r137.A01();
                                }
                                i = this.A01 | 16;
                                this.A01 = i;
                                break;
                            case 386:
                                if ((this.A01 & 32) == 32) {
                                    r12 = (C82053v0) this.A0L.A0T();
                                } else {
                                    r12 = null;
                                }
                                C57452n4 r037 = (C57452n4) r143.A09(r153, C57452n4.A05.A0U());
                                this.A0L = r037;
                                if (r12 != null) {
                                    r12.A04(r037);
                                    this.A0L = (C57452n4) r12.A01();
                                }
                                i = this.A01 | 32;
                                this.A01 = i;
                                break;
                            case 394:
                                C82203vF r138 = (this.A01 & 64) == 64 ? (C82203vF) this.A0T.A0T() : null;
                                C57522nC r038 = (C57522nC) r143.A09(r153, C57522nC.A06.A0U());
                                this.A0T = r038;
                                if (r138 != null) {
                                    r138.A04(r038);
                                    this.A0T = (C57522nC) r138.A01();
                                }
                                i = this.A01 | 64;
                                this.A01 = i;
                                break;
                            case 402:
                                C82233vI r139 = (this.A01 & 128) == 128 ? (C82233vI) this.A0U.A0T() : null;
                                AnonymousClass2RK r039 = (AnonymousClass2RK) r143.A09(r153, AnonymousClass2RK.A05.A0U());
                                this.A0U = r039;
                                if (r139 != null) {
                                    r139.A04(r039);
                                    this.A0U = (AnonymousClass2RK) r139.A01();
                                }
                                i = this.A01 | 128;
                                this.A01 = i;
                                break;
                            case 410:
                                C82073v2 r140 = (this.A01 & 256) == 256 ? (C82073v2) this.A0M.A0T() : null;
                                C47802Cs r040 = (C47802Cs) r143.A09(r153, C47802Cs.A04.A0U());
                                this.A0M = r040;
                                if (r140 != null) {
                                    r140.A04(r040);
                                    this.A0M = (C47802Cs) r140.A01();
                                }
                                i = this.A01 | 256;
                                this.A01 = i;
                                break;
                            case 426:
                                if ((this.A01 & 512) == 512) {
                                    r1 = (C56862m4) this.A0E.A0T();
                                } else {
                                    r1 = null;
                                }
                                C56962mF r041 = (C56962mF) r143.A09(r153, C56962mF.A02.A0U());
                                this.A0E = r041;
                                if (r1 != null) {
                                    r1.A04(r041);
                                    this.A0E = (C56962mF) r1.A01();
                                }
                                i = this.A01 | 512;
                                this.A01 = i;
                                break;
                            default:
                                if (A0a(r143, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r141 = new C28971Pt(e2.getMessage());
                        r141.unfinishedMessage = this;
                        throw new RuntimeException(r141);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C27081Fy();
            case 5:
                return new AnonymousClass1G3();
            case 6:
                break;
            case 7:
                if (A0j == null) {
                    synchronized (C27081Fy.class) {
                        if (A0j == null) {
                            A0j = new AnonymousClass255(A0i);
                        }
                    }
                }
                return A0j;
            default:
                throw new UnsupportedOperationException();
        }
        return A0i;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A0h);
        }
        if ((this.A00 & 2) == 2) {
            C57242mi r0 = this.A0b;
            if (r0 == null) {
                r0 = C57242mi.A03;
            }
            i2 += CodedOutputStream.A0A(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C40821sO r02 = this.A0J;
            if (r02 == null) {
                r02 = C40821sO.A0R;
            }
            i2 += CodedOutputStream.A0A(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C57292mn r03 = this.A08;
            if (r03 == null) {
                r03 = C57292mn.A04;
            }
            i2 += CodedOutputStream.A0A(r03, 4);
        }
        if ((this.A00 & 16) == 16) {
            C57672nR r04 = this.A0Q;
            if (r04 == null) {
                r04 = C57672nR.A0D;
            }
            i2 += CodedOutputStream.A0A(r04, 5);
        }
        if ((this.A00 & 32) == 32) {
            C57712nV r05 = this.A0D;
            if (r05 == null) {
                r05 = C57712nV.A0O;
            }
            i2 += CodedOutputStream.A0A(r05, 6);
        }
        if ((this.A00 & 64) == 64) {
            C40831sP r06 = this.A0C;
            if (r06 == null) {
                r06 = C40831sP.A0L;
            }
            i2 += CodedOutputStream.A0A(r06, 7);
        }
        if ((this.A00 & 128) == 128) {
            C40841sQ r07 = this.A02;
            if (r07 == null) {
                r07 = C40841sQ.A0E;
            }
            i2 += CodedOutputStream.A0A(r07, 8);
        }
        if ((this.A00 & 256) == 256) {
            C40851sR r08 = this.A0f;
            if (r08 == null) {
                r08 = C40851sR.A0O;
            }
            i2 += CodedOutputStream.A0A(r08, 9);
        }
        if ((this.A00 & 512) == 512) {
            C456122i r09 = this.A05;
            if (r09 == null) {
                r09 = C456122i.A05;
            }
            i2 += CodedOutputStream.A0A(r09, 10);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C57132mX r010 = this.A07;
            if (r010 == null) {
                r010 = C57132mX.A03;
            }
            i2 += CodedOutputStream.A0A(r010, 11);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C57692nT r011 = this.A0W;
            if (r011 == null) {
                r011 = C57692nT.A0D;
            }
            i2 += CodedOutputStream.A0A(r011, 12);
        }
        if ((this.A00 & 4096) == 4096) {
            C57302mo r012 = this.A09;
            if (r012 == null) {
                r012 = C57302mo.A04;
            }
            i2 += CodedOutputStream.A0A(r012, 13);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            AnonymousClass229 r013 = this.A0I;
            if (r013 == null) {
                r013 = AnonymousClass229.A0A;
            }
            i2 += CodedOutputStream.A0A(r013, 14);
        }
        if ((this.A00 & 16384) == 16384) {
            C57242mi r014 = this.A0a;
            if (r014 == null) {
                r014 = C57242mi.A03;
            }
            i2 += CodedOutputStream.A0A(r014, 15);
        }
        if ((this.A00 & 32768) == 32768) {
            C57412mz r015 = this.A0Z;
            if (r015 == null) {
                r015 = C57412mz.A04;
            }
            i2 += CodedOutputStream.A0A(r015, 16);
        }
        if ((this.A00 & 65536) == 65536) {
            C35771ii r016 = this.A0P;
            if (r016 == null) {
                r016 = C35771ii.A0B;
            }
            i2 += CodedOutputStream.A0A(r016, 18);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            C57572nH r017 = this.A0Y;
            if (r017 == null) {
                r017 = C57572nH.A08;
            }
            i2 += CodedOutputStream.A0A(r017, 22);
        }
        if ((this.A00 & 262144) == 262144) {
            C56952mE r018 = this.A0A;
            if (r018 == null) {
                r018 = C56952mE.A02;
            }
            i2 += CodedOutputStream.A0A(r018, 23);
        }
        if ((this.A00 & 524288) == 524288) {
            C56942mD r019 = this.A06;
            if (r019 == null) {
                r019 = C56942mD.A02;
            }
            i2 += CodedOutputStream.A0A(r019, 24);
        }
        if ((this.A00 & 1048576) == 1048576) {
            C49692Lu r020 = this.A0e;
            if (r020 == null) {
                r020 = C49692Lu.A05;
            }
            i2 += CodedOutputStream.A0A(r020, 25);
        }
        if ((this.A00 & 2097152) == 2097152) {
            C57702nU r021 = this.A0c;
            if (r021 == null) {
                r021 = C57702nU.A0G;
            }
            i2 += CodedOutputStream.A0A(r021, 26);
        }
        if ((this.A00 & 4194304) == 4194304) {
            C57602nK r022 = this.A0H;
            if (r022 == null) {
                r022 = C57602nK.A09;
            }
            i2 += CodedOutputStream.A0A(r022, 28);
        }
        if ((this.A00 & 8388608) == 8388608) {
            C57462n5 r023 = this.A0d;
            if (r023 == null) {
                r023 = C57462n5.A05;
            }
            i2 += CodedOutputStream.A0A(r023, 29);
        }
        if ((this.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            C57542nE r024 = this.A0V;
            if (r024 == null) {
                r024 = C57542nE.A07;
            }
            i2 += CodedOutputStream.A0A(r024, 30);
        }
        if ((this.A00 & 33554432) == 33554432) {
            C57312mp r025 = this.A0B;
            if (r025 == null) {
                r025 = C57312mp.A04;
            }
            i2 += CodedOutputStream.A0A(r025, 31);
        }
        if ((this.A00 & 67108864) == 67108864) {
            AnonymousClass2n6 r026 = this.A0g;
            if (r026 == null) {
                r026 = AnonymousClass2n6.A05;
            }
            i2 += CodedOutputStream.A0A(r026, 35);
        }
        if ((this.A00 & 134217728) == 134217728) {
            C57742nY r027 = this.A0N;
            if (r027 == null) {
                r027 = C57742nY.A09;
            }
            i2 += CodedOutputStream.A0A(r027, 36);
        }
        if ((this.A00 & 268435456) == 268435456) {
            C56962mF r028 = this.A0G;
            if (r028 == null) {
                r028 = C56962mF.A02;
            }
            i2 += CodedOutputStream.A0A(r028, 37);
        }
        if ((this.A00 & 536870912) == 536870912) {
            C57682nS r029 = this.A0R;
            if (r029 == null) {
                r029 = C57682nS.A0D;
            }
            i2 += CodedOutputStream.A0A(r029, 38);
        }
        if ((this.A00 & 1073741824) == 1073741824) {
            C57512nB r030 = this.A0O;
            if (r030 == null) {
                r030 = C57512nB.A06;
            }
            i2 += CodedOutputStream.A0A(r030, 39);
        }
        if ((this.A00 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            C56962mF r031 = this.A0F;
            if (r031 == null) {
                r031 = C56962mF.A02;
            }
            i2 += CodedOutputStream.A0A(r031, 40);
        }
        if ((this.A01 & 1) == 1) {
            C57552nF r032 = this.A03;
            if (r032 == null) {
                r032 = C57552nF.A08;
            }
            i2 += CodedOutputStream.A0A(r032, 42);
        }
        if ((this.A01 & 2) == 2) {
            C57502nA r033 = this.A04;
            if (r033 == null) {
                r033 = C57502nA.A06;
            }
            i2 += CodedOutputStream.A0A(r033, 43);
        }
        if ((this.A01 & 4) == 4) {
            C57212mf r034 = this.A0S;
            if (r034 == null) {
                r034 = C57212mf.A03;
            }
            i2 += CodedOutputStream.A0A(r034, 44);
        }
        if ((this.A01 & 8) == 8) {
            C57752nZ r035 = this.A0K;
            if (r035 == null) {
                r035 = C57752nZ.A07;
            }
            i2 += CodedOutputStream.A0A(r035, 45);
        }
        if ((this.A01 & 16) == 16) {
            AnonymousClass2RJ r036 = this.A0X;
            if (r036 == null) {
                r036 = AnonymousClass2RJ.A05;
            }
            i2 += CodedOutputStream.A0A(r036, 46);
        }
        if ((this.A01 & 32) == 32) {
            C57452n4 r037 = this.A0L;
            if (r037 == null) {
                r037 = C57452n4.A05;
            }
            i2 += CodedOutputStream.A0A(r037, 48);
        }
        if ((this.A01 & 64) == 64) {
            C57522nC r038 = this.A0T;
            if (r038 == null) {
                r038 = C57522nC.A06;
            }
            i2 += CodedOutputStream.A0A(r038, 49);
        }
        if ((this.A01 & 128) == 128) {
            AnonymousClass2RK r039 = this.A0U;
            if (r039 == null) {
                r039 = AnonymousClass2RK.A05;
            }
            i2 += CodedOutputStream.A0A(r039, 50);
        }
        if ((this.A01 & 256) == 256) {
            C47802Cs r040 = this.A0M;
            if (r040 == null) {
                r040 = C47802Cs.A04;
            }
            i2 += CodedOutputStream.A0A(r040, 51);
        }
        if ((this.A01 & 512) == 512) {
            C56962mF r041 = this.A0E;
            if (r041 == null) {
                r041 = C56962mF.A02;
            }
            i2 += CodedOutputStream.A0A(r041, 53);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0h);
        }
        if ((this.A00 & 2) == 2) {
            C57242mi r0 = this.A0b;
            if (r0 == null) {
                r0 = C57242mi.A03;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 4) == 4) {
            C40821sO r02 = this.A0J;
            if (r02 == null) {
                r02 = C40821sO.A0R;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 8) == 8) {
            C57292mn r03 = this.A08;
            if (r03 == null) {
                r03 = C57292mn.A04;
            }
            codedOutputStream.A0L(r03, 4);
        }
        if ((this.A00 & 16) == 16) {
            C57672nR r04 = this.A0Q;
            if (r04 == null) {
                r04 = C57672nR.A0D;
            }
            codedOutputStream.A0L(r04, 5);
        }
        if ((this.A00 & 32) == 32) {
            C57712nV r05 = this.A0D;
            if (r05 == null) {
                r05 = C57712nV.A0O;
            }
            codedOutputStream.A0L(r05, 6);
        }
        if ((this.A00 & 64) == 64) {
            C40831sP r06 = this.A0C;
            if (r06 == null) {
                r06 = C40831sP.A0L;
            }
            codedOutputStream.A0L(r06, 7);
        }
        if ((this.A00 & 128) == 128) {
            C40841sQ r07 = this.A02;
            if (r07 == null) {
                r07 = C40841sQ.A0E;
            }
            codedOutputStream.A0L(r07, 8);
        }
        if ((this.A00 & 256) == 256) {
            C40851sR r08 = this.A0f;
            if (r08 == null) {
                r08 = C40851sR.A0O;
            }
            codedOutputStream.A0L(r08, 9);
        }
        if ((this.A00 & 512) == 512) {
            C456122i r09 = this.A05;
            if (r09 == null) {
                r09 = C456122i.A05;
            }
            codedOutputStream.A0L(r09, 10);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C57132mX r010 = this.A07;
            if (r010 == null) {
                r010 = C57132mX.A03;
            }
            codedOutputStream.A0L(r010, 11);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            C57692nT r011 = this.A0W;
            if (r011 == null) {
                r011 = C57692nT.A0D;
            }
            codedOutputStream.A0L(r011, 12);
        }
        if ((this.A00 & 4096) == 4096) {
            C57302mo r012 = this.A09;
            if (r012 == null) {
                r012 = C57302mo.A04;
            }
            codedOutputStream.A0L(r012, 13);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            AnonymousClass229 r013 = this.A0I;
            if (r013 == null) {
                r013 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r013, 14);
        }
        if ((this.A00 & 16384) == 16384) {
            C57242mi r014 = this.A0a;
            if (r014 == null) {
                r014 = C57242mi.A03;
            }
            codedOutputStream.A0L(r014, 15);
        }
        if ((this.A00 & 32768) == 32768) {
            C57412mz r015 = this.A0Z;
            if (r015 == null) {
                r015 = C57412mz.A04;
            }
            codedOutputStream.A0L(r015, 16);
        }
        if ((this.A00 & 65536) == 65536) {
            C35771ii r016 = this.A0P;
            if (r016 == null) {
                r016 = C35771ii.A0B;
            }
            codedOutputStream.A0L(r016, 18);
        }
        if ((this.A00 & C25981Bo.A0F) == 131072) {
            C57572nH r017 = this.A0Y;
            if (r017 == null) {
                r017 = C57572nH.A08;
            }
            codedOutputStream.A0L(r017, 22);
        }
        if ((this.A00 & 262144) == 262144) {
            C56952mE r018 = this.A0A;
            if (r018 == null) {
                r018 = C56952mE.A02;
            }
            codedOutputStream.A0L(r018, 23);
        }
        if ((this.A00 & 524288) == 524288) {
            C56942mD r019 = this.A06;
            if (r019 == null) {
                r019 = C56942mD.A02;
            }
            codedOutputStream.A0L(r019, 24);
        }
        if ((this.A00 & 1048576) == 1048576) {
            C49692Lu r020 = this.A0e;
            if (r020 == null) {
                r020 = C49692Lu.A05;
            }
            codedOutputStream.A0L(r020, 25);
        }
        if ((this.A00 & 2097152) == 2097152) {
            C57702nU r021 = this.A0c;
            if (r021 == null) {
                r021 = C57702nU.A0G;
            }
            codedOutputStream.A0L(r021, 26);
        }
        if ((this.A00 & 4194304) == 4194304) {
            C57602nK r022 = this.A0H;
            if (r022 == null) {
                r022 = C57602nK.A09;
            }
            codedOutputStream.A0L(r022, 28);
        }
        if ((this.A00 & 8388608) == 8388608) {
            C57462n5 r023 = this.A0d;
            if (r023 == null) {
                r023 = C57462n5.A05;
            }
            codedOutputStream.A0L(r023, 29);
        }
        if ((this.A00 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            C57542nE r024 = this.A0V;
            if (r024 == null) {
                r024 = C57542nE.A07;
            }
            codedOutputStream.A0L(r024, 30);
        }
        if ((this.A00 & 33554432) == 33554432) {
            C57312mp r025 = this.A0B;
            if (r025 == null) {
                r025 = C57312mp.A04;
            }
            codedOutputStream.A0L(r025, 31);
        }
        if ((this.A00 & 67108864) == 67108864) {
            AnonymousClass2n6 r026 = this.A0g;
            if (r026 == null) {
                r026 = AnonymousClass2n6.A05;
            }
            codedOutputStream.A0L(r026, 35);
        }
        if ((this.A00 & 134217728) == 134217728) {
            C57742nY r027 = this.A0N;
            if (r027 == null) {
                r027 = C57742nY.A09;
            }
            codedOutputStream.A0L(r027, 36);
        }
        if ((this.A00 & 268435456) == 268435456) {
            C56962mF r028 = this.A0G;
            if (r028 == null) {
                r028 = C56962mF.A02;
            }
            codedOutputStream.A0L(r028, 37);
        }
        if ((this.A00 & 536870912) == 536870912) {
            C57682nS r029 = this.A0R;
            if (r029 == null) {
                r029 = C57682nS.A0D;
            }
            codedOutputStream.A0L(r029, 38);
        }
        if ((this.A00 & 1073741824) == 1073741824) {
            C57512nB r030 = this.A0O;
            if (r030 == null) {
                r030 = C57512nB.A06;
            }
            codedOutputStream.A0L(r030, 39);
        }
        if ((this.A00 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            C56962mF r031 = this.A0F;
            if (r031 == null) {
                r031 = C56962mF.A02;
            }
            codedOutputStream.A0L(r031, 40);
        }
        if ((this.A01 & 1) == 1) {
            C57552nF r032 = this.A03;
            if (r032 == null) {
                r032 = C57552nF.A08;
            }
            codedOutputStream.A0L(r032, 42);
        }
        if ((this.A01 & 2) == 2) {
            C57502nA r033 = this.A04;
            if (r033 == null) {
                r033 = C57502nA.A06;
            }
            codedOutputStream.A0L(r033, 43);
        }
        if ((this.A01 & 4) == 4) {
            C57212mf r034 = this.A0S;
            if (r034 == null) {
                r034 = C57212mf.A03;
            }
            codedOutputStream.A0L(r034, 44);
        }
        if ((this.A01 & 8) == 8) {
            C57752nZ r035 = this.A0K;
            if (r035 == null) {
                r035 = C57752nZ.A07;
            }
            codedOutputStream.A0L(r035, 45);
        }
        if ((this.A01 & 16) == 16) {
            AnonymousClass2RJ r036 = this.A0X;
            if (r036 == null) {
                r036 = AnonymousClass2RJ.A05;
            }
            codedOutputStream.A0L(r036, 46);
        }
        if ((this.A01 & 32) == 32) {
            C57452n4 r037 = this.A0L;
            if (r037 == null) {
                r037 = C57452n4.A05;
            }
            codedOutputStream.A0L(r037, 48);
        }
        if ((this.A01 & 64) == 64) {
            C57522nC r038 = this.A0T;
            if (r038 == null) {
                r038 = C57522nC.A06;
            }
            codedOutputStream.A0L(r038, 49);
        }
        if ((this.A01 & 128) == 128) {
            AnonymousClass2RK r039 = this.A0U;
            if (r039 == null) {
                r039 = AnonymousClass2RK.A05;
            }
            codedOutputStream.A0L(r039, 50);
        }
        if ((this.A01 & 256) == 256) {
            C47802Cs r040 = this.A0M;
            if (r040 == null) {
                r040 = C47802Cs.A04;
            }
            codedOutputStream.A0L(r040, 51);
        }
        if ((this.A01 & 512) == 512) {
            C56962mF r041 = this.A0E;
            if (r041 == null) {
                r041 = C56962mF.A02;
            }
            codedOutputStream.A0L(r041, 53);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
