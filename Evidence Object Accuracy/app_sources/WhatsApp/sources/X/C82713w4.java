package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3w4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82713w4 extends AnonymousClass2k7 {
    public final /* synthetic */ AbstractC21000wf A00;

    @Override // X.AnonymousClass2k7
    public void A07(View view, C14260l7 r2, AnonymousClass28D r3, Object obj) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C82713w4(C14260l7 r1, AnonymousClass28D r2, AbstractC21000wf r3) {
        super(r1, r2);
        this.A00 = r3;
    }

    @Override // X.AnonymousClass2k7
    public void A06(View view, C14260l7 r3, AnonymousClass28D r4, Object obj) {
        AnonymousClass3AM.A00(view, r3, r4, this.A00);
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return View.inflate(context, R.layout.wa_bloks_checkbox, null);
    }
}
