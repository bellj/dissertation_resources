package X;

import android.content.Context;
import androidx.appcompat.widget.SearchView;
import com.whatsapp.profile.WebImagePicker;

/* renamed from: X.3hd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74233hd extends SearchView {
    public final /* synthetic */ WebImagePicker A00;

    @Override // androidx.appcompat.widget.SearchView
    public boolean A0J() {
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74233hd(Context context, WebImagePicker webImagePicker) {
        super(context);
        this.A00 = webImagePicker;
    }
}
