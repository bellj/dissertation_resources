package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0Yc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07460Yc implements AnonymousClass02B {
    public final /* synthetic */ BiometricFragment A00;

    public C07460Yc(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        AnonymousClass0P5 r4 = (AnonymousClass0P5) obj;
        if (r4 != null) {
            BiometricFragment biometricFragment = this.A00;
            biometricFragment.A1F(r4.A00, r4.A01);
            AnonymousClass0EP r2 = biometricFragment.A01;
            AnonymousClass016 r0 = r2.A08;
            if (r0 == null) {
                r0 = new AnonymousClass016();
                r2.A08 = r0;
            }
            AnonymousClass0EP.A00(r0, null);
        }
    }
}
