package X;

import com.whatsapp.payments.ui.invites.IndiaUpiPaymentInviteFragment;

/* renamed from: X.5bS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118215bS extends AnonymousClass0Yo {
    public final /* synthetic */ IndiaUpiPaymentInviteFragment A00;

    public C118215bS(IndiaUpiPaymentInviteFragment indiaUpiPaymentInviteFragment) {
        this.A00 = indiaUpiPaymentInviteFragment;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117905ax.class)) {
            return new C117905ax(this.A00.A0C);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
