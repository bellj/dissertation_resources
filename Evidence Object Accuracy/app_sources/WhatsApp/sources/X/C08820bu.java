package X;

import java.io.Closeable;
import java.io.InputStream;

/* renamed from: X.0bu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08820bu implements Closeable {
    public final long[] A00;
    public final InputStream[] A01;
    public final /* synthetic */ C08860by A02;

    public /* synthetic */ C08820bu(C08860by r1, long[] jArr, InputStream[] inputStreamArr) {
        this.A02 = r1;
        this.A01 = inputStreamArr;
        this.A00 = jArr;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        for (InputStream inputStream : this.A01) {
            C08860by.A01(inputStream);
        }
    }
}
