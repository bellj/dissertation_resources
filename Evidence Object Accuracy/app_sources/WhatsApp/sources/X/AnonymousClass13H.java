package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.13H  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass13H {
    public static final Pattern A04 = Pattern.compile("(@\\d+)");
    public static final boolean A05 = Character.isDefined((char) 8296);
    public static final boolean A06 = Character.isDefined((char) 8297);
    public final C15570nT A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C14850m9 A03;

    public AnonymousClass13H(C15570nT r1, C15550nR r2, C15610nY r3, C14850m9 r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public CharSequence A00(CharSequence charSequence, List list) {
        if (list == null || list.isEmpty()) {
            return charSequence;
        }
        SpannableStringBuilder valueOf = SpannableStringBuilder.valueOf(charSequence);
        A03(valueOf, null, list);
        return valueOf;
    }

    public String A01(C15370n3 r4) {
        String str;
        String A01;
        StringBuilder sb = new StringBuilder();
        String str2 = "";
        if (A05) {
            str = "⁨";
        } else {
            str = str2;
        }
        sb.append(str);
        if (r4.A0L()) {
            A01 = C15610nY.A02(r4, false);
        } else if (!TextUtils.isEmpty(r4.A0K)) {
            A01 = r4.A0K;
        } else if (!TextUtils.isEmpty(r4.A0U)) {
            A01 = r4.A0U;
        } else {
            A01 = C248917h.A01(r4);
        }
        sb.append(A01);
        if (A06) {
            str2 = "⁩";
        }
        sb.append(str2);
        return sb.toString();
    }

    public void A02(Context context, SpannableStringBuilder spannableStringBuilder, List list) {
        A03(spannableStringBuilder, new C41681tv(context, this, R.color.link_color, false), list);
    }

    public void A03(SpannableStringBuilder spannableStringBuilder, AbstractC41671tu r11, List list) {
        if (list != null && !list.isEmpty() && !TextUtils.isEmpty(spannableStringBuilder)) {
            HashMap hashMap = new HashMap();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Jid jid = (Jid) it.next();
                if (jid != null) {
                    StringBuilder sb = new StringBuilder("@");
                    String str = jid.user;
                    AnonymousClass009.A05(str);
                    sb.append(str);
                    hashMap.put(sb.toString(), new AnonymousClass01T(jid, null));
                }
            }
            Matcher matcher = A04.matcher(spannableStringBuilder);
            int i = 0;
            while (matcher.find()) {
                String group = matcher.group();
                if (hashMap.containsKey(group)) {
                    AnonymousClass01T r0 = (AnonymousClass01T) hashMap.get(group);
                    AnonymousClass01T r5 = (AnonymousClass01T) r0.A01;
                    if (r5 == null) {
                        AbstractC14640lm r3 = (AbstractC14640lm) r0.A00;
                        C15370n3 A0B = this.A01.A0B(r3);
                        StringBuilder sb2 = new StringBuilder("@");
                        sb2.append(A01(A0B));
                        r5 = new AnonymousClass01T(sb2.toString(), A0B);
                        hashMap.put(group, new AnonymousClass01T(r3, r5));
                    }
                    int start = matcher.start() + i;
                    String str2 = (String) r5.A00;
                    int length = group.length();
                    spannableStringBuilder.replace(start, length + start, (CharSequence) str2);
                    int length2 = str2.length();
                    i += length2 - length;
                    if (r11 != null) {
                        r11.AUx(spannableStringBuilder, (C15370n3) r5.A01, start, length2 + start);
                    }
                }
            }
        }
    }
}
