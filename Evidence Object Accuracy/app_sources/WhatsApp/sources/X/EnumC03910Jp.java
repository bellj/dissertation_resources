package X;

/* renamed from: X.0Jp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03910Jp {
    FULL_SHEET(0),
    HALF_SHEET(1),
    /* Fake field, exist only in values array */
    AUTO_SHEET(2),
    FULL_SCREEN(3),
    FLEXIBLE_SHEET(4);
    
    public final String value;

    EnumC03910Jp(int i) {
        this.value = r2;
    }

    public static EnumC03910Jp A00(String str) {
        EnumC03910Jp[] values = values();
        for (EnumC03910Jp r1 : values) {
            if (r1.toString().equals(str)) {
                return r1;
            }
        }
        StringBuilder sb = new StringBuilder("Error finding Mode enum value for ");
        sb.append(str);
        C28691Op.A00("CdsOpenScreenConfig", sb.toString());
        return FULL_SHEET;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.value;
    }
}
