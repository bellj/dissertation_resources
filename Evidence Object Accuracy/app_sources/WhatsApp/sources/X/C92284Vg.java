package X;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.4Vg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92284Vg {
    public AnonymousClass4YL A00;
    public AnonymousClass4YL A01 = new C114475Lr(this);
    public final ConcurrentHashMap A02;

    public C92284Vg() {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap(100);
        this.A02 = concurrentHashMap;
        concurrentHashMap.put(Date.class, AbstractC114345Ld.A00);
        AnonymousClass4YL r3 = C114495Lt.A0C;
        concurrentHashMap.put(int[].class, r3);
        AnonymousClass4YL r1 = C114495Lt.A05;
        concurrentHashMap.put(Integer[].class, r1);
        concurrentHashMap.put(short[].class, r3);
        concurrentHashMap.put(Short[].class, r1);
        concurrentHashMap.put(long[].class, C114495Lt.A0D);
        concurrentHashMap.put(Long[].class, C114495Lt.A06);
        concurrentHashMap.put(byte[].class, C114495Lt.A08);
        concurrentHashMap.put(Byte[].class, C114495Lt.A01);
        concurrentHashMap.put(char[].class, C114495Lt.A09);
        concurrentHashMap.put(Character[].class, C114495Lt.A02);
        concurrentHashMap.put(float[].class, C114495Lt.A0B);
        concurrentHashMap.put(Float[].class, C114495Lt.A04);
        concurrentHashMap.put(double[].class, C114495Lt.A0A);
        concurrentHashMap.put(Double[].class, C114495Lt.A03);
        concurrentHashMap.put(boolean[].class, C114495Lt.A07);
        concurrentHashMap.put(Boolean[].class, C114495Lt.A00);
        C114465Lq r12 = new C114465Lq(this);
        this.A00 = r12;
        concurrentHashMap.put(AnonymousClass5ZZ.class, r12);
        concurrentHashMap.put(AnonymousClass5VJ.class, this.A00);
        concurrentHashMap.put(AnonymousClass5IB.class, this.A00);
        concurrentHashMap.put(AnonymousClass5IH.class, this.A00);
    }

    public AnonymousClass4YL A00(Class cls) {
        ConcurrentHashMap concurrentHashMap = this.A02;
        AnonymousClass4YL r0 = (AnonymousClass4YL) concurrentHashMap.get(cls);
        if (r0 != null) {
            return r0;
        }
        if (cls != null && (Map.class.isAssignableFrom(cls) || List.class.isAssignableFrom(cls))) {
            C114485Ls r02 = new C114485Ls(cls, this);
            concurrentHashMap.put(cls, r02);
            return r02;
        } else if (cls.isArray()) {
            C114335Lc r03 = new C114335Lc(cls, this);
            concurrentHashMap.putIfAbsent(cls, r03);
            return r03;
        } else if (List.class.isAssignableFrom(cls)) {
            new C114445Lo(cls, this);
            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
        } else if (Map.class.isAssignableFrom(cls)) {
            new C114455Lp(cls, this);
            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
        } else {
            new C114435Ln(cls, this);
            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
        }
    }
}
