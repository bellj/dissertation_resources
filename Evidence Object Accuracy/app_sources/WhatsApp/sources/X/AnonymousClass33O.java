package X;

import android.graphics.RectF;
import java.util.List;
import org.json.JSONObject;

/* renamed from: X.33O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33O extends AbstractC94414bm {
    public C91484Rx A00;

    @Override // X.AbstractC94414bm
    public String A00() {
        return "undo_modify_shape";
    }

    public /* synthetic */ AnonymousClass33O() {
    }

    public AnonymousClass33O(C91484Rx r1, AbstractC454821u r2) {
        super(r2);
        this.A00 = r1;
    }

    @Override // X.AbstractC94414bm
    public void A01(List list) {
        super.A00.A0M(this.A00);
    }

    @Override // X.AbstractC94414bm
    public void A02(JSONObject jSONObject) {
        RectF A0K = C12980iv.A0K();
        A0K.left = (float) jSONObject.getDouble("left");
        A0K.right = (float) jSONObject.getDouble("right");
        A0K.top = (float) jSONObject.getDouble("top");
        A0K.bottom = (float) jSONObject.getDouble("bottom");
        int i = jSONObject.getInt("color");
        float f = (float) jSONObject.getDouble("rotate");
        float f2 = (float) jSONObject.getDouble("strokeWidth");
        if (jSONObject.has("text")) {
            this.A00 = new AnonymousClass45I(A0K, jSONObject.getString("text"), f, f2, i, jSONObject.getInt("fontStyle"));
        } else {
            this.A00 = new C91484Rx(A0K, f, f2, i);
        }
    }

    @Override // X.AbstractC94414bm
    public void A03(JSONObject jSONObject) {
        jSONObject.put("color", this.A00.A02);
        jSONObject.put("rotate", (double) this.A00.A00);
        jSONObject.put("strokeWidth", (double) this.A00.A01);
        jSONObject.put("left", (double) this.A00.A03.left);
        jSONObject.put("right", (double) this.A00.A03.right);
        jSONObject.put("top", (double) this.A00.A03.top);
        jSONObject.put("bottom", (double) this.A00.A03.bottom);
        C91484Rx r2 = this.A00;
        if (r2 instanceof AnonymousClass45I) {
            AnonymousClass45I r22 = (AnonymousClass45I) r2;
            jSONObject.put("text", r22.A01);
            jSONObject.put("fontStyle", r22.A00);
        }
    }
}
