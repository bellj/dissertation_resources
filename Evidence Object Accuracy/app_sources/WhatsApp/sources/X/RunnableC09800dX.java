package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0dX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09800dX implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ BiometricFragment A01;
    public final /* synthetic */ CharSequence A02;

    public RunnableC09800dX(BiometricFragment biometricFragment, CharSequence charSequence, int i) {
        this.A01 = biometricFragment;
        this.A00 = i;
        this.A02 = charSequence;
    }

    @Override // java.lang.Runnable
    public void run() {
        BiometricFragment biometricFragment = this.A01;
        biometricFragment.A1G(this.A00, this.A02);
        biometricFragment.A18();
    }
}
