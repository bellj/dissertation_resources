package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5vB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127865vB {
    public final C30821Yy A00;
    public final UserJid A01;
    public final String A02;
    public final String A03;
    public final String A04;

    public /* synthetic */ C127865vB(C30821Yy r1, UserJid userJid, String str, String str2, String str3) {
        this.A01 = userJid;
        this.A02 = str;
        this.A03 = str2;
        this.A00 = r1;
        this.A04 = str3;
    }
}
