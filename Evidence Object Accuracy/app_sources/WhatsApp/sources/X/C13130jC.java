package X;

import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0jC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13130jC {
    public final C13420jh A00;
    public final String A01;

    public C13130jC(C13420jh r2, Set set) {
        this.A01 = A00(set);
        this.A00 = r2;
    }

    public static String A00(Set set) {
        StringBuilder sb = new StringBuilder();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AnonymousClass00S r1 = (AnonymousClass00S) ((AnonymousClass00P) it.next());
            sb.append(r1.A00);
            sb.append('/');
            sb.append(r1.A01);
            if (it.hasNext()) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
}
