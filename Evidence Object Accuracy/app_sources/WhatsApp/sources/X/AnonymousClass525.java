package X;

/* renamed from: X.525  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass525 implements AnonymousClass5T7 {
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0069, code lost:
        if ((r3.A08() instanceof java.lang.Boolean) != false) goto L_0x006b;
     */
    @Override // X.AnonymousClass5T7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A9i(X.AbstractC94534c0 r3, X.AbstractC94534c0 r4, X.AnonymousClass4RG r5) {
        /*
            r2 = this;
            boolean r0 = r4 instanceof X.C82943wR
            if (r0 != 0) goto L_0x000b
            java.lang.String r0 = "Expected class node"
            X.3wH r0 = X.C82843wH.A00(r0)
            throw r0
        L_0x000b:
            X.3wR r4 = (X.C82943wR) r4
            java.lang.Class r1 = r4.A00
            boolean r0 = r3 instanceof X.C83023wZ
            if (r0 != 0) goto L_0x007d
            boolean r0 = r3 instanceof X.C82933wQ
            if (r0 != 0) goto L_0x007a
            boolean r0 = r3 instanceof X.C82973wU
            if (r0 != 0) goto L_0x0077
            boolean r0 = r3 instanceof X.C82993wW
            if (r0 != 0) goto L_0x0074
            boolean r0 = r3 instanceof X.C82963wT
            if (r0 != 0) goto L_0x007a
            boolean r0 = r3 instanceof X.C82983wV
            if (r0 != 0) goto L_0x0071
            boolean r0 = r3 instanceof X.C83013wY
            if (r0 != 0) goto L_0x006e
            boolean r0 = r3 instanceof X.C82923wP
            if (r0 != 0) goto L_0x007a
            boolean r0 = r3 instanceof X.C83003wX
            if (r0 != 0) goto L_0x003e
            boolean r0 = r3 instanceof X.C82943wR
            if (r0 == 0) goto L_0x006b
            java.lang.Class<java.lang.Class> r0 = java.lang.Class.class
        L_0x0039:
            boolean r0 = X.C12970iu.A1Z(r1, r0)
            return r0
        L_0x003e:
            X.3wX r3 = (X.C83003wX) r3
            java.lang.Object r0 = r3.A08()
            boolean r0 = r0 instanceof java.util.List
            if (r0 != 0) goto L_0x007d
            java.lang.Object r0 = r3.A08()
            boolean r0 = r0 instanceof java.util.Map
            if (r0 == 0) goto L_0x0053
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            goto L_0x0039
        L_0x0053:
            java.lang.Object r0 = r3.A08()
            boolean r0 = r0 instanceof java.lang.Number
            if (r0 != 0) goto L_0x006e
            java.lang.Object r0 = r3.A08()
            boolean r0 = r0 instanceof java.lang.String
            if (r0 != 0) goto L_0x0077
            java.lang.Object r0 = r3.A08()
            boolean r0 = r0 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x007a
        L_0x006b:
            java.lang.Class<java.lang.Boolean> r0 = java.lang.Boolean.class
            goto L_0x0039
        L_0x006e:
            java.lang.Class<java.lang.Number> r0 = java.lang.Number.class
            goto L_0x0039
        L_0x0071:
            java.lang.Class<X.3wV> r0 = X.C82983wV.class
            goto L_0x0039
        L_0x0074:
            java.lang.Class r0 = java.lang.Void.TYPE
            goto L_0x0039
        L_0x0077:
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            goto L_0x0039
        L_0x007a:
            java.lang.Class<java.lang.Void> r0 = java.lang.Void.class
            goto L_0x0039
        L_0x007d:
            java.lang.Class<java.util.List> r0 = java.util.List.class
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass525.A9i(X.4c0, X.4c0, X.4RG):boolean");
    }
}
