package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.2a1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2a1 extends Handler {
    public final /* synthetic */ C54502gp A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2a1(Looper looper, C54502gp r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        C54502gp r0 = this.A00;
        AnonymousClass28D r3 = r0.A03;
        C14210l2 r2 = new C14210l2();
        C14260l7 r1 = r0.A06;
        C28701Oq.A01(r1, r3, C14210l2.A01(r2, r1, 0), (AbstractC14200l1) message.obj);
    }
}
