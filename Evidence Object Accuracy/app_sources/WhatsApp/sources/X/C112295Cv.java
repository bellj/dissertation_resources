package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;

/* renamed from: X.5Cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112295Cv implements Comparator, Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(1);
    public int A00;
    public final String A01;
    public final C100574m8[] A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C112295Cv(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A02 = (C100574m8[]) parcel.createTypedArray(C100574m8.CREATOR);
    }

    public C112295Cv(String str, C100574m8[] r2, boolean z) {
        this.A01 = str;
        r2 = z ? (C100574m8[]) r2.clone() : r2;
        this.A02 = r2;
        Arrays.sort(r2, this);
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        UUID uuid = C95214dK.A03;
        UUID uuid2 = ((C100574m8) obj).A03;
        boolean equals = uuid.equals(uuid2);
        UUID uuid3 = ((C100574m8) obj2).A03;
        if (equals) {
            return uuid.equals(uuid3) ? 0 : 1;
        }
        return uuid2.compareTo(uuid3);
    }

    @Override // java.util.Comparator, java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C112295Cv.class != obj.getClass()) {
                return false;
            }
            C112295Cv r5 = (C112295Cv) obj;
            if (!AnonymousClass3JZ.A0H(this.A01, r5.A01) || !Arrays.equals(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int A04 = (C72463ee.A04(this.A01) * 31) + Arrays.hashCode(this.A02);
        this.A00 = A04;
        return A04;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeTypedArray(this.A02, 0);
    }
}
