package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.5hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121365hj extends AbstractC130285z6 {
    public final String A00;
    public final ArrayList A01;

    public C121365hj(AnonymousClass1V8 r9, String str) {
        int length;
        String str2;
        ArrayList A0l = C12960it.A0l();
        this.A01 = A0l;
        this.A00 = str;
        A0l.clear();
        AnonymousClass1V8[] r7 = r9.A03;
        if (r7 == null || (length = r7.length) == 0) {
            throw new AnonymousClass1V9("Expected head movement directions");
        }
        String[] strArr = new String[length];
        int i = 0;
        do {
            AnonymousClass1V8 r1 = r7[i];
            String A0H = r1.A0H("direction");
            int A04 = r1.A04("sequence");
            if (A04 < 0 || A04 >= length) {
                throw new AnonymousClass1V9("Invalid head movement direction sequence");
            } else if (TextUtils.isEmpty(strArr[A04])) {
                switch (A0H.hashCode()) {
                    case 2715:
                        str2 = "UP";
                        break;
                    case 2104482:
                        str2 = "DOWN";
                        break;
                    case 2332679:
                        str2 = "LEFT";
                        break;
                    case 77974012:
                        str2 = "RIGHT";
                        break;
                    default:
                        throw new AnonymousClass1V9("Invalid head movement direction");
                }
                if (A0H.equals(str2)) {
                    strArr[A04] = A0H;
                    i++;
                } else {
                    throw new AnonymousClass1V9("Invalid head movement direction");
                }
            } else {
                throw new AnonymousClass1V9("Duplicate head movement direction for same sequence");
            }
        } while (i < length);
        A0l.addAll(Arrays.asList(strArr));
    }
}
