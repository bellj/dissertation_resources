package X;

import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;

/* renamed from: X.5Hh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113385Hh extends CertPathValidatorException {
    public Throwable cause;

    public C113385Hh() {
        super("OCSP response expired");
    }

    public C113385Hh(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    public C113385Hh(String str, Throwable th, CertPath certPath, int i) {
        super(str, th, certPath, i);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }

    public static C113385Hh A00(String str, Throwable th, CertPath certPath, int i) {
        return new C113385Hh(str, th, certPath, i);
    }
}
