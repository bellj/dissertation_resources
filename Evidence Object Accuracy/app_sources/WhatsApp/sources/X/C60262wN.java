package X;

import java.util.Map;

/* renamed from: X.2wN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C60262wN extends AnonymousClass4K6 {
    public boolean A00;
    public final Map A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C60262wN) {
                C60262wN r5 = (C60262wN) obj;
                if (!C16700pc.A0O(this.A01, r5.A01) || this.A00 != r5.A00) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.A01.hashCode() * 31;
        boolean z = this.A00;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public C60262wN(Map map, boolean z) {
        super(true);
        this.A01 = map;
        this.A00 = z;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FetchGroupedCategoriesSuccess(categories=");
        A0k.append(this.A01);
        A0k.append(", cached=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
