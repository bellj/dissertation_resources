package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.20b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C450720b {
    public final AbstractC450820c A00;

    public C450720b(AbstractC450820c r1) {
        this.A00 = r1;
    }

    public void A00(UserJid userJid, C15930o9 r6, long j) {
        StringBuilder sb = new StringBuilder("xmpp/reader/on-location-update jid: ");
        sb.append(userJid);
        Log.i(sb.toString());
        AbstractC450820c r3 = this.A00;
        Message obtain = Message.obtain(null, 0, 117, 0, r6);
        Bundle data = obtain.getData();
        data.putParcelable("jid", userJid);
        data.putLong("elapsed", j);
        r3.AYY(obtain);
    }

    public void A01(AnonymousClass1V8 r5, AnonymousClass1OT r6, int i) {
        StringBuilder sb = new StringBuilder("xmpp/reader/on-xmpp-recv type=");
        sb.append(i);
        Log.i(sb.toString());
        AbstractC450820c r3 = this.A00;
        Message obtain = Message.obtain(null, 0, i, 0, r5);
        if (r6 != null) {
            obtain.getData().putParcelable("stanzaKey", r6);
        }
        r3.AYY(obtain);
    }

    public void A02(AnonymousClass1OT r5) {
        StringBuilder sb = new StringBuilder("xmpp/reader/on-ack-stanza stanza-id=");
        sb.append(r5.A07);
        Log.i(sb.toString());
        this.A00.AYY(Message.obtain(null, 0, 205, 0, r5));
    }

    public void A03(AnonymousClass1OT r11, C34081fY r12) {
        Log.i("xmpp/reader/read/on-qr-action-set-chat");
        this.A00.AYY(Message.obtain(null, 0, 39, 0, new C50522Pw(r11.A01, r12, r11.A07, r11.A00)));
    }

    public void A04(String str, int i) {
        StringBuilder sb = new StringBuilder("xmpp/reader/read/on-qr-sync-error ");
        sb.append(i);
        Log.i(sb.toString());
        this.A00.AYY(Message.obtain(null, 0, 29, 0, new AnonymousClass2QJ(str, i)));
    }

    public void A05(String str, String str2, String str3, int i) {
        StringBuilder sb = new StringBuilder("xmpp/reader/on-set-two-factor-auth-error errorCode: ");
        sb.append(i);
        sb.append(" errorMessage: ");
        sb.append(str3);
        Log.w(sb.toString());
        AbstractC450820c r4 = this.A00;
        Bundle bundle = new Bundle();
        bundle.putString("code", str);
        bundle.putString("email", str2);
        bundle.putInt("errorCode", i);
        bundle.putString("errorMessage", str3);
        r4.AYY(Message.obtain(null, 0, 103, 0, bundle));
    }
}
