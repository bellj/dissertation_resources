package X;

/* renamed from: X.10o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C231510o extends C22220yj {
    public C231510o(AnonymousClass1F1 r2) {
        super(r2, 36);
    }

    public int A0A(C37471mS[] r5) {
        if (r5 == null) {
            return A00();
        }
        int i = 0;
        for (int i2 = 0; i2 < A00(); i2++) {
            if (!C37871n9.A01(new C37471mS((int[]) A01(i2)), r5)) {
                i++;
            }
        }
        return i;
    }
}
