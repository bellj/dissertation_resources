package X;

import java.util.List;

/* renamed from: X.1lb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37161lb extends C37171lc {
    public final AnonymousClass4NB A00;
    public final C15370n3 A01;
    public final String A02;
    public final String A03;
    public final List A04;

    public C37161lb(AnonymousClass4NB r2, C15370n3 r3, String str, String str2, List list) {
        super(AnonymousClass39o.A04);
        this.A02 = str;
        this.A03 = str2;
        this.A04 = list;
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && super.equals(obj)) {
            C37161lb r4 = (C37161lb) obj;
            if (this.A02.equals(r4.A02) && this.A03.equals(r4.A03) && this.A04.equals(r4.A04) && this.A01.equals(r4.A01)) {
                return this.A00.equals(r4.A00);
            }
        }
        return false;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return (((((((((super.hashCode() * 31) + this.A02.hashCode()) * 31) + this.A03.hashCode()) * 31) + this.A04.hashCode()) * 31) + this.A01.hashCode()) * 31) + this.A00.hashCode();
    }
}
