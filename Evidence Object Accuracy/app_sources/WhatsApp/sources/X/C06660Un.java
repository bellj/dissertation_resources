package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Un  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06660Un implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ C06590Ug A00;

    public C06660Un(C06590Ug r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.A00.A00 = valueAnimator.getAnimatedFraction();
    }
}
