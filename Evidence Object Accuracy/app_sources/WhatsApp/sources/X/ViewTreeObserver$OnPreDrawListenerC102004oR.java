package X;

import android.view.ViewTreeObserver;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.4oR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC102004oR implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C36571k6 A00;
    public final /* synthetic */ AnonymousClass4RH A01;
    public final /* synthetic */ boolean A02;

    public ViewTreeObserver$OnPreDrawListenerC102004oR(C36571k6 r1, AnonymousClass4RH r2, boolean z) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = z;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass4RH r3 = this.A01;
        C12980iv.A1G(r3.A02, this);
        SelectionCheckView selectionCheckView = r3.A02;
        boolean z = this.A02;
        selectionCheckView.A04(z, true);
        this.A00.A00(r3.A02, z);
        return false;
    }
}
