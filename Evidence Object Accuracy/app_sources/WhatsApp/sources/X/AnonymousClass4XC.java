package X;

/* renamed from: X.4XC  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4XC {
    public C93754ag A00;
    public final int A01;
    public final C106944wX A02;
    public final AnonymousClass5VX A03;

    public AnonymousClass4XC(AnonymousClass5SJ r13, AnonymousClass5VX r14, int i, long j, long j2, long j3, long j4, long j5) {
        this.A03 = r14;
        this.A01 = i;
        this.A02 = new C106944wX(r13, j, j2, j3, j4, j5);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0075, code lost:
        r23.A00 = null;
        r8.AVj();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(X.AnonymousClass5Yf r24, X.AnonymousClass4IG r25) {
        /*
            r23 = this;
        L_0x0000:
            r9 = r23
            X.4ag r6 = r9.A00
            X.C95314dV.A01(r6)
            long r0 = r6.A02
            long r10 = r6.A00
            long r3 = r6.A04
            long r10 = r10 - r0
            int r2 = r9.A01
            long r7 = (long) r2
            int r2 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            r10 = r24
            r7 = r25
            if (r2 > 0) goto L_0x002d
            r2 = 0
            r9.A00 = r2
            X.5VX r2 = r9.A03
            r2.AVj()
            long r3 = r10.AFo()
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 == 0) goto L_0x00af
            r7.A00 = r0
        L_0x002b:
            r0 = 1
            return r0
        L_0x002d:
            long r11 = r10.AFo()
            long r0 = r3 - r11
            r11 = 0
            int r2 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r2 < 0) goto L_0x007b
            r11 = 262144(0x40000, double:1.295163E-318)
            int r2 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r2 > 0) goto L_0x007b
            int r2 = (int) r0
            r10.Ae3(r2)
            r10.Aaj()
            X.5VX r8 = r9.A03
            long r11 = r6.A07
            X.4aK r1 = r8.AbM(r10, r11)
            int r2 = r1.A00
            r0 = -3
            if (r2 == r0) goto L_0x0075
            r0 = -2
            if (r2 == r0) goto L_0x0093
            r0 = -1
            if (r2 == r0) goto L_0x0086
            if (r2 != 0) goto L_0x00b1
            long r3 = r1.A01
            long r0 = r10.AFo()
            long r5 = r3 - r0
            r1 = 0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0075
            r1 = 262144(0x40000, double:1.295163E-318)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0075
            int r0 = (int) r5
            r10.Ae3(r0)
        L_0x0075:
            r0 = 0
            r9.A00 = r0
            r8.AVj()
        L_0x007b:
            long r1 = r10.AFo()
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x00af
            r7.A00 = r3
            goto L_0x002b
        L_0x0086:
            long r15 = r1.A02
            long r4 = r1.A01
            r6.A01 = r15
            r6.A00 = r4
            long r13 = r6.A03
            long r2 = r6.A02
            goto L_0x009f
        L_0x0093:
            long r13 = r1.A02
            long r2 = r1.A01
            r6.A03 = r13
            r6.A02 = r2
            long r15 = r6.A01
            long r4 = r6.A00
        L_0x009f:
            long r0 = r6.A05
            r19 = r4
            r21 = r0
            r17 = r2
            long r0 = X.C93754ag.A00(r11, r13, r15, r17, r19, r21)
            r6.A04 = r0
            goto L_0x0000
        L_0x00af:
            r0 = 0
            return r0
        L_0x00b1:
            java.lang.String r0 = "Invalid case"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4XC.A00(X.5Yf, X.4IG):int");
    }

    public final void A01(long j) {
        C93754ag r0 = this.A00;
        if (r0 == null || r0.A06 != j) {
            C106944wX r1 = this.A02;
            this.A00 = new C93754ag(j, r1.A05.Aep(j), r1.A02, r1.A04, r1.A01, r1.A00);
        }
    }
}
