package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.2ts  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59042ts extends AbstractC59082tw {
    public final C19850um A00;
    public final C92164Uu A01;
    public final AnonymousClass3HN A02;
    public final C18640sm A03;
    public final C15680nj A04;
    public final AnonymousClass4TA A05;
    public final C19870uo A06;
    public final C17220qS A07;
    public final C19840ul A08;

    public C59042ts(C14650lo r2, C19850um r3, C92164Uu r4, AnonymousClass3HN r5, C18640sm r6, C15680nj r7, AnonymousClass4TA r8, C19870uo r9, C17220qS r10, C19840ul r11) {
        super(r2, r8.A00);
        this.A02 = r5;
        this.A07 = r10;
        this.A08 = r11;
        this.A05 = r8;
        this.A00 = r3;
        this.A04 = r7;
        this.A03 = r6;
        this.A06 = r9;
        this.A01 = r4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("ProductRequestProtocolHelper/onDeliveryFailure");
        this.A08.A02("view_product_tag");
        this.A01.A00(this.A05, 0);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r7, String str) {
        this.A08.A02("view_product_tag");
        AnonymousClass3HN r2 = this.A02;
        C44721zR A01 = r2.A01(r7);
        AnonymousClass4TA r5 = this.A05;
        UserJid userJid = r5.A00;
        r2.A03(super.A01, userJid, r7);
        if (A01 != null) {
            List list = A01.A01;
            if (!list.isEmpty()) {
                this.A00.A0C((C44691zO) list.get(0), userJid);
                C92164Uu r4 = this.A01;
                r4.A00.A07.A0H(new RunnableBRunnable0Shape1S1200000_I1(r5, r4, ((C44691zO) list.get(0)).A0D, 8));
                return;
            }
        }
        Log.e("ProductRequestProtocolHelper/onSuccess/error: empty response");
    }
}
