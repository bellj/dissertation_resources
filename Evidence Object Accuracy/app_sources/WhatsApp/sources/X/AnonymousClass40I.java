package X;

/* renamed from: X.40I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass40I extends AnonymousClass4N9 {
    public final AnonymousClass2x6 A00;

    public AnonymousClass40I(AnonymousClass2x6 r2, C68553Vv r3) {
        super(r3, 1);
        this.A00 = r2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A00.equals(((AnonymousClass40I) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
