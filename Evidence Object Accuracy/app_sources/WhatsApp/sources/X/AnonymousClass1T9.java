package X;

import java.security.PrivilegedAction;

/* renamed from: X.1T9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1T9 implements PrivilegedAction {
    public final /* synthetic */ C27511Hu A00;

    public AnonymousClass1T9(C27511Hu r1) {
        this.A00 = r1;
    }

    @Override // java.security.PrivilegedAction
    public Object run() {
        C27511Hu r2 = this.A00;
        r2.A00("org.spongycastle.jcajce.provider.digest.", C27511Hu.A02);
        r2.A00("org.spongycastle.jcajce.provider.symmetric.", C27511Hu.A04);
        r2.A00("org.spongycastle.jcajce.provider.symmetric.", C27511Hu.A03);
        return null;
    }
}
