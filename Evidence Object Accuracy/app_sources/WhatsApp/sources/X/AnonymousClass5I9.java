package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/* renamed from: X.5I9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5I9<E> extends AbstractSet<E> implements Serializable {
    public transient Object[] elements;
    public transient int[] entries;
    public transient int metadata;
    public transient int size;
    public transient Object table;

    public int adjustAfterRemove(int i, int i2) {
        return i - 1;
    }

    public AnonymousClass5I9() {
        init(3);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean add(Object obj) {
        if (needsAllocArrays()) {
            allocArrays();
        }
        Set delegateOrNull = delegateOrNull();
        if (delegateOrNull == null) {
            int[] requireEntries = requireEntries();
            Object[] requireElements = requireElements();
            int i = this.size;
            int i2 = i + 1;
            int smearedHash = C28301Mo.smearedHash(obj);
            int hashTableMask = hashTableMask();
            int i3 = smearedHash & hashTableMask;
            Object requireTable = requireTable();
            int tableGet = C95584e0.tableGet(requireTable, i3);
            if (tableGet == 0) {
                if (i2 <= hashTableMask) {
                    C95584e0.tableSet(requireTable, i3, i2);
                }
                hashTableMask = resizeTable(hashTableMask, C95584e0.newCapacity(hashTableMask), smearedHash, i);
            } else {
                int hashPrefix = C95584e0.getHashPrefix(smearedHash, hashTableMask);
                int i4 = 0;
                while (true) {
                    int i5 = tableGet - 1;
                    int i6 = requireEntries[i5];
                    if (C95584e0.getHashPrefix(i6, hashTableMask) == hashPrefix && AnonymousClass28V.A00(obj, requireElements[i5])) {
                        return false;
                    }
                    int next = C95584e0.getNext(i6, hashTableMask);
                    i4++;
                    if (next != 0) {
                        tableGet = next;
                    } else if (i4 >= 9) {
                        delegateOrNull = convertToHashFloodingResistantImplementation();
                    } else if (i2 <= hashTableMask) {
                        requireEntries[i5] = C95584e0.maskCombine(i6, i2, hashTableMask);
                    }
                }
            }
            resizeMeMaybe(i2);
            insertEntry(i, obj, smearedHash, hashTableMask);
            this.size = i2;
            incrementModCount();
            return true;
        }
        return delegateOrNull.add(obj);
    }

    public int allocArrays() {
        C28291Mn.A05("Arrays already allocated", needsAllocArrays());
        int i = this.metadata;
        int tableSize = C95584e0.tableSize(i);
        this.table = C95584e0.createTable(tableSize);
        setHashTableMask(tableSize - 1);
        this.entries = new int[i];
        this.elements = new Object[i];
        return i;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public void clear() {
        if (!needsAllocArrays()) {
            incrementModCount();
            Set delegateOrNull = delegateOrNull();
            if (delegateOrNull != null) {
                this.metadata = Math.min(Math.max(size(), 3), 1073741823);
                delegateOrNull.clear();
                this.table = null;
            } else {
                Arrays.fill(requireElements(), 0, this.size, (Object) null);
                C95584e0.tableClear(requireTable());
                Arrays.fill(requireEntries(), 0, this.size, 0);
            }
            this.size = 0;
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean contains(Object obj) {
        if (!needsAllocArrays()) {
            Set delegateOrNull = delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.contains(obj);
            }
            int smearedHash = C28301Mo.smearedHash(obj);
            int hashTableMask = hashTableMask();
            int tableGet = C95584e0.tableGet(requireTable(), smearedHash & hashTableMask);
            if (tableGet != 0) {
                int hashPrefix = C95584e0.getHashPrefix(smearedHash, hashTableMask);
                do {
                    int i = tableGet - 1;
                    int entry = entry(i);
                    if (C95584e0.getHashPrefix(entry, hashTableMask) == hashPrefix && AnonymousClass28V.A00(obj, element(i))) {
                        return true;
                    }
                    tableGet = C95584e0.getNext(entry, hashTableMask);
                } while (tableGet != 0);
            }
        }
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set */
    /* JADX WARN: Multi-variable type inference failed */
    public Set convertToHashFloodingResistantImplementation() {
        Set createHashFloodingResistantDelegate = createHashFloodingResistantDelegate(hashTableMask() + 1);
        int firstEntryIndex = firstEntryIndex();
        while (firstEntryIndex >= 0) {
            createHashFloodingResistantDelegate.add(element(firstEntryIndex));
            firstEntryIndex = getSuccessor(firstEntryIndex);
        }
        this.table = createHashFloodingResistantDelegate;
        this.entries = null;
        this.elements = null;
        incrementModCount();
        return createHashFloodingResistantDelegate;
    }

    public static AnonymousClass5I9 create() {
        return new AnonymousClass5I9();
    }

    private Set createHashFloodingResistantDelegate(int i) {
        return new LinkedHashSet(i, 1.0f);
    }

    public Set delegateOrNull() {
        Object obj = this.table;
        if (obj instanceof Set) {
            return (Set) obj;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public Object element(int i) {
        return requireElements()[i];
    }

    private int entry(int i) {
        return requireEntries()[i];
    }

    public int firstEntryIndex() {
        return isEmpty() ? -1 : 0;
    }

    public int getSuccessor(int i) {
        int i2 = i + 1;
        if (i2 >= this.size) {
            return -1;
        }
        return i2;
    }

    private int hashTableMask() {
        return (1 << (this.metadata & 31)) - 1;
    }

    public void incrementModCount() {
        this.metadata += 32;
    }

    public void init(int i) {
        this.metadata = Math.min(Math.max(i, 1), 1073741823);
    }

    public void insertEntry(int i, Object obj, int i2, int i3) {
        setEntry(i, C95584e0.maskCombine(i2, 0, i3));
        setElement(i, obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean isEmpty() {
        return C12960it.A1T(size());
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator iterator() {
        Set delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.iterator();
        }
        return new AnonymousClass5DI(this);
    }

    public void moveLastEntry(int i, int i2) {
        int i3;
        int i4;
        Object requireTable = requireTable();
        int[] requireEntries = requireEntries();
        Object[] requireElements = requireElements();
        int size = size() - 1;
        if (i < size) {
            Object obj = requireElements[size];
            requireElements[i] = obj;
            requireElements[size] = null;
            requireEntries[i] = requireEntries[size];
            requireEntries[size] = 0;
            int smearedHash = C28301Mo.smearedHash(obj) & i2;
            int tableGet = C95584e0.tableGet(requireTable, smearedHash);
            int i5 = size + 1;
            if (tableGet == i5) {
                C95584e0.tableSet(requireTable, smearedHash, i + 1);
                return;
            }
            do {
                i3 = tableGet - 1;
                i4 = requireEntries[i3];
                tableGet = C95584e0.getNext(i4, i2);
            } while (tableGet != i5);
            requireEntries[i3] = C95584e0.maskCombine(i4, i + 1, i2);
            return;
        }
        requireElements[i] = null;
        requireEntries[i] = 0;
    }

    public boolean needsAllocArrays() {
        return C12980iv.A1X(this.table);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: X.5I9<E> */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            init(readInt);
            for (int i = 0; i < readInt; i++) {
                add(objectInputStream.readObject());
            }
            return;
        }
        throw new InvalidObjectException(C12960it.A0e("Invalid size: ", C12980iv.A0t(25), readInt));
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean remove(Object obj) {
        if (!needsAllocArrays()) {
            Set delegateOrNull = delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.remove(obj);
            }
            int hashTableMask = hashTableMask();
            int remove = C95584e0.remove(obj, null, hashTableMask, requireTable(), requireEntries(), requireElements(), null);
            if (remove != -1) {
                moveLastEntry(remove, hashTableMask);
                this.size--;
                incrementModCount();
                return true;
            }
        }
        return false;
    }

    private Object[] requireElements() {
        return this.elements;
    }

    private int[] requireEntries() {
        return this.entries;
    }

    private Object requireTable() {
        return this.table;
    }

    public void resizeEntries(int i) {
        this.entries = Arrays.copyOf(requireEntries(), i);
        this.elements = Arrays.copyOf(requireElements(), i);
    }

    private void resizeMeMaybe(int i) {
        int min;
        int length = requireEntries().length;
        if (i > length && (min = Math.min(1073741823, (Math.max(1, length >>> 1) + length) | 1)) != length) {
            resizeEntries(min);
        }
    }

    private int resizeTable(int i, int i2, int i3, int i4) {
        Object createTable = C95584e0.createTable(i2);
        int i5 = i2 - 1;
        if (i4 != 0) {
            C95584e0.tableSet(createTable, i3 & i5, i4 + 1);
        }
        Object requireTable = requireTable();
        int[] requireEntries = requireEntries();
        for (int i6 = 0; i6 <= i; i6++) {
            int tableGet = C95584e0.tableGet(requireTable, i6);
            while (tableGet != 0) {
                int i7 = tableGet - 1;
                int i8 = requireEntries[i7];
                int hashPrefix = C95584e0.getHashPrefix(i8, i) | i6;
                int i9 = hashPrefix & i5;
                int tableGet2 = C95584e0.tableGet(createTable, i9);
                C95584e0.tableSet(createTable, i9, tableGet);
                requireEntries[i7] = C95584e0.maskCombine(hashPrefix, tableGet2, i5);
                tableGet = C95584e0.getNext(i8, i);
            }
        }
        this.table = createTable;
        setHashTableMask(i5);
        return i5;
    }

    private void setElement(int i, Object obj) {
        requireElements()[i] = obj;
    }

    private void setEntry(int i, int i2) {
        requireEntries()[i] = i2;
    }

    private void setHashTableMask(int i) {
        this.metadata = C95584e0.maskCombine(this.metadata, 32 - Integer.numberOfLeadingZeros(i), 31);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        Set delegateOrNull = delegateOrNull();
        return delegateOrNull != null ? delegateOrNull.size() : this.size;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public Object[] toArray() {
        if (needsAllocArrays()) {
            return new Object[0];
        }
        Set delegateOrNull = delegateOrNull();
        return delegateOrNull != null ? delegateOrNull.toArray() : Arrays.copyOf(requireElements(), this.size);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public Object[] toArray(Object[] objArr) {
        if (needsAllocArrays()) {
            if (objArr.length > 0) {
                objArr[0] = null;
            }
            return objArr;
        }
        Set delegateOrNull = delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.toArray(objArr);
        }
        return C28331Mt.toArrayImpl(requireElements(), 0, this.size, objArr);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(size());
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            objectOutputStream.writeObject(it.next());
        }
    }
}
