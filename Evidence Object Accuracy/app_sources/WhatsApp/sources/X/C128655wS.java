package X;

import com.whatsapp.wabloks.ui.PrivacyNotice.PrivacyNoticeDialogFragment;

/* renamed from: X.5wS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128655wS {
    public final /* synthetic */ PrivacyNoticeDialogFragment A00;

    public /* synthetic */ C128655wS(PrivacyNoticeDialogFragment privacyNoticeDialogFragment) {
        this.A00 = privacyNoticeDialogFragment;
    }

    public final void A00(int i) {
        PrivacyNoticeDialogFragment privacyNoticeDialogFragment = this.A00;
        privacyNoticeDialogFragment.A02.A01(new C50142Oh(i));
        privacyNoticeDialogFragment.A02.A01(new AnonymousClass6E5());
    }
}
