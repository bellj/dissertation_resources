package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0M2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0M2 {
    public static final Map A00;

    static {
        HashMap hashMap = new HashMap(9);
        A00 = hashMap;
        AnonymousClass0JP r2 = AnonymousClass0JP.pt;
        hashMap.put("xx-small", new C08910c3(r2, 0.694f));
        hashMap.put("x-small", new C08910c3(r2, 0.833f));
        hashMap.put("small", new C08910c3(r2, 10.0f));
        hashMap.put("medium", new C08910c3(r2, 12.0f));
        hashMap.put("large", new C08910c3(r2, 14.4f));
        hashMap.put("x-large", new C08910c3(r2, 17.3f));
        hashMap.put("xx-large", new C08910c3(r2, 20.7f));
        AnonymousClass0JP r22 = AnonymousClass0JP.percent;
        hashMap.put("smaller", new C08910c3(r22, 83.33f));
        hashMap.put("larger", new C08910c3(r22, 120.0f));
    }
}
