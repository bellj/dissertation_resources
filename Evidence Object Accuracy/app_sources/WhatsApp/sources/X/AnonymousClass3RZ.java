package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;

/* renamed from: X.3RZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3RZ implements AbstractC009404s {
    public final /* synthetic */ C48992Is A00;
    public final /* synthetic */ C15580nU A01;

    public AnonymousClass3RZ(C48992Is r1, C15580nU r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C48992Is r0 = this.A00;
        C15580nU r1 = this.A01;
        AnonymousClass01J r2 = r0.A00.A03;
        AbstractC15710nm A0Q = C12970iu.A0Q(r2);
        AbstractC14440lR A0T = C12960it.A0T(r2);
        C19990v2 A0c = C12980iv.A0c(r2);
        C20660w7 A0d = C12990iw.A0d(r2);
        C14820m6 A0Z = C12970iu.A0Z(r2);
        C22640zP r5 = (C22640zP) r2.A3Z.get();
        AnonymousClass1E8 r14 = (AnonymousClass1E8) r2.ADy.get();
        C21320xE r11 = (C21320xE) r2.A4Y.get();
        AnonymousClass11A r13 = (AnonymousClass11A) r2.A8o.get();
        C37271lv r3 = new C37271lv(A0Q, r5, C12960it.A0O(r2), C12990iw.A0Z(r2), C12960it.A0P(r2), A0Z, A0c, r11, C12980iv.A0d(r2), r13, r14, (AnonymousClass1E5) r2.AKs.get(), r1, A0d, (C20750wG) r2.AGL.get(), A0T);
        ExecutorC27271Gr r22 = r3.A0U;
        r22.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 46));
        r22.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 44));
        r22.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 47));
        r3.A0V.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 48));
        return r3;
    }
}
