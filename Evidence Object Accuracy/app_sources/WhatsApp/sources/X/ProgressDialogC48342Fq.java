package X;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.KeyEvent;

/* renamed from: X.2Fq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ProgressDialogC48342Fq extends ProgressDialog {
    public ProgressDialogC48342Fq(Context context) {
        super(context);
    }

    @Override // android.app.AlertDialog, android.view.KeyEvent.Callback, android.app.Dialog
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 84) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
