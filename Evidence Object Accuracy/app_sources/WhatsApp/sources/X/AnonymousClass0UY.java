package X;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;

/* renamed from: X.0UY  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UY {
    public ViewParent A00;
    public ViewParent A01;
    public boolean A02;
    public int[] A03;
    public final View A04;

    public AnonymousClass0UY(View view) {
        this.A04 = view;
    }

    public void A00(int i) {
        ViewParent viewParent;
        if (i == 0) {
            viewParent = this.A01;
        } else if (i == 1) {
            viewParent = this.A00;
        } else {
            return;
        }
        if (viewParent != null) {
            View view = this.A04;
            if (viewParent instanceof AbstractC016407t) {
                ((AbstractC016407t) viewParent).AWp(view, i);
            } else if (i == 0) {
                if (Build.VERSION.SDK_INT >= 21) {
                    try {
                        AnonymousClass0UT.A01(view, viewParent);
                    } catch (AbstractMethodError e) {
                        StringBuilder sb = new StringBuilder("ViewParent ");
                        sb.append(viewParent);
                        sb.append(" does not implement interface method onStopNestedScroll");
                        Log.e("ViewParentCompat", sb.toString(), e);
                    }
                } else if (viewParent instanceof AbstractC016507u) {
                    ((AbstractC016507u) viewParent).onStopNestedScroll(view);
                }
            }
            if (i == 0) {
                this.A01 = null;
            } else if (i == 1) {
                this.A00 = null;
            }
        }
    }

    public boolean A01(float f, float f2) {
        ViewParent viewParent;
        if (!this.A02 || (viewParent = this.A01) == null) {
            return false;
        }
        View view = this.A04;
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                return AnonymousClass0UT.A05(view, viewParent, f, f2);
            } catch (AbstractMethodError e) {
                StringBuilder sb = new StringBuilder("ViewParent ");
                sb.append(viewParent);
                sb.append(" does not implement interface method onNestedPreFling");
                Log.e("ViewParentCompat", sb.toString(), e);
                return false;
            }
        } else if (viewParent instanceof AbstractC016507u) {
            return ((AbstractC016507u) viewParent).onNestedPreFling(view, f, f2);
        } else {
            return false;
        }
    }

    public boolean A02(float f, float f2, boolean z) {
        ViewParent viewParent;
        if (!this.A02 || (viewParent = this.A01) == null) {
            return false;
        }
        View view = this.A04;
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                return AnonymousClass0UT.A06(view, viewParent, f, f2, z);
            } catch (AbstractMethodError e) {
                StringBuilder sb = new StringBuilder("ViewParent ");
                sb.append(viewParent);
                sb.append(" does not implement interface method onNestedFling");
                Log.e("ViewParentCompat", sb.toString(), e);
                return false;
            }
        } else if (viewParent instanceof AbstractC016507u) {
            return ((AbstractC016507u) viewParent).onNestedFling(view, f, f2, z);
        } else {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0008, code lost:
        if (r0 == null) goto L_0x000a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0087 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(int r8, int r9) {
        /*
            r7 = this;
            if (r9 == 0) goto L_0x008c
            r0 = 1
            if (r9 != r0) goto L_0x000a
            android.view.ViewParent r0 = r7.A00
        L_0x0007:
            r1 = 1
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            r1 = 0
        L_0x000b:
            r3 = 1
            if (r1 != 0) goto L_0x0037
            boolean r0 = r7.A02
            if (r0 == 0) goto L_0x00af
            android.view.View r5 = r7.A04
            android.view.ViewParent r4 = r5.getParent()
            r2 = r5
        L_0x0019:
            if (r4 == 0) goto L_0x00af
            boolean r0 = r4 instanceof X.AbstractC016407t
            if (r0 == 0) goto L_0x004e
            r0 = r4
            X.07t r0 = (X.AbstractC016407t) r0
            boolean r0 = r0.AWL(r2, r5, r8, r9)
        L_0x0026:
            if (r0 == 0) goto L_0x0080
            if (r9 == 0) goto L_0x004b
            if (r9 != r3) goto L_0x002e
            r7.A00 = r4
        L_0x002e:
            boolean r0 = r4 instanceof X.AbstractC016407t
            if (r0 == 0) goto L_0x0038
            X.07t r4 = (X.AbstractC016407t) r4
            r4.ASz(r2, r5, r8, r9)
        L_0x0037:
            return r3
        L_0x0038:
            if (r9 != 0) goto L_0x0037
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0041
            goto L_0x0090
        L_0x0041:
            boolean r0 = r4 instanceof X.AbstractC016507u
            if (r0 == 0) goto L_0x0037
            X.07u r4 = (X.AbstractC016507u) r4
            r4.onNestedScrollAccepted(r2, r5, r8)
            return r3
        L_0x004b:
            r7.A01 = r4
            goto L_0x002e
        L_0x004e:
            if (r9 != 0) goto L_0x0080
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x005b
            boolean r0 = X.AnonymousClass0UT.A04(r2, r5, r4, r8)     // Catch: AbstractMethodError -> 0x0067
            goto L_0x0026
        L_0x005b:
            boolean r0 = r4 instanceof X.AbstractC016507u
            if (r0 == 0) goto L_0x0080
            r0 = r4
            X.07u r0 = (X.AbstractC016507u) r0
            boolean r0 = r0.onStartNestedScroll(r2, r5, r8)
            goto L_0x0026
        L_0x0067:
            r6 = move-exception
            java.lang.String r0 = "ViewParent "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = " does not implement interface method onStartNestedScroll"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "ViewParentCompat"
            android.util.Log.e(r0, r1, r6)
        L_0x0080:
            boolean r0 = r4 instanceof android.view.View
            if (r0 == 0) goto L_0x0087
            r2 = r4
            android.view.View r2 = (android.view.View) r2
        L_0x0087:
            android.view.ViewParent r4 = r4.getParent()
            goto L_0x0019
        L_0x008c:
            android.view.ViewParent r0 = r7.A01
            goto L_0x0007
        L_0x0090:
            X.AnonymousClass0UT.A00(r2, r5, r4, r8)     // Catch: AbstractMethodError -> 0x0094
            goto L_0x00ae
        L_0x0094:
            r2 = move-exception
            java.lang.String r0 = "ViewParent "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            java.lang.String r0 = " does not implement interface method onNestedScrollAccepted"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "ViewParentCompat"
            android.util.Log.e(r0, r1, r2)
            return r3
        L_0x00ae:
            return r3
        L_0x00af:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0UY.A03(int, int):boolean");
    }

    public boolean A04(int[] iArr, int[] iArr2, int i, int i2, int i3) {
        ViewParent viewParent;
        int i4;
        int i5;
        int[] iArr3 = iArr;
        if (this.A02) {
            if (i3 == 0) {
                viewParent = this.A01;
            } else if (i3 == 1) {
                viewParent = this.A00;
            }
            if (viewParent != null) {
                if (i != 0 || i2 != 0) {
                    if (iArr2 != null) {
                        this.A04.getLocationInWindow(iArr2);
                        i4 = iArr2[0];
                        i5 = iArr2[1];
                    } else {
                        i4 = 0;
                        i5 = 0;
                    }
                    if (iArr == null && (iArr3 = this.A03) == null) {
                        iArr3 = new int[2];
                        this.A03 = iArr3;
                    }
                    iArr3[0] = 0;
                    iArr3[1] = 0;
                    View view = this.A04;
                    if (viewParent instanceof AbstractC016407t) {
                        ((AbstractC016407t) viewParent).ASw(view, iArr3, i, i2, i3);
                    } else if (i3 == 0) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            try {
                                AnonymousClass0UT.A03(view, viewParent, iArr3, i, i2);
                            } catch (AbstractMethodError e) {
                                StringBuilder sb = new StringBuilder("ViewParent ");
                                sb.append(viewParent);
                                sb.append(" does not implement interface method onNestedPreScroll");
                                Log.e("ViewParentCompat", sb.toString(), e);
                            }
                        } else if (viewParent instanceof AbstractC016507u) {
                            ((AbstractC016507u) viewParent).onNestedPreScroll(view, i, i2, iArr3);
                        }
                    }
                    if (iArr2 != null) {
                        view.getLocationInWindow(iArr2);
                        iArr2[0] = iArr2[0] - i4;
                        iArr2[1] = iArr2[1] - i5;
                    }
                    if (!(iArr3[0] == 0 && iArr3[1] == 0)) {
                        return true;
                    }
                } else if (iArr2 != null) {
                    iArr2[0] = 0;
                    iArr2[1] = 0;
                }
            }
        }
        return false;
    }

    public final boolean A05(int[] iArr, int[] iArr2, int i, int i2, int i3, int i4, int i5) {
        ViewParent viewParent;
        int i6;
        int i7;
        int[] iArr3 = iArr2;
        if (this.A02) {
            if (i5 == 0) {
                viewParent = this.A01;
            } else if (i5 == 1) {
                viewParent = this.A00;
            }
            if (viewParent != null) {
                if (i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
                    if (iArr != null) {
                        this.A04.getLocationInWindow(iArr);
                        i6 = iArr[0];
                        i7 = iArr[1];
                    } else {
                        i6 = 0;
                        i7 = 0;
                    }
                    if (iArr2 == null) {
                        iArr3 = this.A03;
                        if (iArr3 == null) {
                            iArr3 = new int[2];
                            this.A03 = iArr3;
                        }
                        iArr3[0] = 0;
                        iArr3[1] = 0;
                    }
                    View view = this.A04;
                    if (viewParent instanceof AbstractC12820ib) {
                        ((AbstractC12820ib) viewParent).ASy(view, iArr3, i, i2, i3, i4, i5);
                    } else {
                        iArr3[0] = iArr3[0] + i3;
                        iArr3[1] = iArr3[1] + i4;
                        if (viewParent instanceof AbstractC016407t) {
                            ((AbstractC016407t) viewParent).ASx(view, i, i2, i3, i4, i5);
                        } else if (i5 == 0) {
                            if (Build.VERSION.SDK_INT >= 21) {
                                try {
                                    AnonymousClass0UT.A02(view, viewParent, i, i2, i3, i4);
                                } catch (AbstractMethodError e) {
                                    StringBuilder sb = new StringBuilder("ViewParent ");
                                    sb.append(viewParent);
                                    sb.append(" does not implement interface method onNestedScroll");
                                    Log.e("ViewParentCompat", sb.toString(), e);
                                }
                            } else if (viewParent instanceof AbstractC016507u) {
                                ((AbstractC016507u) viewParent).onNestedScroll(view, i, i2, i3, i4);
                            }
                        }
                    }
                    if (iArr != null) {
                        view.getLocationInWindow(iArr);
                        iArr[0] = iArr[0] - i6;
                        iArr[1] = iArr[1] - i7;
                    }
                    return true;
                } else if (iArr != null) {
                    iArr[0] = 0;
                    iArr[1] = 0;
                }
            }
        }
        return false;
    }
}
