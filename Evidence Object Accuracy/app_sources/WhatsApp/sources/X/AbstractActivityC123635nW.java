package X;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.security.KeyPair;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5nW */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC123635nW extends AbstractActivityC121745js {
    public C20920wX A00;
    public UserJid A01;
    public C22480z9 A02;
    public AnonymousClass61M A03;
    public C130155yt A04;
    public C129865yQ A05;
    public C130125yq A06;
    public AnonymousClass60Y A07;
    public AnonymousClass61F A08;
    public C129435xi A09;
    public C130105yo A0A;
    public C129235xO A0B;
    public AnonymousClass61C A0C;
    public String A0D = null;
    public Map A0E = null;
    public boolean A0F = false;
    public boolean A0G = false;
    public final Map A0H = C12970iu.A11();

    public static void A1U(BottomSheetDialogFragment bottomSheetDialogFragment) {
        if ((bottomSheetDialogFragment instanceof FingerprintBottomSheet) && C28391Mz.A03()) {
            bottomSheetDialogFragment.A1B();
        }
    }

    public static /* synthetic */ void A1V(C130785zy r1, AbstractActivityC123635nW r2) {
        Object obj;
        if (r1.A06() && (obj = r1.A02) != null) {
            super.A2m(new AnonymousClass68J((C37891nB) obj));
        }
    }

    public static /* synthetic */ void A1W(AbstractActivityC123635nW r1) {
        if (C117305Zk.A1T(r1)) {
            r1.finish();
            return;
        }
        r1.A0F = false;
        super.onBackPressed();
    }

    @Override // X.AbstractActivityC119645em
    public AnonymousClass5TZ A2e() {
        return new AnonymousClass5TZ() { // from class: X.673
            @Override // X.AnonymousClass5TZ
            public final AbstractC17450qp AAO() {
                AbstractActivityC123635nW r1 = AbstractActivityC123635nW.this;
                C126085sJ r3 = new C126085sJ(r1);
                return new AnonymousClass66W(AbstractActivityC121705jc.A0Z(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001c: RETURN  
                      (wrap: X.66W : 0x0019: CONSTRUCTOR  (r0v2 X.66W A[REMOVE]) = 
                      (wrap: X.0qp : 0x0013: INVOKE  (r1v1 X.0qp A[REMOVE]) = 
                      (wrap: X.5jc : 0x0011: IGET  (r0v1 X.5jc A[REMOVE]) = 
                      (wrap: X.672 : 0x000e: CONSTRUCTOR  (r0v0 X.672 A[REMOVE]) = (r1v0 'r1' X.5nW) call: X.672.<init>(X.5jc):void type: CONSTRUCTOR)
                     X.672.A00 X.5jc)
                     type: STATIC call: X.5jc.A0Z(X.5jc):X.0qp)
                      (wrap: X.5qI : 0x0009: CONSTRUCTOR  (r2v0 X.5qI A[REMOVE]) =  call: X.5qI.<init>():void type: CONSTRUCTOR)
                      (r3v0 'r3' X.5sJ)
                     call: X.66W.<init>(X.0qp, X.5qI, X.5sJ):void type: CONSTRUCTOR)
                     in method: X.673.AAO():X.0qp, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0019: CONSTRUCTOR  (r0v2 X.66W A[REMOVE]) = 
                      (wrap: X.0qp : 0x0013: INVOKE  (r1v1 X.0qp A[REMOVE]) = 
                      (wrap: X.5jc : 0x0011: IGET  (r0v1 X.5jc A[REMOVE]) = 
                      (wrap: X.672 : 0x000e: CONSTRUCTOR  (r0v0 X.672 A[REMOVE]) = (r1v0 'r1' X.5nW) call: X.672.<init>(X.5jc):void type: CONSTRUCTOR)
                     X.672.A00 X.5jc)
                     type: STATIC call: X.5jc.A0Z(X.5jc):X.0qp)
                      (wrap: X.5qI : 0x0009: CONSTRUCTOR  (r2v0 X.5qI A[REMOVE]) =  call: X.5qI.<init>():void type: CONSTRUCTOR)
                      (r3v0 'r3' X.5sJ)
                     call: X.66W.<init>(X.0qp, X.5qI, X.5sJ):void type: CONSTRUCTOR in method: X.673.AAO():X.0qp, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:340)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0013: INVOKE  (r1v1 X.0qp A[REMOVE]) = 
                      (wrap: X.5jc : 0x0011: IGET  (r0v1 X.5jc A[REMOVE]) = 
                      (wrap: X.672 : 0x000e: CONSTRUCTOR  (r0v0 X.672 A[REMOVE]) = (r1v0 'r1' X.5nW) call: X.672.<init>(X.5jc):void type: CONSTRUCTOR)
                     X.672.A00 X.5jc)
                     type: STATIC call: X.5jc.A0Z(X.5jc):X.0qp in method: X.673.AAO():X.0qp, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 19 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0011: IGET  (r0v1 X.5jc A[REMOVE]) = 
                      (wrap: X.672 : 0x000e: CONSTRUCTOR  (r0v0 X.672 A[REMOVE]) = (r1v0 'r1' X.5nW) call: X.672.<init>(X.5jc):void type: CONSTRUCTOR)
                     X.672.A00 X.5jc in method: X.673.AAO():X.0qp, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 25 more
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000e: CONSTRUCTOR  (r0v0 X.672 A[REMOVE]) = (r1v0 'r1' X.5nW) call: X.672.<init>(X.5jc):void type: CONSTRUCTOR in method: X.673.AAO():X.0qp, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.addArgDot(InsnGen.java:93)
                    	at jadx.core.codegen.InsnGen.instanceField(InsnGen.java:195)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:452)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 31 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.672, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 37 more
                    */
                /*
                    this = this;
                    X.5nW r1 = X.AbstractActivityC123635nW.this
                    X.5sJ r3 = new X.5sJ
                    r3.<init>(r1)
                    X.5qI r2 = new X.5qI
                    r2.<init>()
                    X.672 r0 = new X.672
                    r0.<init>(r1)
                    X.5jc r0 = r0.A00
                    X.0qp r1 = X.AbstractActivityC121705jc.A0Z(r0)
                    X.66W r0 = new X.66W
                    r0.<init>(r1, r2, r3)
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass673.AAO():X.0qp");
            }
        };
    }

    public void A2n() {
        AnonymousClass61D A03 = AbstractActivityC119225dN.A03(this);
        IDxAListenerShape19S0100000_3_I1 iDxAListenerShape19S0100000_3_I1 = new IDxAListenerShape19S0100000_3_I1(this, 17);
        C130155yt.A00(new IDxAListenerShape19S0100000_3_I1(iDxAListenerShape19S0100000_3_I1, 3), A03.A03, C117295Zj.A0F(AnonymousClass61S.A00("action", "novi-get-funding-source-view-config"), new AnonymousClass61S[1], 0));
    }

    public void A2o() {
        C64873Hg r4 = ((AbstractActivityC119645em) this).A09;
        String A02 = r4.A02("novi_event_logging_surface");
        if (A02 != null) {
            AnonymousClass610 r2 = new AnonymousClass610("EXIT_X_CLICK", A02, "ARROW");
            String A022 = r4.A02("novi_event_logging_app_flow_type");
            if (A022 != null) {
                r2.A00.A0F = A022;
            }
            String A023 = r4.A02("novi_event_logging_sub_surface");
            if (A023 != null) {
                r2.A00.A0i = A023;
            }
            this.A07.A05(r2.A00);
        }
        if (this.A0F) {
            A2p();
        } else {
            super.onBackPressed();
        }
    }

    public final void A2p() {
        boolean z = this.A0G;
        int i = R.string.novi_onboarding_progress_lost_dialog_message;
        if (z) {
            i = R.string.novi_onboarding_progress_preserved_dialog_message;
        }
        new AlertDialog.Builder(this).setMessage(getString(i)).setPositiveButton(getString(R.string.novi_dialog_exit), new IDxCListenerShape10S0100000_3_I1(this, 84)).setNegativeButton(getString(R.string.cancel), (DialogInterface.OnClickListener) null).show();
    }

    public void A2q(BottomSheetDialogFragment bottomSheetDialogFragment, AnonymousClass3FE r11, String str, Map map) {
        this.A0D = str;
        this.A0H.put("register_signing_key_callback", r11);
        this.A0E = map;
        KeyPair A04 = this.A06.A02.A04("alias-signing-key.data-fetch", true);
        AnonymousClass009.A05(A04);
        A2s(new AbstractC136196Lo(bottomSheetDialogFragment, r11, this, map) { // from class: X.6A6
            public final /* synthetic */ BottomSheetDialogFragment A00;
            public final /* synthetic */ AnonymousClass3FE A01;
            public final /* synthetic */ AbstractActivityC123635nW A02;
            public final /* synthetic */ Map A03;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A03 = r4;
                this.A00 = r1;
            }

            @Override // X.AbstractC136196Lo
            public final void AV8(C130785zy r9) {
                AbstractActivityC123635nW r5 = this.A02;
                AnonymousClass3FE r6 = this.A01;
                Map map2 = this.A03;
                BottomSheetDialogFragment bottomSheetDialogFragment2 = this.A00;
                if (r9.A06()) {
                    try {
                        C12970iu.A1C(C130105yo.A01(r5.A08.A0F), "trusted_device_expiry_timestamp_sec", ((AnonymousClass1V8) r9.A02).A0F("signing_key_expiration").A07("expiry_timestamp_sec"));
                        r5.A08.A07();
                        r5.A2n();
                        r5.A0B.A00();
                    } catch (AnonymousClass1V9 unused) {
                        Log.e("Pay: NoviPayBloksActivity/registerDataFetchSigningKey: corrupted stream");
                    }
                    if (r6 != null) {
                        if (map2 != null) {
                            r6.A02("on_success", map2);
                        } else {
                            r6.A00("on_success");
                        }
                    }
                } else {
                    C452120p r3 = r9.A00;
                    if (r3 != null) {
                        r5.A05.A02(r3, 
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0058: INVOKE  
                              (wrap: X.5yQ : 0x0050: IGET  (r2v1 X.5yQ A[REMOVE]) = (r5v0 'r5' X.5nW) X.5nW.A05 X.5yQ)
                              (r3v0 'r3' X.20p)
                              (wrap: X.6HW : 0x0054: CONSTRUCTOR  (r1v4 X.6HW A[REMOVE]) = (r6v0 'r6' X.3FE) call: X.6HW.<init>(X.3FE):void type: CONSTRUCTOR)
                              (null java.lang.Runnable)
                             type: VIRTUAL call: X.5yQ.A02(X.20p, java.lang.Runnable, java.lang.Runnable):void in method: X.6A6.AV8(X.5zy):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0054: CONSTRUCTOR  (r1v4 X.6HW A[REMOVE]) = (r6v0 'r6' X.3FE) call: X.6HW.<init>(X.3FE):void type: CONSTRUCTOR in method: X.6A6.AV8(X.5zy):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 27 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6HW, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 33 more
                            */
                        /*
                            this = this;
                            X.5nW r5 = r8.A02
                            X.3FE r6 = r8.A01
                            java.util.Map r7 = r8.A03
                            com.google.android.material.bottomsheet.BottomSheetDialogFragment r4 = r8.A00
                            boolean r0 = r9.A06()
                            if (r0 == 0) goto L_0x004c
                            java.lang.Object r1 = r9.A02     // Catch: 1V9 -> 0x0039
                            X.1V8 r1 = (X.AnonymousClass1V8) r1     // Catch: 1V9 -> 0x0039
                            java.lang.String r0 = "signing_key_expiration"
                            X.1V8 r1 = r1.A0F(r0)     // Catch: 1V9 -> 0x0039
                            java.lang.String r0 = "expiry_timestamp_sec"
                            long r2 = r1.A07(r0)     // Catch: 1V9 -> 0x0039
                            X.61F r0 = r5.A08     // Catch: 1V9 -> 0x0039
                            X.5yo r0 = r0.A0F     // Catch: 1V9 -> 0x0039
                            android.content.SharedPreferences$Editor r1 = X.C130105yo.A01(r0)     // Catch: 1V9 -> 0x0039
                            java.lang.String r0 = "trusted_device_expiry_timestamp_sec"
                            X.C12970iu.A1C(r1, r0, r2)     // Catch: 1V9 -> 0x0039
                            X.61F r0 = r5.A08     // Catch: 1V9 -> 0x0039
                            r0.A07()     // Catch: 1V9 -> 0x0039
                            r5.A2n()     // Catch: 1V9 -> 0x0039
                            X.5xO r0 = r5.A0B     // Catch: 1V9 -> 0x0039
                            r0.A00()     // Catch: 1V9 -> 0x0039
                            goto L_0x003e
                        L_0x0039:
                            java.lang.String r0 = "Pay: NoviPayBloksActivity/registerDataFetchSigningKey: corrupted stream"
                            com.whatsapp.util.Log.e(r0)
                        L_0x003e:
                            if (r6 == 0) goto L_0x005b
                            java.lang.String r0 = "on_success"
                            if (r7 == 0) goto L_0x0048
                            r6.A02(r0, r7)
                            goto L_0x005b
                        L_0x0048:
                            r6.A00(r0)
                            goto L_0x005b
                        L_0x004c:
                            X.20p r3 = r9.A00
                            if (r3 == 0) goto L_0x005b
                            X.5yQ r2 = r5.A05
                            X.6HW r1 = new X.6HW
                            r1.<init>(r6)
                            r0 = 0
                            r2.A02(r3, r1, r0)
                        L_0x005b:
                            X.AbstractActivityC123635nW.A1U(r4)
                            r5.AaN()
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6A6.AV8(X.5zy):void");
                    }
                }, 18, str, "DATA_FETCH", A04);
            }

            public void A2r(AnonymousClass3FE r6) {
                if (this.A08.A0E()) {
                    C117315Zl.A0T(r6);
                    return;
                }
                this.A04.A0B(C117305Zk.A09(r6, this, 54), AnonymousClass61S.A01("novi-notify-welcome-screen-shown"), "set", 5);
            }

            public void A2s(AbstractC136196Lo r13, Integer num, String str, String str2, KeyPair keyPair) {
                String str3 = AnonymousClass600.A03;
                String A05 = this.A04.A05();
                long A02 = C117315Zl.A02(this);
                String encodeToString = Base64.encodeToString(AnonymousClass61L.A03(keyPair.getPublic().getEncoded()), 2);
                String encodeToString2 = Base64.encodeToString(keyPair.getPublic().getEncoded(), 2);
                JSONObject A0a = C117295Zj.A0a();
                try {
                    C117295Zj.A1L(str3, A05, A0a, A02);
                    A0a.put("signing_key_registration", C117295Zj.A0a().put("key_id", encodeToString).put("key_type", "ECDSA_SECP256R1").put("pub_key_b64", encodeToString2));
                    A0a.put("account_id", str);
                } catch (JSONException unused) {
                    Log.e("PAY: IntentPayloadHelper/getSigningKeyRegistrationIntentPayload/toJson can't construct json");
                }
                C129585xx r3 = new C129585xx(this.A06, "REGISTER_DATA_FETCH_KEY", A0a);
                C1310460z A01 = AnonymousClass61S.A01("novi-register-signing-key");
                AnonymousClass61S[] r2 = new AnonymousClass61S[4];
                AnonymousClass61S.A04("key_id", encodeToString, r2);
                AnonymousClass61S.A05("key_type", "ECDSA_SECP256R1", r2, 1);
                AnonymousClass61S.A05("key", encodeToString2, r2, 2);
                C117305Zk.A1K(A01, "signing_key_request", C12960it.A0m(AnonymousClass61S.A00("scope", str2), r2, 3));
                String A012 = r3.A01(this.A06.A02());
                AnonymousClass009.A05(A012);
                C117305Zk.A1K(A01, "register_signing_key_signed_intent", AnonymousClass61S.A02("value", A012));
                C130155yt r6 = this.A04;
                AnonymousClass016 A0T = C12980iv.A0T();
                r6.A09(new C1331369p(A0T), A01, num, "set", 5);
                C117295Zj.A0s(this, A0T, r13, 160);
            }

            /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
                if ("1".equals(r1) == false) goto L_0x003c;
             */
            @Override // X.AbstractActivityC121705jc, X.AbstractC136496Mt
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void AZC(X.AnonymousClass3FE r6, java.lang.String r7, java.util.Map r8) {
                /*
                    r5 = this;
                    X.AbstractActivityC119225dN.A0M(r6, r7)
                    java.lang.String r0 = "set_navigation_icon"
                    boolean r0 = r7.equals(r0)
                    if (r0 != 0) goto L_0x000f
                    super.AZC(r6, r7, r8)
                    return
                L_0x000f:
                    java.lang.String r0 = "navigation_icon"
                    java.lang.Object r1 = r8.get(r0)
                    java.lang.String r0 = "close"
                    boolean r4 = r0.equals(r1)
                    java.lang.String r0 = "icon_color_filter"
                    java.lang.Object r1 = r8.get(r0)
                    java.lang.String r0 = "white"
                    boolean r3 = r0.equals(r1)
                    java.lang.String r0 = "show_onboarding_close_dialog"
                    java.lang.String r1 = X.C12970iu.A0t(r0, r8)
                    boolean r0 = android.text.TextUtils.isEmpty(r1)
                    java.lang.String r2 = "1"
                    if (r0 != 0) goto L_0x003c
                    boolean r1 = r2.equals(r1)
                    r0 = 1
                    if (r1 != 0) goto L_0x003d
                L_0x003c:
                    r0 = 0
                L_0x003d:
                    r5.A0F = r0
                    java.lang.String r0 = "is_onboarding_progress_preserved"
                    boolean r0 = X.C117305Zk.A1W(r0, r2, r8)
                    r5.A0G = r0
                    X.64C r0 = new X.64C
                    r0.<init>(r4)
                    r5.A2k(r0, r4, r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC123635nW.AZC(X.3FE, java.lang.String, java.util.Map):void");
            }

            @Override // X.AbstractActivityC121705jc, X.AbstractC136496Mt
            public String AZE(String str, Map map) {
                String str2 = (String) map.remove("case");
                if (TextUtils.isEmpty(str2)) {
                    return "";
                }
                if (!str2.equals("launch_novi_help")) {
                    map.put("case", str2);
                    return super.AZE(str, map);
                }
                startActivity(new Intent("android.intent.action.VIEW", C117295Zj.A06(C1310561a.A02(this.A04.A06), C117295Zj.A0X("novi_help_uri", map))));
                return "on_success";
            }

            @Override // X.AbstractActivityC121705jc, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
            public void onActivityResult(int i, int i2, Intent intent) {
                if (i == 30 && i2 == -1) {
                    C12960it.A0t(C130105yo.A01(this.A0A), "wavi_seen_camera_permission_education", true);
                }
                super.onActivityResult(i, i2, intent);
            }

            @Override // X.AbstractActivityC119645em, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
            public void onBackPressed() {
                C64873Hg r7 = ((AbstractActivityC119645em) this).A09;
                String A02 = r7.A02("novi_event_logging_surface");
                String A022 = r7.A02("logging_disabled");
                String A023 = r7.A02("step_up_action_id");
                String A024 = r7.A02("step_up_entry_point");
                String A025 = r7.A02("step_up_type");
                if (!C29941Vi.A00(A022, "true") && A02 != null) {
                    String A026 = r7.A02("novi_event_logging_app_flow_type");
                    AnonymousClass610 r2 = new AnonymousClass610("BACK_CLICK", A02, "SCREEN");
                    if (A026 != null) {
                        r2.A00.A0F = A026;
                    }
                    String A027 = r7.A02("novi_event_logging_sub_surface");
                    if (A027 != null) {
                        r2.A00.A0i = A027;
                    }
                    if (!TextUtils.isEmpty(A023)) {
                        r2.A00.A0E = A023;
                    }
                    if (!TextUtils.isEmpty(A024)) {
                        r2.A00.A0f = A024;
                    }
                    if (!TextUtils.isEmpty(A025)) {
                        r2.A00.A0g = A025;
                    }
                    this.A07.A05(r2.A00);
                }
                if (this.A0F) {
                    A2p();
                } else {
                    super.onBackPressed();
                }
            }

            @Override // X.AbstractActivityC121705jc, X.AbstractActivityC119645em, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            public void onCreate(Bundle bundle) {
                super.onCreate(bundle);
                this.A05 = new C129865yQ(((ActivityC13790kL) this).A00, this, this.A03);
            }
        }
