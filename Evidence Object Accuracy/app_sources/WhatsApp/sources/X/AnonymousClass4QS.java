package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.4QS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QS {
    public long A00;
    public long A01;
    public List A02;

    public AnonymousClass4QS(AnonymousClass4BL[] r2, long j, long j2) {
        this.A00 = j;
        this.A01 = j2;
        this.A02 = Arrays.asList(r2);
    }
}
