package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: X.17u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C250217u {
    public final C15650ng A00;
    public final C18460sU A01;
    public final C16490p7 A02;
    public final C233511i A03;

    public C250217u(C15650ng r1, C18460sU r2, C16490p7 r3, C233511i r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    public static long A00(long j, long j2) {
        if (j2 > 0 && j / 10 > j2) {
            StringBuilder sb = new StringBuilder("MessageRangeUtil/convertTheTimestampIfTooLarge DeleteMessageForMeMutation message timestamp is too large, timestampToConvert=");
            sb.append(j);
            sb.append("; validTimestamp=");
            sb.append(j2);
            Log.w(sb.toString());
            while (j / 10 > j2) {
                j /= 1000;
            }
        }
        return j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0031, code lost:
        if (r1 > 0) goto L_0x0033;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long A01(android.database.Cursor r6, boolean r7) {
        /*
            java.lang.String r5 = "timestamp"
            if (r7 == 0) goto L_0x0034
            java.lang.String r0 = "status"
            int r0 = r6.getColumnIndexOrThrow(r0)
            int r0 = r6.getInt(r0)
            r3 = 0
            if (r0 != 0) goto L_0x0015
            return r3
        L_0x0015:
            java.lang.String r0 = "receipt_server_timestamp"
            int r0 = r6.getColumnIndexOrThrow(r0)
            long r1 = r6.getLong(r0)
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x0033
            java.lang.String r0 = "receipt_device_timestamp"
            int r0 = r6.getColumnIndex(r0)
            if (r0 < 0) goto L_0x0034
            long r1 = r6.getLong(r0)
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0034
        L_0x0033:
            return r1
        L_0x0034:
            int r0 = r6.getColumnIndexOrThrow(r5)
            long r0 = r6.getLong(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C250217u.A01(android.database.Cursor, boolean):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0093 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C461824w A02(android.database.Cursor r13, X.C16310on r14, X.AbstractC14640lm r15) {
        /*
            r12 = this;
            java.lang.String r0 = "key_id"
            int r0 = r13.getColumnIndexOrThrow(r0)
            java.lang.String r8 = r13.getString(r0)
            java.lang.String r0 = "from_me"
            int r0 = r13.getColumnIndexOrThrow(r0)
            int r1 = r13.getInt(r0)
            r0 = 1
            r11 = 0
            if (r1 != r0) goto L_0x0019
            r11 = 1
        L_0x0019:
            long r9 = A01(r13, r11)
            r6 = r15
            boolean r0 = X.C15380n4.A0J(r15)
            r5 = 0
            if (r0 == 0) goto L_0x0094
            if (r11 != 0) goto L_0x0094
            X.0p7 r0 = r12.A02
            r0.A04()
            X.1To r1 = r0.A05
            X.0op r0 = r14.A03
            java.lang.Boolean r0 = r1.A06(r0)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0070
            java.lang.String r0 = "sender_jid_row_id"
            int r0 = r13.getColumnIndexOrThrow(r0)
            long r0 = r13.getLong(r0)
            X.0sU r2 = r12.A01
            com.whatsapp.jid.Jid r4 = r2.A03(r0)
            com.whatsapp.jid.UserJid r7 = com.whatsapp.jid.UserJid.of(r4)
            if (r7 != 0) goto L_0x0095
            com.whatsapp.jid.DeviceJid r2 = com.whatsapp.jid.DeviceJid.of(r4)
            if (r2 != 0) goto L_0x008d
            java.lang.String r3 = "MessageRangeUtil/getSenderJid null or not UserJid/DeviceJid when db migration is completed; senderJidRowId="
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r3)
            r2.append(r0)
            java.lang.String r0 = "; jid="
            r2.append(r0)
            r2.append(r4)
            java.lang.String r0 = r2.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x0070:
            java.lang.String r0 = "sender_jid_raw_string"
            int r0 = r13.getColumnIndexOrThrow(r0)
            java.lang.String r0 = r13.getString(r0)
            com.whatsapp.jid.UserJid r7 = com.whatsapp.jid.UserJid.getNullable(r0)
            if (r7 != 0) goto L_0x0095
            com.whatsapp.jid.DeviceJid r2 = com.whatsapp.jid.DeviceJid.getNullable(r0)
            if (r2 != 0) goto L_0x008d
            java.lang.String r0 = "MessageRangeUtil/getSenderJid null or not UserJid/DeviceJid before db migration"
            com.whatsapp.util.Log.e(r0)
            return r5
        L_0x008d:
            com.whatsapp.jid.UserJid r7 = r2.getUserJid()
            if (r7 != 0) goto L_0x0095
            return r5
        L_0x0094:
            r7 = r5
        L_0x0095:
            X.24w r5 = new X.24w
            r5.<init>(r6, r7, r8, r9, r11)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C250217u.A02(android.database.Cursor, X.0on, X.0lm):X.24w");
    }

    public C34361g1 A03(AbstractC14640lm r22) {
        UserJid userJid;
        HashSet hashSet = new HashSet(Arrays.asList("clearChat", "deleteChat", "deleteMessageForMe"));
        ArrayList arrayList = new ArrayList();
        C233511i r1 = this.A03;
        arrayList.addAll(r1.A08(r22, hashSet, false));
        arrayList.addAll(r1.A08(r22, hashSet, true));
        C34361g1 A04 = A04(r22, true);
        HashSet hashSet2 = new HashSet();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            AnonymousClass1JQ r6 = (AnonymousClass1JQ) it.next();
            if (r6 instanceof AbstractC34391g4) {
                A04 = C34361g1.A01(A04, ((AbstractC34391g4) r6).AEJ());
            } else if (r6 instanceof C34411g6) {
                C34411g6 r62 = (C34411g6) r6;
                long j = r62.A00;
                if (j > 0) {
                    long A00 = A00(j, r62.A04);
                    AnonymousClass1IS r0 = r62.A02;
                    boolean z = r0.A02;
                    String str = r0.A01;
                    AbstractC14640lm r9 = r0.A00;
                    AnonymousClass009.A05(r9);
                    AbstractC14640lm r02 = r62.A01;
                    if (r02 != null) {
                        userJid = UserJid.getNullable(r02.getRawString());
                    } else {
                        userJid = null;
                    }
                    hashSet2.add(new C461824w(r9, userJid, str, A00, z));
                }
            } else {
                StringBuilder sb = new StringBuilder("MessageRangeUtil/createActiveRange unhandledMutation:");
                sb.append(r6.A03());
                Log.e(sb.toString());
            }
        }
        return C34361g1.A01(A04, new C34361g1(hashSet2, Collections.emptySet(), 0, 0));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0096, code lost:
        if (r3 != null) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0055, code lost:
        if (r2 != null) goto L_0x0057;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C34361g1 A04(X.AbstractC14640lm r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 337
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C250217u.A04(X.0lm, boolean):X.1g1");
    }
}
