package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.0VM  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0VM implements Parcelable.ClassLoaderCreator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass0E5(parcel, null);
    }

    @Override // android.os.Parcelable.ClassLoaderCreator
    public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
        return new AnonymousClass0E5(parcel, classLoader);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass0E5[i];
    }
}
