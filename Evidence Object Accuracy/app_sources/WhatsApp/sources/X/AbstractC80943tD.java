package X;

import com.google.common.base.Strings;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;

/* renamed from: X.3tD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC80943tD<E> extends AbstractC113525Hx<E> implements Serializable {
    public static final long serialVersionUID = 0;
    public transient C95614e4 backingMap = newBackingMap(3);
    public transient long size;

    public abstract C95614e4 newBackingMap(int i);

    public AbstractC80943tD(int i) {
    }

    @Override // X.AbstractC113525Hx, X.AnonymousClass5Z2
    public final int add(Object obj, int i) {
        if (i == 0) {
            return count(obj);
        }
        boolean z = true;
        if (C12960it.A1U(i)) {
            int indexOf = this.backingMap.indexOf(obj);
            C95614e4 r0 = this.backingMap;
            if (indexOf == -1) {
                r0.put(obj, i);
                this.size += (long) i;
                return 0;
            }
            int value = r0.getValue(indexOf);
            long j = (long) i;
            long j2 = ((long) value) + j;
            if (j2 > 2147483647L) {
                z = false;
            }
            if (z) {
                this.backingMap.setValue(indexOf, (int) j2);
                this.size += j;
                return value;
            }
            throw C12970iu.A0f(Strings.A00("too many occurrences: %s", Long.valueOf(j2)));
        }
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, i, 0);
        throw C12970iu.A0f(Strings.A00("occurrences cannot be negative: %s", objArr));
    }

    public void addTo(AnonymousClass5Z2 r4) {
        C95614e4 r0 = this.backingMap;
        int firstIndex = r0.firstIndex();
        while (firstIndex >= 0) {
            r4.add(r0.getKey(firstIndex), this.backingMap.getValue(firstIndex));
            r0 = this.backingMap;
            firstIndex = r0.nextIndex(firstIndex);
        }
    }

    @Override // X.AbstractC113525Hx, java.util.AbstractCollection, java.util.Collection
    public final void clear() {
        this.backingMap.clear();
        this.size = 0;
    }

    @Override // X.AnonymousClass5Z2
    public final int count(Object obj) {
        return this.backingMap.get(obj);
    }

    @Override // X.AbstractC113525Hx
    public final int distinctElements() {
        return this.backingMap.size();
    }

    @Override // X.AbstractC113525Hx
    public final Iterator elementIterator() {
        return new C80873t6(this);
    }

    @Override // X.AbstractC113525Hx
    public final Iterator entryIterator() {
        return new C80883t7(this);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public final Iterator iterator() {
        return C95544dv.iteratorImpl(this);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readCount = C95324dW.readCount(objectInputStream);
        this.backingMap = newBackingMap(3);
        C95324dW.populateMultiset(this, objectInputStream, readCount);
    }

    @Override // X.AbstractC113525Hx, X.AnonymousClass5Z2
    public final int remove(Object obj, int i) {
        if (i == 0) {
            return count(obj);
        }
        int i2 = 0;
        if (C12960it.A1U(i)) {
            int indexOf = this.backingMap.indexOf(obj);
            if (indexOf != -1) {
                i2 = this.backingMap.getValue(indexOf);
                if (i2 > i) {
                    this.backingMap.setValue(indexOf, i2 - i);
                } else {
                    this.backingMap.removeEntry(indexOf);
                    i = i2;
                }
                this.size -= (long) i;
            }
            return i2;
        }
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, i, 0);
        throw C12970iu.A0f(Strings.A00("occurrences cannot be negative: %s", objArr));
    }

    @Override // X.AbstractC113525Hx, X.AnonymousClass5Z2
    public final boolean setCount(Object obj, int i, int i2) {
        long j;
        long j2;
        C28251Mi.checkNonnegative(i, "oldCount");
        C28251Mi.checkNonnegative(i2, "newCount");
        int indexOf = this.backingMap.indexOf(obj);
        if (indexOf == -1) {
            if (i == 0) {
                if (i2 > 0) {
                    this.backingMap.put(obj, i2);
                    j = this.size;
                    j2 = j + ((long) i2);
                }
                return true;
            }
            return false;
        }
        if (this.backingMap.getValue(indexOf) == i) {
            C95614e4 r0 = this.backingMap;
            if (i2 == 0) {
                r0.removeEntry(indexOf);
                j2 = this.size - ((long) i);
            } else {
                r0.setValue(indexOf, i2);
                j = this.size;
                i2 -= i;
                j2 = j + ((long) i2);
            }
        }
        return false;
        this.size = j2;
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, X.AnonymousClass5Z2
    public final int size() {
        long j = this.size;
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        C95324dW.writeMultiset(this, objectOutputStream);
    }
}
