package X;

/* renamed from: X.5FQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FQ implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FQ(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r7) {
        short[] sArr = (short[]) obj;
        appendable.append('[');
        boolean z = false;
        for (short s : sArr) {
            z = C72453ed.A1U(appendable, z);
            appendable.append(Short.toString(s));
        }
        appendable.append(']');
    }
}
