package X;

import java.util.List;

/* renamed from: X.6JW  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6JW implements Runnable {
    public final /* synthetic */ AbstractActivityC121655j9 A00;
    public final /* synthetic */ Runnable A01;
    public final /* synthetic */ boolean A02;

    public /* synthetic */ AnonymousClass6JW(AbstractActivityC121655j9 r1, Runnable runnable, boolean z) {
        this.A00 = r1;
        this.A02 = z;
        this.A01 = runnable;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AbstractActivityC121655j9 r4 = this.A00;
        boolean z = this.A02;
        Runnable runnable = this.A01;
        List A0B = r4.A03.A0B();
        r4.A0D = A0B;
        r4.A02 = C241414j.A01(A0B);
        ((ActivityC13810kN) r4).A05.A0H(new Runnable(runnable, z) { // from class: X.6JX
            public final /* synthetic */ Runnable A01;
            public final /* synthetic */ boolean A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AbstractActivityC121655j9 r2 = AbstractActivityC121655j9.this;
                boolean z2 = this.A02;
                Runnable runnable2 = this.A01;
                if (z2) {
                    r2.AaN();
                }
                runnable2.run();
            }
        });
    }
}
