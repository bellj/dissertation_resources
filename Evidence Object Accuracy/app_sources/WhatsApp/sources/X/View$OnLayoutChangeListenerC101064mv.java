package X;

import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import com.whatsapp.search.SearchFragment;

/* renamed from: X.4mv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnLayoutChangeListenerC101064mv implements View.OnLayoutChangeListener {
    public final /* synthetic */ SearchFragment A00;

    public View$OnLayoutChangeListenerC101064mv(SearchFragment searchFragment) {
        this.A00 = searchFragment;
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        view.removeOnLayoutChangeListener(this);
        SearchFragment searchFragment = this.A00;
        searchFragment.A1D(new RunnableBRunnable0Shape16S0100000_I1_2(searchFragment, 8), i, i2, i3, i4, true);
    }
}
