package X;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2ho  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55112ho extends AnonymousClass03U {
    public ImageView A00;
    public ImageView A01;
    public TextEmojiLabel A02;
    public C28801Pb A03;
    public final Context A04;
    public final AnonymousClass12P A05;
    public final AnonymousClass2TT A06;
    public final C22640zP A07;
    public final C15550nR A08;
    public final AnonymousClass1J1 A09;
    public final C19990v2 A0A;
    public final C15600nX A0B;
    public final AnonymousClass11F A0C;
    public final AbstractC14440lR A0D;

    public C55112ho(Context context, View view, AnonymousClass12P r5, AnonymousClass2TT r6, C22640zP r7, C15550nR r8, C15610nY r9, AnonymousClass1J1 r10, C19990v2 r11, C15600nX r12, AnonymousClass11F r13, AnonymousClass12F r14, AbstractC14440lR r15) {
        super(view);
        this.A04 = context;
        this.A0D = r15;
        this.A0A = r11;
        this.A05 = r5;
        this.A08 = r8;
        this.A06 = r6;
        this.A09 = r10;
        this.A0C = r13;
        this.A07 = r7;
        this.A0B = r12;
        this.A03 = new C28801Pb(view, r9, r14, (int) R.id.name);
        this.A02 = C12970iu.A0T(view, R.id.status);
        this.A00 = C12970iu.A0K(view, R.id.avatar);
        this.A01 = C12970iu.A0K(view, R.id.group_chat_info_pin_indicator);
        AnonymousClass028.A0a(this.A00, 2);
        view.setBackgroundResource(R.drawable.selector_orange_gradient);
        view.setFocusable(true);
        view.setClickable(true);
    }
}
