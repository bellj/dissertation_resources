package X;

/* renamed from: X.52J  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass52J implements AnonymousClass5TA {
    public C83153wm A00;
    public final C94394bk A01;

    public /* synthetic */ AnonymousClass52J(C94394bk r1, AnonymousClass4YR r2) {
        this.A01 = r1;
        this.A00 = (C83153wm) r2;
    }

    @Override // X.AnonymousClass5TA
    public boolean ALL(Object obj) {
        C83153wm r3 = this.A00;
        C94394bk r2 = this.A01;
        return r3.A08(r2.A01, r2, obj, r2.A04);
    }
}
