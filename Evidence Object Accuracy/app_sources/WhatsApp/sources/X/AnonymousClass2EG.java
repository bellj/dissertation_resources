package X;

import java.util.List;

/* renamed from: X.2EG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EG {
    public final long A00;
    public final AnonymousClass2EC A01;
    public final String A02;
    public final List A03;

    public AnonymousClass2EG(AnonymousClass2EC r1, String str, List list, long j) {
        this.A02 = str;
        this.A03 = list;
        this.A00 = j;
        this.A01 = r1;
    }
}
