package X;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.10t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C232010t extends AbstractC16280ok {
    public final C16590pI A00;
    public final C231410n A01;
    public final AnonymousClass1VS A02 = new AnonymousClass1VS();

    public C232010t(AbstractC15710nm r9, C16590pI r10, C231410n r11) {
        super(r10.A00, r9, "wa.db", null, 67, true);
        this.A01 = r11;
        this.A00 = r10;
    }

    public static String A00(SQLiteDatabase sQLiteDatabase, String str) {
        String str2 = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("select sql from sqlite_master where type='table' and name='");
            sb.append(str);
            sb.append("';");
            Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
            if (rawQuery != null) {
                if (rawQuery.moveToNext()) {
                    str2 = rawQuery.getString(0);
                }
                rawQuery.close();
                return str2;
            }
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("cannot get schema for ");
            sb2.append(str);
            Log.e(sb2.toString(), e);
        }
        return str2;
    }

    public static final void A01(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS subgroup_info");
        sQLiteDatabase.execSQL("CREATE TABLE subgroup_info (subgroup_raw_jid TEXT NOT NULL, subject TEXT NOT NULL, subject_ts INTEGER, group_type INTEGER NOT NULL DEFAULT 2)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS subgroup_raw_jid_index ON subgroup_info (subgroup_raw_jid)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS group_relationship");
        sQLiteDatabase.execSQL("CREATE TABLE group_relationship (parent_raw_jid TEXT NOT NULL, subgroup_raw_id TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS group_relationship_parent_raw_jid_index ON group_relationship (parent_raw_jid)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS group_relationship_subgroup_raw_jid_index ON group_relationship (subgroup_raw_id)");
    }

    public static void A02(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder();
        sb.append(str3);
        sb.append(" ");
        sb.append(str4);
        if (!str.contains(sb.toString())) {
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("ALTER TABLE ");
                sb2.append(str2);
                sb2.append(" ADD ");
                sb2.append(str3);
                sb2.append(" ");
                sb2.append(str4);
                sQLiteDatabase.execSQL(sb2.toString());
            } catch (SQLiteException e) {
                StringBuilder sb3 = new StringBuilder("cannot add column ");
                sb3.append(str3);
                sb3.append(" ");
                sb3.append(str4);
                sb3.append(" to ");
                sb3.append(str2);
                Log.e(sb3.toString(), e);
            }
        }
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteDatabaseCorruptException e) {
            Log.w("Contacts database is corrupt. Removing...", e);
            A04();
            return AnonymousClass1Tx.A01(super.A00(), this.A01);
        } catch (SQLiteException e2) {
            String obj = e2.toString();
            if (obj.contains("file is encrypted")) {
                Log.w("Contacts database is encrypted. Removing...", e2);
                A04();
                return AnonymousClass1Tx.A01(super.A00(), this.A01);
            } else if (obj.contains("upgrade read-only database")) {
                Log.w("Client actually opened database as read-only and can't upgrade. Switching to writable...", e2);
                return AnonymousClass1Tx.A01(super.A00(), this.A01);
            } else {
                throw e2;
            }
        } catch (StackOverflowError e3) {
            Log.w("StackOverflowError during db init check");
            for (StackTraceElement stackTraceElement : e3.getStackTrace()) {
                if (stackTraceElement.getMethodName().equals("onCorruption")) {
                    Log.w("Contacts database is corrupt. Found via StackOverflowError. Removing...");
                    A04();
                    return AnonymousClass1Tx.A01(super.A00(), this.A01);
                }
            }
            throw e3;
        }
    }

    public void A04() {
        synchronized (this) {
            close();
            Log.i("deleting contact database...");
            Context context = this.A00.A00;
            boolean delete = context.getDatabasePath("wa.db").delete();
            File databasePath = context.getDatabasePath("wa.db");
            String path = databasePath.getPath();
            StringBuilder sb = new StringBuilder();
            sb.append(databasePath.getName());
            sb.append("-journal");
            boolean delete2 = new File(path, sb.toString()).delete();
            File databasePath2 = context.getDatabasePath("wa.db");
            String path2 = databasePath2.getPath();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(databasePath2.getName());
            sb2.append("-wal");
            boolean delete3 = new File(path2, sb2.toString()).delete();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("deleted contact database; databaseDeleted=");
            sb3.append(delete);
            sb3.append("; journalDeleted=");
            sb3.append(delete2);
            sb3.append("; writeAheadLogDeleted=");
            sb3.append(delete3);
            Log.i(sb3.toString());
        }
        for (AnonymousClass1VT r0 : this.A02.A01()) {
            AbstractC21570xd r2 = r0.A00;
            if (r2 instanceof C21580xe) {
                C21580xe r22 = (C21580xe) r2;
                synchronized (r22.A07) {
                    r22.A00 = 0;
                }
            }
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        Log.i("creating contacts database version 67");
        Log.i("creating contacts table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_contacts");
        sQLiteDatabase.execSQL("CREATE TABLE wa_contacts (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, is_whatsapp_user BOOLEAN NOT NULL, status TEXT, status_timestamp INTEGER, number TEXT, raw_contact_id INTEGER, display_name TEXT, phone_type INTEGER, phone_label TEXT, unseen_msg_count INTEGER, photo_ts INTEGER, thumb_ts INTEGER, photo_id_timestamp INTEGER, given_name TEXT, family_name TEXT, wa_name TEXT, sort_name TEXT, nickname TEXT, company TEXT, title TEXT, status_autodownload_disabled INTEGER, keep_timestamp INTEGER, is_spam_reported INTEGER, is_sidelist_synced BOOLEAN DEFAULT 0, is_business_synced BOOLEAN DEFAULT 0, disappearing_mode_duration INTEGER, disappearing_mode_timestamp LONG, history_sync_initial_phash TEXT)");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS is_wa_index ON wa_contacts (is_whatsapp_user);");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS jid_index ON wa_contacts (jid);");
        Log.i("creating system contacts version table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS system_contacts_version_table");
        sQLiteDatabase.execSQL("CREATE TABLE system_contacts_version_table (id INTEGER PRIMARY KEY, version INTEGER)");
        Log.i("creating wa_vnames table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_vnames");
        sQLiteDatabase.execSQL("CREATE TABLE wa_vnames (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, serial INTEGER NOT NULL, issuer TEXT NOT NULL, expires INTEGER, verified_name TEXT NOT NULL, industry TEXT, city TEXT, country TEXT, verified_level INTEGER, identity_unconfirmed_since INTEGER, cert_blob BLOB, host_storage INTEGER DEFAULT 0, actual_actors INTEGER DEFAULT 0, privacy_mode_ts INTEGER DEFAULT 0)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS vname_jid_index ON wa_vnames (jid);");
        Log.i("creating wa_vnames_localized table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_vnames_localized");
        sQLiteDatabase.execSQL("CREATE TABLE wa_vnames_localized (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, lg TEXT NOT NULL, lc TEXT NOT NULL, verified_name TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS vname_localized_index ON wa_vnames_localized (jid, lg, lc);");
        Log.i("creating storage usage table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_contact_storage_usage");
        sQLiteDatabase.execSQL("CREATE TABLE wa_contact_storage_usage (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, conversation_size INTEGER NOT NULL, conversation_message_count INTEGER NOT NULL)");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS wa_contact_storage_usage_index ON wa_contact_storage_usage (jid, conversation_size DESC)");
        Log.i("creating wa_biz_profiles table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS contact_bu_for_business_profiles");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS contact_bd_for_business_profiles");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_biz_profiles");
        sQLiteDatabase.execSQL("CREATE TABLE wa_biz_profiles (_id INTEGER PRIMARY KEY AUTOINCREMENT, jid TEXT NOT NULL, email TEXT, address TEXT, business_description TEXT, latitude REAL, longitude REAL, tag TEXT, vertical TEXT,time_zone TEXT,hours_note TEXT,has_catalog BOOLEAN DEFAULT 0, address_postal_code TEXT, address_city_id TEXT, address_city_name TEXT, commerce_experience TEXT, shop_url TEXT, cart_enabled BOOLEAN DEFAULT 0, commerce_manager_url TEXT, direct_connection_enabled BOOLEAN DEFAULT 0, is_shop_banned BOOLEAN DEFAULT 0, default_postcode TEXT, location_name TEXT, galaxy_business_enabled BOOLEAN DEFAULT 0, cover_photo_url TEXT, cover_photo_id TEXT)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS biz_profile_jid_index ON wa_biz_profiles (jid);");
        sQLiteDatabase.execSQL("CREATE TRIGGER contact_bd_for_business_profiles BEFORE DELETE ON wa_contacts BEGIN DELETE FROM wa_biz_profiles WHERE jid = old.jid; END");
        sQLiteDatabase.execSQL("CREATE TRIGGER contact_bu_for_business_profiles BEFORE UPDATE ON wa_contacts BEGIN UPDATE wa_biz_profiles SET jid = new.jid WHERE jid = old.jid; END");
        Log.i("creating wa_biz_profiles_websites table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS business_profiles_bd_for_websites_trigger");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_biz_profiles_websites");
        sQLiteDatabase.execSQL("CREATE TABLE wa_biz_profiles_websites (_id INTEGER PRIMARY KEY AUTOINCREMENT, wa_biz_profile_id INTEGER NOT NULL, websites TEXT)");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS biz_profile_id_website_index ON wa_biz_profiles_websites(wa_biz_profile_id, websites);");
        sQLiteDatabase.execSQL("CREATE TRIGGER business_profiles_bd_for_websites_trigger BEFORE DELETE ON wa_biz_profiles BEGIN DELETE FROM wa_biz_profiles_websites WHERE wa_biz_profile_id = old._id; END");
        Log.i("creating wa_biz_profiles_hours table for contacts database version 67");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS business_profiles_bd_for_hours_trigger");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_biz_profiles_hours");
        sQLiteDatabase.execSQL("CREATE TABLE wa_biz_profiles_hours (_id INTEGER PRIMARY KEY AUTOINCREMENT, wa_biz_profile_id INTEGER NOT NULL, day_of_week TEXT,mode TEXT,open_time INTEGER,close_time INTEGER)");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS biz_profile_id_hours_index ON wa_biz_profiles_hours(wa_biz_profile_id);");
        sQLiteDatabase.execSQL("CREATE TRIGGER business_profiles_bd_for_hours_trigger BEFORE DELETE ON wa_biz_profiles BEGIN DELETE FROM wa_biz_profiles_hours WHERE wa_biz_profile_id = old._id; END");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS business_profiles_bd_for_categories_trigger");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_biz_profiles_categories");
        sQLiteDatabase.execSQL("CREATE TABLE wa_biz_profiles_categories (_id INTEGER PRIMARY KEY AUTOINCREMENT, wa_biz_profile_id INTEGER NOT NULL, category_id TEXT NOT NULL,category_name TEXT NOT NULL)");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS biz_profile_id_category_index ON wa_biz_profiles_categories(wa_biz_profile_id, category_id);");
        sQLiteDatabase.execSQL("CREATE TRIGGER business_profiles_bd_for_categories_trigger BEFORE DELETE ON wa_biz_profiles BEGIN DELETE FROM wa_biz_profiles_categories WHERE wa_biz_profile_id = old._id; END");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_group_descriptions");
        sQLiteDatabase.execSQL("CREATE TABLE wa_group_descriptions (jid TEXT NOT NULL, description TEXT NOT NULL, description_id INTEGER, description_time INTEGER, description_setter_jid TEXT NOT NULL, description_id_string TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS wa_group_descriptions_jid_index ON wa_group_descriptions(jid)");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS group_admin_settings_deletion_trigger");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_group_admin_settings");
        sQLiteDatabase.execSQL("CREATE TABLE wa_group_admin_settings (jid TEXT NOT NULL, restrict_mode BOOLEAN NOT NULL, announcement_group BOOLEAN NOT NULL, no_frequently_forwarded BOOLEAN NOT NULL, ephemeral_duration INTEGER DEFAULT NULL, creator_jid TEXT, in_app_support BOOLEAN NOT NULL, is_suspended BOOLEAN, group_state INTEGER NOT NULL DEFAULT 0, require_membership_approval BOOLEAN NOT NULL, member_add_mode INTEGER NOT NULL DEFAULT 0,incognito BOOLEAN)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS group_admin_settings_jid_index ON wa_group_admin_settings (jid)");
        sQLiteDatabase.execSQL("CREATE TRIGGER group_admin_settings_deletion_trigger BEFORE DELETE ON wa_contacts BEGIN DELETE FROM wa_group_admin_settings WHERE jid = old.jid; END");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_block_list");
        sQLiteDatabase.execSQL("CREATE TABLE wa_block_list (jid TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS block_list_jid_index ON wa_block_list (jid)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_group_add_black_list");
        sQLiteDatabase.execSQL("CREATE TABLE wa_group_add_black_list (jid TEXT NOT NULL)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS group_add_black_list_jid_index ON wa_group_add_black_list (jid)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_last_seen_block_list");
        sQLiteDatabase.execSQL("CREATE TABLE wa_last_seen_block_list (jid TEXT NOT NULL)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_profile_photo_block_list");
        sQLiteDatabase.execSQL("CREATE TABLE wa_profile_photo_block_list (jid TEXT NOT NULL)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_about_block_list");
        sQLiteDatabase.execSQL("CREATE TABLE wa_about_block_list (jid TEXT NOT NULL)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_props");
        sQLiteDatabase.execSQL("CREATE TABLE wa_props (_id INTEGER PRIMARY KEY AUTOINCREMENT, prop_name TEXT UNIQUE, prop_value TEXT)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_last_entry_point");
        sQLiteDatabase.execSQL("CREATE TABLE wa_last_entry_point (jid TEXT NOT NULL, entry_point_type TEXT NOT NULL, entry_point_id TEXT, entry_point_time INTEGER)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS entry_point_jid_index ON wa_last_entry_point (jid)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_trusted_contacts");
        sQLiteDatabase.execSQL("CREATE TABLE wa_trusted_contacts (jid TEXT PRIMARY KEY NOT NULL, incoming_tc_token BLOB NOT NULL, incoming_tc_token_timestamp LONG NOT NULL)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS incoming_tc_token_timestamp_index ON wa_trusted_contacts (incoming_tc_token_timestamp)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_trusted_contacts_send");
        sQLiteDatabase.execSQL("CREATE TABLE wa_trusted_contacts_send (jid TEXT PRIMARY KEY NOT NULL, sent_tc_token_timestamp LONG NOT NULL)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS sent_tc_token_timestamp_index ON wa_trusted_contacts_send (sent_tc_token_timestamp)");
        A01(sQLiteDatabase);
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS business_profiles_bd_for_linked_accounts_trigger");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS wa_biz_profiles_linked_accounts_table");
        sQLiteDatabase.execSQL("CREATE TABLE wa_biz_profiles_linked_accounts_table (_id INTEGER PRIMARY KEY AUTOINCREMENT, wa_biz_profile_id INTEGER NOT NULL, account_id TEXT NOT NULL,account_type INTEGER NOT NULL,account_display_name TEXT NOT NULL,account_fan_count INTEGER NOT NULL)");
        sQLiteDatabase.execSQL(" CREATE INDEX IF NOT EXISTS wa_biz_profiles_linked_accounts_index ON wa_biz_profiles_linked_accounts_table(wa_biz_profile_id);");
        sQLiteDatabase.execSQL("CREATE TRIGGER business_profiles_bd_for_linked_accounts_trigger BEFORE DELETE ON wa_biz_profiles BEGIN DELETE FROM wa_biz_profiles_linked_accounts_table WHERE wa_biz_profile_id = old._id; END");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS group_membership_count");
        sQLiteDatabase.execSQL("CREATE TABLE group_membership_count (jid_row_id INTEGER PRIMARY KEY, member_count INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS dismissed_chat");
        sQLiteDatabase.execSQL("CREATE TABLE dismissed_chat (chat_jid TEXT PRIMARY KEY, timestamp INTEGER NOT NULL)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("Downgrading contacts database from version ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.w(sb.toString());
        onCreate(sQLiteDatabase);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x01ab, code lost:
        if (r37 == 12) goto L_0x01ad;
     */
    /* JADX WARNING: Removed duplicated region for block: B:121:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0108 A[Catch: all -> 0x0495, LOOP:0: B:17:0x0102->B:19:0x0108, LOOP_END, TRY_LEAVE, TryCatch #2 {all -> 0x0495, blocks: (B:16:0x00e7, B:17:0x0102, B:19:0x0108), top: B:107:0x00e7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0473  */
    @Override // android.database.sqlite.SQLiteOpenHelper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onUpgrade(android.database.sqlite.SQLiteDatabase r36, int r37, int r38) {
        /*
        // Method dump skipped, instructions count: 1312
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C232010t.onUpgrade(android.database.sqlite.SQLiteDatabase, int, int):void");
    }
}
