package X;

/* renamed from: X.4Xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92854Xr {
    public final int A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public C92854Xr(String str, String str2, String str3, int i, boolean z) {
        this.A00 = i;
        this.A03 = str;
        this.A02 = str2;
        this.A01 = str3;
        this.A04 = z;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C92854Xr)) {
            return false;
        }
        C92854Xr r4 = (C92854Xr) obj;
        if (this.A00 != r4.A00 || this.A04 != r4.A04 || !this.A03.equals(r4.A03) || !this.A02.equals(r4.A02) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = this.A00;
        int i2 = 0;
        if (this.A04) {
            i2 = 64;
        }
        return i + i2 + (this.A03.hashCode() * this.A02.hashCode() * this.A01.hashCode());
    }

    public String toString() {
        String str;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A03);
        A0h.append('.');
        A0h.append(this.A02);
        A0h.append(this.A01);
        A0h.append(" (");
        A0h.append(this.A00);
        if (this.A04) {
            str = " itf";
        } else {
            str = "";
        }
        A0h.append(str);
        return C12970iu.A0u(A0h);
    }
}
