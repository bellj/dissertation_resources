package X;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.28T  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass28T implements Iterator {
    public Iterator iterator = AnonymousClass1I4.emptyIterator();
    public Deque metaIterators;
    public Iterator toRemove;
    public Iterator topMetaIterator;

    public AnonymousClass28T(Iterator it) {
        this.topMetaIterator = it;
    }

    private Iterator getTopMetaIterator() {
        while (true) {
            Iterator it = this.topMetaIterator;
            if (it != null && it.hasNext()) {
                return this.topMetaIterator;
            }
            Deque deque = this.metaIterators;
            if (deque == null || deque.isEmpty()) {
                return null;
            }
            this.topMetaIterator = (Iterator) this.metaIterators.removeFirst();
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        while (!this.iterator.hasNext()) {
            Iterator topMetaIterator = getTopMetaIterator();
            this.topMetaIterator = topMetaIterator;
            if (topMetaIterator == null) {
                return false;
            }
            Iterator it = (Iterator) topMetaIterator.next();
            this.iterator = it;
            if (it instanceof AnonymousClass28T) {
                AnonymousClass28T r2 = (AnonymousClass28T) it;
                this.iterator = r2.iterator;
                Deque deque = this.metaIterators;
                if (deque == null) {
                    deque = new ArrayDeque();
                    this.metaIterators = deque;
                }
                deque.addFirst(this.topMetaIterator);
                if (r2.metaIterators != null) {
                    while (!r2.metaIterators.isEmpty()) {
                        this.metaIterators.addFirst(r2.metaIterators.removeLast());
                    }
                }
                this.topMetaIterator = r2.topMetaIterator;
            }
        }
        return true;
    }

    @Override // java.util.Iterator
    public Object next() {
        if (hasNext()) {
            Iterator it = this.iterator;
            this.toRemove = it;
            return it.next();
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        Iterator it = this.toRemove;
        if (it != null) {
            it.remove();
            this.toRemove = null;
            return;
        }
        throw new IllegalStateException("no calls to next() since the last call to remove()");
    }
}
