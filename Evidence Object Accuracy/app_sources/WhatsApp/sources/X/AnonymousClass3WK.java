package X;

import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.whatsapp.R;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;

/* renamed from: X.3WK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WK implements AnonymousClass1MF {
    public final /* synthetic */ ActivityC13810kN A00;
    public final /* synthetic */ BlockConfirmationDialogFragment A01;

    public AnonymousClass3WK(ActivityC13810kN r1, BlockConfirmationDialogFragment blockConfirmationDialogFragment) {
        this.A01 = blockConfirmationDialogFragment;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1MF
    public void AR8(C15370n3 r7) {
        BlockConfirmationDialogFragment blockConfirmationDialogFragment = this.A01;
        ActivityC13810kN r4 = this.A00;
        blockConfirmationDialogFragment.A00.A0H(new RunnableBRunnable0Shape0S1100000_I0(13, r4.getString(R.string.report_and_leave_confirmation), blockConfirmationDialogFragment));
        r4.finish();
    }

    @Override // X.AnonymousClass1MF
    public void AYA(C15370n3 r7) {
        BlockConfirmationDialogFragment blockConfirmationDialogFragment = this.A01;
        ActivityC13810kN r4 = this.A00;
        blockConfirmationDialogFragment.A00.A0H(new RunnableBRunnable0Shape0S1100000_I0(13, C12960it.A0X(r4, blockConfirmationDialogFragment.A06.A04(r7), C12970iu.A1b(), 0, R.string.report_and_block_confirmation), blockConfirmationDialogFragment));
        r4.finish();
    }
}
