package X;

import com.whatsapp.avatar.home.AvatarHomeViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3e1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72103e1 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AvatarHomeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72103e1(AvatarHomeViewModel avatarHomeViewModel) {
        super(1);
        this.this$0 = avatarHomeViewModel;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Throwable th = (Throwable) obj;
        C16700pc.A0E(th, 0);
        Log.i("onConfirmDeleteAvatarClicked/error");
        this.this$0.A00.A0B(new C83913y6(false, true));
        th.printStackTrace();
        return AnonymousClass1WZ.A00;
    }
}
