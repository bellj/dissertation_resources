package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72493eh extends AnimatorListenerAdapter {
    public final /* synthetic */ C48232Fc A00;

    public C72493eh(C48232Fc r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        C48232Fc r2 = this.A00;
        r2.A02.setIconified(true);
        r2.A06.setVisibility(4);
    }
}
