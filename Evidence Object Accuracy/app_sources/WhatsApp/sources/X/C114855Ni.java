package X;

/* renamed from: X.5Ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114855Ni extends AnonymousClass5MA {
    public C114855Ni(AnonymousClass5MA r3) {
        super(r3.A0B(), r3.A00);
    }

    @Override // X.AnonymousClass5NS
    public String toString() {
        return C12960it.A0d(Integer.toHexString(this.A01[0] & 255), C12960it.A0k("NetscapeCertType: 0x"));
    }
}
