package X;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.2L7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2L7 {
    public Object A00;
    public final Condition A01;
    public final ReentrantLock A02;

    public AnonymousClass2L7() {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        this.A02 = reentrantLock;
        this.A01 = reentrantLock.newCondition();
    }

    public boolean A00(Object obj) {
        boolean z;
        AnonymousClass009.A05(obj);
        ReentrantLock reentrantLock = this.A02;
        reentrantLock.lock();
        try {
            if (this.A00 != null) {
                z = false;
            } else {
                this.A00 = obj;
                this.A01.signal();
                z = true;
            }
            return z;
        } finally {
            reentrantLock.unlock();
        }
    }
}
