package X;

/* renamed from: X.5pk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public enum EnumC124565pk {
    FOCUSING,
    CANCELLED,
    /* Fake field, exist only in values array */
    RESET,
    SUCCESS,
    FAILED,
    EXCEPTION,
    AUTOFOCUS_SUCCESS,
    AUTOFOCUS_FAILED
}
