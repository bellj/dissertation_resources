package X;

import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.3XO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XO implements AbstractC39381po {
    public final /* synthetic */ C617331x A00;

    @Override // X.AbstractC39381po
    public void AQA(Exception exc) {
    }

    public AnonymousClass3XO(C617331x r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC39381po
    public void AQX(File file, String str, byte[] bArr) {
        C617331x r2 = this.A00;
        r2.A01 = null;
        if (file == null) {
            Log.w(C12960it.A0d(str, C12960it.A0k("gif/preview/holder file is null for ")));
        } else if (str.equals(r2.A02)) {
            if (bArr != null) {
                r2.A04.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length, AnonymousClass19O.A07));
            }
            AnonymousClass3BO r4 = r2.A09;
            try {
                C38911ou A00 = C38911ou.A00(ParcelFileDescriptor.open(new File(file.getAbsolutePath()), 268435456), false);
                r4.A01 = A00;
                AnonymousClass2Zb A05 = A00.A05(r4.A02.getContext());
                r4.A00 = A05;
                A05.start();
            } catch (IOException e) {
                Log.e("gif/loading/io-exception", e);
            }
            r4.A03.setImageDrawable(r4.A00);
            r2.A04.setVisibility(8);
        }
    }
}
