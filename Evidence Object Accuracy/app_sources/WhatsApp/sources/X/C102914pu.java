package X;

import android.content.Context;
import com.whatsapp.chatinfo.ListChatInfo;

/* renamed from: X.4pu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102914pu implements AbstractC009204q {
    public final /* synthetic */ ListChatInfo A00;

    public C102914pu(ListChatInfo listChatInfo) {
        this.A00 = listChatInfo;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
