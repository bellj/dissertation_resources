package X;

import com.whatsapp.Mp4Ops;

/* renamed from: X.38z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC628938z extends AbstractC16350or {
    public final AbstractC15710nm A00;
    public final Mp4Ops A01;
    public final C18790t3 A02;
    public final C17050qB A03;
    public final C14830m7 A04;
    public final AnonymousClass1O6 A05;
    public final AbstractC39381po A06;
    public final String A07;
    public final boolean A08;

    public AbstractC628938z(AbstractC15710nm r1, Mp4Ops mp4Ops, C18790t3 r3, C17050qB r4, C14830m7 r5, AnonymousClass1O6 r6, AbstractC39381po r7, String str, boolean z) {
        this.A01 = mp4Ops;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A07 = str;
        this.A06 = r7;
        this.A08 = z;
        this.A05 = r6;
        this.A04 = r5;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 3, insn: 0x01bc: MOVE  (r13 I:??[OBJECT, ARRAY]) = (r3 I:??[OBJECT, ARRAY]), block:B:84:0x01bc
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final X.C92384Vr A08() {
        /*
        // Method dump skipped, instructions count: 468
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC628938z.A08():X.4Vr");
    }
}
