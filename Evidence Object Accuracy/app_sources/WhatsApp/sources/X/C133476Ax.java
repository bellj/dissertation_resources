package X;

import com.whatsapp.util.Log;

/* renamed from: X.6Ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133476Ax implements AnonymousClass6MV {
    public final /* synthetic */ C129335xY A00;
    public final /* synthetic */ C126935tg A01;
    public final /* synthetic */ Boolean A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;

    public C133476Ax(C129335xY r1, C126935tg r2, Boolean bool, String str, String str2, String str3) {
        this.A00 = r1;
        this.A05 = str;
        this.A04 = str2;
        this.A02 = bool;
        this.A03 = str3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r4) {
        Log.e("PAY: BrazilPayBloksActivity/provider key iq returned null");
        C126935tg r0 = this.A01;
        AbstractActivityC121705jc.A0l(r0.A00, null, r4.A00);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r8) {
        C129335xY r0 = this.A00;
        String str = this.A05;
        String str2 = this.A04;
        r0.A00(r8, this.A01, this.A02, str, str2, this.A03);
    }
}
