package X;

import java.util.List;

/* renamed from: X.5ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127665ur {
    public final AnonymousClass619 A00;
    public final List A01;
    public final List A02;
    public final List A03;

    public C127665ur(AnonymousClass619 r1, List list, List list2, List list3) {
        this.A03 = list;
        this.A02 = list2;
        this.A01 = list3;
        this.A00 = r1;
    }
}
