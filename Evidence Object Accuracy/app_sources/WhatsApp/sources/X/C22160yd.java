package X;

import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

/* renamed from: X.0yd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22160yd {
    public C14900mE A00;
    public AnonymousClass01d A01;
    public C14830m7 A02;
    public C21250x7 A03;
    public C14850m9 A04;
    public C16120oU A05;
    public C16630pM A06;
    public AbstractC14440lR A07;
    public final AnonymousClass14T A08;

    public C22160yd(C14900mE r1, AnonymousClass01d r2, C14830m7 r3, C21250x7 r4, C14850m9 r5, C16120oU r6, AnonymousClass14T r7, C16630pM r8, AbstractC14440lR r9) {
        this.A00 = r1;
        this.A04 = r5;
        this.A07 = r9;
        this.A01 = r2;
        this.A05 = r6;
        this.A03 = r4;
        this.A08 = r7;
        this.A06 = r8;
        this.A02 = r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0016  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C30761Ys A00(X.AbstractC15340mz r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof X.C28851Pg
            if (r0 == 0) goto L_0x002b
            X.1Pg r5 = (X.C28851Pg) r5
            X.1Pk r0 = r5.A00
            java.util.List r0 = r0.A04
            if (r0 == 0) goto L_0x002b
            java.util.Iterator r3 = r0.iterator()
        L_0x0010:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x002b
            java.lang.Object r2 = r3.next()
            X.1Ys r2 = (X.C30761Ys) r2
            X.0m9 r1 = r4.A04
            boolean r0 = X.C30041Vv.A0Z(r1, r2)
            if (r0 != 0) goto L_0x002a
            boolean r0 = X.C30041Vv.A0a(r1, r2)
            if (r0 == 0) goto L_0x0010
        L_0x002a:
            return r2
        L_0x002b:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22160yd.A00(X.0mz):X.1Ys");
    }

    public String A01(C30761Ys r4) {
        String queryParameter;
        String str;
        C14850m9 r1 = this.A04;
        if (C30041Vv.A0Z(r1, r4)) {
            str = r1.A03(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            queryParameter = r4.A05;
        } else if (!C30041Vv.A0a(r1, r4)) {
            return null;
        } else {
            queryParameter = Uri.parse(r4.A05).getQueryParameter("code");
            str = "otp";
        }
        return queryParameter.replace(str, "");
    }

    public final ArrayList A02() {
        JSONArray jSONArray = new JSONArray(this.A06.A01("otp.logging.dedup").getString("otp.logging.dedup.messageIds", "[]"));
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }

    public void A03(Context context, C28851Pg r11, Integer num) {
        C30761Ys A00 = A00(r11);
        this.A03.A06(r11.A0C(), 1);
        AnonymousClass14T r6 = this.A08;
        r6.A01(r11, 1, num);
        String A01 = A01(A00);
        Intent intent = new Intent();
        String str = A00.A05;
        intent.setPackage(Uri.parse(str).getQueryParameter("package_name"));
        intent.setAction("com.whatsapp.otp.OTP_RETRIEVED");
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities.isEmpty()) {
            StringBuilder sb = new StringBuilder("messageOTP/autofill: no activity for ");
            sb.append(Uri.parse(str).getQueryParameter("cta_display_name"));
            Log.e(sb.toString());
            return;
        }
        intent.setClassName(Uri.parse(str).getQueryParameter("package_name"), queryIntentActivities.get(0).activityInfo.name);
        intent.putExtra("code", A01);
        intent.setFlags(268435456);
        PendingIntent A012 = AnonymousClass1UY.A01(context, 0, new Intent(), 0);
        Bundle extras = intent.getExtras();
        if (extras == null) {
            extras = new Bundle();
        }
        extras.putParcelable("_ci_", A012);
        intent.putExtras(extras);
        context.startActivity(intent);
        C38261nn r5 = new C38261nn();
        r5.A03 = 3;
        r5.A02 = num;
        r5.A01 = 1;
        r5.A05 = Long.valueOf(Long.parseLong(r11.A0C().user));
        r5.A04 = 0;
        r5.A06 = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - r11.A0I));
        r5.A07 = AnonymousClass14T.A00(r11);
        r6.A01.A07(r5);
    }

    public void A04(C28851Pg r4, Integer num) {
        C28881Pj r2 = new C28881Pj();
        r2.A00 = num;
        r2.A01 = 1;
        r2.A03 = r4.A00.A03;
        r2.A02 = Long.valueOf(Long.parseLong(r4.A0C().user));
        this.A05.A07(r2);
    }

    public void A05(C28851Pg r10, Integer num) {
        C30761Ys A00 = A00(r10);
        this.A03.A06(r10.A0C(), 1);
        String A01 = A01(A00);
        try {
            this.A01.A0B().setPrimaryClip(ClipData.newPlainText(A01, A01));
            this.A00.A08(R.string.copy_code_to_clipboard, 1);
        } catch (NullPointerException | SecurityException e) {
            Log.e("messageOTP/copycode", e);
        }
        this.A07.Ab2(new RunnableBRunnable0Shape0S0400000_I0(this, num, r10, A00, 17));
    }

    public boolean A06(C30761Ys r3) {
        C14850m9 r1 = this.A04;
        if (!C30041Vv.A0Z(r1, r3)) {
            return C30041Vv.A0a(r1, r3) && r3.A06.get() == 2;
        }
        return true;
    }

    public boolean A07(C30761Ys r3) {
        if (!C30041Vv.A0a(this.A04, r3) || r3.A06.get() != 1) {
            return false;
        }
        return true;
    }
}
