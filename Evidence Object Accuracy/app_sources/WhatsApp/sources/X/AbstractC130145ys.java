package X;

import android.content.Context;
import com.whatsapp.R;
import java.math.BigDecimal;
import java.util.List;

/* renamed from: X.5ys  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC130145ys {
    public final C16590pI A00;
    public final AnonymousClass018 A01;
    public final AnonymousClass102 A02;
    public final AnonymousClass1IR A03;
    public final AbstractC121025h8 A04;
    public final C130205yy A05;
    public final C129835yN A06;
    public final C123765np A07;

    public AbstractC130145ys(C16590pI r2, AnonymousClass018 r3, AnonymousClass102 r4, AnonymousClass1IR r5, C130205yy r6, C129835yN r7, C123765np r8) {
        this.A01 = r3;
        this.A00 = r2;
        this.A05 = r6;
        this.A06 = r7;
        this.A03 = r5;
        this.A07 = r8;
        this.A02 = r4;
        AbstractC30891Zf r0 = r5.A0A;
        AnonymousClass009.A05(r0);
        AbstractC1316063k r02 = ((C119825fA) r0).A01;
        AnonymousClass009.A05(r02);
        this.A04 = (AbstractC121025h8) r02;
    }

    public CharSequence A00() {
        BigDecimal divide;
        C1316363n r0 = this.A04.A06;
        AnonymousClass6F2 r6 = r0.A02;
        AnonymousClass6F2 r5 = r0.A01;
        Context context = this.A00.A00;
        AbstractC30791Yv r3 = r6.A00;
        if (!(r3 instanceof C30801Yw) || !((C30801Yw) r3).A00(r5.A00)) {
            divide = r5.A01.A00.divide(r6.A01.A00, 4, 4);
        } else {
            divide = BigDecimal.ONE;
        }
        return C1316563p.A01(context, this.A01, r5.A00, r3, divide);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List A01() {
        /*
            r14 = this;
            java.util.LinkedList r11 = new java.util.LinkedList
            r11.<init>()
            r8 = r14
            X.5np r2 = r14.A07
            int r0 = r2.A08()
            switch(r0) {
                case 1: goto L_0x00db;
                case 2: goto L_0x00cc;
                case 3: goto L_0x00cc;
                case 4: goto L_0x000f;
                case 5: goto L_0x00c1;
                case 6: goto L_0x00b7;
                case 7: goto L_0x00b7;
                case 8: goto L_0x00cc;
                case 9: goto L_0x00cc;
                case 10: goto L_0x00cc;
                case 11: goto L_0x00cc;
                default: goto L_0x000f;
            }
        L_0x000f:
            int r0 = r2.A08()
            switch(r0) {
                case 1: goto L_0x00a5;
                case 2: goto L_0x00a5;
                case 3: goto L_0x0097;
                case 4: goto L_0x0016;
                case 5: goto L_0x00a5;
                case 6: goto L_0x00a5;
                case 7: goto L_0x0089;
                case 8: goto L_0x00a5;
                case 9: goto L_0x00a5;
                case 10: goto L_0x00a5;
                case 11: goto L_0x0097;
                default: goto L_0x0016;
            }
        L_0x0016:
            int r0 = r2.A08()
            switch(r0) {
                case 1: goto L_0x0047;
                case 2: goto L_0x0047;
                case 3: goto L_0x0047;
                case 4: goto L_0x001d;
                case 5: goto L_0x0047;
                case 6: goto L_0x0047;
                case 7: goto L_0x0047;
                case 8: goto L_0x0047;
                case 9: goto L_0x0047;
                case 10: goto L_0x0047;
                case 11: goto L_0x0047;
                default: goto L_0x001d;
            }
        L_0x001d:
            r14.A05(r11)
            X.5yy r1 = r14.A05
            r1.A07(r11)
            X.5yN r0 = r14.A06
            X.C123065mY.A00(r0, r11)
            r0 = 2131889974(0x7f120f36, float:1.9414627E38)
            r14.A06(r11, r0)
            r1.A09(r11)
            r1.A0A(r11)
            X.1IR r0 = r14.A03
            X.1Zf r0 = r0.A0A
            X.5fA r0 = (X.C119825fA) r0
            X.63k r0 = r0.A01
            boolean r0 = r0.A03
            r1.A0C(r11, r0)
            r1.A06(r11)
            return r11
        L_0x0047:
            X.0pI r0 = r14.A00
            android.content.Context r7 = r0.A00
            r0 = 2131889977(0x7f120f39, float:1.9414633E38)
            java.lang.String r5 = r7.getString(r0)
            X.5h8 r0 = r14.A04
            X.5h6 r0 = (X.C121005h6) r0
            X.5tQ r0 = r0.A00
            java.lang.String r6 = r0.A00
            r4 = 2131889976(0x7f120f38, float:1.941463E38)
            java.lang.Object[] r3 = X.C12970iu.A1b()
            r2 = 0
            java.lang.String r0 = "••"
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            int r0 = r6.length()
            int r0 = r0 + -4
            java.lang.String r0 = r6.substring(r0)
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            java.lang.String r2 = X.C12960it.A0X(r7, r0, r3, r2, r4)
            X.5yy r0 = r14.A05
            r0.A08(r11)
            r1 = 0
            X.5mF r0 = new X.5mF
            r0.<init>(r5, r2, r1)
            r11.add(r0)
            goto L_0x001d
        L_0x0089:
            X.5yy r4 = r14.A05
            java.lang.String r3 = r2.A04()
            java.lang.String r1 = r2.A09()
            r0 = 2131100664(0x7f0603f8, float:1.7813716E38)
            goto L_0x00b2
        L_0x0097:
            X.5yy r4 = r14.A05
            java.lang.String r3 = r2.A04()
            java.lang.String r1 = r2.A09()
            r0 = 2131100663(0x7f0603f7, float:1.7813714E38)
            goto L_0x00b2
        L_0x00a5:
            X.5yy r4 = r14.A05
            java.lang.String r3 = r2.A04()
            java.lang.String r1 = r2.A09()
            r0 = 2131100662(0x7f0603f6, float:1.7813712E38)
        L_0x00b2:
            r4.A04(r3, r1, r11, r0)
            goto L_0x0016
        L_0x00b7:
            X.5h8 r0 = r14.A04
            X.63n r0 = r0.A06
            X.6F2 r9 = r0.A02
            X.6F2 r10 = r0.A01
            r12 = 1
            goto L_0x00d5
        L_0x00c1:
            X.5h8 r0 = r14.A04
            X.63n r0 = r0.A06
            X.6F2 r9 = r0.A02
            X.6F2 r10 = r0.A01
            r12 = 1
            r13 = 0
            goto L_0x00d6
        L_0x00cc:
            X.5h8 r0 = r14.A04
            X.63n r0 = r0.A06
            X.6F2 r9 = r0.A02
            X.6F2 r10 = r0.A01
            r12 = 0
        L_0x00d5:
            r13 = r12
        L_0x00d6:
            r8.A03(r9, r10, r11, r12, r13)
            goto L_0x000f
        L_0x00db:
            X.5h8 r0 = r14.A04
            X.63n r0 = r0.A06
            X.6F2 r1 = r0.A02
            X.6F2 r0 = r0.A01
            r14.A02(r1, r0, r11)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC130145ys.A01():java.util.List");
    }

    public void A02(AnonymousClass6F2 r8, AnonymousClass6F2 r9, List list) {
        Context context = this.A00.A00;
        AbstractC30791Yv r5 = r8.A00;
        Object[] A1b = C12970iu.A1b();
        AnonymousClass018 r3 = this.A01;
        CharSequence AA7 = r5.AA7(context, C12960it.A0X(context, r9.A06(r3), A1b, 0, R.string.novi_transaction_details_conversion_summary));
        this.A05.A03(C117305Zk.A0e(context, r3, r5, r8.A01, 0), AA7, list);
    }

    public void A03(AnonymousClass6F2 r10, AnonymousClass6F2 r11, List list, boolean z, boolean z2) {
        Context context = this.A00.A00;
        AbstractC30791Yv r5 = r11.A00;
        Object[] A1b = C12970iu.A1b();
        AnonymousClass018 r3 = this.A01;
        CharSequence AA7 = r5.AA7(context, C12960it.A0X(context, r11.A06(r3), A1b, 0, R.string.novi_transaction_details_conversion_summary));
        CharSequence A0e = C117305Zk.A0e(context, r3, r10.A00, r10.A01, 0);
        C123765np r0 = this.A07;
        list.add(new C123285mu(A0e, r0.A07.A0D(((AbstractC130195yx) r0).A01, A0e), AA7, null, null, z, z2));
    }

    public void A04(CharSequence charSequence, List list, int i) {
        String string = this.A00.A00.getString(i);
        AnonymousClass6F2 r0 = this.A04.A06.A01;
        AbstractC30791Yv r3 = r0.A00;
        String AAA = r3.AAA(this.A01, r0.A01, 0);
        StringBuilder A0h = C12960it.A0h();
        A0h.append((Object) AAA);
        A0h.append(" ");
        list.add(C123335mz.A00(string, C12960it.A0d(((AbstractC30781Yu) r3).A04, A0h), 1));
        list.add(new C123075mZ(charSequence));
    }

    public void A05(List list) {
        C129835yN r1;
        int i;
        String string = this.A00.A00.getString(R.string.novi_send_money_review_method_balance);
        switch (this.A07.A08()) {
            case 1:
            case 2:
                this.A05.A07(list);
                r1 = this.A06;
                i = R.string.novi_payment_transaction_details_withdrawing_from_title;
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                this.A05.A07(list);
                r1 = this.A06;
                i = R.string.novi_payment_transaction_details_withdraw_from_title;
                break;
            default:
                return;
        }
        list.add(r1.A01(string, i, R.drawable.novi_logo));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void A06(List list, int i) {
        switch (this.A07.A08()) {
            case 1:
            case 2:
            case 3:
            case 11:
                break;
            case 4:
            case 5:
            case 6:
            case 7:
                A04(A00(), list, i);
                return;
            case 8:
            case 9:
            case 10:
                C129835yN r1 = this.A06;
                C1316463o r0 = this.A04.A03;
                AnonymousClass009.A05(r0);
                list.add(r1.A00(r0.A03, R.string.novi_payment_transaction_details_section_title_claim_id));
                this.A05.A08(list);
                break;
            default:
                return;
        }
        CharSequence A00 = A00();
        Context context = this.A00.A00;
        AnonymousClass018 r4 = this.A01;
        list.add(C123335mz.A00(context.getString(R.string.novi_payment_transaction_details_breakdown_withdraw_amount_label), AnonymousClass613.A00(context, r4, this.A03), 1));
        list.add(C123335mz.A00(context.getString(R.string.novi_send_money_review_extras_fees_label), AnonymousClass613.A01(context, r4, this.A04.A04.A01), 3));
        this.A05.A08(list);
        A04(A00, list, i);
    }
}
