package X;

import android.content.Context;

/* renamed from: X.5gi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120765gi extends C120895gv {
    public final /* synthetic */ C120485gG A00;
    public final /* synthetic */ C125865rw A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120765gi(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120485gG r11, C125865rw r12) {
        super(context, r8, r9, r10, "upi-send-to-vpa");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        this.A00.A06.A05(r3, "in_send_to_vpa_tag");
        this.A01.A00.A3U(r3, false);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        this.A00.A06.A05(r3, "in_send_to_vpa_tag");
        this.A01.A00.A3U(r3, false);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r4) {
        super.A04(r4);
        this.A00.A06.A06("in_send_to_vpa_tag", 2);
        this.A01.A00.A3U(null, false);
    }
}
