package X;

import android.animation.ValueAnimator;
import com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView;

/* renamed from: X.3Ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C65493Ju implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ float A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ C47342Ag A02;

    public /* synthetic */ C65493Ju(C47342Ag r1, float f, int i) {
        this.A02 = r1;
        this.A00 = f;
        this.A01 = i;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        C47342Ag r0 = this.A02;
        float f = this.A00;
        int i = this.A01;
        TitleBarView titleBarView = r0.A0H;
        float A00 = C12960it.A00(valueAnimator);
        AnonymousClass2ZW r02 = titleBarView.A0B;
        r02.A02 = f;
        r02.A03 = i;
        r02.A01 = A00;
        r02.invalidateSelf();
    }
}
