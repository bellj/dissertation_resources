package X;

import java.util.List;

/* renamed from: X.46R  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass46R extends AnonymousClass4VP {
    public final String A00;
    public final String A01;
    public final String A02;
    public final List A03;

    public AnonymousClass46R(String str, String str2, String str3, String str4, List list) {
        super(str, str2);
        this.A00 = str2;
        this.A03 = list;
        this.A01 = str3;
        this.A02 = str4;
    }
}
