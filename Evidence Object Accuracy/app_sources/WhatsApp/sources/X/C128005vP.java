package X;

import java.util.List;

/* renamed from: X.5vP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128005vP {
    public final AnonymousClass619 A00;
    public final AnonymousClass619 A01;
    public final AnonymousClass619 A02;
    public final AnonymousClass619 A03;
    public final String A04;
    public final List A05;

    public C128005vP(AnonymousClass619 r1, AnonymousClass619 r2, AnonymousClass619 r3, AnonymousClass619 r4, String str, List list) {
        this.A03 = r1;
        this.A00 = r2;
        this.A02 = r3;
        this.A05 = list;
        this.A01 = r4;
        this.A04 = str;
    }
}
