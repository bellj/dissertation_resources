package X;

import android.app.Activity;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2hV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54922hV extends AnonymousClass03U {
    public final Activity A00;
    public final AnonymousClass12P A01;

    public C54922hV(Activity activity, View view, AnonymousClass12P r6, AnonymousClass018 r7) {
        super(view);
        this.A00 = activity;
        this.A01 = r6;
        C12970iu.A0K(this.A0H, R.id.chevron).setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A01(activity, R.drawable.chevron, R.color.icon_secondary), r7));
    }
}
