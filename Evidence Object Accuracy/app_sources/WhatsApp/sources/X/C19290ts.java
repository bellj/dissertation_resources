package X;

import android.content.ContentValues;
import android.database.Cursor;

/* renamed from: X.0ts  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19290ts extends AbstractC18500sY {
    public final C14850m9 A00;

    public C19290ts(C18480sW r3, C14850m9 r4) {
        super(r3, "quoted_ui_elements_reply_message", 1);
        this.A00 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("message_row_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("element_type");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("reply_values");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("reply_description");
        C16490p7 r0 = this.A05;
        C16310on A02 = r0.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            long j = -1;
            int i = 0;
            while (cursor.moveToNext()) {
                j = cursor.getLong(columnIndexOrThrow);
                C16310on A01 = r0.get();
                try {
                    C16330op r14 = A01.A03;
                    String l = Long.toString(j);
                    Cursor A09 = r14.A09("SELECT _id, quoted_row_id FROM messages WHERE _id = ?", new String[]{l});
                    if (A09.moveToNext()) {
                        long j2 = A09.getLong(A09.getColumnIndexOrThrow("quoted_row_id"));
                        A09.close();
                        A01.close();
                        if (j2 != 0) {
                            A02 = r0.get();
                            try {
                                boolean z = true;
                                Cursor A092 = A02.A03.A09("SELECT media_wa_type FROM messages_quotes WHERE _id = ?", new String[]{Long.toString(j2)});
                                if (A092.moveToNext()) {
                                    if (A092.getInt(A092.getColumnIndexOrThrow("media_wa_type")) != 46) {
                                        z = false;
                                    }
                                    A092.close();
                                    A02.close();
                                    if (z) {
                                        ContentValues contentValues = new ContentValues(4);
                                        contentValues.put("message_row_id", Long.valueOf(j2));
                                        contentValues.put("element_type", cursor.getString(columnIndexOrThrow2));
                                        contentValues.put("reply_values", cursor.getBlob(columnIndexOrThrow3));
                                        contentValues.put("reply_description", cursor.getString(columnIndexOrThrow4));
                                        A02.A03.A02(contentValues, "message_quoted_ui_elements_reply_legacy");
                                        i++;
                                    }
                                } else {
                                    A092.close();
                                    A02.close();
                                }
                            } finally {
                            }
                        }
                    } else {
                        A09.close();
                        A01.close();
                    }
                    A02.A03.A0C("DELETE FROM message_quoted_ui_elements_reply WHERE message_row_id = ?", new String[]{l});
                } finally {
                }
            }
            A00.A00();
            A00.close();
            A02.close();
            return new AnonymousClass2Ez(j, i);
        } finally {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
        }
    }
}
