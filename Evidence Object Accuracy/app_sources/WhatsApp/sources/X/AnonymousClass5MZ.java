package X;

/* renamed from: X.5MZ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MZ extends AnonymousClass1TM {
    public final AnonymousClass1TN A00;
    public final AnonymousClass1TK A01;

    public static AnonymousClass5MZ A00(Object obj) {
        if (obj instanceof AnonymousClass5MZ) {
            return (AnonymousClass5MZ) obj;
        }
        if (obj != null) {
            return new AnonymousClass5MZ(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    public AnonymousClass5MZ(AbstractC114775Na r2) {
        this.A01 = AnonymousClass1TK.A00(AbstractC114775Na.A00(r2));
        this.A00 = AnonymousClass5NU.A00(AnonymousClass5NU.A01(AbstractC114775Na.A01(r2)));
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A01);
        C94954co.A02(this.A00, A00, 0, true);
        return new AnonymousClass5NZ(A00);
    }
}
