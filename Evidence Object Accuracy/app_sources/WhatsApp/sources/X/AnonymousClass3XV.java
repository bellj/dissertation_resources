package X;

import java.io.IOException;
import java.util.Map;

/* renamed from: X.3XV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3XV implements AbstractC44401yr {
    public final /* synthetic */ AnonymousClass1J7 A00;
    public final /* synthetic */ AnonymousClass1J7 A01;

    public AnonymousClass3XV(AnonymousClass1J7 r1, AnonymousClass1J7 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r5) {
        C16700pc.A0E(r5, 0);
        AnonymousClass5KF r3 = new AnonymousClass5KF(this.A00);
        AnonymousClass12X r2 = r5.A03;
        C16700pc.A0B(r2);
        AnonymousClass12Z r1 = r5.A02;
        C16700pc.A0B(r1);
        if (r5.A00 == 0) {
            Object obj = r1.A00;
            if (obj != null) {
                this.A01.AJ4(obj);
                return;
            }
            return;
        }
        Map map = r2.A00;
        if (map != null) {
            r3.AJ4(map);
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        C16700pc.A0E(iOException, 0);
        this.A00.AJ4(iOException);
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        C16700pc.A0E(exc, 0);
        this.A00.AJ4(exc);
    }
}
