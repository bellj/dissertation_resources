package X;

import com.whatsapp.payments.receiver.IndiaUpiPayIntentReceiverActivity;
import java.util.List;

/* renamed from: X.5oI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124005oI extends AbstractC16350or {
    public final /* synthetic */ AbstractActivityC121515iQ A00;

    public C124005oI(AbstractActivityC121515iQ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AnonymousClass1ZY r4;
        AbstractActivityC121515iQ r7 = this.A00;
        List A02 = ((AbstractActivityC121685jC) r7).A0I.A02();
        C32641cU A01 = ((AbstractActivityC121685jC) r7).A0I.A01("2fa");
        if (!A02.contains(A01)) {
            ((AbstractActivityC121685jC) r7).A0I.A06(A01);
        }
        List A0Z = C117295Zj.A0Z(((AbstractActivityC121685jC) r7).A0P);
        AbstractC28901Pl A00 = C241414j.A00(r7.A00.A0A, A0Z);
        if (!(A00 == null || (r4 = A00.A08) == null)) {
            ((C119755f3) r4).A05 = C117305Zk.A0I(C117305Zk.A0J(), Boolean.class, Boolean.TRUE, "isPinSet");
            C17070qD r0 = ((AbstractActivityC121685jC) r7).A0P;
            r0.A03();
            r0.A09.A0M(A0Z);
        }
        return A00;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AbstractC28901Pl r4 = (AbstractC28901Pl) obj;
        if (r4 != null) {
            AbstractActivityC121515iQ r0 = this.A00;
            C30861Zc r42 = (C30861Zc) r4;
            r0.A00 = r42;
            ((AbstractActivityC121665jA) r0).A04 = r42;
            AnonymousClass01U.A01(r0.getApplicationContext(), IndiaUpiPayIntentReceiverActivity.class, true);
        }
        AbstractActivityC121515iQ r02 = this.A00;
        r02.AaN();
        AbstractActivityC119235dO.A1f(r02);
    }
}
