package X;

import android.content.Context;
import android.widget.ImageView;
import com.google.android.gms.maps.GoogleMapOptions;
import com.whatsapp.R;
import com.whatsapp.location.LocationPicker2;

/* renamed from: X.454  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass454 extends C618632v {
    public final /* synthetic */ LocationPicker2 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass454(Context context, GoogleMapOptions googleMapOptions, LocationPicker2 locationPicker2) {
        super(context, googleMapOptions);
        this.A00 = locationPicker2;
    }

    @Override // X.C618632v
    public void A0A(int i) {
        LocationPicker2 locationPicker2;
        ImageView imageView;
        int i2;
        if (i == 0) {
            locationPicker2 = this.A00;
            imageView = locationPicker2.A0S.A0S;
            i2 = R.drawable.btn_compass_mode_tilt;
        } else if (i == 1) {
            LocationPicker2 locationPicker22 = this.A00;
            locationPicker22.A0S.A0S.setImageResource(R.drawable.btn_myl_active);
            locationPicker22.A0S.A0r = true;
            return;
        } else if (i == 2) {
            locationPicker2 = this.A00;
            imageView = locationPicker2.A0S.A0S;
            i2 = R.drawable.btn_myl;
        } else {
            return;
        }
        imageView.setImageResource(i2);
        locationPicker2.A0S.A0r = false;
    }
}
