package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;

/* renamed from: X.0Ie  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03540Ie extends AnonymousClass2k7 {
    public C03540Ie(C14260l7 r1, AnonymousClass28D r2) {
        super(r1, r2);
    }

    @Override // X.AnonymousClass2k7
    public final void A06(View view, C14260l7 r22, AnonymousClass28D r23, Object obj) {
        Integer valueOf;
        Integer valueOf2;
        Integer valueOf3;
        Integer valueOf4;
        Integer valueOf5;
        Integer valueOf6;
        Integer valueOf7;
        Integer valueOf8;
        Integer valueOf9;
        Integer valueOf10;
        ColorStateList A00;
        boolean A0O = r23.A0O(35, false);
        boolean A0O2 = r23.A0O(51, false);
        AbstractC14200l1 A0G = r23.A0G(36);
        AbstractC12680iK r2 = (AbstractC12680iK) view;
        r2.setChecked(A0O);
        view.setEnabled(r23.A0O(38, true));
        if (A0O2 || A0G != null) {
            r2.setOnCheckedChangeListener(new AnonymousClass0X3(view, this, r22, r23, A0G, A0O2, A0O));
        }
        Context A002 = r22.A00();
        AnonymousClass28D A0F = r23.A0F(40);
        if (A0F == null) {
            valueOf = null;
        } else {
            valueOf = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F));
        }
        AnonymousClass28D A0F2 = r23.A0F(41);
        if (A0F2 == null) {
            valueOf2 = null;
        } else {
            valueOf2 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F2));
        }
        AnonymousClass28D A0F3 = r23.A0F(43);
        if (A0F3 == null) {
            valueOf3 = null;
        } else {
            valueOf3 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F3));
        }
        AnonymousClass28D A0F4 = r23.A0F(48);
        if (A0F4 == null) {
            valueOf4 = null;
        } else {
            valueOf4 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F4));
        }
        AnonymousClass28D A0F5 = r23.A0F(42);
        if (A0F5 == null) {
            valueOf5 = null;
        } else {
            valueOf5 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F5));
        }
        AnonymousClass28D A0F6 = r23.A0F(46);
        if (A0F6 == null) {
            valueOf6 = null;
        } else {
            valueOf6 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F6));
        }
        AnonymousClass28D A0F7 = r23.A0F(45);
        if (A0F7 == null) {
            valueOf7 = null;
        } else {
            valueOf7 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F7));
        }
        AnonymousClass28D A0F8 = r23.A0F(50);
        if (A0F8 == null) {
            valueOf8 = null;
        } else {
            valueOf8 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F8));
        }
        AnonymousClass28D A0F9 = r23.A0F(44);
        if (A0F9 == null) {
            valueOf9 = null;
        } else {
            valueOf9 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F9));
        }
        AnonymousClass28D A0F10 = r23.A0F(49);
        if (A0F10 == null) {
            valueOf10 = null;
        } else {
            valueOf10 = Integer.valueOf(AnonymousClass4Di.A00(r22, A0F10));
        }
        if (valueOf3 == null && valueOf5 == null) {
            A00 = AnonymousClass0TO.A00(A002, valueOf, valueOf2, valueOf4, valueOf6);
        } else {
            A00 = AnonymousClass0TO.A00(A002, valueOf3, valueOf5, valueOf4, valueOf6);
        }
        r2.setThumbTintList(A00);
        if (valueOf7 == null && valueOf9 == null) {
            r2.Ad3(AnonymousClass0TO.A01(A002, valueOf, valueOf2, valueOf8, valueOf10), false);
        } else {
            r2.Ad3(AnonymousClass0TO.A01(A002, valueOf7, valueOf9, valueOf8, valueOf10), true);
        }
    }

    @Override // X.AnonymousClass2k7
    public final void A07(View view, C14260l7 r3, AnonymousClass28D r4, Object obj) {
        ((AbstractC12680iK) view).setOnCheckedChangeListener(null);
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return new AnonymousClass0C0(context);
    }
}
