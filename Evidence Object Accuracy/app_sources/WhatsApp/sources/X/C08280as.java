package X;

import android.graphics.Color;

/* renamed from: X.0as  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08280as implements AbstractC12050hI {
    public static final C08280as A00 = new C08280as();

    @Override // X.AbstractC12050hI
    public Object AYt(AbstractC08850bx r15, float f) {
        double d;
        boolean z = false;
        if (r15.A05() == EnumC03770Jb.BEGIN_ARRAY) {
            z = true;
            r15.A09();
        }
        double A02 = r15.A02();
        double A022 = r15.A02();
        double A023 = r15.A02();
        if (r15.A05() == EnumC03770Jb.NUMBER) {
            d = r15.A02();
        } else {
            d = 1.0d;
        }
        if (z) {
            r15.A0B();
        }
        if (A02 <= 1.0d && A022 <= 1.0d && A023 <= 1.0d) {
            A02 *= 255.0d;
            A022 *= 255.0d;
            A023 *= 255.0d;
            if (d <= 1.0d) {
                d *= 255.0d;
            }
        }
        return Integer.valueOf(Color.argb((int) d, (int) A02, (int) A022, (int) A023));
    }
}
