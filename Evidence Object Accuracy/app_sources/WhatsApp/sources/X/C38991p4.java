package X;

import android.graphics.Bitmap;
import android.os.Build;
import com.whatsapp.GifHelper;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.1p4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38991p4 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public boolean A05;
    public final File A06;
    public final boolean A07;
    public final boolean A08;

    public C38991p4(File file) {
        boolean z;
        Bitmap frameAtTime;
        this.A06 = file;
        try {
            C38911ou.A03(file);
            z = true;
        } catch (IOException unused) {
            z = false;
        }
        this.A08 = z;
        if (z) {
            try {
                C38921ov A03 = C38911ou.A03(file);
                this.A03 = A03.A01;
                this.A01 = A03.A00;
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder("media_file not found: ");
                sb.append(file);
                Log.e(sb.toString(), e);
                throw new AnonymousClass47W();
            }
        } else {
            C38941ox r4 = new C38941ox();
            try {
                try {
                    r4.setDataSource(file.getAbsolutePath());
                    this.A07 = GifHelper.A01(file);
                    String extractMetadata = r4.extractMetadata(9);
                    String extractMetadata2 = r4.extractMetadata(18);
                    String extractMetadata3 = r4.extractMetadata(19);
                    try {
                        long parseLong = Long.parseLong(extractMetadata);
                        this.A04 = parseLong;
                        if (parseLong != 0) {
                            try {
                                this.A03 = Integer.parseInt(extractMetadata2);
                                this.A01 = Integer.parseInt(extractMetadata3);
                            } catch (Exception e2) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("videometa/cannot parse width (");
                                sb2.append(extractMetadata2);
                                sb2.append(") or height (");
                                sb2.append(extractMetadata3);
                                sb2.append(") ");
                                sb2.append(this.A06.getAbsolutePath());
                                sb2.append(" ");
                                sb2.append(this.A06.length());
                                Log.w(sb2.toString(), e2);
                                try {
                                    frameAtTime = r4.getFrameAtTime(0);
                                } catch (Exception | NoSuchMethodError unused2) {
                                }
                                if (frameAtTime != null) {
                                    this.A03 = frameAtTime.getWidth();
                                    int height = frameAtTime.getHeight();
                                    this.A01 = height;
                                    if (this.A03 == 0 || height == 0) {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("videometa/bad width (");
                                        sb3.append(extractMetadata2);
                                        sb3.append(") or height (");
                                        sb3.append(extractMetadata3);
                                        sb3.append(") ");
                                        sb3.append(this.A06.getAbsolutePath());
                                        sb3.append(" ");
                                        sb3.append(this.A06.length());
                                        Log.e(sb3.toString());
                                        throw new AnonymousClass47W();
                                    }
                                } else {
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append("videometa/cannot get frame");
                                    sb4.append(this.A06.getAbsolutePath());
                                    sb4.append(" ");
                                    sb4.append(this.A06.length());
                                    Log.e(sb4.toString());
                                    throw new AnonymousClass47W();
                                }
                            }
                            try {
                                this.A00 = Integer.parseInt(r4.extractMetadata(20));
                            } catch (Exception unused3) {
                            }
                            try {
                                if (Build.VERSION.SDK_INT >= 17) {
                                    this.A02 = Integer.parseInt(r4.extractMetadata(24));
                                }
                            } catch (Exception unused4) {
                            }
                            r4.close();
                            return;
                        }
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("videometa/no duration:");
                        sb5.append(extractMetadata);
                        sb5.append(" ");
                        sb5.append(file.getAbsolutePath());
                        sb5.append(" ");
                        sb5.append(file.length());
                        Log.e(sb5.toString());
                        throw new AnonymousClass47W();
                    } catch (Exception e3) {
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("videometa/cannot parse duration:");
                        sb6.append(extractMetadata);
                        sb6.append(" ");
                        sb6.append(this.A06.getAbsolutePath());
                        sb6.append(" ");
                        sb6.append(this.A06.length());
                        Log.e(sb6.toString(), e3);
                        throw new AnonymousClass47W();
                    }
                } catch (Exception e4) {
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append("videometa/cannot process file:");
                    sb7.append(this.A06.getAbsolutePath());
                    sb7.append(" ");
                    sb7.append(this.A06.length());
                    sb7.append(" ");
                    sb7.append(this.A06.exists());
                    Log.e(sb7.toString(), e4);
                    throw new AnonymousClass47W();
                }
            } catch (Throwable th) {
                try {
                    r4.close();
                } catch (Throwable unused5) {
                }
                throw th;
            }
        }
    }

    public int A00() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        long j = this.A04;
        if (j != 0) {
            return (int) ((this.A06.length() * 8000) / j);
        }
        return 0;
    }

    public int A01() {
        if (Build.VERSION.SDK_INT < 17 && !this.A05) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                int i = AnonymousClass152.A04(this.A06, true).A05;
                this.A02 = i;
                StringBuilder sb = new StringBuilder();
                sb.append("mediafileutils/VideoMeta/getRotation rotation=");
                sb.append(i);
                sb.append(" rotationExtractionTime=");
                sb.append(System.currentTimeMillis() - currentTimeMillis);
                Log.w(sb.toString());
                this.A05 = true;
            } catch (Exception unused) {
            }
        }
        return this.A02;
    }

    public boolean A02() {
        return Math.abs(A01() % 180) == 90;
    }
}
