package X;

import java.util.AbstractList;
import java.util.RandomAccess;

/* renamed from: X.5I3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5I3 extends AbstractList<C08960c8> implements RandomAccess {
    public static final AnonymousClass4WN A02 = new AnonymousClass4WN();
    public final int[] A00;
    public final C08960c8[] A01;

    public static final AnonymousClass5I3 A00(C08960c8... r1) {
        return A02.A00(r1);
    }

    public /* synthetic */ AnonymousClass5I3(int[] iArr, C08960c8[] r2) {
        this.A01 = r2;
        this.A00 = iArr;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* bridge */ boolean contains(Object obj) {
        if (obj == null || (obj instanceof C08960c8)) {
            return super.contains(obj);
        }
        return false;
    }

    @Override // java.util.AbstractList, java.util.List
    public /* bridge */ /* synthetic */ Object get(int i) {
        return this.A01[i];
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ int indexOf(Object obj) {
        if (obj == null || (obj instanceof C08960c8)) {
            return super.indexOf(obj);
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj == null || (obj instanceof C08960c8)) {
            return super.lastIndexOf(obj);
        }
        return -1;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* bridge */ boolean remove(Object obj) {
        if (obj == null || (obj instanceof C08960c8)) {
            return super.remove(obj);
        }
        return false;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* bridge */ int size() {
        return this.A01.length;
    }
}
