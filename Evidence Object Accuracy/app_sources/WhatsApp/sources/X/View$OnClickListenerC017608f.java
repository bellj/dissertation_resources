package X;

import android.view.View;
import android.view.Window;

/* renamed from: X.08f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class View$OnClickListenerC017608f implements View.OnClickListener {
    public final C017708g A00;
    public final /* synthetic */ C017408d A01;

    public View$OnClickListenerC017608f(C017408d r4) {
        this.A01 = r4;
        this.A00 = new C017708g(r4.A09.getContext(), r4.A0C);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        C017408d r0 = this.A01;
        Window.Callback callback = r0.A07;
        if (callback != null && r0.A0D) {
            callback.onMenuItemSelected(0, this.A00);
        }
    }
}
