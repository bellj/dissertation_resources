package X;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.LocaleList;

/* renamed from: X.09t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020609t extends Paint {
    @Override // android.graphics.Paint
    public void setTextLocales(LocaleList localeList) {
    }

    public C020609t() {
    }

    public C020609t(int i) {
        super(i);
    }

    public C020609t(int i, PorterDuff.Mode mode) {
        super(1);
        setXfermode(new PorterDuffXfermode(mode));
    }

    public C020609t(PorterDuff.Mode mode) {
        setXfermode(new PorterDuffXfermode(mode));
    }
}
