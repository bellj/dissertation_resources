package X;

import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import com.whatsapp.R;
import com.whatsapp.contentprovider.MediaProvider;
import com.whatsapp.instrumentation.api.InstrumentationProvider;
import com.whatsapp.migration.export.api.ExportMigrationContentProvider;
import com.whatsapp.registration.directmigration.MigrationContentProvider;
import com.whatsapp.stickers.WhitelistPackQueryContentProvider;
import com.whatsapp.util.Log;

/* renamed from: X.0nE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15420nE extends ContentProvider {
    public boolean A00;

    public static void A00(Context context, String str) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("application/zip");
        intent.putExtra("android.intent.extra.STREAM", MediaProvider.A05("business_activity_report", str));
        intent.putExtra("android.intent.extra.SUBJECT", context.getString(R.string.p2b_report_file_name));
        intent.addFlags(524288);
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.p2b_report_file_name)));
    }

    public synchronized void A01() {
        if (!this.A00) {
            StringBuilder sb = new StringBuilder();
            sb.append("WaBaseContentProvider/ensureInitialized called for ");
            sb.append(getClass().getSimpleName());
            Log.i(sb.toString());
            if (!AnonymousClass01I.A01()) {
                AnonymousClass009.A00();
                AnonymousClass009.A00.block();
            }
            if (this instanceof WhitelistPackQueryContentProvider) {
                WhitelistPackQueryContentProvider whitelistPackQueryContentProvider = (WhitelistPackQueryContentProvider) this;
                whitelistPackQueryContentProvider.A01 = (AnonymousClass0o6) ((AnonymousClass01J) AnonymousClass01M.A00(whitelistPackQueryContentProvider.getContext().getApplicationContext(), AnonymousClass01J.class)).ALV.get();
            } else if (this instanceof MigrationContentProvider) {
                MigrationContentProvider migrationContentProvider = (MigrationContentProvider) this;
                AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(migrationContentProvider.getContext(), AnonymousClass01J.class);
                migrationContentProvider.A01 = r1.A1w();
                migrationContentProvider.A00 = (C14330lG) r1.A7B.get();
                migrationContentProvider.A02 = (C15450nH) r1.AII.get();
                migrationContentProvider.A05 = (C15810nw) r1.A73.get();
                migrationContentProvider.A04 = (C15820nx) r1.A6U.get();
                migrationContentProvider.A0B = (C15830ny) r1.AKH.get();
                migrationContentProvider.A0A = (AbstractC15850o0) r1.ANA.get();
                migrationContentProvider.A09 = (C15860o1) r1.A3H.get();
                migrationContentProvider.A08 = (C15880o3) r1.ACF.get();
                migrationContentProvider.A07 = r1.Ag2();
                migrationContentProvider.A06 = (C15890o4) r1.AN1.get();
                migrationContentProvider.A03 = (C15900o5) r1.A6T.get();
            } else if (this instanceof ExportMigrationContentProvider) {
                ExportMigrationContentProvider exportMigrationContentProvider = (ExportMigrationContentProvider) this;
                Context context = exportMigrationContentProvider.getContext();
                if (context != null) {
                    AnonymousClass01J r12 = (AnonymousClass01J) AnonymousClass01M.A00(context, AnonymousClass01J.class);
                    exportMigrationContentProvider.A02 = r12.A3L();
                    exportMigrationContentProvider.A01 = r12.A7p();
                    exportMigrationContentProvider.A05 = (C15720nn) r12.A6o.get();
                    C15730no r6 = (C15730no) r12.A6r.get();
                    C15740np A3V = r12.A3V();
                    exportMigrationContentProvider.A03 = new C15770ns((AbstractC15710nm) r12.A4o.get(), (C15750nq) r12.ACZ.get(), r6, r12.A3U(), A3V, (AbstractC14440lR) r12.ANe.get());
                    exportMigrationContentProvider.A06 = (C15780nt) r12.A6z.get();
                    exportMigrationContentProvider.A04 = (C15790nu) r12.A6y.get();
                    UriMatcher uriMatcher = new UriMatcher(-1);
                    String str = C15800nv.A03;
                    uriMatcher.addURI(str, "files", 1);
                    uriMatcher.addURI(str, "file/#", 2);
                    exportMigrationContentProvider.A00 = uriMatcher;
                } else {
                    throw new IllegalStateException("Context is not attached.");
                }
            } else if (!(this instanceof InstrumentationProvider)) {
                MediaProvider mediaProvider = (MediaProvider) this;
                AnonymousClass01J r13 = (AnonymousClass01J) AnonymousClass01M.A00(mediaProvider.getContext(), AnonymousClass01J.class);
                mediaProvider.A03 = r13.Aen();
                mediaProvider.A00 = (C14330lG) r13.A7B.get();
                mediaProvider.A01 = (C15550nR) r13.A45.get();
                mediaProvider.A02 = (C15610nY) r13.AMe.get();
                mediaProvider.A04 = r13.Ag8();
                mediaProvider.A06 = (C15650ng) r13.A4m.get();
                mediaProvider.A07 = (C15660nh) r13.ABE.get();
                mediaProvider.A08 = (C15670ni) r13.AIb.get();
                mediaProvider.A05 = (C15680nj) r13.A4e.get();
                mediaProvider.A09 = (C15690nk) r13.A74.get();
            } else {
                InstrumentationProvider instrumentationProvider = (InstrumentationProvider) this;
                Context context2 = instrumentationProvider.getContext();
                if (context2 != null) {
                    AnonymousClass01J r14 = (AnonymousClass01J) AnonymousClass01M.A00(context2, AnonymousClass01J.class);
                    instrumentationProvider.A02 = (C15520nO) r14.A9z.get();
                    instrumentationProvider.A00 = (C15440nG) r14.A9q.get();
                    instrumentationProvider.A03 = (C15490nL) r14.AA0.get();
                    instrumentationProvider.A04 = (C15510nN) r14.AHZ.get();
                    instrumentationProvider.A01 = (C15530nP) r14.A4U.get();
                } else {
                    throw new IllegalStateException("Context is not attached.");
                }
            }
            this.A00 = true;
        }
    }

    @Override // android.content.ContentProvider
    public final boolean onCreate() {
        return true;
    }
}
