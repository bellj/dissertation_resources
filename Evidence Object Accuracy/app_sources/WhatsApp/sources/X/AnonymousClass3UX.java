package X;

/* renamed from: X.3UX  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3UX implements AnonymousClass5TH {
    public final /* synthetic */ long A00;
    public final /* synthetic */ C63293Ba A01;

    public AnonymousClass3UX(C63293Ba r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        if (r8 != 9) goto L_0x002f;
     */
    @Override // X.AnonymousClass5TH
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AOM(int r8) {
        /*
            r7 = this;
            r0 = 5
            boolean r3 = X.C12960it.A1V(r8, r0)
            X.3Ba r2 = r7.A01
            long r5 = java.lang.System.currentTimeMillis()
            long r0 = r7.A00
            long r5 = r5 - r0
            if (r3 == 0) goto L_0x001c
            X.12f r3 = r2.A03
            r2 = 0
            java.lang.Long r1 = java.lang.Long.valueOf(r5)
            r0 = 2
            r3.A03(r2, r1, r2, r0)
            return
        L_0x001c:
            X.12f r4 = r2.A03
            r3 = 3
            r1 = 2
            if (r8 == r1) goto L_0x0044
            if (r8 == r3) goto L_0x0044
            r0 = 7
            if (r8 == r0) goto L_0x0042
            r0 = 8
            if (r8 == r0) goto L_0x0044
            r0 = 9
            if (r8 == r0) goto L_0x0044
        L_0x002f:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            java.lang.String r1 = X.AnonymousClass4EW.A00(r8)
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            r4.A03(r2, r0, r1, r3)
            r0 = 0
            r4.A01 = r0
            return
        L_0x0042:
            r1 = 0
            goto L_0x002f
        L_0x0044:
            r1 = 1
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3UX.AOM(int):void");
    }
}
