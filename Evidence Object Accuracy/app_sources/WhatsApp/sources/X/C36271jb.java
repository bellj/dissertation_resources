package X;

import android.app.Activity;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.R;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import com.whatsapp.mentions.MentionableEntry;

/* renamed from: X.1jb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36271jb {
    public final View A00;
    public final View A01;
    public final ViewTreeObserver.OnGlobalLayoutListener A02;
    public final ImageButton A03;
    public final AbstractC116455Vm A04;
    public final C15270mq A05;
    public final C15330mx A06;
    public final MentionableEntry A07;
    public final C252718t A08;

    public C36271jb(Activity activity, View view, AbstractC15710nm r33, AnonymousClass01d r34, C14820m6 r35, AnonymousClass018 r36, AnonymousClass19M r37, C231510o r38, AnonymousClass193 r39, AbstractC14640lm r40, C16630pM r41, C252718t r42) {
        C1096752p r1 = new C1096752p(this);
        this.A04 = r1;
        AnonymousClass3NO r2 = new AnonymousClass3NO(this);
        this.A02 = r2;
        this.A01 = view;
        this.A08 = r42;
        this.A00 = view.findViewById(R.id.emoji_btn_holder);
        MentionableEntry mentionableEntry = (MentionableEntry) view.findViewById(R.id.comment);
        this.A07 = mentionableEntry;
        mentionableEntry.setInputEnterAction(6);
        mentionableEntry.setFilters(new InputFilter[]{new C100654mG(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)});
        mentionableEntry.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: X.4pR
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                C36271jb r22 = C36271jb.this;
                if (keyEvent == null || keyEvent.getKeyCode() != 66) {
                    return false;
                }
                r22.A07.A03();
                return true;
            }
        });
        mentionableEntry.addTextChangedListener(new AnonymousClass367(mentionableEntry, (TextView) view.findViewById(R.id.counter), r34, r36, r37, r41, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 30, true));
        if (C15380n4.A0J(r40)) {
            mentionableEntry.A0D((ViewGroup) AnonymousClass028.A0D(view, R.id.mention_attach), C15580nU.A02(r40), false, true, true);
        }
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.emoji_picker_btn);
        this.A03 = imageButton;
        C15270mq r6 = new C15270mq(activity, imageButton, r33, (AbstractC49822Mw) activity.findViewById(R.id.main), mentionableEntry, r34, r35, r36, r37, r38, r39, r41, r42);
        this.A05 = r6;
        C15330mx r5 = new C15330mx(activity, r36, r37, r6, r38, (EmojiSearchContainer) view.findViewById(R.id.emoji_search_container), r41);
        this.A06 = r5;
        r5.A00 = new AbstractC14020ki() { // from class: X.56j
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r3) {
                C36271jb.this.A04.APc(r3.A00);
            }
        };
        r6.A0C(r1);
        r6.A0E = new RunnableBRunnable0Shape7S0100000_I0_7(this, 32);
        view.getViewTreeObserver().addOnGlobalLayoutListener(r2);
    }
}
