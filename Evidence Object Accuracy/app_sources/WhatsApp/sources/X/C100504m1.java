package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4m1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100504m1 implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(24);
    public final int A00;
    public final long A01;
    public final long A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C100504m1(int i, long j, long j2) {
        C95314dV.A03(j < j2);
        this.A02 = j;
        this.A01 = j2;
        this.A00 = i;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C100504m1.class != obj.getClass()) {
                return false;
            }
            C100504m1 r7 = (C100504m1) obj;
            if (!(this.A02 == r7.A02 && this.A01 == r7.A01 && this.A00 == r7.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(this.A02);
        objArr[1] = Long.valueOf(this.A01);
        C12990iw.A1V(objArr, this.A00);
        return Arrays.hashCode(objArr);
    }

    @Override // java.lang.Object
    public String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(this.A02);
        objArr[1] = Long.valueOf(this.A01);
        C12990iw.A1V(objArr, this.A00);
        return C72463ee.A0G("Segment: startTimeMs=%d, endTimeMs=%d, speedDivisor=%d", objArr);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A02);
        parcel.writeLong(this.A01);
        parcel.writeInt(this.A00);
    }
}
