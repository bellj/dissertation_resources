package X;

import android.graphics.Matrix;
import android.graphics.PointF;

/* renamed from: X.0QD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QD {
    public AnonymousClass0QR A00;
    public AnonymousClass0QR A01;
    public AnonymousClass0QR A02;
    public AnonymousClass0QR A03;
    public AnonymousClass0QR A04;
    public AnonymousClass0QR A05;
    public AnonymousClass0QR A06;
    public AnonymousClass0H1 A07;
    public AnonymousClass0H1 A08;
    public final Matrix A09 = new Matrix();
    public final Matrix A0A;
    public final Matrix A0B;
    public final Matrix A0C;
    public final float[] A0D;

    public AnonymousClass0QD(C08170ah r4) {
        AnonymousClass0QR A88;
        AnonymousClass0QR A882;
        AnonymousClass0Gx r1;
        AnonymousClass0H1 r12;
        AnonymousClass0H1 r13;
        AnonymousClass0H1 r14;
        C08130ad r0 = r4.A06;
        if (r0 == null) {
            A88 = null;
        } else {
            A88 = r0.A88();
        }
        this.A00 = A88;
        AbstractC12590iA r02 = r4.A08;
        if (r02 == null) {
            A882 = null;
        } else {
            A882 = r02.A88();
        }
        this.A03 = A882;
        AnonymousClass0HB r03 = r4.A07;
        if (r03 == null) {
            r1 = null;
        } else {
            r1 = new AnonymousClass0Gx(r03.A00);
        }
        this.A05 = r1;
        AnonymousClass0H9 r04 = r4.A01;
        if (r04 == null) {
            r12 = null;
        } else {
            r12 = new AnonymousClass0H1(r04.A00);
        }
        this.A04 = r12;
        AnonymousClass0H9 r05 = r4.A02;
        if (r05 == null) {
            r13 = null;
        } else {
            r13 = new AnonymousClass0H1(r05.A00);
        }
        this.A07 = r13;
        if (r13 != null) {
            this.A0A = new Matrix();
            this.A0B = new Matrix();
            this.A0C = new Matrix();
            this.A0D = new float[9];
        }
        AnonymousClass0H9 r06 = r4.A03;
        if (r06 == null) {
            r14 = null;
        } else {
            r14 = new AnonymousClass0H1(r06.A00);
        }
        this.A08 = r14;
        AnonymousClass0HA r07 = r4.A05;
        if (r07 != null) {
            this.A02 = new AnonymousClass0H0(r07.A00);
        }
        AnonymousClass0H9 r08 = r4.A04;
        if (r08 != null) {
            this.A06 = new AnonymousClass0H1(r08.A00);
        } else {
            this.A06 = null;
        }
        AnonymousClass0H9 r09 = r4.A00;
        if (r09 != null) {
            this.A01 = new AnonymousClass0H1(r09.A00);
        } else {
            this.A01 = null;
        }
    }

    public Matrix A00() {
        float cos;
        float sin;
        float[] fArr;
        float A09;
        Matrix matrix = this.A09;
        matrix.reset();
        AnonymousClass0QR r0 = this.A03;
        if (r0 != null) {
            PointF pointF = (PointF) r0.A03();
            float f = pointF.x;
            if (!(f == 0.0f && pointF.y == 0.0f)) {
                matrix.preTranslate(f, pointF.y);
            }
        }
        AnonymousClass0QR r1 = this.A04;
        if (r1 != null) {
            if (r1 instanceof AnonymousClass0Gu) {
                A09 = ((Number) r1.A03()).floatValue();
            } else {
                A09 = ((AnonymousClass0H1) r1).A09();
            }
            if (A09 != 0.0f) {
                matrix.preRotate(A09);
            }
        }
        AnonymousClass0H1 r2 = this.A07;
        if (r2 != null) {
            AnonymousClass0H1 r5 = this.A08;
            if (r5 == null) {
                cos = 0.0f;
                sin = 1.0f;
            } else {
                cos = (float) Math.cos(Math.toRadians((double) ((-r5.A09()) + 90.0f)));
                sin = (float) Math.sin(Math.toRadians((double) ((-r5.A09()) + 90.0f)));
            }
            float tan = (float) Math.tan(Math.toRadians((double) r2.A09()));
            int i = 0;
            do {
                fArr = this.A0D;
                fArr[i] = 0.0f;
                i++;
            } while (i < 9);
            fArr[0] = cos;
            fArr[1] = sin;
            float f2 = -sin;
            fArr[3] = f2;
            fArr[4] = cos;
            fArr[8] = 1.0f;
            Matrix matrix2 = this.A0A;
            matrix2.setValues(fArr);
            int i2 = 0;
            do {
                fArr[i2] = 0.0f;
                i2++;
            } while (i2 < 9);
            fArr[0] = 1.0f;
            fArr[3] = tan;
            fArr[4] = 1.0f;
            fArr[8] = 1.0f;
            Matrix matrix3 = this.A0B;
            matrix3.setValues(fArr);
            int i3 = 0;
            do {
                fArr[i3] = 0.0f;
                i3++;
            } while (i3 < 9);
            fArr[0] = cos;
            fArr[1] = f2;
            fArr[3] = sin;
            fArr[4] = cos;
            fArr[8] = 1.0f;
            Matrix matrix4 = this.A0C;
            matrix4.setValues(fArr);
            matrix3.preConcat(matrix2);
            matrix4.preConcat(matrix3);
            matrix.preConcat(matrix4);
        }
        AnonymousClass0QR r02 = this.A05;
        if (r02 != null) {
            AnonymousClass0OM r22 = (AnonymousClass0OM) r02.A03();
            float f3 = r22.A00;
            if (!(f3 == 1.0f && r22.A01 == 1.0f)) {
                matrix.preScale(f3, r22.A01);
            }
        }
        AnonymousClass0QR r03 = this.A00;
        if (r03 != null) {
            PointF pointF2 = (PointF) r03.A03();
            float f4 = pointF2.x;
            if (!(f4 == 0.0f && pointF2.y == 0.0f)) {
                matrix.preTranslate(-f4, -pointF2.y);
            }
        }
        return matrix;
    }

    public Matrix A01(float f) {
        PointF pointF;
        AnonymousClass0OM r1;
        float f2;
        AnonymousClass0QR r0 = this.A03;
        PointF pointF2 = null;
        if (r0 == null) {
            pointF = null;
        } else {
            pointF = (PointF) r0.A03();
        }
        AnonymousClass0QR r12 = this.A05;
        if (r12 == null) {
            r1 = null;
        } else {
            r1 = (AnonymousClass0OM) r12.A03();
        }
        Matrix matrix = this.A09;
        matrix.reset();
        if (pointF != null) {
            matrix.preTranslate(pointF.x * f, pointF.y * f);
        }
        if (r1 != null) {
            double d = (double) f;
            matrix.preScale((float) Math.pow((double) r1.A00, d), (float) Math.pow((double) r1.A01, d));
        }
        AnonymousClass0QR r02 = this.A04;
        if (r02 != null) {
            float floatValue = ((Number) r02.A03()).floatValue();
            AnonymousClass0QR r03 = this.A00;
            if (r03 != null) {
                pointF2 = (PointF) r03.A03();
            }
            float f3 = floatValue * f;
            float f4 = 0.0f;
            if (pointF2 == null) {
                f2 = 0.0f;
            } else {
                f2 = pointF2.x;
                f4 = pointF2.y;
            }
            matrix.preRotate(f3, f2, f4);
        }
        return matrix;
    }

    public void A02(AbstractC12030hG r2) {
        AnonymousClass0QR r0 = this.A02;
        if (r0 != null) {
            r0.A07.add(r2);
        }
        AnonymousClass0QR r02 = this.A06;
        if (r02 != null) {
            r02.A07.add(r2);
        }
        AnonymousClass0QR r03 = this.A01;
        if (r03 != null) {
            r03.A07.add(r2);
        }
        AnonymousClass0QR r04 = this.A00;
        if (r04 != null) {
            r04.A07.add(r2);
        }
        AnonymousClass0QR r05 = this.A03;
        if (r05 != null) {
            r05.A07.add(r2);
        }
        AnonymousClass0QR r06 = this.A05;
        if (r06 != null) {
            r06.A07.add(r2);
        }
        AnonymousClass0QR r07 = this.A04;
        if (r07 != null) {
            r07.A07.add(r2);
        }
        AnonymousClass0H1 r08 = this.A07;
        if (r08 != null) {
            r08.A07.add(r2);
        }
        AnonymousClass0H1 r09 = this.A08;
        if (r09 != null) {
            r09.A07.add(r2);
        }
    }

    public void A03(AbstractC08070aX r2) {
        r2.A03(this.A02);
        r2.A03(this.A06);
        r2.A03(this.A01);
        r2.A03(this.A00);
        r2.A03(this.A03);
        r2.A03(this.A05);
        r2.A03(this.A04);
        r2.A03(this.A07);
        r2.A03(this.A08);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A04(X.AnonymousClass0SF r4, java.lang.Object r5) {
        /*
        // Method dump skipped, instructions count: 204
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0QD.A04(X.0SF, java.lang.Object):boolean");
    }
}
