package X;

import com.whatsapp.jid.GroupJid;

/* renamed from: X.1E5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1E5 {
    public final C15550nR A00;
    public final C19990v2 A01;
    public final C14850m9 A02;

    public AnonymousClass1E5(C15550nR r1, C19990v2 r2, C14850m9 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public boolean A00(C15370n3 r5) {
        int A02 = this.A01.A02((GroupJid) r5.A0B(C15580nU.class));
        if (!r5.A0K()) {
            return false;
        }
        if ((A02 == 3 || A02 == 1) && r5.A0a && this.A02.A07(1653)) {
            return true;
        }
        return false;
    }
}
