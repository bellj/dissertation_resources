package X;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import com.whatsapp.util.Log;

/* renamed from: X.2p2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58422p2 extends C58272oQ {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ C252818u A01;
    public final /* synthetic */ boolean A02 = true;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58422p2(Context context, Context context2, AnonymousClass12Q r10, C14900mE r11, C252818u r12, AnonymousClass01d r13, String str) {
        super(context, r10, r11, r13, str);
        this.A01 = r12;
        this.A00 = context2;
    }

    @Override // X.C58272oQ, X.AbstractC116465Vn
    public void onClick(View view) {
        String str;
        StringBuilder A0k = C12960it.A0k("wa-link-factory/click-link ");
        String str2 = this.A09;
        Log.i(C12960it.A0d(str2, A0k));
        String A0t = C12970iu.A0t(str2, C252818u.A05);
        if (A0t != null) {
            Uri parse = Uri.parse(A0t);
            if (parse.getAuthority().contains("whatsapp")) {
                Uri.Builder buildUpon = parse.buildUpon();
                AnonymousClass018 r2 = this.A01.A03;
                buildUpon.appendQueryParameter("lg", r2.A06());
                buildUpon.appendQueryParameter("lc", r2.A05());
                if (this.A02) {
                    str = "1";
                } else {
                    str = "0";
                }
                buildUpon.appendQueryParameter("eea", str);
                parse = buildUpon.build();
            }
            Log.i(C12960it.A0b("wa-link-factory/open-link ", parse));
            this.A01.A00.Ab9(this.A00, parse);
        }
    }
}
