package X;

import android.graphics.RectF;

/* renamed from: X.5JA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5JA extends AnonymousClass1WI implements AnonymousClass1WK {
    public AnonymousClass5JA() {
        super(0);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        return new RectF();
    }
}
