package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Ls  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65953Ls implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(68);
    public final long A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C65953Ls(Parcel parcel) {
        this.A01 = C12990iw.A0p(parcel);
        this.A02 = C12990iw.A0p(parcel);
        this.A00 = parcel.readLong();
    }

    public C65953Ls(String str, String str2, long j) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = j;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeLong(this.A00);
    }
}
