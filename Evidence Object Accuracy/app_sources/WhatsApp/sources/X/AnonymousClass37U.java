package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37U extends AbstractC16350or {
    public WeakReference A00;
    public final C22100yW A01;
    public final AnonymousClass139 A02;
    public final C14860mA A03;

    public AnonymousClass37U(AbstractC115995Ts r2, C22100yW r3, AnonymousClass139 r4, C14860mA r5) {
        this.A03 = r5;
        this.A01 = r3;
        this.A02 = r4;
        this.A00 = C12970iu.A10(r2);
    }
}
