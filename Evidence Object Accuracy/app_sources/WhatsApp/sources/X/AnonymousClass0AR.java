package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;

/* renamed from: X.0AR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0AR extends Handler {
    public final /* synthetic */ C06380Tj A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0AR(Looper looper, C06380Tj r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int size;
        C04760My[] r7;
        if (message.what != 1) {
            super.handleMessage(message);
            return;
        }
        C06380Tj r9 = this.A00;
        while (true) {
            synchronized (r9.A04) {
                ArrayList arrayList = r9.A02;
                size = arrayList.size();
                if (size > 0) {
                    r7 = new C04760My[size];
                    arrayList.toArray(r7);
                    arrayList.clear();
                } else {
                    return;
                }
            }
            int i = 0;
            do {
                C04760My r5 = r7[i];
                int size2 = r5.A01.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    AnonymousClass0OZ r1 = (AnonymousClass0OZ) r5.A01.get(i2);
                    if (!r1.A01) {
                        r1.A02.onReceive(r9.A00, r5.A00);
                    }
                }
                i++;
            } while (i < size);
        }
    }
}
