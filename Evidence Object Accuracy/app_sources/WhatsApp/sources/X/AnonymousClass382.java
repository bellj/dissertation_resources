package X;

import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.382  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass382 extends AbstractC16350or {
    public final /* synthetic */ long A00;
    public final /* synthetic */ C16170oZ A01;
    public final /* synthetic */ C14820m6 A02;
    public final /* synthetic */ C21320xE A03;
    public final /* synthetic */ WeakReference A04;
    public final /* synthetic */ List A05;
    public final /* synthetic */ boolean A06;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass382(AbstractC001200n r1, C16170oZ r2, C14820m6 r3, C21320xE r4, WeakReference weakReference, List list, long j, boolean z) {
        super(r1);
        this.A05 = list;
        this.A01 = r2;
        this.A06 = z;
        this.A00 = j;
        this.A04 = weakReference;
        this.A02 = r3;
        this.A03 = r4;
    }
}
