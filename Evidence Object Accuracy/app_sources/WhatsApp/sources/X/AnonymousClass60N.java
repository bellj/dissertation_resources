package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.60N  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass60N {
    public final UserJid A00;
    public final C1315963j A01;
    public final EnumC124545pi A02;
    public final C16380ov A03;
    public final Boolean A04;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass60N) {
                AnonymousClass60N r5 = (AnonymousClass60N) obj;
                if (!C16700pc.A0O(this.A04, r5.A04) || !C16700pc.A0O(this.A01, r5.A01) || !C16700pc.A0O(this.A03, r5.A03) || !C16700pc.A0O(this.A00, r5.A00) || this.A02 != r5.A02) {
                }
            }
            return false;
        }
        return true;
    }

    public AnonymousClass60N() {
        this(null, null, EnumC124545pi.A03, null, null);
    }

    public AnonymousClass60N(UserJid userJid, C1315963j r2, EnumC124545pi r3, C16380ov r4, Boolean bool) {
        this.A04 = bool;
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = userJid;
        this.A02 = r3;
    }

    public int hashCode() {
        int i = 0;
        int A0D = ((((((C72453ed.A0D(this.A04) * 31) + C72453ed.A0D(this.A01)) * 31) + C72453ed.A0D(this.A03)) * 31) + C72453ed.A0D(this.A00)) * 31;
        EnumC124545pi r0 = this.A02;
        if (r0 != null) {
            i = r0.hashCode();
        }
        return A0D + i;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CheckoutData(shouldShowShimmer=");
        A0k.append(this.A04);
        A0k.append(", error=");
        A0k.append(this.A01);
        A0k.append(", orderMessage=");
        A0k.append(this.A03);
        A0k.append(", merchantJid=");
        A0k.append(this.A00);
        A0k.append(", merchantPaymentAccountStatus=");
        A0k.append(this.A02);
        return C12970iu.A0u(A0k);
    }
}
