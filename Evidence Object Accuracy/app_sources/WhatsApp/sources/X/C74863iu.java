package X;

/* renamed from: X.3iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74863iu extends AnonymousClass0QE {
    public final /* synthetic */ C54692h8 A00;

    public C74863iu(C54692h8 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0QE
    public void A01(int i, int i2) {
        C54692h8.A00(this.A00, i, i2);
    }

    @Override // X.AnonymousClass0QE
    public void A02(int i, int i2) {
        C54692h8.A00(this.A00, i, i2);
    }

    @Override // X.AnonymousClass0QE
    public void A03(int i, int i2) {
        C54692h8.A00(this.A00, i, i2);
    }

    @Override // X.AnonymousClass0QE
    public void A04(int i, int i2, int i3) {
        C54692h8 r0 = this.A00;
        C54692h8.A00(r0, i, 1);
        C54692h8.A00(r0, i2, 1);
    }
}
