package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;

/* renamed from: X.1JV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JV extends GroupJid implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100034lG();

    @Override // com.whatsapp.jid.Jid
    public String getServer() {
        return "temp";
    }

    @Override // com.whatsapp.jid.Jid
    public int getType() {
        return 2;
    }

    public AnonymousClass1JV(Parcel parcel) {
        super(parcel);
    }

    public AnonymousClass1JV(String str) {
        super(str);
        if (!str.contains("-")) {
            StringBuilder sb = new StringBuilder("Invalid group id: ");
            sb.append(str);
            throw new AnonymousClass1MW(sb.toString());
        }
    }

    public static AnonymousClass1JV A02(C15570nT r1, String str) {
        r1.A08();
        C27631Ih r0 = r1.A05;
        AnonymousClass009.A05(r0);
        StringBuilder sb = new StringBuilder();
        String str2 = r0.user;
        AnonymousClass009.A05(str2);
        sb.append(str2);
        sb.append("-");
        sb.append(str);
        sb.append("@");
        sb.append("temp");
        return A03(sb.toString());
    }

    public static AnonymousClass1JV A03(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            Jid jid = Jid.get(str);
            if (jid instanceof AnonymousClass1JV) {
                return (AnonymousClass1JV) jid;
            }
            throw new AnonymousClass1MW(str);
        } catch (AnonymousClass1MW unused) {
            return null;
        }
    }

    @Override // com.whatsapp.jid.Jid
    public String getObfuscatedString() {
        return getRawString().substring(getRawString().indexOf("-") + 1);
    }
}
