package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.4QC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QC {
    public final Context A00;
    public final ViewGroup A01;
    public final ImageView A02;

    public AnonymousClass4QC(Context context, ViewGroup viewGroup) {
        this.A01 = viewGroup;
        this.A00 = context;
        this.A02 = C12970iu.A0K(viewGroup, R.id.avatar_tab_icon);
    }
}
