package X;

/* renamed from: X.0SH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SH {
    public static final byte[] A04 = new byte[1792];
    public char A00;
    public int A01;
    public final int A02;
    public final CharSequence A03;

    static {
        int i = 0;
        do {
            A04[i] = Character.getDirectionality(i);
            i++;
        } while (i < 1792);
    }

    public AnonymousClass0SH(CharSequence charSequence) {
        this.A03 = charSequence;
        this.A02 = charSequence.length();
    }

    public byte A00() {
        CharSequence charSequence = this.A03;
        char charAt = charSequence.charAt(this.A01 - 1);
        this.A00 = charAt;
        boolean isLowSurrogate = Character.isLowSurrogate(charAt);
        int i = this.A01;
        if (isLowSurrogate) {
            int codePointBefore = Character.codePointBefore(charSequence, i);
            this.A01 -= Character.charCount(codePointBefore);
            return Character.getDirectionality(codePointBefore);
        }
        this.A01 = i - 1;
        char c = this.A00;
        return c < 1792 ? A04[c] : Character.getDirectionality(c);
    }
}
