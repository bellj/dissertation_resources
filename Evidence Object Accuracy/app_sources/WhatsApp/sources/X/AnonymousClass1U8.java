package X;

import android.content.Context;
import android.os.Looper;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

@Deprecated
/* renamed from: X.1U8  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1U8 {
    public static final Set A00 = Collections.newSetFromMap(new WeakHashMap());

    public abstract Context A02();

    public abstract Looper A03();

    public abstract AnonymousClass1UI A05(AnonymousClass1UI v);

    public abstract AnonymousClass1UI A06(AnonymousClass1UI v);

    public abstract void A08();

    public abstract void A09();

    public abstract boolean A0A();

    public AbstractC72443eb A04(AnonymousClass4DN r2) {
        throw new UnsupportedOperationException();
    }

    public void A07() {
        throw new UnsupportedOperationException();
    }

    public boolean A0B(AnonymousClass5QY r2) {
        throw new UnsupportedOperationException();
    }
}
