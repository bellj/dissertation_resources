package X;

/* renamed from: X.4bR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94214bR {
    public static final C94214bR A01 = new C94214bR(false);
    public final boolean A00;

    public C94214bR(boolean z) {
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && C94214bR.class == obj.getClass() && this.A00 == ((C94214bR) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return !this.A00 ? 1 : 0;
    }
}
