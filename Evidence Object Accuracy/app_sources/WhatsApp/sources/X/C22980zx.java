package X;

import android.text.TextUtils;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Locale;

/* renamed from: X.0zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22980zx {
    public final C15570nT A00;
    public final C15650ng A01;
    public final AnonymousClass102 A02;
    public final C14850m9 A03;
    public final C18600si A04;
    public final C17900ra A05;
    public final C17070qD A06;
    public final AnonymousClass206 A07;
    public final AnonymousClass208 A08;

    public C22980zx(AbstractC15710nm r4, C15570nT r5, C15650ng r6, AnonymousClass102 r7, C14850m9 r8, C18600si r9, C17900ra r10, C17070qD r11) {
        this.A03 = r8;
        this.A00 = r5;
        this.A06 = r11;
        this.A01 = r6;
        this.A04 = r9;
        this.A02 = r7;
        this.A05 = r10;
        AnonymousClass205 r2 = new AnonymousClass205();
        this.A07 = new AnonymousClass206(null, r2, null);
        this.A08 = new AnonymousClass208(r4, r8, r2, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        if ("true".equals(r1) != false) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass20A A00(X.AnonymousClass1V8 r5, java.lang.String r6) {
        /*
            X.20A r4 = new X.20A
            r4.<init>()
            if (r5 == 0) goto L_0x0040
            X.1W9[] r5 = r5.A0L()
            if (r5 == 0) goto L_0x0040
            r3 = 0
        L_0x000e:
            int r0 = r5.length
            if (r3 >= r0) goto L_0x0040
            r0 = r5[r3]
            java.lang.String r2 = r0.A02
            java.lang.String r1 = r0.A03
            boolean r0 = r6.equals(r2)
            if (r0 == 0) goto L_0x0022
            r4.A00 = r1
        L_0x001f:
            int r3 = r3 + 1
            goto L_0x000e
        L_0x0022:
            java.lang.String r0 = "last"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x001f
            java.lang.String r0 = "1"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x003c
            java.lang.String r0 = "true"
            boolean r1 = r0.equals(r1)
            r0 = 0
            if (r1 == 0) goto L_0x003d
        L_0x003c:
            r0 = 1
        L_0x003d:
            r4.A02 = r0
            goto L_0x001f
        L_0x0040:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22980zx.A00(X.1V8, java.lang.String):X.20A");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0135, code lost:
        if (r0 != false) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x016f, code lost:
        if (r13 != false) goto L_0x0171;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0177, code lost:
        if (r11 != false) goto L_0x0179;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC28901Pl A01(X.AnonymousClass1V8 r28) {
        /*
        // Method dump skipped, instructions count: 854
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22980zx.A01(X.1V8):X.1Pl");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0307, code lost:
        if (r2 == false) goto L_0x02a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x012a, code lost:
        if ("PARTIAL".equalsIgnoreCase(r5) == false) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x018a, code lost:
        if (r11.A00.doubleValue() > 0.0d) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01bd, code lost:
        if (r4 == false) goto L_0x01bf;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0286  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02a9  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02ab  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x02cc  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x02cf  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02d2  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x02dd  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x02e7  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x030a  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0277  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1IR A02(com.whatsapp.jid.UserJid r62, com.whatsapp.jid.UserJid r63, X.AnonymousClass20B r64, X.AnonymousClass1V8 r65, java.lang.String r66, boolean r67) {
        /*
        // Method dump skipped, instructions count: 1520
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22980zx.A02(com.whatsapp.jid.UserJid, com.whatsapp.jid.UserJid, X.20B, X.1V8, java.lang.String, boolean):X.1IR");
    }

    public AnonymousClass1IR A03(UserJid userJid, UserJid userJid2, AnonymousClass1V8 r23, long j) {
        String A0I = r23.A0I("country", this.A05.A01().A03);
        AbstractC16830pp r2 = null;
        int A00 = C28421Nd.A00(r23.A0I("version", null), 1);
        String A0I2 = r23.A0I("request-id", null);
        AnonymousClass009.A05(A0I2);
        String A0I3 = r23.A0I("expiry-ts", null);
        AnonymousClass009.A05(A0I3);
        UserJid nullable = UserJid.getNullable(r23.A0I("sender", null));
        if (nullable == null) {
            nullable = userJid;
        }
        AnonymousClass009.A05(A0I);
        String str = ((AbstractC30781Yu) C30771Yt.A06).A04;
        AbstractC38041nQ A01 = this.A06.A01(A0I);
        if (A01 != null) {
            r2 = A01.AFX(str);
        }
        AnonymousClass009.A05(r2);
        AbstractC30791Yv A02 = this.A02.A02(str);
        AnonymousClass009.A05(nullable);
        AnonymousClass1IR A022 = C31001Zq.A02(A02, new C30821Yy(new BigDecimal(0), 1), nullable, userJid2, str, A0I2, A0I, 20, 12, A00, r2.AGi(), 0, j);
        AbstractC30891Zf AIk = r2.AIk();
        if (AIk != null) {
            A022.A03(AIk, C28421Nd.A01(A0I3, j / 1000) * 1000);
        }
        return A022;
    }

    public AnonymousClass1IR A04(UserJid userJid, AnonymousClass1V8 r20, long j) {
        boolean z;
        String A0I;
        C17930rd r0;
        int i;
        C30821Yy A00;
        int i2;
        AbstractC16830pp AFX;
        int A002 = C28421Nd.A00(r20.A0I("version", null), 1);
        String A0I2 = r20.A0I("sync-status", null);
        if (TextUtils.isEmpty(A0I2) || !"PARTIAL".equalsIgnoreCase(A0I2)) {
            z = false;
            A0I = r20.A0I("currency", null);
            r0 = C17930rd.A0F;
        } else {
            z = true;
            A0I = ((AbstractC30781Yu) C30771Yt.A06).A04;
            r0 = C17930rd.A0E;
        }
        String A0I3 = r20.A0I("country", r0.A03);
        String A0I4 = r20.A0I("amount", null);
        UserJid nullable = UserJid.getNullable(r20.A0I("receiver", null));
        AnonymousClass009.A05(nullable);
        String str = "UNSET";
        if (str.equals(A0I3)) {
            String str2 = A0I;
            if (this.A06.A01 != null) {
                if (A0I != null) {
                    str2 = A0I.toUpperCase(Locale.US);
                }
                if (!TextUtils.isEmpty(str2)) {
                    if (str2.equals("BRL")) {
                        str = "BR";
                    } else if (str2.equals("INR")) {
                        str = "IN";
                    }
                }
            }
            A0I3 = str;
        }
        AbstractC38041nQ A01 = this.A06.A01(A0I3);
        if (A01 == null || (AFX = A01.AFX(A0I)) == null) {
            i = 0;
        } else {
            i = AFX.AGi();
        }
        if (TextUtils.isEmpty(A0I)) {
            return null;
        }
        AbstractC30791Yv A02 = this.A02.A02(A0I);
        if (z) {
            A00 = null;
            i2 = 1000;
        } else {
            A00 = C30821Yy.A00(A0I4, ((AbstractC30781Yu) A02).A01);
            i2 = 3;
        }
        AnonymousClass1IR A022 = C31001Zq.A02(A02, A00, userJid, nullable, A0I, null, A0I3, i2, 0, A002, i, 0, j);
        String A0I5 = r20.A0I("transaction-id", null);
        if (!TextUtils.isEmpty(A0I5)) {
            A022.A06(A0I5);
        }
        return A022;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        if (r6.equals(r0) == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1IR A05(X.AnonymousClass20B r9, X.AnonymousClass1V8 r10) {
        /*
            r8 = this;
            java.lang.String r0 = "sender"
            r1 = 0
            r5 = r10
            java.lang.String r0 = r10.A0I(r0, r1)
            com.whatsapp.jid.UserJid r2 = com.whatsapp.jid.UserJid.getNullable(r0)
            java.lang.String r0 = "receiver"
            java.lang.String r0 = r10.A0I(r0, r1)
            com.whatsapp.jid.UserJid r3 = com.whatsapp.jid.UserJid.getNullable(r0)
            java.lang.String r1 = "transaction-type"
            java.lang.String r0 = "p2p"
            java.lang.String r6 = r10.A0I(r1, r0)
            int r0 = r6.hashCode()
            r7 = 0
            r1 = r8
            switch(r0) {
                case -1703305877: goto L_0x0044;
                case -1629586251: goto L_0x0040;
                case -934813832: goto L_0x003d;
                case 1554454174: goto L_0x003a;
                default: goto L_0x0029;
            }
        L_0x0029:
            X.0nT r0 = r8.A00
            boolean r0 = r0.A0F(r2)
            if (r0 != 0) goto L_0x0033
            if (r3 != 0) goto L_0x0034
        L_0x0033:
            r7 = 1
        L_0x0034:
            r4 = r9
            X.1IR r0 = r1.A02(r2, r3, r4, r5, r6, r7)
            return r0
        L_0x003a:
            java.lang.String r0 = "deposit"
            goto L_0x0046
        L_0x003d:
            java.lang.String r0 = "refund"
            goto L_0x0046
        L_0x0040:
            java.lang.String r0 = "withdrawal"
            goto L_0x0046
        L_0x0044:
            java.lang.String r0 = "incentive"
        L_0x0046:
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0029
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22980zx.A05(X.20B, X.1V8):X.1IR");
    }

    public final AnonymousClass1IR A06(AnonymousClass1V8 r10, long j) {
        byte[] bArr;
        String A0I = r10.A0I("country", "IN");
        int A00 = C28421Nd.A00(r10.A0I("version", null), 1);
        try {
            AnonymousClass208 r2 = this.A08;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(DefaultCrypto.BUFFER_SIZE);
            r2.A06(r10, byteArrayOutputStream);
            bArr = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("PAY: PaymentsProtoParser serializeProtocolNode: ");
            sb.append(e);
            Log.e(sb.toString());
            bArr = null;
        }
        StringBuilder sb2 = new StringBuilder("PAY PaymentsProtoParser: buildFuturePaymentFromPayNode: futurePayment country=");
        sb2.append(A0I);
        sb2.append(" version=");
        sb2.append(A00);
        Log.i(sb2.toString());
        AnonymousClass1IR r3 = new AnonymousClass1IR(A0I, 5, A00, j);
        r3.A0R = bArr;
        return r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList A07(X.AnonymousClass1V8 r7) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x0058
            java.lang.String r1 = "wa-support-phone-number"
            r0 = 0
            java.lang.String r1 = r7.A0I(r1, r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0015
            X.0si r0 = r6.A04
            r0.A0I(r1)
        L_0x0015:
            X.1V8[] r0 = r7.A03
            if (r0 == 0) goto L_0x0058
            int r5 = r0.length
            if (r5 <= 0) goto L_0x0058
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r3 = 0
        L_0x0022:
            X.1V8 r2 = r7.A0D(r3)
            X.AnonymousClass009.A05(r2)
            java.lang.String r1 = r2.A00
            int r0 = r1.hashCode()
            switch(r0) {
                case -795192327: goto L_0x0045;
                case -505296440: goto L_0x0042;
                case 3016252: goto L_0x003f;
                case 3046160: goto L_0x003c;
                default: goto L_0x0032;
            }
        L_0x0032:
            java.lang.String r0 = "PAY: PaymentsProtoParser unset payment method"
            com.whatsapp.util.Log.w(r0)
        L_0x0037:
            int r3 = r3 + 1
            if (r3 >= r5) goto L_0x0059
            goto L_0x0022
        L_0x003c:
            java.lang.String r0 = "card"
            goto L_0x0048
        L_0x003f:
            java.lang.String r0 = "bank"
            goto L_0x0048
        L_0x0042:
            java.lang.String r0 = "merchant"
            goto L_0x0048
        L_0x0045:
            java.lang.String r0 = "wallet"
        L_0x0048:
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0032
            X.1Pl r0 = r6.A01(r2)
            if (r0 == 0) goto L_0x0037
            r4.add(r0)
            goto L_0x0037
        L_0x0058:
            r4 = 0
        L_0x0059:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22980zx.A07(X.1V8):java.util.ArrayList");
    }
}
