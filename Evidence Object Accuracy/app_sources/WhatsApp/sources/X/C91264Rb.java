package X;

import android.widget.TextView;

/* renamed from: X.4Rb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C91264Rb {
    public final TextView A00;
    public final AbstractC116035Tw A01;
    public final CharSequence A02;
    public final Object A03;

    public /* synthetic */ C91264Rb(TextView textView, AbstractC116035Tw r2, CharSequence charSequence, Object obj) {
        this.A02 = charSequence;
        this.A00 = textView;
        this.A03 = obj;
        this.A01 = r2;
    }
}
