package X;

import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.util.Log;

/* renamed from: X.2D6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2D6 extends AnonymousClass01A {
    public final AnonymousClass01A A00;

    public AnonymousClass2D6(AnonymousClass01A r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass01A
    public float A00(int i) {
        return this.A00.A00(i);
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        AnonymousClass01A r0 = this.A00;
        int A01 = r0.A01();
        int A012 = r0.A01();
        if (A01 < 214748364) {
            return A012 * 10;
        }
        return A012;
    }

    @Override // X.AnonymousClass01A
    public int A02(Object obj) {
        return this.A00.A02(obj);
    }

    @Override // X.AnonymousClass01A
    public Parcelable A03() {
        return this.A00.A03();
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        AnonymousClass01A r1 = this.A00;
        if (r1.A01() > 0) {
            return r1.A04(i % r1.A01());
        }
        Log.i("infinitepageadapter/getpagetitle/count is zero");
        return null;
    }

    @Override // X.AnonymousClass01A
    public Object A05(ViewGroup viewGroup, int i) {
        AnonymousClass01A r1 = this.A00;
        if (r1.A01() > 0) {
            return r1.A05(viewGroup, i % r1.A01());
        }
        Log.i("infinitepageadapter/instantiateitem/count is zero");
        return null;
    }

    @Override // X.AnonymousClass01A
    public void A06() {
        this.A00.A06();
    }

    @Override // X.AnonymousClass01A
    public void A07(DataSetObserver dataSetObserver) {
        this.A00.A07(dataSetObserver);
    }

    @Override // X.AnonymousClass01A
    public void A08(DataSetObserver dataSetObserver) {
        this.A00.A08(dataSetObserver);
    }

    @Override // X.AnonymousClass01A
    public void A09(Parcelable parcelable, ClassLoader classLoader) {
        this.A00.A09(parcelable, classLoader);
    }

    @Override // X.AnonymousClass01A
    public void A0A(ViewGroup viewGroup) {
        this.A00.A0A(viewGroup);
    }

    @Override // X.AnonymousClass01A
    public void A0B(ViewGroup viewGroup) {
        this.A00.A0B(viewGroup);
    }

    @Override // X.AnonymousClass01A
    public void A0C(ViewGroup viewGroup, Object obj, int i) {
        this.A00.A0C(viewGroup, obj, i);
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01A r1 = this.A00;
        if (r1.A01() <= 0) {
            Log.i("infinitepageadapter/destroyitem/count is zero");
        } else {
            r1.A0D(viewGroup, obj, i % r1.A01());
        }
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return this.A00.A0E(view, obj);
    }
}
