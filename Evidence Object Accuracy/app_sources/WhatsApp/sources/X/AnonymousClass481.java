package X;

/* renamed from: X.481  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass481 extends C236712o {
    public final /* synthetic */ AbstractC14670lq A00;

    public AnonymousClass481(AbstractC14670lq r1) {
        this.A00 = r1;
    }

    @Override // X.C236712o
    public void A00() {
        this.A00.A03();
    }

    @Override // X.C236712o
    public void A02(AnonymousClass1YT r3) {
        AbstractC14670lq r1 = this.A00;
        r1.A0R(r1.A0V());
    }
}
