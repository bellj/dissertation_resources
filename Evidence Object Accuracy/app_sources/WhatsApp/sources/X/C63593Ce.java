package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.3Ce  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63593Ce {
    public final AnonymousClass18T A00;
    public final C22710zW A01;

    public C63593Ce(AnonymousClass18T r1, C22710zW r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00(Context context, AbstractC14640lm r5, AnonymousClass1IS r6, String str, String str2, String str3, String str4, int i, boolean z) {
        Class AF7;
        AbstractC16830pp A02 = this.A00.A06.A02();
        if (z) {
            AF7 = A02.AFV();
        } else {
            AF7 = A02.AF7();
        }
        if (AF7 != null) {
            Intent A0D = C12990iw.A0D(context, AF7);
            C38211ni.A00(A0D, r6);
            A0D.putExtra("extra_order_id", str3);
            A0D.putExtra("extra_order_discount_program_name", str4);
            A0D.putExtra("extra_order_type", str2);
            A0D.putExtra("extra_transaction_type", "p2m");
            A0D.putExtra("extra_payment_config_id", str);
            A0D.putExtra("referral_screen", "order_details");
            if (i > 0) {
                A0D.putExtra("extra_payment_flow_entry_point", i);
            }
            A0D.setFlags(603979776);
            A0D.putExtra("extra_jid", r5.getRawString());
            context.startActivity(A0D);
        }
    }
}
