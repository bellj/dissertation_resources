package X;

import android.content.Context;
import android.widget.ArrayAdapter;

/* renamed from: X.0BO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BO extends ArrayAdapter {
    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public boolean hasStableIds() {
        return true;
    }

    public AnonymousClass0BO(Context context, CharSequence[] charSequenceArr, int i) {
        super(context, i, 16908308, charSequenceArr);
    }
}
