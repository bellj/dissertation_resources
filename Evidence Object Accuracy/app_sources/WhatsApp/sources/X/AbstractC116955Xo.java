package X;

/* renamed from: X.5Xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116955Xo {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K;
    public static final AnonymousClass1TK A0L;
    public static final AnonymousClass1TK A0M;
    public static final AnonymousClass1TK A0N;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("0.4.0.127.0.7");
        A00 = A12;
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("2.2.1", A12);
        A07 = A022;
        A08 = AnonymousClass1TL.A02("1", A022);
        A09 = AnonymousClass1TL.A02("2", A022);
        AnonymousClass1TK A023 = AnonymousClass1TL.A02("2.2.3", A12);
        A01 = A023;
        AnonymousClass1TK A024 = AnonymousClass1TL.A02("1", A023);
        A02 = A024;
        A03 = AnonymousClass1TL.A02("1", A024);
        AnonymousClass1TK A025 = AnonymousClass1TL.A02("2", A023);
        A04 = A025;
        A05 = AnonymousClass1TL.A02("1", A025);
        AnonymousClass1TK A026 = AnonymousClass1TL.A02("2.2.2", A12);
        A0A = A026;
        AnonymousClass1TK A027 = AnonymousClass1TL.A02("1", A026);
        A0H = A027;
        A0L = AnonymousClass1TL.A02("1", A027);
        A0M = AnonymousClass1TL.A02("2", A027);
        A0I = AnonymousClass1TL.A02("3", A027);
        A0J = AnonymousClass1TL.A02("4", A027);
        A0N = AnonymousClass1TL.A02("5", A027);
        A0K = AnonymousClass1TL.A02("6", A027);
        AnonymousClass1TK A028 = AnonymousClass1TL.A02("2", A026);
        A0B = A028;
        A0C = AnonymousClass1TL.A02("1", A028);
        A0D = AnonymousClass1TL.A02("2", A028);
        A0E = AnonymousClass1TL.A02("3", A028);
        A0F = AnonymousClass1TL.A02("4", A028);
        A0G = AnonymousClass1TL.A02("5", A028);
        A06 = AnonymousClass1TL.A02("3.1.2.1", A12);
    }
}
