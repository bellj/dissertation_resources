package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.4EP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4EP {
    public static String A00(int i) {
        switch (i) {
            case 0:
                return "success";
            case 1:
                return "cancel";
            case 2:
                return "failed_insufficient_space";
            case 3:
                return "failed_io";
            case 4:
                return "failed_oom";
            case 5:
                return "failed_bad_media";
            case 6:
                return "failed_no_permissions";
            case 7:
                return "failed_fnf";
            case 8:
                return "failed_server";
            case 9:
                return "failed_request";
            case 10:
                return "failed_request_timeout";
            case 11:
                return "failed_not_finalized";
            case 12:
                return "failed_optimistic_hash";
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return "failed_media_conn";
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return "failed_optimistic_network_unsafe";
            case 15:
                return "failed_throttle";
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return "failed_no_such_algorithm";
            case 17:
                return "failed_network";
            case 18:
                return "failed_watls";
            case 19:
                return "failed_url";
            case C43951xu.A01:
                return "failed_transcoding_unknown";
            case 21:
                return "failed_file_format_unsupported";
            case 22:
                return "failed_dns_lookup";
            case 23:
                return "failed_wamsys";
            case 24:
                return "failed_too_large";
            default:
                return "undefined";
        }
    }
}
