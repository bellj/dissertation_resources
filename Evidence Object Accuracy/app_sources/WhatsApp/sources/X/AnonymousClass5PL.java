package X;

import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;

/* renamed from: X.5PL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5PL extends AnonymousClass5IY implements AnonymousClass5S1 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public boolean A04;

    public AnonymousClass5PL(String str, AnonymousClass1TK r2, int i, int i2, int i3, int i4, boolean z) {
        super(str, r2);
        this.A04 = z;
        this.A03 = i;
        this.A00 = i2;
        this.A02 = i3;
        this.A01 = i4;
    }

    @Override // X.AnonymousClass5IY, javax.crypto.SecretKeyFactorySpi
    public SecretKey engineGenerateSecret(KeySpec keySpec) {
        int i;
        int i2;
        int i3;
        AnonymousClass20L r7;
        String str;
        AnonymousClass1TK r6;
        int i4;
        if (keySpec instanceof PBEKeySpec) {
            PBEKeySpec pBEKeySpec = (PBEKeySpec) keySpec;
            if (pBEKeySpec.getSalt() == null) {
                str = super.A00;
                r6 = super.A01;
                i = this.A03;
                i2 = this.A00;
                i3 = this.A02;
                i4 = this.A01;
                r7 = null;
            } else {
                boolean z = this.A04;
                i = this.A03;
                i2 = this.A00;
                i3 = this.A02;
                if (z) {
                    int i5 = this.A01;
                    AbstractC94944cn A01 = C95114dA.A01(i, i2);
                    byte[] A02 = C95114dA.A02(pBEKeySpec, i);
                    byte[] salt = pBEKeySpec.getSalt();
                    int iterationCount = pBEKeySpec.getIterationCount();
                    A01.A01 = A02;
                    A01.A02 = salt;
                    A01.A00 = iterationCount;
                    r7 = i5 != 0 ? A01.A04(i3, i5) : A01.A03(i3);
                    for (int i6 = 0; i6 != A02.length; i6++) {
                        A02[i6] = 0;
                    }
                } else {
                    AbstractC94944cn A012 = C95114dA.A01(i, i2);
                    byte[] A022 = C95114dA.A02(pBEKeySpec, i);
                    byte[] salt2 = pBEKeySpec.getSalt();
                    int iterationCount2 = pBEKeySpec.getIterationCount();
                    A012.A01 = A022;
                    A012.A02 = salt2;
                    A012.A00 = iterationCount2;
                    r7 = A012.A02(i3);
                    for (int i7 = 0; i7 != A022.length; i7++) {
                        A022[i7] = 0;
                    }
                }
                str = super.A00;
                r6 = super.A01;
                i4 = this.A01;
            }
            return new AnonymousClass5ED(str, pBEKeySpec, r6, r7, i, i2, i3, i4);
        }
        throw new InvalidKeySpecException("Invalid KeySpec");
    }
}
