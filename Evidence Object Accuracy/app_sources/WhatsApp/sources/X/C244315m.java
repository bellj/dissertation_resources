package X;

import com.whatsapp.util.Log;

/* renamed from: X.15m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C244315m {
    public static final byte[] A01 = {2};
    public static final byte[] A02 = {1};
    public final C17220qS A00;

    public C244315m(C17220qS r1) {
        this.A00 = r1;
    }

    public static /* synthetic */ void A00(AnonymousClass1V8 r4, AnonymousClass1OG r5, String str) {
        String str2;
        StringBuilder sb = new StringBuilder("EncryptedBackupProtocolHelper/processError id=");
        sb.append(str);
        Log.e(sb.toString());
        AnonymousClass1V8 A0E = r4.A0E("error");
        if (A0E == null) {
            StringBuilder sb2 = new StringBuilder("EncryptedBackupProtocolHelper/processError error node is empty, id=");
            sb2.append(str);
            Log.e(sb2.toString());
            str2 = "error node is empty";
        } else {
            String A0I = A0E.A0I("text", null);
            if (A0I == null) {
                StringBuilder sb3 = new StringBuilder("EncryptedBackupProtocolHelper/processError error text is empty, id=");
                sb3.append(str);
                Log.e(sb3.toString());
                str2 = "error text is empty";
            } else {
                String A0I2 = A0E.A0I("code", null);
                if (A0I2 == null) {
                    StringBuilder sb4 = new StringBuilder("EncryptedBackupProtocolHelper/processError error code is empty, id=");
                    sb4.append(str);
                    Log.e(sb4.toString());
                    str2 = "error code is empty";
                } else {
                    try {
                        r5.APr(A0I, Integer.parseInt(A0I2));
                        return;
                    } catch (NumberFormatException unused) {
                        StringBuilder sb5 = new StringBuilder("EncryptedBackupProtocolHelper/processError error code is not numerical, id=");
                        sb5.append(str);
                        sb5.append(", error=");
                        sb5.append(A0I2);
                        Log.e(sb5.toString());
                        r5.APr("error code is not numerical", 1);
                        return;
                    }
                }
            }
        }
        r5.APr(str2, 1);
    }

    public static final byte[] A01(AnonymousClass1V8 r1, AnonymousClass1OG r2, String str) {
        byte[] bArr;
        AnonymousClass1V8 A0E = r1.A0E(str);
        if (A0E != null && (bArr = A0E.A01) != null) {
            return bArr;
        }
        StringBuilder sb = new StringBuilder("EncryptedBackupProtocolHelper/getRequiredChildData node with name=");
        sb.append(str);
        sb.append(" was empty");
        Log.e(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append(" was empty");
        r2.APr(sb2.toString(), 1);
        return null;
    }
}
