package X;

import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;

/* renamed from: X.2aO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52182aO extends ClickableSpan {
    public final /* synthetic */ DialogC58312oc A00;

    public C52182aO(DialogC58312oc r1) {
        this.A00 = r1;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        Intent A0E = C12990iw.A0E("android.settings.DATE_SETTINGS");
        DialogC58312oc r0 = this.A00;
        r0.A00.A06(((AnonymousClass27U) r0).A01, A0E);
    }
}
