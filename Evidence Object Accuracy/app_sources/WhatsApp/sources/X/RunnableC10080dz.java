package X;

/* renamed from: X.0dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10080dz implements Runnable {
    public final /* synthetic */ C08800bs A00;
    public final /* synthetic */ C14260l7 A01;
    public final /* synthetic */ C14230l4 A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    public RunnableC10080dz(C08800bs r1, C14260l7 r2, C14230l4 r3, AbstractC14200l1 r4) {
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        C14250l6 r4 = new C14250l6(this.A02);
        AbstractC14200l1 r3 = this.A03;
        C14210l2 r2 = new C14210l2();
        r2.A05(this.A01, 0);
        r4.A01(r2.A03(), r3);
    }
}
