package X;

import android.view.ViewTreeObserver;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;

/* renamed from: X.4ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101554ni implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ MessageDetailsActivity A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101554ni(MessageDetailsActivity messageDetailsActivity) {
        this.A00 = messageDetailsActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        MessageDetailsActivity messageDetailsActivity = this.A00;
        C12980iv.A1F(messageDetailsActivity.A02, this);
        messageDetailsActivity.A02.setSelectionFromTop(1, messageDetailsActivity.getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 3);
    }
}
