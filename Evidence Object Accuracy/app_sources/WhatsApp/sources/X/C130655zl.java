package X;

import com.whatsapp.util.Log;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130655zl {
    public static final HashMap A06;
    public final C20080vB A00;
    public final C127675us A01;
    public final String A02;
    public final X509Certificate A03;
    public final List A04;
    public final boolean A05;

    static {
        HashMap A11 = C12970iu.A11();
        A06 = A11;
        Boolean bool = Boolean.FALSE;
        Boolean bool2 = Boolean.TRUE;
        A11.put("AUTH", Arrays.asList(C117315Zl.A05("novi.authentication.rc", bool), C117315Zl.A05("novi.authentication.prod", bool2)));
        A11.put("GATEWAY", Arrays.asList(C117315Zl.A05("novi.gateway.rc", bool), C117315Zl.A05("novi.gateway.prod", bool2)));
        A11.put("MEDIA", Arrays.asList(C117315Zl.A05("novi.media_storage.rc", bool), C117315Zl.A05("novi.media_storage.prod", bool2)));
        A11.put("RISK", Arrays.asList(C117315Zl.A05("novi.risk.rc", bool), C117315Zl.A05("novi.risk.prod", bool2)));
        A11.put("WALLET_CORE", Arrays.asList(C117315Zl.A05("novi.wallet_core.rc", bool), C117315Zl.A05("novi.wallet_core.prod", bool2)));
    }

    public C130655zl(C20080vB r8, String str, X509Certificate x509Certificate, Date date, List list) {
        String str2;
        Object obj;
        if (list.size() >= 2) {
            this.A00 = r8;
            this.A02 = str;
            this.A03 = x509Certificate;
            this.A04 = C12960it.A0l();
            for (int i = 1; i < list.size(); i++) {
                X509Certificate A00 = C20080vB.A00(C12960it.A0g(list, i), false);
                if (A00 != null) {
                    this.A04.add(A00);
                } else {
                    throw new C124715pz("Invalid certificate path");
                }
            }
            String[] split = ((X509Certificate) this.A04.get(0)).getSubjectX500Principal().getName().split(",");
            int length = split.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    str2 = "";
                    break;
                }
                String str3 = split[i2];
                if (str3.startsWith("CN=")) {
                    str2 = str3.substring(3);
                    break;
                }
                i2++;
            }
            List<AnonymousClass01T> list2 = (List) A06.get(str);
            if (list2 != null) {
                for (AnonymousClass01T r1 : list2) {
                    Object obj2 = r1.A00;
                    if (obj2 != null && ((String) obj2).equals(str2) && (obj = r1.A01) != null) {
                        this.A05 = C12970iu.A1Y(obj);
                        try {
                            C127675us r6 = new C127675us(C12960it.A0g(list, 0), ((Certificate) this.A04.get(0)).getPublicKey(), date);
                            this.A01 = r6;
                            List<? extends Certificate> list3 = this.A04;
                            if (x509Certificate != null) {
                                try {
                                    Iterator<? extends Certificate> it = list3.iterator();
                                    while (it.hasNext()) {
                                        ((X509Certificate) it.next()).checkValidity(date);
                                    }
                                    CertPath generateCertPath = CertificateFactory.getInstance("X.509").generateCertPath(list3);
                                    TrustAnchor trustAnchor = new TrustAnchor(x509Certificate, null);
                                    CertPathValidator instance = CertPathValidator.getInstance("PKIX");
                                    PKIXParameters pKIXParameters = new PKIXParameters(Collections.singleton(trustAnchor));
                                    pKIXParameters.setDate(date);
                                    pKIXParameters.setRevocationEnabled(false);
                                    instance.validate(generateCertPath, pKIXParameters);
                                    if (r6.A01.A03(((Certificate) this.A04.get(0)).getPublicKey()) && date.getTime() < r6.A00) {
                                        return;
                                    }
                                } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | CertPathValidatorException | CertificateException unused) {
                                    Log.e("Can't validate the X509 certificate chain");
                                }
                            }
                            throw new C124715pz("Invalid certificate path");
                        } catch (C124725q0 unused2) {
                            throw new C124715pz("Invalid custom certificate");
                        }
                    }
                }
            }
            throw new C124715pz("Invalid certificate path");
        }
        throw new C124715pz("Invalid certificate path");
    }
}
