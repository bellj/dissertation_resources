package X;

import android.os.IBinder;
import androidx.room.IMultiInstanceInvalidationCallback;

/* renamed from: X.0ZD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZD implements IMultiInstanceInvalidationCallback {
    public IBinder A00;

    public AnonymousClass0ZD(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
