package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.StateSet;

/* renamed from: X.0CA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CA extends AnonymousClass0AB {
    public AnonymousClass0CD A00;
    public boolean A01;

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        return true;
    }

    public AnonymousClass0CA() {
        this(null, null);
    }

    public AnonymousClass0CA(AnonymousClass0CD r2) {
    }

    public AnonymousClass0CA(Resources resources, AnonymousClass0CD r3) {
        A04(new AnonymousClass0CD(resources, r3, this));
        onStateChange(getState());
    }

    @Override // X.AnonymousClass0AB
    public void A04(AnonymousClass09z r2) {
        super.A04(r2);
        if (r2 instanceof AnonymousClass0CD) {
            this.A00 = (AnonymousClass0CD) r2;
        }
    }

    /* renamed from: A05 */
    public AnonymousClass0CD A03() {
        return new AnonymousClass0CD(null, this.A00, this);
    }

    @Override // X.AnonymousClass0AB, android.graphics.drawable.Drawable
    public void applyTheme(Resources.Theme theme) {
        super.applyTheme(theme);
        onStateChange(getState());
    }

    @Override // X.AnonymousClass0AB, android.graphics.drawable.Drawable
    public Drawable mutate() {
        if (!this.A01) {
            super.mutate();
            if (this == this) {
                this.A00.A04();
                this.A01 = true;
            }
        }
        return this;
    }

    @Override // X.AnonymousClass0AB, android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        int A08 = this.A00.A08(iArr);
        if (A08 < 0) {
            A08 = this.A00.A08(StateSet.WILD_CARD);
        }
        return A02(A08) || onStateChange;
    }
}
