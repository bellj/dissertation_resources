package X;

import android.os.Build;

/* renamed from: X.02c  reason: invalid class name */
/* loaded from: classes.dex */
public interface AnonymousClass02c {
    public static final boolean A00;

    @Override // X.AnonymousClass02c
    void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4);

    @Override // X.AnonymousClass02c
    void setAutoSizeTextTypeWithDefaults(int i);

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 27) {
            z = true;
        }
        A00 = z;
    }
}
