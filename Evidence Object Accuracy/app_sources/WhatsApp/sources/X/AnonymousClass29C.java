package X;

import java.util.Iterator;
import java.util.ListIterator;

/* renamed from: X.29C  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29C<E> extends AnonymousClass1Mr<E> {
    public final transient AnonymousClass1Mr forwardList;

    public AnonymousClass29C(AnonymousClass1Mr r1) {
        this.forwardList = r1;
    }

    @Override // X.AnonymousClass1Mr, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return this.forwardList.contains(obj);
    }

    @Override // java.util.List
    public Object get(int i) {
        C28291Mn.A01(i, size());
        return this.forwardList.get(reverseIndex(i));
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public int indexOf(Object obj) {
        int lastIndexOf = this.forwardList.lastIndexOf(obj);
        if (lastIndexOf >= 0) {
            return reverseIndex(lastIndexOf);
        }
        return -1;
    }

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return this.forwardList.isPartialView();
    }

    @Override // X.AnonymousClass1Mr, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public int lastIndexOf(Object obj) {
        int indexOf = this.forwardList.indexOf(obj);
        if (indexOf >= 0) {
            return reverseIndex(indexOf);
        }
        return -1;
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public /* bridge */ /* synthetic */ ListIterator listIterator() {
        return super.listIterator();
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
        return super.listIterator(i);
    }

    @Override // X.AnonymousClass1Mr
    public AnonymousClass1Mr reverse() {
        return this.forwardList;
    }

    private int reverseIndex(int i) {
        return (size() - 1) - i;
    }

    private int reversePosition(int i) {
        return size() - i;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.forwardList.size();
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public AnonymousClass1Mr subList(int i, int i2) {
        C28291Mn.A03(i, i2, size());
        return this.forwardList.subList(reversePosition(i2), reversePosition(i)).reverse();
    }
}
