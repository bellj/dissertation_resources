package X;

import android.graphics.Bitmap;

/* renamed from: X.3kf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75893kf extends AbstractC105924uq implements AnonymousClass5YZ {
    @Override // X.AbstractC105924uq
    public int A01(int i) {
        return i;
    }

    public C75893kf(AbstractC11580gW r1, C93604aR r2, AbstractC115035Pl r3) {
        super(r1, r2, r3);
    }

    @Override // X.AbstractC105924uq
    public int A02(Object obj) {
        return ((Bitmap) obj).getAllocationByteCount();
    }

    @Override // X.AbstractC105924uq
    public Object A04(int i) {
        return Bitmap.createBitmap(1, (int) Math.ceil(((double) i) / 2.0d), Bitmap.Config.RGB_565);
    }

    @Override // X.AbstractC105924uq
    public /* bridge */ /* synthetic */ Object A05(AnonymousClass4XA r3) {
        Bitmap bitmap = (Bitmap) super.A05(r3);
        if (bitmap != null) {
            bitmap.eraseColor(0);
        }
        return bitmap;
    }

    @Override // X.AbstractC105924uq
    public void A08(Object obj) {
        ((Bitmap) obj).recycle();
    }

    @Override // X.AbstractC105924uq
    public boolean A0A(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        return !bitmap.isRecycled() && bitmap.isMutable();
    }
}
