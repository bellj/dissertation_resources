package X;

/* renamed from: X.3cG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71023cG implements AbstractC115475Rr {
    public final AnonymousClass1V8 A00;
    public final C64083Ee A01;
    public final String A02;

    public C71023cG(AbstractC15710nm r12, AnonymousClass1V8 r13) {
        AnonymousClass1V8.A01(r13, "state");
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        AnonymousClass3JT.A04(null, r13, String.class, A0j, A0k, "pass", new String[]{"type"}, false);
        this.A02 = (String) AnonymousClass3JT.A03(null, r13, String.class, A0j, A0k, null, new String[]{"params"}, false);
        this.A01 = (C64083Ee) AnonymousClass3JT.A02(r12, r13, 6);
        this.A00 = r13;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C71023cG.class != obj.getClass()) {
                return false;
            }
            C71023cG r5 = (C71023cG) obj;
            if (!C29941Vi.A00(this.A02, r5.A02) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        return C12960it.A06(this.A01, A1a);
    }
}
