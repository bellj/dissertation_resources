package X;

import com.whatsapp.registration.VerifyTwoFactorAuth;

/* renamed from: X.52S  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52S implements AbstractC116435Vk {
    public final /* synthetic */ VerifyTwoFactorAuth A00;

    @Override // X.AbstractC116435Vk
    public void AT5(String str) {
    }

    public AnonymousClass52S(VerifyTwoFactorAuth verifyTwoFactorAuth) {
        this.A00 = verifyTwoFactorAuth;
    }

    @Override // X.AbstractC116435Vk
    public void AOH(String str) {
        VerifyTwoFactorAuth verifyTwoFactorAuth = this.A00;
        verifyTwoFactorAuth.A2f(0, verifyTwoFactorAuth.A08.getCode(), false);
    }
}
