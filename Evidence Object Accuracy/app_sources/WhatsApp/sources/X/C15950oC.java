package X;

/* renamed from: X.0oC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15950oC {
    public final int A00;
    public final int A01;
    public final String A02;

    public C15950oC(int i, String str, int i2) {
        this.A02 = str;
        this.A01 = i;
        this.A00 = i2;
    }

    public String[] A00() {
        return new String[]{this.A02, String.valueOf(this.A01), String.valueOf(this.A00)};
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C15950oC)) {
            return false;
        }
        C15950oC r4 = (C15950oC) obj;
        if (this.A02.equals(r4.A02) && this.A01 == r4.A01 && this.A00 == r4.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((this.A02.hashCode() * 31) + this.A00) * 31) + this.A01;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AnonymousClass1US.A02(4, this.A02));
        sb.append(":");
        sb.append(this.A00);
        sb.append(":");
        sb.append(this.A01);
        return sb.toString();
    }
}
