package X;

import X.AbstractC001200n;
import X.AnonymousClass074;
import X.AnonymousClass2A9;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.community.iq.CreateCommunityManager$$ExternalSyntheticLambda0;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.2A9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2A9 {
    public long A00;
    public C92204Uy A01;
    public C15580nU A02;
    public final AnonymousClass054 A03;
    public final AbstractC15710nm A04;
    public final C14900mE A05;
    public final C15570nT A06;
    public final C15550nR A07;
    public final AnonymousClass10T A08;
    public final C14830m7 A09;
    public final C14850m9 A0A;
    public final C22050yP A0B;
    public final C20710wC A0C;
    public final AnonymousClass1E8 A0D;
    public final C17220qS A0E;
    public final AnonymousClass10Z A0F;
    public final Map A0G = Collections.synchronizedMap(new HashMap());
    public final AtomicInteger A0H;

    public AnonymousClass2A9(AbstractC001200n r4, AbstractC15710nm r5, C14900mE r6, C15570nT r7, C92204Uy r8, C15550nR r9, AnonymousClass10T r10, C14830m7 r11, C14850m9 r12, C22050yP r13, C20710wC r14, AnonymousClass1E8 r15, C17220qS r16, AnonymousClass10Z r17) {
        this.A09 = r11;
        this.A0A = r12;
        this.A05 = r6;
        this.A04 = r5;
        this.A06 = r7;
        this.A0E = r16;
        this.A07 = r9;
        this.A0C = r14;
        this.A0B = r13;
        this.A08 = r10;
        this.A0F = r17;
        this.A0D = r15;
        this.A01 = r8;
        boolean z = false;
        this.A0H = new AtomicInteger(0);
        AnonymousClass009.A01();
        AnonymousClass009.A0F(((C009804x) r4.ADr()).A02 != AnonymousClass05I.DESTROYED ? true : z);
        CreateCommunityManager$$ExternalSyntheticLambda0 createCommunityManager$$ExternalSyntheticLambda0 = new AnonymousClass054() { // from class: com.whatsapp.community.iq.CreateCommunityManager$$ExternalSyntheticLambda0
            @Override // X.AnonymousClass054
            public final void AWQ(AnonymousClass074 r3, AbstractC001200n r42) {
                AnonymousClass2A9 r1 = AnonymousClass2A9.this;
                if (r3.equals(AnonymousClass074.ON_DESTROY)) {
                    r1.A01 = null;
                }
            }
        };
        this.A03 = createCommunityManager$$ExternalSyntheticLambda0;
        r4.ADr().A00(createCommunityManager$$ExternalSyntheticLambda0);
    }

    public static /* synthetic */ void A00(AnonymousClass2A9 r6, int i) {
        if (i == 0) {
            r6.A0B.A04(11, r6.A09.A00() - r6.A00);
            if (!r6.A0G.isEmpty() && r6.A02 != null) {
                r6.A05.A0H(new RunnableBRunnable0Shape4S0100000_I0_4(r6, 2));
            }
        }
    }
}
