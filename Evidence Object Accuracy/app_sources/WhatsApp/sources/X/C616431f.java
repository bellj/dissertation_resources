package X;

import android.graphics.Bitmap;
import java.io.File;

/* renamed from: X.31f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616431f extends AbstractC35601iM {
    public final long A00;
    public final AnonymousClass19O A01;
    public final String A02;

    @Override // X.AbstractC35611iN
    public int getType() {
        return 4;
    }

    public C616431f(C16440p1 r1, AnonymousClass19O r2, File file, String str, long j, long j2) {
        super(r1, file, j);
        this.A01 = r2;
        this.A00 = j2;
        this.A02 = str;
    }

    @Override // X.AbstractC35611iN
    public String AES() {
        String str = this.A02;
        return str == null ? "application/*" : str;
    }

    @Override // X.AbstractC35611iN
    public Bitmap Aem(int i) {
        Bitmap A01;
        AnonymousClass19O r3 = this.A01;
        AbstractC16130oV r2 = this.A03;
        synchronized (r3) {
            AnonymousClass009.A00();
            A01 = AnonymousClass19O.A01(r2, 100, false, false);
            if (A01 == null) {
                C16460p3 A0G = r2.A0G();
                if (!A0G.A05()) {
                    r3.A03.A01(A0G);
                    byte[] A07 = A0G.A07();
                    if (A07 != null) {
                        A01 = C41511te.A00(AnonymousClass19O.A07, A07, 100);
                    }
                }
                A01 = null;
            }
        }
        return A01;
    }
}
