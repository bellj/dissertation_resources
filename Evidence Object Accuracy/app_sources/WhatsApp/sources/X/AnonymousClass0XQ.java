package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.0XQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XQ implements AbstractC12690iL, AbstractC11250fz {
    public int A00;
    public int A01 = R.layout.abc_action_menu_item_layout;
    public int A02;
    public int A03 = R.layout.abc_action_menu_layout;
    public int A04;
    public Context A05;
    public Context A06;
    public Drawable A07;
    public LayoutInflater A08;
    public LayoutInflater A09;
    public AnonymousClass07H A0A;
    public AbstractC12280hf A0B;
    public AbstractC11630gb A0C;
    public AnonymousClass0CO A0D;
    public AnonymousClass0CH A0E;
    public RunnableC09460cw A0F;
    public AnonymousClass0CR A0G;
    public AnonymousClass0CP A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public boolean A0L;
    public final SparseBooleanArray A0M = new SparseBooleanArray();
    public final AnonymousClass0XK A0N = new AnonymousClass0XK(this);

    @Override // X.AbstractC12690iL
    public boolean A7P(AnonymousClass07H r2, C07340Xp r3) {
        return false;
    }

    @Override // X.AbstractC12690iL
    public boolean A9n(AnonymousClass07H r2, C07340Xp r3) {
        return false;
    }

    public AnonymousClass0XQ(Context context) {
        this.A06 = context;
        this.A09 = LayoutInflater.from(context);
    }

    public View A00(View view, ViewGroup viewGroup, C07340Xp r6) {
        View actionView = r6.getActionView();
        if (actionView == null || r6.A01()) {
            if (!(view instanceof AbstractC12290hg)) {
                view = this.A09.inflate(this.A01, viewGroup, false);
            }
            AbstractC12290hg r4 = (AbstractC12290hg) view;
            r4.AIt(r6, 0);
            ActionMenuItemView actionMenuItemView = (ActionMenuItemView) r4;
            actionMenuItemView.A05 = (ActionMenuView) this.A0C;
            AnonymousClass0CH r0 = this.A0E;
            if (r0 == null) {
                r0 = new AnonymousClass0CH(this);
                this.A0E = r0;
            }
            actionMenuItemView.A04 = r0;
            actionView = (View) r4;
        }
        int i = 0;
        if (r6.isActionViewExpanded()) {
            i = 8;
        }
        actionView.setVisibility(i);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!(layoutParams instanceof C02390Ca)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public boolean A01() {
        AbstractC11630gb r0;
        RunnableC09460cw r2 = this.A0F;
        if (r2 == null || (r0 = this.A0C) == null) {
            AnonymousClass0CP r02 = this.A0H;
            if (r02 == null) {
                return false;
            }
            r02.A01();
            return true;
        }
        ((View) r0).removeCallbacks(r2);
        this.A0F = null;
        return true;
    }

    public boolean A02() {
        AnonymousClass0XP r0;
        AnonymousClass0CP r02 = this.A0H;
        if (r02 == null || (r0 = r02.A03) == null || !r0.AK4()) {
            return false;
        }
        return true;
    }

    public boolean A03() {
        AnonymousClass07H r1;
        if (!this.A0K || A02() || (r1 = this.A0A) == null || this.A0C == null || this.A0F != null) {
            return false;
        }
        r1.A05();
        if (r1.A08.isEmpty()) {
            return false;
        }
        RunnableC09460cw r12 = new RunnableC09460cw(new AnonymousClass0CP(this.A05, this.A0G, this.A0A, this), this);
        this.A0F = r12;
        ((View) this.A0C).post(r12);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f6, code lost:
        if (r13 != false) goto L_0x00c7;
     */
    @Override // X.AbstractC12690iL
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AA1() {
        /*
        // Method dump skipped, instructions count: 251
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0XQ.AA1():boolean");
    }

    @Override // X.AbstractC12690iL
    public void AIn(Context context, AnonymousClass07H r8) {
        this.A05 = context;
        this.A08 = LayoutInflater.from(context);
        this.A0A = r8;
        Resources resources = context.getResources();
        C05280Oy r1 = new C05280Oy(context);
        if (!this.A0L) {
            this.A0K = r1.A01();
        }
        this.A04 = r1.A00.getResources().getDisplayMetrics().widthPixels >> 1;
        this.A02 = r1.A00();
        int i = this.A04;
        if (this.A0K) {
            if (this.A0G == null) {
                AnonymousClass0CR r2 = new AnonymousClass0CR(this.A06, this);
                this.A0G = r2;
                if (this.A0J) {
                    r2.setImageDrawable(this.A07);
                    this.A07 = null;
                    this.A0J = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.A0G.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.A0G.getMeasuredWidth();
        } else {
            this.A0G = null;
        }
        this.A00 = i;
        resources.getDisplayMetrics();
    }

    @Override // X.AbstractC12690iL
    public void AOF(AnonymousClass07H r2, boolean z) {
        A01();
        AnonymousClass0CO r0 = this.A0D;
        if (r0 != null) {
            r0.A01();
        }
        AbstractC12280hf r02 = this.A0B;
        if (r02 != null) {
            r02.AOF(r2, z);
        }
    }

    @Override // X.AbstractC12690iL
    public boolean AWs(AnonymousClass0CK r8) {
        boolean z = false;
        if (r8.hasVisibleItems()) {
            AnonymousClass0CK r0 = r8;
            while (r0.A00 != this.A0A) {
                r0 = (AnonymousClass0CK) r0.A00;
            }
            MenuItem item = r0.getItem();
            ViewGroup viewGroup = (ViewGroup) this.A0C;
            if (viewGroup != null) {
                int childCount = viewGroup.getChildCount();
                int i = 0;
                while (true) {
                    if (i >= childCount) {
                        break;
                    }
                    View childAt = viewGroup.getChildAt(i);
                    if (!(childAt instanceof AbstractC12290hg) || ((AbstractC12290hg) childAt).getItemData() != item) {
                        i++;
                    } else if (childAt != null) {
                        r8.getItem().getItemId();
                        int size = r8.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size) {
                                break;
                            }
                            MenuItem item2 = r8.getItem(i2);
                            if (item2.isVisible() && item2.getIcon() != null) {
                                z = true;
                                break;
                            }
                            i2++;
                        }
                        AnonymousClass0CO r1 = new AnonymousClass0CO(this.A05, childAt, r8, this);
                        this.A0D = r1;
                        r1.A05 = z;
                        AnonymousClass0XP r02 = r1.A03;
                        if (r02 != null) {
                            r02.A07(z);
                        }
                        if (r1.A03()) {
                            AbstractC12280hf r03 = this.A0B;
                            if (r03 != null) {
                                r03.ATF(r8);
                            }
                            return true;
                        }
                        throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
                    }
                }
            }
        }
        return false;
    }

    @Override // X.AbstractC12690iL
    public void Abr(AbstractC12280hf r1) {
        this.A0B = r1;
    }

    @Override // X.AbstractC12690iL
    public void AfQ(boolean z) {
        ArrayList arrayList;
        AbstractC11630gb r1;
        C07340Xp r0;
        ViewGroup viewGroup = (ViewGroup) this.A0C;
        if (viewGroup != null) {
            AnonymousClass07H r02 = this.A0A;
            int i = 0;
            if (r02 != null) {
                r02.A05();
                ArrayList A04 = this.A0A.A04();
                int size = A04.size();
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    C07340Xp r9 = (C07340Xp) A04.get(i3);
                    if ((r9.A02 & 32) == 32) {
                        View childAt = viewGroup.getChildAt(i2);
                        if (childAt instanceof AbstractC12290hg) {
                            r0 = ((AbstractC12290hg) childAt).getItemData();
                        } else {
                            r0 = null;
                        }
                        View A00 = A00(childAt, viewGroup, r9);
                        if (r9 != r0) {
                            A00.setPressed(false);
                            A00.jumpDrawablesToCurrentState();
                        }
                        if (A00 != childAt) {
                            ViewGroup viewGroup2 = (ViewGroup) A00.getParent();
                            if (viewGroup2 != null) {
                                viewGroup2.removeView(A00);
                            }
                            ((ViewGroup) this.A0C).addView(A00, i2);
                        }
                        i2++;
                    }
                }
                i = i2;
            }
            while (i < viewGroup.getChildCount()) {
                if (viewGroup.getChildAt(i) == this.A0G) {
                    i++;
                } else {
                    viewGroup.removeViewAt(i);
                }
            }
        }
        ((View) this.A0C).requestLayout();
        AnonymousClass07H r03 = this.A0A;
        boolean z2 = false;
        if (r03 != null) {
            r03.A05();
            ArrayList arrayList2 = r03.A06;
            int size2 = arrayList2.size();
            for (int i4 = 0; i4 < size2; i4++) {
                AbstractC04730Mv r04 = ((C07340Xp) arrayList2.get(i4)).A0G;
                if (r04 != null) {
                    r04.A00 = this;
                }
            }
        }
        AnonymousClass07H r05 = this.A0A;
        if (r05 != null) {
            r05.A05();
            arrayList = r05.A08;
        } else {
            arrayList = null;
        }
        if (this.A0K && arrayList != null) {
            int size3 = arrayList.size();
            if (size3 == 1) {
                z2 = !((C07340Xp) arrayList.get(0)).isActionViewExpanded();
            } else if (size3 > 0) {
                z2 = true;
            }
        }
        AnonymousClass0CR r12 = this.A0G;
        if (z2) {
            if (r12 == null) {
                r12 = new AnonymousClass0CR(this.A06, this);
                this.A0G = r12;
            }
            ViewGroup viewGroup3 = (ViewGroup) r12.getParent();
            if (viewGroup3 != this.A0C) {
                if (viewGroup3 != null) {
                    viewGroup3.removeView(this.A0G);
                }
                AnonymousClass0CR r2 = this.A0G;
                C02390Ca r13 = new C02390Ca();
                ((LinearLayout.LayoutParams) r13).gravity = 16;
                r13.A04 = true;
                ((ViewGroup) this.A0C).addView(r2, r13);
            }
        } else if (r12 != null && r12.getParent() == (r1 = this.A0C)) {
            ((ViewGroup) r1).removeView(this.A0G);
        }
        ((ActionMenuView) this.A0C).A0B = this.A0K;
    }
}
