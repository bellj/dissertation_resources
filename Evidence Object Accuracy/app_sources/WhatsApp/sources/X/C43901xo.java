package X;

/* renamed from: X.1xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C43901xo {
    public static boolean A00(C14850m9 r2, int i) {
        return String.valueOf(i).startsWith("202102") && !r2.A07(344);
    }

    public static boolean A01(C14850m9 r2, C43831xf r3) {
        return String.valueOf(r3.A00).startsWith("202102") && r2.A07(344);
    }

    public static boolean A02(AnonymousClass12O r2) {
        AnonymousClass12N r1 = r2.A08;
        return r1.A01() == null || r1.A01().A01 != 4;
    }
}
