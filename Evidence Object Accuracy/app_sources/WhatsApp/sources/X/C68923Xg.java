package X;

import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.community.AddGroupsToCommunityActivity;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.3Xg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68923Xg implements AbstractC36361jl {
    public final /* synthetic */ Uri A00;
    public final /* synthetic */ AnonymousClass2A9 A01;
    public final /* synthetic */ Set A02;
    public final /* synthetic */ Set A03;

    public C68923Xg(Uri uri, AnonymousClass2A9 r2, Set set, Set set2) {
        this.A01 = r2;
        this.A00 = uri;
        this.A03 = set;
        this.A02 = set2;
    }

    @Override // X.AbstractC36361jl
    public void APl(int i) {
        C92204Uy r0 = this.A01.A01;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AbstractC36361jl
    public void AWy(C15580nU r22, C47572Bl r23) {
        AnonymousClass1JV r10;
        AnonymousClass2A9 r0 = this.A01;
        r0.A02 = r22;
        Uri uri = this.A00;
        if (uri != null) {
            File file = new File(uri.getPath());
            if (file.exists()) {
                r0.A0F.A0B(r0.A07.A0B(r22), file, false);
            }
        }
        C92204Uy r2 = r0.A01;
        if (r2 != null) {
            AddGroupsToCommunityActivity addGroupsToCommunityActivity = r2.A00;
            addGroupsToCommunityActivity.A0Q.set(r22);
            ((ActivityC13810kN) addGroupsToCommunityActivity).A05.A0J(new RunnableBRunnable0Shape15S0100000_I1_1(r2, 10), 10000);
            int size = ((Set) addGroupsToCommunityActivity.A01.A08.A01()).size();
            int size2 = ((Set) addGroupsToCommunityActivity.A01.A09.A01()).size();
            String A00 = addGroupsToCommunityActivity.A04.A00();
            Integer A0k = C12980iv.A0k();
            if (size2 > 0) {
                addGroupsToCommunityActivity.A0I.A0B(A0k, A0k, C12980iv.A0l(size2), A00);
            }
            if (size > 0) {
                addGroupsToCommunityActivity.A0I.A0B(C12990iw.A0j(), A0k, C12980iv.A0l(size), A00);
            }
            addGroupsToCommunityActivity.A0I.A0B(7, A0k, null, A00);
        }
        Set set = this.A03;
        ArrayList A0l = C12960it.A0l();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            GroupJid of = GroupJid.of(C12970iu.A0a(it).A0D);
            if (of != null) {
                A0l.add(of);
            }
        }
        Set set2 = this.A02;
        int size3 = set2.size() + (!A0l.isEmpty());
        if (size3 == 0) {
            AnonymousClass2A9.A00(r0, size3);
            return;
        }
        r0.A0H.set(size3);
        if (!set2.isEmpty()) {
            Iterator it2 = set2.iterator();
            while (it2.hasNext()) {
                C15370n3 A0a = C12970iu.A0a(it2);
                Jid jid = A0a.A0D;
                if (jid instanceof AnonymousClass1JV) {
                    r10 = (AnonymousClass1JV) jid;
                } else {
                    r10 = null;
                }
                AnonymousClass009.A05(r10);
                String str = A0a.A0K;
                C63323Bd r8 = new C63323Bd(r22, r10, null, str, null, C12960it.A0l(), A0a.A01, false);
                r0.A0G.put(r10, str);
                C14830m7 r15 = r0.A09;
                C14850m9 r6 = r0.A0A;
                new AnonymousClass3ZP(r0.A04, r0.A06, r15, r6, r0.A0C, new C68913Xf(r0, A0a, r10), r8, r0.A0E).A00();
            }
        }
        if (!A0l.isEmpty()) {
            Iterator it3 = A0l.iterator();
            while (it3.hasNext()) {
                AbstractC14640lm A0b = C12990iw.A0b(it3);
                r0.A0G.put(A0b, r0.A07.A0B(A0b).A0K);
            }
            new C63783Cx(r0.A04, r22, r0.A0E, new AnonymousClass3ZV(r0)).A00(A0l);
        }
    }

    @Override // X.AbstractC36361jl
    public void AXX() {
        C92204Uy r0 = this.A01.A01;
        if (r0 != null) {
            r0.A00();
        }
    }
}
