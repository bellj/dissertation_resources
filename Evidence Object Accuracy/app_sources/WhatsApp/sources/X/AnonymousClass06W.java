package X;

import android.content.Context;
import android.content.Intent;

/* renamed from: X.06W  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass06W extends AnonymousClass05K {
    @Override // X.AnonymousClass05K
    public Intent A00(Context context, Object obj) {
        return (Intent) obj;
    }

    @Override // X.AnonymousClass05K
    public Object A02(Intent intent, int i) {
        return new C06830Vg(i, intent);
    }
}
