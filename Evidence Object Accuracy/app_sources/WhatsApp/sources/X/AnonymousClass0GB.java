package X;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0GB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0GB extends AnonymousClass0G0 {
    public AnonymousClass0GB() {
    }

    public AnonymousClass0GB(int i) {
        if ((i & -4) == 0) {
            ((AnonymousClass0G0) this).A00 = i;
            return;
        }
        throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
    }

    @Override // X.AnonymousClass0G0, X.AnonymousClass072
    public void A0U(C05350Pf r4) {
        AnonymousClass0G0.A03(r4);
        r4.A02.put("android:fade:transitionAlpha", Float.valueOf(AnonymousClass0U3.A04.A00(r4.A00)));
    }

    @Override // X.AnonymousClass0G0
    public Animator A0V(View view, ViewGroup viewGroup, C05350Pf r7, C05350Pf r8) {
        Number number;
        float f = 0.0f;
        float f2 = 0.0f;
        if (!(r7 == null || (number = (Number) r7.A02.get("android:fade:transitionAlpha")) == null)) {
            f2 = number.floatValue();
        }
        if (f2 != 1.0f) {
            f = f2;
        }
        return A0X(view, f, 1.0f);
    }

    @Override // X.AnonymousClass0G0
    public Animator A0W(View view, ViewGroup viewGroup, C05350Pf r6, C05350Pf r7) {
        Number number;
        AnonymousClass0U3.A04.A04(view);
        float f = 1.0f;
        if (!(r6 == null || (number = (Number) r6.A02.get("android:fade:transitionAlpha")) == null)) {
            f = number.floatValue();
        }
        return A0X(view, f, 0.0f);
    }

    public final Animator A0X(View view, float f, float f2) {
        if (f == f2) {
            return null;
        }
        AnonymousClass0U3.A04.A05(view, f);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, AnonymousClass0U3.A03, f2);
        ofFloat.addListener(new AnonymousClass09E(view));
        A08(new AnonymousClass0G1(view, this));
        return ofFloat;
    }
}
