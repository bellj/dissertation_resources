package X;

import android.view.View;
import com.whatsapp.CircleWaImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.1ld  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37181ld extends AbstractC37191le {
    public C27131Gd A00;
    public AnonymousClass1J1 A01;
    public final CircleWaImageView A02;
    public final WaImageView A03;
    public final WaTextView A04;
    public final WaTextView A05;
    public final C48842Hz A06;
    public final AnonymousClass10S A07;

    public C37181ld(View view, C48842Hz r4, AnonymousClass10S r5, C21270x9 r6) {
        super(view);
        this.A07 = r5;
        this.A01 = r6.A04(view.getContext(), "business-profile-recent-item");
        this.A06 = r4;
        this.A02 = (CircleWaImageView) AnonymousClass028.A0D(view, R.id.business_avatar);
        this.A04 = (WaTextView) AnonymousClass028.A0D(view, R.id.business_name);
        this.A05 = (WaTextView) AnonymousClass028.A0D(view, R.id.category);
        this.A03 = (WaImageView) AnonymousClass028.A0D(view, R.id.delete_button);
    }
}
