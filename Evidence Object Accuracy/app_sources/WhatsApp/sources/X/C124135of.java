package X;

import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.5of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124135of extends AbstractC16350or {
    public final C17070qD A00;
    public final C30931Zj A01;
    public final WeakReference A02;

    public C124135of(C17070qD r2, C30931Zj r3, C125915s1 r4) {
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = C12970iu.A10(r4);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        int i;
        C17070qD r4 = this.A00;
        r4.A03();
        List A0A = r4.A09.A0A();
        C30931Zj r2 = this.A01;
        StringBuilder A0k = C12960it.A0k("#methods=");
        A0k.append(A0A.size());
        C117295Zj.A1F(r2, A0k);
        if (A0A.size() > 1) {
            i = 201;
        } else {
            r4.A03();
            i = 200;
            if (r4.A08.A0T(1).size() > 0) {
                i = 202;
            }
        }
        return Integer.valueOf(i);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Number number = (Number) obj;
        C125915s1 r0 = (C125915s1) this.A02.get();
        if (r0 != null) {
            C36021jC.A01(r0.A00, number.intValue());
        }
    }
}
