package X;

import android.content.Context;
import android.widget.RelativeLayout;

/* renamed from: X.2dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC53282dd extends RelativeLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC53282dd(Context context) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            ((C61162zV) this).A06 = C12960it.A0Q(((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
