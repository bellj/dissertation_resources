package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.storage.StorageUsageGalleryActivity;
import com.whatsapp.storage.StorageUsageMediaGalleryFragment;
import com.whatsapp.util.Log;
import java.util.Collection;

/* renamed from: X.1MC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MC extends AnonymousClass1MD {
    public final /* synthetic */ StorageUsageGalleryActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1MC(AnonymousClass12P r44, AbstractC15710nm r45, C14900mE r46, C15570nT r47, C15450nH r48, C16170oZ r49, ActivityC13790kL r50, AnonymousClass12T r51, C18850tA r52, C15550nR r53, C22700zV r54, C15610nY r55, AnonymousClass1A6 r56, C255419u r57, AnonymousClass01d r58, C14830m7 r59, C14820m6 r60, AnonymousClass018 r61, C15600nX r62, C253218y r63, C242114q r64, C22100yW r65, C22180yf r66, AnonymousClass19M r67, C231510o r68, AnonymousClass193 r69, C14850m9 r70, C16120oU r71, C20710wC r72, AnonymousClass109 r73, C22370yy r74, AnonymousClass13H r75, C16630pM r76, C88054Ec r77, AnonymousClass12F r78, C253018w r79, AnonymousClass12U r80, StorageUsageGalleryActivity storageUsageGalleryActivity, C23000zz r82, C252718t r83, AbstractC14440lR r84, AnonymousClass01H r85) {
        super(r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r64, r65, r66, r67, r68, r69, r70, r71, r72, r73, r74, r75, r76, r77, r78, r79, r80, r82, r83, r84, r85);
        this.A00 = storageUsageGalleryActivity;
    }

    @Override // X.AnonymousClass1MD
    public void A04() {
        StorageUsageGalleryActivity storageUsageGalleryActivity = this.A00;
        C35451ht r2 = storageUsageGalleryActivity.A0H;
        if (r2 == null || r2.A04.isEmpty()) {
            Log.e("storageusagegallery/dialog/delete no messages");
            return;
        }
        StringBuilder sb = new StringBuilder("storageusagegallery/dialog/delete/");
        sb.append(r2.A04.size());
        Log.i(sb.toString());
        storageUsageGalleryActivity.A2e();
        storageUsageGalleryActivity.A07 = new AnonymousClass02N();
        Collection values = storageUsageGalleryActivity.A0H.A04.values();
        storageUsageGalleryActivity.A0N = new C623837a(storageUsageGalleryActivity.A07, new AnonymousClass5U5(values) { // from class: X.569
            public final /* synthetic */ Collection A01;

            {
                this.A01 = r2;
            }

            @Override // X.AnonymousClass5U5
            public final void APY(Collection collection) {
                AnonymousClass1MC r22 = AnonymousClass1MC.this;
                Collection collection2 = this.A01;
                Log.i("storage-usage-gallery-activity/load duplicate messages/loaded");
                StorageUsageGalleryActivity storageUsageGalleryActivity2 = r22.A00;
                storageUsageGalleryActivity2.A2e();
                StorageUsageGalleryActivity.A02(storageUsageGalleryActivity2, collection, collection2);
            }
        }, storageUsageGalleryActivity.A0O, values);
        storageUsageGalleryActivity.A0n = new RunnableBRunnable0Shape8S0200000_I0_8(this, 22, values);
        Handler handler = storageUsageGalleryActivity.A0q;
        handler.postDelayed(storageUsageGalleryActivity.A0u, 800);
        handler.postDelayed(storageUsageGalleryActivity.A0n, 5000);
        ((ActivityC13830kP) storageUsageGalleryActivity).A05.Aaz(storageUsageGalleryActivity.A0N, new Void[0]);
        Log.i("storage-usage-gallery-activity/load duplicate messages");
    }

    @Override // X.AnonymousClass1MD, X.AnonymousClass02Q
    public void AP3(AbstractC009504t r4) {
        StorageUsageGalleryActivity storageUsageGalleryActivity = this.A00;
        C35451ht r1 = storageUsageGalleryActivity.A0H;
        if (r1 != null) {
            r1.A00();
            storageUsageGalleryActivity.A0H = null;
        }
        storageUsageGalleryActivity.A06 = null;
        StorageUsageMediaGalleryFragment storageUsageMediaGalleryFragment = storageUsageGalleryActivity.A0j;
        if (storageUsageMediaGalleryFragment != null && storageUsageMediaGalleryFragment.A0c()) {
            ((MediaGalleryFragmentBase) storageUsageMediaGalleryFragment).A06.A02();
        }
    }
}
