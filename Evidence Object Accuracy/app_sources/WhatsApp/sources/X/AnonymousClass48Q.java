package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.48Q  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass48Q extends AnonymousClass1JW {
    public AnonymousClass48Q(AbstractC15710nm r2, C15450nH r3, UserJid userJid) {
        super(r2, r3);
        this.A05 = 48;
        this.A0C = userJid;
    }
}
