package X;

/* renamed from: X.2zA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC60952zA extends AbstractC64483Fs {
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006c, code lost:
        if (r0 != null) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(android.app.Activity r8, X.AbstractC17160qM r9, X.AnonymousClass018 r10, X.AnonymousClass1Z8 r11, X.C16660pY r12, java.lang.String r13, long r14) {
        /*
            r7 = this;
            java.lang.String r4 = r11.A01
            if (r4 == 0) goto L_0x0069
            java.util.HashMap r3 = X.C12970iu.A11()
            java.util.HashMap r6 = X.C12970iu.A11()
            java.lang.Class<com.whatsapp.Conversation> r0 = com.whatsapp.Conversation.class
            android.app.Activity r5 = X.AbstractC35731ia.A01(r8, r0)
            com.whatsapp.Conversation r5 = (com.whatsapp.Conversation) r5
            if (r5 == 0) goto L_0x002b
            X.0n3 r0 = r5.A2c
            if (r0 == 0) goto L_0x002b
            java.lang.String r2 = r0.A0D()
            java.lang.String r1 = "business_name"
            X.0n3 r0 = r5.A2c
            if (r2 == 0) goto L_0x006a
            java.lang.String r0 = r0.A0D()
        L_0x0028:
            r6.put(r1, r0)
        L_0x002b:
            java.lang.String r0 = "business_info"
            r3.put(r0, r6)
            java.util.ArrayList r1 = X.C12960it.A0l()
            java.lang.String r0 = "address_message_validate"
            r1.add(r0)
            java.lang.String r0 = "configure_top_bar"
            r1.add(r0)
            java.lang.String r0 = "extension_message_response"
            r1.add(r0)
            java.lang.String r0 = "supported_actions"
            r3.put(r0, r1)
            java.lang.String r1 = r7.A01()
            java.util.Map r0 = X.C65053Hy.A01(r4)
            r3.put(r1, r0)
            java.util.HashMap r2 = X.C12970iu.A11()
            java.lang.String r0 = "commerce"
            r2.put(r0, r3)
            java.util.HashMap r1 = X.C12970iu.A11()
            java.lang.String r0 = "data"
            r1.put(r0, r2)
            r9.AZe(r0, r1)
        L_0x0069:
            return
        L_0x006a:
            java.lang.String r0 = r0.A0K
            if (r0 == 0) goto L_0x002b
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC60952zA.A06(android.app.Activity, X.0qM, X.018, X.1Z8, X.0pY, java.lang.String, long):void");
    }
}
