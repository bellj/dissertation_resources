package X;

import android.media.MediaPlayer;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.4i4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98054i4 implements MediaPlayer.OnCompletionListener {
    public final /* synthetic */ VideoSurfaceView A00;

    public C98054i4(VideoSurfaceView videoSurfaceView) {
        this.A00 = videoSurfaceView;
    }

    @Override // android.media.MediaPlayer.OnCompletionListener
    public void onCompletion(MediaPlayer mediaPlayer) {
        VideoSurfaceView videoSurfaceView = this.A00;
        videoSurfaceView.A02 = 5;
        videoSurfaceView.A06 = 5;
        MediaPlayer.OnCompletionListener onCompletionListener = videoSurfaceView.A09;
        if (onCompletionListener != null) {
            onCompletionListener.onCompletion(videoSurfaceView.A0C);
        }
    }
}
