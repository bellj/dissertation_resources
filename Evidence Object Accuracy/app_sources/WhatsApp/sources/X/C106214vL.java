package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

/* renamed from: X.4vL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106214vL implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setBackground(((C55992k9) obj2).A04);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        Drawable drawable = ((C55992k9) obj).A04;
        Drawable drawable2 = ((C55992k9) obj2).A04;
        if (drawable == null) {
            if (drawable2 != null) {
                return true;
            }
            return false;
        } else if (drawable2 == null || drawable.equals(drawable2)) {
            return false;
        } else {
            return true;
        }
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((View) obj).setBackground(null);
    }
}
