package X;

import java.io.IOException;

/* renamed from: X.5Fk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112945Fk implements AbstractC117255Zd {
    public int A00;
    public C92754Xh A01;
    public boolean A02;

    public C112945Fk(C92754Xh r1, int i, boolean z) {
        this.A02 = z;
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return this.A01.A02(this.A00, this.A02);
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw new AnonymousClass4CU(e.getMessage());
        }
    }
}
