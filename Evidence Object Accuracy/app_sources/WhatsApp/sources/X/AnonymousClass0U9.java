package X;

import android.graphics.Point;

/* renamed from: X.0U9  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0U9 {
    public final AnonymousClass04Q A00;
    public final C05050Ob A01 = new C05050Ob();
    public final float[] A02 = new float[2];

    public static float A02(double d) {
        return ((float) (d + 180.0d)) / 360.0f;
    }

    public AnonymousClass0U9(AnonymousClass04Q r2) {
        this.A00 = r2;
    }

    public static double A00(double d) {
        return ((Math.atan(Math.exp((1.0d - (d * 2.0d)) * 3.141592653589793d)) * 2.0d) - 1.5707963267948966d) * 57.29577951308232d;
    }

    public static float A01(double d) {
        double sin = Math.sin((d * 3.141592653589793d) / 180.0d);
        return (float) (0.5d - (Math.log((sin + 1.0d) / (1.0d - sin)) / 12.566370614359172d));
    }

    public double A03(float f) {
        AnonymousClass04L r0 = this.A00.A0E;
        return (double) (f / (((float) r0.A0J) * r0.A0C));
    }

    public Point A04(AnonymousClass03T r12) {
        double d = r12.A00;
        double d2 = r12.A01;
        float[] fArr = this.A02;
        A08(fArr, (double) A02(d2), (double) A01(d));
        return new Point((int) fArr[0], (int) fArr[1]);
    }

    public AnonymousClass03T A05(float f, float f2) {
        float[] fArr = this.A02;
        A0A(fArr, f, f2);
        return new AnonymousClass03T(A00((double) fArr[1]), (((double) fArr[0]) * 360.0d) - 180.0d);
    }

    public C05430Pn A06() {
        AnonymousClass04Q r3 = this.A00;
        AnonymousClass04L r4 = r3.A0E;
        float f = (float) 0;
        AnonymousClass03T A05 = A05(f, (float) (r4.A0D - r3.A04));
        AnonymousClass03T A052 = A05((float) (r4.A0F - r3.A05), (float) (r4.A0D - r3.A04));
        AnonymousClass03T A053 = A05(f, (float) r3.A06);
        AnonymousClass03T A054 = A05((float) (r4.A0F - r3.A05), (float) r3.A06);
        AnonymousClass0PM r0 = new AnonymousClass0PM();
        r0.A01(A05);
        r0.A01(A053);
        r0.A01(A052);
        r0.A01(A054);
        return new C05430Pn(A05, A052, A053, A054, r0.A00());
    }

    public final void A07(C05050Ob r9) {
        AnonymousClass04L r6 = this.A00.A0E;
        double d = r6.A03;
        double d2 = r6.A01;
        r9.A03 = d - d2;
        r9.A00 = d + d2;
        double d3 = r6.A02;
        double d4 = r6.A00;
        double d5 = d3 - d4;
        r9.A01 = d5;
        double d6 = d3 + d4;
        r9.A02 = d6;
        if (d5 < 0.0d) {
            double ceil = (double) ((int) Math.ceil(-d5));
            r9.A01 = d5 + ceil;
            r9.A02 = d6 + ceil;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if ((r4 - r12) < (r2 - r8)) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(float[] r11, double r12, double r14) {
        /*
            r10 = this;
            X.0Ob r6 = r10.A01
            r10.A07(r6)
            double r4 = r6.A01
            int r0 = (r12 > r4 ? 1 : (r12 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0011
            double r1 = r6.A02
            int r0 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x002b
        L_0x0011:
            double r0 = r4 - r12
            double r1 = java.lang.Math.ceil(r0)
            int r0 = (int) r1
            double r2 = (double) r0
            double r2 = r2 + r12
            double r8 = r6.A02
            int r0 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r0 <= 0) goto L_0x002f
            double r6 = r2 - r8
            r0 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r12 = r2 - r0
            double r4 = r4 - r12
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x002f
        L_0x002b:
            r10.A09(r11, r12, r14)
            return
        L_0x002f:
            r12 = r2
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0U9.A08(float[], double, double):void");
    }

    public void A09(float[] fArr, double d, double d2) {
        AnonymousClass04L r4 = this.A00.A0E;
        double d3 = r4.A02;
        C05050Ob r6 = this.A01;
        A07(r6);
        double d4 = r6.A01;
        if (d4 > d3 || d3 > r6.A02) {
            d3 += (double) ((int) Math.ceil(d4 - d3));
        }
        float f = (float) r4.A0J;
        fArr[0] = ((float) (d - d3)) * f;
        fArr[1] = ((float) (d2 - r4.A03)) * f;
        r4.A0k.mapVectors(fArr);
        fArr[0] = fArr[0] + r4.A04;
        fArr[1] = fArr[1] + r4.A05;
    }

    public void A0A(float[] fArr, float f, float f2) {
        float f3;
        AnonymousClass04L r7 = this.A00.A0E;
        fArr[0] = f - r7.A04;
        fArr[1] = f2 - r7.A05;
        r7.A0l.mapVectors(fArr);
        double d = r7.A02;
        float f4 = fArr[0];
        float f5 = (float) r7.A0J;
        fArr[0] = (float) (d + ((double) (f4 / f5)));
        fArr[1] = (float) (r7.A03 + ((double) (fArr[1] / f5)));
        float f6 = fArr[0];
        if (f6 > 1.0f) {
            f3 = f6 - 1.0f;
        } else if (f6 < 0.0f) {
            f3 = f6 + 1.0f;
        } else {
            return;
        }
        fArr[0] = f3;
    }
}
