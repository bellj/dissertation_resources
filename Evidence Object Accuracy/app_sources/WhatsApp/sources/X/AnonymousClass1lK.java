package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.catalogsearch.view.adapter.CatalogSearchResultsListAdapter;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1lK  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1lK extends AnonymousClass1lL implements AbstractC33071dF, AnonymousClass1lM {
    public final AnonymousClass12P A00;
    public final C15570nT A01;
    public final AnonymousClass19T A02;
    public final C37071lG A03;
    public final UserJid A04;
    public final List A05 = new ArrayList();

    @Override // X.AbstractC33071dF
    public boolean AdR() {
        return true;
    }

    public AnonymousClass1lK(AnonymousClass12P r2, C15570nT r3, AnonymousClass19T r4, C37071lG r5, UserJid userJid) {
        this.A04 = userJid;
        this.A01 = r3;
        this.A00 = r2;
        this.A02 = r4;
        this.A03 = r5;
    }

    public static void A00(AbstractC16710pd r1) {
        AnonymousClass1lK r12 = (AnonymousClass1lK) r1.getValue();
        ((AnonymousClass1lL) r12).A00.clear();
        r12.A05.clear();
        r12.A02();
    }

    public long A0E(String str) {
        for (C92584Wm r1 : this.A05) {
            if (r1.A01.A0D.equals(str)) {
                return r1.A00;
            }
        }
        return 0;
    }

    public AbstractC75723kJ A0F(ViewGroup viewGroup, int i) {
        if (i == 9) {
            return new C59382ub(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_catalog_placeholder, viewGroup, false));
        }
        throw new IllegalStateException("product-list-base-adapter/onCreateViewHolder/unknown view type");
    }

    public void A0G() {
        boolean z = this instanceof CatalogSearchResultsListAdapter;
        boolean A0I = A0I();
        if (!z) {
            if (A0I) {
                List list = ((AnonymousClass1lL) this).A00;
                int size = list.size() - 2;
                int i = (size - 3) + 1;
                if (i < 0) {
                    Log.w("CollectionProductListBaseAdapter/hideLoadingView/hideLoadingView invalied end pos");
                    i = 0;
                }
                while (size >= i) {
                    Object obj = list.get(size);
                    if (obj instanceof C84633zd) {
                        list.remove(obj);
                        A05(size);
                    }
                    size--;
                }
            }
        } else if (A0I) {
            List list2 = ((AnonymousClass1lL) this).A00;
            ArrayList arrayList = new ArrayList();
            for (Object obj2 : list2) {
                if (obj2 instanceof C84633zd) {
                    arrayList.add(obj2);
                }
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                Object next = it.next();
                int indexOf = list2.indexOf(next);
                list2.remove(next);
                A05(indexOf);
            }
        }
    }

    public void A0H() {
        if (!(this instanceof CatalogSearchResultsListAdapter)) {
            List list = ((AnonymousClass1lL) this).A00;
            if (list.size() != 0 && !A0I()) {
                int i = 0;
                do {
                    int size = list.size() - 1;
                    list.add(size, new C84633zd(9));
                    A04(size);
                    i++;
                } while (i < 3);
            }
        } else if (!A0I()) {
            int i2 = 0;
            do {
                i2++;
                List list2 = ((AnonymousClass1lL) this).A00;
                int max = Math.max(0, list2.size() - 1);
                list2.add(max, new C84633zd(9));
                A04(max);
            } while (i2 < 3);
        }
    }

    public boolean A0I() {
        if (!(this instanceof CatalogSearchResultsListAdapter)) {
            List list = ((AnonymousClass1lL) this).A00;
            if (list.size() < 2) {
                return false;
            }
            return list.get(list.size() - 2) instanceof C84633zd;
        }
        List list2 = ((AnonymousClass1lL) this).A00;
        ArrayList arrayList = new ArrayList();
        for (Object obj : list2) {
            if (obj instanceof C84633zd) {
                arrayList.add(obj);
            }
        }
        return !arrayList.isEmpty();
    }

    @Override // X.AbstractC33071dF
    public int ADJ(int i) {
        while (i >= 0) {
            if (AJU(i)) {
                return i;
            }
            i--;
        }
        return -1;
    }

    @Override // X.AnonymousClass1lM
    public C44691zO AFw(int i) {
        return ((C84693zj) ((AnonymousClass1lL) this).A00.get(i)).A01;
    }

    @Override // X.AbstractC33071dF
    public boolean AJU(int i) {
        List list = ((AnonymousClass1lL) this).A00;
        if (i >= list.size() || i < 0 || ((AbstractC89244Jf) list.get(i)).A00 != 14) {
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r3, int i) {
        AbstractC75723kJ r32 = (AbstractC75723kJ) r3;
        if (getItemViewType(i) == 2) {
            ((C59372ua) r32).A00 = ((C84663zg) ((AnonymousClass1lL) this).A00.get(i)).A00;
        }
        r32.A09((AbstractC89244Jf) ((AnonymousClass1lL) this).A00.get(i));
    }
}
