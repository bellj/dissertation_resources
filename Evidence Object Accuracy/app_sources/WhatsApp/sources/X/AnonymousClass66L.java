package X;

import android.media.ImageReader;
import android.os.Handler;
import android.view.Surface;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.66L  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66L implements AbstractC136526Mw {
    public ImageReader A00;
    public C128475wA A01;
    public final ImageReader.OnImageAvailableListener A02 = new AnonymousClass63L(this);
    public final AtomicInteger A03 = new AtomicInteger(0);

    @Override // X.AbstractC136526Mw
    public int ADQ() {
        return 35;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(android.media.ImageReader r6, X.AnonymousClass66L r7) {
        /*
            X.5wA r5 = r7.A01
            android.media.ImageReader r0 = r7.A00
            r4 = 0
            if (r0 == 0) goto L_0x000c
            r0.setOnImageAvailableListener(r4, r4)
            r7.A01 = r4
        L_0x000c:
            android.media.Image r2 = r6.acquireLatestImage()     // Catch: Exception -> 0x002b
            if (r2 == 0) goto L_0x003f
            X.5yb r1 = new X.5yb     // Catch: all -> 0x0022
            r1.<init>()     // Catch: all -> 0x0022
            r0 = 1
            r1.A02(r2, r0, r0)     // Catch: all -> 0x0020
            r4 = r1
            r2.close()     // Catch: Exception -> 0x002b
            goto L_0x003f
        L_0x0020:
            r0 = move-exception
            goto L_0x0024
        L_0x0022:
            r0 = move-exception
            r1 = r4
        L_0x0024:
            r2.close()     // Catch: all -> 0x0027
        L_0x0027:
            throw r0     // Catch: Exception -> 0x0028
        L_0x0028:
            r3 = move-exception
            r4 = r1
            goto L_0x002c
        L_0x002b:
            r3 = move-exception
        L_0x002c:
            java.lang.String r2 = "YuvPhotoProcessor"
            java.lang.String r0 = "Failed to acquire image: "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            java.lang.String r0 = r3.getMessage()
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            X.AnonymousClass616.A01(r2, r0)
        L_0x003f:
            if (r5 == 0) goto L_0x0049
            X.5zj r0 = new X.5zj
            r0.<init>(r4)
            r5.A00(r0)
        L_0x0049:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass66L.A00(android.media.ImageReader, X.66L):void");
    }

    @Override // X.AbstractC136526Mw
    public void AIY(int i, int i2, int i3) {
        this.A00 = ImageReader.newInstance(i, i2, 35, 1);
    }

    @Override // X.AbstractC136526Mw
    public void AZT(Handler handler, C128475wA r4) {
        ImageReader imageReader = this.A00;
        if (imageReader != null) {
            this.A01 = r4;
            imageReader.setOnImageAvailableListener(this.A02, handler);
        }
    }

    @Override // X.AbstractC136526Mw
    public Surface getSurface() {
        ImageReader imageReader = this.A00;
        if (imageReader != null) {
            return imageReader.getSurface();
        }
        return null;
    }

    @Override // X.AbstractC136526Mw
    public void release() {
        ImageReader imageReader = this.A00;
        if (imageReader != null) {
            imageReader.setOnImageAvailableListener(null, null);
            this.A00.close();
            this.A00 = null;
        }
        this.A01 = null;
    }
}
