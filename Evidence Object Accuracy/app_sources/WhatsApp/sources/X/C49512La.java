package X;

import android.os.Handler;
import android.os.Looper;
import java.util.Random;

/* renamed from: X.2La  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49512La {
    public int A00 = 0;
    public long A01;
    public Runnable A02;
    public final Handler A03 = new Handler(Looper.getMainLooper());
    public final C14850m9 A04;
    public final C16120oU A05;
    public final Random A06 = new Random();

    public C49512La(C14850m9 r3, C16120oU r4) {
        this.A05 = r4;
        this.A04 = r3;
    }

    public void A00(Integer num, int i) {
        if (this.A04.A07(1608)) {
            AnonymousClass30N r2 = new AnonymousClass30N();
            r2.A00 = num;
            r2.A01 = 0;
            r2.A02 = Long.valueOf(this.A01);
            int i2 = 17;
            if (i != 97) {
                i2 = 16;
                if (i != 100) {
                    i2 = 13;
                    if (i != 103) {
                        i2 = 12;
                        if (i != 105) {
                            i2 = 14;
                            if (i != 108) {
                                i2 = 2;
                                if (i != 117) {
                                    if (i == 118) {
                                        i2 = 15;
                                    }
                                    this.A05.A06(r2);
                                }
                            }
                        }
                    }
                }
            }
            r2.A01 = Integer.valueOf(i2);
            this.A05.A06(r2);
        }
    }
}
