package X;

import android.view.animation.Animation;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.emoji.EmojiPopupFooter;

/* renamed from: X.2hH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54782hH extends AbstractC05270Ox {
    public final /* synthetic */ C15260mp A00;

    public C54782hH(C15260mp r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        EmojiPopupFooter emojiPopupFooter;
        C52592bM r0;
        int i2 = 0;
        if (i == 0) {
            C15260mp r4 = this.A00;
            int height = ((C15270mq) r4).A08.getHeight();
            if (((C15270mq) r4).A04 > 0 && ((C15270mq) r4).A08.A00 > (height >> 1)) {
                i2 = height;
            }
            emojiPopupFooter = ((C15270mq) r4).A08;
            if (i2 != emojiPopupFooter.A00) {
                r0 = new C52592bM(r4, i2);
            } else {
                return;
            }
        } else if (i == 1 && recyclerView.computeVerticalScrollRange() <= recyclerView.getHeight()) {
            C15260mp r1 = this.A00;
            emojiPopupFooter = ((C15270mq) r1).A08;
            if (emojiPopupFooter.A00 != 0) {
                r0 = new C52592bM(r1, 0);
            } else {
                return;
            }
        } else {
            return;
        }
        emojiPopupFooter.startAnimation(r0);
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        C15260mp r3 = this.A00;
        if (i2 == 0) {
            EmojiPopupFooter emojiPopupFooter = ((C15270mq) r3).A08;
            if (emojiPopupFooter.A00 != 0) {
                emojiPopupFooter.startAnimation(new C52592bM(r3, 0));
                ((C15270mq) r3).A04 = 0;
                return;
            }
            return;
        }
        Animation animation = ((C15270mq) r3).A08.getAnimation();
        if (animation != null) {
            animation.cancel();
        }
        EmojiPopupFooter emojiPopupFooter2 = ((C15270mq) r3).A08;
        emojiPopupFooter2.setTopOffset(emojiPopupFooter2.A00 + i2);
        ((C15270mq) r3).A04 = i2;
    }
}
