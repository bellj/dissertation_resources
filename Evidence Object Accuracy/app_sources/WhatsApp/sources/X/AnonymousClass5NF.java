package X;

import java.io.IOException;

/* renamed from: X.5NF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NF extends AnonymousClass1TL {
    public static final AnonymousClass5NF A01 = new AnonymousClass5NF((byte) 0);
    public static final AnonymousClass5NF A02 = new AnonymousClass5NF((byte) -1);
    public final byte A00;

    public AnonymousClass5NF(byte b) {
        this.A00 = b;
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return 3;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    public static AnonymousClass5NF A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NF)) {
            return (AnonymousClass5NF) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return (AnonymousClass5NF) AnonymousClass1TL.A03((byte[]) obj);
            } catch (IOException e) {
                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("failed to construct boolean from byte[]: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    public static AnonymousClass5NF A01(AnonymousClass5NU r1) {
        AnonymousClass1TL A00 = AnonymousClass5NU.A00(r1);
        if (A00 instanceof AnonymousClass5NF) {
            return A00(A00);
        }
        return A04(AnonymousClass5NH.A05(A00));
    }

    public static AnonymousClass5NF A04(byte[] bArr) {
        if (bArr.length == 1) {
            byte b = bArr[0];
            if (b != -1) {
                return b != 0 ? new AnonymousClass5NF(b) : A01;
            }
            return A02;
        }
        throw C12970iu.A0f("BOOLEAN value should have 1 byte in it");
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return this.A00 != 0 ? A02 : A01;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r4, boolean z) {
        byte b = this.A00;
        if (z) {
            r4.A00.write(1);
        }
        r4.A02(1);
        r4.A00.write(b);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r4) {
        if (!(r4 instanceof AnonymousClass5NF) || C12960it.A1S(this.A00) != C12960it.A1S(((AnonymousClass5NF) r4).A00)) {
            return false;
        }
        return true;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return C12960it.A1S(this.A00) ? 1 : 0;
    }

    public String toString() {
        return this.A00 != 0 ? "TRUE" : "FALSE";
    }
}
