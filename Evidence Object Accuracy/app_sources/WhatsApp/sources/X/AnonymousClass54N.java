package X;

/* renamed from: X.54N  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass54N implements AnonymousClass2K1 {
    public final /* synthetic */ AnonymousClass2K0 A00;

    public /* synthetic */ AnonymousClass54N(AnonymousClass2K0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        AbstractC116565Vy r0 = this.A00.A05;
        if (r0 != null) {
            r0.ANR(i);
        }
    }

    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        AnonymousClass4T8 r2 = (AnonymousClass4T8) obj;
        AbstractC116565Vy r0 = this.A00.A05;
        if (r0 != null) {
            r0.ANS(r2);
        }
    }
}
