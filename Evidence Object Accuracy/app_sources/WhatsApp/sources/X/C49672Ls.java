package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Ls  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49672Ls implements AbstractC49682Lt {
    public final /* synthetic */ HomeActivity A00;

    @Override // X.AbstractC49682Lt
    public int AEl() {
        return 1;
    }

    public C49672Ls(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    public final void A00() {
        HomeActivity homeActivity = this.A00;
        homeActivity.A0b.A0M(false);
        homeActivity.A0b.A0E(0);
        homeActivity.A08.setVisibility(0);
        homeActivity.A0M.A0F(HomeActivity.A02(((ActivityC13830kP) homeActivity).A01, 200), false);
    }

    @Override // X.AbstractC49682Lt
    public void ANa() {
        A00();
        HomeActivity homeActivity = this.A00;
        homeActivity.getWindow().clearFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        homeActivity.getWindow().addFlags(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
    }

    @Override // X.AbstractC49682Lt
    public void AVn() {
        A00();
    }
}
