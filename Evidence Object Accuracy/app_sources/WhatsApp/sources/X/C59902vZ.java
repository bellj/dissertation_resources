package X;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.2vZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59902vZ extends AbstractC37191le {
    public final Activity A00;
    public final FrameLayout A01;
    public final WaImageView A02;
    public final WaTextView A03;
    public final C251118d A04;

    public C59902vZ(Activity activity, View view, C251118d r4) {
        super(view);
        this.A00 = activity;
        this.A04 = r4;
        this.A03 = C12960it.A0N(view, R.id.category_name);
        this.A02 = C12980iv.A0X(view, R.id.category_icon);
        this.A01 = (FrameLayout) AnonymousClass028.A0D(view, R.id.category_layout);
    }
}
