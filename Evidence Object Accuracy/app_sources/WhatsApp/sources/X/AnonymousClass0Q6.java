package X;

import android.os.Build;
import android.view.View;

/* renamed from: X.0Q6  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Q6 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final Class A03;

    public abstract Object A01(View view);

    public abstract void A03(View view, Object obj);

    public abstract boolean A04(Object obj, Object obj2);

    public AnonymousClass0Q6(Class cls, int i, int i2, int i3) {
        this.A02 = i;
        this.A03 = cls;
        this.A00 = i2;
        this.A01 = i3;
    }

    public Object A00(View view) {
        int i = Build.VERSION.SDK_INT;
        if (i >= this.A01) {
            return A01(view);
        }
        if (i < 19) {
            return null;
        }
        Object tag = view.getTag(this.A02);
        if (this.A03.isInstance(tag)) {
            return tag;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        if (r0 == null) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.view.View r3, java.lang.Object r4) {
        /*
            r2 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            int r0 = r2.A01
            if (r1 < r0) goto L_0x000a
            r2.A03(r3, r4)
        L_0x0009:
            return
        L_0x000a:
            r0 = 19
            if (r1 < r0) goto L_0x0009
            java.lang.Object r0 = r2.A00(r3)
            boolean r0 = r2.A04(r0, r4)
            if (r0 == 0) goto L_0x0009
            android.view.View$AccessibilityDelegate r1 = X.AnonymousClass028.A0C(r3)
            if (r1 == 0) goto L_0x0028
            boolean r0 = r1 instanceof X.AnonymousClass07X
            if (r0 == 0) goto L_0x003b
            X.07X r1 = (X.AnonymousClass07X) r1
            X.04v r0 = r1.A00
            if (r0 != 0) goto L_0x002d
        L_0x0028:
            X.04v r0 = new X.04v
            r0.<init>()
        L_0x002d:
            X.AnonymousClass028.A0g(r3, r0)
            int r0 = r2.A02
            r3.setTag(r0, r4)
            int r0 = r2.A00
            X.AnonymousClass028.A0W(r3, r0)
            return
        L_0x003b:
            X.04v r0 = new X.04v
            r0.<init>(r1)
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Q6.A02(android.view.View, java.lang.Object):void");
    }
}
