package X;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.util.ViewOnClickCListenerShape18S0100000_I1_1;
import java.io.File;

/* renamed from: X.2Vy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC51682Vy extends AnonymousClass03U {
    public void A0A() {
    }

    public void A0B(boolean z) {
    }

    public void A0C(boolean z) {
    }

    public AbstractC51682Vy(View view) {
        super(view);
    }

    public void A08() {
        AnonymousClass31z r4;
        if (this instanceof C617331x) {
            C617331x r3 = (C617331x) this;
            if (r3.A00 == null) {
                ImageView imageView = r3.A04;
                imageView.setOnClickListener(null);
                r3.A03.setOnClickListener(null);
                imageView.setVisibility(8);
                return;
            }
            ViewOnClickCListenerShape18S0100000_I1_1 viewOnClickCListenerShape18S0100000_I1_1 = new ViewOnClickCListenerShape18S0100000_I1_1(r3, 23);
            ImageView imageView2 = r3.A04;
            imageView2.setOnClickListener(viewOnClickCListenerShape18S0100000_I1_1);
            ViewGroup viewGroup = r3.A03;
            viewGroup.setOnClickListener(viewOnClickCListenerShape18S0100000_I1_1);
            viewGroup.setContentDescription(viewGroup.getContext().getString(R.string.gif_preview_description));
            WaImageView waImageView = r3.A05;
            if (waImageView != null) {
                C66013Ly r6 = r3.A00;
                C66003Lx r1 = r6.A01;
                int i = r1.A01;
                if (i <= 0) {
                    i = r6.A02.A01;
                }
                int i2 = r1.A00;
                if (i2 <= 0) {
                    i2 = r6.A02.A00;
                }
                if (i == 0 || i2 == 0) {
                    i2 = 1;
                    i = 1;
                }
                StringBuilder sb = new StringBuilder("h,");
                sb.append(i);
                sb.append(":");
                sb.append(i2);
                String obj = sb.toString();
                AnonymousClass064 r0 = (AnonymousClass064) waImageView.getLayoutParams();
                r0.A0t = obj;
                waImageView.setLayoutParams(r0);
            }
            imageView2.setVisibility(0);
            imageView2.setImageDrawable(new ColorDrawable(13421772));
            if (r3.A09 == null) {
                r3.A07.A01(imageView2, r3.A00.A03.A02);
                return;
            }
            String str = r3.A00.A02.A02;
            r3.A02 = str;
            C253719d r12 = r3.A07;
            AnonymousClass3XO r122 = new AnonymousClass3XO(r3);
            AnonymousClass009.A01();
            AnonymousClass1O6 A06 = r12.A07.A06();
            C39391pp A00 = A06.A00(str);
            if (A00 != null) {
                String str2 = A00.A00;
                if (new File(str2).exists() && A00.A02 != null) {
                    r122.AQX(new File(str2), str, A00.A02);
                    r4 = null;
                    r3.A01 = r4;
                }
            }
            Mp4Ops mp4Ops = r12.A05;
            r4 = new AnonymousClass31z(r12.A02, mp4Ops, r12.A06, r12.A08, r12.A09, r12.A0A, A06, r122, str);
            ((AbstractC16350or) r4).A02.executeOnExecutor(r12.A00(), new Void[0]);
            r3.A01 = r4;
        }
    }

    public void A09() {
        if (this instanceof C617331x) {
            C617331x r3 = (C617331x) this;
            AbstractC16350or r1 = r3.A01;
            if (r1 != null) {
                r1.A03(false);
                r3.A01 = null;
            }
            AnonymousClass3BO r12 = r3.A09;
            if (r12 != null) {
                AnonymousClass2Zb r0 = r12.A00;
                if (r0 != null) {
                    r0.stop();
                    r12.A00 = null;
                }
                C38911ou r02 = r12.A01;
                if (r02 != null) {
                    r02.close();
                    r12.A01 = null;
                }
                r12.A03.setImageDrawable(null);
            }
            r3.A02 = null;
        }
    }

    public boolean A0D() {
        return this instanceof C617331x;
    }
}
