package X;

import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.Set;

/* renamed from: X.3T1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3T1 implements AnonymousClass5Sa, AnonymousClass5SZ {
    public IAccountAccessor A00 = null;
    public Set A01 = null;
    public boolean A02 = false;
    public final AbstractC72443eb A03;
    public final AnonymousClass4XF A04;
    public final /* synthetic */ C65863Lh A05;

    public AnonymousClass3T1(AbstractC72443eb r2, AnonymousClass4XF r3, C65863Lh r4) {
        this.A05 = r4;
        this.A03 = r2;
        this.A04 = r3;
    }

    @Override // X.AnonymousClass5Sa
    public final void AV1(C56492ky r3) {
        C12980iv.A18(this.A05.A06, r3, this, 11);
    }

    @Override // X.AnonymousClass5SZ
    public final void AgX(C56492ky r7) {
        C14970mL r5 = (C14970mL) this.A05.A09.get(this.A04);
        if (r5 != null) {
            C13020j0.A00(r5.A0C.A06);
            AbstractC72443eb r4 = r5.A04;
            String A0s = C12980iv.A0s(r4);
            String valueOf = String.valueOf(r7);
            StringBuilder A0t = C12980iv.A0t(A0s.length() + 25 + valueOf.length());
            A0t.append("onSignInFailed for ");
            A0t.append(A0s);
            A0t.append(" with ");
            AbstractC95064d1 r42 = (AbstractC95064d1) r4;
            r42.A0S = C12960it.A0d(valueOf, A0t);
            r42.A8y();
            r5.A07(r7, null);
        }
    }
}
