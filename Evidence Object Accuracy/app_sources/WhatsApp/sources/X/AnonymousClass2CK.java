package X;

import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.os.Handler;
import android.os.Looper;
import com.whatsapp.util.Log;

/* renamed from: X.2CK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2CK extends AbstractC28651Ol {
    public final MediaPlayer A00;
    public final Handler A01;

    public AnonymousClass2CK(int i) {
        Looper myLooper = Looper.myLooper();
        this.A01 = new Handler(myLooper == null ? Looper.getMainLooper() : myLooper);
        MediaPlayer mediaPlayer = new MediaPlayer();
        this.A00 = mediaPlayer;
        mediaPlayer.setAudioStreamType(i);
    }

    @Override // X.AbstractC28651Ol
    public boolean A0E(AbstractC15710nm r7, float f) {
        float f2 = -1.0f;
        try {
            MediaPlayer mediaPlayer = this.A00;
            PlaybackParams playbackParams = mediaPlayer.getPlaybackParams();
            playbackParams.allowDefaults();
            f2 = playbackParams.getSpeed();
            if (Math.abs(f2 - f) >= 0.1f) {
                mediaPlayer.setPlaybackParams(playbackParams.setSpeed(f));
            }
            return true;
        } catch (IllegalArgumentException | IllegalStateException e) {
            StringBuilder sb = new StringBuilder("audioplayer/setPlaybackSpeed failed: currSpeed: ");
            sb.append(f2);
            sb.append(" newSpeed: ");
            sb.append(f);
            sb.append(" ");
            sb.append(e.toString());
            Log.e(sb.toString());
            return false;
        }
    }
}
