package X;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.whatsapp.R;
import java.util.AbstractCollection;

/* renamed from: X.5nM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123565nM extends C118185bP {
    public int A00 = 4;
    public int A01 = 4;
    public C126765tP A02;
    public String A03;
    public boolean A04 = true;
    public boolean A05 = false;
    public final AnonymousClass12P A06;
    public final C130155yt A07;
    public final C30931Zj A08 = C117305Zk.A0V("NoviPaymentTransactionDetailsViewModel", "payment-settings");
    public final AnonymousClass60Y A09;
    public final AnonymousClass61F A0A;
    public final C130095yn A0B;
    public final C129835yN A0C;
    public final C129185xJ A0D;

    public C123565nM(Bundle bundle, AnonymousClass12P r33, C14900mE r34, C15570nT r35, C14650lo r36, AnonymousClass1AA r37, C238013b r38, C15550nR r39, C15610nY r40, AnonymousClass01d r41, C14830m7 r42, C16590pI r43, AnonymousClass018 r44, C15650ng r45, C20300vX r46, C20370ve r47, AnonymousClass102 r48, C241414j r49, C129925yW r50, C20380vf r51, C21860y6 r52, C243515e r53, C22710zW r54, C17070qD r55, C130155yt r56, AnonymousClass1A7 r57, AbstractC16870pt r58, AnonymousClass60Y r59, AnonymousClass61F r60, AnonymousClass17Z r61, C130095yn r62, AnonymousClass604 r63, AnonymousClass14X r64, AbstractC14440lR r65) {
        super(bundle, r34, r35, r36, r37, r38, r39, r41, r42, r43, r44, r45, r46, r47, r48, r49, r50, r51, r52, r53, r54, r55, r57, r58, r61, r63, r64, r65);
        this.A06 = r33;
        this.A09 = r59;
        this.A07 = r56;
        this.A0A = r60;
        this.A0B = r62;
        this.A0C = new C129835yN(r43);
        this.A0D = new C129185xJ(r43.A00, r39, r40, r42, r44, r64);
    }

    public static void A02(Context context, AnonymousClass018 r4, AnonymousClass102 r5, C1316563p r6, AbstractCollection abstractCollection) {
        abstractCollection.add(new C123265ms(C1316563p.A01(context, r4, r5.A02(r6.A03), r5.A02(r6.A04), r6.A05), R.dimen.novi_payment_transaction_detail_breakdown_item_default_top_margin, R.dimen.product_margin_16dp, 8388613, R.color.payments_desc_font_color));
    }

    public static void A03(CharSequence charSequence, CharSequence charSequence2, AbstractCollection abstractCollection, int i) {
        abstractCollection.add(C123335mz.A00(charSequence, charSequence2, i));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x037c, code lost:
        if (r5.A01.A02() == false) goto L_0x037e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x041d  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x041f A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0439 A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0465 A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x04ab A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x04c4 A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0505 A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011e A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0288 A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x03b6 A[Catch: Exception -> 0x0560, TryCatch #0 {Exception -> 0x0560, blocks: (B:3:0x0014, B:11:0x0024, B:14:0x0056, B:15:0x0063, B:16:0x006e, B:17:0x006f, B:21:0x00d0, B:22:0x00d6, B:23:0x00db, B:24:0x00df, B:26:0x00f0, B:28:0x00f6, B:29:0x00f9, B:31:0x011e, B:32:0x012b, B:34:0x0158, B:36:0x01a9, B:37:0x01ac, B:39:0x01b7, B:41:0x01bf, B:42:0x01c4, B:43:0x01c8, B:45:0x01d5, B:47:0x01db, B:48:0x01de, B:51:0x0204, B:53:0x020e, B:56:0x0218, B:59:0x0222, B:62:0x022c, B:65:0x0236, B:66:0x023c, B:69:0x0263, B:71:0x026c, B:72:0x0274, B:74:0x0288, B:75:0x02a4, B:77:0x0304, B:78:0x0329, B:79:0x032c, B:80:0x0333, B:81:0x034a, B:83:0x0375, B:86:0x037f, B:88:0x0384, B:91:0x038c, B:94:0x039c, B:95:0x03a5, B:97:0x03b6, B:99:0x03c4, B:100:0x03d5, B:101:0x0412, B:102:0x041a, B:104:0x041f, B:105:0x0439, B:107:0x0465, B:108:0x0472, B:109:0x04ab, B:110:0x04c4, B:111:0x04f4, B:113:0x0505, B:116:0x051d, B:117:0x0535, B:118:0x0549, B:119:0x0556, B:120:0x055a), top: B:126:0x0014 }] */
    @Override // X.C118185bP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G(java.util.List r32) {
        /*
        // Method dump skipped, instructions count: 1454
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C123565nM.A0G(java.util.List):void");
    }

    public final C130205yy A0S() {
        C127915vG r9 = super.A06;
        AnonymousClass009.A05(r9);
        AnonymousClass1IR r6 = r9.A01;
        AnonymousClass009.A05(r6);
        AbstractC30891Zf r0 = r6.A0A;
        AnonymousClass009.A05(r0);
        AbstractC1316063k r7 = ((C119825fA) r0).A01;
        AnonymousClass009.A05(r7);
        C14830m7 r3 = this.A0N;
        C16590pI r4 = this.A0O;
        return new C130205yy(r4.A00, this.A0L, r3, r4, this.A0P, r6, r7, this, r9, this.A0C, this.A0g);
    }

    public void A0T() {
        C129185xJ r2 = this.A0D;
        AnonymousClass1IR r1 = super.A06.A01;
        AbstractC130195yx A00 = r2.A00(r1.A03);
        AnonymousClass009.A05(A00);
        A00.A06(r1);
        if (A00 instanceof C123765np) {
            C123765np r22 = (C123765np) A00;
            AbstractC121025h8 r0 = r22.A01;
            AnonymousClass009.A05(r0);
            if (r0.A02 == 1) {
                int i = ((AbstractC130195yx) r22).A01.A02;
                if (i == 608 || i == 602) {
                    this.A00 = 1;
                    if (r22.A0A()) {
                        this.A01 = 1;
                    }
                    A09();
                    if (this.A04) {
                        return;
                    }
                    if (this.A0A.A0H() || this.A05) {
                        C130155yt r5 = this.A07;
                        AnonymousClass61S[] r3 = new AnonymousClass61S[2];
                        AnonymousClass61S.A05("action", "novi-get-cash-withdrawal-code", r3, 0);
                        r5.A0B(new IDxAListenerShape19S0100000_3_I1(this, 13), C117305Zk.A0Q(C12960it.A0m(AnonymousClass61S.A00("transaction_id", super.A0B), r3, 1)), "get", 5);
                        return;
                    }
                    this.A05 = true;
                    super.A08.A0B(new C123525nI(505));
                }
            }
        }
    }

    public void A0U() {
        AnonymousClass610 r1 = new AnonymousClass610("GET_HELP_CLICK", "SEND_MONEY", "REVIEW_TRANSACTION", "BODY");
        String str = super.A0B;
        C128365vz r12 = r1.A00;
        C118185bP.A00(r12, this, str);
        this.A09.A06(r12);
        C128315vu A00 = C128315vu.A00(10);
        A00.A05 = super.A06.A01;
        C118185bP.A01(this, A00);
    }

    public void A0V(Context context) {
        Activity A00 = AbstractC35731ia.A00(context);
        AnonymousClass009.A05(A00);
        AbstractC001200n r5 = (AbstractC001200n) A00;
        C127915vG r0 = super.A06;
        if (r0 != null) {
            C130095yn r4 = this.A0B;
            String str = r0.A01.A0K;
            AnonymousClass009.A05(str);
            AnonymousClass016 A0T = C12980iv.A0T();
            r4.A0A.Ab2(new AnonymousClass6JG(A0T, r4, str));
            C117295Zj.A0s(r5, A0T, this, 146);
        }
    }

    public void A0W(C123315mx r6) {
        AnonymousClass610 r1 = new AnonymousClass610("RECEIVER_SELECTED", "PAYMENT_HISTORY", "REVIEW_TRANSACTION", "BODY");
        String str = r6.A09;
        C128365vz r12 = r1.A00;
        r12.A0L = str;
        C118185bP.A00(r12, this, super.A0B);
        this.A09.A06(r12);
        Runnable A04 = super.A04(r6);
        if (A04 != null) {
            A04.run();
        }
    }

    public boolean A0X(String str) {
        return super.A0R(str);
    }
}
