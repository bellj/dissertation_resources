package X;

/* renamed from: X.5H7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5H7 extends RuntimeException {
    public AnonymousClass5H7(String str) {
        super(str);
    }

    public AnonymousClass5H7(String str, Throwable th) {
        super(str, th);
    }

    public AnonymousClass5H7(Throwable th) {
        super(th);
    }
}
