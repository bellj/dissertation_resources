package X;

/* renamed from: X.1zA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44561zA {
    public final int A00;
    public final String A01;

    public C44561zA(int i, String str) {
        this.A00 = i;
        this.A01 = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IntegrityCheckResult{result=");
        sb.append(this.A00);
        sb.append(", jidSuffix='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
