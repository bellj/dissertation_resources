package X;

import android.os.Bundle;
import com.whatsapp.jid.UserJid;

/* renamed from: X.5vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128315vu {
    public int A00;
    public int A01;
    public Bundle A02 = C12970iu.A0D();
    public C15370n3 A03;
    public AbstractC28901Pl A04;
    public AnonymousClass1IR A05;
    public UserJid A06;
    public AnonymousClass1ZR A07;
    public AnonymousClass1IS A08;
    public Class A09;
    public Class A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;
    public boolean A0H;

    public C128315vu(int i) {
        this.A00 = i;
    }

    public static C128315vu A00(int i) {
        return new C128315vu(i);
    }
}
