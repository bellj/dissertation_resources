package X;

import android.hardware.camera2.CaptureRequest;

/* renamed from: X.66I  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66I implements AbstractC136416Ml, AnonymousClass6MJ {
    public static final float[] A0I = new float[4];
    public static final int[] A0J = new int[18];
    public int A00 = 0;
    public C125425rE A01;
    public AnonymousClass6L0 A02;
    public C128455w8 A03;
    public C128465w9 A04;
    public C129475xm A05;
    public AbstractC136176Lh A06;
    public AbstractC136186Li A07;
    public C1310360y A08;
    public Boolean A09;
    public Integer A0A;
    public boolean A0B;
    public final AnonymousClass6Lj A0C;
    public final C129875yR A0D;
    public volatile int A0E = 0;
    public volatile boolean A0F;
    public volatile boolean A0G;
    public volatile boolean A0H;

    public AnonymousClass66I() {
        AnonymousClass66E r1 = new AnonymousClass66E(this);
        this.A0C = r1;
        this.A0G = true;
        C129875yR r0 = new C129875yR();
        this.A0D = r0;
        r0.A01 = r1;
    }

    public final void A00(C1310360y r3) {
        if (this.A0E == 1) {
            this.A0E = 0;
            this.A09 = Boolean.TRUE;
            this.A08 = r3;
            this.A0D.A01();
            AbstractC136186Li r0 = this.A07;
            if (r0 != null) {
                r0.AUG();
                return;
            }
            return;
        }
        throw C12990iw.A0m("Starting preview outside BLOCK_STATE_STARTING_PREVIEW state");
    }

    public final void A01(C1310360y r3) {
        if (this.A0E == 7) {
            this.A0E = 0;
            this.A09 = Boolean.TRUE;
            this.A08 = r3;
            this.A0D.A01();
            return;
        }
        throw C12990iw.A0m("Starting recording outside BLOCK_STATE_STARTING_RECORD state");
    }

    @Override // X.AnonymousClass6MJ
    public void A6e() {
        this.A0D.A00();
    }

    @Override // X.AnonymousClass6MJ
    public /* bridge */ /* synthetic */ Object AGH() {
        Boolean bool = this.A09;
        if (bool == null) {
            throw C12960it.A0U("Start Preview operation hasn't completed yet.");
        } else if (bool.booleanValue()) {
            return this.A08;
        } else {
            throw this.A02;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0074, code lost:
        if (r0 != 5) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0081, code lost:
        if (r7.intValue() != 4) goto L_0x0083;
     */
    @Override // X.AbstractC136416Ml
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANi(X.C1310360y r9, X.C129425xh r10) {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass66I.ANi(X.60y, X.5xh):void");
    }

    @Override // X.AbstractC136416Ml
    public void ANj(C129415xg r3, C1310360y r4) {
        if (!this.A0G) {
            return;
        }
        if (this.A0E == 1 || this.A0E == 7) {
            this.A0E = 0;
            this.A09 = Boolean.FALSE;
            this.A02 = new AnonymousClass6L0(C12960it.A0f(C12960it.A0k("Failed to start operation. Reason: "), r3.A00()));
            if (this.A01 != null) {
                r3.A00();
            }
            this.A0D.A01();
        }
    }

    @Override // X.AbstractC136416Ml
    public void ANk(CaptureRequest captureRequest, C1310360y r4, long j, long j2) {
        if (!this.A0G) {
            return;
        }
        if (this.A0E == 1) {
            A00(r4);
        } else if (this.A0E == 7) {
            A01(r4);
        }
    }
}
