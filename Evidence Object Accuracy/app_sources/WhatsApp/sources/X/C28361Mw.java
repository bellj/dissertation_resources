package X;

import java.io.Serializable;

/* renamed from: X.1Mw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28361Mw implements Serializable {
    public static final long serialVersionUID = 0;
    public final Object[] elements;

    public C28361Mw(Object[] objArr) {
        this.elements = objArr;
    }

    public Object readResolve() {
        return AbstractC17940re.copyOf(this.elements);
    }
}
