package X;

/* renamed from: X.1XL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XL extends C28861Ph implements AbstractC16400ox, AbstractC16410oy, AbstractC16420oz {
    public int A00;
    public String A01;

    public AnonymousClass1XL(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 32, j);
    }

    public AnonymousClass1XL(AnonymousClass1IS r8, AnonymousClass1XL r9, long j) {
        super(r8, r9, j, true);
        this.A01 = r9.A01;
    }
}
