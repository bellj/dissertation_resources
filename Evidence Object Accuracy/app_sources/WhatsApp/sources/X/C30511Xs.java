package X;

import com.whatsapp.jid.UserJid;
import java.text.SimpleDateFormat;
import java.util.Locale;

/* renamed from: X.1Xs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30511Xs extends AnonymousClass1XB {
    public UserJid A00;
    public UserJid A01;
    public transient long A02;
    public final transient AbstractC15710nm A03;

    public C30511Xs(AbstractC15710nm r1, AnonymousClass1IS r2, int i, long j) {
        super(r2, i, j);
        this.A03 = r1;
    }

    public final String A15() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        StringBuilder sb = new StringBuilder("id=");
        sb.append(this.A11);
        sb.append(", created=");
        sb.append(simpleDateFormat.format(Long.valueOf(this.A0I)));
        return sb.toString();
    }
}
