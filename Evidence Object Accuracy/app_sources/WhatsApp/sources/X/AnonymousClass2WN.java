package X;

/* renamed from: X.2WN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2WN extends AnonymousClass2Vu implements AbstractC51662Vw {
    public final AbstractC15340mz A00;

    public AnonymousClass2WN(AbstractC15340mz r2) {
        super(r2, 99);
        this.A00 = r2;
    }

    @Override // X.AbstractC51662Vw
    public AbstractC14640lm ADg() {
        return this.A00.A0z.A00;
    }
}
