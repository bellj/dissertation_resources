package X;

/* renamed from: X.69B  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69B implements AnonymousClass17W {
    public final C22180yf A00;
    public final C14850m9 A01;
    public final AnonymousClass6BC A02;

    @Override // X.AnonymousClass17W
    public String ABE() {
        return "c";
    }

    public AnonymousClass69B(C22180yf r1, C14850m9 r2, AnonymousClass6BC r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass17W
    public boolean A99(String str) {
        return "br".equals(str);
    }

    @Override // X.AnonymousClass17W
    public String AAu() {
        return this.A01.A03(1349);
    }
}
