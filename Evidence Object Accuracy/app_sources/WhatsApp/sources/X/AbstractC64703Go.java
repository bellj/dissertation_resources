package X;

import android.text.TextUtils;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Go  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC64703Go {
    /* JADX DEBUG: Multi-variable search result rejected for r4v6, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r4v7, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r4v9, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        if (android.text.TextUtils.isEmpty((java.lang.String) r7) != false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(java.lang.Object r7, int r8) {
        /*
            r0 = 10
            if (r8 <= r0) goto L_0x0007
            java.lang.String r0 = "ERROR: Recursive toString calls"
            return r0
        L_0x0007:
            java.lang.String r5 = ""
            if (r7 == 0) goto L_0x0018
            boolean r0 = r7 instanceof java.lang.String
            if (r0 == 0) goto L_0x0019
            r0 = r7
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x00eb
        L_0x0018:
            return r5
        L_0x0019:
            boolean r0 = r7 instanceof java.lang.Integer
            if (r0 == 0) goto L_0x0024
            int r4 = X.C12960it.A05(r7)
        L_0x0021:
            if (r4 != 0) goto L_0x00eb
            return r5
        L_0x0024:
            boolean r0 = r7 instanceof java.lang.Long
            if (r0 == 0) goto L_0x0031
            long r2 = X.C12980iv.A0G(r7)
            r0 = 0
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            goto L_0x0021
        L_0x0031:
            boolean r0 = r7 instanceof java.lang.Double
            if (r0 == 0) goto L_0x0041
            r0 = r7
            java.lang.Number r0 = (java.lang.Number) r0
            double r2 = r0.doubleValue()
            r0 = 0
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            goto L_0x0021
        L_0x0041:
            boolean r0 = r7 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x004a
            boolean r4 = X.C12970iu.A1Y(r7)
            goto L_0x0021
        L_0x004a:
            boolean r0 = r7 instanceof java.util.List
            java.lang.String r6 = ", "
            if (r0 == 0) goto L_0x0087
            java.lang.StringBuilder r5 = X.C12960it.A0h()
            if (r8 <= 0) goto L_0x005b
            java.lang.String r0 = "["
            r5.append(r0)
        L_0x005b:
            java.util.List r7 = (java.util.List) r7
            int r3 = r5.length()
            java.util.Iterator r2 = r7.iterator()
        L_0x0065:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0082
            java.lang.Object r1 = r2.next()
            int r0 = r5.length()
            if (r0 <= r3) goto L_0x0078
            r5.append(r6)
        L_0x0078:
            int r0 = r8 + 1
            java.lang.String r0 = A00(r1, r0)
            r5.append(r0)
            goto L_0x0065
        L_0x0082:
            if (r8 <= 0) goto L_0x00e6
            java.lang.String r0 = "]"
            goto L_0x00e3
        L_0x0087:
            boolean r0 = r7 instanceof java.util.Map
            if (r0 == 0) goto L_0x00eb
            java.lang.StringBuilder r5 = X.C12960it.A0h()
            java.util.Map r7 = (java.util.Map) r7
            java.util.TreeMap r0 = new java.util.TreeMap
            r0.<init>(r7)
            java.util.Iterator r7 = X.C12990iw.A0s(r0)
            r4 = 0
            r3 = 0
        L_0x009c:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x00de
            java.util.Map$Entry r2 = X.C12970iu.A15(r7)
            java.lang.Object r1 = r2.getValue()
            int r0 = r8 + 1
            java.lang.String r1 = A00(r1, r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x009c
            if (r8 <= 0) goto L_0x00c5
            if (r4 != 0) goto L_0x00c5
            java.lang.String r0 = "{"
            r5.append(r0)
            int r3 = r5.length()
            r4 = 1
        L_0x00c5:
            int r0 = r5.length()
            if (r0 <= r3) goto L_0x00ce
            r5.append(r6)
        L_0x00ce:
            java.lang.String r0 = X.C12990iw.A0r(r2)
            r5.append(r0)
            r0 = 61
            r5.append(r0)
            r5.append(r1)
            goto L_0x009c
        L_0x00de:
            if (r4 == 0) goto L_0x00e6
            java.lang.String r0 = "}"
        L_0x00e3:
            r5.append(r0)
        L_0x00e6:
            java.lang.String r0 = r5.toString()
            return r0
        L_0x00eb:
            java.lang.String r0 = r7.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC64703Go.A00(java.lang.Object, int):java.lang.String");
    }

    public static String A01(Object obj, Object obj2, AbstractMap abstractMap) {
        abstractMap.put(obj, obj2);
        return A00(abstractMap, 0);
    }

    public void A02(AbstractC64703Go r8) {
        CharSequence charSequence;
        Object obj;
        Map map;
        Map map2;
        if (!(this instanceof C56172kS) && !(this instanceof C56162kR)) {
            if (this instanceof C56202kV) {
                C56202kV r82 = (C56202kV) r8;
                TextUtils.isEmpty(null);
                int i = ((C56202kV) this).A00;
                if (i != 0) {
                    r82.A00 = i;
                }
                TextUtils.isEmpty(null);
                if (!TextUtils.isEmpty(null)) {
                    TextUtils.isEmpty(null);
                    return;
                }
                return;
            } else if (this instanceof C56232kY) {
                C56232kY r2 = (C56232kY) this;
                C56232kY r83 = (C56232kY) r8;
                if (!TextUtils.isEmpty(r2.A00)) {
                    r83.A00 = r2.A00;
                }
                if (!TextUtils.isEmpty(r2.A01)) {
                    r83.A01 = r2.A01;
                }
                if (!TextUtils.isEmpty(r2.A02)) {
                    r83.A02 = r2.A02;
                }
                if (!TextUtils.isEmpty(r2.A03)) {
                    r83.A03 = r2.A03;
                }
                if (r2.A04) {
                    r83.A04 = true;
                }
                TextUtils.isEmpty(null);
                if (r2.A05) {
                    r83.A05 = true;
                    return;
                }
                return;
            } else if (this instanceof C56152kQ) {
                charSequence = null;
                TextUtils.isEmpty(charSequence);
            } else if (!(this instanceof C56142kP)) {
                if (this instanceof C56222kX) {
                    C56222kX r22 = (C56222kX) this;
                    C56222kX r84 = (C56222kX) r8;
                    r84.A00.addAll(r22.A00);
                    r84.A01.addAll(r22.A01);
                    Iterator A0n = C12960it.A0n(r22.A02);
                    while (A0n.hasNext()) {
                        Map.Entry A15 = C12970iu.A15(A0n);
                        Object key = A15.getKey();
                        for (Object obj2 : (List) A15.getValue()) {
                            if (obj2 != null) {
                                if (key == null) {
                                    obj = "";
                                } else {
                                    obj = key;
                                }
                                Map map3 = r84.A02;
                                if (!map3.containsKey(obj)) {
                                    map3.put(obj, C12960it.A0l());
                                }
                                ((List) map3.get(obj)).add(obj2);
                            }
                        }
                    }
                    return;
                } else if (!(this instanceof C56212kW)) {
                    if (this instanceof C56192kU) {
                        map = ((C56192kU) r8).A00;
                        map2 = ((C56192kU) this).A00;
                    } else if (this instanceof C56182kT) {
                        map = ((C56182kT) r8).A00;
                        map2 = ((C56182kT) this).A00;
                    } else if (!(this instanceof C56242kZ)) {
                        ((C56252ka) this).A03((C56252ka) r8);
                        return;
                    } else {
                        C56242kZ r1 = (C56242kZ) this;
                        C56242kZ r85 = (C56242kZ) r8;
                        if (!TextUtils.isEmpty(r1.A00)) {
                            r85.A00 = r1.A00;
                        }
                        if (!TextUtils.isEmpty(r1.A01)) {
                            r85.A01 = r1.A01;
                        }
                        if (!TextUtils.isEmpty(r1.A02)) {
                            r85.A02 = r1.A02;
                        }
                        if (!TextUtils.isEmpty(r1.A03)) {
                            r85.A03 = r1.A03;
                        }
                        if (!TextUtils.isEmpty(r1.A04)) {
                            r85.A04 = r1.A04;
                        }
                        if (!TextUtils.isEmpty(r1.A05)) {
                            r85.A05 = r1.A05;
                        }
                        if (!TextUtils.isEmpty(r1.A06)) {
                            r85.A06 = r1.A06;
                        }
                        if (!TextUtils.isEmpty(r1.A07)) {
                            r85.A07 = r1.A07;
                        }
                        if (!TextUtils.isEmpty(r1.A08)) {
                            r85.A08 = r1.A08;
                        }
                        if (!TextUtils.isEmpty(r1.A09)) {
                            r85.A09 = r1.A09;
                            return;
                        }
                        return;
                    }
                    map.putAll(map2);
                    return;
                } else {
                    C56212kW r12 = (C56212kW) this;
                    C56212kW r86 = (C56212kW) r8;
                    int i2 = r12.A00;
                    if (i2 != 0) {
                        r86.A00 = i2;
                    }
                    int i3 = r12.A01;
                    if (i3 != 0) {
                        r86.A01 = i3;
                    }
                    if (!TextUtils.isEmpty(r12.A02)) {
                        r86.A02 = r12.A02;
                        return;
                    }
                    return;
                }
            }
        }
        charSequence = null;
        TextUtils.isEmpty(null);
        TextUtils.isEmpty(null);
        TextUtils.isEmpty(charSequence);
    }
}
