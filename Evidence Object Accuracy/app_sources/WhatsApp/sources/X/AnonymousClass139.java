package X;

import android.content.pm.PackageManager;
import com.whatsapp.util.Log;

/* renamed from: X.139  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass139 {
    public final AnonymousClass107 A00;
    public final C15440nG A01;
    public final AnonymousClass13B A02;
    public final C15520nO A03;

    public AnonymousClass139(AnonymousClass107 r1, C15440nG r2, AnonymousClass13B r3, C15520nO r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public void A00(String str) {
        C15450nH r0 = this.A01.A00;
        C15470nJ r2 = AbstractC15460nI.A18;
        if (r0.A05(r2)) {
            AnonymousClass107 r1 = this.A00;
            if (r1.A04.A00.A05(r2)) {
                AnonymousClass133 r12 = r1.A05;
                try {
                    if (r12.A02.A01(str).A03) {
                        AnonymousClass01H r02 = (AnonymousClass01H) r12.A00.A00.A00.get(str);
                        if (r02 != null) {
                            ((AbstractC35911iz) r02.get()).A00();
                        }
                        r12.A03.A00(str);
                    }
                } catch (PackageManager.NameNotFoundException unused) {
                }
                StringBuilder sb = new StringBuilder("InstrumentationChangeDispatcher/verification failed, dropping event for package - ");
                sb.append(str);
                Log.w(sb.toString());
            }
            this.A03.A03(str);
        }
    }
}
