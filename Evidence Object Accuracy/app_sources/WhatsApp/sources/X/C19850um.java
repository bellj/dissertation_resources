package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0um  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19850um {
    public final AnonymousClass1FC A00;
    public final Map A01 = new C246516i(5);
    public final Map A02 = new C246516i(100);
    public final Map A03 = new HashMap();

    public C19850um(AnonymousClass1FC r3) {
        this.A00 = r3;
    }

    public int A00(UserJid userJid) {
        int i;
        synchronized (this) {
            List<C44691zO> A08 = A08(userJid);
            i = 7;
            if (A08 != null) {
                for (C44691zO r0 : A08) {
                    i = (i * 31) + r0.hashCode();
                }
            }
        }
        return i;
    }

    public final C44661zL A01(UserJid userJid) {
        C44661zL r0;
        synchronized (this) {
            Map map = this.A01;
            r0 = (C44661zL) map.get(userJid);
            if (r0 == null) {
                r0 = new C44661zL();
                map.put(userJid, r0);
            }
        }
        return r0;
    }

    public C44711zQ A02(UserJid userJid) {
        synchronized (this) {
            C44661zL r0 = (C44661zL) this.A01.get(userJid);
            if (r0 == null) {
                return null;
            }
            return r0.A00;
        }
    }

    public C44711zQ A03(UserJid userJid, String str) {
        C44681zN r0;
        synchronized (this) {
            C44661zL r02 = (C44661zL) this.A01.get(userJid);
            if (r02 == null || (r0 = (C44681zN) r02.A04.get(str)) == null) {
                return null;
            }
            return r0.A00;
        }
    }

    public C44671zM A04(UserJid userJid, String str) {
        C44681zN r0;
        synchronized (this) {
            C44661zL r02 = (C44661zL) this.A01.get(userJid);
            if (r02 == null || (r0 = (C44681zN) r02.A04.get(str)) == null) {
                return null;
            }
            return r0.A01;
        }
    }

    public C44691zO A05(UserJid userJid, String str) {
        C44661zL r2;
        C44701zP r0;
        synchronized (this) {
            if (!TextUtils.isEmpty(str)) {
                Map map = this.A02;
                C44691zO r02 = (C44691zO) map.get(new C44701zP(userJid, str));
                if (r02 != null) {
                    return r02;
                }
                UserJid userJid2 = (UserJid) this.A03.get(str);
                if (userJid2 != null && ((userJid == null || userJid2.equals(userJid)) && (r2 = (C44661zL) this.A01.get(userJid2)) != null)) {
                    Iterator it = r2.A02.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            r5 = (C44691zO) it.next();
                            if (r5.A0D.equals(str)) {
                                r0 = new C44701zP(userJid2, str);
                                break;
                            }
                        } else {
                            for (C44681zN r03 : r2.A04.values()) {
                                for (C44691zO r5 : r03.A01.A04) {
                                    if (r5.A0D.equals(str)) {
                                        r0 = new C44701zP(userJid2, str);
                                    }
                                }
                            }
                        }
                    }
                    map.put(r0, r5);
                    return r5;
                }
            }
            return null;
        }
    }

    public C44741zT A06(UserJid userJid) {
        synchronized (this) {
            List<C44691zO> A08 = A08(userJid);
            if (A08 != null) {
                for (C44691zO r1 : A08) {
                    C44731zS r0 = r1.A01;
                    if (r0 != null && r0.A00 == 0 && !r1.A07) {
                        List list = r1.A06;
                        if (!list.isEmpty()) {
                            return (C44741zT) list.get(0);
                        }
                    }
                }
            }
            return null;
        }
    }

    public List A07(UserJid userJid) {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList();
            C44661zL r3 = (C44661zL) this.A01.get(userJid);
            if (r3 != null) {
                for (String str : r3.A03) {
                    C44681zN r0 = (C44681zN) r3.A04.get(str);
                    if (r0 != null) {
                        arrayList.add(r0.A01);
                    }
                }
            }
        }
        return arrayList;
    }

    public List A08(UserJid userJid) {
        List list;
        synchronized (this) {
            C44661zL r0 = (C44661zL) this.A01.get(userJid);
            if (r0 == null) {
                list = null;
            } else {
                list = Collections.unmodifiableList(r0.A02);
            }
        }
        return list;
    }

    public void A09() {
        synchronized (this) {
            this.A01.clear();
            this.A02.clear();
            this.A03.clear();
        }
    }

    public void A0A(C44651zK r10, UserJid userJid, boolean z) {
        synchronized (this) {
            C44661zL A01 = A01(userJid);
            if (!z) {
                A01.A03.clear();
            }
            for (C44671zM r7 : r10.A01) {
                C44681zN r6 = new C44681zN(r7);
                for (C44691zO r3 : r7.A04) {
                    Map map = this.A02;
                    String str = r3.A0D;
                    map.put(new C44701zP(userJid, str), r3);
                    this.A03.put(str, userJid);
                }
                List list = A01.A03;
                String str2 = r7.A03;
                list.add(str2);
                A01.A04.put(str2, r6);
            }
            A01.A00 = r10.A00;
        }
    }

    public void A0B(C44721zR r7, UserJid userJid, boolean z) {
        synchronized (this) {
            C44661zL A01 = A01(userJid);
            if (!z) {
                ArrayList arrayList = A01.A02;
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    this.A03.remove(((C44691zO) it.next()).A0D);
                }
                arrayList.clear();
            }
            for (C44691zO r3 : r7.A01) {
                A01.A02.add(r3);
                Map map = this.A02;
                String str = r3.A0D;
                map.put(new C44701zP(userJid, str), r3);
                this.A03.put(str, userJid);
            }
            A01.A01 = r7.A00;
            this.A00.A00().A00(userJid);
        }
    }

    public void A0C(C44691zO r7, UserJid userJid) {
        synchronized (this) {
            Map map = this.A02;
            String str = r7.A0D;
            map.put(new C44701zP(userJid, str), r7);
            if (!(userJid == null && (userJid = (UserJid) this.A03.get(str)) == null)) {
                C44661zL A01 = A01(userJid);
                Iterator it = A01.A04.values().iterator();
                while (true) {
                    int i = 0;
                    if (!it.hasNext()) {
                        break;
                    }
                    List list = ((C44681zN) it.next()).A01.A04;
                    while (true) {
                        if (i >= list.size()) {
                            break;
                        } else if (str.equals(((C44691zO) list.get(i)).A0D)) {
                            list.set(i, r7);
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                int i2 = 0;
                while (true) {
                    ArrayList arrayList = A01.A02;
                    if (i2 >= arrayList.size()) {
                        arrayList.add(0, r7);
                        this.A03.put(str, userJid);
                        break;
                    } else if (str.equals(((C44691zO) arrayList.get(i2)).A0D)) {
                        arrayList.set(i2, r7);
                        break;
                    } else {
                        i2++;
                    }
                }
                this.A00.A00().A00(userJid);
            }
        }
    }

    public void A0D(UserJid userJid) {
        synchronized (this) {
            Map map = this.A01;
            C44661zL r4 = (C44661zL) map.get(userJid);
            if (r4 != null) {
                Iterator it = r4.A02.iterator();
                while (it.hasNext()) {
                    Map map2 = this.A03;
                    String str = ((C44691zO) it.next()).A0D;
                    map2.remove(str);
                    this.A02.remove(new C44701zP(userJid, str));
                }
                for (C44681zN r0 : r4.A04.values()) {
                    for (C44691zO r1 : r0.A01.A04) {
                        Map map3 = this.A03;
                        String str2 = r1.A0D;
                        map3.remove(str2);
                        this.A02.remove(new C44701zP(userJid, str2));
                    }
                }
            }
            map.remove(userJid);
            this.A00.A00().A00(userJid);
        }
    }

    public void A0E(UserJid userJid, int i) {
        if (i < 0) {
            StringBuilder sb = new StringBuilder("CatalogCacheManager/trimProductsInCatalogCache/Invalid size argument - ");
            sb.append(i);
            Log.e(sb.toString());
            return;
        }
        synchronized (this) {
            C44661zL r2 = (C44661zL) this.A01.get(userJid);
            if (r2 != null) {
                r2.A01 = new C44711zQ(null, true);
                ArrayList arrayList = r2.A02;
                int size = arrayList.size();
                for (int i2 = 0; i2 < size - i; i2++) {
                    int size2 = arrayList.size() - 1;
                    String str = ((C44691zO) arrayList.get(size2)).A0D;
                    this.A03.remove(str);
                    this.A02.remove(new C44701zP(userJid, str));
                    arrayList.remove(size2);
                }
            }
        }
    }

    public void A0F(UserJid userJid, boolean z) {
        synchronized (this) {
            C44661zL r3 = (C44661zL) this.A01.get(userJid);
            if (r3 != null) {
                r3.A03.clear();
                r3.A04.clear();
                if (z) {
                    r3.A00 = new C44711zQ(null, true);
                }
            }
        }
    }

    public void A0G(String str) {
        C44661zL r4;
        synchronized (this) {
            Map map = this.A03;
            UserJid userJid = (UserJid) map.get(str);
            this.A02.remove(new C44701zP(userJid, str));
            map.remove(str);
            if (!(userJid == null || (r4 = (C44661zL) this.A01.get(userJid)) == null)) {
                int i = 0;
                while (true) {
                    ArrayList arrayList = r4.A02;
                    if (i >= arrayList.size()) {
                        break;
                    } else if (str.equals(((C44691zO) arrayList.get(i)).A0D)) {
                        arrayList.remove(i);
                        break;
                    } else {
                        i++;
                    }
                }
                for (C44681zN r3 : r4.A04.values()) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= r3.A01.A04.size()) {
                            break;
                        } else if (str.equals(((C44691zO) r3.A01.A04.get(i2)).A0D)) {
                            r3.A01.A04.remove(i2);
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
                this.A00.A00().A00(userJid);
            }
        }
    }

    public boolean A0H(UserJid userJid) {
        boolean z;
        synchronized (this) {
            z = false;
            if (this.A01.get(userJid) != null) {
                z = true;
            }
        }
        return z;
    }

    public boolean A0I(UserJid userJid) {
        boolean z;
        synchronized (this) {
            C44661zL r0 = (C44661zL) this.A01.get(userJid);
            z = false;
            if (r0 != null && !r0.A03.isEmpty()) {
                z = true;
            }
        }
        return z;
    }

    public boolean A0J(UserJid userJid) {
        boolean z;
        synchronized (this) {
            C44661zL r0 = (C44661zL) this.A01.get(userJid);
            z = false;
            if (r0 != null && !r0.A02.isEmpty()) {
                z = true;
            }
        }
        return z;
    }
}
