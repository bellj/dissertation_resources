package X;

/* renamed from: X.581  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass581 implements AnonymousClass5XP {
    public final /* synthetic */ C26021Bs A00;

    public /* synthetic */ AnonymousClass581(C26021Bs r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XP
    public void ANg() {
        C26021Bs.A00(this.A00, 2);
    }

    @Override // X.AnonymousClass5XP
    public void ANh() {
        C26021Bs.A00(this.A00, 7);
    }

    @Override // X.AnonymousClass5XP
    public void AOQ(boolean z) {
        if (z) {
            C26021Bs.A00(this.A00, 5);
        }
    }

    @Override // X.AnonymousClass5XP
    public void APl(int i) {
        C26021Bs.A00(this.A00, AnonymousClass4EQ.A00(i));
    }

    @Override // X.AnonymousClass5XP
    public void ARJ() {
        C26021Bs.A00(this.A00, 3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000b, code lost:
        if (r4 == 101) goto L_0x000d;
     */
    @Override // X.AnonymousClass5XP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AU8(int r4) {
        /*
            r3 = this;
            r0 = 301(0x12d, float:4.22E-43)
            if (r4 == r0) goto L_0x000d
            r0 = 104(0x68, float:1.46E-43)
            if (r4 == r0) goto L_0x000d
            r0 = 101(0x65, float:1.42E-43)
            r2 = 0
            if (r4 != r0) goto L_0x000e
        L_0x000d:
            r2 = 1
        L_0x000e:
            X.1Bs r1 = r3.A00
            r0 = 3
            if (r2 == 0) goto L_0x0015
            r0 = 18
        L_0x0015:
            X.C26021Bs.A00(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass581.AU8(int):void");
    }

    @Override // X.AnonymousClass5XP
    public void AU9() {
        C26021Bs.A00(this.A00, 17);
    }

    @Override // X.AnonymousClass5XP
    public void AUL(int i) {
        C26021Bs r1 = this.A00;
        C26021Bs.A00(r1, 3);
        synchronized (r1) {
            r1.A00 = i;
        }
    }
}
