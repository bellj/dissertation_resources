package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68283Uu implements AbstractC13960kc {
    public final /* synthetic */ int A00 = 11;
    public final /* synthetic */ int A01;
    public final /* synthetic */ Context A02;
    public final /* synthetic */ C14580lf A03;
    public final /* synthetic */ C25831Az A04;
    public final /* synthetic */ UserJid A05;

    public C68283Uu(Context context, C14580lf r3, C25831Az r4, UserJid userJid, int i) {
        this.A04 = r4;
        this.A05 = userJid;
        this.A02 = context;
        this.A01 = i;
        this.A03 = r3;
    }

    @Override // X.AbstractC13960kc
    public void AQH(UserJid userJid, int i) {
        if (C29941Vi.A00(this.A05, userJid)) {
            C25831Az r2 = this.A04;
            C14900mE.A00(r2.A01, this, 22);
            this.A03.A02(Boolean.FALSE);
            r2.A04.A06("catalog_collections_view_tag", false);
        }
    }

    @Override // X.AbstractC13960kc
    public void AQI(UserJid userJid, boolean z, boolean z2) {
        UserJid userJid2 = this.A05;
        if (C29941Vi.A00(userJid2, userJid)) {
            C25831Az r4 = this.A04;
            C14900mE.A00(r4.A01, this, 21);
            Context context = this.A02;
            Intent A0M = C14960mK.A0M(context, userJid2, Integer.valueOf(this.A01), this.A00);
            A0M.putExtra("is_prefetched_catalog", true);
            r4.A00.A06(context, A0M);
            this.A03.A02(Boolean.TRUE);
        }
    }
}
