package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.0Dw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC02760Dw extends AnonymousClass5ZU {
    public static final C112755Ep A00 = C112755Ep.A00;

    AbstractC02790Dz A6E(AbstractC02780Dy v);

    void A74(CancellationException cancellationException);

    CancellationException ABF();

    AnonymousClass5VG AJB(AnonymousClass1J7 v, boolean z, boolean z2);

    boolean AJD();

    boolean Ae7();
}
