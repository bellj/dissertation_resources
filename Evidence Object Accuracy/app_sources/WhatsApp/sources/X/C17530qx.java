package X;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/* renamed from: X.0qx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17530qx extends C17540qy {
    public static final Object A00(Map map, Object obj) {
        C16700pc.A0E(map, 0);
        Object obj2 = map.get(obj);
        if (obj2 != null || map.containsKey(obj)) {
            return obj2;
        }
        StringBuilder sb = new StringBuilder("Key ");
        sb.append(obj);
        sb.append(" is missing in the map.");
        throw new NoSuchElementException(sb.toString());
    }

    public static final Map A01() {
        return C71373cp.A00;
    }

    public static final Map A02(C17520qw... r5) {
        int length = r5.length;
        if (length <= 0) {
            return C71373cp.A00;
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(C17540qy.A03(length));
        int i = 0;
        do {
            C17520qw r0 = r5[i];
            i++;
            linkedHashMap.put(r0.first, r0.second);
        } while (i < length);
        return linkedHashMap;
    }
}
