package X;

import java.util.Iterator;

/* renamed from: X.5DN  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DN implements Iterator, AbstractC16910px {
    public final Iterator A00;
    public final /* synthetic */ C112835Ex A01;

    public AnonymousClass5DN(C112835Ex r2) {
        this.A01 = r2;
        this.A00 = r2.A01.iterator();
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A00.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        return this.A01.A00.AJ4(this.A00.next());
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }
}
