package X;

import com.whatsapp.HomeActivity;
import com.whatsapp.R;

/* renamed from: X.2LD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2LD implements AbstractC32851cq {
    public final /* synthetic */ HomeActivity A00;

    public AnonymousClass2LD(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        HomeActivity homeActivity = this.A00;
        boolean A00 = C14950mJ.A00();
        int i = R.string.record_need_sd_card_title_shared_storage;
        if (A00) {
            i = R.string.record_need_sd_card_title;
        }
        boolean A002 = C14950mJ.A00();
        int i2 = R.string.record_need_sd_card_message_shared_storage;
        if (A002) {
            i2 = R.string.record_need_sd_card_message;
        }
        homeActivity.Adr(new Object[0], i, i2);
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        this.A00.Adr(new Object[0], R.string.alert, R.string.permission_storage_need_access);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        HomeActivity homeActivity = this.A00;
        boolean A00 = C14950mJ.A00();
        int i = R.string.record_need_sd_card_title_shared_storage;
        if (A00) {
            i = R.string.record_need_sd_card_title;
        }
        boolean A002 = C14950mJ.A00();
        int i2 = R.string.record_need_sd_card_message_shared_storage;
        if (A002) {
            i2 = R.string.record_need_sd_card_message;
        }
        homeActivity.Adr(new Object[0], i, i2);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        this.A00.Adr(new Object[0], R.string.alert, R.string.permission_storage_need_access);
    }
}
