package X;

/* renamed from: X.1op  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38861op {
    public final AnonymousClass1KS A00;
    public final String A01;
    public final String A02;

    public C38861op(AnonymousClass1KS r1, String str, String str2) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = r1;
    }

    public void A00(AnonymousClass1KS r3) {
        AnonymousClass1KS r1 = this.A00;
        r1.A0F = r3.A0F;
        r1.A07 = r3.A07;
        r1.A05 = r3.A05;
        r1.A0B = r3.A0B;
        r1.A0A = r3.A0A;
        r1.A00 = r3.A00;
        r1.A03 = r3.A03;
        r1.A02 = r3.A02;
        r1.A09 = r3.A09;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("RecentStickerIdentifier{fileHash='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append(", imageHash='");
        sb.append(this.A02);
        sb.append('\'');
        sb.append(", sticker=");
        sb.append(this.A00.toString());
        sb.append('}');
        return sb.toString();
    }
}
