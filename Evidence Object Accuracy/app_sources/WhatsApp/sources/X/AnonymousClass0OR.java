package X;

import android.os.Handler;

/* renamed from: X.0OR  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OR {
    public RunnableC09860dd A00;
    public final Handler A01 = new Handler();
    public final C009804x A02;

    public AnonymousClass0OR(AbstractC001200n r2) {
        this.A02 = new C009804x(r2);
    }

    public final void A00(AnonymousClass074 r3) {
        RunnableC09860dd r0 = this.A00;
        if (r0 != null) {
            r0.run();
        }
        RunnableC09860dd r1 = new RunnableC09860dd(r3, this.A02);
        this.A00 = r1;
        this.A01.postAtFrontOfQueue(r1);
    }
}
