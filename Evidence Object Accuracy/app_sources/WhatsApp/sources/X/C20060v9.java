package X;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;

/* renamed from: X.0v9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20060v9 {
    public final C15570nT A00;
    public final C16510p9 A01;
    public final C16490p7 A02;
    public final C21390xL A03;

    public C20060v9(C15570nT r1, C16510p9 r2, C16490p7 r3, C21390xL r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x012a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1XP r15) {
        /*
        // Method dump skipped, instructions count: 343
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20060v9.A00(X.1XP):void");
    }

    public void A01(AnonymousClass1XP r11, long j) {
        boolean z = false;
        if (r11.A08() == 2) {
            z = true;
        }
        StringBuilder sb = new StringBuilder("LocationMessageStore/insertOrUpdateQuotedLocationMessage/message in main storage; key=");
        sb.append(r11.A0z);
        AnonymousClass009.A0B(sb.toString(), z);
        C16310on A02 = this.A02.A02();
        try {
            ContentValues contentValues = new ContentValues();
            try {
                contentValues.put("message_row_id", Long.valueOf(j));
                contentValues.put("latitude", Double.valueOf(r11.A00));
                contentValues.put("longitude", Double.valueOf(r11.A01));
                if (r11 instanceof AnonymousClass1XO) {
                    AnonymousClass1XO r1 = (AnonymousClass1XO) r11;
                    C30021Vq.A04(contentValues, "place_name", r1.A01);
                    C30021Vq.A04(contentValues, "place_address", r1.A00);
                    C30021Vq.A04(contentValues, "url", r1.A02);
                } else if (r11 instanceof C30341Xa) {
                    contentValues.putNull("place_name");
                    contentValues.putNull("place_address");
                    contentValues.putNull("url");
                }
                C30021Vq.A06(contentValues, "thumbnail", r11.A0G().A07());
                boolean z2 = false;
                if (A02.A03.A03(contentValues, "message_quoted_location") == j) {
                    z2 = true;
                }
                AnonymousClass009.A0C("LocationMessageStore/insertOrUpdateQuotedLocationMessage/inserted row should have same row_id", z2);
            } catch (SQLiteConstraintException e) {
                contentValues.remove("message_row_id");
                if (A02.A03.A00("message_quoted_location", contentValues, "message_row_id = ?", new String[]{String.valueOf(j)}) != 1) {
                    throw e;
                }
            }
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A02() {
        return this.A01.A0G() && this.A03.A01("location_ready", 0) == 2;
    }
}
