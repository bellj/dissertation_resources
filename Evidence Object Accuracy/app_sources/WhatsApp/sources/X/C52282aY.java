package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.ReplacementSpan;

/* renamed from: X.2aY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52282aY extends ReplacementSpan {
    public int A00;
    public int A01;
    public int A02;
    public final C49742Mi A03;
    public final String A04;
    public final String A05;

    public C52282aY(String str, String str2, int i) {
        this.A03 = new C49742Mi(i);
        this.A05 = str;
        this.A04 = str2;
    }

    @Override // android.text.style.ReplacementSpan
    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        C49742Mi r3 = this.A03;
        int i6 = this.A02;
        r3.setBounds((int) f, i3 + i6, (int) (((float) this.A01) + f), i5 - i6);
        r3.draw(canvas);
        canvas.drawText(this.A05, f + ((float) this.A00), (float) i4, paint);
    }

    @Override // android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        if (fontMetricsInt != null) {
            Paint.FontMetricsInt A00 = AnonymousClass3I9.A00(paint);
            int i3 = A00.descent - A00.ascent;
            this.A00 = i3 >> 1;
            int max = Math.max(1, i3 >> 4);
            this.A02 = max;
            int i4 = A00.top - max;
            fontMetricsInt.top = i4;
            int i5 = A00.bottom + max;
            fontMetricsInt.bottom = i5;
            fontMetricsInt.ascent = i4;
            fontMetricsInt.descent = i5;
        }
        String str = this.A05;
        int ceil = (int) Math.ceil((double) (paint.measureText(str, 0, str.length()) + ((float) (this.A00 << 1))));
        this.A01 = ceil;
        return ceil;
    }
}
