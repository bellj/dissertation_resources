package X;

import android.animation.Animator;
import android.view.ViewTreeObserver;

/* renamed from: X.0Wc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC06950Wc implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass0BZ A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC06950Wc(AnonymousClass0BZ r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass0BZ r3 = this.A00;
        if (r3.A0B) {
            int i = r3.A02;
            Animator.AnimatorListener animatorListener = r3.A0D;
            r3.clearAnimation();
            r3.setScaleX(1.5f);
            r3.setScaleY(1.5f);
            r3.animate().setDuration((long) i).setInterpolator(r3.A04).alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setListener(animatorListener);
        } else {
            r3.setTranslationY((float) r3.getHeight());
            r3.A02(r3.A0D, r3.A02);
        }
        r3.A00();
        r3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
