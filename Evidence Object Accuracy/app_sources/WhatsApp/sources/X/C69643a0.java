package X;

import android.view.View;

/* renamed from: X.3a0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69643a0 implements AbstractC116185Ul {
    public View A00;
    public final AnonymousClass1V2 A01;

    public C69643a0(AnonymousClass1V2 r1) {
        this.A01 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02fb, code lost:
        if (X.C29941Vi.A00(r10.A06, r12) != false) goto L_0x02fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x02a5, code lost:
        if (r10 == 37) goto L_0x02a7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x038a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0182  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0193  */
    @Override // X.AbstractC116185Ul
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View AHa(android.content.Context r27, android.view.View r28, android.view.ViewGroup r29, X.AnonymousClass1J1 r30, java.util.List r31, java.util.List r32, java.util.List r33, java.util.List r34, boolean r35) {
        /*
        // Method dump skipped, instructions count: 968
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C69643a0.AHa(android.content.Context, android.view.View, android.view.ViewGroup, X.1J1, java.util.List, java.util.List, java.util.List, java.util.List, boolean):android.view.View");
    }
}
