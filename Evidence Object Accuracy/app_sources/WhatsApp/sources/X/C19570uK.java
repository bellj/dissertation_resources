package X;

import android.net.Uri;

/* renamed from: X.0uK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19570uK implements AbstractC19580uL {
    public final C19560uJ A00;

    public C19570uK(C19560uJ r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC19580uL
    public boolean A5Z(String str) {
        return this.A00.A01.get(str) != null;
    }

    @Override // X.AbstractC19580uL
    public void AHw(String str, String str2) {
        AnonymousClass3FE r4 = (AnonymousClass3FE) this.A00.A01.get(str);
        Uri parse = Uri.parse(str2);
        C16700pc.A0B(parse);
        Object A00 = C63153Am.A00(parse);
        if (A00 == null) {
            A00 = C71373cp.A00;
        }
        if (r4 != null) {
            r4.A02("on_success", C17530qx.A02(new C17520qw("deeplink", str2), new C17520qw("parameters", A00)));
        }
    }
}
