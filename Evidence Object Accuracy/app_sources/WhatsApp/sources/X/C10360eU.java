package X;

import java.util.Comparator;

/* renamed from: X.0eU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10360eU implements Comparator {
    public final /* synthetic */ C04840Ng A00;

    public C10360eU(C04840Ng r1) {
        this.A00 = r1;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        float floatValue = ((Number) ((AnonymousClass01T) obj).A01).floatValue();
        float floatValue2 = ((Number) ((AnonymousClass01T) obj2).A01).floatValue();
        if (floatValue2 <= floatValue) {
            return floatValue > floatValue2 ? -1 : 0;
        }
        return 1;
    }
}
