package X;

import java.security.cert.X509Certificate;

/* renamed from: X.5uH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127305uH {
    public X509Certificate A00 = C20080vB.A00("MIICUDCCAfegAwIBAgIUHkgEbXTAygu7dguuo6MjyvPo5NIwCgYIKoZIzj0EAwIw\nfjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExHjAcBgNVBAoMFUZh\nY2Vib29rIFBheW1lbnRzIEluYzE6MDgGA1UEAwwxRmFjZWJvb2sgUGF5bWVudHMg\nU2VjdXJlIFRpZXIgTWFzdGVyIFNpZ25pbmcgQ2VydDAeFw0yMDA2MDUyMTM0NTda\nFw0yMTA2MDUyMTM0NTdaMH4xCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9y\nbmlhMR4wHAYDVQQKDBVGYWNlYm9vayBQYXltZW50cyBJbmMxOjA4BgNVBAMMMUZh\nY2Vib29rIFBheW1lbnRzIFNlY3VyZSBUaWVyIE1hc3RlciBTaWduaW5nIENlcnQw\nWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARnZ9FlWc2rSXyZyUtL8QwJydBYY2MS\n66NHUT47JHzSgcBp0eG9D2M0uVX8PDPTzyz2fTuECD8v4f2gT+TtYaeqo1MwUTAd\nBgNVHQ4EFgQUODetYD68XqFs905klyRBeS2NyYcwHwYDVR0jBBgwFoAUODetYD68\nXqFs905klyRBeS2NyYcwDwYDVR0TAQH/BAUwAwEB/zAKBggqhkjOPQQDAgNHADBE\nAiBG/FnYWDJstMBNPzuY00DHyLQNwcsVDGDTpukjQPUiZAIgDK+ALUqfDCP/EOaP\nQF2tMIxySbNPehxhprHHZtNf1zg=", false);
    public final C20080vB A01;
    public final C18600si A02;

    public C127305uH(C20080vB r3, C18600si r4) {
        this.A02 = r4;
        this.A01 = r3;
    }
}
