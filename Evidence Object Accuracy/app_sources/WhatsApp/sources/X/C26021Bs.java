package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.1Bs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26021Bs {
    public static final List A0B;
    public int A00 = 0;
    public int A01 = 0;
    public List A02;
    public final C26091Bz A03;
    public final AnonymousClass5XP A04;
    public final C26031Bt A05;
    public final C89614Kq A06;
    public final C89624Kr A07;
    public final AnonymousClass1C6 A08;
    public final List A09 = Collections.unmodifiableList(new ArrayList(A0B));
    public final AtomicInteger A0A = new AtomicInteger(Integer.MIN_VALUE);

    static {
        ArrayList arrayList = new ArrayList();
        Double valueOf = Double.valueOf(0.5d);
        arrayList.add(valueOf);
        arrayList.add(valueOf);
        A0B = Collections.unmodifiableList(arrayList);
    }

    public C26021Bs(C26091Bz r3, C26031Bt r4, AnonymousClass1C6 r5) {
        this.A05 = r4;
        this.A03 = r3;
        this.A08 = r5;
        this.A06 = new C89614Kq(this);
        this.A07 = new C89624Kr(this);
        this.A04 = new AnonymousClass581(this);
    }

    public static /* synthetic */ void A00(C26021Bs r2, int i) {
        synchronized (r2) {
            StringBuilder sb = new StringBuilder();
            sb.append("GoogleMigrate/setCurrentScreen = ");
            sb.append(i);
            Log.i(sb.toString());
            r2.A01 = i;
        }
    }

    public void A01(int i, int i2, int i3) {
        double doubleValue;
        double d = (((double) i2) * 1.0d) / ((double) i3);
        synchronized (this) {
            if (this.A02 == null) {
                List list = this.A09;
                ArrayList arrayList = new ArrayList();
                double d2 = 0.0d;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    arrayList.add(Double.valueOf(d2));
                    d2 += ((Double) list.get(i4)).doubleValue();
                }
                arrayList.add(Double.valueOf(d2));
                this.A02 = Collections.unmodifiableList(arrayList);
            }
        }
        List list2 = this.A09;
        if (i >= list2.size()) {
            doubleValue = 1.0d;
        } else {
            double min = Math.min(Math.max(0.0d, d), 1.0d);
            doubleValue = (((Number) this.A02.get(i)).doubleValue() + (((Number) list2.get(i)).doubleValue() * min)) / ((Number) this.A02.get(list2.size())).doubleValue();
        }
        int i5 = (int) (doubleValue * 100.0d);
        if (i5 != this.A0A.getAndSet(i5)) {
            for (AnonymousClass5XP r0 : this.A05.A01()) {
                r0.AUL(i5);
            }
        }
    }
}
