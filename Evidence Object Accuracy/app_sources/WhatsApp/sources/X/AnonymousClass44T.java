package X;

import com.whatsapp.blocklist.BlockList;
import java.util.Set;

/* renamed from: X.44T  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44T extends AbstractC33331dp {
    public final /* synthetic */ BlockList A00;

    public AnonymousClass44T(BlockList blockList) {
        this.A00 = blockList;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        BlockList.A02(this.A00);
    }
}
