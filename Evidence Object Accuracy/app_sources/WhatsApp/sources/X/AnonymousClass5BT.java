package X;

import java.io.Serializable;

/* renamed from: X.5BT  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BT implements Serializable {
    public static final long serialVersionUID = 0;
    public final AnonymousClass5X4[] elements;

    public AnonymousClass5BT(AnonymousClass5X4[] r1) {
        this.elements = r1;
    }

    private final Object readResolve() {
        AnonymousClass5X4[] r4 = this.elements;
        AnonymousClass5X4 r3 = C112775Er.A00;
        int length = r4.length;
        int i = 0;
        while (i < length) {
            AnonymousClass5X4 r0 = r4[i];
            i++;
            r3 = r3.plus(r0);
        }
        return r3;
    }
}
