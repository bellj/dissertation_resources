package X;

import android.content.SharedPreferences;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.5yq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130125yq {
    public final C129655y5 A00;
    public final C1309560q A01 = new C1309560q();
    public final C1308160b A02;
    public final C125075qe A03;

    public C130125yq(C129655y5 r2, C1308160b r3, C125075qe r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r2;
    }

    public static String A00(C130125yq r5, String str, byte[] bArr) {
        C127675us A01 = r5.A00.A01("AUTH");
        if (A01 == null) {
            return null;
        }
        return Base64.encodeToString(r5.A01.A02(A01, str.getBytes(), AnonymousClass61L.A03(bArr), true), 2);
    }

    public C127385uP A01() {
        short s;
        C1308160b r4 = this.A02;
        r4.A05();
        C127385uP r0 = r4.A02;
        if (r0 != null) {
            return r0;
        }
        C29361Rw A00 = C29361Rw.A00();
        synchronized (r4) {
            r4.A05();
            int A01 = C12970iu.A01(r4.A00(), "last-decryption-key-index") % 240;
            char[] cArr = C1308160b.A0D;
            s = (short) (((cArr[(byte) ((A01 >> 4) & 15)] << '\b') | cArr[(byte) (A01 & 15)]) & 65535);
            r4.A05();
            C129815yL r5 = r4.A03;
            AnonymousClass009.A05(r5);
            int i = 65535 & s;
            r5.A02(C12960it.A0f(C12960it.A0k("decryption."), i), C12960it.A0W(i, "alias-decryption-key."));
            SharedPreferences.Editor edit = r4.A00().edit();
            edit.putInt("last-decryption-key-index", A01 + 1);
            edit.apply();
        }
        C127385uP r9 = new C127385uP(A00, new Date().getTime(), s);
        r4.A02 = r9;
        r4.A05();
        int i2 = r9.A02 & 65535;
        byte[] bArr = new byte[8];
        C16050oM.A02(bArr, 0, r9.A00);
        byte[] A05 = C16050oM.A05(r9.A01.A02(), new byte[]{(byte) (i2 >> 8), (byte) i2}, bArr);
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance2.add(1, 1);
        C129815yL r8 = r4.A03;
        AnonymousClass009.A05(r8);
        r8.A01(r4.A09.A00, C12960it.A0W(i2, "decryption."), C12960it.A0W(i2, "alias-decryption-key."), instance.getTime(), instance2.getTime(), A05);
        if (A05 != null) {
            Arrays.fill(A05, (byte) 0);
        }
        return r4.A02;
    }

    public KeyPair A02() {
        C1308160b r1 = this.A02;
        KeyPair keyPair = r1.A06;
        if (keyPair == null) {
            try {
                keyPair = C117305Zk.A0o();
            } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException unused) {
                Log.e("PAY: NoviKeyStore/createInMemorySigningKeyPair can't create signing key");
                keyPair = null;
            }
            r1.A06 = keyPair;
        }
        return keyPair;
    }

    public KeyPair A03() {
        C1308160b r3 = this.A02;
        r3.A05();
        Map map = r3.A07;
        if (map.get("alias-signing-key.trusted-app-install") != null) {
            return (KeyPair) map.get("alias-signing-key.trusted-app-install");
        }
        return r3.A02("alias-signing-key.trusted-app-install");
    }

    public byte[] A04(byte[] bArr) {
        C127385uP r2;
        byte[] A00;
        String str;
        C1308160b r4 = this.A02;
        int length = bArr.length;
        byte[] bArr2 = null;
        if (length < 52) {
            str = "payload not valid";
        } else if (C1309560q.A00(bArr, 0, 1)[0] != 4) {
            str = "version not supported";
        } else {
            short s = (short) ((bArr[2] & 255) | ((bArr[1] & 255) << 8));
            r4.A05();
            HashMap hashMap = r4.A0C;
            Short valueOf = Short.valueOf(s);
            if (hashMap.containsKey(valueOf)) {
                r2 = (C127385uP) hashMap.get(valueOf);
            } else {
                C129815yL r22 = r4.A03;
                AnonymousClass009.A05(r22);
                int i = s & 65535;
                byte[] A03 = r22.A03(C12960it.A0f(C12960it.A0k("decryption."), i), C12960it.A0W(i, "alias-decryption-key."));
                if (A03 == null) {
                    r2 = null;
                } else {
                    try {
                        byte[][] A07 = C16050oM.A07(A03, 64, 2, 8);
                        C29361Rw A01 = C29361Rw.A01(A07[0]);
                        byte[] bArr3 = A07[1];
                        short s2 = (short) ((bArr3[1] & 255) | ((bArr3[0] & 255) << 8));
                        byte[] bArr4 = A07[2];
                        r2 = new C127385uP(A01, (((long) bArr4[7]) & 255) | ((((long) bArr4[0]) & 255) << 56) | ((((long) bArr4[1]) & 255) << 48) | ((((long) bArr4[2]) & 255) << 40) | ((((long) bArr4[3]) & 255) << 32) | ((((long) bArr4[4]) & 255) << 24) | ((((long) bArr4[5]) & 255) << 16) | ((((long) bArr4[6]) & 255) << 8), s2);
                    } catch (ParseException unused) {
                        Log.w("can't deserialize key");
                        r2 = null;
                    }
                    Arrays.fill(A03, (byte) 0);
                }
            }
            if (r2 != null) {
                C29361Rw r10 = r2.A01;
                byte b = bArr[3];
                AnonymousClass2ST r1 = new AnonymousClass2ST(C1309560q.A00(bArr, 4, 36));
                boolean A1V = C12960it.A1V(b & 1, 1);
                if ((b & 2) == 2) {
                    bArr2 = C1309560q.A00(bArr, 36, 68);
                    A00 = C1309560q.A00(bArr, 68, length);
                } else {
                    A00 = C1309560q.A00(bArr, 36, length);
                }
                C29371Rx r0 = r10.A01;
                byte[][] A06 = C16050oM.A06(C32891cu.A01(AnonymousClass4F7.A00(r0, r1), r1.A01, "walibra_hkdf_info".getBytes(), 44), 32, 12);
                bArr2 = C1309560q.A01(A06[0], A00, A06[1], bArr2, false);
                if (A1V && bArr2 != null) {
                    int length2 = bArr2.length;
                    int i2 = length2 - (bArr2[length2 - 1] & 255);
                    byte[] bArr5 = new byte[i2];
                    System.arraycopy(bArr2, 0, bArr5, 0, i2);
                    return bArr5;
                }
            }
            return bArr2;
        }
        Log.e(str);
        return bArr2;
    }

    public byte[] A05(byte[] bArr, byte[] bArr2) {
        C127675us A01 = this.A00.A01("WALLET_CORE");
        if (A01 == null) {
            return null;
        }
        return this.A01.A02(A01, bArr, bArr2, false);
    }
}
