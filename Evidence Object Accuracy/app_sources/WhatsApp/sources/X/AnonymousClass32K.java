package X;

import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.jid.UserJid;

/* renamed from: X.32K  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass32K extends AnonymousClass44L implements AnonymousClass1W3 {
    public final int A00;
    public final C19810ui A01;
    public final C17560r0 A02;
    public final AnonymousClass4RQ A03;
    public final C17570r1 A04;
    public final AbstractC14440lR A05;

    public AnonymousClass32K(C19810ui r1, C17560r0 r2, AnonymousClass4RQ r3, C17570r1 r4, AbstractC14440lR r5, int i) {
        super(r2);
        this.A02 = r2;
        this.A05 = r5;
        this.A03 = r3;
        this.A01 = r1;
        this.A04 = r4;
        this.A00 = i;
    }

    public static void A00(AnonymousClass32K r2, int i) {
        r2.A05.Ab2(new RunnableBRunnable0Shape14S0100000_I1(r2, i));
    }

    public static void A01(AnonymousClass32K r2, UserJid userJid) {
        C19810ui r1 = r2.A01;
        if (r1.A00.A08()) {
            r1.A01(r2, userJid);
        } else {
            r2.A02();
        }
    }

    public void A02() {
        AnonymousClass1Q5 A00;
        if (this instanceof C59252uF) {
            A00(this, 30);
        } else if (this instanceof C59272uH) {
            C59272uH r2 = (C59272uH) this;
            AnonymousClass2EK.A00(r2.A02, 0);
            r2.A09.A03("plm_details_view_tag");
            A00(r2, 29);
        } else if (this instanceof C59262uG) {
            C59262uG r22 = (C59262uG) this;
            r22.A0A.A03("view_product_tag");
            A00(r22, 28);
        } else if (this instanceof C59282uI) {
            C59282uI r23 = (C59282uI) this;
            if (r23.A05.A06 == null && (A00 = C19840ul.A00(r23.A09)) != null) {
                A00.A08("datasource_catalog");
            }
            A00(r23, 27);
        } else if (!(this instanceof C59242uE)) {
            A00(this, 25);
        } else {
            C59242uE r24 = (C59242uE) this;
            AnonymousClass4Tm r1 = r24.A02;
            if (r1.A06 == null) {
                int i = r1.A02;
                C19840ul r12 = r24.A05;
                if (i == 0) {
                    r12.A03("collection_management_view_tag");
                } else {
                    AnonymousClass1Q5 A002 = C19840ul.A00(r12);
                    if (A002 != null) {
                        A002.A08("datasource_collections");
                    }
                }
            }
            A00(r24, 26);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002c, code lost:
        if (r16 != 2498054) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(com.whatsapp.jid.UserJid r15, int r16, boolean r17) {
        /*
        // Method dump skipped, instructions count: 311
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass32K.A03(com.whatsapp.jid.UserJid, int, boolean):boolean");
    }
}
