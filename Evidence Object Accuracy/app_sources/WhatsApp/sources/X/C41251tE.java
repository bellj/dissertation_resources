package X;

import android.util.Pair;
import java.util.Arrays;
import java.util.Collections;

/* renamed from: X.1tE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41251tE {
    public static final C28601Of A00;
    public static final C28601Of A01;

    static {
        AnonymousClass1YB r7 = new AnonymousClass1YB();
        r7.A01("deleteChat", Arrays.asList("pin_v1", "mute", "clearChat", "deleteChat", "archive", "star", "markChatAsRead", "deleteMessageForMe"));
        r7.A01("clearChat", Arrays.asList("clearChat", "deleteChat", "star", "deleteMessageForMe"));
        r7.A01("archive", Collections.singletonList("pin_v1"));
        r7.A01("deleteMessageForMe", Collections.singletonList("star"));
        A00 = r7.A00();
        AnonymousClass1YB r1 = new AnonymousClass1YB();
        r1.A01("archive", Collections.singletonList("setting_unarchiveChats"));
        A01 = r1.A00();
    }

    public static Pair A00(AnonymousClass1JQ r3) {
        AbstractC14640lm ABH;
        AnonymousClass1IS r1;
        if (r3 instanceof AbstractC34421g7) {
            AbstractC34421g7 r32 = (AbstractC34421g7) r3;
            ABH = r32.ABH();
            r1 = r32.AEI();
        } else if (r3 instanceof AbstractC34381g3) {
            ABH = ((AbstractC34381g3) r3).ABH();
            r1 = null;
        } else {
            StringBuilder sb = new StringBuilder("SyncdCrossIndexDependencyUtil/getValues: ");
            sb.append(r3.A03());
            sb.append(" mutation needs to implement either MessageKeyProvider or ChatJidProvider");
            throw new IllegalArgumentException(sb.toString());
        }
        return new Pair(ABH, r1);
    }
}
