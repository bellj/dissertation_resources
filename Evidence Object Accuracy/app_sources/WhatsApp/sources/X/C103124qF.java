package X;

import android.content.Context;
import com.whatsapp.identity.IdentityVerificationActivity;

/* renamed from: X.4qF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103124qF implements AbstractC009204q {
    public final /* synthetic */ IdentityVerificationActivity A00;

    public C103124qF(IdentityVerificationActivity identityVerificationActivity) {
        this.A00 = identityVerificationActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
