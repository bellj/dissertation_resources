package X;

/* renamed from: X.3an  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70133an implements AnonymousClass5WM {
    public final /* synthetic */ C58582r1 A00;

    public C70133an(C58582r1 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WM
    public void ASv() {
        C36021jC.A00(this.A00.A02, 0);
    }

    @Override // X.AnonymousClass5WM
    public void ATz(boolean z, boolean z2) {
        C58582r1 r2 = this.A00;
        C36021jC.A00(r2.A02, 0);
        long A0L = AbstractActivityC13740kG.A0L(r2.A06, r2);
        C625937v r3 = new C625937v(r2.A0B, r2.A0S, r2.A01, A0L, z, z2);
        r2.A00 = r3;
        r2.A0W.Aaz(r3, new Object[0]);
    }
}
