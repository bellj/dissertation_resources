package X;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import com.whatsapp.R;
import com.whatsapp.camera.overlays.ZoomOverlay;

/* renamed from: X.2UA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UA implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener {
    public float A00 = 1.0f;
    public final ScaleGestureDetector A01;
    public final AnonymousClass0MU A02;
    public final AnonymousClass2U9 A03;

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public AnonymousClass2UA(Context context, AnonymousClass2U9 r3) {
        this.A02 = new AnonymousClass0MU(context, this);
        this.A01 = new ScaleGestureDetector(context, this);
        this.A03 = r3;
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        AnonymousClass2U9 r0 = this.A03;
        motionEvent.getX();
        motionEvent.getY();
        r0.A00.A0A();
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        float scaleFactor = scaleGestureDetector.getScaleFactor();
        float f = this.A00 * scaleFactor * scaleFactor;
        this.A00 = f;
        if (f < 1.0f) {
            this.A00 = 1.0f;
            f = 1.0f;
        }
        AnonymousClass2P8 r1 = this.A03.A00.A0E;
        ZoomOverlay zoomOverlay = r1.A04;
        float maxScale = zoomOverlay.getMaxScale();
        if (maxScale < 1.0f) {
            return true;
        }
        if (f > maxScale) {
            f = maxScale;
        }
        AnonymousClass1s9 r2 = r1.A01;
        int AdD = r2.AdD(Math.round((((float) r2.getMaxZoom()) * (f - 1.0f)) / (maxScale - 1.0f)));
        if (r2.AJy()) {
            return true;
        }
        zoomOverlay.A00 = f;
        zoomOverlay.A02 = zoomOverlay.getContext().getString(R.string.camera_zoom_value, Float.valueOf(((float) AdD) / 100.0f));
        zoomOverlay.invalidate();
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        AnonymousClass2U9 r0 = this.A03;
        float f = this.A00;
        AnonymousClass1s8 r1 = r0.A00;
        if (r1.A0v.isEmpty()) {
            r1.A0N(false);
        }
        AnonymousClass2P8 r12 = r1.A0E;
        boolean AJy = r12.A01.AJy();
        ZoomOverlay zoomOverlay = r12.A04;
        if (!AJy) {
            zoomOverlay.setVisibility(0);
            zoomOverlay.A00 = f;
            zoomOverlay.invalidate();
            zoomOverlay.removeCallbacks(zoomOverlay.A07);
            return true;
        }
        zoomOverlay.setVisibility(4);
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        AnonymousClass1s8 r1 = this.A03.A00;
        r1.A0N(true);
        ZoomOverlay zoomOverlay = r1.A0E.A04;
        zoomOverlay.invalidate();
        zoomOverlay.postDelayed(zoomOverlay.A07, 300);
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        AnonymousClass2U9 r0 = this.A03;
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        AnonymousClass1s8 r1 = r0.A00;
        r1.A0A.AA4(x, y);
        r1.A0A.A7E();
        if (r1.A0w || !r1.A0v.isEmpty()) {
            return true;
        }
        r1.A0N(false);
        return true;
    }
}
