package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

/* renamed from: X.3Nq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66453Nq implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AbstractC14670lq A01;

    public ViewTreeObserver$OnPreDrawListenerC66453Nq(View view, AbstractC14670lq r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A00;
        int width = view.getWidth();
        if (width <= 0) {
            return true;
        }
        C12980iv.A1G(view, this);
        TextView textView = this.A01.A0i;
        int compoundPaddingLeft = width - (textView.getCompoundPaddingLeft() + textView.getCompoundPaddingRight());
        String A0q = C12980iv.A0q(textView);
        float textSize = textView.getTextSize();
        while (textSize > 1.0f && textView.getPaint().measureText(A0q) >= ((float) compoundPaddingLeft)) {
            textSize -= 1.0f;
            textView.setTextSize(textSize);
        }
        return true;
    }
}
