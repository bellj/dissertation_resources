package X;

import android.graphics.Bitmap;
import com.whatsapp.media.transcode.Mozjpeg;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;

/* renamed from: X.3GQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GQ {
    public static final Mozjpeg A00 = new Mozjpeg();

    public static byte[] A00(Bitmap bitmap, int i, boolean z) {
        if (bitmap.getConfig() == Bitmap.Config.ARGB_8888) {
            try {
                File createTempFile = File.createTempFile(C22200yh.A0L(), null);
                A00.A00(bitmap, createTempFile.getAbsolutePath(), i, false, z);
                if (createTempFile.length() > 0) {
                    BufferedInputStream A0h = C12990iw.A0h(createTempFile);
                    try {
                        byte[] A04 = AnonymousClass1P1.A04(A0h);
                        A0h.close();
                        createTempFile.delete();
                        return A04;
                    } catch (Throwable th) {
                        try {
                            A0h.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                }
            } catch (IOException e) {
                Log.e("BitmapCompressor/createCompressedByteArray", e);
                return null;
            }
        }
        return null;
    }
}
