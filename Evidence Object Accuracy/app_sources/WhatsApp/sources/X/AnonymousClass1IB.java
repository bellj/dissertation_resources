package X;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1IB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IB implements RejectedExecutionHandler {
    public final AtomicBoolean A00 = new AtomicBoolean();
    public final boolean A01 = true;

    @Override // java.util.concurrent.RejectedExecutionHandler
    public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
        AbstractC15710nm r2;
        try {
            threadPoolExecutor.getQueue().put(runnable);
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
        if (this.A01 && !this.A00.getAndSet(true) && (r2 = AnonymousClass168.A03) != null) {
            r2.AaV("waworkers-task-queued", String.valueOf(threadPoolExecutor.getActiveCount()), true);
        }
    }
}
