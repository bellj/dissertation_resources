package X;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.whatsapp.R;

/* renamed from: X.0EA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EA extends AnonymousClass0P6 {
    public C05830Rd A00;
    public boolean A01;
    public boolean A02 = false;

    public AnonymousClass0EA(AnonymousClass02N r2, AnonymousClass0Q9 r3, boolean z) {
        super(r2, r3);
        this.A01 = z;
    }

    public C05830Rd A02(Context context) {
        int i;
        int i2;
        Animation loadAnimation;
        C05830Rd r1;
        if (this.A02) {
            return this.A00;
        }
        AnonymousClass0Q9 r0 = super.A01;
        AnonymousClass01E r7 = r0.A04;
        boolean z = false;
        if (r0.A01 == EnumC03930Jr.VISIBLE) {
            z = true;
        }
        boolean z2 = this.A01;
        AnonymousClass0O9 r02 = r7.A0C;
        if (r02 == null) {
            i = 0;
        } else {
            i = r02.A03;
        }
        if (z2) {
            if (z) {
                if (r02 != null) {
                    i2 = r02.A04;
                }
                i2 = 0;
            } else {
                if (r02 != null) {
                    i2 = r02.A05;
                }
                i2 = 0;
            }
        } else if (z) {
            if (r02 != null) {
                i2 = r02.A01;
            }
            i2 = 0;
        } else {
            if (r02 != null) {
                i2 = r02.A02;
            }
            i2 = 0;
        }
        r7.A0O(0, 0, 0, 0);
        ViewGroup viewGroup = r7.A0B;
        C05830Rd r4 = null;
        if (!(viewGroup == null || viewGroup.getTag(R.id.visible_removing_fragment_view_tag) == null)) {
            r7.A0B.setTag(R.id.visible_removing_fragment_view_tag, null);
        }
        ViewGroup viewGroup2 = r7.A0B;
        if (viewGroup2 == null || viewGroup2.getLayoutTransition() == null) {
            if (i2 == 0) {
                if (i != 0) {
                    if (i == 4097) {
                        i2 = R.animator.fragment_open_exit;
                        if (z) {
                            i2 = R.animator.fragment_open_enter;
                        }
                    } else if (i == 4099) {
                        i2 = R.animator.fragment_fade_exit;
                        if (z) {
                            i2 = R.animator.fragment_fade_enter;
                        }
                    } else if (i != 8194) {
                        i2 = -1;
                    } else {
                        i2 = R.animator.fragment_close_exit;
                        if (z) {
                            i2 = R.animator.fragment_close_enter;
                        }
                    }
                }
            }
            boolean equals = "anim".equals(context.getResources().getResourceTypeName(i2));
            try {
                if (equals) {
                    try {
                        loadAnimation = AnimationUtils.loadAnimation(context, i2);
                    } catch (Resources.NotFoundException e) {
                        throw e;
                    } catch (RuntimeException unused) {
                    }
                    if (loadAnimation != null) {
                        r1 = new C05830Rd(loadAnimation);
                        r4 = r1;
                    }
                }
                Animator loadAnimator = AnimatorInflater.loadAnimator(context, i2);
                if (loadAnimator != null) {
                    r1 = new C05830Rd(loadAnimator);
                    r4 = r1;
                }
            } catch (RuntimeException e2) {
                if (!equals) {
                    Animation loadAnimation2 = AnimationUtils.loadAnimation(context, i2);
                    if (loadAnimation2 != null) {
                        r4 = new C05830Rd(loadAnimation2);
                    }
                } else {
                    throw e2;
                }
            }
        }
        this.A00 = r4;
        this.A02 = true;
        return r4;
    }
}
