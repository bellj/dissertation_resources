package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;

/* renamed from: X.1f0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33741f0 extends C33711ex {
    public C33741f0(C16470p4 r1) {
        super(r1);
    }

    @Override // X.C33711ex
    public String A05(Context context) {
        String A01;
        Number number;
        String A05 = super.A05(context);
        C16470p4 r4 = this.A00;
        if (r4 != null) {
            if ("review_order".equals(r4.A00()) && !TextUtils.isEmpty(r4.A01()) && (A01 = r4.A01()) != null && (number = (Number) AnonymousClass3J5.A00.get(AnonymousClass3J5.A03(A01))) != null) {
                String string = context.getResources().getString(number.intValue());
                A05 = TextUtils.isEmpty(A05) ? string : AnonymousClass1US.A09("\n", string, A05);
            }
            if ("payment_method".equals(r4.A00()) && !TextUtils.isEmpty(r4.A01())) {
                String A012 = r4.A01();
                Object obj = null;
                if (A012 != null) {
                    Pair A00 = AnonymousClass3J5.A00(A012);
                    if (A00 != null) {
                        obj = A00.first;
                    }
                    Number number2 = (Number) AnonymousClass3J5.A01.get(obj);
                    if (number2 != null) {
                        String string2 = context.getResources().getString(number2.intValue());
                        return !TextUtils.isEmpty(A05) ? AnonymousClass1US.A09("\n", string2, A05) : string2;
                    }
                }
            }
        }
        return A05;
    }

    @Override // X.C33711ex
    public String A06(Context context) {
        C16470p4 r0 = this.A00;
        if (r0 == null || !"review_order".equals(r0.A00())) {
            return super.A06(context);
        }
        String A05 = A05(context);
        return A05 == null ? "" : A05;
    }

    @Override // X.C33711ex
    public String A08(Context context) {
        C16470p4 r0 = this.A00;
        if (r0 == null || !"review_order".equals(r0.A00())) {
            return super.A08(context);
        }
        String A05 = A05(context);
        return A05 == null ? "" : A05;
    }

    @Override // X.C33711ex
    public void A0A(AbstractC15340mz r10, C39971qq r11) {
        super.A0A(r10, r11);
        C16470p4 ABf = ((AbstractC16390ow) r10).ABf();
        if (!(ABf == null || ABf.A03 == null || ABf.A00 != 5)) {
            AnonymousClass1G3 r3 = r11.A03;
            C57752nZ r0 = ((C27081Fy) r3.A00).A0K;
            if (r0 == null) {
                r0 = C57752nZ.A07;
            }
            AnonymousClass1G4 A0T = r0.A0T();
            AnonymousClass1G4 A0T2 = ((C57752nZ) A0T.A00).A0c().A0T();
            for (AnonymousClass1Z9 r1 : ABf.A03.A00) {
                AnonymousClass1G4 A0T3 = C57162ma.A03.A0T();
                AnonymousClass1Z8 r7 = r1.A01;
                String str = r7.A00;
                A0T3.A03();
                C57162ma r12 = (C57162ma) A0T3.A00;
                r12.A00 |= 1;
                r12.A01 = str;
                String str2 = r7.A01;
                if (str2 != null) {
                    A0T3.A03();
                    C57162ma r13 = (C57162ma) A0T3.A00;
                    r13.A00 |= 2;
                    r13.A02 = str2;
                }
                A0T2.A03();
                C57172mb r2 = (C57172mb) A0T2.A00;
                AnonymousClass1K6 r14 = r2.A02;
                if (!((AnonymousClass1K7) r14).A00) {
                    r14 = AbstractC27091Fz.A0G(r14);
                    r2.A02 = r14;
                }
                r14.add(A0T3.A02());
            }
            A0T.A03();
            C57752nZ r15 = (C57752nZ) A0T.A00;
            r15.A06 = A0T2.A02();
            r15.A01 = 6;
            r3.A07((C57752nZ) A0T.A02());
        }
    }
}
