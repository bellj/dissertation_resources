package X;

import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;

/* renamed from: X.60X  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60X {
    public static final C124845qD A0H = new C124845qD();
    public CameraDevice A00;
    public C130185yw A01;
    public AnonymousClass61Q A02;
    public C1308060a A03;
    public AnonymousClass66L A04;
    public C1308260c A05;
    public AbstractC1311561m A06;
    public C119085cr A07;
    public AbstractC130695zp A08;
    public AbstractC136526Mw A09;
    public C130055yj A0A;
    public boolean A0B;
    public boolean A0C;
    public final C129805yK A0D;
    public final C124845qD A0E;
    public final C1308560f A0F;
    public volatile boolean A0G;

    public AnonymousClass60X(C130055yj r3, C1308560f r4) {
        C124845qD r1 = A0H;
        this.A0F = r4;
        this.A0A = r3;
        this.A0D = new C129805yK(r4);
        this.A0E = r1;
    }

    public void A00() {
        this.A0D.A02("Failed to release PhotoCaptureController.", false);
        this.A00 = null;
        this.A08 = null;
        this.A07 = null;
        this.A03 = null;
        this.A05 = null;
        this.A02 = null;
        this.A01 = null;
        this.A06 = null;
        AbstractC136526Mw r0 = this.A09;
        if (r0 != null) {
            r0.release();
            this.A09 = null;
        }
        AnonymousClass66L r02 = this.A04;
        if (r02 != null) {
            r02.release();
            this.A04 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003b, code lost:
        if (X.C12970iu.A1Y(r5.A06.AAQ(X.AbstractC1311561m.A08)) == false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0057, code lost:
        if (X.C12970iu.A1Y(r5.A06.AAQ(X.AbstractC1311561m.A0C)) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0073, code lost:
        if (r0 == null) goto L_0x00a0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.hardware.camera2.CameraDevice r6, X.AnonymousClass66P r7, X.C130185yw r8, X.AnonymousClass61Q r9, X.C1308060a r10, X.C1308260c r11, X.AbstractC1311561m r12, X.C119085cr r13, X.AbstractC130695zp r14, int r15) {
        /*
        // Method dump skipped, instructions count: 264
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60X.A01(android.hardware.camera2.CameraDevice, X.66P, X.5yw, X.61Q, X.60a, X.60c, X.61m, X.5cr, X.5zp, int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0057, code lost:
        if (X.C12960it.A05(r3.A03(X.AbstractC130685zo.A0A)) != 2) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0069, code lost:
        if (X.C12960it.A05(r3.A03(X.AbstractC130685zo.A0A)) != 1) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x007b, code lost:
        if (r1 != 1) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0159, code lost:
        if (r11 == null) goto L_0x015b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x015f, code lost:
        if (r10 == null) goto L_0x0161;
     */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x045a  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0463  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x04c1  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x04da  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0507  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x0509  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(android.hardware.camera2.CameraManager r31, android.hardware.camera2.CaptureRequest.Builder r32, X.C129455xk r33, X.AnonymousClass66I r34, X.C124855qE r35, X.AnonymousClass60B r36, int r37, int r38, int r39, boolean r40) {
        /*
        // Method dump skipped, instructions count: 1371
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass60X.A02(android.hardware.camera2.CameraManager, android.hardware.camera2.CaptureRequest$Builder, X.5xk, X.66I, X.5qE, X.60B, int, int, int, boolean):void");
    }

    public void A03(CameraManager cameraManager, CaptureRequest.Builder builder, C129455xk r18, AnonymousClass66I r19, C124855qE r20, AnonymousClass60B r21, int i, int i2, int i3, boolean z) {
        String str;
        AnonymousClass61Q r0;
        if (this.A00 == null || (r0 = this.A02) == null || !r0.A0Q) {
            str = "Camera not ready to take photo.";
        } else if (this.A0G) {
            str = "Cannot take photo, another capture in progress.";
        } else if (this.A03.A0D) {
            str = "Cannot take photo, video recording in progress.";
        } else {
            C117305Zk.A1C(this.A07);
            this.A0G = true;
            this.A01.A02();
            this.A0F.A00(new C119005cj(r18, this), "take_photo", new CallableC135986Ko(cameraManager, builder, r18, this, r19, r20, r21, i, i2, i3, z));
            return;
        }
        A04(r18, new AnonymousClass6L0(str));
    }

    public void A04(C129455xk r4, Exception exc) {
        this.A0F.A05(new AnonymousClass6J4(r4, this, exc), this.A0A.A03);
    }
}
