package X;

import android.hardware.camera2.CameraDevice;
import com.whatsapp.util.Log;

/* renamed from: X.2Zk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51862Zk extends CameraDevice.StateCallback {
    public final /* synthetic */ AnonymousClass2NW A00;

    public C51862Zk(AnonymousClass2NW r1) {
        this.A00 = r1;
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onClosed(CameraDevice cameraDevice) {
        AnonymousClass2NW r2 = this.A00;
        r2.A00 = 0;
        Log.i("voip/video/VoipCamera/ cameraDevice closed");
        if (r2.A06) {
            r2.A06 = false;
            if (r2.startOnCameraThread() != 0) {
                r2.cameraEventsDispatcher.A02();
            }
        }
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onDisconnected(CameraDevice cameraDevice) {
        AnonymousClass2NW r1 = this.A00;
        if (cameraDevice == AnonymousClass2NW.A02(r1)) {
            Log.i("voip/video/VoipCamera/ cameraDevice disconnected");
            r1.stopPeriodicCameraCallbackCheck();
            r1.cameraEventsDispatcher.A01();
        }
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onError(CameraDevice cameraDevice, int i) {
        Log.i(C12960it.A0W(i, "voip/video/VoipCamera/ cameraDevice error "));
        this.A00.cameraEventsDispatcher.A02();
    }

    @Override // android.hardware.camera2.CameraDevice.StateCallback
    public void onOpened(CameraDevice cameraDevice) {
        Log.i("voip/video/VoipCamera/ camera opened");
        AnonymousClass2NW r1 = this.A00;
        r1.A00 = 2;
        AnonymousClass2NW.A05(r1, cameraDevice);
        if (r1.videoPort != null && r1.A07() != 0) {
            r1.cameraEventsDispatcher.A02();
        }
    }
}
