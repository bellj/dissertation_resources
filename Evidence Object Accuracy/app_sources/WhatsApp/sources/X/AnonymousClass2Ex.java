package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.whatsapp.chatinfo.view.custom.BusinessChatInfoLayout;

/* renamed from: X.2Ex  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2Ex extends ViewGroup implements AnonymousClass004 {
    public AnonymousClass2P7 A00;

    public AnonymousClass2Ex(Context context) {
        super(context);
        A00();
    }

    public AnonymousClass2Ex(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AnonymousClass2Ex(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        BusinessChatInfoLayout businessChatInfoLayout;
        AbstractC60352wl r1 = (AbstractC60352wl) this;
        if (r1 instanceof BusinessChatInfoLayout) {
            BusinessChatInfoLayout businessChatInfoLayout2 = (BusinessChatInfoLayout) r1;
            if (!businessChatInfoLayout2.A00) {
                businessChatInfoLayout2.A00 = true;
                businessChatInfoLayout = businessChatInfoLayout2;
            } else {
                return;
            }
        } else if (!r1.A00) {
            r1.A00 = true;
            businessChatInfoLayout = r1;
        } else {
            return;
        }
        AnonymousClass01J r2 = ((AnonymousClass2P6) ((AnonymousClass2P5) businessChatInfoLayout.generatedComponent())).A06;
        businessChatInfoLayout.A0Q = (C14850m9) r2.A04.get();
        businessChatInfoLayout.A0M = (AnonymousClass130) r2.A41.get();
        businessChatInfoLayout.A0R = (C20710wC) r2.A8m.get();
        businessChatInfoLayout.A0P = (AnonymousClass19M) r2.A6R.get();
        businessChatInfoLayout.A0N = (C15610nY) r2.AMe.get();
        businessChatInfoLayout.A0O = (AnonymousClass018) r2.ANb.get();
        businessChatInfoLayout.A0S = (AnonymousClass12F) r2.AJM.get();
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
