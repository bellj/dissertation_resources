package X;

import android.content.Context;
import com.whatsapp.settings.Settings;

/* renamed from: X.4qj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103424qj implements AbstractC009204q {
    public final /* synthetic */ Settings A00;

    public C103424qj(Settings settings) {
        this.A00 = settings;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
