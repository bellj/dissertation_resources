package X;

import android.util.Log;
import android.view.View;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0Q9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Q9 {
    public AnonymousClass0JF A00;
    public EnumC03930Jr A01;
    public boolean A02 = false;
    public boolean A03 = false;
    public final AnonymousClass01E A04;
    public final C06460Ts A05;
    public final HashSet A06 = new HashSet();
    public final List A07 = new ArrayList();

    public AnonymousClass0Q9(AnonymousClass02N r3, C06460Ts r4, AnonymousClass0JF r5, EnumC03930Jr r6) {
        AnonymousClass01E r1 = r4.A02;
        this.A01 = r6;
        this.A00 = r5;
        this.A04 = r1;
        r3.A03(new C07350Xq(this));
        this.A05 = r4;
    }

    public void A00() {
        if (!this.A03) {
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb = new StringBuilder("SpecialEffectsController: ");
                sb.append(this);
                sb.append(" has called complete.");
                Log.v("FragmentManager", sb.toString());
            }
            this.A03 = true;
            for (Runnable runnable : this.A07) {
                runnable.run();
            }
        }
        this.A05.A04();
    }

    public void A01() {
        float f;
        if (this.A00 == AnonymousClass0JF.ADDING) {
            C06460Ts r4 = this.A05;
            AnonymousClass01E r3 = r4.A02;
            View findFocus = r3.A0A.findFocus();
            if (findFocus != null) {
                r3.A07().A06 = findFocus;
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb = new StringBuilder("requestFocus: Saved focused view ");
                    sb.append(findFocus);
                    sb.append(" for Fragment ");
                    sb.append(r3);
                    Log.v("FragmentManager", sb.toString());
                }
            }
            View A05 = this.A04.A05();
            if (A05.getParent() == null) {
                r4.A02();
                A05.setAlpha(0.0f);
            }
            if (A05.getAlpha() == 0.0f && A05.getVisibility() == 0) {
                A05.setVisibility(4);
            }
            AnonymousClass0O9 r0 = r3.A0C;
            if (r0 == null) {
                f = 1.0f;
            } else {
                f = r0.A00;
            }
            A05.setAlpha(f);
        }
    }

    public final void A02() {
        if (!this.A02) {
            this.A02 = true;
            HashSet hashSet = this.A06;
            if (hashSet.isEmpty()) {
                A00();
                return;
            }
            Iterator it = new ArrayList(hashSet).iterator();
            while (it.hasNext()) {
                ((AnonymousClass02N) it.next()).A01();
            }
        }
    }

    public final void A03(AnonymousClass0JF r7, EnumC03930Jr r8) {
        AnonymousClass0JF r0;
        int[] iArr = AnonymousClass0MB.A00;
        int ordinal = r7.ordinal();
        int i = iArr[ordinal];
        if (ordinal != 1) {
            if (i == 2) {
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("SpecialEffectsController: For fragment ");
                    sb.append(this.A04);
                    sb.append(" mFinalState = ");
                    sb.append(this.A01);
                    sb.append(" -> REMOVED. mLifecycleImpact  = ");
                    sb.append(this.A00);
                    sb.append(" to REMOVING.");
                    Log.v("FragmentManager", sb.toString());
                }
                this.A01 = EnumC03930Jr.REMOVED;
                r0 = AnonymousClass0JF.REMOVING;
            } else if (i == 3 && this.A01 != EnumC03930Jr.REMOVED) {
                if (AnonymousClass01F.A01(2)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("SpecialEffectsController: For fragment ");
                    sb2.append(this.A04);
                    sb2.append(" mFinalState = ");
                    sb2.append(this.A01);
                    sb2.append(" -> ");
                    sb2.append(r8);
                    sb2.append(". ");
                    Log.v("FragmentManager", sb2.toString());
                }
                this.A01 = r8;
                return;
            } else {
                return;
            }
        } else if (this.A01 == EnumC03930Jr.REMOVED) {
            if (AnonymousClass01F.A01(2)) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("SpecialEffectsController: For fragment ");
                sb3.append(this.A04);
                sb3.append(" mFinalState = REMOVED -> VISIBLE. mLifecycleImpact = ");
                sb3.append(this.A00);
                sb3.append(" to ADDING.");
                Log.v("FragmentManager", sb3.toString());
            }
            this.A01 = EnumC03930Jr.VISIBLE;
            r0 = AnonymousClass0JF.ADDING;
        } else {
            return;
        }
        this.A00 = r0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Operation ");
        sb.append("{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} ");
        sb.append("{");
        sb.append("mFinalState = ");
        sb.append(this.A01);
        sb.append("} ");
        sb.append("{");
        sb.append("mLifecycleImpact = ");
        sb.append(this.A00);
        sb.append("} ");
        sb.append("{");
        sb.append("mFragment = ");
        sb.append(this.A04);
        sb.append("}");
        return sb.toString();
    }
}
