package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3mc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77023mc extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(18);
    public final String A00;
    public final byte[] A01;

    public C77023mc(Parcel parcel) {
        super("PRIV");
        this.A00 = parcel.readString();
        this.A01 = parcel.createByteArray();
    }

    public C77023mc(String str, byte[] bArr) {
        super("PRIV");
        this.A00 = str;
        this.A01 = bArr;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77023mc.class != obj.getClass()) {
                return false;
            }
            C77023mc r5 = (C77023mc) obj;
            if (!AnonymousClass3JZ.A0H(this.A00, r5.A00) || !Arrays.equals(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C72453ed.A05(C72453ed.A0E(this.A00)) + Arrays.hashCode(this.A01);
    }

    @Override // X.AbstractC107404xH, java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.A00);
        A0h.append(": owner=");
        return C12960it.A0d(this.A00, A0h);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeByteArray(this.A01);
    }
}
