package X;

import com.whatsapp.search.SearchViewModel;

/* renamed from: X.2Vv  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Vv implements AbstractC35521iA {
    public final /* synthetic */ SearchViewModel A00;

    @Override // X.AbstractC35521iA
    public void AeB() {
    }

    @Override // X.AbstractC35521iA
    public void AeQ() {
    }

    @Override // X.AbstractC35521iA
    public void close() {
    }

    public AnonymousClass2Vv(SearchViewModel searchViewModel) {
        this.A00 = searchViewModel;
    }

    @Override // X.AbstractC35521iA
    public AbstractC16130oV AEG(int i) {
        if (i == -2) {
            return null;
        }
        SearchViewModel searchViewModel = this.A00;
        if (i < searchViewModel.A0K.A01.size()) {
            return (AbstractC16130oV) searchViewModel.A0K.A01.get(i);
        }
        return null;
    }

    @Override // X.AbstractC35521iA
    public int AFq(AnonymousClass1IS r4) {
        int i = 0;
        while (true) {
            SearchViewModel searchViewModel = this.A00;
            if (i >= searchViewModel.A0K.A01.size()) {
                return -2;
            }
            if (C29941Vi.A00(r4, ((AbstractC15340mz) searchViewModel.A0K.A01.get(i)).A0z)) {
                return i;
            }
            i++;
        }
    }

    @Override // X.AbstractC35521iA
    public void ASu() {
        this.A00.A0O(2);
    }

    @Override // X.AbstractC35521iA
    public void Ac4(Runnable runnable) {
        this.A00.A0Z = runnable;
    }

    @Override // X.AbstractC35521iA
    public void AfO(int i) {
        SearchViewModel searchViewModel = this.A00;
        int A07 = searchViewModel.A07((AbstractC16130oV) searchViewModel.A0K.A01.get(i));
        if (A07 >= 0 && A07 <= searchViewModel.A0A().size()) {
            searchViewModel.A0T.A0A(Integer.valueOf(A07));
        }
    }

    @Override // X.AbstractC35521iA
    public int getCount() {
        return this.A00.A0K.A01.size();
    }
}
