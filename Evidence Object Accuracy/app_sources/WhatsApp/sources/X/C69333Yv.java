package X;

import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.3Yv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69333Yv implements AbstractC32461cC {
    public final /* synthetic */ AnonymousClass38A A00;

    public C69333Yv(AnonymousClass38A r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC32461cC
    public void APl(int i) {
        this.A00.A00 = i;
    }

    @Override // X.AbstractC32461cC
    public void AR7(C15580nU r14, C15580nU r15, UserJid userJid, AnonymousClass1PD r17, String str, String str2, String str3, Map map, int i, int i2, long j, long j2) {
        AnonymousClass38A r1 = this.A00;
        r1.A01 = new C63373Bi(r14, userJid, r17, str, str3, r1.A05.A0B(map), i, i2, j);
    }
}
