package X;

import com.whatsapp.twofactor.SetCodeFragment;

/* renamed from: X.3U1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3U1 implements AbstractC116435Vk {
    public final /* synthetic */ SetCodeFragment A00;

    public AnonymousClass3U1(SetCodeFragment setCodeFragment) {
        this.A00 = setCodeFragment;
    }

    @Override // X.AbstractC116435Vk
    public void AOH(String str) {
        SetCodeFragment setCodeFragment = this.A00;
        if (((AnonymousClass01E) setCodeFragment).A03 >= 7) {
            setCodeFragment.A1A();
            int i = setCodeFragment.A00;
            if (i == 1) {
                setCodeFragment.A04.A02 = str;
            } else if (i == 2) {
                setCodeFragment.A04.A03 = str;
            }
            if (!setCodeFragment.A1B(str)) {
                return;
            }
            if (setCodeFragment.A00 != 2 || !setCodeFragment.A04.A2h(setCodeFragment)) {
                SetCodeFragment.A01(setCodeFragment);
            }
        }
    }

    @Override // X.AbstractC116435Vk
    public void AT5(String str) {
        SetCodeFragment setCodeFragment = this.A00;
        if (((AnonymousClass01E) setCodeFragment).A03 >= 7) {
            int i = setCodeFragment.A00;
            if (i == 1) {
                setCodeFragment.A04.A02 = str;
            } else if (i == 2) {
                setCodeFragment.A04.A03 = str;
            }
            C12990iw.A1G(setCodeFragment.A02);
            setCodeFragment.A1A();
        }
    }
}
