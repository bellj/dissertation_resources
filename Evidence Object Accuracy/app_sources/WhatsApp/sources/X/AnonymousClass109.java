package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0211000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executor;

/* renamed from: X.109  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass109 {
    public boolean A00;
    public boolean A01 = false;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C20870wS A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final C15650ng A08;
    public final AnonymousClass12H A09;
    public final C22830zi A0A;
    public final C22180yf A0B;
    public final C14850m9 A0C;
    public final C16120oU A0D;
    public final C14300lD A0E;
    public final C26751Er A0F;
    public final C17040qA A0G;
    public final C26741Eq A0H;
    public final C26471Dp A0I;
    public final C239713s A0J;
    public final C14540lb A0K;
    public final C26761Es A0L;
    public final C22390z0 A0M;
    public final C22280yp A0N;
    public final C20320vZ A0O;
    public final C22900zp A0P;
    public final AbstractC14440lR A0Q;
    public final AnonymousClass1MO A0R;
    public final Object A0S = new Object();
    public final WeakHashMap A0T = new WeakHashMap();
    public final Executor A0U;

    public AnonymousClass109(AbstractC15710nm r5, C14900mE r6, C15570nT r7, C20870wS r8, C14830m7 r9, C16590pI r10, C15650ng r11, AnonymousClass12H r12, C22830zi r13, C22180yf r14, C14850m9 r15, C16120oU r16, C14300lD r17, C26751Er r18, C17040qA r19, C26741Eq r20, C26471Dp r21, C239713s r22, C14540lb r23, C26761Es r24, C22390z0 r25, C22280yp r26, C20320vZ r27, C22900zp r28, AbstractC14440lR r29) {
        AnonymousClass1MO r0;
        this.A07 = r10;
        this.A06 = r9;
        this.A0C = r15;
        this.A03 = r6;
        this.A02 = r5;
        this.A04 = r7;
        this.A0Q = r29;
        this.A0D = r16;
        this.A0I = r21;
        this.A0B = r14;
        this.A05 = r8;
        this.A0J = r22;
        this.A0O = r27;
        this.A0E = r17;
        this.A0N = r26;
        this.A08 = r11;
        this.A0P = r28;
        this.A0K = r23;
        this.A09 = r12;
        this.A0H = r20;
        this.A0G = r19;
        this.A0A = r13;
        this.A0M = r25;
        this.A0F = r18;
        this.A0L = r24;
        this.A0U = new ExecutorC39691qM(r6);
        if (r15.A02(776) > 0) {
            r0 = new AnonymousClass1MO(r29, r15.A02(776), false);
        } else {
            r0 = null;
        }
        this.A0R = r0;
    }

    public final C38421o4 A00(AbstractC16130oV r7) {
        synchronized (this.A0S) {
            for (Map.Entry entry : this.A0T.entrySet()) {
                C38421o4 r0 = (C38421o4) entry.getKey();
                AnonymousClass1IS r2 = r7.A0z;
                if (r2 != null) {
                    Iterator it = r0.A01.iterator();
                    while (it.hasNext()) {
                        if (r2.equals(((AbstractC16130oV) it.next()).A0z)) {
                            return (C38421o4) entry.getKey();
                        }
                    }
                    continue;
                }
            }
            return null;
        }
    }

    public AnonymousClass1KC A01(AbstractC16130oV r4) {
        AnonymousClass1KC r0;
        C38421o4 A00 = A00(r4);
        synchronized (this.A0S) {
            r0 = (AnonymousClass1KC) this.A0T.get(A00);
        }
        return r0;
    }

    public final void A02(C38421o4 r3, AnonymousClass1KC r4) {
        synchronized (this.A0S) {
            if (r4 == null) {
                this.A0T.remove(r3);
            } else {
                this.A0T.put(r3, r4);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x002b, code lost:
        if (X.C15380n4.A0L(r2) != false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.C38421o4 r12, X.AnonymousClass1KC r13, boolean r14, boolean r15) {
        /*
            r11 = this;
            r6 = r12
            r12.A01()
            r7 = r11
            r8 = r13
            r11.A02(r12, r13)
            X.0lL r0 = r13.A00()
            X.0lK r5 = r0.A05
            java.util.concurrent.CopyOnWriteArrayList r0 = r12.A01
            java.util.List r2 = java.util.Collections.unmodifiableList(r0)
            int r1 = r2.size()
            r0 = 1
            if (r1 != r0) goto L_0x00df
            r0 = 0
            java.lang.Object r0 = r2.get(r0)
            X.0mz r0 = (X.AbstractC15340mz) r0
            X.1IS r0 = r0.A0z
            X.0lm r2 = r0.A00
            boolean r0 = X.C15380n4.A0L(r2)
            if (r0 == 0) goto L_0x00df
        L_0x002d:
            boolean r0 = r11.A01
            if (r0 != 0) goto L_0x003e
            X.0m9 r1 = r11.A0C
            r0 = 1539(0x603, float:2.157E-42)
            boolean r0 = r1.A07(r0)
            r11.A00 = r0
            r0 = 1
            r11.A01 = r0
        L_0x003e:
            boolean r0 = r11.A00
            if (r0 == 0) goto L_0x00da
            X.0lK r0 = X.C14370lK.A08
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x00da
            if (r2 == 0) goto L_0x00da
            X.0yp r0 = r11.A0N
            boolean r0 = r0.A06(r2)
            if (r0 == 0) goto L_0x00da
            java.lang.String r0 = "express"
            r13.A0U = r0
            X.0lS r1 = r13.A0K
            X.AnonymousClass009.A05(r1)
            r0 = 4
            r1.A05(r0)
        L_0x0061:
            X.23m r1 = new X.23m
            r1.<init>(r12, r11, r13)
            java.util.concurrent.Executor r3 = r11.A0U
            X.0lj r0 = r13.A09
            r0.A03(r1, r3)
            X.23n r2 = new X.23n
            r2.<init>(r12, r11)
            X.0lj r1 = r13.A07
            r0 = 0
            r1.A03(r2, r0)
            X.23o r1 = new X.23o
            r1.<init>(r12, r11, r13)
            r4 = 0
            X.0lj r0 = r13.A08
            r0.A03(r1, r4)
            X.23p r1 = new X.23p
            r1.<init>(r12, r11, r13)
            X.0lj r0 = r13.A0C
            r0.A03(r1, r3)
            X.23q r1 = new X.23q
            r1.<init>(r12, r11, r13)
            X.0lj r0 = r13.A0B
            r0.A03(r1, r3)
            X.23r r1 = new X.23r
            r1.<init>(r12, r11, r13)
            X.0lj r0 = r13.A0D
            r0.A03(r1, r4)
            X.23s r1 = new X.23s
            r1.<init>(r12, r11, r13)
            X.0lj r0 = r13.A0F
            r0.A03(r1, r3)
            X.23t r1 = new X.23t
            r1.<init>(r12, r11, r13)
            X.0lj r0 = r13.A0G
            r0.A03(r1, r3)
            X.23u r2 = new X.23u
            r2.<init>(r12, r11, r13, r5)
            java.lang.String r1 = r13.A0U
            java.lang.String r0 = "express"
            if (r1 != r0) goto L_0x00c5
            X.0lj r0 = r13.A06
            r0.A03(r2, r4)
        L_0x00c5:
            X.23v r1 = new X.23v
            r1.<init>(r12, r11, r13)
            X.0lj r0 = r13.A05
            r0.A03(r1, r4)
            r10 = r15
            r9 = r14
            X.23w r5 = new X.23w
            r5.<init>(r6, r7, r8, r9, r10)
            r13.A03(r5, r3)
            return
        L_0x00da:
            java.lang.String r0 = "mms"
            r13.A0U = r0
            goto L_0x0061
        L_0x00df:
            r2 = 0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass109.A03(X.1o4, X.1KC, boolean, boolean):void");
    }

    public void A04(C38421o4 r13, AbstractC14470lU r14, C14480lV r15, AbstractC16130oV r16, byte[] bArr, boolean z, boolean z2, boolean z3, boolean z4) {
        AnonymousClass1KC r5;
        if (r16 == null || (r5 = A01(r16)) == null) {
            r5 = (AnonymousClass1KC) r14;
        }
        this.A0U.execute(new RunnableBRunnable0Shape5S0200000_I0_5(this, 40, r13));
        RunnableC459723y r2 = new Runnable(r13, this, r5, r15, bArr, z3, z4, z2, z) { // from class: X.23y
            public final /* synthetic */ C38421o4 A00;
            public final /* synthetic */ AnonymousClass109 A01;
            public final /* synthetic */ AnonymousClass1KC A02;
            public final /* synthetic */ C14480lV A03;
            public final /* synthetic */ boolean A04;
            public final /* synthetic */ boolean A05;
            public final /* synthetic */ boolean A06;
            public final /* synthetic */ boolean A07;
            public final /* synthetic */ byte[] A08;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
                this.A04 = r6;
                this.A05 = r7;
                this.A03 = r4;
                this.A06 = r8;
                this.A07 = r9;
                this.A08 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AbstractC39731qS r6;
                C39741qT r1;
                AnonymousClass109 r22 = this.A01;
                AnonymousClass1KC r12 = this.A02;
                C38421o4 r9 = this.A00;
                boolean z5 = this.A04;
                boolean z6 = this.A05;
                C14480lV r122 = this.A03;
                boolean z7 = this.A06;
                boolean z8 = this.A07;
                byte[] bArr2 = this.A08;
                if (r12 != null) {
                    Number number = (Number) r12.A0A.A00();
                    if (!r12.A02 && (number == null || number.intValue() == 0)) {
                        if (r12.A0U == null) {
                            r22.A03(r9, r12, z5, z6);
                            C14450lS r3 = r12.A0K;
                            AnonymousClass009.A05(r3);
                            synchronized (r3) {
                                if (!r3.A0D) {
                                    r3.A08 = 0;
                                } else if (!r3.A0C) {
                                    r3.A08 = 3;
                                } else {
                                    r3.A08 = 2;
                                }
                            }
                            AnonymousClass009.A05(r3);
                            r3.A03();
                            r22.A0E.A07(r12);
                            return;
                        }
                        r22.A03(r9, r12, z5, z6);
                        return;
                    }
                }
                AnonymousClass1K9 A01 = AnonymousClass1K9.A01(r22.A04, r9, r22.A0I, r22.A0J, r122, z7);
                C14300lD r52 = r22.A0E;
                AnonymousClass1KC A05 = r52.A05(A01, z8);
                r22.A03(r9, A05, z5, z6);
                if (!(r12 == null || (r6 = (AbstractC39731qS) r12.A0D.A00()) == null || !r6.A02)) {
                    Object A00 = r12.A08.A00();
                    if (A00 != null) {
                        A05.A08.A04(A00);
                    }
                    A05.A0D.A04(r6);
                    C14450lS r23 = r12.A0K;
                    AnonymousClass009.A05(r23);
                    synchronized (r23) {
                        r1 = r23.A04;
                    }
                    if (r1 != null) {
                        C14450lS r0 = A05.A0K;
                        AnonymousClass009.A05(r0);
                        r0.A06(r1);
                    }
                }
                if (r52.A0E(A05.A01().A05, A05.A00().A0E, A05.A0L.A02.A01)) {
                    A05.A03 = bArr2;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(r9.A01());
                sb.append("; action_params: ");
                sb.append(r122);
                r52.A0D(A05, sb.toString());
            }
        };
        AnonymousClass1MO r0 = this.A0R;
        if (r0 != null) {
            r0.execute(r2);
        } else {
            this.A0Q.Ab2(r2);
        }
    }

    public void A05(AbstractC15340mz r5, boolean z) {
        if (!(r5 instanceof AbstractC16130oV)) {
            StringBuilder sb = new StringBuilder("mediajobmanager/cancelmessage; attempt to cancel non-media message: ");
            sb.append(r5.A0z);
            Log.e(sb.toString());
        } else if (C37381mH.A00(r5.A0C, 2) >= 0) {
            StringBuilder sb2 = new StringBuilder("mediajobmanager/cancelmessage; attempt to cancel uploaded message: ");
            sb2.append(r5.A0z);
            Log.e(sb2.toString());
            if (z) {
                this.A03.A07(R.string.file_uploaded, 0);
            }
        } else {
            StringBuilder sb3 = new StringBuilder("mediajobmanager/cancelmessage: ");
            sb3.append(r5.A0z);
            Log.e(sb3.toString());
            AbstractC16130oV r3 = (AbstractC16130oV) r5;
            this.A0M.A01(r5);
            C43431x0 r2 = new C43431x0(this, Collections.singletonList(r3));
            this.A0Q.Ab2(r2);
            r2.A01(new AbstractC14590lg(r3) { // from class: X.23x
                public final /* synthetic */ AbstractC16130oV A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AbstractC14590lg
                public final void accept(Object obj) {
                    AnonymousClass109 r22 = AnonymousClass109.this;
                    AbstractC16130oV r32 = this.A01;
                    if (C26751Er.A01(r32, false)) {
                        r22.A08.A0W(r32);
                        r22.A0Q.Ab2(new RunnableBRunnable0Shape0S0211000_I0(r22, r32, 6, 2, true));
                    }
                }
            }, this.A0U);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        if (r1 == false) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A06(X.AbstractC16130oV r6) {
        /*
            r5 = this;
            X.0oX r0 = r6.A02
            r4 = 1
            if (r0 == 0) goto L_0x000a
            boolean r0 = r0.A0O
            if (r0 == 0) goto L_0x000a
            return r4
        L_0x000a:
            X.1KC r3 = r5.A01(r6)
            if (r3 == 0) goto L_0x0043
            X.0lD r0 = r5.A0E
            X.1Et r2 = r0.A0B
            X.1qQ r0 = r3.A01()
            X.0lK r1 = r0.A05
            X.1Ew r0 = r2.A04
            X.1pI r0 = r0.A00(r1)
            java.lang.Runnable r2 = r0.A00(r3)
            X.1pM r2 = (X.AbstractRunnableC39141pM) r2
            if (r2 == 0) goto L_0x0043
            monitor-enter(r2)
            X.1pJ r1 = r2.A00     // Catch: all -> 0x0040
            monitor-enter(r1)     // Catch: all -> 0x0040
            X.1p6 r0 = r1.A00     // Catch: all -> 0x003d
            monitor-exit(r1)     // Catch: all -> 0x0040
            if (r0 == 0) goto L_0x0038
            boolean r1 = r0.AII()     // Catch: all -> 0x0040
            r0 = 1
            if (r1 != 0) goto L_0x0039
        L_0x0038:
            r0 = 0
        L_0x0039:
            monitor-exit(r2)
            if (r0 == 0) goto L_0x0043
            return r4
        L_0x003d:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0040
            throw r0     // Catch: all -> 0x0040
        L_0x0040:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0043:
            r4 = 0
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass109.A06(X.0oV):boolean");
    }

    public boolean A07(AbstractC16130oV r4) {
        boolean containsKey;
        AnonymousClass1KC A01 = A01(r4);
        if (A01 != null) {
            C14540lb r2 = this.A0E.A0E;
            synchronized (r2) {
                containsKey = r2.A01.containsKey(A01);
            }
            if (containsKey) {
                return true;
            }
        }
        return false;
    }
}
