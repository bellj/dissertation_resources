package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.whatsapp.components.RoundCornerProgressBar;
import com.whatsapp.components.RoundCornerProgressBarV2;

/* renamed from: X.2aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC52492aw extends View implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC52492aw(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v4, types: [com.whatsapp.components.RoundCornerProgressBar] */
    public void A00() {
        AnonymousClass01J A00;
        RoundCornerProgressBarV2 roundCornerProgressBarV2;
        if (this instanceof RoundCornerProgressBarV2) {
            RoundCornerProgressBarV2 roundCornerProgressBarV22 = (RoundCornerProgressBarV2) this;
            if (!roundCornerProgressBarV22.A01) {
                roundCornerProgressBarV22.A01 = true;
                A00 = AnonymousClass2P6.A00(roundCornerProgressBarV22.generatedComponent());
                roundCornerProgressBarV2 = roundCornerProgressBarV22;
            } else {
                return;
            }
        } else if (!this.A01) {
            this.A01 = true;
            A00 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            roundCornerProgressBarV2 = (RoundCornerProgressBar) this;
        } else {
            return;
        }
        roundCornerProgressBarV2.A05 = C12960it.A0R(A00);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
