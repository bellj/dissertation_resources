package X;

import android.content.Context;
import com.whatsapp.search.SearchGridLayoutManager;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.3iX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74703iX extends AbstractC05290Oz {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ SearchGridLayoutManager A01;

    public C74703iX(Context context, SearchGridLayoutManager searchGridLayoutManager) {
        this.A01 = searchGridLayoutManager;
        this.A00 = context;
    }

    @Override // X.AbstractC05290Oz
    public int A00(int i) {
        SearchGridLayoutManager searchGridLayoutManager = this.A01;
        int i2 = this.A00.getResources().getConfiguration().orientation;
        int itemViewType = searchGridLayoutManager.A00.getItemViewType(i);
        if (itemViewType == -1 || itemViewType == 99 || itemViewType == 1 || itemViewType == 2 || itemViewType == 3 || itemViewType == 4) {
            return 6;
        }
        switch (itemViewType) {
            case 6:
            case 7:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
            case 15:
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
            case 17:
            case 18:
            case 19:
            case C43951xu.A01:
            case 21:
            case 22:
            case 23:
                return 6;
            case 8:
                if (i2 == 1) {
                    return 3;
                }
                break;
            case 9:
            case 10:
                if (i2 != 1) {
                    return 1;
                }
                break;
            default:
                throw C12980iv.A0u(C12960it.A0W(itemViewType, "Invalid viewType: "));
        }
        return 2;
    }
}
