package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3pC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78613pC extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98654j2();
    public C78623pD A00;
    public boolean A01;
    public byte[] A02;
    public int[] A03;
    public int[] A04;
    public C78643pF[] A05;
    public String[] A06;
    public byte[][] A07;
    public final C79493qg A08;

    public C78613pC(C78623pD r2, byte[] bArr, int[] iArr, int[] iArr2, C78643pF[] r6, String[] strArr, byte[][] bArr2, boolean z) {
        this.A00 = r2;
        this.A02 = bArr;
        this.A03 = iArr;
        this.A06 = strArr;
        this.A08 = null;
        this.A04 = iArr2;
        this.A07 = bArr2;
        this.A05 = r6;
        this.A01 = z;
    }

    public C78613pC(C79493qg r2, C78623pD r3, boolean z) {
        this.A00 = r3;
        this.A08 = r2;
        this.A03 = null;
        this.A06 = null;
        this.A04 = null;
        this.A07 = null;
        this.A05 = null;
        this.A01 = z;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C78613pC) {
                C78613pC r5 = (C78613pC) obj;
                if (!C13300jT.A00(this.A00, r5.A00) || !Arrays.equals(this.A02, r5.A02) || !Arrays.equals(this.A03, r5.A03) || !Arrays.equals(this.A06, r5.A06) || !C13300jT.A00(this.A08, r5.A08) || !Arrays.equals(this.A04, r5.A04) || !Arrays.deepEquals(this.A07, r5.A07) || !Arrays.equals(this.A05, r5.A05) || this.A01 != r5.A01) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        Object[] objArr = new Object[11];
        objArr[0] = this.A00;
        objArr[1] = this.A02;
        objArr[2] = this.A03;
        objArr[3] = this.A06;
        objArr[4] = this.A08;
        objArr[5] = null;
        objArr[6] = null;
        objArr[7] = this.A04;
        objArr[8] = this.A07;
        objArr[9] = this.A05;
        return C12980iv.A0B(Boolean.valueOf(this.A01), objArr, 10);
    }

    @Override // java.lang.Object
    public final String toString() {
        String str;
        StringBuilder A0k = C12960it.A0k("LogEventParcelable[");
        A0k.append(this.A00);
        A0k.append(", LogEventBytes: ");
        byte[] bArr = this.A02;
        if (bArr == null) {
            str = null;
        } else {
            str = new String(bArr);
        }
        A0k.append(str);
        A0k.append(", TestCodes: ");
        A0k.append(Arrays.toString(this.A03));
        A0k.append(", MendelPackages: ");
        A0k.append(Arrays.toString(this.A06));
        A0k.append(", LogEvent: ");
        A0k.append(this.A08);
        A0k.append(", ExtensionProducer: ");
        A0k.append((Object) null);
        A0k.append(", VeProducer: ");
        A0k.append((Object) null);
        A0k.append(", ExperimentIDs: ");
        A0k.append(Arrays.toString(this.A04));
        A0k.append(", ExperimentTokens: ");
        A0k.append(Arrays.toString(this.A07));
        A0k.append(", ExperimentTokensParcelables: ");
        A0k.append(Arrays.toString(this.A05));
        A0k.append(", AddPhenotypeExperimentTokens: ");
        A0k.append(this.A01);
        return C12960it.A0d("]", A0k);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0B(parcel, this.A00, 2, i, false);
        C95654e8.A0G(parcel, this.A02, 3, false);
        C95654e8.A0H(parcel, this.A03, 4);
        String[] strArr = this.A06;
        if (strArr != null) {
            int A02 = C95654e8.A02(parcel, 5);
            parcel.writeStringArray(strArr);
            C95654e8.A06(parcel, A02);
        }
        C95654e8.A0H(parcel, this.A04, 6);
        C95654e8.A0J(parcel, this.A07, 7);
        C95654e8.A09(parcel, 8, this.A01);
        C95654e8.A0I(parcel, this.A05, 9, i);
        C95654e8.A06(parcel, A00);
    }
}
