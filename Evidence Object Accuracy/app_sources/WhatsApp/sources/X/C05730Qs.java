package X;

import android.view.ContentInfo;
import android.view.View;

/* renamed from: X.0Qs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05730Qs {
    public static AnonymousClass0SR A00(View view, AnonymousClass0SR r3) {
        ContentInfo A01 = r3.A01();
        ContentInfo performReceiveContent = view.performReceiveContent(A01);
        if (performReceiveContent == null) {
            return null;
        }
        if (performReceiveContent == A01) {
            return r3;
        }
        return AnonymousClass0SR.A00(performReceiveContent);
    }

    public static String[] A01(View view) {
        return view.getReceiveContentMimeTypes();
    }
}
