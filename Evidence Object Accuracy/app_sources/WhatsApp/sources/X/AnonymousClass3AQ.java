package X;

import android.location.Location;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3AQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AQ {
    public static void A00(C48122Ek r8, Double d, List list) {
        double distanceTo;
        double doubleValue;
        Location location = new Location("");
        location.setLatitude(r8.A03.doubleValue());
        location.setLongitude(r8.A04.doubleValue());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass3M8 r5 = (AnonymousClass3M8) it.next();
            if (!r5.A01()) {
                distanceTo = 0.0d;
            } else {
                Location location2 = new Location("");
                location2.setLatitude(r5.A04.doubleValue());
                location2.setLongitude(r5.A05.doubleValue());
                distanceTo = (double) location.distanceTo(location2);
            }
            r5.A00 = distanceTo;
            if (d == null) {
                doubleValue = 1.0d;
            } else {
                doubleValue = d.doubleValue();
            }
            r5.A01 = (distanceTo * doubleValue) + r5.A03.doubleValue();
        }
        C12980iv.A1S(list, 11);
    }
}
