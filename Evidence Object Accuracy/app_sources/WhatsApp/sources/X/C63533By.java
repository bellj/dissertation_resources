package X;

import android.content.Context;
import java.util.Map;

/* renamed from: X.3By  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63533By {
    public Map A00;
    public final Context A01;
    public final AbstractC15710nm A02;
    public final C33241dg A03;
    public final AnonymousClass5TI A04;
    public final C22730zY A05;
    public final C26551Dx A06;
    public final AbstractC44761zV A07;
    public final C44791zY A08;
    public final C15810nw A09;
    public final C15890o4 A0A;
    public final C14820m6 A0B;
    public final C15880o3 A0C;
    public final C20850wQ A0D;
    public final AbstractC15850o0 A0E;
    public final String A0F;
    public final String A0G;

    public C63533By(Context context, AbstractC15710nm r3, AnonymousClass5TI r4, C22730zY r5, C26551Dx r6, AbstractC44761zV r7, C44791zY r8, C15810nw r9, C15890o4 r10, C14820m6 r11, C15880o3 r12, C20850wQ r13, C21630xj r14, AbstractC15850o0 r15, String str) {
        this.A0G = str;
        this.A0F = r8.A0E;
        this.A04 = r4;
        this.A02 = r3;
        this.A09 = r9;
        this.A07 = r7;
        this.A08 = r8;
        this.A0E = r15;
        this.A06 = r6;
        this.A01 = context;
        this.A0C = r12;
        this.A0A = r10;
        this.A0B = r11;
        this.A05 = r5;
        this.A0D = r13;
        this.A03 = new C33241dg(r14);
    }
}
