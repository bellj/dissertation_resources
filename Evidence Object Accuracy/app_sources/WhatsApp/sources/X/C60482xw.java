package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0110000_I1;
import com.whatsapp.R;

/* renamed from: X.2xw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60482xw extends AnonymousClass1OY {
    public AnonymousClass11L A00;
    public AnonymousClass11J A01;
    public boolean A02;
    public final TextView A03;

    public C60482xw(Context context, AbstractC13890kV r3, AnonymousClass1XA r4) {
        super(context, r3, r4);
        A0Z();
        setClickable(false);
        setLongClickable(false);
        TextView A0I = C12960it.A0I(this, R.id.info);
        this.A03 = A0I;
        AnonymousClass1OY.A0D(context, this, A0I);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A02) {
            this.A02 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A00 = (AnonymousClass11L) A08.ALG.get();
            this.A01 = (AnonymousClass11J) A08.A3m.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    public void A1M() {
        AbstractC14640lm r2;
        AnonymousClass1XA r3 = (AnonymousClass1XA) ((AbstractC28551Oa) this).A0O;
        C15570nT r22 = ((AnonymousClass1OY) this).A0L;
        if (((AbstractC15340mz) r3).A00 == 2) {
            r2 = r3.A0z.A00;
        } else {
            r22.A08();
            r2 = r22.A05;
        }
        AnonymousClass009.A05(r2);
        ((AnonymousClass1OY) this).A0L.A08();
        String A03 = this.A00.A03(r2, r3.A00);
        Drawable A06 = AnonymousClass1OY.A06(this);
        TextView textView = this.A03;
        textView.setText(C52252aV.A01(textView.getPaint(), A06, A03));
        textView.setOnClickListener(new ViewOnClickCListenerShape1S0110000_I1(this));
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public AnonymousClass1XA getFMessage() {
        return (AnonymousClass1XA) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XA);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
