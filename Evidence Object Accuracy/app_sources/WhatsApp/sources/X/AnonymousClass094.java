package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.094  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass094 extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass072 A00;

    public AnonymousClass094(AnonymousClass072 r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.A0D();
        animator.removeListener(this);
    }
}
