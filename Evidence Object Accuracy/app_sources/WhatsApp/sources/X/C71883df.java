package X;

import android.app.Activity;
import android.os.Bundle;
import com.whatsapp.calling.callrating.CallRatingActivityV2;
import com.whatsapp.calling.callrating.CallRatingBottomSheet;

/* renamed from: X.3df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71883df extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CallRatingActivityV2 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71883df(CallRatingActivityV2 callRatingActivityV2) {
        super(0);
        this.this$0 = callRatingActivityV2;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        Bundle A0H = C12990iw.A0H(this.this$0);
        CallRatingBottomSheet callRatingBottomSheet = new CallRatingBottomSheet(new AnonymousClass1WK(this.this$0) { // from class: X.3dZ
            @Override // X.AnonymousClass1WK
            public /* bridge */ /* synthetic */ Object AJ3() {
                ((Activity) this.receiver).finish();
                return AnonymousClass1WZ.A00;
            }
        });
        if (A0H != null) {
            callRatingBottomSheet.A0U(A0H);
        }
        return callRatingBottomSheet;
    }
}
