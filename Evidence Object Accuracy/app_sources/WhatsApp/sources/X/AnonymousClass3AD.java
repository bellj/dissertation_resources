package X;

import java.util.List;

/* renamed from: X.3AD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AD {
    public static AnonymousClass28D A00(AbstractC116415Vi r11, AnonymousClass28D r12) {
        AnonymousClass28D A62 = r11.A62(r12);
        boolean A1W = C12960it.A1W(A62.A02.get(135));
        int[] A03 = C65093Ic.A03(A62);
        for (int i = 0; i < A03.length; i++) {
            AnonymousClass28D A0F = A62.A0F(A03[i]);
            if (A0F != null) {
                AnonymousClass28D A00 = A00(r11, A0F);
                if (A00 != A0F) {
                    if (A62 == r12) {
                        A62 = new AnonymousClass28D(r12, null, r12.A06, r12.A00);
                    }
                    A62.A02.put(A03[i], A00);
                }
                A1W |= A00.A0O(148, true);
            }
        }
        int[] A02 = C65093Ic.A02(A62);
        for (int i2 = 0; i2 < A02.length; i2++) {
            List A0L = A62.A0L(A02[i2]);
            List list = A0L;
            for (int i3 = 0; i3 < A0L.size(); i3++) {
                AnonymousClass28D A0U = C12980iv.A0U(A0L, i3);
                if (A0U != null) {
                    AnonymousClass28D A002 = A00(r11, A0U);
                    if (A002 != A0U) {
                        if (list == A0L) {
                            list = C12980iv.A0x(A0L);
                        }
                        list.set(i3, A002);
                    }
                    A1W |= A002.A0O(148, true);
                }
            }
            if (list != A0L) {
                if (A62 == r12) {
                    A62 = new AnonymousClass28D(r12, null, r12.A06, r12.A00);
                }
                A62.A02.put(A02[i2], list);
            }
        }
        if (A62 != r12) {
            A62.A02.put(148, Boolean.valueOf(A1W));
        }
        r11.AY4(A62);
        return A62;
    }
}
