package X;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;

/* renamed from: X.1vd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC42671vd extends AbstractC42681ve {
    public C64343Fe A00;
    public C15890o4 A01;
    public C26171Ch A02;
    public C17990rj A03;
    public AnonymousClass109 A04;
    public C22370yy A05;
    public C26161Cg A06;
    public final AbstractView$OnClickListenerC34281fs A07 = new ViewOnClickCListenerShape14S0100000_I0_1(this, 16);
    public final AbstractView$OnClickListenerC34281fs A08 = new ViewOnClickCListenerShape14S0100000_I0_1(this, 14);
    public final AbstractView$OnClickListenerC34281fs A09 = new ViewOnClickCListenerShape14S0100000_I0_1(this, 15);
    public final AbstractView$OnClickListenerC34281fs A0A = new ViewOnClickCListenerShape14S0100000_I0_1(this, 17);
    public final AbstractC35401hl A0B;

    @Override // X.AnonymousClass1OY
    public abstract void A0y();

    public AbstractC42671vd(Context context, AbstractC13890kV r4, AbstractC16130oV r5) {
        super(context, r4, r5);
        this.A0B = C65213Iq.A00(context);
    }

    public static String A0Y(AbstractC15340mz r2) {
        StringBuilder sb = new StringBuilder("date-transition-");
        sb.append(r2.A0z);
        return sb.toString();
    }

    public static String A0Z(String str) {
        StringBuilder sb = new StringBuilder("thumb-transition-");
        sb.append(str);
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x010e, code lost:
        if (r25 != false) goto L_0x007d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0a(android.view.View r20, android.view.View r21, android.view.View r22, android.widget.ImageView r23, boolean r24, boolean r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 284
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC42671vd.A0a(android.view.View, android.view.View, android.view.View, android.widget.ImageView, boolean, boolean, boolean):void");
    }

    @Override // X.AnonymousClass1OY
    public int A0m(int i) {
        if (!TextUtils.isEmpty(getFMessage().A15()) || (!(this instanceof C60852yk) && !(this instanceof C60622yK) && !(this instanceof C60782yd) && !(this instanceof C47412Ap))) {
            return super.A0m(i);
        }
        if (C37381mH.A00(i, 13) >= 0) {
            return R.drawable.message_got_read_receipt_from_target_onmedia;
        }
        if (C37381mH.A00(i, 5) >= 0) {
            return R.drawable.message_got_receipt_from_target_onmedia;
        }
        if (i == 4) {
            return R.drawable.message_got_receipt_from_server_onmedia;
        }
        return R.drawable.message_unsent_onmedia;
    }

    public void A1M(View view, TextEmojiLabel textEmojiLabel, String str) {
        if (textEmojiLabel != null) {
            AbstractC16130oV fMessage = getFMessage();
            Resources resources = getResources();
            if (!TextUtils.isEmpty(str)) {
                textEmojiLabel.setVisibility(0);
                setMessageText(str, textEmojiLabel, fMessage);
                ((AnonymousClass1OY) this).A0E.setTextColor(getSecondaryTextColor());
                ((AnonymousClass1OY) this).A05.setPadding(resources.getDimensionPixelSize(R.dimen.conversation_image_date_padding_right), 0, resources.getDimensionPixelSize(R.dimen.conversation_image_date_padding_right), resources.getDimensionPixelSize(R.dimen.conversation_image_date_padding_bottom));
                ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin = 0;
                return;
            }
            textEmojiLabel.setVisibility(8);
            ((AnonymousClass1OY) this).A0E.setTextColor(AnonymousClass00T.A00(getContext(), R.color.conversation_row_image_text));
            ViewGroup viewGroup = ((AnonymousClass1OY) this).A05;
            viewGroup.setPadding(resources.getDimensionPixelSize(R.dimen.conversation_image_date_padding_right_on_media), 0, resources.getDimensionPixelSize(R.dimen.conversation_image_date_padding_right_on_media), 0);
            viewGroup.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            if (view != null) {
                ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin = (-viewGroup.getMeasuredHeight()) - getResources().getDimensionPixelSize(R.dimen.conversation_image_date_padding_bottom_on_media);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0055, code lost:
        if (r7.A04 == false) goto L_0x0057;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1N(X.AbstractC15340mz r17) {
        /*
            r16 = this;
            r6 = r17
            X.1c2 r0 = r6.A0N
            r5 = r16
            if (r0 == 0) goto L_0x006b
            android.view.ViewGroup r3 = r5.getExternalAdContentHolder()
            if (r3 == 0) goto L_0x006a
            X.3IJ r7 = X.AnonymousClass3IJ.A00(r6)
            r0 = 0
            r3.setVisibility(r0)
            X.3Fe r0 = r5.A00
            if (r0 != 0) goto L_0x004c
            android.content.Context r9 = r5.getContext()
            X.18U r10 = r5.A0K
            X.1BK r14 = r5.A0t
            X.19O r15 = r5.A1O
            X.0zP r11 = r5.A0W
            X.0mE r4 = r5.A0J
            X.0lR r2 = r5.A1P
            X.0t3 r1 = r5.A0O
            X.14x r0 = r5.A0q
            X.4Sf r12 = new X.4Sf
            r12.<init>(r4, r1, r0, r2)
            X.0rj r13 = r5.A03
            X.3Fe r8 = new X.3Fe
            r8.<init>(r9, r10, r11, r12, r13, r14, r15)
            r5.A00 = r8
            com.whatsapp.webpagepreview.WebPagePreviewView r2 = r8.A0A
            r1 = -1
            r0 = -2
            r3.addView(r2, r1, r0)
            X.3Fe r0 = r5.A00
            com.whatsapp.webpagepreview.WebPagePreviewView r1 = r0.A0A
            android.view.View$OnLongClickListener r0 = r5.A1a
            r1.setOnLongClickListener(r0)
        L_0x004c:
            X.1hl r8 = r5.A0B
            boolean r0 = r8 instanceof X.AnonymousClass2I3
            if (r0 == 0) goto L_0x0057
            boolean r0 = r7.A04
            r9 = 1
            if (r0 != 0) goto L_0x0058
        L_0x0057:
            r9 = 0
        L_0x0058:
            X.3Fe r4 = r5.A00
            X.1Ch r0 = r5.A02
            boolean r10 = r0.A00(r6)
            X.1Ch r0 = r5.A02
            boolean r11 = r0.A01(r6)
            r12 = 0
            r4.A02(r5, r6, r7, r8, r9, r10, r11, r12)
        L_0x006a:
            return
        L_0x006b:
            android.view.ViewGroup r1 = r5.getExternalAdContentHolder()
            if (r1 == 0) goto L_0x006a
            X.3Fe r0 = r5.A00
            if (r0 == 0) goto L_0x007d
            com.whatsapp.webpagepreview.WebPagePreviewView r0 = r0.A0A
            r1.removeView(r0)
            r0 = 0
            r5.A00 = r0
        L_0x007d:
            r0 = 8
            r1.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC42671vd.A1N(X.0mz):void");
    }

    public boolean A1O() {
        C16150oX r1;
        AbstractC16130oV fMessage = getFMessage();
        AnonymousClass19O r2 = this.A1O;
        if (fMessage.A0z.A02 || (r1 = fMessage.A02) == null) {
            return false;
        }
        r1.A0X = true;
        r2.A0D(fMessage);
        A0s();
        return true;
    }

    public ViewGroup getExternalAdContentHolder() {
        return (ViewGroup) findViewById(R.id.web_page_preview_holder);
    }

    @Override // X.AbstractC28551Oa
    public AbstractC16130oV getFMessage() {
        return (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
    }

    public int getVideoOriginForFieldstats() {
        AbstractC13890kV r0 = ((AbstractC28551Oa) this).A0a;
        if (r0 != null) {
            int ABe = r0.ABe();
            if (ABe != 0) {
                if (ABe == 1) {
                    return 3;
                }
                if (ABe != 2) {
                }
            }
            return 1;
        }
        return 5;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AbstractC16130oV);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
