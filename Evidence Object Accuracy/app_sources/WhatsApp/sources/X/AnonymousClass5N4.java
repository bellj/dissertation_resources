package X;

import java.util.Enumeration;

/* renamed from: X.5N4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N4 extends AnonymousClass1TM implements AnonymousClass1TJ {
    public AnonymousClass5NG A00;
    public AnonymousClass5NV A01;
    public AnonymousClass5NV A02;
    public AnonymousClass5NV A03;
    public AnonymousClass5NV A04;
    public AnonymousClass5N3 A05;

    public AnonymousClass5N4(AnonymousClass5NG r2, AnonymousClass5NV r3, AnonymousClass5NV r4, AnonymousClass5NV r5, AnonymousClass5N3 r6) {
        this.A00 = r2;
        this.A03 = r3;
        this.A05 = r6;
        this.A01 = r4;
        this.A02 = null;
        this.A04 = r5;
    }

    public AnonymousClass5N4(AbstractC114775Na r5) {
        Enumeration A0C = r5.A0C();
        this.A00 = (AnonymousClass5NG) A0C.nextElement();
        this.A03 = (AnonymousClass5NV) A0C.nextElement();
        Object nextElement = A0C.nextElement();
        this.A05 = nextElement instanceof AnonymousClass5N3 ? (AnonymousClass5N3) nextElement : nextElement != null ? new AnonymousClass5N3(AbstractC114775Na.A04(nextElement)) : null;
        while (A0C.hasMoreElements()) {
            AnonymousClass1TL r2 = (AnonymousClass1TL) A0C.nextElement();
            if (r2 instanceof AnonymousClass5NU) {
                AnonymousClass5NU r22 = (AnonymousClass5NU) r2;
                int i = r22.A00;
                if (i == 0) {
                    this.A01 = AnonymousClass5NV.A01(r22);
                } else if (i == 1) {
                    this.A02 = AnonymousClass5NV.A01(r22);
                } else {
                    throw C12970iu.A0f(C12960it.A0W(i, "unknown tag value "));
                }
            } else {
                this.A04 = (AnonymousClass5NV) r2;
            }
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(6);
        r3.A06(this.A00);
        r3.A06(this.A03);
        r3.A06(this.A05);
        AnonymousClass5NV r0 = this.A01;
        if (r0 != null) {
            C94954co.A03(r0, r3, false);
        }
        AnonymousClass5NV r1 = this.A02;
        if (r1 != null) {
            C94954co.A02(r1, r3, 1, false);
        }
        r3.A06(this.A04);
        return new AnonymousClass5NX(r3);
    }
}
