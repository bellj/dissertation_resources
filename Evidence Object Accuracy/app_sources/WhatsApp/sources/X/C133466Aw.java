package X;

/* renamed from: X.6Aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133466Aw implements AnonymousClass6MV {
    public final /* synthetic */ C129115xC A00;
    public final /* synthetic */ C123595nP A01;
    public final /* synthetic */ AnonymousClass1V8 A02;
    public final /* synthetic */ String A03;

    public C133466Aw(C129115xC r1, C123595nP r2, AnonymousClass1V8 r3, String str) {
        this.A01 = r2;
        this.A03 = str;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        this.A00.A00(r2);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r5) {
        C123595nP r3 = this.A01;
        String str = this.A03;
        r3.A08(r5, this.A00, this.A02, str);
    }
}
