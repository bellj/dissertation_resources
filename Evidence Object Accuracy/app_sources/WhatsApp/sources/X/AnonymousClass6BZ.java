package X;

import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;

/* renamed from: X.6BZ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6BZ implements AnonymousClass2S1 {
    public final /* synthetic */ IDxRCallbackShape0S0200000_3_I1 A00;
    public final /* synthetic */ String A01;

    public AnonymousClass6BZ(IDxRCallbackShape0S0200000_3_I1 iDxRCallbackShape0S0200000_3_I1, String str) {
        this.A00 = iDxRCallbackShape0S0200000_3_I1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass2S1
    public void APk() {
        ((AbstractC1311361k) this.A00.A01).AVJ(this.A01);
    }

    @Override // X.AnonymousClass2S1
    public void AX0(C50932Rx r3) {
        ((AbstractC1311361k) this.A00.A01).AVJ(this.A01);
    }
}
