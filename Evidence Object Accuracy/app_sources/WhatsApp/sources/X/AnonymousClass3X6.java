package X;

import android.graphics.Bitmap;
import android.net.Uri;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3X6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X6 implements AnonymousClass23D {
    public final /* synthetic */ AbstractC35611iN A00;
    public final /* synthetic */ AnonymousClass2TW A01;
    public final /* synthetic */ AnonymousClass2T3 A02;
    public final /* synthetic */ C54902hT A03;

    public AnonymousClass3X6(AbstractC35611iN r1, AnonymousClass2TW r2, AnonymousClass2T3 r3, C54902hT r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        Uri AAE = this.A00.AAE();
        StringBuilder A0h = C12960it.A0h();
        A0h.append(AAE);
        return C12960it.A0d("-gallery_thumb", A0h);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        if (this.A02.getTag() != this) {
            return null;
        }
        C54902hT r2 = this.A03;
        int i = r2.A06;
        if (i == -1) {
            i = r2.A05;
        }
        if (i == -1) {
            return null;
        }
        Bitmap Aem = this.A00.Aem(this.A01.A05.A03);
        return Aem == null ? MediaGalleryFragmentBase.A0U : Aem;
    }
}
