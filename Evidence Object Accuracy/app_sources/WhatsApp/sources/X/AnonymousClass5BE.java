package X;

import android.util.Log;
import java.io.Closeable;
import java.nio.ByteBuffer;

/* renamed from: X.5BE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5BE implements Closeable, AnonymousClass5XR {
    public ByteBuffer A00;
    public final int A01;
    public final long A02 = ((long) System.identityHashCode(this));

    public AnonymousClass5BE(int i) {
        this.A00 = ByteBuffer.allocateDirect(i);
        this.A01 = i;
    }

    public final void A00(AnonymousClass5XR r4, int i) {
        if (r4 instanceof AnonymousClass5BE) {
            AnonymousClass0RA.A01(!isClosed());
            AnonymousClass0RA.A01(!r4.isClosed());
            C87794Cz.A00(0, r4.AGm(), 0, i, this.A01);
            this.A00.position(0);
            r4.AB3().position(0);
            byte[] bArr = new byte[i];
            this.A00.get(bArr, 0, i);
            r4.AB3().put(bArr, 0, i);
            return;
        }
        throw C12970iu.A0f("Cannot copy two incompatible MemoryChunks");
    }

    @Override // X.AnonymousClass5XR
    public void A7m(AnonymousClass5XR r9, int i, int i2, int i3) {
        long AHP = r9.AHP();
        long j = this.A02;
        if (AHP == j) {
            StringBuilder A0k = C12960it.A0k("Copying from BufferMemoryChunk ");
            A0k.append(Long.toHexString(j));
            A0k.append(" to BufferMemoryChunk ");
            A0k.append(Long.toHexString(AHP));
            Log.w("BufferMemoryChunk", C12960it.A0d(" which are the same ", A0k));
            AnonymousClass0RA.A00(false);
        }
        if (AHP < j) {
            synchronized (r9) {
                synchronized (this) {
                    A00(r9, i3);
                }
            }
            return;
        }
        synchronized (this) {
            synchronized (r9) {
                A00(r9, i3);
            }
        }
    }

    @Override // X.AnonymousClass5XR
    public synchronized ByteBuffer AB3() {
        return this.A00;
    }

    @Override // X.AnonymousClass5XR
    public int AGm() {
        return this.A01;
    }

    @Override // X.AnonymousClass5XR
    public long AHP() {
        return this.A02;
    }

    @Override // X.AnonymousClass5XR
    public synchronized byte AZm(int i) {
        boolean z = true;
        AnonymousClass0RA.A01(C12960it.A1T(isClosed() ? 1 : 0));
        AnonymousClass0RA.A00(C12990iw.A1W(i));
        if (i >= this.A01) {
            z = false;
        }
        AnonymousClass0RA.A00(z);
        return this.A00.get(i);
    }

    @Override // X.AnonymousClass5XR
    public synchronized int AZr(byte[] bArr, int i, int i2, int i3) {
        int min;
        AnonymousClass0RA.A01(C12960it.A1T(isClosed() ? 1 : 0));
        int i4 = this.A01;
        min = Math.min(Math.max(0, i4 - i), i3);
        C87794Cz.A00(i, bArr.length, i2, min, i4);
        this.A00.position(i);
        this.A00.get(bArr, i2, min);
        return min;
    }

    @Override // X.AnonymousClass5XR
    public synchronized int AgC(byte[] bArr, int i, int i2, int i3) {
        int min;
        AnonymousClass0RA.A01(C12960it.A1T(isClosed() ? 1 : 0));
        int i4 = this.A01;
        min = Math.min(Math.max(0, i4 - i), i3);
        C87794Cz.A00(i, bArr.length, i2, min, i4);
        this.A00.position(i);
        this.A00.put(bArr, i2, min);
        return min;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable, X.AnonymousClass5XR
    public synchronized void close() {
        this.A00 = null;
    }

    @Override // X.AnonymousClass5XR
    public synchronized boolean isClosed() {
        return C12980iv.A1X(this.A00);
    }
}
