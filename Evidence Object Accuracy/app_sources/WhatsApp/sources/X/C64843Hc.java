package X;

import android.graphics.Bitmap;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.3Hc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64843Hc {
    public static final Bitmap A05 = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
    public C39041pA A00;
    public final long A01;
    public final C006202y A02;
    public final File A03;
    public final Object A04 = C12970iu.A0l();

    public C64843Hc(File file, long j) {
        this.A03 = file;
        this.A01 = j;
        this.A02 = new C74243he(this, (int) (AnonymousClass01V.A00 / 8192));
    }

    public Bitmap A00(String str, int i, int i2) {
        C39051pB r0;
        Throwable th;
        A01();
        synchronized (this.A04) {
            C39041pA r02 = this.A00;
            Bitmap bitmap = null;
            if (r02 != null) {
                try {
                    r0 = r02.A08(str);
                } catch (IOException unused) {
                    Log.e("bitmapcache/journal corrupted");
                    r0 = null;
                }
                if (r0 != null) {
                    try {
                        InputStream inputStream = r0.A00[0];
                        if (inputStream != null) {
                            try {
                                Bitmap bitmap2 = C37501mV.A05(new C41591tm(i, i2), inputStream).A02;
                                if (bitmap2 == null) {
                                    try {
                                        Log.e("bitmapcache/decode failed");
                                        try {
                                            inputStream.close();
                                            return null;
                                        } catch (IOException unused2) {
                                            bitmap = bitmap2;
                                        }
                                    } catch (Throwable th2) {
                                        th = th2;
                                        try {
                                            inputStream.close();
                                        } catch (Throwable unused3) {
                                        }
                                        throw th;
                                    }
                                } else {
                                    bitmap = bitmap2;
                                }
                            } catch (Throwable th3) {
                                th = th3;
                            }
                        }
                        if (inputStream != null) {
                            inputStream.close();
                        }
                    } catch (IOException unused4) {
                    }
                }
            }
            return bitmap;
        }
    }

    public final void A01() {
        synchronized (this.A04) {
            C39041pA r0 = this.A00;
            if (r0 == null || r0.A03 == null) {
                File file = this.A03;
                if (!file.exists() && !file.mkdirs() && !file.exists()) {
                    Log.e(C12960it.A0Z(file, "bitmapcache/initDiskCache: unable to create cache dir ", C12960it.A0h()));
                }
                long usableSpace = file.getUsableSpace();
                long j = this.A01;
                if (usableSpace > j) {
                    try {
                        this.A00 = C39041pA.A00(file, j);
                    } catch (IOException e) {
                        Log.e("bitmapcache/initDiskCache ", e);
                    }
                }
            }
        }
    }

    public void A02(InputStream inputStream, String str) {
        C39041pA r0;
        A01();
        synchronized (this.A04) {
            C39041pA r02 = this.A00;
            if (r02 != null) {
                try {
                    C39051pB A08 = r02.A08(str);
                    if (A08 == null) {
                        C39061pE A07 = this.A00.A07(str);
                        if (A07 != null) {
                            OutputStream A00 = A07.A00();
                            try {
                                C14350lI.A0G(inputStream, A00);
                                A07.A01();
                                A00.close();
                            } catch (Throwable th) {
                                try {
                                    A00.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        }
                    } else {
                        A08.A00[0].close();
                    }
                    r0 = this.A00;
                } catch (Exception e) {
                    Log.e("bitmapcache/download ", e);
                    r0 = this.A00;
                    synchronized (r0) {
                    }
                }
                synchronized (r0) {
                }
            }
        }
    }

    public void A03(boolean z) {
        C006202y r1 = this.A02;
        synchronized (r1) {
            r1.A06(-1);
        }
        synchronized (this.A04) {
            C39041pA r0 = this.A00;
            if (r0 != null) {
                if (z) {
                    try {
                        r0.close();
                        C39041pA.A04(r0.A07);
                    } catch (IOException e) {
                        Log.e("bitmapcache/close ", e);
                    }
                }
                C39041pA r12 = this.A00;
                if (r12.A03 != null) {
                    r12.close();
                }
                this.A00 = null;
            }
        }
    }
}
