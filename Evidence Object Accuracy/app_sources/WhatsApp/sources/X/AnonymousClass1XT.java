package X;

import android.text.TextUtils;

/* renamed from: X.1XT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XT extends C16440p1 implements AbstractC28871Pi, AbstractC16400ox, AbstractC16420oz {
    public C28891Pk A00;

    public AnonymousClass1XT(C16150oX r10, AnonymousClass1IS r11, AnonymousClass1XT r12, long j) {
        super(r10, r11, r12, r12.A0y, j, true);
        this.A00 = r12.A00.A00();
    }

    public AnonymousClass1XT(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 26, j);
    }

    @Override // X.AbstractC28871Pi
    public String ADA() {
        String str;
        if (!TextUtils.isEmpty(this.A00.A02)) {
            StringBuilder sb = new StringBuilder();
            C28891Pk r1 = this.A00;
            sb.append(r1.A01);
            sb.append(" ");
            sb.append(r1.A02);
            str = sb.toString();
        } else {
            str = this.A00.A01;
        }
        String A1B = A1B();
        if (TextUtils.isEmpty(A1B)) {
            return str;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(A1B);
        sb2.append(" ");
        sb2.append(str);
        return sb2.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AEh(AnonymousClass018 r3) {
        StringBuilder sb = new StringBuilder("📄 ");
        sb.append(this.A00.A01);
        return sb.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AFs() {
        return this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public String AG2() {
        return this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public C28891Pk AH7() {
        return this.A00;
    }

    @Override // X.AbstractC28871Pi
    public void Acz(C28891Pk r1) {
        this.A00 = r1;
    }
}
