package X;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import com.whatsapp.R;

/* renamed from: X.0GC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0GC extends AnonymousClass0G0 {
    public static final TimeInterpolator A01 = new AccelerateInterpolator();
    public static final TimeInterpolator A02 = new DecelerateInterpolator();
    public static final AbstractC12410hs A03 = new C03030Fv();
    public static final AbstractC12410hs A04 = new AnonymousClass0Ft();
    public static final AbstractC12410hs A05 = new C02990Fq();
    public static final AbstractC12410hs A06 = new C03010Fs();
    public static final AbstractC12410hs A07 = new C03000Fr();
    public static final AbstractC12410hs A08 = new C03020Fu();
    public AbstractC12410hs A00 = A03;

    public AnonymousClass0GC() {
        A0X(80);
    }

    public static Animator A04(TimeInterpolator timeInterpolator, View view, C05350Pf r17, float f, float f2, float f3, float f4, int i, int i2) {
        float f5 = f;
        float f6 = f2;
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        int[] iArr = (int[]) r17.A00.getTag(R.id.transition_position);
        if (iArr != null) {
            f5 = ((float) (iArr[0] - i)) + translationX;
            f6 = ((float) (iArr[1] - i2)) + translationY;
        }
        int round = i + Math.round(f5 - translationX);
        int round2 = i2 + Math.round(f6 - translationY);
        view.setTranslationX(f5);
        view.setTranslationY(f6);
        if (f5 == f3 && f6 == f4) {
            return null;
        }
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(view, PropertyValuesHolder.ofFloat(View.TRANSLATION_X, f5, f3), PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, f6, f4));
        AnonymousClass09P r8 = new AnonymousClass09P(view, r17.A00, translationX, translationY, round, round2);
        ofPropertyValuesHolder.addListener(r8);
        AnonymousClass0LE.A00(ofPropertyValuesHolder, r8);
        ofPropertyValuesHolder.setInterpolator(timeInterpolator);
        return ofPropertyValuesHolder;
    }

    @Override // X.AnonymousClass0G0, X.AnonymousClass072
    public void A0T(C05350Pf r4) {
        AnonymousClass0G0.A03(r4);
        int[] iArr = new int[2];
        r4.A00.getLocationOnScreen(iArr);
        r4.A02.put("android:slide:screenPosition", iArr);
    }

    @Override // X.AnonymousClass0G0, X.AnonymousClass072
    public void A0U(C05350Pf r4) {
        AnonymousClass0G0.A03(r4);
        int[] iArr = new int[2];
        r4.A00.getLocationOnScreen(iArr);
        r4.A02.put("android:slide:screenPosition", iArr);
    }

    @Override // X.AnonymousClass0G0
    public Animator A0V(View view, ViewGroup viewGroup, C05350Pf r13, C05350Pf r14) {
        int[] iArr = (int[]) r14.A02.get("android:slide:screenPosition");
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        return A04(A02, view, r14, this.A00.ADB(view, viewGroup), this.A00.ADC(view, viewGroup), translationX, translationY, iArr[0], iArr[1]);
    }

    @Override // X.AnonymousClass0G0
    public Animator A0W(View view, ViewGroup viewGroup, C05350Pf r13, C05350Pf r14) {
        if (r13 == null) {
            return null;
        }
        int[] iArr = (int[]) r13.A02.get("android:slide:screenPosition");
        return A04(A01, view, r13, view.getTranslationX(), view.getTranslationY(), this.A00.ADB(view, viewGroup), this.A00.ADC(view, viewGroup), iArr[0], iArr[1]);
    }

    public void A0X(int i) {
        AbstractC12410hs r0;
        if (i == 3) {
            r0 = A05;
        } else if (i == 5) {
            r0 = A06;
        } else if (i == 48) {
            r0 = A08;
        } else if (i == 80) {
            r0 = A03;
        } else if (i == 8388611) {
            r0 = A07;
        } else if (i == 8388613) {
            r0 = A04;
        } else {
            throw new IllegalArgumentException("Invalid slide direction");
        }
        this.A00 = r0;
        AnonymousClass0G6 r02 = new AnonymousClass0G6();
        r02.A00 = i;
        A0N(r02);
    }
}
