package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.31x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C617331x extends AbstractC51682Vy {
    public C66013Ly A00;
    public AbstractC16350or A01;
    public String A02;
    public final ViewGroup A03;
    public final ImageView A04;
    public final WaImageView A05;
    public final C16120oU A06;
    public final C253719d A07;
    public final AnonymousClass5UF A08;
    public final AnonymousClass3BO A09;

    public C617331x(ViewGroup viewGroup, AnonymousClass01d r7, C16120oU r8, C253719d r9, AnonymousClass5UF r10, C16630pM r11) {
        super(C12960it.A0E(viewGroup).inflate(R.layout.gif_search_preview, viewGroup, false));
        this.A07 = r9;
        this.A06 = r8;
        this.A08 = r10;
        View view = this.A0H;
        this.A04 = C12970iu.A0L(view, R.id.thumb_view);
        ViewGroup A0P = C12980iv.A0P(view, R.id.video_preview_container);
        this.A03 = A0P;
        if (AnonymousClass2BK.A01(r7, r11) >= 2012) {
            AnonymousClass3BO r0 = new AnonymousClass3BO(view.getContext());
            this.A09 = r0;
            A0P.addView(r0.A02, C12990iw.A0M());
            this.A05 = C12980iv.A0X(A0P, R.id.gif);
            A0P.setVisibility(0);
        }
    }
}
