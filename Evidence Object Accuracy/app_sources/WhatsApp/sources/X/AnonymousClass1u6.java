package X;

/* renamed from: X.1u6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1u6 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;

    public AnonymousClass1u6() {
        super(1034, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A01);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamForwardPicker {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "forwardPickerContactsSelected", this.A01);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "forwardPickerResult", obj);
        sb.append("}");
        return sb.toString();
    }
}
