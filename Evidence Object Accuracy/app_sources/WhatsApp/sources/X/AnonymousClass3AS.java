package X;

import android.content.DialogInterface;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3AS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AS {
    public static void A00(ActivityC000900k r5, C21740xu r6) {
        C004802e A0S = C12980iv.A0S(r5);
        View A0N = C12980iv.A0N(r5, R.layout.dialog_update_whatsapp);
        View A0D = AnonymousClass028.A0D(A0N, R.id.not_now_btn);
        C12960it.A13(AnonymousClass028.A0D(A0N, R.id.update_btn), r5, r6, 8);
        C12960it.A0z(A0D, r5, 8);
        A0S.A04(new DialogInterface.OnDismissListener() { // from class: X.4hB
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                ActivityC000900k.this.finish();
            }
        });
        A0S.setView(A0N);
        A0S.A05();
    }
}
