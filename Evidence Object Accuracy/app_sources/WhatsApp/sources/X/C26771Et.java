package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.1Et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26771Et {
    public final C16590pI A00;
    public final C16120oU A01;
    public final C26811Ex A02;
    public final C26781Eu A03;
    public final C26801Ew A04;
    public final C22190yg A05;

    public C26771Et(C16590pI r1, C16120oU r2, C26811Ex r3, C26781Eu r4, C26801Ew r5, C22190yg r6) {
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
    }

    public final File A00(File file, String str) {
        long j;
        if (file == null) {
            Log.w("mediaprocessmanager/gettranscodedfile/originalFile is null");
        }
        C22190yg r2 = this.A05;
        if (file != null) {
            j = file.length();
        } else {
            j = -1;
        }
        return r2.A0B(str, j);
    }
}
