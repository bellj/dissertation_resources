package X;

import android.content.SharedPreferences;

/* renamed from: X.1EB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EB {
    public SharedPreferences A00;
    public SharedPreferences A01;
    public final C14830m7 A02;
    public final C15650ng A03;
    public final C16630pM A04;

    public AnonymousClass1EB(C14830m7 r1, C15650ng r2, C16630pM r3) {
        this.A02 = r1;
        this.A03 = r2;
        this.A04 = r3;
    }

    public static final String A00(AbstractC14640lm r2, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(r2.getRawString());
        sb.append(",");
        sb.append(str);
        return sb.toString();
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A04.A01("msg_attribute_pref_file");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public final synchronized SharedPreferences A02() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A01;
        if (sharedPreferences == null) {
            sharedPreferences = this.A04.A01("in_app_msg_source_pref_file");
            this.A01 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
