package X;

import java.util.List;

/* renamed from: X.5zV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130495zV {
    public static List A01;
    public String A00 = "";

    public C130495zV(String str) {
        try {
            new AnonymousClass6LO(str);
            A01 = AnonymousClass6LO.A02;
        } catch (C124795q8 e) {
            throw e;
        } catch (Exception unused) {
            throw new C124795q8(EnumC124485pc.A04);
        }
    }

    public static String A00(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        StringBuilder sb = new StringBuilder(100);
        A01(sb, str2);
        A01(sb, str);
        A01(sb, str6);
        A01(sb, str7);
        A01(sb, str3);
        A01(sb, str5);
        if (str4 != null && !str4.isEmpty()) {
            sb.append(str4);
        }
        int lastIndexOf = sb.lastIndexOf("|");
        if (lastIndexOf != -1 && lastIndexOf == sb.length() - 1) {
            sb.deleteCharAt(lastIndexOf);
        }
        return sb.toString();
    }

    public static void A01(StringBuilder sb, String str) {
        if (str != null && !str.isEmpty()) {
            sb.append(str);
            sb.append("|");
        }
    }
}
