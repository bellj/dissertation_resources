package X;

import android.os.Handler;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;

/* renamed from: X.00b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ExecutorC000000b implements Executor {
    public final Handler A00;

    public ExecutorC000000b(Handler handler) {
        this.A00 = handler;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        Handler handler = this.A00;
        if (!handler.post(runnable)) {
            StringBuilder sb = new StringBuilder();
            sb.append(handler);
            sb.append(" is shutting down");
            throw new RejectedExecutionException(sb.toString());
        }
    }
}
