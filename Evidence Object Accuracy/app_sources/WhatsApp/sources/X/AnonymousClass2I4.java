package X;

import android.graphics.Bitmap;

/* renamed from: X.2I4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2I4 implements AbstractC35401hl {
    @Override // X.AbstractC35401hl
    public void A7N() {
    }

    @Override // X.AbstractC35401hl
    public void A9X() {
    }

    @Override // X.AbstractC35401hl
    public void A9m(boolean z) {
    }

    @Override // X.AbstractC35401hl
    public void A9r(AnonymousClass4NS r1, AnonymousClass1IS r2, String str, String str2, Bitmap[] bitmapArr, int i) {
    }

    @Override // X.AbstractC35401hl
    public int AC4() {
        return 0;
    }

    @Override // X.AbstractC35401hl
    public AnonymousClass1IS AC5() {
        return null;
    }

    @Override // X.AbstractC35401hl
    public boolean ADU() {
        return false;
    }

    @Override // X.AbstractC35401hl
    public boolean ADc() {
        return false;
    }

    @Override // X.AbstractC35401hl
    public void AYz() {
    }

    @Override // X.AbstractC35401hl
    public void Ac0(int i) {
    }

    @Override // X.AbstractC35401hl
    public void AcB(AnonymousClass4NS r1) {
    }

    @Override // X.AbstractC35401hl
    public void AcP(int i) {
    }

    @Override // X.AbstractC35401hl
    public void AeJ(C89114Is r1, ScaleGestureDetector$OnScaleGestureListenerC52942c4 r2) {
    }
}
