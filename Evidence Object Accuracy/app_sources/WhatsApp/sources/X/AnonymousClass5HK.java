package X;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/* renamed from: X.5HK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HK extends ThreadLocal {
    public final /* synthetic */ String A00;

    public AnonymousClass5HK(String str) {
        this.A00 = str;
    }

    @Override // java.lang.ThreadLocal
    public /* bridge */ /* synthetic */ Object initialValue() {
        return new DecimalFormat(this.A00, DecimalFormatSymbols.getInstance(Locale.US));
    }
}
