package X;

import java.util.ArrayList;

/* renamed from: X.0C3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C3 extends AnonymousClass05L {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass052 A01;
    public final /* synthetic */ AnonymousClass05K A02;
    public final /* synthetic */ String A03;

    public AnonymousClass0C3(AnonymousClass052 r1, AnonymousClass05K r2, String str, int i) {
        this.A01 = r1;
        this.A03 = str;
        this.A00 = i;
        this.A02 = r2;
    }

    @Override // X.AnonymousClass05L
    public void A00(C018108l r4, Object obj) {
        AnonymousClass052 r2 = this.A01;
        ArrayList arrayList = r2.A00;
        String str = this.A03;
        arrayList.add(str);
        Number number = (Number) r2.A04.get(str);
        r2.A04(this.A02, obj, number != null ? number.intValue() : this.A00);
    }
}
