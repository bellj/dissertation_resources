package X;

import com.facebook.redex.IDxNFunctionShape17S0100000_2_I1;

/* renamed from: X.3EI  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EI {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3HE A01;
    public final AnonymousClass3EM A02;

    public AnonymousClass3EI(AbstractC15710nm r5, AnonymousClass1V8 r6, AnonymousClass1V8 r7) {
        AnonymousClass1V8.A01(r6, "iq");
        IDxNFunctionShape17S0100000_2_I1 iDxNFunctionShape17S0100000_2_I1 = new IDxNFunctionShape17S0100000_2_I1(r5, 11);
        String[] A08 = C13000ix.A08();
        A08[0] = "fds";
        this.A01 = (AnonymousClass3HE) AnonymousClass3JT.A05(r6, iDxNFunctionShape17S0100000_2_I1, A08);
        this.A02 = (AnonymousClass3EM) AnonymousClass3JT.A05(r6, new AbstractC116095Uc(r5, r7) { // from class: X.58u
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r4) {
                return new AnonymousClass3EM(this.A00, r4, this.A01);
            }
        }, new String[0]);
        this.A00 = r6;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EI.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EI r5 = (AnonymousClass3EI) obj;
            if (!this.A01.equals(r5.A01) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A02, A1a);
    }
}
