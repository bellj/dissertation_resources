package X;

/* renamed from: X.5MV  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MV extends AnonymousClass1TM {
    public AnonymousClass1TK A00;
    public AnonymousClass5NH A01;

    public AnonymousClass5MV(AbstractC114775Na r2) {
        this.A00 = (AnonymousClass1TK) AbstractC114775Na.A00(r2);
        this.A01 = (AnonymousClass5NH) AbstractC114775Na.A01(r2);
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A00);
        return C94954co.A01(this.A01, A00);
    }
}
