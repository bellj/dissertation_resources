package X;

/* renamed from: X.5G1  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5G1 implements AnonymousClass20J {
    public int A00;
    public AnonymousClass5XI A01;
    public byte[] A02 = new byte[64];
    public byte[] A03 = new byte[64];

    public AnonymousClass5G1(AnonymousClass5XI r3) {
        this.A01 = r3;
        this.A00 = r3.ACZ();
    }

    @Override // X.AnonymousClass20J
    public int A97(byte[] bArr, int i) {
        int i2 = this.A00;
        byte[] bArr2 = new byte[i2];
        AnonymousClass5XI r3 = this.A01;
        r3.A97(bArr2, 0);
        byte[] bArr3 = this.A03;
        r3.update(bArr3, 0, bArr3.length);
        r3.update(bArr2, 0, i2);
        int A97 = r3.A97(bArr, i);
        reset();
        return A97;
    }

    @Override // X.AnonymousClass20J
    public int AE2() {
        return this.A00;
    }

    @Override // X.AnonymousClass20J
    public void AfG(byte b) {
        this.A01.AfG(b);
    }

    @Override // X.AnonymousClass20J
    public void reset() {
        AnonymousClass5XI r3 = this.A01;
        r3.reset();
        byte[] bArr = this.A02;
        r3.update(bArr, 0, bArr.length);
    }

    @Override // X.AnonymousClass20J
    public void update(byte[] bArr, int i, int i2) {
        this.A01.update(bArr, i, i2);
    }

    @Override // X.AnonymousClass20J
    public void AIc(AnonymousClass20L r8) {
        byte[] bArr;
        int i;
        AnonymousClass5XI r5 = this.A01;
        r5.reset();
        byte[] bArr2 = ((AnonymousClass20K) r8).A00;
        int length = bArr2.length;
        if (length <= 64) {
            bArr = this.A02;
            System.arraycopy(bArr2, 0, bArr, 0, length);
            while (true) {
                i = bArr.length;
                if (length >= i) {
                    break;
                }
                bArr[length] = 0;
                length++;
            }
        } else {
            r5.update(bArr2, 0, length);
            bArr = this.A02;
            r5.A97(bArr, 0);
            int i2 = this.A00;
            while (true) {
                i = bArr.length;
                if (i2 >= i) {
                    break;
                }
                bArr[i2] = 0;
                i2++;
            }
        }
        byte[] bArr3 = new byte[i];
        this.A03 = bArr3;
        System.arraycopy(bArr, 0, bArr3, 0, i);
        for (int i3 = 0; i3 < i; i3++) {
            bArr[i3] = (byte) (bArr[i3] ^ 54);
        }
        int i4 = 0;
        while (true) {
            byte[] bArr4 = this.A03;
            if (i4 < bArr4.length) {
                bArr4[i4] = (byte) (bArr4[i4] ^ 92);
                i4++;
            } else {
                r5.update(bArr, 0, i);
                return;
            }
        }
    }
}
