package X;

/* renamed from: X.4SP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4SP {
    public final int A00;
    public final AnonymousClass4XD A01;
    public final String A02;
    public final boolean A03;
    public final byte[] A04;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0046, code lost:
        if (r3.equals(r0) != false) goto L_0x0034;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0054  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4SP(java.lang.String r3, byte[] r4, byte[] r5, int r6, int r7, int r8, boolean r9) {
        /*
            r2 = this;
            r2.<init>()
            r1 = 1
            boolean r0 = X.C12960it.A1T(r6)
            if (r5 == 0) goto L_0x000b
            r1 = 0
        L_0x000b:
            r1 = r1 ^ r0
            X.C95314dV.A03(r1)
            r2.A03 = r9
            r2.A02 = r3
            r2.A00 = r6
            r2.A04 = r5
            if (r3 == 0) goto L_0x0034
            int r0 = r3.hashCode()
            switch(r0) {
                case 3046605: goto L_0x004c;
                case 3046671: goto L_0x0049;
                case 3049879: goto L_0x0040;
                case 3049895: goto L_0x003d;
                default: goto L_0x0020;
            }
        L_0x0020:
            java.lang.String r0 = "Unsupported protection scheme type '"
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r3)
            java.lang.String r0 = "'. Assuming AES-CTR crypto mode."
            java.lang.String r1 = X.C12960it.A0d(r0, r1)
            java.lang.String r0 = "TrackEncryptionBox"
            android.util.Log.w(r0, r1)
        L_0x0034:
            r1 = 1
        L_0x0035:
            X.4XD r0 = new X.4XD
            r0.<init>(r1, r4, r7, r8)
            r2.A01 = r0
            return
        L_0x003d:
            java.lang.String r0 = "cens"
            goto L_0x0042
        L_0x0040:
            java.lang.String r0 = "cenc"
        L_0x0042:
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x0034
            goto L_0x0020
        L_0x0049:
            java.lang.String r0 = "cbcs"
            goto L_0x004e
        L_0x004c:
            java.lang.String r0 = "cbc1"
        L_0x004e:
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0020
            r1 = 2
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4SP.<init>(java.lang.String, byte[], byte[], int, int, int, boolean):void");
    }
}
