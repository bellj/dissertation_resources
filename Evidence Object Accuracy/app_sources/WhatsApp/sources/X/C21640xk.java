package X;

import android.text.TextUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.0xk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21640xk {
    public final C14850m9 A00;
    public final C21680xo A01;
    public final Set A02;

    public C21640xk(C14850m9 r3, C21680xo r4) {
        Set unmodifiableSet = Collections.unmodifiableSet(new HashSet());
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = Collections.unmodifiableSet(new HashSet(unmodifiableSet));
    }

    public static boolean A00(String str, String str2) {
        if (!(str == null || str2 == null)) {
            if (str2.equals("all") || str2.equals("none")) {
                throw new IllegalArgumentException("Name 'none' and 'all' are not supported");
            }
            Locale locale = Locale.US;
            String lowerCase = str.toLowerCase(locale);
            String lowerCase2 = str2.toLowerCase(locale);
            String trim = lowerCase.trim();
            if (!TextUtils.isEmpty(trim) && !trim.equals("none")) {
                boolean z = false;
                for (String str3 : trim.split(";")) {
                    String trim2 = str3.trim();
                    if (!TextUtils.isEmpty(trim2)) {
                        if (trim2.equals("all")) {
                            z = true;
                        }
                        if (trim2.equals(lowerCase2)) {
                            z = true;
                        }
                        if (trim2.startsWith("-") && lowerCase2.equals(trim2.substring(1))) {
                        }
                    }
                }
                return z;
            }
        }
        return false;
    }

    public int A01(String str) {
        C14850m9 r2 = this.A00;
        if (A00(r2.A03(207), str)) {
            return 1;
        }
        if (A00(r2.A03(208), str) || !A00(r2.A03(209), str)) {
            return 2;
        }
        return 3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x005d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005e A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject A02(java.lang.String r10) {
        /*
            r9 = this;
            boolean r0 = android.text.TextUtils.isEmpty(r10)
            if (r0 != 0) goto L_0x0067
            java.lang.String r7 = "all"
            boolean r0 = r10.equals(r7)
            if (r0 != 0) goto L_0x005f
            java.lang.String r0 = "none"
            boolean r0 = r10.equals(r0)
            if (r0 != 0) goto L_0x005f
            X.0m9 r1 = r9.A00
            r0 = 277(0x115, float:3.88E-43)
            org.json.JSONObject r2 = r1.A04(r0)
            java.lang.String r1 = "config"
            boolean r0 = r2.has(r1)
            r6 = 0
            if (r0 == 0) goto L_0x0052
            org.json.JSONArray r5 = r2.getJSONArray(r1)     // Catch: JSONException -> 0x0054
            int r4 = r5.length()     // Catch: JSONException -> 0x0054
            r3 = 0
            r8 = r6
            goto L_0x0035
        L_0x0032:
            r8 = r2
        L_0x0033:
            int r3 = r3 + 1
        L_0x0035:
            if (r3 >= r4) goto L_0x005b
            org.json.JSONObject r2 = r5.getJSONObject(r3)     // Catch: JSONException -> 0x0050
            java.lang.String r0 = "name"
            java.lang.String r1 = r2.getString(r0)     // Catch: JSONException -> 0x0050
            boolean r0 = r7.equalsIgnoreCase(r1)     // Catch: JSONException -> 0x0050
            if (r0 == 0) goto L_0x0048
            goto L_0x0032
        L_0x0048:
            boolean r0 = r10.equalsIgnoreCase(r1)     // Catch: JSONException -> 0x0050
            if (r0 == 0) goto L_0x0033
            r6 = r2
            goto L_0x005b
        L_0x0050:
            r1 = move-exception
            goto L_0x0056
        L_0x0052:
            r8 = r6
            goto L_0x005b
        L_0x0054:
            r1 = move-exception
            r8 = r6
        L_0x0056:
            java.lang.String r0 = "failed to parse config for ab prop DB_VERIFICATION_CONTROLS_CODE"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x005b:
            if (r6 != 0) goto L_0x005e
            return r8
        L_0x005e:
            return r6
        L_0x005f:
            java.lang.String r1 = "Name 'none' and 'all' are not supported"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0067:
            java.lang.String r1 = "Name should not be empty or null"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C21640xk.A02(java.lang.String):org.json.JSONObject");
    }
}
