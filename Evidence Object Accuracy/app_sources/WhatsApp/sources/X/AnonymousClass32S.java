package X;

import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.32S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass32S extends RunnableC32531cJ {
    public final /* synthetic */ GroupChatInfo A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass32S(C21320xE r10, GroupChatInfo groupChatInfo, C20710wC r12, C15580nU r13, C14860mA r14) {
        super(r10, r12, r13, null, r14, null, null, 16);
        this.A00 = groupChatInfo;
    }
}
