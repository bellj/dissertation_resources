package X;

import android.util.Pair;
import java.security.PublicKey;

/* renamed from: X.69k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1330869k implements AnonymousClass6MM {
    public final /* synthetic */ AnonymousClass6M0 A00;
    public final /* synthetic */ AnonymousClass605 A01;
    public final /* synthetic */ C128545wH A02;

    public C1330869k(AnonymousClass6M0 r1, AnonymousClass605 r2, C128545wH r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MM
    public void APo(C452120p r2) {
        this.A00.AVD(r2);
    }

    @Override // X.AnonymousClass6MM
    public void AX5(String str) {
        AnonymousClass605 r11 = this.A01;
        C128545wH r12 = this.A02;
        AnonymousClass6M0 r10 = this.A00;
        Pair A03 = r11.A09.A03();
        Object obj = A03.second;
        if (obj != null) {
            r11.A08.A07("[Set Touch ID] success");
            C130775zx r1 = r11.A01;
            byte[] encoded = ((PublicKey) obj).getEncoded();
            C121235hT r6 = new C121235hT(r11.A04.A00, r11.A02, r11.A05, r10, r11, r12, (String) A03.first);
            byte[] A00 = C130775zx.A00(Boolean.TRUE, str, "SETBIO", null, encoded, new Object[0], C117295Zj.A03(r1.A01));
            AnonymousClass1W9[] r3 = new AnonymousClass1W9[1];
            C12960it.A1M("action", "set-payment-bio", r3, 0);
            C128545wH.A01(r12, r1, r6, A00, r3);
            return;
        }
        r11.A08.A07("[Set Touch ID] failure");
    }
}
