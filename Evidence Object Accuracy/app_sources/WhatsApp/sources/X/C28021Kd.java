package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Kd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28021Kd implements Comparable, Parcelable {
    public static final Parcelable.Creator CREATOR = new C99754ko();
    public C28011Kc A00;
    public final String A01;
    public transient AbstractC14640lm A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C28021Kd(C28011Kc r2, AbstractC14640lm r3) {
        this.A02 = r3;
        this.A01 = r3.getRawString();
        this.A00 = r2;
    }

    public C28021Kd(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A01 = readString;
        Parcelable readParcelable = parcel.readParcelable(C28011Kc.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.A00 = (C28011Kc) readParcelable;
    }

    /* renamed from: A00 */
    public int compareTo(C28021Kd r7) {
        C28011Kc r5 = r7.A00;
        long j = r5.A0G;
        C28011Kc r2 = this.A00;
        int signum = (int) Math.signum((float) (j - r2.A0G));
        return signum == 0 ? (int) Math.signum((float) (r5.A06 - r2.A06)) : signum;
    }

    public synchronized AbstractC14640lm A01() {
        AbstractC14640lm r3;
        r3 = this.A02;
        if (r3 == null) {
            String str = this.A01;
            r3 = AbstractC14640lm.A01(str);
            StringBuilder sb = new StringBuilder();
            sb.append("contactRawJid = ");
            sb.append(str);
            AnonymousClass009.A06(r3, sb.toString());
            this.A02 = r3;
        }
        return r3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeParcelable(this.A00, i);
    }
}
