package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.whatsapp.util.Log;

/* renamed from: X.3Z5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z5 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1OE A00;
    public final /* synthetic */ C244315m A01;

    public AnonymousClass3Z5(AnonymousClass1OE r1, C244315m r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/finishLoginOnSuccess/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r8, String str) {
        byte[] bArr;
        AnonymousClass1OH r3;
        AnonymousClass1OE r2 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/finishLoginOnSuccess id=")));
        AnonymousClass1V8 A0E = r8.A0E("success");
        if (A0E == null || (bArr = A0E.A01) == null) {
            Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/finishLoginOnSuccess success was empty id=")));
            r2.APr("success was empty", 1);
            return;
        }
        ((AnonymousClass1OF) r2).A00.A01();
        Log.i("EncBackupManger/finishLogin saving backup key");
        synchronized (r2.A0C) {
            r3 = r2.A02;
        }
        ((AnonymousClass1OF) r2).A01.Ab2(new RunnableBRunnable0Shape0S0400000_I0(r2, r3, bArr, r2.A08, 5));
    }
}
