package X;

/* renamed from: X.4YP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4YP {
    public int A00 = 0;
    public int A01;
    public int A02;
    public byte[] A03;

    public AnonymousClass4YP(byte[] bArr, int i, int i2) {
        this.A03 = bArr;
        this.A02 = i;
        this.A01 = i2;
        A04();
    }

    public int A00() {
        int i = 0;
        int i2 = 0;
        while (!A06()) {
            i2++;
        }
        int i3 = (1 << i2) - 1;
        if (i2 > 0) {
            i = A02(i2);
        }
        int i4 = i3 + i;
        int i5 = 1;
        if (i4 % 2 == 0) {
            i5 = -1;
        }
        return i5 * ((i4 + 1) >> 1);
    }

    public int A01() {
        int i = 0;
        int i2 = 0;
        while (!A06()) {
            i2++;
        }
        int i3 = (1 << i2) - 1;
        if (i2 > 0) {
            i = A02(i2);
        }
        return i3 + i;
    }

    public int A02(int i) {
        int i2;
        int i3 = this.A00 + i;
        this.A00 = i3;
        int i4 = 0;
        while (true) {
            i2 = 2;
            if (i3 <= 8) {
                break;
            }
            i3 -= 8;
            this.A00 = i3;
            byte[] bArr = this.A03;
            int i5 = this.A02;
            i4 |= (bArr[i5] & 255) << i3;
            if (!A07(i5 + 1)) {
                i2 = 1;
            }
            this.A02 = i5 + i2;
        }
        byte[] bArr2 = this.A03;
        int i6 = this.A02;
        int i7 = (-1 >>> (32 - i)) & (i4 | ((bArr2[i6] & 255) >> (8 - i3)));
        if (i3 == 8) {
            this.A00 = 0;
            if (!A07(i6 + 1)) {
                i2 = 1;
            }
            this.A02 = i6 + i2;
        }
        A04();
        return i7;
    }

    public void A03() {
        int i = 1;
        int i2 = this.A00 + 1;
        this.A00 = i2;
        if (i2 == 8) {
            this.A00 = 0;
            int i3 = this.A02;
            if (A07(i3 + 1)) {
                i = 2;
            }
            this.A02 = i3 + i;
        }
        A04();
    }

    public final void A04() {
        int i;
        int i2 = this.A02;
        C95314dV.A04(i2 >= 0 && (i2 < (i = this.A01) || (i2 == i && this.A00 == 0)));
    }

    public void A05(int i) {
        int i2 = this.A02;
        int i3 = i >> 3;
        int i4 = i2 + i3;
        this.A02 = i4;
        int i5 = this.A00 + (i - (i3 << 3));
        this.A00 = i5;
        if (i5 > 7) {
            i4++;
            this.A02 = i4;
            this.A00 = i5 - 8;
        }
        while (true) {
            i2++;
            if (i2 > i4) {
                A04();
                return;
            } else if (A07(i2)) {
                i4++;
                this.A02 = i4;
                i2 += 2;
            }
        }
    }

    public boolean A06() {
        boolean A1S = C12960it.A1S(this.A03[this.A02] & (128 >> this.A00));
        A03();
        return A1S;
    }

    public final boolean A07(int i) {
        if (2 > i || i >= this.A01) {
            return false;
        }
        byte[] bArr = this.A03;
        return bArr[i] == 3 && bArr[i + -2] == 0 && bArr[i - 1] == 0;
    }
}
