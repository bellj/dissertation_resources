package X;

import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.payments.ui.BrazilPaymentActivity;

/* renamed from: X.6Ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133326Ai implements AbstractC136506Mu {
    public final /* synthetic */ FingerprintBottomSheet A00;
    public final /* synthetic */ C30821Yy A01;
    public final /* synthetic */ AbstractC28901Pl A02;
    public final /* synthetic */ AnonymousClass1KC A03;
    public final /* synthetic */ BrazilPaymentActivity A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;

    public C133326Ai(FingerprintBottomSheet fingerprintBottomSheet, C30821Yy r2, AbstractC28901Pl r3, AnonymousClass1KC r4, BrazilPaymentActivity brazilPaymentActivity, String str, String str2) {
        this.A04 = brazilPaymentActivity;
        this.A02 = r3;
        this.A01 = r2;
        this.A05 = str;
        this.A06 = str2;
        this.A03 = r4;
        this.A00 = fingerprintBottomSheet;
    }

    public void A00() {
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        AbstractC28901Pl r2 = this.A02;
        C30821Yy r1 = this.A01;
        String str = this.A05;
        String str2 = this.A06;
        AnonymousClass1KC r3 = this.A03;
        PinBottomSheetDialogFragment A00 = C125035qZ.A00();
        A00.A0B = new AnonymousClass6CG(r1, r2, r3, A00, brazilPaymentActivity, str2, str);
        brazilPaymentActivity.A0N.AL9("enter_pin", brazilPaymentActivity.A00);
        brazilPaymentActivity.Adm(A00);
    }

    @Override // X.AbstractC136506Mu
    public void AKZ(C452120p r4) {
        this.A04.A0N.A06("br_pay_precheck_tag", 3);
    }

    @Override // X.AbstractC136506Mu
    public void AKb() {
        this.A04.A0N.A02(185481152, "br_get_provider_key_tag");
    }

    @Override // X.AbstractC136506Mu
    public void AKk() {
        this.A04.A0N.A06("br_get_provider_key_tag", 3);
    }

    @Override // X.AbstractC136506Mu
    public void AKl() {
        this.A04.A0N.A06("br_get_provider_key_tag", 2);
    }
}
