package X;

import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1OE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1OE extends AnonymousClass1OF implements AnonymousClass1OG {
    public int A00 = 0;
    public int A01 = -1;
    public AnonymousClass1OH A02;
    public C89654Ku A03;
    public AnonymousClass1OG A04;
    public byte[] A05;
    public byte[] A06;
    public byte[] A07;
    public final AnonymousClass1OI A08;
    public final C15820nx A09;
    public final C244315m A0A;
    public final JniBridge A0B;
    public final Object A0C = new Object();
    public final String A0D;

    public AnonymousClass1OE(AnonymousClass1OI r2, C15820nx r3, C244315m r4, C32881ct r5, AbstractC14440lR r6, JniBridge jniBridge, String str) {
        super(r5, r6);
        this.A0B = jniBridge;
        this.A09 = r3;
        this.A0A = r4;
        this.A0D = str;
        this.A08 = r2;
    }
}
