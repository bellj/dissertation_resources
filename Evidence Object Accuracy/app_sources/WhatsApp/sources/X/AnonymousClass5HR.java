package X;

import java.lang.ref.PhantomReference;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.5HR  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5HR extends PhantomReference {
    public AnonymousClass5HR A00;
    public AnonymousClass5HR A01;

    public abstract void destruct();

    public /* synthetic */ AnonymousClass5HR() {
        super(null, AnonymousClass4HT.A03);
    }

    public AnonymousClass5HR(Object obj) {
        super(obj, AnonymousClass4HT.A03);
        AtomicReference atomicReference;
        AnonymousClass5HR r0;
        AnonymousClass4I5 r2 = AnonymousClass4HT.A01;
        do {
            atomicReference = r2.A00;
            r0 = (AnonymousClass5HR) atomicReference.get();
            this.A00 = r0;
        } while (!atomicReference.compareAndSet(r0, this));
    }
}
