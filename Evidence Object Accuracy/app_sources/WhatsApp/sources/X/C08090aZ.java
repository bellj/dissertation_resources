package X;

import java.util.List;

/* renamed from: X.0aZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08090aZ implements AbstractC12660iI {
    public float A00 = -1.0f;
    public final AnonymousClass0U8 A01;

    @Override // X.AbstractC12660iI
    public boolean isEmpty() {
        return false;
    }

    public C08090aZ(List list) {
        this.A01 = (AnonymousClass0U8) list.get(0);
    }

    @Override // X.AbstractC12660iI
    public AnonymousClass0U8 AC6() {
        return this.A01;
    }

    @Override // X.AbstractC12660iI
    public float ACj() {
        return this.A01.A00();
    }

    @Override // X.AbstractC12660iI
    public float AGu() {
        return this.A01.A01();
    }

    @Override // X.AbstractC12660iI
    public boolean AJF(float f) {
        if (this.A00 == f) {
            return true;
        }
        this.A00 = f;
        return false;
    }

    @Override // X.AbstractC12660iI
    public boolean AKI(float f) {
        return !this.A01.A02();
    }
}
