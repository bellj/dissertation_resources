package X;

import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.usernotice.UserNoticeStageUpdateWorker;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/* renamed from: X.12O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12O {
    public final AbstractC15710nm A00;
    public final C14830m7 A01;
    public final C17170qN A02;
    public final C14850m9 A03;
    public final C22230yk A04;
    public final C15510nN A05;
    public final C21870y7 A06;
    public final AnonymousClass12M A07;
    public final AnonymousClass12N A08;
    public final AnonymousClass12L A09;
    public final C21710xr A0A;

    public AnonymousClass12O(AbstractC15710nm r1, C14830m7 r2, C17170qN r3, C14850m9 r4, C22230yk r5, C15510nN r6, C21870y7 r7, AnonymousClass12M r8, AnonymousClass12N r9, AnonymousClass12L r10, C21710xr r11) {
        this.A01 = r2;
        this.A03 = r4;
        this.A00 = r1;
        this.A0A = r11;
        this.A04 = r5;
        this.A09 = r10;
        this.A06 = r7;
        this.A05 = r6;
        this.A07 = r8;
        this.A08 = r9;
        this.A02 = r3;
    }

    public static C43881xk A00(C43891xl r2, int i) {
        C43841xg r0;
        if (!(i == 0 || i == 1)) {
            if (i != 2) {
                if (i == 3) {
                    r0 = r2.A04;
                } else if (i == 4) {
                    r0 = r2.A03;
                } else if (i != 5) {
                    StringBuilder sb = new StringBuilder("Unexpected value: ");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString());
                }
                if (r0 != null) {
                    return r0.A00;
                }
            } else {
                C43911xp r02 = r2.A02;
                if (r02 != null) {
                    return r02.A00;
                }
            }
        }
        return null;
    }

    public C43911xp A01() {
        String str;
        AnonymousClass12N r8 = this.A08;
        C43831xf A01 = r8.A01();
        if (A01 != null && A01.A01 == 2) {
            int i = A01.A00;
            C14850m9 r4 = this.A03;
            if (C43901xo.A00(r4, i)) {
                StringBuilder sb = new StringBuilder("UserNoticeManager/getBanner/green alert disabled, notice: ");
                sb.append(i);
                str = sb.toString();
            } else {
                C43891xl A03 = this.A07.A03(A01);
                if (A03 != null) {
                    C43911xp r3 = A03.A02;
                    if (r3 == null) {
                        Log.e("UserNoticeManager/getBanner/no content for stage 2");
                        this.A00.AaV("UserNoticeManager/getBanner/noContent", null, true);
                        return null;
                    }
                    if (C43901xo.A01(r4, A01)) {
                        if (this.A01.A00() < r8.A00().getLong("current_user_notice_banner_dismiss_timestamp", 0) + 86400000) {
                            Log.i("UserNoticeManager/getBanner/dismissed banner not shown as per timing");
                            A06();
                            return null;
                        }
                        Log.i("UserNoticeManager/getBanner/eligible to show dismissible banner");
                        r8.A00().edit().putLong("current_user_notice_banner_dismiss_timestamp", 0).apply();
                    }
                    C43881xk r1 = r3.A00;
                    if (!A0C(r1)) {
                        str = "UserNoticeManager/getBanner/banner not shown as per timing";
                    } else {
                        A0A(r1, C43901xo.A01(r4, A01));
                        Log.i("UserNoticeManager/getBanner/banner shown");
                        return r3;
                    }
                }
            }
            Log.i(str);
        }
        return null;
    }

    public void A02() {
        C43831xf A01 = this.A08.A01();
        if (A01 == null) {
            Log.e("UserNoticeManager/agreeUserNotice/no current notice to agree");
            this.A00.AaV("UserNoticeManager/agreeUserNotice/noContent", null, true);
            return;
        }
        Log.i("UserNoticeManager/agreeUserNotice");
        A0B(A01, 5);
    }

    public void A03() {
        Log.i("UserNoticeManager/deleteAllUserNoticesWithoutCleanup");
        AnonymousClass12M r4 = this.A07;
        Log.i("UserNoticeContentManager/deleteAllUserNoticeData");
        File A02 = AnonymousClass12M.A02(r4.A02.A00.getFilesDir(), "user_notice");
        if (A02 != null) {
            r4.A08.Ab2(new RunnableBRunnable0Shape13S0100000_I0_13(A02, 11));
        }
        r4.A00 = null;
        AnonymousClass12N r2 = this.A08;
        r2.A00().edit().remove("current_user_notice_id").remove("current_user_notice_stage").remove("current_user_notice_stage_timestamp").remove("current_user_notice_version").remove("current_user_notice_duration_repeat_index").remove("current_user_notice_duration_repeat_timestamp").remove("current_user_notice_banner_dismiss_timestamp").remove("user_notices").apply();
        r2.A02.clear();
    }

    public void A04() {
        StringBuilder sb = new StringBuilder("UserNoticeManager/updateUserNoticeStage/expected current stage: ");
        sb.append(2);
        Log.i(sb.toString());
        C43831xf A01 = this.A08.A01();
        AnonymousClass009.A05(A01);
        int i = A01.A01;
        if (2 < i) {
            StringBuilder sb2 = new StringBuilder("UserNoticeManager/updateUserNoticeStage/already moved forward, stored current stage: ");
            sb2.append(i);
            Log.i(sb2.toString());
            return;
        }
        C43891xl A03 = this.A07.A03(A01);
        AnonymousClass009.A05(A03);
        int i2 = 3;
        if (A03.A04 == null) {
            i2 = 4;
            if (A03.A03 == null) {
                i2 = 5;
            }
        }
        A0B(A01, i2);
    }

    public final void A05() {
        Log.i("UserNoticeManager/cleanupAfterDelete");
        A06();
        ((AnonymousClass022) this.A0A.get()).A07("tag.whatsapp.usernotice.stageupdate");
        AnonymousClass12M r1 = this.A07;
        Log.i("UserNoticeContentManager/cancelWork");
        C21710xr r2 = r1.A09;
        ((AnonymousClass022) r2.get()).A07("tag.whatsapp.usernotice.content.fetch");
        ((AnonymousClass022) r2.get()).A07("tag.whatsapp.usernotice.icon.fetch");
    }

    public final void A06() {
        Log.i("UserNoticeManager/sendWebClientEmptyUpdate");
        this.A04.A00(-1, -1, false);
    }

    public final void A07(int i, int i2, int i3) {
        StringBuilder sb = new StringBuilder("UserNoticeManager/enqueueStageUpdateWork/notice id: ");
        sb.append(i);
        sb.append(" stage: ");
        sb.append(i2);
        sb.append(" version: ");
        sb.append(i3);
        Log.i(sb.toString());
        C006403a r1 = new C006403a();
        r1.A01("notice_id", i);
        r1.A01("stage", i2);
        r1.A01("version", i3);
        C006503b A00 = r1.A00();
        C003901s r12 = new C003901s();
        r12.A01 = EnumC004001t.CONNECTED;
        C004101u r2 = new C004101u(r12);
        C004201x r4 = new C004201x(UserNoticeStageUpdateWorker.class);
        r4.A01.add("tag.whatsapp.usernotice.stageupdate");
        r4.A00.A09 = r2;
        r4.A03(EnumC007503u.EXPONENTIAL, TimeUnit.HOURS, 1);
        r4.A00.A0A = A00;
        StringBuilder sb2 = new StringBuilder("tag.whatsapp.usernotice.stageupdate.");
        sb2.append(i);
        sb2.append(".");
        sb2.append(i2);
        String obj = sb2.toString();
        ((AnonymousClass022) this.A0A.get()).A05(AnonymousClass023.REPLACE, (AnonymousClass021) r4.A00(), obj);
    }

    public final void A08(int i, int i2, int i3) {
        boolean z = true;
        if (i2 <= 1) {
            StringBuilder sb = new StringBuilder("UserNoticeManager/updateWebClient/no update sent, stage: ");
            sb.append(i2);
            Log.i(sb.toString());
        } else if (i2 == 3 || i2 == 5 || C43901xo.A00(this.A03, i)) {
            A06();
        } else {
            if (i2 != 4) {
                z = false;
            }
            StringBuilder sb2 = new StringBuilder("UserNoticeManager/updateWebClient/noticeId: ");
            sb2.append(i);
            sb2.append(" blocking: ");
            sb2.append(z);
            sb2.append(" version:");
            sb2.append(i3);
            Log.i(sb2.toString());
            this.A04.A00(i, i3, z);
        }
    }

    public final void A09(C43891xl r11, C43831xf r12) {
        String str;
        String str2;
        int i;
        C43871xj r0;
        C43871xj r02;
        C43871xj r03;
        int i2 = r12.A01;
        StringBuilder sb = new StringBuilder("UserNoticeManager/transitionUserNoticeStageIfNecessary/noticeId: ");
        sb.append(r12.A00);
        sb.append(" currentStage: ");
        sb.append(i2);
        Log.i(sb.toString());
        if (i2 == 0) {
            Log.i("UserNoticeManager/transitionUserNoticeStageIfNecessary/stage 0, no timing transition needed");
        } else {
            if (i2 == 5) {
                str2 = "UserNoticeManager/transitionUserNoticeStageIfNecessary/stage 5, no timing transition needed";
            } else if (r11 == null) {
                str2 = "UserNoticeManager/transitionUserNoticeStageIfNecessary/no content";
            } else {
                long A00 = this.A01.A00();
                ArrayList arrayList = new ArrayList();
                C43881xk A002 = A00(r11, i2);
                if (!(A002 == null || (r03 = A002.A01) == null)) {
                    arrayList.add(new C43941xt(i2, r03.A00, 1));
                }
                int i3 = 2;
                if (i2 >= 2 || r11.A02 == null) {
                    i3 = 3;
                    if (i2 >= 3 || r11.A04 == null) {
                        i3 = 4;
                        if (i2 >= 4 || r11.A03 == null) {
                            i3 = 5;
                        }
                    }
                }
                while (i3 < 5) {
                    C43881xk A003 = A00(r11, i3);
                    if (!(A003 == null || (r02 = A003.A02) == null)) {
                        arrayList.add(new C43941xt(i3, r02.A00, 0));
                    }
                    C43881xk A004 = A00(r11, i3);
                    if (!(A004 == null || (r0 = A004.A01) == null)) {
                        arrayList.add(new C43941xt(i3, r0.A00, 1));
                    }
                    i3++;
                }
                Iterator it = arrayList.iterator();
                C43941xt r3 = null;
                while (it.hasNext()) {
                    C43941xt r5 = (C43941xt) it.next();
                    if (r5.A02 > A00) {
                        break;
                    }
                    r3 = r5;
                }
                if (r3 != null) {
                    if (r3.A01 == 0) {
                        StringBuilder sb2 = new StringBuilder("UserNoticeManager/handleEligibleFutureStartEndTiming/passed start timing: ");
                        sb2.append(r3.A02);
                        sb2.append(" of stage:");
                        i = r3.A00;
                        sb2.append(i);
                        Log.i(sb2.toString());
                    } else {
                        StringBuilder sb3 = new StringBuilder("UserNoticeManager/handleEligibleFutureStartEndTiming/passed end timing: ");
                        sb3.append(r3.A02);
                        sb3.append(" of stage: ");
                        int i4 = r3.A00;
                        sb3.append(i4);
                        Log.i(sb3.toString());
                        if (i4 != 0) {
                            i = 2;
                            if (i4 >= 2 || r11.A02 == null) {
                                i = 3;
                                if (i4 >= 3 || r11.A04 == null) {
                                    i = 4;
                                    if (i4 >= 4 || r11.A03 == null) {
                                        i = 5;
                                    }
                                }
                            }
                        }
                    }
                    A0B(r12, i);
                }
                C43881xk A005 = A00(r11, i2);
                C43881xk A006 = A00(r11, i3);
                if (A006 != null && A006.A02 != null) {
                    str2 = "UserNoticeManager/handleNextStageStartTime/next stage start time exists";
                } else if (A005 == null) {
                    return;
                } else {
                    if (A005.A01 != null) {
                        str2 = "UserNoticeManager/handleCurrentStageEndTiming/current stage end time exists";
                    } else {
                        C43861xi r32 = A005.A00;
                        if (r32 != null) {
                            Log.i("UserNoticeManager/handleCurrentStageDuration/current stage duration exists");
                            long j = r32.A00;
                            if (j != -1) {
                                long j2 = r12.A03;
                                Log.i("UserNoticeManager/handleCurrentStageStaticDuration/static duration exists");
                                if (A00 >= j2 + j) {
                                    str = "UserNoticeManager/handleCurrentStageStaticDuration/current stage static duration expired";
                                } else {
                                    return;
                                }
                            } else {
                                long[] jArr = r32.A01;
                                if (jArr != null) {
                                    Log.i("UserNoticeManager/handleCurrentStageRepeatDuration/repeat duration exists");
                                    if (this.A08.A00().getInt("current_user_notice_duration_repeat_index", 0) > jArr.length) {
                                        str = "UserNoticeManager/handleCurrentStageRepeatDuration/current stage repeat duration complete";
                                    } else {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            }
                            Log.i(str);
                            A0B(r12, i3);
                            return;
                        }
                        return;
                    }
                }
            }
            Log.i(str2);
            return;
        }
        i = 1;
        A0B(r12, i);
    }

    public final void A0A(C43881xk r6, boolean z) {
        C43861xi r0 = r6.A00;
        if (r0 == null || r0.A01 == null) {
            Log.i("UserNoticeManager/handleRepeatTimingIfNecessary/no repeat duration");
            return;
        }
        Log.i("UserNoticeManager/handleRepeatTimingIfNecessary/set repeat values");
        int i = 1;
        if (!z) {
            i = 1 + this.A08.A00().getInt("current_user_notice_duration_repeat_index", 0);
        }
        AnonymousClass12N r4 = this.A08;
        r4.A00().edit().putInt("current_user_notice_duration_repeat_index", i).apply();
        r4.A00().edit().putLong("current_user_notice_duration_repeat_timestamp", this.A01.A00()).apply();
    }

    public final void A0B(C43831xf r9, int i) {
        int i2 = r9.A00;
        StringBuilder sb = new StringBuilder("UserNoticeManager/updateUserNoticeStage/updating to new stage: ");
        sb.append(i);
        sb.append(" noticeId: ");
        sb.append(i2);
        Log.i(sb.toString());
        AnonymousClass12N r1 = this.A08;
        long A00 = this.A01.A00();
        int i3 = r9.A02;
        r1.A03(new C43831xf(i2, i, i3, A00));
        A08(i2, i, i3);
        r1.A00().edit().remove("current_user_notice_duration_repeat_index").remove("current_user_notice_duration_repeat_timestamp").remove("current_user_notice_duration_static_timestamp_start").apply();
        A07(i2, i, i3);
    }

    public final boolean A0C(C43881xk r12) {
        String obj;
        C43861xi r5 = r12.A00;
        boolean z = true;
        if (r5 == null) {
            obj = "UserNoticeManager/shouldShowStage/no duration";
        } else {
            long A00 = this.A01.A00();
            long j = r5.A00;
            if (j != -1) {
                Log.i("UserNoticeManager/shouldShowStageForStaticDuration/has static duration");
                AnonymousClass12N r10 = this.A08;
                long j2 = r10.A00().getLong("current_user_notice_duration_static_timestamp_start", 0);
                if (j2 == 0) {
                    StringBuilder sb = new StringBuilder("UserNoticeManager/shouldShowStageForStaticDuration/static duration start: ");
                    sb.append(A00);
                    Log.i(sb.toString());
                    r10.A00().edit().putLong("current_user_notice_duration_static_timestamp_start", A00).apply();
                    j2 = A00;
                }
                if (A00 >= j2 + j) {
                    Log.i("UserNoticeManager/shouldShowStageForStaticDuration/static duration expired");
                    return false;
                }
                Log.i("UserNoticeManager/shouldShowStageForStaticDuration/static duration valid");
                return true;
            }
            long[] jArr = r5.A01;
            if (jArr == null) {
                obj = "UserNoticeManager/shouldShowStage/no repeat duration";
            } else {
                AnonymousClass12N r52 = this.A08;
                int i = r52.A00().getInt("current_user_notice_duration_repeat_index", 0);
                if (i == 0) {
                    obj = "UserNoticeManager/shouldShowStage/allow first repeat";
                } else if (i > jArr.length) {
                    Log.i("UserNoticeManager/shouldShowStage/no more repeats");
                    return false;
                } else {
                    if (A00 - r52.A00().getLong("current_user_notice_duration_repeat_timestamp", 0) < jArr[i - 1]) {
                        z = false;
                    }
                    StringBuilder sb2 = new StringBuilder("UserNoticeManager/shouldShowStage/repeatTimeElapse: ");
                    sb2.append(z);
                    obj = sb2.toString();
                }
            }
        }
        Log.i(obj);
        return z;
    }
}
