package X;

import android.content.Context;
import android.widget.LinearLayout;

/* renamed from: X.2V2  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2V2 extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;

    public AnonymousClass2V2(Context context) {
        super(context);
        A00();
    }

    public void A00() {
        if (this instanceof C621334p) {
            C621334p r1 = (C621334p) this;
            if (!r1.A00) {
                r1.A00 = true;
                r1.generatedComponent();
            }
        } else if (this instanceof C621234o) {
            C621234o r12 = (C621234o) this;
            if (!r12.A00) {
                r12.A00 = true;
                r12.generatedComponent();
            }
        } else if (this instanceof AnonymousClass34q) {
            AnonymousClass34q r13 = (AnonymousClass34q) this;
            if (!r13.A00) {
                r13.A00 = true;
                r13.generatedComponent();
            }
        } else if (this instanceof AnonymousClass34d) {
            AnonymousClass34d r14 = (AnonymousClass34d) this;
            if (!r14.A01) {
                r14.A01 = true;
                r14.generatedComponent();
            }
        } else if (this instanceof AnonymousClass34e) {
            AnonymousClass34e r15 = (AnonymousClass34e) this;
            if (!r15.A01) {
                r15.A01 = true;
                r15.generatedComponent();
            }
        } else if (this instanceof AnonymousClass2Uy) {
            AnonymousClass2Uy r16 = (AnonymousClass2Uy) this;
            if (!r16.A01) {
                r16.A01 = true;
                r16.generatedComponent();
            }
        } else if (this instanceof AnonymousClass34c) {
            AnonymousClass34c r17 = (AnonymousClass34c) this;
            if (!r17.A01) {
                r17.A01 = true;
                r17.generatedComponent();
            }
        } else if (!(this instanceof AnonymousClass34b)) {
            AnonymousClass34n r18 = (AnonymousClass34n) this;
            if (r18 instanceof C621434s) {
                C621434s r19 = (C621434s) r18;
                if (!r19.A01) {
                    r19.A01 = true;
                    r19.generatedComponent();
                }
            } else if (!r18.A00) {
                r18.A00 = true;
                r18.generatedComponent();
            }
        } else {
            AnonymousClass34b r110 = (AnonymousClass34b) this;
            if (!r110.A01) {
                r110.A01 = true;
                r110.generatedComponent();
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
