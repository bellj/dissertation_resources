package X;

import android.view.animation.Interpolator;

/* renamed from: X.07S  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass07S implements Interpolator {
    @Override // android.animation.TimeInterpolator
    public float getInterpolation(float f) {
        float f2 = f - 1.0f;
        return (f2 * f2 * f2 * f2 * f2) + 1.0f;
    }
}
