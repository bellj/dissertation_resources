package X;

/* renamed from: X.40B  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass40B extends AnonymousClass4UW {
    public final boolean A00;

    public AnonymousClass40B() {
        this(false);
    }

    public AnonymousClass40B(boolean z) {
        this.A00 = z;
    }

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass40B) && this.A00 == ((AnonymousClass40B) obj).A00);
    }

    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("DistanceChip(isSelected=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
