package X;

import android.content.SharedPreferences;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0nO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15520nO {
    public SharedPreferences A00;
    public final C14830m7 A01;
    public final C16630pM A02;

    public C15520nO(C14830m7 r1, C16630pM r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public static String A00(String str, String str2) {
        StringBuilder sb = new StringBuilder("/package/");
        sb.append(str);
        sb.append("/");
        sb.append(str2);
        return sb.toString();
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("instrumentation");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public Set A02() {
        String substring;
        int indexOf;
        String substring2;
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (String str : A01().getAll().keySet()) {
            if (str.startsWith("/package/") && (indexOf = (substring = str.substring(9)).indexOf("/")) >= 0 && (substring2 = substring.substring(0, indexOf)) != null) {
                hashSet2.add(substring2);
            }
        }
        Iterator it = hashSet2.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next();
            if (A01().getString(A00(str2, "auth/token"), null) != null) {
                hashSet.add(str2);
            }
        }
        return hashSet;
    }

    public void A03(String str) {
        String A00 = A00(str, "auth/token");
        String A002 = A00(str, "auth/token_ts");
        String A003 = A00(str, "auth/encryption_key");
        String A004 = A00(str, "metadata/last_active_time");
        A01().edit().remove(A00).remove(A002).remove(A003).remove(A004).remove(A00(str, "metadata/delayed_notification_shown")).apply();
    }
}
