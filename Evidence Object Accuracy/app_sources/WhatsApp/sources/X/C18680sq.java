package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.0sq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18680sq {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final AnonymousClass13T A02;
    public final C14830m7 A03;
    public final C20290vW A04;
    public final AnonymousClass1YL A05 = new AnonymousClass1YL() { // from class: X.1Ye
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x01d4, code lost:
            if (r14 != null) goto L_0x01d6;
         */
        @Override // X.AnonymousClass1YL
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final X.AnonymousClass1YM A7t(X.AbstractC15590nW r33) {
            /*
            // Method dump skipped, instructions count: 1004
            */
            throw new UnsupportedOperationException("Method not decompiled: X.C30631Ye.A7t(X.0nW):X.1YM");
        }
    };
    public final C245215v A06;
    public final C18460sU A07;
    public final C16490p7 A08;
    public final AnonymousClass170 A09;
    public final C21390xL A0A;
    public final C18770sz A0B;
    public final C14850m9 A0C;
    public final C14840m8 A0D;
    public final AbstractC14440lR A0E;

    public C18680sq(AbstractC15710nm r2, C15570nT r3, AnonymousClass13T r4, C14830m7 r5, C20290vW r6, C245215v r7, C18460sU r8, C16490p7 r9, AnonymousClass170 r10, C21390xL r11, C18770sz r12, C14850m9 r13, C14840m8 r14, AbstractC14440lR r15) {
        this.A03 = r5;
        this.A0C = r13;
        this.A07 = r8;
        this.A00 = r2;
        this.A01 = r3;
        this.A0E = r15;
        this.A0D = r14;
        this.A0A = r11;
        this.A0B = r12;
        this.A02 = r4;
        this.A04 = r6;
        this.A08 = r9;
        this.A06 = r7;
        this.A09 = r10;
    }

    public static final void A00(AnonymousClass1YO r2) {
        Iterator it = AnonymousClass1JO.A00(r2.A04.values()).iterator();
        while (it.hasNext()) {
            ((AnonymousClass1YP) it.next()).A00 = false;
        }
    }

    public final long A01(UserJid userJid) {
        AnonymousClass009.A0B("participant-user-store/invalid-jid", !TextUtils.isEmpty(userJid.getRawString()));
        C18460sU r1 = this.A07;
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        if (userJid.equals(r02)) {
            userJid = C29831Uv.A00;
        }
        return r1.A01(userJid);
    }

    public Set A02(AbstractC15590nW r20) {
        HashSet hashSet = new HashSet();
        C18460sU r7 = this.A07;
        String valueOf = String.valueOf(r7.A01(r20));
        C16310on A01 = this.A08.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT user, server, agent, device, type, raw_string, user_jid_row_id FROM group_participant_user JOIN jid ON user_jid_row_id = jid._id WHERE group_jid_row_id = ?", new String[]{valueOf});
            int columnIndexOrThrow = A09.getColumnIndexOrThrow("user");
            int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("server");
            int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("agent");
            int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("device");
            int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("type");
            int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("raw_string");
            int columnIndexOrThrow7 = A09.getColumnIndexOrThrow("user_jid_row_id");
            while (A09.moveToNext()) {
                Parcelable parcelable = (UserJid) r7.A06(A09, A01, UserJid.class, columnIndexOrThrow, columnIndexOrThrow2, columnIndexOrThrow3, columnIndexOrThrow4, columnIndexOrThrow5, columnIndexOrThrow6, A09.getLong(columnIndexOrThrow7));
                if (parcelable == null) {
                    parcelable = null;
                } else if (parcelable.equals(C29831Uv.A00)) {
                    StringBuilder sb = new StringBuilder("participant-user-store/sanitizeParticipantJid/my jid = ");
                    C15570nT r4 = this.A01;
                    r4.A08();
                    sb.append(r4.A05);
                    Log.i(sb.toString());
                    r4.A08();
                    parcelable = r4.A05;
                    AnonymousClass009.A05(parcelable);
                }
                if (parcelable != null) {
                    hashSet.add(parcelable);
                }
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Set A03(UserJid userJid) {
        HashSet hashSet = new HashSet();
        C16310on A01 = this.A08.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT group_jid_row_id FROM group_participant_user WHERE user_jid_row_id = ?", new String[]{String.valueOf(A01(userJid))});
            while (A09.moveToNext()) {
                AbstractC15590nW r0 = (AbstractC15590nW) this.A07.A07(AbstractC15590nW.class, A09.getLong(A09.getColumnIndexOrThrow("group_jid_row_id")));
                if (r0 != null) {
                    hashSet.add(r0);
                }
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A04(AnonymousClass1YO r12, AbstractC15590nW r13) {
        StringBuilder sb = new StringBuilder("participant-user-store/updateGroupParticipant/");
        sb.append(r13);
        sb.append(" ");
        sb.append(r12);
        Log.i(sb.toString());
        UserJid userJid = r12.A03;
        long A01 = A01(userJid);
        String valueOf = String.valueOf(this.A07.A01(r13));
        String valueOf2 = String.valueOf(A01);
        ContentValues contentValues = new ContentValues(4);
        contentValues.put("group_jid_row_id", valueOf);
        contentValues.put("user_jid_row_id", valueOf2);
        contentValues.put("rank", Integer.valueOf(r12.A01));
        contentValues.put("pending", Integer.valueOf(r12.A02 ? 1 : 0));
        String[] strArr = {valueOf, valueOf2};
        C16310on A02 = this.A08.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r3 = A02.A03;
            if (r3.A00("group_participant_user", contentValues, "group_jid_row_id = ? AND user_jid_row_id = ?", strArr) != 0) {
                this.A09.A01(AnonymousClass1JO.A00(r12.A04.values()), r13, userJid, A01);
            } else {
                r3.A02(contentValues, "group_participant_user");
                this.A09.A00(AnonymousClass1JO.A00(r12.A04.values()), r13, userJid, A01);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(AnonymousClass1YM r5) {
        StringBuilder sb = new StringBuilder("participant-user-store/resetSentSenderKeyForAllParticipants/");
        sb.append(r5);
        Log.i(sb.toString());
        AbstractC15590nW r3 = r5.A03;
        C16310on A02 = this.A08.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            this.A09.A02(r3);
            A06(r5);
            A00.A00();
            A00.close();
            A02.close();
            AnonymousClass13T r0 = this.A02;
            r0.A01.A01(new C30641Yf(r3));
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A06(AnonymousClass1YM r3) {
        Iterator it = r3.A07().iterator();
        while (it.hasNext()) {
            A00((AnonymousClass1YO) it.next());
        }
    }

    public final void A07(AnonymousClass1YM r8, UserJid userJid, boolean z) {
        AnonymousClass1YO r0 = (AnonymousClass1YO) r8.A02.get(userJid);
        AbstractC15590nW r3 = r8.A03;
        if (r0 != null) {
            this.A09.A01(AnonymousClass1JO.A00(r0.A04.values()), r3, userJid, A01(userJid));
        }
        if (z) {
            this.A09.A02(r3);
        }
    }

    public void A08(AbstractC15590nW r7, Collection collection) {
        AnonymousClass1YM A00 = this.A06.A00(this.A05, r7);
        C16310on A02 = this.A08.A02();
        try {
            AnonymousClass1Lx A002 = A02.A00();
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                AnonymousClass1YO r0 = (AnonymousClass1YO) A00.A02.get((UserJid) it.next());
                if (r0 != null) {
                    A04(r0, r7);
                }
            }
            A002.A00();
            A002.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A09(AbstractC15590nW r6, List list) {
        C16310on A02 = this.A08.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            boolean z = false;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if (A0E(r6, (UserJid) it.next())) {
                    z = true;
                }
            }
            if (z) {
                this.A09.A02(r6);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0A(UserJid userJid, Set set, boolean z) {
        C16310on A02 = this.A08.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                A07((AnonymousClass1YM) it.next(), userJid, z);
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0B() {
        return this.A0A.A00("participant_user_ready", 0) == 2;
    }

    public boolean A0C() {
        return A0B() || this.A0A.A01("migration_participant_user_index", 0) > 0;
    }

    public final boolean A0D(AbstractC15590nW r9, long j) {
        StringBuilder sb = new StringBuilder("participant-user-store/removeGroupParticipant/");
        sb.append(r9);
        sb.append(" ");
        sb.append(j);
        Log.i(sb.toString());
        String valueOf = String.valueOf(this.A07.A01(r9));
        C16310on A02 = this.A08.A02();
        try {
            boolean z = false;
            if (A02.A03.A01("group_participant_user", "group_jid_row_id = ? AND user_jid_row_id = ?", new String[]{valueOf, String.valueOf(j)}) != 0) {
                z = true;
            }
            A02.close();
            return z;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0E(AbstractC15590nW r3, UserJid userJid) {
        StringBuilder sb = new StringBuilder("participant-user-store/removeGroupParticipant/");
        sb.append(r3);
        sb.append(" ");
        sb.append(userJid);
        Log.i(sb.toString());
        return A0D(r3, A01(userJid));
    }
}
