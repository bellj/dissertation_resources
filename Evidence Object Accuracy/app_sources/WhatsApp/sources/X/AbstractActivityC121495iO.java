package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextSwitcher;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiIncentivesValuePropsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsValuePropsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsValuePropsBottomSheetActivity;
import com.whatsapp.util.Log;

/* renamed from: X.5iO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121495iO extends AbstractActivityC121555iV {
    public C127425uT A00;
    public C122205l5 A01;
    public String A02;

    public void A30() {
        this.A01.A00.A09("valuePropsContinue");
        A34(this.A02);
        AbstractActivityC121495iO r2 = this.A00.A02;
        Intent A0D = C12990iw.A0D(r2, IndiaUpiPaymentsAccountSetupActivity.class);
        ((AbstractActivityC121665jA) r2).A0Q = true;
        r2.A2v(A0D);
        C35741ib.A00(A0D, "valuePropsContinue");
        r2.A2G(A0D, true);
    }

    public void A31() {
        String str;
        if (!(this instanceof IndiaUpiIncentivesValuePropsActivity)) {
            AbstractActivityC119235dO.A1j(this.A01, 4);
            AnonymousClass6BE r2 = ((AbstractActivityC121665jA) this).A0D;
            Integer A0V = C12960it.A0V();
            Integer A0h = C12970iu.A0h();
            if (!(this instanceof IndiaUpiPaymentsValuePropsBottomSheetActivity)) {
                str = !(this instanceof IndiaUpiPaymentsValuePropsActivity) ? "incentive_value_prop" : "payment_intro_screen";
            } else {
                str = "payment_intro_prompt";
            }
            r2.A02.A07(r2.A03(A0V, A0h, str, this.A02, this.A0g, this.A0f, C12960it.A1V(((AbstractActivityC121665jA) this).A02, 11)));
            return;
        }
        IndiaUpiIncentivesValuePropsActivity indiaUpiIncentivesValuePropsActivity = (IndiaUpiIncentivesValuePropsActivity) this;
        AbstractActivityC119235dO.A1j(((AbstractActivityC121495iO) indiaUpiIncentivesValuePropsActivity).A01, 4);
        AnonymousClass2SP A02 = ((AbstractActivityC121665jA) indiaUpiIncentivesValuePropsActivity).A0D.A02(C12960it.A0V(), C12970iu.A0h(), "incentive_value_prop", null);
        A02.A02 = Boolean.valueOf(AbstractActivityC119235dO.A1l(indiaUpiIncentivesValuePropsActivity));
        AbstractActivityC119235dO.A1b(A02, indiaUpiIncentivesValuePropsActivity);
    }

    public void A32(TextSwitcher textSwitcher) {
        int i = ((AbstractActivityC121665jA) this).A02;
        int i2 = R.string.payments_value_props_desc_text;
        if (i == 11) {
            i2 = R.string.payments_value_props_p2m_desc_text;
        }
        textSwitcher.setText(Html.fromHtml(getString(i2)));
        Context context = textSwitcher.getContext();
        Animation loadAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_in_left);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(context, R.anim.slide_out_right);
        textSwitcher.setInAnimation(loadAnimation);
        textSwitcher.setOutAnimation(loadAnimation2);
        C12960it.A1E(new AnonymousClass5oS(textSwitcher, this), ((ActivityC13830kP) this).A05);
    }

    public void A33(Long l) {
        String str;
        int i;
        AnonymousClass6BE r2 = ((AbstractActivityC121665jA) this).A0D;
        Integer A0i = C12980iv.A0i();
        if (!(this instanceof IndiaUpiPaymentsValuePropsBottomSheetActivity)) {
            str = !(this instanceof IndiaUpiPaymentsValuePropsActivity) ? "incentive_value_prop" : "payment_intro_screen";
        } else {
            str = "payment_intro_prompt";
        }
        AnonymousClass2SP A03 = r2.A03(A0i, null, str, this.A02, this.A0g, this.A0f, C12960it.A1V(((AbstractActivityC121665jA) this).A02, 11));
        if (l != null) {
            long longValue = l.longValue();
            if (longValue <= 10) {
                i = 1;
            } else if (longValue <= 15) {
                i = 2;
            } else {
                i = 4;
                if (longValue <= 20) {
                    i = 3;
                }
            }
            A03.A0A = Integer.valueOf(i);
            Log.i(C12960it.A0d(A03.toString(), C12960it.A0k("PAY: logContactBucketUserActionEvent event:")));
        }
        ((AbstractActivityC121665jA) this).A05.A07(A03);
    }

    public void A34(String str) {
        String str2;
        if (!(this instanceof IndiaUpiIncentivesValuePropsActivity)) {
            AnonymousClass6BE r2 = ((AbstractActivityC121665jA) this).A0D;
            Integer A0V = C12960it.A0V();
            if (!(this instanceof IndiaUpiPaymentsValuePropsBottomSheetActivity)) {
                str2 = !(this instanceof IndiaUpiPaymentsValuePropsActivity) ? "incentive_value_prop" : "payment_intro_screen";
            } else {
                str2 = "payment_intro_prompt";
            }
            r2.A02.A07(r2.A03(A0V, 36, str2, str, this.A0g, this.A0f, C12960it.A1V(((AbstractActivityC121665jA) this).A02, 11)));
            return;
        }
        IndiaUpiIncentivesValuePropsActivity indiaUpiIncentivesValuePropsActivity = (IndiaUpiIncentivesValuePropsActivity) this;
        AnonymousClass2SP A02 = ((AbstractActivityC121665jA) indiaUpiIncentivesValuePropsActivity).A0D.A02(C12960it.A0V(), C12980iv.A0k(), "incentive_value_prop", str);
        A02.A02 = Boolean.valueOf(AbstractActivityC119235dO.A1l(indiaUpiIncentivesValuePropsActivity));
        AbstractActivityC119235dO.A1b(A02, indiaUpiIncentivesValuePropsActivity);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
        A31();
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A02 = getIntent().getStringExtra("referral_screen");
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            A31();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        String str;
        super.onResume();
        this.A01.A00(getIntent());
        this.A01.A00.A09("valuePropsShown");
        C122205l5 r4 = this.A01;
        int i = ((AbstractActivityC121665jA) this).A03;
        long j = (long) ((AbstractActivityC121665jA) this).A02;
        String str2 = this.A02;
        boolean A1l = AbstractActivityC119235dO.A1l(this);
        AnonymousClass1Q5 r6 = r4.A00;
        if (i == 2) {
            str = "skip2fa";
        } else {
            str = "with2fa";
        }
        r6.A0A("setupMode", str, false);
        r6.A07.AKv(r6.A06.A05, "paymentsEntryPoint", j);
        if (str2 != null) {
            r6.A0A("referralScreen", str2, false);
        }
        r6.A0B("paymentsAccountExists", A1l, false);
    }
}
