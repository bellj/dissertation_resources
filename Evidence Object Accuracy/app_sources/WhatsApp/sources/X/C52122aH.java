package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.2aH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52122aH extends PrintDocumentAdapter {
    public Context A00;
    public PrintedPdfDocument A01;
    public final Bitmap A02;
    public final String A03 = "my_qrcode.pdf";

    public C52122aH(Context context, Bitmap bitmap) {
        this.A00 = context;
        this.A02 = bitmap;
    }

    @Override // android.print.PrintDocumentAdapter
    public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes2, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, Bundle bundle) {
        this.A01 = new PrintedPdfDocument(this.A00, printAttributes2);
        if (cancellationSignal.isCanceled()) {
            layoutResultCallback.onLayoutCancelled();
        } else {
            layoutResultCallback.onLayoutFinished(new PrintDocumentInfo.Builder(this.A03).setContentType(0).setPageCount(1).build(), true);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:21:0x0033 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: android.print.pdf.PrintedPdfDocument */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v4, types: [float] */
    /* JADX WARN: Type inference failed for: r3v5 */
    @Override // android.print.PrintDocumentAdapter
    public void onWrite(PageRange[] pageRangeArr, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
        PrintedPdfDocument printedPdfDocument;
        PdfDocument.Page startPage = this.A01.startPage(0);
        Canvas canvas = startPage.getCanvas();
        Bitmap bitmap = this.A02;
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        if (height > 0 && width > 0) {
            printedPdfDocument = ((float) bitmap.getWidth()) / ((float) bitmap.getHeight());
            float f = (float) width;
            float f2 = (float) height;
            if (f / f2 > printedPdfDocument) {
                width = (int) (f2 * printedPdfDocument);
            } else {
                height = (int) (f / printedPdfDocument);
            }
            bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
        }
        try {
            printedPdfDocument = 0;
            printedPdfDocument = 0;
            canvas.drawBitmap(bitmap, (float) ((canvas.getWidth() - bitmap.getWidth()) >> 1), (float) ((canvas.getHeight() - bitmap.getHeight()) >> 1), (Paint) null);
            this.A01.finishPage(startPage);
            try {
                this.A01.writeTo(new FileOutputStream(parcelFileDescriptor.getFileDescriptor()));
                this.A01.close();
                this.A01 = null;
                writeResultCallback.onWriteFinished(new PageRange[]{new PageRange(0, 0)});
            } catch (IOException e) {
                writeResultCallback.onWriteFailed(e.toString());
                this.A01.close();
                this.A01 = null;
            }
        } catch (Throwable th) {
            this.A01.close();
            this.A01 = printedPdfDocument;
            throw th;
        }
    }
}
