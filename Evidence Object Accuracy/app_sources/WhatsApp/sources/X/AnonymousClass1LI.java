package X;

import android.content.Context;
import org.whispersystems.jobqueue.Job;

/* renamed from: X.1LI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1LI extends Job implements AnonymousClass1LJ {
    public transient C20660w7 A00;
    public transient C14920mG A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1LI() {
        /*
            r4 = this;
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            r2 = 0
            com.whatsapp.jobqueue.requirement.ChatConnectionRequirement r0 = new com.whatsapp.jobqueue.requirement.ChatConnectionRequirement
            r0.<init>()
            r3.add(r0)
            r1 = 1
            org.whispersystems.jobqueue.JobParameters r0 = new org.whispersystems.jobqueue.JobParameters
            r0.<init>(r2, r3, r1)
            r4.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1LI.<init>():void");
    }

    @Override // X.AnonymousClass1LJ
    public void Abz(Context context) {
        AnonymousClass01J r1 = (AnonymousClass01J) AnonymousClass01M.A00(context.getApplicationContext(), AnonymousClass01J.class);
        this.A00 = (C20660w7) r1.AIB.get();
        this.A01 = (C14920mG) r1.ALr.get();
    }
}
