package X;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.34M  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34M extends AbstractC52962c8 {
    public AnonymousClass018 A00;
    public final Runnable A01;

    public AnonymousClass34M(Context context, Runnable runnable) {
        super(context);
        this.A01 = runnable;
        FrameLayout.inflate(context, R.layout.quoted_message, this);
        ((FrameLayout) AnonymousClass028.A0D(this, R.id.quoted_message_frame)).setForeground(AnonymousClass2GE.A03(context, C12970iu.A0C(context, R.drawable.balloon_incoming_frame), R.color.conversationEntryBackground));
        C42941w9.A08(AnonymousClass028.A0D(this, R.id.quoted_title_frame), this.A00, 0, context.getResources().getDimensionPixelSize(R.dimen.conversation_title_padding));
        AnonymousClass028.A0D(this, R.id.cancel).setVisibility(0);
        C12960it.A12(AnonymousClass028.A0D(this, R.id.cancel), this, 15);
        TextView A0I = C12960it.A0I(this, R.id.quoted_title);
        A0I.setTextSize(AnonymousClass1OY.A01(context.getResources(), this.A00));
        C27531Hw.A06(A0I);
    }
}
