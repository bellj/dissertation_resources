package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.ConstraintProxyUpdateReceiver;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.0Zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07610Zl implements AbstractC11960h9 {
    public static final String A03 = C06390Tk.A01("CommandHandler");
    public final Context A00;
    public final Object A01 = new Object();
    public final Map A02 = new HashMap();

    public C07610Zl(Context context) {
        this.A00 = context;
    }

    public void A00(Intent intent, C07620Zm r17, int i) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            C06390Tk.A00().A02(A03, String.format("Handling constraints changed %s", intent), new Throwable[0]);
            C05980Rs r6 = new C05980Rs(this.A00, r17, i);
            C07620Zm r8 = r6.A02;
            List<C004401z> AGQ = r8.A05.A04.A0B().AGQ();
            Context context = r6.A01;
            boolean z = false;
            boolean z2 = false;
            boolean z3 = false;
            boolean z4 = false;
            for (C004401z r0 : AGQ) {
                C004101u r1 = r0.A09;
                z |= r1.A04;
                z2 |= r1.A05;
                z3 |= r1.A07;
                boolean z5 = false;
                if (r1.A03 != EnumC004001t.NOT_REQUIRED) {
                    z5 = true;
                }
                z4 |= z5;
                if (z && z2 && z3 && z4) {
                    break;
                }
            }
            Intent intent2 = new Intent("androidx.work.impl.background.systemalarm.UpdateProxies");
            intent2.setComponent(new ComponentName(context, ConstraintProxyUpdateReceiver.class));
            intent2.putExtra("KEY_BATTERY_NOT_LOW_PROXY_ENABLED", z).putExtra("KEY_BATTERY_CHARGING_PROXY_ENABLED", z2).putExtra("KEY_STORAGE_NOT_LOW_PROXY_ENABLED", z3).putExtra("KEY_NETWORK_STATE_PROXY_ENABLED", z4);
            context.sendBroadcast(intent2);
            AnonymousClass0Zu r4 = r6.A03;
            r4.A01(AGQ);
            ArrayList arrayList = new ArrayList(AGQ.size());
            long currentTimeMillis = System.currentTimeMillis();
            for (C004401z r3 : AGQ) {
                String str = r3.A0E;
                if (currentTimeMillis >= r3.A00() && (!(!C004101u.A08.equals(r3.A09)) || r4.A02(str))) {
                    arrayList.add(r3);
                }
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                String str2 = ((C004401z) it.next()).A0E;
                Intent intent3 = new Intent(context, SystemAlarmService.class);
                intent3.setAction("ACTION_DELAY_MET");
                intent3.putExtra("KEY_WORKSPEC_ID", str2);
                C06390Tk.A00().A02(C05980Rs.A04, String.format("Creating a delay_met command for workSpec with id (%s)", str2), new Throwable[0]);
                r8.A03.post(new RunnableC09950dm(intent3, r8, r6.A00));
            }
            r4.A00();
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            C06390Tk.A00().A02(A03, String.format("Handling reschedule %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
            r17.A05.A04();
        } else {
            Bundle extras = intent.getExtras();
            String[] strArr = {"KEY_WORKSPEC_ID"};
            if (extras == null || extras.isEmpty() || extras.get(strArr[0]) == null) {
                C06390Tk.A00().A03(A03, String.format("Invalid request for %s, requires %s.", action, "KEY_WORKSPEC_ID"), new Throwable[0]);
            } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
                String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
                C06390Tk A00 = C06390Tk.A00();
                String str3 = A03;
                A00.A02(str3, String.format("Handling schedule work for %s", string), new Throwable[0]);
                AnonymousClass022 r11 = r17.A05;
                WorkDatabase workDatabase = r11.A04;
                workDatabase.A03();
                try {
                    C004401z AHn = workDatabase.A0B().AHn(string);
                    if (AHn == null) {
                        C06390Tk A002 = C06390Tk.A00();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Skipping scheduling ");
                        sb.append(string);
                        sb.append(" because it's no longer in the DB");
                        A002.A05(str3, sb.toString(), new Throwable[0]);
                    } else if (AHn.A0D.A00()) {
                        C06390Tk A003 = C06390Tk.A00();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Skipping scheduling ");
                        sb2.append(string);
                        sb2.append("because it is finished.");
                        A003.A05(str3, sb2.toString(), new Throwable[0]);
                    } else {
                        long A004 = AHn.A00();
                        if (!(!C004101u.A08.equals(AHn.A09))) {
                            C06390Tk.A00().A02(str3, String.format("Setting up Alarms for %s at %s", string, Long.valueOf(A004)), new Throwable[0]);
                            AnonymousClass0TJ.A00(this.A00, r11, string, A004);
                        } else {
                            C06390Tk.A00().A02(str3, String.format("Opportunistically setting an alarm for %s at %s", string, Long.valueOf(A004)), new Throwable[0]);
                            Context context2 = this.A00;
                            AnonymousClass0TJ.A00(context2, r11, string, A004);
                            Intent intent4 = new Intent(context2, SystemAlarmService.class);
                            intent4.setAction("ACTION_CONSTRAINTS_CHANGED");
                            r17.A03.post(new RunnableC09950dm(intent4, r17, i));
                        }
                        workDatabase.A05();
                    }
                } finally {
                    workDatabase.A04();
                }
            } else if ("ACTION_DELAY_MET".equals(action)) {
                Bundle extras2 = intent.getExtras();
                synchronized (this.A01) {
                    String string2 = extras2.getString("KEY_WORKSPEC_ID");
                    C06390Tk A005 = C06390Tk.A00();
                    String str4 = A03;
                    A005.A02(str4, String.format("Handing delay met for %s", string2), new Throwable[0]);
                    Map map = this.A02;
                    if (!map.containsKey(string2)) {
                        C07640Zo r62 = new C07640Zo(this.A00, r17, string2, i);
                        map.put(string2, r62);
                        Context context3 = r62.A04;
                        String str5 = r62.A08;
                        r62.A01 = AnonymousClass0RM.A00(context3, String.format("%s (%s)", str5, Integer.valueOf(r62.A03)));
                        C06390Tk A006 = C06390Tk.A00();
                        String str6 = C07640Zo.A09;
                        A006.A02(str6, String.format("Acquiring wakelock %s for WorkSpec %s", r62.A01, str5), new Throwable[0]);
                        r62.A01.acquire();
                        C004401z AHn2 = r62.A05.A05.A04.A0B().AHn(str5);
                        if (AHn2 == null) {
                            r62.A01();
                        } else {
                            boolean z6 = !C004101u.A08.equals(AHn2.A09);
                            r62.A02 = z6;
                            if (!z6) {
                                C06390Tk.A00().A02(str6, String.format("No constraints for %s", str5), new Throwable[0]);
                                r62.AM8(Collections.singletonList(str5));
                            } else {
                                r62.A06.A01(Collections.singletonList(AHn2));
                            }
                        }
                    } else {
                        C06390Tk.A00().A02(str4, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", string2), new Throwable[0]);
                    }
                }
            } else if ("ACTION_STOP_WORK".equals(action)) {
                String string3 = intent.getExtras().getString("KEY_WORKSPEC_ID");
                C06390Tk.A00().A02(A03, String.format("Handing stopWork work for %s", string3), new Throwable[0]);
                AnonymousClass022 r02 = r17.A05;
                r02.A09(string3);
                Context context4 = this.A00;
                AbstractC12580i9 A08 = r02.A04.A08();
                AnonymousClass0PC AH2 = A08.AH2(string3);
                if (AH2 != null) {
                    AnonymousClass0TJ.A01(context4, string3, AH2.A00);
                    C06390Tk.A00().A02(AnonymousClass0TJ.A00, String.format("Removing SystemIdInfo for workSpecId (%s)", string3), new Throwable[0]);
                    A08.AaR(string3);
                }
                r17.AQ1(string3, false);
            } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
                Bundle extras3 = intent.getExtras();
                String string4 = extras3.getString("KEY_WORKSPEC_ID");
                boolean z7 = extras3.getBoolean("KEY_NEEDS_RESCHEDULE");
                C06390Tk.A00().A02(A03, String.format("Handling onExecutionCompleted %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
                AQ1(string4, z7);
            } else {
                C06390Tk.A00().A05(A03, String.format("Ignoring intent %s", intent), new Throwable[0]);
            }
        }
    }

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        synchronized (this.A01) {
            AbstractC11960h9 r0 = (AbstractC11960h9) this.A02.remove(str);
            if (r0 != null) {
                r0.AQ1(str, z);
            }
        }
    }
}
