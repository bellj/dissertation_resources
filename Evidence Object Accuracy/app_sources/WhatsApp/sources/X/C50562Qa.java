package X;

/* renamed from: X.2Qa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50562Qa extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public Long A0A;
    public String A0B;

    public C50562Qa() {
        super(2296, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(14, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(13, this.A00);
        r3.Abe(5, this.A05);
        r3.Abe(4, this.A06);
        r3.Abe(2, this.A01);
        r3.Abe(7, this.A07);
        r3.Abe(8, this.A02);
        r3.Abe(1, this.A0B);
        r3.Abe(9, this.A08);
        r3.Abe(10, this.A09);
        r3.Abe(3, this.A0A);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamMdBootstrapHistoryDataDownloaded {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "historySyncStageProgress", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapChatsCount", this.A04);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapHistoryPayloadType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapMessagesCount", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapPayloadSize", this.A06);
        Integer num2 = this.A01;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapPayloadType", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapStepDuration", this.A07);
        Integer num3 = this.A02;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdBootstrapStepResult", obj3);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdSessionId", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdStorageQuotaBytes", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdStorageQuotaUsedBytes", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mdTimestamp", this.A0A);
        sb.append("}");
        return sb.toString();
    }
}
