package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.emoji.search.EmojiSearchContainer;

/* renamed from: X.3j0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74923j0 extends AbstractC018308n {
    public final /* synthetic */ int A00;
    public final /* synthetic */ EmojiSearchContainer A01;

    public C74923j0(EmojiSearchContainer emojiSearchContainer, int i) {
        this.A01 = emojiSearchContainer;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        int i = this.A00;
        rect.set(0, i, i, i);
    }
}
