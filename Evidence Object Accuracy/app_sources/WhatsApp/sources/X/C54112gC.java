package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;

/* renamed from: X.2gC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54112gC extends AnonymousClass02L {
    public final C48922Il A00;

    public C54112gC(C48922Il r3) {
        super(new AnonymousClass0SC(new C74603iN()).A00());
        this.A00 = r3;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC37191le r22 = (AbstractC37191le) r2;
        r22.A08();
        r22.A09(A0E(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (AnonymousClass39o.values()[i].ordinal()) {
            case 0:
                return new C59702vF(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.loading_row));
            case 32:
                return new C59812vQ(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.recent_search_row));
            case 35:
                C48922Il r2 = this.A00;
                View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.business_profile_recent_row);
                C48302Fl r0 = r2.A00;
                AnonymousClass01J r1 = r0.A03;
                return new C37181ld(A0F, AnonymousClass2FL.A00(r0.A01), C12990iw.A0Z(r1), C12970iu.A0W(r1));
            default:
                throw C12960it.A0U(C12960it.A0W(i, "SearchHistoryListAdapter/onCreateViewHolder type not handled: "));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((C37171lc) A0E(i)).A00.ordinal();
    }
}
