package X;

import java.util.concurrent.Callable;

/* renamed from: X.0ee  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10450ee implements Callable {
    public final /* synthetic */ C08860by A00;

    public CallableC10450ee(C08860by r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C08860by r2 = this.A00;
        synchronized (r2) {
            if (r2.A04 != null) {
                r2.A05();
                if (r2.A08()) {
                    r2.A06();
                    r2.A00 = 0;
                }
            }
        }
        return null;
    }
}
