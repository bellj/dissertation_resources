package X;

/* renamed from: X.3Td  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67853Td implements AbstractC116415Vi {
    public AnonymousClass28D A00;
    public final C14250l6 A01;
    public final String A02;

    public /* synthetic */ C67853Td(C14250l6 r1, String str) {
        this.A02 = str;
        this.A01 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r5.A0H().equals(r3) == false) goto L_0x0013;
     */
    @Override // X.AbstractC116415Vi
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass28D A62(X.AnonymousClass28D r5) {
        /*
            r4 = this;
            java.lang.String r3 = r4.A02
            java.lang.String r0 = r5.A0H()
            if (r0 == 0) goto L_0x0013
            java.lang.String r0 = r5.A0H()
            boolean r1 = r0.equals(r3)
            r0 = 1
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            java.lang.String r2 = "Multiple components with the same id found during reflow"
            if (r0 == 0) goto L_0x001e
            X.28D r0 = r4.A00
            if (r0 != 0) goto L_0x005d
            r4.A00 = r5
        L_0x001e:
            java.util.LinkedList r0 = r5.A05
            if (r0 == 0) goto L_0x0042
            java.util.Iterator r1 = r0.iterator()
        L_0x0026:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0042
            java.lang.Object r0 = r1.next()
            X.4RF r0 = (X.AnonymousClass4RF) r0
            java.lang.String r0 = r0.A03
            if (r0 == 0) goto L_0x0026
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0026
            X.28D r0 = r4.A00
            if (r0 != 0) goto L_0x0058
            r4.A00 = r5
        L_0x0042:
            X.28D r0 = r4.A00
            if (r0 == 0) goto L_0x0057
            r0 = 134(0x86, float:1.88E-43)
            X.0l1 r2 = r5.A0G(r0)
            if (r2 == 0) goto L_0x0057
            X.0l3 r1 = X.C14210l2.A02(r5)
            X.0l6 r0 = r4.A01
            r0.A01(r1, r2)
        L_0x0057:
            return r5
        L_0x0058:
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r2)
            throw r0
        L_0x005d:
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67853Td.A62(X.28D):X.28D");
    }

    @Override // X.AbstractC116415Vi
    public void AY4(AnonymousClass28D r2) {
        if (this.A00 == r2) {
            this.A00 = null;
        }
    }
}
