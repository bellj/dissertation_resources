package X;

import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;

/* renamed from: X.2nM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57622nM extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57622nM A0B;
    public static volatile AnonymousClass255 A0C;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public long A07;
    public C57532nD A08;
    public String A09 = "";
    public String A0A = "";

    static {
        C57622nM r0 = new C57622nM();
        A0B = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C82413va r1;
        switch (r15.ordinal()) {
            case 0:
                return A0B;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57622nM r2 = (C57622nM) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A09;
                int i2 = r2.A00;
                this.A09 = r7.Afy(str, r2.A09, A1R, C12960it.A1R(i2));
                this.A07 = r7.Afs(this.A07, r2.A07, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A06 = r7.Afp(this.A06, r2.A06, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A01 = r7.Afp(this.A01, r2.A01, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A0A = r7.Afy(this.A0A, r2.A0A, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A02 = r7.Afp(this.A02, r2.A02, C12960it.A1V(i & 32, 32), C12960it.A1V(i2 & 32, 32));
                this.A04 = r7.Afp(this.A04, r2.A04, C12960it.A1V(i & 64, 64), C12960it.A1V(i2 & 64, 64));
                this.A03 = r7.Afp(this.A03, r2.A03, C12960it.A1V(i & 128, 128), C12960it.A1V(i2 & 128, 128));
                this.A08 = (C57532nD) r7.Aft(this.A08, r2.A08);
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 512, 512);
                int i4 = this.A05;
                int i5 = r2.A00;
                this.A05 = r7.Afp(i4, r2.A05, A1V, C12960it.A1V(i5 & 512, 512));
                if (r7 == C463025i.A00) {
                    this.A00 = i3 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                String A0A = r72.A0A();
                                this.A00 = 1 | this.A00;
                                this.A09 = A0A;
                                break;
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                this.A00 |= 2;
                                this.A07 = r72.A06();
                                break;
                            case 24:
                                this.A00 |= 4;
                                this.A06 = r72.A02();
                                break;
                            case 32:
                                this.A00 |= 8;
                                this.A01 = r72.A02();
                                break;
                            case 42:
                                String A0A2 = r72.A0A();
                                this.A00 |= 16;
                                this.A0A = A0A2;
                                break;
                            case 53:
                                this.A00 |= 32;
                                this.A02 = r72.A01();
                                break;
                            case 61:
                                this.A00 |= 64;
                                this.A04 = r72.A01();
                                break;
                            case 69:
                                this.A00 |= 128;
                                this.A03 = r72.A01();
                                break;
                            case 74:
                                if ((this.A00 & 256) == 256) {
                                    r1 = (C82413va) this.A08.A0T();
                                } else {
                                    r1 = null;
                                }
                                C57532nD r0 = (C57532nD) AbstractC27091Fz.A0H(r72, r22, C57532nD.A06);
                                this.A08 = r0;
                                if (r1 != null) {
                                    this.A08 = (C57532nD) AbstractC27091Fz.A0C(r1, r0);
                                }
                                this.A00 |= 256;
                                break;
                            case 80:
                                int A02 = r72.A02();
                                if (A02 != 0 && A02 != 1) {
                                    super.A0X(10, A02);
                                    break;
                                } else {
                                    this.A00 |= 512;
                                    this.A05 = A02;
                                    break;
                                }
                            default:
                                if (A0a(r72, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57622nM();
            case 5:
                return new C82403vZ();
            case 6:
                break;
            case 7:
                if (A0C == null) {
                    synchronized (C57622nM.class) {
                        if (A0C == null) {
                            A0C = AbstractC27091Fz.A09(A0B);
                        }
                    }
                }
                return A0C;
            default:
                throw C12970iu.A0z();
        }
        return A0B;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A09, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A06(2, this.A07);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A02(3, this.A06, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A02(4, this.A01, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A0A, i2);
        }
        int i4 = this.A00;
        if ((i4 & 32) == 32) {
            i2 += 5;
        }
        if ((i4 & 64) == 64) {
            i2 += 5;
        }
        if ((i4 & 128) == 128) {
            i2 += 5;
        }
        if ((i4 & 256) == 256) {
            C57532nD r0 = this.A08;
            if (r0 == null) {
                r0 = C57532nD.A06;
            }
            i2 = AbstractC27091Fz.A08(r0, 9, i2);
        }
        if ((this.A00 & 512) == 512) {
            i2 = AbstractC27091Fz.A03(10, this.A05, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A09);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A07);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A06);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A01);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A0A);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0D(6, this.A02);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0D(7, this.A04);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0D(8, this.A03);
        }
        if ((this.A00 & 256) == 256) {
            C57532nD r0 = this.A08;
            if (r0 == null) {
                r0 = C57532nD.A06;
            }
            codedOutputStream.A0L(r0, 9);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0E(10, this.A05);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
