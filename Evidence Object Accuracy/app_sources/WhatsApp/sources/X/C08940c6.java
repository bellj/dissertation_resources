package X;

/* renamed from: X.0c6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08940c6 implements Comparable {
    public AnonymousClass0Cq A00;
    public AnonymousClass0QC A01;
    public final /* synthetic */ AnonymousClass0Cq A02;

    public C08940c6(AnonymousClass0Cq r1, AnonymousClass0Cq r2) {
        this.A02 = r1;
        this.A00 = r2;
    }

    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        return this.A01.A02 - ((AnonymousClass0QC) obj).A02;
    }

    @Override // java.lang.Object
    public String toString() {
        String str = "[ ";
        if (this.A01 != null) {
            int i = 0;
            do {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(this.A01.A09[i]);
                sb.append(" ");
                str = sb.toString();
                i++;
            } while (i < 9);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("] ");
            sb2.append(this.A01);
            return sb2.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("] ");
        sb2.append(this.A01);
        return sb2.toString();
    }
}
