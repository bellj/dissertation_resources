package X;

import android.content.Context;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.0yI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21980yI {
    public final C18790t3 A00;
    public final C16590pI A01;
    public final C15690nk A02;

    public C21980yI(C18790t3 r1, C16590pI r2, C15690nk r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public C44171yN A00() {
        File A02 = A02(false);
        if (!A02.exists() || !new File(A02, "thumbnails").exists()) {
            return null;
        }
        return new C44171yN(A03("dark"), A03("light"));
    }

    public File A01(String str) {
        File A02 = A02(false);
        if (A02.exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".jpg");
            File file = new File(A02, sb.toString());
            if (file.exists()) {
                return file;
            }
        }
        return null;
    }

    public File A02(boolean z) {
        File cacheDir;
        String str;
        Context context = this.A01.A00;
        if (!z) {
            cacheDir = context.getFilesDir();
            str = "downloadable/wallpaper";
        } else {
            cacheDir = context.getCacheDir();
            str = "downloadable/wallpaper_tmp";
        }
        return new File(cacheDir, str);
    }

    public final List A03(String str) {
        File[] listFiles;
        File A02 = A02(false);
        if (A02.exists()) {
            File file = new File(A02, "thumbnails");
            if (file.exists()) {
                File file2 = new File(file, str);
                if (file2.exists() && (listFiles = file2.listFiles()) != null) {
                    Arrays.sort(listFiles, new Comparator() { // from class: X.5CS
                        @Override // java.util.Comparator
                        public final int compare(Object obj, Object obj2) {
                            return ((File) obj).getName().compareTo(((File) obj2).getName());
                        }
                    });
                    return Arrays.asList(listFiles);
                }
            }
        }
        return new ArrayList();
    }
}
