package X;

import android.content.Context;
import android.os.Parcel;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Yu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC30781Yu implements AbstractC30791Yv {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final String A05;

    public AbstractC30781Yu(int i, Parcel parcel) {
        this.A00 = i;
        this.A04 = parcel.readString();
        this.A05 = parcel.readString();
        this.A02 = parcel.readInt();
        this.A01 = parcel.readInt();
        this.A03 = parcel.readInt();
    }

    public AbstractC30781Yu(String str, String str2, int i, int i2, int i3, int i4) {
        boolean z = false;
        AnonymousClass009.A0A("BasePaymentCurrency offset should be >= 1", i2 >= 1);
        AnonymousClass009.A0A("BasePaymentCurrency display exponent should be >= 0", i3 >= 0 ? true : z);
        this.A04 = str;
        this.A05 = str2;
        this.A00 = i;
        this.A02 = i2;
        this.A03 = i4;
        this.A01 = i3;
    }

    public AbstractC30781Yu(JSONObject jSONObject) {
        this.A04 = jSONObject.optString("code");
        this.A05 = jSONObject.optString("symbol");
        this.A00 = jSONObject.optInt("currencyType");
        this.A02 = jSONObject.optInt("offset");
        this.A03 = jSONObject.optInt("weight");
        this.A01 = jSONObject.optInt("displayExponent");
    }

    @Override // X.AbstractC30791Yv
    public /* synthetic */ CharSequence AA7(Context context, String str) {
        if (!(this instanceof C30801Yw)) {
            return str;
        }
        return AnonymousClass1Z4.A00(context, str);
    }

    @Override // X.AbstractC30791Yv
    public String AC1(AnonymousClass018 r4) {
        if (!(this instanceof C30771Yt)) {
            return this.A05;
        }
        String str = this.A04;
        String str2 = this.A05;
        if (!C30831Yz.A00.contains(str)) {
            return C30831Yz.A00(str).A02(r4);
        }
        return str2;
    }

    @Override // X.AbstractC30791Yv
    public JSONObject Aew() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("code", this.A04);
            jSONObject.put("symbol", this.A05);
            jSONObject.put("offset", this.A02);
            jSONObject.put("displayExponent", this.A01);
            jSONObject.put("weight", this.A03);
            jSONObject.put("currencyType", this.A00);
            return jSONObject;
        } catch (JSONException e) {
            Log.e("PAY: BasePaymentCurrency toJsonObject threw: ", e);
            return jSONObject;
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AbstractC30781Yu)) {
            return false;
        }
        AbstractC30781Yu r4 = (AbstractC30781Yu) obj;
        if (this.A04.equals(r4.A04) && this.A05.equals(r4.A05) && this.A00 == r4.A00 && this.A02 == r4.A02 && this.A01 == r4.A01 && this.A03 == r4.A03) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (this.A04.hashCode() * 31) + (this.A05.hashCode() * 31) + this.A00 + this.A02 + this.A01 + this.A03;
    }

    @Override // X.AbstractC30791Yv, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeString(this.A04);
        parcel.writeString(this.A05);
        parcel.writeInt(this.A02);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A03);
    }
}
