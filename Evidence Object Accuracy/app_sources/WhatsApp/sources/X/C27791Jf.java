package X;

import java.util.Arrays;

/* renamed from: X.1Jf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27791Jf {
    public static final C27791Jf A02 = new C27791Jf(EnumC27951Jw.A01, new byte[]{2});
    public static final C27791Jf A03 = new C27791Jf(EnumC27951Jw.A02, new byte[]{1});
    public final EnumC27951Jw A00;
    public final byte[] A01;

    public C27791Jf(EnumC27951Jw r1, byte[] bArr) {
        this.A01 = bArr;
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C27791Jf)) {
            return false;
        }
        C27791Jf r4 = (C27791Jf) obj;
        if (!Arrays.equals(this.A01, r4.A01) || this.A00 != r4.A00) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Arrays.hashCode(new Object[]{this.A00}) * 31) + Arrays.hashCode(this.A01);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncdOperation{bytes=");
        sb.append(Arrays.toString(this.A01));
        sb.append(", syncdOperation=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
