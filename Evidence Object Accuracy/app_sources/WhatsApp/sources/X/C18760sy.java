package X;

import android.database.Cursor;

/* renamed from: X.0sy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18760sy extends AbstractC18500sY {
    public final C18740sw A00;
    public final C18750sx A01;

    public C18760sy(C18740sw r3, C18750sx r4, C18480sW r5) {
        super(r5, "call_log", Integer.MIN_VALUE);
        this.A01 = r4;
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        throw new UnsupportedOperationException();
    }

    @Override // X.AbstractC18500sY
    public boolean A0V(C29001Pw r2) {
        this.A01.A07();
        return A0O();
    }
}
