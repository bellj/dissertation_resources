package X;

import android.view.View;
import android.view.animation.Animation;
import com.whatsapp.Conversation;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.2o7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58082o7 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ C58112oA A01;

    public C58082o7(View view, C58112oA r2) {
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AbstractC14670lq r2 = this.A01.A04;
        if (r2.A0P == null) {
            this.A00.setVisibility(0);
            AnonymousClass19A r1 = r2.A1H;
            Log.e("voicenote/voicenotecancelled");
            Iterator A00 = AbstractC16230of.A00(r1);
            while (A00.hasNext()) {
                AnonymousClass2SY r12 = (AnonymousClass2SY) A00.next();
                if (r12 instanceof AnonymousClass39I) {
                    Conversation conversation = ((AnonymousClass39I) r12).A00;
                    if (conversation.A0g != null) {
                        conversation.A09.setVisibility(8);
                    }
                }
            }
        }
    }
}
