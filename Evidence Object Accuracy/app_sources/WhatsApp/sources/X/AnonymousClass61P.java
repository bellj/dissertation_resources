package X;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.61P  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61P {
    public final C14900mE A00;
    public final C16170oZ A01;
    public final C15650ng A02;
    public final C14850m9 A03;
    public final C18610sj A04;
    public final C20320vZ A05;
    public final AbstractC14440lR A06;

    public AnonymousClass61P(C14900mE r1, C16170oZ r2, C15650ng r3, C14850m9 r4, C18610sj r5, C20320vZ r6, AbstractC14440lR r7) {
        this.A03 = r4;
        this.A00 = r1;
        this.A06 = r7;
        this.A01 = r2;
        this.A05 = r6;
        this.A02 = r3;
        this.A04 = r5;
    }

    public static void A00(AlertDialog.Builder builder, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, int i) {
        builder.setPositiveButton(i, onClickListener).setNegativeButton(R.string.remove, onClickListener2).create().show();
    }

    public static final void A01(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        boolean A03 = C18640sm.A03(context);
        int i = R.string.network_required;
        if (A03) {
            i = R.string.network_required_airplane_on;
        }
        builder.setMessage(i).setPositiveButton(R.string.ok, (DialogInterface.OnClickListener) null).create().show();
    }

    public final C28861Ph A02(AbstractC14640lm r13, UserJid userJid, String str, List list, long j) {
        AbstractC15340mz r6;
        C20320vZ r3 = this.A05;
        AnonymousClass009.A05(r13);
        if (j != 0) {
            r6 = this.A02.A0K.A00(j);
        } else {
            r6 = null;
        }
        C28861Ph A03 = r3.A03(null, r13, r6, str, list, 0, false);
        if (C15380n4.A0J(r13) && userJid != null) {
            A03.A0e(userJid);
        }
        return A03;
    }

    public void A03(Context context, C30821Yy r27, C30921Zi r28, AbstractC14640lm r29, UserJid userJid, AnonymousClass22U r31, AbstractC1311061h r32, String str, List list, long j) {
        UserJid userJid2 = userJid;
        r32.AaN();
        if (r31.A01 == 5 || (this.A03.A07(1084) && r31.A00 == 5)) {
            A01(context);
            return;
        }
        int i = r31.A01;
        if (i == 1 || i == 6 || !A04(r31)) {
            int i2 = r31.A01;
            if (i2 != 1 && i2 != 6) {
                A00(new AlertDialog.Builder(context).setTitle(R.string.payment_sticker_upload_failure_dialog_title).setMessage(R.string.payment_sticker_upload_failure_dialog_message), new DialogInterface.OnClickListener(r27, r28, r29, userJid2, r32, this, str, list, j) { // from class: X.62q
                    public final /* synthetic */ long A00;
                    public final /* synthetic */ C30821Yy A01;
                    public final /* synthetic */ C30921Zi A02;
                    public final /* synthetic */ AbstractC14640lm A03;
                    public final /* synthetic */ UserJid A04;
                    public final /* synthetic */ AbstractC1311061h A05;
                    public final /* synthetic */ AnonymousClass61P A06;
                    public final /* synthetic */ String A07;
                    public final /* synthetic */ List A08;

                    {
                        this.A06 = r6;
                        this.A07 = r7;
                        this.A08 = r8;
                        this.A03 = r3;
                        this.A04 = r4;
                        this.A00 = r9;
                        this.A01 = r1;
                        this.A02 = r2;
                        this.A05 = r5;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i3) {
                        AnonymousClass61P r7 = this.A06;
                        String str2 = this.A07;
                        List list2 = this.A08;
                        AbstractC14640lm r5 = this.A03;
                        UserJid userJid3 = this.A04;
                        long j2 = this.A00;
                        C30821Yy r3 = this.A01;
                        C30921Zi r4 = this.A02;
                        AbstractC1311061h r1 = this.A05;
                        r7.A06.Ab2(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0019: INVOKE  
                              (wrap: X.0lR : 0x0012: IGET  (r0v0 X.0lR A[REMOVE]) = (r7v0 'r7' X.61P) X.61P.A06 X.0lR)
                              (wrap: X.6K9 : 0x0016: CONSTRUCTOR  (r2v0 X.6K9 A[REMOVE]) = 
                              (r3v0 'r3' X.1Yy)
                              (r4v0 'r4' X.1Zi)
                              (r5v0 'r5' X.0lm)
                              (r6v0 'userJid3' com.whatsapp.jid.UserJid)
                              (r7v0 'r7' X.61P)
                              (r8v0 'str2' java.lang.String)
                              (r9v0 'list2' java.util.List)
                              (r10v0 'j2' long)
                             call: X.6K9.<init>(X.1Yy, X.1Zi, X.0lm, com.whatsapp.jid.UserJid, X.61P, java.lang.String, java.util.List, long):void type: CONSTRUCTOR)
                             type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.62q.onClick(android.content.DialogInterface, int):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0016: CONSTRUCTOR  (r2v0 X.6K9 A[REMOVE]) = 
                              (r3v0 'r3' X.1Yy)
                              (r4v0 'r4' X.1Zi)
                              (r5v0 'r5' X.0lm)
                              (r6v0 'userJid3' com.whatsapp.jid.UserJid)
                              (r7v0 'r7' X.61P)
                              (r8v0 'str2' java.lang.String)
                              (r9v0 'list2' java.util.List)
                              (r10v0 'j2' long)
                             call: X.6K9.<init>(X.1Yy, X.1Zi, X.0lm, com.whatsapp.jid.UserJid, X.61P, java.lang.String, java.util.List, long):void type: CONSTRUCTOR in method: X.62q.onClick(android.content.DialogInterface, int):void, file: classes4.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 15 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6K9, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 21 more
                            */
                        /*
                            this = this;
                            X.61P r7 = r12.A06
                            java.lang.String r8 = r12.A07
                            java.util.List r9 = r12.A08
                            X.0lm r5 = r12.A03
                            com.whatsapp.jid.UserJid r6 = r12.A04
                            long r10 = r12.A00
                            X.1Yy r3 = r12.A01
                            X.1Zi r4 = r12.A02
                            X.61h r1 = r12.A05
                            X.0lR r0 = r7.A06
                            X.6K9 r2 = new X.6K9
                            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                            r0.Ab2(r2)
                            r1.A9z()
                            return
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.DialogInterface$OnClickListenerC1314062q.onClick(android.content.DialogInterface, int):void");
                    }
                }, new DialogInterface.OnClickListener() { // from class: X.62I
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i3) {
                        AbstractC1311061h.this.AaQ();
                    }
                }, R.string.payment_sticker_upload_failure_dialog_send_without_sticker_action);
            } else if (A04(r31)) {
                A00(new AlertDialog.Builder(context).setTitle(R.string.payment_background_upload_failure_dialog_title).setMessage(R.string.payment_background_upload_failure_dialog_message), new DialogInterface.OnClickListener(r27, r29, userJid2, r31, r32, this) { // from class: X.62o
                    public final /* synthetic */ C30821Yy A00;
                    public final /* synthetic */ AbstractC14640lm A01;
                    public final /* synthetic */ UserJid A02;
                    public final /* synthetic */ AnonymousClass22U A03;
                    public final /* synthetic */ AbstractC1311061h A04;
                    public final /* synthetic */ AnonymousClass61P A05;

                    {
                        this.A05 = r6;
                        this.A04 = r5;
                        this.A01 = r2;
                        this.A02 = r3;
                        this.A00 = r1;
                        this.A03 = r4;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i3) {
                        AnonymousClass61P r9 = this.A05;
                        AbstractC1311061h r8 = this.A04;
                        AbstractC14640lm r7 = this.A01;
                        UserJid userJid3 = this.A02;
                        C30821Yy r5 = this.A00;
                        AnonymousClass22U r4 = this.A03;
                        r8.AaH();
                        C30061Vy r2 = r4.A03;
                        AnonymousClass009.A05(r2);
                        C18610sj r1 = r9.A04;
                        if (!C15380n4.A0J(r7)) {
                            userJid3 = UserJid.of(r7);
                        }
                        r1.A0I(r5, null, userJid3, r2);
                        r9.A00.A0H(new RunnableC135786Ju(r4, r8, r9, r2));
                    }
                }, new DialogInterface.OnClickListener() { // from class: X.62H
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i3) {
                        AbstractC1311061h.this.AaH();
                    }
                }, R.string.payment_background_upload_failure_dialog_send_without_background_action);
            } else {
                C30061Vy r2 = r31.A03;
                AnonymousClass009.A05(r2);
                C18610sj r1 = this.A04;
                if (!C15380n4.A0J(r29)) {
                    userJid2 = UserJid.of(r29);
                }
                r1.A0I(r27, r28, userJid2, r2);
                this.A00.A0H(new RunnableC135786Ju(r31, r32, this, r2));
            }
        } else {
            A00(new AlertDialog.Builder(context).setTitle(R.string.payment_media_upload_failure_dialog_title).setMessage(R.string.payment_media_upload_failure_dialog_message), new DialogInterface.OnClickListener(r27, r29, userJid2, r32, this, str, list, j) { // from class: X.62p
                public final /* synthetic */ long A00;
                public final /* synthetic */ C30821Yy A01;
                public final /* synthetic */ AbstractC14640lm A02;
                public final /* synthetic */ UserJid A03;
                public final /* synthetic */ AbstractC1311061h A04;
                public final /* synthetic */ AnonymousClass61P A05;
                public final /* synthetic */ String A06;
                public final /* synthetic */ List A07;

                {
                    this.A05 = r5;
                    this.A06 = r6;
                    this.A07 = r7;
                    this.A02 = r2;
                    this.A03 = r3;
                    this.A00 = r8;
                    this.A01 = r1;
                    this.A04 = r4;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    AnonymousClass61P r6 = this.A05;
                    String str2 = this.A06;
                    List list2 = this.A07;
                    AbstractC14640lm r4 = this.A02;
                    UserJid userJid3 = this.A03;
                    long j2 = this.A00;
                    C30821Yy r3 = this.A01;
                    AbstractC1311061h r12 = this.A04;
                    r6.A06.Ab2(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0017: INVOKE  
                          (wrap: X.0lR : 0x0010: IGET  (r0v0 X.0lR A[REMOVE]) = (r6v0 'r6' X.61P) X.61P.A06 X.0lR)
                          (wrap: X.6K7 : 0x0014: CONSTRUCTOR  (r2v0 X.6K7 A[REMOVE]) = 
                          (r3v0 'r3' X.1Yy)
                          (r4v0 'r4' X.0lm)
                          (r5v0 'userJid3' com.whatsapp.jid.UserJid)
                          (r6v0 'r6' X.61P)
                          (r7v0 'str2' java.lang.String)
                          (r8v0 'list2' java.util.List)
                          (r9v0 'j2' long)
                         call: X.6K7.<init>(X.1Yy, X.0lm, com.whatsapp.jid.UserJid, X.61P, java.lang.String, java.util.List, long):void type: CONSTRUCTOR)
                         type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.62p.onClick(android.content.DialogInterface, int):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0014: CONSTRUCTOR  (r2v0 X.6K7 A[REMOVE]) = 
                          (r3v0 'r3' X.1Yy)
                          (r4v0 'r4' X.0lm)
                          (r5v0 'userJid3' com.whatsapp.jid.UserJid)
                          (r6v0 'r6' X.61P)
                          (r7v0 'str2' java.lang.String)
                          (r8v0 'list2' java.util.List)
                          (r9v0 'j2' long)
                         call: X.6K7.<init>(X.1Yy, X.0lm, com.whatsapp.jid.UserJid, X.61P, java.lang.String, java.util.List, long):void type: CONSTRUCTOR in method: X.62p.onClick(android.content.DialogInterface, int):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6K7, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                        this = this;
                        X.61P r6 = r11.A05
                        java.lang.String r7 = r11.A06
                        java.util.List r8 = r11.A07
                        X.0lm r4 = r11.A02
                        com.whatsapp.jid.UserJid r5 = r11.A03
                        long r9 = r11.A00
                        X.1Yy r3 = r11.A01
                        X.61h r1 = r11.A04
                        X.0lR r0 = r6.A06
                        X.6K7 r2 = new X.6K7
                        r2.<init>(r3, r4, r5, r6, r7, r8, r9)
                        r0.Ab2(r2)
                        r1.A9z()
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.DialogInterface$OnClickListenerC1313962p.onClick(android.content.DialogInterface, int):void");
                }
            }, new DialogInterface.OnClickListener() { // from class: X.62G
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i3) {
                    AbstractC1311061h r0 = AbstractC1311061h.this;
                    r0.AaQ();
                    r0.AaH();
                }
            }, R.string.payment_media_upload_failure_dialog_send_without_media_action);
        }
    }

    public final boolean A04(AnonymousClass22U r4) {
        int i;
        return (!this.A03.A07(1084) || (i = r4.A00) == 7 || i == 1) ? false : true;
    }
}
