package X;

import android.view.View;

/* renamed from: X.0Df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02610Df extends AnonymousClass0Y9 {
    public final /* synthetic */ RunnableC08990cB A00;

    public C02610Df(RunnableC08990cB r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        LayoutInflater$Factory2C011505o r2 = this.A00.A00;
        r2.A0L.setAlpha(1.0f);
        r2.A0N.A09(null);
        r2.A0N = null;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMD(View view) {
        this.A00.A00.A0L.setVisibility(0);
    }
}
