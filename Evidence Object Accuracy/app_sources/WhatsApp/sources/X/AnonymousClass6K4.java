package X;

import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.6K4  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6K4 implements Runnable {
    public final /* synthetic */ AnonymousClass6LA A00;
    public final /* synthetic */ Exception A01;
    public final /* synthetic */ Object A02;
    public final /* synthetic */ ArrayList A03;
    public final /* synthetic */ boolean A04;

    public AnonymousClass6K4(AnonymousClass6LA r1, Exception exc, Object obj, ArrayList arrayList, boolean z) {
        this.A00 = r1;
        this.A04 = z;
        this.A03 = arrayList;
        this.A01 = exc;
        this.A02 = obj;
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean z = this.A04;
        if (z || !this.A03.isEmpty()) {
            Iterator it = this.A03.iterator();
            while (it.hasNext()) {
                AbstractC129405xf r1 = (AbstractC129405xf) it.next();
                if (z) {
                    r1.A01(this.A02);
                } else {
                    r1.A00(this.A01);
                }
            }
            return;
        }
        throw C117315Zl.A0J(this.A01);
    }
}
