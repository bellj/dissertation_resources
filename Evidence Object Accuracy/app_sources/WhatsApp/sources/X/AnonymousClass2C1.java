package X;

/* renamed from: X.2C1  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2C1 implements AbstractC20260vT, AbstractC47642Bu {
    public AnonymousClass1LC A00;

    public AnonymousClass2C1(C18640sm r1) {
        r1.A03(this);
    }

    @Override // X.AbstractC20260vT
    public void AOa(AnonymousClass1I1 r2) {
        AnonymousClass1LC r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AbstractC47642Bu
    public void AcI(AnonymousClass1LC r1) {
        this.A00 = r1;
    }
}
