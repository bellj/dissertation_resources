package X;

import java.security.Signature;

/* renamed from: X.5GM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GM implements AnonymousClass5VR {
    public final /* synthetic */ AbstractC113485Ht A00;

    public AnonymousClass5GM(AbstractC113485Ht r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VR
    public Signature A8Z(String str) {
        try {
            return Signature.getInstance(str, ((AnonymousClass5GT) this.A00.bcHelper).A00);
        } catch (Exception unused) {
            return Signature.getInstance(str);
        }
    }
}
