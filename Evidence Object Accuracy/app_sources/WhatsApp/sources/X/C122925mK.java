package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.5mK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122925mK extends C122985mQ {
    public final Drawable A00;
    public final CharSequence A01;
    public final CharSequence A02;
    public final Double A03;
    public final Double A04;
    public final String A05;

    public C122925mK(Drawable drawable, CharSequence charSequence, CharSequence charSequence2, Double d, Double d2, String str) {
        super(1011);
        this.A02 = charSequence;
        this.A01 = charSequence2;
        this.A00 = drawable;
        this.A05 = str;
        this.A03 = d;
        this.A04 = d2;
    }
}
