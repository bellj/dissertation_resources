package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.3HZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HZ {
    public static final AnonymousClass3HZ A07 = new AnonymousClass3HZ(1033, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, 1);
    public static final AnonymousClass3HZ A08 = new AnonymousClass3HZ(4201, 4096, 1);
    public static final AnonymousClass3HZ A09;
    public static final AnonymousClass3HZ A0A;
    public static final AnonymousClass3HZ A0B = new AnonymousClass3HZ(19, 16, 1);
    public static final AnonymousClass3HZ A0C;
    public static final AnonymousClass3HZ A0D;
    public static final AnonymousClass3HZ A0E = new AnonymousClass3HZ(285, 256, 0);
    public final int A00;
    public final int A01;
    public final int A02;
    public final C64403Fk A03;
    public final C64403Fk A04;
    public final int[] A05;
    public final int[] A06;

    static {
        AnonymousClass3HZ r4 = new AnonymousClass3HZ(67, 64, 1);
        A09 = r4;
        AnonymousClass3HZ r0 = new AnonymousClass3HZ(301, 256, 1);
        A0C = r0;
        A0A = r0;
        A0D = r4;
    }

    public AnonymousClass3HZ(int i, int i2, int i3) {
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
        int[] iArr = new int[i2];
        this.A05 = iArr;
        int[] iArr2 = new int[i2];
        this.A06 = iArr2;
        int i4 = 1;
        for (int i5 = 0; i5 < i2; i5++) {
            iArr[i5] = i4;
            i4 <<= 1;
            if (i4 >= i2) {
                i4 = (i4 ^ i) & (i2 - 1);
            }
        }
        for (int i6 = 0; i6 < i2 - 1; i6++) {
            iArr2[iArr[i6]] = i6;
        }
        this.A04 = new C64403Fk(this, new int[]{0});
        this.A03 = new C64403Fk(this, new int[]{1});
    }

    public int A00(int i) {
        if (i != 0) {
            return this.A05[(this.A02 - this.A06[i]) - 1];
        }
        throw new ArithmeticException();
    }

    public int A01(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0;
        }
        int[] iArr = this.A05;
        int[] iArr2 = this.A06;
        return iArr[(iArr2[i] + iArr2[i2]) % (this.A02 - 1)];
    }

    public C64403Fk A02(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.A04;
        } else {
            int[] iArr = new int[i + 1];
            iArr[0] = i2;
            return new C64403Fk(this, iArr);
        }
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("GF(0x");
        A0k.append(Integer.toHexString(this.A01));
        A0k.append(',');
        A0k.append(this.A02);
        return C12970iu.A0u(A0k);
    }
}
