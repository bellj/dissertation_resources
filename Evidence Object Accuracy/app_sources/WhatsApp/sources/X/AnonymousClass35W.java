package X;

import com.whatsapp.R;

/* renamed from: X.35W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35W extends AnonymousClass35X {
    public float A00;
    public int A01;
    public int A02 = 3;
    public int A03;
    public int A04;

    public AnonymousClass35W(AnonymousClass12P r3, C14330lG r4, C14900mE r5, C15450nH r6, AnonymousClass01d r7, AnonymousClass018 r8, AnonymousClass19M r9, C14850m9 r10, C244415n r11, AnonymousClass1CH r12, AbstractC15340mz r13, C48242Fd r14, AnonymousClass1CJ r15, AnonymousClass1AB r16, AnonymousClass19O r17, AbstractC14440lR r18, AnonymousClass1CI r19) {
        super(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19);
        this.A09.setId(R.id.status_playback_gif);
    }

    @Override // X.AnonymousClass35X, X.AbstractC33641ei
    public long A01() {
        int i = this.A02;
        if (i > 0) {
            return (long) (i * this.A03);
        }
        long A01 = super.A01();
        return ((long) A0L(A01)) * A01;
    }

    @Override // X.AnonymousClass35X, X.AbstractC33641ei
    public void A07() {
        this.A04 = 0;
        this.A01 = 0;
        this.A00 = 0.0f;
        super.A07();
        AnonymousClass21T r1 = ((AnonymousClass35X) this).A02;
        if (r1 != null) {
            r1.A01 = new AnonymousClass5V2() { // from class: X.3bp
                @Override // X.AnonymousClass5V2
                public final void AOT(AnonymousClass21T r5) {
                    AnonymousClass35W r3 = AnonymousClass35W.this;
                    if (r5.A01() != 0) {
                        r3.A04++;
                    }
                    r3.A01++;
                    if (r3.A03 == 0) {
                        int A02 = ((AnonymousClass35X) r3).A02.A02();
                        r3.A03 = A02;
                        if (A02 > 1) {
                            r3.A02 = r3.A0L((long) A02);
                        } else {
                            r3.A03 = 0;
                        }
                    }
                    int i = r3.A04;
                    int i2 = r3.A02;
                    if (i >= i2 || r3.A01 >= (i2 << 2)) {
                        ((AbstractC33641ei) r3).A05.A00();
                        return;
                    }
                    r3.A00 = 0.0f;
                    r5.A09(0);
                    r5.A07();
                }
            };
        }
    }

    @Override // X.AnonymousClass35X, X.AbstractC33641ei
    public void A08() {
        AnonymousClass21T r1 = ((AnonymousClass35X) this).A02;
        if (r1 != null) {
            r1.A01 = null;
        }
        super.A08();
    }

    public final int A0L(long j) {
        if (((AnonymousClass35X) this).A02 instanceof AnonymousClass39F) {
            return 1;
        }
        if (j == 0) {
            return 3;
        }
        return (int) Math.max(3L, 6000 / j);
    }
}
