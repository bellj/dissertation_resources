package X;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99114jm implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c == 2) {
                i2 = C95664e9.A02(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                intent = (Intent) C95664e9.A07(parcel, Intent.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78683pJ(i, i2, intent);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new C78683pJ[i];
    }
}
