package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.32t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618432t extends AbstractC75713kI {
    public C15370n3 A00;
    public final ImageView A01;
    public final TextEmojiLabel A02;
    public final TextEmojiLabel A03;
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618432t(View view, AbstractView$OnCreateContextMenuListenerC35851ir r4) {
        super(view, r4);
        this.A04 = r4;
        this.A02 = C12970iu.A0U(view, R.id.name);
        ImageView A0L = C12970iu.A0L(view, R.id.avatar);
        this.A01 = A0L;
        this.A03 = C12970iu.A0U(view, R.id.push_name);
        AnonymousClass028.A0a(A0L, 2);
    }

    @Override // X.AbstractC75713kI
    public void A08(C15370n3 r9, C30751Yr r10) {
        this.A00 = r9;
        C12960it.A14(this.A0H, this, r10, 16);
        C15370n3 r1 = this.A00;
        AbstractView$OnCreateContextMenuListenerC35851ir r5 = this.A04;
        C15570nT r0 = r5.A0z;
        r0.A08();
        boolean equals = r1.equals(r0.A01);
        TextEmojiLabel textEmojiLabel = this.A02;
        if (equals) {
            textEmojiLabel.setText(R.string.you);
            String A07 = C38131nZ.A07(r5.A1C, r5.A1J.A03(r5.A0c) - r5.A1A.A00());
            TextEmojiLabel textEmojiLabel2 = this.A03;
            textEmojiLabel2.setText(A07);
            textEmojiLabel2.setVisibility(0);
        } else {
            C15610nY r3 = r5.A17;
            textEmojiLabel.setText(r3.A04(this.A00));
            boolean A0L = r3.A0L(this.A00, -1);
            TextEmojiLabel textEmojiLabel3 = this.A03;
            if (A0L) {
                textEmojiLabel3.setVisibility(0);
                textEmojiLabel3.A0G(null, r3.A09(this.A00));
            } else {
                textEmojiLabel3.setVisibility(8);
            }
        }
        r5.A0b.A07(this.A01, this.A00, false);
    }
}
