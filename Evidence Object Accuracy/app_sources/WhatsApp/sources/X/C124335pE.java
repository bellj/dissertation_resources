package X;

import java.util.Map;

/* renamed from: X.5pE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124335pE extends AbstractC120005fS {
    @Override // X.AbstractC120005fS
    public String A01(C91064Qh r4, Map map) {
        String A01 = super.A00(r4, map);
        if (A01 != null) {
            return A01;
        }
        if (!map.containsKey(2498007)) {
            return null;
        }
        r4.A00 = 2;
        return ((AnonymousClass3H5) map.get(2498007)).A01;
    }
}
