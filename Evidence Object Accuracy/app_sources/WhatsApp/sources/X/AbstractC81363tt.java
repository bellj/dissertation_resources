package X;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: X.3tt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC81363tt extends AbstractC87814Dc implements AbstractFutureC44231yX {
    public static final AnonymousClass4Y6 A00;
    public static final Object A01 = C12970iu.A0l();
    public static final Logger A02 = C72463ee.A0M(AbstractC81363tt.class);
    public static final boolean A03;
    public volatile C94814ca listeners;
    public volatile Object value;
    public volatile C94804cZ waiters;

    static {
        boolean z;
        AnonymousClass4Y6 r6;
        Throwable th;
        try {
            z = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
        } catch (SecurityException unused) {
            z = false;
        }
        A03 = z;
        Throwable th2 = null;
        try {
            r6 = new C81343tr();
            th = null;
        } catch (Throwable th3) {
            th = th3;
            try {
                r6 = new C81333tq(AtomicReferenceFieldUpdater.newUpdater(C94804cZ.class, Thread.class, "thread"), AtomicReferenceFieldUpdater.newUpdater(C94804cZ.class, C94804cZ.class, "next"), AtomicReferenceFieldUpdater.newUpdater(AbstractC81363tt.class, C94804cZ.class, "waiters"), AtomicReferenceFieldUpdater.newUpdater(AbstractC81363tt.class, C94814ca.class, "listeners"), AtomicReferenceFieldUpdater.newUpdater(AbstractC81363tt.class, Object.class, "value"));
            } catch (Throwable th4) {
                th2 = th4;
                r6 = new C81323tp();
            }
        }
        A00 = r6;
        if (th2 != null) {
            Logger logger = A02;
            Level level = Level.SEVERE;
            logger.log(level, "UnsafeAtomicHelper is broken!", th);
            logger.log(level, "SafeAtomicHelper is broken!", th2);
        }
    }

    public static Object A00(Object obj) {
        if (obj instanceof C93254Zs) {
            Throwable th = ((C93254Zs) obj).A00;
            CancellationException cancellationException = new CancellationException("Task was cancelled.");
            cancellationException.initCause(th);
            throw cancellationException;
        } else if (obj instanceof C93214Zo) {
            throw new ExecutionException(((C93214Zo) obj).A00);
        } else if (obj == A01) {
            return null;
        } else {
            return obj;
        }
    }

    public static void A01(AbstractC81363tt r4) {
        C94804cZ r1;
        AnonymousClass4Y6 r2;
        C94814ca r12;
        C94814ca r3 = null;
        do {
            r1 = r4.waiters;
            r2 = A00;
        } while (!r2.A03(r1, C94804cZ.A00, r4));
        while (r1 != null) {
            Thread thread = r1.thread;
            if (thread != null) {
                r1.thread = null;
                LockSupport.unpark(thread);
            }
            r1 = r1.next;
        }
        do {
            r12 = r4.listeners;
        } while (!r2.A02(r12, C94814ca.A03, r4));
        while (r12 != null) {
            C94814ca r0 = r12.A00;
            r12.A00 = r3;
            r3 = r12;
            r12 = r0;
        }
        while (r3 != null) {
            C94814ca r22 = r3.A00;
            A02(r3.A01, r3.A02);
            r3 = r22;
        }
    }

    public static void A02(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e) {
            Logger logger = A02;
            Level level = Level.SEVERE;
            String valueOf = String.valueOf(runnable);
            String valueOf2 = String.valueOf(executor);
            StringBuilder A0t = C12980iv.A0t(valueOf.length() + 57 + valueOf2.length());
            A0t.append("RuntimeException while executing runnable ");
            A0t.append(valueOf);
            A0t.append(" with executor ");
            logger.log(level, C12960it.A0d(valueOf2, A0t), (Throwable) e);
        }
    }

    public final void A03(C94804cZ r6) {
        r6.thread = null;
        while (true) {
            C94804cZ r3 = this.waiters;
            if (r3 != C94804cZ.A00) {
                C94804cZ r2 = null;
                while (r3 != null) {
                    C94804cZ r1 = r3.next;
                    if (r3.thread != null) {
                        r2 = r3;
                    } else if (r2 != null) {
                        r2.next = r1;
                        if (r2.thread == null) {
                            break;
                        }
                    } else if (!A00.A03(r3, r1, this)) {
                        break;
                    }
                    r3 = r1;
                }
                return;
            }
            return;
        }
    }

    public void A04(Object obj) {
        if (A00.A04(this, null, obj)) {
            A01(this);
        }
    }

    @Override // X.AbstractFutureC44231yX
    public void A5i(Runnable runnable, Executor executor) {
        C94814ca r3;
        C94814ca r2;
        C28291Mn.A04(runnable, "Runnable was null.");
        C28291Mn.A04(executor, "Executor was null.");
        if (isDone() || (r3 = this.listeners) == (r2 = C94814ca.A03)) {
            A02(runnable, executor);
        }
        C94814ca r1 = new C94814ca(runnable, executor);
        do {
            r1.A00 = r3;
            if (!A00.A02(r3, r1, this)) {
                r3 = this.listeners;
            } else {
                return;
            }
        } while (r3 != r2);
        A02(runnable, executor);
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        C93254Zs r1;
        Object obj = this.value;
        if (obj != null) {
            return false;
        }
        if (A03) {
            r1 = new C93254Zs(new CancellationException("Future.cancel() was called."));
        } else if (z) {
            r1 = C93254Zs.A02;
        } else {
            r1 = C93254Zs.A01;
        }
        if (!A00.A04(this, obj, r1)) {
            return false;
        }
        A01(this);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002b, code lost:
        java.util.concurrent.locks.LockSupport.park(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0032, code lost:
        if (java.lang.Thread.interrupted() != false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0034, code lost:
        r0 = r4.value;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        if (r0 == null) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0039, code lost:
        A03(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        throw new java.lang.InterruptedException();
     */
    @Override // java.util.concurrent.Future
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get() {
        /*
            r4 = this;
            boolean r0 = java.lang.Thread.interrupted()
            if (r0 != 0) goto L_0x0042
            java.lang.Object r0 = r4.value
            if (r0 != 0) goto L_0x0026
            X.4cZ r3 = r4.waiters
            X.4cZ r2 = X.C94804cZ.A00
            if (r3 == r2) goto L_0x0024
            X.4cZ r1 = new X.4cZ
            r1.<init>()
        L_0x0015:
            X.4Y6 r0 = X.AbstractC81363tt.A00
            r0.A00(r1, r3)
            boolean r0 = r0.A03(r3, r1, r4)
            if (r0 != 0) goto L_0x002b
            X.4cZ r3 = r4.waiters
            if (r3 != r2) goto L_0x0015
        L_0x0024:
            java.lang.Object r0 = r4.value
        L_0x0026:
            java.lang.Object r0 = A00(r0)
            return r0
        L_0x002b:
            java.util.concurrent.locks.LockSupport.park(r4)
            boolean r0 = java.lang.Thread.interrupted()
            if (r0 != 0) goto L_0x0039
            java.lang.Object r0 = r4.value
            if (r0 == 0) goto L_0x002b
            goto L_0x0026
        L_0x0039:
            r4.A03(r1)
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        L_0x0042:
            java.lang.InterruptedException r0 = new java.lang.InterruptedException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC81363tt.get():java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        java.util.concurrent.locks.LockSupport.parkNanos(r15, java.lang.Math.min(r0, 2147483647999999999L));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005c, code lost:
        if (java.lang.Thread.interrupted() != false) goto L_0x0173;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        r0 = r15.value;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0060, code lost:
        if (r0 != null) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0062, code lost:
        r0 = r8 - java.lang.System.nanoTime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006a, code lost:
        if (r0 >= 1000) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x006c, code lost:
        A03(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d6, code lost:
        if (r2 > 1000) goto L_0x00d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0173, code lost:
        A03(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x017b, code lost:
        throw new java.lang.InterruptedException();
     */
    @Override // java.util.concurrent.Future
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object get(long r16, java.util.concurrent.TimeUnit r18) {
        /*
        // Method dump skipped, instructions count: 386
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC81363tt.get(long, java.util.concurrent.TimeUnit):java.lang.Object");
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        return this.value instanceof C93254Zs;
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return true & C12960it.A1W(this.value);
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        Object obj;
        String str2;
        String str3;
        StringBuilder A0h = C12960it.A0h();
        Class<?> cls = getClass();
        String name = cls.getName();
        if (name.startsWith("com.google.common.util.concurrent.")) {
            A0h.append(cls.getSimpleName());
        } else {
            A0h.append(name);
        }
        A0h.append('@');
        A0h.append(C72453ed.A0o(this));
        A0h.append("[status=");
        if (isCancelled()) {
            str = "CANCELLED";
        } else {
            if (!isDone()) {
                int length = A0h.length();
                A0h.append("PENDING");
                try {
                } catch (RuntimeException | StackOverflowError e) {
                    String valueOf = String.valueOf(e.getClass());
                    StringBuilder A0t = C12980iv.A0t(valueOf.length() + 38);
                    A0t.append("Exception thrown from implementation: ");
                    str3 = C12960it.A0d(valueOf, A0t);
                }
                if (this instanceof ScheduledFuture) {
                    long delay = ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS);
                    StringBuilder A0t2 = C12980iv.A0t(41);
                    A0t2.append("remaining delay=[");
                    A0t2.append(delay);
                    str3 = C12960it.A0d(" ms]", A0t2);
                    if (str3 != null) {
                        if (str3.isEmpty()) {
                        }
                        if (str3 != null) {
                            A0h.append(", info=[");
                            A0h.append(str3);
                            A0h.append("]");
                        }
                    }
                }
                if (isDone()) {
                    A0h.delete(length, A0h.length());
                }
                return C12960it.A0d("]", A0h);
            }
            boolean z = false;
            while (true) {
                try {
                    try {
                        obj = get();
                        break;
                    } catch (InterruptedException unused) {
                        z = true;
                    } catch (Throwable th) {
                        if (z) {
                            Thread.currentThread().interrupt();
                        }
                        throw th;
                    }
                } catch (CancellationException unused2) {
                    str = "CANCELLED";
                } catch (RuntimeException e2) {
                    A0h.append("UNKNOWN, cause=[");
                    A0h.append(e2.getClass());
                    str = " thrown from get()]";
                } catch (ExecutionException e3) {
                    A0h.append("FAILURE, cause=[");
                    A0h.append(e3.getCause());
                    A0h.append("]");
                }
            }
            if (z) {
                Thread.currentThread().interrupt();
            }
            A0h.append("SUCCESS, result=[");
            if (obj == null) {
                str2 = "null";
            } else if (obj == this) {
                str2 = "this future";
            } else {
                A0h.append(C12980iv.A0s(obj));
                A0h.append("@");
                str2 = C72453ed.A0o(obj);
            }
            A0h.append(str2);
            A0h.append("]");
            return C12960it.A0d("]", A0h);
        }
        A0h.append(str);
        return C12960it.A0d("]", A0h);
    }
}
