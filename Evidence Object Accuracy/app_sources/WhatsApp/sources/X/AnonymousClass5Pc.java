package X;

import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.5Pc  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5Pc extends C113415Hm {
    public int A00 = 5;
    public Set A01 = Collections.EMPTY_SET;

    @Override // X.C113415Hm
    public void A00(PKIXParameters pKIXParameters) {
        super.A00(pKIXParameters);
        if (pKIXParameters instanceof AnonymousClass5Pc) {
            AnonymousClass5Pc r1 = (AnonymousClass5Pc) pKIXParameters;
            this.A00 = r1.A00;
            this.A01 = new HashSet(r1.A01);
        }
        if (pKIXParameters instanceof PKIXBuilderParameters) {
            this.A00 = ((PKIXBuilderParameters) pKIXParameters).getMaxPathLength();
        }
    }

    public AnonymousClass5Pc(Set set, AbstractC117205Yy r3) {
        super(set);
        this.A07 = r3 != null ? (AbstractC117205Yy) r3.clone() : null;
    }

    @Override // X.C113415Hm, java.security.cert.PKIXParameters, java.security.cert.CertPathParameters, java.lang.Object
    public Object clone() {
        try {
            Set<TrustAnchor> trustAnchors = getTrustAnchors();
            AbstractC117205Yy r0 = this.A07;
            AnonymousClass5Pc r02 = new AnonymousClass5Pc(trustAnchors, r0 != null ? (AbstractC117205Yy) r0.clone() : null);
            r02.A00(this);
            return r02;
        } catch (Exception e) {
            throw C12990iw.A0m(e.getMessage());
        }
    }
}
