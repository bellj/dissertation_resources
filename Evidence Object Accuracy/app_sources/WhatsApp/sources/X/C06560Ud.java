package X;

import android.graphics.Matrix;
import com.whatsapp.voipcalling.CallLinkInfo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.xml.sax.Attributes;

/* renamed from: X.0Ud  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06560Ud {
    public int A00;
    public AbstractC12490i0 A01 = null;
    public AnonymousClass0Q5 A02 = null;
    public AnonymousClass0JX A03 = null;
    public StringBuilder A04 = null;
    public StringBuilder A05 = null;
    public boolean A06 = false;
    public boolean A07 = false;
    public boolean A08 = false;

    public static float A00(float f, float f2, float f3) {
        float f4;
        if (f3 < 0.0f) {
            f3 += 6.0f;
        }
        if (f3 >= 6.0f) {
            f3 -= 6.0f;
        }
        if (f3 < 1.0f) {
            f4 = (f2 - f) * f3;
        } else if (f3 < 3.0f) {
            return f2;
        } else {
            if (f3 >= 4.0f) {
                return f;
            }
            f4 = (f2 - f) * (4.0f - f3);
        }
        return f4 + f;
    }

    public static float A01(String str) {
        int length = str.length();
        if (length != 0) {
            return A02(str, length);
        }
        throw new C11160fq("Invalid float value (empty string)");
    }

    public static float A02(String str, int i) {
        float A00 = new AnonymousClass0SB().A00(str, 0, i);
        if (!Float.isNaN(A00)) {
            return A00;
        }
        StringBuilder sb = new StringBuilder("Invalid float value: ");
        sb.append(str);
        throw new C11160fq(sb.toString());
    }

    public static int A03(float f) {
        if (f < 0.0f) {
            return 0;
        }
        if (f > 255.0f) {
            return 255;
        }
        return Math.round(f);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r7 <= 0.5f) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A04(float r5, float r6, float r7) {
        /*
            r1 = 1135869952(0x43b40000, float:360.0)
            r2 = 0
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            float r5 = r5 % r1
            if (r0 >= 0) goto L_0x0009
            float r5 = r5 + r1
        L_0x0009:
            r0 = 1114636288(0x42700000, float:60.0)
            float r5 = r5 / r0
            r0 = 1120403456(0x42c80000, float:100.0)
            float r6 = r6 / r0
            float r7 = r7 / r0
            r1 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x005b
            r6 = 0
        L_0x0017:
            int r0 = (r7 > r2 ? 1 : (r7 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0028
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            r2 = 1065353216(0x3f800000, float:1.0)
            if (r0 > 0) goto L_0x0055
            r2 = r7
            r0 = 1056964608(0x3f000000, float:0.5)
            int r0 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x0055
        L_0x0028:
            float r6 = r6 + r1
            float r6 = r6 * r2
        L_0x002a:
            r1 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 * r1
            float r2 = r2 - r6
            float r0 = r5 + r1
            float r0 = A00(r2, r6, r0)
            float r4 = A00(r2, r6, r5)
            float r5 = r5 - r1
            float r3 = A00(r2, r6, r5)
            r2 = 1132462080(0x43800000, float:256.0)
            float r0 = r0 * r2
            int r0 = A03(r0)
            int r1 = r0 << 16
            float r4 = r4 * r2
            int r0 = A03(r4)
            int r0 = r0 << 8
            r1 = r1 | r0
            float r3 = r3 * r2
            int r0 = A03(r3)
            r0 = r0 | r1
            return r0
        L_0x0055:
            float r0 = r2 + r6
            float r6 = r6 * r2
            float r6 = r0 - r6
            goto L_0x002a
        L_0x005b:
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0017
            r6 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06560Ud.A04(float, float, float):int");
    }

    public static final Matrix A05(String str) {
        Matrix matrix = new Matrix();
        C06230Sr r7 = new C06230Sr(str);
        r7.A0C();
        while (!r7.A0D()) {
            int i = r7.A01;
            String str2 = r7.A03;
            int charAt = str2.charAt(i);
            while (true) {
                if ((charAt >= 97 && charAt <= 122) || (charAt >= 65 && charAt <= 90)) {
                    charAt = r7.A05();
                }
            }
            int i2 = r7.A01;
            while (C06230Sr.A01(charAt)) {
                charAt = r7.A05();
            }
            if (charAt == 40) {
                r7.A01++;
                String substring = str2.substring(i, i2);
                if (substring != null) {
                    char c = 65535;
                    switch (substring.hashCode()) {
                        case -1081239615:
                            if (substring.equals("matrix")) {
                                c = 0;
                                break;
                            }
                            break;
                        case -925180581:
                            if (substring.equals("rotate")) {
                                c = 1;
                                break;
                            }
                            break;
                        case 109250890:
                            if (substring.equals("scale")) {
                                c = 2;
                                break;
                            }
                            break;
                        case 109493390:
                            if (substring.equals("skewX")) {
                                c = 3;
                                break;
                            }
                            break;
                        case 109493391:
                            if (substring.equals("skewY")) {
                                c = 4;
                                break;
                            }
                            break;
                        case 1052832078:
                            if (substring.equals("translate")) {
                                c = 5;
                                break;
                            }
                            break;
                    }
                    switch (c) {
                        case 0:
                            r7.A0C();
                            float A02 = r7.A02();
                            r7.A0E();
                            float A022 = r7.A02();
                            r7.A0E();
                            float A023 = r7.A02();
                            r7.A0E();
                            float A024 = r7.A02();
                            r7.A0E();
                            float A025 = r7.A02();
                            r7.A0E();
                            float A026 = r7.A02();
                            r7.A0C();
                            if (!Float.isNaN(A026) && r7.A0F(')')) {
                                Matrix matrix2 = new Matrix();
                                matrix2.setValues(new float[]{A02, A023, A025, A022, A024, A026, 0.0f, 0.0f, 1.0f});
                                matrix.preConcat(matrix2);
                                break;
                            } else {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Invalid transform list: ");
                                sb.append(str);
                                throw new C11160fq(sb.toString());
                            }
                        case 1:
                            r7.A0C();
                            float A027 = r7.A02();
                            float A03 = r7.A03();
                            float A032 = r7.A03();
                            r7.A0C();
                            if (Float.isNaN(A027) || !r7.A0F(')')) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Invalid transform list: ");
                                sb2.append(str);
                                throw new C11160fq(sb2.toString());
                            } else if (Float.isNaN(A03)) {
                                matrix.preRotate(A027);
                                break;
                            } else if (!Float.isNaN(A032)) {
                                matrix.preRotate(A027, A03, A032);
                                break;
                            } else {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("Invalid transform list: ");
                                sb3.append(str);
                                throw new C11160fq(sb3.toString());
                            }
                        case 2:
                            r7.A0C();
                            float A028 = r7.A02();
                            float A033 = r7.A03();
                            r7.A0C();
                            if (!Float.isNaN(A028) && r7.A0F(')')) {
                                if (!Float.isNaN(A033)) {
                                    matrix.preScale(A028, A033);
                                    break;
                                } else {
                                    matrix.preScale(A028, A028);
                                    break;
                                }
                            } else {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("Invalid transform list: ");
                                sb4.append(str);
                                throw new C11160fq(sb4.toString());
                            }
                            break;
                        case 3:
                            r7.A0C();
                            float A029 = r7.A02();
                            r7.A0C();
                            if (!Float.isNaN(A029) && r7.A0F(')')) {
                                matrix.preSkew((float) Math.tan(Math.toRadians((double) A029)), 0.0f);
                                break;
                            } else {
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("Invalid transform list: ");
                                sb5.append(str);
                                throw new C11160fq(sb5.toString());
                            }
                        case 4:
                            r7.A0C();
                            float A0210 = r7.A02();
                            r7.A0C();
                            if (!Float.isNaN(A0210) && r7.A0F(')')) {
                                matrix.preSkew(0.0f, (float) Math.tan(Math.toRadians((double) A0210)));
                                break;
                            } else {
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append("Invalid transform list: ");
                                sb6.append(str);
                                throw new C11160fq(sb6.toString());
                            }
                            break;
                        case 5:
                            r7.A0C();
                            float A0211 = r7.A02();
                            float A034 = r7.A03();
                            r7.A0C();
                            if (!Float.isNaN(A0211) && r7.A0F(')')) {
                                if (!Float.isNaN(A034)) {
                                    matrix.preTranslate(A0211, A034);
                                    break;
                                } else {
                                    matrix.preTranslate(A0211, 0.0f);
                                    break;
                                }
                            } else {
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append("Invalid transform list: ");
                                sb7.append(str);
                                throw new C11160fq(sb7.toString());
                            }
                            break;
                        default:
                            StringBuilder sb8 = new StringBuilder("Invalid transform list fn: ");
                            sb8.append(substring);
                            sb8.append(")");
                            throw new C11160fq(sb8.toString());
                    }
                    if (r7.A0D()) {
                        return matrix;
                    }
                    r7.A0E();
                }
            }
            StringBuilder sb9 = new StringBuilder("Bad transform function encountered in transform list: ");
            sb9.append(str);
            throw new C11160fq(sb9.toString());
        }
        return matrix;
    }

    public static C03360Hm A06(String str) {
        int i;
        int i2;
        int i3;
        int i4;
        int A03;
        long j;
        int i5;
        int i6 = 5;
        if (str.charAt(0) == '#') {
            int length = str.length();
            C05860Rg r8 = null;
            if (1 < length) {
                long j2 = 0;
                int i7 = 1;
                while (i7 < length) {
                    char charAt = str.charAt(i7);
                    if (charAt < '0' || charAt > '9') {
                        if (charAt < 'A' || charAt > 'F') {
                            if (charAt < 'a' || charAt > 'f') {
                                break;
                            }
                            j = j2 * 16;
                            i5 = charAt - 'a';
                        } else {
                            j = j2 * 16;
                            i5 = charAt - 'A';
                        }
                        j2 = j + ((long) i5) + 10;
                    } else {
                        j2 = (j2 * 16) + ((long) (charAt - '0'));
                    }
                    if (j2 > 4294967295L) {
                        break;
                    }
                    i7++;
                }
                if (i7 != 1) {
                    r8 = new C05860Rg(j2, i7);
                }
            }
            if (r8 != null) {
                int i8 = r8.A00;
                if (i8 == 4) {
                    int i9 = (int) r8.A01;
                    int i10 = i9 & 3840;
                    int i11 = i9 & 240;
                    int i12 = i9 & 15;
                    return new C03360Hm(i12 | (i10 << 8) | -16777216 | (i10 << 12) | (i11 << 8) | (i11 << 4) | (i12 << 4));
                } else if (i8 == 5) {
                    int i13 = (int) r8.A01;
                    int i14 = 61440 & i13;
                    int i15 = i13 & 3840;
                    int i16 = i13 & 240;
                    int i17 = i13 & 15;
                    return new C03360Hm((i17 << 24) | (i17 << 28) | (i14 << 8) | (i14 << 4) | (i15 << 4) | i15 | i16 | (i16 >> 4));
                } else if (i8 == 7) {
                    i4 = (int) r8.A01;
                    i = i4 | -16777216;
                } else if (i8 == 9) {
                    int i18 = (int) r8.A01;
                    i3 = i18 << 24;
                    i2 = i18 >>> 8;
                    i = i2 | i3;
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Bad hex colour value: ");
                    sb.append(str);
                    throw new C11160fq(sb.toString());
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Bad hex colour value: ");
                sb2.append(str);
                throw new C11160fq(sb2.toString());
            }
        } else {
            String lowerCase = str.toLowerCase(Locale.US);
            boolean startsWith = lowerCase.startsWith("rgba(");
            if (!startsWith) {
                if (lowerCase.startsWith("rgb(")) {
                    i6 = 4;
                } else {
                    boolean startsWith2 = lowerCase.startsWith("hsla(");
                    if (!startsWith2) {
                        if (lowerCase.startsWith("hsl(")) {
                            i6 = 4;
                        } else {
                            Number number = (Number) AnonymousClass0M1.A00.get(lowerCase);
                            if (number != null) {
                                return new C03360Hm(number.intValue());
                            }
                            StringBuilder sb3 = new StringBuilder("Invalid colour keyword: ");
                            sb3.append(lowerCase);
                            throw new C11160fq(sb3.toString());
                        }
                    }
                    C06230Sr r6 = new C06230Sr(str.substring(i6));
                    r6.A0C();
                    float A02 = r6.A02();
                    float A04 = r6.A04(A02);
                    if (!Float.isNaN(A04)) {
                        r6.A0F('%');
                    }
                    float A042 = r6.A04(A04);
                    boolean isNaN = Float.isNaN(A042);
                    if (!isNaN) {
                        r6.A0F('%');
                    }
                    if (startsWith2) {
                        float A043 = r6.A04(A042);
                        r6.A0C();
                        if (Float.isNaN(A043) || !r6.A0F(')')) {
                            StringBuilder sb4 = new StringBuilder("Bad hsla() colour value: ");
                            sb4.append(str);
                            throw new C11160fq(sb4.toString());
                        }
                        i2 = A03(A043 * 256.0f) << 24;
                        i3 = A04(A02, A04, A042);
                        i = i2 | i3;
                    } else {
                        r6.A0C();
                        if (isNaN || !r6.A0F(')')) {
                            StringBuilder sb5 = new StringBuilder("Bad hsl() colour value: ");
                            sb5.append(str);
                            throw new C11160fq(sb5.toString());
                        }
                        i4 = A04(A02, A04, A042);
                        i = i4 | -16777216;
                    }
                }
            }
            C06230Sr r62 = new C06230Sr(str.substring(i6));
            r62.A0C();
            float A022 = r62.A02();
            if (!Float.isNaN(A022) && r62.A0F('%')) {
                A022 = (A022 * 256.0f) / 100.0f;
            }
            float A044 = r62.A04(A022);
            if (!Float.isNaN(A044) && r62.A0F('%')) {
                A044 = (A044 * 256.0f) / 100.0f;
            }
            float A045 = r62.A04(A044);
            if (!Float.isNaN(A045) && r62.A0F('%')) {
                A045 = (A045 * 256.0f) / 100.0f;
            }
            if (startsWith) {
                float A046 = r62.A04(A045);
                r62.A0C();
                if (Float.isNaN(A046) || !r62.A0F(')')) {
                    StringBuilder sb6 = new StringBuilder("Bad rgba() colour value: ");
                    sb6.append(str);
                    throw new C11160fq(sb6.toString());
                }
                A03 = (A03(A046 * 256.0f) << 24) | (A03(A022) << 16);
            } else {
                r62.A0C();
                if (Float.isNaN(A045) || !r62.A0F(')')) {
                    StringBuilder sb7 = new StringBuilder("Bad rgb() colour value: ");
                    sb7.append(str);
                    throw new C11160fq(sb7.toString());
                }
                A03 = (A03(A022) << 16) | -16777216;
            }
            i2 = A03 | (A03(A044) << 8);
            i3 = A03(A045);
            i = i2 | i3;
        }
        return new C03360Hm(i);
    }

    public static C08910c3 A07(C06230Sr r1) {
        if (r1.A0G("auto")) {
            return new C08910c3(0.0f);
        }
        return r1.A06();
    }

    public static C08910c3 A08(String str) {
        int length = str.length();
        if (length != 0) {
            AnonymousClass0JP r3 = AnonymousClass0JP.px;
            int i = length - 1;
            char charAt = str.charAt(i);
            if (charAt == '%') {
                length = i;
                r3 = AnonymousClass0JP.percent;
            } else if (length > 2 && Character.isLetter(charAt)) {
                int i2 = length - 2;
                if (Character.isLetter(str.charAt(i2))) {
                    length = i2;
                    try {
                        r3 = AnonymousClass0JP.valueOf(str.substring(i2).toLowerCase(Locale.US));
                    } catch (IllegalArgumentException unused) {
                        StringBuilder sb = new StringBuilder("Invalid length unit specifier: ");
                        sb.append(str);
                        throw new C11160fq(sb.toString());
                    }
                }
            }
            try {
                return new C08910c3(r3, A02(str, length));
            } catch (NumberFormatException e) {
                StringBuilder sb2 = new StringBuilder("Invalid length value: ");
                sb2.append(str);
                throw new C11160fq(sb2.toString(), e);
            }
        } else {
            throw new C11160fq("Invalid length value (empty string)");
        }
    }

    public static AnonymousClass0JK A09(String str) {
        switch (str.hashCode()) {
            case -1657669071:
                if (str.equals("oblique")) {
                    return AnonymousClass0JK.Oblique;
                }
                return null;
            case -1178781136:
                if (str.equals("italic")) {
                    return AnonymousClass0JK.Italic;
                }
                return null;
            case -1039745817:
                if (str.equals("normal")) {
                    return AnonymousClass0JK.Normal;
                }
                return null;
            default:
                return null;
        }
    }

    public static AbstractC08890c1 A0A(String str) {
        if (str.startsWith("url(")) {
            int indexOf = str.indexOf(")");
            AbstractC08890c1 r3 = null;
            if (indexOf == -1) {
                return new C03340Hk(null, str.substring(4).trim());
            }
            String trim = str.substring(4, indexOf).trim();
            String trim2 = str.substring(indexOf + 1).trim();
            if (trim2.length() > 0) {
                if (trim2.equals("none")) {
                    r3 = C03360Hm.A02;
                } else if (!trim2.equals("currentColor")) {
                    try {
                        r3 = A06(trim2);
                    } catch (C11160fq unused) {
                    }
                } else {
                    r3 = C03350Hl.A00;
                }
            }
            return new C03340Hk(r3, trim);
        } else if (str.equals("none")) {
            return C03360Hm.A02;
        } else {
            if (str.equals("currentColor")) {
                return C03350Hl.A00;
            }
            try {
                return A06(str);
            } catch (C11160fq unused2) {
                return null;
            }
        }
    }

    public static Float A0B(String str) {
        try {
            float A01 = A01(str);
            if (A01 < 0.0f) {
                A01 = 0.0f;
            } else if (A01 > 1.0f) {
                A01 = 1.0f;
            }
            return Float.valueOf(A01);
        } catch (C11160fq unused) {
            return null;
        }
    }

    public static String A0C(String str) {
        String substring;
        if (str.equals("none") || !str.startsWith("url(")) {
            return null;
        }
        if (str.endsWith(")")) {
            substring = str.substring(4, str.length() - 1);
        } else {
            substring = str.substring(4);
        }
        return substring.trim();
    }

    public static List A0D(String str) {
        C06230Sr r3 = new C06230Sr(str);
        ArrayList arrayList = null;
        do {
            String A0A = r3.A0A();
            if (A0A == null && (A0A = r3.A0B(',', true)) == null) {
                return arrayList;
            }
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(A0A);
            r3.A0E();
        } while (!r3.A0D());
        return arrayList;
    }

    public static List A0E(String str) {
        if (str.length() != 0) {
            ArrayList arrayList = new ArrayList(1);
            C06230Sr r4 = new C06230Sr(str);
            r4.A0C();
            while (!r4.A0D()) {
                float A02 = r4.A02();
                if (!Float.isNaN(A02)) {
                    AnonymousClass0JP A07 = r4.A07();
                    if (A07 == null) {
                        A07 = AnonymousClass0JP.px;
                    }
                    arrayList.add(new C08910c3(A07, A02));
                    r4.A0E();
                } else {
                    StringBuilder sb = new StringBuilder("Invalid length list value: ");
                    int i = r4.A01;
                    int i2 = i;
                    while (!r4.A0D() && !C06230Sr.A01(r4.A03.charAt(i2))) {
                        i2 = r4.A01 + 1;
                        r4.A01 = i2;
                    }
                    sb.append(r4.A03.substring(i, r4.A01));
                    throw new C11160fq(sb.toString());
                }
            }
            return arrayList;
        }
        throw new C11160fq("Invalid length list (empty string)");
    }

    public static final void A0F(AnonymousClass0HQ r7, String str, Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            if (EnumC03880Jm.A00(attributes.getLocalName(i)) == EnumC03880Jm.points) {
                C06230Sr r6 = new C06230Sr(attributes.getValue(i));
                ArrayList arrayList = new ArrayList();
                r6.A0C();
                while (!r6.A0D()) {
                    float A02 = r6.A02();
                    if (!Float.isNaN(A02)) {
                        r6.A0E();
                        float A022 = r6.A02();
                        if (!Float.isNaN(A022)) {
                            r6.A0E();
                            arrayList.add(Float.valueOf(A02));
                            arrayList.add(Float.valueOf(A022));
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Invalid <");
                            sb.append(str);
                            sb.append("> points attribute. There should be an even number of coordinates.");
                            throw new C11160fq(sb.toString());
                        }
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Invalid <");
                        sb2.append(str);
                        sb2.append("> points attribute. Non-coordinate content found in list.");
                        throw new C11160fq(sb2.toString());
                    }
                }
                r7.A00 = new float[arrayList.size()];
                Iterator it = arrayList.iterator();
                int i2 = 0;
                while (it.hasNext()) {
                    i2++;
                    r7.A00[i2] = ((Number) it.next()).floatValue();
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x011f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0G(X.C08900c2 r8, java.lang.String r9, java.lang.String r10) {
        /*
        // Method dump skipped, instructions count: 1604
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06560Ud.A0G(X.0c2, java.lang.String, java.lang.String):void");
    }

    public static final void A0H(AbstractC12720iO r7, Attributes attributes) {
        String str;
        Set hashSet;
        for (int i = 0; i < attributes.getLength(); i++) {
            String trim = attributes.getValue(i).trim();
            switch (EnumC03880Jm.A00(attributes.getLocalName(i)).ordinal()) {
                case 52:
                    C06230Sr r5 = new C06230Sr(trim);
                    HashSet hashSet2 = new HashSet();
                    while (!r5.A0D()) {
                        String A0B = r5.A0B(' ', false);
                        if (A0B.startsWith("http://www.w3.org/TR/SVG11/feature#")) {
                            str = A0B.substring(35);
                        } else {
                            str = "UNSUPPORTED";
                        }
                        hashSet2.add(str);
                        r5.A0C();
                    }
                    r7.Ack(hashSet2);
                    break;
                case 53:
                    r7.Acj(trim);
                    break;
                case 54:
                    C06230Sr r2 = new C06230Sr(trim);
                    HashSet hashSet3 = new HashSet();
                    while (!r2.A0D()) {
                        hashSet3.add(r2.A0B(' ', false));
                        r2.A0C();
                    }
                    r7.Acm(hashSet3);
                    break;
                case 55:
                    List A0D = A0D(trim);
                    if (A0D != null) {
                        hashSet = new HashSet(A0D);
                    } else {
                        hashSet = new HashSet(0);
                    }
                    r7.Acl(hashSet);
                    break;
                case 73:
                    C06230Sr r6 = new C06230Sr(trim);
                    HashSet hashSet4 = new HashSet();
                    while (!r6.A0D()) {
                        String A0B2 = r6.A0B(' ', false);
                        int indexOf = A0B2.indexOf(45);
                        if (indexOf != -1) {
                            A0B2 = A0B2.substring(0, indexOf);
                        }
                        hashSet4.add(new Locale(A0B2, "", "").getLanguage());
                        r6.A0C();
                    }
                    r7.Acy(hashSet4);
                    break;
            }
        }
    }

    public static final void A0I(AnonymousClass0I1 r3, Attributes attributes) {
        Boolean bool;
        for (int i = 0; i < attributes.getLength(); i++) {
            String qName = attributes.getQName(i);
            if (qName.equals("id") || qName.equals("xml:id")) {
                r3.A03 = attributes.getValue(i).trim();
                return;
            } else if (qName.equals("xml:space")) {
                String trim = attributes.getValue(i).trim();
                if (CallLinkInfo.DEFAULT_CALL_LINK_CALL_ID.equals(trim)) {
                    bool = Boolean.FALSE;
                } else if ("preserve".equals(trim)) {
                    bool = Boolean.TRUE;
                } else {
                    StringBuilder sb = new StringBuilder("Invalid value for \"xml:space\" attribute: ");
                    sb.append(trim);
                    throw new C11160fq(sb.toString());
                }
                r3.A02 = bool;
                return;
            }
        }
    }

    public static final void A0J(AnonymousClass0I1 r6, Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            String trim = attributes.getValue(i).trim();
            if (trim.length() != 0) {
                switch (EnumC03880Jm.A00(attributes.getLocalName(i)).ordinal()) {
                    case 0:
                        AnonymousClass0I7 r3 = new AnonymousClass0I7(trim);
                        ArrayList arrayList = null;
                        while (!r3.A0D()) {
                            String A0B = r3.A0B(' ', false);
                            if (A0B != null) {
                                if (arrayList == null) {
                                    arrayList = new ArrayList();
                                }
                                arrayList.add(A0B);
                                r3.A0C();
                            }
                        }
                        r6.A04 = arrayList;
                        continue;
                    case C43951xu.A02 /* 72 */:
                        C06230Sr r5 = new C06230Sr(trim.replaceAll("/\\*.*?\\*/", ""));
                        while (true) {
                            String A0B2 = r5.A0B(':', false);
                            r5.A0C();
                            if (r5.A0F(':')) {
                                r5.A0C();
                                String A0B3 = r5.A0B(';', true);
                                if (A0B3 != null) {
                                    r5.A0C();
                                    if (r5.A0D() || r5.A0F(';')) {
                                        C08900c2 r0 = r6.A01;
                                        if (r0 == null) {
                                            r0 = new C08900c2();
                                            r6.A01 = r0;
                                        }
                                        A0G(r0, A0B2, A0B3);
                                        r5.A0C();
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                        break;
                    default:
                        C08900c2 r2 = r6.A00;
                        if (r2 == null) {
                            r2 = new C08900c2();
                            r6.A00 = r2;
                        }
                        A0G(r2, attributes.getLocalName(i), attributes.getValue(i).trim());
                        continue;
                }
            }
        }
    }

    public static void A0K(AbstractC03240Ha r4, String str) {
        C06230Sr r3 = new C06230Sr(str);
        r3.A0C();
        String A0B = r3.A0B(' ', false);
        if ("defer".equals(A0B)) {
            r3.A0C();
            A0B = r3.A0B(' ', false);
        }
        EnumC03780Jc r2 = (EnumC03780Jc) AnonymousClass0M0.A00.get(A0B);
        AnonymousClass0JA r1 = null;
        r3.A0C();
        if (!r3.A0D()) {
            String A0B2 = r3.A0B(' ', false);
            if (A0B2.equals("meet")) {
                r1 = AnonymousClass0JA.meet;
            } else if (A0B2.equals("slice")) {
                r1 = AnonymousClass0JA.slice;
            } else {
                StringBuilder sb = new StringBuilder("Invalid preserveAspectRatio definition: ");
                sb.append(str);
                throw new C11160fq(sb.toString());
            }
        }
        r4.A00 = new AnonymousClass0SV(r2, r1);
    }

    public static final void A0L(AbstractC03420Hs r7, Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            String trim = attributes.getValue(i).trim();
            switch (EnumC03880Jm.A00(attributes.getLocalName(i)).ordinal()) {
                case 48:
                    A0K(r7, trim);
                    break;
                case 80:
                    C06230Sr r0 = new C06230Sr(trim);
                    r0.A0C();
                    float A02 = r0.A02();
                    r0.A0E();
                    float A022 = r0.A02();
                    r0.A0E();
                    float A023 = r0.A02();
                    r0.A0E();
                    float A024 = r0.A02();
                    if (Float.isNaN(A02) || Float.isNaN(A022) || Float.isNaN(A023) || Float.isNaN(A024)) {
                        throw new C11160fq("Invalid viewBox definition - should have four numbers");
                    } else if (A023 < 0.0f) {
                        throw new C11160fq("Invalid viewBox. width cannot be negative");
                    } else if (A024 >= 0.0f) {
                        r7.A00 = new AnonymousClass0SG(A02, A022, A023, A024);
                        break;
                    } else {
                        throw new C11160fq("Invalid viewBox. height cannot be negative");
                    }
                    break;
            }
        }
    }

    public static final void A0M(AbstractC03460Hw r3, Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            String trim = attributes.getValue(i).trim();
            switch (EnumC03880Jm.A00(attributes.getLocalName(i)).ordinal()) {
                case 9:
                    r3.A00 = A0E(trim);
                    break;
                case 10:
                    r3.A01 = A0E(trim);
                    break;
                case 82:
                    r3.A02 = A0E(trim);
                    break;
                case 83:
                    r3.A03 = A0E(trim);
                    break;
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0596 A[Catch: XmlPullParserException -> 0x102d, IOException -> 0x1024, all -> 0x1036, TryCatch #7 {IOException -> 0x1024, XmlPullParserException -> 0x102d, blocks: (B:11:0x0043, B:27:0x0083, B:29:0x0093, B:31:0x0097, B:33:0x009b, B:34:0x00a2, B:35:0x00a7, B:37:0x00ab, B:39:0x00af, B:40:0x00b7, B:42:0x00bd, B:43:0x00c7, B:45:0x00d1, B:46:0x00e7, B:48:0x00f3, B:50:0x00fa, B:51:0x00fe, B:53:0x0106, B:56:0x0110, B:59:0x0117, B:61:0x0121, B:62:0x0123, B:63:0x0127, B:65:0x012c, B:66:0x0136, B:68:0x013c, B:69:0x0141, B:71:0x0145, B:72:0x016e, B:74:0x0178, B:75:0x018e, B:77:0x019a, B:78:0x01a1, B:80:0x01a9, B:83:0x01b3, B:86:0x01ba, B:88:0x01c4, B:89:0x01c6, B:90:0x01ca, B:91:0x01cd, B:92:0x01d3, B:95:0x01db, B:97:0x01e1, B:98:0x01f5, B:100:0x01f9, B:102:0x0201, B:104:0x0206, B:105:0x0218, B:107:0x021e, B:111:0x022a, B:112:0x0230, B:113:0x024a, B:115:0x0250, B:116:0x0264, B:118:0x0268, B:119:0x026f, B:120:0x0276, B:122:0x0282, B:123:0x0289, B:124:0x028a, B:126:0x0296, B:127:0x029d, B:128:0x029e, B:129:0x02a1, B:131:0x02a5, B:132:0x02a9, B:133:0x02ad, B:134:0x02b1, B:136:0x02b5, B:137:0x02d5, B:139:0x02d9, B:140:0x02f6, B:142:0x02fa, B:143:0x0312, B:145:0x0318, B:146:0x032c, B:148:0x0330, B:149:0x0337, B:150:0x033e, B:152:0x034a, B:153:0x0351, B:154:0x0352, B:156:0x035e, B:157:0x0365, B:158:0x0366, B:160:0x0372, B:162:0x037e, B:163:0x0380, B:164:0x0383, B:165:0x038c, B:167:0x0390, B:168:0x03a9, B:170:0x03b1, B:171:0x03c7, B:173:0x03cc, B:175:0x03dd, B:176:0x03e4, B:177:0x03e5, B:179:0x03f5, B:184:0x040e, B:185:0x0419, B:186:0x041c, B:187:0x0420, B:188:0x0425, B:192:0x0433, B:193:0x0434, B:194:0x043a, B:198:0x0453, B:199:0x0456, B:203:0x0475, B:204:0x047a, B:208:0x0494, B:209:0x0498, B:210:0x049f, B:213:0x04af, B:215:0x04b3, B:216:0x04b5, B:220:0x04c5, B:223:0x04d5, B:224:0x04d7, B:227:0x04e5, B:231:0x04f3, B:232:0x04f6, B:235:0x0515, B:236:0x051c, B:244:0x0533, B:245:0x0535, B:246:0x054d, B:250:0x056f, B:251:0x0574, B:252:0x0576, B:253:0x0589, B:254:0x058d, B:256:0x0596, B:258:0x059c, B:266:0x05b2, B:268:0x05be, B:269:0x05dc, B:270:0x05e0, B:271:0x05e7, B:273:0x05eb, B:274:0x0603, B:276:0x0609, B:277:0x061d, B:279:0x0621, B:280:0x0628, B:281:0x062f, B:283:0x063b, B:284:0x0642, B:285:0x0643, B:287:0x064f, B:288:0x0656, B:289:0x0657, B:291:0x0663, B:292:0x066a, B:293:0x066b, B:295:0x0677, B:296:0x067e, B:297:0x067f, B:298:0x0682, B:299:0x0689, B:301:0x068d, B:302:0x06a5, B:304:0x06ab, B:305:0x06bf, B:307:0x06c3, B:308:0x06ca, B:309:0x06d1, B:311:0x06dd, B:312:0x06e4, B:313:0x06e5, B:314:0x06e8, B:315:0x06ef, B:317:0x06f3, B:318:0x070b, B:320:0x0711, B:321:0x0725, B:323:0x0729, B:325:0x0735, B:326:0x073c, B:327:0x073d, B:329:0x0749, B:330:0x0750, B:331:0x0751, B:332:0x0758, B:333:0x075e, B:334:0x0761, B:335:0x0768, B:337:0x076c, B:338:0x0784, B:340:0x078a, B:341:0x079e, B:343:0x07a2, B:344:0x07a9, B:345:0x07b0, B:346:0x07b7, B:347:0x07bd, B:348:0x07c0, B:349:0x07c7, B:351:0x07cb, B:352:0x07ee, B:354:0x07f2, B:355:0x0815, B:357:0x0819, B:358:0x083c, B:360:0x0840, B:362:0x0844, B:364:0x0868, B:365:0x086a, B:366:0x086e, B:367:0x0875, B:369:0x0879, B:371:0x087d, B:372:0x0892, B:374:0x0898, B:376:0x08b0, B:378:0x08bc, B:380:0x08c8, B:381:0x08ca, B:382:0x08cd, B:384:0x08d8, B:385:0x08da, B:386:0x08de, B:387:0x08e5, B:389:0x08e9, B:390:0x0909, B:392:0x090d, B:393:0x092d, B:395:0x0931, B:396:0x0949, B:398:0x094f, B:399:0x0963, B:401:0x0967, B:402:0x096e, B:403:0x0975, B:405:0x0981, B:406:0x0988, B:407:0x0989, B:409:0x0995, B:410:0x099c, B:411:0x099d, B:413:0x09a5, B:414:0x09a8, B:416:0x09b0, B:417:0x09b3, B:420:0x09be, B:421:0x09c2, B:422:0x09c8, B:423:0x09cc, B:424:0x09d5, B:426:0x09d9, B:427:0x09ee, B:429:0x09f4, B:430:0x0a08, B:432:0x0a0c, B:433:0x0a13, B:434:0x0a1a, B:435:0x0a21, B:436:0x0a27, B:437:0x0a2a, B:438:0x0a33, B:440:0x0a37, B:441:0x0a4c, B:443:0x0a52, B:444:0x0a66, B:446:0x0a6a, B:447:0x0a71, B:448:0x0a78, B:450:0x0a84, B:451:0x0a8b, B:452:0x0a8c, B:453:0x0a93, B:454:0x0a99, B:455:0x0a9c, B:456:0x0aa5, B:458:0x0aa9, B:460:0x0aad, B:461:0x0abf, B:463:0x0ac5, B:465:0x0add, B:467:0x0ae3, B:469:0x0aee, B:471:0x0af2, B:473:0x0afa, B:480:0x0b07, B:481:0x0b0b, B:482:0x0b0d, B:483:0x0b10, B:484:0x0b19, B:486:0x0b1d, B:487:0x0b35, B:489:0x0b3b, B:491:0x0b52, B:493:0x0b5a, B:494:0x0b5c, B:495:0x0b5f, B:497:0x0b67, B:498:0x0b6a, B:499:0x0b6d, B:500:0x0b76, B:502:0x0b7a, B:503:0x0b8f, B:505:0x0b95, B:506:0x0ba9, B:508:0x0bad, B:510:0x0bb9, B:512:0x0bc5, B:513:0x0bc8, B:514:0x0bce, B:515:0x0bd1, B:517:0x0bde, B:518:0x0be0, B:519:0x0be4, B:520:0x0beb, B:522:0x0bef, B:523:0x0c07, B:525:0x0c0d, B:526:0x0c25, B:528:0x0c2c, B:530:0x0c31, B:532:0x0c37, B:533:0x0c39, B:534:0x0c3c, B:536:0x0c42, B:537:0x0c45, B:539:0x0c4b, B:540:0x0c4d, B:541:0x0c50, B:543:0x0c56, B:544:0x0c59, B:545:0x0c60, B:546:0x0c67, B:547:0x0c6e, B:549:0x0c7a, B:550:0x0c81, B:551:0x0c82, B:553:0x0c8e, B:554:0x0c95, B:555:0x0c96, B:557:0x0ca2, B:559:0x0cae, B:560:0x0cb0, B:561:0x0cb4, B:562:0x0cbd, B:564:0x0cc1, B:565:0x0cd9, B:567:0x0cdf, B:568:0x0cf3, B:570:0x0cf7, B:571:0x0cfe, B:572:0x0d05, B:574:0x0d11, B:575:0x0d18, B:576:0x0d19, B:578:0x0d25, B:579:0x0d2c, B:580:0x0d2d, B:582:0x0d39, B:584:0x0d45, B:585:0x0d48, B:586:0x0d4b, B:587:0x0d4e, B:588:0x0d57, B:590:0x0d5b, B:591:0x0d78, B:593:0x0d7c, B:594:0x0d91, B:596:0x0d97, B:597:0x0daf, B:602:0x0dbe, B:604:0x0dc4, B:605:0x0dc6, B:606:0x0dc9, B:608:0x0dcf, B:609:0x0dd2, B:611:0x0dd8, B:612:0x0dda, B:613:0x0ddd, B:615:0x0de3, B:616:0x0de6, B:617:0x0ded, B:618:0x0df4, B:620:0x0e00, B:621:0x0e07, B:622:0x0e08, B:624:0x0e14, B:625:0x0e1b, B:626:0x0e1c, B:627:0x0e20, B:628:0x0e29, B:630:0x0e2d, B:631:0x0e46, B:632:0x0e49, B:633:0x0e7b, B:635:0x0e81, B:636:0x0e8f, B:637:0x0e97, B:639:0x0e9f, B:641:0x0ea3, B:643:0x0ea7, B:644:0x0eb2, B:645:0x0eb5, B:646:0x0ebb, B:648:0x0ebf, B:650:0x0ec3, B:651:0x0ecf, B:653:0x0ed5, B:654:0x0ed9, B:655:0x0ee0, B:656:0x0ee1, B:657:0x0ee8, B:658:0x0ee9, B:659:0x0ef0, B:660:0x0ef1, B:661:0x0ef8, B:662:0x0ef9, B:663:0x0f00, B:664:0x0f01, B:665:0x0f08, B:666:0x0f09, B:667:0x0f10, B:668:0x0f11, B:669:0x0f18, B:670:0x0f19, B:671:0x0f20, B:672:0x0f21, B:673:0x0f28, B:674:0x0f29, B:675:0x0f30, B:676:0x0f31, B:677:0x0f38, B:678:0x0f39, B:679:0x0f40, B:680:0x0f41, B:681:0x0f48, B:682:0x0f49, B:683:0x0f50, B:684:0x0f51, B:685:0x0f58, B:686:0x0f59, B:687:0x0f60, B:688:0x0f61, B:689:0x0f68, B:690:0x0f69, B:691:0x0f70, B:692:0x0f71, B:693:0x0f78, B:694:0x0f79, B:695:0x0f80, B:697:0x0f82, B:698:0x0f95, B:699:0x0f96, B:700:0x0f9d, B:701:0x0f9e, B:702:0x0fa5, B:703:0x0fa6, B:704:0x0fad, B:705:0x0fae, B:706:0x0fb5, B:707:0x0fb6, B:708:0x0fbd, B:709:0x0fbe, B:710:0x0fc5, B:711:0x0fc6, B:712:0x0fcd, B:713:0x0fce, B:714:0x0fd5, B:715:0x0fd6, B:716:0x0fdd, B:717:0x0fde, B:718:0x0fe5, B:719:0x0fe6, B:720:0x0fed, B:721:0x0fee, B:722:0x0ff5, B:723:0x0ff6, B:724:0x0ffd, B:725:0x0ffe, B:726:0x1005, B:727:0x1006, B:728:0x100d, B:729:0x100e, B:730:0x1015), top: B:756:0x0043, outer: #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:893:0x041c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass0Q5 A0N(java.io.InputStream r36) {
        /*
        // Method dump skipped, instructions count: 4654
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06560Ud.A0N(java.io.InputStream):X.0Q5");
    }

    public final void A0O(AbstractC03300Hg r5, Attributes attributes) {
        Boolean bool;
        for (int i = 0; i < attributes.getLength(); i++) {
            String trim = attributes.getValue(i).trim();
            switch (EnumC03880Jm.A00(attributes.getLocalName(i)).ordinal()) {
                case 23:
                    r5.A00 = A05(trim);
                    break;
                case 24:
                    if ("objectBoundingBox".equals(trim)) {
                        bool = Boolean.FALSE;
                    } else if ("userSpaceOnUse".equals(trim)) {
                        bool = Boolean.TRUE;
                    } else {
                        throw new C11160fq("Invalid value for attribute gradientUnits");
                    }
                    r5.A02 = bool;
                    break;
                case 26:
                    if (!"".equals(attributes.getURI(i)) && !"http://www.w3.org/1999/xlink".equals(attributes.getURI(i))) {
                        break;
                    } else {
                        r5.A03 = trim;
                        break;
                    }
                    break;
                case 60:
                    try {
                        r5.A01 = AnonymousClass0JB.valueOf(trim);
                        break;
                    } catch (IllegalArgumentException unused) {
                        StringBuilder sb = new StringBuilder("Invalid spreadMethod attribute. \"");
                        sb.append(trim);
                        sb.append("\" is not a valid value.");
                        throw new C11160fq(sb.toString());
                    }
            }
        }
    }

    public final void A0P(AbstractC12130hQ r4, Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            if (EnumC03880Jm.A00(attributes.getLocalName(i)) == EnumC03880Jm.transform) {
                r4.Ad4(A05(attributes.getValue(i)));
            }
        }
    }

    public final void A0Q(String str) {
        AnonymousClass0OO r2;
        AbstractC03490Hz r1 = (AbstractC03490Hz) this.A01;
        int size = r1.A01.size();
        if (size == 0) {
            r2 = null;
        } else {
            r2 = (AnonymousClass0OO) r1.A01.get(size - 1);
        }
        if (r2 instanceof C03330Hj) {
            StringBuilder sb = new StringBuilder();
            C03330Hj r22 = (C03330Hj) r2;
            sb.append(r22.A00);
            sb.append(str);
            r22.A00 = sb.toString();
            return;
        }
        this.A01.A5f(new C03330Hj(str));
    }
}
