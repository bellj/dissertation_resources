package X;

/* renamed from: X.3aM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69863aM implements AbstractC116245Ur {
    public final /* synthetic */ AnonymousClass2B7 A00;

    public C69863aM(AnonymousClass2B7 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116245Ur
    public void AWl(AnonymousClass1KS r5, Integer num, int i) {
        AnonymousClass2B7 r1 = this.A00;
        AbstractC116245Ur r0 = r1.A04;
        if (r0 != null) {
            r0.AWl(r5, num, i);
            if (r1.A01()) {
                C255619w r3 = r1.A0F;
                AnonymousClass009.A05(r3);
                r3.A00();
                AnonymousClass1BQ r02 = r1.A0E;
                AnonymousClass009.A05(r02);
                AnonymousClass016 r2 = r02.A03;
                AnonymousClass009.A05(r2.A01());
                r3.A01(num.intValue(), i, C12980iv.A0z(r2).size());
            }
        }
    }
}
