package X;

import android.text.Editable;
import android.text.TextUtils;
import com.whatsapp.Conversation;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.366  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass366 extends C469928m {
    public final /* synthetic */ C15370n3 A00;
    public final /* synthetic */ MessageReplyActivity A01;

    public AnonymousClass366(C15370n3 r1, MessageReplyActivity messageReplyActivity) {
        this.A01 = messageReplyActivity;
        this.A00 = r1;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        boolean z;
        AnonymousClass1Ly r1;
        String obj = editable.toString();
        boolean isEmpty = TextUtils.isEmpty(obj);
        MessageReplyActivity messageReplyActivity = this.A01;
        C16170oZ r3 = messageReplyActivity.A0D;
        AbstractC14640lm A02 = C15370n3.A02(this.A00);
        if (!isEmpty) {
            r3.A0B(A02, 0);
        } else {
            r3.A09(A02);
        }
        C42971wC.A06(messageReplyActivity, messageReplyActivity.A0i.getPaint(), editable, ((ActivityC13810kN) messageReplyActivity).A08, ((ActivityC13810kN) messageReplyActivity).A0B, messageReplyActivity.A0n);
        if (TextUtils.getTrimmedLength(obj) > 0) {
            z = true;
            messageReplyActivity.A06.setVisibility(8);
        } else {
            z = false;
            if (messageReplyActivity.A15) {
                messageReplyActivity.A06.setVisibility(0);
            }
        }
        messageReplyActivity.A0A.setEnabled(z);
        if (messageReplyActivity.A0B.getVisibility() == 8 && !z) {
            C469928m.A00(messageReplyActivity.A0B, true);
            messageReplyActivity.A0B.setVisibility(0);
            messageReplyActivity.A08.startAnimation(Conversation.A03(C28141Kv.A01(((ActivityC13830kP) messageReplyActivity).A01), true, false));
            messageReplyActivity.A07.startAnimation(Conversation.A02(C28141Kv.A01(((ActivityC13830kP) messageReplyActivity).A01), true));
            messageReplyActivity.A07.setVisibility(0);
            C469928m.A00(messageReplyActivity.A0A, false);
            messageReplyActivity.A0A.setVisibility(8);
        } else if (messageReplyActivity.A0B.getVisibility() == 0 && z) {
            C469928m.A00(messageReplyActivity.A0B, false);
            messageReplyActivity.A0B.setVisibility(8);
            messageReplyActivity.A08.startAnimation(Conversation.A03(C28141Kv.A01(((ActivityC13830kP) messageReplyActivity).A01), false, false));
            messageReplyActivity.A07.startAnimation(Conversation.A02(C28141Kv.A01(((ActivityC13830kP) messageReplyActivity).A01), false));
            messageReplyActivity.A07.setVisibility(8);
            C469928m.A00(messageReplyActivity.A0A, true);
            messageReplyActivity.A0A.setVisibility(0);
        }
        if (messageReplyActivity.A0t.A00 && (r1 = messageReplyActivity.A0v) != null && messageReplyActivity.A0Y.A02) {
            r1.A00(obj, 200);
        }
    }
}
