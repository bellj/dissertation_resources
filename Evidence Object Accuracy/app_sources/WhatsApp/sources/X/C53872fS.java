package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2fS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C53872fS extends AnonymousClass015 {
    public final AnonymousClass017 A00;
    public final AnonymousClass017 A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass3EX A03;
    public final UserJid A04;
    public final C27691It A05;

    public C53872fS(AnonymousClass3EX r3, UserJid userJid) {
        C16700pc.A0E(userJid, 1);
        this.A04 = userJid;
        this.A03 = r3;
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A02 = A0T;
        this.A00 = A0T;
        C27691It A03 = C13000ix.A03();
        this.A05 = A03;
        this.A01 = A03;
        r3.A00 = A0T;
    }
}
