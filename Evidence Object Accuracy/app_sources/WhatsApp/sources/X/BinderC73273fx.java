package X;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.3fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class BinderC73273fx extends Binder implements IInterface {
    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this;
    }

    public BinderC73273fx(String str) {
        attachInterface(this, str);
    }

    public boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (!(this instanceof AbstractBinderC79563qn)) {
            return false;
        }
        AbstractBinderC79563qn r1 = (AbstractBinderC79563qn) this;
        if (i == 1) {
            BinderC56502l7 r0 = new BinderC56502l7(((AbstractBinderC78743pT) r1).A01());
            parcel2.writeNoException();
            parcel2.writeStrongBinder(r0.asBinder());
            return true;
        } else if (i != 2) {
            return false;
        } else {
            int i3 = ((AbstractBinderC78743pT) r1).A00;
            parcel2.writeNoException();
            parcel2.writeInt(i3);
            return true;
        }
    }

    @Override // android.os.Binder
    public final boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i <= 16777215) {
            C72463ee.A0Q(this, parcel);
        } else if (super.onTransact(i, parcel, parcel2, i2)) {
            return true;
        }
        return A00(i, parcel, parcel2, i2);
    }
}
