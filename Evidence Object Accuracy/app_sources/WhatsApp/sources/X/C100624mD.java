package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.4mD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100624mD implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(3);
    public final AnonymousClass5YX[] A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: X.5YX[] */
    /* JADX WARN: Multi-variable type inference failed */
    public C100624mD(Parcel parcel) {
        this.A00 = new AnonymousClass5YX[parcel.readInt()];
        int i = 0;
        while (true) {
            AnonymousClass5YX[] r1 = this.A00;
            if (i < r1.length) {
                r1[i] = C12990iw.A0I(parcel, AnonymousClass5YX.class);
                i++;
            } else {
                return;
            }
        }
    }

    public C100624mD(List list) {
        this.A00 = (AnonymousClass5YX[]) list.toArray(new AnonymousClass5YX[0]);
    }

    public C100624mD(AnonymousClass5YX... r1) {
        this.A00 = r1;
    }

    public C100624mD A00(C100624mD r6) {
        if (r6 == null) {
            return this;
        }
        AnonymousClass5YX[] r4 = r6.A00;
        int length = r4.length;
        if (length == 0) {
            return this;
        }
        AnonymousClass5YX[] r1 = this.A00;
        int length2 = r1.length;
        Object[] copyOf = Arrays.copyOf(r1, length2 + length);
        System.arraycopy(r4, 0, copyOf, length2, length);
        return new C100624mD((AnonymousClass5YX[]) copyOf);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C100624mD.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.A00, ((C100624mD) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        return C12960it.A0d(Arrays.toString(this.A00), C12960it.A0k("entries="));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        AnonymousClass5YX[] r4 = this.A00;
        int length = r4.length;
        parcel.writeInt(length);
        for (AnonymousClass5YX r0 : r4) {
            parcel.writeParcelable(r0, 0);
        }
    }
}
