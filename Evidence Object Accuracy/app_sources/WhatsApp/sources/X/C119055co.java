package X;

import java.util.List;

/* renamed from: X.5co  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119055co extends AbstractC130695zp {
    public final C127805v5 A00;
    public final C129845yO A01;
    public final Boolean A02;
    public final Boolean A03;
    public final Boolean A04;
    public final Boolean A05;
    public final Boolean A06;
    public final Boolean A07;
    public final Boolean A08;
    public final Boolean A09;
    public final Boolean A0A;
    public final Boolean A0B;
    public final Boolean A0C;
    public final Boolean A0D;
    public final Boolean A0E;
    public final Float A0F;
    public final Float A0G;
    public final Float A0H;
    public final Integer A0I;
    public final Integer A0J;
    public final Integer A0K;
    public final Integer A0L;
    public final Integer A0M;
    public final Integer A0N;
    public final List A0O;
    public final List A0P;
    public final List A0Q;
    public final List A0R;
    public final List A0S;
    public final List A0T;
    public final List A0U;
    public final List A0V;
    public final List A0W;
    public final List A0X;
    public final List A0Y;
    public final List A0Z;
    public final List A0a;
    public final List A0b;
    public final List A0c;
    public final List A0d;

    /* JADX WARNING: Code restructure failed: missing block: B:133:0x038f, code lost:
        if (X.C117305Zk.A1Z(r4, 17) != false) goto L_0x0392;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00cd, code lost:
        if (r1.intValue() == 0) goto L_0x00cf;
     */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x02f5  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x030a A[LOOP:8: B:116:0x0308->B:117:0x030a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x030f  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0342  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x039e  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x03c8  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x040b  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0427  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x044b  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0477  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C119055co(android.hardware.Camera.Parameters r21) {
        /*
        // Method dump skipped, instructions count: 1262
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119055co.<init>(android.hardware.Camera$Parameters):void");
    }
}
