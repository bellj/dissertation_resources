package X;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1Of  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28601Of {
    public static final C28601Of A01 = new C28601Of(Collections.emptyMap());
    public final Map A00;

    public C28601Of(Map map) {
        this.A00 = map;
    }

    public static C28601Of A00(Map map) {
        AnonymousClass1YB r1 = new AnonymousClass1YB();
        Map map2 = r1.A00;
        AnonymousClass009.A05(map2);
        map2.putAll(map);
        return r1.A00();
    }

    public AnonymousClass1JO A01() {
        AnonymousClass1YJ r2 = new AnonymousClass1YJ();
        Set entrySet = this.A00.entrySet();
        Set set = r2.A00;
        AnonymousClass009.A05(set);
        set.addAll(entrySet);
        return r2.A00();
    }

    public AnonymousClass1JO A02() {
        AnonymousClass1YJ r2 = new AnonymousClass1YJ();
        Set keySet = this.A00.keySet();
        Set set = r2.A00;
        AnonymousClass009.A05(set);
        set.addAll(keySet);
        return r2.A00();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C28601Of.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((C28601Of) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
