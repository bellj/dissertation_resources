package X;

import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;

/* renamed from: X.5Hs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113475Hs extends X509CertSelector implements AbstractC117205Yy {
    @Override // X.AbstractC117205Yy
    public boolean ALJ(Object obj) {
        if (!(obj instanceof X509Certificate)) {
            return false;
        }
        return super.match((Certificate) obj);
    }

    @Override // java.security.cert.X509CertSelector, java.security.cert.CertSelector
    public boolean match(Certificate certificate) {
        return ALJ(certificate);
    }
}
