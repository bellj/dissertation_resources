package X;

import android.content.Context;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.0IU  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0IU extends AbstractC08590bS {
    public static Thread A02;
    public static final BlockingQueue A03 = new ArrayBlockingQueue(10);
    public static final AtomicBoolean A04 = new AtomicBoolean();
    public static volatile C08860by A05;
    public final AtomicLong A00 = new AtomicLong(0);
    public final AtomicLong A01 = new AtomicLong(0);

    public AnonymousClass0IU(Context context, int i) {
        super(i);
        if (A04.compareAndSet(false, true)) {
            AnonymousClass0UE.A01(new AnonymousClass0IM(context, this));
        }
    }
}
