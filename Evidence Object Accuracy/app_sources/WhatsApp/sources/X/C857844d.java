package X;

import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;
import java.util.Set;

/* renamed from: X.44d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C857844d extends AbstractC33331dp {
    public final /* synthetic */ MessageDetailsActivity A00;

    public C857844d(MessageDetailsActivity messageDetailsActivity) {
        this.A00 = messageDetailsActivity;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        this.A00.A01.notifyDataSetChanged();
    }
}
