package X;

import java.util.List;

/* renamed from: X.1KZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KZ {
    public int A00;
    public long A01;
    public String A02;
    public List A03;
    public List A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final long A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;
    public final String A0F;
    public final String A0G;
    public final String A0H;
    public final String A0I;
    public final String A0J;
    public final boolean A0K;
    public final boolean A0L;
    public final boolean A0M;
    public final boolean A0N;
    public final boolean A0O;

    public AnonymousClass1KZ(C37451mQ r3) {
        String str = r3.A0B;
        AnonymousClass009.A05(str);
        this.A0D = str;
        String str2 = r3.A0D;
        AnonymousClass009.A05(str2);
        this.A0F = str2;
        String str3 = r3.A0F;
        AnonymousClass009.A05(str3);
        this.A0H = str3;
        this.A09 = r3.A02;
        this.A08 = r3.A01;
        this.A05 = r3.A0O;
        this.A0I = r3.A0G;
        this.A0J = r3.A0H;
        List list = r3.A0I;
        AnonymousClass009.A05(list);
        this.A03 = list;
        this.A0O = r3.A0P;
        List list2 = r3.A0J;
        AnonymousClass009.A05(list2);
        this.A04 = list2;
        this.A0E = r3.A0C;
        this.A01 = r3.A00;
        this.A02 = r3.A05;
        this.A0A = r3.A08;
        this.A0B = r3.A09;
        this.A0L = r3.A0L;
        this.A0G = r3.A0E;
        this.A0C = r3.A0A;
        this.A0K = r3.A0K;
        this.A0M = r3.A0M;
        this.A0N = r3.A0N;
    }

    public String A00() {
        if (this.A0O) {
            return null;
        }
        StringBuilder sb = new StringBuilder("https://static.whatsapp.net/sticker?img=");
        sb.append(this.A0A);
        return sb.toString();
    }

    public boolean A01() {
        String str;
        String str2 = this.A02;
        return (str2 == null || (str = this.A0E) == null || str2.equals(str)) ? false : true;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("StickerPack{");
        stringBuffer.append("id='");
        stringBuffer.append(this.A0D);
        stringBuffer.append('\'');
        stringBuffer.append(", name='");
        stringBuffer.append(this.A0F);
        stringBuffer.append('\'');
        stringBuffer.append(", publisher='");
        stringBuffer.append(this.A0H);
        stringBuffer.append('\'');
        stringBuffer.append(", description='");
        stringBuffer.append(this.A09);
        stringBuffer.append('\'');
        stringBuffer.append(", size=");
        stringBuffer.append(this.A08);
        stringBuffer.append(", isDownloading=");
        stringBuffer.append(this.A05);
        stringBuffer.append(", trayImageId='");
        stringBuffer.append(this.A0I);
        stringBuffer.append('\'');
        stringBuffer.append(", trayImagePreviewId='");
        stringBuffer.append(this.A0J);
        stringBuffer.append('\'');
        stringBuffer.append(", previewImageIds=");
        stringBuffer.append(this.A03);
        stringBuffer.append(", stickers=");
        stringBuffer.append(this.A04);
        stringBuffer.append(", order=");
        stringBuffer.append(this.A00);
        stringBuffer.append(", isThirdParty=");
        stringBuffer.append(this.A0O);
        stringBuffer.append(", imageDataHash='");
        stringBuffer.append(this.A0E);
        stringBuffer.append('\'');
        stringBuffer.append(", downloadedSize=");
        stringBuffer.append(this.A01);
        stringBuffer.append(", downloadedImageDataHash='");
        stringBuffer.append(this.A02);
        stringBuffer.append('\'');
        stringBuffer.append(", downloadedTrayImageId='");
        stringBuffer.append(this.A0A);
        stringBuffer.append('\'');
        stringBuffer.append(", downloadedTrayImagePreviewId='");
        stringBuffer.append(this.A0B);
        stringBuffer.append('\'');
        stringBuffer.append(", isUnseen=");
        stringBuffer.append(this.A07);
        stringBuffer.append(", isNew=");
        stringBuffer.append(this.A06);
        stringBuffer.append(", avoidCaching=");
        stringBuffer.append(this.A0L);
        stringBuffer.append(", playLink='");
        stringBuffer.append(this.A0G);
        stringBuffer.append('\'');
        stringBuffer.append(", iOSLink='");
        stringBuffer.append(this.A0C);
        stringBuffer.append('\'');
        stringBuffer.append(", animatedPack=");
        stringBuffer.append(this.A0K);
        stringBuffer.append(", downloadedAnimatedPack=");
        stringBuffer.append(this.A0M);
        stringBuffer.append(", isAvatarStickerPack=");
        stringBuffer.append(this.A0N);
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
