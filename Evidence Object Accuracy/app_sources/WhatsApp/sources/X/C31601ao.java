package X;

/* renamed from: X.1ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31601ao {
    public final int A00;
    public final String A01;

    public C31601ao(String str, int i) {
        this.A01 = str;
        this.A00 = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C31601ao)) {
            return false;
        }
        C31601ao r4 = (C31601ao) obj;
        if (!this.A01.equals(r4.A01) || this.A00 != r4.A00) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A01.hashCode() ^ this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append(":");
        sb.append(this.A00);
        return sb.toString();
    }
}
