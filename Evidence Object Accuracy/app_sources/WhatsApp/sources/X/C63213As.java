package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import com.whatsapp.R;

/* renamed from: X.3As  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63213As {
    public static Drawable A00(Context context) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.status_thumbnail_size);
        shapeDrawable.setIntrinsicHeight(dimensionPixelSize);
        shapeDrawable.setIntrinsicWidth(dimensionPixelSize);
        shapeDrawable.getPaint().setColor(AnonymousClass00T.A00(context, R.color.group_iris));
        Drawable[] drawableArr = new Drawable[2];
        C12970iu.A1U(shapeDrawable, AnonymousClass00T.A04(context, R.drawable.ic_voice_status), drawableArr);
        LayerDrawable layerDrawable = new LayerDrawable(drawableArr);
        int A01 = AnonymousClass3G9.A01(context, 10.0f);
        layerDrawable.setLayerInset(1, A01, A01, A01, A01);
        return layerDrawable;
    }
}
