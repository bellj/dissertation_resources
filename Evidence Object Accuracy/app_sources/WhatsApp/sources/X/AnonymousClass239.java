package X;

import android.content.Context;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import com.whatsapp.webpagepreview.WebPagePreviewView;

/* renamed from: X.239  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass239 {
    public boolean A00;
    public final Context A01;
    public final C89134Iu A02;
    public final C14330lG A03;
    public final C14310lE A04;
    public final AnonymousClass018 A05;
    public final AbstractC14440lR A06;
    public final WebPagePreviewView A07;

    public AnonymousClass239(Context context, C89134Iu r5, C14330lG r6, C14310lE r7, AnonymousClass018 r8, AbstractC14440lR r9, boolean z) {
        this.A01 = context;
        this.A05 = r8;
        this.A02 = r5;
        this.A04 = r7;
        this.A06 = r9;
        this.A03 = r6;
        WebPagePreviewView webPagePreviewView = new WebPagePreviewView(context);
        this.A07 = webPagePreviewView;
        this.A00 = z;
        webPagePreviewView.setImageProgressBarVisibility(false);
        webPagePreviewView.setImageCancelClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 18));
        webPagePreviewView.setImageContentClickListener(new ViewOnClickCListenerShape14S0100000_I0_1(this, 19));
        webPagePreviewView.A01();
    }
}
