package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3wZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83023wZ extends AbstractC94534c0 implements Iterable {
    public List A00 = C12960it.A0l();

    public C83023wZ(Collection collection) {
        for (Object obj : collection) {
            this.A00.add(AbstractC94534c0.A02(obj));
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C83023wZ)) {
            return false;
        }
        return this.A00.equals(((C83023wZ) obj).A00);
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return this.A00.iterator();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("[");
        A0k.append(C95094d8.A00(this.A00, ",", ""));
        return C12960it.A0d("]", A0k);
    }
}
