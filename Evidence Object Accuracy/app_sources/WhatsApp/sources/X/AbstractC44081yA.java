package X;

import android.content.Context;
import android.widget.LinearLayout;

/* renamed from: X.1yA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44081yA extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;

    public AbstractC44081yA(Context context) {
        super(context);
        A00();
    }

    public void A00() {
        if (!(this instanceof C44061y8)) {
            AnonymousClass34W r1 = (AnonymousClass34W) this;
            if (!r1.A05) {
                r1.A05 = true;
                AnonymousClass01J r2 = ((AnonymousClass2P6) ((AnonymousClass2P5) r1.generatedComponent())).A06;
                ((AbstractC44071y9) r1).A05 = (C14850m9) r2.A04.get();
                r1.A0A = (AnonymousClass1CY) r2.ABO.get();
                ((AbstractC44071y9) r1).A00 = (C14900mE) r2.A8X.get();
                ((AbstractC44071y9) r1).A01 = (C239613r) r2.AI9.get();
                ((AbstractC44071y9) r1).A02 = (C16170oZ) r2.AM4.get();
                r1.A0B = (AnonymousClass19O) r2.ACO.get();
                ((AbstractC44071y9) r1).A04 = (C15890o4) r2.AN1.get();
                r1.A07 = (C22370yy) r2.AB0.get();
                r1.A08 = (C26161Cg) r2.AB4.get();
                ((AbstractC44071y9) r1).A03 = (AnonymousClass11P) r2.ABp.get();
                r1.A06 = (AnonymousClass109) r2.AI8.get();
                r1.A0C = C18000rk.A00(r2.ADw);
                r1.A02 = (AnonymousClass018) r2.ANb.get();
                r1.A01 = (AnonymousClass19D) r2.ABo.get();
                r1.A04 = C18000rk.A00(r2.AGl);
                return;
            }
            return;
        }
        C44061y8 r12 = (C44061y8) this;
        if (!r12.A0C) {
            r12.A0C = true;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) r12.generatedComponent());
            AnonymousClass01J r22 = r3.A06;
            ((AbstractC44071y9) r12).A05 = (C14850m9) r22.A04.get();
            ((AbstractC44071y9) r12).A0A = (AnonymousClass1CY) r22.ABO.get();
            ((AbstractC44071y9) r12).A00 = (C14900mE) r22.A8X.get();
            ((AbstractC44071y9) r12).A01 = (C239613r) r22.AI9.get();
            ((AbstractC44071y9) r12).A02 = (C16170oZ) r22.AM4.get();
            ((AbstractC44071y9) r12).A0B = (AnonymousClass19O) r22.ACO.get();
            ((AbstractC44071y9) r12).A04 = (C15890o4) r22.AN1.get();
            ((AbstractC44071y9) r12).A07 = (C22370yy) r22.AB0.get();
            ((AbstractC44071y9) r12).A08 = (C26161Cg) r22.AB4.get();
            ((AbstractC44071y9) r12).A03 = (AnonymousClass11P) r22.ABp.get();
            ((AbstractC44071y9) r12).A06 = (AnonymousClass109) r22.AI8.get();
            ((AbstractC44071y9) r12).A0C = C18000rk.A00(r22.ADw);
            r12.A01 = (C15570nT) r22.AAr.get();
            r12.A03 = (C21270x9) r22.A4A.get();
            r12.A02 = (C15550nR) r22.A45.get();
            r12.A05 = (AnonymousClass018) r22.ANb.get();
            r12.A06 = (C239913u) r22.AC1.get();
            r12.A0B = C18000rk.A00(r22.AGl);
            r12.A00 = (C50532Px) r3.A02.get();
            r12.A04 = (AnonymousClass19D) r22.ABo.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
