package X;

/* renamed from: X.4Di  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Di {
    public static int A00(C14260l7 r3, AnonymousClass28D r4) {
        try {
            if (r3.A06) {
                String A0I = r4.A0I(35);
                if (A0I != null) {
                    return AnonymousClass3JW.A05(A0I);
                }
                return 0;
            }
            String A0I2 = r4.A0I(36);
            if (A0I2 != null) {
                return AnonymousClass3JW.A05(A0I2);
            }
            return 0;
        } catch (AnonymousClass491 unused) {
            C28691Op.A00("ThemedColorUtils", "Error parsing themed color");
            return 0;
        }
    }
}
