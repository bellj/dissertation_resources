package X;

import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchFragment;

/* renamed from: X.3hW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74183hW extends AbstractC010305c {
    public final /* synthetic */ BusinessDirectorySearchFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74183hW(BusinessDirectorySearchFragment businessDirectorySearchFragment) {
        super(true);
        this.A00 = businessDirectorySearchFragment;
    }

    @Override // X.AbstractC010305c
    public void A00() {
        C53862fQ r2 = this.A00.A09;
        AnonymousClass2K2 r1 = r2.A0I;
        if (r1.A00 != null) {
            r1.A00 = null;
            r2.A0D();
            return;
        }
        r2.A08();
    }
}
