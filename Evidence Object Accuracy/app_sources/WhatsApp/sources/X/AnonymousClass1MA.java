package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1MA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MA extends AnonymousClass02M {
    public AnonymousClass1M9 A00;
    public AnonymousClass1M9 A01;
    public AnonymousClass1MB A02;
    public C55122hp A03;
    public String A04;
    public List A05 = new ArrayList();
    public List A06;
    public boolean A07;
    public boolean A08;
    public final int A09;
    public final View.OnClickListener A0A;
    public final LinearLayoutManager A0B;
    public final AbstractC15710nm A0C;
    public final C14900mE A0D;
    public final C15450nH A0E;
    public final C15550nR A0F;
    public final C15610nY A0G;
    public final AnonymousClass1J1 A0H;
    public final AnonymousClass018 A0I;
    public final C16120oU A0J;
    public final AnonymousClass12F A0K;
    public final C27691It A0L;
    public final AbstractC14440lR A0M;
    public final Runnable A0N = new RunnableBRunnable0Shape12S0100000_I0_12(this, 32);
    public final String A0O;
    public final LinkedHashMap A0P;

    public AnonymousClass1MA(View.OnClickListener onClickListener, LinearLayoutManager linearLayoutManager, AbstractC15710nm r7, C14900mE r8, C15450nH r9, C15550nR r10, C15610nY r11, AnonymousClass1J1 r12, AnonymousClass018 r13, C16120oU r14, AnonymousClass12F r15, C27691It r16, AbstractC14440lR r17, String str, int i) {
        this.A0D = r8;
        this.A0C = r7;
        this.A0M = r17;
        this.A0J = r14;
        this.A0E = r9;
        this.A0F = r10;
        this.A0G = r11;
        this.A0I = r13;
        this.A0K = r15;
        this.A0B = linearLayoutManager;
        this.A0H = r12;
        this.A09 = i;
        this.A0O = str;
        this.A0L = r16;
        this.A0A = onClickListener;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        this.A0P = linkedHashMap;
        Boolean bool = Boolean.FALSE;
        linkedHashMap.put(2, bool);
        Boolean bool2 = Boolean.TRUE;
        linkedHashMap.put(1, bool2);
        linkedHashMap.put(3, bool);
        linkedHashMap.put(4, bool2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003c, code lost:
        if (r3.A05.size() != 0) goto L_0x003e;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A0D() {
        /*
            r3 = this;
            int r2 = r3.A0E()
            boolean r0 = r3.A07
            if (r0 == 0) goto L_0x0041
            boolean r0 = r3.A08
            if (r0 != 0) goto L_0x0041
            java.util.List r0 = r3.A05
            int r1 = r0.size()
            int r0 = r3.A0F()
            int r1 = r1 - r0
        L_0x0017:
            int r2 = r2 + r1
            boolean r0 = r3.A07
            r1 = 0
            if (r0 == 0) goto L_0x0030
            boolean r0 = r3.A08
            if (r0 != 0) goto L_0x0030
            java.util.List r0 = r3.A05
            int r0 = r0.size()
            if (r0 == 0) goto L_0x002f
            int r0 = r3.A0F()
            if (r0 <= 0) goto L_0x0030
        L_0x002f:
            r1 = 1
        L_0x0030:
            int r2 = r2 + r1
            boolean r0 = r3.A08
            if (r0 == 0) goto L_0x003e
            java.util.List r0 = r3.A05
            int r1 = r0.size()
            r0 = 1
            if (r1 == 0) goto L_0x003f
        L_0x003e:
            r0 = 0
        L_0x003f:
            int r2 = r2 + r0
            return r2
        L_0x0041:
            java.util.List r0 = r3.A05
            int r1 = r0.size()
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MA.A0D():int");
    }

    public final int A0E() {
        int i = 0;
        for (Map.Entry entry : this.A0P.entrySet()) {
            if (((Boolean) entry.getValue()).booleanValue()) {
                i++;
            }
        }
        return i;
    }

    public final int A0F() {
        int i = 0;
        for (C28021Kd r0 : this.A05) {
            if (r0.A00.A0G == 0) {
                i++;
            }
        }
        return i;
    }

    public final int A0G(int i) {
        int i2 = 0;
        for (Map.Entry entry : this.A0P.entrySet()) {
            if (((Boolean) entry.getValue()).booleanValue()) {
                if (((Number) entry.getKey()).intValue() == i) {
                    return i2;
                }
                i2++;
            }
        }
        return -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r2 < 4) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        if (r0.A02.size() < 4) goto L_0x001b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0H() {
        /*
            r7 = this;
            java.util.LinkedHashMap r6 = r7.A0P
            r4 = 3
            java.lang.Integer r5 = java.lang.Integer.valueOf(r4)
            java.lang.Object r0 = r6.get(r5)
            if (r0 == 0) goto L_0x003f
            X.1M9 r0 = r7.A01
            if (r0 == 0) goto L_0x001b
            java.util.List r0 = r0.A02
            int r1 = r0.size()
            r0 = 4
            r3 = 1
            if (r1 >= r0) goto L_0x001c
        L_0x001b:
            r3 = 0
        L_0x001c:
            X.1M9 r0 = r7.A00
            if (r0 == 0) goto L_0x002a
            java.util.List r0 = r0.A02
            int r2 = r0.size()
            r1 = 4
            r0 = 1
            if (r2 >= r1) goto L_0x002b
        L_0x002a:
            r0 = 0
        L_0x002b:
            if (r3 != 0) goto L_0x0040
            if (r0 != 0) goto L_0x0040
            java.lang.Object r0 = r6.get(r5)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0052
            r0 = 0
        L_0x003c:
            r7.A0I(r4, r0)
        L_0x003f:
            return
        L_0x0040:
            java.lang.Object r0 = r6.get(r5)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0052
            boolean r0 = r7.A08
            if (r0 != 0) goto L_0x003f
            r0 = 1
            goto L_0x003c
        L_0x0052:
            int r0 = r7.A0G(r4)
            if (r0 < 0) goto L_0x003f
            r7.A03(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MA.A0H():void");
    }

    public final void A0I(int i, boolean z) {
        LinkedHashMap linkedHashMap = this.A0P;
        Integer valueOf = Integer.valueOf(i);
        if (linkedHashMap.get(valueOf) != null && ((Boolean) linkedHashMap.get(valueOf)).booleanValue() != z) {
            if (z) {
                linkedHashMap.put(valueOf, Boolean.TRUE);
                int A0G = A0G(i);
                A04(A0G);
                if (A0G == 0) {
                    LinearLayoutManager linearLayoutManager = this.A0B;
                    if (linearLayoutManager.A19() == 0) {
                        linearLayoutManager.A0o(0);
                        return;
                    }
                    return;
                }
                return;
            }
            A05(A0G(i));
            linkedHashMap.put(valueOf, Boolean.FALSE);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ba, code lost:
        if (r12.A02.size() < 4) goto L_0x00bc;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r21, int r22) {
        /*
        // Method dump skipped, instructions count: 1022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MA.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 1) {
            C55122hp r1 = this.A03;
            if (r1 != null) {
                return r1;
            }
            C55122hp r12 = new C55122hp(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_usage_summary_view, viewGroup, false), this.A0C, this.A0I);
            this.A03 = r12;
            return r12;
        } else if (i == 2) {
            return new C75203jT(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_usage_loading_header, viewGroup, false));
        } else {
            if (i == 3) {
                C16120oU r4 = this.A0J;
                return new C75633kA(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_usage_cleanup_suggestions, viewGroup, false), this.A0I, r4);
            } else if (i == 4) {
                return new C75373jk(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_usage_chat_search_view, viewGroup, false));
            } else {
                if (i == 6) {
                    return new C75483jv(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_usage_chat_footer_view, viewGroup, false), this.A0I);
                } else if (i == 7) {
                    C617231w r13 = new C617231w(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_no_result, viewGroup, false));
                    r13.A0E(Boolean.TRUE);
                    return r13;
                } else {
                    C16120oU r7 = this.A0J;
                    return new C55052hi(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_usage_chat_row_item, viewGroup, false), this.A0E, this.A0F, this.A0G, this.A0I, r7, this.A0K);
                }
            }
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        int i2 = 0;
        for (Map.Entry entry : this.A0P.entrySet()) {
            if (((Boolean) entry.getValue()).booleanValue()) {
                if (i2 == i) {
                    return ((Number) entry.getKey()).intValue();
                }
                i2++;
            }
        }
        if (this.A07 && !this.A08 && ((this.A05.size() == 0 || A0F() > 0) && i == A0D() - 1)) {
            return 6;
        }
        if (!this.A08 || this.A05.size() != 0) {
            return 5;
        }
        return 7;
    }
}
