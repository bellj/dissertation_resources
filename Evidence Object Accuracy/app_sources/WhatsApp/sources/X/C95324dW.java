package X;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.4dW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95324dW {
    public static C94194bP getFieldSetter(Class cls, String str) {
        try {
            return new C94194bP(cls.getDeclaredField(str));
        } catch (NoSuchFieldException e) {
            throw new AssertionError(e);
        }
    }

    public static void populateMultiset(AnonymousClass5Z2 r3, ObjectInputStream objectInputStream, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            r3.add(objectInputStream.readObject(), objectInputStream.readInt());
        }
    }

    public static int readCount(ObjectInputStream objectInputStream) {
        return objectInputStream.readInt();
    }

    public static void writeMultimap(AnonymousClass5XH r2, ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(r2.asMap().size());
        Iterator A0n = C12960it.A0n(r2.asMap());
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            objectOutputStream.writeObject(A15.getKey());
            objectOutputStream.writeInt(((Collection) A15.getValue()).size());
            for (Object obj : (Collection) A15.getValue()) {
                objectOutputStream.writeObject(obj);
            }
        }
    }

    public static void writeMultiset(AnonymousClass5Z2 r2, ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(r2.entrySet().size());
        for (AnonymousClass4Y5 r1 : r2.entrySet()) {
            objectOutputStream.writeObject(r1.getElement());
            objectOutputStream.writeInt(r1.getCount());
        }
    }
}
