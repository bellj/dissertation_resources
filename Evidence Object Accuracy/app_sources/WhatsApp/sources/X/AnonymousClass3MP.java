package X;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape0S0220102_I1;

/* renamed from: X.3MP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MP implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener {
    public float A00;
    public float A01 = Float.MAX_VALUE;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06 = 0.8f;
    public Context A07;
    public Matrix A08;
    public Matrix A09 = C13000ix.A01();
    public Matrix A0A = C13000ix.A01();
    public RectF A0B = C12980iv.A0K();
    public RectF A0C = C12980iv.A0K();
    public RectF A0D = C12980iv.A0K();
    public View.OnClickListener A0E;
    public View A0F;
    public RunnableBRunnable0Shape0S0220102_I1 A0G;
    public RunnableBRunnable0Shape0S0220102_I1 A0H;
    public C63853De A0I;
    public RunnableC55822jO A0J;
    public RunnableC76183lD A0K;
    public boolean A0L;
    public boolean A0M = true;
    public boolean A0N;
    public boolean A0O;
    public boolean A0P;

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return true;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public AnonymousClass3MP(Context context, View view, C63853De r5) {
        this.A07 = context;
        this.A0F = view;
        this.A0I = r5;
        this.A0J = new RunnableC55822jO(view, this);
        this.A0H = new RunnableBRunnable0Shape0S0220102_I1(view, this, 1);
        this.A0G = new RunnableBRunnable0Shape0S0220102_I1(view, this, 0);
        this.A0K = new RunnableC76183lD(view, this);
    }

    public final void A00() {
        if (this.A0N) {
            RectF rectF = this.A0B;
            float width = rectF.width();
            float height = rectF.height();
            View view = this.A0F;
            float A04 = (float) C12960it.A04(view, view.getWidth());
            float A03 = (float) C12960it.A03(view);
            this.A04 = 0.0f;
            Matrix matrix = this.A09;
            matrix.reset();
            this.A0C.set(0.0f, 0.0f, A04, A03);
            float f = A04 / width;
            float f2 = A03 / height;
            float min = Math.min(f, f2);
            this.A03 = min;
            float f3 = this.A01;
            float min2 = Math.min(min, f3);
            this.A03 = min2;
            if (Math.abs((f / f2) - 1.0f) < 0.0f) {
                min2 = Math.max(f, f2);
                this.A04 = min2;
            }
            this.A00 = Math.min(min2, f3);
            this.A04 = Math.min(this.A04, f3);
            this.A02 = Math.max(min2 * 8.0f, 8.0f);
            float f4 = width / 2.0f;
            float f5 = height / 2.0f;
            matrix.setTranslate((A04 / 2.0f) - f4, (A03 / 2.0f) - f5);
            float f6 = this.A00;
            matrix.preScale(f6, f6, f4, f5);
            this.A05 = this.A00;
            this.A0A.set(matrix);
            this.A08 = matrix;
            this.A0I.A00(matrix);
        }
    }

    public final void A01(float f, float f2, float f3) {
        float min = Math.min(Math.max(f, this.A03 * this.A06), this.A02);
        float f4 = min / this.A00;
        Matrix matrix = this.A09;
        matrix.postScale(f4, f4, f2, f3);
        this.A00 = min;
        A02(true);
        this.A0I.A00(matrix);
    }

    public final void A02(boolean z) {
        float f;
        RectF rectF = this.A0D;
        rectF.set(this.A0B);
        Matrix matrix = this.A09;
        matrix.mapRect(rectF);
        View view = this.A0F;
        float A02 = C12990iw.A02(view);
        float f2 = rectF.left;
        float f3 = rectF.right;
        float f4 = 0.0f;
        float f5 = A02 - 0.0f;
        if (f3 - f2 < f5) {
            f = ((f5 - (f3 + f2)) / 2.0f) + 0.0f;
        } else {
            f = f2 > 0.0f ? 0.0f - f2 : f3 < A02 ? A02 - f3 : 0.0f;
        }
        float A03 = C12990iw.A03(view);
        float f6 = rectF.top;
        float f7 = rectF.bottom;
        float f8 = A03 - 0.0f;
        if (f7 - f6 < f8) {
            f4 = 0.0f + ((f8 - (f7 + f6)) / 2.0f);
        } else if (f6 > 0.0f) {
            f4 = 0.0f - f6;
        } else if (f7 < A03) {
            f4 = A03 - f7;
        }
        if ((Math.abs(f) > 20.0f || Math.abs(f4) > 20.0f) && !z) {
            RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I1 = this.A0G;
            if (runnableBRunnable0Shape0S0220102_I1 != null && !runnableBRunnable0Shape0S0220102_I1.A05) {
                runnableBRunnable0Shape0S0220102_I1.A02 = -1;
                runnableBRunnable0Shape0S0220102_I1.A00 = f;
                runnableBRunnable0Shape0S0220102_I1.A01 = f4;
                runnableBRunnable0Shape0S0220102_I1.A06 = false;
                runnableBRunnable0Shape0S0220102_I1.A05 = true;
                ((View) runnableBRunnable0Shape0S0220102_I1.A04).postDelayed(runnableBRunnable0Shape0S0220102_I1, 250);
                return;
            }
            return;
        }
        matrix.postTranslate(f, f4);
        this.A0I.A00(matrix);
    }

    public final boolean A03(float f, float f2) {
        float max;
        float max2;
        RectF rectF = this.A0D;
        rectF.set(this.A0B);
        Matrix matrix = this.A09;
        matrix.mapRect(rectF);
        View view = this.A0F;
        float A02 = C12990iw.A02(view);
        float f3 = rectF.left;
        float f4 = rectF.right;
        float f5 = A02 - 0.0f;
        if (f4 - f3 < f5) {
            max = ((f5 - (f4 + f3)) / 2.0f) + 0.0f;
        } else {
            max = Math.max(A02 - f4, Math.min(0.0f - f3, f));
        }
        float A03 = C12990iw.A03(view);
        float f6 = rectF.top;
        float f7 = rectF.bottom;
        float f8 = A03 - 0.0f;
        if (f7 - f6 < f8) {
            max2 = ((f8 - (f7 + f6)) / 2.0f) + 0.0f;
        } else {
            max2 = Math.max(A03 - f7, Math.min(0.0f - f6, f2));
        }
        matrix.postTranslate(max, max2);
        this.A0I.A00(matrix);
        return max == f && max2 == f2;
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        float x;
        float y;
        boolean z = false;
        if (!this.A0M || !this.A0P) {
            return false;
        }
        if (!this.A0L) {
            float f = this.A00;
            float f2 = this.A03;
            float f3 = f2;
            if (f == f2) {
                f3 = 2.0f * f2;
            }
            float min = Math.min(this.A02, Math.max(f2, f3));
            RunnableC55822jO r5 = this.A0J;
            if (r5 != null) {
                if (min == f2) {
                    View view = this.A0F;
                    x = (float) (view.getWidth() >> 1);
                    y = (float) C13000ix.A00(view);
                } else {
                    x = motionEvent.getX();
                    y = motionEvent.getY();
                }
                r5.A00(f, min, x, y, 200);
            }
        }
        this.A0L = false;
        C63853De r3 = this.A0I;
        if (this.A00 != this.A03) {
            z = true;
        }
        r3.A01(z);
        return true;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        if (!this.A0P) {
            return true;
        }
        RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I1 = this.A0H;
        if (runnableBRunnable0Shape0S0220102_I1 != null) {
            C12990iw.A1L(runnableBRunnable0Shape0S0220102_I1);
        }
        RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I12 = this.A0G;
        if (runnableBRunnable0Shape0S0220102_I12 == null) {
            return true;
        }
        C12990iw.A1L(runnableBRunnable0Shape0S0220102_I12);
        return true;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        RunnableBRunnable0Shape0S0220102_I1 runnableBRunnable0Shape0S0220102_I1;
        if (!this.A0P || (runnableBRunnable0Shape0S0220102_I1 = this.A0H) == null || runnableBRunnable0Shape0S0220102_I1.A05) {
            return true;
        }
        runnableBRunnable0Shape0S0220102_I1.A02 = -1;
        runnableBRunnable0Shape0S0220102_I1.A00 = f;
        runnableBRunnable0Shape0S0220102_I1.A01 = f2;
        runnableBRunnable0Shape0S0220102_I1.A06 = false;
        runnableBRunnable0Shape0S0220102_I1.A05 = true;
        ((View) runnableBRunnable0Shape0S0220102_I1.A04).post(runnableBRunnable0Shape0S0220102_I1);
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        if (this.A0P) {
            this.A0O = false;
            A01(this.A00 * scaleGestureDetector.getScaleFactor(), scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
        }
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        if (!this.A0P) {
            return false;
        }
        RunnableC55822jO r1 = this.A0J;
        if (r1 != null) {
            r1.A06 = false;
            r1.A07 = true;
        }
        this.A0O = true;
        C63853De r2 = this.A0I;
        boolean z = false;
        if (this.A00 <= this.A03) {
            z = true;
        }
        r2.A01(z);
        return true;
    }

    @Override // android.view.ScaleGestureDetector.OnScaleGestureListener
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        RunnableC55822jO r2;
        if (this.A0P && this.A0O) {
            this.A0L = true;
            Matrix matrix = this.A09;
            matrix.set(this.A0A);
            this.A00 = this.A05;
            this.A0I.A00(matrix);
        }
        float f = this.A00;
        float f2 = this.A03;
        if (f < f2 && (r2 = this.A0J) != null) {
            View view = this.A0F;
            r2.A00(f, f2, (float) (view.getWidth() >> 1), (float) C13000ix.A00(view), 100);
        }
        C63853De r22 = this.A0I;
        boolean z = false;
        if (this.A00 <= this.A03) {
            z = true;
        }
        r22.A01(z);
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.A0P) {
            A03(-f, -f2);
        }
        return true;
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        View.OnClickListener onClickListener = this.A0E;
        if (onClickListener != null && !this.A0O && this.A0M) {
            onClickListener.onClick(this.A0F);
        }
        this.A0O = false;
        return true;
    }
}
