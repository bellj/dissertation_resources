package X;

import java.util.List;

/* renamed from: X.1KM  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1KM {
    public void A00() {
    }

    public void A01(String str, int i) {
        AnonymousClass1KT r0;
        AnonymousClass1KU r1;
        List list;
        if ((this instanceof AnonymousClass1KN) && (r1 = (r0 = ((AnonymousClass1KN) this).A00).A04) != null && (list = r0.A05) != null) {
            r1.AWh(str, r0.A0F, r0.A0E, r0.A0G, list, i);
        }
    }
}
