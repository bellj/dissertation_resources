package X;

import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.5oq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124245oq extends AbstractC16350or {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C124245oq(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity) {
        super(indiaUpiSendPaymentActivity);
        this.A00 = indiaUpiSendPaymentActivity;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        C17070qD r0 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0P;
        r0.A03();
        C119705ey r2 = (C119705ey) r0.A09.A05(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C);
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A06(C12960it.A0b("got contact vpa: ", r2));
        if (r2 == null || AnonymousClass1ZS.A02(r2.A02)) {
            A08(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C, new AnonymousClass6AO(indiaUpiSendPaymentActivity));
            ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0k = true;
            indiaUpiSendPaymentActivity.A2C(R.string.register_wait_message);
            return null;
        }
        if (AnonymousClass1ZS.A02(r2.A01)) {
            A08(((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C, new AnonymousClass6AP(indiaUpiSendPaymentActivity));
        }
        return r2;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity;
        C119705ey r3 = (C119705ey) obj;
        if (r3 != null) {
            indiaUpiSendPaymentActivity = this.A00;
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08 = r3.A02;
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0M = r3.A03;
            if (!AnonymousClass1ZS.A02(r3.A01)) {
                ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A06 = r3.A01;
            }
        } else {
            indiaUpiSendPaymentActivity = this.A00;
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08 = null;
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0M = null;
        }
        indiaUpiSendPaymentActivity.A3c();
    }

    public final void A08(UserJid userJid, AnonymousClass6MQ r20) {
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        C14850m9 r8 = ((ActivityC13810kN) indiaUpiSendPaymentActivity).A0C;
        C14900mE r5 = ((ActivityC13810kN) indiaUpiSendPaymentActivity).A05;
        C15570nT r6 = ((ActivityC13790kL) indiaUpiSendPaymentActivity).A01;
        C17220qS r9 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0H;
        C17070qD r15 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0P;
        C21860y6 r11 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0I;
        C18610sj r14 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0M;
        AnonymousClass102 r7 = ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A02;
        AnonymousClass6BE r1 = ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0D;
        C18650sn r12 = ((AbstractActivityC121685jC) indiaUpiSendPaymentActivity).A0K;
        C120495gH r3 = new C120495gH(indiaUpiSendPaymentActivity, r5, r6, r7, r8, r9, ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0B, r11, r12, ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A06, r14, r15, r1, ((AbstractActivityC121545iU) indiaUpiSendPaymentActivity).A0B);
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0m.A06(C12960it.A0b("sendGetContactInfoForJid: ", userJid));
        r3.A00(userJid, r20, ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0C.A03());
    }
}
