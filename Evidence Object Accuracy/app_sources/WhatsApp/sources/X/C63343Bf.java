package X;

import java.util.Set;

/* renamed from: X.3Bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C63343Bf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ AbstractC116445Vl A02;
    public final /* synthetic */ AbstractC35511i9 A03;
    public final /* synthetic */ C16170oZ A04;
    public final /* synthetic */ AnonymousClass19J A05;
    public final /* synthetic */ Integer A06;
    public final /* synthetic */ Set A07;
    public final /* synthetic */ boolean A08;

    public /* synthetic */ C63343Bf(AbstractC116445Vl r1, AbstractC35511i9 r2, C16170oZ r3, AnonymousClass19J r4, Integer num, Set set, int i, int i2, boolean z) {
        this.A05 = r4;
        this.A04 = r3;
        this.A03 = r2;
        this.A02 = r1;
        this.A08 = z;
        this.A00 = i;
        this.A06 = num;
        this.A01 = i2;
        this.A07 = set;
    }
}
