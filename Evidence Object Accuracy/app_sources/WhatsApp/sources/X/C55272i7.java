package X;

/* renamed from: X.2i7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55272i7 extends AnonymousClass0FK {
    public final C71323ck A00;

    public C55272i7(AnonymousClass02M r2, C15570nT r3, C15610nY r4) {
        super(r2);
        this.A00 = new C71323ck(r3, r4);
    }

    @Override // X.AnonymousClass0Z4
    public boolean A01(Object obj, Object obj2) {
        return C12970iu.A1Z(((C92624Wr) obj).A01, ((C92624Wr) obj2).A01);
    }

    @Override // X.AnonymousClass0Z4
    public boolean A02(Object obj, Object obj2) {
        C92624Wr r3 = (C92624Wr) obj;
        C92624Wr r4 = (C92624Wr) obj2;
        int i = r3.A00;
        if (i == 1 && i == r4.A00) {
            return ((AnonymousClass3FF) r3.A01).A03.equals(((AnonymousClass3FF) r4.A01).A03);
        }
        return false;
    }

    @Override // X.AnonymousClass0Z4, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        C92624Wr r4 = (C92624Wr) obj;
        C92624Wr r5 = (C92624Wr) obj2;
        int i = r4.A00;
        if (i == 1 && i == r5.A00) {
            return this.A00.compare((AnonymousClass3FF) r4.A01, (AnonymousClass3FF) r5.A01);
        }
        return i - r5.A00;
    }
}
