package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Vp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30011Vp implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1VZ(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1VZ[i];
    }
}
