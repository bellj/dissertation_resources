package X;

import java.util.Map;

/* renamed from: X.3A9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3A9 {
    public static AnonymousClass28D A00(C14260l7 r8, C14230l4 r9, AnonymousClass28D r10) {
        C14270l8 A03 = AnonymousClass3JV.A03(r8);
        C94244bU r6 = r9.A01;
        if (r6 == null) {
            AnonymousClass3J3.A02("Tree resources can only be read from the UI Thread");
            r6 = A03.A04;
            Map map = A03.A08;
            if (map != null) {
                r6 = new C94244bU(r6.A02, r6.A01, map, r6.A00);
            }
        }
        C90764Pd r92 = ((C14240l5) r9).A01;
        AnonymousClass4TT A00 = AnonymousClass3G5.A00(AnonymousClass4Yz.A00, r8, null, r6, r10, C1093551j.A00, r92);
        if (!A00.A03.isEmpty()) {
            C28691Op.A00("BloksBind", "Undefined Behavior: BloksBind::evaluate() returned controller binding operations");
        }
        return A00.A01;
    }
}
