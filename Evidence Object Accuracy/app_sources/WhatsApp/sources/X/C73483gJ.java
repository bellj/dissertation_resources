package X;

import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.support.DescribeProblemActivity;

/* renamed from: X.3gJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73483gJ extends ClickableSpan {
    public final /* synthetic */ DescribeProblemActivity A00;

    public C73483gJ(DescribeProblemActivity describeProblemActivity) {
        this.A00 = describeProblemActivity;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        DescribeProblemActivity describeProblemActivity = this.A00;
        describeProblemActivity.startActivity(new Intent("android.intent.action.VIEW", describeProblemActivity.A01));
    }
}
