package X;

import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape7S0100000_I1_1;

/* renamed from: X.40a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C848440a extends AbstractC75703kH {
    public C848440a(View view, AnonymousClass2Jw r2) {
        super(view, r2);
    }

    @Override // X.AbstractC75703kH
    public void A08(AnonymousClass4UW r4) {
        this.A0H.setOnClickListener(new ViewOnClickCListenerShape7S0100000_I1_1(this, 17));
    }
}
