package X;

/* renamed from: X.4wU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106914wU implements AnonymousClass5WY {
    public final /* synthetic */ AnonymousClass5WY A00;
    public final /* synthetic */ C106854wO A01;

    public C106914wU(AnonymousClass5WY r1, C106854wO r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A00.ACc();
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        C92684Xa AGX = this.A00.AGX(j);
        C94324bc r0 = AGX.A00;
        long j2 = r0.A01;
        long j3 = r0.A00;
        long j4 = this.A01.A00;
        C94324bc r4 = new C94324bc(j2, j3 + j4);
        C94324bc r02 = AGX.A01;
        return C92684Xa.A00(r4, r02.A01, r02.A00 + j4);
    }

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return this.A00.AK2();
    }
}
