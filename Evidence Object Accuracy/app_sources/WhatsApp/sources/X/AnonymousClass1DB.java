package X;

import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.SparseIntArray;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1DB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DB {
    public final C16210od A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final C15450nH A03;
    public final C15810nw A04;
    public final C17050qB A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final C15890o4 A08;
    public final C14820m6 A09;
    public final C19350ty A0A;
    public final C16120oU A0B;
    public final C15510nN A0C;
    public final AtomicBoolean A0D = new AtomicBoolean(false);

    public AnonymousClass1DB(C16210od r3, AbstractC15710nm r4, C14330lG r5, C15450nH r6, C15810nw r7, C17050qB r8, C14830m7 r9, C16590pI r10, C15890o4 r11, C14820m6 r12, C19350ty r13, C16120oU r14, C15510nN r15) {
        this.A07 = r10;
        this.A06 = r9;
        this.A01 = r4;
        this.A02 = r5;
        this.A0B = r14;
        this.A04 = r7;
        this.A03 = r6;
        this.A0A = r13;
        this.A05 = r8;
        this.A08 = r11;
        this.A09 = r12;
        this.A0C = r15;
        this.A00 = r3;
    }

    public static void A00(Uri uri, C16590pI r7, File file) {
        try {
            ContentResolver contentResolver = r7.A00.getContentResolver();
            StringBuilder sb = new StringBuilder();
            sb.append(file.getAbsolutePath());
            sb.append('%');
            contentResolver.delete(uri, "_data LIKE ?", new String[]{sb.toString()});
        } catch (SecurityException e) {
            StringBuilder sb2 = new StringBuilder("externaldirmigration/unscan failed for ");
            sb2.append(file);
            Log.e(sb2.toString(), e);
        }
    }

    public final int A01() {
        int i = this.A09.A00.getInt("external_dir_migration_stage", 0);
        if (i >= 0 && i <= 5) {
            return i;
        }
        StringBuilder sb = new StringBuilder("externaldirmigration/unexpected stage (");
        sb.append(i);
        sb.append(") resetting to not started");
        Log.e(sb.toString());
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0055, code lost:
        if ((!new java.io.File(r28, ".nomedia").exists()) == false) goto L_0x0057;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0154 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A02(android.util.SparseIntArray r25, X.AnonymousClass4RK r26, java.io.File r27, java.io.File r28, boolean r29) {
        /*
        // Method dump skipped, instructions count: 452
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1DB.A02(android.util.SparseIntArray, X.4RK, java.io.File, java.io.File, boolean):int");
    }

    public final void A03(SparseIntArray sparseIntArray, int i) {
        String obj;
        if (sparseIntArray != null) {
            int i2 = sparseIntArray.get(i, 0);
            sparseIntArray.put(i, i2 + 1);
            if (i2 != 0) {
                return;
            }
        }
        if (i == 2 || i == 3 || i == 4 || i == 6) {
            AbstractC15710nm r2 = this.A01;
            StringBuilder sb = new StringBuilder("externaldirmigration/failed: ");
            Integer valueOf = Integer.valueOf(i);
            if (valueOf == null) {
                obj = null;
            } else {
                obj = valueOf.toString();
            }
            sb.append(obj);
            r2.AaV(sb.toString(), null, true);
        }
        C855943k r5 = new C855943k();
        r5.A00 = Integer.valueOf(i);
        SharedPreferences sharedPreferences = this.A09.A00;
        r5.A01 = Long.valueOf(sharedPreferences.getLong("external_dir_migration_attempt_n", -1));
        r5.A04 = Long.valueOf(sharedPreferences.getLong("ext_dir_migration_rescan_time", -1));
        r5.A03 = Long.valueOf(sharedPreferences.getLong("ext_dir_migration_move_time", -1));
        r5.A02 = Long.valueOf(System.currentTimeMillis() - sharedPreferences.getLong("ext_dir_migration_start_time", 0));
        this.A0B.A0B(r5, null, false);
    }

    public final void A04(File file, List list, Set set) {
        File[] listFiles;
        if (file.exists() && (listFiles = file.listFiles()) != null) {
            boolean z = !new File(file, ".nomedia").exists();
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    if (set.add(file2)) {
                        A04(file2, list, set);
                    }
                } else if (z) {
                    list.add(file2);
                }
            }
        }
    }

    public boolean A05() {
        if (!this.A0C.A01() || !this.A03.A05(AbstractC15460nI.A19) || this.A04.A05() == null || A01() == 5) {
            return false;
        }
        return true;
    }
}
