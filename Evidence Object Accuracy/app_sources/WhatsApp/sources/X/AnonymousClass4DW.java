package X;

/* renamed from: X.4DW  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4DW {
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0036, code lost:
        if (r2 <= 126) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(X.AbstractC111925Bi r6) {
        /*
            X.4IW r0 = new X.4IW
            r0.<init>(r6)
            X.5Bi r5 = r0.A00
            int r0 = r5.A02()
            java.lang.StringBuilder r4 = X.C12980iv.A0t(r0)
            r3 = 0
        L_0x0010:
            int r0 = r5.A02()
            if (r3 >= r0) goto L_0x00be
            r6 = r5
            X.3rz r6 = (X.C80273rz) r6
            boolean r0 = r6 instanceof X.C80263ry
            if (r0 != 0) goto L_0x007c
            byte[] r0 = r6.zzb
            byte r2 = r0[r3]
        L_0x0021:
            r0 = 34
            if (r2 == r0) goto L_0x0073
            r0 = 39
            if (r2 == r0) goto L_0x0070
            r1 = 92
            if (r2 == r1) goto L_0x006d
            switch(r2) {
                case 7: goto L_0x0076;
                case 8: goto L_0x006a;
                case 9: goto L_0x0067;
                case 10: goto L_0x0064;
                case 11: goto L_0x0061;
                case 12: goto L_0x005e;
                case 13: goto L_0x005b;
                default: goto L_0x0030;
            }
        L_0x0030:
            r0 = 32
            if (r2 < r0) goto L_0x003f
            r0 = 126(0x7e, float:1.77E-43)
            if (r2 > r0) goto L_0x003f
        L_0x0038:
            char r0 = (char) r2
            r4.append(r0)
        L_0x003c:
            int r3 = r3 + 1
            goto L_0x0010
        L_0x003f:
            r4.append(r1)
            int r0 = r2 >>> 6
            r0 = r0 & 3
            int r0 = r0 + 48
            char r0 = (char) r0
            r4.append(r0)
            int r0 = r2 >>> 3
            r0 = r0 & 7
            int r0 = r0 + 48
            char r0 = (char) r0
            r4.append(r0)
            r0 = r2 & 7
            int r2 = r0 + 48
            goto L_0x0038
        L_0x005b:
            java.lang.String r0 = "\\r"
            goto L_0x0078
        L_0x005e:
            java.lang.String r0 = "\\f"
            goto L_0x0078
        L_0x0061:
            java.lang.String r0 = "\\v"
            goto L_0x0078
        L_0x0064:
            java.lang.String r0 = "\\n"
            goto L_0x0078
        L_0x0067:
            java.lang.String r0 = "\\t"
            goto L_0x0078
        L_0x006a:
            java.lang.String r0 = "\\b"
            goto L_0x0078
        L_0x006d:
            java.lang.String r0 = "\\\\"
            goto L_0x0078
        L_0x0070:
            java.lang.String r0 = "\\'"
            goto L_0x0078
        L_0x0073:
            java.lang.String r0 = "\\\""
            goto L_0x0078
        L_0x0076:
            java.lang.String r0 = "\\a"
        L_0x0078:
            r4.append(r0)
            goto L_0x003c
        L_0x007c:
            X.3ry r6 = (X.C80263ry) r6
            int r2 = r6.zzd
            int r0 = r3 + 1
            int r0 = r2 - r0
            r0 = r0 | r3
            if (r0 >= 0) goto L_0x009b
            if (r3 >= 0) goto L_0x00a4
            r0 = 22
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "Index < 0: "
            java.lang.String r1 = X.C12960it.A0e(r0, r1, r3)
            java.lang.ArrayIndexOutOfBoundsException r0 = new java.lang.ArrayIndexOutOfBoundsException
            r0.<init>(r1)
            throw r0
        L_0x009b:
            byte[] r1 = r6.zzb
            int r0 = r6.zzc
            int r0 = r0 + r3
            byte r2 = r1[r0]
            goto L_0x0021
        L_0x00a4:
            r0 = 40
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "Index > length: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = ", "
            java.lang.String r1 = X.C12960it.A0e(r0, r1, r2)
            java.lang.ArrayIndexOutOfBoundsException r0 = new java.lang.ArrayIndexOutOfBoundsException
            r0.<init>(r1)
            throw r0
        L_0x00be:
            java.lang.String r0 = r4.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4DW.A00(X.5Bi):java.lang.String");
    }
}
