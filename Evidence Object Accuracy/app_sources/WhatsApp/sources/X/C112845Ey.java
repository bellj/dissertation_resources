package X;

import java.util.Iterator;

/* renamed from: X.5Ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112845Ey implements AnonymousClass1WO {
    public final int A00;
    public final CharSequence A01;
    public final AnonymousClass5ZQ A02;

    public C112845Ey(CharSequence charSequence, AnonymousClass5ZQ r2, int i) {
        this.A01 = charSequence;
        this.A00 = i;
        this.A02 = r2;
    }

    @Override // X.AnonymousClass1WO
    public Iterator iterator() {
        return new C71363co(this);
    }
}
