package X;

import android.os.Build;
import java.util.HashSet;

/* renamed from: X.5zM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130405zM {
    public static final HashSet A00 = new AnonymousClass6L7();
    public static final HashSet A01 = new AnonymousClass6L6();
    public static final HashSet A02 = new AnonymousClass6L5();

    public static boolean A00() {
        return Build.VERSION.SDK_INT >= 29 || C1308760h.A02(A01);
    }
}
