package X;

import android.hardware.Camera;
import android.util.Log;
import java.util.List;

/* renamed from: X.5ct  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119105ct extends AbstractC125485rK {
    public C119055co A00;
    public AnonymousClass618 A01;

    public C119105ct(Camera.Parameters parameters, Camera camera, C119055co r9, C119075cq r10, int i) {
        this.A00 = r9;
        this.A01 = new AnonymousClass618(parameters, camera, r9, r10, i);
    }

    public static boolean A00(AnonymousClass618 r1, C125475rJ r2, int i, boolean z) {
        return z | r1.A02(r2, Integer.valueOf(i));
    }

    public static boolean A01(AnonymousClass618 r0, C125475rJ r1, Object obj, boolean z) {
        return z | r0.A02(r1, obj);
    }

    public void A02() {
        A04(super.A00.A00());
    }

    public void A03() {
        List A0Y = C117295Zj.A0Y(AbstractC130695zp.A0k, this.A00);
        int i = 4;
        if (!C117305Zk.A1Z(A0Y, 4)) {
            i = 1;
            if (!C117305Zk.A1Z(A0Y, 1)) {
                i = 6;
                if (!C117305Zk.A1Z(A0Y, 6)) {
                    return;
                }
            }
        }
        super.A00.A01(AbstractC130685zo.A0C, Integer.valueOf(i));
    }

    public void A04(C128385w1 r40) {
        C119075cq r2;
        boolean z;
        String str;
        super.A00 = new C129765yG();
        AnonymousClass618 r15 = this.A01;
        try {
            r2 = (C119075cq) r15.A04.clone();
        } catch (CloneNotSupportedException e) {
            Log.e("ParametersModificationApplier", "Could not clone the camera settings", e);
            r2 = null;
        }
        boolean z2 = r40.A0y;
        if (z2) {
            z = r15.A02(AbstractC130685zo.A0C, Integer.valueOf(r40.A0D));
        } else {
            z = false;
        }
        boolean z3 = r40.A0a;
        if (z3) {
            z = A00(r15, AbstractC130685zo.A00, r40.A07, z);
        }
        boolean z4 = r40.A0p;
        if (z4) {
            z = A00(r15, AbstractC130685zo.A06, r40.A09, z);
        }
        boolean z5 = r40.A0l;
        if (z5) {
            z = A01(r15, AbstractC130685zo.A0O, Boolean.valueOf(r40.A0k), z);
        }
        boolean z6 = r40.A0w;
        if (z6) {
            z = A00(r15, AbstractC130685zo.A0A, r40.A0C, z);
        }
        boolean z7 = r40.A0s;
        if (z7) {
            z = A00(r15, AbstractC130685zo.A08, r40.A0B, z);
        }
        boolean z8 = r40.A0x;
        if (z8) {
            z = A01(r15, AbstractC130685zo.A0B, r40.A0Y, z);
        }
        boolean z9 = r40.A1A;
        if (z9) {
            z = A00(r15, AbstractC130685zo.A0X, r40.A0F, z);
        }
        boolean z10 = r40.A1B;
        if (z10) {
            z = A00(r15, AbstractC130685zo.A0Y, r40.A0G, z);
        }
        boolean z11 = r40.A1C;
        if (z11) {
            z = A01(r15, AbstractC130685zo.A0Z, r40.A0R, z);
        }
        boolean z12 = r40.A1E;
        if (z12) {
            z = A01(r15, AbstractC130685zo.A0b, r40.A0Z, z);
        }
        boolean z13 = r40.A1I;
        if (z13) {
            z = A00(r15, AbstractC130685zo.A0e, r40.A0I, z);
        }
        boolean z14 = r40.A1J;
        if (z14) {
            z = A01(r15, AbstractC130685zo.A0g, r40.A0S, z);
        }
        boolean z15 = r40.A1K;
        if (z15) {
            z = A00(r15, AbstractC130685zo.A0i, r40.A0J, z);
        }
        boolean z16 = r40.A1M;
        if (z16) {
            z = A00(r15, AbstractC130685zo.A0k, r40.A0K, z);
        }
        boolean z17 = r40.A1L;
        if (z17) {
            z = A01(r15, AbstractC130685zo.A0j, r40.A1d, z);
        }
        boolean z18 = r40.A1N;
        if (z18) {
            z = A01(r15, AbstractC130685zo.A0m, r40.A0T, z);
        }
        boolean z19 = r40.A1S;
        if (z19) {
            z = A00(r15, AbstractC130685zo.A0o, r40.A0L, z);
        }
        boolean z20 = r40.A1X;
        if (z20) {
            z = A01(r15, AbstractC130685zo.A0W, Boolean.valueOf(r40.A1W), z);
        }
        boolean z21 = r40.A1V;
        if (z21) {
            z = A01(r15, AbstractC130685zo.A0s, r40.A0U, z);
        }
        boolean z22 = r40.A1Y;
        if (z22) {
            z = A00(r15, AbstractC130685zo.A0t, r40.A0N, z);
        }
        boolean z23 = r40.A1a;
        if (z23) {
            z = A00(r15, AbstractC130685zo.A0v, r40.A0O, z);
        }
        boolean z24 = r40.A17;
        if (z24) {
            z = A01(r15, AbstractC130685zo.A0T, Boolean.valueOf(r40.A16), z);
        }
        boolean z25 = r40.A1R;
        if (z25) {
            z = A01(r15, AbstractC130685zo.A0n, Boolean.valueOf(r40.A1Q), z);
        }
        boolean z26 = r40.A11;
        if (z26) {
            z = A01(r15, AbstractC130685zo.A0D, Double.valueOf(r40.A01), z);
        }
        boolean z27 = r40.A12;
        if (z27) {
            z = A01(r15, AbstractC130685zo.A0E, Double.valueOf(r40.A02), z);
        }
        boolean z28 = r40.A13;
        if (z28) {
            z = A01(r15, AbstractC130685zo.A0F, Double.valueOf(r40.A03), z);
        }
        boolean z29 = r40.A14;
        if (z29) {
            z = A01(r15, AbstractC130685zo.A0G, r40.A0W, z);
        }
        boolean z30 = r40.A15;
        if (z30) {
            z = A01(r15, AbstractC130685zo.A0H, Long.valueOf(r40.A0Q), z);
        }
        boolean z31 = r40.A1H;
        if (z31) {
            z = A00(r15, AbstractC130685zo.A0c, r40.A0H, z);
        }
        if (r40.A1U) {
            z = A00(r15, AbstractC130685zo.A0r, r40.A0M, z);
        }
        boolean z32 = r40.A19;
        if (z32) {
            z = A00(r15, AbstractC130685zo.A0I, r40.A0E, z);
        }
        if (r40.A0r) {
            z = A01(r15, AbstractC130685zo.A0h, null, z);
        }
        boolean z33 = r40.A18;
        if (z33) {
            z = A01(r15, AbstractC130685zo.A0S, Boolean.valueOf(z33), z);
        }
        if (r40.A0g) {
            z = A01(r15, AbstractC130685zo.A0K, Boolean.valueOf(r40.A0f), z);
        }
        if (z) {
            AnonymousClass618.A05.incrementAndGet();
            try {
                AnonymousClass616.A00();
                r15.A02.setParameters(r15.A01);
                AnonymousClass616.A00();
            } catch (RuntimeException e2) {
                AnonymousClass616.A00();
                Object[] objArr = new Object[4];
                if (r2 != null) {
                    str = r2.A04();
                } else {
                    str = "null";
                }
                objArr[0] = str;
                objArr[1] = r15.A04.A04();
                objArr[2] = r15.A01.flatten();
                StringBuilder A0h = C12960it.A0h();
                if (z2) {
                    A0h.append("focusMode=");
                    A0h.append(r40.A0D);
                }
                if (r40.A1D) {
                    A0h.append("lensFocusDistance=");
                    A0h.append(r40.A05);
                }
                if (r40.A00) {
                    A0h.append(",autoExposureEnabled=");
                    A0h.append(r40.A0h);
                }
                if (z3) {
                    A0h.append(",antibanding=");
                    A0h.append(r40.A07);
                }
                if (z4) {
                    A0h.append(",colorEffect=");
                    A0h.append(r40.A09);
                }
                if (z5) {
                    A0h.append(",autoWhiteBalanceLock=");
                    A0h.append(r40.A0k);
                }
                if (z6) {
                    A0h.append(",flashMode=");
                    A0h.append(r40.A0C);
                }
                if (z7) {
                    A0h.append(",exposureCompensation=");
                    A0h.append(r40.A0B);
                }
                if (z8) {
                    A0h.append(",focusAreas=");
                    A0h.append(AnonymousClass61X.A01(r40.A0Y));
                }
                if (z9) {
                    A0h.append(",jpegQuality=");
                    A0h.append(r40.A0F);
                }
                if (z10) {
                    A0h.append(",jpegThumbnailQuality=");
                    A0h.append(r40.A0G);
                }
                if (z11) {
                    A0h.append(",jpegThumbnailSize=");
                    C129845yO.A01(r40.A0R, A0h);
                }
                if (z12) {
                    A0h.append(",meteringAreas=");
                    A0h.append(AnonymousClass61X.A01(r40.A0Z));
                }
                if (z13) {
                    A0h.append(",pictureFormat=");
                    A0h.append(r40.A0I);
                }
                if (z14) {
                    A0h.append(",pictureSize=");
                    C129845yO.A01(r40.A0S, A0h);
                }
                if (r40.A1Z) {
                    A0h.append(",yuvPictureSize=");
                    C129845yO.A01(r40.A0V, A0h);
                }
                if (z15) {
                    A0h.append(",previewFormat=");
                    A0h.append(r40.A0J);
                }
                if (z16) {
                    A0h.append(",previewFrameRate=");
                    A0h.append(r40.A0K);
                }
                if (z17) {
                    A0h.append(",previewFrameRateRange=");
                    int[] iArr = r40.A1d;
                    A0h.append(iArr[0]);
                    A0h.append('-');
                    A0h.append(iArr[1]);
                }
                if (z18) {
                    A0h.append(",previewSize=");
                    C129845yO.A01(r40.A0T, A0h);
                }
                if (z19) {
                    A0h.append(",sceneMode=");
                    A0h.append(r40.A0L);
                }
                if (z20) {
                    A0h.append(",videoStabilizationEnabled=");
                    A0h.append(r40.A1W);
                }
                if (r40.A1P) {
                    A0h.append(",previewStabilizationEnabled=");
                    A0h.append(r40.A1O);
                }
                if (r40.A1G) {
                    A0h.append(",opticalStabilizationEnabled=");
                    A0h.append(r40.A1F);
                }
                if (z21) {
                    A0h.append(",videoSize=");
                    C129845yO.A01(r40.A0U, A0h);
                }
                if (z22) {
                    A0h.append(",whiteBalance=");
                    A0h.append(r40.A0N);
                }
                if (z23) {
                    A0h.append(",zoom=");
                    A0h.append(r40.A0O);
                }
                if (r40.A1T) {
                    A0h.append(",smoothZoom=");
                    A0h.append(r40.A06);
                }
                if (z24) {
                    A0h.append(",hdrEnabled=");
                    A0h.append(r40.A16);
                }
                if (z25) {
                    A0h.append(",recordingHint=");
                    A0h.append(r40.A1Q);
                }
                if (z26) {
                    A0h.append(",gpsAltitude=");
                    A0h.append(r40.A01);
                }
                if (z27) {
                    A0h.append(",gpsLatitude=");
                    A0h.append(r40.A02);
                }
                if (z28) {
                    A0h.append(",gpsLongitude=");
                    A0h.append(r40.A03);
                }
                if (z29) {
                    A0h.append(",gpsProcessingMethod=");
                    A0h.append(r40.A0W);
                }
                if (z30) {
                    A0h.append(",gpsTimestamp=");
                    A0h.append(r40.A0Q);
                }
                if (z31) {
                    A0h.append(",photoRotation=");
                    A0h.append(r40.A0H);
                }
                if (z32) {
                    A0h.append(",isoSensitivity=");
                    A0h.append(r40.A0E);
                }
                if (r40.A0v) {
                    A0h.append(",exposureTime=");
                    A0h.append(r40.A0P);
                }
                if (r40.A0q) {
                    A0h.append(",control3aMode=");
                    A0h.append(r40.A0A);
                }
                if (r40.A0b) {
                    A0h.append(",aperture=");
                    A0h.append(r40.A04);
                }
                if (r40.A0m) {
                    A0h.append(",colorCorrectionGains=");
                    A0h.append(r40.A1b);
                }
                if (r40.A0n) {
                    A0h.append(",colorCorrectionMode=");
                    A0h.append(r40.A08);
                }
                if (r40.A0o) {
                    A0h.append(",colorCorrectionTransform=");
                    A0h.append(r40.A1c);
                }
                if (r40.A10) {
                    A0h.append(",frameMetaDataEnabled=");
                    A0h.append(r40.A0z);
                }
                if (r40.A0e) {
                    A0h.append(",arCoreEnabled=");
                    A0h.append(r40.A0d);
                }
                objArr[3] = A0h.toString();
                throw new RuntimeException(String.format("Failed to apply parameters. previous settings: (%s), new settings: (%s), parameters: (%s), modifications: (%s)", objArr), e2);
            }
        }
    }
}
