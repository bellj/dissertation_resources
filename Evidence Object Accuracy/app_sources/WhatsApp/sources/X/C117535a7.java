package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/* renamed from: X.5a7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117535a7 extends ClickableSpan {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C123365n2 A01;

    public C117535a7(C123365n2 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        C127055ts.A00(((AbstractC118045bB) this.A01).A01, this.A00);
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        C117295Zj.A0k(C16590pI.A00(this.A01.A01), textPaint);
    }
}
