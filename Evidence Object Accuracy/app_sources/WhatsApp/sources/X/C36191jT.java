package X;

import android.view.View;

/* renamed from: X.1jT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C36191jT {
    public final View A00;
    public final ActivityC13810kN A01;
    public final C14900mE A02;
    public final C15450nH A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final C18640sm A06;
    public final AnonymousClass018 A07;
    public final C21320xE A08;
    public final C15600nX A09;
    public final C20710wC A0A;
    public final C15580nU A0B;
    public final C20660w7 A0C;
    public final C14860mA A0D;

    public C36191jT(View view, ActivityC13810kN r2, C14900mE r3, C15450nH r4, C15550nR r5, C15610nY r6, C18640sm r7, AnonymousClass018 r8, C21320xE r9, C15600nX r10, C20710wC r11, C15580nU r12, C20660w7 r13, C14860mA r14) {
        this.A00 = view;
        this.A0B = r12;
        this.A01 = r2;
        this.A02 = r3;
        this.A0D = r14;
        this.A0C = r13;
        this.A03 = r4;
        this.A07 = r8;
        this.A05 = r6;
        this.A04 = r5;
        this.A0A = r11;
        this.A06 = r7;
        this.A08 = r9;
        this.A09 = r10;
    }
}
