package X;

import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1EJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EJ {
    public final C15570nT A00;
    public final C16510p9 A01;
    public final C18460sU A02;
    public final C16490p7 A03;
    public final C14850m9 A04;
    public final JniBridge A05;

    public AnonymousClass1EJ(C15570nT r1, C16510p9 r2, C18460sU r3, C16490p7 r4, C14850m9 r5, JniBridge jniBridge) {
        this.A04 = r5;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
        this.A05 = jniBridge;
        this.A03 = r4;
    }
}
