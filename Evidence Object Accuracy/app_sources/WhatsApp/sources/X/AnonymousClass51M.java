package X;

/* renamed from: X.51M  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass51M implements AnonymousClass28S {
    public final /* synthetic */ boolean A00;

    public /* synthetic */ AnonymousClass51M(boolean z) {
        this.A00 = z;
    }

    @Override // X.AnonymousClass28S
    public final boolean A66(Object obj) {
        boolean z = this.A00;
        C15370n3 r4 = (C15370n3) obj;
        if (r4 == null) {
            return false;
        }
        C29951Vj r2 = r4.A0F;
        if (r2 == null) {
            return true;
        }
        if ((!z || r2.A00 != 3) && r2.A00 == 3) {
            return false;
        }
        return true;
    }
}
