package X;

import android.os.Build;
import android.view.View;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPickerSheet;

/* renamed from: X.2lX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56712lX extends AnonymousClass2UH {
    public final /* synthetic */ GroupCallParticipantPickerSheet A00;

    public C56712lX(GroupCallParticipantPickerSheet groupCallParticipantPickerSheet) {
        this.A00 = groupCallParticipantPickerSheet;
    }

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
        GroupCallParticipantPickerSheet groupCallParticipantPickerSheet = this.A00;
        int top = ((int) (((groupCallParticipantPickerSheet.A01 - ((float) view.getTop())) / groupCallParticipantPickerSheet.A00) * 127.0f)) << 24;
        groupCallParticipantPickerSheet.A02.setColor(top);
        if (Build.VERSION.SDK_INT >= 21) {
            groupCallParticipantPickerSheet.getWindow().setStatusBarColor(top);
        }
    }
}
