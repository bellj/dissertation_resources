package X;

/* renamed from: X.614  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass614 {
    public static int A00(C15450nH r2, C1310661b r3) {
        C16140oW r0;
        String str;
        if (r3.A0D || (str = r3.A00) == null || (!str.equals("GALLERY_QR_CODE") && !str.equals("DEEP_LINK"))) {
            r0 = AbstractC15460nI.A20;
        } else {
            r0 = AbstractC15460nI.A1z;
        }
        return r2.A02(r0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x006e, code lost:
        if (r0 == false) goto L_0x0070;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.content.Intent r4, X.C15450nH r5, X.C1310661b r6) {
        /*
            java.lang.Class<java.lang.String> r3 = java.lang.String.class
            X.2SM r2 = X.C117305Zk.A0J()
            java.lang.String r1 = r6.A0C
            java.lang.String r0 = "upiHandle"
            X.1ZR r1 = X.C117305Zk.A0I(r2, r3, r1, r0)
            java.lang.String r0 = "extra_payment_handle"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A02
            java.lang.String r0 = "extra_merchant_code"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A0B
            java.lang.String r0 = "extra_transaction_ref"
            r4.putExtra(r0, r1)
            X.2SM r2 = X.C117305Zk.A0J()
            java.lang.String r1 = r6.A04
            java.lang.String r0 = "accountHolderName"
            X.1ZR r1 = X.C117305Zk.A0I(r2, r3, r1, r0)
            java.lang.String r0 = "extra_payee_name"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A08
            java.lang.String r0 = "extra_transaction_ref_url"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A01
            java.lang.String r0 = "extra_initiation_mode"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A07
            java.lang.String r0 = "extra_purpose_code"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A05
            java.lang.String r0 = "extra_payment_preset_amount"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A0A
            java.lang.String r0 = "extra_transaction_id"
            r4.putExtra(r0, r1)
            java.lang.String r1 = r6.A06
            java.lang.String r0 = "extra_payment_preset_min_amount"
            r4.putExtra(r0, r1)
            java.lang.String r1 = "extra_skip_value_props_display"
            r0 = 0
            r4.putExtra(r1, r0)
            java.lang.String r1 = r6.A00
            if (r1 == 0) goto L_0x0070
            java.lang.String r0 = "DEEP_LINK"
            boolean r0 = r1.equals(r0)
            r1 = 9
            if (r0 != 0) goto L_0x0072
        L_0x0070:
            r1 = 8
        L_0x0072:
            java.lang.String r0 = "extra_payments_entry_type"
            r4.putExtra(r0, r1)
            int r0 = A00(r5, r6)
            java.lang.String r1 = java.lang.String.valueOf(r0)
            java.lang.String r0 = "extra_payment_preset_max_amount"
            r4.putExtra(r0, r1)
            java.lang.String r1 = "extra_is_first_payment_method"
            r0 = 1
            r4.putExtra(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass614.A01(android.content.Intent, X.0nH, X.61b):void");
    }

    public static boolean A02(String str) {
        return "photo_received".equals(str) || "photo_received_gallery".equals(str);
    }

    public static boolean A03(String str) {
        String str2;
        if (str != null) {
            switch (str.hashCode()) {
                case 1536:
                    str2 = "00";
                    break;
                case 1537:
                    str2 = "01";
                    break;
                case 1538:
                    str2 = "02";
                    break;
                case 1539:
                    str2 = "03";
                    break;
                case 1540:
                    str2 = "04";
                    break;
                case 1541:
                    str2 = "05";
                    break;
                case 1542:
                    str2 = "06";
                    break;
                case 1543:
                    str2 = "07";
                    break;
                case 1544:
                    str2 = "08";
                    break;
                case 1545:
                    str2 = "09";
                    break;
                default:
                    return false;
            }
            if (!str.equals(str2)) {
                return false;
            }
        }
        return true;
    }
}
