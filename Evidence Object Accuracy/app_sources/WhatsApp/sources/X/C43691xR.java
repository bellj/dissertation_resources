package X;

import android.app.Activity;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;

/* renamed from: X.1xR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C43691xR implements AbstractC43701xS {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass1P3 A01;
    public final /* synthetic */ C238013b A02;
    public final /* synthetic */ AnonymousClass1P2 A03;

    public /* synthetic */ C43691xR(Activity activity, AnonymousClass1P3 r2, C238013b r3, AnonymousClass1P2 r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = activity;
        this.A01 = r2;
    }

    @Override // X.AbstractC43701xS
    public final void A6f() {
        C238013b r2 = this.A02;
        r2.A0Q.Ab2(new RunnableBRunnable0Shape0S0400000_I0(r2, this.A03, this.A00, this.A01, 9));
    }
}
