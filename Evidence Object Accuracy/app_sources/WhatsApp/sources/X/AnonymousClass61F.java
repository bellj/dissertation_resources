package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.61F  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass61F {
    public AnonymousClass016 A00;
    public C1315463e A01;
    public C127645up A02;
    public C27691It A03;
    public Long A04;
    public Long A05;
    public String A06 = "";
    public final C14830m7 A07;
    public final AnonymousClass102 A08;
    public final C241414j A09;
    public final C18600si A0A;
    public final C17900ra A0B;
    public final C17070qD A0C;
    public final C1308160b A0D;
    public final C129435xi A0E;
    public final C130105yo A0F;
    public final C117885au A0G;
    public final C117885au A0H = new C117885au();
    public final C117885au A0I;

    public AnonymousClass61F(C14830m7 r2, AnonymousClass102 r3, C241414j r4, C18600si r5, C17900ra r6, C17070qD r7, C1308160b r8, C129435xi r9, C130105yo r10) {
        Long A0f = C117305Zk.A0f();
        this.A05 = A0f;
        this.A04 = A0f;
        this.A07 = r2;
        this.A09 = r4;
        this.A0D = r8;
        this.A0C = r7;
        this.A0A = r5;
        this.A0F = r10;
        this.A08 = r3;
        this.A0B = r6;
        this.A0E = r9;
        this.A0G = new C117885au();
        this.A0I = new C117885au();
    }

    public static AbstractC28901Pl A00(List list) {
        if (list.size() <= 0) {
            return null;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl A0H = C117305Zk.A0H(it);
            if ("Novi".equals(A0H.A0B)) {
                return A0H;
            }
        }
        return null;
    }

    public static AbstractC28901Pl A01(List list) {
        if (list.size() <= 0) {
            return null;
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AbstractC28901Pl A0H = C117305Zk.A0H(it);
            AnonymousClass1ZY r1 = A0H.A08;
            if (((r1 instanceof C119765f4) && ((C119765f4) r1).A06) || ((r1 instanceof C119735f1) && ((C119735f1) r1).A05)) {
                return A0H;
            }
        }
        return null;
    }

    public static C1315463e A02(AbstractC28901Pl r1) {
        AnonymousClass1ZY r12 = r1.A08;
        if (r12 instanceof C119805f8) {
            return ((C119805f8) r12).A02;
        }
        return null;
    }

    public AnonymousClass017 A03() {
        AnonymousClass016 r1 = this.A00;
        if (r1 != null) {
            return r1;
        }
        AnonymousClass016 r12 = new AnonymousClass016(Boolean.valueOf(A0H()));
        this.A00 = r12;
        return r12;
    }

    public C14580lf A04() {
        C14580lf r2 = new C14580lf();
        C14580lf A0C = C117305Zk.A0C(this.A0C);
        A0C.A00(new AbstractC14590lg(r2, A0C, this) { // from class: X.6Ed
            public final /* synthetic */ C14580lf A00;
            public final /* synthetic */ C14580lf A01;
            public final /* synthetic */ AnonymousClass61F A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C14580lf r22 = this.A00;
                C14580lf r1 = this.A01;
                r22.A02(AnonymousClass61F.A00((List) obj));
                r1.A04();
            }
        });
        return r2;
    }

    public List A05(C1315463e r8, List list) {
        String str;
        String str2;
        ArrayList A0l = C12960it.A0l();
        AnonymousClass602 A00 = this.A0E.A00();
        if (list.size() > 0 && A00 != null) {
            List A01 = A00.A01("balance_top_up");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AbstractC28901Pl A0H = C117305Zk.A0H(it);
                AnonymousClass1ZY r2 = A0H.A08;
                if (A01.contains("DEBIT") && (r2 instanceof C119765f4)) {
                    str = r8.A02;
                    str2 = ((C119765f4) r2).A03;
                } else if (A01.contains("BANK") && (r2 instanceof C119735f1)) {
                    str = r8.A02;
                    str2 = ((C119735f1) r2).A02;
                }
                if (str.equals(str2)) {
                    A0l.add(A0H);
                }
            }
        }
        return A0l;
    }

    public void A06() {
        if (A0H()) {
            AnonymousClass600.A03 = C12990iw.A0n();
            C1308160b r0 = this.A0D;
            r0.A02 = null;
            r0.A06 = null;
            A0C("");
            this.A0I.A0A(null);
            this.A02 = null;
            this.A0G.A0A(null);
        }
    }

    public void A07() {
        C27691It r2 = this.A03;
        if (r2 == null) {
            r2 = C13000ix.A03();
            this.A03 = r2;
        }
        C14580lf A04 = A04();
        A04.A00(new C134306Ec(r2, A04, this));
        this.A0H.A0A(Boolean.TRUE);
    }

    public void A08() {
        C241414j r4 = this.A09;
        List<AnonymousClass1ZO> A0C = r4.A0C();
        HashMap A11 = C12970iu.A11();
        A11.put(2, C117315Zl.A05("novi", "unset"));
        for (AnonymousClass1ZO r0 : A0C) {
            UserJid userJid = r0.A05;
            synchronized (r4) {
                r4.A0K(userJid, null, null, A11, null);
            }
        }
        C130105yo.A01(this.A0F).clear().apply();
        C1308160b r2 = this.A0D;
        r2.A06("alias-signing-key.data-fetch");
        r2.A06("alias-signing-key.trusted-app-install");
        r2.A00().edit().clear().apply();
        r2.A0A.A05();
        C117305Zk.A0C(this.A0C).A00(new AbstractC14590lg() { // from class: X.6EG
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AnonymousClass61F r42 = AnonymousClass61F.this;
                HashSet A12 = C12970iu.A12();
                Iterator it = ((List) obj).iterator();
                while (it.hasNext()) {
                    AbstractC28901Pl A0H = C117305Zk.A0H(it);
                    AnonymousClass1ZY r1 = A0H.A08;
                    if ((r1 instanceof C119805f8) || (r1 instanceof C119765f4) || (r1 instanceof C119735f1)) {
                        A12.add(A0H.A0A);
                    }
                }
                Iterator it2 = A12.iterator();
                while (it2.hasNext()) {
                    r42.A0C.A00().A02(C12970iu.A0x(it2));
                }
            }
        });
        AnonymousClass600.A03 = C12990iw.A0n();
        r2.A02 = null;
        r2.A06 = null;
        A0C("");
        this.A0I.A0A(null);
        this.A02 = null;
        this.A01 = null;
    }

    public synchronized void A09() {
        this.A05 = Long.valueOf(this.A07.A01() + 600000);
    }

    public void A0A(AbstractC451720l r12, C1315463e r13) {
        C30841Za r3;
        C12960it.A0t(C117295Zj.A05(this.A0A), "payments_has_willow_account", true);
        C119805f8 r1 = new C119805f8();
        r1.A0C(r13);
        r1.A0B(this.A08);
        if (r13.A00 == null) {
            C17930rd A01 = this.A0B.A01();
            AnonymousClass009.A05(A01);
            C30841Za r32 = new C30841Za(C17930rd.A00(A01.A03), r1.A02.A02, "Novi", r1.A06, r1.A03, 2, 2);
            r32.A08 = r1;
            r32.A00 = ((AnonymousClass1ZZ) r1).A00;
            r32.A0B = "Novi";
            r3 = r32;
        } else {
            r3 = r1.A05();
        }
        C130105yo r0 = this.A0F;
        C12970iu.A1D(C130105yo.A01(r0), "novi_account_id", r13.A02);
        this.A0C.A00().A03(new AbstractC451720l(r12, r13, this) { // from class: X.67z
            public final /* synthetic */ AbstractC451720l A00;
            public final /* synthetic */ C1315463e A01;
            public final /* synthetic */ AnonymousClass61F A02;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                AnonymousClass61F r02 = this.A02;
                C1315463e r14 = this.A01;
                AbstractC451720l r33 = this.A00;
                r02.A01 = r14;
                C130105yo r03 = r02.A0F;
                C12970iu.A1D(C130105yo.A01(r03), "wavi_kyc_status", r14.A03);
                if (r33 != null) {
                    r33.AM7(list);
                }
            }
        }, r3);
    }

    public void A0B(AbstractC136286Ly r3) {
        C1315463e r0 = this.A01;
        if (r0 != null) {
            r3.AT6(r0);
            return;
        }
        C14580lf A04 = A04();
        A04.A00(new AbstractC14590lg(A04, r3) { // from class: X.6EP
            public final /* synthetic */ C14580lf A00;
            public final /* synthetic */ AbstractC136286Ly A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C1315463e r02;
                AbstractC136286Ly r2 = this.A01;
                C14580lf r1 = this.A00;
                AbstractC28901Pl r4 = (AbstractC28901Pl) obj;
                if (r4 != null) {
                    r02 = AnonymousClass61F.A02(r4);
                } else {
                    r02 = null;
                }
                r2.AT6(r02);
                r1.A04();
            }
        });
    }

    public void A0C(String str) {
        this.A06 = str;
        synchronized (this) {
            if (TextUtils.isEmpty(str)) {
                this.A04 = C117305Zk.A0f();
            } else {
                this.A04 = Long.valueOf(this.A07.A01() + 3600000);
                A09();
            }
        }
        AnonymousClass016 r1 = this.A00;
        if (r1 == null) {
            r1 = new AnonymousClass016(Boolean.valueOf(A0H()));
            this.A00 = r1;
        }
        r1.A0A(Boolean.valueOf(A0H()));
    }

    public boolean A0D() {
        C129895yT A03 = this.A0F.A03();
        if (A03 == null || !A03.A04.contains("READ_DISABLED")) {
            return A0H();
        }
        return false;
    }

    public boolean A0E() {
        String string;
        C1315463e r0 = this.A01;
        if (r0 != null) {
            string = r0.A03;
        } else {
            string = this.A0F.A02().getString("wavi_kyc_status", "NOT_READY_FOR_ASSESSMENT");
            AnonymousClass009.A05(string);
        }
        return "ONBOARDED".equals(string);
    }

    public boolean A0F() {
        return this.A07.A00() <= C12980iv.A0F(this.A0F.A02(), "trusted_device_expiry_timestamp_sec") * 1000;
    }

    public boolean A0G() {
        C129895yT A03 = this.A0F.A03();
        if (A03 == null || !A03.A04.contains("READ_DISABLED")) {
            return A0F();
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r4 >= r6.A05.longValue()) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0H() {
        /*
            r6 = this;
            monitor-enter(r6)
            X.0m7 r0 = r6.A07     // Catch: all -> 0x0027
            long r4 = r0.A01()     // Catch: all -> 0x0027
            java.lang.String r0 = r6.A06     // Catch: all -> 0x0027
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch: all -> 0x0027
            if (r0 != 0) goto L_0x0024
            java.lang.Long r0 = r6.A04     // Catch: all -> 0x0027
            long r1 = r0.longValue()     // Catch: all -> 0x0027
            int r0 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0024
            java.lang.Long r0 = r6.A05     // Catch: all -> 0x0027
            long r2 = r0.longValue()     // Catch: all -> 0x0027
            int r1 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            monitor-exit(r6)
            return r0
        L_0x0027:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass61F.A0H():boolean");
    }
}
