package X;

/* renamed from: X.3Hw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65033Hw {
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r7.A07(r8) != false) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(android.widget.ProgressBar r6, X.AnonymousClass109 r7, X.AbstractC16130oV r8) {
        /*
            X.0oX r2 = X.AbstractC15340mz.A00(r8)
            boolean r0 = r2.A0a
            r1 = 0
            if (r0 == 0) goto L_0x005a
            boolean r0 = r2.A0Y
            if (r0 != 0) goto L_0x005a
            r6.setVisibility(r1)
            long r0 = r2.A0C
            int r5 = (int) r0
            X.1KC r0 = r7.A01(r8)
            if (r0 == 0) goto L_0x0035
            X.1qQ r0 = r0.A01()
            boolean r0 = r0.A0G
            if (r0 == 0) goto L_0x0035
            boolean r0 = r7.A06(r8)
            if (r0 == 0) goto L_0x002e
            boolean r1 = r7.A07(r8)
            r0 = 1
            if (r1 == 0) goto L_0x002f
        L_0x002e:
            r0 = 0
        L_0x002f:
            int r5 = r5 >> 1
            if (r0 != 0) goto L_0x0035
            int r5 = r5 + 50
        L_0x0035:
            long r3 = (long) r5
            r1 = 100
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0058
            boolean r0 = r7.A06(r8)
            if (r0 == 0) goto L_0x0048
            boolean r0 = r7.A07(r8)
            if (r0 == 0) goto L_0x0058
        L_0x0048:
            r1 = 1
        L_0x0049:
            if (r5 <= 0) goto L_0x0057
            r0 = 100
            if (r5 < r0) goto L_0x0051
            if (r1 == 0) goto L_0x0057
        L_0x0051:
            r6.setProgress(r5)
            r6.setIndeterminate(r1)
        L_0x0057:
            return r5
        L_0x0058:
            r1 = 0
            goto L_0x0049
        L_0x005a:
            r6.setProgress(r1)
            r0 = 1
            r6.setIndeterminate(r0)
            r0 = 8
            r6.setVisibility(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65033Hw.A00(android.widget.ProgressBar, X.109, X.0oV):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008e, code lost:
        if (r18.A07(r20) != false) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a5, code lost:
        if (r8 >= 0) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if (r18.A07(r20) != false) goto L_0x0026;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0134  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.widget.TextView r16, X.C50362Pg r17, X.AnonymousClass109 r18, X.C22370yy r19, X.AbstractC16130oV r20) {
        /*
        // Method dump skipped, instructions count: 348
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65033Hw.A01(android.widget.TextView, X.2Pg, X.109, X.0yy, X.0oV):void");
    }
}
