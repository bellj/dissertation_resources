package X;

import android.text.InputFilter;

/* renamed from: X.3I5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I5 {
    public static final InputFilter[] A00 = new InputFilter[0];

    public static int A00(String str) {
        if (str.equals("text_no_suggestion")) {
            return 655361;
        }
        if (!str.equals("numbers_and_punctuation")) {
            try {
                return AnonymousClass3JW.A08(str);
            } catch (AnonymousClass491 e) {
                C28691Op.A01("WaRcFormInputComponentBinderUtils", "Error parsing text input type", e);
            }
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0167, code lost:
        if ((r8 & 128) == 128) goto L_0x0169;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.util.Pair A01(android.view.View r11, X.C14260l7 r12, X.AnonymousClass28D r13, java.lang.String r14) {
        /*
        // Method dump skipped, instructions count: 377
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3I5.A01(android.view.View, X.0l7, X.28D, java.lang.String):android.util.Pair");
    }
}
