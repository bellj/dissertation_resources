package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.List;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.2Mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49842Mz extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;
    public final /* synthetic */ DeviceJid[] A01;

    public C49842Mz(AnonymousClass2L8 r1, DeviceJid[] deviceJidArr) {
        this.A00 = r1;
        this.A01 = deviceJidArr;
    }

    @Override // X.AbstractC35941j2
    public void A00(int i) {
        DeviceJid[] deviceJidArr = this.A01;
        for (DeviceJid deviceJid : deviceJidArr) {
            C450720b r1 = this.A00.A0H;
            Log.i("xmpp/reader/on-get-identity-error");
            AbstractC450820c r4 = r1.A00;
            Bundle bundle = new Bundle();
            bundle.putParcelable("jid", deviceJid);
            bundle.putInt("errorCode", i);
            r4.AYY(Message.obtain(null, 0, MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT, 0, bundle));
        }
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r14) {
        byte[] bArr;
        List<AnonymousClass1V8> A0J = r14.A0F("list").A0J("user");
        AnonymousClass2L8 r7 = this.A00;
        Collections.sort(A0J, new C112195Ck(r7));
        for (AnonymousClass1V8 r2 : A0J) {
            Jid A0B = r2.A0B(r7.A04, DeviceJid.class, "jid");
            AnonymousClass1V8 A0E = r2.A0E("error");
            if (A0E != null) {
                C450720b r22 = r7.A0H;
                int A06 = A0E.A06(A0E.A0H("code"), "code");
                Log.i("xmpp/reader/on-get-identity-error");
                AbstractC450820c r4 = r22.A00;
                Bundle bundle = new Bundle();
                bundle.putParcelable("jid", A0B);
                bundle.putInt("errorCode", A06);
                r4.AYY(Message.obtain(null, 0, MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT, 0, bundle));
            } else {
                AnonymousClass1V8 A0F = r2.A0F("identity");
                AnonymousClass1V8 A0F2 = r2.A0F("type");
                AnonymousClass1V8 A0E2 = r2.A0E("device-identity");
                byte[] bArr2 = A0F2.A01;
                if (bArr2 == null || bArr2.length != 1) {
                    throw new AnonymousClass1V9("type node should contain exactly 1 byte");
                }
                C450720b r23 = r7.A0H;
                byte[] bArr3 = A0F.A01;
                byte b = bArr2[0];
                if (A0E2 != null) {
                    bArr = A0E2.A01;
                } else {
                    bArr = null;
                }
                Log.i("xmpp/reader/on-get-identity-success");
                AbstractC450820c r3 = r23.A00;
                Bundle bundle2 = new Bundle();
                bundle2.putParcelable("jid", A0B);
                bundle2.putByteArray("data", bArr3);
                bundle2.putByte("type", b);
                bundle2.putByteArray("deviceIdentity", bArr);
                r3.AYY(Message.obtain(null, 0, 143, 0, bundle2));
            }
        }
    }
}
