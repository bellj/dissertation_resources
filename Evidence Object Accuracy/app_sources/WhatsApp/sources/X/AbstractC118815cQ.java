package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;
import com.whatsapp.util.Log;

/* renamed from: X.5cQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118815cQ extends AnonymousClass03U {
    public AbstractC118815cQ(View view) {
        super(view);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v13, types: [android.view.View] */
    public void A08(C125965s6 r24) {
        int dimensionPixelSize;
        WaTextView waTextView;
        int i;
        int i2;
        String string;
        int i3;
        int i4;
        int i5;
        WaTextView waTextView2;
        String A0t;
        String A0o;
        String str;
        String str2;
        if (!(this instanceof C122125kx)) {
            if (this instanceof C122135ky) {
                C122135ky r10 = (C122135ky) this;
                C122005kl r3 = (C122005kl) r24;
                View view = r10.A0H;
                Context context = view.getContext();
                int i6 = r3.A00;
                int i7 = R.color.secondary_text;
                switch (i6) {
                    case 1:
                        i5 = R.string.order_details_status_pending;
                        string = context.getString(i5);
                        i3 = R.drawable.ic_schedule_24dp;
                        break;
                    case 2:
                        i5 = R.string.order_details_status_processing;
                        string = context.getString(i5);
                        i3 = R.drawable.ic_schedule_24dp;
                        break;
                    case 3:
                        string = context.getString(R.string.order_details_status_completed);
                        i3 = R.drawable.ic_baseline_done_24;
                        i7 = R.color.order_completed_status_pill_color;
                        break;
                    case 4:
                        i4 = R.string.order_details_status_canceled;
                        string = context.getString(i4);
                        i3 = R.drawable.ic_error_24dp;
                        i7 = R.color.red;
                        break;
                    case 5:
                        i2 = R.string.order_details_status_partially_shipped;
                        string = context.getString(i2);
                        i3 = R.drawable.ic_local_shipping_24dp;
                        break;
                    case 6:
                        i2 = R.string.order_details_status_shipped;
                        string = context.getString(i2);
                        i3 = R.drawable.ic_local_shipping_24dp;
                        break;
                    default:
                        Log.e(C12960it.A0W(i6, "OrderStatusMapper/mapStatus can not map order status "));
                        i4 = R.string.order_details_status_unknown;
                        string = context.getString(i4);
                        i3 = R.drawable.ic_error_24dp;
                        i7 = R.color.red;
                        break;
                }
                C127515uc r4 = new C127515uc(i3, string, i7);
                Context context2 = view.getContext();
                int i8 = r4.A01;
                int i9 = r4.A00;
                Drawable A01 = AnonymousClass2GE.A01(context2, i8, i9);
                String str3 = r4.A02;
                WaTextView waTextView3 = r10.A01;
                CharSequence A012 = C52252aV.A01(waTextView3.getPaint(), A01, str3);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(A012);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(view.getResources().getColor(i9)), 0, A012.length(), 33);
                waTextView3.setText(spannableStringBuilder);
                Resources resources = view.getResources();
                int i10 = R.drawable.order_status_background;
                if (i6 == 3) {
                    i10 = R.drawable.order_status_complete_background;
                }
                waTextView3.setBackground(AnonymousClass00X.A04(null, resources, i10));
                String str4 = r3.A01;
                if (TextUtils.isEmpty(str4)) {
                    waTextView = r10.A00;
                } else {
                    WaTextView waTextView4 = r10.A00;
                    waTextView4.setText(str4);
                    waTextView4.setVisibility(0);
                    return;
                }
            } else if (!(this instanceof C122105kv)) {
                if (this instanceof C122115kw) {
                    String str5 = ((C121995kk) r24).A00;
                    boolean isEmpty = TextUtils.isEmpty(str5);
                    WaTextView waTextView5 = ((C122115kw) this).A00;
                    waTextView = waTextView5;
                    if (!isEmpty) {
                        waTextView5.setText(str5);
                        i = 0;
                        waTextView2 = waTextView5;
                    }
                } else if (!(this instanceof C122095ku)) {
                    if (this instanceof C122145kz) {
                        C122145kz r102 = (C122145kz) this;
                        C122055kq r32 = (C122055kq) r24;
                        for (C1315763h r5 : r32.A05) {
                            if (r5.A06.equals(r32.A03)) {
                                WaTextView waTextView6 = r102.A01;
                                C247116o r0 = r102.A02;
                                String str6 = r5.A05;
                                String str7 = r5.A04;
                                C30081Wa A00 = r0.A00();
                                if (!(A00 == null || (A0t = C12970iu.A0t(str6, A00.A02)) == null)) {
                                    str7 = A0t;
                                }
                                waTextView6.setText(str7);
                            }
                        }
                        r102.A00.setOnClickListener(new C123965oE(r32, r102));
                        return;
                    } else if (this instanceof C122165l1) {
                        C122165l1 r103 = (C122165l1) this;
                        C122065kr r33 = (C122065kr) r24;
                        if (r33.A08) {
                            C16470p4 ABf = r33.A04.ABf();
                            if (ABf == null || ABf.A01 == null) {
                                r103.A02.setVisibility(4);
                            } else {
                                WaImageView waImageView = r103.A02;
                                waImageView.setImageDrawable(r33.A01);
                                waImageView.setVisibility(0);
                            }
                            LinearLayout linearLayout = r103.A01;
                            linearLayout.setVisibility(0);
                            C117295Zj.A0n(linearLayout, r33, 134);
                        } else {
                            r103.A01.setVisibility(8);
                        }
                        String str8 = r33.A05;
                        boolean isEmpty2 = TextUtils.isEmpty(str8);
                        Resources resources2 = r103.A0H.getResources();
                        if (isEmpty2) {
                            A0o = resources2.getString(R.string.order_details_message_biz_default);
                        } else {
                            A0o = C12990iw.A0o(resources2, str8, C12970iu.A1b(), 0, R.string.order_details_message_biz);
                        }
                        r103.A03.setText(A0o);
                        if (r33.A07) {
                            LinearLayout linearLayout2 = r103.A00;
                            linearLayout2.setVisibility(0);
                            C117295Zj.A0n(linearLayout2, r33, 133);
                            return;
                        }
                        r103.A00.setVisibility(8);
                        return;
                    } else if (this instanceof C122155l0) {
                        C122155l0 r104 = (C122155l0) this;
                        C122045kp r34 = (C122045kp) r24;
                        WaImageView waImageView2 = r104.A00;
                        waImageView2.setVisibility(0);
                        C21270x9 r42 = r104.A03;
                        View view2 = r104.A0H;
                        AnonymousClass1J1 A04 = r42.A04(view2.getContext(), "payment-checkout-order-details-view");
                        C15370n3 r02 = r34.A00;
                        AnonymousClass009.A05(r02);
                        A04.A06(waImageView2, r02);
                        String str9 = r34.A01;
                        if (!TextUtils.isEmpty(str9)) {
                            waImageView2.setContentDescription(C12990iw.A0o(view2.getResources(), str9, C12970iu.A1b(), 0, R.string.order_details_profile_logo_description));
                        }
                        boolean z = r34.A03;
                        WaTextView waTextView7 = r104.A01;
                        if (z) {
                            waTextView7.setVisibility(0);
                            waTextView7.setText(str9);
                        } else {
                            waTextView7.setVisibility(8);
                        }
                        r104.A02.setText(r34.A02);
                        return;
                    } else if (!(this instanceof C122085kt) && (this instanceof C122185l3)) {
                        C122185l3 r105 = (C122185l3) this;
                        C122035ko r35 = (C122035ko) r24;
                        AnonymousClass018 r13 = r35.A00;
                        AnonymousClass1ZD r1 = r35.A01;
                        AnonymousClass1ZK r6 = r1.A04;
                        AnonymousClass1ZI r52 = r6.A06;
                        String A03 = r1.A03(r13, r52);
                        String str10 = r35.A02;
                        AnonymousClass1ZI r2 = r6.A04;
                        String A032 = r1.A03(r13, r2);
                        String A033 = r1.A03(r13, r6.A05);
                        String A02 = r1.A02(r13);
                        String str11 = null;
                        if (r52 == null) {
                            str = null;
                        } else {
                            str = r52.A02;
                        }
                        AnonymousClass1ZI r03 = r6.A03;
                        if (r03 == null) {
                            str2 = null;
                        } else {
                            str2 = r03.A02;
                        }
                        if (r2 != null) {
                            str11 = r2.A02;
                        }
                        i = 0;
                        if (!TextUtils.isEmpty(A03) || !TextUtils.isEmpty(str10) || !TextUtils.isEmpty(A032)) {
                            r105.A09(0);
                            r105.A0A(r105.A06, r105.A07, r13, null, A033, R.string.order_details_subtotal_label_text);
                            r105.A0A(r105.A08, r105.A09, r13, str, A03, R.string.order_details_tax_label_text);
                            r105.A0A(r105.A02, r105.A03, r13, str2, str10, R.string.order_details_discount_label_text);
                            r105.A0A(r105.A04, r105.A05, r13, str11, A032, R.string.order_details_shipping_label_text);
                        } else {
                            r105.A09(8);
                        }
                        WaTextView waTextView8 = r105.A0A;
                        waTextView8.setText(A02);
                        boolean z2 = r35.A03;
                        WaTextView waTextView9 = r105.A0B;
                        if (z2) {
                            waTextView9.setVisibility(0);
                            waTextView8.setVisibility(0);
                            waTextView2 = r105.A00;
                        } else {
                            waTextView9.setVisibility(8);
                            waTextView8.setVisibility(8);
                            r105.A00.setVisibility(8);
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
                waTextView2.setVisibility(i);
                return;
            } else {
                return;
            }
            i = 8;
            waTextView2 = waTextView;
            waTextView2.setVisibility(i);
            return;
        }
        C122125kx r106 = (C122125kx) this;
        C122025kn r36 = (C122025kn) r24;
        r106.A00.setRotation((float) r36.A01);
        View view3 = r106.A0H;
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view3);
        int i11 = r36.A02;
        int i12 = 0;
        if (i11 == 0) {
            dimensionPixelSize = 0;
        } else {
            dimensionPixelSize = C12960it.A09(view3).getDimensionPixelSize(i11);
        }
        int i13 = r36.A00;
        if (i13 != 0) {
            i12 = C12960it.A09(view3).getDimensionPixelSize(i13);
        }
        A0H.setMargins(A0H.leftMargin, dimensionPixelSize, A0H.rightMargin, i12);
    }
}
