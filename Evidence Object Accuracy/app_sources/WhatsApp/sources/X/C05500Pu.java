package X;

import android.os.Build;
import android.util.Log;
import androidx.biometric.BiometricFragment;
import java.util.concurrent.Executor;

/* renamed from: X.0Pu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05500Pu {
    public AnonymousClass01F A00;

    public C05500Pu(AnonymousClass0PW r4, ActivityC000900k r5, Executor executor) {
        if (r5 == null) {
            throw new IllegalArgumentException("FragmentActivity must not be null.");
        } else if (executor == null) {
            throw new IllegalArgumentException("Executor must not be null.");
        } else if (r4 != null) {
            AnonymousClass01F r2 = r5.A03.A00.A03;
            AnonymousClass0EP r0 = (AnonymousClass0EP) new AnonymousClass02A(r5).A00(AnonymousClass0EP.class);
            this.A00 = r2;
            if (r0 != null) {
                r0.A0H = executor;
                r0.A04 = r4;
            }
        } else {
            throw new IllegalArgumentException("AuthenticationCallback must not be null.");
        }
    }

    public void A00() {
        String str;
        AnonymousClass01F r1 = this.A00;
        if (r1 == null) {
            str = "Unable to start authentication. Client fragment manager was null.";
        } else {
            BiometricFragment biometricFragment = (BiometricFragment) r1.A0A("androidx.biometric.BiometricFragment");
            if (biometricFragment == null) {
                str = "Unable to cancel authentication. BiometricFragment not found.";
            } else {
                biometricFragment.A1E(3);
                return;
            }
        }
        Log.e("BiometricPromptCompat", str);
    }

    public void A01(AnonymousClass0U4 r4, C05000Nw r5) {
        int A00 = AnonymousClass0QX.A00(r4, r5);
        if ((A00 & 255) == 255) {
            throw new IllegalArgumentException("Crypto-based authentication is not supported for Class 2 (Weak) biometrics.");
        } else if (Build.VERSION.SDK_INT >= 30 || (A00 & 32768) == 0) {
            A02(r4, r5);
        } else {
            throw new IllegalArgumentException("Crypto-based authentication is not supported for device credential prior to API 30.");
        }
    }

    public final void A02(AnonymousClass0U4 r5, C05000Nw r6) {
        String str;
        AnonymousClass01F r3 = this.A00;
        if (r3 == null) {
            str = "Unable to start authentication. Client fragment manager was null.";
        } else if (r3.A0m()) {
            str = "Unable to start authentication. Called after onSaveInstanceState().";
        } else {
            BiometricFragment biometricFragment = (BiometricFragment) r3.A0A("androidx.biometric.BiometricFragment");
            if (biometricFragment == null) {
                biometricFragment = new BiometricFragment();
                C004902f r1 = new C004902f(r3);
                r1.A09(biometricFragment, "androidx.biometric.BiometricFragment");
                r1.A00(true);
                r3.A0k(true);
                r3.A0J();
            }
            biometricFragment.A1I(r5, r6);
            return;
        }
        Log.e("BiometricPromptCompat", str);
    }

    public void A03(C05000Nw r3) {
        if (r3 != null) {
            A02(null, r3);
            return;
        }
        throw new IllegalArgumentException("PromptInfo cannot be null.");
    }
}
