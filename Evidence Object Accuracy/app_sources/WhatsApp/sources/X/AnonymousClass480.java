package X;

import com.whatsapp.quickcontact.QuickContactActivity;

/* renamed from: X.480  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass480 extends C236712o {
    public final /* synthetic */ QuickContactActivity A00;

    public AnonymousClass480(QuickContactActivity quickContactActivity) {
        this.A00 = quickContactActivity;
    }

    @Override // X.C236712o
    public void A01(AnonymousClass1YT r2) {
        AbstractC51412Uo r0 = this.A00.A0Y;
        r0.A00();
        r0.A03();
    }

    @Override // X.C236712o
    public void A02(AnonymousClass1YT r2) {
        AbstractC51412Uo r0 = this.A00.A0Y;
        r0.A00();
        r0.A03();
    }
}
