package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34151ff extends AbstractC28131Kt {
    public final long A00;
    public final UserJid A01;
    public final byte[] A02;

    public C34151ff(UserJid userJid, byte[] bArr, long j) {
        super(null, null);
        this.A01 = userJid;
        this.A02 = bArr;
        this.A00 = j;
    }
}
