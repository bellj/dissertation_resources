package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.3xT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83583xT extends AbstractC52172aN {
    public final /* synthetic */ View.OnClickListener A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83583xT(Context context, View.OnClickListener onClickListener) {
        super(context);
        this.A00 = onClickListener;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        this.A00.onClick(view);
    }
}
