package X;

import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0rp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C18050rp {
    public final C16630pM A00;
    public final AbstractC16710pd A01 = AnonymousClass4Yq.A00(new C459924a(this));

    public C18050rp(C16630pM r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    public static final C460124c A00(String str) {
        C460024b r3;
        JSONObject jSONObject = new JSONObject(str);
        String string = jSONObject.getString("id");
        String string2 = jSONObject.getString("title");
        String string3 = jSONObject.getString("description");
        String string4 = jSONObject.getString("ctaText");
        String string5 = jSONObject.getString("scope");
        int i = jSONObject.getInt("type");
        boolean optBoolean = jSONObject.optBoolean("isCancelable", true);
        String optString = jSONObject.optString("phoenix_flow");
        C16700pc.A0B(optString);
        boolean z = false;
        if (optString.length() > 0) {
            z = true;
        }
        C32631cT r4 = null;
        if (z) {
            new C460024b("");
            String string6 = new JSONObject(optString).getString("config");
            C16700pc.A0B(string6);
            r3 = new C460024b(string6);
        } else {
            r3 = null;
        }
        String optString2 = jSONObject.optString("legacy_payment_step_up_info");
        C16700pc.A0B(optString2);
        if (optString2.length() > 0) {
            r4 = C32631cT.A01(optString2);
        }
        C16700pc.A0B(string);
        C16700pc.A0B(string2);
        C16700pc.A0B(string3);
        C16700pc.A0B(string4);
        C16700pc.A0B(string5);
        return new C460124c(r3, r4, string, string2, string3, string4, string5, i, optBoolean);
    }

    public C460124c A01(String str) {
        C16700pc.A0E(str, 0);
        Object value = this.A01.getValue();
        C16700pc.A0B(value);
        String string = ((SharedPreferences) value).getString("framework_alert_list_info", null);
        if (string != null) {
            JSONObject jSONObject = new JSONObject(string);
            Iterator<String> keys = jSONObject.keys();
            C16700pc.A0B(keys);
            while (keys.hasNext()) {
                String next = keys.next();
                if (C16700pc.A0O(next, str)) {
                    String string2 = jSONObject.getString(next);
                    C16700pc.A0B(string2);
                    return A00(string2);
                }
            }
        }
        return null;
    }

    public List A02() {
        Map A03 = A03();
        ArrayList arrayList = new ArrayList();
        if (!A03.isEmpty()) {
            arrayList.addAll(A03.values());
        }
        return arrayList;
    }

    public final Map A03() {
        Object value = this.A01.getValue();
        C16700pc.A0B(value);
        String string = ((SharedPreferences) value).getString("framework_alert_list_info", null);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (string != null) {
            JSONObject jSONObject = new JSONObject(string);
            Iterator<String> keys = jSONObject.keys();
            C16700pc.A0B(keys);
            while (keys.hasNext()) {
                String next = keys.next();
                C16700pc.A0B(next);
                String string2 = jSONObject.getString(next);
                C16700pc.A0B(string2);
                linkedHashMap.put(next, A00(string2));
            }
        }
        return linkedHashMap;
    }

    public void A04(C460124c r7) {
        C17520qw[] r3 = {new C17520qw(r7.A06, r7)};
        LinkedHashMap linkedHashMap = new LinkedHashMap(2);
        C17520qw r0 = r3[0];
        linkedHashMap.put(r0.first, r0.second);
        Map A03 = A03();
        if (true ^ A03.isEmpty()) {
            linkedHashMap.putAll(A03);
        }
        A06(linkedHashMap);
    }

    public void A05(List list) {
        boolean z;
        C16700pc.A0E(list, 0);
        Map A03 = A03();
        if (!A03.isEmpty()) {
            loop0: while (true) {
                z = false;
                for (Object obj : list) {
                    if (!z) {
                        z = true;
                        if (A03.remove(obj) == null) {
                            break;
                        }
                    }
                }
            }
            if (z) {
                A06(A03);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ec A[Catch: JSONException -> 0x00f3, TRY_LEAVE, TryCatch #0 {JSONException -> 0x00f3, blocks: (B:11:0x0080, B:14:0x00a7, B:15:0x00ae, B:17:0x00c1, B:18:0x00c7, B:20:0x00cf, B:21:0x00d4, B:23:0x00e0, B:24:0x00e5, B:25:0x00e8, B:27:0x00ec), top: B:34:0x0080, inners: #1, #2 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A06(java.util.Map r14) {
        /*
        // Method dump skipped, instructions count: 306
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18050rp.A06(java.util.Map):void");
    }
}
