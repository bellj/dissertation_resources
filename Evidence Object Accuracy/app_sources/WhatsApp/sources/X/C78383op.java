package X;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/* renamed from: X.3op  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78383op extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98724j9();
    public final int A00;
    public final int A01;
    public final Account A02;
    public final GoogleSignInAccount A03;

    public C78383op(Account account, GoogleSignInAccount googleSignInAccount, int i, int i2) {
        this.A00 = i;
        this.A02 = account;
        this.A01 = i2;
        this.A03 = googleSignInAccount;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0B(parcel, this.A02, 2, i, false);
        C95654e8.A07(parcel, 3, this.A01);
        C95654e8.A0B(parcel, this.A03, 4, i, false);
        C95654e8.A06(parcel, A00);
    }
}
