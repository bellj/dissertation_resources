package X;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/* renamed from: X.5D1  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5D1 implements Enumeration {
    public int A00 = 0;
    public final /* synthetic */ AnonymousClass5N6 A01;

    public AnonymousClass5D1(AnonymousClass5N6 r2) {
        this.A01 = r2;
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        int i = this.A00;
        AnonymousClass5N6 r0 = this.A01;
        byte[] bArr = ((AnonymousClass5NH) r0).A00;
        int length = bArr.length;
        if (i < length) {
            int min = Math.min(length - i, r0.A00);
            byte[] bArr2 = new byte[min];
            System.arraycopy(bArr, i, bArr2, 0, min);
            this.A00 += min;
            return new AnonymousClass5N5(bArr2);
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return C12990iw.A1Y(this.A00, ((AnonymousClass5NH) this.A01).A00.length);
    }
}
