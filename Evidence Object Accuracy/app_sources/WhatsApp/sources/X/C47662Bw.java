package X;

/* renamed from: X.2Bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C47662Bw implements AbstractC47642Bu {
    public AnonymousClass1LC A00;

    public C47662Bw(AnonymousClass13T r3) {
        r3.A02.A00(new AbstractC30611Yc() { // from class: X.2C8
            @Override // X.AbstractC30611Yc
            public final void APz(Object obj) {
                AnonymousClass1LC r0 = C47662Bw.this.A00;
                if (r0 != null) {
                    r0.A00();
                }
            }
        });
    }

    @Override // X.AbstractC47642Bu
    public void AcI(AnonymousClass1LC r1) {
        this.A00 = r1;
    }
}
