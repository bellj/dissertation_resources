package X;

import org.json.JSONObject;

/* renamed from: X.58U  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58U implements AnonymousClass5W9 {
    @Override // X.AnonymousClass5W9
    public AnonymousClass5UW A8V(JSONObject jSONObject) {
        return new AnonymousClass58K(C72453ed.A0u(jSONObject), jSONObject.getInt("numericalGreaterThanEqual"));
    }

    @Override // X.AnonymousClass5W9
    public String ADP() {
        return "numericalGreaterThanEqual";
    }
}
