package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.3E8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3E8 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass4XG A01;
    public final AbstractC14440lR A02;

    public AnonymousClass3E8(C26211Cl r3, AbstractC14440lR r4) {
        this.A02 = r4;
        this.A01 = new AnonymousClass4XG(r3);
        C12990iw.A1O(this.A02, this, 41);
    }

    public void A00(C48142Em r7) {
        AnonymousClass4XG r5 = this.A01;
        List A00 = r5.A00();
        boolean z = false;
        for (int i = 0; i < A00.size(); i++) {
            if (A00.get(i).equals(r7)) {
                A00.set(i, r7);
                z = true;
            }
        }
        if (!z) {
            A00.add(r7);
        }
        Collections.sort(A00, r5.A02);
        r5.A01.A01(A00.subList(0, Math.min(r5.A00, A00.size())));
        this.A00.A0B(r5.A00());
    }
}
