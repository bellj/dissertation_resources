package X;

/* renamed from: X.600  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass600 {
    public static volatile String A03 = C12990iw.A0n();
    public final C14830m7 A00;
    public final C130125yq A01;
    public final AnonymousClass61F A02;

    public AnonymousClass600(C14830m7 r1, C130125yq r2, AnonymousClass61F r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:14|(2:16|(3:18|(3:20|(2:22|(2:24|(1:26)(2:51|52)))|29)(2:30|(2:32|33))|27)(1:34))(2:36|(2:38|39))|40|53|41|43|(1:45)(1:49)|46|(1:48)|29) */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0077, code lost:
        if (r1.A0H() != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ab, code lost:
        com.whatsapp.util.Log.e("PAY: IntentPayloadHelper/getDataFetchProofIntentPayload/toJson can't construct json");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A00(java.lang.String r9, java.lang.String r10, int r11, boolean r12) {
        /*
        // Method dump skipped, instructions count: 243
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass600.A00(java.lang.String, java.lang.String, int, boolean):java.util.List");
    }
}
