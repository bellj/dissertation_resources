package X;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;
import com.whatsapp.dialogs.AudioVideoBottomSheetDialogFragment;
import com.whatsapp.dialogs.CreateOrAddToContactsDialog;
import com.whatsapp.jid.UserJid;
import java.util.Collections;

/* renamed from: X.2xY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xY extends AbstractC36781kZ {
    public final AnonymousClass12P A00;
    public final AnonymousClass18U A01;
    public final C15570nT A02;
    public final C254719n A03;
    public final C14650lo A04;
    public final AnonymousClass19Q A05;
    public final C238013b A06;
    public final C22700zV A07;
    public final C15610nY A08;
    public final C17720rH A09;
    public final C245716a A0A;
    public final C14850m9 A0B;
    public final C22050yP A0C;
    public final C16120oU A0D;
    public final AnonymousClass1AQ A0E;
    public final C255819y A0F;
    public final AnonymousClass18X A0G;

    public AnonymousClass2xY(ActivityC000800j r34, AnonymousClass12P r35, AbstractC13860kS r36, C14900mE r37, AnonymousClass18U r38, C15570nT r39, C15450nH r40, C16170oZ r41, C18330sH r42, AnonymousClass2TT r43, C254719n r44, C14650lo r45, AnonymousClass19Q r46, C238013b r47, C22330yu r48, C243915i r49, AnonymousClass10S r50, C22700zV r51, C15610nY r52, AbstractC13920kY r53, C255319t r54, AnonymousClass1A6 r55, C17050qB r56, C14820m6 r57, AnonymousClass018 r58, C14950mJ r59, C17720rH r60, C19990v2 r61, C20830wO r62, C15370n3 r63, C22100yW r64, C245716a r65, C14850m9 r66, C22050yP r67, C16120oU r68, C244215l r69, AbstractC14640lm r70, C15860o1 r71, AnonymousClass1AQ r72, AnonymousClass12F r73, AnonymousClass12U r74, C255819y r75, C255719x r76, AbstractC14440lR r77, C21280xA r78, AnonymousClass18X r79) {
        super(r34, r36, r37, r39, r40, r41, r42, r43, r48, r49, r50, r51, r53, r54, r55, r56, r57, r58, r59, r61, r62, r63, r64, r69, r70, r71, r73, r74, r76, r77, r78);
        this.A0B = r66;
        this.A02 = r39;
        this.A0D = r68;
        this.A01 = r38;
        this.A00 = r35;
        this.A08 = r52;
        this.A0G = r79;
        this.A06 = r47;
        this.A0C = r67;
        this.A07 = r51;
        this.A0A = r65;
        this.A03 = r44;
        this.A04 = r45;
        this.A05 = r46;
        this.A0F = r75;
        this.A09 = r60;
        this.A0E = r72;
    }

    public final boolean A06() {
        if (this.A0B.A07(1309) && super.A00.A0B(UserJid.class) != null) {
            if (!this.A07.A02((UserJid) C15370n3.A03(super.A00, UserJid.class))) {
                return true;
            }
        }
        return false;
    }

    public final boolean A07() {
        AbstractC14670lq r0 = ((Conversation) super.A0F).A3y;
        if (r0 == null || !r0.A0V()) {
            AbstractC14640lm r2 = this.A0R;
            C15570nT r1 = this.A02;
            if (r2 != null && !C21280xA.A01() && !r1.A0F(r2)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    public boolean ATH(MenuItem menuItem) {
        ActivityC000800j r5;
        Intent intent;
        int itemId = menuItem.getItemId();
        if (itemId != 11) {
            switch (itemId) {
                case 21:
                    ActivityC000800j r4 = super.A01;
                    r4.startActivity(C14960mK.A0R(r4, C15370n3.A04(super.A00), C12970iu.A0h(), false), AbstractC454421p.A05(r4, r4.findViewById(R.id.transition_start), super.A08.A00(R.string.transition_photo)));
                    break;
                case 22:
                    Conversation conversation = (Conversation) super.A0F;
                    C12960it.A16(CreateOrAddToContactsDialog.A00(conversation.A2c), conversation);
                    return true;
                case 23:
                    UserJid of = UserJid.of(this.A0R);
                    AnonymousClass009.A05(of);
                    if (super.A00.A0J()) {
                        r5 = super.A01;
                        intent = C14960mK.A0S(r5, of, "chat", false, false, false);
                        break;
                    } else {
                        super.A02.Adm(BlockConfirmationDialogFragment.A00(of, "overflow_menu_block", false, false, true));
                        return true;
                    }
                case 24:
                    this.A06.A0B(super.A01, super.A00, false);
                    return true;
                case 25:
                    this.A09.A01(UserJid.of(this.A0R), 8);
                    this.A0F.A00(6);
                    super.A0F.AZU(super.A00, false);
                    return true;
                case 26:
                    this.A09.A01(UserJid.of(this.A0R), 8);
                    this.A0F.A00(7);
                    super.A0F.AZU(super.A00, true);
                    return true;
                case 27:
                case 31:
                    break;
                case 28:
                    this.A09.A01(UserJid.of(this.A0R), 8);
                    this.A0F.A00(9);
                    Bundle A0D = C12970iu.A0D();
                    AudioVideoBottomSheetDialogFragment audioVideoBottomSheetDialogFragment = new AudioVideoBottomSheetDialogFragment();
                    audioVideoBottomSheetDialogFragment.A0U(A0D);
                    super.A02.Adm(audioVideoBottomSheetDialogFragment);
                    return true;
                case 29:
                    C17720rH r3 = this.A09;
                    AbstractC14640lm r52 = this.A0R;
                    r3.A01(UserJid.of(r52), 9);
                    UserJid userJid = (UserJid) r52;
                    AnonymousClass19Q r1 = this.A05;
                    r1.A00(7);
                    r1.A03(userJid, null, null, 26);
                    AnonymousClass12P r32 = this.A00;
                    ActivityC000800j r12 = super.A01;
                    r32.A06(r12, C14960mK.A0M(r12, userJid, null, 12));
                    return true;
                case C25991Bp.A0S:
                    String str = (String) menuItem.getActionView().getTag(R.id.tag_shop_url);
                    if (!TextUtils.isEmpty(str)) {
                        this.A01.Ab9(super.A01, Uri.parse(str));
                        if (this.A0G.AKA()) {
                            AnonymousClass30V r13 = new AnonymousClass30V();
                            r13.A01 = C12970iu.A0g();
                            r13.A00 = 1;
                            this.A0D.A07(r13);
                            return true;
                        }
                    }
                    break;
                default:
                    return super.ATH(menuItem);
            }
            return true;
        }
        this.A0C.A03(5);
        r5 = super.A01;
        intent = C14960mK.A0b(r5.getApplicationContext(), Collections.singletonList(super.A00.A0B(UserJid.class).getRawString()), 5);
        r5.startActivity(intent);
        return true;
    }

    @Override // X.AbstractC36781kZ, X.AbstractC36791ka
    public boolean AUA(Menu menu) {
        StringBuilder A0k = C12960it.A0k("contactconversationmenu/onprepareoptionsmenu ");
        A0k.append(menu.size());
        C12960it.A1F(A0k);
        boolean z = false;
        if (menu.size() == 0) {
            return false;
        }
        A04(menu.findItem(4));
        menu.findItem(21).setVisible(C12960it.A1W(super.A00.A0C));
        MenuItem findItem = menu.findItem(22);
        if (super.A00.A0C == null) {
            this.A02.A08();
            C14850m9 r1 = this.A0B;
            C15370n3 r0 = super.A00;
            if (r0 == null || !C41861uH.A00(r1, r0.A0D)) {
                z = true;
            }
        }
        findItem.setVisible(z);
        boolean A0I = this.A06.A0I(UserJid.of(this.A0R));
        menu.findItem(23).setVisible(!A0I);
        menu.findItem(24).setVisible(A0I);
        return super.AUA(menu);
    }
}
