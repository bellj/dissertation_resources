package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.5lj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122605lj extends AbstractC118835cS {
    public final ImageView A00;
    public final LinearLayout A01;

    public C122605lj(View view) {
        super(view);
        this.A01 = C117305Zk.A07(view, R.id.payment_order_details_container);
        this.A00 = C12970iu.A0K(view, R.id.payment_order_details_icon);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r4, int i) {
        this.A01.setOnClickListener(((C123085ma) r4).A00);
        ImageView imageView = this.A00;
        AnonymousClass2GE.A05(imageView.getContext(), imageView, R.color.settings_icon);
    }
}
