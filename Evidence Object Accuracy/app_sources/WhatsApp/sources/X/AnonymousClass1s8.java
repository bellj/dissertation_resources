package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.whatsapp.CircleWaImageView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import com.whatsapp.camera.bottomsheet.CameraMediaPickerFragment;
import com.whatsapp.camera.overlays.AutofocusOverlay;
import com.whatsapp.camera.recording.RecordingView;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.1s8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1s8 {
    public int A00;
    public long A01;
    public SoundPool A02;
    public View A03;
    public View A04;
    public View A05;
    public View A06;
    public View A07;
    public ActivityC13810kN A08;
    public AnonymousClass2UA A09;
    public AnonymousClass1s9 A0A;
    public C51342Tz A0B;
    public C49632Lo A0C;
    public AnonymousClass2U3 A0D;
    public AnonymousClass2P8 A0E;
    public AnonymousClass1sA A0F;
    public AbstractC35581iK A0G;
    public C453421e A0H = new C453421e();
    public C15580nU A0I;
    public AnonymousClass2JY A0J;
    public File A0K;
    public File A0L;
    public String A0M;
    public List A0N = Collections.emptyList();
    public List A0O;
    public boolean A0P;
    public boolean A0Q;
    public boolean A0R;
    public boolean A0S;
    public boolean A0T;
    public boolean A0U = true;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public final long A0Y;
    public final AnonymousClass01E A0Z;
    public final C14330lG A0a;
    public final C14900mE A0b;
    public final C15450nH A0c;
    public final C18720su A0d;
    public final AnonymousClass1AX A0e;
    public final AnonymousClass01d A0f;
    public final C14830m7 A0g;
    public final C16590pI A0h;
    public final C15890o4 A0i;
    public final C14820m6 A0j;
    public final AnonymousClass018 A0k;
    public final C14850m9 A0l;
    public final AnonymousClass2FO A0m;
    public final C16630pM A0n;
    public final C15690nk A0o;
    public final AbstractC14440lR A0p;
    public final C236712o A0q = new C40701s7(this);
    public final C21260x8 A0r;
    public final C21280xA A0s;
    public final Runnable A0t = new RunnableBRunnable0Shape15S0100000_I1_1(this, 2);
    public final List A0u = new ArrayList();
    public final Set A0v = new LinkedHashSet();
    public final boolean A0w;
    public final /* synthetic */ AbstractC49682Lt A0x;
    public final /* synthetic */ AnonymousClass2GJ A0y;

    public static String A00(int i) {
        switch (i) {
            case 1:
                return "camera_tab";
            case 2:
                return "chat_button";
            case 3:
                return "chat_menu";
            case 4:
                return "status_tab";
            case 5:
                return "status_reply";
            case 6:
                return "smb_quick_reply_edit";
            case 7:
                return "smb_quick_reply_send";
            default:
                return "unknown";
        }
    }

    public AnonymousClass1s8(AnonymousClass01E r3, C14330lG r4, C14900mE r5, C15450nH r6, C18720su r7, AbstractC49682Lt r8, AnonymousClass1AX r9, AnonymousClass2GJ r10, AnonymousClass01d r11, C14830m7 r12, C16590pI r13, C15890o4 r14, C14820m6 r15, AnonymousClass018 r16, C14850m9 r17, AnonymousClass2FO r18, C16630pM r19, AnonymousClass2JY r20, C15690nk r21, AbstractC14440lR r22, C21260x8 r23, C21280xA r24) {
        this.A0y = r10;
        this.A0x = r8;
        this.A0h = r13;
        this.A0g = r12;
        this.A0d = r7;
        this.A0l = r17;
        this.A0b = r5;
        this.A0p = r22;
        this.A0a = r4;
        this.A0c = r6;
        this.A0m = r18;
        this.A0r = r23;
        this.A0s = r24;
        this.A0f = r11;
        this.A0k = r16;
        this.A0Z = r3;
        this.A0i = r14;
        this.A0j = r15;
        this.A0o = r21;
        this.A0e = r9;
        this.A0n = r19;
        this.A0J = r20;
        this.A0w = r17.A07(1857);
        this.A0Y = System.currentTimeMillis();
    }

    public final int A01() {
        int i = 0;
        if (!this.A0l.A07(125) && this.A0B != null && Settings.System.getInt(this.A0h.A00.getContentResolver(), "accelerometer_rotation", 0) == 0 && this.A0B.A00 != -1) {
            i = ((this.A0B.A00 - ((4 - A02().getWindowManager().getDefaultDisplay().getRotation()) % 4)) * 90) % 360;
            while (i < 0) {
                i += 360;
            }
        }
        return i;
    }

    public final ActivityC13810kN A02() {
        ActivityC13810kN r0 = this.A08;
        if (r0 != null) {
            return r0;
        }
        throw new NullPointerException("Host activity is NULL");
    }

    public void A03() {
        AnonymousClass1AX r4 = this.A0e;
        r4.A01 = 0;
        if (r4.A0A) {
            r4.A08.AKw(554251647, "error_message", "permission");
            r4.A05(4);
        }
    }

    public void A04() {
        if (this.A08 != null) {
            AnonymousClass1sA r3 = this.A0F;
            Handler handler = r3.A01;
            handler.removeMessages(0);
            handler.removeMessages(1);
            RecordingView recordingView = r3.A04;
            recordingView.setVisibility(8);
            recordingView.A01.setProgress(0);
            this.A0r.A04(this.A0q);
            C49632Lo r32 = this.A0C;
            if (r32 != null) {
                AbstractC16350or r1 = r32.A05;
                if (r1 != null) {
                    r1.A03(true);
                    r32.A05 = null;
                }
                C457522x r0 = r32.A04;
                if (r0 != null) {
                    r0.A00();
                    r32.A04 = null;
                }
                AnonymousClass2UD r12 = r32.A03;
                if (r12 != null) {
                    AbstractC35581iK r02 = r12.A00;
                    if (r02 != null) {
                        r02.close();
                        r12.A00 = null;
                    }
                    r32.A03 = null;
                }
            }
            this.A08 = null;
        }
    }

    public void A05() {
        if (this.A08 != null) {
            if (this.A0A.AJy()) {
                boolean z = false;
                if (System.currentTimeMillis() - this.A0F.A00 > 1000) {
                    z = true;
                }
                A0O(z);
            }
            if (this.A06.getVisibility() == 0) {
                this.A06.setVisibility(8);
                this.A0D.A0B.setEnabled(false);
            }
            this.A0A.pause();
            C51342Tz r0 = this.A0B;
            if (r0 != null) {
                r0.disable();
            }
        }
    }

    public void A06() {
        if (this.A08 != null && this.A0Q) {
            C49632Lo r0 = this.A0C;
            if (r0 == null || r0.A0A.A0B != 3) {
                A02().findViewById(R.id.root_view).setSystemUiVisibility(4);
            }
            this.A0A.Aar();
            if (this.A06.getVisibility() == 8) {
                this.A06.setVisibility(0);
            }
            C51342Tz r02 = this.A0B;
            if (r02 != null) {
                r02.enable();
            }
        }
    }

    public void A07() {
        C49632Lo r6 = this.A0C;
        ActivityC13810kN A02 = A02();
        AnonymousClass2FO r4 = this.A0m;
        AnonymousClass2UI r3 = new AnonymousClass2UI(this);
        AbstractC14440lR r2 = this.A0p;
        AbstractC16350or r1 = r6.A05;
        if (r1 != null) {
            r1.A03(true);
        }
        AnonymousClass2UJ r12 = new AnonymousClass2UJ(A02, r3, r4);
        r6.A05 = r12;
        r2.Aaz(r12, new Void[0]);
    }

    public void A08() {
        View view;
        AnonymousClass1AX r3 = this.A0e;
        AbstractC49682Lt r5 = this.A0x;
        r3.A04("warm", A00(r5.AEl()));
        r3.A02(554251647, "showCamera");
        ActivityC13810kN r2 = this.A08;
        if (r2 != null) {
            C41691tw.A04(r2, R.color.transparent, 2);
            if (Build.MANUFACTURER.equals("OnePlus")) {
                String str = Build.MODEL;
                if ((str.equals("ONEPLUS A3000") || str.equals("ONEPLUS A3003") || str.equals("ONEPLUS A3010")) && this.A0s.A03()) {
                    this.A0b.A07(R.string.error_camera_disabled_during_video_call, 1);
                    r5.ANa();
                    r3.A01(554251647, "showCamera");
                    r3.A05(4);
                    return;
                }
            }
            this.A0Q = true;
            r3.A02 = SystemClock.elapsedRealtime();
            this.A0A.Aar();
            this.A06.removeCallbacks(this.A0t);
            int i = 0;
            this.A04.setVisibility(0);
            if (this.A06.getVisibility() == 0) {
                view = this.A05;
                i = 8;
            } else {
                view = this.A06;
            }
            view.setVisibility(i);
            A0N(true);
            this.A0Z.A0n(true);
            r3.A01(554251647, "showCamera");
            return;
        }
        r3.A01(554251647, "showCamera");
        r3.A05(87);
        throw new IllegalStateException("need to call onCreate first");
    }

    public final void A09() {
        ActivityC13810kN r9 = this.A08;
        if (r9 != null) {
            AnonymousClass01d r8 = this.A0f;
            AnonymousClass018 r7 = this.A0k;
            Set set = this.A0v;
            AnonymousClass23N.A00(r9, r8, r7.A0I(new Object[]{Integer.valueOf(set.size())}, R.plurals.n_items_selected, (long) set.size()));
        }
    }

    public final void A0A() {
        String str;
        if (!A0Q()) {
            if (this.A0E.A02.getVisibility() == 0) {
                AutofocusOverlay autofocusOverlay = this.A0E.A02;
                autofocusOverlay.A03 = false;
                autofocusOverlay.invalidate();
                autofocusOverlay.postDelayed(autofocusOverlay.A06, 1000);
            }
            if (!this.A0V) {
                this.A0V = true;
                AnonymousClass1AX r2 = this.A0e;
                boolean AJS = this.A0A.AJS();
                r2.A03 = SystemClock.elapsedRealtime();
                if (r2.A0A) {
                    AbstractC21180x0 r3 = r2.A08;
                    r3.ALE(554250848);
                    if (AJS) {
                        str = "front";
                    } else {
                        str = "back";
                    }
                    r3.AKw(554250848, "origin", str);
                }
                this.A0A.ALf();
                AnonymousClass2U3 r6 = this.A0D;
                boolean AJS2 = this.A0A.AJS();
                float f = 180.0f;
                if (AJS2) {
                    f = -180.0f;
                }
                WaImageView waImageView = r6.A0C;
                AnonymousClass2UN r22 = new AnonymousClass2UN(f, (float) (waImageView.getWidth() >> 1), (float) (waImageView.getHeight() >> 1), (float) (-waImageView.getWidth()));
                r22.setDuration(360);
                r22.setInterpolator(new LinearInterpolator());
                r22.setAnimationListener(new AnonymousClass2UO(r6, AJS2));
                waImageView.startAnimation(r22);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0091, code lost:
        if (r2 == 1) goto L_0x0093;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00dc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0B() {
        /*
        // Method dump skipped, instructions count: 263
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1s8.A0B():void");
    }

    public final void A0C() {
        SoundPool soundPool;
        Log.i("cameraui/takepicture");
        if (this.A0X && (soundPool = this.A02) != null) {
            soundPool.play(this.A00, 1.0f, 1.0f, 0, 0, 1.0f);
        }
        Set set = this.A0v;
        if (set.size() >= 30) {
            this.A0b.A0E(A02().getString(R.string.share_too_many_items_with_placeholder, 30), 0);
            return;
        }
        AnonymousClass1AX r2 = this.A0e;
        r2.A00 = SystemClock.elapsedRealtime();
        if (r2.A0A) {
            r2.A08.ALE(554240366);
        }
        AnonymousClass2U3 r22 = this.A0D;
        r22.A0B.setEnabled(false);
        r22.A0C.setEnabled(false);
        r22.A0A.setEnabled(false);
        if (set.isEmpty()) {
            this.A0C.A02();
            this.A0D.A00();
        }
        if (this.A0A.ALa()) {
            if (this.A08 != null) {
                AnonymousClass2U3 r1 = this.A0D;
                Window window = A02().getWindow();
                WindowManager.LayoutParams attributes = window.getAttributes();
                View view = r1.A04;
                view.setVisibility(0);
                view.setBackgroundColor(-3886);
                attributes.screenBrightness = 1.0f;
                window.setAttributes(attributes);
            }
            this.A06.postDelayed(new RunnableBRunnable0Shape15S0100000_I1_1(this, 1), 300);
            return;
        }
        boolean z = false;
        if (set.size() > 0) {
            z = true;
        }
        this.A0A.Aei(new AnonymousClass2AO(this), z);
    }

    public void A0D(int i, int i2, Intent intent) {
        if (!(i == 90 || i == 101)) {
            return;
        }
        if (i2 == -1) {
            AbstractC49682Lt r6 = this.A0x;
            r6.AEl();
            List<AnonymousClass2U0> list = this.A0u;
            for (AnonymousClass2U0 r7 : list) {
                Set set = this.A0v;
                if (set.isEmpty() || set.contains(r7.AAE())) {
                    C22200yh.A0P(A02(), r7.AAE());
                } else {
                    File ACy = r7.ACy();
                    if (!ACy.delete()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("cameraui/cannot-delete-file ");
                        sb.append(ACy);
                        Log.w(sb.toString());
                    }
                }
            }
            this.A0v.clear();
            list.clear();
            C49632Lo r0 = this.A0C;
            if (r0 != null) {
                r0.A00();
            }
            if (!this.A0T) {
                List A07 = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
                if (A07.size() == 1 && !A07.equals(this.A0N)) {
                    Context context = this.A0h.A00;
                    Intent A0i = new C14960mK().A0i(context, (AbstractC14640lm) A07.get(0));
                    C35741ib.A00(A0i, "CameraUi");
                    context.startActivity(A0i);
                }
            }
            r6.AVn();
        } else if (i2 == 1) {
            this.A0H.A01(intent.getExtras());
            if (this.A0T) {
                this.A0N = C15380n4.A07(AbstractC14640lm.class, intent.getStringArrayListExtra("jids"));
            }
            A0L(intent.getParcelableArrayListExtra("android.intent.extra.STREAM"));
        } else if (i2 == 0) {
            if (this.A0v.isEmpty()) {
                List<AnonymousClass2U0> list2 = this.A0u;
                if (!list2.isEmpty()) {
                    for (AnonymousClass2U0 r02 : list2) {
                        File ACy2 = r02.ACy();
                        if (!ACy2.delete()) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("cameraui/cannot-delete-file ");
                            sb2.append(ACy2);
                            Log.w(sb2.toString());
                        }
                    }
                    list2.clear();
                    C49632Lo r03 = this.A0C;
                    if (r03 != null) {
                        r03.A00();
                    }
                }
            }
            A0M(true);
        }
    }

    public void A0E(long j) {
        C49632Lo r0;
        ActivityC13810kN r1 = this.A08;
        if (r1 != null) {
            boolean z = true;
            C41691tw.A04(r1, R.color.lightNavigationBarBackgroundColor, 1);
            AnonymousClass2JY r12 = this.A0J;
            C48192Eu r02 = r12.A00;
            if (r02 != null) {
                r02.A00();
                r12.A00 = null;
            }
            this.A0A.A7E();
            this.A0Q = false;
            this.A0C.A0A.A0M(4);
            View view = this.A06;
            Runnable runnable = this.A0t;
            view.removeCallbacks(runnable);
            if (this.A06.getVisibility() != 4) {
                int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
                View view2 = this.A06;
                if (i == 0) {
                    view2.setVisibility(4);
                } else {
                    view2.postDelayed(runnable, j);
                }
            }
            this.A05.setVisibility(0);
            this.A0A.pause();
            Set set = this.A0v;
            if (set.isEmpty() && this.A0u.isEmpty()) {
                z = false;
            }
            this.A0H = new C453421e();
            set.clear();
            List<AnonymousClass2U0> list = this.A0u;
            if (!list.isEmpty()) {
                for (AnonymousClass2U0 r03 : list) {
                    File ACy = r03.ACy();
                    if (!ACy.delete()) {
                        StringBuilder sb = new StringBuilder("cameraui/cannot-delete-file ");
                        sb.append(ACy);
                        Log.w(sb.toString());
                    }
                }
                list.clear();
            }
            if (z && (r0 = this.A0C) != null) {
                r0.A00();
            }
            this.A0Z.A0n(false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b8, code lost:
        if (r2 == 5) goto L_0x00ba;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(android.graphics.Bitmap r14, X.AnonymousClass01E r15, X.AbstractC35611iN r16, java.util.Collection r17, java.util.List r18) {
        /*
        // Method dump skipped, instructions count: 414
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1s8.A0F(android.graphics.Bitmap, X.01E, X.1iN, java.util.Collection, java.util.List):void");
    }

    public void A0G(Bundle bundle) {
        Object r1;
        Set set = this.A0v;
        set.clear();
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("multi_selected");
        if (parcelableArrayList != null) {
            set.addAll(parcelableArrayList);
        }
        this.A0H.A01(bundle);
        List list = this.A0u;
        list.clear();
        ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("captured_media");
        if (parcelableArrayList2 != null && !parcelableArrayList2.isEmpty()) {
            ContentResolver contentResolver = this.A0h.A00.getContentResolver();
            ArrayList arrayList = new ArrayList(parcelableArrayList2.size());
            Iterator it = parcelableArrayList2.iterator();
            while (it.hasNext()) {
                AnonymousClass2UQ r12 = (AnonymousClass2UQ) it.next();
                byte b = r12.A00;
                if (b == 1) {
                    r1 = new AnonymousClass2UR(contentResolver, r12.A02, r12.A01, r12.A03);
                } else if (b == 3) {
                    r1 = new AnonymousClass2US(r12.A02);
                } else {
                    StringBuilder sb = new StringBuilder("Unsupported media type: ");
                    sb.append((int) b);
                    throw new AssertionError(sb.toString());
                }
                arrayList.add(r1);
            }
            list.addAll(arrayList);
        }
        this.A0R = !list.isEmpty();
        C49632Lo r0 = this.A0C;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass2U3 r2 = this.A0D;
        if (r2 != null) {
            r2.A03(!set.isEmpty(), set.size());
        }
        C49632Lo r02 = this.A0C;
        if (r02 != null) {
            r02.A01();
            boolean z = false;
            if (this.A0C.A0A.A0B == 3) {
                z = true;
            }
            View view = this.A03;
            if (z) {
                view.setVisibility(4);
                AnonymousClass2P8 r22 = this.A0E;
                r22.A00.setBackgroundColor(A02().getResources().getColor(R.color.wds_black));
                return;
            }
            view.setVisibility(0);
        }
    }

    public void A0H(Bundle bundle) {
        bundle.putParcelableArrayList("multi_selected", new ArrayList<>(this.A0v));
        C453421e r0 = this.A0H;
        Bundle bundle2 = new Bundle();
        r0.A02(bundle2);
        bundle.putBundle("media_preview_params", bundle2);
        List<AnonymousClass2U0> list = this.A0u;
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>(list.size());
        for (AnonymousClass2U0 r1 : list) {
            arrayList.add(new AnonymousClass2UQ(r1));
        }
        bundle.putParcelableArrayList("captured_media", arrayList);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x014b A[Catch: all -> 0x0325, TryCatch #0 {all -> 0x0325, blocks: (B:8:0x0022, B:10:0x0045, B:11:0x0049, B:13:0x009f, B:15:0x00ad, B:16:0x00b2, B:18:0x014b, B:20:0x014f, B:21:0x0158, B:23:0x015c, B:24:0x0165, B:26:0x01d5, B:27:0x01da, B:28:0x01e3, B:29:0x01e7, B:31:0x01f6, B:32:0x0211, B:34:0x027a, B:36:0x027e, B:41:0x028a, B:42:0x029c, B:44:0x02bc, B:45:0x02c0, B:47:0x0303, B:52:0x030f), top: B:58:0x0022 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x01d5 A[Catch: all -> 0x0325, TryCatch #0 {all -> 0x0325, blocks: (B:8:0x0022, B:10:0x0045, B:11:0x0049, B:13:0x009f, B:15:0x00ad, B:16:0x00b2, B:18:0x014b, B:20:0x014f, B:21:0x0158, B:23:0x015c, B:24:0x0165, B:26:0x01d5, B:27:0x01da, B:28:0x01e3, B:29:0x01e7, B:31:0x01f6, B:32:0x0211, B:34:0x027a, B:36:0x027e, B:41:0x028a, B:42:0x029c, B:44:0x02bc, B:45:0x02c0, B:47:0x0303, B:52:0x030f), top: B:58:0x0022 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x01e3 A[Catch: all -> 0x0325, TryCatch #0 {all -> 0x0325, blocks: (B:8:0x0022, B:10:0x0045, B:11:0x0049, B:13:0x009f, B:15:0x00ad, B:16:0x00b2, B:18:0x014b, B:20:0x014f, B:21:0x0158, B:23:0x015c, B:24:0x0165, B:26:0x01d5, B:27:0x01da, B:28:0x01e3, B:29:0x01e7, B:31:0x01f6, B:32:0x0211, B:34:0x027a, B:36:0x027e, B:41:0x028a, B:42:0x029c, B:44:0x02bc, B:45:0x02c0, B:47:0x0303, B:52:0x030f), top: B:58:0x0022 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x01f6 A[Catch: all -> 0x0325, TryCatch #0 {all -> 0x0325, blocks: (B:8:0x0022, B:10:0x0045, B:11:0x0049, B:13:0x009f, B:15:0x00ad, B:16:0x00b2, B:18:0x014b, B:20:0x014f, B:21:0x0158, B:23:0x015c, B:24:0x0165, B:26:0x01d5, B:27:0x01da, B:28:0x01e3, B:29:0x01e7, B:31:0x01f6, B:32:0x0211, B:34:0x027a, B:36:0x027e, B:41:0x028a, B:42:0x029c, B:44:0x02bc, B:45:0x02c0, B:47:0x0303, B:52:0x030f), top: B:58:0x0022 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0289  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x02bc A[Catch: all -> 0x0325, TryCatch #0 {all -> 0x0325, blocks: (B:8:0x0022, B:10:0x0045, B:11:0x0049, B:13:0x009f, B:15:0x00ad, B:16:0x00b2, B:18:0x014b, B:20:0x014f, B:21:0x0158, B:23:0x015c, B:24:0x0165, B:26:0x01d5, B:27:0x01da, B:28:0x01e3, B:29:0x01e7, B:31:0x01f6, B:32:0x0211, B:34:0x027a, B:36:0x027e, B:41:0x028a, B:42:0x029c, B:44:0x02bc, B:45:0x02c0, B:47:0x0303, B:52:0x030f), top: B:58:0x0022 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0I(android.view.ViewGroup r19, X.ActivityC13810kN r20, X.C453421e r21, X.C15580nU r22, java.lang.String r23, java.util.ArrayList r24, java.util.List r25, java.util.List r26, long r27, boolean r29, boolean r30, boolean r31, boolean r32) {
        /*
        // Method dump skipped, instructions count: 817
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1s8.A0I(android.view.ViewGroup, X.0kN, X.21e, X.0nU, java.lang.String, java.util.ArrayList, java.util.List, java.util.List, long, boolean, boolean, boolean, boolean):void");
    }

    public final void A0J(AbstractC35611iN r9) {
        if (r9 != null) {
            Uri AAE = r9.AAE();
            Set set = this.A0v;
            if (set.contains(AAE)) {
                set.remove(AAE);
                this.A0H.A00.remove(AAE);
            } else if (set.size() < 30) {
                set.add(AAE);
                this.A0H.A03(new C39341ph(AAE));
            } else {
                this.A0b.A0E(A02().getString(R.string.share_too_many_items_with_placeholder, 30), 0);
            }
            if (!set.isEmpty()) {
                A09();
            }
            this.A0D.A03(!set.isEmpty(), set.size());
            C49632Lo r0 = this.A0C;
            if (r0 != null) {
                r0.A00();
            }
        }
    }

    public final void A0K(AbstractC35611iN r12, AnonymousClass2T3 r13, boolean z) {
        Bitmap bitmap;
        AbstractC35611iN r8;
        if (r12 == null) {
            Log.i("cameraui/showpreview/media-is-null");
            return;
        }
        Uri AAE = r12.AAE();
        StringBuilder sb = new StringBuilder("cameraui/showpreview ");
        sb.append(AAE);
        Log.i(sb.toString());
        ActivityC13810kN r0 = this.A08;
        if (r0 != null && !r0.AJN()) {
            if (z) {
                this.A0u.add(0, r12);
            }
            this.A0H.A03(new C39341ph(AAE));
            Set set = this.A0v;
            if (set.size() > 0) {
                this.A0R = true;
                set.add(AAE);
                C49632Lo r02 = this.A0C;
                if (r02 != null) {
                    r02.A00();
                }
                this.A0D.A03(true, set.size());
                A09();
                this.A0A.Aap();
                A0N(true);
                return;
            }
            Collection singletonList = Collections.singletonList(AAE);
            ArrayList arrayList = null;
            if (!AbstractC454421p.A00 || r13 == null) {
                bitmap = null;
                r8 = null;
            } else {
                arrayList = new ArrayList();
                arrayList.add(new AnonymousClass01T(r13, AnonymousClass028.A0J(r13)));
                View findViewById = A02().findViewById(R.id.header_transition);
                arrayList.add(new AnonymousClass01T(findViewById, AnonymousClass028.A0J(findViewById)));
                View findViewById2 = A02().findViewById(R.id.footer_transition);
                arrayList.add(new AnonymousClass01T(findViewById2, AnonymousClass028.A0J(findViewById2)));
                View findViewById3 = A02().findViewById(R.id.filter_swipe_transition);
                arrayList.add(new AnonymousClass01T(findViewById3, AnonymousClass028.A0J(findViewById3)));
                View findViewById4 = A02().findViewById(R.id.send_button_transition);
                arrayList.add(new AnonymousClass01T(findViewById4, AnonymousClass028.A0J(findViewById4)));
                bitmap = r13.A00;
                r8 = r13.A05;
            }
            A0F(bitmap, this.A0Z, r8, singletonList, arrayList);
        }
    }

    public final void A0L(ArrayList arrayList) {
        Set set = this.A0v;
        set.clear();
        if (arrayList != null) {
            set.addAll(arrayList);
        }
        this.A0P = true;
        this.A0D.A03(!set.isEmpty(), set.size());
        this.A0R = true;
        C49632Lo r0 = this.A0C;
        if (r0 != null) {
            r0.A00();
        }
        A0M(true);
    }

    public void A0M(boolean z) {
        Log.i("cameraui/restoreui");
        A0N(true);
        boolean z2 = false;
        if (z && this.A06.getVisibility() != 0) {
            this.A06.setVisibility(0);
        }
        boolean AJV = this.A0A.AJV();
        if (z && !AJV) {
            this.A06.requestLayout();
            this.A06.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass2UT(this));
        }
        RecordingView recordingView = this.A0F.A04;
        recordingView.setVisibility(8);
        recordingView.A01.setProgress(0);
        AnonymousClass2U3 r4 = this.A0D;
        boolean z3 = false;
        if (this.A0h.A00.getResources().getConfiguration().orientation == 2) {
            z3 = true;
        }
        WaImageView waImageView = r4.A0B;
        waImageView.setVisibility(0);
        waImageView.setEnabled(AJV);
        WaImageView waImageView2 = r4.A0C;
        waImageView2.setEnabled(AJV);
        boolean z4 = true;
        if (waImageView2.getVisibility() == 0) {
            z2 = true;
        }
        int i = 8;
        if (r4.A0F) {
            i = 0;
        }
        waImageView2.setVisibility(i);
        boolean z5 = false;
        if (waImageView2.getVisibility() == 0) {
            z5 = true;
        }
        if (!z2 && z5) {
            waImageView2.startAnimation(r4.A01);
        }
        WaImageView waImageView3 = r4.A0A;
        waImageView3.setEnabled(AJV);
        boolean z6 = false;
        if (waImageView3.getVisibility() == 0) {
            z6 = true;
        }
        r4.A01();
        if (waImageView3.getVisibility() != 0) {
            z4 = false;
        }
        if (!z6 && z4) {
            waImageView3.startAnimation(r4.A01);
        }
        CircleWaImageView circleWaImageView = r4.A08;
        if (circleWaImageView != null) {
            circleWaImageView.setVisibility(0);
        }
        WaImageView waImageView4 = r4.A09;
        if (waImageView4 != null) {
            waImageView4.setVisibility(0);
        }
        if (!z3) {
            r4.A07.setVisibility(0);
        }
    }

    public final void A0N(boolean z) {
        int i;
        this.A0C.A03(z);
        AnonymousClass2U3 r2 = this.A0D;
        Set set = this.A0v;
        boolean z2 = true;
        r2.A03((!set.isEmpty()) & z, set.size());
        AnonymousClass2U3 r3 = this.A0D;
        if (!z || this.A0h.A00.getResources().getConfiguration().orientation == 2) {
            z2 = false;
        }
        TextView textView = r3.A07;
        if (z2) {
            i = 0;
        } else {
            i = 8;
            if (r3.A0E) {
                i = 4;
            }
        }
        textView.setVisibility(i);
    }

    public final void A0O(boolean z) {
        StringBuilder sb;
        Vibrator A0K;
        AnonymousClass1AX r5 = this.A0e;
        r5.A01(554249147, "video_record");
        this.A0D.A0B.clearAnimation();
        StringBuilder sb2 = new StringBuilder("cameraui/stopvideocapture ");
        sb2.append(z);
        Log.i(sb2.toString());
        r5.A06 = SystemClock.elapsedRealtime();
        r5.A02(554249147, "stop_video_recording");
        this.A0A.AeT();
        AnonymousClass1s9 r1 = this.A0A;
        Integer valueOf = Integer.valueOf(r1.getCameraApi());
        int cameraType = r1.getCameraType();
        int i = !r1.AJS();
        String l = Long.toString(r1.getVideoResolution());
        long elapsedRealtime = SystemClock.elapsedRealtime() - r5.A06;
        long j = r5.A04;
        AnonymousClass2UU r8 = new AnonymousClass2UU();
        r8.A02 = Integer.valueOf(cameraType);
        r8.A00 = valueOf;
        r8.A01 = Integer.valueOf(i);
        r8.A05 = l;
        r8.A03 = Long.valueOf(j);
        r8.A04 = Long.valueOf(elapsedRealtime);
        if (r5.A09) {
            r5.A07.A07(r8);
        }
        boolean z2 = r5.A0A;
        if (z2) {
            r5.A01(554249147, "stop_video_recording");
            r5.A03(valueOf, 554249147, cameraType);
            r5.A00(554249147, i);
        }
        if (this.A08 != null) {
            AnonymousClass2U3 r12 = this.A0D;
            Window window = A02().getWindow();
            WindowManager.LayoutParams attributes = window.getAttributes();
            r12.A04.setVisibility(4);
            attributes.screenBrightness = -1.0f;
            window.setAttributes(attributes);
            A02().setRequestedOrientation(-1);
        }
        try {
            if (!(Settings.System.getInt(A02().getContentResolver(), "haptic_feedback_enabled") == 0 || (A0K = this.A0f.A0K()) == null)) {
                A0K.vibrate(75);
            }
        } catch (Settings.SettingNotFoundException e) {
            Log.e("cameraui/stopvideocapture", e);
        }
        if (!z || this.A0L == null) {
            File file = this.A0L;
            if (file == null || !file.exists()) {
                sb = new StringBuilder("cameraui/video file doesn't exist: ");
                sb.append(this.A0L);
            } else {
                if (!this.A0L.delete()) {
                    sb = new StringBuilder("cameraui/failed to delete video ");
                    sb.append(this.A0L.getAbsolutePath());
                }
                this.A0L = null;
                A0M(true);
            }
            Log.e(sb.toString());
            this.A0L = null;
            A0M(true);
        } else {
            r5.A02(554249147, "show_media_preview");
            A0K(new AnonymousClass2US(this.A0L), null, true);
            r5.A01(554249147, "show_media_preview");
        }
        if (z2) {
            r5.A08.AL7(554249147, 2);
        }
    }

    public boolean A0P() {
        boolean z = false;
        if (this.A08 != null) {
            z = true;
        }
        if (z) {
            boolean z2 = false;
            if (this.A0C.A0A.A0B == 3) {
                z2 = true;
            }
            if (z2) {
                AnonymousClass01E A07 = A02().A0V().A07(R.id.gallery_container);
                if (A07 instanceof CameraMediaPickerFragment) {
                    CameraMediaPickerFragment cameraMediaPickerFragment = (CameraMediaPickerFragment) A07;
                    if (cameraMediaPickerFragment.A1J()) {
                        cameraMediaPickerFragment.A1N();
                        return true;
                    }
                }
                this.A0C.A0A.A0M(4);
                return true;
            }
            Set set = this.A0v;
            if (!set.isEmpty()) {
                this.A0H = new C453421e();
                set.clear();
                C49632Lo r0 = this.A0C;
                if (r0 != null) {
                    r0.A00();
                }
                this.A0D.A03(false, set.size());
                return true;
            }
        }
        List<AnonymousClass2U0> list = this.A0u;
        if (!list.isEmpty()) {
            for (AnonymousClass2U0 r02 : list) {
                File ACy = r02.ACy();
                if (!ACy.delete()) {
                    StringBuilder sb = new StringBuilder("cameraui/cannot-delete-file ");
                    sb.append(ACy);
                    Log.w(sb.toString());
                }
            }
            list.clear();
            C49632Lo r03 = this.A0C;
            if (r03 != null) {
                r03.A00();
            }
        }
        return false;
    }

    public final boolean A0Q() {
        if (!this.A0A.AJy()) {
            Handler handler = this.A0F.A01;
            if (!handler.hasMessages(0) && !handler.hasMessages(1)) {
                return false;
            }
        }
        return true;
    }

    public boolean A0R(int i) {
        boolean z = false;
        if (this.A08 != null) {
            z = true;
        }
        if (z && ((i == 25 || i == 24) && this.A0A.AJV())) {
            if (!A0Q()) {
                if (this.A0C.A0A.A0B == 4) {
                    Log.i("cameraui/volume-key-down");
                    this.A0D.A0B.setPressed(true);
                    Handler handler = this.A0F.A01;
                    handler.sendMessageDelayed(handler.obtainMessage(1), 500);
                }
            }
            return true;
        }
        return false;
    }

    public boolean A0S(int i) {
        boolean z = false;
        if (this.A08 != null) {
            z = true;
        }
        if (!z || (i != 25 && i != 24)) {
            return false;
        }
        AnonymousClass1sA r2 = this.A0F;
        Handler handler = r2.A01;
        handler.removeMessages(0);
        handler.removeMessages(1);
        RecordingView recordingView = r2.A04;
        recordingView.setVisibility(8);
        recordingView.A01.setProgress(0);
        if (this.A0A.AJy()) {
            Log.i("cameraui/volume-key-up/stop-video-capture");
            boolean z2 = false;
            if (System.currentTimeMillis() - this.A0F.A00 > 1000) {
                z2 = true;
            }
            A0O(z2);
        } else if (this.A0C.A0A.A0B == 4 && this.A0A.AJV()) {
            Log.i("cameraui/volume-key-up/take-picture");
            A0C();
        }
        this.A0D.A0B.setPressed(false);
        return true;
    }
}
