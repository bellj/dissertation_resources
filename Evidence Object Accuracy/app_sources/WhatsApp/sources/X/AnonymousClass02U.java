package X;

/* renamed from: X.02U  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass02U {
    public static final AnonymousClass02T A00 = new AnonymousClass06E(AnonymousClass06I.A01, false);
    public static final AnonymousClass02T A01;
    public static final AnonymousClass02T A02;
    public static final AnonymousClass02T A03 = AnonymousClass06J.A00;
    public static final AnonymousClass02T A04 = new AnonymousClass06E(null, false);
    public static final AnonymousClass02T A05 = new AnonymousClass06E(null, true);

    static {
        AnonymousClass06G r1 = AnonymousClass06G.A00;
        A01 = new AnonymousClass06E(r1, false);
        A02 = new AnonymousClass06E(r1, true);
    }
}
