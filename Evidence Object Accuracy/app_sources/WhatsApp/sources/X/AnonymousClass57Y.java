package X;

import android.content.Context;

/* renamed from: X.57Y  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass57Y implements AnonymousClass5Z1 {
    public final Context A00;
    public final C14330lG A01;
    public final AnonymousClass19M A02;
    public final AbstractC14470lU A03;
    public final String A04;

    public AnonymousClass57Y(Context context, C14330lG r2, AnonymousClass19M r3, AbstractC14470lU r4, String str) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = str;
        this.A00 = context;
    }
}
