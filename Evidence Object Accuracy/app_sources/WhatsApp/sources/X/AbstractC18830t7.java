package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.0t7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18830t7 {
    public int A00;
    public int A01;
    public final C18790t3 A02;
    public final C16590pI A03;
    public final C18810t5 A04;
    public final C18800t4 A05;
    public final AbstractC14440lR A06;
    public final Integer A07;
    public volatile boolean A08 = true;
    public volatile boolean A09 = false;

    public AbstractC18830t7(C18790t3 r2, C16590pI r3, C18810t5 r4, C18800t4 r5, AbstractC14440lR r6, Integer num) {
        this.A03 = r3;
        this.A06 = r6;
        this.A02 = r2;
        this.A05 = r5;
        this.A04 = r4;
        this.A07 = num;
        this.A00 = 1;
        this.A01 = 1;
    }

    public synchronized File A00(String str) {
        File file = new File(this.A03.A00.getFilesDir(), str);
        if (file.exists() || file.mkdirs()) {
            return file;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SimpleAssetDownloader/getAssetDir/Could not make directory ");
        sb.append(file.getAbsolutePath());
        Log.e(sb.toString());
        return null;
    }

    public String A01(Object obj) {
        if ((this instanceof C18820t6) || (this instanceof C20440vl)) {
            return null;
        }
        if (!(this instanceof C61342zv)) {
            return ((C25841Ba) this).A08.A00.getString("bloks_local_tag", null);
        }
        return ((C61342zv) this).A01.A00.getString("payment_background_store_etag", null);
    }

    public void A02() {
        if (this instanceof C25841Ba) {
            C25841Ba r2 = (C25841Ba) this;
            C14820m6 r3 = r2.A08;
            StringBuilder sb = new StringBuilder("2.22.17.70");
            sb.append(((AbstractC25851Bb) r2.A05).ABm().A03);
            sb.append(" ");
            sb.append(r2.A09.A06());
            r3.A00.edit().putString("bloks_version", sb.toString()).apply();
        }
    }

    public void A03(AnonymousClass2DH r13, AnonymousClass2K5 r14, Object obj, String str) {
        if (!this.A09) {
            this.A09 = true;
            this.A08 = false;
            this.A06.Aaz(new AnonymousClass38E(this.A02, r13, r14, this, this.A04, this.A05, this.A07, obj, str), new Void[0]);
        } else if (r13 != null) {
            r13.ALn();
        }
    }

    public void A04(Object obj, String str) {
        SharedPreferences.Editor putString;
        if (!(this instanceof C18820t6) && !(this instanceof C20440vl)) {
            if (!(this instanceof C61342zv)) {
                putString = ((C25841Ba) this).A08.A00.edit().putString("bloks_local_tag", str);
            } else {
                SharedPreferences.Editor edit = ((C61342zv) this).A01.A00.edit();
                if (str == null) {
                    putString = edit.remove("payment_background_store_etag");
                } else {
                    putString = edit.putString("payment_background_store_etag", str);
                }
            }
            putString.apply();
        }
    }

    public boolean A05() {
        return this instanceof C25841Ba;
    }

    public boolean A06(File file) {
        return file == null || file.list() == null || file.list().length == 0;
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public boolean A07(java.io.InputStream r13, java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 581
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18830t7.A07(java.io.InputStream, java.lang.Object):boolean");
    }

    public boolean A08(Object obj) {
        if ((this instanceof C18820t6) || (this instanceof C20440vl)) {
            return false;
        }
        if (!(this instanceof C61342zv)) {
            return ((C25841Ba) this).A0E();
        }
        return !TextUtils.isEmpty(((C61342zv) this).A01.A00.getString("payment_background_store_etag", null));
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:59:0x0051 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v0, types: [java.util.LinkedHashMap, java.util.AbstractMap] */
    /* JADX WARN: Type inference failed for: r6v3, types: [java.util.Map] */
    /* JADX WARN: Type inference failed for: r6v4, types: [X.3cp] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A09(java.lang.String r9, byte[] r10) {
        /*
            r8 = this;
            boolean r0 = r8 instanceof X.C18820t6
            if (r0 != 0) goto L_0x003f
            boolean r0 = r8 instanceof X.C20440vl
            if (r0 != 0) goto L_0x003d
            boolean r0 = r8 instanceof X.C61342zv
            if (r0 != 0) goto L_0x003d
            r4 = r8
            X.1Ba r4 = (X.C25841Ba) r4
            r3 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r9)     // Catch: Exception -> 0x001e
            if (r0 != 0) goto L_0x001d
            boolean r0 = X.C124885qH.A00(r9, r10)     // Catch: Exception -> 0x001e
            if (r0 == 0) goto L_0x001d
            r3 = 1
        L_0x001d:
            return r3
        L_0x001e:
            r2 = move-exception
            java.lang.String r0 = "BloksAssetManager/verifySignature: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r4.A0B()
            r1.append(r0)
            java.lang.String r0 = "Exception:"
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0)
            return r3
        L_0x003d:
            r0 = 1
            return r0
        L_0x003f:
            r0 = r8
            X.0t6 r0 = (X.C18820t6) r0
            java.lang.String r0 = r0.A01     // Catch: GeneralSecurityException -> 0x00e8
            java.net.URI r1 = new java.net.URI     // Catch: GeneralSecurityException -> 0x00e8
            r1.<init>(r0)     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String r0 = r1.getQuery()     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 != 0) goto L_0x005f
            X.3cp r6 = X.C71373cp.A00     // Catch: GeneralSecurityException -> 0x00e8
        L_0x0051:
            java.lang.String r0 = "signature"
            java.lang.Object r0 = r6.get(r0)     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String r0 = (java.lang.String) r0     // Catch: GeneralSecurityException -> 0x00e8
            boolean r0 = X.C124885qH.A00(r0, r10)     // Catch: GeneralSecurityException -> 0x00e8
            return r0
        L_0x005f:
            java.lang.String r2 = r1.getQuery()     // Catch: GeneralSecurityException -> 0x00e8
            X.C16700pc.A0B(r2)     // Catch: GeneralSecurityException -> 0x00e8
            r4 = 1
            java.lang.String[] r1 = new java.lang.String[r4]     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String r0 = "&"
            r3 = 0
            r1[r3] = r0     // Catch: GeneralSecurityException -> 0x00e8
            java.util.List r0 = X.AnonymousClass03B.A09(r2, r1, r3)     // Catch: GeneralSecurityException -> 0x00e8
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch: GeneralSecurityException -> 0x00e8
            r5.<init>()     // Catch: GeneralSecurityException -> 0x00e8
            java.util.Iterator r7 = r0.iterator()     // Catch: GeneralSecurityException -> 0x00e8
        L_0x007b:
            boolean r0 = r7.hasNext()     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 == 0) goto L_0x00b9
            java.lang.Object r6 = r7.next()     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String r6 = (java.lang.String) r6     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String[] r2 = new java.lang.String[r4]     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String r0 = "="
            r2[r3] = r0     // Catch: GeneralSecurityException -> 0x00e8
            r1 = 2
            java.util.List r2 = X.AnonymousClass03B.A09(r6, r2, r1)     // Catch: GeneralSecurityException -> 0x00e8
            int r0 = r2.size()     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 != r1) goto L_0x007b
            boolean r0 = r2.isEmpty()     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 != 0) goto L_0x00b5
            java.util.Iterator r1 = r2.iterator()     // Catch: GeneralSecurityException -> 0x00e8
        L_0x00a2:
            boolean r0 = r1.hasNext()     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 == 0) goto L_0x00b5
            java.lang.Object r0 = r1.next()     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.String r0 = (java.lang.String) r0     // Catch: GeneralSecurityException -> 0x00e8
            boolean r0 = X.AnonymousClass03C.A0J(r0)     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 == 0) goto L_0x00a2
            goto L_0x007b
        L_0x00b5:
            r5.add(r2)     // Catch: GeneralSecurityException -> 0x00e8
            goto L_0x007b
        L_0x00b9:
            int r0 = X.C16760pi.A0D(r5)     // Catch: GeneralSecurityException -> 0x00e8
            int r1 = X.C17540qy.A03(r0)     // Catch: GeneralSecurityException -> 0x00e8
            r0 = 16
            if (r1 >= r0) goto L_0x00c7
            r1 = 16
        L_0x00c7:
            java.util.LinkedHashMap r6 = new java.util.LinkedHashMap     // Catch: GeneralSecurityException -> 0x00e8
            r6.<init>(r1)     // Catch: GeneralSecurityException -> 0x00e8
            java.util.Iterator r2 = r5.iterator()     // Catch: GeneralSecurityException -> 0x00e8
        L_0x00d0:
            boolean r0 = r2.hasNext()     // Catch: GeneralSecurityException -> 0x00e8
            if (r0 == 0) goto L_0x0051
            java.lang.Object r0 = r2.next()     // Catch: GeneralSecurityException -> 0x00e8
            java.util.List r0 = (java.util.List) r0     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.Object r1 = r0.get(r3)     // Catch: GeneralSecurityException -> 0x00e8
            java.lang.Object r0 = r0.get(r4)     // Catch: GeneralSecurityException -> 0x00e8
            r6.put(r1, r0)     // Catch: GeneralSecurityException -> 0x00e8
            goto L_0x00d0
        L_0x00e8:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18830t7.A09(java.lang.String, byte[]):boolean");
    }
}
