package X;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;

/* renamed from: X.0zs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22930zs {
    public final Handler A00 = new HandlerC33921fI(Looper.getMainLooper(), this);
    public final C16240og A01;
    public final C14850m9 A02;
    public final C245115u A03;
    public final C17220qS A04;
    public final C22280yp A05;
    public final HashMap A06 = new HashMap();

    public C22930zs(C16240og r3, C14850m9 r4, C245115u r5, C17220qS r6, C22280yp r7) {
        this.A02 = r4;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        this.A01 = r3;
    }

    public final AbstractC14640lm A00(AbstractC14640lm r3) {
        HashMap hashMap = this.A06;
        AbstractC14640lm r0 = (AbstractC14640lm) hashMap.get(r3);
        if (r0 != null) {
            return r0;
        }
        hashMap.put(r3, r3);
        return r3;
    }

    public final boolean A01(AbstractC14640lm r5) {
        C33911fH r0;
        String A03 = this.A02.A03(1566);
        if (!"disabled".equals(A03)) {
            C22280yp r1 = this.A05;
            if (!r1.A06(r5)) {
                if (!"new".equals(A03)) {
                    return false;
                }
                HashMap hashMap = r1.A06;
                C33911fH r02 = (C33911fH) hashMap.get(r5);
                if (r02 != null && r02.A02 == 1 && ((r0 = (C33911fH) hashMap.get(r5)) == null || r0.A01 != 0)) {
                    return false;
                }
            }
        }
        return true;
    }
}
