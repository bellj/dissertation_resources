package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5ld  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122545ld extends AbstractC118835cS {
    public final TextView A00;

    public C122545ld(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.title);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        C123125me r32 = (C123125me) r3;
        TextView textView = this.A00;
        textView.setText(r32.A01);
        textView.setOnClickListener(r32.A00);
    }
}
