package X;

import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.ImageView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.redex.IDxLListenerShape13S0100000_1_I1;
import com.facebook.redex.ViewOnClickCListenerShape1S0300000_I1;
import com.whatsapp.R;
import java.util.HashMap;

/* renamed from: X.3IZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IZ {
    public static HandlerThread A0U;
    public static HandlerC51992Zy A0V;
    public static HandlerC52042a9 A0W;
    public static final int A0X = ViewConfiguration.getKeyRepeatDelay();
    public static final int A0Y = ViewConfiguration.getKeyRepeatTimeout();
    public static final HashMap A0Z = C12970iu.A11();
    public int A00;
    public int A01;
    public AbstractC116455Vm A02;
    public C53262da A03;
    public C53252dZ A04;
    public C37471mS[] A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final int A09;
    public final Context A0A;
    public final Paint A0B = C12990iw.A0F();
    public final LayoutInflater A0C;
    public final View.OnClickListener A0D;
    public final View A0E;
    public final View A0F;
    public final ViewGroup A0G;
    public final ViewTreeObserver.OnGlobalLayoutListener A0H = new IDxLListenerShape13S0100000_1_I1(this, 1);
    public final AbsListView.OnScrollListener A0I;
    public final AbsListView.OnScrollListener A0J = new AnonymousClass3O3(this);
    public final ImageView A0K;
    public final ViewPager A0L;
    public final AbstractC15710nm A0M;
    public final C14820m6 A0N;
    public final AnonymousClass19M A0O;
    public final C231510o A0P;
    public final C16630pM A0Q;
    public final boolean A0R;
    public final AnonymousClass2bf[] A0S;
    public final AnonymousClass3HU[] A0T;

    public AnonymousClass3IZ(Context context, View view, ViewGroup viewGroup, AbsListView.OnScrollListener onScrollListener, AbstractC15710nm r17, C14820m6 r18, AnonymousClass018 r19, AnonymousClass19M r20, C231510o r21, C16630pM r22) {
        int length;
        int length2;
        this.A0I = onScrollListener;
        this.A0R = C12960it.A1W(onScrollListener);
        this.A0F = view;
        this.A0A = context;
        this.A0M = r17;
        this.A0O = r20;
        this.A0P = r21;
        this.A0N = r18;
        this.A0Q = r22;
        this.A07 = AnonymousClass00T.A00(context, R.color.emoji_popup_body);
        this.A09 = AnonymousClass00T.A00(context, R.color.paletteElevationOverlay);
        this.A0G = C12980iv.A0P(viewGroup, R.id.emoji_group_layout);
        AnonymousClass3HU[] r1 = new AnonymousClass3HU[EnumC452920z.values().length + 1];
        this.A0T = r1;
        r1[0] = new C58292oa(r21);
        int i = 1;
        while (true) {
            AnonymousClass3HU[] r3 = this.A0T;
            length = r3.length;
            if (i >= length) {
                break;
            }
            r3[i] = new AnonymousClass3HU(EnumC452920z.values()[i - 1], i);
            i++;
        }
        AnonymousClass2bf[] r12 = new AnonymousClass2bf[length];
        this.A0S = r12;
        r12[0] = new AnonymousClass2bf(context, this, r19, 0);
        this.A00 = r21.A00() > 0 ? 0 : 1;
        ViewPager viewPager = (ViewPager) viewGroup.findViewById(R.id.pager);
        this.A0L = viewPager;
        viewPager.setAdapter(new C58342of(this, r19));
        viewPager.A0G(new AnonymousClass3S9(context, this, r19, r22));
        this.A0C = AnonymousClass01d.A01(context);
        this.A06 = context.getResources().getDimensionPixelSize(R.dimen.emoji_picker_item);
        this.A08 = context.getResources().getDimensionPixelSize(R.dimen.emoji_picker_icon);
        AnonymousClass3HU[] r11 = this.A0T;
        for (AnonymousClass3HU r8 : r11) {
            View findViewById = this.A0G.findViewById(r8.A02);
            C12960it.A0r(context, findViewById, r8.A05);
            C12990iw.A1C(findViewById, this, r19, r8, 1);
        }
        if (C28141Kv.A01(r19)) {
            length2 = this.A00;
        } else {
            length2 = (this.A0S.length - 1) - this.A00;
        }
        this.A0L.A0F(length2, false);
        A00(this.A00);
        this.A0D = new ViewOnClickCListenerShape1S0300000_I1(this, r22, r18, 2);
        ImageView A0L = C12970iu.A0L(viewGroup, R.id.delete_symbol_tb);
        this.A0K = A0L;
        if (A0L != null) {
            HandlerC73323g2 r13 = new HandlerC73323g2(Looper.getMainLooper(), this);
            A0L.setClickable(true);
            A0L.setLongClickable(true);
            A0L.setOnTouchListener(new View.OnTouchListener(r13, this) { // from class: X.4nU
                public final /* synthetic */ Handler A00;
                public final /* synthetic */ AnonymousClass3IZ A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view2, MotionEvent motionEvent) {
                    AnonymousClass3IZ r4 = this.A01;
                    Handler handler = this.A00;
                    int action = motionEvent.getAction();
                    if (action == 0) {
                        AbstractC116455Vm r0 = r4.A02;
                        if (r0 == null) {
                            return true;
                        }
                        r0.AMr();
                        handler.sendEmptyMessageDelayed(0, (long) AnonymousClass3IZ.A0Y);
                        return true;
                    } else if (action != 1 && action != 3) {
                        return false;
                    } else {
                        handler.removeMessages(0);
                        return true;
                    }
                }
            });
            C12960it.A10(A0L, this, 7);
            AnonymousClass2GF.A01(this.A0A, A0L, r19, R.drawable.emoji_x);
            C12960it.A0r(context, A0L, R.string.backspace);
        }
        View findViewById2 = viewGroup.findViewById(R.id.emoji_tip);
        this.A0E = findViewById2;
        if (findViewById2 != null) {
            findViewById2.setVisibility(8);
            C12960it.A10(findViewById2.findViewById(R.id.ok), this, 6);
            C12960it.A10(findViewById2, this, 5);
        }
    }

    public final void A00(int i) {
        AnonymousClass3HU[] r7 = this.A0T;
        for (AnonymousClass3HU r3 : r7) {
            ViewGroup viewGroup = this.A0G;
            View findViewById = viewGroup.findViewById(r3.A03);
            View findViewById2 = viewGroup.findViewById(r3.A02);
            if (findViewById2 != null) {
                if (r3.A04 == i) {
                    findViewById2.setSelected(true);
                    C12970iu.A18(findViewById.getContext(), findViewById, R.color.picker_underline_color);
                } else {
                    findViewById2.setSelected(false);
                    findViewById.setBackgroundColor(0);
                }
            }
        }
    }

    public final void A01(C52502ax r5) {
        if (AnonymousClass3JU.A02(r5.A07)) {
            C53262da r1 = new C53262da(r5, this.A0O, new AnonymousClass5UB(r5, this) { // from class: X.56a
                public final /* synthetic */ C52502ax A00;
                public final /* synthetic */ AnonymousClass3IZ A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass5UB
                public final void AW8(int[] iArr) {
                    AnonymousClass3IZ r0 = this.A01;
                    C52502ax r12 = this.A00;
                    r0.A02(iArr);
                    r12.setEmoji(iArr);
                    AnonymousClass3JF.A01(r0.A0Q, iArr);
                    r12.invalidate();
                }
            }, r5.A07);
            this.A03 = r1;
            C38491oB.A01(r5, this.A0F, r1);
        }
    }

    public final void A02(int[] iArr) {
        if (iArr == null) {
            this.A0M.AaV("EmojiPicker/onEmojiSelected/emoji being added is null", null, true);
            return;
        }
        this.A0P.A07(iArr);
        if (this.A00 != 0) {
            this.A0S[0].notifyDataSetChanged();
        }
        AbstractC116455Vm r0 = this.A02;
        if (r0 != null) {
            r0.APc(iArr);
        }
    }
}
