package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.458  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass458 extends AnonymousClass57Y {
    public AnonymousClass458(Context context, C14330lG r2, AnonymousClass19M r3, AbstractC14470lU r4, String str) {
        super(context, r2, r3, r4, str);
    }

    @Override // java.lang.Runnable
    public void run() {
        File A0I = C22200yh.A0I(this.A01, this.A04);
        if (A0I.exists() && !A0I.delete()) {
            Log.w("MediaDeleteDoodleJob/failed-delete-doodle-file");
        }
    }
}
