package X;

import android.os.IBinder;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;

/* renamed from: X.3rE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79803rE extends C65873Li implements IGoogleMapDelegate {
    public C79803rE(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }
}
