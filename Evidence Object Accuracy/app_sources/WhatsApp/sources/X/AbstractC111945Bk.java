package X;

import java.util.Iterator;

/* renamed from: X.5Bk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC111945Bk implements Iterable, AbstractC16910px {
    public final int A00;
    public final int A01;
    public final int A02;

    public AbstractC111945Bk(int i, int i2) {
        this.A00 = i;
        if (i < i2) {
            int i3 = i2 % 1;
            int i4 = i % 1;
            int i5 = ((i3 < 0 ? i3 + 1 : i3) - (i4 < 0 ? i4 + 1 : i4)) % 1;
            i2 -= i5 < 0 ? i5 + 1 : i5;
        }
        this.A01 = i2;
        this.A02 = 1;
    }

    public final int A00() {
        return this.A00;
    }

    public final int A01() {
        return this.A01;
    }

    public final int A02() {
        return this.A02;
    }

    @Override // java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return new AnonymousClass5DO(this.A00, this.A01, this.A02);
    }
}
