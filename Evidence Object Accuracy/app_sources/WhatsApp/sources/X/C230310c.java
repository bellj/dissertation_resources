package X;

import android.os.Message;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.10c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C230310c implements AbstractC15920o8 {
    public final C18990tO A00;

    public C230310c(C18990tO r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{242, 243, 244, 245};
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        C18980tN r1;
        C18990tO r4;
        if (i == 242) {
            Object obj = message.obj;
            AnonymousClass009.A05(obj);
            AnonymousClass1V8 r5 = (AnonymousClass1V8) obj;
            AnonymousClass1V8 A0E = r5.A0E("pair-device");
            if (A0E != null) {
                List<AnonymousClass1V8> A0J = A0E.A0J("ref");
                ArrayList arrayList = new ArrayList();
                for (AnonymousClass1V8 r12 : A0J) {
                    arrayList.add(r12.A0G());
                    r12.A0G();
                }
                r4 = this.A00;
                r5.A0I("id", null);
                r1 = r4.A00;
            }
            return true;
        } else if (i == 243) {
            Object obj2 = message.obj;
            AnonymousClass009.A05(obj2);
            AnonymousClass1V8 r3 = (AnonymousClass1V8) obj2;
            AnonymousClass1V8 A0E2 = r3.A0E("pair-success");
            if (A0E2 != null) {
                AnonymousClass1V8 A0E3 = A0E2.A0E("device-identity");
                AnonymousClass1V8 A0E4 = A0E2.A0E("device");
                if (!(A0E3 == null || A0E4 == null)) {
                    r4 = this.A00;
                    r3.A0I("id", null);
                    A0E4.A0I("jid", null);
                    r1 = r4.A00;
                }
            }
            return true;
        } else if (i != 244 && i != 245) {
            return false;
        } else {
            r1 = this.A00.A00;
        }
        r1.A00(C26571Dz.class);
        return true;
    }
}
