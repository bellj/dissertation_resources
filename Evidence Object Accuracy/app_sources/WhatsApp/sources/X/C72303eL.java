package X;

import com.facebook.redex.RunnableBRunnable0Shape16S0100000_I1_2;
import java.util.List;

/* renamed from: X.3eL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72303eL extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ boolean $invalidate;
    public final /* synthetic */ AnonymousClass1J7 $onError;
    public final /* synthetic */ AnonymousClass1J7 $onSuccess;
    public final /* synthetic */ AnonymousClass3BT this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72303eL(AnonymousClass3BT r2, AnonymousClass1J7 r3, AnonymousClass1J7 r4, boolean z) {
        super(1);
        this.this$0 = r2;
        this.$invalidate = z;
        this.$onError = r3;
        this.$onSuccess = r4;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        C65923Lp r5 = (C65923Lp) obj;
        C16700pc.A0E(r5, 0);
        List list = r5.A00;
        AnonymousClass3BT r0 = this.this$0;
        if (list == null) {
            r0.A00.A0H(new RunnableBRunnable0Shape16S0100000_I1_2(this.$onError, 3));
        } else {
            AnonymousClass1WI.A00(this.this$0.A00, this.$onSuccess, r0.A03.A01(list, this.$invalidate), 40);
        }
        return AnonymousClass1WZ.A00;
    }
}
