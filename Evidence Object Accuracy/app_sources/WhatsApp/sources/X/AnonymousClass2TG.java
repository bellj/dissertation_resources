package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;

/* renamed from: X.2TG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2TG extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2TG A0C;
    public static volatile AnonymousClass255 A0D;
    public int A00;
    public AbstractC27881Jp A01 = AbstractC27881Jp.A01;
    public String A02 = "";
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;

    static {
        AnonymousClass2TG r0 = new AnonymousClass2TG();
        A0C = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        switch (r8.ordinal()) {
            case 0:
                return A0C;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass2TG r10 = (AnonymousClass2TG) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                boolean z2 = this.A0B;
                int i2 = r10.A00;
                boolean z3 = true;
                if ((i2 & 1) != 1) {
                    z3 = false;
                }
                this.A0B = r9.Afl(z, z2, z3, r10.A0B);
                boolean z4 = false;
                if ((i & 2) == 2) {
                    z4 = true;
                }
                boolean z5 = this.A09;
                boolean z6 = false;
                if ((i2 & 2) == 2) {
                    z6 = true;
                }
                this.A09 = r9.Afl(z4, z5, z6, r10.A09);
                boolean z7 = false;
                if ((i & 4) == 4) {
                    z7 = true;
                }
                boolean z8 = this.A03;
                boolean z9 = false;
                if ((i2 & 4) == 4) {
                    z9 = true;
                }
                this.A03 = r9.Afl(z7, z8, z9, r10.A03);
                boolean z10 = false;
                if ((i & 8) == 8) {
                    z10 = true;
                }
                boolean z11 = this.A0A;
                boolean z12 = false;
                if ((i2 & 8) == 8) {
                    z12 = true;
                }
                this.A0A = r9.Afl(z10, z11, z12, r10.A0A);
                boolean z13 = false;
                if ((i & 16) == 16) {
                    z13 = true;
                }
                boolean z14 = this.A08;
                boolean z15 = false;
                if ((i2 & 16) == 16) {
                    z15 = true;
                }
                this.A08 = r9.Afl(z13, z14, z15, r10.A08);
                boolean z16 = false;
                if ((i & 32) == 32) {
                    z16 = true;
                }
                boolean z17 = this.A06;
                boolean z18 = false;
                if ((i2 & 32) == 32) {
                    z18 = true;
                }
                this.A06 = r9.Afl(z16, z17, z18, r10.A06);
                boolean z19 = false;
                if ((i & 64) == 64) {
                    z19 = true;
                }
                boolean z20 = this.A07;
                boolean z21 = false;
                if ((i2 & 64) == 64) {
                    z21 = true;
                }
                this.A07 = r9.Afl(z19, z20, z21, r10.A07);
                boolean z22 = false;
                if ((i & 128) == 128) {
                    z22 = true;
                }
                boolean z23 = this.A04;
                boolean z24 = false;
                if ((i2 & 128) == 128) {
                    z24 = true;
                }
                this.A04 = r9.Afl(z22, z23, z24, r10.A04);
                boolean z25 = false;
                if ((i & 256) == 256) {
                    z25 = true;
                }
                boolean z26 = this.A05;
                boolean z27 = false;
                if ((i2 & 256) == 256) {
                    z27 = true;
                }
                this.A05 = r9.Afl(z25, z26, z27, r10.A05);
                boolean z28 = false;
                if ((i & 512) == 512) {
                    z28 = true;
                }
                String str = this.A02;
                boolean z29 = false;
                if ((i2 & 512) == 512) {
                    z29 = true;
                }
                this.A02 = r9.Afy(str, r10.A02, z28, z29);
                boolean z30 = false;
                if ((i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z30 = true;
                }
                AbstractC27881Jp r2 = this.A01;
                boolean z31 = false;
                if ((i2 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z31 = true;
                }
                this.A01 = r9.Afm(r2, r10.A01, z30, z31);
                if (r9 == C463025i.A00) {
                    this.A00 |= r10.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r92.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 8:
                                this.A00 |= 1;
                                this.A0B = r92.A0F();
                                break;
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                this.A00 |= 2;
                                this.A09 = r92.A0F();
                                break;
                            case 24:
                                this.A00 |= 4;
                                this.A03 = r92.A0F();
                                break;
                            case 32:
                                this.A00 |= 8;
                                this.A0A = r92.A0F();
                                break;
                            case 40:
                                this.A00 |= 16;
                                this.A08 = r92.A0F();
                                break;
                            case 48:
                                this.A00 |= 32;
                                this.A06 = r92.A0F();
                                break;
                            case 56:
                                this.A00 |= 64;
                                this.A07 = r92.A0F();
                                break;
                            case 64:
                                this.A00 |= 128;
                                this.A04 = r92.A0F();
                                break;
                            case C43951xu.A02:
                                this.A00 |= 256;
                                this.A05 = r92.A0F();
                                break;
                            case 82:
                                String A0A = r92.A0A();
                                this.A00 |= 512;
                                this.A02 = A0A;
                                break;
                            case 90:
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A01 = r92.A08();
                                break;
                            default:
                                if (A0a(r92, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r1 = new C28971Pt(e2.getMessage());
                        r1.unfinishedMessage = this;
                        throw new RuntimeException(r1);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2TG();
            case 5:
                return new AnonymousClass2TI();
            case 6:
                break;
            case 7:
                if (A0D == null) {
                    synchronized (AnonymousClass2TG.class) {
                        if (A0D == null) {
                            A0D = new AnonymousClass255(A0C);
                        }
                    }
                }
                return A0D;
            default:
                throw new UnsupportedOperationException();
        }
        return A0C;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A00(1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A00(2);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A00(3);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A00(4);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A00(5);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A00(6);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A00(7);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A00(8);
        }
        if ((i3 & 256) == 256) {
            i2 += CodedOutputStream.A00(9);
        }
        if ((i3 & 512) == 512) {
            i2 += CodedOutputStream.A07(10, this.A02);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A09(this.A01, 11);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0J(1, this.A0B);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0J(2, this.A09);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0J(3, this.A03);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0J(4, this.A0A);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0J(5, this.A08);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0J(6, this.A06);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0J(7, this.A07);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0J(8, this.A04);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0J(9, this.A05);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0I(10, this.A02);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0K(this.A01, 11);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
