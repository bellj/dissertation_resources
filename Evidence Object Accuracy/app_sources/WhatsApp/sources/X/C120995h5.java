package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5h5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120995h5 extends AbstractC1316063k {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(26);
    public final C121035h9 A00;
    public final C1315163b A01;
    public final C1316363n A02;
    public final boolean A03;

    public C120995h5(C1316463o r9, UserJid userJid, AnonymousClass6F2 r11, C1316563p r12, C121035h9 r13, C1316763r r14, C1316363n r15, C1316363n r16, C1316363n r17, AnonymousClass1V8 r18, String str, String str2, String str3, boolean z) {
        super(r18);
        this.A01 = new C1315163b(r9, r11, r12, new AnonymousClass63Z(userJid, r16, str2), r14, new C1315263c(r15, str), str3);
        this.A00 = r13;
        this.A03 = z;
        this.A02 = r17;
    }

    public /* synthetic */ C120995h5(Parcel parcel) {
        super(parcel);
        Parcelable A0I = C12990iw.A0I(parcel, C1315163b.class);
        AnonymousClass009.A05(A0I);
        this.A01 = (C1315163b) A0I;
        this.A00 = (C121035h9) C12990iw.A0I(parcel, C121035h9.class);
        this.A03 = C12960it.A1S(parcel.readInt());
        this.A02 = (C1316363n) C12990iw.A0I(parcel, C1316363n.class);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0097 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C120995h5(java.lang.String r13) {
        /*
            r12 = this;
            r12.<init>(r13)
            org.json.JSONObject r2 = X.C13000ix.A05(r13)
            java.lang.String r0 = "sender"
            java.lang.String r4 = ""
            java.lang.String r1 = r2.optString(r0, r4)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0037
            org.json.JSONObject r3 = X.C13000ix.A05(r1)     // Catch: JSONException -> 0x0032
            java.lang.String r0 = "phone_number"
            java.lang.String r1 = r3.optString(r0, r4)     // Catch: JSONException -> 0x0032
            java.lang.String r0 = "amount"
            java.lang.String r0 = r3.optString(r0, r4)     // Catch: JSONException -> 0x0032
            X.63n r0 = X.C1316363n.A01(r0)     // Catch: JSONException -> 0x0032
            X.AnonymousClass009.A05(r0)     // Catch: JSONException -> 0x0032
            X.63c r10 = new X.63c     // Catch: JSONException -> 0x0032
            r10.<init>(r0, r1)     // Catch: JSONException -> 0x0032
            goto L_0x0038
        L_0x0032:
            java.lang.String r0 = "PAY: Sender fromJsonString threw exception"
            com.whatsapp.util.Log.w(r0)
        L_0x0037:
            r10 = 0
        L_0x0038:
            java.lang.String r0 = "receiver"
            java.lang.String r1 = r2.optString(r0, r4)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0070
            org.json.JSONObject r5 = X.C13000ix.A05(r1)     // Catch: JSONException -> 0x006b
            java.lang.String r0 = "jid"
            java.lang.String r0 = r5.optString(r0, r4)     // Catch: JSONException -> 0x006b
            com.whatsapp.jid.UserJid r3 = com.whatsapp.jid.UserJid.getNullable(r0)     // Catch: JSONException -> 0x006b
            java.lang.String r0 = "phone_number"
            java.lang.String r1 = r5.optString(r0, r4)     // Catch: JSONException -> 0x006b
            java.lang.String r0 = "amount"
            java.lang.String r0 = r5.optString(r0, r4)     // Catch: JSONException -> 0x006b
            X.63n r0 = X.C1316363n.A01(r0)     // Catch: JSONException -> 0x006b
            X.AnonymousClass009.A05(r0)     // Catch: JSONException -> 0x006b
            X.63Z r8 = new X.63Z     // Catch: JSONException -> 0x006b
            r8.<init>(r3, r0, r1)     // Catch: JSONException -> 0x006b
            goto L_0x0071
        L_0x006b:
            java.lang.String r0 = "PAY: Receiver fromJsonString threw exception"
            com.whatsapp.util.Log.w(r0)
        L_0x0070:
            r8 = 0
        L_0x0071:
            java.lang.String r0 = "note"
            java.lang.String r11 = r2.optString(r0, r4)
            java.lang.String r0 = "quote"
            java.lang.String r0 = r2.optString(r0, r4)
            X.63p r7 = X.C1316563p.A00(r0)
            java.lang.String r0 = "claim"
            java.lang.String r0 = r2.optString(r0)
            X.63o r5 = X.C1316463o.A01(r0)
            java.lang.String r0 = "deposit"
            java.lang.String r1 = r2.optString(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x00a2
            X.5h9 r3 = new X.5h9     // Catch: JSONException -> 0x009d
            r3.<init>(r1)     // Catch: JSONException -> 0x009d
            goto L_0x00a3
        L_0x009d:
            java.lang.String r0 = "PAY:NoviDepositTransaction failed to create transaction from the JSON"
            com.whatsapp.util.Log.w(r0)
        L_0x00a2:
            r3 = 0
        L_0x00a3:
            java.lang.String r0 = "balance_debit"
            org.json.JSONObject r0 = r2.optJSONObject(r0)
            X.6F2 r6 = X.AnonymousClass6F2.A02(r0)
            X.63r r9 = X.AbstractC1316063k.A01(r2)
            java.lang.String r1 = "is_unilateral"
            r0 = 0
            boolean r0 = r2.optBoolean(r1, r0)
            r12.A03 = r0
            X.AnonymousClass009.A05(r10)
            X.AnonymousClass009.A05(r8)
            X.AnonymousClass009.A05(r7)
            X.63b r4 = new X.63b
            r4.<init>(r5, r6, r7, r8, r9, r10, r11)
            r12.A01 = r4
            r12.A00 = r3
            java.lang.String r0 = "final_receiver_amount"
            java.lang.String r0 = r2.optString(r0)
            X.63n r0 = X.C1316363n.A01(r0)
            r12.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C120995h5.<init>(java.lang.String):void");
    }

    @Override // X.AbstractC1316063k
    public void A05(JSONObject jSONObject) {
        try {
            C1315163b r2 = this.A01;
            C1315263c r4 = r2.A05;
            JSONObject A0a = C117295Zj.A0a();
            try {
                A0a.put("amount", r4.A00.A02());
                String str = r4.A01;
                if (str == null) {
                    str = "";
                }
                A0a.put("phone_number", str);
            } catch (JSONException unused) {
                Log.w("PAY: Sender toJson threw exception");
            }
            jSONObject.put("sender", A0a);
            AnonymousClass63Z r5 = r2.A03;
            JSONObject A0a2 = C117295Zj.A0a();
            try {
                UserJid userJid = r5.A00;
                String str2 = "";
                A0a2.put("jid", userJid != null ? userJid.getRawString() : str2);
                String str3 = r5.A02;
                if (str3 != null) {
                    str2 = str3;
                }
                A0a2.put("phone_number", str2);
                A0a2.put("amount", r5.A01.A02());
            } catch (JSONException unused2) {
                Log.w("PAY: Receiver toJson threw exception");
            }
            jSONObject.put("receiver", A0a2);
            jSONObject.put("quote", r2.A02.A02());
            jSONObject.put("note", r2.A06);
            C1316463o r0 = r2.A00;
            if (r0 != null) {
                jSONObject.put("claim", r0.A02());
            }
            C121035h9 r02 = this.A00;
            if (r02 != null) {
                jSONObject.put("deposit", r02.A04());
            }
            AnonymousClass6F2 r1 = r2.A01;
            if (r1 != null) {
                C117315Zl.A0W(r1, "balance_debit", jSONObject);
            }
            C1316763r r42 = r2.A04;
            if (r42 != null) {
                JSONObject A0a3 = C117295Zj.A0a();
                int i = r42.A01;
                A0a3.put("reason", i != 1 ? i != 2 ? null : "ASYNC_NOVI_INITIATED" : "CLAIM");
                A0a3.put("completed_timestamp_seconds", r42.A00);
                jSONObject.put("refund_transaction", A0a3);
            }
            jSONObject.put("is_unilateral", this.A03);
            C1316363n r03 = this.A02;
            if (r03 != null) {
                jSONObject.put("final_receiver_amount", r03.A02());
            }
        } catch (JSONException e) {
            Log.w("PAY:NoviTransactionP2P/addTransactionDataToJson: Error while creating a JSON from a transaction", e);
        }
    }

    @Override // X.AbstractC1316063k, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A00, i);
        parcel.writeInt(this.A03 ? 1 : 0);
        parcel.writeParcelable(this.A02, i);
    }
}
