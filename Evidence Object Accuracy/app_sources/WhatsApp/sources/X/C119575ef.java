package X;

import android.app.Activity;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.base.BkFcsPreloadingScreenFragment;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.5ef  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119575ef extends AbstractC17440qo {
    public final C17870rX A00;

    public C119575ef(C17870rX r4) {
        super("wa.action.phoenix.FcsReturnResult", "wa.action.phoenix.FcsReturnResultV2", "wa.action.phoenix.FdsIq", "wa.action.phoenix.FdsIqV2", "wa.action.phoenix.statemachine.GoBack", "wa.action.phoenix.statemachine.GoBackV2");
        this.A00 = r4;
    }

    @Override // X.AbstractC17450qp
    public /* bridge */ /* synthetic */ Object A9j(C14220l3 r11, C1093651k r12, C14240l5 r13) {
        String str;
        AnonymousClass67G r1;
        AnonymousClass67G r14;
        AnonymousClass67G r15;
        Object obj;
        String str2;
        AbstractC17770rM AGE;
        Object obj2;
        C14230l4 r132 = (C14230l4) r13;
        String str3 = r12.A00;
        switch (str3.hashCode()) {
            case -1883695124:
                str = "wa.action.phoenix.FcsReturnResultV2";
                break;
            case -579883706:
                if (!str3.equals("wa.action.phoenix.FdsIqV2")) {
                    return null;
                }
                List list = r11.A00;
                String A0g = C12960it.A0g(list, 0);
                String A0g2 = C12960it.A0g(list, 1);
                String A0g3 = C12960it.A0g(list, 2);
                Map map = (Map) list.get(3);
                C17870rX r6 = this.A00;
                AnonymousClass67O r5 = new AnonymousClass67O(r132, r11, this);
                C16700pc.A0E(A0g, 0);
                C16700pc.A0E(A0g2, 1);
                try {
                    EnumC869549q valueOf = EnumC869549q.valueOf(A0g2);
                    C19750uc r16 = r6.A01;
                    if (map == null || (obj2 = map.get("job_id")) == null) {
                        throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
                    }
                    AnonymousClass17Q A00 = r16.A00((String) obj2);
                    if (A00 == null) {
                        r5.AQD(AnonymousClass3I1.A00(null));
                        return null;
                    }
                    A00.A05(new C69183Yg(r5, r6, A00), valueOf, new AnonymousClass3FB(A0g, A0g3, map));
                    return null;
                } catch (IllegalArgumentException unused) {
                    r5.AQD(AnonymousClass3I1.A00(null));
                    return null;
                }
            case -574422768:
                if (!str3.equals("wa.action.phoenix.statemachine.GoBackV2")) {
                    return null;
                }
                List list2 = r11.A00;
                String A0g4 = C12960it.A0g(list2, 0);
                String A0g5 = C12960it.A0g(list2, 1);
                C17870rX r0 = this.A00;
                C16700pc.A0E(A0g5, 1);
                AnonymousClass17Q A002 = r0.A01.A00(A0g5);
                if (A002 == null) {
                    return null;
                }
                C27661Io A02 = A002.A02();
                A02.A0I.A02(A02.A0L).A01(new AnonymousClass6EA(A0g4, false));
                return null;
            case 775692624:
                str = "wa.action.phoenix.FcsReturnResult";
                break;
            default:
                return null;
        }
        if (!str3.equals(str)) {
            return null;
        }
        List list3 = r11.A00;
        Map map2 = (Map) list3.get(0);
        C1093751l A01 = C94724cR.A01(C117315Zl.A0I(list3));
        C1093751l A012 = C94724cR.A01(list3.get(2));
        C1093751l A013 = C94724cR.A01(list3.get(3));
        HashMap A11 = C12970iu.A11();
        if (A01 != null) {
            r1 = new AbstractC28681Oo() { // from class: X.67G
                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return AbstractC14200l1.this;
                }
            };
        } else {
            r1 = null;
        }
        A11.put("onStartLoading", r1);
        if (A012 != null) {
            r14 = new AbstractC28681Oo() { // from class: X.67G
                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return AbstractC14200l1.this;
                }
            };
        } else {
            r14 = null;
        }
        A11.put("onLoadingCompletion", r14);
        if (A013 != null) {
            r15 = new AbstractC28681Oo() { // from class: X.67G
                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return AbstractC14200l1.this;
                }
            };
        } else {
            r15 = null;
        }
        A11.put("onLoadingFailure", r15);
        C17870rX r4 = this.A00;
        Activity A003 = AnonymousClass1AI.A00(r132);
        C16700pc.A0E(A003, 0);
        C19750uc r17 = r4.A01;
        if (map2 == null || (obj = map2.get("job_id")) == null) {
            throw C12980iv.A0n("null cannot be cast to non-null type kotlin.String");
        }
        AnonymousClass17Q A004 = r17.A00((String) obj);
        if (A004 == null) {
            return null;
        }
        if (A003 instanceof ActivityC000800j) {
            AnonymousClass01F A0V = ((ActivityC000900k) A003).A0V();
            C16700pc.A0B(A0V);
            BkFcsPreloadingScreenFragment A005 = r4.A00(A0V);
            if (A005 != null) {
                A005.A01 = A11;
            }
        }
        C27661Io A022 = A004.A02();
        AnonymousClass4VP r18 = A022.A03;
        if (!(r18 instanceof AnonymousClass46T) || (str2 = ((AnonymousClass46T) r18).A03) == null || (AGE = A022.A0F.AGE(str2)) == null || !(AGE instanceof AbstractC19730ua)) {
            Log.e("WaBkPhoenixInterpreterExtImpl: current resource is not a Bloks resource");
            return null;
        }
        AGE.A03(map2);
        return null;
    }
}
