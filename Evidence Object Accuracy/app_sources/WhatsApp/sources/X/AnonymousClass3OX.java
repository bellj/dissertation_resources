package X;

import android.view.KeyEvent;
import android.widget.TextView;

/* renamed from: X.3OX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OX implements TextView.OnEditorActionListener {
    public final /* synthetic */ C14260l7 A00;
    public final /* synthetic */ C57762na A01;
    public final /* synthetic */ AnonymousClass28D A02;
    public final /* synthetic */ AbstractC14200l1 A03;

    public AnonymousClass3OX(C14260l7 r1, C57762na r2, AnonymousClass28D r3, AbstractC14200l1 r4) {
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // android.widget.TextView.OnEditorActionListener
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        AbstractC14200l1 r5 = this.A03;
        if (r5 == null) {
            return false;
        }
        C14210l2 r0 = new C14210l2();
        AnonymousClass28D r2 = this.A02;
        r0.A05(r2, 0);
        C14260l7 r1 = this.A00;
        C28701Oq.A01(r1, r2, C14210l2.A01(r0, r1, 1), r5);
        return true;
    }
}
