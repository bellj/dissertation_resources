package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1B3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1B3 {
    public Long A00;
    public final AnonymousClass1B0 A01;
    public final C14830m7 A02;

    public AnonymousClass1B3(AnonymousClass1B0 r1, C14830m7 r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    public C48122Ek A00() {
        Double d;
        Double d2;
        try {
            AnonymousClass1B0 r3 = this.A01;
            String string = r3.A02.A00().getString("current_search_location", "");
            if (TextUtils.isEmpty(string)) {
                return null;
            }
            String A00 = C48162Eo.A00(r3.A01, r3.A00, string);
            if (TextUtils.isEmpty(A00)) {
                return null;
            }
            try {
                JSONObject jSONObject = new JSONObject(A00);
                Double valueOf = Double.valueOf(jSONObject.optDouble("radius"));
                Double valueOf2 = Double.valueOf(jSONObject.getDouble("latitude"));
                Double valueOf3 = Double.valueOf(jSONObject.getDouble("longitude"));
                double optDouble = jSONObject.optDouble("imprecise_latitude", -1.0d);
                double optDouble2 = jSONObject.optDouble("imprecise_longitude", -1.0d);
                String string2 = jSONObject.getString("location_description");
                String string3 = jSONObject.getString("provider");
                Double valueOf4 = Double.valueOf(jSONObject.optDouble("accuracy"));
                if (optDouble == -1.0d) {
                    d = null;
                } else {
                    d = Double.valueOf(optDouble);
                }
                if (optDouble2 == -1.0d) {
                    d2 = null;
                } else {
                    d2 = Double.valueOf(optDouble2);
                }
                return new C48122Ek(valueOf, valueOf2, valueOf3, d, d2, valueOf4, string2, string3);
            } catch (JSONException unused) {
                Log.i("SearchLocation/fromJsonString Invalid search location string");
                return null;
            }
        } catch (Exception e) {
            Log.e("SearchLocationRepository/readSearchLocation: Failed to fetch the search location", e);
            return null;
        }
    }

    public Integer A01() {
        C48122Ek A00 = A00();
        if (A00 != null) {
            return Integer.valueOf(A00.A01());
        }
        return null;
    }
}
