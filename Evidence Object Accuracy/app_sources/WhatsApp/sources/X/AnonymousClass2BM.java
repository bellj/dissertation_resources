package X;

import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.Jid;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.2BM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BM extends AnonymousClass2BN {
    public final C14900mE A00;
    public final C15550nR A01;
    public final C14830m7 A02;
    public final AnonymousClass13M A03;
    public final C15580nU A04;
    public final WeakReference A05;

    public AnonymousClass2BM(C14900mE r13, C15550nR r14, C14830m7 r15, AnonymousClass1BB r16, AnonymousClass13M r17, C20000v3 r18, C20050v8 r19, C15660nh r20, C242114q r21, C15370n3 r22, GroupChatInfo groupChatInfo, C22710zW r24, C17070qD r25) {
        super(r13, groupChatInfo, r16, r18, r19, r20, r21, r22, r24, r25);
        this.A02 = r15;
        this.A00 = r13;
        this.A05 = new WeakReference(groupChatInfo);
        this.A01 = r14;
        Jid A0B = r22.A0B(C15580nU.class);
        AnonymousClass009.A05(A0B);
        this.A04 = (C15580nU) A0B;
        this.A03 = r17;
    }

    @Override // X.AnonymousClass2BN
    public Void A08(Void... voidArr) {
        super.A08(voidArr);
        if (((AbstractC16350or) this).A02.isCancelled()) {
            return null;
        }
        List<C28581Od> A00 = this.A03.A00(this.A04, this.A02.A00() / 1000);
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (C28581Od r2 : A00) {
            AbstractC14640lm r1 = r2.A0z.A00;
            if (r1 != null && !hashSet.contains(r1)) {
                hashSet.add(r1);
                C15370n3 A0A = this.A01.A0A(r1);
                if (A0A != null) {
                    arrayList.add(new C90274Ng(A0A, r2));
                }
            }
        }
        this.A00.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(this, 6, arrayList));
        return null;
    }
}
