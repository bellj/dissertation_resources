package X;

/* renamed from: X.0Oi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05120Oi {
    public boolean A00;
    public boolean A01;
    public final AnonymousClass0PP A02;
    public final boolean[] A03;
    public final /* synthetic */ C08860by A04;

    public /* synthetic */ C05120Oi(AnonymousClass0PP r2, C08860by r3) {
        boolean[] zArr;
        this.A04 = r3;
        this.A02 = r2;
        if (r2.A02) {
            zArr = null;
        } else {
            zArr = new boolean[r3.A06];
        }
        this.A03 = zArr;
    }

    public void A00() {
        C08860by.A00(this, this.A04, false);
    }
}
