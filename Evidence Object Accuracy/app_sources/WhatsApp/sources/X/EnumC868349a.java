package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC868349a extends Enum {
    public static final /* synthetic */ EnumC868349a[] A00;

    static {
        EnumC868349a A002 = A00("FLEX", 0);
        EnumC868349a A003 = A00("FLEX_GROW", 1);
        EnumC868349a A004 = A00("FLEX_SHRINK", 2);
        EnumC868349a A005 = A00("FLEX_BASIS", 3);
        EnumC868349a A006 = A00("FLEX_BASIS_PERCENT", 4);
        EnumC868349a A007 = A00("FLEX_BASIS_AUTO", 5);
        EnumC868349a A008 = A00("WIDTH", 6);
        EnumC868349a A009 = A00("WIDTH_PERCENT", 7);
        EnumC868349a A0010 = A00("WIDTH_AUTO", 8);
        EnumC868349a A0011 = A00("MIN_WIDTH", 9);
        EnumC868349a A0012 = A00("MIN_WIDTH_PERCENT", 10);
        EnumC868349a A0013 = A00("MAX_WIDTH", 11);
        EnumC868349a A0014 = A00("MAX_WIDTH_PERCENT", 12);
        EnumC868349a A0015 = A00("HEIGHT", 13);
        EnumC868349a A0016 = A00("HEIGHT_PERCENT", 14);
        EnumC868349a A0017 = A00("HEIGHT_AUTO", 15);
        EnumC868349a A0018 = A00("MIN_HEIGHT", 16);
        EnumC868349a A0019 = A00("MIN_HEIGHT_PERCENT", 17);
        EnumC868349a A0020 = A00("MAX_HEIGHT", 18);
        EnumC868349a A0021 = A00("MAX_HEIGHT_PERCENT", 19);
        EnumC868349a A0022 = A00("ALIGN_SELF", 20);
        EnumC868349a A0023 = A00("POSITION_TYPE", 21);
        EnumC868349a A0024 = A00("ASPECT_RATIO", 22);
        EnumC868349a A0025 = A00("DISPLAY", 23);
        EnumC868349a A0026 = A00("MARGIN", 24);
        EnumC868349a A0027 = A00("MARGIN_PERCENT", 25);
        EnumC868349a A0028 = A00("MARGIN_AUTO", 26);
        EnumC868349a A0029 = A00("POSITION", 27);
        EnumC868349a A0030 = A00("POSITION_PERCENT", 28);
        EnumC868349a A0031 = A00("HAS_MEASURE_FUNCTION", 29);
        EnumC868349a A0032 = A00("HAS_BASELINE_FUNCTION", 30);
        EnumC868349a A0033 = A00("ENABLE_TEXT_ROUNDING", 31);
        EnumC868349a[] r3 = new EnumC868349a[32];
        C72453ed.A1F(A002, A003, A004, A005, r3);
        r3[4] = A006;
        C12970iu.A1R(A007, A008, A009, A0010, r3);
        C72453ed.A1G(A0011, A0012, A0013, A0014, r3);
        C72453ed.A1H(A0015, A0016, A0017, A0018, r3);
        C72453ed.A1I(A0019, A0020, A0021, r3);
        C12960it.A1G(A0022, A0023, A0024, A0025, r3);
        r3[24] = A0026;
        C12960it.A1H(A0027, A0028, A0029, A0030, r3);
        C12970iu.A1S(A0031, A0032, A0033, r3);
        A00 = r3;
    }

    public EnumC868349a(String str, int i) {
    }

    public static EnumC868349a A00(String str, int i) {
        return new EnumC868349a(str, i);
    }

    public static EnumC868349a[] values() {
        return (EnumC868349a[]) A00.clone();
    }
}
