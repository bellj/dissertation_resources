package X;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import com.facebook.redex.ViewOnClickCListenerShape1S0400000_I1;
import com.whatsapp.R;
import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;

/* renamed from: X.3fJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class DialogC72873fJ extends Dialog {
    public final SettingsGoogleDriveViewModel A00;

    public DialogC72873fJ(Context context, SettingsGoogleDriveViewModel settingsGoogleDriveViewModel) {
        super(context);
        this.A00 = settingsGoogleDriveViewModel;
        setCancelable(false);
        setContentView(R.layout.backup_export_progress_dialog);
        View findViewById = findViewById(R.id.cancel_backup_export);
        findViewById.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(findViewById, findViewById(R.id.backup_export_info), findViewById(R.id.backup_export_progress), settingsGoogleDriveViewModel, 1));
    }
}
