package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3Vb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68353Vb implements AnonymousClass5TW {
    public final C28801Pb A00;

    public C68353Vb(View view, AnonymousClass130 r4, C15610nY r5, AnonymousClass12F r6) {
        r4.A05(C12970iu.A0L(view, R.id.contactpicker_row_photo), R.drawable.avatar_contact);
        C28801Pb r0 = new C28801Pb(view, r5, r6, (int) R.id.contactpicker_row_name);
        this.A00 = r0;
        C27531Hw.A06(r0.A01);
    }

    @Override // X.AnonymousClass5TW
    public void ANG(AnonymousClass5TX r3) {
        this.A00.A08(((AnonymousClass542) r3).A00);
    }
}
