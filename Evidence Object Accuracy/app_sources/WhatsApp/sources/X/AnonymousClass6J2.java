package X;

import android.graphics.Point;

/* renamed from: X.6J2  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6J2 implements Runnable {
    public final /* synthetic */ EnumC124565pk A00;
    public final /* synthetic */ C130185yw A01;
    public final /* synthetic */ float[] A02;

    public AnonymousClass6J2(EnumC124565pk r1, C130185yw r2, float[] fArr) {
        this.A01 = r2;
        this.A02 = fArr;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC136156Lf r5 = this.A01.A02;
        if (r5 != null) {
            float[] fArr = this.A02;
            EnumC124565pk r3 = this.A00;
            Point point = null;
            if (fArr != null) {
                point = new Point((int) fArr[0], (int) fArr[1]);
            }
            r5.AQe(point, r3);
        }
    }
}
