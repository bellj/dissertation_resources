package X;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.11S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11S {
    public final AnonymousClass1GP A00 = new AnonymousClass1GP(this);
    public final Set A01 = Collections.newSetFromMap(new ConcurrentHashMap());
}
