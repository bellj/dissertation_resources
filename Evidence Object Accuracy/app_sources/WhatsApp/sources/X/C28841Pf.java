package X;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;

/* renamed from: X.1Pf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28841Pf extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ TextView A00;
    public final /* synthetic */ RunnableBRunnable0Shape0S0400000_I0 A01;
    public final /* synthetic */ String A02;

    public C28841Pf(TextView textView, RunnableBRunnable0Shape0S0400000_I0 runnableBRunnable0Shape0S0400000_I0, String str) {
        this.A01 = runnableBRunnable0Shape0S0400000_I0;
        this.A00 = textView;
        this.A02 = str;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        TextView textView = this.A00;
        ViewGroup.LayoutParams layoutParams = textView.getLayoutParams();
        layoutParams.width = -2;
        textView.setLayoutParams(layoutParams);
        textView.setText(this.A02);
        textView.clearAnimation();
        ((AnonymousClass1PZ) this.A01.A01).A0G = true;
    }
}
