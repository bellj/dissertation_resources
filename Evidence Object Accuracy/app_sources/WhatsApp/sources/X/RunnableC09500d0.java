package X;

import android.util.Log;
import java.lang.reflect.Method;

/* renamed from: X.0d0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09500d0 implements Runnable {
    public final /* synthetic */ Object A00;
    public final /* synthetic */ Object A01;

    public RunnableC09500d0(Object obj, Object obj2) {
        this.A00 = obj;
        this.A01 = obj2;
    }

    @Override // java.lang.Runnable
    public void run() {
        Object obj;
        Object[] objArr;
        try {
            Method method = AnonymousClass0RV.A04;
            if (method != null) {
                obj = this.A00;
                objArr = new Object[]{this.A01, Boolean.FALSE, "AppCompat recreation"};
            } else {
                method = AnonymousClass0RV.A03;
                obj = this.A00;
                objArr = new Object[]{this.A01, Boolean.FALSE};
            }
            method.invoke(obj, objArr);
        } catch (RuntimeException e) {
            if (e.getClass() == RuntimeException.class && e.getMessage() != null && e.getMessage().startsWith("Unable to stop")) {
                throw e;
            }
        } catch (Throwable th) {
            Log.e("ActivityRecreator", "Exception while invoking performStopActivity", th);
        }
    }
}
