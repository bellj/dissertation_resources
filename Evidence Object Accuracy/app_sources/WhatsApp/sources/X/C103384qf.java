package X;

import android.content.Context;
import com.whatsapp.registration.VerifyPhoneNumber;

/* renamed from: X.4qf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103384qf implements AbstractC009204q {
    public final /* synthetic */ VerifyPhoneNumber A00;

    public C103384qf(VerifyPhoneNumber verifyPhoneNumber) {
        this.A00 = verifyPhoneNumber;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
