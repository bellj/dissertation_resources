package X;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/* renamed from: X.5D2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5D2 implements Enumeration {
    public int A00 = 0;
    public final /* synthetic */ AnonymousClass5N6 A01;

    public AnonymousClass5D2(AnonymousClass5N6 r2) {
        this.A01 = r2;
    }

    @Override // java.util.Enumeration
    public Object nextElement() {
        int i = this.A00;
        AnonymousClass5NH[] r1 = this.A01.A01;
        if (i < r1.length) {
            this.A00 = i + 1;
            return r1[i];
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Enumeration
    public boolean hasMoreElements() {
        return C12990iw.A1Y(this.A00, this.A01.A01.length);
    }
}
