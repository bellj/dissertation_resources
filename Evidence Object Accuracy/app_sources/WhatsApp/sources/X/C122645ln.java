package X;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5ln  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122645ln extends AbstractC118835cS {
    public final TextView A00;
    public final TextView A01;
    public final TextView A02;

    public C122645ln(View view) {
        super(view);
        TextView A0I = C12960it.A0I(view, R.id.title);
        this.A02 = A0I;
        this.A01 = C12960it.A0I(view, R.id.subtitle);
        this.A00 = C12960it.A0I(view, R.id.secondSubtitle);
        C27531Hw.A06(A0I);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        C122875mF r32 = (C122875mF) r3;
        this.A02.setText(r32.A02);
        this.A01.setText(r32.A01);
        TextView textView = this.A00;
        CharSequence charSequence = r32.A00;
        textView.setText(charSequence);
        textView.setVisibility(C13010iy.A00(TextUtils.isEmpty(charSequence) ? 1 : 0));
    }
}
