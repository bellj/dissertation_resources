package X;

/* renamed from: X.0Jb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03770Jb {
    BEGIN_ARRAY,
    END_ARRAY,
    BEGIN_OBJECT,
    END_OBJECT,
    NAME,
    STRING,
    NUMBER,
    BOOLEAN,
    NULL,
    END_DOCUMENT
}
