package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.appcompat.widget.Toolbar;
import java.util.ArrayList;

/* renamed from: X.0XN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XN implements AbstractC12690iL {
    public AnonymousClass07H A00;
    public C07340Xp A01;
    public final /* synthetic */ Toolbar A02;

    @Override // X.AbstractC12690iL
    public boolean AA1() {
        return false;
    }

    @Override // X.AbstractC12690iL
    public void AOF(AnonymousClass07H r1, boolean z) {
    }

    @Override // X.AbstractC12690iL
    public boolean AWs(AnonymousClass0CK r2) {
        return false;
    }

    @Override // X.AbstractC12690iL
    public void Abr(AbstractC12280hf r1) {
    }

    public AnonymousClass0XN(Toolbar toolbar) {
        this.A02 = toolbar;
    }

    @Override // X.AbstractC12690iL
    public boolean A7P(AnonymousClass07H r6, C07340Xp r7) {
        Toolbar toolbar = this.A02;
        View view = toolbar.A0G;
        if (view instanceof AnonymousClass07D) {
            ((AnonymousClass07D) view).onActionViewCollapsed();
        }
        toolbar.removeView(toolbar.A0G);
        toolbar.removeView(toolbar.A0H);
        toolbar.A0G = null;
        ArrayList arrayList = toolbar.A0b;
        int size = arrayList.size();
        while (true) {
            size--;
            if (size >= 0) {
                toolbar.addView((View) arrayList.get(size));
            } else {
                arrayList.clear();
                this.A01 = null;
                toolbar.requestLayout();
                r7.A0N = false;
                r7.A0E.A0E(false);
                return true;
            }
        }
    }

    @Override // X.AbstractC12690iL
    public boolean A9n(AnonymousClass07H r6, C07340Xp r7) {
        Toolbar toolbar = this.A02;
        toolbar.A06();
        ViewParent parent = toolbar.A0H.getParent();
        if (parent != toolbar) {
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(toolbar.A0H);
            }
            toolbar.addView(toolbar.A0H);
        }
        View actionView = r7.getActionView();
        toolbar.A0G = actionView;
        this.A01 = r7;
        ViewParent parent2 = actionView.getParent();
        if (parent2 != toolbar) {
            if (parent2 instanceof ViewGroup) {
                ((ViewGroup) parent2).removeView(toolbar.A0G);
            }
            C017908j r2 = new C017908j();
            ((C009604u) r2).A00 = 8388611 | (toolbar.A00 & 112);
            r2.A00 = 2;
            toolbar.A0G.setLayoutParams(r2);
            toolbar.addView(toolbar.A0G);
        }
        int childCount = toolbar.getChildCount();
        while (true) {
            childCount--;
            if (childCount < 0) {
                break;
            }
            View childAt = toolbar.getChildAt(childCount);
            if (!(((C017908j) childAt.getLayoutParams()).A00 == 2 || childAt == toolbar.A0O)) {
                toolbar.removeViewAt(childCount);
                toolbar.A0b.add(childAt);
            }
        }
        toolbar.requestLayout();
        r7.A0N = true;
        r7.A0E.A0E(false);
        View view = toolbar.A0G;
        if (view instanceof AnonymousClass07D) {
            ((AnonymousClass07D) view).onActionViewExpanded();
        }
        return true;
    }

    @Override // X.AbstractC12690iL
    public void AIn(Context context, AnonymousClass07H r4) {
        C07340Xp r0;
        AnonymousClass07H r1 = this.A00;
        if (!(r1 == null || (r0 = this.A01) == null)) {
            r1.A0L(r0);
        }
        this.A00 = r4;
    }

    @Override // X.AbstractC12690iL
    public void AfQ(boolean z) {
        C07340Xp r4 = this.A01;
        if (r4 != null) {
            AnonymousClass07H r3 = this.A00;
            if (r3 != null) {
                int size = r3.size();
                for (int i = 0; i < size; i++) {
                    if (r3.getItem(i) == r4) {
                        return;
                    }
                }
            }
            A7P(r3, r4);
        }
    }
}
