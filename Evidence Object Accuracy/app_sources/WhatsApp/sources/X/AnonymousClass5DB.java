package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5DB  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DB implements Iterator {
    public int A00 = 0;
    public final Object[] A01;

    public AnonymousClass5DB(Object[] objArr) {
        this.A01 = objArr;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return C12990iw.A1Y(this.A00, this.A01.length);
    }

    @Override // java.util.Iterator
    public Object next() {
        int i = this.A00;
        Object[] objArr = this.A01;
        if (i != objArr.length) {
            this.A00 = i + 1;
            return objArr[i];
        }
        throw new NoSuchElementException(C12960it.A0W(i, "Out of elements: "));
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12980iv.A0u("Cannot remove element from an Array.");
    }
}
