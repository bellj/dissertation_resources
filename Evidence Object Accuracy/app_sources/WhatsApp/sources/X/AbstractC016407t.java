package X;

import android.view.View;

/* renamed from: X.07t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC016407t extends AbstractC016507u {
    void ASw(View view, int[] iArr, int i, int i2, int i3);

    void ASx(View view, int i, int i2, int i3, int i4, int i5);

    void ASz(View view, View view2, int i, int i2);

    boolean AWL(View view, View view2, int i, int i2);

    void AWp(View view, int i);
}
