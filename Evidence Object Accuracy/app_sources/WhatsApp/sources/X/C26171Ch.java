package X;

/* renamed from: X.1Ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26171Ch {
    public C14850m9 A00;

    public C26171Ch(C14850m9 r1) {
        this.A00 = r1;
    }

    public boolean A00(AbstractC15340mz r3) {
        C32361c2 r1 = r3.A0N;
        if (r1 == null || !r1.A0A || r1.A07 == null) {
            return false;
        }
        C14850m9 r12 = this.A00;
        return r12.A07(1307) || r12.A07(1325);
    }

    public boolean A01(AbstractC15340mz r3) {
        C32361c2 r0 = r3.A0N;
        return r0 != null && r0.A0B && this.A00.A07(1307);
    }
}
