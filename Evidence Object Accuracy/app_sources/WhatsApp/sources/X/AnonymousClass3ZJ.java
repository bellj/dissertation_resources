package X;

import java.util.List;

/* renamed from: X.3ZJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZJ implements AbstractC21730xt {
    public final C18640sm A00;
    public final C17220qS A01;
    public final AnonymousClass4OY A02;
    public final List A03;

    public AnonymousClass3ZJ(C18640sm r1, C17220qS r2, AnonymousClass4OY r3, List list) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = list;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A02.A00.A0A.A01();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        Long A00;
        int A002 = C41151sz.A00(r6);
        if (A002 > 0) {
            AnonymousClass4OY r2 = this.A02;
            C23000zz r3 = r2.A00;
            C32881ct r1 = r3.A0A;
            if (A002 != 500 || (A00 = r1.A00()) == null) {
                r1.A01();
            } else {
                r3.A02(r2.A01, A00.longValue());
            }
        } else {
            this.A02.A00.A0A.A01();
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r3, String str) {
        C23000zz r1 = this.A02.A00;
        r1.A00 = null;
        r1.A0A.A01();
    }
}
