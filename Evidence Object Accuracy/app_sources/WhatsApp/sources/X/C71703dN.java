package X;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3dN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71703dN<K, V> extends AbstractCollection<V> {
    public final Map map;

    public C71703dN(Map map) {
        this.map = map;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public void clear() {
        map().clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return map().containsValue(obj);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean isEmpty() {
        return map().isEmpty();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return C28271Mk.valueIterator(C12960it.A0n(map()));
    }

    public final Map map() {
        return this.map;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean remove(Object obj) {
        try {
            return super.remove(obj);
        } catch (UnsupportedOperationException unused) {
            Iterator A0n = C12960it.A0n(map());
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                if (AnonymousClass28V.A00(obj, A15.getValue())) {
                    map().remove(A15.getKey());
                    return true;
                }
            }
            return false;
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean removeAll(Collection collection) {
        try {
            return super.removeAll(collection);
        } catch (UnsupportedOperationException unused) {
            HashSet newHashSet = C28281Ml.newHashSet();
            Iterator A0n = C12960it.A0n(map());
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                if (collection.contains(A15.getValue())) {
                    newHashSet.add(A15.getKey());
                }
            }
            return map().keySet().removeAll(newHashSet);
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean retainAll(Collection collection) {
        try {
            return super.retainAll(collection);
        } catch (UnsupportedOperationException unused) {
            HashSet newHashSet = C28281Ml.newHashSet();
            Iterator A0n = C12960it.A0n(map());
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                if (collection.contains(A15.getValue())) {
                    newHashSet.add(A15.getKey());
                }
            }
            return map().keySet().retainAll(newHashSet);
        }
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        return map().size();
    }
}
