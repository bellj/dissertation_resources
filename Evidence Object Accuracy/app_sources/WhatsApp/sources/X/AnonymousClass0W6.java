package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.0W6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0W6 implements View.OnAttachStateChangeListener {
    public final /* synthetic */ AnonymousClass0CN A00;

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewAttachedToWindow(View view) {
    }

    public AnonymousClass0W6(AnonymousClass0CN r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnAttachStateChangeListener
    public void onViewDetachedFromWindow(View view) {
        AnonymousClass0CN r2 = this.A00;
        ViewTreeObserver viewTreeObserver = r2.A04;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                r2.A04 = view.getViewTreeObserver();
            }
            r2.A04.removeGlobalOnLayoutListener(r2.A0F);
        }
        view.removeOnAttachStateChangeListener(this);
    }
}
