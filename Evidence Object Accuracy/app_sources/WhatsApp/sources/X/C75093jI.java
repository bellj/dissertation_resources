package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.biz.catalog.view.CarouselScrollbarView;

/* renamed from: X.3jI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75093jI extends AbstractC05270Ox {
    public final /* synthetic */ CarouselScrollbarView A00;

    public C75093jI(CarouselScrollbarView carouselScrollbarView) {
        this.A00 = carouselScrollbarView;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        this.A00.A00();
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        this.A00.A00();
    }
}
