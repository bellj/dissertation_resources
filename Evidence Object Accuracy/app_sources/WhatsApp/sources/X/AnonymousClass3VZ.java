package X;

import android.view.View;
import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;

/* renamed from: X.3VZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3VZ implements AbstractC116525Vu {
    public final /* synthetic */ CatalogSearchFragment A00;

    public AnonymousClass3VZ(CatalogSearchFragment catalogSearchFragment) {
        this.A00 = catalogSearchFragment;
    }

    @Override // X.AbstractC116525Vu
    public void ARn(C44691zO r3, long j) {
        CatalogSearchFragment catalogSearchFragment = this.A00;
        View A05 = catalogSearchFragment.A05();
        AnonymousClass018 r0 = catalogSearchFragment.A0O;
        if (r0 != null) {
            C12960it.A0w(A05, r0, j);
            return;
        }
        throw C16700pc.A06("whatsAppLocale");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0060, code lost:
        if (r2 < 0) goto L_0x0062;
     */
    @Override // X.AbstractC116525Vu
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AUV(X.C44691zO r19, java.lang.String r20, java.lang.String r21, int r22, long r23) {
        /*
            r18 = this;
            r0 = 1
            r12 = r19
            X.C16700pc.A0E(r12, r0)
            r0 = r18
            com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment r1 = r0.A00
            X.0pd r0 = r1.A0X
            java.lang.Object r4 = r0.getValue()
            X.2fT r4 = (X.AnonymousClass2fT) r4
            com.whatsapp.jid.UserJid r6 = r1.A0P
            if (r6 != 0) goto L_0x001d
            java.lang.String r0 = "bizJid"
            java.lang.RuntimeException r0 = X.C16700pc.A06(r0)
            throw r0
        L_0x001d:
            int r3 = r1.A00
            java.lang.String r10 = r12.A0D
            X.C16700pc.A0B(r10)
            X.3EX r11 = r4.A04
            X.016 r0 = r11.A00
            r5 = 0
            r16 = r23
            if (r0 == 0) goto L_0x008b
            java.lang.Object r0 = r0.A01()
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            if (r0 == 0) goto L_0x008b
            java.util.Iterator r2 = r0.iterator()
        L_0x0039:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0051
            java.lang.Object r1 = r2.next()
            r0 = r1
            X.4Wm r0 = (X.C92584Wm) r0
            X.1zO r0 = r0.A01
            java.lang.String r0 = r0.A0D
            boolean r0 = X.C16700pc.A0O(r0, r10)
            if (r0 == 0) goto L_0x0039
            r5 = r1
        L_0x0051:
            X.4Wm r5 = (X.C92584Wm) r5
            if (r5 == 0) goto L_0x008b
            long r0 = r5.A00
            int r2 = (r0 > r23 ? 1 : (r0 == r23 ? 0 : -1))
            X.1CR r5 = r4.A03
            java.lang.Integer r9 = java.lang.Integer.valueOf(r22)
            r0 = 5
            if (r2 >= 0) goto L_0x0063
        L_0x0062:
            r0 = 4
        L_0x0063:
            java.lang.Integer r7 = java.lang.Integer.valueOf(r0)
            r1 = 2
            r0 = 1
            if (r3 == 0) goto L_0x0089
            if (r3 == r0) goto L_0x0087
            if (r3 == r1) goto L_0x0070
            r1 = -1
        L_0x0070:
            java.lang.Integer r8 = java.lang.Integer.valueOf(r1)
            X.1CQ r0 = r5.A01
            X.3VK r4 = new X.3VK
            r4.<init>(r5, r6, r7, r8, r9, r10)
            r0.A00(r4, r6)
            r14 = r20
            r15 = r21
            r13 = r6
            r11.A01(r12, r13, r14, r15, r16)
            return
        L_0x0087:
            r1 = 3
            goto L_0x0070
        L_0x0089:
            r1 = 1
            goto L_0x0070
        L_0x008b:
            X.1CR r5 = r4.A03
            java.lang.Integer r9 = java.lang.Integer.valueOf(r22)
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3VZ.AUV(X.1zO, java.lang.String, java.lang.String, int, long):void");
    }
}
