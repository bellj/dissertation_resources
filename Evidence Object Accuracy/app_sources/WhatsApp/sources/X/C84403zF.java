package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.search.SearchFragment;

/* renamed from: X.3zF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84403zF extends AnonymousClass2Cu {
    public final /* synthetic */ SearchFragment A00;

    public C84403zF(SearchFragment searchFragment) {
        this.A00 = searchFragment;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        SearchFragment.A01(userJid, this.A00);
    }
}
