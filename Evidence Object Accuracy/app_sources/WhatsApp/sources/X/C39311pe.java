package X;

import android.content.SharedPreferences;
import java.io.File;

/* renamed from: X.1pe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39311pe {
    public static File A00;

    public static synchronized File A00(C14330lG r9, C16630pM r10, C14370lK r11, String str, int i) {
        File file;
        synchronized (C39311pe.class) {
            A00 = C22200yh.A0G(r9, r10, r11, str, 0, i);
            SharedPreferences.Editor edit = r10.A01(AnonymousClass01V.A07).edit();
            edit.putString("external_file_image", A00.getAbsolutePath());
            edit.apply();
            file = A00;
        }
        return file;
    }

    public static synchronized File A01(C16630pM r4) {
        File file;
        String string;
        synchronized (C39311pe.class) {
            if (A00 == null && (string = r4.A01(AnonymousClass01V.A07).getString("external_file_image", null)) != null) {
                A00 = new File(string);
            }
            file = A00;
        }
        return file;
    }
}
