package X;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.3Fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC64383Fi {
    public boolean A00 = false;
    public final Resources A01;
    public final Paint A02;
    public final RectF A03;
    public final Handler A04;
    public final Vibrator A05;
    public final View A06;
    public final ViewGroup A07;
    public final Runnable A08;
    public final Runnable A09;

    public AbstractC64383Fi(RectF rectF, Handler handler, Vibrator vibrator, ViewGroup viewGroup) {
        int i;
        this.A04 = handler;
        this.A07 = viewGroup;
        this.A03 = rectF;
        this.A05 = vibrator;
        this.A01 = viewGroup.getResources();
        Paint A0F = C12990iw.A0F();
        Resources resources = this.A01;
        if (!(this instanceof AnonymousClass338)) {
            i = R.color.status_grid_center;
        } else {
            i = R.color.status_grid_rotation;
        }
        A0F.setColor(resources.getColor(i));
        A0F.setStrokeWidth((float) resources.getDimensionPixelSize(R.dimen.media_guideline_stroke_width));
        C12990iw.A13(A0F);
        this.A02 = A0F;
        C73703gf r4 = new C73703gf(viewGroup.getContext(), this);
        r4.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        r4.setVisibility(4);
        this.A06 = r4;
        viewGroup.addView(r4, 0);
        this.A09 = new RunnableC55552im(r4, 0.0f, 1.0f, 0);
        this.A08 = new RunnableC55552im(r4, 1.0f, 0.0f, 4);
    }

    public Path A00() {
        int i;
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        if (!(this instanceof AnonymousClass338)) {
            return ((AnonymousClass337) this).A01;
        }
        AnonymousClass338 r5 = (AnonymousClass338) this;
        PointF pointF = r5.A02;
        if (pointF == null || (i = r5.A01) == -1) {
            return null;
        }
        if (i == 0) {
            RectF rectF = ((AbstractC64383Fi) r5).A03;
            f = rectF.left;
            f2 = pointF.y;
            f3 = rectF.right;
            f4 = f2;
        } else if (i == 1) {
            float f6 = pointF.y - pointF.x;
            RectF rectF2 = ((AbstractC64383Fi) r5).A03;
            f = rectF2.left;
            float f7 = f6 + f;
            f2 = rectF2.top;
            if (f7 >= f2) {
                f2 = f7;
            } else {
                f = f2 - f6;
            }
            f3 = rectF2.right;
            f5 = f6 + f3;
            f4 = rectF2.bottom;
            if (f5 > f4) {
                f3 = f4 - f6;
            }
            f4 = f5;
        } else if (i == 2) {
            f = pointF.x;
            RectF rectF3 = ((AbstractC64383Fi) r5).A03;
            f2 = rectF3.top;
            f4 = rectF3.bottom;
            f3 = f;
        } else if (i != 3) {
            return null;
        } else {
            float f8 = pointF.y + pointF.x;
            RectF rectF4 = ((AbstractC64383Fi) r5).A03;
            f = rectF4.left;
            float f9 = f8 - f;
            f2 = rectF4.bottom;
            if (f9 <= f2) {
                f2 = f9;
            } else {
                f = f8 - f2;
            }
            f3 = rectF4.right;
            f5 = f8 - f3;
            f4 = rectF4.top;
            if (f5 < f4) {
                f3 = f8 - f4;
            }
            f4 = f5;
        }
        Path path = new Path();
        path.rewind();
        path.moveTo(f, f2);
        path.lineTo(f3, f4);
        return path;
    }

    public void A01() {
        Handler handler = this.A04;
        handler.removeCallbacks(this.A09);
        handler.post(this.A08);
        this.A00 = false;
    }

    public void A02() {
        Vibrator vibrator;
        Handler handler = this.A04;
        handler.removeCallbacks(this.A08);
        handler.post(this.A09);
        if (!this.A00 && (vibrator = this.A05) != null) {
            try {
                vibrator.vibrate(3);
            } catch (NullPointerException e) {
                Log.e("Vibrator is broken on this device.", e);
            }
        }
        this.A00 = true;
    }
}
