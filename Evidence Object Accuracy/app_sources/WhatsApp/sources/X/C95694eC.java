package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.4eC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95694eC {
    public static final AnonymousClass4DR A00 = A0A(false);
    public static final AnonymousClass4DR A01 = A0A(true);
    public static final AnonymousClass4DR A02 = new AnonymousClass4DR();
    public static final Class A03;

    static {
        Class<?> cls;
        try {
            cls = Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            cls = null;
        }
        A03 = cls;
    }

    public static int A00(AnonymousClass5XT r2, Object obj, int i) {
        int A04;
        int A05;
        if (obj instanceof C94424bn) {
            C94424bn r3 = (C94424bn) obj;
            A04 = AnonymousClass4UO.A04(i);
            if (r3.A00 != null) {
                A05 = r3.A00.A02();
            } else {
                A05 = r3.A01 != null ? r3.A01.Ah0() : 0;
            }
        } else {
            A04 = AnonymousClass4UO.A04(i);
            A05 = AbstractC108624zO.A05(r2, (AbstractC117125Yn) obj);
        }
        return AnonymousClass4UO.A05(A05, A04);
    }

    public static int A01(AnonymousClass5XT r5, List list, int i) {
        int A05;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int A04 = AnonymousClass4UO.A04(i) * size;
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            if (obj instanceof C94424bn) {
                C94424bn r1 = (C94424bn) obj;
                if (r1.A00 != null) {
                    A05 = r1.A00.A02();
                } else {
                    A05 = r1.A01 != null ? r1.A01.Ah0() : 0;
                }
            } else {
                A05 = AbstractC108624zO.A05(r5, (AbstractC117125Yn) obj);
            }
            A04 = AnonymousClass4UO.A05(A05, A04);
        }
        return A04;
    }

    public static int A02(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79193qC) {
            C79193qC r5 = (C79193qC) list;
            i = 0;
            while (i2 < size) {
                r5.A03(i2);
                i += AbstractC79223qF.A01(r5.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += AbstractC79223qF.A01(C72453ed.A0Z(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A03(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79193qC) {
            C79193qC r5 = (C79193qC) list;
            i = 0;
            while (i2 < size) {
                r5.A03(i2);
                i += AbstractC79223qF.A01(r5.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += AbstractC79223qF.A01(C72453ed.A0Z(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A04(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79193qC) {
            C79193qC r6 = (C79193qC) list;
            i = 0;
            while (i2 < size) {
                r6.A03(i2);
                i += AbstractC79223qF.A01(C72453ed.A0V(r6.A01[i2]));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += AbstractC79223qF.A01(C72453ed.A0Y(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A05(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79183qB) {
            C79183qB r4 = (C79183qB) list;
            i = 0;
            while (i2 < size) {
                r4.A03(i2);
                i += C72453ed.A08(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C72453ed.A08(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A06(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79183qB) {
            C79183qB r4 = (C79183qB) list;
            i = 0;
            while (i2 < size) {
                r4.A03(i2);
                i += C72453ed.A08(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C72453ed.A08(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A07(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79183qB) {
            C79183qB r4 = (C79183qB) list;
            i = 0;
            while (i2 < size) {
                r4.A03(i2);
                i += C72453ed.A07(r4.A01[i2]);
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                i += C72453ed.A07(C72453ed.A0G(list, i2));
                i2++;
            }
        }
        return i;
    }

    public static int A08(List list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof C79183qB) {
            C79183qB r5 = (C79183qB) list;
            i = 0;
            while (i2 < size) {
                r5.A03(i2);
                int i3 = r5.A01[i2];
                i += C72453ed.A07((i3 >> 31) ^ (i3 << 1));
                i2++;
            }
        } else {
            i = 0;
            while (i2 < size) {
                int A0G = C72453ed.A0G(list, i2);
                i += C72453ed.A07((A0G >> 31) ^ (A0G << 1));
                i2++;
            }
        }
        return i;
    }

    public static int A09(List list, int i) {
        int length;
        int length2;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        int A04 = AnonymousClass4UO.A04(i) * size;
        if (list instanceof AnonymousClass5Z3) {
            AnonymousClass5Z3 r5 = (AnonymousClass5Z3) list;
            while (i2 < size) {
                Object AG5 = r5.AG5(i2);
                if (AG5 instanceof AbstractC111915Bh) {
                    length2 = ((AbstractC111915Bh) AG5).A02();
                } else {
                    String str = (String) AG5;
                    try {
                        length2 = C95164dF.A00(str);
                    } catch (AnonymousClass4CM unused) {
                        length2 = str.getBytes(C93094Zb.A03).length;
                    }
                }
                A04 = AnonymousClass4UO.A05(length2, A04);
                i2++;
            }
        } else {
            while (i2 < size) {
                Object obj = list.get(i2);
                if (obj instanceof AbstractC111915Bh) {
                    length = ((AbstractC111915Bh) obj).A02();
                } else {
                    String str2 = (String) obj;
                    try {
                        length = C95164dF.A00(str2);
                    } catch (AnonymousClass4CM unused2) {
                        length = str2.getBytes(C93094Zb.A03).length;
                    }
                }
                A04 = AnonymousClass4UO.A05(length, A04);
                i2++;
            }
        }
        return A04;
    }

    public static AnonymousClass4DR A0A(boolean z) {
        try {
            Class<?> cls = Class.forName("com.google.protobuf.UnknownFieldSetSchema");
            if (cls != null) {
                return (AnonymousClass4DR) cls.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    public static void A0B(AnonymousClass5XT r3, AbstractC115185Qn r4, List list, int i) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            for (int i2 = 0; i2 < list.size(); i2++) {
                r42.A00.A0B((AbstractC117125Yn) list.get(i2), r3, i);
            }
        }
    }

    public static void A0C(AbstractC115185Qn r5, List list, int i) {
        if (list != null && !list.isEmpty()) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (list instanceof AnonymousClass5Z3) {
                AnonymousClass5Z3 r3 = (AnonymousClass5Z3) list;
                while (i2 < list.size()) {
                    Object AG5 = r3.AG5(i2);
                    boolean z = AG5 instanceof String;
                    AbstractC79223qF r0 = r52.A00;
                    if (z) {
                        r0.A07(i, (String) AG5);
                    } else {
                        r0.A0A((AbstractC111915Bh) AG5, i);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                r52.A00.A07(i, C12960it.A0g(list, i2));
                i2++;
            }
        }
    }

    public static void A0D(AbstractC115185Qn r3, List list, int i) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r32 = (C108684zU) r3;
            for (int i2 = 0; i2 < list.size(); i2++) {
                r32.A00.A0A((AbstractC111915Bh) list.get(i2), i);
            }
        }
    }

    public static void A0E(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r3 = r52.A00;
                AnonymousClass4UO.A06(r3, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 8;
                }
                r3.A05(i3);
                while (i2 < list.size()) {
                    r3.A09(Double.doubleToRawLongBits(C72453ed.A00(list.get(i2))));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r32 = r52.A00;
                long doubleToRawLongBits = Double.doubleToRawLongBits(C72453ed.A00(list.get(i2)));
                AnonymousClass4UO.A06(r32, i, 1);
                r32.A09(doubleToRawLongBits);
                i2++;
            }
        }
    }

    public static void A0F(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 4;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A06(Float.floatToRawIntBits(C72453ed.A02(list.get(i2))));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                int floatToRawIntBits = Float.floatToRawIntBits(C72453ed.A02(list.get(i2)));
                AnonymousClass4UO.A06(r2, i, 5);
                r2.A06(floatToRawIntBits);
                i2++;
            }
        }
    }

    public static void A0G(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r53 = r52.A00;
                AnonymousClass4UO.A06(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += AbstractC79223qF.A01(C72453ed.A0Z(list, i4));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    r53.A08(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r3 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4UO.A06(r3, i, 0);
                r3.A08(A0Z);
                i2++;
            }
        }
    }

    public static void A0H(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r53 = r52.A00;
                AnonymousClass4UO.A06(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += AbstractC79223qF.A01(C72453ed.A0Z(list, i4));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    r53.A08(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r3 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4UO.A06(r3, i, 0);
                r3.A08(A0Z);
                i2++;
            }
        }
    }

    public static void A0I(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r53 = r52.A00;
                AnonymousClass4UO.A06(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += AbstractC79223qF.A01(C72453ed.A0Y(list, i4));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    r53.A08(C72453ed.A0Y(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r3 = r52.A00;
                long A0Y = C72453ed.A0Y(list, i2);
                AnonymousClass4UO.A06(r3, i, 0);
                r3.A08(A0Y);
                i2++;
            }
        }
    }

    public static void A0J(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r3 = r52.A00;
                AnonymousClass4UO.A06(r3, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 8;
                }
                r3.A05(i3);
                while (i2 < list.size()) {
                    r3.A09(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r32 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4UO.A06(r32, i, 1);
                r32.A09(A0Z);
                i2++;
            }
        }
    }

    public static void A0K(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r3 = r52.A00;
                AnonymousClass4UO.A06(r3, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 8;
                }
                r3.A05(i3);
                while (i2 < list.size()) {
                    r3.A09(C72453ed.A0Z(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r32 = r52.A00;
                long A0Z = C72453ed.A0Z(list, i2);
                AnonymousClass4UO.A06(r32, i, 1);
                r32.A09(A0Z);
                i2++;
            }
        }
    }

    public static void A0L(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C72453ed.A08(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    int A0G = C72453ed.A0G(list, i2);
                    if (A0G >= 0) {
                        r43.A05(A0G);
                    } else {
                        r43.A08((long) A0G);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                int A0G2 = C72453ed.A0G(list, i2);
                AnonymousClass4UO.A06(r2, i, 0);
                if (A0G2 >= 0) {
                    r2.A05(A0G2);
                } else {
                    r2.A08((long) A0G2);
                }
                i2++;
            }
        }
    }

    public static void A0M(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C72453ed.A07(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A05(C72453ed.A0G(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                int A0G = C72453ed.A0G(list, i2);
                AnonymousClass4UO.A06(r2, i, 0);
                r2.A05(A0G);
                i2++;
            }
        }
    }

    public static void A0N(AbstractC115185Qn r5, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r52 = (C108684zU) r5;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r53 = r52.A00;
                AnonymousClass4UO.A06(r53, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    int A0G = C72453ed.A0G(list, i4);
                    i3 += C72453ed.A07((A0G >> 31) ^ (A0G << 1));
                }
                r53.A05(i3);
                while (i2 < list.size()) {
                    int A0G2 = C72453ed.A0G(list, i2);
                    r53.A05((A0G2 >> 31) ^ (A0G2 << 1));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r52.A00;
                int A0G3 = C72453ed.A0G(list, i2);
                AnonymousClass4UO.A06(r2, i, 0);
                r2.A05((A0G3 >> 31) ^ (A0G3 << 1));
                i2++;
            }
        }
    }

    public static void A0O(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 4;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A06(C72453ed.A0G(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                int A0G = C72453ed.A0G(list, i2);
                AnonymousClass4UO.A06(r2, i, 5);
                r2.A06(A0G);
                i2++;
            }
        }
    }

    public static void A0P(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3 += 4;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A06(C72453ed.A0G(list, i2));
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                int A0G = C72453ed.A0G(list, i2);
                AnonymousClass4UO.A06(r2, i, 5);
                r2.A06(A0G);
                i2++;
            }
        }
    }

    public static void A0Q(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    i3 += C72453ed.A08(C72453ed.A0G(list, i4));
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    int A0G = C72453ed.A0G(list, i2);
                    if (A0G >= 0) {
                        r43.A05(A0G);
                    } else {
                        r43.A08((long) A0G);
                    }
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                int A0G2 = C72453ed.A0G(list, i2);
                AnonymousClass4UO.A06(r2, i, 0);
                if (A0G2 >= 0) {
                    r2.A05(A0G2);
                } else {
                    r2.A08((long) A0G2);
                }
                i2++;
            }
        }
    }

    public static void A0R(AbstractC115185Qn r4, List list, int i, boolean z) {
        if (!(list == null || list.isEmpty())) {
            C108684zU r42 = (C108684zU) r4;
            int i2 = 0;
            if (z) {
                AbstractC79223qF r43 = r42.A00;
                AnonymousClass4UO.A06(r43, i, 2);
                int i3 = 0;
                for (int i4 = 0; i4 < list.size(); i4++) {
                    list.get(i4);
                    i3++;
                }
                r43.A05(i3);
                while (i2 < list.size()) {
                    r43.A04(C12970iu.A1Y(list.get(i2)) ? (byte) 1 : 0);
                    i2++;
                }
                return;
            }
            while (i2 < list.size()) {
                AbstractC79223qF r2 = r42.A00;
                boolean A1Y = C12970iu.A1Y(list.get(i2));
                AnonymousClass4UO.A06(r2, i, 0);
                r2.A04(A1Y ? (byte) 1 : 0);
                i2++;
            }
        }
    }

    public static void A0S(Object obj, Object obj2) {
        AbstractC79123q5 r9 = (AbstractC79123q5) obj;
        C94984cr r7 = r9.zzjp;
        C94984cr r8 = ((AbstractC79123q5) obj2).zzjp;
        if (!r8.equals(C94984cr.A05)) {
            int i = r7.A00 + r8.A00;
            int[] copyOf = Arrays.copyOf(r7.A03, i);
            System.arraycopy(r8.A03, 0, copyOf, r7.A00, r8.A00);
            Object[] copyOf2 = Arrays.copyOf(r7.A04, i);
            System.arraycopy(r8.A04, 0, copyOf2, r7.A00, r8.A00);
            r7 = new C94984cr(copyOf, copyOf2, i, true);
        }
        r9.zzjp = r7;
    }
}
