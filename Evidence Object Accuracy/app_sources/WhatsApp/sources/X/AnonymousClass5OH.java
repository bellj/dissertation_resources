package X;

/* renamed from: X.5OH  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OH extends AnonymousClass5GY {
    public AnonymousClass5OH() {
    }

    public AnonymousClass5OH(AnonymousClass5OH r1) {
        super(r1);
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5OH(this);
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "SHA-512";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 64;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r1) {
        A05((AnonymousClass5GY) r1);
    }

    @Override // X.AnonymousClass5GY, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        this.A02 = 7640891576956012808L;
        this.A03 = -4942790177534073029L;
        this.A04 = 4354685564936845355L;
        this.A05 = -6534734903238641935L;
        this.A06 = 5840696475078001361L;
        this.A07 = -7276294671716946913L;
        this.A08 = 2270897969802886507L;
        this.A09 = 6620516959819538809L;
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        AnonymousClass5GY.A03(this, bArr, i);
        AbstractC95434di.A03(bArr, i + 48, this.A08);
        AbstractC95434di.A03(bArr, i + 56, this.A09);
        reset();
        return 64;
    }
}
