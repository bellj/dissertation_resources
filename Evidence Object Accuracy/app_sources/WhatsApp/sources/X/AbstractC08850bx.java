package X;

import java.io.Closeable;
import java.util.Arrays;

/* renamed from: X.0bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08850bx implements Closeable {
    public static final String[] A04 = new String[128];
    public int A00;
    public int[] A01 = new int[32];
    public int[] A02 = new int[32];
    public String[] A03 = new String[32];

    public abstract double A02();

    public abstract int A03();

    public abstract int A04(C05850Rf v);

    public abstract EnumC03770Jb A05();

    public abstract String A07();

    public abstract String A08();

    public abstract void A09();

    public abstract void A0A();

    public abstract void A0B();

    public abstract void A0C();

    public abstract void A0D();

    public abstract void A0E();

    public abstract boolean A0H();

    public abstract boolean A0I();

    static {
        String[] strArr;
        int i = 0;
        do {
            strArr = A04;
            strArr[i] = String.format("\\u%04x", Integer.valueOf(i));
            i++;
        } while (i <= 31);
        strArr[34] = "\\\"";
        strArr[92] = "\\\\";
        strArr[9] = "\\t";
        strArr[8] = "\\b";
        strArr[10] = "\\n";
        strArr[13] = "\\r";
        strArr[12] = "\\f";
    }

    public static AbstractC08850bx A01(AbstractC02750Dv r1) {
        return new AnonymousClass0HI(r1);
    }

    public final String A06() {
        int i = this.A00;
        int[] iArr = this.A02;
        String[] strArr = this.A03;
        int[] iArr2 = this.A01;
        StringBuilder sb = new StringBuilder("$");
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = iArr[i2];
            if (i3 == 1 || i3 == 2) {
                sb.append('[');
                sb.append(iArr2[i2]);
                sb.append(']');
            } else if (i3 == 3 || i3 == 4 || i3 == 5) {
                sb.append('.');
                if (strArr[i2] != null) {
                    sb.append(strArr[i2]);
                }
            }
        }
        return sb.toString();
    }

    public final void A0F(int i) {
        int i2 = this.A00;
        int[] iArr = this.A02;
        int length = iArr.length;
        if (i2 == length) {
            if (i2 != 256) {
                this.A02 = Arrays.copyOf(iArr, length << 1);
                String[] strArr = this.A03;
                this.A03 = (String[]) Arrays.copyOf(strArr, strArr.length << 1);
                int[] iArr2 = this.A01;
                this.A01 = Arrays.copyOf(iArr2, iArr2.length << 1);
            } else {
                StringBuilder sb = new StringBuilder("Nesting too deep at ");
                sb.append(A06());
                throw new C10750f9(sb.toString());
            }
        }
        int[] iArr3 = this.A02;
        int i3 = this.A00;
        this.A00 = i3 + 1;
        iArr3[i3] = i;
    }

    public final void A0G(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" at path ");
        sb.append(A06());
        throw new C03640Io(sb.toString());
    }
}
