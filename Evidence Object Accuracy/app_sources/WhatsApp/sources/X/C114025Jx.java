package X;

/* renamed from: X.5Jx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114025Jx extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C114025Jx() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public Object AJ4(Object obj) {
        C16700pc.A0E(obj, 0);
        if (!(obj instanceof AnonymousClass2T1)) {
            return null;
        }
        return obj;
    }
}
