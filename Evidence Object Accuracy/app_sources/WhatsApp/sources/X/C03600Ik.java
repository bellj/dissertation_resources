package X;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

/* renamed from: X.0Ik  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03600Ik extends ByteArrayOutputStream {
    public final /* synthetic */ C08830bv A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C03600Ik(C08830bv r1, int i) {
        super(i);
        this.A00 = r1;
    }

    @Override // java.io.ByteArrayOutputStream, java.lang.Object
    public String toString() {
        int i = ((ByteArrayOutputStream) this).count;
        if (i > 0) {
            int i2 = i - 1;
            if (((ByteArrayOutputStream) this).buf[i2] == 13) {
                i = i2;
            }
        }
        try {
            return new String(((ByteArrayOutputStream) this).buf, 0, i, this.A00.A04.name());
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
