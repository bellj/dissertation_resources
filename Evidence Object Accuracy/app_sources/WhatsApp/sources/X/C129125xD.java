package X;

import android.graphics.BitmapFactory;
import android.widget.ImageView;
import java.io.File;

/* renamed from: X.5xD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129125xD {
    public C38721ob A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final AnonymousClass01H A04;

    public C129125xD(AnonymousClass01H r1, AnonymousClass01H r2, AnonymousClass01H r3, AnonymousClass01H r4) {
        this.A03 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r4;
    }

    public void A00(ImageView imageView, String str, String str2) {
        if (str.startsWith("file:///")) {
            imageView.setImageBitmap(BitmapFactory.decodeFile(str.replaceAll("file:///", "")));
            return;
        }
        C38721ob r0 = this.A00;
        if (r0 == null) {
            C38771og r8 = new C38771og((C14900mE) this.A01.get(), (C18790t3) this.A02.get(), (C18810t5) this.A04.get(), new File(((C16590pI) this.A03.get()).A00.getCacheDir(), "wabloks_images"), "bk-image");
            r8.A05 = true;
            r8.A01 = 16777216;
            r8.A00 = Integer.MAX_VALUE;
            r0 = r8.A00();
            this.A00 = r0;
        }
        int i = r0.A01;
        r0.A02.A01(new AnonymousClass53K(null, null, imageView, null, str, str2, i, i), r0.A03);
    }
}
