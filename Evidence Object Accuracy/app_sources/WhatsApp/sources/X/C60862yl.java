package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.conversation.conversationrow.components.ViewOnceDownloadProgressView;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.Set;

/* renamed from: X.2yl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60862yl extends AbstractC60702yS {
    public final View A00 = AnonymousClass028.A0D(this, R.id.conversation_row_root);
    public final ViewGroup A01;
    public final ViewGroup A02;
    public final FrameLayout A03;
    public final TextView A04;
    public final TextView A05;
    public final WaTextView A06 = C12960it.A0N(this, R.id.view_once_file_size);
    public final WaTextView A07 = C12960it.A0N(this, R.id.view_once_media_type_large);
    public final ViewOnceDownloadProgressView A08;

    public C60862yl(Context context, AbstractC13890kV r7, AbstractC16130oV r8) {
        super(context, r7, r8);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(this, R.id.view_once_media_container_large);
        this.A03 = frameLayout;
        this.A08 = (ViewOnceDownloadProgressView) AnonymousClass028.A0D(this, R.id.view_once_download_large);
        this.A01 = (ViewGroup) AnonymousClass028.A0D(frameLayout, R.id.date_wrapper);
        this.A04 = C12960it.A0I(frameLayout, R.id.date);
        View view = ((AbstractC42661vc) this).A01;
        this.A02 = (ViewGroup) AnonymousClass028.A0D(view, R.id.date_wrapper);
        this.A05 = C12960it.A0I(view, R.id.date);
        frameLayout.setForeground(getInnerFrameForegroundDrawable());
        A1R();
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        ActivityC13810kN r1;
        AbstractC16130oV fMessage = getFMessage();
        AnonymousClass1XH r2 = (AnonymousClass1XH) fMessage;
        if (r2.AHc() == 2) {
            AbstractC15340mz r22 = (AbstractC15340mz) r2;
            C15370n3 A04 = C30041Vv.A04(this.A0o, r22);
            if (A04 != null) {
                boolean z = r22 instanceof AnonymousClass1XI;
                int i = R.string.view_once_video_expired_dialog_title;
                int i2 = R.string.view_once_video_expired_dialog_body;
                if (z) {
                    i = R.string.view_once_photo_expired_dialog_title;
                    i2 = R.string.view_once_photo_expired_dialog_body;
                }
                C004802e A0S = C12980iv.A0S(getContext());
                A0S.A07(i);
                A0S.A0A(C12990iw.A0o(getResources(), ((AnonymousClass1OY) this).A0Z.A04(A04), new Object[1], 0, i2));
                C12970iu.A1I(A0S);
                A0S.A0B(true);
                C12970iu.A1J(A0S);
            }
        } else if (((AbstractC42671vd) this).A01 != null && !AnonymousClass1OY.A0U(this)) {
        } else {
            if (!fMessage.A1A()) {
                Log.w("conversation/row/viewOnce/no file");
                if (!A1O() && (r1 = (ActivityC13810kN) AbstractC35731ia.A01(getContext(), ActivityC13810kN.class)) != null) {
                    ((AbstractC28551Oa) this).A0P.A01(r1);
                    return;
                }
                return;
            }
            AnonymousClass2TS r23 = new AnonymousClass2TS(getContext());
            r23.A07 = true;
            AnonymousClass1IS r12 = fMessage.A0z;
            AbstractC14640lm r0 = r12.A00;
            AnonymousClass009.A05(r0);
            r23.A03 = r0;
            r23.A04 = r12;
            r23.A01 = 3;
            C12990iw.A10(r23.A00(), this);
            postDelayed(new RunnableBRunnable0Shape11S0200000_I1_1(this, 19, fMessage), 220);
        }
    }

    @Override // X.AbstractC42661vc
    public void A1P() {
        super.A1P();
        A19(getFMessage());
    }

    @Override // X.AbstractC42661vc
    public void A1R() {
        int AHc = ((AnonymousClass1XH) getFMessage()).AHc();
        if (AHc == 0) {
            ((AbstractC42661vc) this).A01.setVisibility(8);
            AbstractC16130oV fMessage = getFMessage();
            int A00 = C30041Vv.A00(fMessage);
            setTransitionNames(fMessage);
            AbstractC42661vc.A0X(this.A08, fMessage, A00, false);
            A1U(this.A03, A00, false);
            A1V(fMessage, A00);
            A19(fMessage);
        } else if (AHc == 1) {
            this.A03.setVisibility(8);
            A1Q();
        } else if (AHc == 2) {
            ((AbstractC42661vc) this).A01.setVisibility(8);
            AbstractC16130oV fMessage2 = getFMessage();
            setTransitionNames(fMessage2);
            AbstractC42661vc.A0X(this.A08, fMessage2, 2, false);
            A1U(this.A03, 2, false);
            A1V(fMessage2, 2);
            A19(fMessage2);
        }
    }

    @Override // X.AbstractC42661vc
    public void A1U(View view, int i, boolean z) {
        super.A1U(view, i, z);
        if (i == 2) {
            this.A06.setVisibility(8);
            return;
        }
        AbstractC16130oV fMessage = getFMessage();
        WaTextView waTextView = this.A06;
        waTextView.setText(C30041Vv.A0B(((AbstractC28551Oa) this).A0K, fMessage.A01));
        waTextView.setVisibility(0);
    }

    public final void A1V(AbstractC16130oV r9, int i) {
        String[] A1b;
        FrameLayout frameLayout = this.A03;
        String valueOf = String.valueOf(frameLayout.getContentDescription());
        String A0B = C30041Vv.A0B(((AbstractC28551Oa) this).A0K, r9.A01);
        String A00 = AnonymousClass3JK.A00(((AbstractC28551Oa) this).A0K, this.A0k.A02(r9.A0I));
        AnonymousClass018 r1 = ((AbstractC28551Oa) this).A0K;
        if (i == 2) {
            A1b = C12990iw.A1b(valueOf, A00, 2);
        } else {
            A1b = C12990iw.A1b(valueOf, A0B, 3);
            A1b[2] = A00;
        }
        frameLayout.setContentDescription(C32721cd.A00(r1, Arrays.asList(A1b), false));
    }

    @Override // X.AnonymousClass1OY
    public TextView getDateView() {
        if (((AnonymousClass1XH) getFMessage()).AHc() == 0) {
            return this.A04;
        }
        return this.A05;
    }

    @Override // X.AnonymousClass1OY
    public ViewGroup getDateWrapper() {
        if (((AnonymousClass1XH) getFMessage()).AHc() == 0) {
            return this.A01;
        }
        return this.A02;
    }

    @Override // X.AnonymousClass1OY
    public Set getInnerFrameLayouts() {
        Set innerFrameLayouts = super.getInnerFrameLayouts();
        innerFrameLayouts.add(this.A03);
        return innerFrameLayouts;
    }

    private void setTransitionNames(AbstractC16130oV r4) {
        AnonymousClass028.A0k(((AnonymousClass1OY) this).A0E, AbstractC42671vd.A0Y(r4));
        ImageView imageView = ((AnonymousClass1OY) this).A0B;
        if (imageView != null) {
            StringBuilder sb = new StringBuilder("status-transition-");
            sb.append(r4.A0z);
            AnonymousClass028.A0k(imageView, sb.toString());
        }
    }
}
