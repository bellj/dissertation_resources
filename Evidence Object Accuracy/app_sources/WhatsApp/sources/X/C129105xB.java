package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import java.util.HashMap;

/* renamed from: X.5xB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129105xB {
    public final C14830m7 A00;
    public final C130155yt A01;
    public final C130125yq A02;
    public final AnonymousClass61F A03;
    public final AnonymousClass61C A04;

    public C129105xB(C14830m7 r1, C130155yt r2, C130125yq r3, AnonymousClass61F r4, AnonymousClass61C r5) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
    }

    public Intent A00(Context context, String str, String str2) {
        if (this.A03.A0D()) {
            HashMap A11 = C12970iu.A11();
            A11.put("tpp_access_code_from_deeplink", str2);
            Bundle A0D = C12970iu.A0D();
            A0D.putSerializable("screen_params", A11);
            Intent A0D2 = C12990iw.A0D(context, NoviPayBloksActivity.class);
            A0D2.putExtra("screen_name", "novipay_p_tpp_account_link_consent");
            A0D2.putExtras(A0D);
            A0D2.addFlags(1073741824);
            return A0D2;
        }
        HashMap hashMap = new HashMap(10);
        hashMap.put("login_entry_point", "novi_tpp_consent_page");
        hashMap.put("tpp_access_code_from_deeplink", str2);
        Bundle A0D3 = C12970iu.A0D();
        A0D3.putSerializable("screen_params", hashMap);
        A0D3.putString("screen_name", "novipay_p_login_password");
        A0D3.putInt("login_entry_point", 1);
        Intent A0D4 = C12990iw.A0D(context, NoviPayBloksActivity.class);
        A0D4.putExtras(A0D3);
        A0D4.putExtra("action", str);
        A0D4.putExtra("tpp_access_code_from_deeplink", str2);
        return A0D4;
    }
}
