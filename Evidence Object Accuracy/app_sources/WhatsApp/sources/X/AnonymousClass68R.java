package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.payments.ui.IndiaUpiPaymentSettingsFragment;

/* renamed from: X.68R  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68R implements AnonymousClass6ML {
    public final /* synthetic */ IndiaUpiPaymentSettingsFragment A00;

    public AnonymousClass68R(IndiaUpiPaymentSettingsFragment indiaUpiPaymentSettingsFragment) {
        this.A00 = indiaUpiPaymentSettingsFragment;
    }

    @Override // X.AnonymousClass6ML
    public void AO2(String str, String str2) {
        IndiaUpiPaymentSettingsFragment indiaUpiPaymentSettingsFragment = this.A00;
        indiaUpiPaymentSettingsFragment.A0r.A04((ActivityC13790kL) indiaUpiPaymentSettingsFragment.A0C(), str);
    }

    @Override // X.AnonymousClass6ML
    public void ARq() {
        IndiaUpiPaymentSettingsFragment indiaUpiPaymentSettingsFragment = this.A00;
        Context A01 = indiaUpiPaymentSettingsFragment.A01();
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(A01.getPackageName(), "com.whatsapp.framework.alerts.ui.AlertCardListActivity");
        indiaUpiPaymentSettingsFragment.A0v(A0A);
    }
}
