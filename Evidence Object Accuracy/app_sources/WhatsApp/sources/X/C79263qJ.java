package X;

import java.nio.ByteBuffer;

/* renamed from: X.3qJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79263qJ extends C79283qL {
    public int A00;
    public final ByteBuffer A01;

    public C79263qJ(ByteBuffer byteBuffer) {
        super(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
        this.A01 = byteBuffer;
        this.A00 = byteBuffer.position();
    }
}
