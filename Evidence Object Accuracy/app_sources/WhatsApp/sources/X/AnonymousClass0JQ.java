package X;

/* renamed from: X.0JQ  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0JQ {
    UNRESTRICTED,
    /* Fake field, exist only in values array */
    CONSTANT,
    SLACK,
    ERROR,
    UNKNOWN
}
