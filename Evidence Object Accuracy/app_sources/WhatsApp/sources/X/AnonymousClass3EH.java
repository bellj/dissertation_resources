package X;

import java.util.List;

/* renamed from: X.3EH  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EH {
    public final AnonymousClass1V8 A00;
    public final String A01;
    public final List A02;

    public AnonymousClass3EH(AbstractC15710nm r5, AnonymousClass1V8 r6) {
        AnonymousClass1V8.A01(r6, "data");
        this.A01 = AnonymousClass3JT.A07(r6, "type", new String[1]);
        this.A02 = AnonymousClass3JT.A0A(r5, r6, new String[]{"pay"}, 8);
        this.A00 = r6;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EH.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EH r5 = (AnonymousClass3EH) obj;
            if (!this.A01.equals(r5.A01) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A02, A1a);
    }
}
