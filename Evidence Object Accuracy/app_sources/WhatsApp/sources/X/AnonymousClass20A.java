package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.20A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20A implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99874l0();
    public String A00;
    public boolean A01;
    public boolean A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass20A() {
        this.A00 = "";
        this.A02 = true;
        this.A01 = true;
    }

    public /* synthetic */ AnonymousClass20A(Parcel parcel) {
        this.A00 = "";
        boolean z = true;
        this.A02 = true;
        this.A01 = true;
        this.A00 = parcel.readString();
        this.A02 = parcel.readByte() == 0 ? false : z;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeByte(this.A02 ? (byte) 1 : 0);
    }
}
