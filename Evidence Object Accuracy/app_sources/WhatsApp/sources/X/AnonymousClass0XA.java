package X;

import java.util.ArrayDeque;

/* renamed from: X.0XA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XA implements AnonymousClass06T {
    public final AbstractC010305c A00;
    public final /* synthetic */ AnonymousClass051 A01;

    public AnonymousClass0XA(AbstractC010305c r1, AnonymousClass051 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass06T
    public void cancel() {
        ArrayDeque arrayDeque = this.A01.A01;
        AbstractC010305c r0 = this.A00;
        arrayDeque.remove(r0);
        r0.A00.remove(this);
    }
}
