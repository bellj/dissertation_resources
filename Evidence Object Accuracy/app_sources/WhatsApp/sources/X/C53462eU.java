package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.2eU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53462eU extends AnonymousClass04v {
    public final /* synthetic */ C36591kA A00;

    public C53462eU(C36591kA r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        AccessibilityNodeInfo accessibilityNodeInfo = r4.A02;
        accessibilityNodeInfo.setLongClickable(false);
        r4.A0A(C007804a.A0F);
        accessibilityNodeInfo.setClickable(true);
        r4.A09(C007804a.A05);
    }
}
