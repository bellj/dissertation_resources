package X;

/* renamed from: X.5fP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119975fP extends AbstractC16110oT {
    public String A00;
    public String A01;
    public String A02;
    public String A03;

    public C119975fP() {
        super(2706, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A02);
        r3.Abe(5, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPaymentsWaviUserAction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dataEnv", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "encPayload1", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "encPayload2", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "encPayload3", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
