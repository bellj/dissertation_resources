package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

/* renamed from: X.2QW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QW extends ImageButton {
    public int A00 = getVisibility();

    public AnonymousClass2QW(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public final void A00(int i, boolean z) {
        super.setVisibility(i);
        if (z) {
            this.A00 = i;
        }
    }

    public final int getUserSetVisibility() {
        return this.A00;
    }

    @Override // android.widget.ImageView, android.view.View
    public void setVisibility(int i) {
        A00(i, true);
    }
}
