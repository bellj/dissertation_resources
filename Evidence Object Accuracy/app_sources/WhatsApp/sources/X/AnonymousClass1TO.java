package X;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.1TO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TO extends FilterInputStream {
    public final int A00;
    public final boolean A01;
    public final byte[][] A02;

    public static int A00(InputStream inputStream, int i) {
        int i2 = i & 31;
        if (i2 == 31) {
            int i3 = 0;
            int read = inputStream.read();
            if ((read & 127) == 0) {
                throw new IOException("corrupted stream - invalid high tag number found");
            }
            while (read >= 0) {
                i2 = i3 | (read & 127);
                if ((read & 128) != 0) {
                    i3 = i2 << 7;
                    read = inputStream.read();
                }
            }
            throw new EOFException("EOF found inside tag value.");
        }
        return i2;
    }

    public static int A01(InputStream inputStream, int i, boolean z) {
        int read = inputStream.read();
        if (read < 0) {
            throw new EOFException("EOF found when length expected");
        } else if (read == 128) {
            return -1;
        } else {
            if (read > 127) {
                int i2 = read & 127;
                if (i2 <= 4) {
                    read = 0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        int read2 = inputStream.read();
                        if (read2 >= 0) {
                            read = (read << 8) + read2;
                        } else {
                            throw new EOFException("EOF found reading length");
                        }
                    }
                    if (read < 0) {
                        throw new IOException("corrupted stream - negative length found");
                    } else if (read >= i && !z) {
                        StringBuilder sb = new StringBuilder("corrupted stream - out of bounds length found: ");
                        sb.append(read);
                        sb.append(" >= ");
                        sb.append(i);
                        throw new IOException(sb.toString());
                    }
                } else {
                    StringBuilder sb2 = new StringBuilder("DER length more than 4 bytes: ");
                    sb2.append(i2);
                    throw new IOException(sb2.toString());
                }
            }
            return read;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1TO(InputStream inputStream) {
        super(inputStream);
        int A02 = AnonymousClass1TQ.A02(inputStream);
        this.A00 = A02;
        this.A01 = false;
        this.A02 = new byte[11];
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1TO(InputStream inputStream, boolean z) {
        super(inputStream);
        int A02 = AnonymousClass1TQ.A02(inputStream);
        this.A00 = A02;
        this.A01 = true;
        this.A02 = new byte[11];
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1TO(byte[] r4) {
        /*
            r3 = this;
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r4)
            int r1 = r4.length
            r0 = 0
            r3.<init>(r2)
            r3.A00 = r1
            r3.A01 = r0
            r0 = 11
            byte[][] r0 = new byte[r0]
            r3.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TO.<init>(byte[]):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1TO(byte[] r4, boolean r5) {
        /*
            r3 = this;
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r4)
            int r1 = r4.length
            r0 = 1
            r3.<init>(r2)
            r3.A00 = r1
            r3.A01 = r0
            r0 = 11
            byte[][] r0 = new byte[r0]
            r3.A02 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TO.<init>(byte[], boolean):void");
    }

    public static C94954co A02(C72353eS r2) {
        if (r2.A00 < 1) {
            return new C94954co(0);
        }
        AnonymousClass1TO r1 = new AnonymousClass1TO(r2);
        C94954co r22 = new C94954co(10);
        while (true) {
            AnonymousClass1TL A05 = r1.A05();
            if (A05 == null) {
                return r22;
            }
            r22.A06(A05);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass1TL A03(X.C72353eS r11, byte[][] r12, int r13) {
        /*
        // Method dump skipped, instructions count: 564
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1TO.A03(X.3eS, byte[][], int):X.1TL");
    }

    public static byte[] A04(C72353eS r8, byte[][] bArr) {
        int i = r8.A00;
        if (i >= bArr.length) {
            return r8.A01();
        }
        byte[] bArr2 = bArr[i];
        if (bArr2 == null) {
            bArr2 = new byte[i];
            bArr[i] = bArr2;
        }
        int length = bArr2.length;
        if (i != length) {
            throw new IllegalArgumentException("buffer length not right for data");
        } else if (i == 0) {
            return bArr2;
        } else {
            int i2 = ((AnonymousClass49A) r8).A00;
            if (i < i2) {
                InputStream inputStream = ((AnonymousClass49A) r8).A01;
                int i3 = 0;
                while (i3 < length) {
                    int read = inputStream.read(bArr2, 0 + i3, length - i3);
                    if (read < 0) {
                        break;
                    }
                    i3 += read;
                }
                int i4 = i - i3;
                r8.A00 = i4;
                if (i4 == 0) {
                    r8.A00();
                    return bArr2;
                }
                StringBuilder sb = new StringBuilder("DEF length ");
                sb.append(r8.A01);
                sb.append(" object truncated by ");
                sb.append(i4);
                throw new EOFException(sb.toString());
            }
            StringBuilder sb2 = new StringBuilder("corrupted stream - out of bounds length found: ");
            sb2.append(i);
            sb2.append(" >= ");
            sb2.append(i2);
            throw new IOException(sb2.toString());
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: X.5NH[] */
    /* JADX WARN: Multi-variable type inference failed */
    public AnonymousClass1TL A05() {
        int read = read();
        if (read > 0) {
            int A00 = A00(this, read);
            int i = read & 32;
            boolean z = false;
            if (i != 0) {
                z = true;
            }
            int i2 = this.A00;
            int A01 = A01(this, i2, false);
            if (A01 >= 0) {
                boolean z2 = false;
                if (i != 0) {
                    z2 = true;
                }
                try {
                    C72353eS r2 = new C72353eS(this, A01, i2);
                    if ((read & 64) != 0) {
                        return new AnonymousClass5M8(r2.A01(), A00, z2);
                    }
                    if ((read & 128) != 0) {
                        return new C92754Xh(r2, AnonymousClass1TQ.A02(r2)).A02(A00, z2);
                    }
                    if (!z2) {
                        return A03(r2, this.A02, A00);
                    }
                    if (A00 == 4) {
                        C94954co A02 = A02(r2);
                        int i3 = A02.A00;
                        AnonymousClass5NH[] r1 = new AnonymousClass5NH[i3];
                        for (int i4 = 0; i4 != i3; i4++) {
                            AnonymousClass1TN A05 = A02.A05(i4);
                            if (A05 instanceof AnonymousClass5NH) {
                                r1[i4] = A05;
                            } else {
                                StringBuilder sb = new StringBuilder("unknown object encountered in constructed OCTET STRING: ");
                                sb.append(A05.getClass());
                                throw new AnonymousClass492(sb.toString());
                            }
                        }
                        return new AnonymousClass5N6(r1);
                    } else if (A00 == 8) {
                        return new AnonymousClass5MB(A02(r2));
                    } else {
                        if (A00 != 16) {
                            if (A00 == 17) {
                                C94954co A022 = A02(r2);
                                return A022.A00 < 1 ? AnonymousClass4HI.A01 : new C114805Nd(A022);
                            }
                            StringBuilder sb2 = new StringBuilder("unknown tag ");
                            sb2.append(A00);
                            sb2.append(" encountered");
                            throw new IOException(sb2.toString());
                        } else if (this.A01) {
                            return new AnonymousClass5NW(r2.A01());
                        } else {
                            C94954co A023 = A02(r2);
                            return A023.A00 < 1 ? AnonymousClass4HI.A00 : new AnonymousClass5NY(A023);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    throw new AnonymousClass492("corrupted stream detected", e);
                }
            } else if (z) {
                C92754Xh r12 = new C92754Xh(new C114885Nl(this, i2), i2);
                if ((read & 64) != 0) {
                    return new C112875Fd(r12, A00).ADw();
                }
                if ((read & 128) != 0) {
                    return new C112945Fk(r12, A00, true).ADw();
                }
                if (A00 == 4) {
                    return new C112885Fe(r12).ADw();
                }
                if (A00 == 8) {
                    return new C112865Fc(r12).ADw();
                }
                if (A00 == 16) {
                    return new C112905Fg(r12).ADw();
                }
                if (A00 == 17) {
                    return new C112925Fi(r12).ADw();
                }
                throw new IOException("unknown BER object encountered");
            } else {
                throw new IOException("indefinite-length primitive encoding encountered");
            }
        } else if (read != 0) {
            return null;
        } else {
            throw new IOException("unexpected end-of-contents marker");
        }
    }
}
