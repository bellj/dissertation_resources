package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99674kg implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30141Wg(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30141Wg[i];
    }
}
