package X;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import com.whatsapp.util.Log;

/* renamed from: X.3LR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3LR implements SensorEventListener {
    public boolean A00;
    public final /* synthetic */ C89414Jw A01;
    public final /* synthetic */ C91044Qf A02;

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public AnonymousClass3LR(C89414Jw r1, C91044Qf r2) {
        this.A02 = r2;
        this.A01 = r1;
    }

    @Override // android.hardware.SensorEventListener
    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean z = false;
        float f = sensorEvent.values[0];
        if (f < 5.0f && f != this.A02.A01.getMaximumRange()) {
            z = true;
        }
        if (z != this.A00) {
            this.A00 = z;
            C29631Ua r1 = this.A01.A00;
            Log.i(C12960it.A0b("voip/service/proximitylistener.onchanged ", r1));
            if (z) {
                r1.A0S();
            } else {
                r1.A0R();
            }
        }
    }
}
