package X;

import android.content.SharedPreferences;

/* renamed from: X.0xf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21590xf {
    public final C16510p9 A00;
    public final C15650ng A01;
    public final C21610xh A02;
    public final C20120vF A03;
    public final C21620xi A04;
    public final C16490p7 A05;

    public C21590xf(C16510p9 r1, C15650ng r2, C21610xh r3, C20120vF r4, C21620xi r5, C16490p7 r6) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A04 = r5;
        this.A05 = r6;
        this.A03 = r4;
    }

    public static final void A00(C90224Nb r4, AbstractC14640lm r5, int i, int i2) {
        C14820m6 r0 = r4.A01.A02;
        String rawString = r5.getRawString();
        SharedPreferences sharedPreferences = r0.A00;
        sharedPreferences.edit().putString("storage_usage_deletion_jid", rawString).apply();
        sharedPreferences.edit().putInt("storage_usage_deletion_current_msg_cnt", i2).putInt("storage_usage_deletion_all_msg_cnt", i).apply();
        r4.A00.AUM(i2, i);
    }
}
