package X;

import android.content.Context;
import android.text.TextUtils;

/* renamed from: X.1ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33721ey extends C33711ex {
    public C33721ey(C16470p4 r1) {
        super(r1);
    }

    @Override // X.C33711ex
    public String A06(Context context) {
        String str;
        StringBuilder sb = new StringBuilder("🛍 ");
        C16470p4 r0 = this.A00;
        if (r0 != null) {
            str = r0.A07;
        } else {
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            str = A04();
        }
        sb.append(str);
        return sb.toString();
    }

    @Override // X.C33711ex
    public String A08(Context context) {
        String str;
        C16470p4 r0 = this.A00;
        if (r0 != null) {
            str = r0.A07;
        } else {
            str = null;
        }
        if (TextUtils.isEmpty(str)) {
            return A04();
        }
        return str;
    }

    @Override // X.C33711ex
    public void A0A(AbstractC15340mz r10, C39971qq r11) {
        AbstractC27091Fz r0;
        AnonymousClass4BF r2;
        super.A0A(r10, r11);
        C16470p4 ABf = ((AbstractC16390ow) r10).ABf();
        if (ABf != null && ABf.A05 != null) {
            AnonymousClass1G3 r4 = r11.A03;
            C57752nZ r02 = ((C27081Fy) r4.A00).A0K;
            if (r02 == null) {
                r02 = C57752nZ.A07;
            }
            AnonymousClass1G4 A0T = r02.A0T();
            C57752nZ r1 = (C57752nZ) A0T.A00;
            if (r1.A01 == 4) {
                r0 = (AbstractC27091Fz) r1.A06;
            } else {
                r0 = C57342ms.A04;
            }
            AnonymousClass1G4 A0T2 = r0.A0T();
            AnonymousClass1ZE r8 = ABf.A05;
            String str = r8.A02;
            if (!TextUtils.isEmpty(str)) {
                A0T2.A03();
                C57342ms r12 = (C57342ms) A0T2.A00;
                r12.A00 |= 1;
                r12.A03 = str;
            }
            int i = r8.A00;
            if (i == 1) {
                r2 = AnonymousClass4BF.A01;
            } else if (i == 2) {
                r2 = AnonymousClass4BF.A02;
            } else if (i != 3) {
                r2 = AnonymousClass4BF.A03;
            } else {
                r2 = AnonymousClass4BF.A04;
            }
            A0T2.A03();
            C57342ms r13 = (C57342ms) A0T2.A00;
            r13.A00 |= 2;
            r13.A02 = r2.value;
            int i2 = r8.A01;
            A0T2.A03();
            C57342ms r14 = (C57342ms) A0T2.A00;
            r14.A00 |= 4;
            r14.A01 = i2;
            if (ABf.A00 == 4) {
                A0T.A03();
                C57752nZ r15 = (C57752nZ) A0T.A00;
                r15.A06 = A0T2.A02();
                r15.A01 = 4;
            }
            r4.A07((C57752nZ) A0T.A02());
        }
    }
}
