package X;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0xn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21670xn {
    public final C21640xk A00;
    public final Set A01;

    public C21670xn(C16590pI r4, C21640xk r5) {
        AnonymousClass01M.A00(r4.A00, AnonymousClass01J.class);
        Set set = new AnonymousClass2Ey().A00;
        if (set != null) {
            AbstractC17940re copyOf = AbstractC17940re.copyOf(set);
            this.A01 = new HashSet();
            this.A00 = r5;
            Iterator<E> it = copyOf.iterator();
            while (it.hasNext()) {
                this.A01.add(it.next());
            }
            return;
        }
        throw new NullPointerException("Cannot return null from a non-@Nullable @Provides method");
    }
}
