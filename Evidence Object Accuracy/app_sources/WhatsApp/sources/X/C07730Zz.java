package X;

/* renamed from: X.0Zz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07730Zz implements AbstractC11480gM {
    public final AbstractC02880Ff A00;
    public final AnonymousClass0QN A01;
    public final AbstractC05330Pd A02;
    public final AbstractC05330Pd A03;

    public C07730Zz(AnonymousClass0QN r2) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0FQ(r2, this);
        this.A02 = new AnonymousClass0FV(r2, this);
        this.A03 = new AnonymousClass0FW(r2, this);
    }
}
