package X;

import java.util.Arrays;

/* renamed from: X.5NT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NT extends AnonymousClass1TL implements AnonymousClass5VP {
    public final byte[] A00;

    public AnonymousClass5NT(byte[] bArr) {
        this.A00 = bArr;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A00, 22, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        return AnonymousClass1T7.A02(this.A00);
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    public AnonymousClass5NT(String str) {
        if (str != null) {
            this.A00 = AnonymousClass1T7.A03(str);
            return;
        }
        throw C12980iv.A0n("'string' cannot be null");
    }

    public static AnonymousClass5NT A00(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NT)) {
            return (AnonymousClass5NT) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return (AnonymousClass5NT) AnonymousClass1TL.A03((byte[]) obj);
            } catch (Exception e) {
                throw C12970iu.A0f(C12960it.A0d(e.toString(), C12960it.A0k("encoding error in getInstance: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NT)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NT) r3).A00);
    }

    public String toString() {
        return AnonymousClass1T7.A02(this.A00);
    }
}
