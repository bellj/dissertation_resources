package X;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

/* renamed from: X.0Ig  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03560Ig extends AnonymousClass2k7 {
    public AnonymousClass643 A00;
    public final AbstractC12070hK A01;

    public C03560Ig(AbstractC12070hK r1, C14260l7 r2, AnonymousClass28D r3) {
        super(r2, r3);
        this.A01 = r1;
    }

    @Override // X.AnonymousClass2k7
    public /* bridge */ /* synthetic */ void A06(View view, C14260l7 r2, AnonymousClass28D r3, Object obj) {
        A0A((FrameLayout) view, r2, r3);
    }

    @Override // X.AnonymousClass2k7
    public /* bridge */ /* synthetic */ void A07(View view, C14260l7 r2, AnonymousClass28D r3, Object obj) {
        A09((FrameLayout) view, r2);
    }

    public final AnonymousClass643 A08(C14260l7 r3) {
        AnonymousClass643 r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass643 A8N = this.A01.A8N(r3.A00());
        this.A00 = A8N;
        return A8N;
    }

    public void A09(FrameLayout frameLayout, C14260l7 r3) {
        frameLayout.removeAllViews();
        A08(r3).A07();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r1.equals("front") != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0A(android.widget.FrameLayout r4, X.C14260l7 r5, X.AnonymousClass28D r6) {
        /*
            r3 = this;
            X.643 r2 = r3.A08(r5)
            r0 = 35
            java.lang.String r1 = r6.A0I(r0)
            if (r1 == 0) goto L_0x0015
            java.lang.String r0 = "front"
            boolean r1 = r1.equals(r0)
            r0 = 0
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 1
        L_0x0016:
            r2.A0C(r0)
            android.view.View r0 = r2.A05()
            r4.addView(r0)
            r2.A08()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C03560Ig.A0A(android.widget.FrameLayout, X.0l7, X.28D):void");
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return new FrameLayout(context);
    }
}
