package X;

import java.util.List;

/* renamed from: X.2f8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53732f8 extends AnonymousClass019 {
    public final List A00 = C12960it.A0l();
    public final List A01 = C12960it.A0l();

    public C53732f8(AnonymousClass01F r2) {
        super(r2, 0);
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        return (CharSequence) this.A00.get(i);
    }

    @Override // X.AnonymousClass019
    public AnonymousClass01E A0G(int i) {
        return (AnonymousClass01E) this.A01.get(i);
    }
}
