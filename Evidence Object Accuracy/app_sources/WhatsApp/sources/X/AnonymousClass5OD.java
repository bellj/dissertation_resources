package X;

/* renamed from: X.5OD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OD extends AnonymousClass5OF implements AbstractC115555Rz {
    public static final int[] A0A = {1116352408, 1899447441, -1245643825, -373957723, 961987163, 1508970993, -1841331548, -1424204075, -670586216, 310598401, 607225278, 1426881987, 1925078388, -2132889090, -1680079193, -1046744716, -459576895, -272742522, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, -1740746414, -1473132947, -1341970488, -1084653625, -958395405, -710438585, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, -2117940946, -1838011259, -1564481375, -1474664885, -1035236496, -949202525, -778901479, -694614492, -200395387, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, -2067236844, -1933114872, -1866530822, -1538233109, -1090935817, -965641998};
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int[] A09;

    public AnonymousClass5OD() {
        this.A09 = new int[64];
        reset();
    }

    public AnonymousClass5OD(AnonymousClass5OD r2) {
        super(r2);
        this.A09 = new int[64];
        A0O(r2);
    }

    public static final int A00(int i) {
        return ((i << 10) | (i >>> 22)) ^ (((i >>> 2) | (i << 30)) ^ ((i >>> 13) | (i << 19)));
    }

    public final void A0O(AnonymousClass5OD r5) {
        super.A0M(r5);
        this.A00 = r5.A00;
        this.A01 = r5.A01;
        this.A02 = r5.A02;
        this.A03 = r5.A03;
        this.A04 = r5.A04;
        this.A05 = r5.A05;
        this.A06 = r5.A06;
        this.A07 = r5.A07;
        int[] iArr = r5.A09;
        System.arraycopy(iArr, 0, this.A09, 0, iArr.length);
        this.A08 = r5.A08;
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5OD(this);
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        A0K();
        AbstractC95434di.A01(bArr, this.A00, i);
        AbstractC95434di.A01(bArr, this.A01, i + 4);
        AbstractC95434di.A01(bArr, this.A02, i + 8);
        AbstractC95434di.A01(bArr, this.A03, i + 12);
        AbstractC95434di.A01(bArr, this.A04, i + 16);
        AbstractC95434di.A01(bArr, this.A05, i + 20);
        AbstractC95434di.A01(bArr, this.A06, i + 24);
        reset();
        return 28;
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "SHA-224";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 28;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r1) {
        A0O((AnonymousClass5OD) r1);
    }

    @Override // X.AnonymousClass5OF, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        this.A00 = -1056596264;
        this.A01 = 914150663;
        this.A02 = 812702999;
        this.A03 = -150054599;
        this.A04 = -4191439;
        this.A05 = 1750603025;
        this.A06 = 1694076839;
        this.A07 = -1090891868;
        this.A08 = 0;
        int i = 0;
        while (true) {
            int[] iArr = this.A09;
            if (i != iArr.length) {
                iArr[i] = 0;
                i++;
            } else {
                return;
            }
        }
    }

    public static int A01(int[] iArr, int i, int i2, int i3, int i4) {
        return (((i << 7) | (i >>> 25)) ^ (((i >>> 6) | (i << 26)) ^ ((i >>> 11) | (i << 21)))) + (((i ^ -1) & i3) ^ (i2 & i)) + iArr[i4];
    }

    @Override // X.AnonymousClass5OF
    public void A0L() {
        int[] iArr;
        int i = 16;
        do {
            iArr = this.A09;
            i = AnonymousClass5OF.A0J(iArr, i);
        } while (i <= 63);
        int i2 = this.A00;
        int i3 = this.A01;
        int i4 = this.A02;
        int i5 = this.A03;
        int i6 = this.A04;
        int i7 = this.A05;
        int i8 = this.A06;
        int i9 = this.A07;
        int i10 = 0;
        int i11 = 0;
        do {
            int[] iArr2 = A0A;
            int A06 = C72463ee.A06(iArr, i11, (((i6 << 7) | (i6 >>> 25)) ^ (((i6 >>> 6) | (i6 << 26)) ^ ((i6 >>> 11) | (i6 << 21)))) + (((i6 ^ -1) & i8) ^ (i7 & i6)) + iArr2[i11], i9);
            int i12 = i5 + A06;
            int i13 = i2 & i3;
            int A00 = A06 + A00(i2) + (((i2 & i4) ^ i13) ^ (i3 & i4));
            int i14 = i11 + 1;
            int A062 = C72463ee.A06(iArr, i14, A01(iArr2, i12, i6, i7, i14), i8);
            int i15 = i4 + A062;
            int i16 = A00 & i2;
            int A0E = A062 + AnonymousClass5OF.A0E(A00 & i3, i16, i13, A00(A00));
            int i17 = i14 + 1;
            int A063 = C72463ee.A06(iArr, i17, A01(iArr2, i15, i12, i6, i17), i7);
            int i18 = i3 + A063;
            int i19 = A0E & A00;
            int A0E2 = A063 + AnonymousClass5OF.A0E(A0E & i2, i19, i16, A00(A0E));
            int i20 = i17 + 1;
            int A064 = C72463ee.A06(iArr, i20, A01(iArr2, i18, i15, i12, i20), i6);
            int i21 = i2 + A064;
            int i22 = A0E2 & A0E;
            int A0E3 = A064 + AnonymousClass5OF.A0E(A0E2 & A00, i22, i19, A00(A0E2));
            int i23 = i20 + 1;
            int A065 = C72463ee.A06(iArr, i23, A01(iArr2, i21, i18, i15, i23), i12);
            i9 = A00 + A065;
            int i24 = A0E3 & A0E2;
            i5 = A065 + AnonymousClass5OF.A0E(A0E3 & A0E, i24, i22, A00(A0E3));
            int i25 = i23 + 1;
            int A066 = C72463ee.A06(iArr, i25, A01(iArr2, i9, i21, i18, i25), i15);
            i8 = A0E + A066;
            int i26 = i5 & A0E3;
            i4 = A066 + AnonymousClass5OF.A0E(i5 & A0E2, i26, i24, A00(i5));
            int i27 = i25 + 1;
            int A067 = C72463ee.A06(iArr, i27, A01(iArr2, i8, i9, i21, i27), i18);
            i7 = A0E2 + A067;
            int i28 = i4 & i5;
            i3 = A067 + AnonymousClass5OF.A0E(i4 & A0E3, i28, i26, A00(i4));
            int i29 = i27 + 1;
            int A068 = C72463ee.A06(iArr, i29, A01(iArr2, i7, i8, i9, i29), i21);
            i6 = A0E3 + A068;
            int i30 = i3 & i5;
            i2 = A068 + AnonymousClass5OF.A0E(i30, i3 & i4, i28, A00(i3));
            i11 = i29 + 1;
            i10++;
        } while (i10 < 8);
        this.A00 = i2 + i2;
        this.A01 = i3 + i3;
        this.A02 = i4 + i4;
        this.A03 = i5 + i5;
        this.A04 = i6 + i6;
        this.A05 = i7 + i7;
        this.A06 = i8 + i8;
        this.A07 = i9 + i9;
        this.A08 = 0;
        int i31 = 0;
        do {
            iArr[i31] = 0;
            i31++;
        } while (i31 < 16);
    }
}
