package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.4Sd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91544Sd {
    public final ArrayList A00;
    public final ArrayList A01;
    public final ArrayList A02;
    public final ArrayList A03;
    public final List A04;

    public C91544Sd(ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4, List list) {
        this.A03 = arrayList;
        this.A00 = arrayList2;
        this.A01 = arrayList3;
        this.A02 = arrayList4;
        this.A04 = list;
    }
}
