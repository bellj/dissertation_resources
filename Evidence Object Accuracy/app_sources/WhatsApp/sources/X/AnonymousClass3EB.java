package X;

import android.text.TextUtils;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3EB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3EB {
    public final AbstractC116695Wl A00;
    public final C16820po A01;
    public final AnonymousClass01H A02;

    public AnonymousClass3EB(AbstractC116695Wl r1, C16820po r2, AnonymousClass01H r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public final void A00(C64063Ec r8) {
        AnonymousClass17I r4 = (AnonymousClass17I) this.A02.get();
        if (r8 != null) {
            Map A03 = r4.A03();
            try {
                String str = r8.A01.A00;
                JSONObject jSONObject = new JSONObject();
                Object obj = r8.A03.A00;
                AnonymousClass009.A05(obj);
                JSONObject put = jSONObject.put("fbid", C12980iv.A0G(obj));
                Object obj2 = r8.A04.A00;
                AnonymousClass009.A05(obj2);
                JSONObject put2 = put.put("password", obj2);
                Object obj3 = r8.A02.A00;
                AnonymousClass009.A05(obj3);
                A03.put(str, put2.put("access_token", obj3).put("timestamp", r8.A00).put("ttl", r8.A05).put("analytics_claim", r8.A06).put("usertype", str).toString());
                ((AnonymousClass17D) r4.A05.get()).A01(r4.A01(AnonymousClass17I.A00(A03).toString()));
            } catch (JSONException e) {
                AnonymousClass009.A0D(e);
            }
        } else {
            C16820po r0 = this.A01;
            Map A032 = r4.A03();
            String str2 = r0.A00;
            if (!TextUtils.isEmpty(C12970iu.A0t(str2, A032))) {
                A032.remove(str2);
                ((AnonymousClass17D) r4.A05.get()).A01(r4.A01(new JSONObject(A032).toString()));
            }
        }
        this.A00.AWx(r8);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0024, code lost:
        if ((r2 instanceof java.security.cert.CertificateExpiredException) != false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A01(java.lang.Exception r2) {
        /*
            r1 = this;
            java.lang.String r0 = "FBUserEntityManagement : On error response while sending the payload"
            com.whatsapp.util.Log.e(r0, r2)
            boolean r0 = r2 instanceof X.C47732Ce
            if (r0 == 0) goto L_0x0022
            r0 = r2
            X.2Ce r0 = (X.C47732Ce) r0
            X.1V8 r0 = r0.node
            int r0 = X.C41151sz.A00(r0)
            if (r0 != 0) goto L_0x001c
            java.lang.String r0 = "no code present"
            com.whatsapp.util.Log.e(r0)
        L_0x0019:
            X.AnonymousClass009.A0D(r2)
        L_0x001c:
            X.5Wl r0 = r1.A00
            r0.APp(r2)
            return
        L_0x0022:
            boolean r0 = r2 instanceof java.security.cert.CertificateExpiredException
            if (r0 == 0) goto L_0x0019
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3EB.A01(java.lang.Exception):void");
    }
}
