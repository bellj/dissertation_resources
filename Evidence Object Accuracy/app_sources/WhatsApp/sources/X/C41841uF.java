package X;

import android.content.Context;
import java.io.File;

/* renamed from: X.1uF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41841uF extends AbstractC38591oM implements AbstractC28771Oy {
    public final long A00;
    public final AnonymousClass10V A01;
    public final C16590pI A02;
    public final C22050yP A03;
    public final C41831uE A04;
    public final AbstractC14590lg A05;
    public final String A06;
    public volatile File A07;
    public volatile boolean A08;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public C41841uF(C15450nH r12, C18790t3 r13, AnonymousClass10V r14, C16590pI r15, C14950mJ r16, C14850m9 r17, C22050yP r18, C20110vE r19, C41831uE r20, C22600zL r21, AbstractC14590lg r22, String str, long j) {
        super(r12, r13, r16, r17, r19, r21, null);
        AnonymousClass009.A05(r20);
        this.A00 = j;
        this.A06 = str;
        this.A05 = r22;
        this.A02 = r15;
        this.A04 = r20;
        this.A03 = r18;
        this.A01 = r14;
        A5g(this);
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        Context context = this.A02.A00;
        C41831uE r0 = this.A04;
        File A00 = C17080qE.A00(context, r0.A04, r0.A05);
        if (A00.exists()) {
            A00.delete();
        }
        this.A05.accept(this.A06);
        this.A08 = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x008e  */
    @Override // X.AbstractC28771Oy
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void APQ(X.AnonymousClass1RN r10, X.C28781Oz r11) {
        /*
            r9 = this;
            int r1 = r10.A00
            r0 = 0
            if (r1 != 0) goto L_0x0006
            r0 = 1
        L_0x0006:
            r6 = 0
            if (r0 == 0) goto L_0x0036
            java.io.File r0 = r9.A07
            long r0 = r0.length()
            int r4 = (int) r0
            byte[] r3 = new byte[r4]
            java.io.File r0 = r9.A07     // Catch: FileNotFoundException -> 0x0032, IOException -> 0x003f
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch: FileNotFoundException -> 0x0032, IOException -> 0x003f
            r2.<init>(r0)     // Catch: FileNotFoundException -> 0x0032, IOException -> 0x003f
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch: all -> 0x002d
            r1.<init>(r2)     // Catch: all -> 0x002d
            r1.read(r3, r6, r4)     // Catch: all -> 0x0028
            r1.close()     // Catch: all -> 0x002d
            r2.close()     // Catch: FileNotFoundException -> 0x0032, IOException -> 0x003f
            goto L_0x0045
        L_0x0028:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x002c
        L_0x002c:
            throw r0     // Catch: all -> 0x002d
        L_0x002d:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x0031
        L_0x0031:
            throw r0     // Catch: FileNotFoundException -> 0x0032, IOException -> 0x003f
        L_0x0032:
            r1 = move-exception
            java.lang.String r0 = "ProfilePictureDownload: Could not find picture download file"
            goto L_0x0042
        L_0x0036:
            boolean r0 = X.AnonymousClass1RN.A01(r1)
            r8 = 6
            if (r0 == 0) goto L_0x0058
            r8 = 4
            goto L_0x0058
        L_0x003f:
            r1 = move-exception
            java.lang.String r0 = "ProfilePictureDownload: IO Exception while reading the picture download file"
        L_0x0042:
            com.whatsapp.util.Log.w(r0, r1)
        L_0x0045:
            X.1uE r4 = r9.A04
            r4.A00 = r3
            X.10V r3 = r9.A01
            X.0yn r2 = r3.A00
            r1 = 35
            com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2 r0 = new com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2
            r0.<init>(r3, r1, r4)
            r2.A01(r0)
            r8 = 1
        L_0x0058:
            X.0yP r7 = r9.A03
            X.1uE r0 = r9.A04
            int r0 = r0.A02
            r5 = 1
            if (r0 != r5) goto L_0x0062
            r5 = 2
        L_0x0062:
            long r2 = android.os.SystemClock.elapsedRealtime()
            long r0 = r9.A00
            long r2 = r2 - r0
            java.lang.Long r4 = java.lang.Long.valueOf(r2)
            java.io.File r0 = r9.A07
            if (r0 == 0) goto L_0x008e
            java.io.File r0 = r9.A07
            long r2 = r0.length()
            double r0 = (double) r2
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
        L_0x007c:
            r7.A0A(r0, r4, r8, r5)
            java.io.File r0 = r9.A07
            r0.delete()
            X.0lg r1 = r9.A05
            java.lang.String r0 = r9.A06
            r1.accept(r0)
            r9.A08 = r6
            return
        L_0x008e:
            r0 = 0
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41841uF.APQ(X.1RN, X.1Oz):void");
    }
}
