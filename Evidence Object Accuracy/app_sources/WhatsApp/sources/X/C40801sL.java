package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1sL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40801sL extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40801sL A01;
    public static volatile AnonymousClass255 A02;
    public AnonymousClass1K6 A00 = AnonymousClass277.A01;

    static {
        C40801sL r0 = new C40801sL();
        A01 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.A00.size(); i++) {
            codedOutputStream.A0K((AbstractC27881Jp) this.A00.get(i), 1);
        }
        this.unknownFields.A02(codedOutputStream);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r4, Object obj, Object obj2) {
        switch (r4.ordinal()) {
            case 0:
                return A01;
            case 1:
                this.A00 = ((AbstractC462925h) obj).Afr(this.A00, ((C40801sL) obj2).A00);
                return this;
            case 2:
                AnonymousClass253 r5 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r5.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            AnonymousClass1K6 r1 = this.A00;
                            if (!((AnonymousClass1K7) r1).A00) {
                                r1 = AbstractC27091Fz.A0G(r1);
                                this.A00 = r1;
                            }
                            r1.add(r5.A08());
                        } else if (!A0a(r5, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A00).A00 = false;
                return null;
            case 4:
                return new C40801sL();
            case 5:
                return new AnonymousClass272();
            case 6:
                break;
            case 7:
                if (A02 == null) {
                    synchronized (C40801sL.class) {
                        if (A02 == null) {
                            A02 = new AnonymousClass255(A01);
                        }
                    }
                }
                return A02;
            default:
                throw new UnsupportedOperationException();
        }
        return A01;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A00.size(); i3++) {
            int A03 = ((AbstractC27881Jp) this.A00.get(i3)).A03();
            i2 += CodedOutputStream.A01(A03) + A03;
        }
        int size = 0 + i2 + this.A00.size() + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = size;
        return size;
    }
}
