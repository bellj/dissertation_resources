package X;

import android.app.ActivityManager;
import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.util.Log;

/* renamed from: X.13N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13N {
    public static final String[] A05 = {"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"};
    public boolean A00;
    public final C15570nT A01;
    public final AnonymousClass01d A02;
    public final C14830m7 A03;
    public final C14820m6 A04;

    public AnonymousClass13N(C15570nT r1, AnonymousClass01d r2, C14830m7 r3, C14820m6 r4) {
        this.A03 = r3;
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(android.content.Context r4) {
        /*
            java.lang.String r0 = X.AnonymousClass029.A0A
            X.AnonymousClass03N.A0B = r0
            android.content.Context r0 = r4.getApplicationContext()
            X.AnonymousClass03N.A02 = r0
            java.lang.String r1 = r0.getPackageName()
            java.lang.String r0 = "com.instagram.android"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0069
            java.lang.String r0 = "com.instagram.android.preload"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0069
            java.lang.String r0 = "com.whatsapp"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0064
            java.lang.String r0 = "com.whatsapp.w4b"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0064
            java.lang.String r0 = "com.expresswifi.customer"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x003a
            X.03O r0 = X.AnonymousClass03N.A07
        L_0x0038:
            X.AnonymousClass03N.A0A = r0
        L_0x003a:
            android.content.BroadcastReceiver r0 = X.AnonymousClass03N.A01
            if (r0 != 0) goto L_0x0051
            X.03P r3 = new X.03P
            r3.<init>()
            X.AnonymousClass03N.A01 = r3
            android.content.Context r2 = X.AnonymousClass03N.A02
            java.lang.String r1 = "android.intent.action.LOCALE_CHANGED"
            android.content.IntentFilter r0 = new android.content.IntentFilter
            r0.<init>(r1)
            r2.registerReceiver(r3, r0)
        L_0x0051:
            android.content.Context r0 = r4.getApplicationContext()
            X.AnonymousClass03Q.A02 = r0
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            float r0 = r0.density
            X.AnonymousClass03Q.A00 = r0
            return
        L_0x0064:
            java.lang.String r0 = "https://graph.whatsapp.net/v2.2/maps_configs?fields=base_url,static_base_url,osm_config,url_override_config&pretty=0&access_token="
            X.AnonymousClass03N.A0C = r0
            goto L_0x003a
        L_0x0069:
            java.lang.String r0 = "https://graph.instagram.com/maps_configs?fields=base_url,static_base_url,osm_config,url_override_config&pretty=0&access_token="
            X.AnonymousClass03N.A0C = r0
            X.03O r0 = X.AnonymousClass03N.A06
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13N.A00(android.content.Context):void");
    }

    public static void A01(AnonymousClass01E r4) {
        C35751ig r3 = new C35751ig(r4.A0C());
        r3.A01 = R.drawable.permission_location;
        r3.A0C = C244415n.A02;
        r3.A0B = new String[]{"android.permission.ACCESS_COARSE_LOCATION"};
        r3.A03 = R.string.permission_location_access_on_searching_businesses;
        r3.A02 = R.string.permission_location_info_on_searching_businesses;
        r4.startActivityForResult(r3.A00(), 34);
    }

    public static void A02(C35761ih r4, C30751Yr r5, Integer num) {
        double d = r5.A00;
        r4.A03();
        C35771ii r3 = (C35771ii) r4.A00;
        r3.A04 |= 1;
        r3.A00 = d;
        double d2 = r5.A01;
        r4.A03();
        C35771ii r32 = (C35771ii) r4.A00;
        r32.A04 |= 2;
        r32.A01 = d2;
        int i = r5.A03;
        if (i != -1) {
            r4.A03();
            C35771ii r1 = (C35771ii) r4.A00;
            r1.A04 |= 4;
            r1.A03 = i;
        }
        float f = r5.A02;
        if (f != -1.0f) {
            r4.A03();
            C35771ii r12 = (C35771ii) r4.A00;
            r12.A04 |= 8;
            r12.A02 = f;
        }
        int i2 = r5.A04;
        if (i2 != -1) {
            r4.A03();
            C35771ii r13 = (C35771ii) r4.A00;
            r13.A04 |= 16;
            r13.A05 = i2;
        }
        if (num != null) {
            int intValue = num.intValue();
            r4.A03();
            C35771ii r14 = (C35771ii) r4.A00;
            r14.A04 |= 128;
            r14.A06 = intValue;
        }
    }

    public static boolean A03(Location location, Location location2) {
        if (location2 == null || location2.getTime() + 120000 < location.getTime() || location2.getAccuracy() > location.getAccuracy() || (TextUtils.equals(location2.getProvider(), location.getProvider()) && location2.distanceTo(location) > Math.max(10.0f, location.getAccuracy()))) {
            return true;
        }
        return false;
    }

    public C27081Fy A04(C30751Yr r5, Integer num) {
        AnonymousClass1G3 A00 = C27081Fy.A00();
        C35771ii r0 = ((C27081Fy) A00.A00).A0P;
        if (r0 == null) {
            r0 = C35771ii.A0B;
        }
        C35761ih r02 = (C35761ih) r0.A0T();
        A02(r02, r5, num);
        A00.A03();
        C27081Fy r2 = (C27081Fy) A00.A00;
        r2.A0P = (C35771ii) r02.A02();
        r2.A00 |= 65536;
        return (C27081Fy) A00.A02();
    }

    public C30751Yr A05(Location location) {
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        C30751Yr r6 = new C30751Yr(r02);
        r6.A00 = ((double) Math.round(location.getLatitude() * 1000000.0d)) / 1000000.0d;
        r6.A01 = ((double) Math.round(location.getLongitude() * 1000000.0d)) / 1000000.0d;
        if (location.hasAccuracy()) {
            r6.A03 = (int) location.getAccuracy();
        }
        if (location.hasSpeed()) {
            r6.A02 = ((float) ((int) (location.getSpeed() * 100.0f))) / 100.0f;
        }
        if (location.hasBearing()) {
            r6.A04 = (int) location.getBearing();
        }
        long time = location.getTime();
        r6.A05 = time;
        C14830m7 r3 = this.A03;
        if (time > r3.A00()) {
            r6.A05 = r3.A00();
        }
        return r6;
    }

    public boolean A06(Context context) {
        boolean z = false;
        if (AnonymousClass1UB.A00(context) == 0) {
            z = true;
        }
        if (z && C35781ij.A00(context) == 0) {
            ActivityManager A03 = this.A02.A03();
            if (A03 == null) {
                Log.w("app/has-google-maps-v2 am=false");
            } else if (A03.getDeviceConfigurationInfo().reqGlEsVersion >= 131072) {
                return true;
            }
        }
        return false;
    }
}
