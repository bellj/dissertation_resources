package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZL implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100344ll();
    public AnonymousClass1ZM A00;
    public String A01;
    public String A02;
    public String A03;
    public final int A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZL(AnonymousClass1ZM r2, String str) {
        this.A03 = str;
        this.A00 = r2;
        this.A04 = 2;
    }

    public AnonymousClass1ZL(Parcel parcel) {
        this.A03 = parcel.readString();
        this.A01 = parcel.readString();
        this.A04 = parcel.readInt();
        this.A02 = parcel.readString();
        this.A00 = (AnonymousClass1ZM) parcel.readParcelable(AnonymousClass1ZM.class.getClassLoader());
    }

    public AnonymousClass1ZL(String str, String str2, String str3, int i) {
        this.A03 = str;
        this.A01 = str2;
        this.A02 = str3;
        this.A04 = i;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A03);
        parcel.writeString(this.A01);
        parcel.writeInt(this.A04);
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A00, i);
    }
}
