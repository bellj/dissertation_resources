package X;

import java.util.List;

/* renamed from: X.0H1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0H1 extends AnonymousClass0H3 {
    public AnonymousClass0H1(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r2, float f) {
        return Float.valueOf(A0A(r2, f));
    }

    public float A09() {
        AnonymousClass0U8 AC6 = this.A06.AC6();
        AnonymousClass0MI.A00();
        return A0A(AC6, A01());
    }

    public float A0A(AnonymousClass0U8 r6, float f) {
        Object obj;
        Object obj2 = r6.A0F;
        if (obj2 == null || (obj = r6.A09) == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        AnonymousClass0SF r1 = this.A03;
        if (r1 != null) {
            r6.A08.floatValue();
            A02();
            AnonymousClass0NB r0 = r1.A02;
            r0.A01 = obj2;
            r0.A00 = obj;
            Number number = (Number) r1.A01;
            if (number != null) {
                return number.floatValue();
            }
        }
        float f2 = r6.A03;
        if (f2 == -3987645.8f) {
            f2 = ((Number) obj2).floatValue();
            r6.A03 = f2;
        }
        float f3 = r6.A01;
        if (f3 == -3987645.8f) {
            f3 = ((Number) r6.A09).floatValue();
            r6.A01 = f3;
        }
        return f2 + (f * (f3 - f2));
    }
}
