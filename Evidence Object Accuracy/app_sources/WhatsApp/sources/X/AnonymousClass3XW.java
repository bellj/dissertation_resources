package X;

import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.whatsapp.registration.report.BanReportViewModel;
import java.io.IOException;

/* renamed from: X.3XW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XW implements AbstractC44401yr {
    public final /* synthetic */ int A00;
    public final /* synthetic */ BanReportViewModel A01;

    public AnonymousClass3XW(BanReportViewModel banReportViewModel, int i) {
        this.A01 = banReportViewModel;
        this.A00 = i;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r22) {
        Object obj;
        AnonymousClass12Z r1 = r22.A02;
        int i = r22.A00;
        Integer A0V = C12960it.A0V();
        if (i == 0 && (obj = r1.A00) != null) {
            AnonymousClass4OF r2 = (AnonymousClass4OF) obj;
            String str = r2.A01;
            if ("AVAILABLE".equals(str)) {
                C91804Te r4 = r2.A00;
                if (r4 != null) {
                    BanReportViewModel banReportViewModel = this.A01;
                    banReportViewModel.A06.A06(new AnonymousClass3Y6(banReportViewModel), C14370lK.A08, null, null, r4.A03, r4.A04, r4.A02, r4.A06, r4.A01, null, Base64.decode(r4.A05, 2), 2, 1, 10, 2, (long) r4.A00);
                    return;
                }
            } else {
                boolean equals = "PENDING".equals(str);
                BanReportViewModel banReportViewModel2 = this.A01;
                if (equals) {
                    int i2 = this.A00 + 1;
                    C12970iu.A0E().postDelayed(new RunnableBRunnable0Shape0S0101000_I0(banReportViewModel2, i2, 20), ((long) i2) * 5000);
                    return;
                }
                AnonymousClass4JO r0 = banReportViewModel2.A04;
                String str2 = banReportViewModel2.A00;
                C44341yl r23 = r0.A00;
                AnonymousClass01J r12 = r23.A03;
                C18790t3 A0U = C12990iw.A0U(r12);
                C14820m6 A0Z = C12970iu.A0Z(r12);
                AnonymousClass01H A00 = C18000rk.A00(r12.AMu);
                C14850m9 A0S = C12960it.A0S(r12);
                C44351ym r02 = r23.A02;
                new C44371yo(A0U, A0Z, A0S, (C19930uu) r12.AM5.get(), A00, str2, r02.A0T, r02.A09).AZO(new AnonymousClass3XU(this));
                return;
            }
        }
        this.A01.A02.A0A(A0V);
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        this.A01.A02.A0A(C12960it.A0V());
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        this.A01.A02.A0A(C12960it.A0V());
    }
}
