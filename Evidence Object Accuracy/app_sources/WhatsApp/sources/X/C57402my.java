package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2my  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57402my extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57402my A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;
    public String A03 = "";

    static {
        C57402my r0 = new C57402my();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57402my r9 = (C57402my) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A01;
                int i3 = r9.A00;
                this.A01 = r8.Afp(i2, r9.A01, A1R, C12960it.A1R(i3));
                this.A03 = r8.Afy(this.A03, r9.A03, C12960it.A1V(i & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A02 = r8.Afr(this.A02, r9.A02);
                if (r8 == C463025i.A00) {
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                int A02 = r82.A02();
                                if (A02 == 0 || A02 == 1) {
                                    this.A00 = 1 | this.A00;
                                    this.A01 = A02;
                                } else {
                                    super.A0X(1, A02);
                                }
                            } else if (A03 == 18) {
                                String A0A = r82.A0A();
                                this.A00 |= 2;
                                this.A03 = A0A;
                            } else if (A03 == 26) {
                                AnonymousClass1K6 r1 = this.A02;
                                if (!((AnonymousClass1K7) r1).A00) {
                                    r1 = AbstractC27091Fz.A0G(r1);
                                    this.A02 = r1;
                                }
                                r1.add((C57392mx) AbstractC27091Fz.A0H(r82, r92, C57392mx.A04));
                            } else if (!A0a(r82, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
                break;
            case 3:
                AbstractC27091Fz.A0R(this.A02);
                return null;
            case 4:
                return new C57402my();
            case 5:
                return new C82303vP();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57402my.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A02(1, this.A01) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            i = AbstractC27091Fz.A04(2, this.A03, i);
        }
        for (int i4 = 0; i4 < this.A02.size(); i4++) {
            i = AbstractC27091Fz.A08((AnonymousClass1G1) this.A02.get(i4), 3, i);
        }
        return AbstractC27091Fz.A07(this, i);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A03);
        }
        int i = 0;
        while (i < this.A02.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A02, i, 3);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
