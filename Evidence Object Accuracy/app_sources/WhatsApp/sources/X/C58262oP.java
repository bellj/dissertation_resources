package X;

import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.View;
import java.util.Map;

/* renamed from: X.2oP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58262oP extends AbstractC52172aN {
    public final /* synthetic */ int A00;
    public final /* synthetic */ URLSpan A01;
    public final /* synthetic */ Map A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C58262oP(URLSpan uRLSpan, Map map, int i) {
        super(0, 0, 0);
        this.A02 = map;
        this.A01 = uRLSpan;
        this.A00 = i;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        Runnable runnable = (Runnable) this.A02.get(this.A01.getURL());
        if (runnable != null) {
            runnable.run();
        }
    }

    @Override // X.AbstractC52172aN, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
        int i = this.A00;
        if (i == 0) {
            i = textPaint.linkColor;
        }
        textPaint.setColor(i);
    }
}
