package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KW  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KW implements Callable {
    public final /* synthetic */ long A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KW(AnonymousClass661 r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        AnonymousClass661 r5 = this.A01;
        long j = this.A00;
        if (r5.A0e) {
            r5.A08();
            r5.A07.A02(AnonymousClass60Q.A0Q, Long.valueOf(j));
            return r5.A07;
        }
        throw C12960it.A0U("Not recording video.");
    }
}
