package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77973oA extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99054jg();
    public final String A00;

    public C77973oA(String str) {
        this.A00 = str;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        C95654e8.A0C(parcel, this.A00, 2, C95654e8.A00(parcel));
    }
}
