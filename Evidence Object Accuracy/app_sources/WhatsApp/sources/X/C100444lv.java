package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100444lv implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1R1(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1R1[i];
    }
}
