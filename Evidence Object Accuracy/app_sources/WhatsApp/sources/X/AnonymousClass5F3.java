package X;

import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5F3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5F3 implements AnonymousClass5WP {
    public final AnonymousClass5L9 A00;
    public volatile /* synthetic */ Object _exceptionsHolder;
    public volatile /* synthetic */ int _isCompleting = 0;
    public volatile /* synthetic */ Object _rootCause;

    public AnonymousClass5F3(Throwable th, AnonymousClass5L9 r3) {
        this.A00 = r3;
        this._rootCause = th;
        this._exceptionsHolder = null;
    }

    public static final ArrayList A00() {
        return C12980iv.A0w(4);
    }

    public final Object A01() {
        return this._exceptionsHolder;
    }

    public final Throwable A02() {
        return (Throwable) this._rootCause;
    }

    public final List A03(Throwable th) {
        AbstractList abstractList;
        Object A01 = A01();
        if (A01 == null) {
            abstractList = A00();
        } else if (A01 instanceof Throwable) {
            abstractList = A00();
            abstractList.add(A01);
        } else if (A01 instanceof ArrayList) {
            abstractList = (AbstractList) A01;
        } else {
            throw C12960it.A0U(C16700pc.A08("State is ", A01));
        }
        Throwable A02 = A02();
        if (A02 != null) {
            abstractList.add(0, A02);
        }
        if (th != null && !th.equals(A02)) {
            abstractList.add(th);
        }
        A05(C93134Zf.A04);
        return abstractList;
    }

    public final void A04() {
        this._isCompleting = 1;
    }

    public final void A05(Object obj) {
        this._exceptionsHolder = obj;
    }

    public final void A06(Throwable th) {
        Throwable A02 = A02();
        if (A02 == null) {
            A07(th);
        } else if (th != A02) {
            Object A01 = A01();
            if (A01 == null) {
                A05(th);
            } else if (A01 instanceof Throwable) {
                if (th != A01) {
                    ArrayList A00 = A00();
                    A00.add(A01);
                    A00.add(th);
                    A05(A00);
                }
            } else if (A01 instanceof ArrayList) {
                ((AbstractCollection) A01).add(th);
            } else {
                throw C12960it.A0U(C16700pc.A08("State is ", A01));
            }
        }
    }

    public final void A07(Throwable th) {
        this._rootCause = th;
    }

    public final boolean A08() {
        return C12960it.A1W(A02());
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [boolean, int] */
    public final boolean A09() {
        return this._isCompleting;
    }

    public final boolean A0A() {
        return C12970iu.A1Z(A01(), C93134Zf.A04);
    }

    @Override // X.AnonymousClass5WP
    public AnonymousClass5L9 ADu() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WP
    public boolean AJD() {
        return C12980iv.A1X(A02());
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Finishing[cancelling=");
        A0k.append(A08());
        A0k.append(", completing=");
        A0k.append(A09());
        A0k.append(", rootCause=");
        A0k.append(A02());
        A0k.append(", exceptions=");
        A0k.append(A01());
        A0k.append(", list=");
        A0k.append(ADu());
        return C72453ed.A0t(A0k);
    }
}
