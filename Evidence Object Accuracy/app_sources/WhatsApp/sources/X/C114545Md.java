package X;

/* renamed from: X.5Md  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114545Md extends AnonymousClass1TM {
    public AbstractC114775Na A00;
    public AnonymousClass5MA A01;
    public C114665Mp A02;
    public C114725Mv A03;

    public C114545Md(AbstractC114775Na r4) {
        AnonymousClass1TN A00 = AbstractC114775Na.A00(r4);
        this.A02 = A00 instanceof C114665Mp ? (C114665Mp) A00 : A00 != null ? new C114665Mp(AbstractC114775Na.A04(A00)) : null;
        this.A03 = C114725Mv.A00(r4.A0D(1));
        this.A01 = (AnonymousClass5MA) r4.A0D(2);
        if (r4.A0B() > 3) {
            this.A00 = AbstractC114775Na.A05((AnonymousClass5NU) r4.A0D(3), true);
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(4);
        r3.A06(this.A02);
        r3.A06(this.A03);
        r3.A06(this.A01);
        AbstractC114775Na r2 = this.A00;
        if (r2 != null) {
            C94954co.A02(r2, r3, 0, true);
        }
        return new AnonymousClass5NZ(r3);
    }
}
