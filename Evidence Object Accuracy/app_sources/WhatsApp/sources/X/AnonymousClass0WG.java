package X;

import android.view.View;
import androidx.preference.Preference;

/* renamed from: X.0WG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WG implements View.OnClickListener {
    public final /* synthetic */ Preference A00;

    public AnonymousClass0WG(Preference preference) {
        this.A00 = preference;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        this.A00.A0E(view);
    }
}
