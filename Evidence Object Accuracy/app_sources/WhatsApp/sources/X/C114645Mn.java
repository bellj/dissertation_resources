package X;

import java.util.Enumeration;

/* renamed from: X.5Mn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114645Mn extends AnonymousClass1TM {
    public static final C114725Mv A04 = new C114725Mv(AnonymousClass5ME.A00, AnonymousClass1TJ.A1E);
    public final AnonymousClass5NG A00;
    public final AnonymousClass5NG A01;
    public final AnonymousClass5NH A02;
    public final C114725Mv A03;

    public C114645Mn(AbstractC114775Na r5) {
        Enumeration A0C = r5.A0C();
        this.A02 = (AnonymousClass5NH) A0C.nextElement();
        this.A00 = (AnonymousClass5NG) A0C.nextElement();
        if (A0C.hasMoreElements()) {
            Object nextElement = A0C.nextElement();
            if (nextElement instanceof AnonymousClass5NG) {
                this.A01 = AnonymousClass5NG.A00(nextElement);
                nextElement = A0C.hasMoreElements() ? A0C.nextElement() : nextElement;
            } else {
                this.A01 = null;
            }
            if (nextElement != null) {
                this.A03 = C114725Mv.A00(nextElement);
                return;
            }
        } else {
            this.A01 = null;
        }
        this.A03 = null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r2 = new C94954co(4);
        r2.A06(this.A02);
        r2.A06(this.A00);
        AnonymousClass5NG r0 = this.A01;
        if (r0 != null) {
            r2.A06(r0);
        }
        C114725Mv r1 = this.A03;
        if (r1 != null && !r1.equals(A04)) {
            r2.A06(r1);
        }
        return new AnonymousClass5NZ(r2);
    }

    public C114645Mn(byte[] bArr, int i) {
        this.A02 = new AnonymousClass5N5(AnonymousClass1TT.A02(bArr));
        this.A00 = new AnonymousClass5NG((long) i);
        this.A01 = null;
        this.A03 = null;
    }
}
