package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import java.util.ArrayList;

/* renamed from: X.607  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass607 {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final AnonymousClass20C A05;
    public final AnonymousClass20C A06;
    public final C38171nd A07;
    public final C14850m9 A08;
    public final UserJid A09;
    public final UserJid A0A;
    public final C18650sn A0B;
    public final C18610sj A0C;
    public final C17070qD A0D;
    public final C17070qD A0E;
    public final C129135xE A0F;
    public final C129215xM A0G;
    public final AnonymousClass60T A0H;
    public final C30931Zj A0I = C117305Zk.A0V("PaymentPrecheckAction", "network");
    public final C129385xd A0J;
    public final AnonymousClass17Z A0K;
    public final C50952Rz A0L;
    public final AnonymousClass61E A0M;
    public final C130015yf A0N;
    public final C130775zx A0O;
    public final C18590sh A0P;
    public final AbstractC14440lR A0Q;
    public final String A0R;
    public final String A0S;
    public final String A0T;
    public final String A0U;
    public final String A0V;
    public final String A0W;
    public final String A0X;

    public AnonymousClass607(Context context, C14900mE r13, C15570nT r14, C18640sm r15, C14830m7 r16, AnonymousClass20C r17, AnonymousClass20C r18, C38171nd r19, C14850m9 r20, UserJid userJid, C18650sn r22, C18610sj r23, C17070qD r24, C129135xE r25, AnonymousClass60T r26, C129385xd r27, AnonymousClass17Z r28, C50952Rz r29, AnonymousClass61E r30, C130015yf r31, C18590sh r32, AbstractC14440lR r33, String str, String str2, String str3, String str4, String str5) {
        this.A04 = r16;
        this.A08 = r20;
        this.A00 = context;
        this.A01 = r13;
        this.A02 = r14;
        this.A0Q = r33;
        this.A0F = r25;
        this.A0P = r32;
        this.A0D = r24;
        this.A0N = r31;
        this.A0C = r23;
        this.A0K = r28;
        this.A03 = r15;
        this.A0B = r22;
        this.A0M = r30;
        this.A0H = r26;
        this.A0O = new C130775zx(r14, r16, r23);
        this.A0G = new C129215xM(context, r13, r22, r23, r26, "PIN");
        this.A0R = str;
        r14.A08();
        this.A0A = r14.A05;
        this.A09 = userJid;
        this.A05 = r17;
        this.A06 = r18;
        this.A0W = str4;
        this.A0S = "BR";
        this.A0U = "FB";
        this.A0L = r29;
        this.A0T = str3;
        this.A0V = str2;
        this.A07 = r19;
        this.A0E = r24;
        this.A0J = r27;
        this.A0X = str5;
    }

    public static /* synthetic */ void A00(AnonymousClass607 r10, AbstractC1311361k r11, AnonymousClass1V8 r12, String str) {
        AbstractC16830pp r3;
        ArrayList A0l = C12960it.A0l();
        C117295Zj.A1M("action", "pay-precheck", A0l);
        String str2 = r10.A0S;
        C117295Zj.A1M("country", str2, A0l);
        C117295Zj.A1M("credential-id", r10.A0R, A0l);
        C117295Zj.A1M("nonce", str, A0l);
        A0l.add(new AnonymousClass1W9(r10.A09, "receiver"));
        C117295Zj.A1M("device-id", r10.A0P.A01(), A0l);
        String str3 = r10.A0W;
        C117295Zj.A1M("transaction-type", str3, A0l);
        if (r10.A08.A07(1746) && (!"p2m".equals(str3) || r10.A07 == null)) {
            C117295Zj.A1M("payment_initiator", "buyer", A0l);
        }
        C50952Rz r0 = r10.A0L;
        if (r0 != null) {
            A0l.add(new AnonymousClass1W9("offer_id", r0.A01));
        }
        String str4 = r10.A0T;
        if (!TextUtils.isEmpty(str4)) {
            C117295Zj.A1M("payment-rails", str4, A0l);
        }
        String str5 = r10.A0V;
        if (!TextUtils.isEmpty(str5)) {
            C117295Zj.A1M("request-id", str5, A0l);
        }
        AbstractC38041nQ A01 = r10.A0D.A01(str2);
        if (A01 != null) {
            r3 = A01.AFX(((AbstractC30781Yu) r10.A05.A01).A04);
        } else {
            r3 = null;
        }
        AnonymousClass1V8 r7 = new AnonymousClass1V8(r3.AEY(r10.A05), "amount", new AnonymousClass1W9[0]);
        AnonymousClass1V8 r5 = new AnonymousClass1V8(r3.AEY(r10.A06), "total-amount", new AnonymousClass1W9[0]);
        C38171nd r4 = r10.A07;
        AnonymousClass1V8[] r1 = r4 == null ? new AnonymousClass1V8[]{r12, r7, r5} : new AnonymousClass1V8[]{r12, r7, r5, r4.A00()};
        r11.AKb();
        C117305Zk.A1J(r10.A0C, new IDxRCallbackShape0S0200000_3_I1(r10.A00, r10.A01, r10.A0B, r11, r10, 12), C117295Zj.A0L(A0l, r1));
    }

    public final void A01(AnonymousClass02N r11, C1323666p r12, C128545wH r13) {
        C14830m7 r1 = this.A04;
        String A0U = C117295Zj.A0U(this.A02, r1);
        long A03 = C117295Zj.A03(r1);
        Object[] objArr = new Object[6];
        objArr[0] = C248917h.A03(this.A09);
        AnonymousClass20C r3 = this.A05;
        objArr[1] = Integer.valueOf(r3.A01());
        objArr[2] = Integer.valueOf(r3.A00);
        objArr[3] = ((AbstractC30781Yu) r3.A01).A04;
        C117305Zk.A1M(Long.valueOf(A03), A0U, objArr);
        if (!this.A0M.A08(r11, new C1323466n(this, r12, r13, A0U, A03), AnonymousClass605.A00(objArr))) {
            r12.A01();
        }
    }
}
