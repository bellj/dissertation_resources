package X;

import android.view.View;
import com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0HK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HK extends ViewDragHelper$Callback {
    public final /* synthetic */ AnonymousClass0BC A00;

    public /* synthetic */ AnonymousClass0HK(AnonymousClass0BC r1) {
        this.A00 = r1;
    }

    @Override // com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback
    public int A00(View view) {
        return this.A00.getHeight();
    }

    @Override // com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback
    public int A01(View view, int i, int i2) {
        if (this.A00.A03 != null) {
            return (int) (((float) i2) * 0.15f);
        }
        return i2;
    }

    @Override // com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback
    public int A02(View view, int i, int i2) {
        int AFn;
        int AFn2;
        AnonymousClass0BC r1 = this.A00;
        if (r1.A0G == null) {
            return i;
        }
        int height = r1.getHeight();
        AbstractC12080hL[] r8 = r1.A0G;
        AbstractC12080hL r4 = null;
        AbstractC12080hL r3 = null;
        for (AbstractC12080hL r2 : r8) {
            if (r4 == null) {
                r4 = r2;
            } else {
                int AFn3 = r2.AFn(view, height);
                if (AFn3 >= r3.AFn(view, height)) {
                    if (AFn3 > r4.AFn(view, height)) {
                        r4 = r2;
                    }
                }
            }
            r3 = r2;
        }
        if (r4 == null) {
            AFn = i;
        } else {
            AFn = r4.AFn(view, height);
        }
        if (r3 == null) {
            AFn2 = i;
        } else {
            AFn2 = r3.AFn(view, height);
        }
        return height - Math.max(AFn2, Math.min(AFn, height - i));
    }

    @Override // com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback
    public void A03(int i) {
        AbstractC12080hL r2;
        AnonymousClass0BC r4 = this.A00;
        View view = r4.A02;
        if (view != null && AnonymousClass028.A0r(view) && r4.A04 != null && i == 0) {
            List emptyList = Collections.emptyList();
            View view2 = r4.A02;
            if (view2 == null || !AnonymousClass028.A0r(view2)) {
                r2 = null;
            } else {
                int height = r4.getHeight();
                r2 = r4.A00(view2, emptyList, height - view2.getTop(), height);
            }
            r4.A06 = r2;
            AnonymousClass09V r1 = r4.A04.A00;
            r1.A09.A07.A03();
            if (r2 == AnonymousClass09V.A0H) {
                if (!r1.A0D) {
                    r1.A03(EnumC03710Iv.SWIPE_AWAY);
                }
                r1.A00();
            }
        }
    }

    @Override // com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback
    public void A04(View view, float f, float f2) {
        if (view != null) {
            AnonymousClass0BC r3 = this.A00;
            int height = r3.getHeight();
            C06210Sp r1 = r3.A07;
            r1.A0A.abortAnimation();
            r1.A0A.fling(0, 0, 0, (int) f2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
            int finalY = r1.A0A.getFinalY();
            r1.A0A.abortAnimation();
            int top = height - (view.getTop() + finalY);
            List list = r3.A08;
            if (list == null) {
                list = Collections.emptyList();
            }
            AbstractC12080hL A00 = r3.A00(view, list, top, height);
            if (A00 != null) {
                r3.A06 = A00;
                int AFn = height - A00.AFn(view, height);
                try {
                    int i = r3.A00;
                    if (r1.A0B) {
                        r1.A07.getXVelocity(r1.A02);
                        r1.A0A(AFn, (int) r1.A07.getYVelocity(r1.A02), i);
                        r3.postInvalidateOnAnimation();
                        return;
                    }
                    throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
                } catch (NullPointerException unused) {
                }
            }
        }
    }

    @Override // com.bloks.foa.components.bottomsheet.ViewDragHelper$Callback
    public void A05(View view, int i, int i2, int i3, int i4) {
        AnonymousClass0BC r0 = this.A00;
        AnonymousClass0P3 r1 = r0.A04;
        if (r1 != null) {
            r1.A00(view, r0.getHeight());
        }
    }
}
