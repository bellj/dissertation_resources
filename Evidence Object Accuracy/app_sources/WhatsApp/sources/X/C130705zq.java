package X;

import android.os.Build;
import com.whatsapp.util.Log;
import java.security.KeyPair;
import java.security.KeyStore;

/* renamed from: X.5zq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130705zq {
    public final C136036Kt A00 = new C136036Kt(new AnonymousClass01N() { // from class: X.6Kq
        @Override // X.AnonymousClass01N, X.AnonymousClass01H
        public final Object get() {
            return C130705zq.A00();
        }
    });

    public static /* synthetic */ C130005ye A00() {
        KeyStore keyStore;
        Exception e;
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 18) {
            z = true;
        }
        if (!z) {
            return null;
        }
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e2) {
            e = e2;
            keyStore = null;
        }
        try {
            keyStore.load(null);
        } catch (Exception e3) {
            e = e3;
            Log.w(C12960it.A0b("PAY: TrustedDeviceKeyStore keystore load threw: ", e));
            if (keyStore == null) {
                return null;
            }
            return new C130005ye(keyStore);
        }
        return new C130005ye(keyStore);
    }

    public AnonymousClass01T A01() {
        KeyPair A0o;
        Boolean bool;
        C130005ye r2 = (C130005ye) this.A00.get();
        if (Build.VERSION.SDK_INT < 23 || r2 == null || (A0o = r2.A00()) == null) {
            A0o = C117305Zk.A0o();
            bool = Boolean.FALSE;
        } else {
            bool = Boolean.TRUE;
        }
        return C117315Zl.A05(A0o, bool);
    }
}
