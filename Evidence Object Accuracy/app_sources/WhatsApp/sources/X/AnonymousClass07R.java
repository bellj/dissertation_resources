package X;

import java.util.Comparator;

/* renamed from: X.07R  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass07R implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((AnonymousClass07U) obj).A02 - ((AnonymousClass07U) obj2).A02;
    }
}
