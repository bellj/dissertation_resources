package X;

import android.os.Bundle;
import android.util.Base64;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.5bJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118125bJ extends AnonymousClass015 {
    public String A00;
    public final Bundle A01;
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final C130155yt A04;
    public final C128715wY A05;
    public final C130125yq A06;
    public final AnonymousClass60Y A07;
    public final AnonymousClass61F A08;
    public final C1316663q A09;
    public final C129445xj A0A = new C129445xj(this);
    public final C27691It A0B = C13000ix.A03();
    public final List A0C = C12960it.A0l();
    public final List A0D = C12960it.A0l();

    public C118125bJ(Bundle bundle, C15570nT r3, C130155yt r4, C130125yq r5, AnonymousClass60Y r6, AnonymousClass61F r7, C22980zx r8) {
        this.A01 = bundle;
        this.A04 = r4;
        this.A06 = r5;
        this.A08 = r7;
        this.A07 = r6;
        this.A05 = new C128715wY(r3, r8);
        this.A09 = (C1316663q) C117315Zl.A03(bundle, "step_up");
    }

    public static /* synthetic */ void A00(C1315463e r13, C118125bJ r14) {
        String encodeToString;
        if (r13 != null) {
            AnonymousClass61S[] r3 = new AnonymousClass61S[2];
            C1316663q r2 = r14.A09;
            r3[0] = AnonymousClass61S.A00("entry_flow", r2.A03);
            C1310460z A0B = C117315Zl.A0B("step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", r2.A04), r3, 1));
            C1310460z A01 = AnonymousClass61S.A01("novi-answer-text-input-step-up-challenge");
            ArrayList arrayList = A01.A02;
            arrayList.add(A0B);
            for (C127455uW r9 : r14.A0C) {
                String str = r9.A02;
                AnonymousClass009.A05(str);
                C130125yq r10 = r14.A06;
                String str2 = r13.A02;
                C127675us A012 = r10.A00.A01("RISK");
                byte[] bytes = str2.getBytes();
                if (A012 == null) {
                    encodeToString = null;
                } else {
                    encodeToString = Base64.encodeToString(r10.A01.A02(A012, str.getBytes(), AnonymousClass61L.A03(bytes), true), 2);
                }
                AnonymousClass61S[] r22 = new AnonymousClass61S[2];
                AnonymousClass61S.A05("challenge_id", r9.A00.A01, r22, 0);
                AnonymousClass009.A05(encodeToString);
                C117305Zk.A1R("responses", arrayList, C12960it.A0m(AnonymousClass61S.A00("encrypted_answer", encodeToString), r22, 1));
            }
            int i = r14.A01.getInt("step_up_origin_action");
            C130155yt.A04(new IDxAListenerShape19S0100000_3_I1(r14, 15), r14.A04, A01, i, 4);
        }
    }

    public void A04(AnonymousClass610 r4) {
        C1316663q r2 = this.A09;
        String str = r2.A02;
        C128365vz r1 = r4.A00;
        r1.A0E = str;
        r1.A0f = r2.A03;
        r1.A0g = "STEP_UP_TEXT_INPUT";
        if (r1.A0k == null) {
            r1.A0k = this.A00;
        }
        this.A07.A05(r1);
    }

    public void A05(C126065sH r8) {
        int i = r8.A00;
        if (i == 0) {
            int i2 = this.A01.getInt("step_up_origin_action");
            C1316663q r4 = this.A09;
            AnonymousClass61D.A01(new IDxAListenerShape19S0100000_3_I1(this, 16), this.A04, this.A05, r4, "TEXT_INPUT", i2);
        } else if (i == 1) {
            this.A08.A0B(new AbstractC136286Ly() { // from class: X.6BK
                @Override // X.AbstractC136286Ly
                public final void AT6(C1315463e r2) {
                    C118125bJ.A00(r2, C118125bJ.this);
                }
            });
            A04(new AnonymousClass610("TEXT_INPUT_SUBMIT_CLICK", "TEXT_INPUT", "BUTTON"));
        }
    }
}
