package X;

import dalvik.system.DexFile;
import java.io.File;
import java.lang.reflect.Constructor;

/* renamed from: X.0Yt  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yt implements AbstractC11800gt {
    public final Constructor A00;

    public AnonymousClass0Yt(Class cls) {
        Constructor constructor = cls.getConstructor(File.class, File.class, DexFile.class);
        this.A00 = constructor;
        constructor.setAccessible(true);
    }

    @Override // X.AbstractC11800gt
    public Object ALe(DexFile dexFile, File file) {
        return this.A00.newInstance(file, file, dexFile);
    }
}
