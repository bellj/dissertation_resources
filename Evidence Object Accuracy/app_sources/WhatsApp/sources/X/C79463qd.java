package X;

/* renamed from: X.3qd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79463qd extends C79503qh implements Cloneable {
    public static volatile C79463qd[] A00;

    public C79463qd() {
        ((C79503qh) this).A00 = null;
        ((AbstractC94974cq) this).A00 = -1;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final int A03() {
        int A03 = super.A03();
        return !"".equals("") ? A03 + C95484do.A00(1) + C95484do.A00(2) : A03;
    }

    @Override // X.C79503qh, X.AbstractC94974cq
    public final void A05(C95484do r3) {
        if (!"".equals("")) {
            r3.A07(1, "");
            r3.A07(2, "");
        }
        super.A05(r3);
    }

    @Override // X.AbstractC94974cq, java.lang.Object
    public final /* synthetic */ Object clone() {
        try {
            return super.A06();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof C79463qd) {
                C79503qh r5 = (C79503qh) obj;
                if ("".equals("")) {
                    AnonymousClass5BY r1 = ((C79503qh) this).A00;
                    if (r1 != null && r1.A00 != 0) {
                        return r1.equals(r5.A00);
                    }
                    AnonymousClass5BY r0 = r5.A00;
                    if (r0 == null || r0.A00 == 0) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = 0;
        int hashCode = "".hashCode();
        int hashCode2 = (((((C79463qd.class.getName().hashCode() + 527) * 31) + hashCode) * 31) + hashCode) * 31;
        AnonymousClass5BY r1 = ((C79503qh) this).A00;
        if (!(r1 == null || r1.A00 == 0)) {
            i = r1.hashCode();
        }
        return hashCode2 + i;
    }
}
