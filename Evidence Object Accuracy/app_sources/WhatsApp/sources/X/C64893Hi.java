package X;

import android.animation.Animator;
import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import com.facebook.rendercore.RootHostView;
import com.whatsapp.R;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.3Hi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64893Hi {
    public RootHostView A00;
    public Map A01;
    public final AnonymousClass3HL A02;
    public final C14260l7 A03;
    public final AnonymousClass4MY A04;
    public final C14270l8 A05;
    public final List A06;
    public final AtomicBoolean A07 = new AtomicBoolean(false);
    public final AtomicReference A08;

    public /* synthetic */ C64893Hi(Context context, SparseArray sparseArray, AnonymousClass3JI r11, C64173En r12, Map map, Map map2) {
        AtomicReference atomicReference = new AtomicReference();
        this.A08 = atomicReference;
        C93634aU r6 = r11.A00;
        List list = r6.A01;
        this.A06 = list;
        C88824Hg.A01.incrementAndGet();
        C1093551j r5 = C1093551j.A00;
        C14260l7 A01 = AnonymousClass3JV.A01(context, sparseArray, new C14270l8(r6, r11.A01, new AnonymousClass4ZD(), r5), r12, r11.A02);
        this.A03 = A01;
        C14270l8 A03 = AnonymousClass3JV.A03(A01);
        this.A05 = A03;
        this.A01 = AnonymousClass3JI.A04(list, map);
        AnonymousClass4MY r2 = new AnonymousClass4MY(A03);
        this.A04 = r2;
        A01.A01.get(R.id.bk_context_key_rendercore_extensions_creator);
        this.A02 = new AnonymousClass3HL(context, r2, A01);
        C92554Wj A012 = A03.A01(A01, new AnonymousClass51X(this), map2);
        atomicReference.compareAndSet(null, A012);
        A06(A012);
    }

    public static AnonymousClass4Y0 A00(Context context, AnonymousClass3JI r2, C64173En r3) {
        return new AnonymousClass4Y0(context, r2, r3);
    }

    public View A01() {
        return this.A00;
    }

    public C14260l7 A02() {
        if (this.A07.get()) {
            C28691Op.A00("BloksHostingComponent", "Trying to access a BloksContext form a destroyed BloksHostingComponent");
        }
        return this.A03;
    }

    public void A03() {
        A04();
        C14260l7 A02 = A02();
        HashMap hashMap = (HashMap) A02.A02(R.id.bk_context_key_animations);
        Iterator A0t = C12990iw.A0t((AbstractMap) hashMap.clone());
        while (A0t.hasNext()) {
            ((Animator) A0t.next()).cancel();
        }
        if (!hashMap.isEmpty()) {
            C28691Op.A00("BloksAnimation", "Animator map is non-empty after cleanup!");
            hashMap.clear();
        }
        HashMap hashMap2 = (HashMap) A02.A02(R.id.bk_context_key_timers);
        Iterator A0t2 = C12990iw.A0t((AbstractMap) hashMap2.clone());
        while (A0t2.hasNext()) {
            ((C14190l0) A0t2.next()).A01();
        }
        if (!hashMap2.isEmpty()) {
            C28691Op.A00("BloksTimer", "Timer map is non-empty after cleanup!");
            hashMap2.clear();
        }
        C14270l8 A03 = AnonymousClass3JV.A03(A02);
        A03.A09 = true;
        A03.A01.A00();
        this.A07.set(true);
        C88824Hg.A02.incrementAndGet();
    }

    public void A04() {
        if (this.A07.get()) {
            C28691Op.A00("BloksHostingComponent", "Trying to detach a view from a destroyed BloksHostingComponent");
            return;
        }
        RootHostView rootHostView = this.A00;
        if (rootHostView != null) {
            rootHostView.setRenderState(null);
            this.A00 = null;
        }
    }

    public void A05(RootHostView rootHostView) {
        if (this.A07.get()) {
            C28691Op.A00("BloksHostingComponent", "Trying to attach a view to a destroyed BloksHostingComponent");
            return;
        }
        RootHostView rootHostView2 = this.A00;
        if (!(rootHostView2 == null || rootHostView2 == rootHostView)) {
            A04();
        }
        this.A00 = rootHostView;
        rootHostView.setRenderState(this.A02);
        this.A00.requestLayout();
    }

    public final void A06(C92554Wj r13) {
        int i;
        boolean z;
        if (!this.A07.get()) {
            AnonymousClass3SS r7 = new AnonymousClass3SS(this.A03, this.A04.A00, r13, this.A01);
            AnonymousClass3HL r1 = this.A02;
            synchronized (r1) {
                if (r1.A01 <= -1) {
                    AnonymousClass3IP r6 = r1.A05;
                    r1.A01 = -1;
                    r1.A07 = r7;
                    int i2 = r1.A04;
                    if (i2 != -1 && (i = r1.A02) != -1) {
                        int i3 = r1.A03;
                        r1.A03 = i3 + 1;
                        AnonymousClass3D8 r4 = new AnonymousClass3D8(r1.A0A, r6, r7, r1.A0D, i3, i2, i);
                        r1.A06 = r4;
                        AnonymousClass3IP A00 = r4.A00();
                        synchronized (r1) {
                            if (i3 > r1.A00) {
                                r1.A00 = i3;
                                r1.A05 = A00;
                                z = true;
                            } else {
                                z = false;
                            }
                            if (r1.A06 == r4) {
                                r1.A06 = null;
                            }
                        }
                        if (!z) {
                            return;
                        }
                        if (AnonymousClass3J3.A03()) {
                            r1.A00();
                            return;
                        }
                        HandlerC52012a0 r2 = r1.A0B;
                        if (!r2.hasMessages(99)) {
                            r2.sendEmptyMessage(99);
                            return;
                        }
                        return;
                    }
                    return;
                }
                throw C12960it.A0U("Setting an unversioned tree after calling setVersionedTree is not supported. If this RenderState takes its version from a parent tree make sure to always call setVersionedTree");
            }
        }
    }
}
