package X;

/* renamed from: X.2Dw  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2Dw {
    public final AbstractC14640lm A00;
    public final AbstractC14640lm A01;
    public final String A02;
    public final String A03;
    public final byte[] A04;
    public final byte[] A05;

    public AnonymousClass2Dw(AbstractC14640lm r1, AbstractC14640lm r2, String str, String str2, byte[] bArr, byte[] bArr2) {
        this.A01 = r1;
        this.A00 = r2;
        this.A03 = str;
        this.A02 = str2;
        this.A04 = bArr;
        this.A05 = bArr2;
    }
}
