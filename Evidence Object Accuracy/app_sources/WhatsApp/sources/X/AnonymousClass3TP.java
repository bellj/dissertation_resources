package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import java.util.concurrent.Executor;

/* renamed from: X.3TP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3TP implements AbstractC13620k1, AbstractC13630k2, AbstractC13640k3, AbstractC13670k8 {
    public final AnonymousClass5R1 A00;
    public final C13600jz A01;
    public final Executor A02;

    public AnonymousClass3TP(AnonymousClass5R1 r1, C13600jz r2, Executor executor) {
        this.A02 = executor;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC13620k1
    public final void ANf() {
        this.A01.A02();
    }

    @Override // X.AbstractC13630k2
    public final void AQA(Exception exc) {
        this.A01.A07(exc);
    }

    @Override // X.AbstractC13640k3
    public final void AX4(Object obj) {
        this.A01.A08(obj);
    }

    @Override // X.AbstractC13670k8
    public final void Agv(C13600jz r4) {
        this.A02.execute(new RunnableBRunnable0Shape10S0200000_I1(r4, 25, this));
    }
}
