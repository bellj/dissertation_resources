package X;

/* renamed from: X.318  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass318 extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public String A07;
    public String A08;

    public AnonymousClass318() {
        super(1650, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A02);
        r3.Abe(3, this.A03);
        r3.Abe(9, this.A07);
        r3.Abe(2, this.A00);
        r3.Abe(7, this.A04);
        r3.Abe(6, this.A05);
        r3.Abe(5, this.A06);
        r3.Abe(8, this.A01);
        r3.Abe(1, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamGifBatchLoad {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchConnectionDownloadT", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchConnectionSetupT", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchErrorMessage", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchEventType", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchHttpCode", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchOverallT", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchParseResponseT", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifBatchResult", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "gifProvider", this.A08);
        return C12960it.A0d("}", A0k);
    }
}
