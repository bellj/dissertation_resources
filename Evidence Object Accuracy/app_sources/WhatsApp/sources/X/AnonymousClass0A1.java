package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* renamed from: X.0A1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0A1 extends Drawable {
    public boolean A00;
    public final Paint A01;
    public final Path A02 = new Path();
    public final RectF A03;
    public final float[] A04;

    public AnonymousClass0A1() {
        Paint paint = new Paint();
        this.A01 = paint;
        this.A03 = new RectF();
        this.A04 = new float[8];
        this.A00 = true;
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (this.A00) {
            RectF rectF = this.A03;
            float f = this.A04[0];
            canvas.drawRoundRect(rectF, f, f, this.A01);
            return;
        }
        canvas.drawPath(this.A02, this.A01);
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        float[] fArr = this.A04;
        int length = fArr.length;
        int i = 0;
        while (true) {
            if (i < length) {
                if (Float.compare(fArr[i], 0.0f) == 0) {
                    break;
                }
                i++;
            } else if (this.A01.getAlpha() != 255) {
                break;
            } else {
                return -1;
            }
        }
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        RectF rectF = this.A03;
        rectF.set(rect);
        if (!this.A00) {
            Path path = this.A02;
            path.reset();
            path.addRoundRect(rectF, this.A04, Path.Direction.CW);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        Paint paint = this.A01;
        if (i != paint.getAlpha()) {
            paint.setAlpha(i);
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.A01.setColorFilter(colorFilter);
        invalidateSelf();
    }
}
