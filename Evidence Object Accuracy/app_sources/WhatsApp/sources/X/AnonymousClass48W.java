package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.48W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass48W extends AnonymousClass1JW {
    public final UserJid A00;

    public AnonymousClass48W(AbstractC15710nm r2, C15450nH r3, UserJid userJid) {
        super(r2, r3);
        this.A05 = 21;
        this.A00 = userJid;
    }
}
