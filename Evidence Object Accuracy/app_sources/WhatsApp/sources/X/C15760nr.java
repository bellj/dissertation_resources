package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Random;

/* renamed from: X.0nr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15760nr {
    public final C19540uH A00;
    public final C19530uG A01;
    public final AnonymousClass01H A02;
    public final ThreadLocal A03 = new ThreadLocal();

    public C15760nr(C19540uH r2, C19530uG r3, AnonymousClass01H r4) {
        this.A02 = r4;
        this.A01 = r3;
        this.A00 = r2;
    }

    public long A00(File file, String str, boolean z) {
        C19530uG r3 = this.A01;
        String canonicalPath = file.getCanonicalPath();
        long length = file.length();
        ThreadLocal threadLocal = this.A03;
        byte[] bArr = (byte[]) threadLocal.get();
        if (bArr == null) {
            bArr = new byte[16];
            threadLocal.set(bArr);
        }
        ((Random) this.A02.get()).nextBytes(bArr);
        return r3.A01(canonicalPath, str, Base64.encodeToString(bArr, 2), length, z);
    }

    public void A01() {
        AnonymousClass113 r0;
        C19540uH r1 = this.A01.A00;
        synchronized (r1) {
            r0 = r1.A00;
            if (r0 == null) {
                r0 = (AnonymousClass113) r1.A02.get();
                r1.A00 = r0;
            }
        }
        C16310on A02 = r0.A02();
        try {
            A02.A03.A01("exported_files_metadata", null, null);
            A02.close();
            C19540uH r2 = this.A00;
            synchronized (r2) {
                AnonymousClass113 r02 = r2.A00;
                if (r02 != null) {
                    r02.close();
                    r2.A00 = null;
                }
                r2.A01.deleteDatabase("migration_export_metadata.db");
                Log.i("ExportMetadata/removeDatabase/deleted");
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
