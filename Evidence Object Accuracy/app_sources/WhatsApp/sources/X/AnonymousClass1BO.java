package X;

import android.content.SharedPreferences;
import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import java.util.Set;

/* renamed from: X.1BO  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1BO {
    public final AbstractC15710nm A00;
    public final C17220qS A01;
    public final AnonymousClass1BM A02;
    public final AbstractC14440lR A03;
    public final String A04;

    public AnonymousClass1BO(AbstractC15710nm r1, C17220qS r2, AnonymousClass1BM r3, AbstractC14440lR r4, String str) {
        this.A04 = str;
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    public C27691It A00() {
        int i;
        C27691It r2 = new C27691It();
        C17220qS r7 = this.A01;
        String A01 = r7.A01();
        AnonymousClass1V8 r9 = new AnonymousClass1V8(new AnonymousClass1V8(new AnonymousClass1V8("list", AnonymousClass1BM.A00(this.A04, A02())), "privacy", (AnonymousClass1W9[]) null), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("xmlns", "privacy"), new AnonymousClass1W9("type", "get")});
        if (!(this instanceof AnonymousClass2I8)) {
            i = !(this instanceof AnonymousClass2I5) ? !(this instanceof AnonymousClass1BN) ? 293 : 227 : 288;
        } else {
            i = 291;
        }
        r7.A0A(new AnonymousClass3Z1(this, r2), r9, A01, i, 32000);
        return r2;
    }

    public C27691It A01(Set set) {
        C27691It r3 = new C27691It();
        this.A03.Ab2(new RunnableBRunnable0Shape1S0300000_I0_1(this, set, r3, 26));
        return r3;
    }

    public String A02() {
        SharedPreferences sharedPreferences;
        String str;
        if (this instanceof AnonymousClass2I8) {
            sharedPreferences = ((AnonymousClass2I8) this).A01.A00;
            str = "profile_photo_block_list_hash";
        } else if (this instanceof AnonymousClass2I5) {
            sharedPreferences = ((AnonymousClass2I5) this).A01.A00;
            str = "last_seen_block_list_hash";
        } else if (!(this instanceof AnonymousClass1BN)) {
            sharedPreferences = ((AnonymousClass2I0) this).A01.A00;
            str = "about_block_list_hash";
        } else {
            sharedPreferences = ((AnonymousClass1BN) this).A01.A00;
            str = "group_add_blacklist_hash";
        }
        return sharedPreferences.getString(str, null);
    }

    public Set A03() {
        C16310on r2;
        Set set;
        if (this instanceof AnonymousClass2I8) {
            r2 = ((AbstractC21570xd) ((AnonymousClass2I8) this).A00.A06).A00.get();
            try {
                Cursor A03 = AbstractC21570xd.A03(r2, "wa_profile_photo_block_list", null, null, "CONTACT_PROFILE_PHOTO_BLOCK_LIST", new String[]{"jid"}, null);
                set = C21580xe.A07(A03, "contact-mgr-db/unable to get profile photo block list");
                if (A03 != null) {
                    A03.close();
                }
            } finally {
                try {
                    r2.close();
                } catch (Throwable unused) {
                }
            }
        } else if (this instanceof AnonymousClass2I5) {
            r2 = ((AbstractC21570xd) ((AnonymousClass2I5) this).A00.A06).A00.get();
            try {
                Cursor A032 = AbstractC21570xd.A03(r2, "wa_last_seen_block_list", null, null, "CONTACT_LAST_SEEN_BLOCK_LIST", new String[]{"jid"}, null);
                set = C21580xe.A07(A032, "contact-mgr-db/unable to get last seen block list");
                if (A032 != null) {
                    A032.close();
                }
            } finally {
                try {
                    r2.close();
                } catch (Throwable unused2) {
                }
            }
        } else if (!(this instanceof AnonymousClass1BN)) {
            r2 = ((AbstractC21570xd) ((AnonymousClass2I0) this).A00.A06).A00.get();
            try {
                Cursor A033 = AbstractC21570xd.A03(r2, "wa_about_block_list", null, null, "CONTACT_ABOUT_BLOCK_LIST", new String[]{"jid"}, null);
                set = C21580xe.A07(A033, "contact-mgr-db/unable to get about block list");
                if (A033 != null) {
                    A033.close();
                }
            } finally {
            }
        } else {
            r2 = ((AbstractC21570xd) ((AnonymousClass1BN) this).A00.A06).A00.get();
            try {
                Cursor A034 = AbstractC21570xd.A03(r2, "wa_group_add_black_list", null, null, "CONTACT_GROUP_ADD_BLACK_LIST", new String[]{"jid"}, null);
                set = C21580xe.A07(A034, "contact-mgr-db/unable to get group add block list");
                if (A034 != null) {
                    A034.close();
                }
            } finally {
            }
        }
        r2.close();
        return set;
    }

    public void A04(String str, Set set, boolean z) {
        int i;
        SharedPreferences.Editor edit;
        String str2;
        if (this instanceof AnonymousClass2I8) {
            AnonymousClass2I8 r3 = (AnonymousClass2I8) this;
            C21580xe r2 = r3.A00.A06;
            C28181Ma r1 = new C28181Ma(true);
            r1.A03();
            r2.A0P("wa_profile_photo_block_list", set);
            r1.A00();
            SharedPreferences sharedPreferences = r3.A01.A00;
            sharedPreferences.edit().putString("profile_photo_block_list_hash", str).apply();
            if (z) {
                i = 3;
                edit = sharedPreferences.edit();
                str2 = "privacy_profile_photo";
            } else {
                return;
            }
        } else if (this instanceof AnonymousClass2I5) {
            AnonymousClass2I5 r32 = (AnonymousClass2I5) this;
            C21580xe r22 = r32.A00.A06;
            C28181Ma r12 = new C28181Ma(true);
            r12.A03();
            r22.A0P("wa_last_seen_block_list", set);
            r12.A00();
            SharedPreferences sharedPreferences2 = r32.A01.A00;
            sharedPreferences2.edit().putString("last_seen_block_list_hash", str).apply();
            if (z) {
                i = 3;
                edit = sharedPreferences2.edit();
                str2 = "privacy_last_seen";
            } else {
                return;
            }
        } else if (!(this instanceof AnonymousClass1BN)) {
            AnonymousClass2I0 r33 = (AnonymousClass2I0) this;
            C21580xe r23 = r33.A00.A06;
            C28181Ma r13 = new C28181Ma(true);
            r13.A03();
            r23.A0P("wa_about_block_list", set);
            r13.A00();
            SharedPreferences sharedPreferences3 = r33.A01.A00;
            sharedPreferences3.edit().putString("about_block_list_hash", str).apply();
            if (z) {
                i = 3;
                edit = sharedPreferences3.edit();
                str2 = "privacy_status";
            } else {
                return;
            }
        } else {
            AnonymousClass1BN r34 = (AnonymousClass1BN) this;
            C21580xe r24 = r34.A00.A06;
            C28181Ma r14 = new C28181Ma(true);
            r14.A03();
            r24.A0P("wa_group_add_black_list", set);
            r14.A00();
            SharedPreferences sharedPreferences4 = r34.A01.A00;
            sharedPreferences4.edit().putString("group_add_blacklist_hash", str).apply();
            if (z) {
                i = 3;
                edit = sharedPreferences4.edit();
                str2 = "privacy_groupadd";
            } else {
                return;
            }
        }
        edit.putInt(str2, i).apply();
    }
}
