package X;

import android.text.TextUtils;
import android.util.Pair;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3AW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AW {
    public static Pair A00(AnonymousClass018 r8, List list) {
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        int size = list.size();
        String str = "";
        for (int i = 0; i < size; i++) {
            C15370n3 ABZ = ((AbstractC116005Tt) list.get(i)).ABZ();
            if (ABZ != null) {
                String str2 = ABZ.A0K;
                if (!TextUtils.isEmpty(str2)) {
                    String upperCase = AnonymousClass1US.A03(1, str2).toUpperCase(C12970iu.A14(r8));
                    if (Character.isDigit(upperCase.codePointAt(0)) || "+".equals(upperCase)) {
                        upperCase = "#";
                    }
                    if (!str.equals(upperCase)) {
                        A0l.add(upperCase);
                        C12980iv.A1R(A0l2, i);
                        str = upperCase;
                    }
                }
            }
        }
        return C12990iw.A0L(A0l, A0l2);
    }
}
