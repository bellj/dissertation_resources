package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0C2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C2 extends AnonymousClass05L {
    public final /* synthetic */ AnonymousClass05K A00;
    public final /* synthetic */ AnonymousClass01E A01;
    public final /* synthetic */ AtomicReference A02;

    public AnonymousClass0C2(AnonymousClass05K r1, AnonymousClass01E r2, AtomicReference atomicReference) {
        this.A01 = r2;
        this.A02 = atomicReference;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass05L
    public void A00(C018108l r3, Object obj) {
        AnonymousClass05L r0 = (AnonymousClass05L) this.A02.get();
        if (r0 != null) {
            r0.A00(null, obj);
            return;
        }
        throw new IllegalStateException("Operation cannot be started before fragment is in created state");
    }
}
