package X;

/* renamed from: X.5Gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113235Gs extends RuntimeException {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C113235Gs(android.os.Parcel r5, java.lang.String r6) {
        /*
            r4 = this;
            int r3 = r5.dataPosition()
            int r2 = r5.dataSize()
            int r0 = X.C12970iu.A07(r6)
            int r0 = r0 + 41
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            r1.append(r6)
            java.lang.String r0 = " Parcel: pos="
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = " size="
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r2)
            r4.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C113235Gs.<init>(android.os.Parcel, java.lang.String):void");
    }
}
