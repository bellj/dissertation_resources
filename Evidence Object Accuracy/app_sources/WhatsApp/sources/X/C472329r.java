package X;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import com.facebook.msys.mci.DefaultCrypto;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.29r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C472329r {
    public static boolean A00;
    public static boolean A01;
    public static final AtomicBoolean A02 = new AtomicBoolean();
    public static final AtomicBoolean A03 = new AtomicBoolean();

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006e, code lost:
        if (r0.booleanValue() != false) goto L_0x0070;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(android.content.Context r10, int r11) {
        /*
        // Method dump skipped, instructions count: 333
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C472329r.A00(android.content.Context, int):int");
    }

    @Deprecated
    public static void A01(Context context, int i) {
        C471929k r1 = C471929k.A00;
        int A002 = r1.A00(context, i);
        if (A002 != 0) {
            Intent A012 = r1.A01(context, "e", A002);
            StringBuilder sb = new StringBuilder(57);
            sb.append("GooglePlayServices not available due to error ");
            sb.append(A002);
            Log.e("GooglePlayServicesUtil", sb.toString());
            if (A012 == null) {
                throw new AnonymousClass29w(A002);
            }
            throw new AnonymousClass29x(A012, "Google Play Services not available", A002);
        }
    }

    public static boolean A02(Context context) {
        try {
            if (!A00) {
                try {
                    PackageInfo packageInfo = C15080mX.A00(context).A00.getPackageManager().getPackageInfo("com.google.android.gms", 64);
                    C472929z.A00(context);
                    if (packageInfo == null || C472929z.A01(packageInfo, false) || !C472929z.A01(packageInfo, true)) {
                        A01 = false;
                    } else {
                        A01 = true;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", e);
                }
            }
            return A01 || !"user".equals(Build.TYPE);
        } finally {
            A00 = true;
        }
    }

    public static boolean A03(Context context) {
        boolean equals = "com.google.android.gms".equals("com.google.android.gms");
        if (C472729v.A02()) {
            try {
                for (PackageInstaller.SessionInfo sessionInfo : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if ("com.google.android.gms".equals(sessionInfo.getAppPackageName())) {
                        break;
                    }
                }
            } catch (PackageManager.NameNotFoundException | Exception unused) {
            }
        }
        ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo("com.google.android.gms", DefaultCrypto.BUFFER_SIZE);
        if (equals) {
            return applicationInfo.enabled;
        }
        if (applicationInfo.enabled) {
            if (C472729v.A00()) {
                Object systemService = context.getSystemService("user");
                C13020j0.A01(systemService);
                Bundle applicationRestrictions = ((UserManager) systemService).getApplicationRestrictions(context.getPackageName());
                if (applicationRestrictions == null || !"true".equals(applicationRestrictions.getString("restricted_profile"))) {
                    break;
                }
                return false;
            }
            return true;
        }
        return false;
    }
}
