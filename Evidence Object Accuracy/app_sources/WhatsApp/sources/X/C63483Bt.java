package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.status.ContactStatusThumbnail;
import com.whatsapp.status.playback.MyStatusesActivity;

/* renamed from: X.3Bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63483Bt {
    public final View A00;
    public final View A01;
    public final View A02;
    public final ImageView A03;
    public final LinearLayout A04;
    public final ProgressBar A05;
    public final TextView A06;
    public final TextView A07;
    public final SelectionCheckView A08;
    public final ContactStatusThumbnail A09;
    public final /* synthetic */ MyStatusesActivity A0A;

    public C63483Bt(View view, MyStatusesActivity myStatusesActivity) {
        this.A0A = myStatusesActivity;
        this.A00 = view;
        ContactStatusThumbnail contactStatusThumbnail = (ContactStatusThumbnail) view.findViewById(R.id.contact_photo);
        this.A09 = contactStatusThumbnail;
        contactStatusThumbnail.setClickable(false);
        View findViewById = view.findViewById(R.id.contact_selector);
        this.A01 = findViewById;
        findViewById.setClickable(false);
        this.A06 = C12960it.A0J(view, R.id.date_time);
        ImageView A0L = C12970iu.A0L(view, R.id.overflow_icon);
        this.A03 = A0L;
        A0L.setOnClickListener(myStatusesActivity.A0w);
        TextView A0J = C12960it.A0J(view, R.id.views_count);
        this.A07 = A0J;
        View findViewById2 = view.findViewById(R.id.retry_button);
        this.A02 = findViewById2;
        findViewById2.setOnClickListener(myStatusesActivity.A0x);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);
        this.A05 = progressBar;
        C88134Ek.A00(progressBar, AnonymousClass00T.A00(view.getContext(), R.color.primary_light));
        this.A08 = (SelectionCheckView) view.findViewById(R.id.selection_check);
        this.A04 = (LinearLayout) view.findViewById(R.id.title_container);
        C27531Hw.A06(A0J);
    }
}
