package X;

import android.content.Context;
import java.util.List;

/* renamed from: X.34s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C621434s extends C621134m {
    public AnonymousClass34a A00;
    public boolean A01;
    public final AnonymousClass1BK A02;

    public C621434s(Context context, C15570nT r16, C15550nR r17, C15610nY r18, C63563Cb r19, C63543Bz r20, AnonymousClass01d r21, C14830m7 r22, AnonymousClass018 r23, AnonymousClass1BK r24, AnonymousClass19M r25, C16630pM r26, AnonymousClass12F r27) {
        super(context, r16, r17, r18, r19, r20, r21, r22, r23, r25, r26, r27);
        A00();
        this.A02 = r24;
    }

    /* renamed from: A07 */
    public void A05(C28861Ph r4, List list) {
        super.A05(r4, list);
        if (C64643Gi.A00(getContext(), this.A02, r4, 0).A00.A01 != null) {
            this.A00.setVisibility(0);
            this.A00.setMessage(r4, list);
            return;
        }
        this.A00.setVisibility(8);
    }
}
