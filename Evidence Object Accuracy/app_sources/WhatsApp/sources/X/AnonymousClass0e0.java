package X;

/* renamed from: X.0e0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0e0 implements Runnable {
    public final /* synthetic */ AnonymousClass00N A00;
    public final /* synthetic */ AnonymousClass0EJ A01;
    public final /* synthetic */ AnonymousClass0Q9 A02;
    public final /* synthetic */ AnonymousClass0Q9 A03;
    public final /* synthetic */ boolean A04;

    public AnonymousClass0e0(AnonymousClass00N r1, AnonymousClass0EJ r2, AnonymousClass0Q9 r3, AnonymousClass0Q9 r4, boolean z) {
        this.A01 = r2;
        this.A03 = r3;
        this.A02 = r4;
        this.A04 = z;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0TQ.A00(this.A00, this.A03.A04, this.A02.A04, this.A04, false);
    }
}
