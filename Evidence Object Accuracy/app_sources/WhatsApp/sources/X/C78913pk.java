package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.3pk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78913pk extends AbstractC78753pU {
    public static final HashMap A05;
    public static final Parcelable.Creator CREATOR = new C98524ip();
    public int A00;
    public C78923pl A01;
    public ArrayList A02;
    public final int A03;
    public final Set A04;

    public C78913pk() {
        this.A04 = new HashSet(1);
        this.A03 = 1;
    }

    public C78913pk(C78923pl r1, ArrayList arrayList, Set set, int i, int i2) {
        this.A04 = set;
        this.A03 = i;
        this.A02 = arrayList;
        this.A00 = i2;
        this.A01 = r1;
    }

    static {
        HashMap A11 = C12970iu.A11();
        A05 = A11;
        A11.put("authenticatorData", new C78633pE(C78933pm.class, "authenticatorData", 11, 11, 2, true, true));
        A11.put("progress", new C78633pE(C78923pl.class, "progress", 11, 11, 4, false, false));
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        Set set = this.A04;
        if (AbstractC78753pU.A00(set, 1)) {
            C95654e8.A07(parcel, 1, this.A03);
        }
        if (AbstractC78753pU.A00(set, 2)) {
            C95654e8.A0F(parcel, this.A02, 2, true);
        }
        if (AbstractC78753pU.A00(set, 3)) {
            C95654e8.A07(parcel, 3, this.A00);
        }
        if (AbstractC78753pU.A00(set, 4)) {
            C95654e8.A0B(parcel, this.A01, 4, i, true);
        }
        C95654e8.A06(parcel, A00);
    }
}
