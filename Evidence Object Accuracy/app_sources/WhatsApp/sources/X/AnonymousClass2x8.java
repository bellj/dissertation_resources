package X;

/* renamed from: X.2x8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2x8 extends C71333cl {
    public AnonymousClass2x8(C15610nY r1, AnonymousClass018 r2) {
        super(r1, r2);
    }

    @Override // X.C71333cl
    public int A00(C15370n3 r3, C15370n3 r4) {
        Integer A01 = AnonymousClass4Yh.A01(A01(r3), A01(r4));
        return A01 == null ? super.compare(r3, r4) : A01.intValue();
    }
}
