package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.whatsapp.util.Log;
import java.util.concurrent.RejectedExecutionException;

/* renamed from: X.09h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC019809h extends BroadcastReceiver {
    public abstract void A00(Context context, String str);

    public abstract void A01(Context context, String str);

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            AnonymousClass0NG r4 = AnonymousClass0NG.A02;
            if (r4 == null) {
                r4 = new AnonymousClass0NG(context);
                AnonymousClass0NG.A02 = r4;
            }
            RunnableC10200eC r3 = new RunnableC10200eC(context, intent, this);
            PowerManager.WakeLock newWakeLock = r4.A00.newWakeLock(1, "FBNSPreloadWakefulExecutor");
            newWakeLock.setReferenceCounted(false);
            newWakeLock.acquire(60000);
            try {
                r4.A01.execute(new RunnableC10000dr(newWakeLock, r4, r3));
            } catch (RejectedExecutionException e) {
                Log.e("FBNSPreloadWakefulExecutor/Notification skipped", e);
                newWakeLock.release();
            }
        }
    }
}
