package X;

import com.whatsapp.R;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.0Iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class EnumC03700Iu extends Enum {
    public static final EnumC03700Iu A00 = new EnumC03700Iu("BACKGROUND_DEEMPHASIZED", 1, R.attr.cds_usage_background_deemphasized, -920329, R.color.cds_gray_01);
    public static final EnumC03700Iu A01 = new EnumC03700Iu("BOTTOM_SHEET_HANDLE", 2, R.attr.cds_usage_bottom_sheet_handle, -3419431, R.color.cds_gray_03);
    public static final EnumC03700Iu A02 = new EnumC03700Iu("OVERLAY_ON_SURFACE", 3, R.attr.cds_usage_overlay_on_surface, -1728053248, R.color.cds_black_a60);
    public static final EnumC03700Iu A03 = new EnumC03700Iu("PRIMARY_BUTTON_BACKGROUND", 4, R.attr.cds_usage_primary_button_background, -14931149, R.color.cds_gray_10);
    public static final EnumC03700Iu A04 = new EnumC03700Iu("PRIMARY_BUTTON_TEXT", 5, R.attr.cds_usage_primary_button_text, -1, R.color.cds_white);
    public static final EnumC03700Iu A05 = new EnumC03700Iu("PRIMARY_ICON", 6, R.attr.cds_usage_primary_icon, -14931149, R.color.cds_gray_10);
    public static final EnumC03700Iu A06 = new EnumC03700Iu("PRIMARY_TEXT", 7, R.attr.cds_usage_primary_text, -14931149, R.color.cds_gray_10);
    public static final EnumC03700Iu A07 = new EnumC03700Iu("PROGRESS_RING_ON_NEUTRAL_FOREGROUND", 8, R.attr.cds_usage_progress_ring_on_neutral_foreground, -14931149, R.color.cds_gray_10);
    public static final EnumC03700Iu A08 = new EnumC03700Iu("SECONDARY_BUTTON_BACKGROUND", 9, R.attr.cds_usage_secondary_button_background, 0, R.color.cds_transparent);
    public static final EnumC03700Iu A09 = new EnumC03700Iu("SECONDARY_BUTTON_STROKE", 10, R.attr.cds_usage_secondary_button_stroke, -3419431, R.color.cds_gray_03);
    public static final EnumC03700Iu A0A = new EnumC03700Iu("SECONDARY_BUTTON_TEXT", 11, R.attr.cds_usage_secondary_button_text, -14931149, R.color.cds_gray_10);
    public static final EnumC03700Iu A0B = new EnumC03700Iu("SECONDARY_ICON", 12, R.attr.cds_usage_secondary_icon, -12166551, R.color.cds_gray_07);
    public static final EnumC03700Iu A0C = new EnumC03700Iu("SURFACE_BACKGROUND", 13, R.attr.cds_usage_surface_background, -1, R.color.cds_white);
    public final int attr;
    public final int lightModeFallBackColorInt;
    public final int lightModeFallBackColorRes;

    static {
        new EnumC03700Iu("ACCENT", 0, R.attr.cds_usage_accent, -14931149, R.color.cds_gray_10);
    }

    public EnumC03700Iu(String str, int i, int i2, int i3, int i4) {
        this.attr = i2;
        this.lightModeFallBackColorInt = i3;
        this.lightModeFallBackColorRes = i4;
    }
}
