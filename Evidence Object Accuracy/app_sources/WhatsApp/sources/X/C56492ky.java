package X;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.2ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56492ky extends AnonymousClass1U5 {
    public static final C56492ky A04 = new C56492ky(0);
    public static final Parcelable.Creator CREATOR = new C98854jM();
    public final int A00;
    public final int A01;
    public final PendingIntent A02;
    public final String A03;

    public C56492ky(PendingIntent pendingIntent, String str, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = pendingIntent;
        this.A03 = str;
    }

    public boolean A00() {
        return (this.A01 == 0 || this.A02 == null) ? false : true;
    }

    public C56492ky(int i) {
        this(null, null, 1, i);
    }

    public C56492ky(int i, PendingIntent pendingIntent) {
        this(pendingIntent, null, 1, i);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof C56492ky) {
                C56492ky r5 = (C56492ky) obj;
                if (this.A01 != r5.A01 || !C13300jT.A00(this.A02, r5.A02) || !C13300jT.A00(this.A03, r5.A03)) {
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        Object[] objArr = new Object[3];
        C12960it.A1O(objArr, this.A01);
        objArr[1] = this.A02;
        return C12980iv.A0B(this.A03, objArr, 2);
    }

    @Override // java.lang.Object
    public String toString() {
        String str;
        C13290jS r3 = new C13290jS(this);
        int i = this.A01;
        if (i == 99) {
            str = "UNFINISHED";
        } else if (i != 1500) {
            switch (i) {
                case -1:
                    str = "UNKNOWN";
                    break;
                case 0:
                    str = "SUCCESS";
                    break;
                case 1:
                    str = "SERVICE_MISSING";
                    break;
                case 2:
                    str = "SERVICE_VERSION_UPDATE_REQUIRED";
                    break;
                case 3:
                    str = "SERVICE_DISABLED";
                    break;
                case 4:
                    str = "SIGN_IN_REQUIRED";
                    break;
                case 5:
                    str = "INVALID_ACCOUNT";
                    break;
                case 6:
                    str = "RESOLUTION_REQUIRED";
                    break;
                case 7:
                    str = "NETWORK_ERROR";
                    break;
                case 8:
                    str = "INTERNAL_ERROR";
                    break;
                case 9:
                    str = "SERVICE_INVALID";
                    break;
                case 10:
                    str = "DEVELOPER_ERROR";
                    break;
                case 11:
                    str = "LICENSE_CHECK_FAILED";
                    break;
                default:
                    switch (i) {
                        case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                            str = "CANCELED";
                            break;
                        case UrlRequest.Status.READING_RESPONSE /* 14 */:
                            str = "TIMEOUT";
                            break;
                        case 15:
                            str = "INTERRUPTED";
                            break;
                        case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                            str = "API_UNAVAILABLE";
                            break;
                        case 17:
                            str = "SIGN_IN_FAILED";
                            break;
                        case 18:
                            str = "SERVICE_UPDATING";
                            break;
                        case 19:
                            str = "SERVICE_MISSING_PERMISSION";
                            break;
                        case C43951xu.A01:
                            str = "RESTRICTED_PROFILE";
                            break;
                        case 21:
                            str = "API_VERSION_UPDATE_REQUIRED";
                            break;
                        case 22:
                            str = "RESOLUTION_ACTIVITY_NOT_FOUND";
                            break;
                        case 23:
                            str = "API_DISABLED";
                            break;
                        case 24:
                            str = "API_DISABLED_FOR_CONNECTION";
                            break;
                        default:
                            StringBuilder A0t = C12980iv.A0t(31);
                            A0t.append("UNKNOWN_ERROR_CODE(");
                            A0t.append(i);
                            str = C12960it.A0d(")", A0t);
                            break;
                    }
            }
        } else {
            str = "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        r3.A00(str, "statusCode");
        r3.A00(this.A02, "resolution");
        r3.A00(this.A03, "message");
        return r3.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A07(parcel, 2, this.A01);
        C95654e8.A0B(parcel, this.A02, 3, i, false);
        C95654e8.A0D(parcel, this.A03, 4, false);
        C95654e8.A06(parcel, A01);
    }
}
