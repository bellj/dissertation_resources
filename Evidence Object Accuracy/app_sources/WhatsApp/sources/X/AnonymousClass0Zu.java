package X;

import android.content.Context;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.0Zu  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Zu implements AbstractC11440gI {
    public static final String A03 = C06390Tk.A01("WorkConstraintsTracker");
    public final AbstractC12450hw A00;
    public final Object A01 = new Object();
    public final AnonymousClass0Zt[] A02;

    public AnonymousClass0Zu(Context context, AbstractC12450hw r6, AbstractC11500gO r7) {
        Context applicationContext = context.getApplicationContext();
        this.A00 = r6;
        this.A02 = new AnonymousClass0Zt[]{new AnonymousClass0GZ(applicationContext, r7), new C03080Ga(applicationContext, r7), new C03110Gd(applicationContext, r7), new C03090Gb(applicationContext, r7), new C03100Gc(applicationContext, r7), new C03130Gf(applicationContext, r7), new C03120Ge(applicationContext, r7)};
    }

    public void A00() {
        synchronized (this.A01) {
            AnonymousClass0Zt[] r5 = this.A02;
            for (AnonymousClass0Zt r2 : r5) {
                List list = r2.A03;
                if (!list.isEmpty()) {
                    list.clear();
                    r2.A01.A03(r2);
                }
            }
        }
    }

    public void A01(Iterable iterable) {
        synchronized (this.A01) {
            AnonymousClass0Zt[] r6 = this.A02;
            for (AnonymousClass0Zt r2 : r6) {
                if (r2.A00 != null) {
                    r2.A00 = null;
                    r2.A00(null, r2.A02);
                }
            }
            for (AnonymousClass0Zt r8 : r6) {
                List list = r8.A03;
                list.clear();
                Iterator it = iterable.iterator();
                while (it.hasNext()) {
                    C004401z r1 = (C004401z) it.next();
                    if (r8.A01(r1)) {
                        list.add(r1.A0E);
                    }
                }
                boolean isEmpty = list.isEmpty();
                AbstractC06170Sl r11 = r8.A01;
                if (isEmpty) {
                    r11.A03(r8);
                } else {
                    synchronized (r11.A03) {
                        Set set = r11.A04;
                        if (set.add(r8)) {
                            if (set.size() == 1) {
                                r11.A00 = r11.A00();
                                C06390Tk.A00().A02(AbstractC06170Sl.A05, String.format("%s: initial state = %s", r11.getClass().getSimpleName(), r11.A00), new Throwable[0]);
                                r11.A01();
                            }
                            Object obj = r11.A00;
                            r8.A02 = obj;
                            r8.A00(r8.A00, obj);
                        }
                    }
                }
                r8.A00(r8.A00, r8.A02);
            }
            for (AnonymousClass0Zt r12 : r6) {
                if (r12.A00 != this) {
                    r12.A00 = this;
                    r12.A00(this, r12.A02);
                }
            }
        }
    }

    public boolean A02(String str) {
        synchronized (this.A01) {
            AnonymousClass0Zt[] r3 = this.A02;
            for (AnonymousClass0Zt r7 : r3) {
                Object obj = r7.A02;
                if (obj != null && r7.A02(obj) && r7.A03.contains(str)) {
                    C06390Tk.A00().A02(A03, String.format("Work %s constrained by %s", str, r7.getClass().getSimpleName()), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }
}
