package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.0oO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C16070oO {
    public static final long A03 = TimeUnit.DAYS.toMillis(7);
    public final long A00;
    public final String A01;
    public final String A02;

    public C16070oO(String str, String str2, long j) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = j;
    }
}
