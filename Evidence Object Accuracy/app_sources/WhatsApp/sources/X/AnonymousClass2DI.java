package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2DI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DI {
    public final AbstractC28901Pl A00;
    public final List A01 = new ArrayList();

    public AnonymousClass2DI(AnonymousClass1ZW r2) {
        this.A00 = r2;
    }

    public String A00() {
        AnonymousClass1ZY r0;
        String str;
        AbstractC28901Pl r02 = this.A00;
        if (r02 == null || (r0 = r02.A08) == null) {
            r0 = null;
        }
        if (r0 == null || (str = ((AnonymousClass1ZX) r0).A06) == null) {
            return "NONE";
        }
        return str;
    }

    public String A01() {
        AnonymousClass1ZY r1;
        AnonymousClass1ZX r12;
        AbstractC28901Pl r0 = this.A00;
        if (r0 == null || (r1 = r0.A08) == null || !(r1 instanceof AnonymousClass1ZX) || (r12 = (AnonymousClass1ZX) r1) == null) {
            return null;
        }
        return r12.A05;
    }

    public String A02() {
        AnonymousClass1ZR r0;
        Object obj;
        AbstractC28901Pl r02 = this.A00;
        return (r02 == null || (r0 = r02.A09) == null || (obj = r0.A00) == null) ? "" : (String) obj;
    }
}
