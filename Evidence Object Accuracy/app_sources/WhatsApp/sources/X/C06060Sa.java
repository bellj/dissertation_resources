package X;

import android.content.Context;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import com.whatsapp.R;
import java.lang.ref.WeakReference;

/* renamed from: X.0Sa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06060Sa {
    public final int A00;
    public final int A01;
    public final int A02;
    public final Interpolator A03;
    public final Interpolator A04;
    public final AbstractC12100hN A05;
    public final AbstractC12110hO A06;
    public final C14260l7 A07;
    public final AnonymousClass3JI A08;
    public final String A09;
    public final boolean A0A;

    public /* synthetic */ C06060Sa(Interpolator interpolator, Interpolator interpolator2, AnonymousClass0b9 r15, AbstractC12100hN r16, AbstractC12110hO r17, C14260l7 r18, AnonymousClass3JI r19, String str, int i, int i2, int i3, boolean z) {
        this(interpolator, interpolator2, r16, r17, r18, r19, str, i, i2, i3, z);
    }

    public C06060Sa(Interpolator interpolator, Interpolator interpolator2, AbstractC12100hN r3, AbstractC12110hO r4, C14260l7 r5, AnonymousClass3JI r6, String str, int i, int i2, int i3, boolean z) {
        this.A07 = r5;
        this.A08 = r6;
        this.A00 = i;
        this.A02 = i2;
        this.A01 = i3;
        this.A04 = interpolator;
        this.A03 = interpolator2;
        this.A06 = r4;
        this.A05 = r3;
        this.A09 = str;
        this.A0A = z;
    }

    public final AnonymousClass0BZ A00() {
        C14260l7 r3 = this.A07;
        AnonymousClass0BZ r2 = new AnonymousClass0BZ(r3.A00(), this.A0A);
        r2.setBloksContentView(r3, this.A08);
        r2.A00 = this.A00;
        r2.A02 = this.A02;
        r2.A01 = this.A01;
        r2.A04 = this.A04;
        r2.A03 = this.A03;
        r2.A06 = new AnonymousClass0b9(this);
        r2.A05 = new AnonymousClass0b7(r2, this);
        r2.setTag(R.id.foa_toast_tag_server_id, this.A09);
        return r2;
    }

    public void A01() {
        Context A00 = this.A07.A00();
        AnonymousClass0BZ A002 = A00();
        boolean z = this.A0A;
        AnonymousClass0BZ r1 = (AnonymousClass0BZ) C04470Lv.A00.get();
        if (r1 != null) {
            r1.A01(r1.A01);
        }
        int i = -1;
        if (z) {
            i = -2;
        }
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(i, -2, 99, 8, -3);
        int i2 = 80;
        if (z) {
            i2 = 17;
        }
        layoutParams.gravity = i2;
        try {
            WindowManager windowManager = (WindowManager) A00.getSystemService("window");
            if (windowManager != null) {
                windowManager.addView(A002, layoutParams);
                C04470Lv.A00 = new WeakReference(A002);
                A002.setAlpha(0.0f);
                A002.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC06950Wc(A002));
                return;
            }
            throw new IllegalStateException("Window manager required but not found.");
        } catch (WindowManager.BadTokenException unused) {
        }
    }
}
