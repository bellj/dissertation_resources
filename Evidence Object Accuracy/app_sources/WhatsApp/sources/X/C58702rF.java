package X;

import android.graphics.Bitmap;

/* renamed from: X.2rF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58702rF extends AbstractC83923y7 {
    public final int A00;
    public final Bitmap A01;
    public final boolean A02;

    public C58702rF(Bitmap bitmap, int i, boolean z) {
        C16700pc.A0E(bitmap, 1);
        this.A01 = bitmap;
        this.A02 = z;
        this.A00 = i;
    }

    @Override // X.AbstractC83923y7
    public boolean A00() {
        return this.A02;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C58702rF) {
                C58702rF r5 = (C58702rF) obj;
                if (!(C16700pc.A0O(this.A01, r5.A01) && this.A02 == r5.A02 && Integer.valueOf(this.A00).intValue() == Integer.valueOf(r5.A00).intValue())) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.A01.hashCode() * 31;
        boolean z = this.A02;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return C12990iw.A08(Integer.valueOf(this.A00), (hashCode + i) * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Data(bitmap=");
        A0k.append(this.A01);
        A0k.append(", isSelected=");
        A0k.append(this.A02);
        A0k.append(", ringColor=");
        A0k.append(Integer.valueOf(this.A00).intValue());
        return C12970iu.A0u(A0k);
    }
}
