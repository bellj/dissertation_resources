package X;

import android.content.Context;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.2V1  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2V1 extends AnonymousClass2V2 {
    public ViewGroup A00 = ((ViewGroup) findViewById(R.id.search_message_container_attachment));
    public ViewGroup A01 = ((ViewGroup) findViewById(R.id.search_message_container_content));
    public ViewGroup A02 = ((ViewGroup) findViewById(R.id.search_message_container_header));
    public ViewGroup A03 = ((ViewGroup) findViewById(R.id.search_message_container_media));

    public AnonymousClass2V1(Context context) {
        super(context);
        setOrientation(0);
        LinearLayout.inflate(getContext(), R.layout.search_message_container, this);
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(16843534, typedValue, true);
        setBackgroundResource(typedValue.resourceId);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.search_item_horizontal_margin);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.search_item_message_vertical_margin);
        setPadding(dimensionPixelSize, dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2);
        setMinimumHeight(getResources().getDimensionPixelSize(R.dimen.search_message_min_height));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v4, resolved type: X.1y8 */
    /* JADX DEBUG: Multi-variable search result rejected for r3v5, resolved type: X.34Z */
    /* JADX DEBUG: Multi-variable search result rejected for r3v6, resolved type: X.34X */
    /* JADX DEBUG: Multi-variable search result rejected for r3v7, resolved type: X.34Y */
    /* JADX DEBUG: Multi-variable search result rejected for r3v8, resolved type: X.34W */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
        // Method dump skipped, instructions count: 407
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2V1.A01():void");
    }
}
