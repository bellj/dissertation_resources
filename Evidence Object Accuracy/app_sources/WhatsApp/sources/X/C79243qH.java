package X;

/* renamed from: X.3qH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C79243qH extends AbstractC79253qI {
    public final byte[] zzfp;

    public C79243qH(byte[] bArr) {
        this.zzfp = bArr;
    }

    public int A04() {
        if (!(this instanceof C79233qG)) {
            return 0;
        }
        return ((C79233qG) this).zzfm;
    }

    @Override // X.AbstractC111915Bh, java.lang.Object
    public final boolean equals(Object obj) {
        int A02;
        AbstractC111915Bh r7;
        int A022;
        if (obj != this) {
            if ((obj instanceof AbstractC111915Bh) && (A02 = A02()) == (A022 = (r7 = (AbstractC111915Bh) obj).A02())) {
                if (A02 != 0) {
                    if (!(obj instanceof C79243qH)) {
                        return obj.equals(this);
                    }
                    int i = this.zzfk;
                    int i2 = r7.zzfk;
                    if (i == 0 || i2 == 0 || i == i2) {
                        if (A02 > A022) {
                            StringBuilder A0t = C12980iv.A0t(40);
                            A0t.append("Length too large: ");
                            A0t.append(A02);
                            throw C12970iu.A0f(C12960it.A0f(A0t, A02));
                        } else if (A02 <= A022) {
                            boolean z = r7 instanceof C79243qH;
                            C79243qH r72 = (C79243qH) r7;
                            if (z) {
                                byte[] bArr = this.zzfp;
                                byte[] bArr2 = r72.zzfp;
                                int A04 = A04();
                                int i3 = A02 + A04;
                                int A042 = r72.A04();
                                while (A04 < i3) {
                                    if (bArr[A04] != bArr2[A042]) {
                                        return false;
                                    }
                                    A04++;
                                    A042++;
                                }
                                return true;
                            }
                            int A00 = AbstractC111915Bh.A00(0, A02, r72.A02());
                            Object r4 = A00 == 0 ? AbstractC111915Bh.A00 : new C79233qG(r72.zzfp, r72.A04(), A00);
                            int A002 = AbstractC111915Bh.A00(0, A02, A02());
                            return r4.equals(A002 == 0 ? AbstractC111915Bh.A00 : new C79233qG(this.zzfp, A04(), A002));
                        } else {
                            StringBuilder A0t2 = C12980iv.A0t(59);
                            A0t2.append("Ran off end of other: 0, ");
                            A0t2.append(A02);
                            throw C12970iu.A0f(C12960it.A0e(", ", A0t2, A022));
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }
}
