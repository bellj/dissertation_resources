package X;

import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1YO  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1YO {
    public int A00;
    public int A01;
    public boolean A02;
    public final UserJid A03;
    public final ConcurrentHashMap A04;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1YO(com.whatsapp.jid.UserJid r3, int r4, boolean r5, boolean r6) {
        /*
            r2 = this;
            com.whatsapp.jid.DeviceJid r1 = com.whatsapp.jid.DeviceJid.of(r3)
            X.AnonymousClass009.A05(r1)
            X.1YP r0 = new X.1YP
            r0.<init>(r1, r6)
            java.util.Set r0 = java.util.Collections.singleton(r0)
            r2.<init>(r3, r0, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1YO.<init>(com.whatsapp.jid.UserJid, int, boolean, boolean):void");
    }

    public AnonymousClass1YO(UserJid userJid, Set set, int i, boolean z) {
        this.A04 = new ConcurrentHashMap();
        this.A03 = userJid;
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AnonymousClass1YP r2 = (AnonymousClass1YP) it.next();
            this.A04.put(r2.A01, r2);
        }
        this.A01 = i;
        this.A02 = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AnonymousClass1YO.class == obj.getClass()) {
            AnonymousClass1YO r4 = (AnonymousClass1YO) obj;
            if (this.A01 == r4.A01 && this.A02 == r4.A02 && this.A03.equals(r4.A03)) {
                return this.A04.equals(r4.A04);
            }
        }
        return false;
    }

    public int hashCode() {
        return (((((this.A03.hashCode() * 31) + this.A04.hashCode()) * 31) + this.A01) * 31) + (this.A02 ? 1 : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupParticipant{jid='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append(", rank=");
        sb.append(this.A01);
        sb.append(", pending=");
        sb.append(this.A02);
        sb.append(", participantDevices=");
        StringBuilder sb2 = new StringBuilder("[");
        for (Object obj : this.A04.values()) {
            sb2.append(obj);
            sb2.append(", ");
        }
        sb2.append("]");
        sb.append(sb2.toString());
        sb.append('}');
        return sb.toString();
    }
}
