package X;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: X.0ff  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11050ff extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ ClassLoader $classLoader;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C11050ff(ClassLoader classLoader) {
        super(0);
        this.$classLoader = classLoader;
    }

    /* renamed from: A00 */
    public final Boolean AJ3() {
        boolean z = false;
        Method declaredMethod = this.$classLoader.loadClass("androidx.window.extensions.WindowExtensionsProvider").getDeclaredMethod("getWindowExtensions", new Class[0]);
        Class<?> loadClass = this.$classLoader.loadClass("androidx.window.extensions.WindowExtensions");
        C16700pc.A0B(declaredMethod);
        C16700pc.A0B(loadClass);
        if (declaredMethod.getReturnType().equals(loadClass) && Modifier.isPublic(declaredMethod.getModifiers())) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
