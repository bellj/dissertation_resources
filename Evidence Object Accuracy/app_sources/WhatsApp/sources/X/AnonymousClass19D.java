package X;

import android.app.Activity;

/* renamed from: X.19D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19D {
    public final C236912q A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final AnonymousClass11R A03;
    public final AnonymousClass19B A04;
    public final AnonymousClass11P A05;
    public final AnonymousClass01d A06;
    public final C16590pI A07;
    public final C15890o4 A08;
    public final C14820m6 A09;
    public final AnonymousClass12H A0A;
    public final C14850m9 A0B;
    public final C22050yP A0C;
    public final AnonymousClass19C A0D;
    public final C21300xC A0E;
    public final AnonymousClass199 A0F;
    public final AnonymousClass19A A0G;
    public final AnonymousClass01H A0H;

    public AnonymousClass19D(C236912q r2, AbstractC15710nm r3, C14900mE r4, AnonymousClass11R r5, AnonymousClass19B r6, AnonymousClass11P r7, AnonymousClass01d r8, C16590pI r9, C15890o4 r10, C14820m6 r11, AnonymousClass12H r12, C14850m9 r13, C22050yP r14, AnonymousClass19C r15, C21300xC r16, AnonymousClass199 r17, AnonymousClass19A r18, AnonymousClass01H r19) {
        this.A07 = r9;
        this.A0B = r13;
        this.A02 = r4;
        this.A01 = r3;
        this.A0F = r17;
        this.A06 = r8;
        this.A0E = r16;
        this.A0A = r12;
        this.A0C = r14;
        this.A00 = r2;
        this.A03 = r5;
        this.A0G = r18;
        this.A08 = r10;
        this.A09 = r11;
        this.A04 = r6;
        this.A0D = r15;
        this.A05 = r7;
        this.A0H = r19;
    }

    public C35191hP A00(Activity activity, C30421Xi r6, boolean z) {
        AnonymousClass11P r1 = this.A05;
        if (r1.A0D(r6)) {
            C35191hP A00 = r1.A00();
            AnonymousClass009.A05(A00);
            return A00;
        }
        boolean z2 = true;
        C35191hP A01 = A01(activity, true, z);
        A01.A0O = r6;
        if (!r6.A0z.A02) {
            int i = ((AbstractC15340mz) r6).A0C;
            if (!(i == 9 || i == 10)) {
                z2 = false;
            }
            A01.A0T = z2;
        }
        return A01;
    }

    public C35191hP A01(Activity activity, boolean z, boolean z2) {
        C16590pI r1 = this.A07;
        C14850m9 r12 = this.A0B;
        C14900mE r15 = this.A02;
        AbstractC15710nm r14 = this.A01;
        AnonymousClass199 r13 = this.A0F;
        AnonymousClass01d r122 = this.A06;
        C21300xC r11 = this.A0E;
        AnonymousClass12H r10 = this.A0A;
        C22050yP r9 = this.A0C;
        AnonymousClass19A r8 = this.A0G;
        C15890o4 r7 = this.A08;
        C14820m6 r6 = this.A09;
        AnonymousClass19B r5 = this.A04;
        return new C35191hP(activity, this.A00, r14, r15, this.A03, r5, this.A05, r122, r1, r7, r6, r10, r12, r9, this.A0D, r11, r13, r8, this.A0H, z, z2);
    }
}
