package X;

import com.whatsapp.nativelibloader.WhatsAppLibLoader;

/* renamed from: X.01K  reason: invalid class name */
/* loaded from: classes.dex */
public interface AnonymousClass01K {
    C14850m9 A5U();

    C15210mk A5y();

    C18060rq A5z();

    C18070rr A61();

    C18720su A6o();

    C18640sm A7Z();

    AbstractC15710nm A7p();

    C19350ty A7q();

    C19800uh AAF();

    C19980v1 AKK();

    C19390u2 AZ9();

    C20890wU Ab8();

    C15230mm AeO();

    C14950mJ AeW();

    C14830m7 Aen();

    C14820m6 Ag2();

    AbstractC14440lR Ag3();

    C16120oU Ag5();

    C21030wi Ag6();

    WhatsAppLibLoader Ag7();

    AnonymousClass018 Ag8();
}
