package X;

/* renamed from: X.1y5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44041y5 {
    public static final String A00;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(AnonymousClass1Ux.A01("message", C16500p8.A00));
        sb.append(" FROM ");
        sb.append("message_view AS message");
        sb.append(" JOIN ");
        sb.append("status_list AS chat_list");
        sb.append(" ON ");
        sb.append("chat_list.key_remote_jid = message.sender_jid_raw_string");
        sb.append(" WHERE ");
        sb.append("message.chat_row_id = ?");
        sb.append(" AND ");
        sb.append("message.from_me = 0");
        sb.append(" AND ");
        sb.append("chat_list.last_read_message_table_id >= message._id");
        sb.append(" AND ");
        sb.append("chat_list.last_read_receipt_sent_message_table_id < message._id");
        sb.append(" AND ");
        sb.append("chat_list.last_read_receipt_sent_message_table_id > 0");
        sb.append(" AND ");
        sb.append("message.message_type != 15");
        sb.append(" ORDER BY message.");
        sb.append("_id");
        sb.append(" DESC LIMIT 4096");
        A00 = sb.toString();
    }
}
