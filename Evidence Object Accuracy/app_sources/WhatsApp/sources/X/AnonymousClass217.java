package X;

import java.util.List;

/* renamed from: X.217  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass217 {
    public final AnonymousClass21A A00;
    public final String A01;
    public final List A02;

    public AnonymousClass217(AnonymousClass21A r1, String str, List list) {
        this.A01 = str;
        this.A02 = list;
        this.A00 = r1;
    }
}
