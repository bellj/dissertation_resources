package X;

import com.whatsapp.util.Log;
import java.util.Arrays;

/* renamed from: X.1ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32791ck implements AbstractC21730xt {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C19500uD A01;
    public final /* synthetic */ Runnable A02;
    public final /* synthetic */ byte[] A03;
    public final /* synthetic */ byte[] A04;

    public C32791ck(C19500uD r1, Runnable runnable, byte[] bArr, byte[] bArr2, int i) {
        this.A01 = r1;
        this.A00 = i;
        this.A03 = bArr;
        this.A04 = bArr2;
        this.A02 = runnable;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("BackupSendMethods/sendGetCipherKey/failed to deliver id=");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r9, String str) {
        for (AnonymousClass1V8 r1 : r9.A0J("error")) {
            if (r1 != null) {
                String A0I = r1.A0I("code", null);
                String A0I2 = r1.A0I("text", null);
                StringBuilder sb = new StringBuilder("BackupSendMethods/sendGetCipherKey id=");
                sb.append(str);
                sb.append(" error=");
                sb.append(A0I);
                sb.append(" ");
                sb.append(A0I2);
                Log.w(sb.toString());
                if (A0I != null) {
                    int parseInt = Integer.parseInt(A0I);
                    int i = this.A00;
                    byte[] bArr = this.A03;
                    byte[] bArr2 = this.A04;
                    Runnable runnable = this.A02;
                    Arrays.toString((byte[]) null);
                    Arrays.toString((byte[]) null);
                    Arrays.toString(bArr);
                    Arrays.toString(bArr2);
                    StringBuilder sb2 = new StringBuilder("BackupSendMethods/handleCreateCipherKeyResponse failed to create a key, creation_mode= ");
                    sb2.append(i);
                    sb2.append(", error_code=");
                    sb2.append(parseInt);
                    Log.e(sb2.toString());
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r16, java.lang.String r17) {
        /*
            r15 = this;
            r0 = 0
            r1 = r16
            X.1V8 r2 = r1.A0D(r0)
            X.AnonymousClass009.A05(r2)
            java.lang.String r0 = "crypto"
            X.AnonymousClass1V8.A01(r2, r0)
            java.lang.String r1 = "version"
            r0 = 0
            java.lang.String r5 = r2.A0I(r1, r0)
            java.lang.String r0 = "code"
            X.1V8 r0 = r2.A0F(r0)
            byte[] r7 = r0.A01
            java.lang.String r0 = "password"
            X.1V8 r0 = r2.A0F(r0)
            byte[] r6 = r0.A01
            int r4 = r15.A00
            byte[] r8 = r15.A03
            byte[] r14 = r15.A04
            java.lang.Runnable r2 = r15.A02
            java.lang.String r3 = ", error_code="
            java.util.Arrays.toString(r7)
            java.util.Arrays.toString(r6)
            java.util.Arrays.toString(r8)
            java.util.Arrays.toString(r14)
            r9 = 0
            if (r5 == 0) goto L_0x0077
            if (r7 == 0) goto L_0x0077
            if (r6 == 0) goto L_0x0077
            r0 = 1
            if (r4 == r0) goto L_0x0069
            r0 = 2
            if (r4 == r0) goto L_0x0061
            java.lang.String r0 = "BackupSendMethods/handleCreateCipherKeyResponse unknown creation mode "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
        L_0x0054:
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x005b:
            if (r2 == 0) goto L_0x0060
            r2.run()
        L_0x0060:
            return
        L_0x0061:
            X.0uD r0 = r15.A01
            X.0uB r4 = r0.A03
            r4.A00(r5, r6, r7, r8, r9)
            goto L_0x005b
        L_0x0069:
            X.0uD r0 = r15.A01
            X.0pI r0 = r0.A02
            android.content.Context r9 = r0.A00
            r10 = r5
            r11 = r7
            r12 = r6
            r13 = r8
            X.C32781cj.A09(r9, r10, r11, r12, r13, r14)
            goto L_0x005b
        L_0x0077:
            java.lang.String r0 = "BackupSendMethods/handleCreateCipherKeyResponse failed to create a key, creation_mode= "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r4)
            r1.append(r3)
            r1.append(r9)
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32791ck.AX9(X.1V8, java.lang.String):void");
    }
}
