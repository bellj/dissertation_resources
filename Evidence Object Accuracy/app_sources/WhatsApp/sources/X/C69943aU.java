package X;

import android.text.Layout;
import android.text.StaticLayout;
import android.widget.TextView;

/* renamed from: X.3aU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69943aU implements AnonymousClass5WJ {
    @Override // X.AnonymousClass5WJ
    public void Acs(TextView textView) {
    }

    @Override // X.AnonymousClass5WJ
    public Layout A8L(TextView textView, CharSequence charSequence, int i) {
        Layout layout = textView.getLayout();
        return new StaticLayout(AnonymousClass1US.A01(charSequence), textView.getPaint(), i, layout.getAlignment(), layout.getSpacingMultiplier(), layout.getSpacingAdd(), true);
    }
}
