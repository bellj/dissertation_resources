package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.4TJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4TJ {
    public final Drawable A00;
    public final AbstractC116285Uv A01;
    public final AbstractC116295Uw A02;
    public final String A03;
    public final String A04;
    public final String A05;

    public AnonymousClass4TJ(Drawable drawable, AbstractC116285Uv r2, AbstractC116295Uw r3, String str, String str2, String str3) {
        this.A00 = drawable;
        this.A04 = str;
        this.A03 = str2;
        this.A05 = str3;
        this.A01 = r2;
        this.A02 = r3;
    }
}
