package X;

import java.util.Comparator;
import java.util.SortedSet;

/* renamed from: X.4Yg  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Yg {
    public static Comparator comparator(SortedSet sortedSet) {
        Comparator comparator = sortedSet.comparator();
        return comparator == null ? AbstractC112285Cu.natural() : comparator;
    }

    public static boolean hasSameComparator(Comparator comparator, Iterable iterable) {
        Comparator comparator2;
        if (iterable instanceof SortedSet) {
            comparator2 = comparator((SortedSet) iterable);
        } else if (!(iterable instanceof AnonymousClass5Z0)) {
            return false;
        } else {
            comparator2 = ((AnonymousClass5Z0) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }
}
