package X;

import android.graphics.Rect;
import android.view.View;
import com.whatsapp.util.Log;

/* renamed from: X.1eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC33631eh {
    public View A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public boolean A05 = false;
    public final Rect A06 = new Rect();

    public void A00() {
        StringBuilder sb = new StringBuilder("playbackPage/onConfigurationChanged page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(((AbstractC33621eg) this).A0L.A01);
        Log.i(sb.toString());
    }

    public void A01() {
        this.A01 = false;
        StringBuilder sb = new StringBuilder("playbackPage/onDestroy page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(((AbstractC33621eg) this).A0L.A01);
        Log.i(sb.toString());
    }

    public void A02() {
        this.A03 = false;
        StringBuilder sb = new StringBuilder("playbackPage/onPause page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(((AbstractC33621eg) this).A0L.A01);
        Log.i(sb.toString());
    }

    public void A03() {
        this.A03 = true;
        StringBuilder sb = new StringBuilder("playbackPage/onResume page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(((AbstractC33621eg) this).A0L.A01);
        Log.i(sb.toString());
    }

    public void A04() {
        this.A04 = true;
        StringBuilder sb = new StringBuilder("playbackPage/onViewActive page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(((AbstractC33621eg) this).A0L.A01);
        Log.i(sb.toString());
    }

    public void A05() {
        this.A04 = false;
        StringBuilder sb = new StringBuilder("playbackPage/onViewInactive page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(((AbstractC33621eg) this).A0L.A01);
        Log.i(sb.toString());
    }

    public void A06(int i) {
        if (!(this instanceof AbstractC33621eg)) {
            this.A05 = false;
            return;
        }
        AbstractC33621eg r1 = (AbstractC33621eg) this;
        ((AbstractC33631eh) r1).A05 = false;
        r1.A0J(i);
    }

    public void A07(int i) {
        Integer num;
        if (!(this instanceof AbstractC33621eg)) {
            this.A05 = true;
            return;
        }
        AbstractC33621eg r2 = (AbstractC33621eg) this;
        ((AbstractC33631eh) r2).A05 = true;
        if (r2.A0A() instanceof AnonymousClass35V) {
            num = ((AnonymousClass35V) r2.A0A()).A01;
        } else {
            num = null;
        }
        r2.A0L(num, i, r2.A06);
    }

    public void A08(Rect rect) {
        this.A06.set(rect);
    }
}
