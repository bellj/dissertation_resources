package X;

import android.view.View;

/* renamed from: X.4my  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class View$OnLongClickListenerC101094my implements View.OnLongClickListener {
    public final /* synthetic */ int A00;

    public View$OnLongClickListenerC101094my(int i) {
        this.A00 = i;
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        return view.performAccessibilityAction(this.A00, null);
    }
}
