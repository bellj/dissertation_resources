package X;

import android.util.Pair;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.200  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass200 extends C248717f {
    public static final Set A00;
    public static final Set A01;

    static {
        HashSet hashSet = new HashSet();
        hashSet.add(Pair.create("com.facebook.wakizashi", "Xo8WBi6jzSxKDVR4drqm84yr9iU"));
        hashSet.add(Pair.create("com.whatsapp.intrumentation.sample", "Xo8WBi6jzSxKDVR4drqm84yr9iU"));
        hashSet.add(Pair.create("com.facebook.stella_debug", "Xo8WBi6jzSxKDVR4drqm84yr9iU"));
        hashSet.add(Pair.create("com.facebook.assistantplayground", "Xo8WBi6jzSxKDVR4drqm84yr9iU"));
        hashSet.add(Pair.create("com.whatsapp", "HfqsFpVx2hvmL2FpTQgY5bCSyHo"));
        A00 = Collections.unmodifiableSet(hashSet);
        HashSet hashSet2 = new HashSet();
        hashSet2.add(Pair.create("com.facebook.stella", "_H-OYUFZvtFrvtzR6NdYRD0eaTA"));
        A01 = Collections.unmodifiableSet(hashSet2);
    }

    public AnonymousClass200() {
        super(A00, A01);
    }
}
