package X;

import java.io.PrintWriter;
import java.io.StringWriter;

/* renamed from: X.4ut  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105954ut implements AbstractC12230ha {
    public final /* synthetic */ AnonymousClass4I1 A00;
    public final /* synthetic */ AnonymousClass4Cx A01;

    public C105954ut(AnonymousClass4I1 r1, AnonymousClass4Cx r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC12230ha
    public void AaW(AnonymousClass0SW r4, Throwable th) {
        String obj;
        Object[] objArr = new Object[4];
        C12960it.A1O(objArr, System.identityHashCode(this));
        C12980iv.A1T(objArr, System.identityHashCode(r4));
        objArr[2] = C12980iv.A0s(r4.A00());
        if (th == null) {
            obj = "";
        } else {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            obj = stringWriter.toString();
        }
        objArr[3] = obj;
        AnonymousClass0UN.A03("Fresco", "Finalized without closing: %x %x (type = %s).\nStack:\n%s", objArr);
    }
}
