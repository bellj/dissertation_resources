package X;

import android.text.Editable;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.whatsapp.CodeInputField;

/* renamed from: X.0l9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14280l9 extends PasswordTransformationMethod {
    public int A00 = -1;
    public C71143cS A01;
    public Runnable A02;
    public String A03 = "";
    public final CodeInputField A04;

    public C14280l9(CodeInputField codeInputField) {
        this.A04 = codeInputField;
    }

    @Override // android.text.method.PasswordTransformationMethod, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        super.afterTextChanged(editable);
        if (this.A02 == null) {
            this.A02 = new RunnableBRunnable0Shape0S0100000_I0(this, 20);
        }
        if (!this.A03.equals(editable.toString().replaceAll("[^0-9, ]", ""))) {
            this.A03 = editable.toString().replaceAll("[^0-9, ]", "");
            this.A01.A00(editable);
            CodeInputField codeInputField = this.A04;
            if (codeInputField.getHandler() != null) {
                codeInputField.getHandler().removeCallbacks(this.A02);
                codeInputField.getHandler().postDelayed(this.A02, 1500);
            }
        }
    }

    @Override // android.text.method.PasswordTransformationMethod, android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.beforeTextChanged(charSequence, i, i2, i3);
        CodeInputField codeInputField = this.A04;
        if (codeInputField.getHandler() != null && this.A02 != null) {
            codeInputField.getHandler().removeCallbacks(this.A02);
        }
    }

    @Override // android.text.method.PasswordTransformationMethod, android.text.method.TransformationMethod
    public CharSequence getTransformation(CharSequence charSequence, View view) {
        C71143cS r0 = new C71143cS(this, charSequence);
        this.A01 = r0;
        return r0;
    }
}
