package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1Zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30961Zm implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100134lQ();
    public final String A00;
    public final String[] A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public /* synthetic */ C30961Zm(Parcel parcel) {
        String readString = parcel.readString();
        AnonymousClass009.A05(readString);
        this.A00 = readString;
        String[] createStringArray = parcel.createStringArray();
        AnonymousClass009.A05(createStringArray);
        this.A01 = createStringArray;
    }

    public C30961Zm(String str, String[] strArr) {
        this.A00 = str;
        this.A01 = strArr;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeStringArray(this.A01);
    }
}
