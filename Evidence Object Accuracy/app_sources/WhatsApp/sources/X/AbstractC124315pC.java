package X;

import org.json.JSONObject;

/* renamed from: X.5pC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC124315pC extends AnonymousClass12Z {
    @Override // X.AnonymousClass12Z
    public void A00(JSONObject jSONObject, long j) {
        this.A00 = jSONObject.getJSONObject(A01(j)).getString("payload");
    }

    public String A01(long j) {
        if (!(this instanceof AnonymousClass5pB)) {
            if (4595048977247919L == j) {
                return "whatsapp_pmtd_bloks_getprivatelayout";
            }
            throw new IllegalArgumentException();
        } else if (j == 3651100555017197L) {
            return "whatsapp_bloks_getlayout";
        } else {
            if (j == 3958953970834604L) {
                return "whatsapp_bloks_getprivatelayout";
            }
            throw new IllegalArgumentException();
        }
    }
}
