package X;

/* renamed from: X.4ws  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107154ws implements AnonymousClass5X7 {
    public int A00;
    public int A01;
    public int A02 = 0;
    public long A03;
    public long A04;
    public AnonymousClass5X6 A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public final AnonymousClass4W1 A09;
    public final C95304dT A0A;
    public final String A0B;

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107154ws(String str) {
        C95304dT A05 = C95304dT.A05(4);
        this.A0A = A05;
        A05.A02[0] = -1;
        this.A09 = new AnonymousClass4W1();
        this.A0B = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d5, code lost:
        if ((r7[r8] & 224) != 224) goto L_0x00d7;
     */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r18) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107154ws.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r2, C92824Xo r3) {
        r3.A03();
        this.A06 = r3.A02();
        this.A05 = C92824Xo.A00(r2, r3);
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A04 = j;
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A02 = 0;
        this.A00 = 0;
        this.A08 = false;
    }
}
