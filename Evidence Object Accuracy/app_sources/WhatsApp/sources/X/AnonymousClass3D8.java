package X;

import android.content.Context;
import android.os.Process;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3D8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3D8 {
    public final int A00;
    public final int A01;
    public final int A02;
    public final RunnableFuture A03;
    public final AtomicInteger A04 = new AtomicInteger(-1);
    public volatile AnonymousClass3IP A05;

    public AnonymousClass3D8(Context context, AnonymousClass3IP r4, AnonymousClass5SD r5, Object obj, int i, int i2, int i3) {
        this.A05 = r4;
        this.A01 = i;
        this.A02 = i2;
        this.A00 = i3;
        this.A03 = new FutureTask(new CallableC71473cz(context, this, r5, obj));
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:23:0x000f */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: X.3IP */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [java.util.concurrent.atomic.AtomicInteger] */
    /* JADX WARN: Type inference failed for: r2v2, types: [X.3IP] */
    /* JADX WARN: Type inference failed for: r2v3 */
    public AnonymousClass3IP A00() {
        AnonymousClass3IP r2 = this.A04;
        boolean compareAndSet = r2.compareAndSet(-1, Process.myTid());
        RunnableFuture runnableFuture = this.A03;
        if (!compareAndSet) {
            return (AnonymousClass3IP) AnonymousClass3J3.A01(runnableFuture, r2.get());
        }
        try {
            runnableFuture.run();
            r2 = 0;
            r2 = 0;
            try {
                return (AnonymousClass3IP) runnableFuture.get();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e2) {
                if (e2.getCause() instanceof RuntimeException) {
                    throw ((RuntimeException) e2.getCause());
                }
                throw new RuntimeException(e2.getCause());
            }
        } finally {
            this.A05 = r2;
        }
    }
}
