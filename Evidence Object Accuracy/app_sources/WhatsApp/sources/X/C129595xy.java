package X;

import java.util.List;

/* renamed from: X.5xy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C129595xy {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3EM A01;
    public final List A02;

    public C129595xy(AbstractC15710nm r15, AnonymousClass1V8 r16, C126265sb r17) {
        AnonymousClass1V8.A01(r16, "iq");
        AnonymousClass1V8 r5 = r17.A00;
        AnonymousClass3JT.A04(null, r16, String.class, C12970iu.A0j(), C12970iu.A0k(), "br-get-payout-banks", new String[]{"account", "action"}, false);
        this.A01 = (AnonymousClass3EM) AnonymousClass3JT.A05(r16, new AbstractC116095Uc(r15, r5) { // from class: X.6DR
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r4) {
                return new AnonymousClass3EM(this.A00, r4, this.A01);
            }
        }, new String[0]);
        this.A02 = AnonymousClass3JT.A0B(r16, new AbstractC116095Uc() { // from class: X.6DO
            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r2) {
                return new AnonymousClass60D(r2);
            }
        }, new String[]{"account", "bank"}, 0, Long.MAX_VALUE);
        this.A00 = r16;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C129595xy.class != obj.getClass()) {
                return false;
            }
            C129595xy r5 = (C129595xy) obj;
            if (!this.A02.equals(r5.A02) || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        return C12960it.A06(this.A01, A1a);
    }
}
