package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C462225a extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C462225a A03;
    public static volatile AnonymousClass255 A04;
    public float A00;
    public int A01;
    public String A02 = "";

    static {
        C462225a r0 = new C462225a();
        A03 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A01 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A02);
        }
        if ((this.A01 & 2) == 2) {
            i2 += 5;
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A02);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0D(2, Float.floatToRawIntBits(this.A00));
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
