package X;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.List;

/* renamed from: X.63U  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63U implements Handler.Callback {
    public final /* synthetic */ AnonymousClass60C A00;

    public /* synthetic */ AnonymousClass63U(AnonymousClass60C r1) {
        this.A00 = r1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        C129775yH r1 = this.A00.A05;
        if (!r1.A00.isEmpty()) {
            List list = r1.A00;
            int i = 0;
            switch (message.what) {
                case 0:
                    while (i < list.size()) {
                        list.get(i);
                        int i2 = message.arg1;
                        StringBuilder A0k = C12960it.A0k("Time to take photo: ");
                        A0k.append(i2);
                        Log.d("Camera1Performance", C12960it.A0d("ms", A0k));
                        i++;
                    }
                    break;
                case 1:
                    while (i < list.size()) {
                        list.get(i);
                        int i3 = message.arg1;
                        StringBuilder A0k2 = C12960it.A0k("Time to switch camera: ");
                        A0k2.append(i3);
                        Log.d("Camera1Performance", C12960it.A0d("ms", A0k2));
                        i++;
                    }
                    break;
                case 2:
                    while (i < list.size()) {
                        list.get(i);
                        i++;
                    }
                    break;
                case 3:
                    while (i < list.size()) {
                        list.get(i);
                        i++;
                    }
                    break;
                case 4:
                    while (i < list.size()) {
                        list.get(i);
                        int i4 = message.arg1;
                        StringBuilder A0k3 = C12960it.A0k("Time for first surface texture update: ");
                        A0k3.append(i4);
                        Log.d("Camera1Performance", C12960it.A0d("ms", A0k3));
                        i++;
                    }
                    break;
                case 5:
                    while (i < list.size()) {
                        list.get(i);
                        int i5 = message.arg1;
                        StringBuilder A0k4 = C12960it.A0k("Time for first preview frame: ");
                        A0k4.append(i5);
                        Log.d("Camera1Performance", C12960it.A0d("ms", A0k4));
                        i++;
                    }
                    break;
                case 6:
                    while (i < list.size()) {
                        list.get(i);
                        int i6 = message.arg1;
                        StringBuilder A0k5 = C12960it.A0k("Time to complete capture: ");
                        A0k5.append(i6);
                        Log.d("Camera1Performance", C12960it.A0d("ms", A0k5));
                        i++;
                    }
                    break;
            }
        }
        return true;
    }
}
