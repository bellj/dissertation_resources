package X;

import android.graphics.Path;
import android.util.Log;

/* renamed from: X.06w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C014506w {
    public char A00;
    public float[] A01;

    public C014506w(C014506w r5) {
        this.A00 = r5.A00;
        float[] fArr = r5.A01;
        int length = fArr.length - 0;
        int min = Math.min(length, length);
        float[] fArr2 = new float[length];
        System.arraycopy(fArr, 0, fArr2, 0, min);
        this.A01 = fArr2;
    }

    public C014506w(float[] fArr, char c) {
        this.A00 = c;
        this.A01 = fArr;
    }

    public static void A00(Path path, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z, boolean z2) {
        double d;
        double d2;
        double radians = Math.toRadians((double) f7);
        double cos = Math.cos(radians);
        double sin = Math.sin(radians);
        double d3 = (double) f;
        double d4 = d3;
        double d5 = (double) f2;
        double d6 = (d3 * cos) + (d5 * sin);
        double d7 = (double) f5;
        double d8 = d6 / d7;
        double d9 = (double) f6;
        double d10 = ((((double) (-f)) * sin) + (d5 * cos)) / d9;
        double d11 = (double) f4;
        double d12 = ((((double) f3) * cos) + (d11 * sin)) / d7;
        double d13 = ((((double) (-f3)) * sin) + (d11 * cos)) / d9;
        double d14 = d8 - d12;
        double d15 = d10 - d13;
        double d16 = (d8 + d12) / 2.0d;
        double d17 = (d10 + d13) / 2.0d;
        double d18 = (d14 * d14) + (d15 * d15);
        if (d18 == 0.0d) {
            Log.w("PathParser", " Points are coincident");
            return;
        }
        double d19 = (1.0d / d18) - 0.25d;
        if (d19 < 0.0d) {
            StringBuilder sb = new StringBuilder("Points are too far apart ");
            sb.append(d18);
            Log.w("PathParser", sb.toString());
            float sqrt = (float) (Math.sqrt(d18) / 1.99999d);
            A00(path, f, f2, f3, f4, f5 * sqrt, f6 * sqrt, f7, z, z2);
            return;
        }
        double sqrt2 = Math.sqrt(d19);
        double d20 = d14 * sqrt2;
        double d21 = sqrt2 * d15;
        if (z == z2) {
            d = d16 - d21;
            d2 = d17 + d20;
        } else {
            d = d16 + d21;
            d2 = d17 - d20;
        }
        double atan2 = Math.atan2(d10 - d2, d8 - d);
        double atan22 = Math.atan2(d13 - d2, d12 - d) - atan2;
        boolean z3 = false;
        if (atan22 >= 0.0d) {
            z3 = true;
        }
        if (z2 != z3) {
            atan22 = atan22 > 0.0d ? atan22 - 6.283185307179586d : atan22 + 6.283185307179586d;
        }
        double d22 = d * d7;
        double d23 = d2 * d9;
        double d24 = (d22 * cos) - (d23 * sin);
        double d25 = (d22 * sin) + (d23 * cos);
        int ceil = (int) Math.ceil(Math.abs((atan22 * 4.0d) / 3.141592653589793d));
        double cos2 = Math.cos(atan2);
        double sin2 = Math.sin(atan2);
        double d26 = -d7;
        double d27 = d26 * cos;
        double d28 = d9 * sin;
        double d29 = (d27 * sin2) - (d28 * cos2);
        double d30 = d26 * sin;
        double d31 = d9 * cos;
        double d32 = (sin2 * d30) + (cos2 * d31);
        double d33 = atan22 / ((double) ceil);
        int i = 0;
        while (i < ceil) {
            double d34 = atan2 + d33;
            double sin3 = Math.sin(d34);
            double cos3 = Math.cos(d34);
            double d35 = (d24 + ((d7 * cos) * cos3)) - (d28 * sin3);
            double d36 = d25 + (d7 * sin * cos3) + (d31 * sin3);
            double d37 = (d27 * sin3) - (d28 * cos3);
            double d38 = (sin3 * d30) + (cos3 * d31);
            double d39 = d34 - atan2;
            double tan = Math.tan(d39 / 2.0d);
            double sin4 = (Math.sin(d39) * (Math.sqrt(((tan * 3.0d) * tan) + 4.0d) - 1.0d)) / 3.0d;
            path.rLineTo(0.0f, 0.0f);
            path.cubicTo((float) (d4 + (d29 * sin4)), (float) (d5 + (d32 * sin4)), (float) (d35 - (sin4 * d37)), (float) (d36 - (sin4 * d38)), (float) d35, (float) d36);
            i++;
            d4 = d35;
            atan2 = d34;
            d32 = d38;
            d29 = d37;
            d5 = d36;
        }
    }

    public static void A01(Path path, C014506w[] r35) {
        int i;
        int i2;
        int i3;
        int i4;
        float f;
        float f2;
        int i5;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        int i6;
        int i7;
        float f9;
        float f10;
        float[] fArr = new float[6];
        char c = 'm';
        for (int i8 = 0; i8 < r35.length; i8++) {
            C014506w r0 = r35[i8];
            char c2 = r0.A00;
            float[] fArr2 = r0.A01;
            float f11 = fArr[0];
            float f12 = fArr[1];
            float f13 = fArr[2];
            float f14 = fArr[3];
            float f15 = fArr[4];
            float f16 = fArr[5];
            switch (c2) {
                case 'A':
                case 'a':
                    i = 7;
                    break;
                case 'C':
                case 'c':
                    i = 6;
                    break;
                case C43951xu.A02 /* 72 */:
                case 'V':
                case 'h':
                case 'v':
                    i = 1;
                    break;
                case 'Q':
                case 'S':
                case 'q':
                case 's':
                    i = 4;
                    break;
                case 'Z':
                case 'z':
                    path.close();
                    f11 = f15;
                    path.moveTo(f11, f16);
                    f13 = f11;
                    f12 = f16;
                    f14 = f16;
                default:
                    i = 2;
                    break;
            }
            int i9 = 0;
            while (i9 < fArr2.length) {
                if (c2 != 'A') {
                    if (c2 == 'C') {
                        int i10 = i9 + 2;
                        int i11 = i9 + 3;
                        int i12 = i9 + 4;
                        int i13 = i9 + 5;
                        path.cubicTo(fArr2[i9], fArr2[i9 + 1], fArr2[i10], fArr2[i11], fArr2[i12], fArr2[i13]);
                        f11 = fArr2[i12];
                        f12 = fArr2[i13];
                        f13 = fArr2[i10];
                        f14 = fArr2[i11];
                    } else if (c2 != 'H') {
                        if (c2 == 'Q') {
                            int i14 = i9 + 1;
                            i3 = i9 + 2;
                            i2 = i9 + 3;
                            path.quadTo(fArr2[i9], fArr2[i14], fArr2[i3], fArr2[i2]);
                            f13 = fArr2[i9];
                            f14 = fArr2[i14];
                        } else if (c2 == 'V') {
                            path.lineTo(f11, fArr2[i9]);
                            f12 = fArr2[i9];
                        } else if (c2 != 'a') {
                            if (c2 == 'c') {
                                f8 = fArr2[i9];
                                f7 = fArr2[i9 + 1];
                                i6 = i9 + 2;
                                f6 = fArr2[i6];
                                i7 = i9 + 3;
                                f5 = fArr2[i7];
                                i5 = i9 + 4;
                                f4 = fArr2[i5];
                                i4 = i9 + 5;
                            } else if (c2 == 'h') {
                                path.rLineTo(fArr2[i9], 0.0f);
                                f11 += fArr2[i9];
                            } else if (c2 != 'q') {
                                if (c2 == 'v') {
                                    path.rLineTo(0.0f, fArr2[i9]);
                                    f2 = fArr2[i9];
                                } else if (c2 == 'L') {
                                    i2 = i9 + 1;
                                    path.lineTo(fArr2[i9], fArr2[i2]);
                                    f11 = fArr2[i9];
                                    f12 = fArr2[i2];
                                } else if (c2 == 'M') {
                                    f11 = fArr2[i9];
                                    f12 = fArr2[i9 + 1];
                                    if (i9 > 0) {
                                        path.lineTo(f11, f12);
                                    } else {
                                        path.moveTo(f11, f12);
                                        f16 = f12;
                                        f15 = f11;
                                    }
                                } else if (c2 == 'S') {
                                    if (c == 'c' || c == 's' || c == 'C' || c == 'S') {
                                        f11 = (f11 * 2.0f) - f13;
                                        f12 = (f12 * 2.0f) - f14;
                                    }
                                    int i15 = i9 + 1;
                                    i3 = i9 + 2;
                                    i2 = i9 + 3;
                                    path.cubicTo(f11, f12, fArr2[i9], fArr2[i15], fArr2[i3], fArr2[i2]);
                                    f13 = fArr2[i9];
                                    f14 = fArr2[i15];
                                } else if (c2 == 'T') {
                                    if (c == 'q' || c == 't' || c == 'Q' || c == 'T') {
                                        f11 = (f11 * 2.0f) - f13;
                                        f12 = (f12 * 2.0f) - f14;
                                    }
                                    int i16 = i9 + 1;
                                    path.quadTo(f11, f12, fArr2[i9], fArr2[i16]);
                                    float f17 = fArr2[i9];
                                    f12 = fArr2[i16];
                                    f14 = f12;
                                    f13 = f11;
                                    f11 = f17;
                                } else if (c2 == 'l') {
                                    i4 = i9 + 1;
                                    path.rLineTo(fArr2[i9], fArr2[i4]);
                                    f = fArr2[i9];
                                    f11 += f;
                                    f2 = fArr2[i4];
                                } else if (c2 == 'm') {
                                    float f18 = fArr2[i9];
                                    f11 += f18;
                                    float f19 = fArr2[i9 + 1];
                                    f12 += f19;
                                    if (i9 > 0) {
                                        path.rLineTo(f18, f19);
                                    } else {
                                        path.rMoveTo(f18, f19);
                                        f16 = f12;
                                        f15 = f11;
                                    }
                                } else if (c2 == 's') {
                                    if (c == 'c' || c == 's' || c == 'C' || c == 'S') {
                                        f8 = f11 - f13;
                                        f7 = f12 - f14;
                                    } else {
                                        f8 = 0.0f;
                                        f7 = 0.0f;
                                    }
                                    i6 = i9;
                                    f6 = fArr2[i9];
                                    i7 = i9 + 1;
                                    f5 = fArr2[i7];
                                    i5 = i9 + 2;
                                    f4 = fArr2[i5];
                                    i4 = i9 + 3;
                                } else if (c2 == 't') {
                                    if (c == 'q' || c == 't' || c == 'Q' || c == 'T') {
                                        f9 = f11 - f13;
                                        f10 = f12 - f14;
                                    } else {
                                        f10 = 0.0f;
                                        f9 = 0.0f;
                                    }
                                    int i17 = i9 + 1;
                                    path.rQuadTo(f9, f10, fArr2[i9], fArr2[i17]);
                                    f13 = f9 + f11;
                                    f14 = f10 + f12;
                                    f11 += fArr2[i9];
                                    f2 = fArr2[i17];
                                }
                                f12 += f2;
                            } else {
                                int i18 = i9 + 1;
                                i5 = i9 + 2;
                                i4 = i9 + 3;
                                path.rQuadTo(fArr2[i9], fArr2[i18], fArr2[i5], fArr2[i4]);
                                f13 = fArr2[i9] + f11;
                                f3 = fArr2[i18];
                                f14 = f3 + f12;
                                f = fArr2[i5];
                                f11 += f;
                                f2 = fArr2[i4];
                                f12 += f2;
                            }
                            path.rCubicTo(f8, f7, f6, f5, f4, fArr2[i4]);
                            f13 = fArr2[i6] + f11;
                            f3 = fArr2[i7];
                            f14 = f3 + f12;
                            f = fArr2[i5];
                            f11 += f;
                            f2 = fArr2[i4];
                            f12 += f2;
                        } else {
                            int i19 = i9 + 5;
                            float f20 = fArr2[i19] + f11;
                            int i20 = i9 + 6;
                            float f21 = fArr2[i20] + f12;
                            float f22 = fArr2[i9];
                            float f23 = fArr2[i9 + 1];
                            float f24 = fArr2[i9 + 2];
                            boolean z = false;
                            if (fArr2[i9 + 3] != 0.0f) {
                                z = true;
                            }
                            boolean z2 = false;
                            if (fArr2[i9 + 4] != 0.0f) {
                                z2 = true;
                            }
                            A00(path, f11, f12, f20, f21, f22, f23, f24, z, z2);
                            f11 += fArr2[i19];
                            f12 += fArr2[i20];
                        }
                        f11 = fArr2[i3];
                        f12 = fArr2[i2];
                    } else {
                        path.lineTo(fArr2[i9], f12);
                        f11 = fArr2[i9];
                    }
                    i9 += i;
                    c = c2;
                } else {
                    int i21 = i9 + 5;
                    float f25 = fArr2[i21];
                    int i22 = i9 + 6;
                    float f26 = fArr2[i22];
                    float f27 = fArr2[i9];
                    float f28 = fArr2[i9 + 1];
                    float f29 = fArr2[i9 + 2];
                    boolean z3 = false;
                    if (fArr2[i9 + 3] != 0.0f) {
                        z3 = true;
                    }
                    boolean z4 = false;
                    if (fArr2[i9 + 4] != 0.0f) {
                        z4 = true;
                    }
                    A00(path, f11, f12, f25, f26, f27, f28, f29, z3, z4);
                    f11 = fArr2[i21];
                    f12 = fArr2[i22];
                }
                f14 = f12;
                f13 = f11;
                i9 += i;
                c = c2;
            }
            fArr[0] = f11;
            fArr[1] = f12;
            fArr[2] = f13;
            fArr[3] = f14;
            fArr[4] = f15;
            fArr[5] = f16;
            c = r35[i8].A00;
        }
    }
}
