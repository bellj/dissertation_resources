package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0mh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class ServiceConnectionC15180mh implements ServiceConnection {
    public int A00 = 0;
    public AnonymousClass3BJ A01;
    public final Messenger A02 = new Messenger(new HandlerC13540jt(Looper.getMainLooper(), new Handler.Callback(this) { // from class: X.4iL
        public final ServiceConnectionC15180mh A00;

        {
            this.A00 = r1;
        }

        @Override // android.os.Handler.Callback
        public final boolean handleMessage(Message message) {
            return this.A00.A02(message);
        }
    }));
    public final SparseArray A03 = new SparseArray();
    public final Queue A04 = new ArrayDeque();
    public final /* synthetic */ C13570jw A05;

    public /* synthetic */ ServiceConnectionC15180mh(C13570jw r4) {
        this.A05 = r4;
    }

    public final synchronized void A00() {
        if (this.A00 == 2 && this.A04.isEmpty() && this.A03.size() == 0) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
            }
            this.A00 = 3;
            AnonymousClass3IW.A00().A01(this.A05.A02, this);
        }
    }

    public final synchronized void A01(int i, String str) {
        SparseArray sparseArray;
        String str2;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                str2 = "Disconnected: ".concat(valueOf);
            } else {
                str2 = new String("Disconnected: ");
            }
            Log.d("MessengerIpcClient", str2);
        }
        int i2 = this.A00;
        if (i2 == 0) {
            throw new IllegalStateException();
        } else if (i2 == 1 || i2 == 2) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Unbinding service");
            }
            this.A00 = 4;
            AnonymousClass3IW.A00().A01(this.A05.A02, this);
            C13680k9 r3 = new C13680k9(i, str);
            Queue<AbstractC13590jy> queue = this.A04;
            for (AbstractC13590jy r0 : queue) {
                r0.A01(r3);
            }
            queue.clear();
            int i3 = 0;
            while (true) {
                sparseArray = this.A03;
                if (i3 >= sparseArray.size()) {
                    break;
                }
                ((AbstractC13590jy) sparseArray.valueAt(i3)).A01(r3);
                i3++;
            }
            sparseArray.clear();
        } else if (i2 == 3) {
            this.A00 = 4;
        } else if (i2 != 4) {
            StringBuilder sb = new StringBuilder(26);
            sb.append("Unknown state: ");
            sb.append(i2);
            throw new IllegalStateException(sb.toString());
        }
    }

    public final boolean A02(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Received response to request: ");
            sb.append(i);
            Log.d("MessengerIpcClient", sb.toString());
        }
        synchronized (this) {
            SparseArray sparseArray = this.A03;
            AbstractC13590jy r4 = (AbstractC13590jy) sparseArray.get(i);
            if (r4 == null) {
                StringBuilder sb2 = new StringBuilder(50);
                sb2.append("Received response for unknown request: ");
                sb2.append(i);
                Log.w("MessengerIpcClient", sb2.toString());
                return true;
            }
            sparseArray.remove(i);
            A00();
            Bundle data = message.getData();
            if (data.getBoolean("unsupported", false)) {
                r4.A01(new C13680k9(4, "Not supported by GmsCore"));
                return true;
            }
            r4.A00(data);
            return true;
        }
    }

    public final synchronized boolean A03(AbstractC13590jy r10) {
        int i = this.A00;
        if (i == 0) {
            this.A04.add(r10);
            if (this.A00 == 0) {
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Starting bind to GmsCore");
                }
                this.A00 = 1;
                Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                intent.setPackage("com.google.android.gms");
                AnonymousClass3IW A00 = AnonymousClass3IW.A00();
                C13570jw r1 = this.A05;
                Context context = r1.A02;
                if (!A00.A02(context, intent, this, context.getClass().getName(), 1)) {
                    A01(0, "Unable to bind to service");
                } else {
                    r1.A03.schedule(new RunnableBRunnable0Shape0S0100000_I0(this, 14), 30, TimeUnit.SECONDS);
                }
            } else {
                throw new IllegalStateException();
            }
        } else if (i == 1) {
            this.A04.add(r10);
        } else if (i == 2) {
            this.A04.add(r10);
            this.A05.A03.execute(new RunnableC55442iT(this));
        } else if (i == 3 || i == 4) {
            return false;
        } else {
            StringBuilder sb = new StringBuilder(26);
            sb.append("Unknown state: ");
            sb.append(i);
            throw new IllegalStateException(sb.toString());
        }
        return true;
    }

    @Override // android.content.ServiceConnection
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        this.A05.A03.execute(new RunnableBRunnable0Shape0S0200000_I0(this, 3, iBinder));
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        this.A05.A03.execute(new RunnableBRunnable0Shape0S0100000_I0(this, 12));
    }
}
