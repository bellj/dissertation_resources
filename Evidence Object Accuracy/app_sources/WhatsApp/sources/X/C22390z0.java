package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0z0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22390z0 {
    public final C20740wF A00;
    public final C22420z3 A01;
    public final AbstractC14440lR A02;
    public final Map A03 = new HashMap();

    public C22390z0(C20740wF r2, C22420z3 r3, AbstractC14440lR r4) {
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r3;
    }

    public final synchronized AnonymousClass22P A00(AbstractC14640lm r4) {
        AnonymousClass22P r1;
        Map map = this.A03;
        r1 = (AnonymousClass22P) map.get(r4.getRawString());
        if (r1 == null) {
            r1 = new AnonymousClass22P(this);
            map.put(r4.getRawString(), r1);
        }
        return r1;
    }

    public void A01(AbstractC15340mz r5) {
        AnonymousClass1IS r3 = r5.A0z;
        AbstractC14640lm r0 = r3.A00;
        AnonymousClass009.A05(r0);
        AnonymousClass22P A00 = A00(r0);
        synchronized (A00) {
            boolean z = false;
            if (A00.A01.remove(r3) != null) {
                z = true;
            }
            A00.A00.remove(r3);
            A00.toString();
            if (z) {
                A00.A00();
            }
        }
    }
}
