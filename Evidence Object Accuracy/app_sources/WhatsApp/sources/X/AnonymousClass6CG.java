package X;

import android.view.View;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.payments.ui.BrazilPaymentActivity;

/* renamed from: X.6CG  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CG implements AnonymousClass6MZ {
    public final /* synthetic */ C30821Yy A00;
    public final /* synthetic */ AbstractC28901Pl A01;
    public final /* synthetic */ AnonymousClass1KC A02;
    public final /* synthetic */ PinBottomSheetDialogFragment A03;
    public final /* synthetic */ BrazilPaymentActivity A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;

    public AnonymousClass6CG(C30821Yy r1, AbstractC28901Pl r2, AnonymousClass1KC r3, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, BrazilPaymentActivity brazilPaymentActivity, String str, String str2) {
        this.A04 = brazilPaymentActivity;
        this.A03 = pinBottomSheetDialogFragment;
        this.A00 = r1;
        this.A01 = r2;
        this.A06 = str;
        this.A05 = str2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass6MZ
    public void AOP(String str) {
        this.A03.A1N();
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        int intValue = this.A00.A00.scaleByPowerOfTen(3).intValue();
        AbstractC30791Yv r3 = C30771Yt.A04;
        C94074bD r2 = new C94074bD();
        r2.A02 = (long) intValue;
        r2.A01 = 1000;
        r2.A03 = r3;
        AnonymousClass607 A2p = brazilPaymentActivity.A2p(this.A01, r2.A00(), this.A06, "payment_pin", brazilPaymentActivity.A0Z);
        C133316Ah r32 = new C133316Ah(this);
        A2p.A0Q.Ab2(new AnonymousClass6FU(A2p));
        AnonymousClass60T r1 = A2p.A0H;
        String str2 = A2p.A0U;
        AnonymousClass6B7 A0C = C117315Zl.A0C(r1, str2, "PIN");
        if (A0C != null) {
            C128545wH r22 = new C128545wH(A0C);
            A2p.A0F.A00(new C1330469g(A2p, r32, r22), r22, str);
            return;
        }
        r32.A00.A04.A0N.A02(185478830, "br_get_provider_key_tag");
        A2p.A0G.A00(new C133426As(A2p, r32, str), str2);
    }

    @Override // X.AnonymousClass6MZ
    public void AQi(View view) {
        BrazilPaymentActivity brazilPaymentActivity = this.A04;
        C125605rW r3 = new C125605rW(brazilPaymentActivity);
        C12960it.A1E(new C124065oO(r3, ((AbstractActivityC121685jC) brazilPaymentActivity).A0P), ((ActivityC13830kP) brazilPaymentActivity).A05);
    }
}
