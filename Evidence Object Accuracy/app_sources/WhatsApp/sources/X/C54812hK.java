package X;

import android.content.res.Resources;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2hK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54812hK extends AbstractC05270Ox {
    public Resources A00;
    public final GridLayoutManager A01;
    public final boolean A02;

    public C54812hK(Resources resources, GridLayoutManager gridLayoutManager, C14850m9 r4) {
        this.A01 = gridLayoutManager;
        this.A00 = resources;
        this.A02 = r4.A07(272);
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        A02(recyclerView);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0044, code lost:
        if (r1 < (r7 + r5)) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0066, code lost:
        if (r12 != false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006a, code lost:
        if (r1 <= r6) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006f, code lost:
        if (r1 <= r8) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A02(androidx.recyclerview.widget.RecyclerView r14) {
        /*
            r13 = this;
            androidx.recyclerview.widget.GridLayoutManager r10 = r13.A01
            int r9 = r10.A1A()
            int r8 = r10.A1C()
            int r7 = r10.A19()
            int r6 = r10.A1B()
            int r5 = r10.A00
            android.content.res.Resources r0 = r13.A00
            android.content.res.Configuration r0 = r0.getConfiguration()
            int r0 = r0.orientation
            r4 = 1
            boolean r12 = X.C12960it.A1V(r0, r4)
            int r3 = r10.A06()
            r2 = 0
        L_0x0026:
            if (r2 >= r3) goto L_0x0074
            android.view.View r0 = r10.A0D(r2)
            if (r0 == 0) goto L_0x0074
            int r1 = androidx.recyclerview.widget.RecyclerView.A00(r0)
            X.03U r11 = r14.A0E(r0)
            X.2hl r11 = (X.C55082hl) r11
            boolean r0 = r13.A02
            if (r0 == 0) goto L_0x0066
            if (r12 == 0) goto L_0x006d
            if (r8 == r6) goto L_0x0068
            if (r1 < r7) goto L_0x0072
            int r0 = r7 + r5
            if (r1 >= r0) goto L_0x0072
        L_0x0046:
            r1 = 1
        L_0x0047:
            boolean r0 = r11.A02
            if (r0 == r1) goto L_0x005a
            r11.A02 = r1
            if (r1 == 0) goto L_0x005d
            boolean r0 = r11.A03
            if (r0 == 0) goto L_0x005d
            com.whatsapp.stickers.StickerView r0 = r11.A07
            r0.A04 = r4
            r0.A02()
        L_0x005a:
            int r2 = r2 + 1
            goto L_0x0026
        L_0x005d:
            com.whatsapp.stickers.StickerView r1 = r11.A07
            r0 = 0
            r1.A04 = r0
            r1.A03()
            goto L_0x005a
        L_0x0066:
            if (r12 == 0) goto L_0x006d
        L_0x0068:
            if (r1 < r7) goto L_0x0072
            if (r1 > r6) goto L_0x0072
            goto L_0x0046
        L_0x006d:
            if (r1 < r9) goto L_0x0072
            if (r1 > r8) goto L_0x0072
            goto L_0x0046
        L_0x0072:
            r1 = 0
            goto L_0x0047
        L_0x0074:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54812hK.A02(androidx.recyclerview.widget.RecyclerView):void");
    }
}
