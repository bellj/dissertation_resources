package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.1xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43791xb extends AbstractC16280ok {
    public final C231410n A00;

    public C43791xb(AbstractC15710nm r8, C16590pI r9, C231410n r10, String str) {
        super(r9.A00, r8, str, new ReentrantReadWriteLock(), 1, true);
        this.A00 = r10;
    }

    public static final void A00(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append(" ");
        sb.append(str3);
        if (!str.contains(sb.toString())) {
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("ALTER TABLE cart_item ADD ");
                sb2.append(str2);
                sb2.append(" ");
                sb2.append(str3);
                sQLiteDatabase.execSQL(sb2.toString());
            } catch (SQLiteException e) {
                StringBuilder sb3 = new StringBuilder("commerce-db-helper/add-column ");
                sb3.append(str2);
                Log.e(sb3.toString(), e);
            }
        }
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        try {
            return AnonymousClass1Tx.A01(super.A00(), this.A00);
        } catch (SQLiteException e) {
            Log.e("failed to open writable commerce store", e);
            return AnonymousClass1Tx.A01(super.A00(), this.A00);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS cart_item");
        sQLiteDatabase.execSQL("CREATE TABLE cart_item(_id INTEGER PRIMARY KEY AUTOINCREMENT,business_id TEXT NOT NULL, product_id TEXT NOT NULL, product_title TEXT, product_price_1000 INTEGER, product_currency_code TEXT, product_image_id TEXT, product_quantity INTEGER, product_sale_price_1000 INTEGER, product_sale_start_date TIMESTAMP, product_sale_end_date TIMESTAMP, product_max_available INTEGER)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS business_id_index on cart_item (business_id)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }

    @Override // X.AbstractC16280ok, android.database.sqlite.SQLiteOpenHelper
    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        Cursor rawQuery;
        super.onOpen(sQLiteDatabase);
        String str = "";
        try {
            rawQuery = sQLiteDatabase.rawQuery("select sql from sqlite_master where type='table' and name='cart_item';", null);
        } catch (Exception e) {
            Log.e("commerce-db-helper/schema cart_item", e);
        }
        if (rawQuery != null) {
            if (rawQuery.moveToNext()) {
                str = rawQuery.getString(0);
            }
            rawQuery.close();
            if (str == null) {
                return;
            }
        }
        A00(sQLiteDatabase, str, "product_sale_price_1000", "INTEGER");
        A00(sQLiteDatabase, str, "product_sale_start_date", "TIMESTAMP");
        A00(sQLiteDatabase, str, "product_sale_end_date", "TIMESTAMP");
        A00(sQLiteDatabase, str, "product_max_available", "INTEGER");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        onCreate(sQLiteDatabase);
    }
}
