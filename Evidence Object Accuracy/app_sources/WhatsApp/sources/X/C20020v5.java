package X;

import android.content.SharedPreferences;
import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.0v5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20020v5 {
    public SharedPreferences A00;
    public final C15570nT A01;
    public final C14650lo A02;
    public final C15550nR A03;
    public final C22700zV A04;
    public final AbstractC40571ro A05;
    public final C40531rk A06;
    public final AnonymousClass1EB A07;
    public final C14830m7 A08;
    public final C22610zM A09;
    public final C16510p9 A0A;
    public final C19990v2 A0B;
    public final C15650ng A0C;
    public final C15600nX A0D;
    public final C20040v7 A0E;
    public final AnonymousClass14Z A0F = new C40541rl(this);
    public final AbstractC18860tB A0G;
    public final C20140vH A0H;
    public final AnonymousClass134 A0I;
    public final C242114q A0J;
    public final C22180yf A0K;
    public final AnonymousClass150 A0L;
    public final C14850m9 A0M;
    public final C16120oU A0N;
    public final C16630pM A0O;
    public final C15860o1 A0P;
    public final C240514a A0Q;
    public final ExecutorC27271Gr A0R;
    public final C236712o A0S;

    public C20020v5(C15570nT r9, C14650lo r10, C15550nR r11, C22700zV r12, AnonymousClass1EC r13, C22620zN r14, AnonymousClass1EB r15, C14830m7 r16, C22610zM r17, C16510p9 r18, C19990v2 r19, C15650ng r20, C15600nX r21, C20040v7 r22, AnonymousClass12H r23, C20140vH r24, AnonymousClass134 r25, C242114q r26, C22180yf r27, AnonymousClass150 r28, C14850m9 r29, C16120oU r30, C16630pM r31, C15860o1 r32, C240514a r33, AbstractC14440lR r34, C21260x8 r35) {
        C40531rk r5 = new C40531rk(this);
        this.A06 = r5;
        C40551rm r4 = new C40551rm(this);
        this.A0S = r4;
        C40561rn r3 = new C40561rn(this);
        this.A05 = r3;
        C35371hi r2 = new C35371hi(this);
        this.A0G = r2;
        this.A08 = r16;
        this.A0M = r29;
        this.A0A = r18;
        this.A01 = r9;
        this.A0B = r19;
        this.A0N = r30;
        this.A0H = r24;
        this.A0I = r25;
        this.A0E = r22;
        this.A0K = r27;
        this.A03 = r11;
        this.A0C = r20;
        this.A0P = r32;
        this.A0Q = r33;
        this.A0J = r26;
        this.A04 = r12;
        this.A02 = r10;
        this.A0D = r21;
        this.A0O = r31;
        this.A09 = r17;
        this.A07 = r15;
        this.A0L = r28;
        this.A0R = new ExecutorC27271Gr(r34, false);
        r23.A03(r2);
        r13.A03(r3);
        r35.A03(r4);
        if (r29.A07(1022)) {
            r14.A03(r5);
        }
    }

    public static String A00(AbstractC27881Jp r3, String str, String str2) {
        byte[] bArr;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        try {
            byte[] bytes = new String(sb.toString().getBytes(), AnonymousClass01V.A0A).getBytes();
            byte[] A04 = r3.A04();
            Mac instance = Mac.getInstance(DefaultCrypto.HMAC_SHA256);
            instance.init(new SecretKeySpec(A04, DefaultCrypto.HMAC_SHA256));
            bArr = instance.doFinal(bytes);
        } catch (Exception e) {
            Log.e("ChatMessageCounts/getThreadIDHash failed to compute hmac", e);
            bArr = null;
        }
        if (bArr != null) {
            return Base64.encodeToString(bArr, 2);
        }
        return null;
    }

    public static final void A01(SharedPreferences sharedPreferences, C40581rp r2, String str) {
        sharedPreferences.edit().putString(str, r2.toString()).apply();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ff, code lost:
        if (r7 == null) goto L_0x010c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0178, code lost:
        if (5 != r3.A05(android.net.Uri.parse(r1))) goto L_0x017b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x007d A[Catch: all -> 0x028d, TryCatch #4 {, blocks: (B:30:0x0065, B:32:0x006f, B:34:0x0075, B:36:0x007d, B:37:0x0096, B:42:0x009e, B:45:0x00a8, B:46:0x00b0, B:48:0x00b6, B:50:0x00ba, B:51:0x00c2, B:52:0x00c9, B:54:0x00cd, B:56:0x00d4, B:58:0x00e4, B:60:0x00e8, B:63:0x00ee), top: B:149:0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0096 A[Catch: all -> 0x028d, TryCatch #4 {, blocks: (B:30:0x0065, B:32:0x006f, B:34:0x0075, B:36:0x007d, B:37:0x0096, B:42:0x009e, B:45:0x00a8, B:46:0x00b0, B:48:0x00b6, B:50:0x00ba, B:51:0x00c2, B:52:0x00c9, B:54:0x00cd, B:56:0x00d4, B:58:0x00e4, B:60:0x00e8, B:63:0x00ee), top: B:149:0x0065 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A02(X.C20020v5 r10, X.AbstractC14640lm r11, X.AnonymousClass1IS r12, boolean r13, boolean r14, boolean r15, boolean r16) {
        /*
        // Method dump skipped, instructions count: 660
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20020v5.A02(X.0v5, X.0lm, X.1IS, boolean, boolean, boolean, boolean):void");
    }

    public synchronized long A03(Random random) {
        long j;
        SharedPreferences A04 = A04();
        j = A04.getLong("start_time_ms", 0);
        if (j == 0) {
            j = this.A08.A00() - ((long) random.nextInt(86400000));
            A04.edit().putLong("start_time_ms", j).apply();
        }
        return j;
    }

    public final SharedPreferences A04() {
        SharedPreferences sharedPreferences = this.A00;
        if (sharedPreferences != null) {
            return sharedPreferences;
        }
        SharedPreferences A01 = this.A0O.A01("chatCounts");
        this.A00 = A01;
        return A01;
    }

    public synchronized AbstractC27881Jp A05(Random random) {
        byte[] decode;
        SharedPreferences A04 = A04();
        if (!this.A00.contains("thread_user_secret")) {
            byte[] bArr = new byte[32];
            random.nextBytes(bArr);
            this.A00.edit().putString("thread_user_secret", Base64.encodeToString(bArr, 2)).apply();
        }
        decode = Base64.decode(A04.getString("thread_user_secret", ""), 2);
        return AbstractC27881Jp.A01(decode, 0, decode.length);
    }

    public String A06(String str) {
        return A00(A05(new Random()), str, new SimpleDateFormat("yyyy/MM/dd", Locale.US).format(new Date(A03(new Random()) - 28800000)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0742, code lost:
        if (r13 < 0) goto L_0x0744;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x07d1, code lost:
        if (r4.A0C == null) goto L_0x07d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x028e, code lost:
        if (r3 != null) goto L_0x0290;
     */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x036f A[Catch: all -> 0x094d, TryCatch #16 {, blocks: (B:4:0x0003, B:6:0x0023, B:7:0x0034, B:9:0x003a, B:11:0x0049, B:13:0x0052, B:15:0x0058, B:17:0x0061, B:18:0x0066, B:19:0x006a, B:20:0x006e, B:22:0x0074, B:24:0x0098, B:26:0x00e4, B:27:0x0100, B:29:0x0108, B:31:0x0110, B:32:0x011d, B:33:0x0121, B:35:0x0129, B:36:0x012d, B:37:0x0131, B:39:0x013f, B:41:0x015f, B:42:0x018e, B:44:0x0198, B:89:0x0369, B:91:0x036f, B:93:0x037c, B:94:0x0383, B:95:0x0391, B:96:0x0394, B:97:0x0396, B:99:0x03b6, B:101:0x03e0, B:103:0x03e6, B:104:0x03fe, B:106:0x0408, B:108:0x0428, B:110:0x043e, B:111:0x0446, B:113:0x0450, B:115:0x0463, B:117:0x0489, B:118:0x04c9, B:120:0x04d3, B:122:0x04f3, B:123:0x04ff, B:125:0x0509, B:127:0x0529, B:128:0x053c, B:130:0x0546, B:132:0x0566, B:133:0x0579, B:135:0x057f, B:137:0x05b4, B:139:0x05ba, B:141:0x05cc, B:142:0x05d0, B:144:0x05d6, B:146:0x05f8, B:147:0x0602, B:149:0x0608, B:151:0x061a, B:153:0x0620, B:157:0x0629, B:159:0x062f, B:161:0x063b, B:162:0x0644, B:165:0x0650, B:168:0x065a, B:169:0x0660, B:171:0x0668, B:173:0x066e, B:177:0x0685, B:178:0x068b, B:181:0x069a, B:186:0x06ae, B:187:0x06b3, B:188:0x06b7, B:189:0x06b9, B:191:0x06d0, B:192:0x06de, B:194:0x06e8, B:199:0x06f4, B:200:0x06fa, B:202:0x070a, B:210:0x073b, B:212:0x0744, B:214:0x074e, B:220:0x077a, B:223:0x0783, B:225:0x07aa, B:229:0x07b3, B:231:0x07ce, B:234:0x07d4, B:240:0x07e4, B:242:0x07ec, B:244:0x07f2, B:247:0x0808, B:249:0x0812, B:250:0x0819, B:251:0x081f, B:265:0x08a0, B:267:0x08a8, B:268:0x08ae, B:269:0x08eb, B:271:0x08f1, B:273:0x0903, B:275:0x090b, B:277:0x091c, B:280:0x092c, B:46:0x019e, B:48:0x01b0, B:50:0x01bc, B:51:0x01d2, B:53:0x0213, B:55:0x0219, B:67:0x0293, B:68:0x0296, B:69:0x0298, B:71:0x029e, B:72:0x02b4, B:74:0x02ba, B:75:0x02c0, B:82:0x034d, B:86:0x035d, B:87:0x0362, B:56:0x0261, B:66:0x0290, B:256:0x086f, B:76:0x0311, B:81:0x034a, B:260:0x087b, B:203:0x0710, B:209:0x0738, B:204:0x0722, B:206:0x0728, B:208:0x0734, B:215:0x0754, B:219:0x0777, B:264:0x0895, B:224:0x0789), top: B:298:0x0003 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A07() {
        /*
        // Method dump skipped, instructions count: 2384
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20020v5.A07():void");
    }

    public final void A08(AnonymousClass024 r4, UserJid userJid) {
        C15370n3 A0A;
        if (this.A0M.A07(1034) && !this.A01.A0F(userJid) && (A0A = this.A03.A0A(userJid)) != null && A0A.A0J()) {
            this.A0R.execute(new RunnableBRunnable0Shape0S0300000_I0(this, userJid, r4, 26));
        }
    }

    public void A09(Jid jid, int i) {
        if (this.A0M.A07(1022) && !this.A01.A0F(jid)) {
            this.A0R.execute(new RunnableBRunnable0Shape0S0201000_I0(this, jid, i, 11));
        }
    }

    public void A0A(AnonymousClass1YT r5, Integer num) {
        Object obj;
        C14850m9 r2 = this.A0M;
        if (r2.A07(1251) && r5 != null && num != null) {
            if (!r5.A0B()) {
                obj = r5.A03().A01;
            } else if (r2.A07(1251)) {
                obj = r5.A04;
            } else {
                return;
            }
            if (obj != null) {
                this.A0R.execute(new RunnableBRunnable0Shape0S0300000_I0(this, obj, num, 25));
            }
        }
    }
}
