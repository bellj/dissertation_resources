package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/* renamed from: X.1J1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1J1 {
    public AnonymousClass28E A00;
    public final float A01;
    public final float A02;
    public final int A03;
    public final C51482Va A04 = new C51482Va();
    public final String A05;
    public final boolean A06;
    public final /* synthetic */ C21270x9 A07;

    public /* synthetic */ AnonymousClass1J1(C21270x9 r3, String str, float f, int i, boolean z) {
        this.A07 = r3;
        this.A03 = i;
        this.A02 = f;
        this.A01 = -2.14748365E9f;
        this.A05 = str;
        this.A06 = z;
    }

    public void A00() {
        AnonymousClass28E r1 = this.A00;
        if (r1 != null) {
            r1.A05 = true;
            r1.interrupt();
            this.A00 = null;
        }
    }

    public final void A01(ImageView imageView, AnonymousClass28F r12, C15370n3 r13, float f, int i, boolean z) {
        String A04;
        if (z) {
            C21270x9 r2 = this.A07;
            if (r2.A01.A0F(r13.A0D)) {
                A04 = imageView.getContext().getString(R.string.you);
            } else {
                A04 = r2.A04.A04(r13);
            }
            imageView.setContentDescription(A04);
        }
        String A0E = r13.A0E(f, i);
        if (A0E == null) {
            r12.Adv(imageView);
            return;
        }
        boolean equals = A0E.equals(imageView.getTag());
        imageView.setTag(A0E);
        Bitmap bitmap = (Bitmap) this.A07.A05.A02.A01().A00(A0E);
        if (bitmap != null) {
            r12.Adh(bitmap, imageView, true);
            return;
        }
        if (!equals || !r13.A0X) {
            r12.Adv(imageView);
        }
        if (r13.A0X) {
            A03(imageView, r12, r13, A0E, f, i);
        }
    }

    public void A02(ImageView imageView, AnonymousClass28F r12, C15370n3 r13, boolean z) {
        float f = this.A02;
        C21270x9 r0 = this.A07;
        if (r0.A09.A0W(r0.A07.A02((GroupJid) r13.A0B(GroupJid.class)))) {
            f = this.A01;
        }
        A01(imageView, r12, r13, f, this.A03, z);
    }

    public final void A03(ImageView imageView, AnonymousClass28F r15, Object obj, Object obj2, float f, int i) {
        C51482Va r9 = this.A04;
        Stack stack = r9.A00;
        synchronized (stack) {
            int i2 = 0;
            while (i2 < stack.size()) {
                if (((C51512Vd) stack.get(i2)).A04 == imageView) {
                    stack.remove(i2);
                } else {
                    i2++;
                }
            }
        }
        C51512Vd r2 = new C51512Vd(imageView, r15, obj, obj2, f, i);
        synchronized (stack) {
            stack.add(0, r2);
            stack.notifyAll();
            AnonymousClass28E r22 = this.A00;
            if (r22 == null || (this.A06 && r22.A05)) {
                String str = this.A05;
                C21270x9 r0 = this.A07;
                AnonymousClass28E r6 = new AnonymousClass28E(r0.A00, r0.A03, r9, r0.A06, str, this.A06);
                this.A00 = r6;
                r6.start();
            }
        }
    }

    public void A04(ImageView imageView, AnonymousClass28F r13, C30721Yo r14, float f, int i) {
        int length;
        imageView.setContentDescription(r14.A08());
        ArrayList arrayList = new ArrayList();
        List<C30741Yq> list = r14.A05;
        if (list != null) {
            for (C30741Yq r0 : list) {
                UserJid userJid = r0.A01;
                if (userJid != null) {
                    arrayList.add(userJid);
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            AbstractC14640lm r2 = (AbstractC14640lm) it.next();
            if (C15380n4.A0L(r2)) {
                C21270x9 r1 = this.A07;
                C15370n3 A0A = r1.A03.A0A(r2);
                if (A0A != null) {
                    A01(imageView, new C51502Vc(r1.A02, null, r1.A08, r1.A09), A0A, f, i, true);
                    return;
                }
            }
        }
        byte[] bArr = r14.A09;
        if (bArr == null || (length = bArr.length) <= 0) {
            r13.Adv(imageView);
        } else {
            r13.Adh(BitmapFactory.decodeByteArray(bArr, 0, length), imageView, true);
        }
    }

    public void A05(ImageView imageView, C51492Vb r15) {
        imageView.setContentDescription(r15.A06);
        String obj = Long.valueOf(r15.A04).toString();
        imageView.setTag(obj);
        Bitmap bitmap = r15.A00;
        if (bitmap != null) {
            C21270x9 r0 = this.A07;
            new C51502Vc(r0.A02, null, r0.A08, r0.A09).Adh(bitmap, imageView, true);
            return;
        }
        C15370n3 r02 = r15.A01;
        if (r02 != null) {
            A06(imageView, r02);
            return;
        }
        C21270x9 r03 = this.A07;
        A03(imageView, new C51502Vc(r03.A02, null, r03.A08, r03.A09), r15, obj, this.A02, this.A03);
    }

    public void A06(ImageView imageView, C15370n3 r3) {
        if (imageView != null) {
            A07(imageView, r3, true);
        }
    }

    public void A07(ImageView imageView, C15370n3 r6, boolean z) {
        C21270x9 r0 = this.A07;
        A02(imageView, new C51502Vc(r0.A02, r6, r0.A08, r0.A09), r6, z);
    }

    public void A08(ImageView imageView, C30721Yo r12) {
        C21270x9 r0 = this.A07;
        A04(imageView, new C51502Vc(r0.A02, null, r0.A08, r0.A09), r12, this.A02, this.A03);
    }
}
