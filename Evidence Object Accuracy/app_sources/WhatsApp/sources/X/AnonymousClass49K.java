package X;

import java.lang.reflect.Method;

/* renamed from: X.49K  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass49K extends ClassLoader {
    public static final String A00 = AbstractC95364da.class.getName();
    public static final Class[] A01;

    static {
        Class cls = Integer.TYPE;
        A01 = new Class[]{String.class, byte[].class, cls, cls};
    }

    public AnonymousClass49K(ClassLoader classLoader) {
        super(classLoader);
    }

    public Class A00(String str, byte[] bArr) {
        try {
            Method declaredMethod = ClassLoader.class.getDeclaredMethod("defineClass", A01);
            declaredMethod.setAccessible(true);
            ClassLoader parent = getParent();
            Object[] objArr = new Object[4];
            objArr[0] = str;
            objArr[1] = bArr;
            C12960it.A1P(objArr, 0, 2);
            C12960it.A1P(objArr, bArr.length, 3);
            return (Class) declaredMethod.invoke(parent, objArr);
        } catch (Exception unused) {
            return defineClass(str, bArr, 0, bArr.length);
        }
    }

    @Override // java.lang.ClassLoader
    public synchronized Class loadClass(String str, boolean z) {
        Class<?> cls;
        if (str.equals(A00)) {
            cls = AbstractC95364da.class;
        } else {
            cls = super.loadClass(str, z);
        }
        return cls;
    }
}
