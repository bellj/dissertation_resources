package X;

/* renamed from: X.5X4  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5X4 {
    Object fold(Object obj, AnonymousClass5ZQ v);

    AnonymousClass5ZU get(AbstractC115495Rt v);

    AnonymousClass5X4 minusKey(AbstractC115495Rt v);

    AnonymousClass5X4 plus(AnonymousClass5X4 v);
}
