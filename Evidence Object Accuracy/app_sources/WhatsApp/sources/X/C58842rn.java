package X;

import com.whatsapp.backup.google.GoogleBackupService;

/* renamed from: X.2rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58842rn extends AbstractC84013yG {
    public final /* synthetic */ GoogleBackupService A00;
    public final /* synthetic */ C65113Ie A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;

    public C58842rn(GoogleBackupService googleBackupService, C65113Ie r2, String str, String str2) {
        this.A00 = googleBackupService;
        this.A01 = r2;
        this.A02 = str;
        this.A03 = str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001a  */
    @Override // X.AbstractC84013yG, X.AbstractC84023yH, X.AnonymousClass4UU
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A00(int r15) {
        /*
        // Method dump skipped, instructions count: 350
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C58842rn.A00(int):java.lang.Object");
    }
}
