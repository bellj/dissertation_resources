package X;

import com.whatsapp.stickers.StickerStorePackPreviewActivity;

/* renamed from: X.58k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1111858k implements AbstractC17410ql {
    public final /* synthetic */ StickerStorePackPreviewActivity A00;

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
    }

    public C1111858k(StickerStorePackPreviewActivity stickerStorePackPreviewActivity) {
        this.A00 = stickerStorePackPreviewActivity;
    }

    @Override // X.AbstractC17410ql
    public void AMj() {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = this.A00;
        if (stickerStorePackPreviewActivity.A2f()) {
            stickerStorePackPreviewActivity.finish();
        }
    }

    @Override // X.AbstractC17410ql
    public void AMk() {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = this.A00;
        if (stickerStorePackPreviewActivity.A0V) {
            stickerStorePackPreviewActivity.finish();
        }
    }
}
