package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1JG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JG implements AbstractC21730xt {
    public AbstractC18870tC A00;
    public boolean A01 = false;
    public boolean A02 = true;
    public final C14900mE A03;
    public final C16240og A04;
    public final AnonymousClass1JC A05;
    public final C17220qS A06;
    public final AbstractC14440lR A07;

    public AnonymousClass1JG(C14900mE r2, C16240og r3, AnonymousClass1JC r4, C17220qS r5, AbstractC14440lR r6) {
        this.A03 = r2;
        this.A07 = r6;
        this.A06 = r5;
        this.A04 = r3;
        this.A05 = r4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        if (!this.A01) {
            this.A03.A0I(new RunnableBRunnable0Shape9S0100000_I0_9(this.A05, 42));
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        if (!this.A01) {
            Log.e("GetCTWAContextIQ/response-error");
            AnonymousClass1V8 A0E = r5.A0E("error");
            if (A0E != null) {
                this.A03.A0I(new RunnableBRunnable0Shape0S0101000_I0(this, A0E.A05("code", 0), 16));
            }
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r15, String str) {
        String A0G;
        String A0G2;
        String A0G3;
        String A0G4;
        String A0G5;
        String str2;
        String A0G6;
        String str3;
        String A0G7;
        if (!this.A01) {
            AnonymousClass1V8 A0E = r15.A0E("context");
            if (A0E != null) {
                AnonymousClass1V8 A0E2 = A0E.A0E("headline");
                if (A0E2 == null) {
                    A0G = null;
                } else {
                    A0G = A0E2.A0G();
                }
                AnonymousClass1V8 A0E3 = A0E.A0E("body");
                if (A0E3 == null) {
                    A0G2 = null;
                } else {
                    A0G2 = A0E3.A0G();
                }
                AnonymousClass1V8 A0E4 = A0E.A0E("source");
                if (A0E4 != null) {
                    AnonymousClass1V8 A0E5 = A0E4.A0E("id");
                    if (A0E5 == null) {
                        A0G3 = null;
                    } else {
                        A0G3 = A0E5.A0G();
                    }
                    AnonymousClass1V8 A0E6 = A0E4.A0E("type");
                    if (A0E6 == null) {
                        A0G4 = null;
                    } else {
                        A0G4 = A0E6.A0G();
                    }
                    AnonymousClass1V8 A0E7 = A0E4.A0E("url");
                    if (A0E7 == null) {
                        A0G5 = null;
                    } else {
                        A0G5 = A0E7.A0G();
                    }
                    if (!TextUtils.isEmpty(A0G3) && !TextUtils.isEmpty(A0G4) && !TextUtils.isEmpty(A0G5)) {
                        AnonymousClass4QY r7 = new AnonymousClass4QY(A0G3, A0G4, A0G5);
                        AnonymousClass1V8 A0E8 = A0E.A0E("thumbnail");
                        AnonymousClass4OC r8 = null;
                        byte[] bArr = null;
                        r8 = null;
                        if (A0E8 != null) {
                            AnonymousClass1V8 A0E9 = A0E8.A0E("url");
                            if (A0E9 == null) {
                                A0G7 = null;
                            } else {
                                A0G7 = A0E9.A0G();
                            }
                            if (!TextUtils.isEmpty(A0G7)) {
                                AnonymousClass1V8 A0E10 = A0E8.A0E("bytes");
                                if (A0E10 != null) {
                                    bArr = A0E10.A01;
                                }
                                r8 = new AnonymousClass4OC(A0G7, bArr);
                            }
                        }
                        AnonymousClass1V8 A0E11 = A0E.A0E("welcome_message");
                        if (A0E11 != null) {
                            str2 = A0E11.A0G();
                        } else {
                            str2 = null;
                        }
                        List<AnonymousClass1V8> A0J = A0E.A0J("icebreaker");
                        ArrayList arrayList = new ArrayList();
                        if (A0J != null) {
                            for (AnonymousClass1V8 r5 : A0J) {
                                AnonymousClass1V8 A0E12 = r5.A0E("question");
                                if (!(A0E12 == null || A0E12.A0G() == null)) {
                                    String A0G8 = A0E12.A0G();
                                    AnonymousClass1V8 A0E13 = r5.A0E("response");
                                    if (A0E13 == null || A0E13.A0G() == null) {
                                        str3 = null;
                                    } else {
                                        str3 = A0E13.A0G();
                                    }
                                    arrayList.add(new C92664Wv(A0G8, str3));
                                }
                            }
                        }
                        AnonymousClass1V8 A0E14 = A0E.A0E("video");
                        C89704Kz r9 = null;
                        if (A0E14 != null) {
                            AnonymousClass1V8 A0E15 = A0E14.A0E("url");
                            if (A0E15 == null) {
                                A0G6 = null;
                            } else {
                                A0G6 = A0E15.A0G();
                            }
                            if (!TextUtils.isEmpty(A0G6)) {
                                r9 = new C89704Kz(A0G6);
                            }
                        }
                        this.A03.A0I(new RunnableBRunnable0Shape7S0200000_I0_7(this, 8, new AnonymousClass1JF(r7, r8, r9, A0G, A0G2, str2, arrayList)));
                        return;
                    }
                }
            }
            StringBuilder sb = new StringBuilder("GetCTWAContextIQ/onSuccess corrupted-response context iq=");
            sb.append(str);
            Log.e(sb.toString());
            this.A03.A0I(new RunnableBRunnable0Shape9S0100000_I0_9(this, 43));
        }
    }
}
