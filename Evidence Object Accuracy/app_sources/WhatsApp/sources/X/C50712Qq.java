package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2Qq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50712Qq extends AnimatorListenerAdapter {
    public final /* synthetic */ C50632Qh A00;
    public final /* synthetic */ boolean A01;

    public C50712Qq(C50632Qh r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C50632Qh r1 = this.A00;
        r1.A05 = 0;
        r1.A07 = null;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        C50632Qh r3 = this.A00;
        r3.A0N.A00(0, this.A01);
        r3.A05 = 2;
        r3.A07 = animator;
    }
}
