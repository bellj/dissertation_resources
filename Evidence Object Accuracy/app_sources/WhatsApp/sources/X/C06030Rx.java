package X;

import android.util.SparseBooleanArray;
import java.util.List;
import java.util.Map;

/* renamed from: X.0Rx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06030Rx {
    public static final AbstractC11320g6 A05 = new AnonymousClass0Yv();
    public final SparseBooleanArray A00 = new SparseBooleanArray();
    public final AnonymousClass0QA A01;
    public final List A02;
    public final List A03;
    public final Map A04 = new AnonymousClass00N();

    public C06030Rx(List list, List list2) {
        this.A02 = list;
        this.A03 = list2;
        List list3 = this.A02;
        int size = list3.size();
        int i = Integer.MIN_VALUE;
        AnonymousClass0QA r3 = null;
        for (int i2 = 0; i2 < size; i2++) {
            AnonymousClass0QA r1 = (AnonymousClass0QA) list3.get(i2);
            if (r1.A06 > i) {
                i = r1.A06;
                r3 = r1;
            }
        }
        this.A01 = r3;
    }
}
