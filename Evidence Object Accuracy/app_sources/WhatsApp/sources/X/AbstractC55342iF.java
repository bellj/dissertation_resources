package X;

import android.content.Context;
import android.content.res.Resources;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape1S0201000_I1;
import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.WallpaperPreview;
import com.whatsapp.settings.chat.wallpaper.downloadable.picker.DownloadableWallpaperPreviewActivity;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.2iF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC55342iF extends AnonymousClass01A {
    public final Context A00;
    public final Resources A01;

    public AbstractC55342iF(Context context, Resources resources) {
        this.A00 = context;
        this.A01 = resources;
    }

    @Override // X.AnonymousClass01A
    public Object A05(ViewGroup viewGroup, int i) {
        Context context;
        int i2;
        int i3;
        Pair create;
        String string;
        String string2;
        int i4;
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        boolean z = this instanceof AnonymousClass35K;
        if (!z) {
            if (!(this instanceof AnonymousClass35I)) {
                SolidColorWallpaperPreview solidColorWallpaperPreview = ((AnonymousClass35J) this).A02;
                if (i == 0) {
                    i4 = R.string.wallpaper_preview_chat_content_swipe_left;
                } else {
                    int length = solidColorWallpaperPreview.A0D.length - 1;
                    i4 = R.string.wallpaper_preview_chat_content_swipe_right;
                    if (i < length) {
                        i4 = R.string.wallpaper_color_preview_swipe_left_or_right;
                    }
                }
                string = solidColorWallpaperPreview.getString(i4);
                string2 = solidColorWallpaperPreview.A2f();
            } else {
                AnonymousClass35I r6 = (AnonymousClass35I) this;
                WallpaperPreview wallpaperPreview = r6.A00;
                int A01 = r6.A01() - 1;
                int i5 = R.string.library_preview_chat_content_swipe_left;
                if (i == A01) {
                    i5 = R.string.library_preview_chat_content_swipe_right;
                }
                string = wallpaperPreview.getString(i5);
                int i6 = R.string.library_preview_chat_content_swipe_right;
                if (i == 0) {
                    i6 = R.string.library_preview_chat_content_swipe_left;
                }
                string2 = wallpaperPreview.getString(i6);
            }
            create = Pair.create(string, string2);
        } else {
            AnonymousClass35K r62 = (AnonymousClass35K) this;
            if (i == 0) {
                context = ((AbstractC55342iF) r62).A00;
                i2 = R.string.library_preview_chat_content_swipe_left;
            } else {
                int A012 = r62.A01() - 1;
                context = ((AbstractC55342iF) r62).A00;
                i2 = R.string.library_preview_chat_content_swipe_right;
                if (i < A012) {
                    i2 = R.string.wallpaper_preview_swipe_left_or_right;
                }
            }
            String string3 = context.getString(i2);
            if (r62.A00 == null) {
                boolean A08 = C41691tw.A08(context);
                i3 = R.string.wallpaper_set_light_wallpaper_bubble_message;
                if (A08) {
                    i3 = R.string.wallpaper_set_dark_wallpaper_bubble_message;
                }
            } else {
                boolean z2 = r62.A08;
                i3 = R.string.wallpaper_set_with_custom_wallpaper_bubble_message;
                if (z2) {
                    i3 = R.string.wallpaper_set_without_custom_wallpaper_bubble_message;
                }
            }
            create = Pair.create(string3, context.getString(i3));
        }
        AnonymousClass35E r0 = new AnonymousClass35E(this.A00, this.A01, (String) create.first, (String) create.second);
        r0.setLayoutParams(layoutParams);
        viewGroup.addView(r0);
        if (z) {
            AnonymousClass35K r3 = (AnonymousClass35K) this;
            r0.setDownloadClickListener(new ViewOnClickCListenerShape1S0201000_I1(r3, r0, i, 5));
            List list = r3.A04;
            if (i < list.size()) {
                r3.A0F(r0, i);
                return r0;
            }
            int size = i - list.size();
            AnonymousClass38V r5 = new AnonymousClass38V(r0.getContext(), r0.A00, r0.A06, r0.A04, C12960it.A05(r3.A06.get(size)), C12960it.A05(r3.A05.get(size)));
            r0.A02.setVisibility(8);
            DownloadableWallpaperPreviewActivity downloadableWallpaperPreviewActivity = r3.A02.A01;
            Set set = downloadableWallpaperPreviewActivity.A08;
            Integer valueOf = Integer.valueOf(i);
            set.add(valueOf);
            if (downloadableWallpaperPreviewActivity.A01.getCurrentItem() == i) {
                ((AnonymousClass35C) downloadableWallpaperPreviewActivity).A00.setEnabled(true);
            }
            C12980iv.A1M((AbstractC16350or) r3.A07.put(valueOf, r5));
            C12960it.A1E(r5, r3.A03);
            return r0;
        } else if (!(this instanceof AnonymousClass35I)) {
            AnonymousClass35J r32 = (AnonymousClass35J) this;
            r0.setBackgroundColor(r32.A02.A0D[i]);
            if (r32.A00) {
                Context context2 = r0.getContext();
                r0.A05.setImageDrawable(AnonymousClass2GE.A04(C12970iu.A0C(context2, R.drawable.whatsapp_doodle), context2.getResources().getIntArray(R.array.wallpaper_doodle_tint_colors)[i]));
            } else {
                r0.A05.setImageDrawable(null);
            }
            Map map = r32.A01;
            Integer valueOf2 = Integer.valueOf(i);
            map.put(valueOf2, Boolean.valueOf(r32.A00));
            r0.setTag(valueOf2);
            return r0;
        } else {
            WallpaperPreview wallpaperPreview2 = ((AnonymousClass35I) this).A00;
            AnonymousClass38V r4 = new AnonymousClass38V(r0.getContext(), r0.A00, r0.A06, r0.A04, C12960it.A05(wallpaperPreview2.A0B.get(i)), C12960it.A05(wallpaperPreview2.A0A.get(i)));
            C12980iv.A1M((AbstractC16350or) wallpaperPreview2.A0E.put(Integer.valueOf(i), r4));
            C12960it.A1E(r4, ((ActivityC13830kP) wallpaperPreview2).A05);
            return r0;
        }
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        viewGroup.removeView((View) obj);
    }
}
