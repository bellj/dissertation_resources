package X;

import android.graphics.PointF;
import android.util.Property;

/* renamed from: X.0Ai  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C02180Ai extends Property {
    public C02180Ai() {
        super(PointF.class, "bottomRight");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return null;
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        AnonymousClass0O3 r7 = (AnonymousClass0O3) obj;
        PointF pointF = (PointF) obj2;
        int round = Math.round(pointF.x);
        r7.A03 = round;
        int round2 = Math.round(pointF.y);
        r7.A00 = round2;
        int i = r7.A01 + 1;
        r7.A01 = i;
        if (r7.A05 == i) {
            AnonymousClass0U3.A04.A06(r7.A06, r7.A02, r7.A04, round, round2);
            r7.A05 = 0;
            r7.A01 = 0;
        }
    }
}
