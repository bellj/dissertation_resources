package X;

import android.os.SystemClock;

/* renamed from: X.006  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass006 {
    public static final AnonymousClass006 A03 = new AnonymousClass006();
    public long A00 = SystemClock.elapsedRealtime();
    public long A01 = SystemClock.uptimeMillis();
    public long A02 = System.nanoTime();
}
