package X;

import android.app.Activity;
import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.2jC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class RunnableC55722jC extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ C90904Pr A01;
    public final /* synthetic */ AnonymousClass1WA A02;
    public final /* synthetic */ C25751Ap A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ boolean A06;

    public /* synthetic */ RunnableC55722jC(Activity activity, C90904Pr r2, AnonymousClass1WA r3, C25751Ap r4, String str, String str2, boolean z) {
        this.A06 = z;
        this.A00 = activity;
        this.A01 = r2;
        this.A04 = str;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = str2;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:36:? */
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r5v3, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v5, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v7, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r6v3, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v4, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v5, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v6, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r5v8, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v9, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v10, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v11, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v12, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r5v13, resolved type: com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel */
    /* JADX DEBUG: Multi-variable search result rejected for r6v8, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v9, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v10, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v11, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v12, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r6v13, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v2 */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006f, code lost:
        if (r1.contains(2498058) != true) goto L_0x0071;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r9 = this;
            boolean r5 = r9.A06
            android.app.Activity r1 = r9.A00
            X.4Pr r2 = r9.A01
            java.lang.String r4 = r9.A04
            X.1WA r7 = r9.A02
            X.1Ap r6 = r9.A03
            java.lang.String r8 = r9.A05
            r3 = 1
            X.C16700pc.A0E(r1, r3)
            r0 = 4
            X.C16700pc.A0E(r7, r0)
            if (r5 == 0) goto L_0x0098
            X.00k r1 = (X.ActivityC000900k) r1
            X.01F r1 = r1.A0V()
            java.lang.String r0 = "fds_bottom_sheet_container"
            X.01E r0 = r1.A0A(r0)
            if (r0 == 0) goto L_0x00ac
            X.01F r1 = r0.A0E()
            java.lang.String r0 = "BK_FRAGMENT"
            X.01E r0 = r1.A0A(r0)
            if (r0 == 0) goto L_0x00ac
            X.02A r1 = X.C13000ix.A02(r0)
            java.lang.Class<com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel> r0 = com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel.class
            X.015 r5 = r1.A00(r0)
            X.C16700pc.A0B(r5)
            com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel r5 = (com.whatsapp.wabloks.commerce.ui.viewmodel.WaBkGalaxyLayoutViewModel) r5
            java.lang.String r7 = r7.A07
            r0 = 0
            X.C16700pc.A0E(r7, r0)
            if (r8 != 0) goto L_0x0092
            X.0sm r0 = r5.A00
            boolean r0 = r0.A0B()
            if (r0 != 0) goto L_0x0055
            r6 = 2131888214(0x7f120856, float:1.9411057E38)
            goto L_0x0074
        L_0x0055:
            if (r6 == 0) goto L_0x0071
            java.util.Map r0 = r6.A00
            if (r0 == 0) goto L_0x0071
            java.util.Set r1 = r0.keySet()
            if (r1 == 0) goto L_0x0071
            r0 = 2498058(0x261e0a, float:3.500525E-39)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.contains(r0)
            r6 = 2131888216(0x7f120858, float:1.9411061E38)
            if (r0 == r3) goto L_0x0074
        L_0x0071:
            r6 = 2131888215(0x7f120857, float:1.941106E38)
        L_0x0074:
            org.json.JSONObject r1 = X.C13000ix.A05(r7)     // Catch: JSONException -> 0x00a2
            java.lang.String r0 = "action"
            java.lang.String r1 = r1.optString(r0)     // Catch: JSONException -> 0x00a2
            java.lang.String r0 = "init"
            boolean r0 = X.C16700pc.A0O(r1, r0)     // Catch: JSONException -> 0x00a2
            if (r0 == 0) goto L_0x008c
            X.1It r0 = r5.A02     // Catch: JSONException -> 0x00a2
            X.C12960it.A1A(r0, r6)     // Catch: JSONException -> 0x00a2
            goto L_0x00ac
        L_0x008c:
            X.1It r0 = r5.A03     // Catch: JSONException -> 0x00a2
            X.C12960it.A1A(r0, r6)     // Catch: JSONException -> 0x00a2
            goto L_0x00ac
        L_0x0092:
            X.1It r0 = r5.A01
            r0.A0B(r8)
            goto L_0x00ac
        L_0x0098:
            if (r2 == 0) goto L_0x00d6
            java.util.Map r5 = X.C65053Hy.A01(r4)
            java.lang.String r4 = "success"
            goto L_0x00b4
        L_0x00a2:
            java.lang.String r0 = "WaBkGalaxyLayoutViewModel/delegateBloksDataError/invalid json structure of data"
            com.whatsapp.util.Log.w(r0)
            X.1It r0 = r5.A02
            X.C12960it.A1A(r0, r6)
        L_0x00ac:
            if (r2 == 0) goto L_0x00d6
            java.util.Map r5 = X.C65053Hy.A01(r4)
            java.lang.String r4 = "failure"
        L_0x00b4:
            X.0l3 r0 = r2.A01
            java.util.List r0 = r0.A00
            java.lang.Object r1 = r0.get(r3)
            boolean r0 = r1 instanceof X.C94724cR
            if (r0 == 0) goto L_0x00d6
            X.0l4 r0 = r2.A00
            X.0l6 r2 = new X.0l6
            r2.<init>(r0)
            X.4cR r1 = (X.C94724cR) r1
            X.51l r1 = r1.A00
            X.0l2 r0 = X.C14210l2.A00(r4)
            X.0l3 r0 = X.C14210l2.A01(r0, r5, r3)
            r2.A01(r0, r1)
        L_0x00d6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC55722jC.run():void");
    }
}
