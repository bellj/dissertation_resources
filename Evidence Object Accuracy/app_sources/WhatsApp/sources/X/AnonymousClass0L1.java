package X;

import android.os.Build;
import android.view.animation.Interpolator;

/* renamed from: X.0L1  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0L1 {
    public static Interpolator A00(float f, float f2, float f3, float f4) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0L0.A00(f, f2, f3, f4);
        }
        return new animation.InterpolatorC07070Wo(f, f2, f3, f4);
    }
}
