package X;

import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;

/* renamed from: X.32m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617832m extends AbstractC628338t {
    public final C14900mE A00;
    public final WeakReference A01;

    public C617832m(C14900mE r2, GroupChatInfo groupChatInfo, C15580nU r4, UserJid userJid, C20660w7 r6) {
        super(r4, userJid, r6);
        this.A00 = r2;
        this.A01 = C12970iu.A10(groupChatInfo);
    }
}
