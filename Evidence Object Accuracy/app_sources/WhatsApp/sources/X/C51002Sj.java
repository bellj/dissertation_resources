package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.WaMediaThumbnailView;

/* renamed from: X.2Sj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51002Sj {
    public final Bitmap A00;
    public final View A01;
    public final AbstractC35611iN A02;
    public final String A03;

    public C51002Sj() {
        this(null, null, null, null, null, 15);
    }

    public /* synthetic */ C51002Sj(Bitmap bitmap, View view, AbstractC35611iN r4, String str, C51012Sk r6, int i) {
        this.A03 = null;
        this.A01 = null;
        this.A00 = null;
        this.A02 = null;
    }

    public C51002Sj(WaMediaThumbnailView waMediaThumbnailView, String str) {
        C16700pc.A0E(waMediaThumbnailView, 1);
        Bitmap bitmap = waMediaThumbnailView.A00;
        AbstractC35611iN r0 = waMediaThumbnailView.A01;
        this.A03 = str;
        this.A01 = waMediaThumbnailView;
        this.A00 = bitmap;
        this.A02 = r0;
    }
}
