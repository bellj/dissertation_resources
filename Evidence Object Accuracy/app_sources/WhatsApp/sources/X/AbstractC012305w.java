package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: X.05w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC012305w {
    Drawable A8J(Context context, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser);
}
