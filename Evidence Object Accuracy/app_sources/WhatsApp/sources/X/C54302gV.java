package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.List;

/* renamed from: X.2gV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54302gV extends AnonymousClass02M {
    public final C48952Io A00;
    public final List A01 = C12960it.A0l();

    public C54302gV(C48952Io r2) {
        this.A00 = r2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC37191le r22 = (AbstractC37191le) r2;
        r22.A08();
        r22.A09(this.A01.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        switch (AnonymousClass39o.values()[i].ordinal()) {
            case 7:
                C48952Io r2 = this.A00;
                View A0F = C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.security_footer_row);
                AnonymousClass01J r1 = r2.A00.A03;
                return new C59882vX(A0F, (AnonymousClass1B3) r1.AI0.get(), (AnonymousClass1B8) r1.A2C.get());
            case 8:
                C53162d0 r3 = new C53162d0(viewGroup.getContext());
                r3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                r3.setFAQLink("how-to-search-for-businesses-inside-whatsapp");
                return new C59962vf(r3);
            case 23:
                return new AnonymousClass40Y(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.neighbourhoods_title_row));
            case 24:
                return new C59772vM(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.neighbourhood_row));
            case 36:
                return new AnonymousClass40T(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.location_search_no_result_row));
            default:
                Log.e(C12960it.A0W(i, "DirectorySetLocationListAdaptor/onCreateViewHolder type not handled: "));
                throw C12960it.A0U(C12960it.A0f(C12960it.A0j("DirectorySetLocationListAdaptor/onCreateViewHolder type not handled: "), i));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((C37171lc) this.A01.get(i)).A00.ordinal();
    }
}
