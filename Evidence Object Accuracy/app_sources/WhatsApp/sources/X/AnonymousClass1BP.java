package X;

import android.app.Activity;
import android.widget.ImageButton;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.WaEditText;

/* renamed from: X.1BP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1BP {
    public Activity A00;
    public ImageButton A01;
    public KeyboardPopupLayout A02;
    public WaEditText A03;
    public AnonymousClass240 A04 = null;
    public AnonymousClass4TX A05;
    public AnonymousClass3XR A06 = null;
    public AnonymousClass2B7 A07 = null;
    public final AbstractC15710nm A08;
    public final C15450nH A09;
    public final AnonymousClass01d A0A;
    public final C14820m6 A0B;
    public final AnonymousClass018 A0C;
    public final C14850m9 A0D;
    public final C16630pM A0E;
    public final AnonymousClass12V A0F;
    public final C18170s1 A0G;
    public final C20980wd A0H;
    public final C235812f A0I;
    public final C252718t A0J;

    public AnonymousClass1BP(AbstractC15710nm r2, C15450nH r3, AnonymousClass01d r4, C14820m6 r5, AnonymousClass018 r6, C14850m9 r7, C16630pM r8, AnonymousClass12V r9, C18170s1 r10, C20980wd r11, C235812f r12, C252718t r13) {
        this.A0D = r7;
        this.A0J = r13;
        this.A08 = r2;
        this.A09 = r3;
        this.A0I = r12;
        this.A0A = r4;
        this.A0C = r6;
        this.A0G = r10;
        this.A0F = r9;
        this.A0B = r5;
        this.A0E = r8;
        this.A0H = r11;
    }

    public C15260mp A00() {
        Activity activity = this.A00;
        AnonymousClass009.A05(activity);
        C14850m9 r1 = this.A0D;
        C252718t r12 = this.A0J;
        AbstractC15710nm r15 = this.A08;
        C15450nH r14 = this.A09;
        C235812f r13 = this.A0I;
        AnonymousClass01d r122 = this.A0A;
        AnonymousClass018 r11 = this.A0C;
        AnonymousClass12V r10 = this.A0F;
        C14820m6 r9 = this.A0B;
        C16630pM r8 = this.A0E;
        C20980wd r7 = this.A0H;
        KeyboardPopupLayout keyboardPopupLayout = this.A02;
        AnonymousClass009.A03(keyboardPopupLayout);
        WaEditText waEditText = this.A03;
        AnonymousClass009.A03(waEditText);
        ImageButton imageButton = this.A01;
        AnonymousClass2B7 r3 = this.A07;
        AnonymousClass3XR r2 = this.A06;
        return new C15260mp(activity, imageButton, r15, keyboardPopupLayout, r14, waEditText, r122, r9, r11, this.A04, this.A05, r2, r3, r1, r8, r10, r7, r13, r12);
    }
}
