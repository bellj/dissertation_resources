package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import com.whatsapp.R;

/* renamed from: X.0BS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BS extends Button implements AnonymousClass012, AnonymousClass02c, AbstractC004702d {
    public final AnonymousClass085 A00;
    public final AnonymousClass086 A01;

    public AnonymousClass0BS(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.buttonStyle);
    }

    public AnonymousClass0BS(Context context, AttributeSet attributeSet, int i) {
        super(AnonymousClass083.A00(context), attributeSet, i);
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass085 r0 = new AnonymousClass085(this);
        this.A00 = r0;
        r0.A05(attributeSet, i);
        AnonymousClass086 r02 = new AnonymousClass086(this);
        this.A01 = r02;
        r02.A0A(attributeSet, i);
        r02.A02();
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass086 r02 = this.A01;
        if (r02 != null) {
            r02.A02();
        }
    }

    @Override // android.widget.TextView
    public int getAutoSizeMaxTextSize() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeMaxTextSize();
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            return Math.round(r0.A0C.A00);
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeMinTextSize() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeMinTextSize();
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            return Math.round(r0.A0C.A01);
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int getAutoSizeStepGranularity() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeStepGranularity();
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            return Math.round(r0.A0C.A02);
        }
        return -1;
    }

    @Override // android.widget.TextView
    public int[] getAutoSizeTextAvailableSizes() {
        if (AnonymousClass02c.A00) {
            return super.getAutoSizeTextAvailableSizes();
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            return r0.A0C.A07;
        }
        return new int[0];
    }

    @Override // android.widget.TextView
    public int getAutoSizeTextType() {
        if (!AnonymousClass02c.A00) {
            AnonymousClass086 r0 = this.A01;
            if (r0 != null) {
                return r0.A0C.A03;
            }
            return 0;
        } else if (super.getAutoSizeTextType() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    public ColorStateList getSupportCompoundDrawablesTintList() {
        C016107p r0 = this.A01.A08;
        if (r0 != null) {
            return r0.A00;
        }
        return null;
    }

    public PorterDuff.Mode getSupportCompoundDrawablesTintMode() {
        C016107p r0 = this.A01.A08;
        if (r0 != null) {
            return r0.A01;
        }
        return null;
    }

    @Override // android.view.View
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    @Override // android.widget.TextView, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        AnonymousClass086 r1 = this.A01;
        if (r1 != null && !AnonymousClass02c.A00) {
            r1.A0C.A04();
        }
    }

    @Override // android.widget.TextView
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        AnonymousClass086 r1 = this.A01;
        if (r1 != null && !AnonymousClass02c.A00) {
            AnonymousClass08C r12 = r1.A0C;
            if ((!(r12.A09 instanceof AnonymousClass011)) && r12.A03 != 0) {
                r12.A04();
            }
        }
    }

    @Override // android.widget.TextView, X.AnonymousClass02c
    public void setAutoSizeTextTypeUniformWithConfiguration(int i, int i2, int i3, int i4) {
        if (AnonymousClass02c.A00) {
            super.setAutoSizeTextTypeUniformWithConfiguration(i, i2, i3, i4);
            return;
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            r0.A04(i, i2, i3, i4);
        }
    }

    @Override // android.widget.TextView
    public void setAutoSizeTextTypeUniformWithPresetSizes(int[] iArr, int i) {
        if (AnonymousClass02c.A00) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(iArr, i);
            return;
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            r0.A0B(iArr, i);
        }
    }

    @Override // android.widget.TextView, X.AnonymousClass02c
    public void setAutoSizeTextTypeWithDefaults(int i) {
        if (AnonymousClass02c.A00) {
            super.setAutoSizeTextTypeWithDefaults(i);
            return;
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            r0.A03(i);
        }
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(AnonymousClass04D.A02(callback, this));
    }

    public void setSupportAllCaps(boolean z) {
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            r0.A0B.setAllCaps(z);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A04(mode);
        }
    }

    @Override // X.AbstractC004702d
    public void setSupportCompoundDrawablesTintList(ColorStateList colorStateList) {
        AnonymousClass086 r0 = this.A01;
        r0.A07(colorStateList);
        r0.A02();
    }

    @Override // X.AbstractC004702d
    public void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode) {
        AnonymousClass086 r0 = this.A01;
        r0.A08(mode);
        r0.A02();
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            r0.A05(context, i);
        }
    }

    @Override // android.widget.TextView
    public void setTextSize(int i, float f) {
        if (AnonymousClass02c.A00) {
            super.setTextSize(i, f);
            return;
        }
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            AnonymousClass08C r1 = r0.A0C;
            if (!(!(r1.A09 instanceof AnonymousClass011)) || r1.A03 == 0) {
                r1.A06(i, f);
            }
        }
    }
}
