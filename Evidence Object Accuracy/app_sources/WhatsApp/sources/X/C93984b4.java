package X;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.4b4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93984b4 {
    public static final C93984b4 A02 = new C93984b4();
    public final AbstractC115175Qm A00;
    public final ConcurrentMap A01 = new ConcurrentHashMap();

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0026, code lost:
        if (r0 == null) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C93984b4() {
        /*
            r3 = this;
            r3.<init>()
            java.util.concurrent.ConcurrentHashMap r0 = new java.util.concurrent.ConcurrentHashMap
            r0.<init>()
            r3.A01 = r0
            r0 = 1
            java.lang.String[] r1 = new java.lang.String[r0]
            java.lang.String r0 = "com.google.protobuf.AndroidProto3SchemaFactory"
            r2 = 0
            r1[r2] = r0
            r0 = r1[r2]
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch: all -> 0x0028
            java.lang.Class[] r0 = new java.lang.Class[r2]     // Catch: all -> 0x0028
            java.lang.reflect.Constructor r1 = r1.getConstructor(r0)     // Catch: all -> 0x0028
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch: all -> 0x0028
            java.lang.Object r0 = r1.newInstance(r0)     // Catch: all -> 0x0028
            X.5Qm r0 = (X.AbstractC115175Qm) r0     // Catch: all -> 0x0028
            if (r0 != 0) goto L_0x002d
        L_0x0028:
            X.4zT r0 = new X.4zT
            r0.<init>()
        L_0x002d:
            r3.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C93984b4.<init>():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:124:0x01fe A[EDGE_INSN: B:124:0x01fe->B:116:0x01fe ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0150  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0156  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass5XT A00(java.lang.Class r39) {
        /*
        // Method dump skipped, instructions count: 563
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C93984b4.A00(java.lang.Class):X.5XT");
    }
}
