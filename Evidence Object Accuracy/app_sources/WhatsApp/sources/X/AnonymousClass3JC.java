package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3JC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JC {
    public static final byte[] A04 = "endobj".getBytes();
    public static final byte[] A05 = "%FDF-".getBytes();
    public static final byte[] A06 = " obj".getBytes();
    public static final byte[] A07 = "%PDF-".getBytes();
    public static final byte[] A08 = "stream".getBytes();
    public static final String[] A09 = {"/RichMedia", "/JS", "/JavaScript", "/AA", "/Launch", "/RichMediaInstance"};
    public int A00;
    public AnonymousClass4VQ A01 = new AnonymousClass4VQ();
    public File A02;
    public boolean A03;

    public static boolean A01(int i) {
        return i == 47 || i == 60 || i == 62 || i == 91 || i == 93 || i == 40 || i == 41 || i == -1;
    }

    public static boolean A02(int i) {
        return i == 0 || i == 9 || i == 10 || i == 12 || i == 13 || i == 32;
    }

    public AnonymousClass3JC(File file) {
        this.A02 = file;
    }

    public static void A00(InputStream inputStream) {
        int read;
        do {
            read = inputStream.read();
        } while (A02(read));
        if (read != -1) {
            while (true) {
                if (read == 40) {
                    while (true) {
                        int read2 = inputStream.read();
                        if (read2 == 92) {
                            inputStream.read();
                        } else if (!(read2 == 41 || read2 == -1)) {
                        }
                    }
                } else if (read == 60) {
                    do {
                    } while (inputStream.read() != 62);
                } else if (read == 91) {
                    A00(inputStream);
                } else if (read == 93 || read == -1) {
                    return;
                }
                read = inputStream.read();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0070, code lost:
        if (r4 == 47) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0072, code lost:
        r1.append((char) r4);
        r4 = r7.read();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007e, code lost:
        if (A01(r4) == false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0084, code lost:
        if (A02(r4) != false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0086, code lost:
        r5 = r1.toString().trim();
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050 A[LOOP:4: B:25:0x0050->B:26:0x0058, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b A[PHI: r4 
      PHI: (r4v5 int) = (r4v4 int), (r4v12 int) binds: [B:24:0x004e, B:67:0x005b] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0031 A[EDGE_INSN: B:58:0x0031->B:17:0x0031 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x004a A[EDGE_INSN: B:65:0x004a->B:23:0x004a ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map A03(java.io.InputStream r7, int r8) {
        /*
            r6 = this;
            r0 = 500(0x1f4, float:7.0E-43)
            if (r8 > r0) goto L_0x00c4
            java.util.HashMap r2 = X.C12970iu.A11()
        L_0x0008:
            int r4 = r7.read()
            boolean r0 = A02(r4)
            if (r0 == 0) goto L_0x0013
            goto L_0x0008
        L_0x0013:
            boolean r0 = A02(r4)
            if (r0 == 0) goto L_0x0024
        L_0x0019:
            int r4 = r7.read()
            boolean r0 = A02(r4)
            if (r0 == 0) goto L_0x0024
            goto L_0x0019
        L_0x0024:
            r5 = -1
            if (r4 == r5) goto L_0x0031
            r1 = 62
            if (r4 != r1) goto L_0x0032
            int r0 = r7.read()
            if (r0 != r1) goto L_0x0032
        L_0x0031:
            return r2
        L_0x0032:
            java.lang.StringBuilder r3 = X.C12960it.A0h()
        L_0x0036:
            char r0 = (char) r4
            r3.append(r0)
            int r4 = r7.read()
            boolean r0 = A01(r4)
            if (r0 != 0) goto L_0x004a
            boolean r0 = A02(r4)
            if (r0 == 0) goto L_0x0036
        L_0x004a:
            boolean r0 = A02(r4)
            if (r0 == 0) goto L_0x005b
        L_0x0050:
            int r4 = r7.read()
            boolean r0 = A02(r4)
            if (r0 == 0) goto L_0x005b
            goto L_0x0050
        L_0x005b:
            if (r4 == r5) goto L_0x0031
            r5 = 0
            r0 = 40
            if (r4 == r0) goto L_0x00a8
            r0 = 60
            if (r4 == r0) goto L_0x009b
            r0 = 91
            if (r4 == r0) goto L_0x00bc
            java.lang.StringBuilder r1 = X.C12960it.A0h()
            r0 = 47
            if (r4 != r0) goto L_0x007a
        L_0x0072:
            char r0 = (char) r4
            r1.append(r0)
            int r4 = r7.read()
        L_0x007a:
            boolean r0 = A01(r4)
            if (r0 == 0) goto L_0x0072
            boolean r0 = A02(r4)
            if (r0 != 0) goto L_0x0072
            java.lang.String r0 = r1.toString()
            java.lang.String r5 = r0.trim()
        L_0x008e:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = r0.trim()
            r2.put(r0, r5)
            goto L_0x0013
        L_0x009b:
            int r4 = r7.read()
            if (r4 != r0) goto L_0x008e
            int r0 = r8 + 1
            java.util.Map r5 = r6.A03(r7, r0)
            goto L_0x00bf
        L_0x00a8:
            int r1 = r7.read()
            r0 = 92
            if (r1 != r0) goto L_0x00b4
            r7.read()
            goto L_0x00a8
        L_0x00b4:
            r0 = 41
            if (r1 == r0) goto L_0x00bf
            r0 = -1
            if (r1 != r0) goto L_0x00a8
            goto L_0x00bf
        L_0x00bc:
            A00(r7)
        L_0x00bf:
            int r4 = r7.read()
            goto L_0x008e
        L_0x00c4:
            X.4Bt r0 = new X.4Bt
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JC.A03(java.io.InputStream, int):java.util.Map");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        throw new X.C87504Bt();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r7 = this;
            r5 = 0
            r7.A00 = r5
            r7.A03 = r5
            java.io.File r0 = r7.A02
            java.io.BufferedInputStream r3 = X.C12990iw.A0h(r0)
            X.4VQ r6 = r7.A01     // Catch: all -> 0x004b
            byte[] r4 = r6.A01     // Catch: all -> 0x004b
            java.util.Arrays.fill(r4, r5)     // Catch: all -> 0x004b
            r2 = 0
        L_0x0013:
            int r0 = r3.read()     // Catch: all -> 0x004b
            if (r0 < 0) goto L_0x0045
            byte r1 = (byte) r0     // Catch: all -> 0x004b
            int r0 = r6.A00     // Catch: all -> 0x004b
            r4[r0] = r1     // Catch: all -> 0x004b
            int r1 = r0 + 1
            r6.A00 = r1     // Catch: all -> 0x004b
            int r0 = r4.length     // Catch: all -> 0x004b
            int r1 = r1 % r0
            r6.A00 = r1     // Catch: all -> 0x004b
            byte[] r0 = X.AnonymousClass3JC.A07     // Catch: all -> 0x004b
            boolean r0 = r6.A00(r0)     // Catch: all -> 0x004b
            if (r0 != 0) goto L_0x003d
            byte[] r0 = X.AnonymousClass3JC.A05     // Catch: all -> 0x004b
            boolean r0 = r6.A00(r0)     // Catch: all -> 0x004b
            if (r0 != 0) goto L_0x003d
            int r2 = r2 + 1
            r0 = 1024(0x400, float:1.435E-42)
            if (r2 >= r0) goto L_0x0045
            goto L_0x0013
        L_0x003d:
            r0 = 1
            r7.A05(r3, r5, r0)     // Catch: all -> 0x004b
            r3.close()
            return
        L_0x0045:
            X.4Bt r0 = new X.4Bt     // Catch: all -> 0x004b
            r0.<init>()     // Catch: all -> 0x004b
            throw r0     // Catch: all -> 0x004b
        L_0x004b:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x004f
        L_0x004f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JC.A04():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x011c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x000a A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(java.io.InputStream r10, int r11, boolean r12) {
        /*
        // Method dump skipped, instructions count: 298
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JC.A05(java.io.InputStream, int, boolean):void");
    }

    public final void A06(String str) {
        if (!TextUtils.isEmpty(str)) {
            int i = 0;
            if (str.charAt(0) == '/' && str.indexOf(35) >= 0) {
                StringBuilder A0h = C12960it.A0h();
                int length = str.length();
                while (i < length) {
                    char charAt = str.charAt(i);
                    if (charAt != '#' || i > length - 3) {
                        A0h.append(charAt);
                    } else {
                        try {
                            A0h.append((char) Integer.parseInt(str.substring(i + 1, i + 3), 16));
                            i += 2;
                        } catch (NumberFormatException unused) {
                            A0h.append(charAt);
                        }
                    }
                    i++;
                }
                str = A0h.toString();
            }
        }
        for (String str2 : A09) {
            if (str2.equals(str)) {
                this.A03 = true;
                Log.i(C12960it.A0d(str, C12960it.A0k("pdfparser/checkname pdf contains suspicious name ")));
            }
        }
    }

    public final void A07(Map map) {
        if (map != null) {
            Iterator A0n = C12960it.A0n(map);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                A06(C12990iw.A0r(A15));
                Object value = A15.getValue();
                if (value instanceof String) {
                    A06((String) value);
                } else if (value instanceof Map) {
                    A07((Map) value);
                }
            }
        }
    }

    public final boolean A08(InputStream inputStream, byte[] bArr) {
        AnonymousClass4VQ r3 = this.A01;
        byte[] bArr2 = r3.A01;
        Arrays.fill(bArr2, (byte) 0);
        do {
            int read = inputStream.read();
            if (read < 0) {
                return false;
            }
            byte b = (byte) read;
            int i = r3.A00;
            bArr2[i] = b;
            int i2 = i + 1;
            r3.A00 = i2;
            r3.A00 = i2 % bArr2.length;
        } while (!r3.A00(bArr));
        return true;
    }
}
