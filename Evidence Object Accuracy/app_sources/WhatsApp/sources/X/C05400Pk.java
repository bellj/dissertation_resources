package X;

import android.graphics.Rect;

/* renamed from: X.0Pk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05400Pk {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;

    public C05400Pk(Rect rect) {
        int i = rect.left;
        int i2 = rect.top;
        int i3 = rect.right;
        int i4 = rect.bottom;
        this.A01 = i;
        this.A03 = i2;
        this.A02 = i3;
        this.A00 = i4;
    }

    public boolean equals(Object obj) {
        Class<?> cls;
        if (this != obj) {
            if (obj == null) {
                cls = null;
            } else {
                cls = obj.getClass();
            }
            if (C05400Pk.class.equals(cls)) {
                if (obj != null) {
                    C05400Pk r5 = (C05400Pk) obj;
                    if (!(this.A01 == r5.A01 && this.A03 == r5.A03 && this.A02 == r5.A02 && this.A00 == r5.A00)) {
                    }
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type androidx.window.core.Bounds");
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.A01 * 31) + this.A03) * 31) + this.A02) * 31) + this.A00;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append((Object) "Bounds");
        sb.append(" { [");
        sb.append(this.A01);
        sb.append(',');
        sb.append(this.A03);
        sb.append(',');
        sb.append(this.A02);
        sb.append(',');
        sb.append(this.A00);
        sb.append("] }");
        return sb.toString();
    }
}
