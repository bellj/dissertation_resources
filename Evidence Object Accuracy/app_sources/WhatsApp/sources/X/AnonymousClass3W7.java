package X;

/* renamed from: X.3W7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3W7 implements AnonymousClass5W0 {
    public final /* synthetic */ AnonymousClass3M8 A00;
    public final /* synthetic */ C59652v5 A01;

    public AnonymousClass3W7(AnonymousClass3M8 r1, C59652v5 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5W0
    public void ASk() {
        Object obj;
        C89354Jq r0 = this.A01.A01;
        AnonymousClass3M8 r4 = this.A00;
        C68553Vv r3 = r0.A00;
        AnonymousClass4N6 r02 = (AnonymousClass4N6) r3.A07.get(C12980iv.A0i());
        if (r02 != null && (obj = r02.A01) != null) {
            C91524Sb r2 = r3.A05;
            r2.A02 = new AnonymousClass4N3(r4, ((AnonymousClass4T8) obj).A03.indexOf(r4));
            r2.A01 = 11;
            r3.A01();
        }
    }

    @Override // X.AnonymousClass5W0
    public void AUI() {
        Object obj;
        C89354Jq r0 = this.A01.A01;
        AnonymousClass3M8 r4 = this.A00;
        C68553Vv r3 = r0.A00;
        AnonymousClass4N6 r02 = (AnonymousClass4N6) r3.A07.get(C12980iv.A0i());
        if (r02 != null && (obj = r02.A01) != null) {
            C91524Sb r2 = r3.A05;
            r2.A02 = new AnonymousClass4N3(r4, ((AnonymousClass4T8) obj).A03.indexOf(r4));
            r2.A01 = 6;
            r3.A01();
        }
    }
}
