package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5D8  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5D8 implements Iterator {
    public int A00 = 2;
    public Object A01;

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003b, code lost:
        r6 = 0;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        r5 = r3.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
        if (r1 >= r5) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        r0 = r3[r1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        if (r0 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        r0 = "null";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004b, code lost:
        r0 = r0.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0050, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0051, code lost:
        r5 = X.C12980iv.A0s(r0);
        r4 = X.C72453ed.A0o(r0);
        r2 = X.C12980iv.A0t((r5.length() + 1) + X.C12970iu.A07(r4));
        r2.append(r5);
        r2.append('@');
        r4 = X.C12960it.A0d(r4, r2);
        r9 = java.util.logging.Logger.getLogger("com.google.common.base.Strings");
        r10 = java.util.logging.Level.WARNING;
        r0 = r4.length();
        r9.logp(r10, "com.google.common.base.Strings", "lenientToString", X.C72453ed.A0s("Exception during lenientFormat for ", r4, r0), (java.lang.Throwable) r14);
        r5 = X.C12980iv.A0s(r14);
        r2 = X.C12980iv.A0t((r0 + 9) + r5.length());
        r2.append("<");
        r2.append(r4);
        r2.append(" threw ");
        r2.append(r5);
        r0 = X.C12960it.A0d(">", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00df, code lost:
        if (r3 >= r1) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e1, code lost:
        r6.charAt(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00e4, code lost:
        if (r3 >= r1) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00e6, code lost:
        r6.charAt(r1 - 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ed, code lost:
        if (r4.A04 == false) goto L_0x0168;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fe, code lost:
        r10 = r8.length();
        r4 = X.C12980iv.A0t((r5 << 4) + r10);
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x010a, code lost:
        if (r6 >= r5) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x010c, code lost:
        r2 = r8.indexOf("%s", r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0112, code lost:
        if (r2 == -1) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0114, code lost:
        r4.append((java.lang.CharSequence) r8, r9, r2);
        r4.append(r3[r6]);
        r9 = r2 + 2;
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0122, code lost:
        r4.append((java.lang.CharSequence) r8, r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0125, code lost:
        if (r6 >= r5) goto L_0x0148;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0127, code lost:
        r4.append(" [");
        r2 = r6 + 1;
        r4.append(r3[r6]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0133, code lost:
        if (r2 >= r5) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0135, code lost:
        r4.append(", ");
        r4.append(r3[r2]);
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0143, code lost:
        r4.append(']');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0151, code lost:
        throw new java.lang.IndexOutOfBoundsException(r4.toString());
     */
    @Override // java.util.Iterator
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean hasNext() {
        /*
        // Method dump skipped, instructions count: 403
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5D8.hasNext():boolean");
    }

    @Override // java.util.Iterator
    public final Object next() {
        if (hasNext()) {
            this.A00 = 2;
            Object obj = this.A01;
            this.A01 = null;
            return obj;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw C12970iu.A0z();
    }
}
