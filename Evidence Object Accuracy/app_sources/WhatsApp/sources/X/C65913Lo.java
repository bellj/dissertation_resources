package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3Lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65913Lo implements Parcelable {
    public static final C100164lT CREATOR = new C100164lT();
    public final boolean A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C65913Lo) && this.A00 == ((C65913Lo) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public C65913Lo(boolean z) {
        this.A00 = z;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("AvatarGetConfigEntity(hasAvatar=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        C16700pc.A0E(parcel, 0);
        parcel.writeByte(this.A00 ? (byte) 1 : 0);
    }
}
