package X;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;

/* renamed from: X.3Eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64243Eu {
    public float A00;
    public float A01 = 1.0f;
    public int A02;
    public int A03;
    public int A04;
    public Rect A05;
    public RectF A06;
    public RectF A07;
    public DisplayMetrics A08;
    public final Matrix A09 = C13000ix.A01();
    public final Matrix A0A = C13000ix.A01();
    public final RectF A0B = C12980iv.A0K();

    public void A00(AnonymousClass3JD r4) {
        this.A06 = r4.A04;
        RectF rectF = r4.A03;
        this.A07 = rectF;
        int i = r4.A02;
        this.A02 = i;
        this.A05 = null;
        this.A01 = 1.0f;
        if (rectF != null) {
            C63113Ai.A00(this.A09, rectF, (float) i);
        }
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("DoodleViewState{bitmapRect=");
        A0k.append(this.A06);
        A0k.append(", cropRect=");
        A0k.append(this.A07);
        A0k.append(", rotate=");
        A0k.append(this.A02);
        A0k.append(", rotateMatrix=");
        A0k.append(this.A09);
        A0k.append(", zoomScale=");
        A0k.append(this.A01);
        A0k.append(", zoomRect=");
        A0k.append(this.A05);
        A0k.append(", zoomMatrix=");
        A0k.append(this.A0A);
        A0k.append(", displayRect=");
        A0k.append(this.A0B);
        A0k.append(", screenScale=");
        A0k.append(this.A00);
        A0k.append(", displayMetrics=");
        A0k.append(this.A08);
        A0k.append(", viewWidth=");
        A0k.append(this.A04);
        A0k.append(", viewHeight=");
        A0k.append(this.A03);
        return C12960it.A0d("}", A0k);
    }
}
