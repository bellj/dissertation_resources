package X;

import android.transition.Transition;

/* renamed from: X.3xu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83793xu extends AbstractC100714mM {
    public final /* synthetic */ AbstractC35501i8 A00;

    public C83793xu(AbstractC35501i8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        this.A00.AXp(false);
    }
}
