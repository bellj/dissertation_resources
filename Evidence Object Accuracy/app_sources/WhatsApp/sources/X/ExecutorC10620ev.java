package X;

import java.util.concurrent.Executor;

/* renamed from: X.0ev  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final /* synthetic */ class ExecutorC10620ev implements Executor {
    public static final /* synthetic */ ExecutorC10620ev A00 = new ExecutorC10620ev();

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
