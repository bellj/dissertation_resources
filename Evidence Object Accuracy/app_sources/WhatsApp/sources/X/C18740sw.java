package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0sw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18740sw {
    public final C236212j A00;
    public final C18460sU A01;
    public final C236812p A02;
    public final C16490p7 A03;
    public final C21390xL A04;

    public C18740sw(C236212j r1, C18460sU r2, C236812p r3, C16490p7 r4, C21390xL r5) {
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public final ContentValues A00(AnonymousClass1YU r7, AnonymousClass1YT r8) {
        long j;
        long j2;
        ContentValues contentValues = new ContentValues();
        C18460sU r5 = this.A01;
        contentValues.put("jid_row_id", Long.valueOf(r5.A01(r7.A01)));
        contentValues.put("from_me", Integer.valueOf(r7.A03 ? 1 : 0));
        contentValues.put("call_id", r7.A02);
        contentValues.put("transaction_id", Integer.valueOf(r7.A00));
        contentValues.put("timestamp", Long.valueOf(r8.A09));
        contentValues.put("video_call", Boolean.valueOf(r8.A0H));
        contentValues.put("duration", Integer.valueOf(r8.A01));
        contentValues.put("call_result", Integer.valueOf(r8.A00));
        contentValues.put("bytes_transferred", Long.valueOf(r8.A02));
        GroupJid groupJid = r8.A04;
        long j3 = 0;
        if (groupJid != null) {
            j = r5.A01(groupJid);
        } else {
            j = 0;
        }
        contentValues.put("group_jid_row_id", Long.valueOf(j));
        contentValues.put("is_joinable_group_call", Boolean.valueOf(r8.A0G));
        DeviceJid deviceJid = r8.A0A;
        if (deviceJid != null) {
            j2 = r5.A01(deviceJid);
        } else {
            j2 = 0;
        }
        contentValues.put("call_creator_device_jid_row_id", Long.valueOf(j2));
        contentValues.put("call_random_id", r8.A07);
        if (r8.A0F != null) {
            j3 = r8.A0F.A00;
        }
        contentValues.put("call_link_row_id", Long.valueOf(j3));
        return contentValues;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:27:0x00c7 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:32:0x00c7 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v1, types: [com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid] */
    /* JADX WARN: Type inference failed for: r4v2 */
    /* JADX WARN: Type inference failed for: r4v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r4v4, types: [android.database.Cursor] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1YT A01(android.database.Cursor r39, android.database.Cursor r40) {
        /*
        // Method dump skipped, instructions count: 404
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18740sw.A01(android.database.Cursor, android.database.Cursor):X.1YT");
    }

    public AnonymousClass1YT A02(AnonymousClass1YU r40) {
        String str;
        C16310on A01 = this.A03.get();
        try {
            C16330op r9 = A01.A03;
            String[] strArr = new String[4];
            strArr[0] = r40.A02;
            C18460sU r2 = this.A01;
            strArr[1] = Long.toString(r2.A01(r40.A01));
            if (r40.A03) {
                str = "1";
            } else {
                str = "0";
            }
            strArr[2] = str;
            strArr[3] = Integer.toString(r40.A00);
            Cursor A09 = r9.A09("SELECT call_log._id, call_log.call_id, timestamp, video_call, duration, call_result, bytes_transferred, call_log.group_jid_row_id, is_joinable_group_call, call_creator_device_jid_row_id, call_random_id, call_log_row_id, joinable_video_call, call_link._id AS call_link_id, token, creator_jid_row_id FROM call_log LEFT JOIN joinable_call_log ON joinable_call_log.call_log_row_id = call_log._id LEFT JOIN call_link ON call_link._id = call_link_row_id WHERE call_log.call_id = ? AND jid_row_id = ? AND from_me = ? AND transaction_id = ?", strArr);
            if (A09.moveToLast()) {
                Cursor A092 = r9.A09("SELECT _id, jid_row_id, call_result FROM call_log_participant_v2 WHERE call_log_row_id = ? ORDER BY _id", new String[]{Long.toString(A09.getLong(A09.getColumnIndexOrThrow("_id")))});
                long j = A09.getLong(A09.getColumnIndexOrThrow("_id"));
                long j2 = A09.getLong(A09.getColumnIndexOrThrow("timestamp"));
                boolean z = false;
                if (A09.getInt(A09.getColumnIndexOrThrow("video_call")) > 0) {
                    z = true;
                }
                int i = A09.getInt(A09.getColumnIndexOrThrow("duration"));
                int i2 = A09.getInt(A09.getColumnIndexOrThrow("call_result"));
                long j3 = A09.getLong(A09.getColumnIndexOrThrow("bytes_transferred"));
                int i3 = A09.getInt(A09.getColumnIndexOrThrow("group_jid_row_id"));
                boolean z2 = false;
                if (A09.getInt(A09.getColumnIndexOrThrow("is_joinable_group_call")) > 0) {
                    z2 = true;
                }
                long j4 = A09.getLong(A09.getColumnIndexOrThrow("call_creator_device_jid_row_id"));
                ArrayList arrayList = new ArrayList();
                while (A092.moveToNext()) {
                    long j5 = A092.getLong(A092.getColumnIndexOrThrow("_id"));
                    int i4 = A092.getInt(A092.getColumnIndexOrThrow("jid_row_id"));
                    UserJid of = UserJid.of(r2.A03((long) i4));
                    if (C15380n4.A0L(of)) {
                        arrayList.add(new AnonymousClass1YV(of, A092.getInt(A092.getColumnIndexOrThrow("call_result")), j5));
                    } else {
                        StringBuilder sb = new StringBuilder("CallLogStore/readCallLogFromCursors/error getting jid; participantJidRowId=");
                        sb.append(i4);
                        Log.e(sb.toString());
                    }
                }
                String string = A09.getString(A09.getColumnIndexOrThrow("call_random_id"));
                AnonymousClass1YT r18 = new AnonymousClass1YT(this.A00.A00(A09), DeviceJid.of(r2.A03(j4)), GroupJid.of(r2.A03((long) i3)), null, r40, this.A02.A01(A09), string, arrayList, i, i2, j, j2, j3, z, false, false, z2);
                A092.close();
                A09.close();
                A01.close();
                return r18;
            }
            A09.close();
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Deprecated
    public List A03(C30401Xg r32) {
        if (((AbstractC30391Xf) r32).A00) {
            ArrayList arrayList = new ArrayList();
            AnonymousClass1YT A14 = r32.A14();
            if (A14 != null) {
                arrayList.add(A14);
            }
            return arrayList;
        }
        String[] strArr = {Long.toString(r32.A11)};
        ArrayList arrayList2 = new ArrayList();
        C16490p7 r0 = this.A03;
        C16310on A01 = r0.get();
        try {
            r0.A04();
            C16330op r3 = A01.A03;
            AnonymousClass009.A05(r3);
            if (!TextUtils.isEmpty(AnonymousClass1Uj.A00(r3, "table", "call_logs"))) {
                Cursor A09 = r3.A09("SELECT _id, transaction_id, timestamp, video_call, duration, call_result, bytes_transferred FROM call_logs WHERE message_row_id = ?", strArr);
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
                while (A09.moveToNext()) {
                    Cursor A092 = r3.A09("SELECT _id, jid, call_result FROM call_log_participant WHERE call_logs_row_id = ? ORDER BY _id", new String[]{Long.toString(A09.getLong(columnIndexOrThrow))});
                    long j = A09.getLong(A09.getColumnIndexOrThrow("_id"));
                    int i = A09.getInt(A09.getColumnIndexOrThrow("transaction_id"));
                    long j2 = A09.getLong(A09.getColumnIndexOrThrow("timestamp"));
                    boolean z = false;
                    if (A09.getInt(A09.getColumnIndexOrThrow("video_call")) > 0) {
                        z = true;
                    }
                    int i2 = A09.getInt(A09.getColumnIndexOrThrow("duration"));
                    int i3 = A09.getInt(A09.getColumnIndexOrThrow("call_result"));
                    long j3 = A09.getLong(A09.getColumnIndexOrThrow("bytes_transferred"));
                    ArrayList arrayList3 = new ArrayList();
                    while (A092.moveToNext()) {
                        long j4 = A092.getLong(A092.getColumnIndexOrThrow("_id"));
                        UserJid nullable = UserJid.getNullable(A092.getString(A092.getColumnIndexOrThrow("jid")));
                        if (C15380n4.A0L(nullable)) {
                            arrayList3.add(new AnonymousClass1YV(nullable, A092.getInt(A092.getColumnIndexOrThrow("call_result")), j4));
                        }
                    }
                    AnonymousClass1YT A00 = AnonymousClass1YT.A00(null, r32, arrayList3, i, i2, i3, j, j2, j3, z, false, false);
                    if (A00 != null) {
                        arrayList2.add(A00);
                    }
                    A092.close();
                }
                A09.close();
            }
            A01.close();
            return arrayList2;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A04(AnonymousClass1YT r18) {
        boolean z = false;
        if (r18.A02() != -1) {
            z = true;
        }
        AnonymousClass009.A0B("CallLog row_id is not set", z);
        C16310on A02 = this.A03.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            for (Object obj : r18.A04()) {
                AnonymousClass1YV r6 = (AnonymousClass1YV) obj;
                if (r6.A01()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("call_log_row_id", Long.valueOf(r18.A02()));
                    contentValues.put("jid_row_id", Long.valueOf(this.A01.A01(r6.A02)));
                    contentValues.put("call_result", Integer.valueOf(r6.A00));
                    if (r6.A00() != -1) {
                        A02.A03.A00("call_log_participant_v2", contentValues, "_id = ?", new String[]{Long.toString(r6.A00())});
                    } else {
                        long A05 = A02.A03.A05(contentValues, "call_log_participant_v2");
                        synchronized (obj) {
                            r6.A01 = A05;
                        }
                    }
                    synchronized (obj) {
                        r6.A03 = false;
                    }
                }
            }
            synchronized (r18) {
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public synchronized void A05(AnonymousClass1YT r10) {
        String str;
        C16310on A02 = this.A03.A02();
        C16330op r6 = A02.A03;
        String[] strArr = new String[4];
        C18460sU r1 = this.A01;
        AnonymousClass1YU r7 = r10.A0B;
        strArr[0] = Long.toString(r1.A01(r7.A01));
        if (r7.A03) {
            str = "1";
        } else {
            str = "0";
        }
        strArr[1] = str;
        strArr[2] = r7.A02;
        strArr[3] = Integer.toString(r7.A00);
        int A01 = r6.A01("call_log", "jid_row_id = ? AND from_me = ? AND call_id = ? AND transaction_id = ?", strArr);
        StringBuilder sb = new StringBuilder();
        sb.append("CallLogStore/deleteCallLog/rowId=");
        sb.append(r10.A02());
        sb.append("; count=");
        sb.append(A01);
        Log.i(sb.toString());
        A02.close();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r6.A0C() == false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void A06(X.AnonymousClass1YT r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = r6.A0E     // Catch: all -> 0x004c
            if (r0 != 0) goto L_0x0010
            boolean r0 = r6.A0D     // Catch: all -> 0x004c
            if (r0 != 0) goto L_0x0010
            boolean r0 = r6.A0C()     // Catch: all -> 0x004c
            r1 = 1
            if (r0 != 0) goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            java.lang.String r0 = "Only regular call log is stored here"
            X.AnonymousClass009.A0B(r0, r1)     // Catch: all -> 0x004c
            X.0p7 r0 = r5.A03     // Catch: all -> 0x004c
            X.0on r4 = r0.A02()     // Catch: all -> 0x004c
            X.1Lx r3 = r4.A00()     // Catch: all -> 0x0047
            X.1YU r0 = r6.A0B     // Catch: all -> 0x0042
            android.content.ContentValues r2 = r5.A00(r0, r6)     // Catch: all -> 0x0042
            X.0op r1 = r4.A03     // Catch: all -> 0x0042
            java.lang.String r0 = "call_log"
            long r0 = r1.A03(r2, r0)     // Catch: all -> 0x0042
            r6.A07(r0)     // Catch: all -> 0x0042
            r6.A05()     // Catch: all -> 0x0042
            r5.A04(r6)     // Catch: all -> 0x0042
            r3.A00()     // Catch: all -> 0x0042
            r3.close()     // Catch: all -> 0x0047
            r4.close()     // Catch: all -> 0x004c
            monitor-exit(r5)
            return
        L_0x0042:
            r0 = move-exception
            r3.close()     // Catch: all -> 0x0046
        L_0x0046:
            throw r0     // Catch: all -> 0x0047
        L_0x0047:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x004b
        L_0x004b:
            throw r0     // Catch: all -> 0x004c
        L_0x004c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18740sw.A06(X.1YT):void");
    }
}
