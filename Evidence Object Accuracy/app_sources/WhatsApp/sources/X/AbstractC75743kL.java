package X;

import android.view.View;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;

/* renamed from: X.3kL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC75743kL extends AnonymousClass03U {
    public final ParticipantsListViewModel A00;

    public void A08() {
    }

    public abstract void A09(C92194Ux v);

    public AbstractC75743kL(View view, ParticipantsListViewModel participantsListViewModel) {
        super(view);
        this.A00 = participantsListViewModel;
    }
}
