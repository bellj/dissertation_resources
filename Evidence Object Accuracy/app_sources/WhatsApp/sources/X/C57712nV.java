package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.2nV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57712nV extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57712nV A0O;
    public static volatile AnonymousClass255 A0P;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public AbstractC27881Jp A0A;
    public AbstractC27881Jp A0B;
    public AbstractC27881Jp A0C;
    public AbstractC27881Jp A0D;
    public AbstractC27881Jp A0E;
    public C43261wh A0F;
    public String A0G = "";
    public String A0H = "";
    public String A0I;
    public String A0J = "";
    public String A0K = "";
    public String A0L;
    public String A0M = "";
    public boolean A0N;

    static {
        C57712nV r0 = new C57712nV();
        A0O = r0;
        r0.A0W();
    }

    public C57712nV() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A0B = r0;
        this.A0L = "";
        this.A0E = r0;
        this.A0D = r0;
        this.A0C = r0;
        this.A0I = "";
        this.A0A = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        int A02;
        int i;
        C81603uH r1;
        switch (r15.ordinal()) {
            case 0:
                return A0O;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57712nV r2 = (C57712nV) obj2;
                int i2 = this.A01;
                boolean A1R = C12960it.A1R(i2);
                String str = this.A0K;
                int i3 = r2.A01;
                this.A0K = r7.Afy(str, r2.A0K, A1R, C12960it.A1R(i3));
                this.A0J = r7.Afy(this.A0J, r2.A0J, C12960it.A1V(i2 & 2, 2), C12960it.A1V(i3 & 2, 2));
                this.A0G = r7.Afy(this.A0G, r2.A0G, C12960it.A1V(i2 & 4, 4), C12960it.A1V(i3 & 4, 4));
                this.A0H = r7.Afy(this.A0H, r2.A0H, C12960it.A1V(i2 & 8, 8), C12960it.A1V(i3 & 8, 8));
                this.A0M = r7.Afy(this.A0M, r2.A0M, C12960it.A1V(i2 & 16, 16), C12960it.A1V(i3 & 16, 16));
                this.A06 = r7.Afp(this.A06, r2.A06, C12960it.A1V(i2 & 32, 32), C12960it.A1V(i3 & 32, 32));
                this.A00 = r7.Afp(this.A00, r2.A00, C12960it.A1V(i2 & 64, 64), C12960it.A1V(i3 & 64, 64));
                this.A02 = r7.Afp(this.A02, r2.A02, C12960it.A1V(i2 & 128, 128), C12960it.A1V(i3 & 128, 128));
                this.A05 = r7.Afp(this.A05, r2.A05, C12960it.A1V(i2 & 256, 256), C12960it.A1V(i3 & 256, 256));
                this.A0B = r7.Afm(this.A0B, r2.A0B, C12960it.A1V(i2 & 512, 512), C12960it.A1V(i3 & 512, 512));
                this.A0F = (C43261wh) r7.Aft(this.A0F, r2.A0F);
                int i4 = this.A01;
                boolean A1V = C12960it.A1V(i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
                boolean z = this.A0N;
                int i5 = r2.A01;
                this.A0N = r7.Afl(A1V, z, C12960it.A1V(i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH), r2.A0N);
                this.A0L = r7.Afy(this.A0L, r2.A0L, C12960it.A1V(i4 & 4096, 4096), C12960it.A1V(i5 & 4096, 4096));
                this.A0E = r7.Afm(this.A0E, r2.A0E, C12960it.A1V(i4 & DefaultCrypto.BUFFER_SIZE, DefaultCrypto.BUFFER_SIZE), C12960it.A1V(i5 & DefaultCrypto.BUFFER_SIZE, DefaultCrypto.BUFFER_SIZE));
                this.A0D = r7.Afm(this.A0D, r2.A0D, C12960it.A1V(this.A01 & 16384, 16384), C12960it.A1V(r2.A01 & 16384, 16384));
                this.A0C = r7.Afm(this.A0C, r2.A0C, C12960it.A1V(this.A01 & 32768, 32768), C12960it.A1V(r2.A01 & 32768, 32768));
                int i6 = this.A01;
                boolean A1V2 = C12960it.A1V(i6 & 65536, 65536);
                long j = this.A09;
                int i7 = r2.A01;
                this.A09 = r7.Afs(j, r2.A09, A1V2, C12960it.A1V(i7 & 65536, 65536));
                this.A07 = r7.Afp(this.A07, r2.A07, C12960it.A1V(i6 & C25981Bo.A0F, C25981Bo.A0F), C12960it.A1V(i7 & C25981Bo.A0F, C25981Bo.A0F));
                this.A08 = r7.Afp(this.A08, r2.A08, C12960it.A1V(i6 & 262144, 262144), C12960it.A1V(i7 & 262144, 262144));
                this.A04 = r7.Afp(this.A04, r2.A04, C12960it.A1V(i6 & 524288, 524288), C12960it.A1V(i7 & 524288, 524288));
                this.A0I = r7.Afy(this.A0I, r2.A0I, C12960it.A1V(i6 & 1048576, 1048576), C12960it.A1V(i7 & 1048576, 1048576));
                this.A0A = r7.Afm(this.A0A, r2.A0A, C12960it.A1V(i6 & 2097152, 2097152), C12960it.A1V(i7 & 2097152, 2097152));
                int i8 = this.A01;
                boolean A1V3 = C12960it.A1V(i8 & 4194304, 4194304);
                int i9 = this.A03;
                int i10 = r2.A01;
                this.A03 = r7.Afp(i9, r2.A03, A1V3, C12960it.A1V(i10 & 4194304, 4194304));
                if (r7 == C463025i.A00) {
                    this.A01 = i8 | i10;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r22 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r72.A0A();
                                    this.A01 = 1 | this.A01;
                                    this.A0K = A0A;
                                    continue;
                                case 18:
                                    String A0A2 = r72.A0A();
                                    this.A01 |= 2;
                                    this.A0J = A0A2;
                                    continue;
                                case 34:
                                    String A0A3 = r72.A0A();
                                    this.A01 |= 4;
                                    this.A0G = A0A3;
                                    continue;
                                case 42:
                                    String A0A4 = r72.A0A();
                                    this.A01 |= 8;
                                    this.A0H = A0A4;
                                    continue;
                                case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                    String A0A5 = r72.A0A();
                                    this.A01 |= 16;
                                    this.A0M = A0A5;
                                    continue;
                                case 61:
                                    this.A01 |= 32;
                                    this.A06 = r72.A01();
                                    continue;
                                case 69:
                                    this.A01 |= 64;
                                    this.A00 = r72.A01();
                                    continue;
                                case C43951xu.A02:
                                    A02 = r72.A02();
                                    if (AnonymousClass4BY.A00(A02) == null) {
                                        i = 9;
                                        break;
                                    } else {
                                        this.A01 |= 128;
                                        this.A02 = A02;
                                        continue;
                                    }
                                case 80:
                                    A02 = r72.A02();
                                    if (A02 != 0 && A02 != 1) {
                                        i = 10;
                                        break;
                                    } else {
                                        this.A01 |= 256;
                                        this.A05 = A02;
                                        continue;
                                    }
                                    break;
                                case 130:
                                    this.A01 |= 512;
                                    this.A0B = r72.A08();
                                    continue;
                                case 138:
                                    if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                                        r1 = (C81603uH) this.A0F.A0T();
                                    } else {
                                        r1 = null;
                                    }
                                    C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r72, r22, C43261wh.A0O);
                                    this.A0F = r0;
                                    if (r1 != null) {
                                        this.A0F = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                                    }
                                    this.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    continue;
                                case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                                    this.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A0N = r72.A0F();
                                    continue;
                                case 154:
                                    String A0A6 = r72.A0A();
                                    this.A01 |= 4096;
                                    this.A0L = A0A6;
                                    continue;
                                case 162:
                                    this.A01 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A0E = r72.A08();
                                    continue;
                                case 170:
                                    this.A01 |= 16384;
                                    this.A0D = r72.A08();
                                    continue;
                                case 178:
                                    this.A01 |= 32768;
                                    this.A0C = r72.A08();
                                    continue;
                                case 184:
                                    this.A01 |= 65536;
                                    this.A09 = r72.A06();
                                    continue;
                                case 192:
                                    this.A01 |= C25981Bo.A0F;
                                    this.A07 = r72.A02();
                                    continue;
                                case 200:
                                    this.A01 |= 262144;
                                    this.A08 = r72.A02();
                                    continue;
                                case 208:
                                    A02 = r72.A02();
                                    if (AnonymousClass4BU.A00(A02) == null) {
                                        i = 26;
                                        break;
                                    } else {
                                        this.A01 |= 524288;
                                        this.A04 = A02;
                                        continue;
                                    }
                                case 218:
                                    String A0A7 = r72.A0A();
                                    this.A01 |= 1048576;
                                    this.A0I = A0A7;
                                    continue;
                                case 226:
                                    this.A01 |= 2097152;
                                    this.A0A = r72.A08();
                                    continue;
                                case 232:
                                    A02 = r72.A02();
                                    if (AnonymousClass4BU.A00(A02) == null) {
                                        i = 29;
                                        break;
                                    } else {
                                        this.A01 |= 4194304;
                                        this.A03 = A02;
                                        continue;
                                    }
                                default:
                                    if (!A0a(r72, A03)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            super.A0X(i, A02);
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57712nV();
            case 5:
                return new C81883uj();
            case 6:
                break;
            case 7:
                if (A0P == null) {
                    synchronized (C57712nV.class) {
                        if (A0P == null) {
                            A0P = AbstractC27091Fz.A09(A0O);
                        }
                    }
                }
                return A0P;
            default:
                throw C12970iu.A0z();
        }
        return A0O;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A01 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A0K, 0);
        }
        if ((this.A01 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A0J, i2);
        }
        if ((this.A01 & 4) == 4) {
            i2 = AbstractC27091Fz.A04(4, this.A0G, i2);
        }
        if ((this.A01 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(5, this.A0H, i2);
        }
        if ((this.A01 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(6, this.A0M, i2);
        }
        int i3 = this.A01;
        if ((i3 & 32) == 32) {
            i2 += 5;
        }
        if ((i3 & 64) == 64) {
            i2 += 5;
        }
        if ((i3 & 128) == 128) {
            i2 = AbstractC27091Fz.A03(9, this.A02, i2);
        }
        if ((i3 & 256) == 256) {
            i2 = AbstractC27091Fz.A03(10, this.A05, i2);
        }
        if ((i3 & 512) == 512) {
            i2 = AbstractC27091Fz.A05(this.A0B, 16, i2);
        }
        if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C43261wh r0 = this.A0F;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 17, i2);
        }
        int i4 = this.A01;
        if ((i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A00(18);
        }
        if ((i4 & 4096) == 4096) {
            i2 = AbstractC27091Fz.A04(19, this.A0L, i2);
        }
        int i5 = this.A01;
        if ((i5 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i2 = AbstractC27091Fz.A05(this.A0E, 20, i2);
        }
        if ((i5 & 16384) == 16384) {
            i2 = AbstractC27091Fz.A05(this.A0D, 21, i2);
        }
        if ((i5 & 32768) == 32768) {
            i2 = AbstractC27091Fz.A05(this.A0C, 22, i2);
        }
        if ((i5 & 65536) == 65536) {
            i2 += CodedOutputStream.A05(23, this.A09);
        }
        if ((i5 & C25981Bo.A0F) == 131072) {
            i2 = AbstractC27091Fz.A02(24, this.A07, i2);
        }
        if ((i5 & 262144) == 262144) {
            i2 = AbstractC27091Fz.A02(25, this.A08, i2);
        }
        if ((i5 & 524288) == 524288) {
            i2 = AbstractC27091Fz.A03(26, this.A04, i2);
        }
        if ((i5 & 1048576) == 1048576) {
            i2 = AbstractC27091Fz.A04(27, this.A0I, i2);
        }
        int i6 = this.A01;
        if ((i6 & 2097152) == 2097152) {
            i2 = AbstractC27091Fz.A05(this.A0A, 28, i2);
        }
        if ((i6 & 4194304) == 4194304) {
            i2 = AbstractC27091Fz.A03(29, this.A03, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0K);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0I(2, this.A0J);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0I(4, this.A0G);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0I(5, this.A0H);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0I(6, this.A0M);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0D(7, this.A06);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0D(8, this.A00);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0E(9, this.A02);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0E(10, this.A05);
        }
        if ((this.A01 & 512) == 512) {
            codedOutputStream.A0K(this.A0B, 16);
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            C43261wh r0 = this.A0F;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        if ((this.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0J(18, this.A0N);
        }
        if ((this.A01 & 4096) == 4096) {
            codedOutputStream.A0I(19, this.A0L);
        }
        if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0K(this.A0E, 20);
        }
        if ((this.A01 & 16384) == 16384) {
            codedOutputStream.A0K(this.A0D, 21);
        }
        if ((this.A01 & 32768) == 32768) {
            codedOutputStream.A0K(this.A0C, 22);
        }
        if ((this.A01 & 65536) == 65536) {
            codedOutputStream.A0H(23, this.A09);
        }
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0F(24, this.A07);
        }
        if ((this.A01 & 262144) == 262144) {
            codedOutputStream.A0F(25, this.A08);
        }
        if ((this.A01 & 524288) == 524288) {
            codedOutputStream.A0E(26, this.A04);
        }
        if ((this.A01 & 1048576) == 1048576) {
            codedOutputStream.A0I(27, this.A0I);
        }
        if ((this.A01 & 2097152) == 2097152) {
            codedOutputStream.A0K(this.A0A, 28);
        }
        if ((this.A01 & 4194304) == 4194304) {
            codedOutputStream.A0E(29, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
