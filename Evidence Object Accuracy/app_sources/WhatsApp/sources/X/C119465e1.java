package X;

import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import java.security.Signature;
import java.util.Map;

/* renamed from: X.5e1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119465e1 extends AbstractC58672rA {
    public final /* synthetic */ FingerprintBottomSheet A00;
    public final /* synthetic */ AnonymousClass3FE A01;
    public final /* synthetic */ NoviPayBloksActivity A02;
    public final /* synthetic */ Map A03;

    public C119465e1(FingerprintBottomSheet fingerprintBottomSheet, AnonymousClass3FE r2, NoviPayBloksActivity noviPayBloksActivity, Map map) {
        this.A02 = noviPayBloksActivity;
        this.A01 = r2;
        this.A00 = fingerprintBottomSheet;
        this.A03 = map;
    }

    @Override // X.AnonymousClass4UT
    public void A00() {
        AnonymousClass61H.A03(this.A00);
    }

    @Override // X.AbstractC58672rA
    public void A01() {
        this.A00.A1B();
    }

    @Override // X.AbstractC58672rA
    public void A02() {
        this.A00.A1B();
    }

    @Override // X.AbstractC58672rA
    public void A03(int i) {
        this.A02.A2v(i);
    }

    @Override // X.AbstractC58672rA
    public void A04(AnonymousClass02N r3, AnonymousClass21K r4) {
        ((AbstractActivityC121705jc) this.A02).A0N.A08(r3, r4, new byte[1]);
    }

    @Override // X.AbstractC58672rA
    public void A05(Signature signature) {
        NoviPayBloksActivity noviPayBloksActivity = this.A02;
        NoviPayBloksActivity.A1J(this.A00, this.A01, noviPayBloksActivity, signature, this.A03);
    }
}
