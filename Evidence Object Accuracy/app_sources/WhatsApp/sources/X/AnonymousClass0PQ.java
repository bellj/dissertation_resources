package X;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

/* renamed from: X.0PQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PQ {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = 0;
    public Paint A04 = new Paint(3);
    public C06440Tp A05 = null;
    public C06440Tp A06 = null;
    public C06440Tp[] A07 = new C06440Tp[4];
    public final Rect A08 = new Rect();
    public final RectF A09 = new RectF();

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        if (r16 == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        r5 = r20.A04;
        r5.setAlpha(255);
        r3 = r20.A07;
        r9 = 0;
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        if (r3[r9] == null) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        if (r3[r9].A04 != (r20.A02 + 1)) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0054, code lost:
        if (r3[r9].A01() == null) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0056, code lost:
        r13 = r13 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0058, code lost:
        r9 = r9 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005a, code lost:
        if (r9 < 4) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        r0 = r20.A05;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005e, code lost:
        if (r0 == null) goto L_0x00ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0060, code lost:
        r12 = r0.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0064, code lost:
        if (r13 == 4) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0066, code lost:
        if (r12 == null) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0068, code lost:
        if (r12 == r7) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006a, code lost:
        r10 = r20.A02;
        r1 = r20.A05;
        r10 = r10 - r1.A04;
        r2 = r1.A00 >> r10;
        r9 = (1 << r10) - 1;
        r1 = (r20.A00 & r9) * r2;
        r9 = (r9 & r20.A01) * r2;
        r11 = r20.A08;
        r11.set(r1, r9, r1 + r2, r2 + r9);
        r10 = r20.A09;
        r1 = r20.A05;
        r10.set(r22, r23, ((float) r1.A01) + r22, ((float) r1.A00) + r23);
        r21.drawBitmap(r12, r11, r10, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a1, code lost:
        if (r13 <= 0) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a3, code lost:
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a4, code lost:
        r10 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a5, code lost:
        r15 = r3[(r11 << 1) + r10];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00aa, code lost:
        if (r15 == null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b1, code lost:
        if (r15.A04 != (r20.A02 + r14)) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b3, code lost:
        r12 = r15.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b7, code lost:
        if (r12 == null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b9, code lost:
        if (r12 == r7) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00bb, code lost:
        r14 = r15.A00;
        r1 = r14 >> 1;
        r13 = ((float) (r1 * r11)) + r22;
        r9 = ((float) (r1 * r10)) + r23;
        r2 = r20.A08;
        r2.set(0, 0, r15.A01, r14);
        r14 = r20.A09;
        r1 = (float) r1;
        r14.set(r13, r9, r13 + r1, r1 + r9);
        r21.drawBitmap(r12, r2, r14, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00df, code lost:
        r10 = r10 + 1;
        r14 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00e3, code lost:
        if (r10 < 2) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e5, code lost:
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e7, code lost:
        if (r11 >= 2) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ea, code lost:
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ed, code lost:
        if (r6 == 255) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f3, code lost:
        if (r16 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f5, code lost:
        r3 = r20.A04;
        r3.setAlpha(r6);
        r21.drawBitmap(r4, r22, r23, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0103, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(android.graphics.Canvas r21, float r22, float r23) {
        /*
        // Method dump skipped, instructions count: 261
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0PQ.A00(android.graphics.Canvas, float, float):void");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("{tile=");
        Object obj = this.A06;
        Object obj2 = "{x}";
        if (obj == null) {
            obj = obj2;
        }
        sb.append(obj);
        sb.append(", mParentTile=");
        C06440Tp r0 = this.A05;
        if (r0 != null) {
            obj2 = r0;
        }
        sb.append(obj2);
        sb.append(", status=");
        sb.append(this.A03);
        sb.append("}");
        return sb.toString();
    }
}
