package X;

/* renamed from: X.3Gy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64803Gy {
    public static final C64803Gy A02 = new C64803Gy(AnonymousClass4AC.AUTO, 1.0E21f);
    public final float A00;
    public final AnonymousClass4AC A01;

    public C64803Gy(AnonymousClass4AC r1, float f) {
        this.A00 = f;
        this.A01 = r1;
    }

    public String toString() {
        switch (this.A01.ordinal()) {
            case 0:
                return Float.toString(this.A00);
            case 1:
                StringBuilder A0h = C12960it.A0h();
                A0h.append(this.A00);
                return C12960it.A0d("%", A0h);
            case 2:
                return "auto";
            default:
                throw new IllegalStateException();
        }
    }
}
