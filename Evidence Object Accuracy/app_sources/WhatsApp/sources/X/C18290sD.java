package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0sD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18290sD {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C19380u1 A02;
    public final C14830m7 A03;
    public final C14820m6 A04;
    public final C14950mJ A05;
    public final C18460sU A06;
    public final C20820wN A07;
    public final C16490p7 A08;
    public final C21660xm A09;
    public final C21630xj A0A;
    public final C16120oU A0B;
    public final AtomicBoolean A0C = new AtomicBoolean(false);

    public C18290sD(AbstractC15710nm r3, C15570nT r4, C19380u1 r5, C14830m7 r6, C14820m6 r7, C14950mJ r8, C18460sU r9, C20820wN r10, C16490p7 r11, C21660xm r12, C21630xj r13, C16120oU r14) {
        this.A03 = r6;
        this.A06 = r9;
        this.A00 = r3;
        this.A01 = r4;
        this.A0B = r14;
        this.A05 = r8;
        this.A02 = r5;
        this.A0A = r13;
        this.A04 = r7;
        this.A08 = r11;
        this.A09 = r12;
        this.A07 = r10;
    }

    public int A00(AbstractC18500sY r10, AnonymousClass23H r11) {
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            for (AbstractC18500sY r2 : new ArrayList(this.A0A.A00().A00.values())) {
                if (r2.A0G().contains(r10.A0C)) {
                    arrayList.add(r2);
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            AbstractC18500sY r4 = (AbstractC18500sY) it.next();
            int A00 = A00(r4, r11);
            if (A00 != 3) {
                StringBuilder sb = new StringBuilder("DatabaseMigrationManager/processMigrations; name=");
                sb.append(r10.A0C);
                sb.append("; cannot rollback, because reverse dependency ");
                sb.append(r4.A0C);
                sb.append(" cannot be rolled (");
                sb.append(A00);
                sb.append(")");
                Log.i(sb.toString());
                r11.A01++;
                return A00;
            }
        }
        if (r10.A0O() || r10.A0N() || r10.A0R()) {
            try {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("DatabaseMigrationManager/processMigrations; name=");
                String str = r10.A0C;
                sb2.append(str);
                sb2.append("; trying to rollback migration.");
                Log.i(sb2.toString());
                r10.A0K();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("DatabaseMigrationManager/processMigrations; name=");
                sb3.append(str);
                sb3.append("; migration was rolled back.");
                Log.i(sb3.toString());
                if (r10.A0O() || r10.A0N()) {
                    this.A00.AaV("db-rollback-had-no-effect", str, false);
                    r11.A01++;
                    return 3;
                }
                r11.A02++;
                return 3;
            } catch (Exception e) {
                AbstractC15710nm r42 = this.A00;
                StringBuilder sb4 = new StringBuilder("name=");
                String str2 = r10.A0C;
                sb4.append(str2);
                sb4.append(", ");
                sb4.append(e.toString());
                r42.AaV("db-rollback-failed", sb4.toString(), false);
                StringBuilder sb5 = new StringBuilder("DatabaseMigrationManager/processMigrations/error; name=");
                sb5.append(str2);
                sb5.append("; failed to rollback migration.");
                Log.e(sb5.toString());
                r11.A00++;
                return 4;
            }
        } else {
            StringBuilder sb6 = new StringBuilder("DatabaseMigrationManager/processMigrations; name=");
            sb6.append(r10.A0C);
            sb6.append("; rollback not needed, already in original state");
            Log.i(sb6.toString());
            return 3;
        }
    }

    public final AnonymousClass23H A01(C29001Pw r18, List list) {
        boolean z;
        int i;
        StringBuilder sb;
        String str;
        Long A00;
        AnonymousClass23H r3 = new AnonymousClass23H();
        ArrayList arrayList = new ArrayList();
        do {
            z = false;
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                AbstractC18500sY r7 = (AbstractC18500sY) it.next();
                StringBuilder sb2 = new StringBuilder("DatabaseMigrationManager/handleMigrationPhase; name=");
                String str2 = r7.A0C;
                sb2.append(str2);
                sb2.append("; start processing.");
                Log.i(sb2.toString());
                if (!r18.A00()) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("DatabaseMigrationManager/handleMigrationPhase; name=");
                    sb3.append(str2);
                    sb3.append("; conditions check requires to stop migration process.");
                    Log.i(sb3.toString());
                    break;
                }
                StringBuilder sb4 = new StringBuilder("DatabaseMigrationManager/processMigrations/");
                sb4.append(str2);
                Log.i(sb4.toString());
                char c = 3;
                char c2 = 2;
                if (r7.A03() != 3) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("DatabaseMigrationManager/processMigrations; name=");
                    sb5.append(str2);
                    sb5.append("; migration is disabled, skipping.");
                    Log.i(sb5.toString());
                    A04(r7, 3);
                } else {
                    if (r7.A0R()) {
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append("DatabaseMigrationManager/processMigrations; name=");
                        sb6.append(str2);
                        sb6.append("; stale and needs rollback, skipping.");
                        Log.i(sb6.toString());
                        i = 10;
                    } else if (r7.A0O()) {
                        StringBuilder sb7 = new StringBuilder();
                        sb7.append("DatabaseMigrationManager/processMigrations; name=");
                        sb7.append(str2);
                        sb7.append("; already migrated, skipping.");
                        Log.i(sb7.toString());
                        A04(r7, 4);
                    } else if (!(r7 instanceof C19230tm) && !(r7 instanceof AnonymousClass0t2) && r7.A07() > 3 && !r7.A0P() && !r7.A0Q()) {
                        StringBuilder sb8 = new StringBuilder();
                        sb8.append("DatabaseMigrationManager/processMigrations; name=");
                        sb8.append(str2);
                        sb8.append("; migration exceeds retry count; mark it as stuck and skip.");
                        Log.i(sb8.toString());
                        i = 8;
                    } else if (!r7.A0M()) {
                        StringBuilder sb9 = new StringBuilder();
                        sb9.append("DatabaseMigrationManager/processMigrations; name=");
                        sb9.append(str2);
                        sb9.append("; not enough storage to migrate, skipping.");
                        Log.i(sb9.toString());
                        A04(r7, 2);
                    } else {
                        if (!r7.A0N()) {
                            long A05 = r7.A05();
                            if (A05 > 0 && (A00 = r7.A04.A00()) != null && A00.longValue() > A05) {
                                StringBuilder sb10 = new StringBuilder();
                                sb10.append("DatabaseMigrationManager/processMigrations; name=");
                                sb10.append(str2);
                                sb10.append("; database size is too large, skipping.");
                                Log.w(sb10.toString());
                                i = 9;
                            }
                        }
                        c2 = 1;
                        if (!r7.A0T()) {
                            StringBuilder sb11 = new StringBuilder();
                            sb11.append("DatabaseMigrationManager/processMigrations; name=");
                            sb11.append(str2);
                            sb11.append("; pre requisites check failed, not ready.");
                            Log.w(sb11.toString());
                            i = 5;
                        } else if (!r7.A0S()) {
                            for (String str3 : r7.A0G()) {
                                AbstractC18500sY A01 = this.A0A.A01(str3);
                                if (A01 == null) {
                                    sb = new StringBuilder();
                                    sb.append("DatabaseMigrationManager/processMigrations; name=");
                                    sb.append(str2);
                                    sb.append(";  has a dependency '");
                                    sb.append(str3);
                                    str = "' - not found, skipping.";
                                } else if (!A01.A0O()) {
                                    sb = new StringBuilder();
                                    sb.append("DatabaseMigrationManager/processMigrations; name=");
                                    sb.append(str2);
                                    sb.append("; has a dependency '");
                                    sb.append(str3);
                                    str = "' - not migrated, not ready.";
                                } else if (A01.A0R()) {
                                    sb = new StringBuilder();
                                    sb.append("DatabaseMigrationManager/processMigrations; name=");
                                    sb.append(str2);
                                    sb.append("; has a dependency '");
                                    sb.append(str3);
                                    str = "' - stale, not ready.";
                                }
                                sb.append(str);
                                Log.w(sb.toString());
                            }
                            i = 7;
                        } else {
                            if (!r7.A0V(r18)) {
                                c = 4;
                            }
                            c2 = c;
                        }
                    }
                    A04(r7, i);
                }
                if (c2 == 2) {
                    arrayList.add(r7);
                    r3.A01++;
                } else if (c2 == 3) {
                    arrayList.add(r7);
                    r3.A02++;
                    z = true;
                } else if (c2 == 4) {
                    arrayList.add(r7);
                    r3.A00++;
                    StringBuilder sb12 = new StringBuilder("DatabaseMigrationManager/handleMigrationPhase/migration failed; migration.name=");
                    sb12.append(str2);
                    Log.w(sb12.toString());
                }
            }
            list.removeAll(arrayList);
            arrayList.clear();
        } while (z);
        r3.A01 += (long) list.size();
        return r3;
    }

    public void A02(int i) {
        Log.i("DatabaseMigrationManager/processAllConsistencyChecks");
        A03(new C29001Pw(new AbstractC28981Pu[0]), new HashSet(this.A0A.A00().A02().A00), 8, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ff, code lost:
        if (r7 == null) goto L_0x0104;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.C29001Pw r37, java.util.Set r38, int r39, int r40) {
        /*
        // Method dump skipped, instructions count: 1389
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18290sD.A03(X.1Pw, java.util.Set, int, int):void");
    }

    public void A04(AbstractC18500sY r9, int i) {
        C16490p7 r0 = this.A08;
        r0.A04();
        double length = (double) r0.A07.length();
        C44001xz r2 = new C44001xz();
        long j = (long) length;
        List list = this.A09.A00;
        r2.A01 = Double.valueOf((double) C21660xm.A00(list, j));
        r2.A00 = Double.valueOf((double) C21660xm.A00(list, j));
        r2.A09 = r9.A0C;
        r2.A02 = Double.valueOf((double) C21660xm.A00(list, (long) ((double) this.A05.A02())));
        r2.A05 = 0L;
        r2.A07 = 0L;
        r2.A08 = 0L;
        r2.A06 = Long.valueOf(r9.A07());
        r2.A04 = 1;
        r2.A03 = Integer.valueOf(i);
        this.A0B.A07(r2);
    }
}
