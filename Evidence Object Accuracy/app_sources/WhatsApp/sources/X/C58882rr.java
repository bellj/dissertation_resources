package X;

import java.io.File;

/* renamed from: X.2rr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58882rr extends AbstractC84023yH {
    public int A00 = 0;
    public final /* synthetic */ AnonymousClass5TI A01;
    public final /* synthetic */ AbstractC44761zV A02;
    public final /* synthetic */ C44791zY A03;
    public final /* synthetic */ AnonymousClass3HV A04;
    public final /* synthetic */ File A05;

    public C58882rr(AnonymousClass5TI r2, AbstractC44761zV r3, C44791zY r4, AnonymousClass3HV r5, File file) {
        this.A03 = r4;
        this.A05 = file;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        Boolean bool = null;
        try {
            C44791zY r4 = this.A03;
            File file = this.A05;
            if (r4.A0C(this.A01, this.A02, this.A04, file)) {
                bool = Boolean.TRUE;
                return bool;
            }
        } catch (C84153yV e) {
            int i2 = this.A00;
            if (i2 <= 5) {
                this.A00 = i2 + 1;
            } else {
                throw new C84193yZ(e);
            }
        }
        return bool;
    }
}
