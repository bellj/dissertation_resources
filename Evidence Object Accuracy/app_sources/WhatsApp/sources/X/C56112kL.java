package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;

/* renamed from: X.2kL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56112kL extends AbstractC67743Ss {
    public long A00;
    public AssetFileDescriptor A01;
    public Uri A02;
    public FileInputStream A03;
    public boolean A04;
    public final ContentResolver A05;

    public C56112kL(Context context) {
        super(false);
        this.A05 = context.getContentResolver();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A02;
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r13) {
        try {
            Uri uri = r13.A04;
            this.A02 = uri;
            A01();
            AssetFileDescriptor openAssetFileDescriptor = this.A05.openAssetFileDescriptor(uri, "r");
            this.A01 = openAssetFileDescriptor;
            if (openAssetFileDescriptor != null) {
                FileInputStream fileInputStream = new FileInputStream(openAssetFileDescriptor.getFileDescriptor());
                this.A03 = fileInputStream;
                long startOffset = openAssetFileDescriptor.getStartOffset();
                long j = r13.A03;
                long skip = fileInputStream.skip(j + startOffset) - startOffset;
                if (skip == j) {
                    long j2 = r13.A02;
                    if (j2 != -1) {
                        this.A00 = j2;
                    } else {
                        long length = openAssetFileDescriptor.getLength();
                        if (length == -1) {
                            FileChannel channel = fileInputStream.getChannel();
                            long size = channel.size();
                            if (size == 0) {
                                this.A00 = -1;
                            } else {
                                long position = size - channel.position();
                                this.A00 = position;
                                if (position < 0) {
                                    throw new EOFException();
                                }
                            }
                        } else {
                            long j3 = length - skip;
                            this.A00 = j3;
                            if (j3 < 0) {
                                throw new EOFException();
                            }
                        }
                    }
                    this.A04 = true;
                    A03(r13);
                    return this.A00;
                }
                throw new EOFException();
            }
            throw new FileNotFoundException(C12960it.A0Z(uri, "Could not open file descriptor for: ", C12960it.A0h()));
        } catch (IOException e) {
            throw new C867348p(e);
        }
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        this.A02 = null;
        try {
            try {
                FileInputStream fileInputStream = this.A03;
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                try {
                    this.A03 = null;
                    try {
                        AssetFileDescriptor assetFileDescriptor = this.A01;
                        if (assetFileDescriptor != null) {
                            assetFileDescriptor.close();
                        }
                        this.A01 = null;
                        if (this.A04) {
                            this.A04 = false;
                            A00();
                        }
                    } catch (IOException e) {
                        throw new C867348p(e);
                    }
                } catch (Throwable th) {
                    this.A01 = null;
                    if (this.A04) {
                        this.A04 = false;
                        A00();
                    }
                    throw th;
                }
            } catch (IOException e2) {
                throw new C867348p(e2);
            }
        } catch (Throwable th2) {
            try {
                this.A03 = null;
                try {
                    AssetFileDescriptor assetFileDescriptor2 = this.A01;
                    if (assetFileDescriptor2 != null) {
                        assetFileDescriptor2.close();
                    }
                    this.A01 = null;
                    if (this.A04) {
                        this.A04 = false;
                        A00();
                    }
                    throw th2;
                } catch (IOException e3) {
                    throw new C867348p(e3);
                }
            } catch (Throwable th3) {
                this.A01 = null;
                if (this.A04) {
                    this.A04 = false;
                    A00();
                }
                throw th3;
            }
        }
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        if (i2 == 0) {
            return 0;
        }
        long j = this.A00;
        if (j != 0) {
            if (j != -1) {
                try {
                    i2 = (int) Math.min(j, (long) i2);
                } catch (IOException e) {
                    throw new C867348p(e);
                }
            }
            int read = this.A03.read(bArr, i, i2);
            if (read != -1) {
                long j2 = this.A00;
                if (j2 != -1) {
                    this.A00 = j2 - ((long) read);
                }
                A02(read);
                return read;
            } else if (this.A00 != -1) {
                throw new C867348p(new EOFException());
            }
        }
        return -1;
    }
}
