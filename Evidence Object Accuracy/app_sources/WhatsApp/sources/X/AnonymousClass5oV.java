package X;

/* renamed from: X.5oV  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oV extends AbstractC16350or {
    public final /* synthetic */ AbstractActivityC121545iU A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass5oV(AbstractActivityC121545iU r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        AbstractActivityC121545iU r0 = this.A00;
        C16590pI r1 = ((AbstractActivityC121685jC) r0).A07;
        C14900mE r15 = ((ActivityC13810kN) r0).A05;
        AbstractC15710nm r14 = ((ActivityC13810kN) r0).A03;
        C15570nT r13 = ((ActivityC13790kL) r0).A01;
        AbstractC14440lR r12 = ((ActivityC13830kP) r0).A05;
        C64513Fv r11 = r0.A06;
        C17220qS r10 = ((AbstractActivityC121685jC) r0).A0H;
        C18590sh r9 = r0.A0C;
        C18600si r8 = ((AbstractActivityC121665jA) r0).A0C;
        C130165yu r7 = r0.A0F;
        C18610sj r6 = ((AbstractActivityC121685jC) r0).A0M;
        AnonymousClass162 r5 = r0.A07;
        AbstractC17860rW r4 = ((AbstractActivityC121685jC) r0).A0L;
        AnonymousClass6BE r3 = ((AbstractActivityC121665jA) r0).A0D;
        return new C129315xW(r14, r15, r13, r1, r10, ((AbstractActivityC121665jA) r0).A0B, r0.A04, ((AbstractActivityC121685jC) r0).A0K, r11, r4, r8, r6, r5, r3, r9, r12, r7);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        C129315xW r3 = (C129315xW) obj;
        AbstractActivityC121545iU r1 = this.A00;
        if (!r1.AJN()) {
            r1.A05 = r3;
            if (this.A01) {
                r3.A00();
            }
        }
    }
}
