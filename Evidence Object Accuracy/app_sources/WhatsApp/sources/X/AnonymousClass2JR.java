package X;

import android.graphics.drawable.Drawable;
import java.util.Arrays;

/* renamed from: X.2JR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JR {
    public final Drawable A00;
    public final Integer A01;
    public final String A02;
    public final boolean A03;

    public AnonymousClass2JR(Drawable drawable, Integer num, String str, boolean z) {
        this.A02 = str;
        this.A00 = drawable;
        this.A01 = num;
        this.A03 = z;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass2JR r5 = (AnonymousClass2JR) obj;
            if (!this.A02.equals(r5.A02) || this.A00 != r5.A00 || !C29941Vi.A00(this.A01, r5.A01) || this.A03 != r5.A03) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A02, this.A00, this.A01, Boolean.valueOf(this.A03)});
    }
}
