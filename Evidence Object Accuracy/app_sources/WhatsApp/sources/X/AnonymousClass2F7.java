package X;

import android.database.sqlite.SQLiteTransactionListener;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.2F7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2F7 implements SQLiteTransactionListener {
    public final /* synthetic */ AbstractC18500sY A00;
    public final /* synthetic */ AtomicBoolean A01;

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onBegin() {
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onCommit() {
    }

    public AnonymousClass2F7(AbstractC18500sY r1, AtomicBoolean atomicBoolean) {
        this.A00 = r1;
        this.A01 = atomicBoolean;
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onRollback() {
        this.A01.set(false);
    }
}
