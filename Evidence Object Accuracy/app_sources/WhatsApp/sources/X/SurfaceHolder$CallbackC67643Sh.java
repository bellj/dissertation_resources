package X;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;
import com.facebook.redex.IDxEventShape6S0200000_2_I1;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.3Sh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class SurfaceHolder$CallbackC67643Sh implements AnonymousClass5XZ, AbstractC72373eU, AnonymousClass5XS, AnonymousClass5SS, AnonymousClass5SO, SurfaceHolder.Callback, TextureView.SurfaceTextureListener, AbstractC115045Pq, AnonymousClass5Pr, AnonymousClass5Pz {
    public final /* synthetic */ C47492Ax A00;

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ARY(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ASS(AnonymousClass4XL r1, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATo(C94344be r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATr(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATs(AnonymousClass3A1 r1) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATt(boolean z, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void ATx(int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AVk() {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AWU(List list) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXV(Timeline timeline, int i) {
        AnonymousClass4DD.A00(this, timeline, i);
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXW(Timeline timeline, Object obj, int i) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXl(C100564m7 r1, C92524Wg r2) {
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public /* synthetic */ SurfaceHolder$CallbackC67643Sh(C47492Ax r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC72373eU
    public void AMR(String str, long j, long j2) {
        this.A00.A0U.AMR(str, j, j2);
    }

    @Override // X.AbstractC72373eU
    public void AMS(String str) {
        this.A00.A0U.AMS(str);
    }

    @Override // X.AbstractC72373eU
    public void AMT(AnonymousClass4U3 r3) {
        C47492Ax r1 = this.A00;
        r1.A0U.AMT(r3);
        r1.A09 = null;
        r1.A0C = null;
    }

    @Override // X.AbstractC72373eU
    public void AMU(AnonymousClass4U3 r2) {
        C47492Ax r0 = this.A00;
        r0.A0C = r2;
        r0.A0U.AMU(r2);
    }

    @Override // X.AbstractC72373eU
    public void AMV(C100614mC r2, AnonymousClass4XN r3) {
        C47492Ax r0 = this.A00;
        r0.A09 = r2;
        r0.A0U.AMV(r2, r3);
    }

    @Override // X.AbstractC72373eU
    public void AMX(long j) {
        this.A00.A0U.AMX(j);
    }

    @Override // X.AbstractC72373eU
    public void AMY(Exception exc) {
        this.A00.A0U.AMY(exc);
    }

    @Override // X.AbstractC72373eU
    public void AMZ(int i, long j, long j2) {
        this.A00.A0U.AMZ(i, j, j2);
    }

    @Override // X.AnonymousClass5SS
    public void AOn(List list) {
        C47492Ax r0 = this.A00;
        r0.A0F = list;
        Iterator it = r0.A0Y.iterator();
        while (it.hasNext()) {
            ((AnonymousClass5SS) it.next()).AOn(list);
        }
    }

    @Override // X.AnonymousClass5XS
    public void APX(int i, long j) {
        this.A00.A0U.APX(i, j);
    }

    @Override // X.AnonymousClass5XZ
    public void AQ4(boolean z) {
        C47492Ax.A00(this.A00);
    }

    @Override // X.AnonymousClass5XZ
    public void ARX(boolean z) {
    }

    @Override // X.AnonymousClass5SO
    public void ASo(C100624mD r6) {
        C47492Ax r4 = this.A00;
        C106424vg r3 = r4.A0U;
        AnonymousClass4XT A03 = r3.A03(r3.A06.A00);
        r3.A05(A03, new IDxEventShape6S0200000_2_I1(r6, 10, A03), 1007);
        Iterator it = r4.A0X.iterator();
        while (it.hasNext()) {
            ((AnonymousClass5SO) it.next()).ASo(r6);
        }
    }

    @Override // X.AnonymousClass5XZ
    public void ATm(boolean z, int i) {
        C47492Ax.A00(this.A00);
    }

    @Override // X.AnonymousClass5XZ
    public void ATq(int i) {
        C47492Ax.A00(this.A00);
    }

    @Override // X.AnonymousClass5XS
    public void AUw(Surface surface) {
        C47492Ax r1 = this.A00;
        r1.A0U.AUw(surface);
        if (r1.A06 == surface) {
            Iterator it = r1.A0Z.iterator();
            while (it.hasNext()) {
                ((C67653Si) ((AnonymousClass5QR) it.next())).A00.A09.setVisibility(8);
            }
        }
    }

    @Override // X.AbstractC72373eU
    public void AW9(boolean z) {
        C47492Ax r1 = this.A00;
        if (r1.A0J != z) {
            r1.A0J = z;
            r1.A0U.AW9(z);
            Iterator it = r1.A0V.iterator();
            if (it.hasNext()) {
                it.next();
                throw C12980iv.A0n("onSkipSilenceEnabledChanged");
            }
        }
    }

    @Override // X.AnonymousClass5XS
    public void AYE(String str, long j, long j2) {
        this.A00.A0U.AYE(str, j, j2);
    }

    @Override // X.AnonymousClass5XS
    public void AYF(String str) {
        this.A00.A0U.AYF(str);
    }

    @Override // X.AnonymousClass5XS
    public void AYG(AnonymousClass4U3 r3) {
        C47492Ax r1 = this.A00;
        r1.A0U.AYG(r3);
        r1.A0A = null;
        r1.A0D = null;
    }

    @Override // X.AnonymousClass5XS
    public void AYH(AnonymousClass4U3 r2) {
        C47492Ax r0 = this.A00;
        r0.A0D = r2;
        r0.A0U.AYH(r2);
    }

    @Override // X.AnonymousClass5XS
    public void AYI(long j, int i) {
        this.A00.A0U.AYI(j, i);
    }

    @Override // X.AnonymousClass5XS
    public void AYJ(C100614mC r2, AnonymousClass4XN r3) {
        C47492Ax r0 = this.A00;
        r0.A0A = r2;
        r0.A0U.AYJ(r2, r3);
    }

    @Override // X.AnonymousClass5XS
    public void AYK(float f, int i, int i2, int i3) {
        AspectRatioFrameLayout aspectRatioFrameLayout;
        float f2;
        float f3;
        C47492Ax r1 = this.A00;
        r1.A0U.AYK(f, i, i2, i3);
        Iterator it = r1.A0Z.iterator();
        while (it.hasNext()) {
            Log.i(C12960it.A0W(i3, "WAExoPlayerView/onVideoSizeChanged/unappliedRotationDegrees="));
            C47482Aw r2 = ((C67653Si) ((AnonymousClass5QR) it.next())).A00;
            float f4 = 1.0f;
            if (!r2.A0E) {
                ((C52392aj) r2.A0A).setRotationAngle(i3);
                if (i3 == 90 || i3 == 270) {
                    aspectRatioFrameLayout = r2.A0B;
                    if (i2 != 0) {
                        f2 = (float) i2;
                        f3 = ((float) i) * f;
                        f4 = f2 / f3;
                        aspectRatioFrameLayout.setAspectRatio(f4);
                    } else {
                        aspectRatioFrameLayout.setAspectRatio(f4);
                    }
                }
            }
            aspectRatioFrameLayout = r2.A0B;
            if (i2 != 0) {
                f2 = ((float) i) * f;
                f3 = (float) i2;
                f4 = f2 / f3;
                aspectRatioFrameLayout.setAspectRatio(f4);
            } else {
                aspectRatioFrameLayout.setAspectRatio(f4);
            }
        }
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        C47492Ax r2 = this.A00;
        r2.A07(new Surface(surfaceTexture), true);
        r2.A05(i, i2);
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        C47492Ax r2 = this.A00;
        r2.A07(null, true);
        r2.A05(0, 0);
        return true;
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        this.A00.A05(i, i2);
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        this.A00.A05(i2, i3);
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.A00.A07(surfaceHolder.getSurface(), false);
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        C47492Ax r2 = this.A00;
        r2.A07(null, false);
        r2.A05(0, 0);
    }
}
