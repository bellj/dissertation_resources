package X;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.14m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C241714m {
    public final AbstractC15710nm A00;
    public final C34901gt A01 = new C34901gt();
    public final C18460sU A02;
    public final C20850wQ A03;
    public final C16490p7 A04;
    public final C21390xL A05;
    public final Map A06 = Collections.synchronizedMap(new C246516i(200));

    public C241714m(AbstractC15710nm r3, C18460sU r4, C20850wQ r5, C16490p7 r6, C21390xL r7) {
        this.A02 = r4;
        this.A00 = r3;
        this.A05 = r7;
        this.A04 = r6;
        this.A03 = r5;
    }

    public void A00() {
        C34901gt r2 = this.A01;
        synchronized (r2) {
            Iterator it = r2.A02.values().iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }
}
