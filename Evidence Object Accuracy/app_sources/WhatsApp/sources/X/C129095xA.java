package X;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5xA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129095xA {
    public C14830m7 A00;
    public C16590pI A01;
    public C18600si A02;
    public C30931Zj A03 = C117305Zk.A0V("NetworkDeviceIdManager", "infra");
    public JSONObject A04;

    public C129095xA(C14830m7 r3, C16590pI r4, C18600si r5) {
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
    }

    public String A00(int i) {
        String valueOf;
        String optString;
        String encodeToString;
        synchronized (this) {
            JSONObject jSONObject = this.A04;
            if (jSONObject == null) {
                try {
                    String A0p = C12980iv.A0p(this.A02.A01(), "payments_network_id_map");
                    if (A0p != null) {
                        jSONObject = C13000ix.A05(A0p);
                    } else {
                        jSONObject = C117295Zj.A0a();
                    }
                    this.A04 = jSONObject;
                } catch (JSONException e) {
                    this.A03.A05(C12960it.A0d(e.getMessage(), C12960it.A0k("JSONObject instantiation ")));
                    jSONObject = C117295Zj.A0a();
                    this.A04 = jSONObject;
                }
            }
            valueOf = String.valueOf(i);
            optString = jSONObject.optString(valueOf, null);
        }
        if (!TextUtils.isEmpty(optString)) {
            C30931Zj r2 = this.A03;
            StringBuilder A0k = C12960it.A0k("getNetworkId with CARD ");
            A0k.append(i);
            A0k.append(": from cache: ");
            r2.A04(C12960it.A0d(optString, A0k));
            return optString;
        }
        Context context = this.A01.A00;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(Settings.Secure.getString(context.getContentResolver(), "android_id"));
        A0h.append(System.currentTimeMillis());
        byte[] bytes = A0h.toString().getBytes();
        if (i == 1 || i == 5) {
            encodeToString = Base64.encodeToString(C003501n.A06(bytes, bytes, 128, 150).getEncoded(), 11);
        } else {
            encodeToString = null;
        }
        synchronized (this) {
            try {
                this.A04.put(valueOf, encodeToString);
                C18600si r1 = this.A02;
                C12970iu.A1D(C117295Zj.A05(r1), "payments_network_id_map", this.A04.toString());
            } catch (JSONException unused) {
                this.A03.A05("setDeviceId :: failed");
            }
        }
        return encodeToString;
    }
}
