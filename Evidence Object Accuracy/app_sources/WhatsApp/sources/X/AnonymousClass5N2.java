package X;

import java.util.Enumeration;

/* renamed from: X.5N2  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5N2 extends AnonymousClass1TM implements AbstractC115545Ry {
    public static AbstractC94164bM A05 = C114895Nm.A0g;
    public int A00;
    public AnonymousClass5NZ A01;
    public AbstractC94164bM A02;
    public boolean A03;
    public C114605Mj[] A04;

    public AnonymousClass5N2(AnonymousClass5N2 r2, AbstractC94164bM r3) {
        this.A02 = r3;
        this.A04 = r2.A04;
        this.A01 = r2.A01;
    }

    public static AnonymousClass5N2 A01(Object obj, AbstractC94164bM r3) {
        if (obj instanceof AnonymousClass5N2) {
            return new AnonymousClass5N2((AnonymousClass5N2) obj, r3);
        }
        if (obj != null) {
            return new AnonymousClass5N2(AbstractC114775Na.A04(obj), r3);
        }
        return null;
    }

    public C114605Mj[] A03() {
        return (C114605Mj[]) this.A04.clone();
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A01;
    }

    public String toString() {
        return this.A02.A01(this);
    }

    public AnonymousClass5N2(AbstractC114775Na r7, AbstractC94164bM r8) {
        this.A02 = r8;
        this.A04 = new C114605Mj[r7.A0B()];
        Enumeration A0C = r7.A0C();
        boolean z = true;
        int i = 0;
        while (A0C.hasMoreElements()) {
            Object nextElement = A0C.nextElement();
            C114605Mj A00 = C114605Mj.A00(nextElement);
            z &= C12970iu.A1Z(A00, nextElement);
            i++;
            this.A04[i] = A00;
        }
        this.A01 = z ? (AnonymousClass5NZ) r7.A06() : new AnonymousClass5NZ(this.A04);
    }

    public static AnonymousClass5N2 A00(Object obj) {
        if (obj instanceof AnonymousClass5N2) {
            return (AnonymousClass5N2) obj;
        }
        if (obj != null) {
            return new AnonymousClass5N2(AbstractC114775Na.A04(obj), A05);
        }
        return null;
    }

    @Override // X.AnonymousClass1TM
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!((obj instanceof AnonymousClass5N2) || (obj instanceof AbstractC114775Na))) {
                return false;
            }
            AnonymousClass1TN r13 = (AnonymousClass1TN) obj;
            if (!this.A01.A04(r13.Aer())) {
                try {
                    AbstractC94164bM r3 = this.A02;
                    AnonymousClass5N2 r1 = new AnonymousClass5N2(AbstractC114775Na.A04((Object) r13.Aer()), A05);
                    if (!(r3 instanceof C114915No)) {
                        C114605Mj[] A03 = A03();
                        C114605Mj[] A032 = r1.A03();
                        int length = A03.length;
                        int length2 = A032.length;
                        if (length != length2) {
                            return false;
                        }
                        boolean z = (A03[0].A03() == null || A032[0].A03() == null) ? false : !A03[0].A03().A01.A04(A032[0].A03().A01);
                        for (int i = 0; i != length; i++) {
                            C114605Mj r32 = A03[i];
                            int i2 = 0;
                            if (z) {
                                i2 = length2 - 1;
                                while (i2 >= 0) {
                                    if (A032[i2] == null || !C95344dY.A04(r32, A032[i2])) {
                                        i2--;
                                    }
                                }
                                return false;
                            }
                            while (i2 != length2) {
                                if (A032[i2] == null || !C95344dY.A04(r32, A032[i2])) {
                                    i2++;
                                }
                            }
                            return false;
                            A032[i2] = null;
                        }
                        return true;
                    }
                    C114605Mj[] A033 = A03();
                    C114605Mj[] A034 = r1.A03();
                    int length3 = A033.length;
                    if (length3 != A034.length) {
                        return false;
                    }
                    for (int i3 = 0; i3 != length3; i3++) {
                        if (!C95344dY.A04(A033[i3], A034[i3])) {
                            return false;
                        }
                    }
                    return true;
                } catch (Exception unused) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override // X.AnonymousClass1TM
    public int hashCode() {
        if (this.A03) {
            return this.A00;
        }
        this.A03 = true;
        C114605Mj[] A03 = A03();
        int i = 0;
        for (int i2 = 0; i2 != A03.length; i2++) {
            C114605Mj r5 = A03[i2];
            if (r5.A00.A01.length > 1) {
                AnonymousClass5MW[] A04 = r5.A04();
                for (int i3 = 0; i3 != A04.length; i3++) {
                    AnonymousClass5MW r4 = A04[i3];
                    i = (i ^ r4.A01.hashCode()) ^ C95344dY.A00(r4.A00).hashCode();
                }
            } else {
                i = (i ^ r5.A03().A01.hashCode()) ^ C95344dY.A00(A03[i2].A03().A00).hashCode();
            }
        }
        this.A00 = i;
        return i;
    }
}
