package X;

import com.whatsapp.status.viewmodels.StatusesViewModel;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/* renamed from: X.2zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61212zb extends AbstractCallableC112595Dz {
    public final /* synthetic */ StatusesViewModel A00;

    public /* synthetic */ C61212zb(StatusesViewModel statusesViewModel) {
        this.A00 = statusesViewModel;
    }

    @Override // X.AbstractCallableC112595Dz
    public /* bridge */ /* synthetic */ Object A01() {
        StatusesViewModel statusesViewModel = this.A00;
        C18470sV r0 = statusesViewModel.A09;
        r0.A09();
        Map unmodifiableMap = Collections.unmodifiableMap(r0.A07);
        if (!statusesViewModel.A0H.compareAndSet(false, true)) {
            return unmodifiableMap;
        }
        Set set = statusesViewModel.A0G;
        synchronized (set) {
            set.addAll(unmodifiableMap.keySet());
        }
        return unmodifiableMap;
    }
}
