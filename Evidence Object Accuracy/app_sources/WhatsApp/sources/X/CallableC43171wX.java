package X;

import java.util.concurrent.Callable;

/* renamed from: X.1wX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CallableC43171wX implements Callable {
    public C38101nW A00;
    public AnonymousClass1EK A01;

    public CallableC43171wX(C38101nW r1, AnonymousClass1EK r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C38101nW r2 = this.A00;
        if (r2 == null) {
            return null;
        }
        this.A01.A00(r2);
        return new C38091nV(r2.A05(), r2.A06());
    }
}
