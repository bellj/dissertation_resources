package X;

import java.util.concurrent.Executor;

/* renamed from: X.05O  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05O extends AnonymousClass05P {
    public static final Executor A02 = new AnonymousClass05Q();
    public static volatile AnonymousClass05O A03;
    public AnonymousClass05P A00;
    public AnonymousClass05P A01;

    public AnonymousClass05O() {
        AnonymousClass05R r0 = new AnonymousClass05R();
        this.A00 = r0;
        this.A01 = r0;
    }

    public static AnonymousClass05O A00() {
        if (A03 == null) {
            synchronized (AnonymousClass05O.class) {
                if (A03 == null) {
                    A03 = new AnonymousClass05O();
                }
            }
        }
        return A03;
    }

    @Override // X.AnonymousClass05P
    public void A01(Runnable runnable) {
        this.A01.A01(runnable);
    }

    @Override // X.AnonymousClass05P
    public void A02(Runnable runnable) {
        this.A01.A02(runnable);
    }

    @Override // X.AnonymousClass05P
    public boolean A03() {
        return this.A01.A03();
    }
}
