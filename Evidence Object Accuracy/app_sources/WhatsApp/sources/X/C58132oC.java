package X;

import android.view.animation.Animation;

/* renamed from: X.2oC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58132oC extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AbstractC36001jA A00;

    public C58132oC(AbstractC36001jA r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AbstractC36001jA r4 = this.A00;
        r4.A0J(r4.A03(), null, r4.A01(), false);
    }
}
