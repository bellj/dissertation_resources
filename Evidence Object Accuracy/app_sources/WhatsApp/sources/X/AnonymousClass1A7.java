package X;

import android.text.TextUtils;
import android.util.Base64;
import java.util.ArrayList;

/* renamed from: X.1A7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1A7 {
    public final C14900mE A00;
    public final C18640sm A01;
    public final C16590pI A02;
    public final C15650ng A03;
    public final AnonymousClass102 A04;
    public final C18650sn A05;
    public final C18600si A06;
    public final C243515e A07;
    public final C18610sj A08;
    public final C17070qD A09;
    public final C22980zx A0A;

    public AnonymousClass1A7(C14900mE r1, C18640sm r2, C16590pI r3, C15650ng r4, AnonymousClass102 r5, C18650sn r6, C18600si r7, C243515e r8, C18610sj r9, C17070qD r10, C22980zx r11) {
        this.A00 = r1;
        this.A02 = r3;
        this.A09 = r10;
        this.A03 = r4;
        this.A06 = r7;
        this.A0A = r11;
        this.A08 = r9;
        this.A04 = r5;
        this.A01 = r2;
        this.A05 = r6;
        this.A07 = r8;
    }

    public void A00(AnonymousClass1FK r15, Integer num, Integer num2, String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new AnonymousClass1W9("action", "get-transactions"));
        if (!TextUtils.isEmpty(str)) {
            arrayList.add(new AnonymousClass1W9("after", str));
        }
        if (num != null) {
            arrayList.add(new AnonymousClass1W9("version", num.intValue()));
        }
        if (num2 != null) {
            arrayList.add(new AnonymousClass1W9("limit", num2.intValue()));
        }
        AnonymousClass20B A00 = AnonymousClass20B.A00();
        if (!A00.A00.get()) {
            arrayList.add(new AnonymousClass1W9("client-public-key", Base64.encodeToString(A00.A02, 10)));
            AnonymousClass1V8 r10 = new AnonymousClass1V8("account", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0]));
            AbstractC16870pt ACx = this.A09.A02().ACx();
            if (ACx != null) {
                ACx.AeG();
            }
            this.A08.A0F(new C452020o(this.A02.A00, this.A00, r15, this.A05, this, A00, true), r10, "get", 0);
            return;
        }
        throw new IllegalStateException("key has been destroyed");
    }

    public void A01(AnonymousClass1FK r15, String str, boolean z) {
        AnonymousClass20B A00 = AnonymousClass20B.A00();
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[4];
        r4[0] = new AnonymousClass1W9("action", "get-transaction");
        int i = 1;
        r4[1] = new AnonymousClass1W9("id", str);
        if (!z) {
            i = 2;
        }
        r4[2] = new AnonymousClass1W9("version", i);
        if (!A00.A00.get()) {
            r4[3] = new AnonymousClass1W9("client-public-key", Base64.encodeToString(A00.A02, 10));
            this.A08.A0F(new C452020o(this.A02.A00, this.A00, r15, this.A05, this, A00, false), new AnonymousClass1V8("account", r4), "get", 0);
            return;
        }
        throw new IllegalStateException("key has been destroyed");
    }
}
