package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.util.Log;

/* renamed from: X.1kY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36771kY extends BaseAdapter implements Filterable {
    public Filter A00;
    public final /* synthetic */ CallsHistoryFragment A01;

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 4;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public C36771kY(CallsHistoryFragment callsHistoryFragment) {
        this.A01 = callsHistoryFragment;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A01.A0f.size();
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        Filter filter = this.A00;
        if (filter != null) {
            return filter;
        }
        C36961kx r1 = new C36961kx(this.A01);
        this.A00 = r1;
        return r1;
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A01.A0f.get(i);
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) this.A01.A0f.get(i).hashCode();
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        return ((AnonymousClass5W1) this.A01.A0f.get(i)).ADd();
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AbstractC92184Uw r5;
        AnonymousClass5W1 r0;
        View view2 = view;
        CallsHistoryFragment callsHistoryFragment = this.A01;
        AnonymousClass5W1 r3 = (AnonymousClass5W1) callsHistoryFragment.A0f.get(i);
        int ADd = r3.ADd();
        if (ADd != 0) {
            if (ADd == 1 || ADd == 2) {
                if (view == null) {
                    boolean A07 = callsHistoryFragment.A0P.A07(367);
                    int i2 = R.layout.calls_row_legacy;
                    if (A07) {
                        i2 = R.layout.calls_row;
                    }
                    view2 = callsHistoryFragment.A04().inflate(i2, viewGroup, false);
                    if (ADd == 1) {
                        r5 = new C60152w6(view2, callsHistoryFragment, callsHistoryFragment.A0B, callsHistoryFragment.A0D, callsHistoryFragment.A0F, callsHistoryFragment.A0P, callsHistoryFragment.A0T, callsHistoryFragment.A0Z);
                    } else {
                        C14830m7 r02 = callsHistoryFragment.A0J;
                        C14850m9 r03 = callsHistoryFragment.A0P;
                        C15450nH r04 = callsHistoryFragment.A05;
                        C21250x7 r15 = callsHistoryFragment.A0O;
                        AnonymousClass19Z r14 = callsHistoryFragment.A0Z;
                        r5 = new C60172w8(view2, r04, callsHistoryFragment, callsHistoryFragment.A0B, callsHistoryFragment.A0D, callsHistoryFragment.A0F, callsHistoryFragment.A0E, callsHistoryFragment.A0I, r02, callsHistoryFragment.A0K, r15, r03, callsHistoryFragment.A0Q, callsHistoryFragment.A0T, r14);
                    }
                    view2.setTag(r5);
                } else {
                    r5 = (AbstractC92184Uw) view2.getTag();
                }
                AnonymousClass028.A0g(view2, new C53542ec(this, r3));
                r5.A00 = r3;
            } else if (ADd != 3) {
                Log.e("callsfragment/callsadapter/getview Unknown list item type ");
                AnonymousClass009.A0A("Unknown list item type", false);
                return null;
            } else {
                if (view == null) {
                    view2 = callsHistoryFragment.A04().inflate(R.layout.joinable_calls_row, viewGroup, false);
                    C14850m9 r05 = callsHistoryFragment.A0P;
                    C15450nH r06 = callsHistoryFragment.A05;
                    C21250x7 r07 = callsHistoryFragment.A0O;
                    AnonymousClass19Z r152 = callsHistoryFragment.A0Z;
                    AnonymousClass018 r142 = callsHistoryFragment.A0K;
                    C15550nR r13 = callsHistoryFragment.A0B;
                    C15610nY r12 = callsHistoryFragment.A0D;
                    C20710wC r11 = callsHistoryFragment.A0Q;
                    AnonymousClass12F r10 = callsHistoryFragment.A0T;
                    r5 = new C60162w7(view2, callsHistoryFragment.A04, r06, callsHistoryFragment, r13, r12, callsHistoryFragment.A0E, callsHistoryFragment.A0F, r142, callsHistoryFragment.A0M, r07, r05, r11, r10, r152);
                    view2.setTag(r5);
                } else {
                    r5 = (AbstractC92184Uw) view2.getTag();
                }
                r5.A00 = r3;
                int i3 = i + 1;
                if (i3 < getCount() && (r0 = (AnonymousClass5W1) callsHistoryFragment.A0f.get(i3)) != null && r0.ADd() == 3) {
                    int dimensionPixelSize = callsHistoryFragment.A02().getDimensionPixelSize(R.dimen.joinable_calls_container_margin_bottom);
                    View A0D = AnonymousClass028.A0D(view2, R.id.call_row_container);
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) A0D.getLayoutParams();
                    C42941w9.A09(A0D, callsHistoryFragment.A0K, marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, dimensionPixelSize);
                }
            }
            r5.A00(i);
            return view2;
        }
        if (view == null) {
            view2 = callsHistoryFragment.A04().inflate(R.layout.calls_tab_list_section, viewGroup, false);
            view2.setEnabled(false);
        }
        AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) view2.getLayoutParams();
        layoutParams.height = -1;
        view2.setLayoutParams(layoutParams);
        TextView textView = (TextView) view2.findViewById(R.id.title);
        C27531Hw.A06(textView);
        textView.setText(((C1101154h) r3).A00);
        return view2;
    }
}
