package X;

import android.location.Location;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.1je  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36291je extends AbstractView$OnCreateContextMenuListenerC35851ir {
    public final /* synthetic */ GroupChatLiveLocationsActivity2 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C36291je(AnonymousClass12P r24, C244615p r25, C14900mE r26, C15570nT r27, C16240og r28, C22330yu r29, AnonymousClass130 r30, C15550nR r31, AnonymousClass10S r32, C15610nY r33, C21270x9 r34, AnonymousClass131 r35, C14830m7 r36, C15890o4 r37, AnonymousClass018 r38, AnonymousClass12H r39, C244215l r40, GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2, C16030oK r42, AnonymousClass13O r43, C244415n r44, AnonymousClass19Z r45) {
        super(r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40, r42, r43, r44, r45);
        this.A00 = groupChatLiveLocationsActivity2;
    }

    @Override // X.AbstractView$OnCreateContextMenuListenerC35851ir
    public void A0C() {
        super.A0C();
        this.A00.A2f();
    }

    @Override // X.AbstractView$OnCreateContextMenuListenerC35851ir, android.location.LocationListener
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = this.A00;
        if (groupChatLiveLocationsActivity2.A0M.A0s && location != null) {
            AnonymousClass009.A05(groupChatLiveLocationsActivity2.A06);
            groupChatLiveLocationsActivity2.A06.A0B(C65193Io.A01(new LatLng(location.getLatitude(), location.getLongitude())), groupChatLiveLocationsActivity2.A05);
        }
        groupChatLiveLocationsActivity2.A0L.A06 = location;
    }
}
