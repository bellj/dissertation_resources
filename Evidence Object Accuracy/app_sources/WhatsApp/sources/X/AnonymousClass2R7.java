package X;

/* renamed from: X.2R7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2R7 extends AnonymousClass1V4 {
    public long A00;
    public long A01;
    public boolean A02;
    public final int A03;
    public final String A04;
    public final String A05;

    public AnonymousClass2R7(AbstractC15710nm r14, C14830m7 r15, C16120oU r16, C17230qT r17, Integer num, String str, String str2, String str3, int i, long j, long j2) {
        super(r14, r15, r16, r17, num, str, 1, j, j2);
        this.A05 = str2;
        this.A03 = i;
        this.A04 = str3;
    }

    @Override // X.AnonymousClass1V4
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("; type = ");
        sb.append(this.A05);
        sb.append("; messageType = ");
        sb.append(this.A03);
        sb.append("; retryVersion = ");
        sb.append(this.A04);
        sb.append("; hasOrphaned = ");
        sb.append(this.A02);
        sb.append("; totalCount = ");
        sb.append(this.A01);
        sb.append("; processedCount = ");
        sb.append(this.A00);
        return sb.toString();
    }
}
