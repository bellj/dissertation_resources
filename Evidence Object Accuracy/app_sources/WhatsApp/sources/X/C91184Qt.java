package X;

import java.util.HashSet;

/* renamed from: X.4Qt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91184Qt {
    public int A00;
    public AnonymousClass5SB A01;
    public HashSet A02;
    public String[] A03;

    public C91184Qt(String[] strArr, int i) {
        int length = strArr.length;
        if (length < 3) {
            throw C12970iu.A0f("Annotation conditions should have at least 3 elements");
        } else if (i >= 0) {
            String str = strArr[1];
            if (str.equals("any")) {
                this.A01 = new C106144vE();
            } else if (str.equals("all")) {
                this.A01 = new C106134vD();
            } else {
                StringBuilder A0k = C12960it.A0k("'");
                A0k.append(str);
                throw C12970iu.A0f(C12960it.A0d("' is not a valid operation", A0k));
            }
            int i2 = length - 2;
            String[] strArr2 = new String[i2];
            this.A03 = strArr2;
            for (int i3 = 0; i3 < i2; i3++) {
                strArr2[i3] = strArr[i3 + 2];
            }
            this.A02 = C12970iu.A12();
            this.A00 = i;
        } else {
            throw C12970iu.A0f(C12960it.A0W(i, "Fallback sampling rate < 0: "));
        }
    }
}
