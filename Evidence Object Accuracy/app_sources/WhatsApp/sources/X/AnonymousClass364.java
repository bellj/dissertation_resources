package X;

import android.view.View;
import com.whatsapp.picker.search.StickerSearchDialogFragment;

/* renamed from: X.364  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass364 extends C469928m {
    public final /* synthetic */ View A00;
    public final /* synthetic */ StickerSearchDialogFragment A01;

    public AnonymousClass364(View view, StickerSearchDialogFragment stickerSearchDialogFragment) {
        this.A01 = stickerSearchDialogFragment;
        this.A00 = view;
    }
}
