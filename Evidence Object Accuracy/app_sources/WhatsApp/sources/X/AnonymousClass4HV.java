package X;

/* renamed from: X.4HV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4HV {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    @Deprecated
    public static final AnonymousClass1UE A02;
    @Deprecated
    public static final AbstractC115305Qz A03 = new C1091350n();

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77643nc r2 = new C77643nc();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "SafetyNet.API");
    }
}
