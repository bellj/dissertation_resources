package X;

import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3JK  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3JK {
    public static final Pattern A00 = Pattern.compile("à\\(s\\)");
    public static final Pattern A01 = Pattern.compile("la\\(s\\)");

    public static String A00(AnonymousClass018 r1, long j) {
        Calendar instance = Calendar.getInstance(C12970iu.A14(r1));
        instance.setTimeInMillis(j);
        return A03(r1, instance);
    }

    public static String A01(AnonymousClass018 r3, String str, long j) {
        Matcher matcher;
        String str2;
        String A06 = r3.A06();
        if (A06.equals("es")) {
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(j);
            int i = instance.get(11);
            if (i == 1 || (i == 13 && !r3.A04().A00)) {
                matcher = A01.matcher(str);
                str2 = "la";
            } else {
                matcher = A01.matcher(str);
                str2 = "las";
            }
        } else if (!A06.equals("pt")) {
            return str;
        } else {
            Calendar instance2 = Calendar.getInstance();
            instance2.setTimeInMillis(j);
            int i2 = instance2.get(11);
            boolean z = true;
            if (i2 == 0) {
                z = r3.A04().A00;
            } else if (i2 != 1) {
                z = i2 != 13 ? false : true ^ r3.A04().A00;
            }
            matcher = A00.matcher(str);
            if (z) {
                str2 = "à";
            } else {
                str2 = "às";
            }
        }
        return matcher.replaceAll(str2);
    }

    public static String A02(AnonymousClass018 r10, String str, Calendar calendar) {
        int i;
        String A08;
        int[] iArr;
        int[] iArr2;
        Locale A14;
        String str2;
        Object[] objArr;
        char c;
        int i2;
        Integer num;
        String str3;
        int length = str.length();
        StringBuilder A0h = C12960it.A0h();
        int i3 = 0;
        boolean z = false;
        while (i3 < length) {
            char charAt = str.charAt(i3);
            if (charAt == '\'') {
                z = !z;
            } else if (z || "aBhHKm".indexOf(charAt) == -1) {
                A0h.append(charAt);
            } else {
                int i4 = i3;
                while (i4 < length && str.charAt(i4) == charAt) {
                    i4++;
                }
                int i5 = i4 - i3;
                if (charAt != 'B') {
                    if (charAt == 'H') {
                        A14 = C12970iu.A14(r10);
                        str2 = i5 == 1 ? "%d" : "%02d";
                        objArr = new Object[1];
                        c = 0;
                        i2 = 11;
                    } else if (charAt == 'K') {
                        int i6 = calendar.get(10);
                        A14 = C12970iu.A14(r10);
                        str2 = i5 == 1 ? "%d" : "%02d";
                        objArr = new Object[1];
                        c = 0;
                        num = Integer.valueOf(i6);
                        objArr[c] = num;
                        A08 = String.format(A14, str2, objArr);
                        A0h.append(A08);
                        i3 = i4 - 1;
                    } else if (charAt != 'a') {
                        if (charAt != 'h') {
                            if (charAt == 'm') {
                                A14 = C12970iu.A14(r10);
                                if (i5 == 1) {
                                    str2 = "%d";
                                } else {
                                    str2 = "%02d";
                                }
                                objArr = new Object[1];
                                c = 0;
                                i2 = 12;
                            }
                            i3 = i4 - 1;
                        } else {
                            int i7 = calendar.get(10);
                            if (i7 == 0) {
                                i7 = 12;
                            }
                            Locale A142 = C12970iu.A14(r10);
                            if (i5 == 1) {
                                str3 = "%d";
                            } else {
                                str3 = "%02d";
                            }
                            Object[] objArr2 = new Object[1];
                            C12960it.A1P(objArr2, i7, 0);
                            A08 = String.format(A142, str3, objArr2);
                            A0h.append(A08);
                            i3 = i4 - 1;
                        }
                    }
                    num = Integer.valueOf(calendar.get(i2));
                    objArr[c] = num;
                    A08 = String.format(A14, str2, objArr);
                    A0h.append(A08);
                    i3 = i4 - 1;
                } else {
                    Locale A143 = C12970iu.A14(r10);
                    String A05 = AbstractC27291Gt.A05(A143);
                    AnonymousClass00N r1 = AnonymousClass3B2.A00;
                    AnonymousClass4QA r8 = (AnonymousClass4QA) r1.get(A05);
                    if (!(r8 == null && (r8 = (AnonymousClass4QA) r1.get(A143.getLanguage())) == null)) {
                        int i8 = calendar.get(11);
                        if (!(calendar.get(12) != 0 || (iArr = r8.A00) == null || (iArr2 = r8.A01) == null)) {
                            for (int i9 = 0; i9 < iArr.length; i9++) {
                                if (iArr[i9] == i8) {
                                    i = iArr2[i9];
                                    break;
                                }
                            }
                        }
                        i = r8.A02[i8];
                        A08 = r10.A08(i);
                        A0h.append(A08);
                        i3 = i4 - 1;
                    }
                }
                i = 219;
                if (calendar.get(9) == 0) {
                    i = 210;
                }
                A08 = r10.A08(i);
                A0h.append(A08);
                i3 = i4 - 1;
            }
            i3++;
        }
        return A0h.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
        if (r1 == 3) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A03(X.AnonymousClass018 r3, java.util.Calendar r4) {
        /*
            X.1Kv r0 = r3.A04()
            boolean r0 = r0.A00
            if (r0 == 0) goto L_0x0013
            r2 = 224(0xe0, float:3.14E-43)
        L_0x000a:
            java.lang.String r0 = r3.A08(r2)
            java.lang.String r0 = A02(r3, r0, r4)
            return r0
        L_0x0013:
            java.util.Locale r0 = X.C12970iu.A14(r3)
            int r1 = X.AbstractC27291Gt.A00(r0)
            r0 = 2
            if (r1 == r0) goto L_0x0023
            r0 = 3
            r2 = 223(0xdf, float:3.12E-43)
            if (r1 != r0) goto L_0x000a
        L_0x0023:
            r2 = 222(0xde, float:3.11E-43)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JK.A03(X.018, java.util.Calendar):java.lang.String");
    }

    public static String A04(AnonymousClass018 r15, Calendar calendar, Calendar calendar2) {
        int i;
        String str;
        Locale locale;
        String str2;
        Object[] objArr;
        char c;
        int i2;
        String str3;
        String str4;
        if (r15.A04().A00) {
            i = 228;
            if (calendar.get(11) == calendar2.get(11)) {
                i = 229;
            }
        } else {
            int A002 = AbstractC27291Gt.A00(C12970iu.A14(r15));
            if (A002 == 2 || A002 == 3) {
                String A08 = r15.A08(222);
                Object[] A1a = C12980iv.A1a();
                A1a[0] = A02(r15, A08, calendar);
                A1a[1] = A02(r15, A08, calendar2);
                return r15.A0A(230, A1a);
            }
            i = 227;
            if (calendar.get(11) != calendar2.get(11)) {
                i = 225;
                if (calendar.get(9) == calendar2.get(9)) {
                    i = 226;
                }
            }
        }
        String A082 = r15.A08(i);
        int length = A082.length();
        StringBuilder A0h = C12960it.A0h();
        int i3 = 0;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        while (i3 < length) {
            char charAt = A082.charAt(i3);
            if (charAt == '\'') {
                z = !z;
            } else if (z || "ahHKm".indexOf(charAt) == -1) {
                A0h.append(charAt);
            } else {
                int i4 = i3;
                while (i4 < length && A082.charAt(i4) == charAt) {
                    i4++;
                }
                int i5 = i4 - i3;
                if (charAt == 'H') {
                    Calendar calendar3 = calendar2;
                    if (!z3) {
                        calendar3 = calendar;
                    }
                    locale = C12970iu.A14(r15);
                    if (i5 == 1) {
                        str2 = "%d";
                    } else {
                        str2 = "%02d";
                    }
                    objArr = new Object[1];
                    c = 0;
                    i2 = calendar3.get(11);
                } else if (charAt != 'K') {
                    if (charAt == 'a') {
                        Calendar calendar4 = calendar2;
                        if (!z2) {
                            calendar4 = calendar;
                        }
                        int i6 = 219;
                        if (calendar4.get(9) == 0) {
                            i6 = 210;
                        }
                        A0h.append(r15.A08(i6));
                        z2 = true;
                    } else if (charAt == 'h') {
                        Calendar calendar5 = calendar2;
                        if (!z3) {
                            calendar5 = calendar;
                        }
                        int i7 = calendar5.get(10);
                        if (i7 == 0) {
                            i7 = 12;
                        }
                        Locale A14 = C12970iu.A14(r15);
                        if (i5 == 1) {
                            str3 = "%d";
                        } else {
                            str3 = "%02d";
                        }
                        Object[] objArr2 = new Object[1];
                        C12960it.A1P(objArr2, i7, 0);
                        str = String.format(A14, str3, objArr2);
                        A0h.append(str);
                        z3 = true;
                    } else if (charAt == 'm') {
                        Calendar calendar6 = calendar2;
                        if (!z4) {
                            calendar6 = calendar;
                        }
                        Locale A142 = C12970iu.A14(r15);
                        if (i5 == 1) {
                            str4 = "%d";
                        } else {
                            str4 = "%02d";
                        }
                        Object[] objArr3 = new Object[1];
                        C12960it.A1P(objArr3, calendar6.get(12), 0);
                        A0h.append(String.format(A142, str4, objArr3));
                        z4 = true;
                    }
                    i3 = i4 - 1;
                } else {
                    Calendar calendar7 = calendar2;
                    if (!z3) {
                        calendar7 = calendar;
                    }
                    i2 = calendar7.get(10);
                    locale = C12970iu.A14(r15);
                    if (i5 == 1) {
                        str2 = "%d";
                    } else {
                        str2 = "%02d";
                    }
                    objArr = new Object[1];
                    c = 0;
                }
                objArr[c] = Integer.valueOf(i2);
                str = String.format(locale, str2, objArr);
                A0h.append(str);
                z3 = true;
                i3 = i4 - 1;
            }
            i3++;
        }
        return A0h.toString();
    }
}
