package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.4iW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98334iW implements IInterface {
    public final IBinder A00;
    public final String A01;

    public C98334iW(IBinder iBinder, String str) {
        this.A00 = iBinder;
        this.A01 = str;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }
}
