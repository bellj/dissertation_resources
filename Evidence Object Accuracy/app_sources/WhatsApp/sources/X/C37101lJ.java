package X;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.Me;
import com.whatsapp.R;
import com.whatsapp.biz.catalog.view.CatalogHeader;
import com.whatsapp.biz.catalog.view.CategoryMediaCard;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.chromium.net.UrlRequest;

/* renamed from: X.1lJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37101lJ extends AnonymousClass1lK implements AnonymousClass1lN {
    public long A00 = 1;
    public String A01;
    public List A02;
    public boolean A03;
    public boolean A04;
    public final Activity A05;
    public final AnonymousClass19Y A06;
    public final C19850um A07;
    public final AnonymousClass19Q A08;
    public final C53842fM A09;
    public final AbstractC116525Vu A0A;
    public final C15550nR A0B;
    public final C22700zV A0C;
    public final C15610nY A0D;
    public final C14820m6 A0E;
    public final AnonymousClass018 A0F;
    public final C14850m9 A0G;
    public final AnonymousClass11G A0H;
    public final Map A0I = new HashMap();

    public C37101lJ(Activity activity, AnonymousClass12P r10, C15570nT r11, AnonymousClass19Y r12, C19850um r13, AnonymousClass19Q r14, AnonymousClass19T r15, C37071lG r16, C53842fM r17, AbstractC116525Vu r18, C15550nR r19, C22700zV r20, C15610nY r21, C14820m6 r22, AnonymousClass018 r23, C14850m9 r24, AnonymousClass11G r25, UserJid userJid) {
        super(r10, r11, r15, r16, userJid);
        A07(true);
        this.A06 = r12;
        this.A0H = r25;
        this.A07 = r13;
        this.A05 = activity;
        this.A04 = false;
        this.A03 = false;
        this.A0G = r24;
        this.A0B = r19;
        this.A0D = r21;
        this.A0F = r23;
        this.A0C = r20;
        this.A08 = r14;
        this.A0E = r22;
        this.A0A = r18;
        this.A09 = r17;
        A0P(userJid);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        String str;
        StringBuilder sb;
        String str2;
        switch (getItemViewType(i)) {
            case 1:
                return -2;
            case 2:
                return -3;
            case 3:
                return -4;
            case 4:
                return -5;
            case 5:
                C84693zj r2 = (C84693zj) ((AnonymousClass1lL) this).A00.get(i);
                StringBuilder sb2 = new StringBuilder();
                sb2.append("product_");
                sb2.append(r2.A01.A0D);
                str = sb2.toString();
                String str3 = r2.A02;
                if (str3 != null) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("_in_collection_");
                    sb3.append(str3);
                    str = sb3.toString();
                    break;
                }
                break;
            case 6:
                return -6;
            case 7:
                sb = new StringBuilder("collection_");
                str2 = ((C84703zk) ((AnonymousClass1lL) this).A00.get(i)).A02;
                sb.append(str2);
                str = sb.toString();
                break;
            case 8:
                sb = new StringBuilder("collection_review_status_banner");
                str2 = ((C84683zi) ((AnonymousClass1lL) this).A00.get(i)).A01;
                sb.append(str2);
                str = sb.toString();
                break;
            case 9:
                return -7;
            case 10:
                return -8;
            case 11:
                return -10;
            case 12:
                ((AnonymousClass1lL) this).A00.get(i);
                throw new NullPointerException("orderProduct");
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return -9;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return -11;
            case 15:
                return -12;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return -13;
            default:
                return 0;
        }
        Map map = this.A0I;
        if (!map.containsKey(str)) {
            long j = this.A00;
            this.A00 = 1 + j;
            map.put(str, Long.valueOf(j));
        }
        return ((Number) map.get(str)).longValue();
    }

    @Override // X.AnonymousClass1lK
    public AbstractC75723kJ A0F(ViewGroup viewGroup, int i) {
        if (i == 2) {
            UserJid userJid = super.A04;
            C15570nT r6 = ((AnonymousClass1lK) this).A01;
            AnonymousClass12P r5 = ((AnonymousClass1lK) this).A00;
            C15550nR r7 = this.A0B;
            C15610nY r9 = this.A0D;
            return new C59372ua(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_product_catalog_list_footer, viewGroup, false), r5, r6, r7, this.A0C, r9, userJid);
        } else if (i == 5) {
            Activity activity = this.A05;
            UserJid userJid2 = super.A04;
            C15570nT r52 = ((AnonymousClass1lK) this).A01;
            AnonymousClass018 r12 = this.A0F;
            C37071lG r72 = super.A03;
            AnonymousClass19Q r62 = this.A08;
            AbstractC116525Vu r11 = this.A0A;
            View A0F = C12960it.A0F(LayoutInflater.from(activity), viewGroup, R.layout.business_product_catalog_list_product);
            AnonymousClass23N.A01(A0F);
            return new C59302uQ(A0F, r52, r62, r72, this, this, null, r11, r12, userJid2);
        } else if (i == 7) {
            return new C59362uZ(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_catalog_list_collection_header, viewGroup, false), ((AnonymousClass1lK) this).A00, this, this, super.A04);
        } else if (i == 10) {
            return new C84713zl(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_product_catalog_list_loading, viewGroup, false));
        } else {
            switch (i) {
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    AnonymousClass5TS r4 = (AnonymousClass5TS) this.A05;
                    View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.postcode_item_layout, viewGroup, false);
                    inflate.setOnClickListener(new ViewOnClickCListenerShape17S0100000_I1(r4, 19));
                    r4.setPostcodeAndLocationViews(inflate);
                    return new C59332uW(inflate);
                case 15:
                    AnonymousClass12P r92 = ((AnonymousClass1lK) this).A00;
                    UserJid userJid3 = super.A04;
                    return new C59352uY(r92, super.A03, (CategoryMediaCard) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.catalog_list_category_media_card, viewGroup, false), this.A09, userJid3);
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    return new C59342uX(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.catalog_list_category_header, viewGroup, false), this.A09);
                default:
                    if (i != 1) {
                        return super.A0F(viewGroup, i);
                    }
                    UserJid userJid4 = super.A04;
                    return new C59322uV(((AnonymousClass1lK) this).A00, ((AnonymousClass1lK) this).A01, (CatalogHeader) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_product_catalog_list_header, viewGroup, false), userJid4);
            }
        }
    }

    public final int A0J() {
        List list = ((AnonymousClass1lL) this).A00;
        if (list.size() <= 0 || !(list.get(list.size() - 1) instanceof C84663zg)) {
            return -1;
        }
        return list.size() - 1;
    }

    public void A0K() {
        List list = ((AnonymousClass1lL) this).A00;
        if (list.size() <= 0 || !(list.get(0) instanceof C84623zc)) {
            list.add(0, new C84623zc());
            A04(0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r0 != false) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0L() {
        /*
            r4 = this;
            X.19T r2 = r4.A02
            boolean r0 = r2.A01
            if (r0 != 0) goto L_0x003b
            boolean r0 = r2.A00
            if (r0 != 0) goto L_0x003b
            r4.A0G()
        L_0x000d:
            int r1 = r4.A0J()
            r0 = -1
            if (r1 == r0) goto L_0x0038
            java.util.List r0 = r4.A00
            java.lang.Object r3 = r0.get(r1)
            X.3zg r3 = (X.C84663zg) r3
            boolean r0 = r2.A01
            if (r0 != 0) goto L_0x0039
            boolean r0 = r2.A00
            if (r0 != 0) goto L_0x0039
            X.0um r2 = r4.A07
            com.whatsapp.jid.UserJid r1 = r4.A04
            boolean r0 = r2.A0J(r1)
            if (r0 != 0) goto L_0x0035
            boolean r0 = r2.A0I(r1)
            r1 = 3
            if (r0 == 0) goto L_0x0036
        L_0x0035:
            r1 = 1
        L_0x0036:
            r3.A00 = r1
        L_0x0038:
            return
        L_0x0039:
            r1 = 5
            goto L_0x0036
        L_0x003b:
            r4.A0H()
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37101lJ.A0L():void");
    }

    public void A0M() {
        List list = ((AnonymousClass1lL) this).A00;
        if (list.size() > 0 && (list.get(0) instanceof C84623zc)) {
            A03(0);
        }
    }

    public void A0N(int i) {
        AnonymousClass19T r1 = super.A02;
        if (r1.A01 || r1.A00) {
            A0H();
        } else {
            A0G();
        }
        int A0J = A0J();
        if (A0J != -1) {
            C84663zg r2 = (C84663zg) ((AnonymousClass1lL) this).A00.get(A0J);
            int i2 = 1;
            if (i != 404) {
                if (i == 406) {
                    Activity activity = this.A05;
                    C15570nT r22 = ((AnonymousClass1lK) this).A01;
                    AnonymousClass19Y r6 = this.A06;
                    AnonymousClass11G r5 = this.A0H;
                    WeakReference weakReference = C87694Cn.A00;
                    if (weakReference == null || weakReference.get() == null || !((Dialog) weakReference.get()).isShowing()) {
                        r22.A08();
                        Me me = r22.A00;
                        C004802e r23 = new C004802e(activity);
                        r23.A06(R.string.catalog_hidden);
                        r23.A0B(true);
                        r23.setPositiveButton(R.string.cancel, null);
                        r23.A00(R.string.register_user_support_button, new DialogInterface.OnClickListener(activity, me, r6, r5) { // from class: X.3Kp
                            public final /* synthetic */ Activity A00;
                            public final /* synthetic */ Me A01;
                            public final /* synthetic */ AnonymousClass19Y A02;
                            public final /* synthetic */ AnonymousClass11G A03;

                            {
                                this.A00 = r1;
                                this.A02 = r3;
                                this.A03 = r4;
                                this.A01 = r2;
                            }

                            @Override // android.content.DialogInterface.OnClickListener
                            public final void onClick(DialogInterface dialogInterface, int i3) {
                                String A0d;
                                Activity activity2 = this.A00;
                                AnonymousClass19Y r4 = this.A02;
                                AnonymousClass11G r0 = this.A03;
                                Me me2 = this.A01;
                                boolean A00 = r0.A00();
                                StringBuilder A0k = C12960it.A0k("catalog not available");
                                if (me2 == null) {
                                    A0d = "";
                                } else {
                                    StringBuilder A0k2 = C12960it.A0k(" +");
                                    A0k2.append(me2.cc);
                                    A0d = C12960it.A0d(me2.number, A0k2);
                                }
                                activity2.startActivity(r4.A00(activity2, null, null, null, C12960it.A0d(A0d, A0k), null, null, null, A00));
                            }
                        });
                        AnonymousClass04S create = r23.create();
                        C87694Cn.A00 = new WeakReference(create);
                        create.show();
                    }
                    A03(A0J);
                } else if (i == -1) {
                    i2 = 4;
                } else {
                    StringBuilder sb = new StringBuilder("business-catalog-list-adapter/request-catalog/fetch-catalog-error/error: ");
                    sb.append(i);
                    Log.w(sb.toString());
                    i2 = 2;
                }
            }
            r2.A00 = i2;
            A03(A0J);
        }
    }

    public void A0O(C44691zO r10) {
        List list;
        if (r10.A01()) {
            boolean z = true;
            int i = 0;
            while (true) {
                list = ((AnonymousClass1lL) this).A00;
                if (i >= list.size()) {
                    break;
                }
                AbstractC89244Jf r2 = (AbstractC89244Jf) list.get(i);
                if (r2 instanceof C84693zj) {
                    C84693zj r22 = (C84693zj) r2;
                    String str = r22.A01.A0D;
                    String str2 = r10.A0D;
                    if (str.equals(str2)) {
                        r22.A01 = r10;
                        r22.A00 = A0E(str2);
                        A03(i);
                        z = false;
                    }
                }
                i++;
            }
            if (z) {
                int i2 = 0;
                boolean z2 = false;
                boolean z3 = false;
                for (int i3 = 0; i3 < list.size(); i3++) {
                    AbstractC89244Jf r6 = (AbstractC89244Jf) list.get(i3);
                    int i4 = r6.A00;
                    if (!(i4 == 16 || i4 == 15 || i4 == 1 || i4 == 14)) {
                        if (r6 instanceof C84703zk) {
                            if ("catalog_products_all_items_collection_id".equals(((C84703zk) r6).A02)) {
                                z3 = true;
                            }
                            i2++;
                            z2 = true;
                        } else {
                            if (r6 instanceof C84693zj) {
                                if (AnonymousClass1US.A0C(((C84693zj) r6).A02)) {
                                    break;
                                }
                            } else if (!(r6 instanceof C84683zi)) {
                                break;
                            }
                        }
                    }
                    i2++;
                }
                if ((!z2 || z3) && i2 != -1) {
                    list.add(i2, new C84693zj(r10, 5, A0E(r10.A0D)));
                    A04(i2);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0163, code lost:
        if (r0.A01 != false) goto L_0x0165;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0P(com.whatsapp.jid.UserJid r14) {
        /*
        // Method dump skipped, instructions count: 382
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37101lJ.A0P(com.whatsapp.jid.UserJid):void");
    }

    @Override // X.AnonymousClass1lN
    public C90174Mw ABV(int i) {
        int i2 = 0;
        boolean z = false;
        int i3 = -1;
        int i4 = -1;
        while (true) {
            if (i2 <= i) {
                AbstractC89244Jf r2 = (AbstractC89244Jf) ((AnonymousClass1lL) this).A00.get(i2);
                if (r2 instanceof C84703zk) {
                    if ("catalog_products_all_items_collection_id".equals(((C84703zk) r2).A02)) {
                        break;
                    }
                    i3++;
                    z = true;
                    i4 = -1;
                }
                if (r2 instanceof C84693zj) {
                    if (!z) {
                        break;
                    }
                    i4++;
                }
                i2++;
            } else if (z) {
                return new C90174Mw(String.valueOf(i3), String.valueOf(i4));
            }
        }
        return null;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 2) {
            UserJid userJid = super.A04;
            C15570nT r6 = ((AnonymousClass1lK) this).A01;
            AnonymousClass12P r5 = ((AnonymousClass1lK) this).A00;
            C15550nR r7 = this.A0B;
            C15610nY r9 = this.A0D;
            return new C59372ua(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_product_catalog_list_footer, viewGroup, false), r5, r6, r7, this.A0C, r9, userJid);
        } else if (i == 5) {
            Activity activity = this.A05;
            UserJid userJid2 = super.A04;
            C15570nT r52 = ((AnonymousClass1lK) this).A01;
            AnonymousClass018 r12 = this.A0F;
            C37071lG r72 = super.A03;
            AnonymousClass19Q r62 = this.A08;
            AbstractC116525Vu r11 = this.A0A;
            View A0F = C12960it.A0F(LayoutInflater.from(activity), viewGroup, R.layout.business_product_catalog_list_product);
            AnonymousClass23N.A01(A0F);
            return new C59302uQ(A0F, r52, r62, r72, this, this, null, r11, r12, userJid2);
        } else if (i == 7) {
            return new C59362uZ(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_catalog_list_collection_header, viewGroup, false), ((AnonymousClass1lK) this).A00, this, this, super.A04);
        } else if (i == 10) {
            return new C84713zl(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_product_catalog_list_loading, viewGroup, false));
        } else {
            switch (i) {
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    AnonymousClass5TS r4 = (AnonymousClass5TS) this.A05;
                    View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.postcode_item_layout, viewGroup, false);
                    inflate.setOnClickListener(new ViewOnClickCListenerShape17S0100000_I1(r4, 19));
                    r4.setPostcodeAndLocationViews(inflate);
                    return new C59332uW(inflate);
                case 15:
                    AnonymousClass12P r92 = ((AnonymousClass1lK) this).A00;
                    UserJid userJid3 = super.A04;
                    return new C59352uY(r92, super.A03, (CategoryMediaCard) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.catalog_list_category_media_card, viewGroup, false), this.A09, userJid3);
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    return new C59342uX(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.catalog_list_category_header, viewGroup, false), this.A09);
                default:
                    if (i != 1) {
                        return super.A0F(viewGroup, i);
                    }
                    UserJid userJid4 = super.A04;
                    return new C59322uV(((AnonymousClass1lK) this).A00, ((AnonymousClass1lK) this).A01, (CatalogHeader) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.business_product_catalog_list_header, viewGroup, false), userJid4);
            }
        }
    }
}
