package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

/* renamed from: X.0Uz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06770Uz implements Application.ActivityLifecycleCallbacks {
    public Activity A00;
    public Object A01;
    public boolean A02 = false;
    public boolean A03 = false;
    public boolean A04 = false;
    public final int A05;

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
    }

    public C06770Uz(Activity activity) {
        this.A00 = activity;
        this.A05 = activity.hashCode();
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        if (this.A00 == activity) {
            this.A00 = null;
            this.A02 = true;
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
        if (this.A02 && !this.A04 && !this.A03) {
            Object obj = this.A01;
            int i = this.A05;
            try {
                Object obj2 = AnonymousClass0RV.A02.get(activity);
                if (obj2 == obj && activity.hashCode() == i) {
                    AnonymousClass0RV.A00.postAtFrontOfQueue(new RunnableC09500d0(AnonymousClass0RV.A01.get(activity), obj2));
                    this.A04 = true;
                    this.A01 = null;
                }
            } catch (Throwable th) {
                Log.e("ActivityRecreator", "Exception while fetching field values", th);
            }
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
        if (this.A00 == activity) {
            this.A03 = true;
        }
    }
}
