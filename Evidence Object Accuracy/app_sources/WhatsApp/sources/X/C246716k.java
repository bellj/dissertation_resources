package X;

/* renamed from: X.16k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C246716k {
    public final C15450nH A00;
    public final C14850m9 A01;

    public C246716k(C15450nH r1, C14850m9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public int A00() {
        int A02 = this.A01.A02(471);
        if (A02 <= 0) {
            A02 = this.A00.A02(AbstractC15460nI.A1J);
        }
        return A02 & 1020;
    }
}
