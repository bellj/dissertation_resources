package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.backup.encryptedbackup.PasswordInputFragment;

/* renamed from: X.4mK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100694mK implements TextWatcher {
    public final /* synthetic */ PasswordInputFragment A00;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C100694mK(PasswordInputFragment passwordInputFragment) {
        this.A00 = passwordInputFragment;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A00.A1B();
    }
}
