package X;

import java.io.File;
import java.io.IOException;

/* renamed from: X.28Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass28Q {
    public final AnonymousClass1D6 A00;
    public final C14820m6 A01;
    public final C14850m9 A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;

    public AnonymousClass28Q(C14330lG r3, AnonymousClass1D6 r4, C14820m6 r5, C14850m9 r6) {
        this.A02 = r6;
        this.A00 = r4;
        this.A01 = r5;
        try {
            File file = r3.A04().A02;
            C14330lG.A03(file, false);
            this.A03 = file.getCanonicalPath();
            File file2 = r3.A04().A0M;
            C14330lG.A03(file2, false);
            this.A06 = file2.getCanonicalPath();
            this.A04 = r3.A04().A05.getCanonicalPath();
            File file3 = r3.A04().A0A;
            C14330lG.A03(file3, false);
            this.A05 = file3.getCanonicalPath();
            this.A07 = r3.A04().A0N.getCanonicalPath();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0044, code lost:
        if (r10.startsWith(r8.A03) == false) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0009, code lost:
        if (r10.startsWith(r8.A06) == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00(java.io.File r9, java.lang.String r10) {
        /*
            r8 = this;
            if (r10 == 0) goto L_0x000b
            java.lang.String r0 = r8.A06
            boolean r1 = r10.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            r3 = 0
            if (r0 != 0) goto L_0x0025
            X.0m6 r0 = r8.A01
            android.content.SharedPreferences r1 = r0.A00
            java.lang.String r0 = "gdrive_include_videos_in_backup"
            boolean r0 = r1.getBoolean(r0, r3)
            if (r0 != 0) goto L_0x0026
            if (r10 == 0) goto L_0x0026
            java.lang.String r0 = r8.A07
            boolean r0 = r10.startsWith(r0)
            if (r0 == 0) goto L_0x0026
        L_0x0025:
            return r3
        L_0x0026:
            X.0m9 r6 = r8.A02
            r0 = 1239(0x4d7, float:1.736E-42)
            int r0 = r6.A02(r0)
            long r4 = (long) r0
            r1 = 1000000(0xf4240, double:4.940656E-318)
            long r4 = r4 * r1
            r0 = 1240(0x4d8, float:1.738E-42)
            int r0 = r6.A02(r0)
            long r6 = (long) r0
            long r6 = r6 * r1
            if (r10 == 0) goto L_0x0046
            java.lang.String r0 = r8.A03
            boolean r1 = r10.startsWith(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0047
        L_0x0046:
            r0 = 0
        L_0x0047:
            long r1 = r9.length()
            if (r0 == 0) goto L_0x004e
            r4 = r6
        L_0x004e:
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0074
            java.lang.String r0 = "gdrive-util/should-backup/too-large "
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            java.lang.String r0 = r9.getAbsolutePath()
            r2.append(r0)
            java.lang.String r0 = " size:"
            r2.append(r0)
            long r0 = r9.length()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            com.whatsapp.util.Log.w(r0)
            return r3
        L_0x0074:
            java.lang.String r1 = r9.getName()
            java.lang.String r0 = "."
            int r2 = r1.lastIndexOf(r0)
            r0 = -1
            if (r2 == r0) goto L_0x008a
            int r1 = r1.length()
            r0 = 1
            int r1 = r1 - r0
            if (r2 == r1) goto L_0x008a
            return r0
        L_0x008a:
            r9.getAbsolutePath()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass28Q.A00(java.io.File, java.lang.String):boolean");
    }
}
