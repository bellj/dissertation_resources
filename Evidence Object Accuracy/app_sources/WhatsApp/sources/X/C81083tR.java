package X;

import java.util.Comparator;

/* renamed from: X.3tR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81083tR extends C17960rg {
    public final Comparator comparator;

    public C81083tR(Comparator comparator) {
        this.comparator = comparator;
    }

    @Override // X.C17960rg, X.AbstractC17970rh, X.AbstractC17980ri
    public C81083tR add(Object obj) {
        super.add(obj);
        return this;
    }

    @Override // X.C17960rg, X.AbstractC17970rh
    public C81083tR add(Object... objArr) {
        super.add(objArr);
        return this;
    }

    @Override // X.C17960rg, X.AbstractC17970rh, X.AbstractC17980ri
    public C81083tR addAll(Iterable iterable) {
        super.addAll(iterable);
        return this;
    }

    @Override // X.C17960rg
    public AbstractC81103tT build() {
        AbstractC81103tT construct = AbstractC81103tT.construct(this.comparator, this.size, this.contents);
        this.size = construct.size();
        this.forceCopy = true;
        return construct;
    }
}
