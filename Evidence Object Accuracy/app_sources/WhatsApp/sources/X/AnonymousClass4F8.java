package X;

import android.os.SystemClock;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* renamed from: X.4F8  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4F8 {
    public static boolean A00(CountDownLatch countDownLatch) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        boolean z = false;
        long j = 5000;
        boolean z2 = false;
        while (true) {
            try {
                z = countDownLatch.await(j, TimeUnit.MILLISECONDS);
                if (z2) {
                }
            } catch (InterruptedException unused) {
                z2 = true;
                j = 5000 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                if (j <= 0) {
                    break;
                }
            }
        }
        Thread.currentThread().interrupt();
        return z;
    }
}
