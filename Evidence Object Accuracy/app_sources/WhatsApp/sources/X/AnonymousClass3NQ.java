package X;

import android.view.ViewTreeObserver;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;

/* renamed from: X.3NQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NQ implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ StickerStorePackPreviewActivity A00;

    public AnonymousClass3NQ(StickerStorePackPreviewActivity stickerStorePackPreviewActivity) {
        this.A00 = stickerStorePackPreviewActivity;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        StickerStorePackPreviewActivity stickerStorePackPreviewActivity = this.A00;
        int width = stickerStorePackPreviewActivity.A0B.getWidth() / C12960it.A09(stickerStorePackPreviewActivity.A0B).getDimensionPixelSize(R.dimen.sticker_store_preview_item);
        if (stickerStorePackPreviewActivity.A00 != width) {
            stickerStorePackPreviewActivity.A0A.A1h(width);
            stickerStorePackPreviewActivity.A00 = width;
            C54472gm r0 = stickerStorePackPreviewActivity.A0M;
            if (r0 != null) {
                r0.A02();
            }
        }
    }
}
