package X;

import com.whatsapp.payments.ui.PaymentsUnavailableDialogFragment;
import java.util.HashMap;

/* renamed from: X.5wo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128875wo {
    public final /* synthetic */ ActivityC13810kN A00;
    public final /* synthetic */ C128135vc A01;
    public final /* synthetic */ C126965tj A02;

    public C128875wo(ActivityC13810kN r1, C128135vc r2, C126965tj r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public void A00(C452120p r5) {
        C128135vc r2 = this.A01;
        r2.A07.A06(C12960it.A0b("performDobComplianceCheck onError: ", r5));
        C129565xv r0 = r2.A06;
        ActivityC13810kN r22 = this.A00;
        r0.A01.A06(C12960it.A0b("maybeHandleUnderageError: ", r5));
        int i = r5.A00;
        if (i == 2896001) {
            r22.Adm(PaymentsUnavailableDialogFragment.A01());
            C117315Zl.A0S(this.A02.A00);
        } else if (i == 10755) {
            C117315Zl.A0S(this.A02.A00);
            r22.Adm(PaymentsUnavailableDialogFragment.A00());
        } else {
            C126965tj r3 = this.A02;
            HashMap A11 = C12970iu.A11();
            A11.put("error_code", String.valueOf(i));
            r3.A00.A01("on_exception", A11);
        }
    }
}
