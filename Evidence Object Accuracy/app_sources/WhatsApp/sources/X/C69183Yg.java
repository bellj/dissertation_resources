package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;

/* renamed from: X.3Yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C69183Yg implements AnonymousClass5WA {
    public final /* synthetic */ AnonymousClass2DQ A00;
    public final /* synthetic */ C17870rX A01;
    public final /* synthetic */ AnonymousClass17Q A02;

    public C69183Yg(AnonymousClass2DQ r1, C17870rX r2, AnonymousClass17Q r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WA
    public void AQE(AnonymousClass3FA r6) {
        C16700pc.A0E(r6, 0);
        this.A01.A00.A0I(new RunnableBRunnable0Shape6S0200000_I0_6(this.A00, 45, AnonymousClass3I1.A00(r6)));
    }

    @Override // X.AnonymousClass5WA
    public void AQF(AnonymousClass3EI r6) {
        AnonymousClass3I1.A02(this.A02, r6);
        this.A01.A00.A0I(new RunnableBRunnable0Shape6S0200000_I0_6(this.A00, 45, AnonymousClass3I1.A01(r6)));
    }
}
