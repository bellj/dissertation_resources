package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.0HL  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0HL extends C04590Mh {
    public final int A00 = 13;
    public final List A01;
    public final Map A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0HL(List list, Map map) {
        super("BloksSurface_process_async_actions");
        C16700pc.A0E(map, 2);
        this.A01 = list;
        this.A02 = map;
    }
}
