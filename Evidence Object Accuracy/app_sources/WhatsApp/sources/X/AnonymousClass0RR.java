package X;

import android.util.SparseArray;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0RR  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0RR {
    public static final SparseArray A00 = new SparseArray();
    public static final Object A01 = new Object();
    public static final AtomicInteger A02 = new AtomicInteger(0);

    public static Object A00(Class cls, Integer num) {
        try {
            synchronized (A01) {
                SparseArray sparseArray = A00;
                int intValue = num.intValue();
                if (sparseArray.indexOfKey(intValue) < 0) {
                    return null;
                }
                Object cast = cls.cast(sparseArray.get(intValue));
                sparseArray.delete(intValue);
                return cast;
            }
        } catch (ClassCastException e) {
            C28691Op.A01("BloksDataStorage", "Casting error when retrieving data", e);
            return null;
        }
    }
}
