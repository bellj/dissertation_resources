package X;

import java.util.Arrays;

/* renamed from: X.4wY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106954wY implements AnonymousClass5WY {
    public final int A00;
    public final long A01;
    public final int[] A02;
    public final long[] A03;
    public final long[] A04;
    public final long[] A05;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public C106954wY(int[] iArr, long[] jArr, long[] jArr2, long[] jArr3) {
        this.A02 = iArr;
        this.A04 = jArr;
        this.A03 = jArr2;
        this.A05 = jArr3;
        int length = iArr.length;
        this.A00 = length;
        if (length > 0) {
            int i = length - 1;
            this.A01 = jArr2[i] + jArr3[i];
        }
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A01;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        long[] jArr = this.A05;
        int A06 = AnonymousClass3JZ.A06(jArr, j, true);
        long j2 = jArr[A06];
        long[] jArr2 = this.A04;
        C94324bc r4 = new C94324bc(j2, jArr2[A06]);
        if (r4.A01 >= j || A06 == this.A00 - 1) {
            return new C92684Xa(r4, r4);
        }
        int i = A06 + 1;
        return C92684Xa.A00(r4, jArr[i], jArr2[i]);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("ChunkIndex(length=");
        A0k.append(this.A00);
        A0k.append(", sizes=");
        A0k.append(Arrays.toString(this.A02));
        A0k.append(", offsets=");
        A0k.append(Arrays.toString(this.A04));
        A0k.append(", timeUs=");
        A0k.append(Arrays.toString(this.A05));
        A0k.append(", durationsUs=");
        A0k.append(Arrays.toString(this.A03));
        return C12960it.A0d(")", A0k);
    }
}
