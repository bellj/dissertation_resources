package X;

/* renamed from: X.43p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C856443p extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;

    public C856443p() {
        super(2300, new AnonymousClass00E(1, 20, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(11, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(12, this.A02);
        r3.Abe(9, this.A03);
        r3.Abe(1, this.A04);
        r3.Abe(7, this.A05);
        r3.Abe(8, this.A06);
        r3.Abe(5, this.A07);
        r3.Abe(10, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMdAppStateSyncDaily {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "crossIndexConflictCount", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "invalidActionCount", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "keyRotationRemoveCount", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "missingKeyCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mutationCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "storedMutationCount", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "unsetActionCount", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "unsupportedActionCount", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "uploadConflictCount", this.A08);
        return C12960it.A0d("}", A0k);
    }
}
