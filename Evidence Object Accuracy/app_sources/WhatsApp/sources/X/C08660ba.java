package X;

/* renamed from: X.0ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C08660ba implements AbstractC12230ha {
    @Override // X.AbstractC12230ha
    public void AaW(AnonymousClass0SW r7, Throwable th) {
        Object[] objArr = {Integer.valueOf(System.identityHashCode(this)), Integer.valueOf(System.identityHashCode(r7)), r7.A00().getClass().getName()};
        AbstractC12710iN r2 = AnonymousClass0UN.A00;
        if (r2.AJi(5)) {
            r2.Ag0(C08870bz.class.getSimpleName(), String.format(null, "Finalized without closing: %x %x (type = %s)", objArr));
        }
    }
}
