package X;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.39b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C629039b extends C124405pS {
    public AbstractC28681Oo A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C629039b(AnonymousClass018 r2, WaBloksActivity waBloksActivity) {
        super(r2, waBloksActivity);
        C16700pc.A0E(r2, 1);
    }

    @Override // X.AbstractC119405dv
    public void A01() {
        AbstractC28681Oo r1 = this.A00;
        if (r1 != null) {
            AnonymousClass1AI.A08(this.A04.AHe(), r1);
        }
    }

    @Override // X.AbstractC119405dv
    public boolean A02() {
        return C12960it.A1W(this.A00);
    }

    @Override // X.C124405pS, X.AbstractC119405dv
    public void A03(Intent intent, Bundle bundle) {
        super.A03(intent, bundle);
        A00().A0I(this.A01);
    }

    @Override // X.C124405pS, X.AbstractC119405dv
    public void A04(AbstractC115815Ta r4) {
        super.A04(r4);
        AnonymousClass3BN r2 = new AnonymousClass3BN(r4.AAL().A0F(40));
        this.A00 = null;
        if (r2.A00 != null) {
            this.A00 = new AbstractC28681Oo() { // from class: X.54K
                @Override // X.AbstractC28681Oo
                public final AbstractC14200l1 AAN() {
                    return AnonymousClass3BN.this.A00;
                }
            };
        }
        String A0I = r4.AAL().A0I(36);
        this.A01 = A0I;
        if (AnonymousClass1US.A0C(A0I)) {
            this.A01 = r2.A02;
        }
        A00().A0I(this.A01);
    }

    @Override // X.AbstractC119405dv, X.C28791Pa, android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        C16700pc.A0E(activity, 0);
        super.onActivityCreated(activity, bundle);
        C41691tw.A02(activity, R.color.primary);
        WaBloksActivity waBloksActivity = this.A04;
        Toolbar toolbar = (Toolbar) C16700pc.A00(waBloksActivity, R.id.wabloks_screen_toolbar);
        AnonymousClass2GF A00 = AnonymousClass2GF.A00(waBloksActivity, this.A03, R.drawable.ic_back);
        A00.setColorFilter(AnonymousClass00T.A00(activity, R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(A00);
        toolbar.setTitleTextColor(AnonymousClass00T.A00(activity, R.color.screen_title_text));
        C12970iu.A18(activity, toolbar, R.color.primary);
    }
}
