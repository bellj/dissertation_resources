package X;

import java.util.List;

/* renamed from: X.5bv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118505bv extends AnonymousClass0Q0 {
    public final List A00;
    public final List A01;

    public C118505bv(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        List list = this.A01;
        boolean z = ((C129025x3) list.get(i)).A00;
        List list2 = this.A00;
        return z == ((C129025x3) list2.get(i2)).A00 && ((C129025x3) list.get(i)).A01 == ((C129025x3) list2.get(i2)).A01 && ((C129025x3) list.get(i)).A02 == ((C129025x3) list2.get(i2)).A02;
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        C30921Zi r1 = ((C129025x3) this.A01.get(i)).A03;
        C30921Zi r2 = ((C129025x3) this.A00.get(i2)).A03;
        if (r1 == null) {
            if (r2 == null) {
                return true;
            }
        } else if (r2 != null) {
            return r1.A0F.equals(r2.A0F);
        }
        return false;
    }
}
