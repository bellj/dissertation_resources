package X;

/* renamed from: X.2Af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47332Af extends AnonymousClass2AX {
    public final C44641zJ A00;
    public final byte[] A01;
    public final byte[] A02;

    public C47332Af(C33241dg r7, String str, String str2, byte[] bArr, byte[] bArr2) {
        this.A02 = bArr;
        this.A01 = bArr2;
        C44621zH r3 = (C44621zH) C44631zI.A0e.A0T();
        if (str != null) {
            r3.A03();
            C44631zI r1 = (C44631zI) r3.A00;
            r1.A01 |= 1;
            r1.A04 = str;
        }
        if (str2 != null) {
            String substring = str2.substring(str2.length() - 2);
            r3.A03();
            C44631zI r12 = (C44631zI) r3.A00;
            r12.A01 |= 4;
            r12.A06 = substring;
        }
        if (r7 != null) {
            r7.A03(r3);
        }
        C82503vj r5 = (C82503vj) C44641zJ.A05.A0T();
        EnumC87184An r2 = EnumC87184An.A01;
        r5.A03();
        C44641zJ r13 = (C44641zJ) r5.A00;
        r13.A00 |= 1;
        r13.A01 = r2.value;
        AnonymousClass1G4 A0T = C56912mA.A02.A0T();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
        A0T.A03();
        C56912mA r14 = (C56912mA) A0T.A00;
        r14.A00 |= 1;
        r14.A01 = A01;
        r5.A03();
        C44641zJ r15 = (C44641zJ) r5.A00;
        r15.A03 = (C56912mA) A0T.A02();
        r15.A00 |= 4;
        r5.A05(r3);
        this.A00 = (C44641zJ) r5.A02();
    }
}
