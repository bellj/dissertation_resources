package X;

import android.hardware.camera2.CameraAccessException;
import java.util.concurrent.Callable;

/* renamed from: X.6Kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135936Kj implements Callable {
    public final /* synthetic */ AnonymousClass61Q A00;
    public final /* synthetic */ boolean A01;
    public final /* synthetic */ boolean A02;

    public CallableC135936Kj(AnonymousClass61Q r1, boolean z, boolean z2) {
        this.A00 = r1;
        this.A01 = z;
        this.A02 = z2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        try {
            this.A00.A0F(this.A01, this.A02);
            return null;
        } catch (CameraAccessException | IllegalArgumentException unused) {
            return null;
        } catch (Exception e) {
            throw new AnonymousClass6L0(C12960it.A0d(e.getMessage(), C12960it.A0k("Could not start preview: ")));
        }
    }
}
