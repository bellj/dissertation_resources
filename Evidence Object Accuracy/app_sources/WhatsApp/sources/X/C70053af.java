package X;

import com.whatsapp.userban.ui.viewmodel.BanAppealViewModel;

/* renamed from: X.3af  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70053af implements AnonymousClass23a {
    public final /* synthetic */ BanAppealViewModel A00;

    public C70053af(BanAppealViewModel banAppealViewModel) {
        this.A00 = banAppealViewModel;
    }

    @Override // X.AnonymousClass23a
    public void AQB(Integer num) {
        BanAppealViewModel banAppealViewModel = this.A00;
        banAppealViewModel.A02.A0A(C12970iu.A0h());
        banAppealViewModel.A01.A0A(num);
    }

    @Override // X.AnonymousClass23a
    public void AX3(C457723b r5) {
        BanAppealViewModel banAppealViewModel = this.A00;
        int A04 = banAppealViewModel.A04(r5.A00, true);
        banAppealViewModel.A02.A0A(C12970iu.A0g());
        C12970iu.A1Q(banAppealViewModel.A0B, A04);
    }
}
