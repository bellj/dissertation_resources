package X;

/* renamed from: X.2Oz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50292Oz extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public String A05;
    public String A06;

    public C50292Oz() {
        super(3634, new AnonymousClass00E(1, 1, 1), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A04);
        r3.Abe(3, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(5, this.A05);
        r3.Abe(6, this.A03);
        r3.Abe(7, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamDirectorySearchRanking {");
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "actionOnBusiness", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "businessRankingPosition", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "finalRankingScore", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "proximity", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rankingResultId", this.A05);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "searchEndpoint", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "searchSessionId", this.A06);
        sb.append("}");
        return sb.toString();
    }
}
