package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.2GP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GP extends Handler {
    public final C21840y4 A00;
    public final C21820y2 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2GP(Looper looper, C21840y4 r2, C21820y2 r3) {
        super(looper);
        AnonymousClass009.A05(looper);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (!this.A01.A00) {
            this.A00.A01(true);
        }
    }
}
