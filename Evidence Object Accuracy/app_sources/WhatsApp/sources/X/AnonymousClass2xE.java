package X;

import com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker;

/* renamed from: X.2xE  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2xE extends AbstractActivityC36611kC {
    public boolean A00 = false;

    public AnonymousClass2xE() {
        ActivityC13830kP.A1P(this, 34);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            GroupCallParticipantPicker groupCallParticipantPicker = (GroupCallParticipantPicker) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, groupCallParticipantPicker);
            ActivityC13810kN.A10(A1M, groupCallParticipantPicker);
            ActivityC13770kJ.A0O(A1M, groupCallParticipantPicker, ActivityC13790kL.A0S(r3, A1M, groupCallParticipantPicker, ActivityC13790kL.A0Y(A1M, groupCallParticipantPicker)));
            ActivityC13770kJ.A0N(A1M, groupCallParticipantPicker);
            groupCallParticipantPicker.A00 = (AnonymousClass19Z) A1M.A2o.get();
        }
    }
}
