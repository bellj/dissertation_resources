package X;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.preference.PreferenceFragmentCompat;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0F3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0F3 extends AbstractC018308n {
    public int A00;
    public Drawable A01;
    public boolean A02 = true;
    public final /* synthetic */ PreferenceFragmentCompat A03;

    public AnonymousClass0F3(PreferenceFragmentCompat preferenceFragmentCompat) {
        this.A03 = preferenceFragmentCompat;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r4, RecyclerView recyclerView) {
        if (A03(view, recyclerView)) {
            rect.bottom = this.A00;
        }
    }

    @Override // X.AbstractC018308n
    public void A02(Canvas canvas, C05480Ps r9, RecyclerView recyclerView) {
        if (this.A01 != null) {
            int childCount = recyclerView.getChildCount();
            int width = recyclerView.getWidth();
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                if (A03(childAt, recyclerView)) {
                    int y = ((int) childAt.getY()) + childAt.getHeight();
                    this.A01.setBounds(0, y, width, this.A00 + y);
                    this.A01.draw(canvas);
                }
            }
        }
    }

    public final boolean A03(View view, RecyclerView recyclerView) {
        AnonymousClass03U A0E = recyclerView.A0E(view);
        if (!(A0E instanceof AnonymousClass0FF) || !((AnonymousClass0FF) A0E).A01) {
            return false;
        }
        boolean z = this.A02;
        int indexOfChild = recyclerView.indexOfChild(view);
        if (indexOfChild >= recyclerView.getChildCount() - 1) {
            return z;
        }
        AnonymousClass03U A0E2 = recyclerView.A0E(recyclerView.getChildAt(indexOfChild + 1));
        if (!(A0E2 instanceof AnonymousClass0FF) || !((AnonymousClass0FF) A0E2).A00) {
            return false;
        }
        return true;
    }
}
