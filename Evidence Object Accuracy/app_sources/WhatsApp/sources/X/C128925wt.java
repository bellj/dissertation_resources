package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.5wt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128925wt {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;
    public final /* synthetic */ String A02;

    public /* synthetic */ C128925wt(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, String str) {
        this.A01 = brazilPayBloksActivity;
        this.A02 = str;
        this.A00 = r1;
    }

    public final void A00(C452120p r12) {
        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
        String str = this.A02;
        AnonymousClass3FE r4 = this.A00;
        if (r12 != null) {
            int i = r12.A01;
            HashMap A11 = C12970iu.A11();
            A11.put("remaining_retries", String.valueOf(i));
            C117295Zj.A1D(r12, A11);
            if (i >= 0) {
                C38051nR A00 = ((AbstractActivityC121705jc) brazilPayBloksActivity).A0G.A00();
                AnonymousClass67Z r5 = new AnonymousClass5U6(i) { // from class: X.67Z
                    public final /* synthetic */ int A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // X.AnonymousClass5U6
                    public final void AfI(AbstractC28901Pl r3) {
                        int i2 = this.A00;
                        AbstractC30871Zd r0 = (AbstractC30871Zd) r3.A08;
                        if (r0 != null) {
                            r0.A04 = i2;
                        }
                    }
                };
                C1326467r r6 = new AbstractC451720l(A11) { // from class: X.67r
                    public final /* synthetic */ Map A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AbstractC451720l
                    public final void AM7(List list) {
                        AnonymousClass3FE.this.A01("on_failure", this.A01);
                    }
                };
                AbstractC14440lR r9 = A00.A03;
                C12960it.A1E(new C61312zp(r5, r6, A00.A01, A00.A02, r9, str), r9);
                return;
            }
            r4.A01("on_failure", A11);
            return;
        }
        C117315Zl.A0T(r4);
    }
}
