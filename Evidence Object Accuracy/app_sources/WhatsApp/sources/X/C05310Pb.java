package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Pb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05310Pb {
    public List A00 = null;

    public void A00(AnonymousClass0OU r4) {
        if (this.A00 == null) {
            this.A00 = new ArrayList();
        }
        int i = 0;
        while (true) {
            int size = this.A00.size();
            List list = this.A00;
            if (i >= size) {
                list.add(r4);
                return;
            } else if (((AnonymousClass0OU) list.get(i)).A00.A00 > r4.A00.A00) {
                this.A00.add(i, r4);
                return;
            } else {
                i++;
            }
        }
    }

    public void A01(C05310Pb r3) {
        List list = r3.A00;
        if (list != null) {
            if (this.A00 == null) {
                this.A00 = new ArrayList(list.size());
            }
            for (AnonymousClass0OU r0 : r3.A00) {
                A00(r0);
            }
        }
    }

    public String toString() {
        List<Object> list = this.A00;
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Object obj : list) {
            sb.append(obj.toString());
            sb.append('\n');
        }
        return sb.toString();
    }
}
