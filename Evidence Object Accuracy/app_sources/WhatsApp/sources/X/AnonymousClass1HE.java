package X;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1HE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HE {
    public final HashMap A00 = new HashMap();
    public final HashMap A01 = new HashMap();
    public final HashMap A02 = new HashMap();
    public final AtomicLong A03 = new AtomicLong();
    public final AtomicLong A04 = new AtomicLong();

    public static String A00(Runnable runnable) {
        StringBuilder sb = new StringBuilder("wa_");
        sb.append(runnable.hashCode());
        return sb.toString();
    }

    public synchronized AnonymousClass1IA A01(String str) {
        return (AnonymousClass1IA) this.A01.get(str);
    }

    public synchronized AnonymousClass1IG A02(String str) {
        AnonymousClass1IG r0;
        r0 = (AnonymousClass1IG) this.A00.get(str);
        if (r0 == null) {
            AnonymousClass1IA r02 = (AnonymousClass1IA) this.A01.get(str);
            if (r02 != null) {
                r0 = A03(r02).A00;
            } else {
                r0 = AnonymousClass1IG.A01;
            }
        }
        return r0;
    }

    public final synchronized AnonymousClass1IH A03(AnonymousClass1IA r3) {
        AnonymousClass1IH r0;
        r0 = (AnonymousClass1IH) this.A02.get(r3);
        if (r0 == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("ThreadPoolExecutor ");
            sb.append(r3.toString());
            sb.append(" must be added using addThreadPoolExecutor");
            throw new IllegalStateException(sb.toString());
        }
        return r0;
    }
}
