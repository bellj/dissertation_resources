package X;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.2hL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C54822hL extends AbstractC05270Ox {
    public int A00;
    public int A01 = 0;
    public int A02 = 0;
    public final /* synthetic */ AnonymousClass1KW A03;

    public /* synthetic */ C54822hL(Context context, AnonymousClass1KW r4) {
        this.A03 = r4;
        this.A00 = context.getResources().getDimensionPixelSize(R.dimen.shape_picker_search_header_size);
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        int i2 = this.A02;
        if (i2 == 0 && i != i2) {
            this.A03.A0M.A0A.clearFocus();
        }
        this.A02 = i;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        int i3 = this.A01 + i2;
        this.A01 = i3;
        this.A03.A08.setAlpha(Math.min(1.0f, ((float) i3) / ((float) this.A00)));
    }
}
