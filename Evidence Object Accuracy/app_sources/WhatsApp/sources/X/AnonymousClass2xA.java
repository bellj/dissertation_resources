package X;

import java.util.Iterator;
import java.util.List;

/* renamed from: X.2xA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xA extends AnonymousClass36m {
    public final C15450nH A00;
    public final C15600nX A01;
    public final C20710wC A02;

    public AnonymousClass2xA(C15450nH r5, C15610nY r6, AbstractActivityC36611kC r7, AnonymousClass018 r8, C15600nX r9, C20710wC r10, List list) {
        super(r6, r7, r8, list);
        this.A00 = r5;
        this.A02 = r10;
        this.A01 = r9;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            this.A03.add(C12970iu.A0a(it).A0B(AbstractC14640lm.class));
        }
    }
}
