package X;

import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.1V4  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1V4 {
    public int A00 = -1;
    public long A01;
    public final int A02;
    public final long A03;
    public final long A04;
    public final AbstractC15710nm A05;
    public final C14830m7 A06;
    public final C16120oU A07;
    public final C17230qT A08;
    public final Integer A09;
    public final Integer A0A;
    public final String A0B;
    public final Map A0C = new LinkedHashMap();

    public AnonymousClass1V4(AbstractC15710nm r4, C14830m7 r5, C16120oU r6, C17230qT r7, Integer num, String str, int i, long j, long j2) {
        this.A06 = r5;
        this.A05 = r4;
        this.A07 = r6;
        this.A08 = r7;
        this.A0B = str;
        this.A03 = j;
        this.A02 = i;
        this.A00 = 1;
        this.A01 = j2;
        this.A04 = j2;
        AbstractC16110oT A00 = A00(-1, 0);
        this.A09 = r6.A00(A00.samplingRate, A00.code, false);
        this.A0A = num;
    }

    public AbstractC16110oT A00(int i, long j) {
        if (this instanceof AnonymousClass2R7) {
            AnonymousClass2R7 r4 = (AnonymousClass2R7) this;
            AnonymousClass31A r3 = new AnonymousClass31A();
            r3.A03 = Long.valueOf(j);
            r3.A00 = Boolean.valueOf(r4.A02);
            Integer num = r4.A0A;
            if (num != null) {
                r3.A04 = Long.valueOf((long) num.intValue());
            }
            r3.A05 = Long.valueOf(r4.A00);
            r3.A06 = Long.valueOf(C28421Nd.A01(r4.A04, 0));
            r3.A02 = Integer.valueOf(i);
            r3.A07 = Long.valueOf(r4.A01);
            r3.A08 = r4.A05;
            r3.A01 = Integer.valueOf(r4.A03);
            return r3;
        } else if (this instanceof AnonymousClass2QR) {
            AnonymousClass2QR r32 = (AnonymousClass2QR) this;
            C614430j r2 = new C614430j();
            r2.A01 = Long.valueOf(j);
            Integer num2 = r32.A0A;
            if (num2 != null) {
                r2.A02 = Long.valueOf((long) num2.intValue());
            }
            r2.A00 = Integer.valueOf(i);
            r2.A04 = r32.A01;
            r2.A03 = r32.A00;
            return r2;
        } else if (!(this instanceof AnonymousClass2LM)) {
            C619333d r33 = (C619333d) this;
            AnonymousClass30X r22 = new AnonymousClass30X();
            r22.A02 = Long.valueOf(j);
            r22.A00 = Integer.valueOf(i);
            Integer num3 = r33.A0A;
            if (num3 != null) {
                r22.A03 = Long.valueOf((long) num3.intValue());
            }
            r22.A01 = Integer.valueOf(r33.A00);
            return r22;
        } else {
            AnonymousClass2LM r34 = (AnonymousClass2LM) this;
            AnonymousClass31E r23 = new AnonymousClass31E();
            r23.A00 = Boolean.valueOf(r34.A05);
            r23.A04 = Integer.valueOf(r34.A00);
            r23.A08 = Long.valueOf(j);
            r23.A01 = Boolean.valueOf(r34.A02);
            r23.A02 = Boolean.valueOf(r34.A04);
            Integer num4 = r34.A0A;
            if (num4 != null) {
                r23.A09 = Long.valueOf((long) num4.intValue());
            }
            r23.A03 = Boolean.valueOf(r34.A06);
            r23.A05 = Integer.valueOf(i);
            r23.A06 = Integer.valueOf(r34.A03);
            r23.A07 = Long.valueOf(r34.A01);
            return r23;
        }
    }

    public String A01() {
        if (this instanceof AnonymousClass2R7) {
            return "ReceiptStanza";
        }
        if (!(this instanceof AnonymousClass2QR)) {
            return !(this instanceof AnonymousClass2LM) ? "CallStanza" : "MessageStanza";
        }
        return "NotificationStanza";
    }

    public synchronized void A02(int i) {
        if (i != -1) {
            int i2 = this.A00;
            if (i != i2) {
                long uptimeMillis = SystemClock.uptimeMillis();
                A03(i2, uptimeMillis - this.A01);
                this.A00 = i;
                this.A01 = uptimeMillis;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid stage ");
        sb.append(this.A00);
        sb.append(" ");
        sb.append(this.A0B);
        String obj = sb.toString();
        AbstractC15710nm r2 = this.A05;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(A01());
        sb2.append("/failed new stage check");
        r2.AaV(sb2.toString(), obj, true);
    }

    public final void A03(int i, long j) {
        Integer num = this.A09;
        if (num != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(A01());
            sb.append("/onStageComplete stage = ");
            sb.append(i);
            sb.append("; duration = ");
            sb.append(j);
            sb.append("; ");
            sb.append(this);
            Log.i(sb.toString());
            Map map = this.A0C;
            map.put(Integer.valueOf(i), Long.valueOf(j));
            if (i == 0) {
                for (Map.Entry entry : map.entrySet()) {
                    this.A07.A08(A00(((Number) entry.getKey()).intValue(), ((Number) entry.getValue()).longValue()), num.intValue());
                }
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("stanzaId = ");
        sb.append(this.A0B);
        sb.append("; loggableStanzaType = ");
        sb.append(this.A02);
        sb.append("; currentStage = ");
        sb.append(this.A00);
        sb.append("; offlineCount = ");
        sb.append(this.A0A);
        return sb.toString();
    }
}
