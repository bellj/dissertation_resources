package X;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.36k  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36k extends C52252aV {
    public Paint.FontMetricsInt A00;
    public C73013fX A01;
    public boolean A02;
    public final int A03;
    public final int A04;
    public final CharSequence A05;

    public AnonymousClass36k(Context context, Paint.FontMetricsInt fontMetricsInt, Drawable drawable, CharSequence charSequence) {
        super(drawable);
        this.A05 = charSequence;
        this.A04 = AnonymousClass00T.A00(context, R.color.link_color);
        this.A03 = (int) C12960it.A01(context);
        this.A00 = fontMetricsInt;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        if (r21.A02 == false) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        r4 = r21.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        if (r4 != null) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        r4 = new X.C73013fX(r21.A04, r30);
        r21.A01 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        r3 = (float) (A03().getBounds().right + r21.A03);
        r2 = (((float) r28) + r4.A00) + (r4.A01 / 2.0f);
        r22.drawLine(r26, r2, r26 + r3, r2, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0063, code lost:
        super.draw(r22, r23, r24, r25, r26, r27, r28, r29, r30);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006c, code lost:
        return;
     */
    @Override // X.C52252aV, android.text.style.DynamicDrawableSpan, android.text.style.ReplacementSpan
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r22, java.lang.CharSequence r23, int r24, int r25, float r26, int r27, int r28, int r29, android.graphics.Paint r30) {
        /*
            r21 = this;
            r3 = 0
        L_0x0001:
            r5 = r21
            java.lang.CharSequence r1 = r5.A05
            int r0 = r1.length()
            r8 = r24
            r7 = r23
            if (r3 >= r0) goto L_0x0024
            int r2 = r24 + r3
            int r0 = r7.length()
            if (r2 >= r0) goto L_0x0024
            char r1 = r1.charAt(r3)
            char r0 = r7.charAt(r2)
            if (r1 != r0) goto L_0x006c
            int r3 = r3 + 1
            goto L_0x0001
        L_0x0024:
            boolean r0 = r5.A02
            r10 = r26
            r6 = r22
            r14 = r30
            r12 = r28
            if (r0 == 0) goto L_0x0063
            X.3fX r4 = r5.A01
            if (r4 != 0) goto L_0x003d
            int r0 = r5.A04
            X.3fX r4 = new X.3fX
            r4.<init>(r0, r14)
            r5.A01 = r4
        L_0x003d:
            android.graphics.drawable.Drawable r0 = r5.A03()
            android.graphics.Rect r0 = r0.getBounds()
            int r1 = r0.right
            int r0 = r5.A03
            int r1 = r1 + r0
            float r3 = (float) r1
            float r2 = (float) r12
            float r0 = r4.A00
            float r2 = r2 + r0
            float r1 = r4.A01
            r0 = 1073741824(0x40000000, float:2.0)
            float r1 = r1 / r0
            float r2 = r2 + r1
            float r18 = r26 + r3
            r19 = r2
            r15 = r6
            r16 = r10
            r17 = r2
            r20 = r4
            r15.drawLine(r16, r17, r18, r19, r20)
        L_0x0063:
            r9 = r25
            r13 = r29
            r11 = r27
            super.draw(r6, r7, r8, r9, r10, r11, r12, r13, r14)
        L_0x006c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass36k.draw(android.graphics.Canvas, java.lang.CharSequence, int, int, float, int, int, int, android.graphics.Paint):void");
    }

    @Override // android.text.style.DynamicDrawableSpan, android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        int i3;
        Rect bounds = A03().getBounds();
        if (fontMetricsInt != null) {
            int height = bounds.height();
            Paint.FontMetricsInt fontMetricsInt2 = this.A00;
            int i4 = fontMetricsInt2.descent;
            int i5 = fontMetricsInt2.ascent;
            int max = Math.max(0, (height - i4) + i5);
            fontMetricsInt.ascent = i5 - max;
            fontMetricsInt.descent = i4 + max;
            fontMetricsInt.top = fontMetricsInt2.top - max;
            fontMetricsInt.bottom = fontMetricsInt2.bottom + max;
        }
        int i6 = 0;
        while (true) {
            CharSequence charSequence2 = this.A05;
            if (i6 >= charSequence2.length() || (i3 = i + i6) >= charSequence.length()) {
                break;
            } else if (charSequence2.charAt(i6) != charSequence.charAt(i3)) {
                return 0;
            } else {
                i6++;
            }
        }
        return bounds.right + this.A03;
    }
}
