package X;

/* renamed from: X.3B3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3B3 {
    public static final AnonymousClass1NC A00;

    static {
        AnonymousClass1NC r2 = new AnonymousClass1NC(10);
        A00 = r2;
        r2.A03("BJ", new String[]{"fr-BJ", "ha-NG"});
        r2.A03("CM", new String[]{"en-CM", "fr-CM", "ha-NG"});
        r2.A03("TD", new String[]{"fr-TD", "ar-TD", "ha-NG"});
        r2.A03("ET", new String[]{"am-ET", "en-GB", "om-ET"});
        r2.A03("GH", new String[]{"en-GH", "ha-GH"});
        r2.A03("IL", new String[]{"iw-IL", "ar-IL", "en-IL", "ru-RU", "am-ET"});
        r2.A03("KE", new String[]{"en-KE", "om-KE", "sw-KE"});
        r2.A03("NE", new String[]{"ar-TD", "fr-NE", "ha-NE"});
        r2.A03("NG", new String[]{"en-NG", "ha-NG"});
        r2.A03("SO", new String[]{"en-GB", "ar-SO", "om-ET"});
    }
}
