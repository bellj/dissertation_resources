package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3XH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XH implements AnonymousClass23E {
    public Context A00;
    public C616531g A01;
    public final int A02;
    public final Drawable A03;
    public final C006202y A04;
    public final AnonymousClass3X7 A05;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XH(C006202y r3, AnonymousClass3X7 r4, C616531g r5) {
        Context context = r5.getContext();
        this.A00 = context;
        this.A05 = r4;
        this.A01 = r5;
        this.A04 = r3;
        int A00 = AnonymousClass00T.A00(context, R.color.camera_thumb);
        this.A02 = A00;
        this.A03 = new ColorDrawable(A00);
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        C616531g r1 = this.A01;
        r1.setBackgroundColor(this.A02);
        r1.setImageDrawable(null);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        C616531g r4 = this.A01;
        Object tag = r4.getTag();
        AnonymousClass3X7 r3 = this.A05;
        if (tag == r3) {
            if (bitmap == MediaGalleryFragmentBase.A0U) {
                r4.setScaleType(ImageView.ScaleType.CENTER);
                r4.setBackgroundColor(this.A02);
                r4.setImageResource(R.drawable.ic_missing_thumbnail_picture);
            } else {
                C12990iw.A1E(r4);
                r4.setBackgroundResource(0);
                if (!z) {
                    Drawable[] drawableArr = new Drawable[2];
                    drawableArr[0] = this.A03;
                    C12960it.A15(r4, new BitmapDrawable(this.A00.getResources(), bitmap), drawableArr);
                } else {
                    r4.setImageBitmap(bitmap);
                }
            }
            this.A04.A08(r3.AH5(), bitmap);
        }
    }
}
