package X;

import com.whatsapp.profile.WebImagePicker;

/* renamed from: X.4rM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103814rM implements AnonymousClass07L {
    public final /* synthetic */ WebImagePicker A00;

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        return false;
    }

    public C103814rM(WebImagePicker webImagePicker) {
        this.A00 = webImagePicker;
    }

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        this.A00.A2h();
        return true;
    }
}
