package X;

import java.util.HashMap;

/* renamed from: X.1wU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43141wU implements AbstractC28771Oy {
    public final /* synthetic */ C22370yy A00;
    public final /* synthetic */ String A01;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public C43141wU(C22370yy r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        HashMap hashMap = this.A00.A0r;
        synchronized (hashMap) {
            hashMap.remove(this.A01);
        }
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r3, C28781Oz r4) {
        HashMap hashMap = this.A00.A0r;
        synchronized (hashMap) {
            hashMap.remove(this.A01);
        }
    }
}
