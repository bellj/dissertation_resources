package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4lm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100354lm implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new AnonymousClass1ZM(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1ZM[i];
    }
}
