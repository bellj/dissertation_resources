package X;

import android.view.View;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.whatsapp.R;

/* renamed from: X.3jV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C75223jV extends AnonymousClass03U {
    public final AppCompatRadioButton A00;

    public C75223jV(View view) {
        super(view);
        View findViewById = view.findViewById(R.id.reason);
        C16700pc.A0B(findViewById);
        this.A00 = (AppCompatRadioButton) findViewById;
    }
}
