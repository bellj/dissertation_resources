package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.4if  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98424if implements IInterface, AnonymousClass5Y2 {
    public final IBinder A00;

    public C98424if(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public IBinder asBinder() {
        return this.A00;
    }
}
