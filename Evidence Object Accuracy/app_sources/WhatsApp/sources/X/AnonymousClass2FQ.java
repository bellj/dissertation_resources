package X;

import java.io.File;

/* renamed from: X.2FQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2FQ {
    public final long A00;
    public final long A01;
    public final File A02;
    public final String A03;
    public final String A04;

    public AnonymousClass2FQ(File file, String str, String str2, long j, long j2) {
        this.A00 = j;
        this.A02 = file;
        this.A04 = str;
        this.A01 = j2;
        this.A03 = str2;
    }
}
