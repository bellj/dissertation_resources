package X;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.4YC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4YC {
    public static int A0E;
    public double A00 = 0.005d;
    public double A01;
    public double A02 = 0.005d;
    public double A03;
    public double A04 = 0.0d;
    public C93374a4 A05;
    public CopyOnWriteArraySet A06 = new CopyOnWriteArraySet();
    public boolean A07;
    public boolean A08 = true;
    public final C89814Lm A09 = new C89814Lm();
    public final C89814Lm A0A = new C89814Lm();
    public final C89814Lm A0B = new C89814Lm();
    public final C94254bV A0C;
    public final String A0D;

    public AnonymousClass4YC(C94254bV r4) {
        this.A0C = r4;
        StringBuilder A0k = C12960it.A0k("spring:");
        int i = A0E;
        A0E = i + 1;
        this.A0D = C12960it.A0f(A0k, i);
        this.A05 = C93374a4.A02;
    }

    public double A00() {
        return this.A09.A00;
    }

    public void A01(double d) {
        this.A03 = d;
        C89814Lm r3 = this.A09;
        r3.A00 = d;
        this.A0C.A02(this.A0D);
        Iterator it = this.A06.iterator();
        while (it.hasNext()) {
            ((AnonymousClass5VV) it.next()).AWE(this);
        }
        double d2 = r3.A00;
        this.A01 = d2;
        this.A0B.A00 = d2;
        r3.A01 = 0.0d;
    }

    public void A02(double d) {
        if (this.A01 != d || !A04()) {
            this.A03 = this.A09.A00;
            this.A01 = d;
            this.A0C.A02(this.A0D);
            Iterator it = this.A06.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    public void A03(AnonymousClass5VV r2) {
        if (r2 != null) {
            this.A06.add(r2);
            return;
        }
        throw C12970iu.A0f("newListener is required");
    }

    public boolean A04() {
        C89814Lm r5 = this.A09;
        if (Math.abs(r5.A01) <= this.A02) {
            return Math.abs(this.A01 - r5.A00) <= this.A00 || this.A05.A01 == 0.0d;
        }
        return false;
    }
}
