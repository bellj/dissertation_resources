package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.3DS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DS {
    public boolean A00 = false;
    public final AnonymousClass1A9 A01;
    public final C19990v2 A02;
    public final C15650ng A03;
    public final C15600nX A04;
    public final AnonymousClass134 A05;
    public final C242114q A06;
    public final C36051jF A07;
    public final C15860o1 A08;
    public final List A09 = C12960it.A0l();

    public AnonymousClass3DS(AnonymousClass1A9 r4, C19990v2 r5, C15650ng r6, C15600nX r7, AnonymousClass134 r8, C242114q r9, C36051jF r10, C15860o1 r11) {
        this.A02 = r5;
        this.A05 = r8;
        this.A03 = r6;
        this.A08 = r11;
        this.A06 = r9;
        this.A07 = r10;
        this.A04 = r7;
        this.A01 = r4;
        C48522Gp r1 = r10.A00;
        if (r1 != null) {
            C69203Yi r2 = new C69203Yi(this, r10);
            AnonymousClass009.A01();
            AnonymousClass009.A01();
            if (r1.A01) {
                r2.AM4(r1.A00);
                return;
            }
            List list = r1.A03;
            list.add(r2);
            Collections.sort(list, new C48552Gs());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.C15370n3 r11) {
        /*
        // Method dump skipped, instructions count: 217
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3DS.A00(X.0n3):void");
    }
}
