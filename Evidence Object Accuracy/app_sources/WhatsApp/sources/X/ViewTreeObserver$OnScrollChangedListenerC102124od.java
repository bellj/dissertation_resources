package X;

import android.view.ViewTreeObserver;
import com.whatsapp.registration.RegistrationScrollView;

/* renamed from: X.4od  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class ViewTreeObserver$OnScrollChangedListenerC102124od implements ViewTreeObserver.OnScrollChangedListener {
    public final /* synthetic */ RegistrationScrollView A00;

    public /* synthetic */ ViewTreeObserver$OnScrollChangedListenerC102124od(RegistrationScrollView registrationScrollView) {
        this.A00 = registrationScrollView;
    }

    @Override // android.view.ViewTreeObserver.OnScrollChangedListener
    public final void onScrollChanged() {
        RegistrationScrollView.A01(this.A00);
    }
}
