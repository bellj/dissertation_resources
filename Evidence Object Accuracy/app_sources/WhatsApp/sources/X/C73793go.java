package X;

import android.graphics.Outline;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewOutlineProvider;

/* renamed from: X.3go  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73793go extends ViewOutlineProvider {
    public final /* synthetic */ AnonymousClass2CF A00;

    public /* synthetic */ C73793go(AnonymousClass2CF r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        AnonymousClass2CF r2 = this.A00;
        outline.setAlpha(((float) r2.A0K) / 255.0f);
        Rect rect = new Rect();
        r2.A0Y.roundOut(rect);
        outline.setRoundRect(rect, (float) r2.A0J);
    }
}
