package X;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

/* renamed from: X.3w1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82683w1 extends AnonymousClass2k7 {
    @Override // X.AnonymousClass2k7
    public void A06(View view, C14260l7 r2, AnonymousClass28D r3, Object obj) {
    }

    @Override // X.AnonymousClass2k7
    public void A07(View view, C14260l7 r2, AnonymousClass28D r3, Object obj) {
    }

    public C82683w1(C14260l7 r1, AnonymousClass28D r2) {
        super(r1, r2);
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return new ProgressBar(context);
    }
}
