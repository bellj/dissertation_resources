package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import com.whatsapp.R;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.2aC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class HandlerC52072aC extends Handler {
    public final Context A00;
    public final /* synthetic */ AnonymousClass1KW A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HandlerC52072aC(Context context, Looper looper, AnonymousClass1KW r3) {
        super(looper);
        this.A01 = r3;
        AnonymousClass009.A05(looper);
        this.A00 = context;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        AbstractC454821u A8X;
        Object r1;
        AnonymousClass1KW r4 = this.A01;
        String A01 = AnonymousClass1KW.A01(message.getData());
        Context context = this.A00;
        ConcurrentHashMap concurrentHashMap = r4.A0d;
        Reference reference = (Reference) concurrentHashMap.get(A01);
        if (reference == null || (r1 = reference.get()) == null) {
            ConcurrentHashMap concurrentHashMap2 = r4.A0e;
            Reference reference2 = (Reference) concurrentHashMap2.get(A01);
            if (reference2 == null || (A8X = (AbstractC454821u) reference2.get()) == null) {
                AbstractC470728v r5 = (AbstractC470728v) r4.A0f.get(A01);
                if (r5 != null) {
                    A8X = r5.A8X(context, r4.A0B, true);
                    concurrentHashMap2.put(A01, new SoftReference(A8X));
                    if (A8X == null) {
                        return;
                    }
                } else {
                    return;
                }
            }
            String A0H = A8X.A0H(context);
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.shape_picker_new_shape_size);
            if (A8X.A0J()) {
                A8X.A09(r4.A01);
            }
            if (A8X.A0K()) {
                A8X.A0O(r4.A00);
            }
            Drawable A0F = A8X.A0F();
            if (A0F == null) {
                float f = (float) dimensionPixelSize;
                A8X.A0Q(C12980iv.A0K(), 0.0f, 0.0f, f, f);
                Bitmap createBitmap = Bitmap.createBitmap(dimensionPixelSize, dimensionPixelSize, Bitmap.Config.ARGB_8888);
                A8X.A0P(new Canvas(createBitmap));
                A0F = new BitmapDrawable(context.getResources(), createBitmap);
            }
            r1 = new C90414Nu(A0F, A0H);
        }
        concurrentHashMap.put(A01, new SoftReference(r1));
        View view = (View) message.obj;
        if (view != null) {
            view.setTag(r1);
            Message obtain = Message.obtain(r4.A0J, 0, 0, 0, view);
            obtain.setData(AnonymousClass1KW.A00(A01));
            obtain.sendToTarget();
        }
    }
}
