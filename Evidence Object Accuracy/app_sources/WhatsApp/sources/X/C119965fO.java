package X;

/* renamed from: X.5fO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119965fO extends AbstractC16110oT {
    public Boolean A00;
    public String A01;
    public String A02;

    public C119965fO() {
        super(2010, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamWaPaymentsGetStarted {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "paymentsContinueSelected", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "paymentsCountryCode", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "paymentsEventId", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
