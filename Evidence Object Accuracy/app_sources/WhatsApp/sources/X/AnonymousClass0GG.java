package X;

/* renamed from: X.0GG  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0GG extends AnonymousClass0SP {
    public final AbstractC11930h6 A00;
    public final AnonymousClass0JH A01;
    public final Object A02;
    public final String A03 = "SidecarAdapter";

    public AnonymousClass0GG(AbstractC11930h6 r2, AnonymousClass0JH r3, Object obj) {
        this.A02 = obj;
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0SP
    public AnonymousClass0SP A01(String str, AnonymousClass1J7 r8) {
        C16700pc.A0E(str, 0);
        Object obj = this.A02;
        if (((Boolean) r8.AJ4(obj)).booleanValue()) {
            return this;
        }
        return new AnonymousClass0GH(this.A00, this.A01, obj, this.A03, str);
    }

    @Override // X.AnonymousClass0SP
    public Object A02() {
        return this.A02;
    }
}
