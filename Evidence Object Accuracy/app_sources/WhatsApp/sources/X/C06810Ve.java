package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;
import java.util.ArrayList;

/* renamed from: X.0Ve  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06810Ve implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(16);
    public int A00;
    public String A01 = null;
    public ArrayList A02;
    public ArrayList A03;
    public ArrayList A04;
    public ArrayList A05 = new ArrayList();
    public ArrayList A06 = new ArrayList();
    public C06820Vf[] A07;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06810Ve() {
    }

    public C06810Ve(Parcel parcel) {
        this.A02 = parcel.createTypedArrayList(C06850Vi.CREATOR);
        this.A03 = parcel.createStringArrayList();
        this.A07 = (C06820Vf[]) parcel.createTypedArray(C06820Vf.CREATOR);
        this.A00 = parcel.readInt();
        this.A01 = parcel.readString();
        this.A05 = parcel.createStringArrayList();
        this.A06 = parcel.createTypedArrayList(Bundle.CREATOR);
        this.A04 = parcel.createTypedArrayList(C06790Vc.CREATOR);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.A02);
        parcel.writeStringList(this.A03);
        parcel.writeTypedArray(this.A07, i);
        parcel.writeInt(this.A00);
        parcel.writeString(this.A01);
        parcel.writeStringList(this.A05);
        parcel.writeTypedList(this.A06);
        parcel.writeTypedList(this.A04);
    }
}
