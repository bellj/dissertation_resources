package X;

import java.lang.reflect.Constructor;
import javax.crypto.BadPaddingException;

/* renamed from: X.5GR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GR implements AnonymousClass5XV {
    public static final Constructor A01;
    public AnonymousClass5XQ A00;

    public AnonymousClass5GR(AnonymousClass5XQ r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XV
    public int A97(byte[] bArr, int i) {
        try {
            return this.A00.A97(bArr, i);
        } catch (C114965Nt e) {
            Constructor constructor = A01;
            if (constructor != null) {
                BadPaddingException badPaddingException = null;
                try {
                    badPaddingException = (BadPaddingException) constructor.newInstance(e.getMessage());
                } catch (Exception unused) {
                }
                if (badPaddingException != null) {
                    throw badPaddingException;
                }
            }
            throw new BadPaddingException(e.getMessage());
        }
    }

    @Override // X.AnonymousClass5XV
    public String AAf() {
        AnonymousClass5XQ r1 = this.A00;
        return r1 instanceof AbstractC117285Zg ? ((AbstractC117285Zg) r1).AHO().AAf() : r1.AAf();
    }

    @Override // X.AnonymousClass5XV
    public int AEp(int i) {
        return this.A00.AEp(i);
    }

    @Override // X.AnonymousClass5XV
    public AnonymousClass5XE AHO() {
        AnonymousClass5XQ r1 = this.A00;
        if (r1 instanceof AbstractC117285Zg) {
            return ((AbstractC117285Zg) r1).AHO();
        }
        return null;
    }

    @Override // X.AnonymousClass5XV
    public int AHQ(int i) {
        return this.A00.AHQ(i);
    }

    @Override // X.AnonymousClass5XV
    public void AIf(AnonymousClass20L r2, boolean z) {
        this.A00.AIf(r2, z);
    }

    @Override // X.AnonymousClass5XV
    public int AZZ(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        return this.A00.AZZ(bArr, i, i2, bArr2, i3);
    }

    @Override // X.AnonymousClass5XV
    public void AfL(byte[] bArr, int i, int i2) {
        this.A00.AZX(bArr, i, i2);
    }

    @Override // X.AnonymousClass5XV
    public boolean AgA() {
        return false;
    }

    static {
        Class A00 = AnonymousClass1TA.A00(AnonymousClass5PQ.class, "javax.crypto.AEADBadTagException");
        Constructor constructor = null;
        if (A00 != null) {
            try {
                constructor = A00.getConstructor(String.class);
            } catch (Exception unused) {
                constructor = null;
            }
        }
        A01 = constructor;
    }
}
