package X;

import android.content.Context;
import com.whatsapp.registration.RegisterName;

/* renamed from: X.4qd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103364qd implements AbstractC009204q {
    public final /* synthetic */ RegisterName A00;

    public C103364qd(RegisterName registerName) {
        this.A00 = registerName;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
