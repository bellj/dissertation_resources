package X;

/* renamed from: X.4x3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107264x3 implements AbstractC116595Wb {
    public int A00;
    public int A01;
    public boolean A02;
    public boolean A03;
    public final AnonymousClass5VY A04;
    public final C95304dT A05 = C95304dT.A05(32);

    public C107264x3(AnonymousClass5VY r2) {
        this.A04 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r10.A03 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x009d, code lost:
        if (r10.A02 == false) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009f, code lost:
        r5 = r8.A02;
        r4 = 0;
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a3, code lost:
        if (r4 >= r9) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a5, code lost:
        r0 = X.AnonymousClass3JZ.A0B[((r0 >>> 24) ^ (r5[r4] & 255)) & 255] ^ (r0 << 8);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b8, code lost:
        if (r0 != 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ba, code lost:
        r8.A0R(r9 - 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c0, code lost:
        r8.A0R(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c3, code lost:
        r8.A0S(0);
        r10.A04.A7a(r8);
     */
    @Override // X.AbstractC116595Wb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7b(X.C95304dT r11, int r12) {
        /*
            r10 = this;
            r7 = 1
            r12 = r12 & r7
            r6 = 0
            if (r12 == 0) goto L_0x00cd
            r2 = 1
            int r0 = r11.A0C()
            int r1 = r11.A01
            int r1 = r1 + r0
        L_0x000d:
            boolean r0 = r10.A03
            if (r0 == 0) goto L_0x001a
            if (r2 == 0) goto L_0x0037
            r10.A03 = r6
            r11.A0S(r1)
        L_0x0018:
            r10.A00 = r6
        L_0x001a:
            int r1 = X.C95304dT.A00(r11)
            if (r1 <= 0) goto L_0x0037
            int r2 = r10.A00
            r4 = 3
            if (r2 >= r4) goto L_0x0085
            if (r2 != 0) goto L_0x0038
            int r1 = r11.A0C()
            int r0 = r11.A01
            int r0 = r0 - r7
            r11.A0S(r0)
            r0 = 255(0xff, float:3.57E-43)
            if (r1 != r0) goto L_0x0038
        L_0x0035:
            r10.A03 = r7
        L_0x0037:
            return
        L_0x0038:
            int r1 = X.C95304dT.A00(r11)
            int r2 = r10.A00
            int r0 = 3 - r2
            int r1 = java.lang.Math.min(r1, r0)
            X.4dT r3 = r10.A05
            byte[] r0 = r3.A02
            r11.A0V(r0, r2, r1)
            int r0 = r10.A00
            int r0 = r0 + r1
            r10.A00 = r0
            if (r0 != r4) goto L_0x001a
            r3.A0S(r6)
            r3.A0R(r4)
            int r2 = X.C95304dT.A01(r3, r7)
            int r1 = r3.A0C()
            r0 = r2 & 128(0x80, float:1.794E-43)
            boolean r0 = X.C12960it.A1S(r0)
            r10.A02 = r0
            r0 = r2 & 15
            int r2 = r0 << 8
            r2 = r2 | r1
            int r2 = r2 + r4
            r10.A01 = r2
            byte[] r0 = r3.A02
            int r0 = r0.length
            if (r0 >= r2) goto L_0x001a
            r1 = 4098(0x1002, float:5.743E-42)
            int r0 = r0 << 1
            int r0 = java.lang.Math.max(r2, r0)
            int r0 = java.lang.Math.min(r1, r0)
            r3.A0P(r0)
            goto L_0x001a
        L_0x0085:
            int r0 = r10.A01
            int r1 = X.C72463ee.A02(r0, r2, r1)
            X.4dT r8 = r10.A05
            byte[] r0 = r8.A02
            r11.A0V(r0, r2, r1)
            int r0 = r10.A00
            int r0 = r0 + r1
            r10.A00 = r0
            int r9 = r10.A01
            if (r0 != r9) goto L_0x001a
            boolean r0 = r10.A02
            if (r0 == 0) goto L_0x00c0
            byte[] r5 = r8.A02
            r4 = 0
            r0 = -1
        L_0x00a3:
            if (r4 >= r9) goto L_0x00b8
            int r3 = r0 << 8
            int[] r2 = X.AnonymousClass3JZ.A0B
            int r1 = r0 >>> 24
            byte r0 = r5[r4]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r1 = r1 ^ r0
            r0 = r1 & 255(0xff, float:3.57E-43)
            r0 = r2[r0]
            r0 = r0 ^ r3
            int r4 = r4 + 1
            goto L_0x00a3
        L_0x00b8:
            if (r0 != 0) goto L_0x0035
            int r0 = r9 + -4
            r8.A0R(r0)
            goto L_0x00c3
        L_0x00c0:
            r8.A0R(r9)
        L_0x00c3:
            r8.A0S(r6)
            X.5VY r0 = r10.A04
            r0.A7a(r8)
            goto L_0x0018
        L_0x00cd:
            r2 = 0
            r1 = -1
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107264x3.A7b(X.4dT, int):void");
    }

    @Override // X.AbstractC116595Wb
    public void AIe(AbstractC14070ko r2, C92824Xo r3, AnonymousClass4YB r4) {
        this.A04.AIe(r2, r3, r4);
        this.A03 = true;
    }

    @Override // X.AbstractC116595Wb
    public void AbP() {
        this.A03 = true;
    }
}
