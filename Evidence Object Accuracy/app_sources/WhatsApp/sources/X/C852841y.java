package X;

import com.whatsapp.calling.callhistory.group.GroupCallLogActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.41y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852841y extends C27131Gd {
    public final /* synthetic */ GroupCallLogActivity A00;

    public C852841y(GroupCallLogActivity groupCallLogActivity) {
        this.A00 = groupCallLogActivity;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r2) {
        C54482gn.A00(this.A00.A00, r2);
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        C54482gn.A00(this.A00.A00, userJid);
    }
}
