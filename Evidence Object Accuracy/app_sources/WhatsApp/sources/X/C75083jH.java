package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3jH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75083jH extends AbstractC05270Ox {
    public int A00 = 0;
    public final /* synthetic */ int A01 = 3;
    public final /* synthetic */ AnonymousClass185 A02;

    public C75083jH(AnonymousClass185 r2) {
        this.A02 = r2;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        if (i == 0) {
            this.A02.A00();
        } else if (i == 1 && this.A00 == 0) {
            this.A02.A01(this.A01);
        }
        this.A00 = i;
    }
}
