package X;

/* renamed from: X.3eZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC72423eZ {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K;
    public static final AnonymousClass1TK A0L;
    public static final AnonymousClass1TK A0M;
    public static final AnonymousClass1TK A0N;
    public static final AnonymousClass1TK A0O;
    public static final AnonymousClass1TK A0P;
    public static final AnonymousClass1TK A0Q;
    public static final AnonymousClass1TK A0R;

    static {
        AnonymousClass1TK r6 = new AnonymousClass1TK("1.3.36.3");
        A0P = r6;
        A0K = AnonymousClass1TL.A02("2.1", r6);
        A0J = AnonymousClass1TL.A02("2.2", r6);
        A0L = AnonymousClass1TL.A02("2.3", r6);
        AnonymousClass1TK A022 = AnonymousClass1TL.A02("3.1", r6);
        A0Q = A022;
        A0N = AnonymousClass1TL.A02("2", A022);
        A0M = AnonymousClass1TL.A02("3", A022);
        A0O = AnonymousClass1TL.A02("4", A022);
        AnonymousClass1TK A023 = AnonymousClass1TL.A02("3.2", r6);
        A0E = A023;
        A0G = AnonymousClass1TL.A02("1", A023);
        A0F = AnonymousClass1TL.A02("2", A023);
        AnonymousClass1TK A024 = AnonymousClass1TL.A02("3.2.8", r6);
        A0H = A024;
        AnonymousClass1TK A025 = AnonymousClass1TL.A02("1", A024);
        A0I = A025;
        AnonymousClass1TK A026 = AnonymousClass1TL.A02("1", A025);
        A0R = A026;
        A00 = AnonymousClass1TL.A02("1", A026);
        A01 = AnonymousClass1TL.A02("2", A026);
        A02 = AnonymousClass1TL.A02("3", A026);
        A03 = AnonymousClass1TL.A02("4", A026);
        A04 = AnonymousClass1TL.A02("5", A026);
        A05 = AnonymousClass1TL.A02("6", A026);
        A06 = AnonymousClass1TL.A02("7", A026);
        A07 = AnonymousClass1TL.A02("8", A026);
        A08 = AnonymousClass1TL.A02("9", A026);
        A09 = AnonymousClass1TL.A02("10", A026);
        A0A = AnonymousClass1TL.A02("11", A026);
        A0B = AnonymousClass1TL.A02("12", A026);
        A0C = AnonymousClass1TL.A02("13", A026);
        A0D = AnonymousClass1TL.A02("14", A026);
    }
}
