package X;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.util.Log;

/* renamed from: X.1ww */
/* loaded from: classes2.dex */
public class C43391ww implements AbstractC43401wx {
    public Handler A00;
    public C51872Zl A01;
    public final AnonymousClass01d A02;
    public final C15410nB A03;
    public final C19890uq A04;

    public C43391ww(AnonymousClass01d r1, C15410nB r2, C19890uq r3) {
        this.A02 = r1;
        this.A04 = r3;
        this.A03 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        if (r5 != false) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void A00(X.C43391ww r7) {
        /*
            X.2Zl r0 = r7.A01
            X.AnonymousClass009.A05(r0)
            android.net.Network r1 = r0.A00
            if (r1 == 0) goto L_0x0044
            r6 = 1
            long r2 = r1.getNetworkHandle()
        L_0x000e:
            X.01d r0 = r7.A02
            android.net.ConnectivityManager r0 = r0.A0H()
            r5 = 0
            if (r0 == 0) goto L_0x002f
            if (r1 == 0) goto L_0x002f
            android.net.NetworkCapabilities r1 = r0.getNetworkCapabilities(r1)
            r0 = 1
            if (r1 == 0) goto L_0x002f
            boolean r0 = r1.hasTransport(r0)
            if (r0 == 0) goto L_0x002f
            r0 = 17
            boolean r0 = r1.hasCapability(r0)
            if (r0 == 0) goto L_0x002f
            r5 = 1
        L_0x002f:
            r4 = 1
            X.0nB r0 = r7.A03
            r0.A00()
            X.0uq r1 = r7.A04
            if (r6 == 0) goto L_0x003c
            r0 = 1
            if (r5 == 0) goto L_0x003d
        L_0x003c:
            r0 = 0
        L_0x003d:
            r1.A0D(r2, r0)
            r1.A0I(r5, r4)
            return
        L_0x0044:
            r6 = 0
            r2 = -1
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C43391ww.A00(X.1ww):void");
    }

    public static /* synthetic */ boolean A01(Network network, C43391ww r2) {
        NetworkCapabilities networkCapabilities;
        ConnectivityManager A0H = r2.A02.A0H();
        if (A0H == null || (networkCapabilities = A0H.getNetworkCapabilities(network)) == null || !networkCapabilities.hasTransport(1) || !networkCapabilities.hasCapability(17)) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC43401wx
    public long AC7() {
        Network activeNetwork;
        ConnectivityManager A0H = this.A02.A0H();
        if (A0H == null || (activeNetwork = A0H.getActiveNetwork()) == null) {
            return -1;
        }
        return activeNetwork.getNetworkHandle();
    }

    @Override // X.AbstractC43401wx
    public void AaZ() {
        Handler handler = this.A00;
        AnonymousClass009.A05(handler);
        handler.post(new RunnableBRunnable0Shape8S0100000_I0_8(this, 35));
    }

    @Override // X.AbstractC43401wx
    public void AeC(Handler handler) {
        boolean z = false;
        if (this.A01 == null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        this.A00 = handler;
        this.A01 = new C51872Zl(this);
        ConnectivityManager A0H = this.A02.A0H();
        if (A0H != null) {
            try {
                A0H.registerDefaultNetworkCallback(this.A01, handler);
            } catch (SecurityException unused) {
            }
        } else {
            Log.e("xmpp/handler/network/startNetworkCallbacks cm null");
        }
    }

    @Override // X.AbstractC43401wx
    public void AeS() {
        boolean z = false;
        if (this.A01 != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        ConnectivityManager A0H = this.A02.A0H();
        if (A0H != null) {
            A0H.unregisterNetworkCallback(this.A01);
        }
        this.A01 = null;
        this.A00 = null;
    }

    @Override // X.AbstractC43401wx
    public boolean isConnected() {
        C51872Zl r0 = this.A01;
        return (r0 == null || r0.A00 == null) ? false : true;
    }
}
