package X;

import android.graphics.Point;
import android.graphics.Rect;

/* renamed from: X.5ug  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127555ug {
    public final Point A00;
    public final Point A01;
    public final Point A02;
    public final Rect A03;

    public C127555ug(Point point, Point point2, Point point3, Rect rect) {
        this.A03 = rect;
        this.A00 = point;
        this.A02 = point2;
        this.A01 = point3;
    }
}
