package X;

import android.app.Activity;
import android.content.DialogInterface;

/* renamed from: X.4es  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class DialogInterface$OnCancelListenerC96114es implements DialogInterface.OnCancelListener {
    public final /* synthetic */ Activity A00;

    public /* synthetic */ DialogInterface$OnCancelListenerC96114es(Activity activity) {
        this.A00 = activity;
    }

    @Override // android.content.DialogInterface.OnCancelListener
    public final void onCancel(DialogInterface dialogInterface) {
        this.A00.finish();
    }
}
