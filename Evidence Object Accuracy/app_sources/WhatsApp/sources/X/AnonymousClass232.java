package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.232  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass232 implements AbstractC13940ka {
    public long A00;
    public C14650lo A01;
    public C14830m7 A02;
    public C255919z A03;
    public UserJid A04;
    public boolean A05 = false;
    public final AnonymousClass016 A06;

    public AnonymousClass232(C14650lo r2, C14830m7 r3, C255919z r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A06 = new AnonymousClass016();
    }

    @Override // X.AbstractC13940ka
    public void AR0() {
        this.A01.A03(new AnonymousClass53O(this), this.A04);
    }

    @Override // X.AbstractC13940ka
    public void AR1() {
        this.A01.A03(new AnonymousClass53O(this), this.A04);
    }
}
