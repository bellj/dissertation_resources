package X;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;

/* renamed from: X.2h4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54652h4 extends AbstractC018308n {
    public float A00;
    public C54842hN A01;
    public boolean A02 = false;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final Paint A09 = C12990iw.A0F();
    public final Rect A0A;
    public final RectF A0B = C12980iv.A0K();
    public final RectF A0C = C12980iv.A0K();
    public final AnonymousClass4T0 A0D;
    public final boolean A0E;
    public final boolean A0F;

    public C54652h4(Rect rect, AnonymousClass4T0 r3, int i, int i2, int i3, int i4, boolean z, boolean z2) {
        this.A03 = i2;
        this.A05 = i;
        this.A08 = Color.alpha(i);
        this.A04 = Color.alpha(i2);
        this.A07 = i3;
        this.A06 = i4;
        this.A0E = z;
        if (z) {
            this.A00 = 1.0f;
        }
        this.A0A = rect;
        this.A0F = z2;
        this.A0D = r3;
    }

    @Override // X.AbstractC018308n
    public void A02(Canvas canvas, C05480Ps r13, RecyclerView recyclerView) {
        RectF rectF;
        RectF rectF2;
        float f;
        int i;
        Rect rect;
        float width;
        C54842hN r1 = this.A01;
        if (r1 != null && !this.A02 && !r1.A04 && r1.A02) {
            RunnableBRunnable0Shape10S0200000_I1 runnableBRunnable0Shape10S0200000_I1 = new RunnableBRunnable0Shape10S0200000_I1(r1, 29, recyclerView);
            r1.A01 = runnableBRunnable0Shape10S0200000_I1;
            recyclerView.postDelayed(runnableBRunnable0Shape10S0200000_I1, 1500);
        }
        boolean z = true;
        this.A02 = true;
        if (this.A00 != 0.0f) {
            AnonymousClass02H layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof LinearLayoutManager) {
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
                boolean A1V = C12960it.A1V(AnonymousClass028.A05(((AnonymousClass02H) linearLayoutManager).A07), 1);
                int i2 = linearLayoutManager.A01;
                int A1G = linearLayoutManager.A1G(r13);
                int A1I = linearLayoutManager.A1I(r13);
                if (i2 == 1) {
                    int height = recyclerView.getHeight();
                    boolean z2 = this.A0F;
                    if (z2) {
                        Rect rect2 = this.A0A;
                        height = (height - rect2.top) - rect2.bottom;
                    }
                    int A1H = linearLayoutManager.A1H(r13);
                    if (A1I != A1G) {
                        int round = Math.round((((float) height) * ((float) A1G)) / ((float) A1I));
                        int i3 = height >> 1;
                        if (round > i3) {
                            round = i3;
                        }
                        int round2 = Math.round((((float) (height - round)) * ((float) A1H)) / ((float) (A1I - A1G)));
                        rectF = this.A0C;
                        if (A1V) {
                            rect = this.A0A;
                            int i4 = rect.right;
                            rectF.left = (float) i4;
                            width = (float) (this.A07 + i4);
                        } else {
                            rect = this.A0A;
                            rectF.left = (float) ((recyclerView.getWidth() - this.A07) - rect.right);
                            width = (float) (recyclerView.getWidth() - rect.right);
                        }
                        rectF.right = width;
                        rectF.top = (float) round2;
                        rectF.bottom = (float) (round2 + round);
                        rectF2 = this.A0B;
                        rectF2.left = rectF.left;
                        rectF2.right = width;
                        rectF2.top = 0.0f;
                        rectF2.bottom = C12990iw.A03(recyclerView);
                        if (z2) {
                            float f2 = rectF.top;
                            float f3 = (float) rect.top;
                            rectF.top = f2 + f3;
                            rectF.bottom += f3;
                            rectF2.top += f3;
                            rectF2.bottom -= (float) rect.bottom;
                        }
                    } else {
                        return;
                    }
                } else {
                    int width2 = recyclerView.getWidth();
                    boolean z3 = this.A0F;
                    if (z3) {
                        Rect rect3 = this.A0A;
                        width2 = (width2 - rect3.left) - rect3.right;
                    }
                    int A1H2 = linearLayoutManager.A1H(r13);
                    if (A1I != A1G) {
                        int round3 = Math.round((((float) width2) * ((float) A1G)) / ((float) A1I));
                        int i5 = width2 >> 1;
                        if (round3 > i5) {
                            round3 = i5;
                        }
                        int round4 = Math.round((((float) (width2 - round3)) * ((float) A1H2)) / ((float) (A1I - A1G)));
                        rectF = this.A0C;
                        rectF.left = (float) round4;
                        rectF.right = (float) (round4 + round3);
                        Rect rect4 = this.A0A;
                        rectF.top = (float) ((recyclerView.getHeight() - this.A07) - rect4.bottom);
                        rectF.bottom = (float) (recyclerView.getHeight() - rect4.bottom);
                        rectF2 = this.A0B;
                        rectF2.left = 0.0f;
                        rectF2.top = rectF.top;
                        rectF2.right = C12990iw.A02(recyclerView);
                        rectF2.bottom = rectF.bottom;
                        if (z3) {
                            float f4 = rectF.left;
                            if (A1V) {
                                float f5 = (float) rect4.right;
                                rectF.left = f4 + f5;
                                rectF.right += f5;
                                rectF2.left += f5;
                                f = rectF2.right;
                                i = rect4.left;
                            } else {
                                float f6 = (float) rect4.left;
                                rectF.left = f4 + f6;
                                rectF.right += f6;
                                rectF2.left += f6;
                                f = rectF2.right;
                                i = rect4.right;
                            }
                            rectF2.right = f - ((float) i);
                        }
                    } else {
                        return;
                    }
                }
                int i6 = this.A03;
                if (i6 != 0) {
                    AnonymousClass4T0 r8 = this.A0D;
                    float f7 = r8.A03;
                    if (f7 == 0.0f || !canvas.isHardwareAccelerated()) {
                        z = false;
                    } else {
                        this.A09.setShadowLayer(f7, r8.A01, r8.A00, r8.A05);
                    }
                    Paint paint = this.A09;
                    paint.setColor(i6);
                    paint.setAlpha((int) (this.A00 * ((float) this.A04)));
                    float f8 = (float) this.A06;
                    canvas.drawRoundRect(rectF2, f8, f8, paint);
                    if (z) {
                        paint.clearShadowLayer();
                    }
                }
                Paint paint2 = this.A09;
                paint2.setColor(this.A05);
                paint2.setAlpha((int) (this.A00 * ((float) this.A08)));
                float f9 = (float) this.A06;
                canvas.drawRoundRect(rectF, f9, f9, paint2);
            }
        }
    }
}
