package X;

import com.facebook.msys.mci.DataTask;
import com.facebook.msys.mci.NetworkSession;
import com.facebook.msys.mci.NetworkUtils;
import com.facebook.msys.mci.UrlRequest;
import com.facebook.msys.mci.UrlResponse;
import com.whatsapp.util.Log;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.3Eo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64183Eo {
    public long A00 = 0;
    public long A01;
    public final DataTask A02;
    public final NetworkSession A03;
    public final UrlRequest A04;
    public final C90514Oe A05;
    public final BufferedOutputStream A06;
    public final HttpURLConnection A07;
    public final /* synthetic */ C37561md A08;

    public C64183Eo(DataTask dataTask, NetworkSession networkSession, C90514Oe r11, C37561md r12) {
        this.A08 = r12;
        this.A05 = r11;
        this.A02 = dataTask;
        this.A03 = networkSession;
        UrlRequest urlRequest = dataTask.mUrlRequest;
        this.A04 = urlRequest;
        try {
            HttpsURLConnection A01 = r12.A01(urlRequest, null, null);
            this.A07 = A01;
            A01.setDoOutput(true);
            A01.setChunkedStreamingMode(10240);
            Long valueOf = Long.valueOf(dataTask.mContentLength);
            if (valueOf != null) {
                long longValue = valueOf.longValue();
                if (longValue > 0) {
                    this.A01 = longValue;
                    this.A06 = new BufferedOutputStream(new AnonymousClass23V(r12.A02, A01.getOutputStream(), null, 29), 10240);
                    networkSession.executeInNetworkContext(new C75973kp(dataTask, networkSession, this, r12));
                    return;
                }
            }
            throw C12990iw.A0i("Content-Length cannot be empty for streaming upload");
        } catch (IOException | IllegalArgumentException | IndexOutOfBoundsException e) {
            Log.e("wa-msys/NetworkSession: Error while initializing StreamingUploadDataTask", e);
            IOException iOException = new IOException(e);
            A01(NetworkUtils.newErrorURLResponse(this.A04), iOException, null);
            throw iOException;
        }
    }

    public final void A00() {
        try {
            BufferedOutputStream bufferedOutputStream = this.A06;
            if (bufferedOutputStream != null) {
                this.A07.connect();
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
            }
        } catch (IOException | IllegalArgumentException | IndexOutOfBoundsException e) {
            Log.e("wa-msys/NetworkSession: Exception while attempting to close the active output stream.  This could mean that the output stream has been previously closed.", e);
        }
    }

    public final void A01(UrlResponse urlResponse, IOException iOException, byte[] bArr) {
        A00();
        this.A07.disconnect();
        C90514Oe r0 = this.A05;
        r0.A01.A07.remove(r0.A00.mTaskIdentifier);
        NetworkUtils.markDataTaskCompleted(this.A02, this.A03, urlResponse, "wa-msys/NetworkSession: ", bArr, null, iOException);
    }
}
