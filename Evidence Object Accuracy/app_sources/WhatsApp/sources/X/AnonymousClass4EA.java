package X;

import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.4EA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4EA {
    public static AnonymousClass1WZ A00(AbstractC16710pd r1) {
        ImageView imageView = (ImageView) r1.getValue();
        C16700pc.A0E(imageView, 0);
        imageView.setImageResource(R.drawable.category_image_placeholder_bg);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        return AnonymousClass1WZ.A00;
    }
}
