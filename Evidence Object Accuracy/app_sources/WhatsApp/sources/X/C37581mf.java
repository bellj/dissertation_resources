package X;

import android.net.Uri;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.1mf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37581mf {
    public final C18790t3 A00;
    public final C19930uu A01;

    public C37581mf(C18790t3 r1, C19930uu r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public AbstractC37631mk A00(C18800t4 r5, String str, String str2) {
        try {
            try {
                URLConnection openConnection = new URL(Uri.parse(str).toString()).openConnection();
                if (openConnection instanceof HttpsURLConnection) {
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) openConnection;
                    httpsURLConnection.setSSLSocketFactory(r5.A02());
                    httpsURLConnection.setConnectTimeout(15000);
                    httpsURLConnection.setReadTimeout(30000);
                    if (str2 != null) {
                        httpsURLConnection.addRequestProperty("If-None-Match", str2);
                    }
                    httpsURLConnection.setRequestProperty("User-Agent", this.A01.A00());
                    return new C37621mj(null, httpsURLConnection);
                }
                throw new IOException("WaHttpUrlConnectionClient/createDownloadableFilesConnection/failed to open http url connection");
            } catch (IOException e) {
                throw new IOException("WaHttpUrlConnectionClient/createDownloadableFilesConnection/failed to open http url connection", e);
            }
        } catch (MalformedURLException e2) {
            Log.e("WaHttpUrlConnectionClient/createDownloadableFilesConnection/malformed-url : ", e2);
            throw e2;
        }
    }

    public final AbstractC37631mk A01(Integer num, String str, String str2, String str3, Map map, boolean z, boolean z2) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(30000);
        if (str3 == null) {
            str3 = this.A01.A00();
        }
        httpURLConnection.setRequestProperty("User-Agent", str3);
        if (z) {
            httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        }
        if (str2 != null) {
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            if (z2) {
                httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
            }
        }
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
        }
        if (str2 != null) {
            AnonymousClass23V r2 = new AnonymousClass23V(this.A00, httpURLConnection.getOutputStream(), null, num);
            try {
                if (z2) {
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(r2);
                    gZIPOutputStream.write(str2.getBytes(AnonymousClass01V.A08));
                    gZIPOutputStream.close();
                } else {
                    r2.write(str2.getBytes(AnonymousClass01V.A08));
                }
                r2.close();
            } catch (Throwable th) {
                try {
                    r2.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        httpURLConnection.connect();
        return new C37621mj(null, httpURLConnection);
    }

    public AbstractC37631mk A02(String str) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.connect();
        return new C37621mj(null, httpURLConnection);
    }
}
