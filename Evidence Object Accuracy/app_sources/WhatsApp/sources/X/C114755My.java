package X;

/* renamed from: X.5My  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114755My extends AnonymousClass1TM implements AbstractC115545Ry {
    public int A00;
    public AnonymousClass1TN A01;

    public C114755My() {
        this.A00 = 0;
        this.A01 = AnonymousClass5ME.A00;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return new C114835Ng(this.A01, this.A00, false);
    }

    public C114755My(AnonymousClass5NU r3) {
        AnonymousClass1TN r1;
        int i = r3.A00;
        this.A00 = i;
        if (i != 0) {
            if (i == 1) {
                AbstractC114775Na A05 = AbstractC114775Na.A05(r3, false);
                if (A05 != null) {
                    r1 = new AnonymousClass5MJ(AbstractC114775Na.A04((Object) A05));
                } else {
                    r1 = null;
                }
                this.A01 = r1;
            } else if (i != 2) {
                throw C12970iu.A0f(C12960it.A0W(i, "Unknown tag encountered: "));
            }
        }
        r1 = AnonymousClass5ME.A00;
        this.A01 = r1;
    }
}
