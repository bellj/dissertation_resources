package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57452n4 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57452n4 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01 = 0;
    public C43261wh A02;
    public C57012mK A03;
    public Object A04;

    static {
        C57452n4 r0 = new C57452n4();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C82043uz r1;
        int i;
        C81603uH r12;
        C82063v1 r13;
        EnumC87254Au r0;
        boolean z = false;
        switch (r7.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57452n4 r9 = (C57452n4) obj2;
                this.A03 = (C57012mK) r8.Aft(this.A03, r9.A03);
                this.A02 = (C43261wh) r8.Aft(this.A02, r9.A02);
                int i2 = r9.A01;
                if (i2 == 0) {
                    r0 = EnumC87254Au.A01;
                } else if (i2 != 2) {
                    r0 = null;
                } else {
                    r0 = EnumC87254Au.A02;
                }
                switch (r0.ordinal()) {
                    case 0:
                        if (this.A01 == 2) {
                            z = true;
                        }
                        this.A04 = r8.Afv(this.A04, r9.A04, z);
                        break;
                    case 1:
                        if (this.A01 != 0) {
                            z = true;
                        }
                        r8.Afw(z);
                        break;
                }
                if (r8 == C463025i.A00) {
                    int i3 = r9.A01;
                    if (i3 != 0) {
                        this.A01 = i3;
                    }
                    this.A00 |= r9.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r82.A03();
                            if (A03 == 0) {
                                break;
                            } else {
                                if (A03 == 10) {
                                    if ((this.A00 & 1) == 1) {
                                        r1 = (C82043uz) this.A03.A0T();
                                    } else {
                                        r1 = null;
                                    }
                                    C57012mK r02 = (C57012mK) AbstractC27091Fz.A0H(r82, r92, C57012mK.A02);
                                    this.A03 = r02;
                                    if (r1 != null) {
                                        this.A03 = (C57012mK) AbstractC27091Fz.A0C(r1, r02);
                                    }
                                    i = this.A00 | 1;
                                } else if (A03 == 18) {
                                    if (this.A01 == 2) {
                                        r13 = (C82063v1) ((C57352mt) this.A04).A0T();
                                    } else {
                                        r13 = null;
                                    }
                                    AnonymousClass1G1 A0H = AbstractC27091Fz.A0H(r82, r92, C57352mt.A04);
                                    this.A04 = A0H;
                                    if (r13 != null) {
                                        this.A04 = AbstractC27091Fz.A0C(r13, (C57352mt) A0H);
                                    }
                                    this.A01 = 2;
                                } else if (A03 == 122) {
                                    if ((this.A00 & 4) == 4) {
                                        r12 = (C81603uH) this.A02.A0T();
                                    } else {
                                        r12 = null;
                                    }
                                    C43261wh r03 = (C43261wh) AbstractC27091Fz.A0H(r82, r92, C43261wh.A0O);
                                    this.A02 = r03;
                                    if (r12 != null) {
                                        this.A02 = (C43261wh) AbstractC27091Fz.A0C(r12, r03);
                                    }
                                    i = this.A00 | 4;
                                } else if (!A0a(r82, A03)) {
                                    break;
                                }
                                this.A00 = i;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57452n4();
            case 5:
                return new C82053v0();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C57452n4.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C57012mK r0 = this.A03;
            if (r0 == null) {
                r0 = C57012mK.A02;
            }
            i2 = AbstractC27091Fz.A08(r0, 1, 0);
        }
        if (this.A01 == 2) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G0) this.A04, 2, i2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r02 = this.A02;
            if (r02 == null) {
                r02 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r02, 15, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C57012mK r0 = this.A03;
            if (r0 == null) {
                r0 = C57012mK.A02;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if (this.A01 == 2) {
            AbstractC27091Fz.A0O(codedOutputStream, this.A04, 2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r02 = this.A02;
            if (r02 == null) {
                r02 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r02, 15);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
