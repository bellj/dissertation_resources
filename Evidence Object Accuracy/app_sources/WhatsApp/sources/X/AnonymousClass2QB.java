package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.whatsapp.util.MarqueeToolbar;

/* renamed from: X.2QB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QB extends AnonymousClass04v {
    public final /* synthetic */ MarqueeToolbar A00;

    public AnonymousClass2QB(MarqueeToolbar marqueeToolbar) {
        this.A00 = marqueeToolbar;
    }

    @Override // X.AnonymousClass04v
    public void A00(View view, int i) {
        if (i != 4) {
            super.A00(view, i);
        }
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
        accessibilityNodeInfo.setClickable(false);
        accessibilityNodeInfo.setSelected(false);
        for (C007804a r2 : r6.A03()) {
            if (r2.A00() == 16 || r2.A00() == 4) {
                r6.A0A(r2);
            }
        }
    }
}
