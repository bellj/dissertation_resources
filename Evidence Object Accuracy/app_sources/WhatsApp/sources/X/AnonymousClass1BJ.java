package X;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1BJ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1BJ {
    public static final HashMap A0G = new C39491pz();
    public C39451pv A00 = null;
    public final SparseArray A01 = new SparseArray();
    public final SparseArray A02 = new SparseArray();
    public final SparseIntArray A03 = new SparseIntArray();
    public final AbstractC15710nm A04;
    public final C18790t3 A05;
    public final C18640sm A06;
    public final C14830m7 A07;
    public final C16590pI A08;
    public final C17170qN A09;
    public final C14820m6 A0A;
    public final AnonymousClass1BH A0B;
    public final C18810t5 A0C;
    public final C18800t4 A0D;
    public final AbstractC14440lR A0E;
    public final List A0F = new ArrayList();

    public AnonymousClass1BJ(AbstractC15710nm r2, C18790t3 r3, C18640sm r4, C14830m7 r5, C16590pI r6, C17170qN r7, C14820m6 r8, AnonymousClass1BH r9, C18810t5 r10, C18800t4 r11, AbstractC14440lR r12) {
        this.A08 = r6;
        this.A07 = r5;
        this.A04 = r2;
        this.A0E = r12;
        this.A05 = r3;
        this.A0B = r9;
        this.A0D = r11;
        this.A0C = r10;
        this.A0A = r8;
        this.A06 = r4;
        this.A09 = r7;
    }

    public static void A00(C14820m6 r9) {
        int i = 0;
        String[] strArr = {"manifest", "filter", "doodle_emoji"};
        do {
            String str = strArr[i];
            SharedPreferences sharedPreferences = r9.A00;
            StringBuilder sb = new StringBuilder("downloadable_category_local_info_json_");
            sb.append(str);
            String string = sharedPreferences.getString(sb.toString(), null);
            if (!TextUtils.isEmpty(string)) {
                try {
                    AnonymousClass009.A05(string);
                    JSONObject jSONObject = new JSONObject(string);
                    Map A01 = C39451pv.A01(jSONObject);
                    if (A01 != null && A01.containsKey("0")) {
                        String num = Integer.toString(-1);
                        if (!A01.containsKey(num)) {
                            Object obj = A01.get("0");
                            A01.clear();
                            A01.put(num, obj);
                            jSONObject.put("bundles", new JSONObject(A01));
                        }
                    }
                    r9.A0u(str, jSONObject.toString());
                } catch (JSONException e) {
                    Log.e("CategoryManager/migrateLocalCatInfo", e);
                }
            }
            i++;
        } while (i < 3);
    }

    public synchronized int A01(int i) {
        return this.A03.get(i, 0);
    }

    public synchronized C39451pv A02() {
        String str;
        String str2;
        C39451pv r3 = this.A00;
        if (r3 == null) {
            r3 = null;
            try {
                C14820m6 r1 = this.A0A;
                A00(r1);
                if (!(this instanceof AnonymousClass1BI)) {
                    str2 = "doodle_emoji";
                } else {
                    str2 = "filter";
                }
                SharedPreferences sharedPreferences = r1.A00;
                StringBuilder sb = new StringBuilder("downloadable_category_local_info_json_");
                sb.append(str2);
                String string = sharedPreferences.getString(sb.toString(), null);
                if (!TextUtils.isEmpty(string)) {
                    C39451pv A00 = C39451pv.A00(string);
                    this.A00 = A00;
                    return A00;
                }
            } catch (JSONException e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("CategoryManager/getLocalIdHash/json exception while getting local category info for ");
                if (!(this instanceof AnonymousClass1BI)) {
                    str = "doodle_emoji";
                } else {
                    str = "filter";
                }
                sb2.append(str);
                sb2.append(e.getMessage());
                C39461pw.A02(sb2.toString());
            }
        }
        return r3;
    }

    public Object A03() {
        HashMap A0E;
        SparseArray sparseArray;
        if (!(this instanceof AnonymousClass1BI)) {
            AnonymousClass1EN r0 = (AnonymousClass1EN) this;
            synchronized (this) {
                sparseArray = r0.A00;
                if (sparseArray.size() == 0) {
                    sparseArray = null;
                }
            }
            return sparseArray;
        }
        AnonymousClass1BI r02 = (AnonymousClass1BI) this;
        synchronized (this) {
            A0E = r02.A0E();
            if (A0E.isEmpty()) {
                A0E = null;
            }
        }
        return A0E;
    }

    public Map A04(String str, String str2, String str3, String str4, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("category", str);
        if (str2 != null) {
            hashMap.put("locale", str2);
        }
        if (str3 != null) {
            hashMap.put("existing_id", str3);
        }
        return hashMap;
    }

    public void A05() {
        if (this instanceof AnonymousClass1BI) {
            AnonymousClass1BI r1 = (AnonymousClass1BI) this;
            synchronized (r1) {
                r1.A00.clear();
            }
        }
    }

    public synchronized void A06() {
        String str;
        if (!(this instanceof AnonymousClass1BI)) {
            str = "doodle_emoji";
        } else {
            str = "filter";
        }
        this.A0A.A0u(str, null);
        this.A00 = null;
    }

    public final synchronized void A07(int i) {
        this.A01.put(i, Long.valueOf(this.A07.A00()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0140, code lost:
        if (r13 == 2) goto L_0x0142;
     */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x019d A[Catch: all -> 0x01c5, TryCatch #2 {, blocks: (B:96:0x0198, B:98:0x019d, B:100:0x01ad, B:101:0x01b5), top: B:111:0x0198 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 470
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BJ.A08(int, int):void");
    }

    public synchronized void A09(int i, int i2) {
        SparseIntArray sparseIntArray = this.A03;
        int i3 = sparseIntArray.get(i2, 0);
        if (!(i3 == 3 && i == 3)) {
            if (i3 != 1) {
                if (i3 == 3 && i == 1) {
                }
                HashMap hashMap = A0G;
                hashMap.get(Integer.valueOf(i3));
                hashMap.get(Integer.valueOf(i));
                sparseIntArray.put(i2, i);
            } else if (i != 1) {
                HashMap hashMap = A0G;
                hashMap.get(Integer.valueOf(i3));
                hashMap.get(Integer.valueOf(i));
                sparseIntArray.put(i2, i);
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("CategoryManager/setState/State change ERROR - ");
        HashMap hashMap2 = A0G;
        sb.append((String) hashMap2.get(Integer.valueOf(i3)));
        sb.append(" to ");
        sb.append((String) hashMap2.get(Integer.valueOf(i)));
        sb.append("!");
        Log.e(sb.toString());
    }

    public synchronized void A0A(AbstractC39501q0 r5, int i) {
        int A01 = A01(i);
        if (A01 == 3 || A01 == 1) {
            this.A0F.add(r5);
        } else {
            if (!(A01 == 4 || A01 == 2)) {
                if (A01 != 5 || A03() == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("CategoryManager/registerCallback/Unexpected state encountered - ");
                    sb.append((String) A0G.get(Integer.valueOf(A01)));
                    Log.e(sb.toString());
                } else {
                    Object A03 = A03();
                    AnonymousClass009.A05(A03);
                    r5.AUc(A03);
                }
            }
            r5.APk();
        }
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        	at jadx.core.utils.BlockUtils.lambda$getPathCross$3(BlockUtils.java:689)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:689)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:728)
        	at jadx.core.dex.visitors.regions.IfMakerHelper.restructureIf(IfMakerHelper.java:88)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:707)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:737)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:737)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    public final void A0B(X.C39451pv r20, X.AnonymousClass1VN r21, java.lang.String r22, int r23) {
        /*
        // Method dump skipped, instructions count: 1040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1BJ.A0B(X.1pv, X.1VN, java.lang.String, int):void");
    }

    public final void A0C(String str) {
        synchronized (this) {
            List list = this.A0F;
            if (!list.isEmpty()) {
                ArrayList arrayList = new ArrayList(list);
                list.clear();
                if (str == null || A03() == null) {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        ((AbstractC39501q0) it.next()).APk();
                    }
                    return;
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    ((AbstractC39501q0) it2.next()).AUc(A03());
                }
            }
        }
    }

    public boolean A0D(int i) {
        boolean contains;
        if (!(this instanceof AnonymousClass1BI)) {
            AnonymousClass1EN r2 = (AnonymousClass1EN) this;
            synchronized (r2) {
                r2.A0F(i);
                contains = r2.A02.contains(Integer.valueOf(i));
            }
            return contains;
        }
        AnonymousClass1BI r22 = (AnonymousClass1BI) this;
        boolean z = false;
        if (i == -1) {
            z = true;
        }
        AnonymousClass009.A0E(z);
        return r22.A0G();
    }
}
