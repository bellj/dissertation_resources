package X;

import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;

/* renamed from: X.1hO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35181hO extends AbstractC18860tB {
    public final /* synthetic */ MessageDetailsActivity A00;

    public C35181hO(MessageDetailsActivity messageDetailsActivity) {
        this.A00 = messageDetailsActivity;
    }

    public final void A03(AbstractC15340mz r5) {
        if (r5 != null) {
            AnonymousClass1IS r3 = r5.A0z;
            String str = r3.A01;
            MessageDetailsActivity messageDetailsActivity = this.A00;
            if (!str.equals(messageDetailsActivity.A0P.A0z.A01)) {
                return;
            }
            if (r3.A02 || (r5 instanceof C27671Iq)) {
                messageDetailsActivity.A2e();
                messageDetailsActivity.A0D.A0s();
            }
        }
    }
}
