package X;

import com.whatsapp.jid.UserJid;
import java.util.concurrent.TimeUnit;

/* renamed from: X.22F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22F {
    public static final long A06 = TimeUnit.DAYS.toMillis(7);
    public int A00;
    public long A01;
    public boolean A02;
    public final UserJid A03;
    public final String A04;
    public final String A05;

    public AnonymousClass22F(UserJid userJid, String str, String str2, int i, long j, boolean z) {
        this.A03 = userJid;
        this.A04 = str;
        this.A05 = str2;
        this.A00 = i;
        this.A02 = z;
        this.A01 = j;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ConversionTuple{userJid=");
        sb.append(this.A03);
        sb.append(", data='");
        sb.append(this.A04);
        sb.append('\'');
        sb.append(", source='");
        sb.append(this.A05);
        sb.append('\'');
        sb.append(", bizCount=");
        sb.append(this.A00);
        sb.append(", hasUserSentLastMessage=");
        sb.append(this.A02);
        sb.append(", lastInteractionTimestampMs=");
        sb.append(this.A01);
        sb.append('}');
        return sb.toString();
    }
}
