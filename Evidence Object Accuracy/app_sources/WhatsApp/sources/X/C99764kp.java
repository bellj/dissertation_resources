package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99764kp implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30861Zc(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30861Zc[i];
    }
}
