package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1GZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1GZ implements AbstractC27101Ga {
    public final /* synthetic */ C22860zl A00;

    @Override // X.AbstractC27101Ga
    public void AQx(DeviceJid deviceJid, int i) {
    }

    public AnonymousClass1GZ(C22860zl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC27101Ga
    public void ARG(DeviceJid deviceJid) {
        UserJid userJid = deviceJid.getUserJid();
        C22860zl r6 = this.A00;
        C22700zV r4 = r6.A04;
        AnonymousClass1M2 A00 = r4.A00(userJid);
        if (A00 != null) {
            StringBuilder sb = new StringBuilder("confirming unconfirmed vname cert; jid=");
            sb.append(userJid);
            Log.i(sb.toString());
            byte[] bArr = A00.A09;
            if (bArr != null) {
                r4.A04(userJid, A00.A00(), bArr, A00.A03);
            }
            r6.A09.A0N(userJid, C42181un.A00(A00).A01());
        }
        if (deviceJid.device == 0) {
            r6.A0E.A02(userJid);
        }
    }

    @Override // X.AbstractC27101Ga
    public void ARH(DeviceJid deviceJid) {
        C22860zl.A00(this.A00, deviceJid, false);
    }

    @Override // X.AbstractC27101Ga
    public void ARI(DeviceJid deviceJid) {
        C22860zl.A00(this.A00, deviceJid, true);
    }
}
