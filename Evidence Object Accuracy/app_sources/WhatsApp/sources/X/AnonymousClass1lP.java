package X;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPicker;

/* renamed from: X.1lP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1lP extends AnonymousClass02M {
    public final /* synthetic */ AbstractActivityC36611kC A00;

    public /* synthetic */ AnonymousClass1lP(AbstractActivityC36611kC r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.A0g.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r8, int i) {
        C75513jy r82 = (C75513jy) r8;
        AbstractActivityC36611kC r5 = this.A00;
        C15370n3 r6 = (C15370n3) r5.A0g.get(i);
        r82.A01.setText(r5.A0L.A0A(r6, -1));
        AnonymousClass1J1 r1 = r5.A0M;
        if (r1 != null) {
            r1.A07(r82.A02, r6, false);
        }
        View view = r82.A00;
        view.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 15, r6));
        view.setContentDescription(r5.getString(R.string.selected_contact_content_description, r5.A0L.A04(r6)));
        AnonymousClass23N.A02(view, R.string.accessibility_action_click_selected_contact_to_deselect);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        int i2;
        AbstractActivityC36611kC r0 = this.A00;
        LayoutInflater layoutInflater = r0.getLayoutInflater();
        if (!(r0 instanceof GroupCallParticipantPicker)) {
            i2 = R.layout.selected_contact;
        } else {
            i2 = R.layout.selected_contact_group_call;
        }
        return new C75513jy(layoutInflater.inflate(i2, viewGroup, false));
    }
}
