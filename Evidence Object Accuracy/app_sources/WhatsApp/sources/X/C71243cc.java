package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.3cc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71243cc implements Comparator {
    public final Collator A00;

    public C71243cc(AnonymousClass018 r3) {
        Collator instance = Collator.getInstance(C12970iu.A14(r3));
        this.A00 = instance;
        instance.setDecomposition(1);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        String str = (String) obj;
        String str2 = (String) obj2;
        Integer A01 = AnonymousClass4Yh.A01(str, str2);
        if (A01 == null) {
            return this.A00.compare(str, str2);
        }
        return A01.intValue();
    }
}
