package X;

/* renamed from: X.1ez  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33731ez extends C33711ex {
    public C33731ez(C16470p4 r1) {
        super(r1);
    }

    @Override // X.C33711ex
    public String A09(AnonymousClass018 r8) {
        StringBuilder sb = new StringBuilder();
        C33711ex.A00(super.A09(r8), "\n", sb);
        C16470p4 r0 = this.A00;
        if (r0 != null) {
            for (AnonymousClass1ZA r1 : r0.A09) {
                String str = r1.A00;
                sb.append(str);
                C33711ex.A00(str, "\n", sb);
                for (AnonymousClass1ZB r2 : r1.A01) {
                    C33711ex.A00(r2.A02, " ", sb);
                    C33711ex.A00(r2.A00, "\n", sb);
                }
            }
        }
        return sb.toString();
    }

    @Override // X.C33711ex
    public void A0A(AbstractC15340mz r1, C39971qq r2) {
        AnonymousClass3GO.A00(r1, r2);
    }
}
