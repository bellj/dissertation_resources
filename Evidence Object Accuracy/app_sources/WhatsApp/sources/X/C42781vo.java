package X;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42781vo implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100464lx();
    public int A00;
    public int A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public boolean A05;
    public final int A06;
    public final int A07;
    public final Intent A08;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Deprecated
    public C42781vo(int i, int i2, Intent intent) {
        this.A08 = intent;
        this.A07 = i;
        this.A06 = i2;
    }

    public C42781vo(Parcel parcel) {
        Integer num;
        Integer num2;
        Parcelable readParcelable = parcel.readParcelable(Intent.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.A08 = (Intent) readParcelable;
        this.A07 = parcel.readInt();
        this.A06 = parcel.readInt();
        this.A05 = parcel.readByte() != 0;
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        int readInt = parcel.readInt();
        Integer num3 = null;
        if (readInt != -1) {
            num = Integer.valueOf(readInt);
        } else {
            num = null;
        }
        this.A02 = num;
        int readInt2 = parcel.readInt();
        if (readInt2 != -1) {
            num2 = Integer.valueOf(readInt2);
        } else {
            num2 = null;
        }
        this.A03 = num2;
        int readInt3 = parcel.readInt();
        this.A04 = readInt3 != -1 ? Integer.valueOf(readInt3) : num3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        int i3;
        parcel.writeParcelable(this.A08, i);
        parcel.writeInt(this.A07);
        parcel.writeInt(this.A06);
        parcel.writeByte(this.A05 ? (byte) 1 : 0);
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        Integer num = this.A02;
        int i4 = -1;
        if (num != null) {
            i2 = num.intValue();
        } else {
            i2 = -1;
        }
        parcel.writeInt(i2);
        Integer num2 = this.A03;
        if (num2 != null) {
            i3 = num2.intValue();
        } else {
            i3 = -1;
        }
        parcel.writeInt(i3);
        Integer num3 = this.A04;
        if (num3 != null) {
            i4 = num3.intValue();
        }
        parcel.writeInt(i4);
    }
}
