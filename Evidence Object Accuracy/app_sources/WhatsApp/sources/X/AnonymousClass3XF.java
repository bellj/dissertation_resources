package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3XF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XF implements AnonymousClass23E {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass23D A01;
    public final /* synthetic */ AnonymousClass35H A02;
    public final /* synthetic */ String A03;

    @Override // X.AnonymousClass23E
    public void A6L() {
    }

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XF(Context context, AnonymousClass23D r2, AnonymousClass35H r3, String str) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = context;
        this.A03 = str;
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        AnonymousClass35H r2 = this.A02;
        AnonymousClass2d1 r3 = r2.A05;
        if (r3.getTag() == this.A01 && bitmap != MediaGalleryFragmentBase.A0U) {
            r2.A01 = true;
            r3.A00(new BitmapDrawable(this.A00.getResources(), bitmap), null, this.A03);
        }
    }
}
