package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton$Behavior;
import com.whatsapp.R;

@CoordinatorLayout.DefaultBehavior(FloatingActionButton$Behavior.class)
/* renamed from: X.2QV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QV extends AnonymousClass2QW implements AnonymousClass012, AnonymousClass03Y, AnonymousClass2QX {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public ColorStateList A05;
    public ColorStateList A06;
    public ColorStateList A07;
    public PorterDuff.Mode A08;
    public PorterDuff.Mode A09;
    public C50632Qh A0A;
    public boolean A0B;
    public final Rect A0C;
    public final Rect A0D;
    public final AnonymousClass07j A0E;
    public final C50622Qg A0F;

    public AnonymousClass2QV(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.floatingActionButtonStyle);
    }

    public AnonymousClass2QV(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A0C = new Rect();
        this.A0D = new Rect();
        TypedArray A00 = C50582Qc.A00(context, attributeSet, C50572Qb.A07, new int[0], i, 2131952626);
        this.A05 = C50592Qd.A00(context, A00, 1);
        this.A08 = C50602Qe.A00(null, A00.getInt(2, -1));
        this.A07 = C50592Qd.A00(context, A00, 11);
        this.A04 = A00.getInt(6, -1);
        this.A01 = A00.getDimensionPixelSize(5, 0);
        this.A00 = A00.getDimensionPixelSize(3, 0);
        float dimension = A00.getDimension(4, 0.0f);
        float dimension2 = A00.getDimension(8, 0.0f);
        float dimension3 = A00.getDimension(10, 0.0f);
        this.A0B = A00.getBoolean(13, false);
        this.A03 = A00.getDimensionPixelSize(9, 0);
        C50612Qf A01 = C50612Qf.A01(context, A00, 12);
        C50612Qf A012 = C50612Qf.A01(context, A00, 7);
        A00.recycle();
        AnonymousClass07j r0 = new AnonymousClass07j(this);
        this.A0E = r0;
        r0.A02(attributeSet, i);
        this.A0F = new C50622Qg(this);
        getImpl().A0C(this.A05, this.A07, this.A08, this.A00);
        C50632Qh impl = getImpl();
        if (impl.A00 != dimension) {
            impl.A00 = dimension;
            impl.A0A(dimension, impl.A01, impl.A03);
        }
        C50632Qh impl2 = getImpl();
        if (impl2.A01 != dimension2) {
            impl2.A01 = dimension2;
            impl2.A0A(impl2.A00, dimension2, impl2.A03);
        }
        C50632Qh impl3 = getImpl();
        if (impl3.A03 != dimension3) {
            impl3.A03 = dimension3;
            impl3.A0A(impl3.A00, impl3.A01, dimension3);
        }
        C50632Qh impl4 = getImpl();
        int i2 = this.A03;
        if (impl4.A06 != i2) {
            impl4.A06 = i2;
            float f = impl4.A02;
            impl4.A02 = f;
            Matrix matrix = impl4.A0I;
            impl4.A0D(matrix, f);
            impl4.A0N.setImageMatrix(matrix);
        }
        getImpl().A0F = A01;
        getImpl().A0E = A012;
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public final int A01(int i) {
        int i2 = this.A01;
        if (i2 != 0) {
            return i2;
        }
        Resources resources = getResources();
        if (i != -1) {
            int i3 = R.dimen.design_fab_size_mini;
            if (i != 1) {
                i3 = R.dimen.design_fab_size_normal;
            }
            return resources.getDimensionPixelSize(i3);
        } else if (Math.max(resources.getConfiguration().screenWidthDp, resources.getConfiguration().screenHeightDp) < 470) {
            return A01(1);
        } else {
            return A01(0);
        }
    }

    public final void A02() {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            ColorStateList colorStateList = this.A06;
            if (colorStateList == null) {
                C015607k.A08(drawable);
                return;
            }
            int colorForState = colorStateList.getColorForState(getDrawableState(), 0);
            PorterDuff.Mode mode = this.A09;
            if (mode == null) {
                mode = PorterDuff.Mode.SRC_IN;
            }
            drawable.mutate().setColorFilter(C011905s.A00(mode, colorForState));
        }
    }

    public void A03(boolean z) {
        C50632Qh impl = getImpl();
        AnonymousClass2QW r3 = impl.A0N;
        int visibility = r3.getVisibility();
        int i = impl.A05;
        if (visibility == 0) {
            if (i == 1) {
                return;
            }
        } else if (i != 2) {
            return;
        }
        Animator animator = impl.A07;
        if (animator != null) {
            animator.cancel();
        }
        if (!AnonymousClass028.A0r(r3) || r3.isInEditMode()) {
            int i2 = 4;
            if (z) {
                i2 = 8;
            }
            r3.A00(i2, z);
            return;
        }
        C50612Qf r1 = impl.A0E;
        if (r1 == null && (r1 = impl.A0C) == null) {
            r1 = C50612Qf.A00(r3.getContext(), R.animator.design_fab_hide_motion_spec);
            impl.A0C = r1;
        }
        AnimatorSet A02 = impl.A02(r1, 0.0f, 0.0f, 0.0f);
        A02.addListener(new C50662Ql(impl, z));
        A02.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0028, code lost:
        if (r4.isInEditMode() != false) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(boolean r6) {
        /*
            r5 = this;
            X.2Qh r3 = r5.getImpl()
            X.2QW r4 = r3.A0N
            int r2 = r4.getVisibility()
            r0 = 1
            int r1 = r3.A05
            if (r2 == 0) goto L_0x0013
            r0 = 2
            if (r1 != r0) goto L_0x0016
            return
        L_0x0013:
            if (r1 == r0) goto L_0x0016
            return
        L_0x0016:
            android.animation.Animator r0 = r3.A07
            if (r0 == 0) goto L_0x001d
            r0.cancel()
        L_0x001d:
            boolean r0 = X.AnonymousClass028.A0r(r4)
            if (r0 == 0) goto L_0x002a
            boolean r1 = r4.isInEditMode()
            r0 = 1
            if (r1 == 0) goto L_0x002b
        L_0x002a:
            r0 = 0
        L_0x002b:
            r2 = 1065353216(0x3f800000, float:1.0)
            if (r0 == 0) goto L_0x006e
            int r0 = r4.getVisibility()
            if (r0 == 0) goto L_0x0049
            r1 = 0
            r4.setAlpha(r1)
            r4.setScaleY(r1)
            r4.setScaleX(r1)
            r3.A02 = r1
            android.graphics.Matrix r0 = r3.A0I
            r3.A0D(r0, r1)
            r4.setImageMatrix(r0)
        L_0x0049:
            X.2Qf r0 = r3.A0F
            if (r0 != 0) goto L_0x005e
            X.2Qf r0 = r3.A0D
            if (r0 != 0) goto L_0x005e
            android.content.Context r1 = r4.getContext()
            r0 = 2130837506(0x7f020002, float:1.7279968E38)
            X.2Qf r0 = X.C50612Qf.A00(r1, r0)
            r3.A0D = r0
        L_0x005e:
            android.animation.AnimatorSet r1 = r3.A02(r0, r2, r2, r2)
            X.2Qq r0 = new X.2Qq
            r0.<init>(r3, r6)
            r1.addListener(r0)
            r1.start()
            return
        L_0x006e:
            r0 = 0
            r4.A00(r0, r6)
            r4.setAlpha(r2)
            r4.setScaleY(r2)
            r4.setScaleX(r2)
            r3.A02 = r2
            android.graphics.Matrix r0 = r3.A0I
            r3.A0D(r0, r2)
            r4.setImageMatrix(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2QV.A04(boolean):void");
    }

    @Override // android.widget.ImageView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        getImpl().A0G(getDrawableState());
    }

    @Override // android.view.View
    public ColorStateList getBackgroundTintList() {
        return this.A05;
    }

    @Override // android.view.View
    public PorterDuff.Mode getBackgroundTintMode() {
        return this.A08;
    }

    public float getCompatElevation() {
        return getImpl().A01();
    }

    public float getCompatHoveredFocusedTranslationZ() {
        return getImpl().A01;
    }

    public float getCompatPressedTranslationZ() {
        return getImpl().A03;
    }

    public Drawable getContentBackground() {
        return getImpl().A08;
    }

    public int getCustomSize() {
        return this.A01;
    }

    public int getExpandedComponentIdHint() {
        return this.A0F.A00;
    }

    public C50612Qf getHideMotionSpec() {
        return getImpl().A0E;
    }

    private C50632Qh getImpl() {
        C50632Qh r1 = this.A0A;
        if (r1 == null) {
            if (Build.VERSION.SDK_INT >= 21) {
                r1 = new AnonymousClass2Qk(this, new C50642Qi(this));
            } else {
                r1 = new C50632Qh(this, new C50642Qi(this));
            }
            this.A0A = r1;
        }
        return r1;
    }

    @Deprecated
    public int getRippleColor() {
        ColorStateList colorStateList = this.A07;
        if (colorStateList != null) {
            return colorStateList.getDefaultColor();
        }
        return 0;
    }

    public ColorStateList getRippleColorStateList() {
        return this.A07;
    }

    public C50612Qf getShowMotionSpec() {
        return getImpl().A0F;
    }

    public int getSize() {
        return this.A04;
    }

    public int getSizeDimension() {
        return A01(this.A04);
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        return this.A05;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        return this.A08;
    }

    public ColorStateList getSupportImageTintList() {
        return this.A06;
    }

    public PorterDuff.Mode getSupportImageTintMode() {
        return this.A09;
    }

    public boolean getUseCompatPadding() {
        return this.A0B;
    }

    @Override // android.widget.ImageView, android.view.View
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        getImpl().A06();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        C50632Qh impl = getImpl();
        if (impl.A0H()) {
            if (impl.A0B == null) {
                impl.A0B = new ViewTreeObserver$OnPreDrawListenerC50672Qm(impl);
            }
            impl.A0N.getViewTreeObserver().addOnPreDrawListener(impl.A0B);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C50632Qh impl = getImpl();
        if (impl.A0B != null) {
            impl.A0N.getViewTreeObserver().removeOnPreDrawListener(impl.A0B);
            impl.A0B = null;
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int A01 = A01(this.A04);
        this.A02 = (A01 - this.A03) >> 1;
        getImpl().A09();
        int i3 = A01;
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        if (mode == Integer.MIN_VALUE) {
            i3 = Math.min(A01, size);
        } else if (mode != 0) {
            if (mode == 1073741824) {
                i3 = size;
            } else {
                throw new IllegalArgumentException();
            }
        }
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode2 == Integer.MIN_VALUE) {
            A01 = Math.min(A01, size2);
        } else if (mode2 != 0) {
            if (mode2 == 1073741824) {
                A01 = size2;
            } else {
                throw new IllegalArgumentException();
            }
        }
        int min = Math.min(i3, A01);
        Rect rect = this.A0C;
        setMeasuredDimension(rect.left + min + rect.right, min + rect.top + rect.bottom);
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof C50692Qo)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        C50692Qo r4 = (C50692Qo) parcelable;
        super.onRestoreInstanceState(((AbstractC015707l) r4).A00);
        this.A0F.A01((Bundle) r4.A00.get("expandableWidgetHelper"));
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        C50692Qo r3 = new C50692Qo(super.onSaveInstanceState());
        r3.A00.put("expandableWidgetHelper", this.A0F.A00());
        return r3;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            Rect rect = this.A0D;
            if (AnonymousClass028.A0r(this)) {
                rect.set(0, 0, getWidth(), getHeight());
                int i = rect.left;
                Rect rect2 = this.A0C;
                rect.left = i + rect2.left;
                rect.top += rect2.top;
                rect.right -= rect2.right;
                rect.bottom -= rect2.bottom;
                if (!rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                    return false;
                }
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        Log.i("FloatingActionButton", "Setting a custom background is not supported.");
    }

    @Override // android.view.View
    public void setBackgroundTintList(ColorStateList colorStateList) {
        if (this.A05 != colorStateList) {
            this.A05 = colorStateList;
            C50632Qh impl = getImpl();
            Drawable drawable = impl.A0A;
            if (drawable != null) {
                C015607k.A04(colorStateList, drawable);
            }
            C50702Qp r2 = impl.A0G;
            if (r2 != null) {
                if (colorStateList != null) {
                    r2.A04 = colorStateList.getColorForState(r2.getState(), r2.A04);
                }
                r2.A07 = colorStateList;
                r2.A08 = true;
                r2.invalidateSelf();
            }
        }
    }

    @Override // android.view.View
    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.A08 != mode) {
            this.A08 = mode;
            Drawable drawable = getImpl().A0A;
            if (drawable != null) {
                C015607k.A07(mode, drawable);
            }
        }
    }

    public void setCompatElevation(float f) {
        C50632Qh impl = getImpl();
        if (impl.A00 != f) {
            impl.A00 = f;
            impl.A0A(f, impl.A01, impl.A03);
        }
    }

    public void setCompatElevationResource(int i) {
        setCompatElevation(getResources().getDimension(i));
    }

    public void setCompatHoveredFocusedTranslationZ(float f) {
        C50632Qh impl = getImpl();
        if (impl.A01 != f) {
            impl.A01 = f;
            impl.A0A(impl.A00, f, impl.A03);
        }
    }

    public void setCompatHoveredFocusedTranslationZResource(int i) {
        setCompatHoveredFocusedTranslationZ(getResources().getDimension(i));
    }

    public void setCompatPressedTranslationZ(float f) {
        C50632Qh impl = getImpl();
        if (impl.A03 != f) {
            impl.A03 = f;
            impl.A0A(impl.A00, impl.A01, f);
        }
    }

    public void setCompatPressedTranslationZResource(int i) {
        setCompatPressedTranslationZ(getResources().getDimension(i));
    }

    public void setCustomSize(int i) {
        if (i >= 0) {
            this.A01 = i;
            return;
        }
        throw new IllegalArgumentException("Custom size must be non-negative");
    }

    public void setExpandedComponentIdHint(int i) {
        this.A0F.A00 = i;
    }

    public void setHideMotionSpec(C50612Qf r2) {
        getImpl().A0E = r2;
    }

    public void setHideMotionSpecResource(int i) {
        setHideMotionSpec(C50612Qf.A00(getContext(), i));
    }

    @Override // android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        C50632Qh impl = getImpl();
        float f = impl.A02;
        impl.A02 = f;
        Matrix matrix = impl.A0I;
        impl.A0D(matrix, f);
        impl.A0N.setImageMatrix(matrix);
    }

    @Override // android.widget.ImageView
    public void setImageResource(int i) {
        this.A0E.A01(i);
    }

    public void setRippleColor(int i) {
        setRippleColor(ColorStateList.valueOf(i));
    }

    public void setRippleColor(ColorStateList colorStateList) {
        if (this.A07 != colorStateList) {
            this.A07 = colorStateList;
            getImpl().A0B(this.A07);
        }
    }

    public void setShowMotionSpec(C50612Qf r2) {
        getImpl().A0F = r2;
    }

    public void setShowMotionSpecResource(int i) {
        setShowMotionSpec(C50612Qf.A00(getContext(), i));
    }

    public void setSize(int i) {
        this.A01 = 0;
        if (i != this.A04) {
            this.A04 = i;
            requestLayout();
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        setBackgroundTintList(colorStateList);
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        setBackgroundTintMode(mode);
    }

    @Override // X.AnonymousClass03Y
    public void setSupportImageTintList(ColorStateList colorStateList) {
        if (this.A06 != colorStateList) {
            this.A06 = colorStateList;
            A02();
        }
    }

    @Override // X.AnonymousClass03Y
    public void setSupportImageTintMode(PorterDuff.Mode mode) {
        if (this.A09 != mode) {
            this.A09 = mode;
            A02();
        }
    }

    public void setUseCompatPadding(boolean z) {
        if (this.A0B != z) {
            this.A0B = z;
            getImpl().A07();
        }
    }
}
