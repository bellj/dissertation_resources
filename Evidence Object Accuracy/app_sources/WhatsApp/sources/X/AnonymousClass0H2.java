package X;

import android.graphics.PointF;
import java.util.List;

/* renamed from: X.0H2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0H2 extends AnonymousClass0H3 {
    public final PointF A00 = new PointF();

    public AnonymousClass0H2(List list) {
        super(list);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A04(AnonymousClass0U8 r2, float f) {
        return A09(r2, f, f);
    }

    @Override // X.AnonymousClass0QR
    public /* bridge */ /* synthetic */ Object A05(AnonymousClass0U8 r2, float f, float f2, float f3) {
        return A09(r2, f2, f3);
    }

    public PointF A09(AnonymousClass0U8 r7, float f, float f2) {
        Object obj;
        Object obj2 = r7.A0F;
        if (obj2 == null || (obj = r7.A09) == null) {
            throw new IllegalStateException("Missing values for keyframe.");
        }
        PointF pointF = (PointF) obj2;
        PointF pointF2 = (PointF) obj;
        AnonymousClass0SF r1 = this.A03;
        if (r1 != null) {
            r7.A08.floatValue();
            A02();
            AnonymousClass0NB r0 = r1.A02;
            r0.A01 = pointF;
            r0.A00 = pointF2;
            PointF pointF3 = (PointF) r1.A01;
            if (pointF3 != null) {
                return pointF3;
            }
        }
        PointF pointF4 = this.A00;
        float f3 = pointF.x;
        float f4 = pointF.y;
        pointF4.set(f3 + (f * (pointF2.x - f3)), f4 + (f2 * (pointF2.y - f4)));
        return pointF4;
    }
}
