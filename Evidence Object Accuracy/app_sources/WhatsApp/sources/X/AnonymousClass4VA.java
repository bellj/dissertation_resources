package X;

/* renamed from: X.4VA  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4VA {
    public final String A00;

    public AnonymousClass4VA(String str) {
        this.A00 = str;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("<");
        A0k.append(this.A00);
        return C72463ee.A0H(A0k, '>');
    }
}
