package X;

import android.util.Log;

/* renamed from: X.4wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107054wi implements AnonymousClass5WZ {
    public final int A00;
    public final int A01;
    public final C95304dT A02;

    public C107054wi(C100614mC r6, C76873mN r7) {
        C95304dT r4 = r7.A00;
        this.A02 = r4;
        int A02 = C95304dT.A02(r4, 12);
        if ("audio/raw".equals(r6.A0T)) {
            int A03 = AnonymousClass3JZ.A03(r6.A0B, r6.A06);
            if (A02 == 0 || A02 % A03 != 0) {
                StringBuilder A0k = C12960it.A0k("Audio sample size mismatch. stsd sample size: ");
                A0k.append(A03);
                Log.w("AtomParsers", C12960it.A0e(", stsz sample size: ", A0k, A02));
                A02 = A03;
            }
            this.A00 = A02;
            this.A01 = r4.A0E();
        }
        if (A02 == 0) {
            A02 = -1;
        }
        this.A00 = A02;
        this.A01 = r4.A0E();
    }

    @Override // X.AnonymousClass5WZ
    public int AD3() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WZ
    public int AGN() {
        return this.A01;
    }

    @Override // X.AnonymousClass5WZ
    public int AZv() {
        int i = this.A00;
        return i == -1 ? this.A02.A0E() : i;
    }
}
