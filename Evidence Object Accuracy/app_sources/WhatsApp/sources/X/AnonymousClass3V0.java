package X;

import com.whatsapp.biz.product.viewmodel.ComplianceInfoViewModel;

/* renamed from: X.3V0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3V0 implements AnonymousClass285 {
    public final /* synthetic */ ComplianceInfoViewModel A00;
    public final /* synthetic */ String A01;

    public AnonymousClass3V0(ComplianceInfoViewModel complianceInfoViewModel, String str) {
        this.A00 = complianceInfoViewModel;
        this.A01 = str;
    }

    @Override // X.AnonymousClass285
    public void AQP(String str, int i) {
        AnonymousClass016 r1;
        int i2;
        boolean equals = this.A01.equals(str);
        ComplianceInfoViewModel complianceInfoViewModel = this.A00;
        if (equals) {
            complianceInfoViewModel.A04.A0M.remove(this);
            r1 = complianceInfoViewModel.A01;
            i2 = 3;
        } else {
            r1 = complianceInfoViewModel.A01;
            i2 = 2;
        }
        C12960it.A1A(r1, i2);
    }

    @Override // X.AnonymousClass285
    public void AQQ(AnonymousClass4TA r5, String str) {
        AnonymousClass016 r1;
        int i;
        AnonymousClass3M1 r12;
        String str2 = this.A01;
        if (str2.equals(str)) {
            ComplianceInfoViewModel complianceInfoViewModel = this.A00;
            complianceInfoViewModel.A04.A0M.remove(this);
            C44691zO A05 = complianceInfoViewModel.A02.A05(null, str2);
            if (!(A05 == null || (r12 = A05.A09) == null)) {
                complianceInfoViewModel.A00.A0B(r12);
                r1 = complianceInfoViewModel.A01;
                i = 1;
                C12960it.A1A(r1, i);
            }
        }
        r1 = this.A00.A01;
        i = 2;
        C12960it.A1A(r1, i);
    }
}
