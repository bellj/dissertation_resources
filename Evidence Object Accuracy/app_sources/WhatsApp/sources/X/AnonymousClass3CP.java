package X;

import android.net.Uri;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.3CP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3CP {
    public final C14830m7 A00;

    public AnonymousClass3CP(C14830m7 r1) {
        this.A00 = r1;
    }

    public AbstractC455722e A00(Uri uri, UserJid userJid) {
        String queryParameter;
        String str;
        if (!(userJid == null || uri == null || (queryParameter = uri.getQueryParameter("source")) == null)) {
            if (queryParameter.length() > 32) {
                str = "CTWA: LoggingEntryPointFactory/fromDeepLink/ctwa ads source exceeds max length";
            } else {
                String queryParameter2 = uri.getQueryParameter("data");
                if (queryParameter2 != null) {
                    if (queryParameter2.length() <= 512) {
                        return new C455922g(userJid, queryParameter2, queryParameter, System.currentTimeMillis());
                    }
                    str = "CTWA: LoggingEntryPointFactory/fromDeepLink/ctwa ads payload exceeds max length";
                }
            }
            Log.w(str);
        }
        return new C456322k();
    }
}
