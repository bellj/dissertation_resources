package X;

import android.content.SharedPreferences;

/* renamed from: X.1o7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38451o7 {
    public static boolean A00(C14830m7 r10, C14820m6 r11, C14850m9 r12, AbstractC15340mz r13) {
        if (!r12.A07(249)) {
            return false;
        }
        if ((!(r13 instanceof AnonymousClass1X7) && !(r13 instanceof AnonymousClass1X3)) || !C15380n4.A0N(r13.A0z.A00) || r12.A07(1116)) {
            return false;
        }
        SharedPreferences sharedPreferences = r11.A00;
        if ((sharedPreferences.getLong("status_tab_last_opened_time", 0) == 0 || r10.A00() - sharedPreferences.getLong("status_tab_last_opened_time", 0) > 1209600000) && (sharedPreferences.getLong("registration_initialized_time", 0) <= 0 || System.currentTimeMillis() - sharedPreferences.getLong("registration_initialized_time", 0) > 1209600000)) {
            return false;
        }
        return true;
    }
}
