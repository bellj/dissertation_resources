package X;

import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryActivity;

/* renamed from: X.3MQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MQ implements MenuItem.OnActionExpandListener {
    public final /* synthetic */ MediaGalleryActivity A00;

    public AnonymousClass3MQ(MediaGalleryActivity mediaGalleryActivity) {
        this.A00 = mediaGalleryActivity;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        MediaGalleryActivity mediaGalleryActivity = this.A00;
        mediaGalleryActivity.A0p = null;
        ((C53072cg) mediaGalleryActivity.findViewById(R.id.toolbar).getLayoutParams()).A00 = 21;
        return true;
    }

    @Override // android.view.MenuItem.OnActionExpandListener
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        ((C53072cg) this.A00.findViewById(R.id.toolbar).getLayoutParams()).A00 = 0;
        return true;
    }
}
