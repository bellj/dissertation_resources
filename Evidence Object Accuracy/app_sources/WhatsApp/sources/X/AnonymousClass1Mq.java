package X;

/* renamed from: X.1Mq  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Mq extends AnonymousClass1Mr<Object> {
    public final transient Object[] alternatingKeysAndValues;
    public final transient int offset;
    public final transient int size;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return true;
    }

    public AnonymousClass1Mq(Object[] objArr, int i, int i2) {
        this.alternatingKeysAndValues = objArr;
        this.offset = i;
        this.size = i2;
    }

    @Override // java.util.List
    public Object get(int i) {
        C28291Mn.A01(i, this.size);
        return this.alternatingKeysAndValues[(i << 1) + this.offset];
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.size;
    }
}
