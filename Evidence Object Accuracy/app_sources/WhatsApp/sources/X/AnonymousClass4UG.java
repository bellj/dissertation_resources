package X;

import android.app.ActivityManager;
import android.content.Context;
import java.util.Set;

/* renamed from: X.4UG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4UG {
    public final Context A00;
    public final AnonymousClass0O1 A01;
    public final AnonymousClass0O1 A02;
    public final AbstractC12220hZ A03;
    public final AbstractC12220hZ A04;
    public final AbstractC11580gW A05;
    public final C87764Cv A06 = new C87764Cv();
    public final AnonymousClass4FR A07;
    public final AnonymousClass4FS A08;
    public final AnonymousClass4I2 A09;
    public final AnonymousClass5Pk A0A;
    public final AnonymousClass4I3 A0B;
    public final AnonymousClass4Cx A0C;
    public final AnonymousClass4XR A0D;
    public final C93274Zu A0E;
    public final Set A0F;
    public final Set A0G;

    public /* synthetic */ AnonymousClass4UG(AnonymousClass4SK r4) {
        AnonymousClass4FR r0;
        AnonymousClass4FS r02;
        C08650bZ r03;
        AnonymousClass4Yy.A00();
        this.A0B = new AnonymousClass4I3(r4.A04);
        Context context = r4.A03;
        this.A03 = new C105904uo((ActivityManager) context.getSystemService("activity"));
        synchronized (AnonymousClass4FR.class) {
            r0 = AnonymousClass4FR.A00;
            if (r0 == null) {
                r0 = new AnonymousClass4FR();
                AnonymousClass4FR.A00 = r0;
            }
        }
        this.A07 = r0;
        this.A00 = context;
        this.A09 = new AnonymousClass4I2(new C87774Cw());
        synchronized (AnonymousClass4FS.class) {
            r02 = AnonymousClass4FS.A00;
            if (r02 == null) {
                r02 = new AnonymousClass4FS();
                AnonymousClass4FS.A00 = r02;
            }
        }
        this.A08 = r02;
        this.A04 = new C105894un(this);
        try {
            AnonymousClass4Yy.A00();
            AnonymousClass0O1 r2 = new AnonymousClass0O1(new AnonymousClass0NU(context));
            AnonymousClass4Yy.A00();
            this.A01 = r2;
            synchronized (C08650bZ.class) {
                r03 = C08650bZ.A00;
                if (r03 == null) {
                    r03 = new C08650bZ();
                    C08650bZ.A00 = r03;
                }
            }
            this.A05 = r03;
            int i = r4.A00;
            i = i < 0 ? 30000 : i;
            AnonymousClass4Yy.A00();
            this.A0E = new C93274Zu(i);
            AnonymousClass4Yy.A00();
            new AnonymousClass4D0();
            AnonymousClass4XR r1 = new AnonymousClass4XR(new C91954Tw());
            this.A0D = r1;
            new C87784Cy();
            this.A0G = C12970iu.A12();
            this.A0F = C12970iu.A12();
            this.A02 = r2;
            this.A0A = new C106074v5(r1.A08.A03.A00);
            this.A0C = r4.A01;
        } finally {
            AnonymousClass4Yy.A00();
        }
    }
}
