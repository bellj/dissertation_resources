package X;

import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

/* renamed from: X.0co  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09380co implements Runnable {
    public final /* synthetic */ C05150Ol A00;

    public RunnableC09380co(C05150Ol r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        C05150Ol r2 = this.A00;
        FrameLayout frameLayout = r2.A00;
        if (frameLayout != null) {
            ViewParent parent = frameLayout.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(r2.A00);
            }
            r2.A00.removeAllViews();
        }
    }
}
