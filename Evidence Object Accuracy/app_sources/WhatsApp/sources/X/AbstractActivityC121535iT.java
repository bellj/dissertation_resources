package X;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;
import com.whatsapp.payments.ui.IndiaUpiOnboardingErrorEducationActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* renamed from: X.5iT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121535iT extends AbstractActivityC121575iX implements AnonymousClass6MP {
    public AnonymousClass102 A00;
    public C119715ez A01;
    public C452120p A02;
    public C64513Fv A03;
    public C120545gM A04;
    public AnonymousClass69E A05;
    public C122205l5 A06;
    public ArrayList A07;
    public ArrayList A08;
    public boolean A09 = false;
    public final C30931Zj A0A = C117315Zl.A0D("IndiaUpiPaymentBankSetupActivity");

    public final void A30(C119715ez r5, C452120p r6, ArrayList arrayList, ArrayList arrayList2) {
        AnonymousClass60V r0;
        if (C120545gM.A00(r5, ((AbstractActivityC121665jA) this).A0B, arrayList, arrayList2)) {
            A33(((AbstractActivityC121665jA) this).A0A.A05);
            return;
        }
        if (r6 == null) {
            C30931Zj r2 = this.A0A;
            StringBuilder A0k = C12960it.A0k("onBanksList empty. showErrorAndFinish error: ");
            C117315Zl.A0V(this.A03, "upi-get-banks", A0k);
            C117295Zj.A1F(r2, A0k);
            r0 = this.A05.A04(this.A03, 0);
        } else if (!AnonymousClass69E.A02(this, "upi-get-banks", r6.A00, true)) {
            boolean A07 = this.A03.A07("upi-get-banks");
            C30931Zj r22 = this.A0A;
            if (A07) {
                StringBuilder A0k2 = C12960it.A0k("onBanksList failure. Retry sendGetBanksList error: ");
                C117315Zl.A0V(this.A03, "upi-get-banks", A0k2);
                C117295Zj.A1F(r22, A0k2);
                this.A04.A01();
                ((AbstractActivityC121665jA) this).A0D.AeG();
                return;
            }
            StringBuilder A0k3 = C12960it.A0k("onBanksList failure. showErrorAndFinish error: ");
            C117315Zl.A0V(this.A03, "upi-get-banks", A0k3);
            C117295Zj.A1F(r22, A0k3);
            r0 = this.A05.A04(this.A03, r6.A00);
        } else {
            return;
        }
        A32(r0);
    }

    public final void A31(C452120p r7) {
        if (!AnonymousClass69E.A02(this, "upi-batch", r7.A00, false)) {
            C30931Zj r2 = this.A0A;
            StringBuilder A0k = C12960it.A0k("onBatchError: ");
            A0k.append(r7);
            r2.A06(C12960it.A0d("; showErrorAndFinish", A0k));
            int i = r7.A00;
            if (i == 21129) {
                AnonymousClass6G6 r5 = new Runnable() { // from class: X.6G6
                    @Override // java.lang.Runnable
                    public final void run() {
                        AbstractActivityC121535iT.this.finish();
                    }
                };
                C004802e A0S = C12980iv.A0S(this);
                A0S.A07(R.string.upi_mandate_account_reregistration_with_active_mandate_error_title);
                A0S.A06(R.string.upi_mandate_account_reregistration_with_active_mandate_error_messsage);
                A0S.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(r5) { // from class: X.62U
                    public final /* synthetic */ Runnable A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        AbstractActivityC121665jA r0 = AbstractActivityC121665jA.this;
                        Runnable runnable = this.A01;
                        dialogInterface.dismiss();
                        new Handler(r0.getMainLooper()).post(runnable);
                    }
                });
                A0S.A0B(false);
                A0S.A05();
                return;
            }
            A32(this.A05.A04(this.A03, i));
        }
    }

    public final void A32(AnonymousClass60V r4) {
        int i;
        AbstractActivityC119235dO.A1j(this.A06, 3);
        C30931Zj r2 = this.A0A;
        StringBuilder A0k = C12960it.A0k("showErrorAndFinish: ");
        A0k.append(r4.A00);
        C117295Zj.A1F(r2, A0k);
        A2r();
        if (r4.A00 == 0) {
            r4.A00 = R.string.payments_setup_error;
            String str = this.A03.A04;
            if ("upi-batch".equalsIgnoreCase(str)) {
                i = R.string.payments_error_create_payment_account;
            } else if ("upi-get-banks".equalsIgnoreCase(str)) {
                i = R.string.payments_error_banks_list;
            }
            r4.A00 = i;
        }
        if (((AbstractActivityC121665jA) this).A0N) {
            A2q();
            Intent A0D = C12990iw.A0D(this, IndiaUpiOnboardingErrorEducationActivity.class);
            if (r4.A01 != null) {
                A0D.putExtra("error_text", r4.A01(this));
            }
            A0D.putExtra("error", r4.A00);
            AbstractActivityC119235dO.A0n(A0D, this);
            return;
        }
        AbstractActivityC119235dO.A1J(this, r4);
    }

    public void A33(List list) {
        IndiaUpiBankPickerActivity indiaUpiBankPickerActivity = (IndiaUpiBankPickerActivity) this;
        ArrayList A0x = C12980iv.A0x(list);
        Collections.sort(A0x, new Comparator() { // from class: X.6KF
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                String A0B = ((AbstractC30851Zb) obj).A0B();
                AnonymousClass009.A05(A0B);
                String A0B2 = ((AbstractC30851Zb) obj2).A0B();
                AnonymousClass009.A05(A0B2);
                return A0B.compareTo(A0B2);
            }
        });
        indiaUpiBankPickerActivity.A0H = A0x;
        int i = 0;
        if (list == null || list.isEmpty()) {
            int i2 = 0;
            do {
                indiaUpiBankPickerActivity.A03.addView(View.inflate(indiaUpiBankPickerActivity, R.layout.india_upi_payment_bank_picker_list_row_shimmer, null), new LinearLayout.LayoutParams(-1, -2));
                i2++;
            } while (i2 < 25);
            indiaUpiBankPickerActivity.A02.setVisibility(0);
            indiaUpiBankPickerActivity.A00.setVisibility(8);
            indiaUpiBankPickerActivity.A04.setVisibility(8);
            ((ShimmerFrameLayout) indiaUpiBankPickerActivity.A02).A02();
            return;
        }
        indiaUpiBankPickerActivity.A00.setVisibility(0);
        indiaUpiBankPickerActivity.A02.setVisibility(8);
        indiaUpiBankPickerActivity.A04.setVisibility(8);
        ((ShimmerFrameLayout) indiaUpiBankPickerActivity.A02).A03();
        indiaUpiBankPickerActivity.A03.removeAllViews();
        List<C119755f3> list2 = indiaUpiBankPickerActivity.A0H;
        ArrayList A0l = C12960it.A0l();
        for (C119755f3 r1 : list2) {
            if (r1.A0I) {
                A0l.add(r1);
            }
        }
        ArrayList A0l2 = C12960it.A0l();
        Character ch = null;
        for (AbstractC30851Zb r4 : list2) {
            String A0B = r4.A0B();
            AnonymousClass009.A04(A0B);
            char charAt = A0B.charAt(0);
            if (ch == null || ch.charValue() != charAt) {
                ch = Character.valueOf(charAt);
                A0l2.add(ch.toString());
            }
            A0l2.add(r4);
        }
        AnonymousClass01T A05 = C117315Zl.A05(A0l, A0l2);
        indiaUpiBankPickerActivity.A0I = (List) A05.A00;
        List list3 = (List) A05.A01;
        indiaUpiBankPickerActivity.A0J = list3;
        C118595c4 r0 = indiaUpiBankPickerActivity.A0C;
        r0.A00 = list3;
        r0.A02();
        C118595c4 r12 = indiaUpiBankPickerActivity.A0B;
        r12.A00 = indiaUpiBankPickerActivity.A0I;
        r12.A02();
        View view = indiaUpiBankPickerActivity.A01;
        List list4 = indiaUpiBankPickerActivity.A0I;
        if (list4 == null || list4.isEmpty()) {
            i = 8;
        }
        view.setVisibility(i);
        ((AbstractActivityC121535iT) indiaUpiBankPickerActivity).A06.A00.A09("bankPickerShown");
    }

    @Override // X.AnonymousClass6MP
    public void AN9(C119715ez r5, C452120p r6, ArrayList arrayList, ArrayList arrayList2) {
        Object valueOf;
        C30931Zj r3 = this.A0A;
        StringBuilder A0k = C12960it.A0k("banks returned: ");
        if (arrayList == null) {
            valueOf = "null";
        } else {
            valueOf = Integer.valueOf(arrayList.size());
        }
        A0k.append(valueOf);
        C117295Zj.A1F(r3, A0k);
        boolean z = !((AbstractActivityC121685jC) this).A0I.A0C();
        AnonymousClass6BE r1 = ((AbstractActivityC121665jA) this).A0D;
        int i = 4;
        if (z) {
            i = 3;
        }
        AnonymousClass2SP A01 = r1.A01(r6, i);
        A01.A0Z = "nav_bank_select";
        AbstractActivityC119235dO.A1b(A01, this);
        r3.A06(C12960it.A0b("logBanksList: ", A01));
        this.A08 = arrayList;
        this.A07 = arrayList2;
        this.A01 = r5;
        this.A02 = r6;
        if (!((AbstractActivityC121665jA) this).A0P) {
            A30(r5, r6, arrayList, arrayList2);
        }
    }

    @Override // X.AnonymousClass6MP
    public void ANA(C452120p r5) {
        AnonymousClass2SP A01 = ((AbstractActivityC121665jA) this).A0D.A01(r5, 3);
        A01.A0Z = "nav_bank_select";
        AbstractActivityC119235dO.A1b(A01, this);
        this.A0A.A06(C12960it.A0b("logBanksList: ", A01));
        if (!((AbstractActivityC121665jA) this).A0P) {
            A31(r5);
            return;
        }
        this.A09 = true;
        this.A02 = r5;
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        C30931Zj r2 = this.A0A;
        StringBuilder A0k = C12960it.A0k("onActivityResult: request: ");
        A0k.append(i);
        A0k.append(" result: ");
        r2.A0A(C12960it.A0f(A0k, i2), null);
        if (i != 1000) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 != -1) {
            A2q();
            finish();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.A03 = ((AbstractActivityC121665jA) this).A0A.A04;
        C118025b9 A00 = this.A0Y.A00(this);
        this.A0X = A00;
        C14900mE r2 = ((ActivityC13810kN) this).A05;
        C17220qS r4 = ((AbstractActivityC121685jC) this).A0H;
        C17070qD r9 = ((AbstractActivityC121685jC) this).A0P;
        C1308460e r5 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r8 = ((AbstractActivityC121685jC) this).A0M;
        this.A04 = new C120545gM(this, r2, this.A00, r4, r5, ((AbstractActivityC121665jA) this).A0B, ((AbstractActivityC121685jC) this).A0K, r8, r9, this, A00);
        onConfigurationChanged(C12980iv.A0H(this));
    }

    @Override // X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A04.A00 = null;
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (!isFinishing()) {
            C30931Zj r2 = this.A0A;
            StringBuilder A0k = C12960it.A0k("bank setup onResume states: ");
            A0k.append(this.A03);
            C117295Zj.A1F(r2, A0k);
            ArrayList arrayList = ((AbstractActivityC121665jA) this).A0A.A05;
            if (arrayList == null) {
                boolean A0C = ((AbstractActivityC121685jC) this).A0I.A0C();
                C120545gM r9 = this.A04;
                if (!A0C) {
                    Log.i("PAY: IndiaUpiPaymentSetup createPaymentAccountBatch called");
                    C64513Fv r8 = ((C126705tJ) r9).A00;
                    r8.A04("upi-batch");
                    C17220qS r3 = r9.A04;
                    String A01 = r3.A01();
                    C117295Zj.A1B(r3, new C120595gR(r9.A01, r9.A02, r9.A07, r8, r9), new C126465sv(new AnonymousClass3CT(A01)).A00, A01);
                } else {
                    r9.A01();
                }
                ((AbstractActivityC121665jA) this).A0D.AeG();
                return;
            }
            A33(arrayList);
        }
    }
}
