package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.2uF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59252uF extends AnonymousClass32K {
    public final C63923Dl A00;
    public final C19830uk A01;
    public final C91864Tn A02;
    public final C59022tq A03;
    public final C18640sm A04;
    public final C19840ul A05;

    public C59252uF(C63923Dl r9, C19810ui r10, C17560r0 r11, AnonymousClass4RQ r12, C17570r1 r13, C19830uk r14, C91864Tn r15, C59022tq r16, C18640sm r17, C19840ul r18, AbstractC14440lR r19) {
        super(r10, r11, r12, r13, r19, 5);
        this.A05 = r18;
        this.A04 = r17;
        this.A02 = r15;
        this.A00 = r9;
        this.A01 = r14;
        this.A03 = r16;
    }

    @Override // X.AnonymousClass44L
    public void A00(AnonymousClass3H5 r4, JSONObject jSONObject, int i) {
        A04();
        Log.e("GetSingleCollectionGraphQLService/sendRequest/onErrorResponse");
        if (!A03(this.A02.A03, r4.A00, true)) {
            this.A00.A00(i);
        }
    }

    public final void A04() {
        if (this.A02.A04 == null) {
            this.A05.A02("view_collection_details_tag");
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        A04();
        Log.e("GetSingleCollectionGraphQLService/sendRequest/onDeliveryFailure");
        if (!A03(this.A02.A03, -1, false)) {
            this.A00.A00(-1);
        }
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e("GetSingleCollectionGraphQLServicesendRequest/direct-connection-error");
        this.A00.A00(422);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        Log.i("GetSingleCollectionGraphQLService/onDirectConnectionSucceeded/retry-request");
        A02();
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        A04();
        Log.e("GetSingleCollectionGraphQLService/sendRequest/onError");
        if (!A03(this.A02.A03, 0, false)) {
            this.A00.A00(0);
        }
    }
}
