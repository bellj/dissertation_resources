package X;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import com.whatsapp.util.Log;

/* renamed from: X.1UW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UW implements LocationListener {
    public final /* synthetic */ C14860mA A00;
    public final /* synthetic */ C14880mC A01;

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AnonymousClass1UW(C14860mA r1, C14880mC r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        if (location != null) {
            StringBuilder sb = new StringBuilder("qrsession/location/changed ");
            sb.append(location.getTime());
            sb.append(" ");
            sb.append(location.getAccuracy());
            Log.i(sb.toString());
            C14860mA r3 = this.A00;
            r3.A0M.Ab2(new RunnableBRunnable0Shape2S0300000_I0_2(location, r3, this.A01));
            r3.A0D.A04(this);
        }
    }
}
