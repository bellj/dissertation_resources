package X;

/* renamed from: X.13E  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass13E {
    public String A00() {
        if (this instanceof AnonymousClass13G) {
            return "message_revoked";
        }
        if (this instanceof AnonymousClass13I) {
            return "incoming_message";
        }
        if (!(this instanceof AnonymousClass13J)) {
            return !(this instanceof AnonymousClass13D) ? "authorization_revoked" : "call_ended";
        }
        return "call_state_changed";
    }
}
