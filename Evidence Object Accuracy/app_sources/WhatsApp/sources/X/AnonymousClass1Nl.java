package X;

import com.whatsapp.util.Log;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.1Nl  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Nl implements AbstractC28461Nh {
    public final /* synthetic */ AnonymousClass1Nk A00;
    public final /* synthetic */ AnonymousClass16A A01;

    public AnonymousClass1Nl(AnonymousClass1Nk r1, AnonymousClass16A r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void AOt(long j) {
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        StringBuilder sb = new StringBuilder("app/CrashLogs/uploadServerOkay/error received: ");
        sb.append(str);
        Log.e(sb.toString());
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        try {
            if ("only_exception".equals(new JSONObject(str).optString("upload", ""))) {
                this.A00.A00 = "exception_only";
            } else {
                this.A00.A00 = "exception_and_logs";
            }
        } catch (JSONException unused) {
            this.A00.A00 = "exception_and_logs";
        }
    }
}
