package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.5nd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123665nd extends C123675ne {
    public C123665nd(Context context) {
        super(context);
    }

    @Override // X.C123675ne
    public int getLayoutRes() {
        return R.layout.payment_incentive_nux_view;
    }
}
