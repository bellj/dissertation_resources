package X;

import android.app.KeyguardManager;
import android.content.Intent;

/* renamed from: X.0KG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KG {
    public static Intent A00(KeyguardManager keyguardManager, CharSequence charSequence, CharSequence charSequence2) {
        return keyguardManager.createConfirmDeviceCredentialIntent(charSequence, charSequence2);
    }
}
