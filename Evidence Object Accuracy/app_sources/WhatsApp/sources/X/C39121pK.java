package X;

import java.io.File;

/* renamed from: X.1pK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39121pK extends AbstractC39111pJ {
    public final long A00;
    public final long A01;
    public final AnonymousClass3JD A02;
    public final File A03;

    public C39121pK(C39741qT r10, AbstractC14470lU r11, C39781qX r12, C39771qW r13, AbstractC39761qV r14, AnonymousClass3JD r15, File file, File file2, long j, long j2) {
        super(r10, r11, r12, r13, r14, file);
        this.A00 = j;
        this.A01 = j2;
        this.A02 = r15;
        this.A03 = file2;
    }
}
