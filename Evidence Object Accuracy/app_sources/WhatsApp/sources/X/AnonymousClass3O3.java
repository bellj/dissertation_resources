package X;

import android.widget.AbsListView;

/* renamed from: X.3O3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3O3 implements AbsListView.OnScrollListener {
    public final /* synthetic */ AnonymousClass3IZ A00;

    public AnonymousClass3O3(AnonymousClass3IZ r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0048, code lost:
        if (r5 <= 1.0f) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(android.widget.AbsListView r9) {
        /*
            r8 = this;
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0061
            X.3IZ r4 = r8.A00
            android.view.ViewGroup r3 = r4.A0G
            if (r3 == 0) goto L_0x0061
            r1 = 1061997773(0x3f4ccccd, float:0.8)
            android.content.res.Resources r0 = X.C12960it.A09(r9)
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            float r2 = r0.density
            float r2 = r2 * r1
            r0 = 1056964608(0x3f000000, float:0.5)
            float r2 = r2 + r0
            int r1 = r9.getFirstVisiblePosition()
            r0 = 0
            android.view.View r0 = r9.getChildAt(r0)
            r7 = 0
            if (r0 != 0) goto L_0x0062
            r0 = 0
        L_0x002a:
            r6 = 1065353216(0x3f800000, float:1.0)
            r5 = 1065353216(0x3f800000, float:1.0)
            if (r1 != 0) goto L_0x004a
            float r5 = -r0
            android.content.res.Resources r1 = X.C12960it.A09(r9)
            r0 = 2131165921(0x7f0702e1, float:1.7946073E38)
            int r0 = r1.getDimensionPixelSize(r0)
            float r0 = (float) r0
            float r5 = r5 / r0
            float r5 = java.lang.Math.min(r5, r6)
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x004b
            int r0 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x004b
        L_0x004a:
            float r2 = r2 * r5
        L_0x004b:
            int r1 = r4.A09
            r0 = 1095761920(0x41500000, float:13.0)
            float r5 = r5 * r0
            int r0 = (int) r5
            int r1 = X.C016907y.A06(r1, r0)
            int r0 = r4.A07
            int r0 = X.C016907y.A05(r1, r0)
            r3.setBackgroundColor(r0)
            X.AnonymousClass028.A0V(r3, r2)
        L_0x0061:
            return
        L_0x0062:
            int r0 = r0.getTop()
            float r0 = (float) r0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3O3.A00(android.widget.AbsListView):void");
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        A00(absListView);
        this.A00.A0I.onScroll(absListView, i, i2, i3);
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
        A00(absListView);
        this.A00.A0I.onScrollStateChanged(absListView, i);
    }
}
