package X;

import android.content.Context;
import com.whatsapp.profile.ProfileInfoActivity;

/* renamed from: X.4qT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103264qT implements AbstractC009204q {
    public final /* synthetic */ ProfileInfoActivity A00;

    public C103264qT(ProfileInfoActivity profileInfoActivity) {
        this.A00 = profileInfoActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
