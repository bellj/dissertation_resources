package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57532nD extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57532nD A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public long A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;
    public String A05 = "";

    static {
        C57532nD r0 = new C57532nD();
        A06 = r0;
        r0.A0W();
    }

    public C57532nD() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A04 = r0;
        this.A03 = r0;
        this.A02 = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57532nD r5 = (C57532nD) obj2;
                this.A04 = r7.Afm(this.A04, r5.A04, C12960it.A1R(this.A00), C12960it.A1R(r5.A00));
                int i = this.A00;
                boolean A1V = C12960it.A1V(i & 2, 2);
                long j = this.A01;
                int i2 = r5.A00;
                this.A01 = r7.Afs(j, r5.A01, A1V, C12960it.A1V(i2 & 2, 2));
                this.A03 = r7.Afm(this.A03, r5.A03, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A02 = r7.Afm(this.A02, r5.A02, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r5.A00 & 8, 8));
                int i3 = this.A00;
                boolean A1V2 = C12960it.A1V(i3 & 16, 16);
                String str = this.A05;
                int i4 = r5.A00;
                this.A05 = r7.Afy(str, r5.A05, A1V2, C12960it.A1V(i4 & 16, 16));
                if (r7 == C463025i.A00) {
                    this.A00 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            this.A00 |= 1;
                            this.A04 = r72.A08();
                        } else if (A03 == 16) {
                            this.A00 |= 2;
                            this.A01 = r72.A06();
                        } else if (A03 == 26) {
                            this.A00 |= 4;
                            this.A03 = r72.A08();
                        } else if (A03 == 34) {
                            this.A00 |= 8;
                            this.A02 = r72.A08();
                        } else if (A03 == 42) {
                            String A0A = r72.A0A();
                            this.A00 |= 16;
                            this.A05 = A0A;
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57532nD();
            case 5:
                return new C82413va();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C57532nD.class) {
                        if (A07 == null) {
                            A07 = AbstractC27091Fz.A09(A06);
                        }
                    }
                }
                return A07;
            default:
                throw C12970iu.A0z();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = AbstractC27091Fz.A05(this.A04, 1, 0);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A05(2, this.A01);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A03, 3, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A05(this.A02, 4, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A05, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A04, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A03, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A02, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A05);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
