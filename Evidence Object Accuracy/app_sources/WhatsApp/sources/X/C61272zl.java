package X;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2zl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61272zl extends AbstractC37911nD {
    public int A00;
    public final List A01;

    public C61272zl(C37891nB r3, InputStream inputStream, int[] iArr) {
        super(r3, inputStream);
        ArrayList A0l = C12960it.A0l();
        this.A01 = A0l;
        A0l.addAll(C63073Ad.A00(null, iArr));
    }
}
