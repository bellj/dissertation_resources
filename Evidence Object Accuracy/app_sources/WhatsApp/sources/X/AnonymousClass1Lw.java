package X;

import android.os.ConditionVariable;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import org.whispersystems.jobqueue.Job;

/* renamed from: X.1Lw  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Lw extends Thread {
    public final ConditionVariable A00 = new ConditionVariable(true);
    public final /* synthetic */ AnonymousClass1Lv A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1Lw(AnonymousClass1Lv r3) {
        super("ReadyJobsProducer");
        this.A01 = r3;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        boolean z;
        Job job;
        while (true) {
            ConditionVariable conditionVariable = this.A00;
            conditionVariable.block();
            conditionVariable.close();
            AnonymousClass1Lv r6 = this.A01;
            synchronized (r6) {
                LinkedList linkedList = r6.A01;
                z = false;
                job = null;
                if (!linkedList.isEmpty()) {
                    ListIterator listIterator = linkedList.listIterator();
                    HashMap hashMap = new HashMap();
                    while (true) {
                        if (!listIterator.hasNext()) {
                            break;
                        }
                        Job job2 = (Job) listIterator.next();
                        String str = job2.parameters.groupId;
                        if ((str == null || !r6.A03.contains(str)) && job2.A02()) {
                            if (r6.A06 && !TextUtils.isEmpty(job2.parameters.groupId) && hashMap.containsKey(job2.parameters.groupId)) {
                                Job job3 = (Job) hashMap.get(job2.parameters.groupId);
                                if (job3.A02()) {
                                    AnonymousClass2C5 r0 = r6.A00;
                                    if (r0 != null) {
                                        r0.A00.A01.AaV("JobQueue/DeterministicJobSelection/Fixed", null, false);
                                    }
                                    while (listIterator.hasPrevious() && listIterator.previous() != job3) {
                                    }
                                    hashMap.remove(job2.parameters.groupId);
                                    job2 = job3;
                                } else {
                                    AnonymousClass2C5 r02 = r6.A00;
                                    if (r02 != null) {
                                        r02.A00.A01.AaV("JobQueue/DeterministicJobSelection/Broken", null, false);
                                    }
                                }
                            }
                            listIterator.remove();
                            String str2 = job2.parameters.groupId;
                            if (str2 != null) {
                                r6.A03.add(str2);
                                int A00 = r6.A00(str2);
                                if (A00 != 0) {
                                    if (A00 == 1) {
                                        r6.A02.remove(str2);
                                    } else {
                                        r6.A02.put(str2, Integer.valueOf(A00 - 1));
                                    }
                                }
                            }
                            if (!r6.A04.offer(job2)) {
                                z = listIterator.hasNext();
                                job = job2;
                                break;
                            }
                        } else if (r6.A06 && !TextUtils.isEmpty(job2.parameters.groupId) && !hashMap.containsKey(job2.parameters.groupId)) {
                            hashMap.put(job2.parameters.groupId, job2);
                        }
                    }
                }
            }
            if (job != null) {
                if (z) {
                    conditionVariable.open();
                }
                try {
                    r6.A04.put(job);
                } catch (InterruptedException unused) {
                }
            }
        }
    }
}
