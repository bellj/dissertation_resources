package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0100100_I1;
import java.util.Collection;
import java.util.concurrent.Executor;

/* renamed from: X.2r8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58652r8 extends AbstractRunnableC14570le {
    public final long A00;
    public final AnonymousClass02N A01;
    public final AbstractC15710nm A02;
    public final String A03;
    public final Collection A04;

    public C58652r8(AbstractC15710nm r7, C14900mE r8, String str, Collection collection, long j) {
        this.A02 = r7;
        this.A03 = str;
        this.A04 = collection;
        this.A00 = j;
        Executor executor = r8.A06;
        AnonymousClass02N r2 = new AnonymousClass02N();
        executor.execute(new RunnableBRunnable0Shape1S0100100_I1(r2, 10000, 3));
        this.A01 = r2;
    }

    @Override // X.AbstractRunnableC14570le, X.AbstractC14600lh
    public void cancel() {
        super.cancel();
        this.A01.A01();
    }
}
