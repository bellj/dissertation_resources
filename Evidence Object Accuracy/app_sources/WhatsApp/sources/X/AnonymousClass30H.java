package X;

/* renamed from: X.30H  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30H extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;

    public AnonymousClass30H() {
        super(2980, new AnonymousClass00E(1, 1, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(1, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamXplatformMigrationDataReady {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "completedMigration", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "completedRegistration", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
