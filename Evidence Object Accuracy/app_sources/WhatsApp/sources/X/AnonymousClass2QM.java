package X;

import com.whatsapp.PagerSlidingTabStrip;

/* renamed from: X.2QM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QM implements AnonymousClass070 {
    public final /* synthetic */ PagerSlidingTabStrip A00;

    public /* synthetic */ AnonymousClass2QM(PagerSlidingTabStrip pagerSlidingTabStrip) {
        this.A00 = pagerSlidingTabStrip;
    }

    @Override // X.AnonymousClass070
    public void ATO(int i) {
        if (i == 0) {
            PagerSlidingTabStrip pagerSlidingTabStrip = this.A00;
            PagerSlidingTabStrip.A00(pagerSlidingTabStrip, pagerSlidingTabStrip.A0N.getCurrentItem(), 0);
        }
        AnonymousClass070 r0 = this.A00.A0M;
        if (r0 != null) {
            r0.ATO(i);
        }
    }

    @Override // X.AnonymousClass070
    public void ATP(int i, float f, int i2) {
        PagerSlidingTabStrip pagerSlidingTabStrip = this.A00;
        pagerSlidingTabStrip.A01 = i;
        pagerSlidingTabStrip.A00 = f;
        PagerSlidingTabStrip.A00(pagerSlidingTabStrip, i, (int) (((float) pagerSlidingTabStrip.A0L.getChildAt(i).getWidth()) * f));
        pagerSlidingTabStrip.invalidate();
        AnonymousClass070 r0 = pagerSlidingTabStrip.A0M;
        if (r0 != null) {
            r0.ATP(i, f, i2);
        }
    }

    @Override // X.AnonymousClass070
    public void ATQ(int i) {
        AnonymousClass070 r0 = this.A00.A0M;
        if (r0 != null) {
            r0.ATQ(i);
        }
    }
}
