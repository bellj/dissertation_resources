package X;

import com.whatsapp.voipcalling.GlVideoRenderer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.chromium.net.UrlRequest;

/* renamed from: X.2DE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DE {
    public static final Object A08 = "false";
    public static final Object A09 = "length";
    public static final Object A0A = C64983Hr.A00(1.0d);
    public static final Object A0B = "true";
    public static final Object A0C = C64983Hr.A00(0.0d);
    public int A00;
    public int A01;
    public int A02;
    public C89094Iq A03;
    public int[] A04 = new int[16];
    public Object[] A05 = new Object[16];
    public final Object A06 = new HashMap();
    public final Random A07 = new Random();

    public static double A00(Object obj) {
        Number number;
        if (A0D(obj) && (number = (Number) obj) != null) {
            return number.doubleValue();
        }
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue() ? 1.0d : 0.0d;
        }
        if (obj instanceof Long) {
            return (double) ((Number) obj).longValue();
        }
        return Double.NaN;
    }

    public static final int A01(Object obj) {
        if (obj == null) {
            return 0;
        }
        if (obj instanceof Boolean) {
            return 1;
        }
        if (obj instanceof Long) {
            return 3;
        }
        if (A0D(obj)) {
            return 4;
        }
        if (obj instanceof String) {
            return 2;
        }
        if (obj instanceof List) {
            return 6;
        }
        if (obj instanceof Map) {
            return 7;
        }
        return obj instanceof C94724cR ? 8 : 5;
    }

    public static C90024Mh A02(Object obj) {
        C90024Mh r0 = ((C94724cR) obj).A00.A01;
        if (r0 != null) {
            return r0;
        }
        throw new IllegalStateException("The Lispy expression has not been parsed");
    }

    public static Number A03(Object obj) {
        Object obj2;
        Number number;
        if (A0D(obj) && (number = (Number) obj) != null) {
            return number;
        }
        if (obj instanceof Boolean) {
            if (((Boolean) obj).booleanValue()) {
                obj2 = A0A;
            } else {
                obj2 = A0C;
            }
            if (A0D(obj2)) {
                return (Number) obj2;
            }
            return null;
        } else if (obj instanceof Long) {
            return C64983Hr.A00((double) ((Number) obj).longValue());
        } else {
            return null;
        }
    }

    public static Number A04(Object obj) {
        int doubleValue;
        double d;
        Number number;
        if ((obj instanceof Long) && (number = (Number) obj) != null) {
            doubleValue = number.intValue();
        } else if (obj instanceof Boolean) {
            d = ((Boolean) obj).booleanValue() ? 1.0d : 0.0d;
            return C64983Hr.A00(d);
        } else if (!A0D(obj)) {
            return null;
        } else {
            doubleValue = (int) ((long) ((Number) obj).doubleValue());
        }
        d = (double) doubleValue;
        return C64983Hr.A00(d);
    }

    public static Number A05(Object obj) {
        long doubleValue;
        Number number;
        if ((obj instanceof Long) && (number = (Number) obj) != null) {
            return number;
        }
        if (obj instanceof Boolean) {
            if (((Boolean) obj).booleanValue()) {
                doubleValue = 1;
            } else {
                doubleValue = 0;
            }
        } else if (!A0D(obj)) {
            return null;
        } else {
            doubleValue = (long) ((Number) obj).doubleValue();
        }
        return Long.valueOf(doubleValue);
    }

    public static void A06(C90024Mh r4, StringBuilder sb, int i) {
        C92344Vn r3 = r4.A00;
        ByteBuffer A00 = r3.A00();
        if ((A00.get(i) & 255) == 4) {
            sb.append("  at extension function ");
            sb.append((String) r3.A03.A01(A00.getShort(i + 1) & 65535));
            sb.append('\n');
        }
        sb.append("  at offset ");
        AnonymousClass3IR r2 = r3.A03;
        sb.append(i - r2.A04[2].A02);
        sb.append("  (offset ");
        sb.append(i - r3.A02);
        sb.append(" in function ");
        sb.append((String) r2.A01(r3.A01));
        sb.append(')');
        sb.append(" in script \"");
        sb.append(r2.A00);
        sb.append('\"');
        sb.append("\n");
    }

    public static final void A07(Object obj, Object obj2, String str) {
        String name;
        String name2;
        StringBuilder sb = new StringBuilder("TypeError: ");
        sb.append(str);
        sb.append(". Got ");
        if (obj == null) {
            name = "null";
        } else {
            name = obj.getClass().getName();
        }
        sb.append(name);
        sb.append(" and ");
        if (obj2 == null) {
            name2 = "null";
        } else {
            name2 = obj2.getClass().getName();
        }
        sb.append(name2);
        throw new AnonymousClass5H3(sb.toString());
    }

    public static void A08(String str) {
        StringBuilder sb = new StringBuilder("RangeError: ");
        sb.append(str);
        throw new AnonymousClass5H3(sb.toString());
    }

    public static void A09(String str) {
        StringBuilder sb = new StringBuilder("TypeError: ");
        sb.append(str);
        throw new AnonymousClass5H3(sb.toString());
    }

    public static final void A0A(String str) {
        StringBuilder sb = new StringBuilder("InvalidBytecode: ");
        sb.append(str);
        throw new AnonymousClass5H7(sb.toString());
    }

    public static void A0B(String str, Object obj) {
        String name;
        StringBuilder sb = new StringBuilder("TypeError: ");
        sb.append(str);
        sb.append(". Got ");
        if (obj == null) {
            name = "null";
        } else {
            name = obj.getClass().getName();
        }
        sb.append(name);
        throw new AnonymousClass5H3(sb.toString());
    }

    public static void A0C(String str, Throwable th) {
        StringBuilder sb = new StringBuilder("TypeError: ");
        sb.append(str);
        throw new AnonymousClass5H3(sb.toString(), th);
    }

    public static boolean A0D(Object obj) {
        return (obj instanceof Number) && !(obj instanceof Long);
    }

    public static boolean A0E(Object obj) {
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue();
        }
        boolean z = false;
        if (obj == null) {
            z = true;
        }
        if (!z) {
            if (obj instanceof String) {
                return !((String) obj).isEmpty();
            }
            if (A0D(obj)) {
                double doubleValue = ((Number) obj).doubleValue();
                if (doubleValue != 0.0d && !Double.isNaN(doubleValue)) {
                    return true;
                }
            } else if (!(obj instanceof Long) || ((Number) obj).longValue() != 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean A0F(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == 'x' || charAt < '\t') {
                return true;
            }
            if ((charAt > '\r' && charAt < ' ') || charAt > '~') {
                return true;
            }
        }
        return false;
    }

    public int A0G(Object obj, String str, int i) {
        int i2;
        int i3;
        if (obj instanceof Long) {
            long longValue = ((Number) obj).longValue();
            i2 = (int) longValue;
            i3 = (((long) i2) > longValue ? 1 : (((long) i2) == longValue ? 0 : -1));
        } else {
            if (A0D(obj)) {
                double doubleValue = ((Number) obj).doubleValue();
                i2 = (int) doubleValue;
                i3 = (((double) i2) > doubleValue ? 1 : (((double) i2) == doubleValue ? 0 : -1));
            }
            A09(str);
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        if (i3 == 0 && i2 >= 0 && i2 <= i) {
            return i2;
        }
        A09(str);
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final C90024Mh A0H(Object obj, int i) {
        if (obj instanceof C94724cR) {
            return A02(obj);
        }
        StringBuilder sb = new StringBuilder("Expected stack value of closure type for opcode ");
        sb.append(C88524Fz.A00[i]);
        A0B(sb.toString(), obj);
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final Object A0I(Number number, Number number2, int i) {
        double d;
        int i2;
        long j;
        String str;
        switch (i) {
            case 0:
                d = number.doubleValue() * number2.doubleValue();
                return C64983Hr.A00(d);
            case 1:
                d = (double) ((int) ((number.longValue() & 4294967295L) * (number2.longValue() & 4294967295L)));
                return C64983Hr.A00(d);
            case 2:
                d = number.doubleValue() / number2.doubleValue();
                return C64983Hr.A00(d);
            case 3:
                d = number.doubleValue() % number2.doubleValue();
                return C64983Hr.A00(d);
            case 4:
                d = number.doubleValue() - number2.doubleValue();
                return C64983Hr.A00(d);
            case 5:
                d = number.doubleValue() + number2.doubleValue();
                return C64983Hr.A00(d);
            case 6:
                i2 = ((int) ((long) number.doubleValue())) & ((int) ((long) number2.doubleValue()));
                d = (double) i2;
                return C64983Hr.A00(d);
            case 7:
                i2 = ((int) ((long) number.doubleValue())) | ((int) ((long) number2.doubleValue()));
                d = (double) i2;
                return C64983Hr.A00(d);
            case 8:
                i2 = ((int) ((long) number.doubleValue())) ^ ((int) ((long) number2.doubleValue()));
                d = (double) i2;
                return C64983Hr.A00(d);
            case 9:
                i2 = ((int) ((long) number.doubleValue())) << (((int) ((long) number2.doubleValue())) & 31);
                d = (double) i2;
                return C64983Hr.A00(d);
            case 10:
                i2 = ((int) ((long) number.doubleValue())) >> (((int) ((long) number2.doubleValue())) & 31);
                d = (double) i2;
                return C64983Hr.A00(d);
            case 11:
                d = (double) (((long) (((int) ((long) number.doubleValue())) >>> (((int) ((long) number2.doubleValue())) & 31))) & 4294967295L);
                return C64983Hr.A00(d);
            case 12:
                j = number.longValue() * number2.longValue();
                return Long.valueOf(j);
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                if (number2.longValue() == 0) {
                    str = "INT64_DIV division by zero";
                    A09(str);
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
                j = number.longValue() / number2.longValue();
                return Long.valueOf(j);
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                if (number2.longValue() == 0) {
                    str = "INT64_MOD division by zero";
                    A09(str);
                    throw new RuntimeException("Redex: Unreachable code after no-return invoke");
                }
                j = number.longValue() % number2.longValue();
                return Long.valueOf(j);
            case 15:
                j = number.longValue() - number2.longValue();
                return Long.valueOf(j);
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                j = number.longValue() + number2.longValue();
                return Long.valueOf(j);
            case 17:
                j = number.longValue() & number2.longValue();
                return Long.valueOf(j);
            case 18:
                j = number.longValue() | number2.longValue();
                return Long.valueOf(j);
            case 19:
                j = number.longValue() ^ number2.longValue();
                return Long.valueOf(j);
            case C43951xu.A01:
                j = number.longValue() << ((int) (number2.longValue() & 63));
                return Long.valueOf(j);
            case 21:
                j = number.longValue() >> ((int) (number2.longValue() & 63));
                return Long.valueOf(j);
            case 22:
                j = number.longValue() >>> ((int) (number2.longValue() & 63));
                return Long.valueOf(j);
            default:
                throw new IllegalArgumentException("Invalid callback index");
        }
    }

    public Object A0J(Object obj) {
        if (obj instanceof Boolean) {
            if (((Boolean) obj).booleanValue()) {
                return "true";
            }
            return "false";
        } else if (obj instanceof Long) {
            return Long.toString(((Number) obj).longValue());
        } else {
            if (A0D(obj)) {
                double doubleValue = ((Number) obj).doubleValue();
                int i = (int) doubleValue;
                if (((double) i) == doubleValue) {
                    return Integer.toString(i);
                }
                String d = Double.toString(doubleValue);
                int indexOf = d.indexOf(69);
                if (indexOf < 0) {
                    return d;
                }
                int length = d.length();
                int i2 = length + 1;
                char[] cArr = new char[i2];
                d.getChars(0, indexOf, cArr, 0);
                cArr[indexOf] = 'e';
                int i3 = indexOf + 1;
                if (d.charAt(i3) == '-') {
                    d.getChars(i3, length, cArr, i3);
                } else {
                    cArr[i3] = '+';
                    d.getChars(i3, length, cArr, indexOf + 2);
                    length = i2;
                }
                return new String(cArr, 0, length);
            } else if (obj instanceof String) {
                return obj;
            } else {
                A0B("Value cannot be converted to string", obj);
                throw new RuntimeException("Redex: Unreachable code after no-return invoke");
            }
        }
    }

    public final Map A0K(Object obj, int i) {
        if (obj instanceof Map) {
            return (Map) obj;
        }
        StringBuilder sb = new StringBuilder("Expected stack value of map type for opcode ");
        sb.append(C88524Fz.A00[i]);
        A0B(sb.toString(), obj);
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:371:0x096f, code lost:
        A09(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:372:0x0979, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:391:0x09e3, code lost:
        A09(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:392:0x09ed, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:442:0x0ac7, code lost:
        A09(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:443:0x0ad1, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:502:0x0ba6, code lost:
        A09(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:503:0x0bb0, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:557:0x0c66, code lost:
        A09(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:558:0x0c70, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:645:0x0dc8, code lost:
        A09(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:646:0x0dd2, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0186, code lost:
        if (r3 != null) goto L_0x0188;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x018c, code lost:
        if (r0 != 0) goto L_0x0188;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:707:0x0f21, code lost:
        if (r1 != null) goto L_0x0f24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:864:0x1394, code lost:
        A08(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:865:0x139e, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:880:0x140f, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:881:0x1410, code lost:
        A08("ArrayCopy dst index out of range");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:882:0x141c, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:883:0x141d, code lost:
        A08("ArrayCopy src index out of range");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:884:0x1429, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:885:0x142a, code lost:
        r0 = "StringSearch length out of range";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:886:0x142d, code lost:
        r0 = "StringSearch offset out of range";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:887:0x142f, code lost:
        A08(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:888:0x1439, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:889:0x143a, code lost:
        r0 = "SubstringCompare length out of range";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:890:0x143d, code lost:
        r0 = "SubstringCompare offset out of range";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:891:0x143f, code lost:
        A08(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:892:0x1449, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:893:0x144a, code lost:
        r0 = "Substring length out of range";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:894:0x144d, code lost:
        r0 = "Substring offset out of range";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:895:0x144f, code lost:
        A08(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:896:0x1459, code lost:
        throw new java.lang.RuntimeException("Redex: Unreachable code after no-return invoke");
     */
    /* JADX WARNING: Removed duplicated region for block: B:1028:0x1363 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:1052:0x1271 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:1053:0x1271 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0193  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0L() {
        /*
        // Method dump skipped, instructions count: 5646
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2DE.A0L():void");
    }

    public final void A0M() {
        int i = this.A01 - 1;
        this.A01 = i;
        this.A05[i] = null;
    }

    public final void A0N(int i) {
        int i2 = this.A01 - i;
        this.A01 = i2;
        Arrays.fill(this.A05, i2, i + i2, (Object) null);
    }

    public final void A0O(int i) {
        Object[] objArr = this.A05;
        int length = objArr.length;
        int i2 = this.A01;
        if (length - i2 < i) {
            int i3 = i2 + i;
            int i4 = 536870912;
            if (i3 > 536870912) {
                throw new IllegalStateException("MinScript stack overflow");
            }
            do {
                length <<= 1;
            } while (length < i3);
            if (length <= 536870912) {
                i4 = length;
            }
            Object[] objArr2 = new Object[i4];
            System.arraycopy(objArr, 0, objArr2, 0, i2);
            this.A05 = objArr2;
            int[] iArr = new int[i4];
            System.arraycopy(this.A04, 0, iArr, 0, this.A01);
            this.A04 = iArr;
        }
    }

    public final void A0P(int i, Object obj) {
        this.A05[(this.A01 - i) - 1] = obj;
    }

    public final void A0Q(int i, String str) {
        int i2 = this.A01 - 1;
        Object[] objArr = this.A05;
        Object obj = objArr[i2 - 0];
        Object obj2 = objArr[i2 - 1];
        Number A03 = A03(obj2);
        Number A032 = A03(obj);
        if (A03 == null || A032 == null) {
            StringBuilder sb = new StringBuilder("Incompatible operand types of ");
            sb.append(str);
            A07(obj2, obj, sb.toString());
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        A0P(1, A0I(A03, A032, i));
        A0M();
    }

    public final void A0R(int i, String str) {
        int i2 = this.A01 - 1;
        Object[] objArr = this.A05;
        Object obj = objArr[i2 - 0];
        Object obj2 = objArr[i2 - 1];
        Number A05 = A05(obj2);
        Number A052 = A05(obj);
        if (A05 == null || A052 == null) {
            StringBuilder sb = new StringBuilder("Incompatible operand types of ");
            sb.append(str);
            A07(obj2, obj, sb.toString());
            throw new RuntimeException("Redex: Unreachable code after no-return invoke");
        }
        A0P(1, A0I(A05, A052, i));
        A0M();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r4 <= 0) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        r11 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        A0P(1, java.lang.Boolean.valueOf(r11));
        A0M();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0087, code lost:
        if (r4 < 0) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008f, code lost:
        if (r4 > 0) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0097, code lost:
        if (r4 >= 0) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0S(int r13, java.lang.String r14) {
        /*
            r12 = this;
            r11 = 0
            int r0 = r12.A01
            int r3 = r0 + -1
            int r1 = r3 - r11
            java.lang.Object[] r0 = r12.A05
            r2 = r0[r1]
            r7 = 1
            int r3 = r3 - r7
            r3 = r0[r3]
            boolean r0 = r3 instanceof java.lang.Long
            java.lang.String r5 = "Invalid relational op"
            r8 = 3
            r6 = 2
            if (r0 == 0) goto L_0x0045
            boolean r0 = r2 instanceof java.lang.Long
            if (r0 == 0) goto L_0x0045
            java.lang.Number r3 = (java.lang.Number) r3
            long r9 = r3.longValue()
            boolean r0 = r2 instanceof java.lang.Long
            if (r0 == 0) goto L_0x0043
            java.lang.Number r2 = (java.lang.Number) r2
        L_0x0027:
            long r0 = r2.longValue()
            if (r13 == 0) goto L_0x0095
            if (r13 == r7) goto L_0x008d
            if (r13 == r6) goto L_0x0085
            if (r13 != r8) goto L_0x009a
            int r4 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
        L_0x0035:
            if (r4 > 0) goto L_0x0038
        L_0x0037:
            r11 = 1
        L_0x0038:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r11)
            r12.A0P(r7, r0)
            r12.A0M()
            return
        L_0x0043:
            r2 = 0
            goto L_0x0027
        L_0x0045:
            boolean r0 = r3 instanceof java.lang.String
            if (r0 == 0) goto L_0x0063
            boolean r0 = r2 instanceof java.lang.String
            if (r0 == 0) goto L_0x0063
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r2 = (java.lang.String) r2
            int r4 = r3.compareTo(r2)
            if (r13 == 0) goto L_0x0097
            if (r13 == r7) goto L_0x008f
            if (r13 == r6) goto L_0x0087
            if (r13 == r8) goto L_0x0035
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r5)
            throw r0
        L_0x0063:
            java.lang.Number r1 = A03(r3)
            java.lang.Number r0 = A03(r2)
            if (r1 == 0) goto L_0x00a6
            if (r0 == 0) goto L_0x00a6
            double r2 = r1.doubleValue()
            double r0 = r0.doubleValue()
            if (r13 == 0) goto L_0x0092
            if (r13 == r7) goto L_0x008a
            if (r13 == r6) goto L_0x0082
            if (r13 != r8) goto L_0x00a0
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            goto L_0x0035
        L_0x0082:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            goto L_0x0087
        L_0x0085:
            int r4 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
        L_0x0087:
            if (r4 >= 0) goto L_0x0038
            goto L_0x0037
        L_0x008a:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            goto L_0x008f
        L_0x008d:
            int r4 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
        L_0x008f:
            if (r4 <= 0) goto L_0x0038
            goto L_0x0037
        L_0x0092:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            goto L_0x0097
        L_0x0095:
            int r4 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
        L_0x0097:
            if (r4 < 0) goto L_0x0038
            goto L_0x0037
        L_0x009a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r5)
            throw r0
        L_0x00a0:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r5)
            throw r0
        L_0x00a6:
            java.lang.String r1 = "Incompatible operand types of "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r14)
            java.lang.String r0 = r0.toString()
            A07(r3, r2, r0)
            java.lang.String r1 = "Redex: Unreachable code after no-return invoke"
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2DE.A0S(int, java.lang.String):void");
    }

    public void A0T(Object obj) {
        Object[] objArr = this.A05;
        int i = this.A01;
        objArr[i] = obj;
        this.A01 = i + 1;
    }

    public final void A0U(Object obj, Object obj2, int i, int i2) {
        A0T(obj);
        int[] iArr = this.A04;
        int i3 = this.A01;
        iArr[i3] = i;
        this.A01 = i3 + 1;
        A0T(obj2);
        int i4 = this.A01;
        iArr[i4] = i2;
        int i5 = i4 + 1;
        this.A01 = i5;
        iArr[i5] = this.A00;
        int i6 = i5 + 1;
        this.A01 = i6;
        this.A00 = i6 - 1;
    }
}
