package X;

/* renamed from: X.4Ad  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87084Ad {
    DEFAULT_PATH_LEAF_TO_NULL,
    ALWAYS_RETURN_LIST,
    AS_PATH_LIST,
    SUPPRESS_EXCEPTIONS,
    REQUIRE_PROPERTIES
}
