package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.IOException;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.1xd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C43811xd extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C43811xd A0l;
    public static volatile AnonymousClass255 A0m;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public int A0H;
    public int A0I;
    public int A0J;
    public int A0K;
    public int A0L;
    public int A0M;
    public int A0N;
    public int A0O;
    public int A0P;
    public int A0Q;
    public int A0R;
    public int A0S;
    public int A0T;
    public int A0U;
    public int A0V;
    public int A0W;
    public int A0X;
    public int A0Y;
    public int A0Z;
    public int A0a;
    public int A0b;
    public int A0c;
    public int A0d;
    public int A0e;
    public int A0f;
    public int A0g;
    public int A0h;
    public int A0i;
    public int A0j;
    public int A0k;

    static {
        C43811xd r0 = new C43811xd();
        A0l = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r26, Object obj, Object obj2) {
        int A02;
        switch (r26.ordinal()) {
            case 0:
                return A0l;
            case 1:
                AbstractC462925h r11 = (AbstractC462925h) obj;
                C43811xd r10 = (C43811xd) obj2;
                int i = this.A01;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A0H;
                int i3 = r10.A01;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A0H = r11.Afp(i2, r10.A0H, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                int i4 = this.A0j;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A0j = r11.Afp(i4, r10.A0j, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                int i5 = this.A0F;
                boolean z6 = false;
                if ((i3 & 4) == 4) {
                    z6 = true;
                }
                this.A0F = r11.Afp(i5, r10.A0F, z5, z6);
                boolean z7 = false;
                if ((i & 8) == 8) {
                    z7 = true;
                }
                int i6 = this.A0E;
                boolean z8 = false;
                if ((i3 & 8) == 8) {
                    z8 = true;
                }
                this.A0E = r11.Afp(i6, r10.A0E, z7, z8);
                boolean z9 = false;
                if ((i & 16) == 16) {
                    z9 = true;
                }
                int i7 = this.A04;
                boolean z10 = false;
                if ((i3 & 16) == 16) {
                    z10 = true;
                }
                this.A04 = r11.Afp(i7, r10.A04, z9, z10);
                boolean z11 = false;
                if ((i & 32) == 32) {
                    z11 = true;
                }
                int i8 = this.A0Q;
                boolean z12 = false;
                if ((i3 & 32) == 32) {
                    z12 = true;
                }
                this.A0Q = r11.Afp(i8, r10.A0Q, z11, z12);
                boolean z13 = false;
                if ((i & 64) == 64) {
                    z13 = true;
                }
                int i9 = this.A0K;
                boolean z14 = false;
                if ((i3 & 64) == 64) {
                    z14 = true;
                }
                this.A0K = r11.Afp(i9, r10.A0K, z13, z14);
                boolean z15 = false;
                if ((i & 128) == 128) {
                    z15 = true;
                }
                int i10 = this.A0R;
                boolean z16 = false;
                if ((i3 & 128) == 128) {
                    z16 = true;
                }
                this.A0R = r11.Afp(i10, r10.A0R, z15, z16);
                boolean z17 = false;
                if ((i & 256) == 256) {
                    z17 = true;
                }
                int i11 = this.A0i;
                boolean z18 = false;
                if ((i3 & 256) == 256) {
                    z18 = true;
                }
                this.A0i = r11.Afp(i11, r10.A0i, z17, z18);
                boolean z19 = false;
                if ((i & 512) == 512) {
                    z19 = true;
                }
                int i12 = this.A0S;
                boolean z20 = false;
                if ((i3 & 512) == 512) {
                    z20 = true;
                }
                this.A0S = r11.Afp(i12, r10.A0S, z19, z20);
                boolean z21 = false;
                if ((i & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z21 = true;
                }
                int i13 = this.A0P;
                boolean z22 = false;
                if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z22 = true;
                }
                this.A0P = r11.Afp(i13, r10.A0P, z21, z22);
                boolean z23 = false;
                if ((i & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z23 = true;
                }
                int i14 = this.A0Z;
                boolean z24 = false;
                if ((2048 & i3) == 2048) {
                    z24 = true;
                }
                this.A0Z = r11.Afp(i14, r10.A0Z, z23, z24);
                boolean z25 = false;
                if ((i & 4096) == 4096) {
                    z25 = true;
                }
                int i15 = this.A0J;
                boolean z26 = false;
                if ((4096 & i3) == 4096) {
                    z26 = true;
                }
                this.A0J = r11.Afp(i15, r10.A0J, z25, z26);
                boolean z27 = false;
                if ((i & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z27 = true;
                }
                int i16 = this.A0I;
                boolean z28 = false;
                if ((i3 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                    z28 = true;
                }
                this.A0I = r11.Afp(i16, r10.A0I, z27, z28);
                boolean z29 = false;
                if ((i & 16384) == 16384) {
                    z29 = true;
                }
                int i17 = this.A0N;
                boolean z30 = false;
                if ((i3 & 16384) == 16384) {
                    z30 = true;
                }
                this.A0N = r11.Afp(i17, r10.A0N, z29, z30);
                boolean z31 = false;
                if ((i & 32768) == 32768) {
                    z31 = true;
                }
                int i18 = this.A0M;
                boolean z32 = false;
                if ((i3 & 32768) == 32768) {
                    z32 = true;
                }
                this.A0M = r11.Afp(i18, r10.A0M, z31, z32);
                boolean z33 = false;
                if ((i & 65536) == 65536) {
                    z33 = true;
                }
                int i19 = this.A0g;
                boolean z34 = false;
                if ((i3 & 65536) == 65536) {
                    z34 = true;
                }
                this.A0g = r11.Afp(i19, r10.A0g, z33, z34);
                boolean z35 = false;
                if ((i & C25981Bo.A0F) == 131072) {
                    z35 = true;
                }
                int i20 = this.A0f;
                boolean z36 = false;
                if ((i3 & C25981Bo.A0F) == 131072) {
                    z36 = true;
                }
                this.A0f = r11.Afp(i20, r10.A0f, z35, z36);
                boolean z37 = false;
                if ((i & 262144) == 262144) {
                    z37 = true;
                }
                int i21 = this.A0Y;
                boolean z38 = false;
                if ((i3 & 262144) == 262144) {
                    z38 = true;
                }
                this.A0Y = r11.Afp(i21, r10.A0Y, z37, z38);
                boolean z39 = false;
                if ((i & 524288) == 524288) {
                    z39 = true;
                }
                int i22 = this.A0k;
                boolean z40 = false;
                if ((i3 & 524288) == 524288) {
                    z40 = true;
                }
                this.A0k = r11.Afp(i22, r10.A0k, z39, z40);
                boolean z41 = false;
                if ((i & 1048576) == 1048576) {
                    z41 = true;
                }
                int i23 = this.A0d;
                boolean z42 = false;
                if ((i3 & 1048576) == 1048576) {
                    z42 = true;
                }
                this.A0d = r11.Afp(i23, r10.A0d, z41, z42);
                boolean z43 = false;
                if ((i & 2097152) == 2097152) {
                    z43 = true;
                }
                int i24 = this.A0B;
                boolean z44 = false;
                if ((i3 & 2097152) == 2097152) {
                    z44 = true;
                }
                this.A0B = r11.Afp(i24, r10.A0B, z43, z44);
                boolean z45 = false;
                if ((i & 4194304) == 4194304) {
                    z45 = true;
                }
                int i25 = this.A0G;
                boolean z46 = false;
                if ((i3 & 4194304) == 4194304) {
                    z46 = true;
                }
                this.A0G = r11.Afp(i25, r10.A0G, z45, z46);
                boolean z47 = false;
                if ((i & 8388608) == 8388608) {
                    z47 = true;
                }
                int i26 = this.A0V;
                boolean z48 = false;
                if ((i3 & 8388608) == 8388608) {
                    z48 = true;
                }
                this.A0V = r11.Afp(i26, r10.A0V, z47, z48);
                boolean z49 = false;
                if ((i & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                    z49 = true;
                }
                int i27 = this.A03;
                boolean z50 = false;
                if ((i3 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
                    z50 = true;
                }
                this.A03 = r11.Afp(i27, r10.A03, z49, z50);
                boolean z51 = false;
                if ((i & 33554432) == 33554432) {
                    z51 = true;
                }
                int i28 = this.A0X;
                boolean z52 = false;
                if ((i3 & 33554432) == 33554432) {
                    z52 = true;
                }
                this.A0X = r11.Afp(i28, r10.A0X, z51, z52);
                boolean z53 = false;
                if ((i & 67108864) == 67108864) {
                    z53 = true;
                }
                int i29 = this.A0h;
                boolean z54 = false;
                if ((i3 & 67108864) == 67108864) {
                    z54 = true;
                }
                this.A0h = r11.Afp(i29, r10.A0h, z53, z54);
                boolean z55 = false;
                if ((i & 134217728) == 134217728) {
                    z55 = true;
                }
                int i30 = this.A0c;
                boolean z56 = false;
                if ((i3 & 134217728) == 134217728) {
                    z56 = true;
                }
                this.A0c = r11.Afp(i30, r10.A0c, z55, z56);
                boolean z57 = false;
                if ((i & 268435456) == 268435456) {
                    z57 = true;
                }
                int i31 = this.A0b;
                boolean z58 = false;
                if ((i3 & 268435456) == 268435456) {
                    z58 = true;
                }
                this.A0b = r11.Afp(i31, r10.A0b, z57, z58);
                boolean z59 = false;
                if ((i & 536870912) == 536870912) {
                    z59 = true;
                }
                int i32 = this.A09;
                boolean z60 = false;
                if ((i3 & 536870912) == 536870912) {
                    z60 = true;
                }
                this.A09 = r11.Afp(i32, r10.A09, z59, z60);
                boolean z61 = false;
                if ((i & 1073741824) == 1073741824) {
                    z61 = true;
                }
                int i33 = this.A06;
                boolean z62 = false;
                if ((i3 & 1073741824) == 1073741824) {
                    z62 = true;
                }
                this.A06 = r11.Afp(i33, r10.A06, z61, z62);
                boolean z63 = false;
                if ((i & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                    z63 = true;
                }
                int i34 = this.A0T;
                boolean z64 = false;
                if ((i3 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
                    z64 = true;
                }
                this.A0T = r11.Afp(i34, r10.A0T, z63, z64);
                int i35 = this.A02;
                boolean z65 = true;
                if ((i35 & 1) != 1) {
                    z65 = false;
                }
                int i36 = this.A0U;
                int i37 = r10.A02;
                boolean z66 = true;
                if ((i37 & 1) != 1) {
                    z66 = false;
                }
                this.A0U = r11.Afp(i36, r10.A0U, z65, z66);
                boolean z67 = false;
                if ((i35 & 2) == 2) {
                    z67 = true;
                }
                int i38 = this.A0e;
                boolean z68 = false;
                if ((i37 & 2) == 2) {
                    z68 = true;
                }
                this.A0e = r11.Afp(i38, r10.A0e, z67, z68);
                boolean z69 = false;
                if ((i35 & 4) == 4) {
                    z69 = true;
                }
                int i39 = this.A0a;
                boolean z70 = false;
                if ((i37 & 4) == 4) {
                    z70 = true;
                }
                this.A0a = r11.Afp(i39, r10.A0a, z69, z70);
                boolean z71 = false;
                if ((i35 & 8) == 8) {
                    z71 = true;
                }
                int i40 = this.A0D;
                boolean z72 = false;
                if ((i37 & 8) == 8) {
                    z72 = true;
                }
                this.A0D = r11.Afp(i40, r10.A0D, z71, z72);
                boolean z73 = false;
                if ((i35 & 16) == 16) {
                    z73 = true;
                }
                int i41 = this.A0C;
                boolean z74 = false;
                if ((i37 & 16) == 16) {
                    z74 = true;
                }
                this.A0C = r11.Afp(i41, r10.A0C, z73, z74);
                boolean z75 = false;
                if ((i35 & 32) == 32) {
                    z75 = true;
                }
                int i42 = this.A0W;
                boolean z76 = false;
                if ((i37 & 32) == 32) {
                    z76 = true;
                }
                this.A0W = r11.Afp(i42, r10.A0W, z75, z76);
                boolean z77 = false;
                if ((i35 & 64) == 64) {
                    z77 = true;
                }
                int i43 = this.A00;
                boolean z78 = false;
                if ((i37 & 64) == 64) {
                    z78 = true;
                }
                this.A00 = r11.Afp(i43, r10.A00, z77, z78);
                boolean z79 = false;
                if ((i35 & 128) == 128) {
                    z79 = true;
                }
                int i44 = this.A08;
                boolean z80 = false;
                if ((i37 & 128) == 128) {
                    z80 = true;
                }
                this.A08 = r11.Afp(i44, r10.A08, z79, z80);
                boolean z81 = false;
                if ((i35 & 256) == 256) {
                    z81 = true;
                }
                int i45 = this.A07;
                boolean z82 = false;
                if ((i37 & 256) == 256) {
                    z82 = true;
                }
                this.A07 = r11.Afp(i45, r10.A07, z81, z82);
                boolean z83 = false;
                if ((i35 & 512) == 512) {
                    z83 = true;
                }
                int i46 = this.A0L;
                boolean z84 = false;
                if ((i37 & 512) == 512) {
                    z84 = true;
                }
                this.A0L = r11.Afp(i46, r10.A0L, z83, z84);
                boolean z85 = false;
                if ((i35 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z85 = true;
                }
                int i47 = this.A05;
                boolean z86 = false;
                if ((i37 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z86 = true;
                }
                this.A05 = r11.Afp(i47, r10.A05, z85, z86);
                boolean z87 = false;
                if ((i35 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z87 = true;
                }
                int i48 = this.A0A;
                boolean z88 = false;
                if ((i37 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z88 = true;
                }
                this.A0A = r11.Afp(i48, r10.A0A, z87, z88);
                boolean z89 = false;
                if ((i35 & 4096) == 4096) {
                    z89 = true;
                }
                int i49 = this.A0O;
                boolean z90 = false;
                if ((i37 & 4096) == 4096) {
                    z90 = true;
                }
                this.A0O = r11.Afp(i49, r10.A0O, z89, z90);
                if (r11 == C463025i.A00) {
                    this.A01 = i | i3;
                    this.A02 = i35 | i37;
                }
                return this;
            case 2:
                AnonymousClass253 r112 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r112.A03();
                            int i50 = 32;
                            switch (A03) {
                                case 0:
                                    break;
                                case 8:
                                    int A022 = r112.A02();
                                    if (EnumC43821xe.A00(A022) == null) {
                                        super.A0X(1, A022);
                                        continue;
                                    } else {
                                        this.A01 |= 1;
                                        this.A0H = A022;
                                    }
                                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                    int A023 = r112.A02();
                                    if (EnumC43821xe.A00(A023) == null) {
                                        super.A0X(2, A023);
                                        continue;
                                    } else {
                                        this.A01 |= 2;
                                        this.A0j = A023;
                                    }
                                case 24:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 3;
                                        break;
                                    } else {
                                        this.A01 |= 4;
                                        this.A0F = A02;
                                        continue;
                                    }
                                case 32:
                                    int A024 = r112.A02();
                                    if (EnumC43821xe.A00(A024) == null) {
                                        super.A0X(4, A024);
                                        continue;
                                    } else {
                                        this.A01 |= 8;
                                        this.A0E = A024;
                                    }
                                case 40:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 5;
                                        break;
                                    } else {
                                        this.A01 |= 16;
                                        this.A04 = A02;
                                        continue;
                                    }
                                case 48:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 6;
                                        break;
                                    } else {
                                        this.A01 = 32 | this.A01;
                                        this.A0Q = A02;
                                        continue;
                                    }
                                case 56:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 7;
                                        break;
                                    } else {
                                        this.A01 |= 64;
                                        this.A0K = A02;
                                        continue;
                                    }
                                case 64:
                                    int A025 = r112.A02();
                                    if (EnumC43821xe.A00(A025) == null) {
                                        super.A0X(8, A025);
                                        continue;
                                    } else {
                                        this.A01 |= 128;
                                        this.A0R = A025;
                                    }
                                case C43951xu.A02 /* 72 */:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 9;
                                        break;
                                    } else {
                                        this.A01 |= 256;
                                        this.A0i = A02;
                                        continue;
                                    }
                                case 80:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 10;
                                        break;
                                    } else {
                                        this.A01 |= 512;
                                        this.A0S = A02;
                                        continue;
                                    }
                                case 88:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 11;
                                        break;
                                    } else {
                                        this.A01 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                        this.A0P = A02;
                                        continue;
                                    }
                                case 96:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 12;
                                        break;
                                    } else {
                                        this.A01 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                        this.A0Z = A02;
                                        continue;
                                    }
                                case 104:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 13;
                                        break;
                                    } else {
                                        this.A01 |= 4096;
                                        this.A0J = A02;
                                        continue;
                                    }
                                case 112:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 14;
                                        break;
                                    } else {
                                        this.A01 |= DefaultCrypto.BUFFER_SIZE;
                                        this.A0I = A02;
                                        continue;
                                    }
                                case 120:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 15;
                                        break;
                                    } else {
                                        this.A01 |= 16384;
                                        this.A0N = A02;
                                        continue;
                                    }
                                case MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT /* 144 */:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 18;
                                        break;
                                    } else {
                                        this.A01 |= 32768;
                                        this.A0M = A02;
                                        continue;
                                    }
                                case 152:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 19;
                                        break;
                                    } else {
                                        this.A01 |= 65536;
                                        this.A0g = A02;
                                        continue;
                                    }
                                case 160:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 20;
                                        break;
                                    } else {
                                        this.A01 |= C25981Bo.A0F;
                                        this.A0f = A02;
                                        continue;
                                    }
                                case 168:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 21;
                                        break;
                                    } else {
                                        this.A01 |= 262144;
                                        this.A0Y = A02;
                                        continue;
                                    }
                                case MediaCodecVideoEncoder.MIN_ENCODER_WIDTH /* 176 */:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 22;
                                        break;
                                    } else {
                                        this.A01 |= 524288;
                                        this.A0k = A02;
                                        continue;
                                    }
                                case 184:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 23;
                                        break;
                                    } else {
                                        this.A01 |= 1048576;
                                        this.A0d = A02;
                                        continue;
                                    }
                                case 192:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 24;
                                        break;
                                    } else {
                                        this.A01 |= 2097152;
                                        this.A0B = A02;
                                        continue;
                                    }
                                case 200:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 25;
                                        break;
                                    } else {
                                        this.A01 |= 4194304;
                                        this.A0G = A02;
                                        continue;
                                    }
                                case 208:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 26;
                                        break;
                                    } else {
                                        this.A01 |= 8388608;
                                        this.A0V = A02;
                                        continue;
                                    }
                                case 216:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 27;
                                        break;
                                    } else {
                                        this.A01 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
                                        this.A03 = A02;
                                        continue;
                                    }
                                case 224:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 28;
                                        break;
                                    } else {
                                        this.A01 |= 33554432;
                                        this.A0X = A02;
                                        continue;
                                    }
                                case 232:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 29;
                                        break;
                                    } else {
                                        this.A01 |= 67108864;
                                        this.A0h = A02;
                                        continue;
                                    }
                                case 240:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 30;
                                        break;
                                    } else {
                                        this.A01 |= 134217728;
                                        this.A0c = A02;
                                        continue;
                                    }
                                case 248:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 31;
                                        break;
                                    } else {
                                        this.A01 |= 268435456;
                                        this.A0b = A02;
                                        continue;
                                    }
                                case 256:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        break;
                                    } else {
                                        this.A01 |= 536870912;
                                        this.A09 = A02;
                                        continue;
                                    }
                                case 264:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 33;
                                        break;
                                    } else {
                                        this.A01 |= 1073741824;
                                        this.A06 = A02;
                                        continue;
                                    }
                                case 272:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 34;
                                        break;
                                    } else {
                                        this.A01 |= Integer.MIN_VALUE;
                                        this.A0T = A02;
                                        continue;
                                    }
                                case 288:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 36;
                                        break;
                                    } else {
                                        this.A02 |= 1;
                                        this.A0U = A02;
                                        continue;
                                    }
                                case 296:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 37;
                                        break;
                                    } else {
                                        this.A02 |= 2;
                                        this.A0e = A02;
                                        continue;
                                    }
                                case 312:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 39;
                                        break;
                                    } else {
                                        this.A02 |= 4;
                                        this.A0a = A02;
                                        continue;
                                    }
                                case 320:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 40;
                                        break;
                                    } else {
                                        this.A02 |= 8;
                                        this.A0D = A02;
                                        continue;
                                    }
                                case 328:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 41;
                                        break;
                                    } else {
                                        this.A02 |= 16;
                                        this.A0C = A02;
                                        continue;
                                    }
                                case 336:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 42;
                                        break;
                                    } else {
                                        this.A02 = 32 | this.A02;
                                        this.A0W = A02;
                                        continue;
                                    }
                                case 344:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 43;
                                        break;
                                    } else {
                                        this.A02 |= 64;
                                        this.A00 = A02;
                                        continue;
                                    }
                                case 352:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 44;
                                        break;
                                    } else {
                                        this.A02 |= 128;
                                        this.A08 = A02;
                                        continue;
                                    }
                                case 360:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 45;
                                        break;
                                    } else {
                                        this.A02 |= 256;
                                        this.A07 = A02;
                                        continue;
                                    }
                                case 368:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 46;
                                        break;
                                    } else {
                                        this.A02 |= 512;
                                        this.A0L = A02;
                                        continue;
                                    }
                                case 376:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 47;
                                        break;
                                    } else {
                                        this.A02 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                        this.A05 = A02;
                                        continue;
                                    }
                                case 384:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 48;
                                        break;
                                    } else {
                                        this.A02 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                        this.A0A = A02;
                                        continue;
                                    }
                                case 392:
                                    A02 = r112.A02();
                                    if (EnumC43821xe.A00(A02) == null) {
                                        i50 = 49;
                                        break;
                                    } else {
                                        this.A02 |= 4096;
                                        this.A0O = A02;
                                        continue;
                                    }
                                default:
                                    if (!A0a(r112, A03)) {
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                            super.A0X(i50, A02);
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C43811xd();
            case 5:
                return new C82483vh();
            case 6:
                break;
            case 7:
                if (A0m == null) {
                    synchronized (C43811xd.class) {
                        if (A0m == null) {
                            A0m = new AnonymousClass255(A0l);
                        }
                    }
                }
                return A0m;
            default:
                throw new UnsupportedOperationException();
        }
        return A0l;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A01;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A02(1, this.A0H);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A02(2, this.A0j);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A02(3, this.A0F);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A02(4, this.A0E);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A02(5, this.A04);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A02(6, this.A0Q);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A02(7, this.A0K);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A02(8, this.A0R);
        }
        if ((i3 & 256) == 256) {
            i2 += CodedOutputStream.A02(9, this.A0i);
        }
        if ((i3 & 512) == 512) {
            i2 += CodedOutputStream.A02(10, this.A0S);
        }
        if ((i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A02(11, this.A0P);
        }
        if ((i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A02(12, this.A0Z);
        }
        if ((i3 & 4096) == 4096) {
            i2 += CodedOutputStream.A02(13, this.A0J);
        }
        if ((i3 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i2 += CodedOutputStream.A02(14, this.A0I);
        }
        if ((i3 & 16384) == 16384) {
            i2 += CodedOutputStream.A02(15, this.A0N);
        }
        if ((i3 & 32768) == 32768) {
            i2 += CodedOutputStream.A02(18, this.A0M);
        }
        if ((i3 & 65536) == 65536) {
            i2 += CodedOutputStream.A02(19, this.A0g);
        }
        if ((i3 & C25981Bo.A0F) == 131072) {
            i2 += CodedOutputStream.A02(20, this.A0f);
        }
        if ((i3 & 262144) == 262144) {
            i2 += CodedOutputStream.A02(21, this.A0Y);
        }
        if ((i3 & 524288) == 524288) {
            i2 += CodedOutputStream.A02(22, this.A0k);
        }
        if ((i3 & 1048576) == 1048576) {
            i2 += CodedOutputStream.A02(23, this.A0d);
        }
        if ((i3 & 2097152) == 2097152) {
            i2 += CodedOutputStream.A02(24, this.A0B);
        }
        if ((i3 & 4194304) == 4194304) {
            i2 += CodedOutputStream.A02(25, this.A0G);
        }
        if ((i3 & 8388608) == 8388608) {
            i2 += CodedOutputStream.A02(26, this.A0V);
        }
        if ((i3 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            i2 += CodedOutputStream.A02(27, this.A03);
        }
        if ((i3 & 33554432) == 33554432) {
            i2 += CodedOutputStream.A02(28, this.A0X);
        }
        if ((i3 & 67108864) == 67108864) {
            i2 += CodedOutputStream.A02(29, this.A0h);
        }
        if ((i3 & 134217728) == 134217728) {
            i2 += CodedOutputStream.A02(30, this.A0c);
        }
        if ((i3 & 268435456) == 268435456) {
            i2 += CodedOutputStream.A02(31, this.A0b);
        }
        if ((i3 & 536870912) == 536870912) {
            i2 += CodedOutputStream.A02(32, this.A09);
        }
        if ((i3 & 1073741824) == 1073741824) {
            i2 += CodedOutputStream.A02(33, this.A06);
        }
        if ((i3 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            i2 += CodedOutputStream.A02(34, this.A0T);
        }
        int i4 = this.A02;
        if ((i4 & 1) == 1) {
            i2 += CodedOutputStream.A02(36, this.A0U);
        }
        if ((i4 & 2) == 2) {
            i2 += CodedOutputStream.A02(37, this.A0e);
        }
        if ((i4 & 4) == 4) {
            i2 += CodedOutputStream.A02(39, this.A0a);
        }
        if ((i4 & 8) == 8) {
            i2 += CodedOutputStream.A02(40, this.A0D);
        }
        if ((i4 & 16) == 16) {
            i2 += CodedOutputStream.A02(41, this.A0C);
        }
        if ((i4 & 32) == 32) {
            i2 += CodedOutputStream.A02(42, this.A0W);
        }
        if ((i4 & 64) == 64) {
            i2 += CodedOutputStream.A02(43, this.A00);
        }
        if ((i4 & 128) == 128) {
            i2 += CodedOutputStream.A02(44, this.A08);
        }
        if ((i4 & 256) == 256) {
            i2 += CodedOutputStream.A02(45, this.A07);
        }
        if ((i4 & 512) == 512) {
            i2 += CodedOutputStream.A02(46, this.A0L);
        }
        if ((i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A02(47, this.A05);
        }
        if ((i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A02(48, this.A0A);
        }
        if ((i4 & 4096) == 4096) {
            i2 += CodedOutputStream.A02(49, this.A0O);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0E(1, this.A0H);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0E(2, this.A0j);
        }
        if ((this.A01 & 4) == 4) {
            codedOutputStream.A0E(3, this.A0F);
        }
        if ((this.A01 & 8) == 8) {
            codedOutputStream.A0E(4, this.A0E);
        }
        if ((this.A01 & 16) == 16) {
            codedOutputStream.A0E(5, this.A04);
        }
        if ((this.A01 & 32) == 32) {
            codedOutputStream.A0E(6, this.A0Q);
        }
        if ((this.A01 & 64) == 64) {
            codedOutputStream.A0E(7, this.A0K);
        }
        if ((this.A01 & 128) == 128) {
            codedOutputStream.A0E(8, this.A0R);
        }
        if ((this.A01 & 256) == 256) {
            codedOutputStream.A0E(9, this.A0i);
        }
        if ((this.A01 & 512) == 512) {
            codedOutputStream.A0E(10, this.A0S);
        }
        if ((this.A01 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0E(11, this.A0P);
        }
        if ((this.A01 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0E(12, this.A0Z);
        }
        if ((this.A01 & 4096) == 4096) {
            codedOutputStream.A0E(13, this.A0J);
        }
        if ((this.A01 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0E(14, this.A0I);
        }
        if ((this.A01 & 16384) == 16384) {
            codedOutputStream.A0E(15, this.A0N);
        }
        if ((this.A01 & 32768) == 32768) {
            codedOutputStream.A0E(18, this.A0M);
        }
        if ((this.A01 & 65536) == 65536) {
            codedOutputStream.A0E(19, this.A0g);
        }
        if ((this.A01 & C25981Bo.A0F) == 131072) {
            codedOutputStream.A0E(20, this.A0f);
        }
        if ((this.A01 & 262144) == 262144) {
            codedOutputStream.A0E(21, this.A0Y);
        }
        if ((this.A01 & 524288) == 524288) {
            codedOutputStream.A0E(22, this.A0k);
        }
        if ((this.A01 & 1048576) == 1048576) {
            codedOutputStream.A0E(23, this.A0d);
        }
        if ((this.A01 & 2097152) == 2097152) {
            codedOutputStream.A0E(24, this.A0B);
        }
        if ((this.A01 & 4194304) == 4194304) {
            codedOutputStream.A0E(25, this.A0G);
        }
        if ((this.A01 & 8388608) == 8388608) {
            codedOutputStream.A0E(26, this.A0V);
        }
        if ((this.A01 & EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING) == 16777216) {
            codedOutputStream.A0E(27, this.A03);
        }
        if ((this.A01 & 33554432) == 33554432) {
            codedOutputStream.A0E(28, this.A0X);
        }
        if ((this.A01 & 67108864) == 67108864) {
            codedOutputStream.A0E(29, this.A0h);
        }
        if ((this.A01 & 134217728) == 134217728) {
            codedOutputStream.A0E(30, this.A0c);
        }
        if ((this.A01 & 268435456) == 268435456) {
            codedOutputStream.A0E(31, this.A0b);
        }
        if ((this.A01 & 536870912) == 536870912) {
            codedOutputStream.A0E(32, this.A09);
        }
        if ((this.A01 & 1073741824) == 1073741824) {
            codedOutputStream.A0E(33, this.A06);
        }
        if ((this.A01 & Integer.MIN_VALUE) == Integer.MIN_VALUE) {
            codedOutputStream.A0E(34, this.A0T);
        }
        if ((this.A02 & 1) == 1) {
            codedOutputStream.A0E(36, this.A0U);
        }
        if ((this.A02 & 2) == 2) {
            codedOutputStream.A0E(37, this.A0e);
        }
        if ((this.A02 & 4) == 4) {
            codedOutputStream.A0E(39, this.A0a);
        }
        if ((this.A02 & 8) == 8) {
            codedOutputStream.A0E(40, this.A0D);
        }
        if ((this.A02 & 16) == 16) {
            codedOutputStream.A0E(41, this.A0C);
        }
        if ((this.A02 & 32) == 32) {
            codedOutputStream.A0E(42, this.A0W);
        }
        if ((this.A02 & 64) == 64) {
            codedOutputStream.A0E(43, this.A00);
        }
        if ((this.A02 & 128) == 128) {
            codedOutputStream.A0E(44, this.A08);
        }
        if ((this.A02 & 256) == 256) {
            codedOutputStream.A0E(45, this.A07);
        }
        if ((this.A02 & 512) == 512) {
            codedOutputStream.A0E(46, this.A0L);
        }
        if ((this.A02 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0E(47, this.A05);
        }
        if ((this.A02 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0E(48, this.A0A);
        }
        if ((this.A02 & 4096) == 4096) {
            codedOutputStream.A0E(49, this.A0O);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
