package X;

import android.app.Dialog;
import android.view.View;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.0EG  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EG extends AnonymousClass05W {
    public final /* synthetic */ DialogFragment A00;
    public final /* synthetic */ AnonymousClass05W A01;

    public AnonymousClass0EG(DialogFragment dialogFragment, AnonymousClass05W r2) {
        this.A00 = dialogFragment;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass05W
    public View A00(int i) {
        AnonymousClass05W r1 = this.A01;
        if (r1.A01()) {
            return r1.A00(i);
        }
        Dialog dialog = this.A00.A03;
        if (dialog != null) {
            return dialog.findViewById(i);
        }
        return null;
    }

    @Override // X.AnonymousClass05W
    public boolean A01() {
        return this.A01.A01() || this.A00.A0B;
    }
}
