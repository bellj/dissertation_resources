package X;

import android.net.Uri;
import com.whatsapp.Conversation;

/* renamed from: X.530  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass530 implements AnonymousClass24S {
    public final /* synthetic */ Conversation A00;

    @Override // X.AnonymousClass24S
    public void AQ8() {
    }

    @Override // X.AnonymousClass24S
    public void AY7(Uri uri) {
    }

    @Override // X.AnonymousClass24S
    public void AY8(Uri uri) {
    }

    public AnonymousClass530(Conversation conversation) {
        this.A00 = conversation;
    }
}
