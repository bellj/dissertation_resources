package X;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.conversation.conversationrow.ConversationRowImage$RowImageView;

/* renamed from: X.3at  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70193at implements AbstractC41521tf {
    public final /* synthetic */ C60782yd A00;

    public C70193at(C60782yd r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.A06.A02.A03();
    }

    @Override // X.AbstractC41521tf
    public void AQV() {
        this.A00.A1O();
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r8) {
        int i;
        ImageView.ScaleType scaleType;
        if (bitmap == null || !(r8 instanceof AbstractC16130oV)) {
            C60782yd r2 = this.A00;
            ConversationRowImage$RowImageView conversationRowImage$RowImageView = r2.A06;
            conversationRowImage$RowImageView.setScaleType(ImageView.ScaleType.CENTER);
            AnonymousClass2GE.A06(r2, conversationRowImage$RowImageView);
            r2.A07 = false;
            return;
        }
        C16150oX A00 = AbstractC15340mz.A00((AbstractC16130oV) r8);
        int i2 = A00.A08;
        if (!(i2 == 0 || (i = A00.A06) == 0)) {
            C60782yd r0 = this.A00;
            boolean z = r0 instanceof C60772yc;
            ConversationRowImage$RowImageView conversationRowImage$RowImageView2 = r0.A06;
            conversationRowImage$RowImageView2.A04(i2, i);
            if (((AbstractC28551Oa) r0).A0R || z) {
                scaleType = ImageView.ScaleType.CENTER_CROP;
            } else {
                scaleType = ImageView.ScaleType.MATRIX;
            }
            conversationRowImage$RowImageView2.setScaleType(scaleType);
        }
        this.A00.A06.setImageBitmap(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        C60782yd r1 = this.A00;
        r1.A07 = false;
        r1.A06.setBackgroundColor(-7829368);
    }
}
