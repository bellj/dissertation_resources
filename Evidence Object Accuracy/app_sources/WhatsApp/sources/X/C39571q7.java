package X;

/* renamed from: X.1q7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C39571q7 implements AbstractC39531q3 {
    public final /* synthetic */ AnonymousClass19M A00;

    public /* synthetic */ C39571q7(AnonymousClass19M r1) {
        this.A00 = r1;
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AbstractC39531q3
    public final android.graphics.Bitmap AKW(android.content.res.Resources r11, int r12) {
        /*
            r10 = this;
            X.19M r0 = r10.A00
            X.1EM r9 = r0.A03
            r8 = 1
            int[] r0 = X.AnonymousClass1EM.A04
            boolean r0 = X.C37871n9.A03(r0, r12)
            if (r0 == 0) goto L_0x0018
            X.0m9 r1 = r9.A00
            r0 = 1884(0x75c, float:2.64E-42)
            boolean r0 = r1.A07(r0)
            r7 = 1
            if (r0 != 0) goto L_0x0019
        L_0x0018:
            r7 = 0
        L_0x0019:
            if (r7 == 0) goto L_0x001f
            java.lang.String r6 = "compressed/emojis/e%04d.obi"
        L_0x001d:
            r3 = 0
            goto L_0x0022
        L_0x001f:
            java.lang.String r6 = "emoji/e%04d.png"
            goto L_0x001d
        L_0x0022:
            android.content.res.AssetManager r5 = r11.getAssets()     // Catch: IOException -> 0x006e
            java.util.Locale r4 = java.util.Locale.US     // Catch: IOException -> 0x006e
            java.lang.Object[] r2 = new java.lang.Object[r8]     // Catch: IOException -> 0x006e
            r1 = 0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r12)     // Catch: IOException -> 0x006e
            r2[r1] = r0     // Catch: IOException -> 0x006e
            java.lang.String r0 = java.lang.String.format(r4, r6, r2)     // Catch: IOException -> 0x006e
            java.io.InputStream r2 = r5.open(r0)     // Catch: IOException -> 0x006e
            if (r7 == 0) goto L_0x0042
            android.graphics.Bitmap r0 = r9.A00(r2, r8)     // Catch: all -> 0x0067
            if (r2 == 0) goto L_0x0066
            goto L_0x0063
        L_0x0042:
            android.util.DisplayMetrics r0 = r11.getDisplayMetrics()     // Catch: all -> 0x0067
            float r1 = r0.density     // Catch: all -> 0x0067
            r0 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x005d
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options     // Catch: all -> 0x0067
            r1.<init>()     // Catch: all -> 0x0067
            r0 = 2
            r1.inSampleSize = r0     // Catch: all -> 0x0067
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2, r3, r1)     // Catch: all -> 0x0067
            if (r2 == 0) goto L_0x0066
            goto L_0x0063
        L_0x005d:
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch: all -> 0x0067
            if (r2 == 0) goto L_0x0066
        L_0x0063:
            r2.close()     // Catch: IOException -> 0x006e
        L_0x0066:
            return r0
        L_0x0067:
            r0 = move-exception
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch: all -> 0x006d
        L_0x006d:
            throw r0     // Catch: IOException -> 0x006e
        L_0x006e:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39571q7.AKW(android.content.res.Resources, int):android.graphics.Bitmap");
    }
}
