package X;

import java.util.Map;
import java.util.Set;

/* renamed from: X.4Zv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93284Zv {
    public final Map A00;
    public final Set A01;

    public C93284Zv() {
        this.A01 = C12970iu.A12();
        this.A00 = C12970iu.A11();
    }

    public C93284Zv(Map map, Set set) {
        this.A01 = set;
        this.A00 = map;
    }
}
