package X;

import android.util.Size;

/* renamed from: X.2NR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2NR {
    public final int A00;
    public final int A01;

    public AnonymousClass2NR(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
    }

    public static AnonymousClass2NR A00(Size size) {
        if (size != null) {
            return new AnonymousClass2NR(size.getWidth(), size.getHeight());
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append("x");
        sb.append(this.A00);
        return sb.toString();
    }
}
