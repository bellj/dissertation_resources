package X;

/* renamed from: X.0OJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0OJ {
    public final long A00;
    public final long A01;

    public AnonymousClass0OJ(long j, long j2) {
        if (j2 == 0) {
            this.A01 = 0;
            this.A00 = 1;
            return;
        }
        this.A01 = j;
        this.A00 = j2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A01);
        sb.append("/");
        sb.append(this.A00);
        return sb.toString();
    }
}
