package X;

import com.whatsapp.jid.GroupJid;
import java.lang.ref.WeakReference;

/* renamed from: X.37R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37R extends AbstractC16350or {
    public final C236812p A00;
    public final GroupJid A01;
    public final WeakReference A02;

    public AnonymousClass37R(C236812p r2, GroupJid groupJid, AnonymousClass5VA r4) {
        this.A00 = r2;
        this.A02 = C12970iu.A10(r4);
        this.A01 = groupJid;
    }
}
