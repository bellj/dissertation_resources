package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.status.StatusesFragment;

/* renamed from: X.2I2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2I2 {
    public View A00;
    public final C14850m9 A01;
    public final AnonymousClass1AT A02;
    public final C253118x A03;

    public AnonymousClass2I2(C14850m9 r1, AnonymousClass1AT r2, C253118x r3) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
    }

    public void A00(ListView listView, AnonymousClass01E r11) {
        int i;
        if (this.A01.A07(1071)) {
            View inflate = r11.A04().inflate(R.layout.e2ee_text_with_image_layout, (ViewGroup) listView, false);
            this.A00 = inflate;
            TextView textView = (TextView) AnonymousClass028.A0D(inflate, R.id.e2ee_main_text);
            if (r11 instanceof StatusesFragment) {
                i = R.string.status_messages_are_e2ee;
            } else {
                boolean z = r11 instanceof CallsHistoryFragment;
                i = R.string.personal_messages_are_e2ee;
                if (z) {
                    i = R.string.personal_calls_are_e2ee;
                }
            }
            textView.setText(this.A03.A03(r11.A01(), new RunnableBRunnable0Shape3S0200000_I0_3(this, 28, r11), r11.A0I(i), "%s", R.color.primary_light));
            textView.setMovementMethod(new C52162aM());
            listView.addFooterView(this.A00);
        }
    }

    public void A01(AnonymousClass01E r4) {
        View view;
        C14850m9 r2 = this.A01;
        if (r2.A07(1071) && (view = this.A00) != null && view.getVisibility() == 0 && r4.A0j && r2.A07(1071)) {
            int i = 8;
            if (r4 instanceof CallsHistoryFragment) {
                i = 6;
            } else if (r4 instanceof ConversationsFragment) {
                i = 7;
            }
            this.A02.A00(i, 0);
        }
    }
}
