package X;

import android.os.Bundle;

/* renamed from: X.0UU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UU {
    public static final EnumC03920Jq A08 = EnumC03920Jq.STATIC;
    public static final EnumC03890Jn A09 = EnumC03890Jn.AUTO;
    public static final EnumC03900Jo A0A = EnumC03900Jo.AUTO;
    public static final EnumC03910Jp A0B = EnumC03910Jp.FULL_SHEET;
    public final EnumC03920Jq A00;
    public final EnumC03890Jn A01;
    public final EnumC03900Jo A02;
    public final EnumC03910Jp A03;
    public final C14260l7 A04;
    public final C14230l4 A05;
    public final AnonymousClass28D A06;
    public final AbstractC14200l1 A07;

    public AnonymousClass0UU(EnumC03920Jq r1, EnumC03890Jn r2, EnumC03900Jo r3, EnumC03910Jp r4, C14260l7 r5, C14230l4 r6, AnonymousClass28D r7, AbstractC14200l1 r8) {
        this.A05 = r6;
        this.A04 = r5;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A07 = r8;
    }

    public static AnonymousClass0J0 A00(String str) {
        try {
            return AnonymousClass0J0.valueOf(str);
        } catch (IllegalArgumentException e) {
            C28691Op.A02("CdsOpenScreenConfig", e);
            return AnonymousClass0J0.NEVER_ANIMATED;
        }
    }

    public static AnonymousClass0J1 A01(String str) {
        try {
            return AnonymousClass0J1.valueOf(str);
        } catch (IllegalArgumentException e) {
            C28691Op.A02("CdsOpenScreenConfig", e);
            return AnonymousClass0J1.FULL_SHEET;
        }
    }

    public static AnonymousClass0UU A02() {
        return new AnonymousClass0UU(A08, A09, A0A, A0B, null, null, null, null);
    }

    public static AnonymousClass0UU A03(Bundle bundle) {
        Object A00;
        Object A002;
        Object A003;
        Object A004;
        int i = bundle.getInt("bloks_interpreter_environment", -1);
        if (i == -1) {
            A00 = null;
        } else {
            A00 = AnonymousClass0RR.A00(C14230l4.class, Integer.valueOf(i));
        }
        C14230l4 r8 = (C14230l4) A00;
        int i2 = bundle.getInt("bloks_context", -1);
        if (i2 == -1) {
            A002 = null;
        } else {
            A002 = AnonymousClass0RR.A00(C14260l7.class, Integer.valueOf(i2));
        }
        C14260l7 r7 = (C14260l7) A002;
        int i3 = bundle.getInt("bloks_model", -1);
        if (i3 == -1) {
            A003 = null;
        } else {
            A003 = AnonymousClass0RR.A00(AnonymousClass28D.class, Integer.valueOf(i3));
        }
        AnonymousClass28D r9 = (AnonymousClass28D) A003;
        EnumC03900Jo A005 = EnumC03900Jo.A00(bundle.getString("drag_to_dismiss", "auto"));
        EnumC03910Jp A006 = EnumC03910Jp.A00(bundle.getString("mode", "full_sheet"));
        EnumC03920Jq A007 = EnumC03920Jq.A00(bundle.getString("background_mode", "static"));
        EnumC03890Jn A008 = EnumC03890Jn.A00(bundle.getString("dimmed_background_tap_to_dismiss", "static"));
        int i4 = bundle.getInt("on_dismiss_callback", -1);
        if (i4 == -1) {
            A004 = null;
        } else {
            A004 = AnonymousClass0RR.A00(AbstractC14200l1.class, Integer.valueOf(i4));
        }
        bundle.getParcelable("native_on_dismiss_callback");
        return new AnonymousClass0UU(A007, A008, A005, A006, r7, r8, r9, (AbstractC14200l1) A004);
    }

    public static void A04(Bundle bundle, Object obj, String str) {
        if (obj != null) {
            int incrementAndGet = AnonymousClass0RR.A02.incrementAndGet();
            synchronized (AnonymousClass0RR.A01) {
                AnonymousClass0RR.A00.append(incrementAndGet, obj);
            }
            bundle.putInt(str, Integer.valueOf(incrementAndGet).intValue());
        }
    }

    public Bundle A05() {
        Bundle bundle = new Bundle();
        bundle.putString("drag_to_dismiss", this.A02.value);
        bundle.putString("mode", this.A03.value);
        bundle.putString("background_mode", this.A00.value);
        bundle.putString("dimmed_background_tap_to_dismiss", this.A01.value);
        A04(bundle, this.A05, "bloks_interpreter_environment");
        A04(bundle, this.A04, "bloks_context");
        A04(bundle, this.A06, "bloks_model");
        A04(bundle, this.A07, "on_dismiss_callback");
        bundle.putParcelable("native_on_dismiss_callback", null);
        return bundle;
    }
}
