package X;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.2kg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class DialogInterface$OnCancelListenerC56312kg extends LifecycleCallback implements DialogInterface.OnCancelListener {
    public final Handler A00 = new HandlerC472529t(Looper.getMainLooper());
    public final AnonymousClass01b A01;
    public final C471729i A02;
    public final C65863Lh A03;
    public final AtomicReference A04 = new AtomicReference(null);
    public volatile boolean A05;

    public DialogInterface$OnCancelListenerC56312kg(C471729i r3, C65863Lh r4, AbstractC116815Wz r5) {
        super(r5);
        this.A02 = r3;
        this.A01 = new AnonymousClass01b(0);
        this.A03 = r4;
        super.A00.A5e(this, "ConnectionlessLifecycleHelper");
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public final void A04(Bundle bundle) {
        AnonymousClass4MG r0;
        if (bundle != null) {
            AtomicReference atomicReference = this.A04;
            if (bundle.getBoolean("resolving_error", false)) {
                r0 = new AnonymousClass4MG(new C56492ky(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1));
            } else {
                r0 = null;
            }
            atomicReference.set(r0);
        }
    }

    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public final void A05(Bundle bundle) {
        AnonymousClass4MG r2 = (AnonymousClass4MG) this.A04.get();
        if (r2 != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", r2.A00);
            C56492ky r22 = r2.A01;
            bundle.putInt("failed_status", r22.A01);
            bundle.putParcelable("failed_resolution", r22.A02);
        }
    }

    @Override // android.content.DialogInterface.OnCancelListener
    public final void onCancel(DialogInterface dialogInterface) {
        int i;
        C56492ky r3 = new C56492ky(13, null);
        AtomicReference atomicReference = this.A04;
        AnonymousClass4MG r0 = (AnonymousClass4MG) atomicReference.get();
        if (r0 == null) {
            i = -1;
        } else {
            i = r0.A00;
        }
        atomicReference.set(null);
        this.A03.A04(r3, i);
    }
}
