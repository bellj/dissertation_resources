package X;

import android.util.Pair;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.2tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59012tp extends AnonymousClass2EI {
    public final AnonymousClass4SY A00;
    public final C89234Je A01;
    public final C19870uo A02;
    public final C17220qS A03;
    public final C19840ul A04;
    public final AnonymousClass1VC A05 = new AnonymousClass1VC();

    public C59012tp(C14650lo r2, AnonymousClass4SY r3, C89234Je r4, C19870uo r5, C17220qS r6, C19840ul r7) {
        super(r2);
        this.A04 = r7;
        this.A03 = r6;
        this.A01 = r4;
        this.A00 = r3;
        this.A02 = r5;
    }

    public final AnonymousClass1V8 A02(String str) {
        ArrayList A0l = C12960it.A0l();
        AnonymousClass4SY r4 = this.A00;
        AnonymousClass2EI.A00("width", Integer.toString(r4.A01), A0l);
        AnonymousClass2EI.A00("height", Integer.toString(r4.A00), A0l);
        AnonymousClass1V8 r5 = new AnonymousClass1V8("image_dimensions", (AnonymousClass1W9[]) null, (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[0]));
        AnonymousClass1V8 r0 = new AnonymousClass1V8("token", r4.A04, (AnonymousClass1W9[]) null);
        ArrayList A0l2 = C12960it.A0l();
        A0l2.add(r5);
        A0l2.add(r0);
        C14650lo r02 = super.A01;
        String A01 = r02.A07.A01(r4.A02);
        if (A01 != null) {
            AnonymousClass2EI.A00("direct_connection_encrypted_info", A01, A0l2);
        }
        AnonymousClass1W9[] r2 = new AnonymousClass1W9[2];
        C12960it.A1M("op", "get", r2, 0);
        r2[1] = new AnonymousClass1W9("id", r4.A03);
        AnonymousClass1V8 r3 = new AnonymousClass1V8("order", r2, (AnonymousClass1V8[]) A0l2.toArray(new AnonymousClass1V8[0]));
        AnonymousClass1W9[] r22 = new AnonymousClass1W9[5];
        C12960it.A1M("smax_id", "5", r22, 0);
        C12960it.A1M("id", str, r22, 1);
        C12960it.A1M("xmlns", "fb:thrift_iq", r22, 2);
        C12960it.A1M("type", "get", r22, 3);
        return AnonymousClass1V8.A00(AnonymousClass1VY.A00, r3, r22, 4);
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A04.A02("order_view_tag");
        StringBuilder A0k = C12960it.A0k("GetOrderProtocol/delivery-error with iqId ");
        A0k.append(str);
        Log.w(C12960it.A0d(">", A0k));
        this.A05.A00(new AnonymousClass2JZ(str));
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        this.A05.A01(new AnonymousClass2EF(C12990iw.A0L(421, "Failed to generate direct connection info"), null));
        Log.i(C12960it.A0b("GetOrderProtocol/onDirectConnectionError/jid= ", userJid));
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        String A01 = this.A03.A01();
        this.A02.A02(this, A02(A01), A01, 248);
        Log.i(C12960it.A0b("GetOrderProtocol/onDirectConnectionSucceeded/Retrying with jid= ", userJid));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        this.A04.A02("order_view_tag");
        Pair A01 = C41151sz.A01(r5);
        if (A01 == null) {
            this.A05.A01(new AnonymousClass2EF(C12990iw.A0L(C12960it.A0V(), "error code is null"), null));
        } else if (!A01(this.A00.A02, C12960it.A05(A01.first))) {
            this.A05.A01(new AnonymousClass2EF(A01, null));
            StringBuilder A0k = C12960it.A0k("GetOrderProtocol/response-error with iqId <");
            A0k.append(str);
            Log.w(C12960it.A0Z(A01, "> and error ", A0k));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x010d  */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r29, java.lang.String r30) {
        /*
        // Method dump skipped, instructions count: 290
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C59012tp.AX9(X.1V8, java.lang.String):void");
    }
}
