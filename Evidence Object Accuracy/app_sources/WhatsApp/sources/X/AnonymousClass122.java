package X;

/* renamed from: X.122  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass122 {
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0035 A[Catch: IllegalArgumentException | JSONException -> 0x0080, TryCatch #0 {IllegalArgumentException | JSONException -> 0x0080, blocks: (B:5:0x0009, B:8:0x0015, B:17:0x0026, B:18:0x002a, B:19:0x002f, B:21:0x0035, B:24:0x003d, B:27:0x0045, B:29:0x0048, B:31:0x0051, B:33:0x0055, B:38:0x005c, B:40:0x0060, B:42:0x0063, B:44:0x0067, B:46:0x006a, B:49:0x0073, B:52:0x007a), top: B:57:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003d A[Catch: IllegalArgumentException | JSONException -> 0x0080, TryCatch #0 {IllegalArgumentException | JSONException -> 0x0080, blocks: (B:5:0x0009, B:8:0x0015, B:17:0x0026, B:18:0x002a, B:19:0x002f, B:21:0x0035, B:24:0x003d, B:27:0x0045, B:29:0x0048, B:31:0x0051, B:33:0x0055, B:38:0x005c, B:40:0x0060, B:42:0x0063, B:44:0x0067, B:46:0x006a, B:49:0x0073, B:52:0x007a), top: B:57:0x0009 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C31091Zz A00(org.json.JSONArray r12) {
        /*
            int r0 = r12.length()
            r6 = 2
            r5 = 0
            if (r0 < r6) goto L_0x0086
            r0 = 0
            int r11 = r12.getInt(r0)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            r0 = 1
            java.lang.String r4 = r12.getString(r0)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r11 == 0) goto L_0x0015
            goto L_0x001a
        L_0x0015:
            java.lang.String r3 = r12.getString(r6)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            goto L_0x001d
        L_0x001a:
            if (r11 == r6) goto L_0x0015
            r3 = r5
        L_0x001d:
            r2 = 3
            if (r11 != r6) goto L_0x0021
            goto L_0x0026
        L_0x0021:
            r1 = r5
            if (r11 == r6) goto L_0x002a
            r7 = r5
            goto L_0x002f
        L_0x0026:
            java.lang.String r1 = r12.getString(r2)     // Catch: JSONException | IllegalArgumentException -> 0x0080
        L_0x002a:
            r0 = 4
            java.lang.String r7 = r12.getString(r0)     // Catch: JSONException | IllegalArgumentException -> 0x0080
        L_0x002f:
            byte[] r8 = android.util.Base64.decode(r4, r2)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r3 == 0) goto L_0x003a
            byte[] r9 = android.util.Base64.decode(r3, r2)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            goto L_0x003b
        L_0x003a:
            r9 = r5
        L_0x003b:
            if (r1 == 0) goto L_0x0042
            byte[] r10 = android.util.Base64.decode(r1, r2)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            goto L_0x0043
        L_0x0042:
            r10 = r5
        L_0x0043:
            if (r8 == 0) goto L_0x007a
            int r0 = r8.length     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 <= 0) goto L_0x007a
            X.1Zz r6 = new X.1Zz     // Catch: JSONException | IllegalArgumentException -> 0x0080
            r6.<init>(r7, r8, r9, r10, r11)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            int r1 = r6.A00     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r1 != 0) goto L_0x0059
            byte[] r0 = r6.A03     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 == 0) goto L_0x0073
            int r0 = r0.length     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 <= 0) goto L_0x0073
            return r6
        L_0x0059:
            r0 = 2
            if (r1 != r0) goto L_0x0079
            byte[] r0 = r6.A03     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 == 0) goto L_0x0073
            int r0 = r0.length     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 <= 0) goto L_0x0073
            byte[] r0 = r6.A04     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 == 0) goto L_0x0073
            int r0 = r0.length     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 <= 0) goto L_0x0073
            java.lang.String r0 = r6.A01     // Catch: JSONException | IllegalArgumentException -> 0x0080
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            if (r0 != 0) goto L_0x0073
            return r6
        L_0x0073:
            java.lang.String r0 = "KeyData/failed to parse json/key data not valid"
            com.whatsapp.util.Log.e(r0)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            return r5
        L_0x0079:
            return r6
        L_0x007a:
            java.lang.String r0 = "KeyData/failed to parse json/wrong data"
            com.whatsapp.util.Log.e(r0)     // Catch: JSONException | IllegalArgumentException -> 0x0080
            return r5
        L_0x0080:
            r1 = move-exception
            java.lang.String r0 = "KeyData/failed to parse json"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x0086:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass122.A00(org.json.JSONArray):X.1Zz");
    }
}
