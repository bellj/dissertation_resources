package X;

import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilConfirmReceivePaymentFragment;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.ConfirmReceivePaymentFragment;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/* renamed from: X.6AJ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AJ implements AnonymousClass6MO {
    public final /* synthetic */ BrazilConfirmReceivePaymentFragment A00;
    public final /* synthetic */ PaymentBottomSheet A01;
    public final /* synthetic */ String A02;

    public AnonymousClass6AJ(BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment, PaymentBottomSheet paymentBottomSheet, String str) {
        this.A00 = brazilConfirmReceivePaymentFragment;
        this.A02 = str;
        this.A01 = paymentBottomSheet;
    }

    @Override // X.AnonymousClass6MO
    public void ANl(C30881Ze r4) {
        Log.i("PAY: BrazilConfirmReceivePayment BrazilGetVerificationMethods - onCardVerified");
        ((ConfirmReceivePaymentFragment) this.A00).A04.A00().A03(new AbstractC451720l(r4, this, this.A01) { // from class: X.680
            public final /* synthetic */ C30881Ze A00;
            public final /* synthetic */ AnonymousClass6AJ A01;
            public final /* synthetic */ PaymentBottomSheet A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                AnonymousClass6AJ r0 = this.A01;
                C30881Ze r6 = this.A00;
                PaymentBottomSheet paymentBottomSheet = this.A02;
                BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment = r0.A00;
                brazilConfirmReceivePaymentFragment.A00.A03();
                C130065yk r3 = brazilConfirmReceivePaymentFragment.A0I;
                Intent A0D = C12990iw.A0D(brazilConfirmReceivePaymentFragment.A0B(), BrazilPayBloksActivity.class);
                A0D.putExtra("screen_params", r3.A02(r6, null));
                A0D.putExtra("screen_name", "brpay_p_card_verified");
                brazilConfirmReceivePaymentFragment.A0v(A0D);
                if (paymentBottomSheet != null) {
                    paymentBottomSheet.A1B();
                }
            }
        }, r4);
    }

    @Override // X.AnonymousClass6MO
    public void AVP(C452120p r7, ArrayList arrayList) {
        JSONArray A02;
        BrazilConfirmReceivePaymentFragment brazilConfirmReceivePaymentFragment = this.A00;
        brazilConfirmReceivePaymentFragment.A00.A03();
        if (r7 != null || arrayList == null || arrayList.isEmpty() || (A02 = brazilConfirmReceivePaymentFragment.A08.A02(arrayList)) == null || C1309660r.A01(arrayList)) {
            Log.i(C12960it.A0f(C12960it.A0k("PAY: BrazilConfirmReceivePayment GetVerificationMethods Error: "), 0));
            brazilConfirmReceivePaymentFragment.A09.A01(brazilConfirmReceivePaymentFragment.A0p(), brazilConfirmReceivePaymentFragment.A06, brazilConfirmReceivePaymentFragment.A0B, 0, R.string.payment_verify_card_error).show();
            return;
        }
        String str = this.A02;
        String obj = A02.toString();
        C17070qD r0 = ((ConfirmReceivePaymentFragment) brazilConfirmReceivePaymentFragment).A04;
        r0.A03();
        C30881Ze r2 = (C30881Ze) r0.A09.A08(str);
        if (r2 != null) {
            brazilConfirmReceivePaymentFragment.A0v(brazilConfirmReceivePaymentFragment.A0I.A00(brazilConfirmReceivePaymentFragment.A0B(), r2, obj));
        }
        PaymentBottomSheet paymentBottomSheet = this.A01;
        if (paymentBottomSheet != null) {
            paymentBottomSheet.A1B();
        }
    }
}
