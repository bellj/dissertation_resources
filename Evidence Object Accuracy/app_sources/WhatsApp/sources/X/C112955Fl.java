package X;

import java.util.Arrays;

/* renamed from: X.5Fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112955Fl implements AnonymousClass5XE {
    public int A00;
    public AnonymousClass5XE A01;
    public boolean A02;
    public byte[] A03;
    public byte[] A04;
    public byte[] A05;

    public C112955Fl(AnonymousClass5XE r3) {
        this.A01 = r3;
        int AAt = r3.AAt();
        this.A00 = AAt;
        this.A03 = new byte[AAt];
        this.A05 = new byte[AAt];
        this.A04 = new byte[AAt];
    }

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return this.A01.AAt();
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A01);
        return C12960it.A0d("/CBC", A0h);
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r7, boolean z) {
        boolean z2 = this.A02;
        this.A02 = z;
        if (r7 instanceof C113075Fx) {
            C113075Fx r72 = (C113075Fx) r7;
            byte[] bArr = r72.A01;
            int length = bArr.length;
            if (length == this.A00) {
                System.arraycopy(bArr, 0, this.A03, 0, length);
                reset();
                r7 = r72.A00;
                if (r7 == null) {
                    if (z2 != z) {
                        throw C12970iu.A0f("cannot change encrypting state without providing key.");
                    }
                    return;
                }
            } else {
                throw C12970iu.A0f("initialisation vector must be the same length as block size");
            }
        } else {
            reset();
            if (r7 == null) {
                if (z2 != z) {
                    throw C12970iu.A0f("cannot change encrypting state without providing key.");
                }
                return;
            }
        }
        this.A01.AIf(r7, z);
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        if (this.A02) {
            int i3 = this.A00;
            if (i3 + i <= bArr.length) {
                for (int i4 = 0; i4 < i3; i4++) {
                    byte[] bArr3 = this.A05;
                    C72453ed.A1S(bArr, bArr3, i + i4, bArr3[i4], i4);
                }
                int AZY = this.A01.AZY(this.A05, bArr2, 0, i2);
                byte[] bArr4 = this.A05;
                System.arraycopy(bArr2, i2, bArr4, 0, bArr4.length);
                return AZY;
            }
            throw new AnonymousClass5O2("input buffer too short");
        }
        int i5 = this.A00;
        if (i + i5 <= bArr.length) {
            System.arraycopy(bArr, i, this.A04, 0, i5);
            int AZY2 = this.A01.AZY(bArr, bArr2, i, i2);
            for (int i6 = 0; i6 < i5; i6++) {
                int i7 = i2 + i6;
                C72453ed.A1S(this.A05, bArr2, i6, bArr2[i7], i7);
            }
            byte[] bArr5 = this.A05;
            this.A05 = this.A04;
            this.A04 = bArr5;
            return AZY2;
        }
        throw new AnonymousClass5O2("input buffer too short");
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
        byte[] bArr = this.A03;
        System.arraycopy(bArr, 0, this.A05, 0, bArr.length);
        Arrays.fill(this.A04, (byte) 0);
        this.A01.reset();
    }
}
