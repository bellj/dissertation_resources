package X;

import android.content.Intent;
import android.net.Uri;

/* renamed from: X.2I7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2I7 {
    public final C21740xu A00;

    public AnonymousClass2I7(C21740xu r1) {
        this.A00 = r1;
    }

    public Intent A00(String str) {
        StringBuilder sb = new StringBuilder("https://play.google.com/store/apps/details?id=com.whatsapp.w4b&utm_source=");
        sb.append(str);
        return new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
    }
}
