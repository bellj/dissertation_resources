package X;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;

/* renamed from: X.2cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53132cx extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass12P A00;
    public C15550nR A01;
    public C15600nX A02;
    public C15370n3 A03;
    public AnonymousClass1E5 A04;
    public C15580nU A05;
    public AnonymousClass2P7 A06;
    public boolean A07;
    public final View A08;
    public final View A09;
    public final View A0A;
    public final View A0B;

    public C53132cx(Context context) {
        super(context);
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A00 = C12980iv.A0W(A00);
            this.A01 = C12960it.A0O(A00);
            this.A04 = (AnonymousClass1E5) A00.AKs.get();
            this.A02 = C12980iv.A0d(A00);
        }
        View inflate = LinearLayout.inflate(getContext(), R.layout.community_home_report, this);
        this.A0A = inflate;
        this.A09 = AnonymousClass028.A0D(inflate, R.id.community_no_longer_description);
        View A0D = AnonymousClass028.A0D(inflate, R.id.report_community_button);
        this.A0B = A0D;
        C12960it.A14(A0D, this, context, 12);
        View A0D2 = AnonymousClass028.A0D(inflate, R.id.delete_community_btn);
        this.A08 = A0D2;
        C12960it.A14(A0D2, this, context, 13);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }
}
