package X;

/* renamed from: X.35i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C621935i extends C38711oa {
    public final AnonymousClass016 A00;
    public final AnonymousClass016 A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass1BT A03;
    public final C235512c A04;

    public C621935i(AnonymousClass016 r1, AnonymousClass016 r2, AnonymousClass016 r3, AnonymousClass1BT r4, C235512c r5) {
        super(r5);
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r1;
        this.A02 = r2;
        this.A00 = r3;
    }

    @Override // X.C38711oa
    public Void A08(Void... voidArr) {
        this.A03.A05();
        this.A01.A0A(Boolean.TRUE);
        if (((AbstractC16350or) this).A02.isCancelled()) {
            return null;
        }
        this.A00.A0A(new C90424Nv(this.A04.A0D(0)));
        this.A02.A0A(C12960it.A0V());
        return super.A08(voidArr);
    }
}
