package X;

import com.whatsapp.util.Log;

/* renamed from: X.1vz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42851vz implements AbstractC21730xt {
    public final int A00;
    public final AbstractC15710nm A01;
    public final C20710wC A02;
    public final C17220qS A03;

    public C42851vz(AbstractC15710nm r1, C20710wC r2, C17220qS r3, int i) {
        this.A01 = r1;
        this.A03 = r3;
        this.A02 = r2;
        this.A00 = i;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("GroupRequestProtocolHelper/onDeliveryFailure/iqId=");
        sb.append(str);
        Log.e(sb.toString());
        this.A02.A0D(this.A00);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r3, String str) {
        StringBuilder sb = new StringBuilder("GroupRequestProtocolHelper/onError/iqId=");
        sb.append(str);
        Log.e(sb.toString());
        this.A02.A0D(this.A00);
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0203 A[Catch: all -> 0x020f, TryCatch #3 {1MW -> 0x0212, blocks: (B:4:0x0016, B:5:0x0020, B:7:0x0026, B:10:0x007d, B:13:0x0089, B:16:0x0095, B:19:0x00a1, B:22:0x00ae, B:25:0x00bb, B:27:0x00d4, B:28:0x00f0, B:30:0x00f6, B:31:0x0100, B:33:0x011e, B:34:0x0152, B:36:0x0158, B:38:0x0162, B:40:0x016a, B:41:0x0183, B:44:0x0195, B:47:0x019a, B:49:0x01a3, B:50:0x01a9, B:51:0x01b5, B:53:0x01b8, B:66:0x01ff, B:68:0x0203, B:69:0x0206, B:71:0x0208, B:74:0x020e), top: B:79:0x0016 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0208 A[Catch: all -> 0x020f, TryCatch #3 {1MW -> 0x0212, blocks: (B:4:0x0016, B:5:0x0020, B:7:0x0026, B:10:0x007d, B:13:0x0089, B:16:0x0095, B:19:0x00a1, B:22:0x00ae, B:25:0x00bb, B:27:0x00d4, B:28:0x00f0, B:30:0x00f6, B:31:0x0100, B:33:0x011e, B:34:0x0152, B:36:0x0158, B:38:0x0162, B:40:0x016a, B:41:0x0183, B:44:0x0195, B:47:0x019a, B:49:0x01a3, B:50:0x01a9, B:51:0x01b5, B:53:0x01b8, B:66:0x01ff, B:68:0x0203, B:69:0x0206, B:71:0x0208, B:74:0x020e), top: B:79:0x0016 }] */
    @Override // X.AbstractC21730xt
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AX9(X.AnonymousClass1V8 r41, java.lang.String r42) {
        /*
        // Method dump skipped, instructions count: 547
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42851vz.AX9(X.1V8, java.lang.String):void");
    }
}
