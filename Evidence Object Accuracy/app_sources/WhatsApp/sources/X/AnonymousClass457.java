package X;

/* renamed from: X.457  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass457 extends C36261ja {
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass457(AbstractC14640lm r2, AbstractView$OnCreateContextMenuListenerC35851ir r3) {
        super(r2, true);
        this.A00 = r3;
    }

    @Override // X.C36261ja
    public void A02(int i) {
        super.A02(i);
        if (i > 0) {
            this.A00.A0C = (long) i;
        }
        this.A00.A0K();
    }
}
