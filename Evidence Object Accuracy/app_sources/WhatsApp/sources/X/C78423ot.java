package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78423ot extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98714j8();
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final long A05;
    public final long A06;
    public final String A07;
    public final String A08;

    public C78423ot(String str, String str2, int i, int i2, int i3, int i4, int i5, long j, long j2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A05 = j;
        this.A06 = j2;
        this.A07 = str;
        this.A08 = str2;
        this.A03 = i4;
        this.A04 = i5;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A07(parcel, 2, this.A01);
        C95654e8.A07(parcel, 3, this.A02);
        C95654e8.A08(parcel, 4, this.A05);
        C95654e8.A08(parcel, 5, this.A06);
        C95654e8.A0D(parcel, this.A07, 6, false);
        C95654e8.A0D(parcel, this.A08, 7, false);
        C95654e8.A07(parcel, 8, this.A03);
        C95654e8.A07(parcel, 9, this.A04);
        C95654e8.A06(parcel, A00);
    }
}
