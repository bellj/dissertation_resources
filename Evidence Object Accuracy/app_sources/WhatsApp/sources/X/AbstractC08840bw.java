package X;

import java.io.Closeable;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0bw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC08840bw implements Closeable {
    public Map A00 = new HashMap();

    public abstract int A00();

    public abstract boolean A01();

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public abstract void close();

    @Override // java.lang.Object
    public void finalize() {
        if (!A01()) {
            AnonymousClass0UN.A03("CloseableImage", "finalize: %s %x still open.", getClass().getSimpleName(), Integer.valueOf(System.identityHashCode(this)));
            try {
                close();
            } finally {
                super.finalize();
            }
        }
    }
}
