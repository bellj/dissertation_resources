package X;

import android.util.SparseIntArray;
import com.whatsapp.util.Log;
import java.nio.MappedByteBuffer;
import java.nio.charset.Charset;
import java.util.List;

/* renamed from: X.1Tj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29511Tj {
    public final int A00;
    public final SparseIntArray A01 = new SparseIntArray();
    public final SparseIntArray A02 = new SparseIntArray();
    public final MappedByteBuffer A03;
    public final Charset A04;

    public C29511Tj(MappedByteBuffer mappedByteBuffer, List list) {
        this.A03 = mappedByteBuffer;
        int A00 = A00(0);
        int A01 = A01(2);
        byte b = mappedByteBuffer.get(6);
        Charset[] charsetArr = AnonymousClass1Th.A02;
        if (b >= charsetArr.length && C27221Gm.A00.get() != null) {
            Log.e("MMappedStringPack: unrecognized encoding");
        }
        this.A04 = charsetArr[b];
        this.A00 = A01(7);
        if (!list.isEmpty()) {
            int size = list.size();
            int[] iArr = new int[size];
            int i = 11;
            int i2 = 0;
            for (int i3 = 0; i3 < A00; i3++) {
                MappedByteBuffer mappedByteBuffer2 = this.A03;
                mappedByteBuffer2.position(i);
                int position = mappedByteBuffer2.position();
                int i4 = 5;
                if (mappedByteBuffer2.get(position + 2) == 0) {
                    i4 = 2;
                } else if (mappedByteBuffer2.get(position + 5) != 0) {
                    i4 = 7;
                }
                byte[] bArr = new byte[i4];
                mappedByteBuffer2.get(bArr, 0, i4);
                int indexOf = list.indexOf(new String(bArr, 0, i4, AnonymousClass1Th.A01));
                if (indexOf != -1) {
                    i2++;
                    iArr[indexOf] = i;
                    if (i2 >= list.size()) {
                        break;
                    }
                }
                i += 11;
            }
            for (int i5 = 0; i5 < size; i5++) {
                int i6 = iArr[i5];
                if (i6 != 0) {
                    this.A03.position(i6 + 7);
                    int A012 = A01(this.A03.position()) + A01;
                    int A002 = A00(A012);
                    int i7 = A012 + 2;
                    int A003 = A00(i7);
                    int i8 = i7 + 2;
                    for (int i9 = 0; i9 < A002; i9++) {
                        int i10 = i8 + 2;
                        this.A02.append(A00(i8), i10);
                        i8 = i10 + 6;
                    }
                    for (int i11 = 0; i11 < A003; i11++) {
                        int i12 = i8 + 2;
                        this.A01.append(A00(i8), i12);
                        i8 = i12 + 1;
                        for (int i13 = 0; i13 < this.A03.get(i12); i13++) {
                            i8 += 7;
                        }
                    }
                }
            }
        } else if (C27221Gm.A00.get() != null) {
            Log.e("MMappedStringPack: parentLocales is empty");
        }
    }

    public final int A00(int i) {
        MappedByteBuffer mappedByteBuffer = this.A03;
        return ((mappedByteBuffer.get(i + 1) & 255) << 8) | (mappedByteBuffer.get(i) & 255);
    }

    public final int A01(int i) {
        MappedByteBuffer mappedByteBuffer = this.A03;
        return ((mappedByteBuffer.get(i + 3) & 255) << 24) | (mappedByteBuffer.get(i) & 255) | ((mappedByteBuffer.get(i + 1) & 255) << 8) | ((mappedByteBuffer.get(i + 2) & 255) << 16);
    }

    public synchronized String A02(int i) {
        String str;
        int i2 = this.A02.get(i);
        if (i2 == 0) {
            str = null;
        } else {
            MappedByteBuffer mappedByteBuffer = this.A03;
            mappedByteBuffer.position(i2);
            int position = mappedByteBuffer.position();
            int A01 = A01(position);
            int A00 = A00(position + 4);
            byte[] bArr = new byte[A00];
            mappedByteBuffer.position(this.A00 + A01);
            mappedByteBuffer.get(bArr, 0, A00);
            str = new String(bArr, this.A04);
        }
        return str;
    }
}
