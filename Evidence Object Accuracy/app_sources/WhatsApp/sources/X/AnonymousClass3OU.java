package X;

import android.widget.SeekBar;
import com.whatsapp.search.views.itemviews.AudioPlayerView;

/* renamed from: X.3OU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OU implements SeekBar.OnSeekBarChangeListener {
    public AbstractC102584pN A00;
    public boolean A01;
    public final AnonymousClass11P A02;
    public final AudioPlayerView A03;
    public final AbstractC116165Uj A04;
    public final AnonymousClass01H A05;

    public AnonymousClass3OU(AnonymousClass11P r1, AudioPlayerView audioPlayerView, AbstractC116165Uj r3, AbstractC102584pN r4, AnonymousClass01H r5) {
        this.A03 = audioPlayerView;
        this.A04 = r3;
        this.A02 = r1;
        this.A05 = r5;
        this.A00 = r4;
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (z) {
            AbstractC102584pN r0 = this.A00;
            r0.onProgressChanged(seekBar, i, z);
            r0.A00(i / 1000);
            AudioPlayerView audioPlayerView = this.A03;
            audioPlayerView.setSeekbarContentDescription((long) audioPlayerView.A07.getProgress());
        }
        AudioPlayerView audioPlayerView2 = this.A03;
        if (audioPlayerView2.A04.isEnabled()) {
            audioPlayerView2.A04.setPlaybackPercentage((((float) i) * 1.0f) / ((float) audioPlayerView2.A00));
        }
        C35191hP.A01(this.A04.ACs(), audioPlayerView2.A07.getProgress());
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStartTrackingTouch(SeekBar seekBar) {
        C30421Xi ACs = this.A04.ACs();
        this.A01 = false;
        AnonymousClass11P r2 = this.A02;
        C35191hP A00 = r2.A00();
        if (r2.A0D(ACs) && r2.A0B() && A00 != null) {
            A00.A0E(true);
            this.A01 = true;
        }
    }

    @Override // android.widget.SeekBar.OnSeekBarChangeListener
    public void onStopTrackingTouch(SeekBar seekBar) {
        C30421Xi ACs = this.A04.ACs();
        AbstractC102584pN r2 = this.A00;
        r2.onStopTrackingTouch(seekBar);
        AnonymousClass11P r1 = this.A02;
        if (!r1.A0D(ACs) || r1.A0B() || !this.A01) {
            r2.A00(((AbstractC16130oV) ACs).A00);
            int progress = this.A03.A07.getProgress();
            ((AnonymousClass19F) this.A05.get()).Acg(ACs.A11, progress);
            C35191hP.A01(ACs, progress);
            return;
        }
        int i = 0;
        this.A01 = false;
        C35191hP A00 = r1.A00();
        if (A00 != null) {
            A00.A09(this.A03.A07.getProgress());
            if (ACs.A1C()) {
                i = C35191hP.A0x;
            }
            A00.A0A(i, true, false);
        }
    }
}
