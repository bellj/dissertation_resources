package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/* renamed from: X.0sS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18440sS {
    public AbstractC18440sS(AnonymousClass14A r1) {
        r1.A03(this);
    }

    public void A00(boolean z) {
        AnonymousClass1O4 r1;
        int i;
        if (!(this instanceof AnonymousClass19O)) {
            if (this instanceof AnonymousClass14B) {
                C18720su r12 = ((AnonymousClass14B) this).A00;
                AnonymousClass1O4 r0 = r12.A0N;
                if (z) {
                    if (r0 != null) {
                        r1 = r12.A0N;
                        i = SearchActionVerificationClientService.NOTIFICATION_ID;
                    } else {
                        return;
                    }
                } else if (r0 != null) {
                    r1 = r12.A0N;
                    i = 0;
                } else {
                    return;
                }
            } else if (!(this instanceof AnonymousClass1EL)) {
                C18720su r3 = (C18720su) this;
                int i2 = 0;
                if (z) {
                    i2 = SearchActionVerificationClientService.NOTIFICATION_ID;
                }
                synchronized (this) {
                    r3.AKm(null);
                    Map map = r3.A07;
                    for (WeakReference weakReference : map.values()) {
                        List list = (List) weakReference.get();
                        if (list != null) {
                            list.clear();
                        }
                        weakReference.clear();
                    }
                    map.clear();
                    r3.A01().A02(i2);
                    r3.A02().A02(i2);
                    if (r3.A0M != null) {
                        r3.A0M.A02(SearchActionVerificationClientService.NOTIFICATION_ID);
                    }
                    synchronized (r3.A0K) {
                        C006202y r13 = r3.A01;
                        if (r13 != null) {
                            r13.A06(-1);
                        }
                    }
                    synchronized (r3.A0C) {
                        AnonymousClass1O4 r14 = r3.A02;
                        if (r14 != null) {
                            r14.A02(0);
                        }
                    }
                    AnonymousClass169 r15 = r3.A09;
                    synchronized (r15) {
                        r15.A01.clear();
                    }
                    r3.AKm(null);
                }
            } else {
                C18720su r16 = ((AnonymousClass1EL) this).A00;
                AnonymousClass1O4 r02 = r16.A0M;
                if (z) {
                    if (r02 != null) {
                        r1 = r16.A0M;
                        i = SearchActionVerificationClientService.NOTIFICATION_ID;
                    } else {
                        return;
                    }
                } else if (r02 != null) {
                    r1 = r16.A0M;
                    i = 0;
                } else {
                    return;
                }
            }
            r1.A02(i);
            return;
        }
        AnonymousClass19O r03 = (AnonymousClass19O) this;
        synchronized (this) {
            r03.A05.clear();
        }
    }
}
