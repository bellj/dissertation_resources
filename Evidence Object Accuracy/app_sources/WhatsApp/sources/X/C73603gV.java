package X;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.whatsapp.voicerecorder.VoiceNoteSeekBar;

/* renamed from: X.3gV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73603gV extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ VoiceNoteSeekBar A00;

    public C73603gV(VoiceNoteSeekBar voiceNoteSeekBar) {
        this.A00 = voiceNoteSeekBar;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
        VoiceNoteSeekBar voiceNoteSeekBar = this.A00;
        if (voiceNoteSeekBar.isLongClickable()) {
            voiceNoteSeekBar.performLongClick();
        }
    }
}
