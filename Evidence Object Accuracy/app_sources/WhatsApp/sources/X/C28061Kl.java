package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1Kl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28061Kl {
    public static boolean A00(C15550nR r3, C22700zV r4, C14850m9 r5, AbstractC15340mz r6) {
        if (!r5.A07(1105) || r6 == null) {
            return false;
        }
        AbstractC14640lm r0 = r6.A0z.A00;
        AnonymousClass009.A05(r0);
        C15370n3 A08 = r3.A08(r0);
        if (A08 == null) {
            return false;
        }
        UserJid userJid = (UserJid) A08.A0B(UserJid.class);
        if (AnonymousClass3GN.A01(r5, userJid) || !new C38301nr(r4, userJid).A03()) {
            return false;
        }
        return true;
    }

    public static boolean A01(C22700zV r2, C14850m9 r3, UserJid userJid) {
        if (!r3.A07(1105) || userJid == null || AnonymousClass3GN.A01(r3, userJid) || !new C38301nr(r2, userJid).A03()) {
            return false;
        }
        return true;
    }
}
