package X;

import android.content.DialogInterface;
import androidx.biometric.FingerprintDialogFragment;

/* renamed from: X.0V5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V5 implements DialogInterface.OnClickListener {
    public final /* synthetic */ FingerprintDialogFragment A00;

    public AnonymousClass0V5(FingerprintDialogFragment fingerprintDialogFragment) {
        this.A00 = fingerprintDialogFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        this.A00.A04.A06(true);
    }
}
