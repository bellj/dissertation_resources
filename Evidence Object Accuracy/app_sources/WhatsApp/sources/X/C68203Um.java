package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.3Um  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C68203Um implements AnonymousClass5XB {
    public int A00;
    public int A01;
    public List A02;
    public boolean A03;
    public final int A04;
    public final C44741zT A05;
    public final AnonymousClass5TN A06;
    public final AnonymousClass5TO A07;
    public final AnonymousClass2E5 A08;
    public final WeakReference A09;

    public C68203Um(ImageView imageView, C44741zT r3, AnonymousClass5TN r4, AnonymousClass5TO r5, AnonymousClass2E5 r6, int i, int i2, int i3) {
        this.A05 = r3;
        this.A04 = i;
        this.A08 = r6;
        this.A06 = r4;
        this.A07 = r5;
        this.A01 = i2;
        this.A00 = i3;
        this.A09 = C12970iu.A10(imageView);
    }

    public boolean A00() {
        View view = (View) this.A09.get();
        if (view == null) {
            return !this.A03;
        }
        String str = (String) view.getTag(R.id.image_id);
        int A05 = C12960it.A05(view.getTag(R.id.image_quality));
        if (this.A03 || !str.equals(this.A05.A04)) {
            return false;
        }
        if (A05 == this.A04 || A05 == 1) {
            return true;
        }
        return false;
    }

    @Override // X.AnonymousClass5XB
    public boolean A9o() {
        return C12970iu.A1W(this.A04);
    }

    @Override // X.AnonymousClass5XB
    public int AE9() {
        return this.A00;
    }

    @Override // X.AnonymousClass5XB
    public int AEB() {
        return this.A01;
    }

    @Override // X.AnonymousClass5XB
    public String AHT() {
        String str;
        return (this.A04 != 2 || (str = this.A05.A01) == null) ? this.A05.A00 : str;
    }

    @Override // X.AnonymousClass5XB
    public String getId() {
        String str = this.A05.A04;
        int i = this.A04;
        StringBuilder A0j = C12960it.A0j(str);
        A0j.append('_');
        if (i == 1) {
            i = 3;
        }
        return C12960it.A0f(A0j, i);
    }
}
