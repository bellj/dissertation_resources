package X;

/* renamed from: X.1f7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33811f7 extends AbstractC16110oT {
    public Boolean A00;
    public Double A01;
    public Integer A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;

    public C33811f7() {
        super(2200, new AnonymousClass00E(1, 20, 200), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(9, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(5, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(7, this.A05);
        r3.Abe(8, this.A06);
        r3.Abe(2, this.A07);
        r3.Abe(4, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamImageDownloadHashMismatch {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isStreaming", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaSize", this.A01);
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scan1Length", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scan2Length", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scan3Length", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scan4Length", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scanForHashMismatch", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "scansAvailable", this.A08);
        sb.append("}");
        return sb.toString();
    }
}
