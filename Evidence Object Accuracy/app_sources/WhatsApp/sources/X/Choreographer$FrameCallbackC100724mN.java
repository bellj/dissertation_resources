package X;

import android.view.Choreographer;

/* renamed from: X.4mN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class Choreographer$FrameCallbackC100724mN implements Choreographer.FrameCallback {
    public final /* synthetic */ AnonymousClass4SM A00;

    public Choreographer$FrameCallbackC100724mN(AnonymousClass4SM r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0172, code lost:
        if (r8 > r0) goto L_0x017c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x017a, code lost:
        if (r8 < r0) goto L_0x017c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01cd  */
    @Override // android.view.Choreographer.FrameCallback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doFrame(long r46) {
        /*
        // Method dump skipped, instructions count: 533
        */
        throw new UnsupportedOperationException("Method not decompiled: X.Choreographer$FrameCallbackC100724mN.doFrame(long):void");
    }
}
