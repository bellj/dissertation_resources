package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4xO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107474xO implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(22);
    public final long A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final long A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107474xO(long j, long j2, long j3, long j4, long j5) {
        this.A02 = j;
        this.A01 = j2;
        this.A00 = j3;
        this.A04 = j4;
        this.A03 = j5;
    }

    public /* synthetic */ C107474xO(Parcel parcel) {
        this.A02 = parcel.readLong();
        this.A01 = parcel.readLong();
        this.A00 = parcel.readLong();
        this.A04 = parcel.readLong();
        this.A03 = parcel.readLong();
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107474xO.class != obj.getClass()) {
                return false;
            }
            C107474xO r7 = (C107474xO) obj;
            if (!(this.A02 == r7.A02 && this.A01 == r7.A01 && this.A00 == r7.A00 && this.A04 == r7.A04 && this.A03 == r7.A03)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return C72453ed.A0A(C72453ed.A0A(C72453ed.A0A(C72453ed.A05(C72453ed.A0B(this.A02)), this.A01), this.A00), this.A04) + C72453ed.A0B(this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Motion photo metadata: photoStartPosition=");
        A0k.append(this.A02);
        A0k.append(", photoSize=");
        A0k.append(this.A01);
        A0k.append(", photoPresentationTimestampUs=");
        A0k.append(this.A00);
        A0k.append(", videoStartPosition=");
        A0k.append(this.A04);
        A0k.append(", videoSize=");
        return C12970iu.A0w(A0k, this.A03);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A02);
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeLong(this.A04);
        parcel.writeLong(this.A03);
    }
}
