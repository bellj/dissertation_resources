package X;

import java.util.Iterator;
import java.util.ListIterator;

/* renamed from: X.29D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass29D extends AnonymousClass1Mr<E> {
    public final transient int length;
    public final transient int offset;
    public final /* synthetic */ AnonymousClass1Mr this$0;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return true;
    }

    public AnonymousClass29D(AnonymousClass1Mr r1, int i, int i2) {
        this.this$0 = r1;
        this.offset = i;
        this.length = i2;
    }

    @Override // java.util.List
    public Object get(int i) {
        C28291Mn.A01(i, this.length);
        return this.this$0.get(i + this.offset);
    }

    @Override // X.AbstractC17950rf
    public Object[] internalArray() {
        return this.this$0.internalArray();
    }

    @Override // X.AbstractC17950rf
    public int internalArrayEnd() {
        return this.this$0.internalArrayStart() + this.offset + this.length;
    }

    @Override // X.AbstractC17950rf
    public int internalArrayStart() {
        return this.this$0.internalArrayStart() + this.offset;
    }

    @Override // X.AnonymousClass1Mr, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public /* bridge */ /* synthetic */ ListIterator listIterator() {
        return super.listIterator();
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public /* bridge */ /* synthetic */ ListIterator listIterator(int i) {
        return super.listIterator(i);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.length;
    }

    @Override // X.AnonymousClass1Mr, java.util.List
    public AnonymousClass1Mr subList(int i, int i2) {
        C28291Mn.A03(i, i2, this.length);
        AnonymousClass1Mr r1 = this.this$0;
        int i3 = this.offset;
        return r1.subList(i + i3, i2 + i3);
    }
}
