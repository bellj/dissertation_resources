package X;

/* renamed from: X.3BL  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BL {
    public final AnonymousClass1V8 A00;
    public final AnonymousClass3CT A01;

    public AnonymousClass3BL(AnonymousClass3CT r4) {
        C41141sy A00 = C41141sy.A00();
        C41141sy.A01(A00, "xmlns", "urn:xmpp:whatsapp:account");
        A00.A07(r4.A00, C12960it.A0l());
        this.A01 = r4;
        this.A00 = A00.A03();
    }
}
