package X;

/* renamed from: X.5wX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC128705wX {
    public final AbstractC15710nm A00;
    public final C17220qS A01;

    public AbstractC128705wX(AbstractC15710nm r1, C17220qS r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(AbstractC128495wC r14, Object obj, int i) {
        C17220qS r6 = this.A01;
        String A01 = r6.A01();
        C91334Ri r15 = (C91334Ri) obj;
        C41141sy A0M = C117295Zj.A0M();
        A0M.A04(new AnonymousClass1W9("smax_id", i));
        C41141sy.A01(A0M, "id", A01);
        C41141sy.A01(A0M, "xmlns", "fb:graphql");
        C41141sy.A01(A0M, "type", "get");
        A0M.A04(new AnonymousClass1W9(AnonymousClass1VY.A00, "to"));
        C41141sy r5 = new C41141sy("auth_metadata");
        C117315Zl.A0X(r5, "timestamp", C117295Zj.A03(((C119885fG) this).A00));
        C41141sy.A01(r5, "version", "1");
        C117295Zj.A1H(r5, A0M);
        C41141sy r3 = new C41141sy("encryption_metadata");
        C41141sy.A01(r3, "version", "1");
        C41141sy.A01(r3, "algorithm", "rsa2048");
        C41141sy r1 = new C41141sy("encrypted_key");
        r1.A01 = r15.A01;
        C117295Zj.A1H(r1, r3);
        C41141sy r12 = new C41141sy("encrypted_data");
        r12.A01 = r15.A00;
        C117295Zj.A1H(r12, r3);
        C41141sy r13 = new C41141sy("nonce");
        r13.A01 = r15.A02;
        C117295Zj.A1H(r13, r3);
        C41141sy r16 = new C41141sy("auth_tag");
        r16.A01 = r15.A03;
        C117295Zj.A1H(r16, r3);
        C117295Zj.A1H(r3, A0M);
        r6.A0D(new AnonymousClass6DK(this, r14), A0M.A03(), A01, 264, 32000);
    }
}
