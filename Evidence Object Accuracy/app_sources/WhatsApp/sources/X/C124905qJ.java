package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.5qJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124905qJ {
    public static C119935fL A00(String str, List list, Map map, int i, int i2) {
        AnonymousClass009.A0B(C12960it.A0f(C12960it.A0k("BloksFieldStatParser/parseFieldStat/invalid serialization/length="), list.size()), C12960it.A1T(list.size() % 4));
        int size = list.size() >> 2;
        ArrayList A0l = C12960it.A0l();
        for (int i3 = 0; i3 < size; i3++) {
            int i4 = i3 << 2;
            int A05 = C12960it.A05(list.get(i4 + 2));
            Object obj = list.get(i4 + 3);
            Object A00 = C124915qK.A00(obj, A05, false);
            if (A00 != null) {
                int A052 = C12960it.A05(list.get(i4));
                A0l.add(new C127585uj(A00, C124915qK.A00(obj, A05, true), C12960it.A0g(list, i4 + 1), A052));
            }
        }
        return new C119935fL(new AnonymousClass00E("1".equals(map.get("log_all_for_debug")), C28421Nd.A00(C12970iu.A0t("sample_rate_debug", map), 1), C28421Nd.A00(C12970iu.A0t("sample_rate_beta", map), 20), C28421Nd.A00(C12970iu.A0t("sample_rate_beta", map), 20), C28421Nd.A00(C12970iu.A0t("sample_rate_release", map), 20)), str, (C127585uj[]) A0l.toArray(new C127585uj[0]), i, i2);
    }
}
