package X;

/* renamed from: X.116  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass116 {
    public final C15570nT A00;
    public final C15890o4 A01;

    public AnonymousClass116(C15570nT r1, C15890o4 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public boolean A00() {
        C15890o4 r1 = this.A01;
        if (r1.A02("android.permission.READ_CONTACTS") == 0 && r1.A02("android.permission.WRITE_CONTACTS") == 0) {
            return true;
        }
        this.A00.A08();
        return false;
    }
}
