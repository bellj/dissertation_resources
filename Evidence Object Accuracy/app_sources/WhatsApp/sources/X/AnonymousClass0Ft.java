package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0Ft  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Ft extends AnonymousClass0ZL {
    @Override // X.AbstractC12410hs
    public float ADB(View view, ViewGroup viewGroup) {
        boolean z = true;
        if (AnonymousClass028.A05(viewGroup) != 1) {
            z = false;
        }
        float translationX = view.getTranslationX();
        float width = (float) viewGroup.getWidth();
        if (z) {
            return translationX - width;
        }
        return translationX + width;
    }
}
