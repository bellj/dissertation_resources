package X;

/* renamed from: X.3Ec  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64063Ec {
    public final long A00;
    public final C16820po A01;
    public final AnonymousClass1ZR A02;
    public final AnonymousClass1ZR A03;
    public final AnonymousClass1ZR A04;
    public final Long A05;
    public final String A06;

    public C64063Ec(C16820po r6, Long l, String str, String str2, String str3, long j, long j2) {
        this.A03 = new AnonymousClass1ZR(new AnonymousClass2SM(), Long.class, Long.valueOf(j), "WaFbid");
        this.A04 = new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, str, "WaFbPassword");
        this.A02 = new AnonymousClass1ZR(new AnonymousClass2SM(), Long.class, str2, "WaFbAccessToken");
        this.A00 = j2;
        this.A05 = l;
        this.A01 = r6;
        this.A06 = str3;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C64063Ec) {
                C64063Ec r6 = (C64063Ec) obj;
                Object obj2 = this.A03.A00;
                AnonymousClass009.A05(obj2);
                Object obj3 = r6.A03.A00;
                AnonymousClass009.A05(obj3);
                if (C29941Vi.A00(obj2, obj3)) {
                    Object obj4 = this.A04.A00;
                    AnonymousClass009.A05(obj4);
                    Object obj5 = r6.A04.A00;
                    AnonymousClass009.A05(obj5);
                    if (C29941Vi.A00(obj4, obj5)) {
                        Object obj6 = this.A02.A00;
                        AnonymousClass009.A05(obj6);
                        Object obj7 = r6.A02.A00;
                        AnonymousClass009.A05(obj7);
                        if (!C29941Vi.A00(obj6, obj7) || !C29941Vi.A00(Long.valueOf(this.A00), Long.valueOf(r6.A00)) || !C29941Vi.A00(this.A05, r6.A05) || !C29941Vi.A00(this.A01, r6.A01)) {
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[6];
        Object obj = this.A03.A00;
        AnonymousClass009.A05(obj);
        objArr[0] = obj;
        Object obj2 = this.A04.A00;
        AnonymousClass009.A05(obj2);
        objArr[1] = obj2;
        objArr[2] = this.A02;
        objArr[3] = Long.valueOf(this.A00);
        objArr[4] = this.A05;
        return C12980iv.A0B(this.A01, objArr, 5);
    }
}
