package X;

/* renamed from: X.4ZS  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ZS {
    public static final byte[] A00 = {0, 0, 0, 1};
    public static final String[] A01 = {"", "A", "B", "C"};

    public static String A00(AnonymousClass4YP r13) {
        r13.A05(24);
        int A02 = r13.A02(2);
        boolean A06 = r13.A06();
        int A022 = r13.A02(5);
        int i = 0;
        for (int i2 = 0; i2 < 32; i2++) {
            if (r13.A06()) {
                i |= 1 << i2;
            }
        }
        int i3 = 6;
        int[] iArr = new int[6];
        for (int i4 = 0; i4 < 6; i4++) {
            iArr[i4] = r13.A02(8);
        }
        int A023 = r13.A02(8);
        Object[] objArr = new Object[5];
        objArr[0] = A01[A02];
        C12960it.A1P(objArr, A022, 1);
        C12960it.A1P(objArr, i, 2);
        char c = 'L';
        if (A06) {
            c = 'H';
        }
        objArr[3] = Character.valueOf(c);
        C12960it.A1P(objArr, A023, 4);
        StringBuilder A0k = C12960it.A0k(C72463ee.A0G("hvc1.%s%d.%X.%c%d", objArr));
        while (true) {
            if (iArr[i3 - 1] != 0) {
                int i5 = 0;
                do {
                    Object[] objArr2 = new Object[1];
                    C12960it.A1P(objArr2, iArr[i5], 0);
                    A0k.append(String.format(".%02X", objArr2));
                    i5++;
                } while (i5 < i3);
                break;
            }
            i3--;
            if (i3 <= 0) {
                break;
            }
        }
        return A0k.toString();
    }
}
