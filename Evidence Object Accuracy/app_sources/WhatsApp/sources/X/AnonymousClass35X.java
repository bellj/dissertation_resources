package X;

import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.mediaview.PhotoView;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.ExoPlayerErrorFrame;
import java.io.File;
import java.util.List;

/* renamed from: X.35X  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35X extends AbstractC33641ei {
    public float A00 = 0.0f;
    public AnonymousClass2VE A01;
    public AnonymousClass21T A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07 = true;
    public final View A08;
    public final FrameLayout A09;
    public final FrameLayout A0A;
    public final C15450nH A0B;
    public final C14850m9 A0C;
    public final C244415n A0D;
    public final DoodleView A0E;
    public final PhotoView A0F;
    public final AnonymousClass1X3 A0G;
    public final AnonymousClass5WI A0H;
    public final AnonymousClass1CJ A0I;
    public final AnonymousClass19O A0J;
    public final AbstractC14440lR A0K;
    public final ExoPlayerErrorFrame A0L;
    public final AnonymousClass1CI A0M;

    public AnonymousClass35X(AnonymousClass12P r17, C14330lG r18, C14900mE r19, C15450nH r20, AnonymousClass01d r21, AnonymousClass018 r22, AnonymousClass19M r23, C14850m9 r24, C244415n r25, AnonymousClass1CH r26, AbstractC15340mz r27, C48242Fd r28, AnonymousClass1CJ r29, AnonymousClass1AB r30, AnonymousClass19O r31, AbstractC14440lR r32, AnonymousClass1CI r33) {
        super(r17, r19, r21, r22, r26, r28);
        String str;
        this.A0C = r24;
        this.A0B = r20;
        this.A0D = r25;
        this.A0K = r32;
        this.A0J = r31;
        this.A0M = r33;
        this.A0I = r29;
        AnonymousClass009.A05(r27);
        AnonymousClass1X3 r7 = (AnonymousClass1X3) r27;
        this.A0G = r7;
        this.A0A = (FrameLayout) AnonymousClass12P.A00(A02()).findViewById(R.id.video_playback_container);
        this.A08 = AnonymousClass12P.A00(A02()).findViewById(R.id.video_playback_container_overlay);
        FrameLayout frameLayout = new FrameLayout(A02());
        this.A09 = frameLayout;
        PhotoView photoView = new PhotoView(A02());
        this.A0F = photoView;
        photoView.A01 = 0.0f;
        photoView.A07(false);
        photoView.A0V = false;
        photoView.setEnabled(false);
        photoView.setBackgroundColor(A02().getResources().getColor(R.color.black));
        ExoPlayerErrorFrame exoPlayerErrorFrame = new ExoPlayerErrorFrame(A02(), null);
        this.A0L = exoPlayerErrorFrame;
        frameLayout.addView(photoView);
        frameLayout.addView(exoPlayerErrorFrame);
        frameLayout.setId(R.id.status_playback_video);
        A0J();
        C16150oX A00 = AbstractC15340mz.A00(r7);
        DoodleView doodleView = null;
        if (r7.A0z.A02 && !A00.A0P && !A00.A0O && (str = A00.A0H) != null) {
            File A0I = C22200yh.A0I(r18, str);
            if (A0I.exists()) {
                doodleView = new DoodleView(A02());
                AnonymousClass3JD A02 = AnonymousClass3JD.A02(A02(), r22, r23, r30, A0I);
                if (A02 != null) {
                    doodleView.setLayerType(1, null);
                    doodleView.setEnabled(false);
                    doodleView.setDoodle(A02);
                    C12980iv.A1D(doodleView, frameLayout);
                }
            }
        }
        this.A0E = doodleView;
        this.A0H = new AnonymousClass59M(this);
    }

    @Override // X.AbstractC33641ei
    public long A01() {
        long j;
        AnonymousClass1X3 r0 = this.A0G;
        C16150oX A00 = AbstractC15340mz.A00(r0);
        long j2 = 0;
        if (r0.A0z.A02 && !A00.A0P && !A00.A0O) {
            long j3 = A00.A0D;
            if (j3 >= 0) {
                long j4 = A00.A0E;
                if (j4 > 0) {
                    j = j4 - j3;
                    return Math.min(((long) this.A0B.A02(AbstractC15460nI.A28)) * 1000, j);
                }
            }
        }
        AnonymousClass21T r02 = this.A02;
        if (r02 != null) {
            j2 = (long) r02.A02();
        }
        j = j2;
        return Math.min(((long) this.A0B.A02(AbstractC15460nI.A28)) * 1000, j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x007c, code lost:
        if (r7 != false) goto L_0x0051;
     */
    @Override // X.AbstractC33641ei
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07() {
        /*
            r11 = this;
            X.1CJ r2 = r11.A0I
            X.5WI r1 = r11.A0H
            java.util.List r0 = r2.A04
            if (r0 != 0) goto L_0x000e
            java.util.ArrayList r0 = X.C12960it.A0l()
            r2.A04 = r0
        L_0x000e:
            r0.add(r1)
            boolean r0 = r2.A05
            r11.A0K(r0)
            boolean r0 = r11.A06
            r10 = 0
            if (r0 == 0) goto L_0x0020
            r11.A06 = r10
            r11.A0E()
        L_0x0020:
            boolean r0 = r11.A04
            if (r0 != 0) goto L_0x0078
            r11.A0F()
            X.21T r9 = r11.A02
            if (r9 == 0) goto L_0x007f
            X.1X3 r0 = r11.A0G
            X.0oX r8 = X.AbstractC15340mz.A00(r0)
            X.1IS r0 = r0.A0z
            boolean r7 = r0.A02
            if (r7 == 0) goto L_0x0079
            boolean r0 = r8.A0P
            if (r0 != 0) goto L_0x0079
            boolean r0 = r8.A0O
            if (r0 != 0) goto L_0x0079
            long r3 = r8.A0D
            r5 = 0
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 < 0) goto L_0x0079
            long r1 = r8.A0E
            int r0 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x0079
            int r0 = (int) r3
            r9.A09(r0)
        L_0x0051:
            boolean r0 = r8.A0P
            if (r0 != 0) goto L_0x0061
            boolean r0 = r8.A0O
            if (r0 != 0) goto L_0x0061
            boolean r0 = r8.A0N
            if (r0 == 0) goto L_0x0061
            r0 = 1
            r11.A0K(r0)
        L_0x0061:
            X.21T r0 = r11.A02
            r0.A07()
            com.whatsapp.mediacomposer.doodle.DoodleView r2 = r11.A0E
            if (r2 == 0) goto L_0x0075
            X.21v r1 = r2.A0E
            r0 = 1
            r1.A0A = r0
            android.os.SystemClock.elapsedRealtime()
            r2.invalidate()
        L_0x0075:
            r11.A0I()
        L_0x0078:
            return
        L_0x0079:
            r9.A09(r10)
            if (r7 == 0) goto L_0x0061
            goto L_0x0051
        L_0x007f:
            java.lang.String r0 = "video player is null for "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            X.1X3 r0 = r11.A0G
            X.1IS r0 = r0.A0z
            java.lang.String r0 = X.C12970iu.A0s(r0, r1)
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass35X.A07():void");
    }

    @Override // X.AbstractC33641ei
    public void A08() {
        A0G();
        AnonymousClass21T r1 = this.A02;
        if (r1 != null && !r1.A0D()) {
            r1.A08();
        }
        DoodleView doodleView = this.A0E;
        if (doodleView != null) {
            doodleView.A0E.A0A = false;
            doodleView.invalidate();
        }
        A0D();
        A0H();
        this.A08.setVisibility(0);
        A0J();
        this.A00 = 0.0f;
        this.A07 = true;
        AnonymousClass1CJ r0 = this.A0I;
        AnonymousClass5WI r12 = this.A0H;
        List list = r0.A04;
        if (list != null) {
            list.remove(r12);
        }
    }

    public final int A0C() {
        AnonymousClass21T r6 = this.A02;
        if (r6 == null) {
            return 0;
        }
        AnonymousClass1X3 r0 = this.A0G;
        C16150oX A00 = AbstractC15340mz.A00(r0);
        if (!r0.A0z.A02 || A00.A0P || A00.A0O || A00.A0D < 0 || A00.A0E <= 0) {
            return r6.A01();
        }
        return r6.A01() - ((int) A00.A0D);
    }

    public final void A0D() {
        AnonymousClass1CJ r1 = this.A0I;
        Log.i("AudioManager/on-abandon-audio-focus");
        if (r1.A03 == this) {
            Handler handler = r1.A02;
            Runnable runnable = r1.A07;
            handler.removeCallbacks(runnable);
            r1.A02.postDelayed(runnable, 1000);
        }
        this.A03 = false;
    }

    public final void A0E() {
        PhotoView photoView = this.A0F;
        if (photoView.getVisibility() == 0) {
            View A0G = C12970iu.A0G(AnonymousClass12P.A00(A02()));
            this.A0J.A07(photoView, this.A0G, new C70353b9(this, Math.max(A0G.getWidth(), A0G.getHeight())));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0137  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0F() {
        /*
        // Method dump skipped, instructions count: 748
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass35X.A0F():void");
    }

    public final void A0G() {
        StringBuilder A0k = C12960it.A0k("videoContainer=");
        boolean z = true;
        A0k.append(C12960it.A1T(this.A0A.getVisibility()));
        A0k.append("videoPlaybackContainerOverlay=");
        A0k.append(C12960it.A1T(this.A08.getVisibility()));
        A0k.append("photoView=");
        A0k.append(C12960it.A1T(this.A0F.getVisibility()));
        A0k.append("mainView=");
        if (this.A09.getVisibility() != 0) {
            z = false;
        }
        A0k.append(z);
        A0k.append(" isPlaybackStarted=");
        A0k.append(super.A05.A00.A05);
        A0k.toString();
    }

    public final void A0H() {
        A0G();
        this.A0L.setVisibility(8);
        AnonymousClass21T r1 = this.A02;
        if (r1 != null) {
            r1.A01 = null;
            r1.A03 = null;
            r1.A02 = null;
            r1.A00 = null;
            if (r1.A0D()) {
                AnonymousClass21S r2 = this.A0M.A00;
                if (r2 != null) {
                    C47492Ax r0 = r2.A08;
                    if (r0 == null || r0.AFl() == 1) {
                        r2.A0M = false;
                    } else {
                        r2.A0M = true;
                        r2.A08.A0A(false);
                    }
                }
            } else {
                r1.A08();
            }
            this.A02.A06();
            this.A02 = null;
        }
    }

    public final void A0I() {
        if (!this.A05 && !this.A03 && super.A05.A00.A05) {
            this.A03 = true;
            this.A0I.A02(this);
        }
    }

    public final void A0J() {
        PhotoView photoView = this.A0F;
        if (photoView.getVisibility() != 0) {
            A0G();
            photoView.setVisibility(0);
        }
    }

    public void A0K(boolean z) {
        if (!(this instanceof AnonymousClass35W)) {
            this.A05 = z;
            AnonymousClass21T r0 = this.A02;
            if (r0 != null) {
                r0.A0A(z);
            }
            A0I();
        }
    }
}
