package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;

/* renamed from: X.1iP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1iP extends Handler {
    public final /* synthetic */ C21370xJ A00;
    public final /* synthetic */ C21380xK A01;
    public final /* synthetic */ AnonymousClass12H A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1iP(Looper looper, C21370xJ r2, C21380xK r3, AnonymousClass12H r4) {
        super(looper);
        this.A01 = r3;
        this.A02 = r4;
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        C22170ye r10;
        long j;
        String str;
        AbstractC15340mz r3 = (AbstractC15340mz) message.obj;
        int i = message.what;
        if (i == 3) {
            AnonymousClass12H r2 = this.A02;
            for (AbstractC18860tB r4 : r2.A01()) {
                r2.A00++;
                if (r4 instanceof C22520zD) {
                    C22520zD r42 = (C22520zD) r4;
                    if (r3 != null && r3.A12 >= -1) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("app/message/received/duplicate ");
                        AnonymousClass1IS r6 = r3.A0z;
                        String str2 = r6.A01;
                        sb.append(str2);
                        sb.append(" ");
                        C15570nT r5 = r42.A04;
                        r5.A08();
                        sb.append(r5.A05);
                        sb.append(" ");
                        Jid jid = r6.A00;
                        sb.append(jid);
                        sb.append(" ");
                        sb.append(r3.A0B());
                        Log.i(sb.toString());
                        if (!r6.A02) {
                            r42.A0d.A04(r3);
                        } else if (r3 instanceof C30461Xm) {
                            C30461Xm r0 = (C30461Xm) r3;
                            AnonymousClass1OT r1 = r0.A04;
                            if (r1 != null) {
                                r42.A0f.A0E(r1);
                            } else if (((AnonymousClass1XB) r0).A00 == 6) {
                                r10 = r42.A0d;
                                j = r3.A14;
                                str = "picture";
                                r10.A03(jid, str2, str, j);
                            }
                        } else {
                            EnumC35661iT r12 = r3.A0O;
                            if (r12 == EnumC35661iT.RELAY || r12 == EnumC35661iT.RETRY) {
                                r42.A0j.A0F(str2, 200);
                                r10 = r42.A0d;
                                j = r3.A14;
                                r5.A08();
                                C27621Ig r02 = r5.A01;
                                AnonymousClass009.A05(r02);
                                jid = r02.A0D;
                                str = "web";
                                r10.A03(jid, str2, str, j);
                            }
                        }
                    }
                }
            }
        } else if (i == 4) {
            C21370xJ r13 = this.A00;
            AbstractC14640lm r03 = r3.A0z.A00;
            AnonymousClass009.A05(r03);
            r13.A01(r03);
            this.A02.A07(r3, message.arg1);
        } else if (i == 5) {
            C21380xK r43 = this.A01;
            r43.A04.A07(r3, message.arg1);
            AnonymousClass1IR r22 = r3.A0L;
            if (r22 != null) {
                for (AbstractC35651iS r04 : r43.A05.A01()) {
                    r04.ATe(r22);
                }
            }
            C21370xJ r23 = r43.A03;
            AbstractC14640lm r14 = r3.A0z.A00;
            AnonymousClass009.A05(r14);
            r23.A03(r14, false);
        } else if (i == 6) {
            this.A00.A00();
        } else if (i == 7) {
            AnonymousClass12H r52 = this.A02;
            for (AbstractC18860tB r62 : r52.A01()) {
                r52.A00++;
                if (r62 instanceof C22520zD) {
                    C22520zD r63 = (C22520zD) r62;
                    if (r63.A0M.A01()) {
                        r63.A0f.A06.A08(Message.obtain(null, 0, 22, 0, new RunnableBRunnable0Shape1S0100000_I0_1(r63, 15)), false);
                    }
                }
            }
        }
    }
}
