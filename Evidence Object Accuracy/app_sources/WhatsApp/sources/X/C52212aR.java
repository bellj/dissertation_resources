package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.companionmode.registration.ConvertPrimaryToCompanionActivity;

/* renamed from: X.2aR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52212aR extends ClickableSpan {
    public final /* synthetic */ ConvertPrimaryToCompanionActivity A00;

    public C52212aR(ConvertPrimaryToCompanionActivity convertPrimaryToCompanionActivity) {
        this.A00 = convertPrimaryToCompanionActivity;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        ConvertPrimaryToCompanionActivity convertPrimaryToCompanionActivity = this.A00;
        Context context = convertPrimaryToCompanionActivity.A02.A00;
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(context.getPackageName(), "com.whatsapp.backup.google.SettingsGoogleDrive");
        convertPrimaryToCompanionActivity.startActivity(A0A);
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(this.A00.getResources().getColor(R.color.hyper_link_color));
        textPaint.setUnderlineText(false);
    }
}
