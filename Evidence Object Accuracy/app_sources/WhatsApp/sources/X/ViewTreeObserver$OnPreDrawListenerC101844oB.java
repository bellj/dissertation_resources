package X;

import android.view.View;
import android.view.ViewTreeObserver;

/* renamed from: X.4oB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101844oB implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass23G A00;

    public ViewTreeObserver$OnPreDrawListenerC101844oB(AnonymousClass23G r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass23G r3 = this.A00;
        View view = r3.A02;
        C12980iv.A1G(view, this);
        if (r3.A00 != -1) {
            return true;
        }
        r3.A00 = view.getHeight();
        return true;
    }
}
