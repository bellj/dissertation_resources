package X;

import android.widget.Filter;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.1kx */
/* loaded from: classes2.dex */
public class C36961kx extends Filter {
    public ArrayList A00;
    public boolean A01;
    public final Object A02 = new Object();
    public final /* synthetic */ CallsHistoryFragment A03;

    public /* synthetic */ C36961kx(CallsHistoryFragment callsHistoryFragment) {
        this.A03 = callsHistoryFragment;
    }

    public static /* synthetic */ void A00(C36961kx r2) {
        synchronized (r2.A02) {
            r2.A00 = null;
        }
    }

    public final ArrayList A01(Collection collection) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            C64503Fu r3 = (C64503Fu) it.next();
            ArrayList arrayList3 = r3.A03;
            if (arrayList3.isEmpty() || ((AnonymousClass1YT) arrayList3.get(0)).A06 == null) {
                arrayList.add(new C1100854e(r3));
            } else {
                arrayList2.add(new C1101054g(r3));
            }
        }
        if (!arrayList2.isEmpty()) {
            arrayList.addAll(0, arrayList2);
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0106, code lost:
        if (r3.A01.A0M(r2, r6, true) != false) goto L_0x0065;
     */
    @Override // android.widget.Filter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.widget.Filter.FilterResults performFiltering(java.lang.CharSequence r16) {
        /*
        // Method dump skipped, instructions count: 400
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36961kx.performFiltering(java.lang.CharSequence):android.widget.Filter$FilterResults");
    }

    @Override // android.widget.Filter
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        CallsHistoryFragment callsHistoryFragment;
        ArrayList arrayList;
        String charSequence2;
        Object obj = filterResults.values;
        if (obj == null) {
            Log.e("voip/CallsFragment/publishResults got null values: exception in performFiltering?");
            callsHistoryFragment = this.A03;
            arrayList = A01(callsHistoryFragment.A0g.values());
        } else {
            callsHistoryFragment = this.A03;
            arrayList = (ArrayList) obj;
        }
        callsHistoryFragment.A0f = arrayList;
        callsHistoryFragment.A0d = charSequence;
        if (charSequence == null) {
            charSequence2 = null;
        } else {
            charSequence2 = charSequence.toString();
        }
        callsHistoryFragment.A0e = C32751cg.A02(callsHistoryFragment.A0K, charSequence2);
        callsHistoryFragment.A1D();
        callsHistoryFragment.A07.notifyDataSetChanged();
    }
}
