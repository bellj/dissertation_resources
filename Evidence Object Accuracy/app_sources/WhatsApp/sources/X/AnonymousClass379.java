package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.379  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass379 extends AbstractC16350or {
    public final WeakReference A00;
    public final WeakReference A01;

    public AnonymousClass379(Context context, C89424Jx r3) {
        this.A01 = C12970iu.A10(r3);
        this.A00 = C12970iu.A10(context);
    }
}
