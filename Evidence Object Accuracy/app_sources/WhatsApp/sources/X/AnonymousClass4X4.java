package X;

import android.media.AudioTrack;
import android.os.Handler;

/* renamed from: X.4X4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4X4 {
    public final AudioTrack.StreamEventCallback A00;
    public final Handler A01 = new Handler();
    public final /* synthetic */ C106534vr A02;

    public AnonymousClass4X4(C106534vr r2) {
        this.A02 = r2;
        this.A00 = new C73173fn(this, r2);
    }

    public void A00(AudioTrack audioTrack) {
        audioTrack.registerStreamEventCallback(new AnonymousClass5E1(this.A01), this.A00);
    }

    public void A01(AudioTrack audioTrack) {
        audioTrack.unregisterStreamEventCallback(this.A00);
        this.A01.removeCallbacksAndMessages(null);
    }
}
