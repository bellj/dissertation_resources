package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.messagejob.AsyncMessageTokenizationJob;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/* renamed from: X.0mn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15240mn {
    public static final Pattern A0G = Pattern.compile("((?<= )|(?= ))");
    public static final int[] A0H = {105, 118, 103, 97, 100, C43951xu.A03};
    public final C15570nT A00;
    public final C20670w8 A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final AnonymousClass018 A04;
    public final C16370ot A05;
    public final C16510p9 A06;
    public final C20830wO A07;
    public final AnonymousClass15Q A08;
    public final C18460sU A09;
    public final C20040v7 A0A;
    public final C20850wQ A0B;
    public final C16490p7 A0C;
    public final C21390xL A0D;
    public final C21230x5 A0E;
    public final Map A0F = new HashMap();

    public C15240mn(C15570nT r2, C20670w8 r3, C15550nR r4, C15610nY r5, AnonymousClass018 r6, C16370ot r7, C16510p9 r8, C20830wO r9, AnonymousClass15Q r10, C18460sU r11, C20040v7 r12, C20850wQ r13, C16490p7 r14, C21390xL r15, C21230x5 r16) {
        this.A09 = r11;
        this.A06 = r8;
        this.A00 = r2;
        this.A01 = r3;
        this.A0A = r12;
        this.A02 = r4;
        this.A03 = r5;
        this.A04 = r6;
        this.A0D = r15;
        this.A05 = r7;
        this.A0C = r14;
        this.A08 = r10;
        this.A0E = r16;
        this.A07 = r9;
        this.A0B = r13;
        A0K(new C34911gu());
        A0K(new C34931gw());
    }

    public static final String A00(C30441Xk r4, String str) {
        String str2;
        String str3 = "";
        if (r4 == null) {
            str2 = str3;
        } else {
            str2 = r4.A00;
            str3 = r4.A01;
        }
        if (!TextUtils.isEmpty(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" ");
            sb.append(str2);
            str2 = sb.toString();
        }
        if (TextUtils.isEmpty(str3)) {
            return str2;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str2);
        sb2.append(" ");
        sb2.append(str3);
        return sb2.toString();
    }

    public static String A01(List list, List list2) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("SELECT ");
        sb2.append(C16500p8.A00);
        sb2.append(" FROM ");
        sb2.append("message_view AS message JOIN ( ");
        sb.append(sb2.toString());
        sb.append("SELECT message_row_id FROM labeled_messages JOIN labels ON labeled_messages.label_id = labels._id WHERE label_name = ?");
        list2.add(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            sb.append(" INTERSECT ");
            sb.append("SELECT message_row_id FROM labeled_messages JOIN labels ON labeled_messages.label_id = labels._id WHERE label_name = ?");
            list2.add(list.get(i));
        }
        sb.append(") ON message._id = message_row_id");
        return sb.toString();
    }

    public static boolean A02(AnonymousClass02N r1) {
        return r1 != null && r1.A04();
    }

    public static final boolean A03(AbstractC15340mz r1, boolean z) {
        AbstractC14640lm r0 = r1.A0z.A00;
        return r0 == null || C15380n4.A0N(r0) || (r1 instanceof C34771gg) || (r1 instanceof AbstractC30391Xf) || (r1 instanceof AnonymousClass1XK) || (!z && (r1 instanceof C30061Vy)) || (r1 instanceof AnonymousClass1XB) || (r1 instanceof AnonymousClass1XI) || (r1 instanceof AnonymousClass1XG);
    }

    public long A04() {
        long A01 = this.A0D.A01("fts_ready", 0);
        if (!(A01 == 0 || A01 == 1 || A01 == 3)) {
            if (A01 == 2 || A01 == 5) {
                return 5;
            }
            AnonymousClass009.A07("Unknown Fts version, falling back to V1");
        }
        return 1;
    }

    /* JADX INFO: finally extract failed */
    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        	at java.util.BitSet.or(BitSet.java:940)
        	at jadx.core.utils.BlockUtils.lambda$getPathCross$3(BlockUtils.java:689)
        	at java.util.ArrayList.forEach(ArrayList.java:1259)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:689)
        	at jadx.core.utils.BlockUtils.getPathCross(BlockUtils.java:728)
        	at jadx.core.dex.visitors.regions.IfMakerHelper.restructureIf(IfMakerHelper.java:88)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:707)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:732)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:737)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    public android.util.Pair A05(X.AnonymousClass02N r22, X.C15250mo r23, java.lang.Integer r24) {
        /*
        // Method dump skipped, instructions count: 1239
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15240mn.A05(X.02N, X.0mo, java.lang.Integer):android.util.Pair");
    }

    public final C34981h1 A06(C34981h1 r14, String str) {
        C16310on A02 = this.A0C.A02();
        try {
            ContentValues contentValues = new ContentValues(2);
            long j = r14.A02;
            contentValues.put("docid", Long.valueOf(j));
            contentValues.put("content", str);
            try {
                r14 = new C34981h1(1, A02.A03.A03(contentValues, "messages_fts"), r14.A01);
            } catch (SQLiteConstraintException unused) {
                contentValues.remove("docid");
                A02.A03.A00("messages_fts", contentValues, "docid = ?", new String[]{String.valueOf(j)});
            }
            A02.close();
            return r14;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused2) {
            }
            throw th;
        }
    }

    public C34981h1 A07(AbstractC15340mz r21, Map map, long j, boolean z) {
        String A0D;
        int i;
        if (z || A0O()) {
            AnonymousClass1IS r6 = r21.A0z;
            AbstractC14640lm r5 = r6.A00;
            if (A03(r21, false)) {
                i = -6;
            } else {
                String A0E = A0E(r21);
                String str = (String) map.get(Long.valueOf(r21.A12));
                if (str == null) {
                    if (A0E.length() >= 4096 || !(!C35001h3.A00.matcher(A0E).find())) {
                        this.A01.A00(new AsyncMessageTokenizationJob(r21));
                        str = A0E;
                    } else {
                        str = A0H(A0E);
                    }
                }
                UserJid of = UserJid.of(r21.A0B());
                String A0F = A0F(r21);
                C34981h1 r12 = r13;
                C34981h1 r13 = new C34981h1(1, r21.A12, r21.A11);
                boolean z2 = r6.A02;
                if (of != null) {
                    A0D = A0D(of);
                } else if (z2) {
                    A0D = "1";
                } else {
                    A0D = "0";
                }
                String A0D2 = A0D(r5);
                C16310on A02 = this.A0C.A02();
                try {
                    ContentValues contentValues = new ContentValues(4);
                    contentValues.put("content", str);
                    StringBuilder sb = new StringBuilder();
                    sb.append(A0D);
                    sb.append(" ");
                    sb.append(A0D2);
                    contentValues.put("fts_jid", sb.toString());
                    contentValues.put("fts_namespace", A0F);
                    long j2 = r13.A02;
                    contentValues.put("docid", Long.valueOf(j2));
                    try {
                        r12 = new C34981h1(1, A02.A03.A03(contentValues, "message_ftsv2"), r13.A01);
                    } catch (SQLiteConstraintException unused) {
                        contentValues.remove("docid");
                        A02.A03.A00("message_ftsv2", contentValues, "docid = ?", new String[]{String.valueOf(j2)});
                    }
                    A02.close();
                    if (TextUtils.isEmpty(str) || j != 1) {
                        return r12;
                    }
                    return A06(r13, str);
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused2) {
                    }
                    throw th;
                }
            }
        } else {
            i = -5;
        }
        return C34981h1.A00(i);
    }

    public final C34981h1 A08(String str, String str2, String[] strArr) {
        int i;
        C16310on A01;
        Cursor A09;
        try {
            A01 = this.A0C.get();
            A09 = A01.A03.A09(str, strArr);
        } catch (SQLiteException e) {
            if (e.getMessage() == null || !e.getMessage().contains("FTS expression tree is too large")) {
                StringBuilder sb = new StringBuilder("FtsMessageStore/getRowIdForJidSearch/error/");
                sb.append(str2);
                Log.e(sb.toString(), e);
                i = -3;
            } else {
                StringBuilder sb2 = new StringBuilder("FtsMessageStore/getRowIdForJidSearch/too-large/");
                sb2.append(str2);
                Log.e(sb2.toString(), e);
                i = -2;
            }
        }
        try {
            if (A09.moveToNext()) {
                C34981h1 r3 = new C34981h1(1, A09.getLong(A09.getColumnIndexOrThrow("docid")), A09.getLong(A09.getColumnIndexOrThrow("_id")));
                A09.close();
                A01.close();
                return r3;
            }
            A09.close();
            A01.close();
            i = -4;
            return C34981h1.A00(i);
        } catch (Throwable th) {
            if (A09 != null) {
                try {
                    A09.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }

    public C15250mo A09(AbstractC14640lm r4) {
        C15250mo r2 = new C15250mo(this.A04);
        r2.A04 = r4;
        if (C15380n4.A0J(r4)) {
            List singletonList = Collections.singletonList(new C34951gy());
            if (r2.A0B != null) {
                Log.e("FtsQuery/cannot re-set contactPreFilter");
                return r2;
            }
            r2.A0B = singletonList;
            return r2;
        }
        r2.A0C = Collections.emptyList();
        return r2;
    }

    public AbstractC15340mz A0A(Cursor cursor, int i, int i2) {
        try {
            long j = cursor.getLong(i2);
            AbstractC14640lm A05 = this.A06.A05(cursor.getLong(i));
            if (C15380n4.A0N(A05) || A05 == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("FtsMessageStore/FtsMessageStoreMigration/getMessageForFtsFromCursor/missing cursor chatjid; rowId=");
                sb.append(j);
                Log.e(sb.toString());
                return null;
            }
            try {
                AbstractC15340mz A02 = this.A05.A02(cursor, A05, false, true);
                if (A02 != null) {
                    return A02;
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("FtsMessageStore/FtsMessageStoreMigration/getMessageForFtsFromCursor/null message; rowId=");
                sb2.append(j);
                Log.e(sb2.toString());
                return null;
            } catch (AssertionError | ClassCastException e) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("FtsMessageStore/FtsMessageStoreMigration/getMessageForFtsFromCursor/bad message; rowId=");
                sb3.append(j);
                Log.e(sb3.toString(), e);
                return null;
            }
        } catch (Exception e2) {
            Log.e("FtsMessageStore/FtsMessageStoreMigration/getMessageForFtsFromCursor/failed on cursor", e2);
            return null;
        }
    }

    public String A0B(AnonymousClass02N r13, C15250mo r14, Integer num) {
        String str = "";
        if (!r14.A02().isEmpty()) {
            String A01 = r14.A01();
            if (A01.startsWith("\"") && A01.endsWith("\"") && A01.length() > 2) {
                str = A01;
            } else if (!r14.A0F || A04() != 5) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                List<String> A02 = r14.A02();
                ArrayList arrayList = new ArrayList(A02.size());
                for (String str2 : A02) {
                    StringBuilder sb2 = new StringBuilder("content:");
                    sb2.append(str2);
                    arrayList.add(sb2.toString());
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append(TextUtils.join(" ", arrayList));
                sb3.append("*");
                sb.append(sb3.toString());
                str = sb.toString();
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str);
                if (!r14.A02().isEmpty()) {
                    List A0I = A0I(r13, r14, num);
                    StringBuilder sb5 = new StringBuilder();
                    int size = A0I.size();
                    for (int i = 0; i < size && !A02(r13); i++) {
                        boolean z = false;
                        if (i == size - 1) {
                            z = true;
                        }
                        sb5.append(" ");
                        Pair pair = (Pair) A0I.get(i);
                        StringBuilder sb6 = new StringBuilder("content:");
                        sb6.append((String) pair.first);
                        if (z) {
                            sb6.append('*');
                        }
                        for (int i2 = 0; i2 < ((List) pair.second).size(); i2++) {
                            AbstractC14640lm r11 = (AbstractC14640lm) ((List) pair.second).get(i2);
                            if (C15380n4.A0L(r11) || C15380n4.A0J(r11)) {
                                sb6.append(" OR ");
                                sb6.append("fts_jid:");
                                sb6.append(A0D(r11));
                            }
                        }
                        sb5.append(sb6.toString());
                    }
                    str = sb5.toString().trim();
                }
                sb4.append(str);
                str = sb4.toString();
            }
        }
        AbstractC14640lm r5 = r14.A04;
        if (r5 != null) {
            AnonymousClass009.A05(r5);
            StringBuilder sb7 = new StringBuilder();
            sb7.append(str);
            sb7.append(" ");
            boolean z2 = r14.A0G;
            StringBuilder sb8 = new StringBuilder("fts_jid:");
            sb8.append(A0D(r5));
            String obj = sb8.toString();
            if (z2 && !C15380n4.A0J(r5)) {
                StringBuilder sb9 = new StringBuilder();
                sb9.append(obj);
                sb9.append(" fts_jid:0 OR fts_jid:1");
                obj = sb9.toString();
            }
            sb7.append(obj);
            str = sb7.toString();
        }
        return A0C(r13, r14, str);
    }

    public final String A0C(AnonymousClass02N r11, C15250mo r12, String str) {
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        boolean z2 = false;
        for (Map.Entry entry : this.A0F.entrySet()) {
            if (A02(r11)) {
                break;
            }
            String str2 = (String) entry.getKey();
            C34941gx AEa = ((AbstractC34921gv) entry.getValue()).AEa(r12);
            if (AEa != null) {
                for (String str3 : AEa.A00) {
                    if (A02(r11)) {
                        break;
                    }
                    StringBuilder sb = new StringBuilder("fts_namespace:");
                    sb.append(str2);
                    sb.append(str3);
                    arrayList.add(sb.toString());
                    z2 = true;
                }
                for (String str4 : AEa.A01) {
                    if (!A02(r11)) {
                        StringBuilder sb2 = new StringBuilder("fts_namespace:-");
                        sb2.append(str2);
                        sb2.append(str4);
                        arrayList.add(sb2.toString());
                        z = true;
                    }
                }
            }
        }
        if (A02(r11) || !z || z2 || !TextUtils.isEmpty(str)) {
            String join = TextUtils.join(" ", arrayList);
            if (TextUtils.isEmpty(join)) {
                return str;
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(" ");
            sb3.append(join);
            return sb3.toString();
        }
        throw new IllegalStateException("Compiled FTS query comprised entirely of NOTs");
    }

    public String A0D(AbstractC14640lm r5) {
        return Long.toString(this.A09.A01(r5) + 10, 36);
    }

    public String A0E(AbstractC15340mz r11) {
        String str;
        BigDecimal bigDecimal;
        int i;
        UserJid userJid;
        C30821Yy r0;
        AnonymousClass1ZD r5;
        StringBuilder sb;
        String str2;
        String str3;
        String str4;
        String str5 = null;
        str5 = null;
        str5 = null;
        str5 = null;
        str5 = null;
        r6 = null;
        r6 = null;
        String str6 = null;
        C15370n3 r6 = null;
        str5 = null;
        if (r11 instanceof AbstractC16390ow) {
            C33711ex ACL = ((AbstractC16390ow) r11).ACL();
            if (ACL != null) {
                AnonymousClass018 r7 = this.A04;
                if (!(ACL instanceof C34991h2)) {
                    C16470p4 r3 = ACL.A00;
                    if (r3 != null) {
                        sb = new StringBuilder();
                        String A04 = ACL.A04();
                        str2 = " ";
                        C33711ex.A00(A04, str2, sb);
                        AnonymousClass1Z6 r02 = r3.A02;
                        if (r02 != null) {
                            str4 = r02.A00;
                        } else {
                            str4 = null;
                        }
                        C33711ex.A00(str4, str2, sb);
                        C33711ex.A00(r3.A07, str2, sb);
                        str3 = r3.A08;
                        C33711ex.A00(str3, str2, sb);
                        str5 = sb.toString();
                    }
                } else {
                    C16470p4 r4 = ACL.A00;
                    if (!(r4 == null || (r5 = r4.A01) == null)) {
                        AnonymousClass1ZK r8 = r5.A04;
                        String A0B = r7.A0B(R.string.checkout_native_flow_message_quantity_text, Integer.valueOf(r8.A08.size()));
                        sb = new StringBuilder();
                        str2 = " ";
                        C33711ex.A00(r8.A00(), str2, sb);
                        if (!(r5.A03 == null || r5.A05 == null)) {
                            C33711ex.A00(r5.A02(r7), str2, sb);
                        }
                        C33711ex.A00(A0B, str2, sb);
                        C33711ex.A00(r4.A07, str2, sb);
                        str3 = r4.A08;
                        C33711ex.A00(str3, str2, sb);
                        str5 = sb.toString();
                    }
                }
            }
        } else if (r11 instanceof AbstractC28871Pi) {
            str5 = ((AbstractC28871Pi) r11).ADA();
        } else if (r11 instanceof C28861Ph) {
            C28861Ph r112 = (C28861Ph) r11;
            boolean A0y = r112.A0y();
            str = r112.A0I();
            if (A0y) {
                str5 = A00(r112.A0F().A00, str);
            } else {
                if (!TextUtils.isEmpty(r112.A05)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" ");
                    sb2.append(r112.A05);
                    str = sb2.toString();
                }
                if (!TextUtils.isEmpty(r112.A03)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(" ");
                    sb3.append(r112.A03);
                    str = sb3.toString();
                }
                if (!TextUtils.isEmpty(r112.A06)) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str);
                    sb4.append(" ");
                    sb4.append(r112.A06);
                    str = sb4.toString();
                }
                AnonymousClass1IR r1 = r112.A0L;
                if (!(r1 == null || (r0 = r1.A08) == null)) {
                    BigDecimal bigDecimal2 = r0.A00;
                    String str7 = r1.A0I;
                    if (bigDecimal2 != null && !TextUtils.isEmpty(bigDecimal2.toPlainString()) && !TextUtils.isEmpty(str7)) {
                        str6 = bigDecimal2.toPlainString();
                        try {
                            C30711Yn r32 = new C30711Yn(str7);
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append(str6);
                            sb5.append(" ");
                            sb5.append(r32.A03(this.A04, bigDecimal2, true));
                            str6 = sb5.toString();
                        } catch (IllegalArgumentException unused) {
                        }
                    }
                    if (!TextUtils.isEmpty(str6)) {
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append(str);
                        sb6.append(" ");
                        sb6.append(str6);
                        str5 = sb6.toString();
                    }
                }
                str5 = str;
            }
        } else if (r11 instanceof AnonymousClass1XB) {
            AnonymousClass1XB r113 = (AnonymousClass1XB) r11;
            if (TextUtils.isEmpty(r113.A0I())) {
                str = "";
            } else {
                str = r113.A0I();
            }
            if ((r113 instanceof C30511Xs) && (userJid = ((C30511Xs) r113).A01) != null) {
                StringBuilder sb7 = new StringBuilder();
                sb7.append(str);
                sb7.append(" ");
                sb7.append(userJid.getRawString());
                str = sb7.toString();
            }
            if (r113 instanceof C30491Xp) {
                C30491Xp r33 = (C30491Xp) r113;
                if (!TextUtils.isEmpty(r33.A00)) {
                    AbstractC14640lm r12 = r113.A0z.A00;
                    if (r12 != null) {
                        r6 = this.A02.A0B(r12);
                    }
                    if (r6 != null) {
                        boolean z = true;
                        if (!r6.A0J() || !((i = r6.A06) == 2 || i == 1)) {
                            z = false;
                        }
                        if (!z) {
                            StringBuilder sb8 = new StringBuilder();
                            sb8.append(str);
                            sb8.append(" ");
                            sb8.append(r33.A00);
                            str5 = sb8.toString();
                        }
                    }
                }
            }
            str5 = str;
        } else if ((r11 instanceof AnonymousClass1X7) || (r11 instanceof AnonymousClass1X3)) {
            if (r11 instanceof AnonymousClass1XV) {
                AnonymousClass1XV r114 = (AnonymousClass1XV) r11;
                str5 = r114.A09;
                if (TextUtils.isEmpty(str5)) {
                    str5 = "";
                }
                String str8 = r114.A02;
                if (!TextUtils.isEmpty(str8)) {
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append(str5);
                    sb9.append(" ");
                    sb9.append(str8);
                    str5 = sb9.toString();
                }
                String str9 = r114.A05;
                if (!TextUtils.isEmpty(str9)) {
                    StringBuilder sb10 = new StringBuilder();
                    sb10.append(str5);
                    sb10.append(" ");
                    sb10.append(str9);
                    str5 = sb10.toString();
                }
                if (!TextUtils.isEmpty(r114.A04)) {
                    StringBuilder sb11 = new StringBuilder();
                    sb11.append(str5);
                    sb11.append(" ");
                    sb11.append(r114.A04);
                    str5 = sb11.toString();
                }
                if (r114.A0A != null && !TextUtils.isEmpty(r114.A03)) {
                    C30711Yn r72 = new C30711Yn(r114.A03);
                    AnonymousClass018 r42 = this.A04;
                    String A03 = r72.A03(r42, r114.A0A, true);
                    BigDecimal bigDecimal3 = r114.A0B;
                    if (!(bigDecimal3 == null || BigDecimal.ZERO.compareTo(bigDecimal3) == 0)) {
                        StringBuilder sb12 = new StringBuilder();
                        sb12.append(A03);
                        sb12.append(" ");
                        sb12.append(r72.A03(r42, r114.A0B, true));
                        A03 = sb12.toString();
                    }
                    StringBuilder sb13 = new StringBuilder();
                    sb13.append(str5);
                    sb13.append(" ");
                    sb13.append(A03);
                    str5 = sb13.toString();
                }
            } else {
                if (!r11.A0y()) {
                    str5 = ((AbstractC16130oV) r11).A15();
                }
                str5 = A00(r11.A0F().A00, r11.A0I());
            }
        } else if (r11 instanceof C30341Xa) {
            str5 = ((C30341Xa) r11).A03;
        } else if (r11 instanceof C16440p1) {
            if (!r11.A0y()) {
                str5 = ((C16440p1) r11).A1B();
            }
            str5 = A00(r11.A0F().A00, r11.A0I());
        } else if (r11 instanceof AnonymousClass1XO) {
            if (!r11.A0y()) {
                str5 = ((AnonymousClass1XO) r11).A17();
            }
            str5 = A00(r11.A0F().A00, r11.A0I());
        } else if (r11 instanceof C30411Xh) {
            str5 = ((C30411Xh) r11).A00;
        } else if (r11 instanceof C30351Xb) {
            List<C30731Yp> list = ((C30351Xb) r11).A02;
            if (list != null) {
                StringBuilder sb14 = new StringBuilder();
                for (C30731Yp r03 : list) {
                    sb14.append(r03.A01.A08());
                    sb14.append(" ");
                }
                str5 = sb14.toString();
            }
        } else if (r11 instanceof C28581Od) {
            str5 = r11.A0K();
        } else if (r11 instanceof AnonymousClass1XF) {
            AnonymousClass1XF r115 = (AnonymousClass1XF) r11;
            AnonymousClass018 r43 = this.A04;
            if (!TextUtils.isEmpty(r115.A07)) {
                str5 = r115.A07;
            } else {
                str5 = "";
            }
            if (!TextUtils.isEmpty(r115.A05)) {
                StringBuilder sb15 = new StringBuilder();
                sb15.append(str5);
                sb15.append(" ");
                sb15.append(r115.A05);
                str5 = sb15.toString();
            }
            String str10 = r115.A04;
            if (!(str10 == null || (bigDecimal = r115.A09) == null)) {
                StringBuilder sb16 = new StringBuilder();
                sb16.append(str5);
                sb16.append(" ");
                sb16.append(new C30711Yn(str10).A03(r43, bigDecimal, true));
                str5 = sb16.toString();
            }
        } else if (r11 instanceof C27671Iq) {
            str5 = ((C27671Iq) r11).A02;
        }
        if (str5 != null) {
            return str5;
        }
        return "";
    }

    public final String A0F(AbstractC15340mz r7) {
        Set<String> AEH;
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry : this.A0F.entrySet()) {
            if (!(entry == null || entry.getValue() == null || (AEH = ((AbstractC34921gv) entry.getValue()).AEH(r7)) == null)) {
                String str = (String) entry.getKey();
                for (String str2 : AEH) {
                    if (str2 != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append(str2);
                        arrayList.add(sb.toString());
                    }
                }
            }
        }
        return TextUtils.join(" ", arrayList);
    }

    @Deprecated
    public String A0G(String str) {
        boolean z;
        String obj;
        StringBuilder sb;
        String trim = A0H(str).trim();
        int length = trim.length();
        if (length == 0) {
            return trim;
        }
        if (!trim.startsWith("\"") || !trim.endsWith("\"") || length <= 2) {
            z = false;
        } else {
            z = true;
            trim = trim.substring(1, length - 1);
        }
        String trim2 = C32751cg.A02.matcher(trim).replaceAll(" ").trim();
        if (trim2.length() == 0) {
            return trim2;
        }
        if (z) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("\"");
            sb2.append(trim2);
            sb2.append("\"");
            obj = sb2.toString();
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(trim2);
            sb3.append("*");
            obj = sb3.toString();
        }
        if (obj.indexOf(105) == -1) {
            return obj;
        }
        if (z) {
            sb = new StringBuilder();
            sb.append(obj);
            sb.append(" OR ");
            sb.append(obj.replace('i', (char) 305));
        } else {
            String[] split = A0G.split(obj);
            sb = new StringBuilder();
            for (String str2 : split) {
                if (str2.indexOf(105) != -1) {
                    sb.append(str2);
                    sb.append(" OR ");
                    str2 = str2.replace('i', (char) 305);
                }
                sb.append(str2);
            }
        }
        return sb.toString();
    }

    public String A0H(String str) {
        AnonymousClass018 r1 = this.A04;
        if (str.isEmpty()) {
            return str;
        }
        boolean z = false;
        if (!C35001h3.A00.matcher(str).find()) {
            z = true;
            str = AnonymousClass1US.A08(str);
        }
        StringBuilder sb = new StringBuilder(str.length());
        BreakIterator A01 = C32751cg.A01(r1);
        A01.setText(str);
        int first = A01.first();
        while (true) {
            int next = A01.next();
            first = next;
            if (next == -1) {
                break;
            } else if (next - first != 1 || str.codePointAt(first) != 32) {
                CharSequence subSequence = str.subSequence(first, next);
                if (!z) {
                    subSequence = AnonymousClass1US.A08(subSequence);
                }
                sb.append(subSequence);
                sb.append(' ');
            }
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return sb.toString();
    }

    public List A0I(AnonymousClass02N r22, C15250mo r23, Integer num) {
        Map map;
        C21230x5 r2;
        ArrayList arrayList;
        ArrayList<AbstractC14640lm> arrayList2;
        ArrayList arrayList3 = new ArrayList();
        List A02 = r23.A02();
        for (int i = 0; i < A02.size() && !A02(r22); i++) {
            String str = (String) A02.get(i);
            if (str != null) {
                boolean z = false;
                if (i == A02.size() - 1) {
                    z = true;
                }
                synchronized (r23) {
                    map = r23.A0E;
                    if (map == null) {
                        map = new ConcurrentHashMap();
                        r23.A0E = map;
                    }
                }
                synchronized (r23.A0I) {
                    List list = (List) map.get(str);
                    if (list != null) {
                        r2 = this.A0E;
                        C34961gz.A01(r2, num, "fts_cached_jids");
                        arrayList2 = list;
                    } else {
                        ArrayList arrayList4 = new ArrayList();
                        List singletonList = Collections.singletonList(str);
                        A0M(r23);
                        r2 = this.A0E;
                        C34961gz.A01(r2, num, "fts_warm_cache");
                        boolean z2 = false;
                        if (r23.A0C != null) {
                            z2 = true;
                        }
                        AnonymousClass009.A0A("contact list null after warming", z2);
                        C28181Ma r17 = new C28181Ma("FtsMessageStore/filter");
                        List<C15370n3> list2 = r23.A0C;
                        AnonymousClass009.A05(list2);
                        for (C15370n3 r15 : list2) {
                            if (A02(r22) && z) {
                                C34961gz.A01(r2, num, "fts_last_cancel");
                                arrayList = arrayList4;
                                break;
                            }
                            AbstractC14640lm r14 = (AbstractC14640lm) r15.A0B(AbstractC14640lm.class);
                            if (r14 != null && ((r14 instanceof UserJid) || (r14 instanceof GroupJid))) {
                                C15610nY r9 = this.A03;
                                boolean z3 = false;
                                if (str.length() > 1) {
                                    z3 = true;
                                }
                                if (r9.A0M(r15, singletonList, z3)) {
                                    arrayList4.add(r14);
                                }
                            }
                        }
                        map.put(str, arrayList4);
                        C34961gz.A01(r2, num, "fts_search");
                        r17.A01();
                        arrayList2 = arrayList4;
                    }
                    if (r23.A0A != null) {
                        ArrayList arrayList5 = new ArrayList();
                        for (AbstractC14640lm r1 : arrayList2) {
                            if (C20830wO.A00(r1, r23.A0A)) {
                                arrayList5.add(r1);
                            }
                        }
                        C34961gz.A01(r2, num, "fts_filtered");
                        arrayList = arrayList5;
                    } else {
                        C34961gz.A01(r2, num, "fts_unfiltered");
                        arrayList = arrayList2;
                    }
                }
                C34961gz.A01(r2, num, "fts_token_jids");
                arrayList3.add(Pair.create(str, arrayList));
            }
        }
        return arrayList3;
    }

    public void A0J() {
        C28181Ma r6 = new C28181Ma("FtsMessageStore/drop");
        C16490p7 r2 = this.A0C;
        C16310on A02 = r2.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16330op r1 = A02.A03;
            r1.A0B(C29771Up.A00("messages_fts"));
            r1.A0B(C29771Up.A00("message_ftsv2"));
            r2.A04();
            r2.A05.A09(A02);
            C21390xL r3 = this.A0D;
            r3.A04("fts_index_start", 0);
            r3.A05("fts_ready", 0);
            A00.A00();
            A00.close();
            A02.close();
            r6.A01();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A0K(AbstractC34921gv r4) {
        String AEZ = r4.AEZ();
        Map map = this.A0F;
        if (map.containsKey(AEZ)) {
            AnonymousClass009.A07("Namespace already registered");
        }
        map.put(AEZ, r4);
    }

    public void A0L(C15250mo r3) {
        try {
            A0M(r3);
        } catch (IllegalStateException e) {
            Log.e("FtsMessageStore/safeWarm/failed to warm contact list", e);
        }
    }

    public void A0M(C15250mo r6) {
        if (r6.A0C == null) {
            synchronized (r6.A0I) {
                if (r6.A0C == null) {
                    C28181Ma r4 = new C28181Ma("FtsMessageStore/getSearchableContacts");
                    LinkedList linkedList = new LinkedList(this.A07.A04().values());
                    r6.A0C = linkedList;
                    if (r6.A0B != null) {
                        Iterator it = linkedList.iterator();
                        while (it.hasNext()) {
                            AbstractC14640lm r1 = (AbstractC14640lm) ((C15370n3) it.next()).A0B(AbstractC14640lm.class);
                            if (r1 == null || !C20830wO.A00(r1, r6.A0B)) {
                                it.remove();
                            }
                        }
                    }
                    List list = r6.A0C;
                    AnonymousClass009.A05(list);
                    list.size();
                    r4.A01();
                }
            }
        }
    }

    public void A0N(AbstractC15340mz r7) {
        A07(r7, Collections.emptyMap(), A04(), false);
    }

    public boolean A0O() {
        return this.A0D.A01("fts_ready", 0) % 2 != 0;
    }
}
