package X;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.util.Range;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.camera.VoipPhysicalCamera;
import java.util.Arrays;

/* renamed from: X.2Zj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51852Zj extends CameraCaptureSession.StateCallback {
    public final /* synthetic */ CaptureRequest.Builder A00;
    public final /* synthetic */ AnonymousClass2NW A01;

    public C51852Zj(CaptureRequest.Builder builder, AnonymousClass2NW r2) {
        this.A01 = r2;
        this.A00 = builder;
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
        AnonymousClass2NW r1 = this.A01;
        r1.A07 = false;
        Log.i("voip/video/VoipCamera/ cameraDevice configure failed");
        r1.cameraEventsDispatcher.A02();
    }

    @Override // android.hardware.camera2.CameraCaptureSession.StateCallback
    public void onConfigured(CameraCaptureSession cameraCaptureSession) {
        AnonymousClass2NW r2 = this.A01;
        r2.A07 = false;
        if (AnonymousClass2NW.A02(r2) == null) {
            Log.i("voip/video/VoipCamera/ cameraDevice configured, but device is null");
            r2.cameraEventsDispatcher.A02();
            return;
        }
        Log.i("voip/video/VoipCamera/ cameraDevice configured");
        CaptureRequest.Builder builder = this.A00;
        builder.set(CaptureRequest.CONTROL_MODE, C12960it.A0V());
        builder.set(CaptureRequest.CONTROL_AF_MODE, C12970iu.A0h());
        builder.set(CaptureRequest.FLASH_MODE, 0);
        Range[] rangeArr = (Range[]) AnonymousClass2NW.A01(r2).get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        if (rangeArr != null) {
            int i = Integer.MIN_VALUE;
            Range range = null;
            for (Range range2 : rangeArr) {
                int fpsRangeScore = VoipPhysicalCamera.fpsRangeScore(C12960it.A05(range2.getLower()) * 1000, C12960it.A05(range2.getUpper()) * 1000, r2.A0F.A01);
                StringBuilder A0k = C12960it.A0k("voip/video/VoipCamera/startOnCameraThread check fps [");
                A0k.append(range2.getLower());
                A0k.append(", ");
                A0k.append(range2.getUpper());
                A0k.append("], score: ");
                A0k.append(fpsRangeScore);
                C12960it.A1F(A0k);
                if (fpsRangeScore > i) {
                    range = range2;
                    i = fpsRangeScore;
                }
            }
            if (range != null) {
                StringBuilder A0k2 = C12960it.A0k("voip/video/VoipCamera/startOnCameraThread with fps range [");
                A0k2.append(range.getLower());
                A0k2.append(", ");
                A0k2.append(range.getUpper());
                A0k2.append("], score: ");
                A0k2.append(i);
                A0k2.append(", supported ranges : ");
                Log.i(C12960it.A0d(Arrays.toString(rangeArr), A0k2));
                builder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, range);
            }
        }
        try {
            cameraCaptureSession.setRepeatingRequest(builder.build(), null, r2.cameraThreadHandler);
            r2.startPeriodicCameraCallbackCheck();
        } catch (CameraAccessException unused) {
            Log.i("voip/video/VoipCamera/ failed to start preview");
            r2.cameraEventsDispatcher.A02();
        }
    }
}
