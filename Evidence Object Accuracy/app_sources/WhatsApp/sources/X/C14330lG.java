package X;

import android.media.MediaScannerConnection;
import android.net.Uri;
import com.whatsapp.NativeMediaHandler;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.0lG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14330lG {
    public static SimpleDateFormat A06 = new SimpleDateFormat("yyyyww", Locale.US);
    public static final String A07;
    public static final String A08;
    public static final String A09;
    public static final String A0A;
    public static final String A0B;
    public static final String A0C;
    public static final String A0D;
    public static final String A0E;
    public static final String A0F;
    public static final String A0G;
    public C14340lH A00;
    public final NativeMediaHandler A01;
    public final C15810nw A02;
    public final C16590pI A03;
    public final C15690nk A04;
    public final Object A05 = new Object();

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("WhatsApp");
        sb.append(" Audio");
        A09 = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("WhatsApp");
        sb2.append(" Animated Gifs");
        A08 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("WhatsApp");
        sb3.append(" Voice Notes");
        A0G = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("WhatsApp");
        sb4.append(" Video");
        A0F = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append("WhatsApp");
        sb5.append(" Images");
        A0C = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append("WhatsApp");
        sb6.append(" Documents");
        A0B = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append("WhatsApp");
        sb7.append(" Profile Photos");
        A0D = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
        sb8.append("WhatsApp");
        sb8.append(" Calls");
        A0A = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append("WhatsApp");
        sb9.append(" Stickers");
        A0E = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
        sb10.append("WhatsApp");
        sb10.append(" History Sync");
        A07 = sb10.toString();
    }

    public C14330lG(NativeMediaHandler nativeMediaHandler, C15810nw r3, C16590pI r4, C15690nk r5) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = nativeMediaHandler;
        this.A04 = r5;
    }

    public static File A00(File file, String str) {
        boolean z = false;
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                z = true;
            }
        }
        if (!z && !file.mkdirs()) {
            Log.e("app/extsharedfile/folder/created/false");
        }
        return new File(file, str);
    }

    public static final File A01(File file, String str, String str2, String str3) {
        String name;
        if (str != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.replace('/', '-'));
            sb.append(str3);
            name = sb.toString();
        } else if (str2 != null) {
            name = new File(str2).getName();
        } else {
            Log.e("fmessageio/getDownloadFile/no_url");
            return null;
        }
        return A00(file, name);
    }

    public static synchronized String A02(File file, String str) {
        String obj;
        boolean z;
        synchronized (C14330lG.class) {
            String l = Long.toString(System.currentTimeMillis(), 36);
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("-");
            sb.append(l);
            obj = sb.toString();
            File[] listFiles = file.listFiles(new FilenameFilter(obj) { // from class: X.1Tt
                public final /* synthetic */ String A00;

                {
                    this.A00 = r1;
                }

                @Override // java.io.FilenameFilter
                public final boolean accept(File file2, String str2) {
                    return str2.startsWith(this.A00);
                }
            });
            if (listFiles != null) {
                do {
                    z = false;
                    for (File file2 : listFiles) {
                        if (file2.getName().equals(obj)) {
                            StringBuilder sb2 = new StringBuilder();
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(str);
                            sb3.append("-");
                            sb3.append(l);
                            sb2.append(sb3.toString());
                            sb2.append(UUID.randomUUID().toString());
                            obj = sb2.toString();
                            z = true;
                        }
                    }
                } while (z);
            }
        }
        return obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        if (r3.isDirectory() != false) goto L_0x0030;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A03(java.io.File r3, boolean r4) {
        /*
            boolean r0 = r3.exists()     // Catch: all -> 0x005b
            if (r0 == 0) goto L_0x000f
            boolean r0 = r3.isFile()     // Catch: all -> 0x005b
            if (r0 == 0) goto L_0x002a
            r3.delete()     // Catch: all -> 0x005b
        L_0x000f:
            boolean r0 = r3.mkdirs()     // Catch: all -> 0x005b
            if (r0 != 0) goto L_0x0030
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x005b
            r1.<init>()     // Catch: all -> 0x005b
            java.lang.String r0 = "fmessageio/prepareFolder/mkdirs failed: "
            r1.append(r0)     // Catch: all -> 0x005b
            r1.append(r3)     // Catch: all -> 0x005b
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x005b
            com.whatsapp.util.Log.e(r0)     // Catch: all -> 0x005b
            goto L_0x0030
        L_0x002a:
            boolean r0 = r3.isDirectory()     // Catch: all -> 0x005b
            if (r0 == 0) goto L_0x000f
        L_0x0030:
            if (r4 == 0) goto L_0x005a
            java.lang.String r0 = ".nomedia"
            java.io.File r1 = new java.io.File     // Catch: all -> 0x005b
            r1.<init>(r3, r0)     // Catch: all -> 0x005b
            boolean r0 = r1.exists()     // Catch: all -> 0x005b
            if (r0 != 0) goto L_0x005a
            r1.createNewFile()     // Catch: IOException -> 0x0043, all -> 0x005b
            goto L_0x0059
        L_0x0043:
            r2 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: all -> 0x005b
            r1.<init>()     // Catch: all -> 0x005b
            java.lang.String r0 = "fmessageio/prepareFolder "
            r1.append(r0)     // Catch: all -> 0x005b
            r1.append(r3)     // Catch: all -> 0x005b
            java.lang.String r0 = r1.toString()     // Catch: all -> 0x005b
            com.whatsapp.util.Log.e(r0, r2)     // Catch: all -> 0x005b
            goto L_0x005a
        L_0x0059:
            return
        L_0x005a:
            return
        L_0x005b:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14330lG.A03(java.io.File, boolean):void");
    }

    public C14340lH A04() {
        C14340lH r0;
        synchronized (this.A05) {
            if (this.A00 == null) {
                Log.i("fmessageio/media-dirs-null");
                A0P();
            }
            r0 = this.A00;
            AnonymousClass009.A05(r0);
        }
        return r0;
    }

    public File A05() {
        return new File(this.A03.A00.getFilesDir(), "business_activity_report.zip");
    }

    public File A06() {
        File file = new File(this.A03.A00.getCacheDir(), "export_chats");
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public File A07() {
        File file = new File(this.A03.A00.getCacheDir(), "export_business_activity");
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public File A08() {
        File file = new File(this.A03.A00.getCacheDir(), "export_gdpr");
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public File A09() {
        File file = new File(this.A03.A00.getFilesDir(), "Payment Backgrounds");
        A03(file, false);
        return file;
    }

    public File A0A() {
        File file = new File(this.A03.A00.getFilesDir(), "Stickers");
        A03(file, false);
        return file;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00bf, code lost:
        if (r1 == null) goto L_0x003e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0046 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File A0B(byte r7, int r8, int r9) {
        /*
        // Method dump skipped, instructions count: 200
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14330lG.A0B(byte, int, int):java.io.File");
    }

    public File A0C(C14370lK r5, String str, String str2, String str3, boolean z, boolean z2) {
        File file;
        File filesDir;
        String str4;
        if (z) {
            filesDir = this.A03.A00.getFilesDir();
            str4 = "gdpr.zip.enc.tmp";
        } else if (z2) {
            filesDir = this.A03.A00.getFilesDir();
            str4 = "business_activity_report.zip.enc.tmp";
        } else if (C14370lK.A0P == r5) {
            filesDir = this.A03.A00.getFilesDir();
            str4 = "payment_background_img.enc.tmp";
        } else {
            if (C14370lK.A0K == r5) {
                file = new File(this.A03.A00.getFilesDir(), A07);
                A03(file, false);
            } else {
                file = A04().A03;
                A03(file, true);
                if (str != null) {
                    return A01(file, str, str3, ".enc.tmp");
                }
            }
            return A01(file, str2, str3, ".enc.tmp");
        }
        return new File(filesDir, str4);
    }

    public File A0D(C14370lK r7, String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        String str3;
        File file;
        boolean z5;
        File filesDir;
        String str4;
        if (z4) {
            file = A04().A03;
            A03(file, true);
            str3 = ".thumb.tmp";
        } else {
            if (z) {
                filesDir = this.A03.A00.getFilesDir();
                str4 = "gdpr.zip.tmp";
            } else if (z2) {
                filesDir = this.A03.A00.getFilesDir();
                str4 = "business_activity_report.zip.tmp";
            } else {
                str3 = ".tmp";
                if (C14370lK.A0K == r7) {
                    file = new File(this.A03.A00.getFilesDir(), A07);
                    z5 = false;
                } else if (C14370lK.A0P == r7) {
                    filesDir = this.A03.A00.getFilesDir();
                    str4 = "payment_background_img.tmp";
                } else if (z3) {
                    String l = Long.toString(System.currentTimeMillis(), 36);
                    File file2 = A04().A09;
                    StringBuilder sb = new StringBuilder();
                    String str5 = r7.A02;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str5);
                    sb2.append("-");
                    sb2.append(l);
                    sb.append(sb2.toString());
                    sb.append(str3);
                    return new File(file2, sb.toString());
                } else {
                    file = A04().A03;
                    z5 = true;
                }
                A03(file, z5);
            }
            return new File(filesDir, str4);
        }
        return A01(file, str, str2, str3);
    }

    public File A0E(AbstractC16130oV r5) {
        AbstractC14640lm r0 = r5.A0z.A00;
        if (r0 != null && r0.getType() == 9) {
            return new File(this.A03.A00.getFilesDir(), "gdpr.zip.tmp");
        }
        File file = A04().A03;
        A03(file, true);
        return A01(file, r5.A05, r5.A08, ".tmp");
    }

    public File A0F(File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(file.getName());
        sb.append(".chck");
        return A0M(sb.toString());
    }

    public File A0G(String str) {
        String str2;
        File filesDir = this.A03.A00.getFilesDir();
        if ("personal".equals(str)) {
            str2 = "dyi.zip";
        } else {
            str2 = "business_dyi.zip";
        }
        return new File(filesDir, str2);
    }

    public File A0H(String str) {
        File A062 = A06();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".txt");
        return new File(A062, sb.toString().replaceAll("[?:\\\\/*\"<>|]", ""));
    }

    public File A0I(String str) {
        File A072 = A07();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".zip");
        return new File(A072, sb.toString().replaceAll("[?:\\\\/*\"<>|]", ""));
    }

    public File A0J(String str) {
        String str2;
        File cacheDir = this.A03.A00.getCacheDir();
        if ("personal".equals(str)) {
            str2 = "export_personal_dyi";
        } else {
            str2 = "export_business_dyi";
        }
        File file = new File(cacheDir, str2);
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public File A0K(String str) {
        File A082 = A08();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".zip");
        return new File(A082, sb.toString().replaceAll("[?:\\\\/*\"<>|]", ""));
    }

    public File A0L(String str) {
        File file = new File(this.A03.A00.getCacheDir(), "support");
        if (!file.exists()) {
            file.mkdir();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".zip");
        return new File(file, sb.toString().replaceAll("[?:\\\\/*\"<>|]", ""));
    }

    public File A0M(String str) {
        File file = A04().A03;
        A03(file, true);
        return A00(file, str);
    }

    public File A0N(String str, String str2) {
        File A0J = A0J(str2);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".zip");
        return new File(A0J, sb.toString().replaceAll("[?:\\\\/*\"<>|]", ""));
    }

    public File A0O(String str, String str2) {
        File A072 = this.A02.A07(".StickerThumbs");
        A03(A072, false);
        return A01(A072, str, str2, ".thumb.webp");
    }

    public void A0P() {
        synchronized (this.A05) {
            Log.i("fmessageio/initExternalStorageDirectory");
            this.A00 = new C14340lH(this);
        }
    }

    public final void A0Q(File file) {
        File[] listFiles;
        File file2 = new File(file, ".nomedia");
        if (file2.exists() && file2.delete() && (listFiles = file.listFiles()) != null) {
            A0R(null, Arrays.asList(listFiles));
        }
    }

    public void A0R(Runnable runnable, List list) {
        int size = list.size();
        String[] strArr = new String[list.size()];
        for (int i = 0; i < size; i++) {
            strArr[i] = ((File) list.get(i)).getAbsolutePath();
        }
        MediaScannerConnection.scanFile(this.A03.A00, strArr, null, new MediaScannerConnection.OnScanCompletedListener(runnable, new AtomicInteger(size)) { // from class: X.1Tu
            public final /* synthetic */ Runnable A00;
            public final /* synthetic */ AtomicInteger A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // android.media.MediaScannerConnection.OnScanCompletedListener
            public final void onScanCompleted(String str, Uri uri) {
                AtomicInteger atomicInteger = this.A01;
                Runnable runnable2 = this.A00;
                StringBuilder sb = new StringBuilder("fmessageio/rescan/scan completed: file=");
                sb.append(str);
                sb.append(" uri=");
                sb.append(uri);
                Log.i(sb.toString());
                if (atomicInteger.decrementAndGet() <= 0 && runnable2 != null) {
                    runnable2.run();
                }
            }
        });
    }

    public boolean A0S(File file) {
        return file.getCanonicalPath().startsWith(A04().A0A.getCanonicalPath());
    }

    public boolean A0T(File file) {
        return file.getCanonicalPath().startsWith(A04().A03.getCanonicalPath()) || file.getCanonicalPath().startsWith(A04().A09.getCanonicalPath());
    }

    public boolean A0U(File file) {
        return file.getCanonicalPath().startsWith(A04().A08.getCanonicalPath());
    }

    public boolean A0V(File file) {
        if (!A0S(file)) {
            return false;
        }
        String canonicalPath = file.getCanonicalPath();
        C14340lH A04 = A04();
        if (canonicalPath.startsWith(A04.A0H.getCanonicalPath()) || canonicalPath.startsWith(A04.A0I.getCanonicalPath()) || canonicalPath.startsWith(A04.A0J.getCanonicalPath()) || canonicalPath.startsWith(A04.A0K.getCanonicalPath()) || canonicalPath.startsWith(A04.A0L.getCanonicalPath()) || canonicalPath.startsWith(A04.A0M.getCanonicalPath()) || canonicalPath.startsWith(A04.A0B.getCanonicalPath()) || canonicalPath.startsWith(A04.A0C.getCanonicalPath()) || canonicalPath.startsWith(A04.A0D.getCanonicalPath()) || canonicalPath.startsWith(A04.A0E.getCanonicalPath()) || canonicalPath.startsWith(A04.A0F.getCanonicalPath()) || canonicalPath.startsWith(A04.A04.getCanonicalPath()) || canonicalPath.startsWith(A04.A0O.getCanonicalPath())) {
            return false;
        }
        return true;
    }
}
