package X;

import android.widget.TextView;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.6HY  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6HY implements Runnable {
    public final /* synthetic */ PaymentView A00;

    public /* synthetic */ AnonymousClass6HY(PaymentView paymentView) {
        this.A00 = paymentView;
    }

    @Override // java.lang.Runnable
    public final void run() {
        PaymentView paymentView = this.A00;
        TextView textView = paymentView.A0J;
        if (textView != null) {
            textView.startAnimation(paymentView.A05);
        }
    }
}
