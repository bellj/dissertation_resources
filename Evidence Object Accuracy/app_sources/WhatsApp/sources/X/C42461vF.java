package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;
import com.whatsapp.conversation.conversationrow.ConversationPaymentRowTransactionLayout;
import java.util.Set;

/* renamed from: X.1vF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C42461vF extends AbstractC42471vG implements AbstractC42481vH, AnonymousClass1In {
    public C21740xu A00;
    public C15890o4 A01;
    public AnonymousClass109 A02;
    public C22370yy A03;
    public C21860y6 A04;
    public AnonymousClass18P A05;
    public C17900ra A06;
    public C22460z7 A07;
    public AnonymousClass1A8 A08;
    public AnonymousClass14X A09;
    public final View A0A;
    public final View A0B;
    public final View A0C;
    public final View A0D = AnonymousClass028.A0D(this, R.id.payment_unsupported_icon);
    public final View A0E;
    public final View A0F;
    public final View A0G;
    public final View A0H;
    public final View A0I = AnonymousClass028.A0D(this, R.id.text_and_date);
    public final FrameLayout A0J = ((FrameLayout) AnonymousClass028.A0D(this, R.id.payment_amount_container));
    public final FrameLayout A0K;
    public final FrameLayout A0L;
    public final FrameLayout A0M;
    public final ImageView A0N;
    public final LinearLayout A0O = ((LinearLayout) AnonymousClass028.A0D(this, R.id.main_layout));
    public final TextEmojiLabel A0P;
    public final TextEmojiLabel A0Q = ((TextEmojiLabel) AnonymousClass028.A0D(this, R.id.payment_note));
    public final TextEmojiLabel A0R = ((TextEmojiLabel) AnonymousClass028.A0D(this, R.id.transaction_status));
    public final WaTextView A0S = ((WaTextView) AnonymousClass028.A0D(this, R.id.payment_symbol));
    public final ConversationPaymentRowTransactionLayout A0T = ((ConversationPaymentRowTransactionLayout) AnonymousClass028.A0D(this, R.id.transaction_status_container));
    public final C64853Hd A0U;

    @Override // X.AbstractC28551Oa
    public int getBubbleAlpha() {
        return 255;
    }

    public C42461vF(Context context, AbstractC13890kV r15, AbstractC15340mz r16) {
        super(context, r15, r16);
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) AnonymousClass028.A0D(this, R.id.message_text);
        this.A0P = textEmojiLabel;
        textEmojiLabel.setTypeface(null, 0);
        textEmojiLabel.A07 = new C52162aM();
        textEmojiLabel.setAutoLinkMask(0);
        textEmojiLabel.setLinksClickable(false);
        textEmojiLabel.setFocusable(false);
        textEmojiLabel.setClickable(false);
        textEmojiLabel.setLongClickable(false);
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(this, R.id.payment_container);
        this.A0K = frameLayout;
        FrameLayout frameLayout2 = (FrameLayout) AnonymousClass028.A0D(this, R.id.requested_message_holder);
        this.A0M = frameLayout2;
        this.A0B = AnonymousClass028.A0D(this, R.id.payment_shimmer);
        this.A0C = AnonymousClass028.A0D(this, R.id.payment_loading_error);
        this.A0L = (FrameLayout) AnonymousClass028.A0D(this, R.id.payment_security_strip);
        this.A0N = (ImageView) AnonymousClass028.A0D(this, R.id.security_strip_image);
        this.A0A = findViewById(R.id.accept_payment_container);
        this.A0G = findViewById(R.id.send_payment_again_container);
        this.A0F = findViewById(R.id.retry_withdrawal_container);
        this.A0E = findViewById(R.id.request_actions_container);
        frameLayout.setForeground(getInnerFrameForegroundDrawable());
        frameLayout2.setForeground(getInnerFrameForegroundDrawable());
        this.A0H = AnonymousClass028.A0D(this, R.id.media_container);
        C14850m9 r8 = ((AbstractC28551Oa) this).A0L;
        C239613r r4 = ((AnonymousClass1OY) this).A0M;
        C16170oZ r5 = ((AnonymousClass1OY) this).A0R;
        AnonymousClass018 r7 = ((AbstractC28551Oa) this).A0K;
        AnonymousClass19O r12 = this.A1O;
        this.A0U = new C64853Hd(this, r4, r5, this.A01, r7, r8, this.A02, this.A03, this.A1K, r12);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != getFMessage()) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x018e, code lost:
        if (r0 != 10) goto L_0x0192;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x02c1  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0353  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x03db  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0426  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04d3  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0521  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0529  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x0553  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0559  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x055d  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0636  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x025c  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1M() {
        /*
        // Method dump skipped, instructions count: 1594
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42461vF.A1M():void");
    }

    public final void A1N() {
        this.A0J.setVisibility(8);
        View view = this.A0A;
        if (view != null) {
            view.setVisibility(8);
        }
        View view2 = this.A0F;
        if (view2 != null) {
            view2.setVisibility(8);
        }
        View view3 = this.A0G;
        if (view3 != null) {
            view3.setVisibility(8);
        }
        this.A0O.setOnClickListener(null);
        this.A0R.setVisibility(8);
        this.A0M.setVisibility(8);
        View view4 = this.A0E;
        if (view4 != null) {
            view4.setVisibility(8);
        }
    }

    public final boolean A1O() {
        return ((AbstractC28551Oa) this).A0L.A07(605) || ((AbstractC28551Oa) this).A0L.A07(629);
    }

    @Override // X.AnonymousClass1In
    public void ATd() {
        A0s();
    }

    @Override // X.AbstractC42481vH
    public void Ae9() {
        if (((AbstractC28551Oa) this).A0L.A07(812) || ((AbstractC28551Oa) this).A0L.A07(811)) {
            this.A0U.A0H.A02();
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_payment_incoming;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_payment_incoming;
    }

    @Override // X.AnonymousClass1OY
    public Set getInnerFrameLayouts() {
        Set innerFrameLayouts = super.getInnerFrameLayouts();
        innerFrameLayouts.add(this.A0K);
        innerFrameLayouts.add(this.A0M);
        return innerFrameLayouts;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        return ((int) getResources().getDimension(R.dimen.payment_bubble_amount_width)) + (((int) getResources().getDimension(R.dimen.payment_bubble_margin_width)) << 1);
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_payment_outgoing;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r3) {
        boolean z = false;
        if (r3.A0L != null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        ((AbstractC28551Oa) this).A0O = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        if ("en".equals(r2.A06.A06()) != false) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0096, code lost:
        if ("en".equals(r2.A06.A06()) != false) goto L_0x0098;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setPaymentContext(X.AbstractC15340mz r9, com.whatsapp.TextEmojiLabel r10) {
        /*
            r8 = this;
            X.14X r2 = r8.A09
            X.1IS r0 = r9.A0z
            X.0lm r0 = r0.A00
            if (r0 == 0) goto L_0x00b7
            X.1IR r5 = r9.A0L
            if (r5 == 0) goto L_0x00b7
            boolean r0 = r5.A0F()
            java.lang.String r3 = ""
            java.lang.String r6 = "en"
            r4 = 1
            r7 = 0
            X.0nT r1 = r2.A00
            if (r0 == 0) goto L_0x0083
            com.whatsapp.jid.UserJid r0 = r5.A0E
            boolean r0 = r1.A0F(r0)
            if (r0 == 0) goto L_0x002f
            X.018 r0 = r2.A06
            java.lang.String r0 = r0.A06()
            boolean r0 = r6.equals(r0)
            r1 = 1
            if (r0 == 0) goto L_0x0034
        L_0x002f:
            r1 = 0
            java.lang.String r3 = r2.A0N(r5)
        L_0x0034:
            X.0pI r0 = r2.A05
            android.content.Context r2 = r0.A00
            if (r1 == 0) goto L_0x00ab
            r0 = 2131890556(0x7f12117c, float:1.9415807E38)
        L_0x003d:
            java.lang.String r0 = r2.getString(r0)
        L_0x0041:
            android.util.Pair r2 = new android.util.Pair
            r2.<init>(r3, r0)
        L_0x0046:
            java.lang.Object r1 = r2.second
            java.lang.String r1 = (java.lang.String) r1
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x00bf
            java.lang.Object r5 = r2.first
            java.lang.String r5 = (java.lang.String) r5
            android.text.SpannableStringBuilder r4 = new android.text.SpannableStringBuilder
            r4.<init>(r1)
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            r3 = 0
            if (r0 != 0) goto L_0x007f
            android.content.Context r0 = r8.getContext()
            X.2aZ r2 = new X.2aZ
            r2.<init>(r0)
            int r1 = r1.length()
            int r0 = r5.length()
            int r0 = r1 - r0
            r4.setSpan(r2, r0, r1, r3)
            android.widget.TextView$BufferType r0 = android.widget.TextView.BufferType.SPANNABLE
            r10.setText(r4, r0)
        L_0x007b:
            r10.setVisibility(r3)
            return
        L_0x007f:
            r10.setText(r4)
            goto L_0x007b
        L_0x0083:
            com.whatsapp.jid.UserJid r0 = r5.A0D
            boolean r0 = r1.A0F(r0)
            if (r0 == 0) goto L_0x0098
            X.018 r0 = r2.A06
            java.lang.String r0 = r0.A06()
            boolean r0 = r6.equals(r0)
            r1 = 1
            if (r0 == 0) goto L_0x009d
        L_0x0098:
            r1 = 0
            java.lang.String r3 = r2.A0M(r5)
        L_0x009d:
            X.0pI r0 = r2.A05
            android.content.Context r2 = r0.A00
            if (r1 == 0) goto L_0x00a7
            r0 = 2131890672(0x7f1211f0, float:1.9416042E38)
            goto L_0x003d
        L_0x00a7:
            r1 = 2131890668(0x7f1211ec, float:1.9416034E38)
            goto L_0x00ae
        L_0x00ab:
            r1 = 2131890555(0x7f12117b, float:1.9415805E38)
        L_0x00ae:
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r7] = r3
            java.lang.String r0 = r2.getString(r1, r0)
            goto L_0x0041
        L_0x00b7:
            java.lang.String r0 = ""
            android.util.Pair r2 = new android.util.Pair
            r2.<init>(r0, r0)
            goto L_0x0046
        L_0x00bf:
            r0 = 8
            r10.setVisibility(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C42461vF.setPaymentContext(X.0mz, com.whatsapp.TextEmojiLabel):void");
    }

    public final void setPaymentStatusText(String str, String str2, String str3, int i, TextEmojiLabel textEmojiLabel) {
        String str4;
        if (!TextUtils.isEmpty(str2)) {
            str4 = getContext().getString(R.string.payments_pill_status_without_separator, str, str2);
        } else {
            str4 = str;
        }
        if (!TextUtils.isEmpty(str3)) {
            str4 = getContext().getString(R.string.payments_pill_status_with_separator, str4, str3);
        }
        int indexOf = str4.indexOf(str);
        int length = str.length() + indexOf;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str4);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(getResources().getColor(i)), indexOf, length, 0);
        spannableStringBuilder.setSpan(new C52292aZ(getContext()), indexOf, length, 0);
        textEmojiLabel.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
    }

    public final void setRequestPaymentText(AnonymousClass1IR r5, TextView textView) {
        Pair A0A = this.A09.A0A(r5);
        String str = (String) A0A.first;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder((String) A0A.second);
        if (!TextUtils.isEmpty(str)) {
            spannableStringBuilder.setSpan(new C52292aZ(getContext()), 0, str.length(), 0);
            textView.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
            return;
        }
        textView.setText(spannableStringBuilder);
    }
}
