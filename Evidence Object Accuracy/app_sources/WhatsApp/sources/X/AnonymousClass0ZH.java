package X;

import android.content.Context;
import android.os.Build;
import java.io.File;

/* renamed from: X.0ZH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZH implements AbstractC12910il {
    public C020309q A00;
    public boolean A01;
    public final Context A02;
    public final AnonymousClass0SX A03;
    public final Object A04 = new Object();
    public final String A05;
    public final boolean A06;

    public AnonymousClass0ZH(Context context, AnonymousClass0SX r3, String str, boolean z) {
        this.A02 = context;
        this.A05 = str;
        this.A03 = r3;
        this.A06 = z;
    }

    public final C020309q A00() {
        C020309q r0;
        C020309q r3;
        String str;
        synchronized (this.A04) {
            if (this.A00 == null) {
                AnonymousClass0ZE[] r5 = new AnonymousClass0ZE[1];
                if (Build.VERSION.SDK_INT < 23 || (str = this.A05) == null || !this.A06) {
                    r3 = new C020309q(this.A02, this.A03, this.A05, r5);
                } else {
                    Context context = this.A02;
                    r3 = new C020309q(context, this.A03, new File(context.getNoBackupFilesDir(), str).getAbsolutePath(), r5);
                }
                this.A00 = r3;
                r3.setWriteAheadLoggingEnabled(this.A01);
            }
            r0 = this.A00;
        }
        return r0;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        A00().close();
    }
}
