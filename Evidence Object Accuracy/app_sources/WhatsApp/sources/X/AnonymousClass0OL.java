package X;

/* renamed from: X.0OL  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0OL {
    public final int A00;
    public final int A01;

    public abstract void A00(AbstractC12920im v);

    public AnonymousClass0OL(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
    }
}
