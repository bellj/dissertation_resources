package X;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/* renamed from: X.0ZJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZJ implements AbstractC12940io, AbstractC12390hq {
    public static final TreeMap A08 = new TreeMap();
    public int A00;
    public final int A01;
    public final double[] A02;
    public final int[] A03;
    public final long[] A04;
    public final String[] A05;
    public final byte[][] A06;
    public volatile String A07;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public AnonymousClass0ZJ(int i) {
        this.A01 = i;
        int i2 = i + 1;
        this.A03 = new int[i2];
        this.A04 = new long[i2];
        this.A02 = new double[i2];
        this.A05 = new String[i2];
        this.A06 = new byte[i2];
    }

    public static AnonymousClass0ZJ A00(String str, int i) {
        TreeMap treeMap = A08;
        synchronized (treeMap) {
            Map.Entry ceilingEntry = treeMap.ceilingEntry(Integer.valueOf(i));
            if (ceilingEntry != null) {
                treeMap.remove(ceilingEntry.getKey());
                AnonymousClass0ZJ r0 = (AnonymousClass0ZJ) ceilingEntry.getValue();
                r0.A07 = str;
                r0.A00 = i;
                return r0;
            }
            AnonymousClass0ZJ r02 = new AnonymousClass0ZJ(i);
            r02.A07 = str;
            r02.A00 = i;
            return r02;
        }
    }

    public void A01() {
        TreeMap treeMap = A08;
        synchronized (treeMap) {
            treeMap.put(Integer.valueOf(this.A01), this);
            if (treeMap.size() > 15) {
                int size = treeMap.size() - 10;
                Iterator it = treeMap.descendingKeySet().iterator();
                while (true) {
                    int i = size - 1;
                    if (size <= 0) {
                        break;
                    }
                    it.next();
                    it.remove();
                    size = i;
                }
            }
        }
    }

    @Override // X.AbstractC12940io
    public void A6P(int i, byte[] bArr) {
        this.A03[i] = 5;
        this.A06[i] = bArr;
    }

    @Override // X.AbstractC12940io
    public void A6R(int i, double d) {
        this.A03[i] = 3;
        this.A02[i] = d;
    }

    @Override // X.AbstractC12940io
    public void A6S(int i, long j) {
        this.A03[i] = 2;
        this.A04[i] = j;
    }

    @Override // X.AbstractC12940io
    public void A6T(int i) {
        this.A03[i] = 1;
    }

    @Override // X.AbstractC12940io
    public void A6U(int i, String str) {
        this.A03[i] = 4;
        this.A05[i] = str;
    }

    @Override // X.AbstractC12390hq
    public void A6V(AbstractC12940io r5) {
        for (int i = 1; i <= this.A00; i++) {
            int i2 = this.A03[i];
            if (i2 == 1) {
                r5.A6T(i);
            } else if (i2 == 2) {
                r5.A6S(i, this.A04[i]);
            } else if (i2 == 3) {
                r5.A6R(i, this.A02[i]);
            } else if (i2 == 4) {
                r5.A6U(i, this.A05[i]);
            } else if (i2 == 5) {
                r5.A6P(i, this.A06[i]);
            }
        }
    }

    @Override // X.AbstractC12390hq
    public String AGr() {
        return this.A07;
    }
}
