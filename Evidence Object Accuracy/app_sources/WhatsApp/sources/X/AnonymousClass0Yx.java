package X;

import android.text.TextUtils;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import com.whatsapp.R;

/* renamed from: X.0Yx  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0Yx implements AbstractC11840gx {
    public static AnonymousClass0Yx A00;

    @Override // X.AbstractC11840gx
    public CharSequence AZc(Preference preference) {
        EditTextPreference editTextPreference = (EditTextPreference) preference;
        if (TextUtils.isEmpty(editTextPreference.A00)) {
            return ((Preference) editTextPreference).A05.getString(R.string.not_set);
        }
        return editTextPreference.A00;
    }
}
