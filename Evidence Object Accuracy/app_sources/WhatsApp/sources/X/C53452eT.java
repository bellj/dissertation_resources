package X;

import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.whatsapp.Conversation;
import com.whatsapp.R;

/* renamed from: X.2eT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53452eT extends AnonymousClass04v {
    public final /* synthetic */ Conversation A00;

    public C53452eT(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r5) {
        super.A06(view, r5);
        AccessibilityNodeInfo accessibilityNodeInfo = r5.A02;
        accessibilityNodeInfo.setClickable(false);
        accessibilityNodeInfo.setLongClickable(false);
        r5.A0A(C007804a.A05);
        accessibilityNodeInfo.setContentDescription(this.A00.getString(R.string.record_audio_description));
    }
}
