package X;

import android.widget.BaseAdapter;
import com.whatsapp.status.playback.MyStatusesActivity;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1et  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33671et extends BaseAdapter {
    public List A00 = new ArrayList();
    public final /* synthetic */ MyStatusesActivity A01;

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    public /* synthetic */ C33671et(MyStatusesActivity myStatusesActivity) {
        this.A01 = myStatusesActivity;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A00.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A00.get(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0225, code lost:
        if (r7 == 37) goto L_0x0227;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0149  */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r16, android.view.View r17, android.view.ViewGroup r18) {
        /*
        // Method dump skipped, instructions count: 567
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33671et.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
