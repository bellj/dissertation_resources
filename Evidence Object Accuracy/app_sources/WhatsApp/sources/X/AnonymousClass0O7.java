package X;

import android.content.Context;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.0O7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0O7 {
    public final Context A00;
    public final EnumC03830Jh A01;
    public final AnonymousClass0MX A02;
    public final AbstractC11890h2 A03;
    public final String A04;
    public final List A05;
    public final Executor A06;
    public final Executor A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;

    public AnonymousClass0O7(Context context, EnumC03830Jh r2, AnonymousClass0MX r3, AbstractC11890h2 r4, String str, List list, Executor executor, Executor executor2, boolean z, boolean z2, boolean z3) {
        this.A03 = r4;
        this.A00 = context;
        this.A04 = str;
        this.A02 = r3;
        this.A05 = list;
        this.A09 = z;
        this.A01 = r2;
        this.A06 = executor;
        this.A07 = executor2;
        this.A0A = z2;
        this.A08 = z3;
    }
}
