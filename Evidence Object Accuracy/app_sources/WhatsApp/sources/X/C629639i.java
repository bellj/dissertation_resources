package X;

import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.File;
import java.io.RandomAccessFile;
import java.security.MessageDigest;

/* renamed from: X.39i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C629639i extends RandomAccessFile {
    public long A00;
    public byte[] A01;
    public final MessageDigest A02 = MessageDigest.getInstance("SHA-256");

    public C629639i(File file, String str) {
        super(file, str);
    }

    public String A00() {
        byte[] bArr = this.A01;
        if (bArr == null) {
            seek(this.A00);
            do {
            } while (read(new byte[DefaultCrypto.BUFFER_SIZE], 0, DefaultCrypto.BUFFER_SIZE) != -1);
            bArr = this.A02.digest();
            this.A01 = bArr;
        }
        return Base64.encodeToString(bArr, 2);
    }

    public final void A01(long j, byte[] bArr, int i, int i2) {
        if (i2 > 0) {
            long j2 = this.A00;
            if (j2 >= j && j2 < ((long) i2) + j) {
                this.A01 = null;
                int i3 = ((int) (j - j2)) + i2;
                this.A02.update(bArr, (i + i2) - i3, i3);
                this.A00 += (long) i3;
                length();
            }
        }
    }

    @Override // java.io.RandomAccessFile
    public int read(byte[] bArr, int i, int i2) {
        int read = super.read(bArr, i, i2);
        A01(getFilePointer() - ((long) read), bArr, i, read);
        return read;
    }

    @Override // java.io.RandomAccessFile, java.io.DataOutput
    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    @Override // java.io.RandomAccessFile, java.io.DataOutput
    public void write(byte[] bArr, int i, int i2) {
        super.write(bArr, i, i2);
        A01(getFilePointer() - ((long) i2), bArr, i, i2);
    }
}
