package X;

import android.content.Context;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2xt  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xt extends AnonymousClass1OY {
    public AnonymousClass11L A00;
    public boolean A01;
    public final TextView A02;

    public AnonymousClass2xt(Context context, AbstractC13890kV r4, AnonymousClass1XB r5) {
        super(context, r4, r5);
        A0Z();
        setClickable(false);
        setLongClickable(false);
        TextView A0I = C12960it.A0I(this, R.id.info);
        this.A02 = A0I;
        A0I.setBackgroundResource(R.drawable.date_balloon);
        A0I.setTextSize(AnonymousClass1OY.A00(getResources()));
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A00 = (AnonymousClass11L) A08.ALG.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (((X.AnonymousClass1OY) r5).A0L.A0F(r1) == false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A1M() {
        /*
            r5 = this;
            X.0mz r4 = r5.A0O
            X.1XB r4 = (X.AnonymousClass1XB) r4
            boolean r0 = r4.A14()
            r2 = 0
            if (r0 == 0) goto L_0x001a
            X.0lm r1 = r4.A0B()
            if (r1 == 0) goto L_0x001a
            X.0nT r0 = r5.A0L
            boolean r0 = r0.A0F(r1)
            r3 = 1
            if (r0 != 0) goto L_0x001b
        L_0x001a:
            r3 = 0
        L_0x001b:
            X.11L r0 = r5.A00
            java.lang.String r0 = r0.A0A(r4, r2)
            android.widget.TextView r2 = r5.A02
            r2.setText(r0)
            r1 = 0
            com.facebook.redex.ViewOnClickCListenerShape1S0110000_I1 r0 = new com.facebook.redex.ViewOnClickCListenerShape1S0110000_I1
            r0.<init>(r5, r1, r3)
            r2.setOnClickListener(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2xt.A1M():void");
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public C32261bs getFMessage() {
        return (C32261bs) ((AbstractC28551Oa) this).A0O;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_divider;
    }

    @Override // X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C32261bs);
        ((AbstractC28551Oa) this).A0O = r2;
    }
}
