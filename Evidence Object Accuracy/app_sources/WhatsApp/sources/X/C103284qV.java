package X;

import android.content.Context;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.4qV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103284qV implements AbstractC009204q {
    public final /* synthetic */ ViewProfilePhoto A00;

    public C103284qV(ViewProfilePhoto viewProfilePhoto) {
        this.A00 = viewProfilePhoto;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
