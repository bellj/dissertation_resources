package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.65v  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass65v implements AbstractC009404s {
    public final AnonymousClass12H A00;
    public final C14850m9 A01;
    public final UserJid A02;
    public final C243515e A03;
    public final AnonymousClass2I6 A04;
    public final AnonymousClass1IS A05;
    public final AbstractC14440lR A06;
    public final boolean A07;

    public AnonymousClass65v(AnonymousClass12H r2, C14850m9 r3, UserJid userJid, C243515e r5, AnonymousClass2I6 r6, AnonymousClass1IS r7, AbstractC14440lR r8, boolean z) {
        C16700pc.A0E(r3, 1);
        C117295Zj.A1K(r8, r2, r5, r6);
        C16700pc.A0E(r7, 7);
        this.A01 = r3;
        this.A06 = r8;
        this.A00 = r2;
        this.A03 = r5;
        this.A04 = r6;
        this.A02 = userJid;
        this.A05 = r7;
        this.A07 = z;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C14850m9 r2 = this.A01;
        AbstractC14440lR r7 = this.A06;
        return new C118155bM(this.A00, r2, this.A02, this.A03, this.A04, this.A05, r7, this.A07);
    }
}
