package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.whatsapp.R;
import com.whatsapp.location.LiveLocationPrivacyActivity;

/* renamed from: X.2bh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52792bh extends BaseAdapter {
    public final /* synthetic */ LiveLocationPrivacyActivity A00;

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public /* synthetic */ C52792bh(LiveLocationPrivacyActivity liveLocationPrivacyActivity) {
        this.A00 = liveLocationPrivacyActivity;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A00.A0G.size();
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A00.A0G.get(i);
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return ((C15370n3) C12990iw.A0k(this.A00.A0G, i)).A08();
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C91404Rp r0;
        if (view == null) {
            view = C12960it.A0F(this.A00.getLayoutInflater(), viewGroup, R.layout.live_location_privacy_row);
            r0 = new C91404Rp();
            r0.A02 = C12970iu.A0U(view, R.id.name);
            r0.A01 = C12960it.A0J(view, R.id.time_left);
            r0.A00 = C12970iu.A0L(view, R.id.avatar);
            view.setTag(r0);
        } else {
            r0 = (C91404Rp) view.getTag();
        }
        LiveLocationPrivacyActivity liveLocationPrivacyActivity = this.A00;
        C15370n3 r5 = (C15370n3) C12990iw.A0k(liveLocationPrivacyActivity.A0G, i);
        if (r5 != null) {
            long A00 = ((ActivityC13790kL) liveLocationPrivacyActivity).A05.A00();
            long A03 = liveLocationPrivacyActivity.A0D.A03(C15370n3.A01(r5));
            r0.A03 = r5;
            r0.A01.setText(C38131nZ.A07(((ActivityC13830kP) liveLocationPrivacyActivity).A01, A03 - A00));
            r0.A02.setText(((ActivityC13830kP) liveLocationPrivacyActivity).A01.A0F(liveLocationPrivacyActivity.A07.A04(r5)));
            AnonymousClass028.A0a(r0.A00, 2);
            liveLocationPrivacyActivity.A08.A06(r0.A00, r0.A03);
        }
        return view;
    }
}
