package X;

/* renamed from: X.4T1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4T1 {
    public final long A00;
    public final long A01;
    public final C28741Ov A02;
    public final boolean A03;
    public final boolean A04;
    public final boolean A05;

    public AnonymousClass4T1(C28741Ov r1, long j, long j2, boolean z, boolean z2, boolean z3) {
        this.A02 = r1;
        this.A00 = j;
        this.A01 = j2;
        this.A04 = z;
        this.A03 = z2;
        this.A05 = z3;
    }
}
