package X;

import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;

/* renamed from: X.5j0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121615j0 extends AbstractActivityC121525iS {
    public boolean A00 = false;

    public AbstractActivityC121615j0() {
        C117295Zj.A0p(this, 42);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = (IndiaUpiCheckOrderDetailsActivity) this;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r2, indiaUpiCheckOrderDetailsActivity);
            ActivityC13810kN.A10(A1M, indiaUpiCheckOrderDetailsActivity);
            AbstractActivityC119235dO.A1S(r2, A1M, indiaUpiCheckOrderDetailsActivity, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(r2, A1M, indiaUpiCheckOrderDetailsActivity, ActivityC13790kL.A0Y(A1M, indiaUpiCheckOrderDetailsActivity)), indiaUpiCheckOrderDetailsActivity));
            AbstractActivityC119235dO.A1Y(A1M, indiaUpiCheckOrderDetailsActivity);
            AbstractActivityC119235dO.A1Z(A1M, indiaUpiCheckOrderDetailsActivity);
            ((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A0Y = AbstractActivityC119235dO.A0M(r2, A1M, indiaUpiCheckOrderDetailsActivity, AbstractActivityC119235dO.A0m(A1M, indiaUpiCheckOrderDetailsActivity));
            indiaUpiCheckOrderDetailsActivity.A05 = (AnonymousClass17Q) A1M.A7n.get();
            indiaUpiCheckOrderDetailsActivity.A0C = (AnonymousClass14X) A1M.AFM.get();
            indiaUpiCheckOrderDetailsActivity.A01 = C12960it.A0R(A1M);
            indiaUpiCheckOrderDetailsActivity.A0B = (C25881Be) A1M.AEa.get();
            indiaUpiCheckOrderDetailsActivity.A02 = (AnonymousClass12H) A1M.AC5.get();
            indiaUpiCheckOrderDetailsActivity.A04 = (AnonymousClass1A7) A1M.AEs.get();
            indiaUpiCheckOrderDetailsActivity.A0E = (AnonymousClass1FI) A1M.ADq.get();
            indiaUpiCheckOrderDetailsActivity.A08 = r2.A0B();
        }
    }
}
