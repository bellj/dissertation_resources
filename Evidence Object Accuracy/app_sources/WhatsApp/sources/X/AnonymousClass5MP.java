package X;

import java.math.BigInteger;

/* renamed from: X.5MP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MP extends AnonymousClass1TM {
    public BigInteger A00;

    public AnonymousClass5MP(BigInteger bigInteger) {
        this.A00 = bigInteger;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return new AnonymousClass5NG(this.A00);
    }

    public String toString() {
        return C12970iu.A0s(this.A00, C12960it.A0k("CRLNumber: "));
    }
}
