package X;

/* renamed from: X.1Jy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC27971Jy extends Exception {
    public AbstractC27971Jy(String str) {
        super(str);
    }

    public AbstractC27971Jy(String str, Throwable th) {
        super(str, th);
    }

    public AbstractC27971Jy(Throwable th) {
        super(th);
    }
}
