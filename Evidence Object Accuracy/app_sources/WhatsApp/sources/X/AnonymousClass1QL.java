package X;

import com.facebook.soloader.SoLoader;

/* renamed from: X.1QL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1QL {
    public static AnonymousClass1QM A00;

    public static void A00(String str) {
        synchronized (AnonymousClass1QL.class) {
            if (A00 == null) {
                throw new IllegalStateException("NativeLoader has not been initialized.  To use standard native library loading, call NativeLoader.init(new SystemDelegate()).");
            }
        }
        SoLoader.A04(str);
    }
}
