package X;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ListView;

/* renamed from: X.0WV  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WV implements View.OnTouchListener {
    public static final int A0H = ViewConfiguration.getTapTimeout();
    public int A00;
    public int A01;
    public Runnable A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public float[] A08 = {Float.MAX_VALUE, Float.MAX_VALUE};
    public float[] A09 = {Float.MAX_VALUE, Float.MAX_VALUE};
    public float[] A0A = {0.0f, 0.0f};
    public float[] A0B = {0.0f, 0.0f};
    public float[] A0C = {0.0f, 0.0f};
    public final View A0D;
    public final Interpolator A0E = new AccelerateInterpolator();
    public final ListView A0F;
    public final C05190Op A0G = new C05190Op();

    public AnonymousClass0WV(ListView listView) {
        this.A0D = listView;
        float f = Resources.getSystem().getDisplayMetrics().density;
        float[] fArr = this.A09;
        float f2 = ((float) ((int) ((1575.0f * f) + 0.5f))) / 1000.0f;
        fArr[0] = f2;
        fArr[1] = f2;
        float[] fArr2 = this.A0A;
        float f3 = ((float) ((int) ((f * 315.0f) + 0.5f))) / 1000.0f;
        fArr2[0] = f3;
        fArr2[1] = f3;
        this.A01 = 1;
        float[] fArr3 = this.A08;
        fArr3[0] = Float.MAX_VALUE;
        fArr3[1] = Float.MAX_VALUE;
        float[] fArr4 = this.A0B;
        fArr4[0] = 0.2f;
        fArr4[1] = 0.2f;
        float[] fArr5 = this.A0C;
        float f4 = 1.0f / 1000.0f;
        fArr5[0] = f4;
        fArr5[1] = f4;
        this.A00 = A0H;
        C05190Op r1 = this.A0G;
        r1.A04 = 500;
        r1.A03 = 500;
        this.A0F = listView;
    }

    public final float A00(float f, float f2) {
        if (f2 != 0.0f) {
            int i = this.A01;
            if (i == 0 || i == 1) {
                if (f < f2) {
                    if (f >= 0.0f) {
                        return 1.0f - (f / f2);
                    }
                    if (this.A04 && i == 1) {
                        return 1.0f;
                    }
                }
            } else if (i == 2 && f < 0.0f) {
                return f / (-f2);
            }
        }
        return 0.0f;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0034 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final float A01(float r7, float r8, float r9, int r10) {
        /*
            r6 = this;
            float[] r0 = r6.A0B
            r3 = r0[r10]
            float[] r0 = r6.A08
            r2 = r0[r10]
            float r3 = r3 * r8
            r1 = 0
            int r0 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0049
            r3 = r2
        L_0x000f:
            float r0 = r6.A00(r7, r3)
            float r8 = r8 - r7
            float r2 = r6.A00(r8, r3)
            float r2 = r2 - r0
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x003c
            android.view.animation.Interpolator r1 = r6.A0E
            float r0 = -r2
            float r0 = r1.getInterpolation(r0)
            float r5 = -r0
        L_0x0025:
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r0 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0035
            r5 = 1065353216(0x3f800000, float:1.0)
        L_0x002f:
            r1 = 0
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x004f
            return r1
        L_0x0035:
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x002f
            r5 = -1082130432(0xffffffffbf800000, float:-1.0)
            goto L_0x002f
        L_0x003c:
            int r0 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0047
            android.view.animation.Interpolator r0 = r6.A0E
            float r5 = r0.getInterpolation(r2)
            goto L_0x0025
        L_0x0047:
            r5 = 0
            goto L_0x002f
        L_0x0049:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x000f
            r3 = 0
            goto L_0x000f
        L_0x004f:
            float[] r0 = r6.A0C
            r4 = r0[r10]
            float[] r0 = r6.A0A
            r3 = r0[r10]
            float[] r0 = r6.A09
            r2 = r0[r10]
            float r4 = r4 * r9
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x006c
            float r5 = r5 * r4
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x006b
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            r2 = r5
            if (r0 >= 0) goto L_0x006b
            return r3
        L_0x006b:
            return r2
        L_0x006c:
            float r1 = -r5
            float r1 = r1 * r4
            int r0 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x0078
            int r0 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            r2 = r1
            if (r0 >= 0) goto L_0x0078
            r2 = r3
        L_0x0078:
            float r2 = -r2
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WV.A01(float, float, float, int):float");
    }

    public final void A02() {
        if (this.A07) {
            this.A04 = false;
            return;
        }
        C05190Op r6 = this.A0G;
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        int i = (int) (currentAnimationTimeMillis - r6.A06);
        int i2 = r6.A03;
        if (i <= i2) {
            i2 = i;
            if (i < 0) {
                i2 = 0;
            }
        }
        r6.A02 = i2;
        r6.A00 = r6.A00(currentAnimationTimeMillis);
        r6.A07 = currentAnimationTimeMillis;
    }

    public boolean A03() {
        ListView listView;
        int count;
        float f = this.A0G.A01;
        int abs = (int) (f / Math.abs(f));
        if (abs == 0 || (count = (listView = this.A0F).getCount()) == 0) {
            return false;
        }
        int childCount = listView.getChildCount();
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int i = firstVisiblePosition + childCount;
        if (abs > 0) {
            if (i < count || listView.getChildAt(childCount - 1).getBottom() > listView.getHeight()) {
                return true;
            }
            return false;
        } else if (firstVisiblePosition > 0 || listView.getChildAt(0).getTop() < 0) {
            return true;
        } else {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0012, code lost:
        if (r1 != 3) goto L_0x0014;
     */
    @Override // android.view.View.OnTouchListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r7, android.view.MotionEvent r8) {
        /*
            r6 = this;
            boolean r0 = r6.A05
            r5 = 0
            if (r0 == 0) goto L_0x0014
            int r1 = r8.getActionMasked()
            r4 = 1
            if (r1 == 0) goto L_0x0015
            if (r1 == r4) goto L_0x006e
            r0 = 2
            if (r1 == r0) goto L_0x0019
            r0 = 3
            if (r1 == r0) goto L_0x006e
        L_0x0014:
            return r5
        L_0x0015:
            r6.A06 = r4
            r6.A03 = r5
        L_0x0019:
            float r2 = r8.getX()
            int r0 = r7.getWidth()
            float r1 = (float) r0
            android.view.View r3 = r6.A0D
            int r0 = r3.getWidth()
            float r0 = (float) r0
            r6.A01(r2, r1, r0, r5)
            float r2 = r8.getY()
            int r0 = r7.getHeight()
            float r1 = (float) r0
            int r0 = r3.getHeight()
            float r0 = (float) r0
            float r1 = r6.A01(r2, r1, r0, r4)
            X.0Op r0 = r6.A0G
            r0.A01 = r1
            boolean r0 = r6.A04
            if (r0 != 0) goto L_0x0014
            boolean r0 = r6.A03()
            if (r0 == 0) goto L_0x0014
            java.lang.Runnable r2 = r6.A02
            if (r2 != 0) goto L_0x0057
            X.0e9 r2 = new X.0e9
            r2.<init>(r6)
            r6.A02 = r2
        L_0x0057:
            r6.A04 = r4
            r6.A07 = r4
            boolean r0 = r6.A03
            if (r0 != 0) goto L_0x006a
            int r0 = r6.A00
            if (r0 <= 0) goto L_0x006a
            long r0 = (long) r0
            r3.postOnAnimationDelayed(r2, r0)
        L_0x0067:
            r6.A03 = r4
            return r5
        L_0x006a:
            r2.run()
            goto L_0x0067
        L_0x006e:
            r6.A02()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WV.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
