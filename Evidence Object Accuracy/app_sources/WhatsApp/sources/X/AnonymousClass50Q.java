package X;

import org.chromium.net.UrlRequest;

/* renamed from: X.50Q  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass50Q implements AbstractC115655Sk {
    public static final AbstractC115655Sk A00 = new AnonymousClass50Q();

    @Override // X.AbstractC115655Sk
    public final boolean Agr(int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return true;
            default:
                return false;
        }
    }
}
