package X;

/* renamed from: X.1sT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public enum EnumC40871sT {
    A02(0),
    A01(1),
    A03(2);
    
    public final int value;

    EnumC40871sT(int i) {
        this.value = i;
    }
}
