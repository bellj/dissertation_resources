package X;

import android.content.ClipData;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1MD  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1MD implements AnonymousClass02Q {
    public MenuItem A00;
    public MenuItem A01;
    public MenuItem A02;
    public MenuItem A03;
    public MenuItem A04;
    public MenuItem A05;
    public MenuItem A06;
    public MenuItem A07;
    public MenuItem A08;
    public MenuItem A09;
    public MenuItem A0A;
    public MenuItem A0B;
    public MenuItem A0C;
    public MenuItem A0D;
    public MenuItem A0E;
    public MenuItem A0F;
    public MenuItem A0G;
    public final AnonymousClass12P A0H;
    public final AbstractC15710nm A0I;
    public final C14900mE A0J;
    public final C15570nT A0K;
    public final C15450nH A0L;
    public final C16170oZ A0M;
    public final ActivityC13790kL A0N;
    public final AnonymousClass12T A0O;
    public final C18850tA A0P;
    public final C15550nR A0Q;
    public final C22700zV A0R;
    public final C15610nY A0S;
    public final C63943Do A0T = new C63943Do();
    public final AnonymousClass1A6 A0U;
    public final C255419u A0V;
    public final AnonymousClass01d A0W;
    public final C14830m7 A0X;
    public final C14820m6 A0Y;
    public final AnonymousClass018 A0Z;
    public final C15600nX A0a;
    public final C253218y A0b;
    public final C242114q A0c;
    public final C22100yW A0d;
    public final C22180yf A0e;
    public final AnonymousClass19M A0f;
    public final C231510o A0g;
    public final AnonymousClass193 A0h;
    public final C14850m9 A0i;
    public final C16120oU A0j;
    public final C20710wC A0k;
    public final AnonymousClass109 A0l;
    public final C22370yy A0m;
    public final AnonymousClass13H A0n;
    public final C16630pM A0o;
    public final C88054Ec A0p;
    public final AnonymousClass12F A0q;
    public final C253018w A0r;
    public final AnonymousClass12U A0s;
    public final C23000zz A0t;
    public final C252718t A0u;
    public final AbstractC14440lR A0v;
    public final AnonymousClass01H A0w;

    public AnonymousClass1MD(AnonymousClass12P r2, AbstractC15710nm r3, C14900mE r4, C15570nT r5, C15450nH r6, C16170oZ r7, ActivityC13790kL r8, AnonymousClass12T r9, C18850tA r10, C15550nR r11, C22700zV r12, C15610nY r13, AnonymousClass1A6 r14, C255419u r15, AnonymousClass01d r16, C14830m7 r17, C14820m6 r18, AnonymousClass018 r19, C15600nX r20, C253218y r21, C242114q r22, C22100yW r23, C22180yf r24, AnonymousClass19M r25, C231510o r26, AnonymousClass193 r27, C14850m9 r28, C16120oU r29, C20710wC r30, AnonymousClass109 r31, C22370yy r32, AnonymousClass13H r33, C16630pM r34, C88054Ec r35, AnonymousClass12F r36, C253018w r37, AnonymousClass12U r38, C23000zz r39, C252718t r40, AbstractC14440lR r41, AnonymousClass01H r42) {
        this.A0N = r8;
        this.A0X = r17;
        this.A0i = r28;
        this.A0J = r4;
        this.A0u = r40;
        this.A0n = r33;
        this.A0r = r37;
        this.A0I = r3;
        this.A0K = r5;
        this.A0v = r41;
        this.A0s = r38;
        this.A0j = r29;
        this.A0f = r25;
        this.A0L = r6;
        this.A0P = r10;
        this.A0M = r7;
        this.A0g = r26;
        this.A0p = r35;
        this.A0H = r2;
        this.A0Q = r11;
        this.A0e = r24;
        this.A0W = r16;
        this.A0S = r13;
        this.A0Z = r19;
        this.A0k = r30;
        this.A0q = r36;
        this.A0b = r21;
        this.A0O = r9;
        this.A0h = r27;
        this.A0c = r22;
        this.A0t = r39;
        this.A0R = r12;
        this.A0Y = r18;
        this.A0m = r32;
        this.A0d = r23;
        this.A0V = r15;
        this.A0l = r31;
        this.A0a = r20;
        this.A0o = r34;
        this.A0w = r42;
        this.A0U = r14;
    }

    public static void A00(Context context, C14900mE r11, C15570nT r12, C15550nR r13, C15610nY r14, AnonymousClass01d r15, AnonymousClass018 r16, AnonymousClass13H r17, C16630pM r18, Collection collection) {
        String A0I;
        String str;
        String str2;
        String str3;
        String A04;
        HashSet hashSet = new HashSet();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            if (!r2.A0y() || r2.A0F().A00 == null) {
                byte b = r2.A0y;
                if (b == 0 || b == 32 || b == 46) {
                    A0I = r2.A0I();
                } else {
                    A0I = null;
                    if (r2 instanceof AbstractC16390ow) {
                        C33711ex ACL = ((AbstractC16390ow) r2).ACL();
                        if (ACL != null) {
                            if (!(ACL instanceof C34991h2)) {
                                C16470p4 r8 = ACL.A00;
                                if (r8 != null) {
                                    StringBuilder sb3 = new StringBuilder();
                                    C33711ex.A00(ACL.A04(), "\n", sb3);
                                    AnonymousClass1Z6 r0 = r8.A02;
                                    if (r0 != null) {
                                        str3 = r0.A00;
                                    } else {
                                        str3 = null;
                                    }
                                    C33711ex.A00(str3, "\n", sb3);
                                    C33711ex.A00(r8.A07, "\n", sb3);
                                    C33711ex.A00(r8.A08, "\n", sb3);
                                    A0I = sb3.toString();
                                }
                            } else {
                                StringBuilder sb4 = new StringBuilder();
                                C16470p4 r4 = ACL.A00;
                                if (r4 != null) {
                                    str = r4.A07;
                                } else {
                                    str = null;
                                }
                                C33711ex.A00(str, "\n", sb4);
                                if (r4 != null) {
                                    str2 = r4.A08;
                                } else {
                                    str2 = null;
                                }
                                C33711ex.A00(str2, "\n", sb4);
                                A0I = sb4.toString();
                            }
                        }
                    } else if (r2 instanceof AnonymousClass1XV) {
                        A0I = ((AnonymousClass1XV) r2).A1E();
                    } else if (r2 instanceof AbstractC16130oV) {
                        A0I = ((AbstractC16130oV) r2).A15();
                    } else if (r2 instanceof C28851Pg) {
                        A0I = ((C28851Pg) r2).A18();
                    }
                }
            } else {
                A0I = C35011h5.A02(r2.A0F().A00, r2.A0I());
            }
            if (!TextUtils.isEmpty(A0I)) {
                StringBuilder sb5 = new StringBuilder();
                if (sb.length() != 0) {
                    sb5.append('\n');
                }
                if (collection.size() > 1) {
                    sb5.append('[');
                    sb5.append(DateUtils.formatDateTime(context, r2.A0I, 655377));
                    sb5.append("] ");
                    if (r2.A0z.A02) {
                        A04 = r12.A05();
                    } else {
                        A04 = r14.A04(r13.A0B(r2.A0C()));
                    }
                    sb5.append(A04);
                    sb5.append(": ");
                }
                sb.append((CharSequence) sb5);
                sb2.append((CharSequence) sb5);
                sb2.append(A0I);
                List list = r2.A0o;
                if (list != null) {
                    sb.append(r17.A00(A0I, list));
                    hashSet.addAll(r2.A0o);
                } else {
                    sb.append(A0I);
                }
            }
        }
        String obj = sb.toString();
        SharedPreferences.Editor edit = r18.A01(AnonymousClass01V.A07).edit();
        if (!hashSet.isEmpty()) {
            edit.putString("copied_message", obj);
            edit.putString("copied_message_without_mentions", sb2.toString());
            edit.putString("copied_message_jids", AnonymousClass1Y6.A00(hashSet));
        } else {
            edit.remove("copied_message");
            edit.remove("copied_message_without_mentions");
            edit.remove("copied_message_jids");
        }
        edit.apply();
        try {
            r15.A0B().setPrimaryClip(ClipData.newPlainText(obj, obj));
            if (collection.size() == 1) {
                r11.A07(R.string.message_copied, 0);
                return;
            }
            r11.A0E(r16.A0I(new Object[]{Integer.valueOf(collection.size())}, R.plurals.messages_copied, (long) collection.size()), 0);
        } catch (NullPointerException | SecurityException e) {
            Log.e("conversation/copymessage/npe", e);
            r11.A07(R.string.view_contact_unsupport, 0);
        }
    }

    public AbstractC15340mz A01() {
        Map A02 = A02();
        AnonymousClass009.A05(A02);
        return (AbstractC15340mz) ((Map.Entry) A02.entrySet().iterator().next()).getValue();
    }

    public Map A02() {
        C35451ht r0;
        if (this instanceof AnonymousClass1MC) {
            r0 = ((AnonymousClass1MC) this).A00.A0H;
            if (r0 == null) {
                return null;
            }
        } else if (this instanceof AnonymousClass2z6) {
            return ((AnonymousClass2z6) this).A00.A16;
        } else {
            if (this instanceof AnonymousClass2z4) {
                r0 = ((AnonymousClass2z4) this).A00.A0F;
                if (r0 == null) {
                    return null;
                }
            } else if (this instanceof C60942z3) {
                r0 = ((AbstractActivityC13750kH) ((C60942z3) this).A00).A0J;
                if (r0 == null) {
                    return null;
                }
            } else if (!(this instanceof AnonymousClass2z5)) {
                r0 = ((AbstractActivityC13750kH) ((AnonymousClass2z7) this).A00).A0J;
                if (r0 == null) {
                    return null;
                }
            } else {
                r0 = ((AbstractActivityC13750kH) ((AnonymousClass2z5) this).A00).A0J;
                if (r0 == null) {
                    return null;
                }
            }
        }
        return r0.A04;
    }

    public void A03() {
        AbstractC009504t r0;
        if (this instanceof AnonymousClass1MC) {
            r0 = ((AnonymousClass1MC) this).A00.A06;
        } else if (this instanceof AnonymousClass2z6) {
            r0 = ((AnonymousClass2z6) this).A00.A01;
        } else if (this instanceof AnonymousClass2z4) {
            r0 = ((AnonymousClass2z4) this).A00.A06;
        } else if (this instanceof C60942z3) {
            r0 = ((AbstractActivityC13750kH) ((C60942z3) this).A00).A01;
        } else if (!(this instanceof AnonymousClass2z5)) {
            r0 = ((AbstractActivityC13750kH) ((AnonymousClass2z7) this).A00).A01;
        } else {
            r0 = ((AbstractActivityC13750kH) ((AnonymousClass2z5) this).A00).A01;
        }
        if (r0 != null) {
            r0.A05();
        }
    }

    public void A04() {
        Map A02 = A02();
        if (A02 == null || A02.isEmpty()) {
            Log.e("conversation/deleteselectedessages/nothingselected");
        } else {
            C36021jC.A01(this.A0N, 13);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0338, code lost:
        if (X.C30041Vv.A0v(r8) != false) goto L_0x033a;
     */
    @Override // X.AnonymousClass02Q
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean ALr(android.view.MenuItem r19, X.AbstractC009504t r20) {
        /*
        // Method dump skipped, instructions count: 963
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MD.ALr(android.view.MenuItem, X.04t):boolean");
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r13) {
        this.A0B = menu.add(0, R.id.menuitem_reply, 0, R.string.notification_quick_reply).setIcon(R.drawable.ic_action_reply);
        this.A00 = menu.add(0, R.id.menuitem_star, 0, R.string.add_star).setIcon(R.drawable.ic_action_star);
        this.A0A = menu.add(0, R.id.menuitem_unstar, 0, R.string.remove_star).setIcon(R.drawable.ic_action_unstar);
        this.A05 = menu.add(0, R.id.menuitem_details, 0, R.string.info).setIcon(R.drawable.ic_action_info);
        this.A04 = menu.add(0, R.id.menuitem_delete, 0, R.string.delete).setIcon(R.drawable.ic_action_delete);
        this.A03 = menu.add(0, R.id.menuitem_copy, 0, R.string.copy).setIcon(R.drawable.ic_action_copy);
        this.A0E = menu.add(0, R.id.menuitem_share, 0, R.string.share).setIcon(R.drawable.ic_action_share);
        this.A02 = menu.add(0, R.id.menuitem_cancel_transfer, 0, R.string.cancel).setIcon(R.drawable.ic_action_cancel);
        this.A07 = menu.add(0, R.id.menuitem_forward, 0, R.string.conversation_menu_forward).setIcon(R.drawable.ic_action_forward);
        this.A06 = menu.add(0, R.id.menuitem_message_edit, 0, R.string.conversation_menu_message_edit).setIcon(R.drawable.ic_action_edit);
        this.A0C = menu.add(0, R.id.menuitem_reply_privately, 0, R.string.reply_privately);
        this.A01 = menu.add(0, R.id.menuitem_add_to_contacts, 0, R.string.add_contact);
        this.A08 = menu.add(0, R.id.menuitem_message_contact, 0, R.string.message_contact_name);
        this.A0F = menu.add(0, R.id.menuitem_share_third_party, 0, R.string.menuitem_status_share);
        this.A0G = menu.add(0, R.id.menuitem_share_cross, 0, R.string.menuitem_status_share_with_fb);
        this.A0D = menu.add(0, R.id.menuitem_report_message, 0, R.string.report_spam);
        this.A09 = menu.add(0, R.id.menuitem_rate_message, 0, R.string.rate_message);
        C63943Do r0 = this.A0T;
        r0.A00(R.id.menuitem_reply_privately);
        r0.A00(R.id.menuitem_add_to_contacts);
        r0.A00(R.id.menuitem_message_contact);
        r0.A00(R.id.menuitem_share_third_party);
        r0.A00(R.id.menuitem_share_cross);
        r0.A00(R.id.menuitem_report_message);
        r0.A00(R.id.menuitem_rate_message);
        Set set = r0.A01;
        set.add(Integer.valueOf((int) R.id.menuitem_forward));
        set.add(Integer.valueOf((int) R.id.menuitem_delete));
        return true;
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r2) {
        Log.i("conversation/selectionended");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v6, resolved type: int */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v5 */
    /* JADX WARN: Type inference failed for: r8v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01c1, code lost:
        if (X.C30041Vv.A0W(r4, r2) == false) goto L_0x01c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x028e, code lost:
        if (r13 != false) goto L_0x0290;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0299, code lost:
        if (r13 != false) goto L_0x029b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x048c, code lost:
        if (r7.A0i.A01() == false) goto L_0x048e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c5, code lost:
        if (android.text.TextUtils.isEmpty(r1.A0I()) != false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00cb, code lost:
        if (X.C30041Vv.A0l(r1) != false) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cd, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00dd, code lost:
        if (r1 == 1) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0101, code lost:
        if (r4 != 55) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x016e, code lost:
        if (r1.A02 != false) goto L_0x0170;
     */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x04a3  */
    @Override // X.AnonymousClass02Q
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean AU7(android.view.Menu r22, X.AbstractC009504t r23) {
        /*
        // Method dump skipped, instructions count: 1250
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1MD.AU7(android.view.Menu, X.04t):boolean");
    }
}
