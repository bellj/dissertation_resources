package X;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import sun.misc.Unsafe;

/* renamed from: X.4e6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95634e6 {
    public static final long A00 = ((long) A01(byte[].class));
    public static final AnonymousClass4YS A01;
    public static final Class A02 = AnonymousClass4ZV.A00;
    public static final Unsafe A03;
    public static final boolean A04 = C12970iu.A1Z(ByteOrder.nativeOrder(), ByteOrder.BIG_ENDIAN);
    public static final boolean A05;
    public static final boolean A06;

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0138 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    static {
        /*
        // Method dump skipped, instructions count: 551
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95634e6.<clinit>():void");
    }

    public static byte A00(byte[] bArr, long j) {
        return A01.A01(bArr, A00 + j);
    }

    public static int A01(Class cls) {
        if (A06) {
            return A01.A00.arrayBaseOffset(cls);
        }
        return -1;
    }

    public static Object A02(Class cls) {
        try {
            return A03.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Object A03(Object obj, long j) {
        return A01.A00.getObject(obj, j);
    }

    public static Field A04() {
        Field field;
        Field field2;
        if (AnonymousClass4ZV.A00()) {
            try {
                field2 = Buffer.class.getDeclaredField("effectiveDirectAddress");
            } catch (Throwable unused) {
                field2 = null;
            }
            if (field2 != null) {
                return field2;
            }
        }
        try {
            field = Buffer.class.getDeclaredField("address");
        } catch (Throwable unused2) {
            field = null;
        }
        if (field == null || field.getType() != Long.TYPE) {
            return null;
        }
        return field;
    }

    public static Unsafe A05() {
        try {
            return (Unsafe) AccessController.doPrivileged(new C112015Bs());
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void A06(Class cls) {
        if (A06) {
            A01.A00.arrayIndexScale(cls);
        }
    }

    public static void A07(Object obj, long j, byte b) {
        long j2 = -4 & j;
        AnonymousClass4YS r4 = A01;
        r4.A09(obj, j2, C72453ed.A09(((int) j) ^ -1, r4.A04(obj, j2), b));
    }

    public static void A08(Object obj, long j, byte b) {
        long j2 = -4 & j;
        AnonymousClass4YS r4 = A01;
        r4.A09(obj, j2, C72453ed.A09((int) j, r4.A04(obj, j2), b));
    }

    public static void A09(Object obj, long j, Object obj2) {
        A01.A00.putObject(obj, j, obj2);
    }

    public static void A0A(byte[] bArr, byte b, long j) {
        A01.A06(bArr, A00 + j, b);
    }
}
