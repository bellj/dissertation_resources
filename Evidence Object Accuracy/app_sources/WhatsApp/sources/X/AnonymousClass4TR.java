package X;

import android.os.SystemClock;
import java.util.TimeZone;

/* renamed from: X.4TR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4TR {
    public int A00;
    public EnumC87344Bd A01;
    public String A02;
    public boolean A03;
    public boolean A04 = true;
    public final C79493qg A05;
    public final /* synthetic */ C93784aj A06;

    public /* synthetic */ AnonymousClass4TR(C93784aj r6, byte[] bArr) {
        this.A06 = r6;
        this.A00 = r6.A00;
        this.A02 = r6.A02;
        this.A01 = r6.A01;
        C79493qg r4 = new C79493qg();
        this.A05 = r4;
        this.A03 = false;
        r4.A08 = C95184dH.A00(r6.A04);
        long currentTimeMillis = System.currentTimeMillis();
        r4.A01 = currentTimeMillis;
        r4.A02 = SystemClock.elapsedRealtime();
        r4.A03 = (long) (TimeZone.getDefault().getOffset(currentTimeMillis) / 1000);
        r4.A0A = bArr;
    }
}
