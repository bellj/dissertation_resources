package X;

import android.content.Context;

/* renamed from: X.5gB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120435gB extends C126705tJ {
    public AbstractC136216Lr A00;
    public final Context A01;
    public final C14900mE A02;
    public final AnonymousClass102 A03;
    public final C17220qS A04;
    public final C1308460e A05;
    public final C1329668y A06;
    public final C18650sn A07;
    public final C17070qD A08;

    public C120435gB(Context context, C14900mE r3, AnonymousClass102 r4, C17220qS r5, C1308460e r6, C1329668y r7, C18650sn r8, C18610sj r9, C17070qD r10, AbstractC136216Lr r11) {
        super(r6.A04, r9);
        this.A01 = context;
        this.A02 = r3;
        this.A04 = r5;
        this.A08 = r10;
        this.A05 = r6;
        this.A03 = r4;
        this.A07 = r8;
        this.A06 = r7;
        this.A00 = r11;
    }
}
