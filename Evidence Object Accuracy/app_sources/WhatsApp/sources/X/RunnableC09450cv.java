package X;

import java.util.concurrent.BlockingQueue;

/* renamed from: X.0cv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09450cv implements Runnable {
    public final /* synthetic */ C10930fR A00;

    public /* synthetic */ RunnableC09450cv(C10930fR r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            C10930fR r6 = this.A00;
            BlockingQueue blockingQueue = r6.A02;
            Runnable runnable = (Runnable) blockingQueue.poll();
            if (runnable != null) {
                runnable.run();
            } else {
                AnonymousClass0UN.A02(C10930fR.class, r6.A01, "%s: Worker has nothing to run");
            }
            int decrementAndGet = r6.A05.decrementAndGet();
            if (!blockingQueue.isEmpty()) {
                r6.A00();
            } else {
                AnonymousClass0UN.A01(C10930fR.class, r6.A01, Integer.valueOf(decrementAndGet), "%s: worker finished; %d workers left");
            }
        } catch (Throwable th) {
            C10930fR r1 = this.A00;
            int decrementAndGet2 = r1.A05.decrementAndGet();
            if (!r1.A02.isEmpty()) {
                r1.A00();
                throw th;
            } else {
                AnonymousClass0UN.A01(C10930fR.class, r1.A01, Integer.valueOf(decrementAndGet2), "%s: worker finished; %d workers left");
                throw th;
            }
        }
    }
}
