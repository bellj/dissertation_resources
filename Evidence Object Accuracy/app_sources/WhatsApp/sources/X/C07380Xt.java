package X;

import android.os.LocaleList;
import java.util.Locale;

/* renamed from: X.0Xt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07380Xt implements AnonymousClass06D {
    public final LocaleList A00;

    public C07380Xt(Object obj) {
        this.A00 = (LocaleList) obj;
    }

    @Override // X.AnonymousClass06D
    public Locale AAS(int i) {
        return this.A00.get(0);
    }

    @Override // X.AnonymousClass06D
    public Object ADx() {
        return this.A00;
    }

    @Override // X.AnonymousClass06D
    public String Aex() {
        return this.A00.toLanguageTags();
    }

    public boolean equals(Object obj) {
        return this.A00.equals(((AnonymousClass06D) obj).ADx());
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    @Override // X.AnonymousClass06D
    public int size() {
        return this.A00.size();
    }

    public String toString() {
        return this.A00.toString();
    }
}
