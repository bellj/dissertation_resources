package X;

/* renamed from: X.31E  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31E extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Integer A04;
    public Integer A05;
    public Integer A06;
    public Long A07;
    public Long A08;
    public Long A09;

    public AnonymousClass31E() {
        super(2494, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A00);
        r3.Abe(3, this.A04);
        r3.Abe(10, this.A07);
        r3.Abe(1, this.A08);
        r3.Abe(6, this.A01);
        r3.Abe(7, this.A02);
        r3.Abe(2, this.A09);
        r3.Abe(8, this.A03);
        r3.Abe(9, this.A05);
        r3.Abe(4, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamMessageStanzaReceive {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "hasSenderKeyDistributionMessage", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A04));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaDecryptQueueSize", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaDuration", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaE2eSuccess", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaIsEphemeral", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaOfflineCount", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaRevoke", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageStanzaStage", C12960it.A0Y(this.A05));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageType", C12960it.A0Y(this.A06));
        return C12960it.A0d("}", A0k);
    }
}
