package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.0rr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18070rr {
    public final AnonymousClass1Q5 A00;

    public C18070rr(C14830m7 r10, C16120oU r11, C21230x5 r12, AbstractC21180x0 r13) {
        AnonymousClass1Q5 r2 = new AnonymousClass1Q5(r10, r11, r12, r13, "ApplicationCreatePerfTracker", 703926783);
        r2.A06.A03 = true;
        this.A00 = r2;
    }

    public void A00() {
        AnonymousClass1Q5 r1 = this.A00;
        r1.A07("app_creation_on_create");
        r1.A0C(2);
    }

    public void A01() {
        this.A00.A08("app_creation_on_create");
    }

    public void A02(long j) {
        AnonymousClass1Q5 r0 = this.A00;
        r0.A08.ALI("perf_origin", "ApplicationCreatePerfTracker", TimeUnit.NANOSECONDS, 703926783, j);
        r0.A06(j);
    }

    public void A03(String str) {
        this.A00.A07(str);
    }

    public void A04(String str) {
        this.A00.A08(str);
    }
}
