package X;

import java.util.Comparator;

/* renamed from: X.5CI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5CI implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        C92414Vu r6 = (C92414Vu) obj;
        C92414Vu r7 = (C92414Vu) obj2;
        long j = r6.A01;
        long j2 = r7.A01;
        if (j != j2) {
            return j < j2 ? 1 : -1;
        }
        return r6.A03 - r7.A03;
    }
}
