package X;

import android.database.sqlite.SQLiteTransactionListener;
import java.util.AbstractMap;

/* renamed from: X.1Ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29621Ty implements SQLiteTransactionListener {
    public final ThreadLocal A00 = new AnonymousClass1U1(this);
    public final ThreadLocal A01 = new AnonymousClass1U0(this);

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onBegin() {
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onCommit() {
        Object obj = this.A01.get();
        AnonymousClass009.A05(obj);
        AbstractMap abstractMap = (AbstractMap) obj;
        try {
            for (SQLiteTransactionListener sQLiteTransactionListener : abstractMap.values()) {
                sQLiteTransactionListener.onCommit();
            }
        } finally {
            abstractMap.clear();
            this.A00.set(Boolean.FALSE);
        }
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onRollback() {
        Object obj = this.A01.get();
        AnonymousClass009.A05(obj);
        AbstractMap abstractMap = (AbstractMap) obj;
        try {
            for (SQLiteTransactionListener sQLiteTransactionListener : abstractMap.values()) {
                sQLiteTransactionListener.onRollback();
            }
        } finally {
            abstractMap.clear();
            this.A00.set(Boolean.FALSE);
        }
    }
}
