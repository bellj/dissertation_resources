package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.HomeActivity;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.community.AddGroupsToCommunityActivity;
import com.whatsapp.conversationslist.ConversationsFragment;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.group.NewGroup;
import com.whatsapp.invites.ViewGroupInviteActivity;
import com.whatsapp.util.Log;

/* renamed from: X.0xE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21320xE extends AbstractC16230of {
    public void A05() {
        for (C27151Gf r4 : A01()) {
            if (r4 instanceof C33141dV) {
                ((C33141dV) r4).A00.A0D();
            } else if (r4 instanceof C32901cv) {
                C32901cv r42 = (C32901cv) r4;
                r42.A00.A0C.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(r42, 27));
            } else if (r4 instanceof C32911cw) {
                C32911cw r43 = (C32911cw) r4;
                Log.i("ViewGroupInviteActivity/onConversationsListChanged");
                ViewGroupInviteActivity viewGroupInviteActivity = r43.A00;
                if (viewGroupInviteActivity.A0J != null) {
                    ((ActivityC13810kN) viewGroupInviteActivity).A05.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(r43, 23));
                }
            } else if (r4 instanceof C32921cx) {
                Log.i("newgroup/onConversationsListChanged");
                NewGroup newGroup = ((C32921cx) r4).A00;
                Object obj = newGroup.A0c.get();
                if (obj != null) {
                    ((ActivityC13810kN) newGroup).A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(newGroup, 19, obj));
                }
            } else if (r4 instanceof C32941cz) {
                ConversationsFragment conversationsFragment = ((C32941cz) r4).A00;
                conversationsFragment.A0L.A0H(new RunnableBRunnable0Shape5S0100000_I0_5(conversationsFragment, 29));
            } else if (r4 instanceof C32951d0) {
                C33061dE r3 = ((C32951d0) r4).A00.A0A;
                r3.A07.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(r3, 1));
            } else if (r4 instanceof C33151dW) {
                C33151dW r44 = (C33151dW) r4;
                Log.i("AddGroupsToCommunityActivity/onConversationsListChanged/");
                AddGroupsToCommunityActivity addGroupsToCommunityActivity = r44.A00;
                Object obj2 = addGroupsToCommunityActivity.A0Q.get();
                if (obj2 != null) {
                    ((ActivityC13810kN) addGroupsToCommunityActivity).A05.A0H(new RunnableBRunnable0Shape2S0200000_I0_2(r44, 2, obj2));
                }
            } else if (r4 instanceof C32981d3) {
                C32981d3 r45 = (C32981d3) r4;
                Log.i("acceptlink/onConversationsListChanged");
                AcceptInviteLinkActivity acceptInviteLinkActivity = r45.A00;
                if (acceptInviteLinkActivity.A0G != null) {
                    ((ActivityC13810kN) acceptInviteLinkActivity).A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(r45, 37));
                }
            } else if (r4 instanceof C32991d4) {
                HomeActivity homeActivity = ((C32991d4) r4).A00;
                ((ActivityC13810kN) homeActivity).A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(homeActivity, 6));
                if (((ActivityC13810kN) homeActivity).A0C.A07(1266) && homeActivity.A0x.A01() == 0) {
                    ((ActivityC13810kN) homeActivity).A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(homeActivity, 8));
                }
            }
        }
    }

    public void A06(int i) {
        for (C27151Gf r1 : A01()) {
            if (r1 instanceof C32941cz) {
                ((C32941cz) r1).A00.A1O(i);
            }
        }
    }

    public void A07(AbstractC14640lm r7) {
        for (C27151Gf r3 : A01()) {
            if (r3 instanceof C32901cv) {
                C32901cv r32 = (C32901cv) r3;
                r32.A00.A0C.A0I(new RunnableBRunnable0Shape7S0200000_I0_7(r32, 41, r7));
            } else if (r3 instanceof C32911cw) {
                C32911cw r33 = (C32911cw) r3;
                StringBuilder sb = new StringBuilder("ViewGroupInviteActivity/onConversationChanged/");
                sb.append(r7);
                Log.i(sb.toString());
                ViewGroupInviteActivity viewGroupInviteActivity = r33.A00;
                C15580nU r0 = viewGroupInviteActivity.A0J;
                if (r0 != null && r0.equals(r7)) {
                    ((ActivityC13810kN) viewGroupInviteActivity).A05.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(r33, 24));
                }
            } else if (r3 instanceof C32921cx) {
                C32921cx r34 = (C32921cx) r3;
                StringBuilder sb2 = new StringBuilder("newgroup/onConversationChanged/");
                sb2.append(r7);
                Log.i(sb2.toString());
                NewGroup newGroup = r34.A00;
                Object obj = newGroup.A0c.get();
                if (obj != null && obj.equals(r7)) {
                    ((ActivityC13810kN) newGroup).A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(r34, 20, obj));
                }
            } else if (r3 instanceof C32931cy) {
                C32931cy r35 = (C32931cy) r3;
                if (r7 != null) {
                    GroupChatInfo groupChatInfo = r35.A00;
                    if (r7.equals(groupChatInfo.A1C)) {
                        ((ActivityC13810kN) groupChatInfo).A05.A0H(new RunnableBRunnable0Shape6S0100000_I0_6(groupChatInfo, 40));
                    }
                }
            } else if (r3 instanceof C32941cz) {
                C32941cz r36 = (C32941cz) r3;
                r36.A00.A0L.A0H(new RunnableBRunnable0Shape3S0200000_I0_3(r36, 19, r7));
            } else if (r3 instanceof C32951d0) {
                ((C32951d0) r3).A00.A0A.A0E();
            } else if (r3 instanceof C32961d1) {
                C32961d1 r37 = (C32961d1) r3;
                r37.A00.A0O.execute(new RunnableBRunnable0Shape2S0200000_I0_2(r37, 13, r7));
            } else if (r3 instanceof C32971d2) {
                ContactInfoActivity contactInfoActivity = ((C32971d2) r3).A00;
                if (!(contactInfoActivity.A0s == null || r7 == null || !r7.equals(contactInfoActivity.A2v()))) {
                    ((ActivityC13810kN) contactInfoActivity).A05.A0H(new RunnableBRunnable0Shape3S0100000_I0_3(contactInfoActivity, 40));
                }
            } else if (r3 instanceof C32981d3) {
                C32981d3 r38 = (C32981d3) r3;
                StringBuilder sb3 = new StringBuilder("acceptlink/onConversationChanged/");
                sb3.append(r7);
                Log.i(sb3.toString());
                AcceptInviteLinkActivity acceptInviteLinkActivity = r38.A00;
                C15580nU r02 = acceptInviteLinkActivity.A0G;
                if (r02 != null && r02.equals(r7)) {
                    ((ActivityC13810kN) acceptInviteLinkActivity).A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(r38, 38));
                }
            } else if (r3 instanceof C32991d4) {
                HomeActivity homeActivity = ((C32991d4) r3).A00;
                ((ActivityC13810kN) homeActivity).A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(homeActivity, 7));
            }
        }
    }

    public void A08(AbstractC14640lm r3) {
        AnonymousClass009.A05(r3);
        for (C27151Gf r0 : A01()) {
            r0.A01(r3);
        }
    }

    public void A09(AbstractC14640lm r6, boolean z) {
        for (C27151Gf r3 : A01()) {
            if (r3 instanceof C32941cz) {
                C32941cz r32 = (C32941cz) r3;
                r32.A00.A0L.A0H(new RunnableBRunnable0Shape0S0210000_I0(r32, r6, 5, z));
            }
        }
    }
}
