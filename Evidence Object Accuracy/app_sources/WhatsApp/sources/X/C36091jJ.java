package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.1jJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36091jJ {
    public final List A00;
    public final Set A01;
    public final boolean A02;

    public C36091jJ(List list, Set set, boolean z) {
        this.A00 = list;
        this.A01 = set;
        this.A02 = z;
    }
}
