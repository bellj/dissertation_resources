package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0DH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DH extends C06400Tl {
    public static Class A00;
    public static Constructor A01;
    public static Method A02;
    public static Method A03;
    public static boolean A04;

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.lang.Class */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.reflect.Method */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.reflect.Method */
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: java.lang.reflect.Method */
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: java.lang.Class */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: java.lang.reflect.Method */
    /* JADX WARN: Multi-variable type inference failed */
    public static void A00() {
        Class cls;
        Method method;
        Method method2;
        if (!A04) {
            A04 = true;
            Constructor<?> constructor = null;
            try {
                Class<?> cls2 = Class.forName("android.graphics.FontFamily");
                Constructor<?> constructor2 = cls2.getConstructor(new Class[0]);
                Method method3 = cls2.getMethod("addFontWeightStyle", String.class, Integer.TYPE, Boolean.TYPE);
                constructor = constructor2;
                method2 = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(cls2, 1).getClass());
                method = method3;
                cls = cls2;
            } catch (ClassNotFoundException | NoSuchMethodException e) {
                Log.e("TypefaceCompatApi21Impl", e.getClass().getName(), e);
                method2 = constructor;
                cls = constructor;
                method = constructor;
            }
            A01 = constructor;
            A00 = cls;
            A02 = method;
            A03 = method2;
        }
    }

    @Override // X.C06400Tl
    public Typeface A04(Context context, Resources resources, AnonymousClass0MQ r15, int i) {
        A00();
        try {
            Object newInstance = A01.newInstance(new Object[0]);
            C05020Ny[] r8 = r15.A00;
            for (C05020Ny r2 : r8) {
                File A002 = C06500Tw.A00(context);
                if (A002 == null) {
                    return null;
                }
                try {
                    if (C06500Tw.A02(resources, A002, r2.A00)) {
                        String path = A002.getPath();
                        int i2 = r2.A02;
                        boolean z = r2.A05;
                        A00();
                        try {
                            if (((Boolean) A02.invoke(newInstance, path, Integer.valueOf(i2), Boolean.valueOf(z))).booleanValue()) {
                                A002.delete();
                            }
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    return null;
                } catch (RuntimeException unused) {
                    return null;
                } finally {
                    A002.delete();
                }
            }
            A00();
            try {
                Object newInstance2 = Array.newInstance(A00, 1);
                Array.set(newInstance2, 0, newInstance);
                return (Typeface) A03.invoke(null, newInstance2);
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new RuntimeException(e2);
            }
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }

    @Override // X.C06400Tl
    public Typeface A06(Context context, CancellationSignal cancellationSignal, C04920No[] r8, int i) {
        File file;
        Typeface typeface;
        String readlink;
        if (r8.length >= 1) {
            try {
                ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(A08(r8, i).A03, "r", null);
                if (openFileDescriptor != null) {
                    try {
                        StringBuilder sb = new StringBuilder();
                        sb.append("/proc/self/fd/");
                        sb.append(openFileDescriptor.getFd());
                        readlink = Os.readlink(sb.toString());
                    } catch (ErrnoException unused) {
                    }
                    if (OsConstants.S_ISREG(Os.stat(readlink).st_mode)) {
                        file = new File(readlink);
                        if (file != null || !file.canRead()) {
                            FileInputStream fileInputStream = new FileInputStream(openFileDescriptor.getFileDescriptor());
                            typeface = super.A07(context, fileInputStream);
                            fileInputStream.close();
                        } else {
                            typeface = Typeface.createFromFile(file);
                        }
                        openFileDescriptor.close();
                        return typeface;
                    }
                    file = null;
                    if (file != null) {
                    }
                    FileInputStream fileInputStream = new FileInputStream(openFileDescriptor.getFileDescriptor());
                    typeface = super.A07(context, fileInputStream);
                    fileInputStream.close();
                    openFileDescriptor.close();
                    return typeface;
                }
            } catch (IOException unused2) {
                return null;
            }
        }
        return null;
    }
}
