package X;

import android.graphics.Bitmap;
import android.net.Uri;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.3X4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X4 implements AnonymousClass23D {
    public final /* synthetic */ Uri A00;
    public final /* synthetic */ ImageComposerFragment A01;

    public AnonymousClass3X4(Uri uri, ImageComposerFragment imageComposerFragment) {
        this.A01 = imageComposerFragment;
        this.A00 = uri;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        return ((MediaComposerFragment) this.A01).A00.toString();
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        try {
            Uri uri = this.A00;
            ImageComposerFragment imageComposerFragment = this.A01;
            C15450nH r2 = ((MediaComposerFragment) imageComposerFragment).A04;
            C22190yg r1 = ((MediaComposerFragment) imageComposerFragment).A0L;
            int A02 = r2.A02(AbstractC15460nI.A1T);
            Bitmap A07 = r1.A07(uri, A02, A02);
            AnonymousClass21U r12 = imageComposerFragment.A07;
            r12.A04 = A07;
            r12.A0B = false;
            r12.A02();
            return A07;
        } catch (C39351pj | IOException | OutOfMemoryError e) {
            Log.e("ImageComposerFragment/loadbitmap", e);
            return null;
        }
    }
}
