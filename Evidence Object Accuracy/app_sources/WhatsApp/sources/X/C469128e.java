package X;

import java.util.Iterator;

/* renamed from: X.28e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C469128e extends AbstractC469228f {
    public final /* synthetic */ AbstractC469028d val$function;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C469128e(Iterator it, AbstractC469028d r2) {
        super(it);
        this.val$function = r2;
    }

    @Override // X.AbstractC469228f
    public Object transform(Object obj) {
        return this.val$function.apply(obj);
    }
}
