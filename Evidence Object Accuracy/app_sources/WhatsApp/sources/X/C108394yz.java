package X;

/* renamed from: X.4yz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C108394yz implements AnonymousClass5Sa {
    public final /* synthetic */ AbstractC95064d1 A00;

    public C108394yz(AbstractC95064d1 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5Sa
    public final void AV1(C56492ky r4) {
        if (r4.A01 == 0) {
            AbstractC95064d1 r2 = this.A00;
            r2.AG7(null, ((AbstractC77963o9) r2).A01);
            return;
        }
        AbstractC115065Qb r0 = this.A00.A0K;
        if (r0 != null) {
            ((C108384yy) r0).A00.onConnectionFailed(r4);
        }
    }
}
