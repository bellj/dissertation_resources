package X;

import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308460e {
    public int A00;
    public int A01;
    public int A02;
    public C119715ez A03;
    public C64513Fv A04;
    public ArrayList A05;
    public ArrayList A06;
    public HashMap A07;
    public final C1329668y A08;
    public final AnonymousClass6BE A09;

    public C1308460e(C1329668y r1, AnonymousClass6BE r2) {
        this.A09 = r2;
        this.A08 = r1;
        A09();
    }

    public static String A00(String str, HashMap hashMap) {
        if (hashMap.get(str) == null) {
            return null;
        }
        try {
            JSONObject jSONObject = C13000ix.A05((String) hashMap.get(str)).getJSONObject("data");
            StringBuilder A0h = C12960it.A0h();
            A0h.append(jSONObject.getString("code"));
            A0h.append(",");
            A0h.append(jSONObject.getString("ki"));
            A0h.append(",");
            return C12960it.A0d(jSONObject.getString("encryptedBase64String"), A0h);
        } catch (JSONException e) {
            StringBuilder A0k = C12960it.A0k("PAY: IndiaUPIPaymentBankSetup getEncryptedBlob read: ");
            A0k.append(str);
            Log.i(C12960it.A0d("  blob threw: ", A0k), e);
            return null;
        }
    }

    public final C119715ez A01(C119755f3 r5) {
        ArrayList<String> arrayList;
        C119715ez r0;
        Bundle bundle;
        C1329668y r2 = this.A08;
        if (r2.A0L()) {
            String A07 = r2.A07();
            if (TextUtils.isEmpty(A07)) {
                r2.AfV(r5);
                if (r5 != null) {
                    A07 = r5.A0A;
                } else {
                    A07 = r2.A07();
                }
            }
            if (!TextUtils.isEmpty(A07)) {
                return A02(A07);
            }
            return null;
        } else if (((r5 != null && (arrayList = r5.A0G) != null && !arrayList.isEmpty()) || ((r0 = this.A03) != null && (bundle = r0.A00) != null && (arrayList = bundle.getStringArrayList("pspRouting")) != null)) && !arrayList.isEmpty()) {
            return A02(arrayList.get(this.A01 % arrayList.size()));
        } else {
            Log.e("PAY: IndiaUPIPaymentSetup psps list is null or empty");
            return null;
        }
    }

    public final C119715ez A02(String str) {
        ArrayList arrayList = this.A06;
        if (arrayList == null || arrayList.size() <= 0) {
            Log.e("PAY: IndiaUPIPaymentSetup pspConfig list is null or empty");
            return null;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C119715ez r1 = (C119715ez) it.next();
            if (str.equals(r1.A09())) {
                return r1;
            }
        }
        return null;
    }

    public C127295uG A03(C64513Fv r8, ArrayList arrayList) {
        ArrayList A0l = C12960it.A0l();
        ArrayList A0l2 = C12960it.A0l();
        Iterator it = arrayList.iterator();
        C119715ez r2 = null;
        while (it.hasNext()) {
            AnonymousClass1ZP r5 = (AnonymousClass1ZP) it.next();
            if (r5 instanceof C119715ez) {
                C119715ez r52 = (C119715ez) r5;
                if (r52.A08() != null) {
                    r8.A05("upi-list-keys");
                    String A08 = r52.A08();
                    if (!TextUtils.isEmpty(A08)) {
                        this.A08.A0H(A08);
                    }
                } else if (r52.A09() != null) {
                    A0l2.add(r52);
                } else {
                    Bundle bundle = r52.A00;
                    if (!(bundle == null || bundle.getStringArrayList("pspRouting") == null)) {
                        r2 = r52;
                    }
                }
            } else if (r5 instanceof C119755f3) {
                A0l.add(r5);
            }
        }
        return new C127295uG(r2, A0l2, A0l);
    }

    public String A04(C119755f3 r4) {
        ArrayList A07 = A07(r4);
        if (A07 == null) {
            return null;
        }
        AnonymousClass009.A09("", A07);
        int size = A07.size();
        this.A00 = size;
        return (String) A07.get(this.A02 % size);
    }

    public String A05(C119755f3 r3) {
        String A07 = this.A08.A07();
        if (!TextUtils.isEmpty(A07)) {
            return A07;
        }
        C119715ez A01 = A01(r3);
        return (A01 == null || TextUtils.isEmpty(A01.A09())) ? "ICICI" : A01.A09();
    }

    public String A06(C119755f3 r3) {
        C119715ez A01 = A01(r3);
        if (A01 == null || TextUtils.isEmpty(A01.A0C())) {
            return this.A08.A06();
        }
        return A01.A0C();
    }

    public final ArrayList A07(C119755f3 r3) {
        Bundle bundle;
        ArrayList<String> stringArrayList;
        C119715ez A01 = A01(r3);
        if (A01 != null && (bundle = A01.A00) != null && (stringArrayList = bundle.getStringArrayList("smsGateways")) != null && stringArrayList.size() > 0 && !stringArrayList.isEmpty()) {
            return stringArrayList;
        }
        Log.e("PAY: IndiaUPIPaymentSetup smsGateways list is null or empty");
        return null;
    }

    public void A08() {
        int i = this.A02 + 1;
        if (i != this.A00 || this.A06.size() <= 1) {
            this.A02 = i;
            return;
        }
        this.A02 = 0;
        this.A01++;
    }

    public void A09() {
        this.A04 = new C64513Fv();
        this.A01 = 0;
        this.A02 = 0;
        this.A00 = 0;
        this.A05 = null;
        this.A06 = null;
        this.A03 = null;
        this.A07 = null;
        this.A09.reset();
    }

    public void A0A(C119715ez r3, ArrayList arrayList, ArrayList arrayList2) {
        this.A05 = arrayList;
        this.A06 = arrayList2;
        this.A03 = r3;
        Log.i(C12960it.A0b("PAY: IndiaUpiPaymentSetup setPspAndBanksList got banks: ", arrayList));
        StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiPaymentSetup setPspAndBanksList pspConfig: ");
        A0k.append(this.A06);
        C12960it.A1F(A0k);
        Log.i(C12960it.A0b("PAY: IndiaUpiPaymentSetup setPspAndBanksList pspRouting: ", r3));
    }
}
