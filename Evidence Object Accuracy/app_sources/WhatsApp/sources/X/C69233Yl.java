package X;

import android.content.Context;
import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: X.3Yl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69233Yl implements AbstractC116745Wq {
    public int A00;
    public int A01 = -1;
    public int A02;
    public long A03 = 0;
    public AnonymousClass242 A04;
    public C54422gh A05;
    public ArrayList A06;
    public final Context A07;
    public final View A08;
    public final LinearLayoutManager A09;
    public final RecyclerView A0A;
    public final AnonymousClass018 A0B;
    public final HashMap A0C = C12970iu.A11();

    public C69233Yl(Context context, View view, AnonymousClass018 r7) {
        this.A07 = context;
        this.A0B = r7;
        this.A08 = view.findViewById(R.id.sticker_picker_header);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.sticker_header_recycler);
        this.A0A = recyclerView;
        recyclerView.A0h = false;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager();
        this.A09 = linearLayoutManager;
        linearLayoutManager.A1Q(0);
        recyclerView.setLayoutManager(linearLayoutManager);
        C74533iG r0 = new C74533iG(this);
        ((AbstractC008704k) r0).A00 = false;
        recyclerView.setItemAnimator(r0);
        linearLayoutManager.A1Z(C28141Kv.A00(r7));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0064 A[LOOP:0: B:20:0x0062->B:21:0x0064, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AbstractC69213Yj[] r8) {
        /*
            r7 = this;
            java.util.ArrayList r3 = X.C12960it.A0l()
            int r6 = r8.length
            r4 = 0
            if (r6 <= 0) goto L_0x007b
            r1 = r8[r4]
            boolean r0 = r1 instanceof X.AnonymousClass35o
            if (r0 == 0) goto L_0x007b
            java.lang.String r1 = r1.getId()
            X.4b2 r0 = new X.4b2
            r0.<init>(r4, r1)
            r3.add(r0)
            r5 = 1
        L_0x001b:
            if (r5 >= r6) goto L_0x0031
            r1 = r8[r5]
            boolean r0 = r1 instanceof X.C622235r
            if (r0 == 0) goto L_0x0031
            java.lang.String r1 = r1.getId()
            X.4b2 r0 = new X.4b2
            r0.<init>(r5, r1)
            r3.add(r0)
            int r5 = r5 + 1
        L_0x0031:
            if (r5 >= r6) goto L_0x0047
            r1 = r8[r5]
            boolean r0 = r1 instanceof X.AnonymousClass35q
            if (r0 == 0) goto L_0x0047
            java.lang.String r1 = r1.getId()
            X.4b2 r0 = new X.4b2
            r0.<init>(r5, r1)
            r3.add(r0)
            int r5 = r5 + 1
        L_0x0047:
            r2 = -1
            if (r5 >= r6) goto L_0x0071
            r0 = r8[r5]
            boolean r0 = r0 instanceof X.AnonymousClass35p
            if (r0 == 0) goto L_0x0071
            X.47G r1 = new X.47G
            r1.<init>(r5)
            int r0 = r7.A02
            boolean r2 = X.C12960it.A1V(r0, r2)
            r7.A02 = r5
            r3.add(r1)
            r0 = 4
            int r5 = r5 + r0
        L_0x0062:
            if (r5 >= r6) goto L_0x007d
            r1 = r8[r5]
            X.4b2 r0 = new X.4b2
            r0.<init>(r1, r5)
            r3.add(r0)
            int r5 = r5 + 1
            goto L_0x0062
        L_0x0071:
            int r0 = r7.A02
            if (r0 == r2) goto L_0x0079
            r7.A02 = r2
            r2 = 1
            goto L_0x0062
        L_0x0079:
            r2 = 0
            goto L_0x0062
        L_0x007b:
            r5 = 0
            goto L_0x001b
        L_0x007d:
            androidx.recyclerview.widget.LinearLayoutManager r0 = r7.A09
            boolean r0 = r0.A08
            if (r0 == 0) goto L_0x0086
            java.util.Collections.reverse(r3)
        L_0x0086:
            java.util.ArrayList r0 = r7.A06
            if (r0 != 0) goto L_0x009a
            X.018 r0 = r7.A0B
            boolean r0 = X.C28141Kv.A00(r0)
            if (r0 == 0) goto L_0x0098
            int r0 = r3.size()
            int r4 = r0 + -1
        L_0x0098:
            r7.A01 = r4
        L_0x009a:
            r7.A06 = r3
            X.2gh r0 = r7.A05
            if (r0 == 0) goto L_0x00aa
            if (r2 != 0) goto L_0x00aa
            r0.A01 = r8
            r0.A00 = r3
            r0.A02()
            return
        L_0x00aa:
            X.2gh r1 = new X.2gh
            r1.<init>(r7, r3, r8)
            r7.A05 = r1
            androidx.recyclerview.widget.RecyclerView r0 = r7.A0A
            r0.setAdapter(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C69233Yl.A00(X.3Yj[]):void");
    }

    @Override // X.AbstractC116745Wq
    public View AHZ() {
        return this.A08;
    }

    @Override // X.AbstractC116745Wq
    public void ATQ(int i) {
        C74743ii r1;
        int i2 = this.A00;
        if (i != i2) {
            ArrayList arrayList = this.A06;
            int i3 = 0;
            if (arrayList != null) {
                while (i3 < arrayList.size()) {
                    if (((C93964b2) arrayList.get(i3)).A00(i2)) {
                        break;
                    }
                    i3++;
                }
            }
            i3 = -1;
            this.A00 = i;
            if (this.A05 != null) {
                ArrayList arrayList2 = this.A06;
                int i4 = 0;
                if (arrayList2 != null) {
                    while (i4 < arrayList2.size()) {
                        if (((C93964b2) arrayList2.get(i4)).A00(i)) {
                            break;
                        }
                        i4++;
                    }
                }
                i4 = -1;
                if (i4 != i3) {
                    LinearLayoutManager linearLayoutManager = this.A09;
                    int A19 = linearLayoutManager.A19();
                    int A1B = linearLayoutManager.A1B();
                    int i5 = A1B - A19;
                    if (C28141Kv.A00(this.A0B) && this.A02 != -1) {
                        View A0D = linearLayoutManager.A0D(linearLayoutManager.A06() - 1);
                        int i6 = this.A02;
                        if (A1B >= i6 || i4 >= i6) {
                            this.A0A.requestChildFocus(A0D, A0D);
                        } else {
                            this.A0A.clearChildFocus(A0D);
                        }
                    }
                    int i7 = (i5 << 1) / 5;
                    int i8 = i4 - i7;
                    if (i8 < A19) {
                        if (i8 < 0) {
                            i8 = 0;
                        }
                        r1 = new C74743ii(this.A08.getContext(), this);
                        ((AbstractC05520Pw) r1).A00 = i8;
                    } else {
                        int i9 = i4 + i7;
                        if (i9 > A1B) {
                            if (i9 >= linearLayoutManager.A07()) {
                                i9 = linearLayoutManager.A07() - 1;
                            }
                            r1 = new C74743ii(this.A08.getContext(), this);
                            ((AbstractC05520Pw) r1).A00 = i9;
                        }
                    }
                    linearLayoutManager.A0R(r1);
                }
                this.A05.A02();
            }
        }
    }

    @Override // X.AbstractC116745Wq
    public void Abx(AnonymousClass242 r4) {
        this.A04 = r4;
        if (r4 != null) {
            int A00 = r4.A00();
            if (A00 < 0) {
                C12990iw.A1R("StickerPickerHeader/setContentPicker/getCurrentPageIndex < 0", Locale.US, new Object[0]);
                A00 = 0;
            }
            ATQ(A00);
        }
    }
}
