package X;

/* renamed from: X.3xZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83643xZ extends AnonymousClass2GG {
    public final AnonymousClass018 A00;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r4.A06().equals("iw") != false) goto L_0x0013;
     */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C83643xZ(android.graphics.drawable.Drawable r3, X.AnonymousClass018 r4) {
        /*
            r2 = this;
            boolean r0 = X.C28141Kv.A00(r4)
            if (r0 == 0) goto L_0x0013
            java.lang.String r1 = r4.A06()
            java.lang.String r0 = "iw"
            boolean r1 = r1.equals(r0)
            r0 = 1
            if (r1 == 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            r2.<init>(r3, r0)
            r2.A00 = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C83643xZ.<init>(android.graphics.drawable.Drawable, X.018):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r0 != false) goto L_0x0015;
     */
    @Override // X.AnonymousClass2GG, android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r3) {
        /*
            r2 = this;
            X.018 r1 = r2.A00
            boolean r0 = X.C28141Kv.A00(r1)
            if (r0 == 0) goto L_0x0015
            java.lang.String r1 = r1.A06()
            java.lang.String r0 = "iw"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            r1 = 0
        L_0x0016:
            boolean r0 = r2.A00
            if (r0 == r1) goto L_0x001f
            r2.A00 = r1
            r2.invalidateSelf()
        L_0x001f:
            super.draw(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C83643xZ.draw(android.graphics.Canvas):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if (r0 != false) goto L_0x0015;
     */
    @Override // X.AnonymousClass2GG, android.graphics.drawable.InsetDrawable, android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getPadding(android.graphics.Rect r3) {
        /*
            r2 = this;
            X.018 r1 = r2.A00
            boolean r0 = X.C28141Kv.A00(r1)
            if (r0 == 0) goto L_0x0015
            java.lang.String r1 = r1.A06()
            java.lang.String r0 = "iw"
            boolean r0 = r1.equals(r0)
            r1 = 1
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            r1 = 0
        L_0x0016:
            boolean r0 = r2.A00
            if (r0 == r1) goto L_0x001f
            r2.A00 = r1
            r2.invalidateSelf()
        L_0x001f:
            boolean r0 = super.getPadding(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C83643xZ.getPadding(android.graphics.Rect):boolean");
    }
}
