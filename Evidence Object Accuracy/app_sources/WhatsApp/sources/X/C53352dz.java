package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.2dz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53352dz extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;
    public final int A02;
    public final int A03;
    public final Paint A04;
    public final /* synthetic */ AnonymousClass2BB A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C53352dz(Context context, AnonymousClass2BB r6, boolean z) {
        super(context, null);
        this.A05 = r6;
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        Paint A0G = C12990iw.A0G(1);
        this.A04 = A0G;
        C12980iv.A12(context, A0G, z ? R.color.wds_emerald_500 : R.color.thumbnail_selected_stroke);
        A0G.setStrokeWidth((float) context.getResources().getDimensionPixelSize(R.dimen.gallery_picker_preview_selection_border));
        C12990iw.A13(A0G);
        A0G.setAntiAlias(true);
        this.A02 = AnonymousClass00T.A00(context, z ? R.color.thumbnail_selected_background_new : R.color.thumbnail_selected_background);
        this.A03 = AnonymousClass00T.A00(context, R.color.thumbnail_selected_background_on_delete);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(this.A02);
        canvas.drawRect(0.0f, 0.0f, C12990iw.A02(this), C12990iw.A03(this), this.A04);
        if (this.A05.A0B) {
            canvas.drawColor(this.A03);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        int defaultSize = ImageView.getDefaultSize(getSuggestedMinimumWidth(), i);
        setMeasuredDimension(defaultSize, defaultSize);
    }
}
