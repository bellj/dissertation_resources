package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import com.whatsapp.Conversation;

/* renamed from: X.3Nd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC66323Nd implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ ViewGroup A02;
    public final /* synthetic */ Conversation A03;
    public final /* synthetic */ boolean A04;

    public ViewTreeObserver$OnGlobalLayoutListenerC66323Nd(View view, ViewGroup viewGroup, Conversation conversation, int i, boolean z) {
        this.A03 = conversation;
        this.A02 = viewGroup;
        this.A04 = z;
        this.A01 = view;
        this.A00 = i;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        ViewGroup viewGroup = this.A02;
        C12980iv.A1F(viewGroup, this);
        int height = viewGroup.getHeight();
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) height, 0.0f);
        translateAnimation.setDuration(250);
        if (this.A04) {
            this.A03.A1g.startAnimation(translateAnimation);
        }
        Conversation conversation = this.A03;
        if (viewGroup == conversation.A0S && conversation.A0P.getVisibility() == 0) {
            conversation.A0P.startAnimation(translateAnimation);
        }
        if (conversation.A2w.A0H) {
            conversation.A0T.startAnimation(translateAnimation);
        }
        this.A01.startAnimation(translateAnimation);
        Drawable background = conversation.A0A.getBackground();
        if (!(background instanceof AnonymousClass2Zg)) {
            AnonymousClass2Zg.A00(new AnonymousClass2Zg(background), conversation.A0A);
        }
        AnonymousClass2Zg r0 = (AnonymousClass2Zg) conversation.A0A.getBackground();
        r0.A00 = height;
        r0.invalidateSelf();
        C52612bO r2 = new C52612bO(background, this, height);
        r2.setStartTime(-1);
        r2.setDuration(250);
        r2.setAnimationListener(new C57992nx(this));
        conversation.A0A.startAnimation(r2);
    }
}
