package X;

/* renamed from: X.4WF  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4WF {
    public boolean equals(Object obj) {
        if (this != obj) {
            return obj != null && AnonymousClass4WF.class == obj.getClass();
        }
        return true;
    }

    public int hashCode() {
        return 0;
    }
}
