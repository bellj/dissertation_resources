package X;

import dalvik.system.PathClassLoader;

/* renamed from: X.48i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C866648i extends PathClassLoader {
    public C866648i(String str, ClassLoader classLoader) {
        super(str, classLoader);
    }

    @Override // java.lang.ClassLoader
    public final Class loadClass(String str, boolean z) {
        if (!str.startsWith("java.") && !str.startsWith("android.")) {
            try {
                return findClass(str);
            } catch (ClassNotFoundException unused) {
            }
        }
        return super.loadClass(str, z);
    }
}
