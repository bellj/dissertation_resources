package X;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0DW  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0DW extends AnonymousClass04v {
    public static final Rect A09 = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    public static final AbstractC11280g2 A0A = new AnonymousClass0YH();
    public int A00 = Integer.MIN_VALUE;
    public int A01 = Integer.MIN_VALUE;
    public C02740Dt A02;
    public final Rect A03 = new Rect();
    public final Rect A04 = new Rect();
    public final Rect A05 = new Rect();
    public final View A06;
    public final AccessibilityManager A07;
    public final int[] A08 = new int[2];
    public int mHoveredVirtualViewId = Integer.MIN_VALUE;

    public abstract int A07(float f, float f2);

    public void A0B(AnonymousClass04Z r1) {
    }

    public abstract void A0C(AnonymousClass04Z v, int i);

    public abstract void A0D(List list);

    public abstract boolean A0G(int i, int i2, Bundle bundle);

    public AnonymousClass0DW(View view) {
        if (view != null) {
            this.A06 = view;
            this.A07 = (AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (view.getImportantForAccessibility() == 0) {
                AnonymousClass028.A0a(view, 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }

    public static int A00(Rect rect, Rect rect2, int i) {
        int i2;
        int i3;
        if (i == 17) {
            i2 = rect.left;
            i3 = rect2.right;
        } else if (i == 33) {
            i2 = rect.top;
            i3 = rect2.bottom;
        } else if (i == 66) {
            i2 = rect2.left;
            i3 = rect.right;
        } else if (i == 130) {
            i2 = rect2.top;
            i3 = rect.bottom;
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return Math.max(0, i2 - i3);
    }

    public static int A01(Rect rect, Rect rect2, int i) {
        int i2;
        int i3;
        int i4;
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    if (i != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                }
            }
            i2 = rect.left + (rect.width() >> 1);
            i3 = rect2.left;
            i4 = rect2.width();
            return Math.abs(i2 - (i3 + (i4 >> 1)));
        }
        i2 = rect.top + (rect.height() >> 1);
        i3 = rect2.top;
        i4 = rect2.height();
        return Math.abs(i2 - (i3 + (i4 >> 1)));
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0030 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(android.graphics.Rect r3, android.graphics.Rect r4, int r5) {
        /*
            r0 = 17
            r2 = 1
            if (r5 == r0) goto L_0x0024
            r0 = 33
            if (r5 == r0) goto L_0x0019
            r0 = 66
            if (r5 == r0) goto L_0x0024
            r0 = 130(0x82, float:1.82E-43)
            if (r5 == r0) goto L_0x0019
            java.lang.String r1 = "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0019:
            int r1 = r4.right
            int r0 = r3.left
            if (r1 < r0) goto L_0x0031
            int r1 = r4.left
            int r0 = r3.right
            goto L_0x002e
        L_0x0024:
            int r1 = r4.bottom
            int r0 = r3.top
            if (r1 < r0) goto L_0x0031
            int r1 = r4.top
            int r0 = r3.bottom
        L_0x002e:
            if (r1 > r0) goto L_0x0031
            return r2
        L_0x0031:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DW.A02(android.graphics.Rect, android.graphics.Rect, int):boolean");
    }

    public static boolean A03(Rect rect, Rect rect2, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (i == 17) {
            int i6 = rect.right;
            int i7 = rect2.right;
            if (i6 <= i7 && rect.left < i7) {
                return false;
            }
            i2 = rect.left;
            i3 = rect2.left;
        } else if (i != 33) {
            if (i == 66) {
                int i8 = rect.left;
                int i9 = rect2.left;
                if (i8 >= i9 && rect.right > i9) {
                    return false;
                }
                i4 = rect.right;
                i5 = rect2.right;
            } else if (i == 130) {
                int i10 = rect.top;
                int i11 = rect2.top;
                if (i10 >= i11 && rect.bottom > i11) {
                    return false;
                }
                i4 = rect.bottom;
                i5 = rect2.bottom;
            } else {
                throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
            }
            if (i4 < i5) {
                return true;
            }
            return false;
        } else {
            int i12 = rect.bottom;
            int i13 = rect2.bottom;
            if (i12 <= i13 && rect.top < i13) {
                return false;
            }
            i2 = rect.top;
            i3 = rect2.top;
        }
        if (i2 > i3) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0022, code lost:
        if (r1 <= r0) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0025, code lost:
        if (r2 == false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0029, code lost:
        if (r8 == 17) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002d, code lost:
        if (r8 == 66) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002f, code lost:
        r2 = A00(r5, r6, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0033, code lost:
        if (r8 == 17) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0037, code lost:
        if (r8 == 33) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0039, code lost:
        if (r8 == 66) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x003d, code lost:
        if (r8 != 130) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x003f, code lost:
        r1 = r7.bottom;
        r0 = r5.bottom;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0048, code lost:
        if (r2 >= java.lang.Math.max(1, r1 - r0)) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004a, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004c, code lost:
        r1 = r7.right;
        r0 = r5.right;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0051, code lost:
        r1 = r5.top;
        r0 = r7.top;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0056, code lost:
        r1 = r5.left;
        r0 = r7.left;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0069, code lost:
        if (r1 >= r0) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x006c, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0075, code lost:
        throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0076, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A04(android.graphics.Rect r5, android.graphics.Rect r6, android.graphics.Rect r7, int r8) {
        /*
            boolean r1 = A02(r5, r6, r8)
            boolean r0 = A02(r5, r7, r8)
            r4 = 0
            if (r0 != 0) goto L_0x004b
            if (r1 == 0) goto L_0x004b
            r0 = 17
            r2 = 1
            if (r8 == r0) goto L_0x0065
            r0 = 33
            if (r8 == r0) goto L_0x0060
            r0 = 66
            if (r8 == r0) goto L_0x005b
            r0 = 130(0x82, float:1.82E-43)
            if (r8 != r0) goto L_0x0077
            int r1 = r5.bottom
            int r0 = r7.top
        L_0x0022:
            if (r1 > r0) goto L_0x006c
        L_0x0024:
            r3 = 1
            if (r2 == 0) goto L_0x0076
            r0 = 17
            if (r8 == r0) goto L_0x0076
            r1 = 66
            if (r8 == r1) goto L_0x0076
            int r2 = A00(r5, r6, r8)
            if (r8 == r0) goto L_0x0056
            r0 = 33
            if (r8 == r0) goto L_0x0051
            if (r8 == r1) goto L_0x004c
            r0 = 130(0x82, float:1.82E-43)
            if (r8 != r0) goto L_0x006e
            int r1 = r7.bottom
            int r0 = r5.bottom
        L_0x0043:
            int r1 = r1 - r0
            int r0 = java.lang.Math.max(r3, r1)
            if (r2 >= r0) goto L_0x004b
            r4 = 1
        L_0x004b:
            return r4
        L_0x004c:
            int r1 = r7.right
            int r0 = r5.right
            goto L_0x0043
        L_0x0051:
            int r1 = r5.top
            int r0 = r7.top
            goto L_0x0043
        L_0x0056:
            int r1 = r5.left
            int r0 = r7.left
            goto L_0x0043
        L_0x005b:
            int r1 = r5.right
            int r0 = r7.left
            goto L_0x0022
        L_0x0060:
            int r1 = r5.top
            int r0 = r7.bottom
            goto L_0x0069
        L_0x0065:
            int r1 = r5.left
            int r0 = r7.right
        L_0x0069:
            if (r1 < r0) goto L_0x006c
            goto L_0x0024
        L_0x006c:
            r2 = 0
            goto L_0x0024
        L_0x006e:
            java.lang.String r1 = "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        L_0x0076:
            return r3
        L_0x0077:
            java.lang.String r1 = "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DW.A04(android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, int):boolean");
    }

    @Override // X.AnonymousClass04v
    public C06070Sb A05(View view) {
        C02740Dt r0 = this.A02;
        if (r0 != null) {
            return r0;
        }
        C02740Dt r02 = new C02740Dt(this);
        this.A02 = r02;
        return r02;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r2) {
        super.A06(view, r2);
        A0B(r2);
    }

    public AnonymousClass04Z A08(int i) {
        if (i != -1) {
            return A09(i);
        }
        View view = this.A06;
        AnonymousClass04Z r5 = new AnonymousClass04Z(AccessibilityNodeInfo.obtain(view));
        AccessibilityNodeInfo accessibilityNodeInfo = r5.A02;
        view.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        ArrayList arrayList = new ArrayList();
        A0D(arrayList);
        if (accessibilityNodeInfo.getChildCount() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                accessibilityNodeInfo.addChild(view, ((Number) arrayList.get(i2)).intValue());
            }
            return r5;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    public final AnonymousClass04Z A09(int i) {
        boolean z;
        AnonymousClass04Z r5 = new AnonymousClass04Z(AccessibilityNodeInfo.obtain());
        AccessibilityNodeInfo accessibilityNodeInfo = r5.A02;
        accessibilityNodeInfo.setEnabled(true);
        accessibilityNodeInfo.setFocusable(true);
        accessibilityNodeInfo.setClassName("android.view.View");
        Rect rect = A09;
        accessibilityNodeInfo.setBoundsInParent(rect);
        accessibilityNodeInfo.setBoundsInScreen(rect);
        View view = this.A06;
        r5.A00 = -1;
        accessibilityNodeInfo.setParent(view);
        A0C(r5, i);
        if (r5.A02() == null && accessibilityNodeInfo.getContentDescription() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        Rect rect2 = this.A03;
        accessibilityNodeInfo.getBoundsInParent(rect2);
        if (!rect2.equals(rect)) {
            int actions = accessibilityNodeInfo.getActions();
            if ((actions & 64) != 0) {
                throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            } else if ((actions & 128) == 0) {
                accessibilityNodeInfo.setPackageName(view.getContext().getPackageName());
                r5.A01 = i;
                accessibilityNodeInfo.setSource(view, i);
                if (this.A00 == i) {
                    accessibilityNodeInfo.setAccessibilityFocused(true);
                    accessibilityNodeInfo.addAction(128);
                } else {
                    accessibilityNodeInfo.setAccessibilityFocused(false);
                    accessibilityNodeInfo.addAction(64);
                }
                if (this.A01 == i) {
                    z = true;
                    accessibilityNodeInfo.addAction(2);
                } else {
                    z = false;
                    if (accessibilityNodeInfo.isFocusable()) {
                        accessibilityNodeInfo.addAction(1);
                    }
                }
                accessibilityNodeInfo.setFocused(z);
                int[] iArr = this.A08;
                view.getLocationOnScreen(iArr);
                Rect rect3 = this.A04;
                accessibilityNodeInfo.getBoundsInScreen(rect3);
                if (rect3.equals(rect)) {
                    accessibilityNodeInfo.getBoundsInParent(rect3);
                    if (r5.A00 != -1) {
                        AnonymousClass04Z r11 = new AnonymousClass04Z(AccessibilityNodeInfo.obtain());
                        for (int i2 = r5.A00; i2 != -1; i2 = r11.A00) {
                            r11.A00 = -1;
                            AccessibilityNodeInfo accessibilityNodeInfo2 = r11.A02;
                            accessibilityNodeInfo2.setParent(view, -1);
                            accessibilityNodeInfo2.setBoundsInParent(rect);
                            A0C(r11, i2);
                            accessibilityNodeInfo2.getBoundsInParent(rect2);
                            rect3.offset(rect2.left, rect2.top);
                        }
                        r11.A02.recycle();
                    }
                    rect3.offset(iArr[0] - view.getScrollX(), iArr[1] - view.getScrollY());
                }
                Rect rect4 = this.A05;
                if (view.getLocalVisibleRect(rect4)) {
                    rect4.offset(iArr[0] - view.getScrollX(), iArr[1] - view.getScrollY());
                    if (rect3.intersect(rect4)) {
                        accessibilityNodeInfo.setBoundsInScreen(rect3);
                        if (!rect3.isEmpty() && view.getWindowVisibility() == 0) {
                            ViewParent parent = view.getParent();
                            while (true) {
                                if (parent instanceof View) {
                                    View view2 = (View) parent;
                                    if (view2.getAlpha() <= 0.0f || view2.getVisibility() != 0) {
                                        break;
                                    }
                                    parent = view2.getParent();
                                } else if (parent != null) {
                                    accessibilityNodeInfo.setVisibleToUser(true);
                                }
                            }
                        }
                    }
                }
                return r5;
            } else {
                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
        } else {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
    }

    public final void A0A(int i, int i2) {
        View view;
        ViewParent parent;
        if (i != Integer.MIN_VALUE && this.A07.isEnabled() && (parent = (view = this.A06).getParent()) != null) {
            AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
            if (i != -1) {
                AnonymousClass04Z A08 = A08(i);
                obtain.getText().add(A08.A02());
                AccessibilityNodeInfo accessibilityNodeInfo = A08.A02;
                obtain.setContentDescription(accessibilityNodeInfo.getContentDescription());
                obtain.setScrollable(accessibilityNodeInfo.isScrollable());
                obtain.setPassword(accessibilityNodeInfo.isPassword());
                obtain.setEnabled(accessibilityNodeInfo.isEnabled());
                obtain.setChecked(accessibilityNodeInfo.isChecked());
                if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
                    obtain.setClassName(accessibilityNodeInfo.getClassName());
                    obtain.setSource(view, i);
                    obtain.setPackageName(view.getContext().getPackageName());
                } else {
                    throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
                }
            } else {
                view.onInitializeAccessibilityEvent(obtain);
            }
            parent.requestSendAccessibilityEvent(view, obtain);
        }
    }

    public final void A0E(boolean z, int i, Rect rect) {
        int i2 = this.A01;
        if (i2 != Integer.MIN_VALUE && i2 == i2) {
            this.A01 = Integer.MIN_VALUE;
            A0A(i2, 8);
        }
        if (z) {
            A0H(i, rect);
        }
    }

    public final boolean A0F(int i) {
        int i2;
        View view = this.A06;
        if ((!view.isFocused() && !view.requestFocus()) || (i2 = this.A01) == i) {
            return false;
        }
        if (i2 != Integer.MIN_VALUE && i2 == i2) {
            this.A01 = Integer.MIN_VALUE;
            A0A(i2, 8);
        }
        this.A01 = i;
        A0A(i, 8);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x018b, code lost:
        if (r0 >= 0) goto L_0x0166;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0164, code lost:
        if (r0 < r1) goto L_0x0166;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0166, code lost:
        r9 = r3.get(r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0084  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0H(int r15, android.graphics.Rect r16) {
        /*
        // Method dump skipped, instructions count: 416
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0DW.A0H(int, android.graphics.Rect):boolean");
    }

    public final boolean A0I(KeyEvent keyEvent) {
        int i;
        int i2 = 0;
        if (keyEvent.getAction() != 1) {
            int keyCode = keyEvent.getKeyCode();
            if (keyCode != 61) {
                if (keyCode != 66) {
                    switch (keyCode) {
                        case 19:
                        case C43951xu.A01 /* 20 */:
                        case 21:
                        case 22:
                            if (keyEvent.hasNoModifiers()) {
                                if (keyCode == 19) {
                                    i = 33;
                                } else if (keyCode != 21) {
                                    i = 66;
                                    if (keyCode != 22) {
                                        i = 130;
                                    }
                                } else {
                                    i = 17;
                                }
                                int repeatCount = keyEvent.getRepeatCount() + 1;
                                boolean z = false;
                                while (i2 < repeatCount && A0H(i, null)) {
                                    i2++;
                                    z = true;
                                }
                                return z;
                            }
                            break;
                    }
                }
                if (keyEvent.hasNoModifiers() && keyEvent.getRepeatCount() == 0) {
                    int i3 = this.A01;
                    if (i3 == Integer.MIN_VALUE) {
                        return true;
                    }
                    A0G(i3, 16, null);
                    return true;
                }
            } else if (keyEvent.hasNoModifiers()) {
                return A0H(2, null);
            } else {
                if (keyEvent.hasModifiers(1)) {
                    return A0H(1, null);
                }
            }
        }
        return false;
    }

    public final boolean A0J(MotionEvent motionEvent) {
        AccessibilityManager accessibilityManager = this.A07;
        if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 7 || action == 9) {
            int A07 = A07(motionEvent.getX(), motionEvent.getY());
            updateHoveredVirtualView(A07);
            if (A07 != Integer.MIN_VALUE) {
                return true;
            }
            return false;
        } else if (action != 10 || this.mHoveredVirtualViewId == Integer.MIN_VALUE) {
            return false;
        } else {
            updateHoveredVirtualView(Integer.MIN_VALUE);
            return true;
        }
    }

    public final void updateHoveredVirtualView(int i) {
        int i2 = this.mHoveredVirtualViewId;
        if (i2 != i) {
            this.mHoveredVirtualViewId = i;
            A0A(i, 128);
            A0A(i2, 256);
        }
    }
}
