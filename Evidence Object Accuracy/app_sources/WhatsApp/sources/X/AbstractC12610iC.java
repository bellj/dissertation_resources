package X;

import android.view.Menu;
import android.view.Window;

/* renamed from: X.0iC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12610iC {
    boolean AJp();

    void setMenu(Menu menu, AbstractC12280hf v);

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
