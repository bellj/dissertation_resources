package X;

import android.graphics.Typeface;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0DF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DF extends AnonymousClass0DG {
    @Override // X.AnonymousClass0DG
    public Typeface A09(Object obj) {
        try {
            Object newInstance = Array.newInstance(((AnonymousClass0DG) this).A00, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) this.A05.invoke(null, newInstance, "sans-serif", -1, -1);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override // X.AnonymousClass0DG
    public Method A0A(Class cls) {
        Class cls2 = Integer.TYPE;
        Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass(), String.class, cls2, cls2);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }
}
