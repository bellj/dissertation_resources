package X;

import android.database.Cursor;
import java.util.HashMap;

/* renamed from: X.1sB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1sB {
    public static boolean A01(int i) {
        return i == 1 || i == 4 || i == 2 || i == 3;
    }

    public static HashMap A00(Cursor cursor, byte b) {
        String[] strArr;
        HashMap hashMap = new HashMap();
        AnonymousClass1Tx.A03(cursor, hashMap, AnonymousClass4HC.A01);
        if (b == 56) {
            strArr = AnonymousClass4HB.A01;
        } else if (b == 67) {
            strArr = AnonymousClass4HA.A01;
        } else if (b == 68) {
            strArr = AnonymousClass4H9.A01;
        } else {
            throw new IllegalArgumentException("MessageAddOnUtils/getColIndexesForMessageAddOnStatements messageAddOnType not supported");
        }
        AnonymousClass1Tx.A03(cursor, hashMap, strArr);
        return hashMap;
    }

    public static boolean A02(C27081Fy r3) {
        if (r3 == null) {
            return false;
        }
        int i = r3.A01;
        if ((i & 16) == 16 || (i & 256) == 256) {
            return true;
        }
        if ((i & 128) != 128) {
            return false;
        }
        AnonymousClass2RK r0 = r3.A0U;
        if (r0 == null) {
            r0 = AnonymousClass2RK.A05;
        }
        return (r0.A00 & 2) == 2;
    }
}
