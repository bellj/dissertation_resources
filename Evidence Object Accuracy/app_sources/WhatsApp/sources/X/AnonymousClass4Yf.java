package X;

import java.util.Map;

/* renamed from: X.4Yf  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4Yf {
    public static boolean equalsImpl(AnonymousClass5XH r1, Object obj) {
        if (obj == r1) {
            return true;
        }
        if (obj instanceof AnonymousClass5XH) {
            return r1.asMap().equals(((AnonymousClass5XH) obj).asMap());
        }
        return false;
    }

    public static AbstractC117165Yt newListMultimap(Map map, AbstractC115805Sz r2) {
        return new C80863t5(map, r2);
    }
}
