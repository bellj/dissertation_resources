package X;

import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/* renamed from: X.15O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15O {
    public static final Random A02 = new Random();
    public static volatile SecureRandom A03;
    public final C15570nT A00;
    public final C14830m7 A01;

    public AnonymousClass15O(C15570nT r1, C14830m7 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static String A00(C15570nT r0, C14830m7 r1, boolean z) {
        byte[] A01 = A01(r0, r1, z);
        if (A01 != null) {
            return C003501n.A03(A01);
        }
        throw new IllegalStateException("message id could not be created");
    }

    public static byte[] A01(C15570nT r7, C14830m7 r8, boolean z) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            long A00 = r8.A00();
            byte[] bArr = new byte[8];
            for (int i = 7; i >= 0; i--) {
                bArr[i] = (byte) ((int) A00);
                A00 >>= 8;
            }
            instance.update(bArr);
            r7.A08();
            C27631Ih r0 = r7.A05;
            AnonymousClass009.A05(r0);
            instance.update(r0.getRawString().getBytes());
            byte[] bArr2 = new byte[16];
            if (z) {
                if (A03 == null) {
                    synchronized (AnonymousClass15O.class) {
                        if (A03 == null) {
                            A03 = new SecureRandom();
                        }
                    }
                }
                A03.nextBytes(bArr2);
            } else {
                A02.nextBytes(bArr2);
            }
            instance.update(bArr2);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            Log.w("unable to provide message id hash due to missing md5 algorithm", e);
            return null;
        }
    }

    public AnonymousClass1IS A02(AbstractC14640lm r4, boolean z) {
        return new AnonymousClass1IS(r4, A00(this.A00, this.A01, false), z);
    }
}
