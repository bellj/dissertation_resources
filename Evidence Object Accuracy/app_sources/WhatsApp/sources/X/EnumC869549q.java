package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC869549q extends Enum {
    public static final /* synthetic */ EnumC869549q[] A00;
    public static final EnumC869549q A01;

    public static EnumC869549q valueOf(String str) {
        return (EnumC869549q) Enum.valueOf(EnumC869549q.class, str);
    }

    public static EnumC869549q[] values() {
        return (EnumC869549q[]) A00.clone();
    }

    static {
        EnumC869549q r3 = new EnumC869549q("GET", 0);
        A01 = r3;
        EnumC869549q[] r0 = new EnumC869549q[2];
        C72453ed.A1J(r3, new EnumC869549q("SET", 1), r0);
        A00 = r0;
    }

    public EnumC869549q(String str, int i) {
    }
}
