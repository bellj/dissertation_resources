package X;

import android.content.Context;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.TextureView;
import com.google.android.exoplayer2.Timeline;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.TimeoutException;

/* renamed from: X.2Ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47492Ax extends AbstractC47502Ay implements AnonymousClass2B0 {
    public float A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public AudioTrack A05;
    public Surface A06;
    public SurfaceHolder A07;
    public TextureView A08;
    public C100614mC A09;
    public C100614mC A0A;
    public AnonymousClass3HR A0B;
    public AnonymousClass4U3 A0C;
    public AnonymousClass4U3 A0D;
    public C92484Wc A0E;
    public List A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public boolean A0J;
    public boolean A0K;
    public final long A0L;
    public final Context A0M;
    public final C89844Lp A0N;
    public final AnonymousClass3FG A0O;
    public final C76513ll A0P;
    public final SurfaceHolder$CallbackC67643Sh A0Q;
    public final AnonymousClass3HW A0R;
    public final AnonymousClass4IC A0S;
    public final AnonymousClass4ID A0T;
    public final C106424vg A0U;
    public final CopyOnWriteArraySet A0V;
    public final CopyOnWriteArraySet A0W;
    public final CopyOnWriteArraySet A0X;
    public final CopyOnWriteArraySet A0Y;
    public final CopyOnWriteArraySet A0Z;
    public final AbstractC117055Yb[] A0a;

    @Deprecated
    public C47492Ax(Context context, Looper looper, AnonymousClass5Pv r25, AnonymousClass5SH r26, C106424vg r27, AnonymousClass5QA r28, AnonymousClass4M7 r29, AnonymousClass5QN r30, AnonymousClass5Xd r31) {
        int A00;
        C106874wQ r3 = new C106874wQ();
        new C77353n9(context);
        new C107574xY(r3, new C107874y5(context));
        new C106404ve();
        C67733Sr.A00(context);
        new C106424vg(AnonymousClass5Xd.A00);
        if (Looper.myLooper() == null) {
            Looper.getMainLooper();
        }
        AnonymousClass3HR r5 = AnonymousClass3HR.A03;
        C94224bS r12 = C94224bS.A02;
        C89854Lq r0 = new C89854Lq();
        C106394vd r9 = new C106394vd(r0.A00, r0.A01);
        Context applicationContext = context.getApplicationContext();
        this.A0M = applicationContext;
        this.A0U = r27;
        this.A0B = r5;
        this.A04 = 1;
        this.A0J = false;
        this.A0L = 2000;
        SurfaceHolder$CallbackC67643Sh r1 = new SurfaceHolder$CallbackC67643Sh(this);
        this.A0Q = r1;
        this.A0Z = new CopyOnWriteArraySet();
        this.A0V = new CopyOnWriteArraySet();
        this.A0Y = new CopyOnWriteArraySet();
        this.A0X = new CopyOnWriteArraySet();
        this.A0W = new CopyOnWriteArraySet();
        Handler handler = new Handler(looper);
        AbstractC117055Yb[] A8T = r26.A8T(handler, r1, r1, r1, r1);
        this.A0a = A8T;
        this.A00 = 1.0f;
        if (AnonymousClass3JZ.A01 < 21) {
            AudioTrack audioTrack = this.A05;
            if (!(audioTrack == null || audioTrack.getAudioSessionId() == 0)) {
                this.A05.release();
                this.A05 = null;
            }
            AudioTrack audioTrack2 = this.A05;
            if (audioTrack2 == null) {
                audioTrack2 = new AudioTrack(3, 4000, 4, 2, 2, 0, 0);
                this.A05 = audioTrack2;
            }
            A00 = audioTrack2.getAudioSessionId();
        } else {
            A00 = C95214dK.A00(applicationContext);
        }
        this.A01 = A00;
        this.A0F = Collections.emptyList();
        this.A0K = true;
        C76513ll r7 = new C76513ll(looper, r9, r25, this, r12, r27, r28, r29, r30, r31, A8T);
        this.A0P = r7;
        r7.A5h(r1);
        this.A0N = new C89844Lp(context, handler, r1);
        this.A0O = new AnonymousClass3FG(context, handler, r1);
        AnonymousClass3HW r4 = new AnonymousClass3HW(context, handler, r1);
        this.A0R = r4;
        r4.A03(AnonymousClass3JZ.A02(this.A0B.A02));
        this.A0S = new AnonymousClass4IC(context);
        this.A0T = new AnonymousClass4ID(context);
        this.A0E = new C92484Wc(r4.A01(), r4.A05.getStreamMaxVolume(r4.A00));
        A09(Integer.valueOf(this.A01), 1, 102);
        A09(Integer.valueOf(this.A01), 2, 102);
        A09(this.A0B, 1, 3);
        A09(Integer.valueOf(this.A04), 2, 4);
        A09(Boolean.valueOf(this.A0J), 1, 101);
    }

    public static /* synthetic */ void A00(C47492Ax r2) {
        int AFl = r2.AFl();
        if (AFl == 1) {
            return;
        }
        if (AFl == 2 || AFl == 3) {
            r2.A03();
            r2.A03();
            r2.A03();
        } else if (AFl != 4) {
            throw new IllegalStateException();
        }
    }

    public void A01() {
        String str;
        boolean z;
        AudioTrack audioTrack;
        A03();
        if (AnonymousClass3JZ.A01 < 21 && (audioTrack = this.A05) != null) {
            audioTrack.release();
            this.A05 = null;
        }
        AnonymousClass3HW r3 = this.A0R;
        C72893fL r1 = r3.A02;
        if (r1 != null) {
            try {
                r3.A04.unregisterReceiver(r1);
            } catch (RuntimeException e) {
                C64923Hl.A02("StreamVolumeManager", "Error unregistering stream volume receiver", e);
            }
            r3.A02 = null;
        }
        AnonymousClass3FG r0 = this.A0O;
        r0.A02 = null;
        r0.A00();
        C76513ll r32 = this.A0P;
        StringBuilder sb = new StringBuilder("Release ");
        sb.append(Integer.toHexString(System.identityHashCode(r32)));
        sb.append(" [");
        sb.append("ExoPlayerLib/2.13.3");
        sb.append("] [");
        sb.append(AnonymousClass3JZ.A03);
        sb.append("] [");
        synchronized (AnonymousClass4ZR.class) {
            str = AnonymousClass4ZR.A00;
        }
        sb.append(str);
        sb.append("]");
        Log.i("ExoPlayerImpl", sb.toString());
        C107544xV r4 = r32.A0B;
        synchronized (r4) {
            if (r4.A0F || !r4.A0L.isAlive()) {
                z = true;
            } else {
                ((C107914yA) r4.A0Z).A00.sendEmptyMessage(7);
                AnonymousClass51Q r9 = new AbstractC115805Sz() { // from class: X.51Q
                    @Override // X.AbstractC115805Sz
                    public final Object get() {
                        return Boolean.valueOf(C107544xV.this.A0F);
                    }
                };
                long j = r4.A0K;
                synchronized (r4) {
                    long elapsedRealtime = SystemClock.elapsedRealtime() + j;
                    boolean z2 = false;
                    while (!((Boolean) r9.get()).booleanValue() && j > 0) {
                        try {
                            r4.wait(j);
                        } catch (InterruptedException unused) {
                            z2 = true;
                        }
                        j = elapsedRealtime - SystemClock.elapsedRealtime();
                    }
                    if (z2) {
                        Thread.currentThread().interrupt();
                    }
                    z = r4.A0F;
                }
            }
        }
        if (!z) {
            C92874Xt r2 = r32.A0K;
            r2.A02(new AnonymousClass5SU() { // from class: X.4yB
                @Override // X.AnonymousClass5SU
                public final void AJ7(Object obj) {
                    ((AnonymousClass5XZ) obj).ATs(new AnonymousClass3A1(null, null, new C87524Bv(1), 1, -1, 4, false));
                }
            }, 11);
            r2.A00();
        }
        r32.A0K.A01();
        ((C107914yA) r32.A0J).A00.removeCallbacksAndMessages(null);
        C106424vg r12 = r32.A0D;
        if (r12 != null) {
            ((C67733Sr) r32.A0H).A09.A00(r12);
        }
        C95034cy A01 = r32.A05.A01(1);
        r32.A05 = A01;
        C95034cy A06 = A01.A06(A01.A07);
        r32.A05 = A06;
        A06.A0F = A06.A0G;
        r32.A05.A0H = 0;
        C106424vg r22 = this.A0U;
        AnonymousClass4XT A03 = r22.A03(r22.A06.A00);
        r22.A03.put(1036, A03);
        ((C107914yA) r22.A01.A02).A00.obtainMessage(1, 1036, 0, new AnonymousClass5SU() { // from class: X.4yF
            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }).sendToTarget();
        A02();
        Surface surface = this.A06;
        if (surface != null) {
            if (this.A0H) {
                surface.release();
            }
            this.A06 = null;
        }
        this.A0F = Collections.emptyList();
        this.A0I = true;
    }

    public final void A02() {
        TextureView textureView = this.A08;
        if (textureView != null) {
            if (textureView.getSurfaceTextureListener() != this.A0Q) {
                Log.w("SimpleExoPlayer", "SurfaceTextureListener already unset or replaced.");
            } else {
                this.A08.setSurfaceTextureListener(null);
            }
            this.A08 = null;
        }
        SurfaceHolder surfaceHolder = this.A07;
        if (surfaceHolder != null) {
            surfaceHolder.removeCallback(this.A0Q);
            this.A07 = null;
        }
    }

    public final void A03() {
        IllegalStateException illegalStateException;
        if (Looper.myLooper() == this.A0P.A09) {
            return;
        }
        if (!this.A0K) {
            if (this.A0G) {
                illegalStateException = null;
            } else {
                illegalStateException = new IllegalStateException();
            }
            C64923Hl.A02("SimpleExoPlayer", "Player is accessed on the wrong thread. See https://exoplayer.dev/issues/player-accessed-on-wrong-thread", illegalStateException);
            this.A0G = true;
            return;
        }
        throw new IllegalStateException("Player is accessed on the wrong thread. See https://exoplayer.dev/issues/player-accessed-on-wrong-thread");
    }

    public void A04(float f) {
        A03();
        float max = Math.max(0.0f, Math.min(f, 1.0f));
        if (this.A00 != max) {
            this.A00 = max;
            A09(Float.valueOf(max * this.A0O.A00), 1, 2);
            C106424vg r3 = this.A0U;
            AnonymousClass4XT A03 = r3.A03(r3.A06.A02);
            r3.A05(A03, new AnonymousClass5SU() { // from class: X.4yH
                @Override // X.AnonymousClass5SU
                public final void AJ7(Object obj) {
                }
            }, 1019);
            Iterator it = this.A0V.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NullPointerException("onVolumeChanged");
            }
        }
    }

    public final void A05(int i, int i2) {
        if (i != this.A03 || i2 != this.A02) {
            this.A03 = i;
            this.A02 = i2;
            C106424vg r3 = this.A0U;
            AnonymousClass4XT A03 = r3.A03(r3.A06.A02);
            r3.A05(A03, new AnonymousClass5SU() { // from class: X.4yE
                @Override // X.AnonymousClass5SU
                public final void AJ7(Object obj) {
                }
            }, 1029);
            Iterator it = this.A0Z.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }

    public final void A06(int i, int i2, boolean z) {
        boolean z2;
        int i3 = 0;
        if (!z || i == -1) {
            z2 = false;
        } else {
            z2 = true;
            if (i != 1) {
                i3 = 1;
            }
        }
        this.A0P.A04(i3, i2, z2);
    }

    public final void A07(Surface surface, boolean z) {
        ArrayList arrayList = new ArrayList();
        AbstractC117055Yb[] r7 = this.A0a;
        for (AbstractC117055Yb r15 : r7) {
            if (((AbstractC106444vi) r15).A09 == 2) {
                C76513ll r8 = this.A0P;
                C107544xV r14 = r8.A0B;
                Timeline timeline = r8.A05.A05;
                r8.ACF();
                C64193Ep r12 = new C64193Ep(r14.A0M, r14, r15, timeline, r8.A0I);
                boolean z2 = !r12.A05;
                C95314dV.A04(z2);
                r12.A00 = 1;
                C95314dV.A04(z2);
                r12.A02 = surface;
                r12.A00();
                arrayList.add(r12);
            }
        }
        Surface surface2 = this.A06;
        if (!(surface2 == null || surface2 == surface)) {
            try {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    C64193Ep r72 = (C64193Ep) it.next();
                    long j = this.A0L;
                    synchronized (r72) {
                        C95314dV.A04(r72.A05);
                        boolean z3 = false;
                        if (r72.A01.getThread() != Thread.currentThread()) {
                            z3 = true;
                        }
                        C95314dV.A04(z3);
                        long elapsedRealtime = SystemClock.elapsedRealtime() + j;
                        while (!r72.A04) {
                            if (j > 0) {
                                r72.wait(j);
                                j = elapsedRealtime - SystemClock.elapsedRealtime();
                            } else {
                                throw new TimeoutException("Message delivery timed out.");
                            }
                        }
                    }
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            } catch (TimeoutException unused2) {
                this.A0P.A05(new AnonymousClass3A1(null, null, new C87524Bv(3), 1, -1, 4, false), false);
            }
            if (this.A0H) {
                this.A06.release();
            }
        }
        this.A06 = surface;
        this.A0H = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b0, code lost:
        if (r7 >= r11.A01) goto L_0x00b2;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(X.AnonymousClass2CD r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 295
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47492Ax.A08(X.2CD, boolean):void");
    }

    public final void A09(Object obj, int i, int i2) {
        AbstractC117055Yb[] r3 = this.A0a;
        for (AbstractC117055Yb r7 : r3) {
            if (((AbstractC106444vi) r7).A09 == i) {
                C76513ll r4 = this.A0P;
                C107544xV r6 = r4.A0B;
                Timeline timeline = r4.A05.A05;
                r4.ACF();
                C64193Ep r42 = new C64193Ep(r6.A0M, r6, r7, timeline, r4.A0I);
                boolean z = !r42.A05;
                C95314dV.A04(z);
                r42.A00 = i2;
                C95314dV.A04(z);
                r42.A02 = obj;
                r42.A00();
            }
        }
    }

    public void A0A(boolean z) {
        A03();
        AnonymousClass3FG r0 = this.A0O;
        A03();
        C76513ll r1 = this.A0P;
        r0.A00();
        r1.A05(null, z);
        this.A0F = Collections.emptyList();
    }

    @Override // X.AbstractC47512Az
    public void A5h(AnonymousClass5XZ r2) {
        this.A0P.A5h(r2);
    }

    @Override // X.AbstractC47512Az
    public long AB1() {
        A03();
        return this.A0P.AB1();
    }

    @Override // X.AbstractC47512Az
    public long ABh() {
        A03();
        return this.A0P.ABh();
    }

    @Override // X.AbstractC47512Az
    public int AC2() {
        A03();
        return this.A0P.AC2();
    }

    @Override // X.AbstractC47512Az
    public int AC3() {
        A03();
        return this.A0P.AC3();
    }

    @Override // X.AbstractC47512Az
    public int AC8() {
        A03();
        return this.A0P.AC8();
    }

    @Override // X.AbstractC47512Az
    public long AC9() {
        A03();
        return this.A0P.AC9();
    }

    @Override // X.AbstractC47512Az
    public Timeline ACE() {
        A03();
        return this.A0P.A05.A05;
    }

    @Override // X.AbstractC47512Az
    public int ACF() {
        A03();
        return this.A0P.ACF();
    }

    @Override // X.AbstractC47512Az
    public long ACb() {
        A03();
        return this.A0P.ACb();
    }

    @Override // X.AbstractC47512Az
    public boolean AFj() {
        A03();
        return this.A0P.A05.A0D;
    }

    @Override // X.AbstractC47512Az
    public int AFl() {
        A03();
        return this.A0P.A05.A00;
    }

    @Override // X.AbstractC47512Az
    public long AHF() {
        A03();
        return this.A0P.AHF();
    }

    @Override // X.AbstractC47512Az
    public boolean AJt() {
        A03();
        return this.A0P.AJt();
    }

    @Override // X.AbstractC47512Az
    public void AaK(AnonymousClass5XZ r2) {
        this.A0P.AaK(r2);
    }

    @Override // X.AbstractC47512Az
    public void AbS(int i, long j) {
        A03();
        C106424vg r3 = this.A0U;
        if (!r3.A02) {
            AnonymousClass4XT A03 = r3.A03(r3.A06.A00);
            r3.A02 = true;
            r3.A05(A03, new AnonymousClass5SU() { // from class: X.4yG
                @Override // X.AnonymousClass5SU
                public final void AJ7(Object obj) {
                }
            }, -1);
        }
        this.A0P.AbS(i, j);
    }

    @Override // X.AbstractC47512Az
    public void AcW(boolean z) {
        A03();
        AnonymousClass3FG r0 = this.A0O;
        A03();
        int i = -1;
        r0.A00();
        if (z) {
            i = 1;
        }
        int i2 = 1;
        if (z && i != 1) {
            i2 = 2;
        }
        A06(i, i2, z);
    }
}
