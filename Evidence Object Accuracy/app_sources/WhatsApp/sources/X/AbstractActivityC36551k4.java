package X;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.IDxObserverShape3S0100000_1_I1;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.whatsapp.ContentDistributionRecipientsPickerActivity$DiscardChangesConfirmationDialogFragment;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.blocklist.UnblockDialogFragment;
import com.whatsapp.group.GroupAddBlacklistPickerActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.lastseen.LastSeenBlockListPickerActivity;
import com.whatsapp.profile.ProfilePhotoBlockListPickerActivity;
import com.whatsapp.status.AboutStatusBlockListPickerActivity;
import com.whatsapp.status.StatusRecipientsActivity;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape13S0100000_I0;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.1k4 */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC36551k4 extends AbstractActivityC36561k5 {
    public MenuItem A00;
    public MenuItem A01;
    public View A02;
    public AnonymousClass37A A03;
    public C623336s A04;
    public C48232Fc A05;
    public C238013b A06;
    public C22330yu A07;
    public AnonymousClass116 A08;
    public C15550nR A09;
    public AnonymousClass10S A0A;
    public C15610nY A0B;
    public AnonymousClass1J1 A0C;
    public C21270x9 A0D;
    public C244215l A0E;
    public AnonymousClass12F A0F;
    public AnonymousClass12U A0G;
    public String A0H;
    public ArrayList A0I;
    public List A0J = new ArrayList();
    public Set A0K = new HashSet();
    public boolean A0L = true;
    public final Handler A0M;
    public final C36571k6 A0N = new C36571k6(this);
    public final AnonymousClass2Dn A0O;
    public final C27131Gd A0P;
    public final AbstractC33331dp A0Q;
    public final Runnable A0R;
    public final Set A0S = new HashSet();
    public final Set A0T;
    public final Set A0U = new HashSet();

    public AbstractActivityC36551k4() {
        HashSet hashSet = new HashSet();
        this.A0T = hashSet;
        this.A0R = new RunnableBRunnable0Shape0S0100000_I0(hashSet, 21);
        this.A0M = new Handler(Looper.getMainLooper());
        this.A0P = new C36541k3(this);
        this.A0O = new AnonymousClass41N(this);
        this.A0Q = new AnonymousClass44R(this);
    }

    public static /* synthetic */ void A02(AbstractActivityC36551k4 r3) {
        AnonymousClass37A r1 = r3.A03;
        if (r1 != null) {
            r1.A03(true);
            r3.A03 = null;
        }
        AnonymousClass37A r2 = new AnonymousClass37A(r3, r3.A0I, r3.A0J);
        r3.A03 = r2;
        ((ActivityC13830kP) r3).A05.Aaz(r2, new Void[0]);
    }

    public void A2g() {
        A2i();
        ListView listView = (ListView) findViewById(16908298);
        View view = new View(this);
        view.setLayoutParams(new AbsListView.LayoutParams(1, getResources().getDimensionPixelSize(R.dimen.actionbar_height)));
        listView.addFooterView(view, null, false);
        invalidateOptionsMenu();
        listView.setAdapter((ListAdapter) this.A0N);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: X.3O6
            @Override // android.widget.AdapterView.OnItemClickListener
            public final void onItemClick(AdapterView adapterView, View view2, int i, long j) {
                SearchView searchView;
                AbstractActivityC36551k4 r4 = AbstractActivityC36551k4.this;
                if (view2.getTag() instanceof AnonymousClass4RH) {
                    UserJid userJid = ((AnonymousClass4RH) view2.getTag()).A03;
                    if (!r4.A06.A0I(userJid)) {
                        Set set = r4.A0U;
                        if (set.contains(userJid)) {
                            set.remove(userJid);
                        } else {
                            set.add(userJid);
                        }
                        if (!TextUtils.isEmpty(r4.A0H) && set.contains(userJid) && (searchView = r4.A05.A02) != null) {
                            EditText editText = (EditText) searchView.findViewById(R.id.search_src_text);
                            editText.setSelection(0, editText.length());
                        }
                        r4.A0T.add(userJid);
                        Handler handler = r4.A0M;
                        Runnable runnable = r4.A0R;
                        handler.removeCallbacks(runnable);
                        handler.postDelayed(runnable, 200);
                        r4.A2h();
                        r4.A0N.notifyDataSetChanged();
                    } else if (r4 instanceof StatusRecipientsActivity) {
                        C15370n3 A0B = r4.A09.A0B(userJid);
                        C12960it.A16(UnblockDialogFragment.A00(
                        /*  JADX ERROR: Method code generation error
                            jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0042: INVOKE  
                              (wrap: com.whatsapp.blocklist.UnblockDialogFragment : 0x003e: INVOKE  (r0v21 com.whatsapp.blocklist.UnblockDialogFragment A[REMOVE]) = 
                              (wrap: X.3Vd : 0x003b: CONSTRUCTOR  (r0v20 X.3Vd A[REMOVE]) = (r4v0 'r4' X.1k4), (r5v0 'A0B' X.0n3) call: X.3Vd.<init>(X.1k4, X.0n3):void type: CONSTRUCTOR)
                              (wrap: java.lang.String : 0x0032: INVOKE  (r2v4 java.lang.String A[REMOVE]) = 
                              (r4v0 'r4' X.1k4)
                              (wrap: java.lang.String : 0x002d: INVOKE  (r0v19 java.lang.String A[REMOVE]) = (wrap: X.0nY : 0x002b: IGET  (r0v18 X.0nY A[REMOVE]) = (r4v0 'r4' X.1k4) X.1k4.A0B X.0nY), (r5v0 'A0B' X.0n3) type: VIRTUAL call: X.0nY.A04(X.0n3):java.lang.String)
                              (wrap: java.lang.Object[] : 0x0027: INVOKE  (r1v3 java.lang.Object[] A[REMOVE]) =  type: STATIC call: X.0iu.A1b():java.lang.Object[])
                              (0 int)
                              (wrap: ?? : ?: SGET   com.whatsapp.R.string.unblock_before_status int)
                             type: STATIC call: X.0it.A0X(android.content.Context, java.lang.Object, java.lang.Object[], int, int):java.lang.String)
                              (wrap: ?? : ?: SGET   com.whatsapp.R.string.blocked_title int)
                              false
                             type: STATIC call: com.whatsapp.blocklist.UnblockDialogFragment.A00(X.5TY, java.lang.String, int, boolean):com.whatsapp.blocklist.UnblockDialogFragment)
                              (r4v0 'r4' X.1k4)
                             type: STATIC call: X.0it.A16(androidx.fragment.app.DialogFragment, X.00k):void in method: X.3O6.onItemClick(android.widget.AdapterView, android.view.View, int, long):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                            	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.codegen.RegionGen.connectElseIf(RegionGen.java:170)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:147)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                            	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                            	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.dex.regions.Region.generate(Region.java:35)
                            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                            	at java.util.ArrayList.forEach(ArrayList.java:1259)
                            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003e: INVOKE  (r0v21 com.whatsapp.blocklist.UnblockDialogFragment A[REMOVE]) = 
                              (wrap: X.3Vd : 0x003b: CONSTRUCTOR  (r0v20 X.3Vd A[REMOVE]) = (r4v0 'r4' X.1k4), (r5v0 'A0B' X.0n3) call: X.3Vd.<init>(X.1k4, X.0n3):void type: CONSTRUCTOR)
                              (wrap: java.lang.String : 0x0032: INVOKE  (r2v4 java.lang.String A[REMOVE]) = 
                              (r4v0 'r4' X.1k4)
                              (wrap: java.lang.String : 0x002d: INVOKE  (r0v19 java.lang.String A[REMOVE]) = (wrap: X.0nY : 0x002b: IGET  (r0v18 X.0nY A[REMOVE]) = (r4v0 'r4' X.1k4) X.1k4.A0B X.0nY), (r5v0 'A0B' X.0n3) type: VIRTUAL call: X.0nY.A04(X.0n3):java.lang.String)
                              (wrap: java.lang.Object[] : 0x0027: INVOKE  (r1v3 java.lang.Object[] A[REMOVE]) =  type: STATIC call: X.0iu.A1b():java.lang.Object[])
                              (0 int)
                              (wrap: ?? : ?: SGET   com.whatsapp.R.string.unblock_before_status int)
                             type: STATIC call: X.0it.A0X(android.content.Context, java.lang.Object, java.lang.Object[], int, int):java.lang.String)
                              (wrap: ?? : ?: SGET   com.whatsapp.R.string.blocked_title int)
                              false
                             type: STATIC call: com.whatsapp.blocklist.UnblockDialogFragment.A00(X.5TY, java.lang.String, int, boolean):com.whatsapp.blocklist.UnblockDialogFragment in method: X.3O6.onItemClick(android.widget.AdapterView, android.view.View, int, long):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                            	... 29 more
                            Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x003b: CONSTRUCTOR  (r0v20 X.3Vd A[REMOVE]) = (r4v0 'r4' X.1k4), (r5v0 'A0B' X.0n3) call: X.3Vd.<init>(X.1k4, X.0n3):void type: CONSTRUCTOR in method: X.3O6.onItemClick(android.widget.AdapterView, android.view.View, int, long):void, file: classes2.dex
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 35 more
                            Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.3Vd, state: NOT_LOADED
                            	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                            	... 41 more
                            */
                        /*
                            this = this;
                            X.1k4 r4 = X.AbstractActivityC36551k4.this
                            java.lang.Object r0 = r8.getTag()
                            boolean r0 = r0 instanceof X.AnonymousClass4RH
                            if (r0 == 0) goto L_0x0045
                            java.lang.Object r0 = r8.getTag()
                            X.4RH r0 = (X.AnonymousClass4RH) r0
                            com.whatsapp.jid.UserJid r3 = r0.A03
                            X.13b r0 = r4.A06
                            boolean r0 = r0.A0I(r3)
                            if (r0 == 0) goto L_0x0046
                            boolean r0 = r4 instanceof com.whatsapp.status.StatusRecipientsActivity
                            if (r0 == 0) goto L_0x0045
                            r2 = 2131892432(0x7f1218d0, float:1.9419612E38)
                            X.0nR r0 = r4.A09
                            X.0n3 r5 = r0.A0B(r3)
                            java.lang.Object[] r1 = X.C12970iu.A1b()
                            X.0nY r0 = r4.A0B
                            java.lang.String r0 = r0.A04(r5)
                            r3 = 0
                            java.lang.String r2 = X.C12960it.A0X(r4, r0, r1, r3, r2)
                            r1 = 2131886624(0x7f120220, float:1.9407832E38)
                            X.3Vd r0 = new X.3Vd
                            r0.<init>(r4, r5)
                            com.whatsapp.blocklist.UnblockDialogFragment r0 = com.whatsapp.blocklist.UnblockDialogFragment.A00(r0, r2, r1, r3)
                            X.C12960it.A16(r0, r4)
                        L_0x0045:
                            return
                        L_0x0046:
                            java.util.Set r1 = r4.A0U
                            boolean r0 = r1.contains(r3)
                            if (r0 == 0) goto L_0x0090
                            r1.remove(r3)
                        L_0x0051:
                            java.lang.String r0 = r4.A0H
                            boolean r0 = android.text.TextUtils.isEmpty(r0)
                            if (r0 != 0) goto L_0x0076
                            boolean r0 = r1.contains(r3)
                            if (r0 == 0) goto L_0x0076
                            X.2Fc r0 = r4.A05
                            androidx.appcompat.widget.SearchView r1 = r0.A02
                            if (r1 == 0) goto L_0x0076
                            r0 = 2131365738(0x7f0a0f6a, float:1.835135E38)
                            android.view.View r2 = r1.findViewById(r0)
                            android.widget.EditText r2 = (android.widget.EditText) r2
                            r1 = 0
                            int r0 = r2.length()
                            r2.setSelection(r1, r0)
                        L_0x0076:
                            java.util.Set r0 = r4.A0T
                            r0.add(r3)
                            android.os.Handler r3 = r4.A0M
                            java.lang.Runnable r2 = r4.A0R
                            r3.removeCallbacks(r2)
                            r0 = 200(0xc8, double:9.9E-322)
                            r3.postDelayed(r2, r0)
                            r4.A2h()
                            X.1k6 r0 = r4.A0N
                            r0.notifyDataSetChanged()
                            return
                        L_0x0090:
                            r1.add(r3)
                            goto L_0x0051
                        */
                        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3O6.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
                    }
                });
                A2h();
            }

            public void A2h() {
                AnonymousClass018 r5;
                int i;
                String A0I;
                int i2;
                boolean z = this.A0L;
                Set set = this.A0U;
                boolean isEmpty = set.isEmpty();
                if (z) {
                    if (isEmpty) {
                        i2 = R.string.no_contacts_excluded;
                        A0I = getString(i2);
                    } else {
                        r5 = ((ActivityC13830kP) this).A01;
                        i = R.plurals.status_contacts_excluded;
                        A0I = r5.A0I(new Object[]{Integer.valueOf(set.size())}, i, (long) set.size());
                    }
                } else if (isEmpty) {
                    i2 = R.string.no_contacts_selected;
                    A0I = getString(i2);
                } else {
                    r5 = ((ActivityC13830kP) this).A01;
                    i = R.plurals.status_contacts_selected;
                    A0I = r5.A0I(new Object[]{Integer.valueOf(set.size())}, i, (long) set.size());
                }
                MenuItem menuItem = this.A01;
                if (menuItem != null) {
                    int size = set.size();
                    int size2 = this.A0K.size();
                    int i3 = R.string.select_all;
                    if (size == size2) {
                        i3 = R.string.unselect_all;
                    }
                    menuItem.setTitle(i3);
                }
                AbstractC005102i A1U = A1U();
                AnonymousClass009.A05(A1U);
                A1U.A0H(A0I);
            }

            public final void A2i() {
                C623336s r0 = this.A04;
                if (r0 != null) {
                    r0.A03(true);
                }
                AnonymousClass37A r02 = this.A03;
                if (r02 != null) {
                    r02.A03(true);
                    this.A03 = null;
                }
                C623336s r2 = new C623336s(this, this.A0U);
                this.A04 = r2;
                ((ActivityC13830kP) this).A05.Aaz(r2, new Void[0]);
            }

            @Override // X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
            public void onActivityResult(int i, int i2, Intent intent) {
                if (i != 150) {
                    super.onActivityResult(i, i2, intent);
                } else if (i2 != -1) {
                    Log.i("statusrecipients/permissions denied");
                    finish();
                }
            }

            @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
            public void onBackPressed() {
                if (this.A05.A05()) {
                    this.A05.A04(true);
                    return;
                }
                Set set = this.A0S;
                Set set2 = this.A0U;
                if (!set.containsAll(set2) || !set2.containsAll(set)) {
                    Adm(new ContentDistributionRecipientsPickerActivity$DiscardChangesConfirmationDialogFragment());
                } else {
                    finish();
                }
            }

            @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            public void onCreate(Bundle bundle) {
                int i;
                if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().addFlags(Integer.MIN_VALUE);
                }
                super.onCreate(bundle);
                setContentView(R.layout.status_contact_picker);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                A1e(toolbar);
                this.A0C = this.A0D.A04(this, "content-distribution-recipients-picker");
                this.A05 = new C48232Fc(this, findViewById(R.id.search_holder), new C66723Or(this), toolbar, ((ActivityC13830kP) this).A01);
                this.A0L = getIntent().getBooleanExtra("is_black_list", true);
                AbstractC005102i A1U = A1U();
                AnonymousClass009.A05(A1U);
                A1U.A0M(true);
                boolean z = this instanceof StatusRecipientsActivity;
                if (this.A0L) {
                    if (z) {
                        i = R.string.status_recipients_black_list;
                    } else if (this instanceof AboutStatusBlockListPickerActivity) {
                        i = R.string.select_about_recipients_block_list;
                    } else if (this instanceof ProfilePhotoBlockListPickerActivity) {
                        i = R.string.select_profile_photo_recipients_block_list;
                    } else if (!(this instanceof LastSeenBlockListPickerActivity)) {
                        i = R.string.group_add_permission_blacklist;
                    } else {
                        i = R.string.select_last_seen_recipients_block_list;
                    }
                } else if (!z) {
                    i = 0;
                } else {
                    i = R.string.status_recipients_white_list;
                }
                A1U.A0A(i);
                if (bundle != null) {
                    List A07 = C15380n4.A07(UserJid.class, bundle.getStringArrayList("selected_jids"));
                    if (!A07.isEmpty()) {
                        this.A0U.addAll(A07);
                    }
                } else if (!this.A08.A00()) {
                    RequestPermissionActivity.A0D(this, R.string.permission_contacts_access_on_new_broadcast_request, R.string.permission_contacts_access_on_new_broadcast);
                }
                View findViewById = findViewById(R.id.done);
                this.A02 = findViewById;
                findViewById.setOnClickListener(new ViewOnClickCListenerShape13S0100000_I0(this, 0));
                if (this instanceof AboutStatusBlockListPickerActivity) {
                    AboutStatusBlockListPickerActivity aboutStatusBlockListPickerActivity = (AboutStatusBlockListPickerActivity) this;
                    aboutStatusBlockListPickerActivity.A00.A00().A05(aboutStatusBlockListPickerActivity, new IDxObserverShape3S0100000_1_I1(aboutStatusBlockListPickerActivity, 92));
                } else if (this instanceof ProfilePhotoBlockListPickerActivity) {
                    ProfilePhotoBlockListPickerActivity profilePhotoBlockListPickerActivity = (ProfilePhotoBlockListPickerActivity) this;
                    profilePhotoBlockListPickerActivity.A00.A00().A05(profilePhotoBlockListPickerActivity, new IDxObserverShape3S0100000_1_I1(profilePhotoBlockListPickerActivity, 86));
                } else if (this instanceof LastSeenBlockListPickerActivity) {
                    LastSeenBlockListPickerActivity lastSeenBlockListPickerActivity = (LastSeenBlockListPickerActivity) this;
                    lastSeenBlockListPickerActivity.A00.A00().A05(lastSeenBlockListPickerActivity, new IDxObserverShape3S0100000_1_I1(lastSeenBlockListPickerActivity, 80));
                } else if (!(this instanceof GroupAddBlacklistPickerActivity)) {
                    A2g();
                } else {
                    GroupAddBlacklistPickerActivity groupAddBlacklistPickerActivity = (GroupAddBlacklistPickerActivity) this;
                    groupAddBlacklistPickerActivity.A00.A00().A05(groupAddBlacklistPickerActivity, new IDxObserverShape3S0100000_1_I1(groupAddBlacklistPickerActivity, 76));
                }
                findViewById(16908292).setVisibility(0);
                findViewById(R.id.init_contacts_progress).setVisibility(0);
                this.A0A.A03(this.A0P);
                this.A07.A03(this.A0O);
                this.A0E.A03(this.A0Q);
            }

            @Override // X.ActivityC13790kL, android.app.Activity
            public boolean onCreateOptionsMenu(Menu menu) {
                MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(R.drawable.ic_action_search);
                this.A00 = icon;
                icon.setShowAsAction(10);
                this.A00.setOnActionExpandListener(new MenuItem$OnActionExpandListenerC100754mQ(this));
                this.A00.setVisible(!this.A0J.isEmpty());
                int i = R.string.select_all;
                MenuItem icon2 = menu.add(0, R.id.menuitem_select_all, 0, R.string.select_all).setIcon(R.drawable.ic_action_select_all);
                this.A01 = icon2;
                icon2.setShowAsAction(2);
                MenuItem menuItem = this.A01;
                if (this.A0U.size() == this.A0K.size()) {
                    i = R.string.unselect_all;
                }
                menuItem.setTitle(i);
                return super.onCreateOptionsMenu(menu);
            }

            @Override // X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
            public void onDestroy() {
                super.onDestroy();
                this.A0A.A04(this.A0P);
                this.A07.A04(this.A0O);
                this.A0E.A04(this.A0Q);
                this.A0C.A00();
                C623336s r0 = this.A04;
                if (r0 != null) {
                    r0.A03(true);
                    this.A04 = null;
                }
                AnonymousClass37A r02 = this.A03;
                if (r02 != null) {
                    r02.A03(true);
                    this.A03 = null;
                }
            }

            @Override // X.ActivityC13810kN, android.app.Activity
            public boolean onOptionsItemSelected(MenuItem menuItem) {
                int itemId = menuItem.getItemId();
                if (itemId == R.id.menuitem_search) {
                    onSearchRequested();
                    return true;
                } else if (itemId == R.id.menuitem_select_all) {
                    Set set = this.A0U;
                    if (set.size() != this.A0K.size()) {
                        int i = 0;
                        while (true) {
                            C36571k6 r1 = this.A0N;
                            if (i >= r1.getCount()) {
                                break;
                            }
                            set.add(((C15370n3) r1.A00.get(i)).A0B(UserJid.class));
                            i++;
                        }
                    } else {
                        set.clear();
                    }
                    this.A0N.notifyDataSetChanged();
                    A2h();
                    return true;
                } else if (itemId != 16908332) {
                    return true;
                } else {
                    Set set2 = this.A0S;
                    Set set3 = this.A0U;
                    if (!set2.containsAll(set3) || !set3.containsAll(set2)) {
                        Adm(new ContentDistributionRecipientsPickerActivity$DiscardChangesConfirmationDialogFragment());
                        return true;
                    }
                    finish();
                    return true;
                }
            }

            @Override // X.ActivityC13770kJ, android.app.Activity
            public void onRestoreInstanceState(Bundle bundle) {
                super.onRestoreInstanceState(bundle);
                this.A05.A02(bundle);
            }

            @Override // X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
            public void onSaveInstanceState(Bundle bundle) {
                super.onSaveInstanceState(bundle);
                Set set = this.A0U;
                if (!set.isEmpty()) {
                    bundle.putStringArrayList("selected_jids", C15380n4.A06(set));
                }
                this.A05.A03(bundle);
            }

            @Override // android.app.Activity, android.view.Window.Callback
            public boolean onSearchRequested() {
                this.A05.A01();
                return false;
            }
        }
