package X;

import android.util.Base64;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import org.json.JSONObject;

/* renamed from: X.3Gk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64663Gk {
    public final int A00;
    public final long A01;
    public final Integer A02;
    public final String A03;
    public final X509Certificate A04;
    public final X509Certificate A05;

    public /* synthetic */ C64663Gk(Integer num, String str, X509Certificate x509Certificate, X509Certificate x509Certificate2, int i, long j) {
        this.A04 = x509Certificate;
        this.A05 = x509Certificate2;
        this.A00 = i;
        this.A01 = j;
        this.A03 = str;
        this.A02 = num;
    }

    public /* synthetic */ C64663Gk(String str) {
        JSONObject A05 = C13000ix.A05(str);
        byte[] decode = Base64.decode(A05.getString("e_cert"), 3);
        byte[] decode2 = Base64.decode(A05.getString("s_cert"), 3);
        CertificateFactory instance = CertificateFactory.getInstance("X.509");
        this.A04 = (X509Certificate) instance.generateCertificate(new ByteArrayInputStream(decode));
        this.A05 = (X509Certificate) instance.generateCertificate(new ByteArrayInputStream(decode2));
        this.A00 = A05.optInt("ttl");
        this.A01 = A05.optLong("ts");
        this.A03 = A05.optString("ppk");
        this.A02 = Integer.valueOf(A05.optInt("ppk_id"));
    }
}
