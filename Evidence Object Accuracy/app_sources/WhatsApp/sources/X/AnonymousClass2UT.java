package X;

import android.view.ViewTreeObserver;

/* renamed from: X.2UT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UT implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass1s8 A00;

    public AnonymousClass2UT(AnonymousClass1s8 r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass1s8 r1 = this.A00;
        r1.A06.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        r1.A0A.Aap();
    }
}
