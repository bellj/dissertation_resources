package X;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.SparseArray;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3pV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78763pV extends AbstractC78953po {
    public static final Parcelable.Creator CREATOR = new C98844jL();
    public int A00;
    public int A01;
    public final int A02;
    public final Parcel A03;
    public final C78443ov A04;
    public final String A05;

    public C78763pV(Parcel parcel, C78443ov r4, int i) {
        this.A02 = i;
        C13020j0.A01(parcel);
        this.A03 = parcel;
        this.A04 = r4;
        this.A05 = r4 == null ? null : r4.A01;
        this.A00 = 2;
    }

    public static BigDecimal A00(Parcel parcel, int i) {
        int A03 = C95664e9.A03(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (A03 == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        int readInt = parcel.readInt();
        parcel.setDataPosition(dataPosition + A03);
        return new BigDecimal(new BigInteger(createByteArray), readInt);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r2.A02.containsKey(r5) != false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(X.C78633pE r4, java.lang.Object r5, java.lang.StringBuilder r6) {
        /*
            X.5Qd r2 = r4.A00
            if (r2 == 0) goto L_0x001e
            X.3pN r2 = (X.C78693pN) r2
            java.lang.Number r5 = (java.lang.Number) r5
            android.util.SparseArray r1 = r2.A01
            int r0 = r5.intValue()
            java.lang.Object r1 = r1.get(r0)
            if (r1 != 0) goto L_0x0043
            java.util.HashMap r0 = r2.A02
            java.lang.String r5 = "gms_unknown"
            boolean r0 = r0.containsKey(r5)
            if (r0 == 0) goto L_0x0043
        L_0x001e:
            boolean r0 = r4.A09
            if (r0 == 0) goto L_0x004b
            java.util.AbstractList r5 = (java.util.AbstractList) r5
            java.lang.String r0 = "["
            r6.append(r0)
            int r3 = r5.size()
            r2 = 0
        L_0x002e:
            if (r2 >= r3) goto L_0x0045
            if (r2 == 0) goto L_0x0037
            java.lang.String r0 = ","
            r6.append(r0)
        L_0x0037:
            int r1 = r4.A02
            java.lang.Object r0 = r5.get(r2)
            A02(r0, r6, r1)
            int r2 = r2 + 1
            goto L_0x002e
        L_0x0043:
            r5 = r1
            goto L_0x001e
        L_0x0045:
            java.lang.String r0 = "]"
            r6.append(r0)
            return
        L_0x004b:
            int r0 = r4.A02
            A02(r5, r6, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C78763pV.A01(X.3pE, java.lang.Object, java.lang.StringBuilder):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void A02(java.lang.Object r2, java.lang.StringBuilder r3, int r4) {
        /*
            java.lang.String r1 = "\""
            switch(r4) {
                case 0: goto L_0x0016;
                case 1: goto L_0x0016;
                case 2: goto L_0x0016;
                case 3: goto L_0x0016;
                case 4: goto L_0x0016;
                case 5: goto L_0x0016;
                case 6: goto L_0x0016;
                case 7: goto L_0x001a;
                case 8: goto L_0x0029;
                case 9: goto L_0x0030;
                case 10: goto L_0x0046;
                case 11: goto L_0x004f;
                default: goto L_0x0005;
            }
        L_0x0005:
            r0 = 26
            java.lang.StringBuilder r1 = X.C12980iv.A0t(r0)
            java.lang.String r0 = "Unknown type = "
            java.lang.String r0 = X.C12960it.A0e(r0, r1, r4)
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        L_0x0016:
            r3.append(r2)
            return
        L_0x001a:
            r3.append(r1)
            X.C13020j0.A01(r2)
            java.lang.String r0 = r2.toString()
            java.lang.String r0 = X.AnonymousClass4ZT.A00(r0)
            goto L_0x003a
        L_0x0029:
            r3.append(r1)
            byte[] r2 = (byte[]) r2
            r0 = 0
            goto L_0x0037
        L_0x0030:
            r3.append(r1)
            byte[] r2 = (byte[]) r2
            r0 = 10
        L_0x0037:
            if (r2 != 0) goto L_0x0041
            r0 = 0
        L_0x003a:
            r3.append(r0)
            r3.append(r1)
            return
        L_0x0041:
            java.lang.String r0 = android.util.Base64.encodeToString(r2, r0)
            goto L_0x003a
        L_0x0046:
            X.C13020j0.A01(r2)
            java.util.HashMap r2 = (java.util.HashMap) r2
            X.AnonymousClass4DP.A00(r3, r2)
            return
        L_0x004f:
            java.lang.String r0 = "Method does not accept concrete type."
            java.lang.IllegalArgumentException r0 = X.C12970iu.A0f(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C78763pV.A02(java.lang.Object, java.lang.StringBuilder, int):void");
    }

    public final void A07(Parcel parcel, StringBuilder sb, Map map) {
        Parcel obtain;
        BigInteger bigInteger;
        String str;
        boolean[] createBooleanArray;
        BigDecimal[] bigDecimalArr;
        double[] createDoubleArray;
        float[] createFloatArray;
        long[] createLongArray;
        BigInteger[] bigIntegerArr;
        BigInteger bigInteger2;
        SparseArray sparseArray = new SparseArray();
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            sparseArray.put(((C78633pE) A15.getValue()).A04, A15);
        }
        sb.append('{');
        int A01 = C95664e9.A01(parcel);
        boolean z = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            Map.Entry entry = (Map.Entry) sparseArray.get((char) readInt);
            if (entry != null) {
                if (z) {
                    sb.append(",");
                }
                C78633pE r9 = (C78633pE) entry.getValue();
                sb.append("\"");
                sb.append((String) entry.getKey());
                sb.append("\":");
                if (r9.A00 != null) {
                    int i = r9.A03;
                    switch (i) {
                        case 0:
                            A01(r9, Integer.valueOf(C95664e9.A02(parcel, readInt)), sb);
                            break;
                        case 1:
                            int A03 = C95664e9.A03(parcel, readInt);
                            int dataPosition = parcel.dataPosition();
                            if (A03 == 0) {
                                bigInteger2 = null;
                            } else {
                                byte[] createByteArray = parcel.createByteArray();
                                parcel.setDataPosition(dataPosition + A03);
                                bigInteger2 = new BigInteger(createByteArray);
                            }
                            A01(r9, bigInteger2, sb);
                            break;
                        case 2:
                            A01(r9, Long.valueOf(C95664e9.A04(parcel, readInt)), sb);
                            break;
                        case 3:
                            A01(r9, Float.valueOf(C95664e9.A00(parcel, readInt)), sb);
                            break;
                        case 4:
                            C95664e9.A0F(parcel, readInt, 8);
                            A01(r9, Double.valueOf(parcel.readDouble()), sb);
                            break;
                        case 5:
                            A01(r9, A00(parcel, readInt), sb);
                            break;
                        case 6:
                            A01(r9, Boolean.valueOf(C12960it.A1S(C95664e9.A02(parcel, readInt))), sb);
                            break;
                        case 7:
                            A01(r9, C95664e9.A08(parcel, readInt), sb);
                            break;
                        case 8:
                        case 9:
                            A01(r9, C95664e9.A0I(parcel, readInt), sb);
                            break;
                        case 10:
                            Bundle A05 = C95664e9.A05(parcel, readInt);
                            HashMap A11 = C12970iu.A11();
                            Iterator<String> it = A05.keySet().iterator();
                            while (it.hasNext()) {
                                String A0x = C12970iu.A0x(it);
                                String string = A05.getString(A0x);
                                C13020j0.A01(string);
                                A11.put(A0x, string);
                            }
                            A01(r9, A11, sb);
                            break;
                        case 11:
                            throw C12970iu.A0f("Method does not accept concrete type.");
                        default:
                            throw C12970iu.A0f(C12960it.A0e("Unknown field out type = ", C12980iv.A0t(36), i));
                    }
                } else if (r9.A0A) {
                    sb.append("[");
                    switch (r9.A03) {
                        case 0:
                            int[] A0J = C95664e9.A0J(parcel, readInt);
                            int length = A0J.length;
                            for (int i2 = 0; i2 < length; i2++) {
                                if (i2 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Integer.toString(A0J[i2]));
                            }
                            break;
                        case 1:
                            int A032 = C95664e9.A03(parcel, readInt);
                            int dataPosition2 = parcel.dataPosition();
                            if (A032 == 0) {
                                bigIntegerArr = null;
                            } else {
                                int readInt2 = parcel.readInt();
                                bigIntegerArr = new BigInteger[readInt2];
                                for (int i3 = 0; i3 < readInt2; i3++) {
                                    bigIntegerArr[i3] = new BigInteger(parcel.createByteArray());
                                }
                                parcel.setDataPosition(dataPosition2 + A032);
                            }
                            int length2 = bigIntegerArr.length;
                            for (int i4 = 0; i4 < length2; i4++) {
                                if (i4 != 0) {
                                    sb.append(",");
                                }
                                sb.append(bigIntegerArr[i4]);
                            }
                            break;
                        case 2:
                            int A033 = C95664e9.A03(parcel, readInt);
                            int dataPosition3 = parcel.dataPosition();
                            if (A033 == 0) {
                                createLongArray = null;
                            } else {
                                createLongArray = parcel.createLongArray();
                                parcel.setDataPosition(dataPosition3 + A033);
                            }
                            int length3 = createLongArray.length;
                            for (int i5 = 0; i5 < length3; i5++) {
                                if (i5 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Long.toString(createLongArray[i5]));
                            }
                            break;
                        case 3:
                            int A034 = C95664e9.A03(parcel, readInt);
                            int dataPosition4 = parcel.dataPosition();
                            if (A034 == 0) {
                                createFloatArray = null;
                            } else {
                                createFloatArray = parcel.createFloatArray();
                                parcel.setDataPosition(dataPosition4 + A034);
                            }
                            int length4 = createFloatArray.length;
                            for (int i6 = 0; i6 < length4; i6++) {
                                if (i6 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Float.toString(createFloatArray[i6]));
                            }
                            break;
                        case 4:
                            int A035 = C95664e9.A03(parcel, readInt);
                            int dataPosition5 = parcel.dataPosition();
                            if (A035 == 0) {
                                createDoubleArray = null;
                            } else {
                                createDoubleArray = parcel.createDoubleArray();
                                parcel.setDataPosition(dataPosition5 + A035);
                            }
                            int length5 = createDoubleArray.length;
                            for (int i7 = 0; i7 < length5; i7++) {
                                if (i7 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Double.toString(createDoubleArray[i7]));
                            }
                            break;
                        case 5:
                            int A036 = C95664e9.A03(parcel, readInt);
                            int dataPosition6 = parcel.dataPosition();
                            if (A036 == 0) {
                                bigDecimalArr = null;
                            } else {
                                int readInt3 = parcel.readInt();
                                bigDecimalArr = new BigDecimal[readInt3];
                                for (int i8 = 0; i8 < readInt3; i8++) {
                                    byte[] createByteArray2 = parcel.createByteArray();
                                    bigDecimalArr[i8] = new BigDecimal(new BigInteger(createByteArray2), parcel.readInt());
                                }
                                parcel.setDataPosition(dataPosition6 + A036);
                            }
                            int length6 = bigDecimalArr.length;
                            for (int i9 = 0; i9 < length6; i9++) {
                                if (i9 != 0) {
                                    sb.append(",");
                                }
                                sb.append(bigDecimalArr[i9]);
                            }
                            break;
                        case 6:
                            int A037 = C95664e9.A03(parcel, readInt);
                            int dataPosition7 = parcel.dataPosition();
                            if (A037 == 0) {
                                createBooleanArray = null;
                            } else {
                                createBooleanArray = parcel.createBooleanArray();
                                parcel.setDataPosition(dataPosition7 + A037);
                            }
                            int length7 = createBooleanArray.length;
                            for (int i10 = 0; i10 < length7; i10++) {
                                if (i10 != 0) {
                                    sb.append(",");
                                }
                                sb.append(Boolean.toString(createBooleanArray[i10]));
                            }
                            break;
                        case 7:
                            String[] A0L = C95664e9.A0L(parcel, readInt);
                            int length8 = A0L.length;
                            for (int i11 = 0; i11 < length8; i11++) {
                                if (i11 != 0) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(A0L[i11]);
                                sb.append("\"");
                            }
                            break;
                        case 8:
                        case 9:
                        case 10:
                            throw C12980iv.A0u("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                        case 11:
                            int A038 = C95664e9.A03(parcel, readInt);
                            int dataPosition8 = parcel.dataPosition();
                            Parcel[] parcelArr = null;
                            if (A038 != 0) {
                                int readInt4 = parcel.readInt();
                                Parcel[] parcelArr2 = new Parcel[readInt4];
                                for (int i12 = 0; i12 < readInt4; i12++) {
                                    int readInt5 = parcel.readInt();
                                    if (readInt5 != 0) {
                                        int dataPosition9 = parcel.dataPosition();
                                        Parcel obtain2 = Parcel.obtain();
                                        obtain2.appendFrom(parcel, dataPosition9, readInt5);
                                        parcelArr2[i12] = obtain2;
                                        parcel.setDataPosition(dataPosition9 + readInt5);
                                    } else {
                                        parcelArr2[i12] = null;
                                    }
                                }
                                parcel.setDataPosition(dataPosition8 + A038);
                                parcelArr = parcelArr2;
                            }
                            int length9 = parcelArr.length;
                            for (int i13 = 0; i13 < length9; i13++) {
                                if (i13 > 0) {
                                    sb.append(",");
                                }
                                parcelArr[i13].setDataPosition(0);
                                String str2 = r9.A08;
                                C13020j0.A01(str2);
                                C78443ov r0 = r9.A01;
                                C13020j0.A01(r0);
                                Map map2 = (Map) r0.A02.get(str2);
                                C13020j0.A01(map2);
                                A07(parcelArr[i13], sb, map2);
                            }
                            break;
                        default:
                            throw C12960it.A0U("Unknown field type out.");
                    }
                    sb.append("]");
                } else {
                    switch (r9.A03) {
                        case 0:
                            sb.append(C95664e9.A02(parcel, readInt));
                            break;
                        case 1:
                            int A039 = C95664e9.A03(parcel, readInt);
                            int dataPosition10 = parcel.dataPosition();
                            if (A039 == 0) {
                                bigInteger = null;
                            } else {
                                byte[] createByteArray3 = parcel.createByteArray();
                                parcel.setDataPosition(dataPosition10 + A039);
                                bigInteger = new BigInteger(createByteArray3);
                            }
                            sb.append(bigInteger);
                            break;
                        case 2:
                            sb.append(C95664e9.A04(parcel, readInt));
                            break;
                        case 3:
                            sb.append(C95664e9.A00(parcel, readInt));
                            break;
                        case 4:
                            C95664e9.A0F(parcel, readInt, 8);
                            sb.append(parcel.readDouble());
                            break;
                        case 5:
                            sb.append(A00(parcel, readInt));
                            break;
                        case 6:
                            sb.append(C12960it.A1S(C95664e9.A02(parcel, readInt)));
                            break;
                        case 7:
                            String A08 = C95664e9.A08(parcel, readInt);
                            sb.append("\"");
                            sb.append(AnonymousClass4ZT.A00(A08));
                            sb.append("\"");
                            break;
                        case 8:
                            byte[] A0I = C95664e9.A0I(parcel, readInt);
                            sb.append("\"");
                            if (A0I != null) {
                                str = Base64.encodeToString(A0I, 0);
                                sb.append(str);
                                sb.append("\"");
                                break;
                            }
                            str = null;
                            sb.append(str);
                            sb.append("\"");
                        case 9:
                            byte[] A0I2 = C95664e9.A0I(parcel, readInt);
                            sb.append("\"");
                            if (A0I2 != null) {
                                str = Base64.encodeToString(A0I2, 10);
                                sb.append(str);
                                sb.append("\"");
                                break;
                            }
                            str = null;
                            sb.append(str);
                            sb.append("\"");
                        case 10:
                            Bundle A052 = C95664e9.A05(parcel, readInt);
                            Set<String> keySet = A052.keySet();
                            sb.append("{");
                            Iterator<String> it2 = keySet.iterator();
                            boolean z2 = true;
                            while (it2.hasNext()) {
                                String A0x2 = C12970iu.A0x(it2);
                                if (!z2) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(A0x2);
                                sb.append("\":\"");
                                sb.append(AnonymousClass4ZT.A00(A052.getString(A0x2)));
                                sb.append("\"");
                                z2 = false;
                            }
                            sb.append("}");
                            break;
                        case 11:
                            int A0310 = C95664e9.A03(parcel, readInt);
                            int dataPosition11 = parcel.dataPosition();
                            if (A0310 == 0) {
                                obtain = null;
                            } else {
                                obtain = Parcel.obtain();
                                obtain.appendFrom(parcel, dataPosition11, A0310);
                                parcel.setDataPosition(dataPosition11 + A0310);
                            }
                            obtain.setDataPosition(0);
                            String str3 = r9.A08;
                            C13020j0.A01(str3);
                            C78443ov r02 = r9.A01;
                            C13020j0.A01(r02);
                            Map map3 = (Map) r02.A02.get(str3);
                            C13020j0.A01(map3);
                            A07(obtain, sb, map3);
                            break;
                        default:
                            throw C12960it.A0U("Unknown field type out");
                    }
                }
                z = true;
            }
        }
        if (parcel.dataPosition() == A01) {
            sb.append('}');
            return;
        }
        throw new C113235Gs(parcel, C12960it.A0e("Overread allowed size end=", C12980iv.A0t(37), A01));
    }

    @Override // X.AbstractC95014cu, java.lang.Object
    public final String toString() {
        Parcel parcel;
        int A00;
        C78443ov r4 = this.A04;
        C13020j0.A02(r4, "Cannot convert to JSON on client side.");
        int i = this.A00;
        if (i != 0) {
            if (i == 1) {
                parcel = this.A03;
                A00 = this.A01;
            }
            Parcel parcel2 = this.A03;
            parcel2.setDataPosition(0);
            StringBuilder A0t = C12980iv.A0t(100);
            String str = this.A05;
            C13020j0.A01(str);
            Map map = (Map) r4.A02.get(str);
            C13020j0.A01(map);
            A07(parcel2, A0t, map);
            return A0t.toString();
        }
        parcel = this.A03;
        A00 = C95654e8.A00(parcel);
        this.A01 = A00;
        C95654e8.A06(parcel, A00);
        this.A00 = 2;
        Parcel parcel2 = this.A03;
        parcel2.setDataPosition(0);
        StringBuilder A0t = C12980iv.A0t(100);
        String str = this.A05;
        C13020j0.A01(str);
        Map map = (Map) r4.A02.get(str);
        C13020j0.A01(map);
        A07(parcel2, A0t, map);
        return A0t.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022  */
    @Override // android.os.Parcelable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void writeToParcel(android.os.Parcel r6, int r7) {
        /*
            r5 = this;
            r3 = 20293(0x4f45, float:2.8437E-41)
            int r4 = X.C95654e8.A02(r6, r3)
            int r0 = r5.A02
            r1 = 1
            X.C95654e8.A07(r6, r1, r0)
            int r0 = r5.A00
            r2 = 2
            if (r0 == 0) goto L_0x003a
            if (r0 != r1) goto L_0x001c
            android.os.Parcel r1 = r5.A03
            int r0 = r5.A01
        L_0x0017:
            X.C95654e8.A06(r1, r0)
            r5.A00 = r2
        L_0x001c:
            android.os.Parcel r3 = r5.A03
            r2 = 0
            r0 = 2
            if (r3 == 0) goto L_0x0030
            int r1 = X.C95654e8.A02(r6, r0)
            int r0 = r3.dataSize()
            r6.appendFrom(r3, r2, r0)
            X.C95654e8.A06(r6, r1)
        L_0x0030:
            X.3ov r1 = r5.A04
            r0 = 3
            X.C95654e8.A0B(r6, r1, r0, r7, r2)
            X.C95654e8.A06(r6, r4)
            return
        L_0x003a:
            android.os.Parcel r1 = r5.A03
            int r0 = X.C95654e8.A02(r1, r3)
            r5.A01 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C78763pV.writeToParcel(android.os.Parcel, int):void");
    }
}
