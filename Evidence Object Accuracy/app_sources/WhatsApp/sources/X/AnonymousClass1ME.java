package X;

import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;

/* renamed from: X.1ME  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ME implements AnonymousClass1MF {
    public final C14900mE A00;
    public final C15610nY A01;
    public final C16590pI A02;

    public /* synthetic */ AnonymousClass1ME(C14900mE r1, C15610nY r2, C16590pI r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass1MF
    public void AR8(C15370n3 r4) {
        C14900mE r2 = this.A00;
        r2.A03();
        r2.A0H(new RunnableBRunnable0Shape12S0100000_I0_12(this, 40));
    }

    @Override // X.AnonymousClass1MF
    public void AYA(C15370n3 r4) {
        C14900mE r2 = this.A00;
        r2.A03();
        r2.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 24, r4));
    }
}
