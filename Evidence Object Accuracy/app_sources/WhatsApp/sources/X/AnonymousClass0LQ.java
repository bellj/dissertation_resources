package X;

import android.content.Context;
import android.util.TypedValue;

/* renamed from: X.0LQ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0LQ {
    public static float A00(Context context, float f) {
        return TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
    }
}
