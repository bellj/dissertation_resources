package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2Q4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Q4 extends AnonymousClass2PA {
    public final AnonymousClass2Q5 A00;

    public AnonymousClass2Q4(Jid jid, AnonymousClass2Q5 r2, String str, long j) {
        super(jid, str, j);
        this.A00 = r2;
    }
}
