package X;

import android.os.SystemClock;
import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.37u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625837u extends AbstractC16350or {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C18850tA A01;
    public final C242114q A02;
    public final AbstractC14640lm A03;
    public final C22230yk A04;
    public final WeakReference A05;

    public C625837u(C18850tA r3, StarredMessagesActivity starredMessagesActivity, C242114q r5, AbstractC14640lm r6, C22230yk r7) {
        this.A01 = r3;
        this.A04 = r7;
        this.A02 = r5;
        this.A05 = C12970iu.A10(starredMessagesActivity);
        this.A03 = r6;
    }
}
