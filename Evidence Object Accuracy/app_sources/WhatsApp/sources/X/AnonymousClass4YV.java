package X;

import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.google.android.exoplayer2.Timeline;

/* renamed from: X.4YV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4YV {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public AnonymousClass4YD A04;
    public AnonymousClass4YD A05;
    public AnonymousClass4YD A06;
    public Object A07;
    public boolean A08;
    public final Handler A09;
    public final AnonymousClass4YJ A0A = new AnonymousClass4YJ();
    public final C94404bl A0B = new C94404bl();
    public final C106424vg A0C;

    public AnonymousClass4YV(Handler handler, C106424vg r3) {
        this.A0C = r3;
        this.A09 = handler;
    }

    public AnonymousClass4YD A00() {
        AnonymousClass4YD r2 = this.A05;
        if (r2 == null) {
            return null;
        }
        if (r2 == this.A06) {
            this.A06 = r2.A01;
        }
        r2.A03();
        int i = this.A00 - 1;
        this.A00 = i;
        if (i == 0) {
            this.A04 = null;
            AnonymousClass4YD r1 = this.A05;
            this.A07 = r1.A0B;
            this.A03 = r1.A02.A04.A03;
        }
        this.A05 = this.A05.A01;
        A08();
        return this.A05;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e5, code lost:
        r4 = r8.A04;
        r2 = r9.A02;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass4Y1 A01(X.AnonymousClass4YD r26, com.google.android.exoplayer2.Timeline r27, long r28) {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YV.A01(X.4YD, com.google.android.exoplayer2.Timeline, long):X.4Y1");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r6.A02 != -1) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass4Y1 A02(X.AnonymousClass4Y1 r19, com.google.android.exoplayer2.Timeline r20) {
        /*
            r18 = this;
            r3 = r19
            X.1Ov r6 = r3.A04
            boolean r2 = r6.A00()
            if (r2 != 0) goto L_0x0010
            int r1 = r6.A02
            r0 = -1
            r15 = 1
            if (r1 == r0) goto L_0x0011
        L_0x0010:
            r15 = 0
        L_0x0011:
            r1 = r18
            r5 = r20
            boolean r16 = r1.A0B(r5, r6)
            boolean r17 = r1.A0C(r5, r6, r15)
            java.lang.Object r0 = r6.A04
            X.4YJ r4 = r1.A0A
            r5.A0A(r4, r0)
            if (r2 == 0) goto L_0x003a
            int r1 = r6.A00
            int r0 = r6.A01
            long r13 = r4.A04(r1, r0)
        L_0x002e:
            long r7 = r3.A03
            long r9 = r3.A02
            long r11 = r3.A01
            X.4Y1 r5 = new X.4Y1
            r5.<init>(r6, r7, r9, r11, r13, r15, r16, r17)
            return r5
        L_0x003a:
            long r13 = r3.A01
            r1 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            int r0 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x004b
            r1 = -9223372036854775808
            int r0 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x002e
        L_0x004b:
            long r13 = r4.A01
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YV.A02(X.4Y1, com.google.android.exoplayer2.Timeline):X.4Y1");
    }

    public final AnonymousClass4Y1 A03(Timeline timeline, C28741Ov r12, long j, long j2) {
        Object obj = r12.A04;
        timeline.A0A(this.A0A, obj);
        if (r12.A00()) {
            return A04(timeline, obj, r12.A00, r12.A01, j, r12.A03);
        }
        return A05(timeline, obj, j2, j, r12.A03);
    }

    public final AnonymousClass4Y1 A04(Timeline timeline, Object obj, int i, int i2, long j, long j2) {
        C28741Ov r6 = new C28741Ov(obj, i, i2, j2);
        long A04 = timeline.A0A(this.A0A, r6.A04).A04(r6.A00, r6.A01);
        long j3 = 0;
        if (A04 != -9223372036854775807L && 0 >= A04) {
            j3 = Math.max(0L, A04 - 1);
        }
        return new AnonymousClass4Y1(r6, j3, j, -9223372036854775807L, A04, false, false, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        if (r7.A02 != -1) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass4Y1 A05(com.google.android.exoplayer2.Timeline r20, java.lang.Object r21, long r22, long r24, long r26) {
        /*
            r19 = this;
            r8 = r22
            r5 = r19
            X.4YJ r3 = r5.A0A
            r4 = r21
            r6 = r20
            r6.A0A(r3, r4)
            int r2 = r3.A02(r8)
            r0 = r26
            X.1Ov r7 = new X.1Ov
            r7.<init>(r4, r0, r2)
            boolean r0 = r7.A00()
            if (r0 != 0) goto L_0x0024
            int r1 = r7.A02
            r0 = -1
            r4 = 1
            if (r1 == r0) goto L_0x0025
        L_0x0024:
            r4 = 0
        L_0x0025:
            boolean r17 = r5.A0B(r6, r7)
            boolean r18 = r5.A0C(r6, r7, r4)
            r5 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r0 = -1
            if (r2 == r0) goto L_0x0062
            X.4bh r0 = r3.A03
            long[] r0 = r0.A02
            r12 = r0[r2]
            int r0 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0067
            r1 = -9223372036854775808
            int r0 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0067
            r14 = r12
        L_0x0046:
            int r0 = (r14 > r5 ? 1 : (r14 == r5 ? 0 : -1))
            if (r0 == 0) goto L_0x0058
            int r0 = (r22 > r14 ? 1 : (r22 == r14 ? 0 : -1))
            if (r0 < 0) goto L_0x0058
            r2 = 0
            r5 = 1
            long r0 = r14 - r5
            long r8 = java.lang.Math.max(r2, r0)
        L_0x0058:
            r10 = r24
            r16 = r4
            X.4Y1 r6 = new X.4Y1
            r6.<init>(r7, r8, r10, r12, r14, r16, r17, r18)
            return r6
        L_0x0062:
            r12 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
        L_0x0067:
            long r14 = r3.A01
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YV.A05(com.google.android.exoplayer2.Timeline, java.lang.Object, long, long, long):X.4Y1");
    }

    public C28741Ov A06(Timeline timeline, Object obj, long j) {
        long j2;
        int A04;
        AnonymousClass4YJ r3 = this.A0A;
        int A00 = AnonymousClass4YJ.A00(r3, timeline, obj);
        Object obj2 = this.A07;
        if (obj2 == null || (A04 = timeline.A04(obj2)) == -1 || timeline.A09(r3, A04, false).A00 != A00) {
            AnonymousClass4YD r4 = this.A05;
            while (true) {
                if (r4 != null) {
                    if (r4.A0B.equals(obj)) {
                        break;
                    }
                    r4 = r4.A01;
                } else {
                    r4 = this.A05;
                    while (r4 != null) {
                        int A042 = timeline.A04(r4.A0B);
                        if (A042 == -1 || timeline.A09(r3, A042, false).A00 != A00) {
                            r4 = r4.A01;
                        }
                    }
                    j2 = this.A02;
                    this.A02 = 1 + j2;
                    if (this.A05 == null) {
                        this.A07 = obj;
                        this.A03 = j2;
                    }
                }
            }
            j2 = r4.A02.A04.A03;
        } else {
            j2 = this.A03;
        }
        timeline.A0A(r3, obj);
        int A03 = r3.A03(j);
        if (A03 == -1) {
            return new C28741Ov(obj, j2, r3.A02(j));
        }
        return new C28741Ov(obj, A03, r3.A01(A03), j2);
    }

    public void A07() {
        if (this.A00 != 0) {
            AnonymousClass4YD r2 = this.A05;
            C95314dV.A01(r2);
            this.A07 = r2.A0B;
            this.A03 = r2.A02.A04.A03;
            do {
                r2.A03();
                r2 = r2.A01;
            } while (r2 != null);
            this.A05 = null;
            this.A04 = null;
            this.A06 = null;
            this.A00 = 0;
            A08();
        }
    }

    public final void A08() {
        C28741Ov r3;
        if (this.A0C != null) {
            AnonymousClass29A builder = AnonymousClass1Mr.builder();
            for (AnonymousClass4YD r1 = this.A05; r1 != null; r1 = r1.A01) {
                builder.add((Object) r1.A02.A04);
            }
            AnonymousClass4YD r0 = this.A06;
            if (r0 == null) {
                r3 = null;
            } else {
                r3 = r0.A02.A04;
            }
            this.A09.post(new RunnableBRunnable0Shape3S0300000_I1(this, builder, r3, 0));
        }
    }

    public boolean A09(AnonymousClass4YD r5) {
        boolean z = false;
        C95314dV.A04(C12960it.A1W(r5));
        if (!r5.equals(this.A04)) {
            this.A04 = r5;
            while (r5.A01 != null) {
                r5 = r5.A01;
                if (r5 == this.A06) {
                    this.A06 = this.A05;
                    z = true;
                }
                r5.A03();
                this.A00--;
            }
            AnonymousClass4YD r2 = this.A04;
            if (null != r2.A01) {
                r2.A01 = null;
            }
            A08();
        }
        return z;
    }

    public final boolean A0A(Timeline timeline) {
        AnonymousClass4YD r1;
        AnonymousClass4YD r2 = this.A05;
        if (r2 == null) {
            return true;
        }
        int A04 = timeline.A04(r2.A0B);
        while (true) {
            A04 = timeline.A03(this.A0A, this.A0B, A04, this.A01, this.A08);
            while (true) {
                r1 = r2.A01;
                if (r1 == null || r2.A02.A06) {
                    break;
                }
                r2 = r1;
            }
            if (A04 == -1 || r1 == null || timeline.A04(r1.A0B) != A04) {
                break;
            }
            r2 = r1;
        }
        boolean A09 = A09(r2);
        r2.A02 = A02(r2.A02, timeline);
        return !A09;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000a, code lost:
        if (r6.A02 != -1) goto L_0x000c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0B(com.google.android.exoplayer2.Timeline r5, X.C28741Ov r6) {
        /*
            r4 = this;
            boolean r0 = r6.A00()
            if (r0 != 0) goto L_0x000c
            int r2 = r6.A02
            r1 = -1
            r0 = 1
            if (r2 == r1) goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            r3 = 0
            if (r0 == 0) goto L_0x0027
            java.lang.Object r1 = r6.A04
            X.4YJ r0 = r4.A0A
            int r2 = X.AnonymousClass4YJ.A00(r0, r5, r1)
            int r1 = r5.A04(r1)
            X.4bl r0 = r4.A0B
            X.4bl r0 = X.C72463ee.A0B(r0, r5, r2)
            int r0 = r0.A01
            if (r0 != r1) goto L_0x0027
            r3 = 1
        L_0x0027:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YV.A0B(com.google.android.exoplayer2.Timeline, X.1Ov):boolean");
    }

    public final boolean A0C(Timeline timeline, C28741Ov r9, boolean z) {
        int A04 = timeline.A04(r9.A04);
        AnonymousClass4YJ r2 = this.A0A;
        int i = timeline.A09(r2, A04, false).A00;
        C94404bl r3 = this.A0B;
        return !C72463ee.A0B(r3, timeline, i).A0A && timeline.A03(r2, r3, A04, this.A01, this.A08) == -1 && z;
    }
}
