package X;

import java.math.BigInteger;

/* renamed from: X.5Mc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114535Mc extends AnonymousClass1TM {
    public static final BigInteger A03 = BigInteger.valueOf(0);
    public AnonymousClass5NG A00;
    public AnonymousClass5NG A01;
    public AnonymousClass5N1 A02;

    public C114535Mc(AbstractC114775Na r5) {
        AnonymousClass5NU r0;
        this.A02 = AnonymousClass5N1.A01(r5.A0D(0));
        int A0B = r5.A0B();
        if (A0B != 1) {
            if (A0B == 2) {
                r0 = AnonymousClass5NU.A01(r5.A0D(1));
                int i = r0.A00;
                if (i == 0) {
                    this.A01 = AnonymousClass5NG.A01(r0);
                    return;
                } else if (i != 1) {
                    throw C12970iu.A0f(C12960it.A0W(i, "Bad tag number: "));
                }
            } else if (A0B == 3) {
                AnonymousClass5NU A01 = AnonymousClass5NU.A01(r5.A0D(1));
                int i2 = A01.A00;
                if (i2 == 0) {
                    this.A01 = AnonymousClass5NG.A01(A01);
                    r0 = AnonymousClass5NU.A01(r5.A0D(2));
                    int i3 = r0.A00;
                    if (i3 != 1) {
                        throw C12970iu.A0f(C12960it.A0W(i3, "Bad tag number for 'maximum': "));
                    }
                } else {
                    throw C12970iu.A0f(C12960it.A0W(i2, "Bad tag number for 'minimum': "));
                }
            } else {
                throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r5.A0B()));
            }
            this.A00 = AnonymousClass5NG.A01(r0);
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(3);
        r3.A06(this.A02);
        AnonymousClass5NG r1 = this.A01;
        if (r1 != null && !r1.A0C(A03)) {
            C94954co.A03(r1, r3, false);
        }
        AnonymousClass5NG r12 = this.A00;
        if (r12 != null) {
            C94954co.A02(r12, r3, 1, false);
        }
        return new AnonymousClass5NZ(r3);
    }
}
