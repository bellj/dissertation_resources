package X;

import android.graphics.PointF;
import com.facebook.redex.IDxAListenerShape6S0100000_2_I1;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.R;
import com.whatsapp.base.Hilt_WaDialogFragment;
import com.whatsapp.base.WaDialogFragment;
import com.whatsapp.dialogs.FAQLearnMoreDialogFragment;
import com.whatsapp.group.GroupSettingsActivity;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.cert.CertPath;
import java.security.cert.X509Certificate;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLException;
import org.json.JSONObject;
import org.whispersystems.curve25519.JavaCurve25519Provider;

/* renamed from: X.3ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C72453ed {
    public static double A00(Object obj) {
        return ((Number) obj).doubleValue();
    }

    public static float A01(PointF pointF, PointF pointF2) {
        float f = pointF.x - pointF2.x;
        float f2 = pointF.y - pointF2.y;
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }

    public static float A02(Object obj) {
        return ((Number) obj).floatValue();
    }

    public static int A03() {
        if (C14950mJ.A00()) {
            return R.string.record_need_sd_card_message;
        }
        return R.string.record_need_sd_card_message_shared_storage;
    }

    public static int A04() {
        if (C14950mJ.A00()) {
            return R.string.record_need_sd_card_title;
        }
        return R.string.record_need_sd_card_title_shared_storage;
    }

    public static int A05(int i) {
        return (527 + i) * 31;
    }

    public static int A06(int i) {
        if (i >= 0) {
            return C80253rx.A00(i);
        }
        return 10;
    }

    public static int A07(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) != 0) {
            return (i & -268435456) == 0 ? 4 : 5;
        }
        return 3;
    }

    public static int A08(int i) {
        if (i >= 0) {
            return AbstractC79223qF.A00(i);
        }
        return 10;
    }

    public static int A09(int i, int i2, int i3) {
        int i4 = (i & 3) << 3;
        return ((255 & i3) << i4) | (i2 & ((255 << i4) ^ -1));
    }

    public static int A0A(int i, long j) {
        return (i + ((int) (j ^ (j >>> 32)))) * 31;
    }

    public static int A0B(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static int A0C(Object obj) {
        return ((List) obj).size();
    }

    public static int A0D(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    public static int A0E(String str) {
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public static int A0F(CertPath certPath, int i, int i2) {
        X509Certificate x509Certificate = (X509Certificate) certPath.getCertificates().get(i);
        return (x509Certificate.getSubjectDN().equals(x509Certificate.getIssuerDN()) || i2 == 0) ? i2 : i2 - 1;
    }

    public static int A0G(List list, int i) {
        return ((Number) list.get(i)).intValue();
    }

    public static int A0H(byte[] bArr) {
        int length = bArr.length;
        return AnonymousClass1TQ.A00(length) + 1 + length;
    }

    public static int A0I(byte[] bArr, char c, char c2, int i) {
        int codePoint = Character.toCodePoint(c, c2);
        int i2 = i + 1;
        bArr[i] = (byte) ((codePoint >>> 18) | 240);
        int i3 = i2 + 1;
        bArr[i2] = (byte) (((codePoint >>> 12) & 63) | 128);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (((codePoint >>> 6) & 63) | 128);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((codePoint & 63) | 128);
        return i5;
    }

    public static int A0J(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    public static int A0K(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    public static int A0L(byte[] bArr, int i) {
        return (bArr[i + 1] & 255) | ((bArr[i] & 255) << 8);
    }

    public static int A0M(byte[] bArr, int i, int i2) {
        int i3 = i + 1;
        bArr[i] = (byte) ((i2 >>> 6) | 960);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i2 & 63) | 128);
        return i4;
    }

    public static int A0N(byte[] bArr, int i, int i2) {
        int i3 = i + 1;
        bArr[i] = (byte) ((i2 >>> 12) | 480);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (((i2 >>> 6) & 63) | 128);
        return i4;
    }

    public static int A0O(byte[] bArr, int i, int i2) {
        int i3 = i + 1;
        bArr[i] = (byte) (i2 >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i2 >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i2 >>> 8);
        return i5;
    }

    public static int A0P(byte[] bArr, int i, int i2, int i3) {
        int i4 = i3 + 1;
        return (bArr[i4 + 1] & 255) | i | i2 | ((bArr[i4] & 255) << 8);
    }

    public static int A0Q(byte[] bArr, byte[] bArr2, int i, int i2) {
        bArr2[i] = (byte) (i2 ^ bArr[i]);
        return i + 1;
    }

    public static int A0R(AnonymousClass1TN[] r0, int i, int i2) {
        return i2 + r0[i].Aer().A06().A05();
    }

    public static int A0S(AnonymousClass1TN[] r0, int i, int i2) {
        return i2 + r0[i].Aer().A07().A05();
    }

    public static long A0T(int i, int i2) {
        return (((long) i2) & 4294967295L) | ((((long) i) & 4294967295L) << 32);
    }

    public static long A0U(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    public static long A0V(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static long A0W(long j, long j2) {
        return (j * j2) / SearchActionVerificationClientService.MS_TO_NS;
    }

    public static long A0X(long j, long j2) {
        return Math.max(0L, Math.min(j, j2));
    }

    public static long A0Y(List list, int i) {
        long longValue = ((Number) list.get(i)).longValue();
        return (longValue >> 63) ^ (longValue << 1);
    }

    public static long A0Z(List list, int i) {
        return ((Number) list.get(i)).longValue();
    }

    public static long A0a(byte[] bArr, int i) {
        return ((((long) bArr[i + 2]) << 16) & 16711680) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) << 8) & 65280);
    }

    public static long A0b(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    public static long A0c(byte[] bArr, int i) {
        return ((((long) bArr[i + 3]) << 24) & 4278190080L) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) << 8) & 65280) | ((((long) bArr[i + 2]) << 16) & 16711680);
    }

    public static AnonymousClass5XT A0d(Object obj) {
        return C93984b4.A02.A00(obj.getClass());
    }

    public static AnonymousClass01J A0e(Hilt_WaDialogFragment hilt_WaDialogFragment) {
        return ((C51112Sw) ((AbstractC51092Su) hilt_WaDialogFragment.generatedComponent())).A0Y;
    }

    public static AnonymousClass1NR A0f(Throwable th) {
        return new AnonymousClass1NR(new SSLException(th), (byte) 80);
    }

    public static ArrayIndexOutOfBoundsException A0g(char c, long j) {
        StringBuilder sb = new StringBuilder(46);
        sb.append("Failed writing ");
        sb.append(c);
        sb.append(" at index ");
        sb.append(j);
        return new ArrayIndexOutOfBoundsException(sb.toString());
    }

    public static IllegalArgumentException A0h() {
        return new IllegalArgumentException();
    }

    public static IndexOutOfBoundsException A0i(int i, int i2) {
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return new IndexOutOfBoundsException(sb.toString());
    }

    public static NoSuchMethodError A0j(Iterator it) {
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        return new NoSuchMethodError();
    }

    public static Object A0k(Class cls, String str) {
        return cls.getDeclaredMethod(str, new Class[0]).invoke(null, new Object[0]);
    }

    public static Object A0l(String str) {
        return Class.forName(str).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
    }

    public static RuntimeException A0m() {
        C16700pc.A09();
        return new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public static String A0n(int i) {
        StringBuilder sb = new StringBuilder(54);
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(((long) i) + 4294967296L);
        return sb.toString();
    }

    public static String A0o(Object obj) {
        return Integer.toHexString(System.identityHashCode(obj));
    }

    public static String A0p(Object obj, StringBuilder sb) {
        sb.append(obj.getClass().getSimpleName());
        sb.append('@');
        return Integer.toHexString(System.identityHashCode(obj));
    }

    public static String A0q(String str) {
        return String.valueOf(str.substring(0, 1).toLowerCase());
    }

    public static String A0r(String str, Object obj) {
        String valueOf = String.valueOf(obj);
        if (valueOf.length() != 0) {
            return str.concat(valueOf);
        }
        return new String(str);
    }

    public static String A0s(String str, String str2, int i) {
        if (i != 0) {
            return str.concat(str2);
        }
        return new String(str);
    }

    public static String A0t(StringBuilder sb) {
        sb.append(']');
        return sb.toString();
    }

    public static String A0u(JSONObject jSONObject) {
        C16700pc.A0E(jSONObject, 0);
        String string = jSONObject.getString("variable");
        C16700pc.A0B(string);
        return string;
    }

    public static StringBuilder A0v(Object obj, String str) {
        String name = obj.getClass().getName();
        StringBuilder sb = new StringBuilder(name.length() + 62 + str.length());
        sb.append("Serializing ");
        sb.append(name);
        sb.append(" to a ");
        sb.append(str);
        return sb;
    }

    public static Throwable A0w(Throwable th) {
        return th.getCause() != null ? th.getCause() : th;
    }

    public static ByteBuffer A0x(int i) {
        return ByteBuffer.allocateDirect(i).order(ByteOrder.nativeOrder());
    }

    public static X509Certificate A0y(CertPath certPath, int i) {
        return (X509Certificate) certPath.getCertificates().get(i);
    }

    public static HashSet A0z(Collection collection) {
        HashSet hashSet = new HashSet(collection);
        hashSet.remove(C95674eA.A08);
        hashSet.remove(C95674eA.A03);
        hashSet.remove(C95674eA.A0B);
        hashSet.remove(C95674eA.A06);
        hashSet.remove(C95674eA.A07);
        hashSet.remove(C95674eA.A05);
        hashSet.remove(C95674eA.A0A);
        hashSet.remove(C95674eA.A02);
        hashSet.remove(C95674eA.A0C);
        hashSet.remove(C95674eA.A09);
        return hashSet;
    }

    public static Iterator A10(Map map) {
        return map.keySet().iterator();
    }

    public static AbstractC114165Kl A11() {
        ThreadLocal threadLocal = C94654cI.A00;
        AbstractC114165Kl r0 = (AbstractC114165Kl) threadLocal.get();
        if (r0 != null) {
            return r0;
        }
        C114225Kr r02 = new C114225Kr(Thread.currentThread());
        threadLocal.set(r02);
        return r02;
    }

    public static AnonymousClass1TK A12(String str) {
        return new AnonymousClass1TK(str);
    }

    public static AnonymousClass1TK A13(String str) {
        return new AnonymousClass1TK(str).A0B();
    }

    public static C93524aJ A14(C93524aJ r1, C93524aJ r2, C93524aJ[] r3, int i, int i2) {
        JavaCurve25519Provider.A05(r1, r2, ((i ^ i2) - 1) >>> 31);
        return r3[i2];
    }

    public static void A15(long j, byte[] bArr, int i, int i2) {
        bArr[i2] = (byte) ((int) (j >> i));
    }

    public static void A16(IDxAListenerShape6S0100000_2_I1 iDxAListenerShape6S0100000_2_I1) {
        ((AbstractActivityC13850kR) iDxAListenerShape6S0100000_2_I1.A00).A1k();
    }

    public static void A17(C93844ap r1, AnonymousClass5X6 r2) {
        r2.AA6(new C100614mC(r1));
    }

    public static void A18(AnonymousClass01J r1, WaDialogFragment waDialogFragment) {
        waDialogFragment.A00 = (AnonymousClass182) r1.A94.get();
        waDialogFragment.A01 = (AnonymousClass180) r1.ALt.get();
    }

    public static void A19(AnonymousClass01J r1, FAQLearnMoreDialogFragment fAQLearnMoreDialogFragment) {
        fAQLearnMoreDialogFragment.A02 = (AnonymousClass19M) r1.A6R.get();
        fAQLearnMoreDialogFragment.A00 = (AnonymousClass12P) r1.A0H.get();
        fAQLearnMoreDialogFragment.A03 = (C252018m) r1.A7g.get();
        fAQLearnMoreDialogFragment.A01 = (AnonymousClass018) r1.ANb.get();
    }

    public static void A1A(AnonymousClass01J r1, GroupSettingsActivity.AdminSettingsDialogFragment adminSettingsDialogFragment) {
        adminSettingsDialogFragment.A0E = (C14860mA) r1.ANU.get();
        adminSettingsDialogFragment.A0C = (C20660w7) r1.AIB.get();
        adminSettingsDialogFragment.A01 = (C15450nH) r1.AII.get();
        adminSettingsDialogFragment.A02 = (C15550nR) r1.A45.get();
        adminSettingsDialogFragment.A05 = (AnonymousClass018) r1.ANb.get();
        adminSettingsDialogFragment.A07 = (C15650ng) r1.A4m.get();
        adminSettingsDialogFragment.A0A = (C20710wC) r1.A8m.get();
        adminSettingsDialogFragment.A0D = (C22140ya) r1.ALE.get();
        adminSettingsDialogFragment.A06 = (C21320xE) r1.A4Y.get();
        adminSettingsDialogFragment.A08 = (C15600nX) r1.A8x.get();
        adminSettingsDialogFragment.A03 = (C18640sm) r1.A3u.get();
    }

    public static void A1B(AnonymousClass01J r1, GroupSettingsActivity.AdminSettingsDialogFragment adminSettingsDialogFragment) {
        adminSettingsDialogFragment.A04 = (C14830m7) r1.ALb.get();
        adminSettingsDialogFragment.A00 = (C14900mE) r1.A8X.get();
    }

    public static void A1C(ActivityC13810kN r3) {
        r3.Adr(new Object[0], R.string.alert, R.string.permission_storage_need_access);
    }

    public static void A1D(AbstractC28551Oa r2) {
        if (r2.A06 == 2) {
            r2.A06 = 0;
        }
    }

    public static void A1E(Appendable appendable, int i) {
        appendable.append("\\u");
        appendable.append("0123456789ABCDEF".charAt((i >> 12) & 15));
        appendable.append("0123456789ABCDEF".charAt((i >> 8) & 15));
        appendable.append("0123456789ABCDEF".charAt((i >> 4) & 15));
        appendable.append("0123456789ABCDEF".charAt((i >> 0) & 15));
    }

    public static void A1F(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[0] = obj;
        objArr[1] = obj2;
        objArr[2] = obj3;
        objArr[3] = obj4;
    }

    public static void A1G(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[9] = obj;
        objArr[10] = obj2;
        objArr[11] = obj3;
        objArr[12] = obj4;
    }

    public static void A1H(Object obj, Object obj2, Object obj3, Object obj4, Object[] objArr) {
        objArr[13] = obj;
        objArr[14] = obj2;
        objArr[15] = obj3;
        objArr[16] = obj4;
    }

    public static void A1I(Object obj, Object obj2, Object obj3, Object[] objArr) {
        objArr[17] = obj;
        objArr[18] = obj2;
        objArr[19] = obj3;
    }

    public static void A1J(Object obj, Object obj2, Object[] objArr) {
        objArr[0] = obj;
        objArr[1] = obj2;
    }

    public static void A1K(Object obj, String str, String str2, StringBuilder sb) {
        sb.append(str);
        sb.append(obj);
        sb.append(str2);
    }

    public static void A1L(Object obj, AbstractMap abstractMap) {
        abstractMap.put(obj, "SHA384WITHRSA");
        abstractMap.put(AnonymousClass1TJ.A2G, "SHA512WITHRSA");
        abstractMap.put(AbstractC116985Xr.A0M, "GOST3411WITHGOST3410");
        abstractMap.put(AbstractC116985Xr.A0L, "GOST3411WITHECGOST3410");
        abstractMap.put(AbstractC116935Xm.A0H, "GOST3411-2012-256WITHECGOST3410-2012-256");
        abstractMap.put(AbstractC116935Xm.A0I, "GOST3411-2012-512WITHECGOST3410-2012-512");
        abstractMap.put(AbstractC116945Xn.A03, "SHA1WITHPLAIN-ECDSA");
        abstractMap.put(AbstractC116945Xn.A04, "SHA224WITHPLAIN-ECDSA");
        abstractMap.put(AbstractC116945Xn.A05, "SHA256WITHPLAIN-ECDSA");
        abstractMap.put(AbstractC116945Xn.A06, "SHA384WITHPLAIN-ECDSA");
        abstractMap.put(AbstractC116945Xn.A07, "SHA512WITHPLAIN-ECDSA");
        abstractMap.put(AbstractC116945Xn.A02, "RIPEMD160WITHPLAIN-ECDSA");
        abstractMap.put(AbstractC116955Xo.A0C, "SHA1WITHCVC-ECDSA");
        abstractMap.put(AbstractC116955Xo.A0D, "SHA224WITHCVC-ECDSA");
        abstractMap.put(AbstractC116955Xo.A0E, "SHA256WITHCVC-ECDSA");
        abstractMap.put(AbstractC116955Xo.A0F, "SHA384WITHCVC-ECDSA");
    }

    public static void A1M(Object obj, AbstractMap abstractMap) {
        abstractMap.put(obj, "SHA512WITHCVC-ECDSA");
        abstractMap.put(AbstractC116875Xg.A00, "XMSS");
        abstractMap.put(AbstractC116875Xg.A01, "XMSSMT");
        abstractMap.put(new AnonymousClass1TK("1.2.840.113549.1.1.4"), "MD5WITHRSA");
        abstractMap.put(new AnonymousClass1TK("1.2.840.113549.1.1.2"), "MD2WITHRSA");
        abstractMap.put(new AnonymousClass1TK("1.2.840.10040.4.3"), "SHA1WITHDSA");
        abstractMap.put(AbstractC72433ea.A0X, "SHA1WITHECDSA");
        abstractMap.put(AbstractC72433ea.A0Z, "SHA224WITHECDSA");
        abstractMap.put(AbstractC72433ea.A0a, "SHA256WITHECDSA");
        abstractMap.put(AbstractC72433ea.A0b, "SHA384WITHECDSA");
        abstractMap.put(AbstractC72433ea.A0c, "SHA512WITHECDSA");
        abstractMap.put(AnonymousClass1TW.A0B, "SHA1WITHRSA");
        abstractMap.put(AnonymousClass1TW.A05, "SHA1WITHDSA");
        abstractMap.put(AnonymousClass1TY.A01, "SHA224WITHDSA");
        abstractMap.put(AnonymousClass1TY.A02, "SHA256WITHDSA");
    }

    public static void A1N(String str, String str2, String str3, StringBuffer stringBuffer) {
        stringBuffer.append("    ");
        stringBuffer.append(str);
        stringBuffer.append(":");
        stringBuffer.append(str2);
        stringBuffer.append("    ");
        stringBuffer.append("    ");
        stringBuffer.append(str3);
        stringBuffer.append(str2);
    }

    public static void A1O(String str, StringBuilder sb, int i, int i2) {
        sb.append(str.substring(i, i2));
    }

    public static void A1P(Method method, AbstractCollection abstractCollection, AbstractMap abstractMap, AbstractMap abstractMap2) {
        abstractMap.put(method.getName(), method);
        if (method.getParameterTypes().length == 0) {
            abstractMap2.put(method.getName(), method);
            if (method.getName().startsWith("get")) {
                abstractCollection.add(method.getName());
            }
        }
    }

    public static void A1Q(ByteBuffer byteBuffer, int i) {
        byteBuffer.put(AnonymousClass3JS.A06(i));
    }

    public static void A1R(byte[] bArr, int i, long j) {
        bArr[28] = (byte) i;
        bArr[29] = (byte) ((int) (j >> 1));
        bArr[30] = (byte) ((int) (j >> 9));
        bArr[31] = (byte) ((int) (j >> 17));
    }

    public static void A1S(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        bArr2[i3] = (byte) (i2 ^ bArr[i]);
    }

    public static void A1T(int[] iArr) {
        iArr[1] = 0;
        iArr[2] = 0;
        iArr[3] = 0;
        iArr[4] = 0;
        iArr[5] = 0;
        iArr[6] = 0;
        iArr[7] = 0;
        iArr[8] = 0;
        iArr[9] = 0;
    }

    public static boolean A1U(Appendable appendable, boolean z) {
        if (!z) {
            return true;
        }
        appendable.append(',');
        return z;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: java.lang.Class */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: java.lang.Class[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static boolean A1V(Class cls, Object obj) {
        Class cls2 = Boolean.TYPE;
        cls.getMethod("peekLong", obj, cls2);
        cls.getMethod("pokeLong", obj, Long.TYPE, cls2);
        Class cls3 = Integer.TYPE;
        cls.getMethod("pokeInt", obj, cls3, cls2);
        cls.getMethod("peekInt", obj, cls2);
        cls.getMethod("pokeByte", obj, Byte.TYPE);
        cls.getMethod("peekByte", obj);
        cls.getMethod("pokeByteArray", obj, byte[].class, cls3, cls3);
        cls.getMethod("peekByteArray", obj, byte[].class, cls3, cls3);
        return true;
    }

    public static boolean A1W(boolean[] zArr) {
        zArr[0] = false;
        zArr[1] = false;
        zArr[2] = false;
        return false;
    }

    public static byte[] A1X(ByteBuffer byteBuffer) {
        byte[] bArr = new byte[2];
        byteBuffer.get(bArr);
        byte[] bArr2 = new byte[AnonymousClass3JS.A01(bArr)];
        byteBuffer.get(bArr2);
        return bArr2;
    }

    public static int[] A1Y() {
        return new int[]{1, 2, 3, 4, 5, 6, 7};
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.lang.Class */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: java.lang.Class[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: java.lang.Class[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static Class[] A1Z(Class cls, Object obj) {
        cls.getMethod("getByte", obj);
        cls.getMethod("putByte", obj, Byte.TYPE);
        cls.getMethod("getInt", obj);
        cls.getMethod("putInt", obj, Integer.TYPE);
        return new Class[]{obj};
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Class[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static Class[] A1a(Object obj, int i) {
        Class[] clsArr = new Class[i];
        clsArr[0] = Object.class;
        clsArr[1] = obj;
        return clsArr;
    }

    public static String[] A1b() {
        return new String[]{"unspecified", "keyCompromise", "cACompromise", "affiliationChanged", "superseded", "cessationOfOperation", "certificateHold", "unknown", "removeFromCRL", "privilegeWithdrawn", "aACompromise"};
    }
}
