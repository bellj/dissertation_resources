package X;

import android.content.Context;
import com.facebook.redex.IDxObserverShape5S0100000_3_I1;

/* renamed from: X.5ty  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127115ty {
    public String A00;
    public boolean A01;

    public static void A00(Context context, AnonymousClass017 r2, int i) {
        String string = context.getString(i);
        C127115ty r0 = new C127115ty();
        r0.A00 = string;
        r2.A0B(r0);
    }

    public static void A01(AnonymousClass017 r2) {
        C127115ty r1 = new C127115ty();
        r1.A01 = true;
        r2.A0B(r1);
    }

    public static void A02(IDxObserverShape5S0100000_3_I1 iDxObserverShape5S0100000_3_I1, Object obj) {
        ActivityC13810kN r1 = (ActivityC13810kN) iDxObserverShape5S0100000_3_I1.A00;
        C127115ty r2 = (C127115ty) obj;
        r1.AaN();
        if (!r2.A01) {
            r1.A2P(r2.A00);
        }
    }
}
