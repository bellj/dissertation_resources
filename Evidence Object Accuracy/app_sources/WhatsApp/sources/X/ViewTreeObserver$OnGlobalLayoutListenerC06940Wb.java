package X;

import android.view.ViewTreeObserver;
import androidx.appcompat.widget.AppCompatSpinner;

/* renamed from: X.0Wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC06940Wb implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ C02400Cd A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC06940Wb(C02400Cd r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        C02400Cd r2 = this.A00;
        AppCompatSpinner appCompatSpinner = r2.A04;
        if (!AnonymousClass028.A0q(appCompatSpinner) || !appCompatSpinner.getGlobalVisibleRect(r2.A03)) {
            r2.dismiss();
            return;
        }
        r2.A02();
        ViewTreeObserver$OnGlobalLayoutListenerC06940Wb.super.Ade();
    }
}
