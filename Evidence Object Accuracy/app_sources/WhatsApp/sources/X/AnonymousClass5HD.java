package X;

import android.media.AudioTrack;

/* renamed from: X.5HD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5HD extends Thread {
    public final /* synthetic */ AudioTrack A00;
    public final /* synthetic */ C106534vr A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5HD(AudioTrack audioTrack, C106534vr r3) {
        super("ExoPlayer:AudioTrackReleaseThread");
        this.A01 = r3;
        this.A00 = audioTrack;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        try {
            AudioTrack audioTrack = this.A00;
            audioTrack.flush();
            audioTrack.release();
        } finally {
            this.A01.A0b.open();
        }
    }
}
