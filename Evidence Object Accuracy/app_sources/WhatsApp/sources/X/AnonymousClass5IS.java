package X;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/* renamed from: X.5IS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IS extends FutureTask {
    public final /* synthetic */ AbstractRunnableC14570le A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5IS(AbstractRunnableC14570le r1, Callable callable) {
        super(callable);
        this.A00 = r1;
    }

    @Override // java.util.concurrent.FutureTask
    public void done() {
        AbstractRunnableC14570le r2 = this.A00;
        if (r2.A02.isCancelled()) {
            r2.A00.A04(Boolean.TRUE);
        }
        r2.A01.countDown();
    }
}
