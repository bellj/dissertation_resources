package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.payments.ui.stepup.NoviReviewVideoSelfieActivity;
import java.util.ArrayList;

/* renamed from: X.5b4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117975b4 extends AnonymousClass015 {
    public int A00;
    public C1316663q A01;
    public C27691It A02 = C13000ix.A03();
    public String A03;
    public String A04;
    public ArrayList A05;
    public final Bundle A06;

    public C117975b4(Bundle bundle) {
        this.A06 = bundle;
    }

    public void A04(Context context, C126015sC r10) {
        int i = r10.A00;
        if (i == 0) {
            Bundle bundle = this.A06;
            String string = bundle.getString("video_selfie_challenge_id");
            AnonymousClass009.A05(string);
            this.A03 = string;
            String string2 = bundle.getString("disable_face_rec");
            AnonymousClass009.A05(string2);
            this.A04 = string2;
            ArrayList<String> stringArrayList = bundle.getStringArrayList("video_selfie_head_directions");
            AnonymousClass009.A05(stringArrayList);
            this.A05 = stringArrayList;
            C1316663q r0 = (C1316663q) bundle.getParcelable("step_up");
            this.A01 = r0;
            AnonymousClass009.A05(r0);
            this.A00 = bundle.getInt("step_up_origin_action", 1);
        } else if (i == 1) {
            C127485uZ r1 = new C127485uZ(0);
            r1.A02 = this.A05;
            this.A02.A0B(r1);
        } else if (i == 2) {
            C127485uZ r7 = new C127485uZ(1);
            String str = this.A03;
            String str2 = this.A04;
            ArrayList<String> arrayList = this.A05;
            C1316663q r3 = this.A01;
            int i2 = this.A00;
            Intent A0D = C12990iw.A0D(context, NoviReviewVideoSelfieActivity.class);
            AnonymousClass009.A05(str);
            A0D.putExtra("video_selfie_challenge_id", str);
            AnonymousClass009.A05(str2);
            A0D.putExtra("disable_face_rec", str2);
            AnonymousClass009.A05(r3);
            A0D.putExtra("step_up", r3);
            A0D.putExtra("step_up_origin_action", i2);
            AnonymousClass009.A05(arrayList);
            A0D.putStringArrayListExtra("video_selfie_head_directions", arrayList);
            r7.A01 = A0D;
            this.A02.A0B(r7);
        }
    }
}
