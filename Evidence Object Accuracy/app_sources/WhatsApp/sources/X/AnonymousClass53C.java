package X;

import com.whatsapp.backup.encryptedbackup.EncBackupViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.53C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass53C implements AnonymousClass1OI {
    public final /* synthetic */ EncBackupViewModel A00;

    public AnonymousClass53C(EncBackupViewModel encBackupViewModel) {
        this.A00 = encBackupViewModel;
    }

    @Override // X.AnonymousClass1OI
    public void APs(String str, int i, int i2, int i3, int i4) {
        EncBackupViewModel encBackupViewModel = this.A00;
        if (i == 0) {
            Log.i("EncBackupViewModel/successfully saved encryption key");
            encBackupViewModel.A07.A0A(-1);
            return;
        }
        Log.e("EncBackupViewModel/failed to save encryption key");
    }

    @Override // X.AnonymousClass1OI
    public void AWu() {
        EncBackupViewModel encBackupViewModel = this.A00;
        Log.i("EncBackupViewModel/successfully saved encryption key");
        encBackupViewModel.A07.A0A(-1);
    }
}
