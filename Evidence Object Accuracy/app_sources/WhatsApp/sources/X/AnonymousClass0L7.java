package X;

import android.icu.text.DecimalFormatSymbols;
import java.util.Locale;

/* renamed from: X.0L7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0L7 {
    public static DecimalFormatSymbols A00(Locale locale) {
        return DecimalFormatSymbols.getInstance(locale);
    }
}
