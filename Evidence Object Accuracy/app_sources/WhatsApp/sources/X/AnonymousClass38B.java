package X;

import android.content.Context;
import android.graphics.Bitmap;
import java.lang.ref.WeakReference;

/* renamed from: X.38B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38B extends AbstractC16350or {
    public int A00;
    public AnonymousClass018 A01;
    public AnonymousClass21U A02;
    public View$OnClickListenerC55222hz A03;
    public AnonymousClass21W A04;
    public WeakReference A05;
    public Bitmap[] A06;
    public boolean[] A07;
    public boolean[] A08;
    public final AnonymousClass1BI A09;

    public AnonymousClass38B(Context context, AnonymousClass018 r3, AnonymousClass1BI r4, AnonymousClass21U r5, View$OnClickListenerC55222hz r6, AnonymousClass21W r7, Bitmap[] bitmapArr, boolean[] zArr, boolean[] zArr2, int i) {
        this.A05 = C12970iu.A10(context);
        this.A02 = r5;
        this.A01 = r3;
        this.A04 = r7;
        this.A00 = i;
        this.A06 = bitmapArr;
        this.A08 = zArr;
        this.A07 = zArr2;
        this.A03 = r6;
        this.A09 = r4;
    }
}
