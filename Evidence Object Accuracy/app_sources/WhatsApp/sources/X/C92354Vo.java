package X;

import java.util.Collection;
import java.util.EnumSet;

/* renamed from: X.4Vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92354Vo {
    public AbstractC117035Xw A00;
    public C93904aw A01;
    public Collection A02 = C12960it.A0l();
    public EnumSet A03 = EnumSet.noneOf(EnumC87084Ad.class);

    public C92364Vp A00() {
        C93904aw r3;
        AbstractC117035Xw r4 = this.A00;
        if (r4 == null || (r3 = this.A01) == null) {
            C93224Zp r0 = C93224Zp.A01;
            if (r4 == null) {
                r4 = new AnonymousClass52M();
                this.A00 = r4;
            }
            r3 = this.A01;
            if (r3 == null) {
                r3 = r0.A00;
                this.A01 = r3;
            }
        }
        return new C92364Vp(r4, r3, this.A02, this.A03);
    }
}
