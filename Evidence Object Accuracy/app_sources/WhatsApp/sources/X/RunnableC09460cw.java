package X;

import android.view.View;

/* renamed from: X.0cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09460cw implements Runnable {
    public AnonymousClass0CP A00;
    public final /* synthetic */ AnonymousClass0XQ A01;

    public RunnableC09460cw(AnonymousClass0CP r1, AnonymousClass0XQ r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC011605p r0;
        AnonymousClass0XQ r2 = this.A01;
        AnonymousClass07H r1 = r2.A0A;
        if (!(r1 == null || (r0 = r1.A03) == null)) {
            r0.ASi(r1);
        }
        View view = (View) r2.A0C;
        if (!(view == null || view.getWindowToken() == null)) {
            AnonymousClass0CP r12 = this.A00;
            if (r12.A03()) {
                r2.A0H = r12;
            }
        }
        r2.A0F = null;
    }
}
