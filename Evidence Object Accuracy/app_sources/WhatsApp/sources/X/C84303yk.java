package X;

/* renamed from: X.3yk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84303yk extends AbstractC87994Dv {
    public final long A00;
    public final long A01;

    public C84303yk(long j, long j2) {
        this.A00 = j;
        this.A01 = j2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C84303yk r7 = (C84303yk) obj;
            if (!(this.A00 == r7.A00 && this.A01 == r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = Long.valueOf(this.A00);
        return C12960it.A06(Long.valueOf(this.A01), A1a);
    }
}
