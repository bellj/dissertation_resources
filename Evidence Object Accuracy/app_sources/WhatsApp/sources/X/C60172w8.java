package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.components.SelectionCheckView;
import com.whatsapp.ui.voip.MultiContactThumbnail;
import com.whatsapp.util.ViewOnClickCListenerShape17S0100000_I1;

/* renamed from: X.2w8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60172w8 extends AbstractC92184Uw {
    public final View.OnTouchListener A00 = new AnonymousClass2E0(0.15f, 0.15f, 0.15f, 0.15f);
    public final View A01;
    public final ImageView A02;
    public final ImageView A03;
    public final ImageView A04;
    public final ImageView A05;
    public final TextView A06;
    public final TextView A07;
    public final C15450nH A08;
    public final C28801Pb A09;
    public final CallsHistoryFragment A0A;
    public final SelectionCheckView A0B;
    public final C15550nR A0C;
    public final C15610nY A0D;
    public final AnonymousClass28F A0E = new C1102554v(this);
    public final AnonymousClass1J1 A0F;
    public final AnonymousClass1J1 A0G;
    public final AnonymousClass01d A0H;
    public final C14830m7 A0I;
    public final AnonymousClass018 A0J;
    public final C21250x7 A0K;
    public final C14850m9 A0L;
    public final C20710wC A0M;
    public final MultiContactThumbnail A0N;
    public final AbstractView$OnClickListenerC34281fs A0O = new ViewOnClickCListenerShape17S0100000_I1(this, 34);
    public final AnonymousClass19Z A0P;

    public C60172w8(View view, C15450nH r5, CallsHistoryFragment callsHistoryFragment, C15550nR r7, C15610nY r8, AnonymousClass1J1 r9, AnonymousClass1J1 r10, AnonymousClass01d r11, C14830m7 r12, AnonymousClass018 r13, C21250x7 r14, C14850m9 r15, C20710wC r16, AnonymousClass12F r17, AnonymousClass19Z r18) {
        this.A0I = r12;
        this.A0L = r15;
        this.A08 = r5;
        this.A0K = r14;
        this.A0P = r18;
        this.A0C = r7;
        this.A0H = r11;
        this.A0D = r8;
        this.A0J = r13;
        this.A0M = r16;
        this.A0G = r9;
        this.A0F = r10;
        this.A0A = callsHistoryFragment;
        this.A03 = C12970iu.A0K(view, R.id.contact_photo);
        C28801Pb r2 = new C28801Pb(view, r8, r17, (int) R.id.contact_name);
        this.A09 = r2;
        this.A07 = C12960it.A0I(view, R.id.date_time);
        this.A02 = C12970iu.A0K(view, R.id.call_type_icon);
        this.A06 = C12960it.A0I(view, R.id.count);
        this.A05 = C12970iu.A0K(view, R.id.voice_call);
        this.A04 = C12970iu.A0K(view, R.id.video_call);
        this.A0B = (SelectionCheckView) AnonymousClass028.A0D(view, R.id.selection_check);
        this.A01 = AnonymousClass028.A0D(view, R.id.call_row_container);
        MultiContactThumbnail multiContactThumbnail = (MultiContactThumbnail) AnonymousClass028.A0D(view, R.id.multi_contact_photo);
        this.A0N = multiContactThumbnail;
        AnonymousClass028.A0a(multiContactThumbnail, 2);
        C27531Hw.A06(r2.A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x017c, code lost:
        if (r7 == 5) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0063, code lost:
        if (r4 != null) goto L_0x0065;
     */
    @Override // X.AbstractC92184Uw
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(int r23) {
        /*
        // Method dump skipped, instructions count: 584
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60172w8.A00(int):void");
    }
}
