package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: X.1iR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC35641iR extends Handler {
    public final /* synthetic */ C21370xJ A00;
    public final /* synthetic */ C19990v2 A01;
    public final /* synthetic */ C21380xK A02;
    public final /* synthetic */ AnonymousClass12H A03;
    public final /* synthetic */ C18470sV A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC35641iR(Looper looper, C21370xJ r2, C19990v2 r3, C21380xK r4, AnonymousClass12H r5, C18470sV r6) {
        super(looper);
        this.A02 = r4;
        this.A00 = r2;
        this.A03 = r5;
        this.A04 = r6;
        this.A01 = r3;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        C21370xJ r2;
        AbstractC14640lm r1;
        int i = message.what;
        if (i == 1) {
            r2 = this.A00;
            r1 = AbstractC14640lm.A01((String) message.obj);
            AnonymousClass009.A05(r1);
        } else if (i == 2) {
            r1 = AbstractC14640lm.A01((String) message.obj);
            AnonymousClass009.A05(r1);
            this.A03.A06(r1);
            r2 = this.A00;
        } else if (i == 8) {
            C18470sV r0 = this.A04;
            r0.A09();
            r0.A07.clear();
            AnonymousClass12H r3 = this.A03;
            r3.A06(AnonymousClass1VX.A00);
            for (AbstractC14640lm r12 : this.A01.A0A()) {
                r3.A06(r12);
                this.A00.A03(r12, true);
            }
            return;
        } else if (i == 9) {
            C18470sV r02 = this.A04;
            r02.A09();
            r02.A07.clear();
            AnonymousClass12H r6 = this.A03;
            r6.A06(AnonymousClass1VX.A00);
            C19990v2 r5 = this.A01;
            for (AbstractC14640lm r22 : r5.A0A()) {
                if (!C15380n4.A0K(r22)) {
                    synchronized (r5) {
                        if (r22 != null) {
                            r5.A0B().remove(r22);
                        }
                    }
                    C21370xJ r13 = this.A00;
                    r13.A07.A0C(r22);
                    C21320xE r03 = r13.A06;
                    r03.A05();
                    r03.A08(r22);
                } else {
                    this.A00.A03(r22, true);
                }
                r6.A06(r22);
            }
            return;
        } else {
            return;
        }
        r2.A03(r1, false);
    }
}
