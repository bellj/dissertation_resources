package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: X.3eq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72583eq extends AnimatorListenerAdapter {
    public final /* synthetic */ View A00;
    public final /* synthetic */ Runnable A01;

    public C72583eq(View view, Runnable runnable) {
        this.A00 = view;
        this.A01 = runnable;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.setVisibility(8);
        Runnable runnable = this.A01;
        if (runnable != null) {
            runnable.run();
        }
    }
}
