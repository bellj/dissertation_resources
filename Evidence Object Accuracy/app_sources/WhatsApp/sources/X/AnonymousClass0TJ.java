package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;

/* renamed from: X.0TJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TJ {
    public static final String A00 = C06390Tk.A01("Alarms");

    public static void A00(Context context, AnonymousClass022 r7, String str, long j) {
        int A01;
        WorkDatabase workDatabase = r7.A04;
        AbstractC12580i9 A08 = workDatabase.A08();
        AnonymousClass0PC AH2 = A08.AH2(str);
        if (AH2 != null) {
            A01 = AH2.A00;
            A01(context, str, A01);
        } else {
            AnonymousClass0P1 r2 = new AnonymousClass0P1(workDatabase);
            synchronized (AnonymousClass0P1.class) {
                A01 = r2.A01("next_alarm_manager_id");
            }
            A08.AJ1(new AnonymousClass0PC(str, A01));
        }
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        int i = Build.VERSION.SDK_INT;
        int i2 = 134217728;
        if (i >= 23) {
            i2 = 201326592;
        }
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        PendingIntent service = PendingIntent.getService(context, A01, intent, i2);
        if (alarmManager == null) {
            return;
        }
        if (i >= 19) {
            alarmManager.setExact(0, j, service);
        } else {
            alarmManager.set(0, j, service);
        }
    }

    public static void A01(Context context, String str, int i) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        int i2 = 536870912;
        if (Build.VERSION.SDK_INT >= 23) {
            i2 = 603979776;
        }
        PendingIntent service = PendingIntent.getService(context, i, intent, i2);
        if (service != null && alarmManager != null) {
            C06390Tk.A00().A02(A00, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", str, Integer.valueOf(i)), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }
}
