package X;

import java.util.concurrent.Executor;

/* renamed from: X.5Kv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class ExecutorC114265Kv extends AbstractC114195Ko implements Executor {
    public static final AbstractC10990fX A00;
    public static final ExecutorC114265Kv A01 = new ExecutorC114265Kv();

    @Override // X.AbstractC10990fX, java.lang.Object
    public String toString() {
        return "Dispatchers.IO";
    }

    static {
        C114175Km r3 = C114175Km.A00;
        int i = C88594Gg.A00;
        int i2 = 64;
        if (64 < i) {
            i2 = i;
        }
        A00 = r3.A02((int) C88274Ey.A00("kotlinx.coroutines.io.parallelism", (long) i2, (long) 1, (long) Integer.MAX_VALUE));
    }

    @Override // X.AbstractC10990fX
    public AbstractC10990fX A02(int i) {
        return C114175Km.A00.A02(i);
    }

    @Override // X.AbstractC10990fX
    public void A04(Runnable runnable, AnonymousClass5X4 r3) {
        A00.A04(runnable, r3);
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        A04(runnable, C112775Er.A00);
    }
}
