package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.0zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22890zo implements AbstractC18270sB {
    public AnonymousClass21R A00;
    public final C239613r A01;
    public final C16170oZ A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C14820m6 A05;
    public final C15650ng A06;
    public final C16490p7 A07;
    public final AnonymousClass132 A08;
    public final C21400xM A09;
    public final C239713s A0A;
    public final C20740wF A0B;
    public final C239813t A0C;
    public final AbstractC14440lR A0D;

    public C22890zo(C239613r r1, C16170oZ r2, C18640sm r3, C14830m7 r4, C14820m6 r5, C15650ng r6, C16490p7 r7, AnonymousClass132 r8, C21400xM r9, C239713s r10, C20740wF r11, C239813t r12, AbstractC14440lR r13) {
        this.A04 = r4;
        this.A01 = r1;
        this.A0D = r13;
        this.A02 = r2;
        this.A0A = r10;
        this.A06 = r6;
        this.A0B = r11;
        this.A08 = r8;
        this.A05 = r5;
        this.A09 = r9;
        this.A07 = r7;
        this.A03 = r3;
        this.A0C = r12;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00(X.AbstractC15340mz r12) {
        /*
        // Method dump skipped, instructions count: 236
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22890zo.A00(X.0mz):boolean");
    }

    @Override // X.AbstractC18270sB
    public void ARF() {
        C16490p7 r0 = this.A07;
        r0.A04();
        if (r0.A01) {
            Iterator it = this.A08.A01().iterator();
            while (it.hasNext()) {
                AbstractC15340mz r9 = (AbstractC15340mz) it.next();
                long A00 = this.A04.A00();
                long j = r9.A0I;
                if (10800000 + j < A00 && j + 86400000 >= A00 && A00(r9)) {
                    SharedPreferences sharedPreferences = this.A05.A00;
                    if (sharedPreferences.getLong("last_unsent_notification_time", 0) + 86400000 < A00) {
                        Log.i("Posting notification about unsent messages");
                        sharedPreferences.edit().putLong("last_unsent_notification_time", A00).apply();
                        C239813t r6 = this.A0C;
                        Context context = r6.A00.A00;
                        String string = context.getString(R.string.messages_failed_notification_title);
                        String string2 = context.getString(R.string.messages_failed_notification_message);
                        PendingIntent A002 = AnonymousClass1UY.A00(context, 1, C14960mK.A02(context), 0);
                        C005602s A003 = C22630zO.A00(context);
                        A003.A0J = "failure_notifications@1";
                        A003.A0B(string);
                        A003.A05(System.currentTimeMillis());
                        A003.A02(3);
                        A003.A0D(true);
                        A003.A0A(string);
                        A003.A09(string2);
                        A003.A09 = A002;
                        C18360sK.A01(A003, R.drawable.notifybar);
                        r6.A01.A03(6, A003.A01());
                        r6.A02 = true;
                        return;
                    }
                    return;
                }
            }
        }
    }
}
