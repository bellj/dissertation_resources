package X;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.38m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627638m extends AbstractC16350or {
    public long A00;
    public ProgressDialog A01;
    public String A02;
    public String A03;
    public final AnonymousClass10G A04;
    public final C18790t3 A05;
    public final AbstractC32851cq A06 = new AnonymousClass3WY(this);
    public final C17050qB A07;
    public final C14820m6 A08;
    public final AnonymousClass018 A09;
    public final C14950mJ A0A;
    public final AnonymousClass1MJ A0B;
    public final AnonymousClass11G A0C;
    public final AnonymousClass23Q A0D;
    public final C254819o A0E;
    public final C252018m A0F;
    public final C22650zQ A0G;
    public final String A0H;
    public final String A0I;
    public final String A0J;
    public final WeakReference A0K;
    public final List A0L;
    public final Uri[] A0M;

    public C627638m(AnonymousClass10G r2, ActivityC13810kN r3, C18790t3 r4, C17050qB r5, C14820m6 r6, AnonymousClass018 r7, C14950mJ r8, AnonymousClass1MJ r9, AnonymousClass11G r10, AnonymousClass23Q r11, C254819o r12, C252018m r13, C22650zQ r14, String str, String str2, String str3, List list, Uri[] uriArr) {
        this.A0K = C12970iu.A10(r3);
        this.A05 = r4;
        this.A0G = r14;
        this.A0A = r8;
        this.A0F = r13;
        this.A09 = r7;
        this.A04 = r2;
        this.A07 = r5;
        this.A0C = r10;
        this.A08 = r6;
        this.A0E = r12;
        this.A0D = r11;
        this.A0H = str;
        this.A0J = str2;
        this.A0L = list;
        this.A0I = str3;
        this.A0M = uriArr;
        this.A0B = r9;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        Object obj;
        Throwable e;
        String str;
        String str2;
        HttpURLConnection httpURLConnection;
        String A0n;
        C18790t3 r7;
        JSONArray jSONArray;
        int length;
        Context context = (Context) this.A0K.get();
        if (context == null) {
            return null;
        }
        C14950mJ r2 = this.A0A;
        long A02 = r2.A02();
        this.A03 = Environment.getExternalStorageState();
        if (this.A07.A03(this.A06)) {
            this.A00 = r2.A01();
        }
        Pair A00 = this.A0E.A00();
        AnonymousClass10G r8 = this.A04;
        String str3 = this.A0H;
        String str4 = this.A0J;
        C91934Tu r4 = null;
        long j = this.A00;
        String str5 = this.A03;
        List list = this.A0L;
        String A04 = r8.A00.A04(context, A00, str3, str4, null, str5, list, AnonymousClass4EN.A00(this.A0B), j, A02, true, true);
        this.A02 = A04;
        Log.i(C12960it.A0d(A04, C12960it.A0k("searchSupportTask/doInBackground/debugInfo: ")));
        try {
            Uri.Builder A01 = this.A0F.A01();
            A01.appendPath("client_search.php");
            A01.appendQueryParameter("platform", "android");
            AnonymousClass018 r72 = this.A09;
            A01.appendQueryParameter("lg", r72.A06());
            A01.appendQueryParameter("lc", r72.A05());
            if (this.A0G.A04()) {
                str = "1";
            } else {
                str = "0";
            }
            A01.appendQueryParameter("eea", str);
            str2 = this.A0I;
            A01.appendQueryParameter("query", str2);
            A01.appendQueryParameter("manufacturer", Build.MANUFACTURER);
            A01.appendQueryParameter("os_version", Build.VERSION.RELEASE);
            A01.appendQueryParameter("ccode", this.A08.A0B());
            A01.appendQueryParameter("app_version", "2.22.17.70");
            A01.appendQueryParameter((String) A00.first, (String) A00.second);
            URLConnection openConnection = new URL(A01.toString()).openConnection();
            openConnection.setConnectTimeout(30000);
            openConnection.setReadTimeout(30000);
            httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            A0n = C12990iw.A0n();
            StringBuilder A0h = C12960it.A0h();
            A0h.append("multipart/form-data; boundary=");
            httpURLConnection.setRequestProperty("Content-Type", C12960it.A0d(A0n, A0h));
            r7 = this.A05;
            obj = null;
        } catch (IOException | JSONException e2) {
            e = e2;
            obj = null;
        }
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new AnonymousClass23V(r7, httpURLConnection.getOutputStream(), null, 20));
            try {
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append("--");
                A0h2.append(A0n);
                bufferedOutputStream.write(C12960it.A0d("\r\n", A0h2).getBytes());
                bufferedOutputStream.write("Content-Disposition: form-data; name=\"debug_info\"\r\n\r\n".getBytes());
                bufferedOutputStream.write(this.A02.getBytes());
                StringBuilder A0h3 = C12960it.A0h();
                A0h3.append("\r\n--");
                A0h3.append(A0n);
                bufferedOutputStream.write(C12960it.A0d("--\r\n", A0h3).getBytes());
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
                AnonymousClass1oJ r11 = new AnonymousClass1oJ(r7, httpURLConnection.getInputStream(), null, 20);
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(r11));
                    StringBuilder A0h4 = C12960it.A0h();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        A0h4.append(readLine);
                    }
                    String obj2 = A0h4.toString();
                    if (!TextUtils.isEmpty(obj2) && (length = (jSONArray = new JSONArray(obj2)).length()) != 0) {
                        ArrayList A0w = C12980iv.A0w(length);
                        ArrayList A0w2 = C12980iv.A0w(length);
                        ArrayList A0w3 = C12980iv.A0w(length);
                        ArrayList A0w4 = C12980iv.A0w(length);
                        for (int i = 0; i < length; i++) {
                            JSONObject optJSONObject = jSONArray.optJSONObject(i);
                            A0w.add(optJSONObject.getString("title"));
                            A0w2.add(optJSONObject.getString("description"));
                            A0w3.add(optJSONObject.getString("url"));
                            A0w4.add(optJSONObject.getString("id"));
                        }
                        ArrayList A0l = C12960it.A0l();
                        Uri[] uriArr = this.A0M;
                        for (Uri uri : uriArr) {
                            if (uri != null) {
                                A0l.add(uri);
                            }
                        }
                        r4 = new C91934Tu(str2, this.A02, A0w, A0w2, A0w3, A0w4, A0l, list, length);
                    }
                    bufferedReader.close();
                    r11.close();
                    return r4;
                } catch (Throwable th) {
                    try {
                        r11.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                try {
                    bufferedOutputStream.close();
                } catch (Throwable unused2) {
                }
                throw th2;
            }
        } catch (IOException | JSONException e3) {
            e = e3;
            Log.e(C12960it.A0b("searchSupportTask/doInBackground/error: ", e), e);
            return obj;
        }
    }
}
