package X;

import android.text.TextUtils;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.5fT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC120015fT extends AnonymousClass19V {
    public final String A00;
    public final String A01;

    public AbstractC120015fT(C18790t3 r14, C14820m6 r15, C14850m9 r16, AnonymousClass18L r17, AnonymousClass01H r18, String str, String str2, String str3, Map map, AnonymousClass01N r23, AnonymousClass01N r24, long j) {
        super(r14, r15, r16, r17, r18, str, map, r23, r24, j);
        this.A00 = str2;
        this.A01 = str3;
    }

    @Override // X.AnonymousClass19V
    public void A02(JSONObject jSONObject) {
        JSONObject A0a = C117295Zj.A0a();
        A04(A0a);
        jSONObject.put("variables", A0a.toString());
    }

    public String A03() {
        return "version";
    }

    public void A04(JSONObject jSONObject) {
        jSONObject.put("app_id", this.A00);
        jSONObject.put(A03(), "8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666");
        String str = this.A01;
        if (TextUtils.isEmpty(str)) {
            str = "{}";
        }
        jSONObject.put("params", str);
    }
}
