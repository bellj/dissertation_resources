package X;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/* renamed from: X.4eo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C96074eo implements Application.ActivityLifecycleCallbacks {
    public final /* synthetic */ C63733Cs A00;

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
    }

    public C96074eo(C63733Cs r1) {
        this.A00 = r1;
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        C63733Cs r1 = this.A00;
        if (activity == r1.A01.get()) {
            r1.A00();
        }
    }
}
