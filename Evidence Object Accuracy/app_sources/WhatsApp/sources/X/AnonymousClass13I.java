package X;

import com.whatsapp.jid.Jid;
import java.util.List;
import org.json.JSONObject;

/* renamed from: X.13I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13I extends AnonymousClass13E {
    public final C15570nT A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final AnonymousClass01d A03;
    public final C16590pI A04;
    public final C19990v2 A05;
    public final C15620nZ A06;
    public final AnonymousClass13F A07;
    public final AnonymousClass13H A08;
    public final C16630pM A09;
    public final C15860o1 A0A;

    public AnonymousClass13I(C15570nT r1, C15550nR r2, C15610nY r3, AnonymousClass01d r4, C16590pI r5, C19990v2 r6, C15620nZ r7, AnonymousClass13F r8, AnonymousClass13H r9, C16630pM r10, C15860o1 r11) {
        this.A04 = r5;
        this.A08 = r9;
        this.A00 = r1;
        this.A05 = r6;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A0A = r11;
        this.A09 = r10;
        this.A06 = r7;
        this.A07 = r8;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004a, code lost:
        if (r0.isBlocked() != false) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1m3 A01(X.AbstractC15340mz r7, X.C15430nF r8) {
        /*
            r6 = this;
            X.1IS r0 = r7.A0z
            X.0lm r4 = r0.A00
            X.AnonymousClass009.A05(r4)
            X.0o1 r1 = r6.A0A
            boolean r0 = r1.A0S(r4)
            r3 = 0
            if (r0 != 0) goto L_0x004c
            int r5 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r5 < r0) goto L_0x004d
            java.lang.String r0 = r4.getRawString()
            X.1da r0 = r1.A08(r0)
            X.1dj r0 = (X.C33271dj) r0
            java.lang.String r1 = r0.A0C()
            X.01d r0 = r6.A03
            android.app.NotificationManager r2 = r0.A08()
            if (r2 == 0) goto L_0x004d
            android.app.NotificationChannel r1 = r2.getNotificationChannel(r1)
            if (r1 == 0) goto L_0x004d
            int r0 = r1.getImportance()
            if (r0 == 0) goto L_0x004c
            r0 = 28
            if (r5 < r0) goto L_0x004d
            java.lang.String r0 = r1.getGroup()
            android.app.NotificationChannelGroup r0 = r2.getNotificationChannelGroup(r0)
            if (r0 == 0) goto L_0x004d
            boolean r0 = r0.isBlocked()
            if (r0 == 0) goto L_0x004d
        L_0x004c:
            return r3
        L_0x004d:
            X.0v2 r0 = r6.A05
            boolean r0 = r0.A0E(r4)
            if (r0 != 0) goto L_0x004c
            org.json.JSONObject r1 = r6.A03(r7, r8)
            if (r1 == 0) goto L_0x004c
            java.lang.String r0 = r6.A00()
            X.1m3 r3 = new X.1m3
            r3.<init>(r0, r1)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13I.A01(X.0mz, X.0nF):X.1m3");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x004e */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.lang.CharSequence] */
    /* JADX WARN: Type inference failed for: r3v2, types: [android.text.SpannableStringBuilder] */
    /* JADX WARN: Type inference failed for: r0v6, types: [X.13H] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String A02(java.lang.String r5, java.util.List r6) {
        /*
            r4 = this;
            java.lang.String r2 = X.AbstractC32741cf.A02(r5)
            X.01d r1 = r4.A03
            X.0pM r0 = r4.A09
            java.lang.CharSequence r3 = X.C42971wC.A03(r1, r0, r2)
            if (r6 == 0) goto L_0x004e
            boolean r0 = r6.isEmpty()
            if (r0 != 0) goto L_0x004e
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 != 0) goto L_0x004e
            android.text.SpannableStringBuilder r3 = android.text.SpannableStringBuilder.valueOf(r3)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            X.580 r1 = new X.580
            r1.<init>(r2)
            X.13H r0 = r4.A08
            r0.A03(r3, r1, r6)
            java.util.Comparator r0 = java.util.Collections.reverseOrder()
            java.util.Collections.sort(r2, r0)
            java.util.Iterator r2 = r2.iterator()
        L_0x0038:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x004e
            java.lang.Object r0 = r2.next()
            java.lang.Number r0 = (java.lang.Number) r0
            int r1 = r0.intValue()
            int r0 = r1 + 1
            r3.delete(r1, r0)
            goto L_0x0038
        L_0x004e:
            java.lang.CharSequence r0 = X.AnonymousClass1US.A01(r3)
            if (r0 != 0) goto L_0x0056
            r0 = 0
            return r0
        L_0x0056:
            java.lang.String r0 = r0.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13I.A02(java.lang.String, java.util.List):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0129 A[Catch: JSONException -> 0x017b, TryCatch #0 {JSONException -> 0x017b, blocks: (B:3:0x0001, B:5:0x0010, B:7:0x0014, B:9:0x0018, B:11:0x0020, B:13:0x0025, B:16:0x0031, B:17:0x0047, B:20:0x0053, B:23:0x0061, B:26:0x0067, B:27:0x007b, B:29:0x007f, B:31:0x0083, B:33:0x0087, B:34:0x008d, B:35:0x0094, B:37:0x0098, B:38:0x00a1, B:41:0x00a9, B:43:0x00ad, B:44:0x00b7, B:46:0x00bb, B:47:0x00cb, B:49:0x00cf, B:50:0x00dc, B:52:0x00e0, B:54:0x00e4, B:56:0x00e8, B:57:0x00f2, B:59:0x00f6, B:62:0x00fb, B:63:0x0101, B:64:0x0109, B:66:0x0115, B:67:0x011c, B:70:0x0129, B:71:0x0136), top: B:77:0x0001 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0177  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject A03(X.AbstractC15340mz r10, X.C15430nF r11) {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass13I.A03(X.0mz, X.0nF):org.json.JSONObject");
    }

    public final void A04(AbstractC15340mz r3, JSONObject jSONObject) {
        jSONObject.put("text", A02(C22630zO.A05(this.A04.A00, r3), r3.A0o));
    }

    public final void A05(AbstractC15340mz r5, JSONObject jSONObject) {
        jSONObject.put("user_mentioned", false);
        List<Jid> list = r5.A0o;
        if (list != null) {
            for (Jid jid : list) {
                if (this.A00.A0F(jid)) {
                    jSONObject.put("user_mentioned", true);
                    return;
                }
            }
        }
    }
}
