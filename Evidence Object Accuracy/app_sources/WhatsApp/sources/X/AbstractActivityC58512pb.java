package X;

import com.whatsapp.status.playback.StatusPlaybackActivity;

/* renamed from: X.2pb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC58512pb extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC58512pb() {
        ActivityC13830kP.A1P(this, 127);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            StatusPlaybackActivity statusPlaybackActivity = (StatusPlaybackActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ActivityC13830kP.A1K(this);
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, statusPlaybackActivity);
            ActivityC13810kN.A10(A1M, statusPlaybackActivity);
            ((ActivityC13790kL) statusPlaybackActivity).A08 = ActivityC13790kL.A0S(r3, A1M, statusPlaybackActivity, ActivityC13790kL.A0Y(A1M, statusPlaybackActivity));
            statusPlaybackActivity.A09 = (C18470sV) A1M.AK8.get();
            statusPlaybackActivity.A0B = (C15860o1) A1M.A3H.get();
            statusPlaybackActivity.A0G = (AnonymousClass1CI) A1M.AHh.get();
            statusPlaybackActivity.A08 = C12970iu.A0Y(A1M);
            statusPlaybackActivity.A0C = (AnonymousClass1BD) A1M.AKA.get();
            statusPlaybackActivity.A0F = (AnonymousClass1CJ) A1M.AK3.get();
        }
    }
}
