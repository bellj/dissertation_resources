package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Adapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.BidiToolbar;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.chatinfo.view.custom.BusinessChatInfoLayout;
import com.whatsapp.chatinfo.view.custom.ChatInfoLayoutV2;
import com.whatsapp.components.ScalingFrameLayout;
import java.util.Arrays;

/* renamed from: X.2Ew */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2Ew extends AnonymousClass2Ex {
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public long A09;
    public View.OnClickListener A0A;
    public View A0B;
    public View A0C;
    public View A0D;
    public View A0E;
    public ListView A0F;
    public TextView A0G;
    public BidiToolbar A0H;
    public TextEmojiLabel A0I;
    public C28801Pb A0J;
    public AbstractC115985Tr A0K;
    public ScalingFrameLayout A0L;
    public AnonymousClass130 A0M;
    public C15610nY A0N;
    public AnonymousClass018 A0O;
    public AnonymousClass19M A0P;
    public C14850m9 A0Q;
    public C20710wC A0R;
    public AnonymousClass12F A0S;
    public CharSequence A0T;
    public CharSequence A0U;
    public String A0V;
    public boolean A0W;
    public final ViewTreeObserver.OnGlobalLayoutListener A0X = new ViewTreeObserver$OnGlobalLayoutListenerC101514ne(this);

    public abstract int A03(int i);

    public abstract int getToolbarColorResId();

    public abstract void setOnPhotoClickListener(View.OnClickListener onClickListener);

    public abstract void setRadius(float f);

    public abstract void setStatusData(C92434Vw v);

    public AnonymousClass2Ew(Context context) {
        super(context);
        A0A(context);
    }

    public AnonymousClass2Ew(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0A(context);
    }

    public AnonymousClass2Ew(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A0A(context);
    }

    public static /* synthetic */ void A00(AnonymousClass2Ew r3) {
        if (r3.getWidth() > r3.getHeight()) {
            ((ActivityC000900k) AnonymousClass12P.A00(r3.getContext())).A0d();
            r3.A0F.setOnScrollListener(new C102214om(r3));
            return;
        }
        int A03 = r3.A03(r3.getMeasuredWidth()) - r3.A02(r3.getMeasuredWidth());
        r3.A08 = A03;
        r3.A0F.setSelectionFromTop(0, A03);
        r3.setScrollPos(r3.A08);
        r3.A0F.post(new RunnableC55452iU(r3));
    }

    public int A02(int i) {
        ImageView imageView;
        ChatInfoLayoutV2 chatInfoLayoutV2 = (ChatInfoLayoutV2) this;
        if (!(chatInfoLayoutV2 instanceof BusinessChatInfoLayout) || (imageView = chatInfoLayoutV2.A05) == null || imageView.getDrawable() == null) {
            return chatInfoLayoutV2.A03(i);
        }
        return (int) (((float) i) * 0.5625f);
    }

    public void A04() {
        this.A06 = 0;
        if (this.A0C.getVisibility() != 0) {
            this.A0C.setVisibility(0);
        }
    }

    public void A05() {
        this.A0D = findViewById(R.id.photo_overlay);
        this.A0E = findViewById(R.id.subject_layout);
        this.A0J = new C28801Pb(this, this.A0N, this.A0S, (int) R.id.conversation_contact_name);
        AnonymousClass23N.A02(findViewById(R.id.conversation_contact_name), R.string.action_open_image);
        this.A0L = (ScalingFrameLayout) findViewById(R.id.conversation_contact_name_scaler);
        this.A0H = (BidiToolbar) AnonymousClass028.A0D(this, R.id.toolbar);
        TextView textView = (TextView) findViewById(R.id.conversation_contact_status);
        this.A0G = textView;
        if (textView != null) {
            textView.setClickable(false);
        }
        this.A0I = (TextEmojiLabel) findViewById(R.id.push_name);
        this.A0B = findViewById(R.id.header);
        this.A0F = (ListView) findViewById(16908298);
        this.A0C = AnonymousClass028.A0D(this, R.id.header_placeholder);
        this.A01 = this.A0J.A01.getTextSize();
        A06();
    }

    public void A06() {
        Display defaultDisplay = AnonymousClass12P.A00(getContext()).getWindowManager().getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        this.A0C.setLayoutParams(new LinearLayout.LayoutParams(-1, A02(point.x)));
        this.A0F.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver$OnGlobalLayoutListenerC101504nd(this));
    }

    public void A07() {
        CharSequence charSequence = this.A0U;
        if (charSequence != null) {
            this.A0C.setContentDescription(C32721cd.A00(this.A0O, Arrays.asList(this.A0V == null ? new String[]{charSequence.toString()} : new String[]{charSequence.toString(), this.A0V}), false));
        }
        AnonymousClass23N.A02(this.A0C, R.string.action_open_image);
    }

    public void A08() {
        int i;
        View childAt = this.A0F.getChildAt(0);
        if (childAt != null) {
            if (this.A0F.getFirstVisiblePosition() == 0) {
                i = childAt.getTop();
            } else {
                i = -getHeight();
            }
            setScrollPos(i);
        }
        AbstractC115985Tr r0 = this.A0K;
        if (r0 != null) {
            r0.AVZ();
        }
    }

    public void A09(int i, int i2) {
        this.A04 = i;
        this.A03 = i2;
        if (getWidth() < getHeight()) {
            float f = this.A00;
            if (f > 0.0f) {
                int i3 = (int) (((float) this.A04) * f * f);
                int i4 = (int) (((float) this.A03) * f * f);
                C42941w9.A08(this.A0E, this.A0O, i3, i4);
            }
        }
    }

    public final void A0A(Context context) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true)) {
            this.A07 = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        }
        this.A02 = (float) getResources().getDimensionPixelSize(R.dimen.condensed_title_text_size);
    }

    public void A0B(View view, View view2, View view3, Adapter adapter) {
        this.A0F.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(view, view2, view3, adapter, this) { // from class: X.3Ne
            public final /* synthetic */ View A00;
            public final /* synthetic */ View A01;
            public final /* synthetic */ View A02;
            public final /* synthetic */ Adapter A03;
            public final /* synthetic */ AnonymousClass2Ew A04;

            {
                this.A04 = r5;
                this.A00 = r1;
                this.A03 = r4;
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                AnonymousClass2Ew r6 = this.A04;
                View view4 = this.A00;
                Adapter adapter2 = this.A03;
                View view5 = this.A01;
                View view6 = this.A02;
                if (r6.A0F.isLayoutRequested()) {
                    return;
                }
                if (r6.getHeight() >= r6.getWidth()) {
                    int max = Math.max(r6.getMeasuredHeight() - ((((view4.getMeasuredHeight() - r6.getMeasuredWidth()) + (adapter2.getCount() * r6.getResources().getDimensionPixelSize(R.dimen.small_list_row_height))) + view5.getMeasuredHeight()) + r6.A03(r6.A0B.getMeasuredWidth())), 0) + r6.getResources().getDimensionPixelSize(R.dimen.card_v_padding2);
                    if (view6.getPaddingBottom() != max) {
                        view6.setPadding(0, 0, 0, max);
                    }
                } else if (view6.getPaddingBottom() != 0) {
                    view6.setPadding(0, 0, 0, 0);
                }
            }
        });
    }

    public final void A0C(CharSequence charSequence) {
        if (this.A0I != null) {
            boolean isEmpty = TextUtils.isEmpty(charSequence);
            TextEmojiLabel textEmojiLabel = this.A0I;
            if (isEmpty) {
                textEmojiLabel.setVisibility(8);
            } else {
                textEmojiLabel.setVisibility(0);
                this.A0I.A0G(null, charSequence);
            }
            this.A0I.setOnClickListener(this.A0A);
            AnonymousClass23N.A02(this.A0I, R.string.action_open_image);
        }
    }

    public int getColor() {
        return this.A05;
    }

    public int getToolbarColor() {
        return AnonymousClass00T.A00(getContext(), R.color.toolbar_icon_color_light_mode);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        View view;
        int measuredWidth;
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int paddingTop = getPaddingTop();
        int paddingBottom = i6 - getPaddingBottom();
        int paddingLeft = getPaddingLeft();
        int paddingRight = i5 - getPaddingRight();
        if (i6 > i5) {
            View view2 = this.A0B;
            view2.layout(paddingLeft, paddingTop, paddingRight, view2.getMeasuredHeight() + paddingTop);
            view = this.A0F;
        } else if (this.A0O.A04().A06) {
            ListView listView = this.A0F;
            listView.layout(0 + paddingLeft, paddingTop, listView.getMeasuredWidth() + paddingLeft, paddingBottom);
            view = this.A0B;
            measuredWidth = paddingLeft + this.A0F.getMeasuredWidth() + 0;
            view.layout(measuredWidth, paddingTop, paddingRight, paddingBottom);
        } else {
            View view3 = this.A0B;
            view3.layout(paddingLeft, paddingTop, view3.getMeasuredWidth() + paddingLeft, paddingBottom);
            view = this.A0F;
            paddingLeft += this.A0B.getMeasuredWidth();
        }
        measuredWidth = paddingLeft + 0;
        paddingRight -= 0;
        view.layout(measuredWidth, paddingTop, paddingRight, paddingBottom);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        View view = this.A0C;
        if (measuredHeight >= measuredWidth) {
            if (!(view == null || view.getVisibility() == 0)) {
                this.A0D.setOnClickListener(null);
                this.A0D.setClickable(false);
                this.A0C.setVisibility(0);
                this.A0F.getViewTreeObserver().addOnGlobalLayoutListener(this.A0X);
            }
            this.A0B.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(Math.max(this.A07, this.A06), 1073741824));
            this.A0F.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824) - 0, i2);
            return;
        }
        if (view.getVisibility() != 8) {
            this.A0D.setOnClickListener(this.A0A);
            AnonymousClass23N.A02(this.A0D, R.string.action_open_image);
            this.A0D.setClickable(true);
            C28801Pb r0 = this.A0J;
            r0.A01.setOnClickListener(this.A0A);
            this.A0C.setVisibility(8);
            this.A0F.post(new RunnableBRunnable0Shape3S0100000_I0_3(this, 41));
        }
        int i3 = (int) (((float) measuredWidth) * 0.618f);
        this.A0B.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth - i3, 1073741824), i2);
        this.A0F.measure(View.MeasureSpec.makeMeasureSpec(i3 - 0, 1073741824), i2);
    }

    public void setColor(int i) {
        int i2 = (i & 16777215) | (this.A05 & -16777216);
        this.A05 = i2;
        this.A0D.setBackgroundColor(i2);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
        this.A0A = onClickListener;
    }

    public void setOnScrollListener(AbstractC115985Tr r1) {
        this.A0K = r1;
    }

    public void setPushName(String str) {
        this.A0T = str;
        A0C(str);
    }

    public void setScrollPos(int i) {
        int width = getWidth();
        int height = getHeight();
        boolean z = this.A0W;
        if (width < height) {
            if (z) {
                A04();
            }
            this.A0W = false;
            this.A09 = Math.max(this.A09, ((long) (((((float) (i - this.A08)) / ((float) getHeight())) * 100.0f) - 100.0f)) * -1);
            int max = Math.max(this.A07, A02(getWidth()) + i);
            int A03 = A03(getWidth());
            this.A00 = Math.max(0.0f, ((float) (A03 - max)) / ((float) (A03 - this.A07)));
            TextUtils.TruncateAt ellipsize = this.A0J.A01.getEllipsize();
            int i2 = this.A07 << 1;
            TextEmojiLabel textEmojiLabel = this.A0J.A01;
            if (max < i2) {
                textEmojiLabel.setSingleLine(true);
                this.A0J.A01.setEllipsize(TextUtils.TruncateAt.END);
                setSubtitleSingleLine(true);
                ((ViewGroup.MarginLayoutParams) this.A0J.A01.getLayoutParams()).setMargins(0, 0, 0, 0);
            } else {
                textEmojiLabel.setSingleLine(false);
                this.A0J.A01.setEllipsize(null);
                setSubtitleSingleLine(false);
                int i3 = this.A07;
                ((ViewGroup.MarginLayoutParams) this.A0J.A01.getLayoutParams()).setMargins(0, Math.min(i3, max - (i3 << 1)), 0, 0);
            }
            A0C(this.A0T);
            if (ellipsize != this.A0J.A01.getEllipsize()) {
                this.A0J.A08(this.A0U);
                A07();
            }
            if (this.A06 != max) {
                this.A06 = max;
                int i4 = this.A05 >> 24;
                if (max == this.A07) {
                    if (i4 != -1) {
                        this.A0J.A01.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
                        TextView textView = this.A0G;
                        if (textView != null) {
                            textView.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
                        }
                    }
                } else if (i4 == -1) {
                    this.A0J.A01.setShadowLayer(1.0f, 1.0f, 1.0f, -10066330);
                    TextView textView2 = this.A0G;
                    if (textView2 != null) {
                        textView2.setShadowLayer(1.0f, 1.0f, 1.0f, -10066330);
                    }
                }
                ChatInfoLayoutV2 chatInfoLayoutV2 = (ChatInfoLayoutV2) this;
                float f = chatInfoLayoutV2.getResources().getDisplayMetrics().density;
                float f2 = ((AnonymousClass2Ew) chatInfoLayoutV2).A00;
                float f3 = 1.0f - f2;
                int i5 = (int) (f2 * 255.0f);
                int i6 = (int) (((float) ((AnonymousClass2Ew) chatInfoLayoutV2).A04) * f2 * f2);
                int i7 = (int) (((float) ((AnonymousClass2Ew) chatInfoLayoutV2).A03) * f2 * f2);
                float f4 = ((AnonymousClass2Ew) chatInfoLayoutV2).A01;
                float f5 = (f4 - ((f4 - ((AnonymousClass2Ew) chatInfoLayoutV2).A02) * f2)) / f4;
                int i8 = (f2 > 0.8f ? 1 : (f2 == 0.8f ? 0 : -1));
                TextView textView3 = chatInfoLayoutV2.A07;
                if (i8 > 0) {
                    textView3.setAlpha((float) i5);
                    chatInfoLayoutV2.A07.setVisibility(0);
                } else {
                    textView3.setVisibility(8);
                }
                ScalingFrameLayout scalingFrameLayout = chatInfoLayoutV2.A0L;
                scalingFrameLayout.A00 = f5;
                chatInfoLayoutV2.A09.A00 = f5;
                ((ViewGroup.MarginLayoutParams) scalingFrameLayout.getLayoutParams()).setMargins(0, 0, 0, 0);
                ((AnonymousClass2Ew) chatInfoLayoutV2).A05 = (i5 << 24) | (((AnonymousClass2Ew) chatInfoLayoutV2).A05 & 16777215);
                if (chatInfoLayoutV2.getContext() instanceof AbstractActivityC33001d7) {
                    AbstractActivityC33001d7 r1 = (AbstractActivityC33001d7) AnonymousClass12P.A01(chatInfoLayoutV2.getContext(), AbstractActivityC33001d7.class);
                    if (i5 > 0) {
                        r1.A2m(((AnonymousClass2Ew) chatInfoLayoutV2).A05);
                    } else if (C28391Mz.A03()) {
                        C41691tw.A03(r1, R.color.lightStatusBarBackgroundColor);
                    } else {
                        C41691tw.A02(r1, R.color.neutral_primary_dark);
                    }
                }
                chatInfoLayoutV2.A0D.setBackgroundColor(((AnonymousClass2Ew) chatInfoLayoutV2).A05);
                if (!C41691tw.A08(chatInfoLayoutV2.getContext())) {
                    int i9 = (int) (((AnonymousClass2Ew) chatInfoLayoutV2).A00 * 255.0f);
                    if (i9 < 111) {
                        i9 = 111;
                    }
                    int i10 = i9 & 255;
                    chatInfoLayoutV2.setToolbarIconColorIfNeeded((i10 << 0) | -16777216 | (i10 << 16) | (i10 << 8));
                }
                C42941w9.A08(chatInfoLayoutV2.A0E, chatInfoLayoutV2.A0O, i6, i7);
                chatInfoLayoutV2.A0D();
                boolean z2 = false;
                if (((AnonymousClass2Ew) chatInfoLayoutV2).A00 <= 0.95f) {
                    z2 = true;
                }
                chatInfoLayoutV2.A0B = z2;
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) chatInfoLayoutV2.A05.getLayoutParams();
                marginLayoutParams.setMargins(marginLayoutParams.leftMargin, marginLayoutParams.topMargin, marginLayoutParams.rightMargin, (int) (((float) chatInfoLayoutV2.getResources().getDimensionPixelSize(R.dimen.business_profile_photo_bg_margin_bottom)) * f3));
                chatInfoLayoutV2.A05.setLayoutParams(marginLayoutParams);
                chatInfoLayoutV2.A0D.setLayoutParams(marginLayoutParams);
                if (chatInfoLayoutV2.A0C) {
                    chatInfoLayoutV2.A08.setAnimationValue(((AnonymousClass2Ew) chatInfoLayoutV2).A00);
                } else {
                    int i11 = (int) ((((float) chatInfoLayoutV2.A03) * f3) / 2.0f);
                    int i12 = (f > 1.0f ? 1 : (f == 1.0f ? 0 : -1));
                    Resources resources = chatInfoLayoutV2.getResources();
                    int i13 = R.dimen.business_profile_photo_margin_low_density;
                    if (i12 > 0) {
                        i13 = R.dimen.business_profile_photo_margin;
                    }
                    int dimensionPixelSize = resources.getDimensionPixelSize(i13);
                    chatInfoLayoutV2.A04.setPadding(i11, i11, i11, i11);
                    chatInfoLayoutV2.A06.setPadding(i11, i11, i11, i11);
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) chatInfoLayoutV2.A04.getLayoutParams();
                    boolean z3 = !chatInfoLayoutV2.A0O.A04().A06;
                    int i14 = (int) (((AnonymousClass2Ew) chatInfoLayoutV2).A00 * ((float) dimensionPixelSize));
                    if (z3) {
                        layoutParams.setMargins(0, 0, i14, 0);
                    } else {
                        layoutParams.setMargins(i14, 0, 0, 0);
                    }
                    Drawable drawable = null;
                    if (((AnonymousClass2Ew) chatInfoLayoutV2).A00 > 0.95f) {
                        chatInfoLayoutV2.A04.setBackground(null);
                        layoutParams.setMargins(0, 0, 0, 0);
                        layoutParams.gravity = 8388611;
                    } else {
                        boolean z4 = false;
                        if (chatInfoLayoutV2.A00 != -2.14748365E9f) {
                            z4 = true;
                        }
                        View view = chatInfoLayoutV2.A04;
                        if (z4) {
                            drawable = AnonymousClass00T.A04(chatInfoLayoutV2.getContext(), R.drawable.business_profile_photo_bg);
                        }
                        view.setBackground(drawable);
                        layoutParams.gravity = 1;
                    }
                    chatInfoLayoutV2.A04.setLayoutParams(layoutParams);
                }
                chatInfoLayoutV2.A0E();
                chatInfoLayoutV2.requestLayout();
            }
        } else if (!z) {
            this.A0W = true;
            ChatInfoLayoutV2 chatInfoLayoutV22 = (ChatInfoLayoutV2) this;
            ((AnonymousClass2Ew) chatInfoLayoutV22).A0C.setVisibility(8);
            chatInfoLayoutV22.A0D.setBackgroundColor(0);
            chatInfoLayoutV22.A0L.setVisibility(8);
            chatInfoLayoutV22.A09.A00 = 1.0f;
            chatInfoLayoutV22.A04.setBackground(AnonymousClass00T.A04(chatInfoLayoutV22.getContext(), R.drawable.business_profile_photo_bg));
            chatInfoLayoutV22.A0D();
            ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) chatInfoLayoutV22.A05.getLayoutParams();
            marginLayoutParams2.setMargins(0, 0, 0, 0);
            chatInfoLayoutV22.A05.setLayoutParams(marginLayoutParams2);
            chatInfoLayoutV22.A0D.setLayoutParams(marginLayoutParams2);
            ViewGroup.MarginLayoutParams marginLayoutParams3 = (ViewGroup.MarginLayoutParams) chatInfoLayoutV22.A04.getLayoutParams();
            marginLayoutParams3.setMargins(0, 0, 0, 0);
            chatInfoLayoutV22.A04.setLayoutParams(marginLayoutParams3);
            int dimensionPixelSize2 = chatInfoLayoutV22.getResources().getDimensionPixelSize(R.dimen.business_profile_photo_bg_circle_padding);
            chatInfoLayoutV22.A04.setPadding(dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
            chatInfoLayoutV22.setToolbarIconColorIfNeeded(chatInfoLayoutV22.getToolbarColor());
        }
    }

    private void setSubtitleSingleLine(boolean z) {
        TextView textView = this.A0G;
        if (textView != null) {
            textView.setSingleLine(z);
        }
    }

    public void setSubtitleText(String str) {
        TextView textView = this.A0G;
        if (textView != null && str != null) {
            this.A0V = str;
            textView.setText(str);
            TextView textView2 = this.A0G;
            int i = 0;
            if (TextUtils.isEmpty(str)) {
                i = 8;
            }
            textView2.setVisibility(i);
            A07();
        }
    }

    public void setTitleText(String str) {
        CharSequence A04 = AbstractC36671kL.A04(getContext(), this.A0J.A01.getPaint(), this.A0P, str, 0.9f);
        this.A0U = A04;
        this.A0J.A08(A04);
        C28801Pb r0 = this.A0J;
        r0.A01.setOnClickListener(this.A0A);
        A07();
    }

    public void setTitleVerified(boolean z) {
        C28801Pb r1 = this.A0J;
        int i = 0;
        if (z) {
            i = 2;
        }
        r1.A05(i);
    }

    public void setToolbarIconColorIfNeeded(int i) {
        if (!C41691tw.A08(getContext())) {
            Drawable navigationIcon = this.A0H.getNavigationIcon();
            Drawable overflowIcon = this.A0H.getOverflowIcon();
            if (navigationIcon != null && overflowIcon != null) {
                PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
                overflowIcon.setColorFilter(i, mode);
                navigationIcon.setColorFilter(i, mode);
                this.A0H.setNavigationIcon(navigationIcon);
                this.A0H.setOverflowIcon(overflowIcon);
            }
        }
    }
}
