package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC870349y extends Enum {
    public static final /* synthetic */ EnumC870349y[] A00;
    public static final EnumC870349y A01;
    public static final EnumC870349y A02;

    public static EnumC870349y valueOf(String str) {
        return (EnumC870349y) Enum.valueOf(EnumC870349y.class, str);
    }

    public static EnumC870349y[] values() {
        return (EnumC870349y[]) A00.clone();
    }

    static {
        EnumC870349y r3 = new EnumC870349y("AVATAR_EDITOR", 0);
        A01 = r3;
        EnumC870349y r1 = new EnumC870349y("CLOSE_SCREEN", 1);
        A02 = r1;
        EnumC870349y[] r0 = new EnumC870349y[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public EnumC870349y(String str, int i) {
    }
}
