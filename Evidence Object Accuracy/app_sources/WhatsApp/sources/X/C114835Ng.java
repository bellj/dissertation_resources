package X;

/* renamed from: X.5Ng  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114835Ng extends AnonymousClass5NU {
    public C114835Ng(AnonymousClass1TN r3) {
        super(r3, 0, true);
    }

    public C114835Ng(AnonymousClass1TN r1, int i, boolean z) {
        super(r1, i, z);
    }

    @Override // X.AnonymousClass5NU, X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return this;
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int A01;
        int A05 = AnonymousClass5NU.A00(this).A06().A05();
        if (this.A02) {
            A01 = AnonymousClass1TQ.A01(this.A00) + AnonymousClass1TQ.A00(A05);
        } else {
            A05--;
            A01 = AnonymousClass1TQ.A01(this.A00);
        }
        return A01 + A05;
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return this.A02 || AnonymousClass5NU.A00(this).A06().A09();
    }
}
