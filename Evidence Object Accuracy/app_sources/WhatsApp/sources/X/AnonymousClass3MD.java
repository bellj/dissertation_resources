package X;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.whatsapp.components.TextAndDateLayout;

/* renamed from: X.3MD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MD implements TextWatcher {
    public final /* synthetic */ TextView A00;
    public final /* synthetic */ TextAndDateLayout A01;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass3MD(TextView textView, TextAndDateLayout textAndDateLayout) {
        this.A01 = textAndDateLayout;
        this.A00 = textView;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        int i = 5;
        if (C42941w9.A0G(editable)) {
            i = 3;
        }
        layoutParams.gravity = i;
        this.A00.setLayoutParams(layoutParams);
    }
}
