package X;

import android.content.Context;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;

/* renamed from: X.4q6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103034q6 implements AbstractC009204q {
    public final /* synthetic */ MessageDetailsActivity A00;

    public C103034q6(MessageDetailsActivity messageDetailsActivity) {
        this.A00 = messageDetailsActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
