package X;

/* renamed from: X.5vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128255vo {
    public final C14900mE A00;
    public final C16590pI A01;
    public final AnonymousClass102 A02;
    public final C17220qS A03;
    public final AnonymousClass68Z A04;
    public final C1308460e A05;
    public final C18650sn A06;
    public final C18610sj A07;
    public final C121265hX A08;
    public final C18590sh A09;

    public C128255vo(C14900mE r1, C16590pI r2, AnonymousClass102 r3, C17220qS r4, AnonymousClass68Z r5, C1308460e r6, C18650sn r7, C18610sj r8, C121265hX r9, C18590sh r10) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A09 = r10;
        this.A05 = r6;
        this.A07 = r8;
        this.A02 = r3;
        this.A04 = r5;
        this.A06 = r7;
        this.A08 = r9;
    }
}
