package X;

/* renamed from: X.4BC  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4BC {
    A03(1),
    A04(2),
    A02(3),
    A01(0);
    
    public final int value;

    AnonymousClass4BC(int i) {
        this.value = i;
    }
}
