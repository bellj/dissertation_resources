package X;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/* renamed from: X.4aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93904aw {
    public static C92284Vg A01;
    public final Callable A00 = new AnonymousClass5DX(A01);

    static {
        C92284Vg r2 = new C92284Vg();
        A01 = r2;
        C92284Vg A00 = A00(BigInteger.class, new C114365Lf(), A00(Date.class, new AnonymousClass5Lh(), A00(String.class, new C114425Lm(), A00(BigDecimal.class, new C114355Le(), A00(Float.TYPE, new C114395Lj(), A00(Float.class, new C114395Lj(), A00(Double.TYPE, new C114385Li(), A00(Double.class, new C114385Li(), A00(Integer.TYPE, new C114405Lk(), A00(Integer.class, new C114405Lk(), A00(Long.TYPE, new C114415Ll(), A00(Long.class, new C114415Ll(), r2))))))))))));
        A00.A02.put(Boolean.TYPE, new C114375Lg());
    }

    public static C92284Vg A00(Object obj, Object obj2, C92284Vg r3) {
        r3.A02.put(obj, obj2);
        return A01;
    }

    public Object A01(Class cls, Object obj) {
        String str;
        Class<?> cls2 = obj.getClass();
        if (cls.isAssignableFrom(cls2)) {
            return obj;
        }
        try {
            boolean z = obj instanceof Map;
            if (!z && !(obj instanceof List)) {
                return ((C92284Vg) this.A00.call()).A00(cls).A03(obj);
            }
            if (z) {
                Map map = (Map) obj;
                C94884ch r2 = C94884ch.A04;
                StringBuilder A0h = C12960it.A0h();
                try {
                    if (map == null) {
                        A0h.append((CharSequence) "null");
                    } else {
                        C94904cj.A06.AgH(A0h, map, r2);
                    }
                } catch (IOException unused) {
                }
                str = A0h.toString();
            } else if (obj instanceof List) {
                List list = (List) obj;
                C94884ch r22 = C94884ch.A04;
                StringBuilder A0h2 = C12960it.A0h();
                try {
                    if (list == null) {
                        A0h2.append((CharSequence) "null");
                    } else {
                        C94904cj.A03.AgH(A0h2, list, r22);
                    }
                } catch (IOException unused2) {
                }
                str = A0h2.toString();
            } else {
                StringBuilder A0h3 = C12960it.A0h();
                A0h3.append(cls2.getName());
                throw C12980iv.A0u(C12960it.A0d(" can not be converted to JSON", A0h3));
            }
            try {
                C94764cV r1 = new C94764cV(C94764cV.A01);
                return new AnonymousClass5LL(r1.A00).A0A(str, AnonymousClass4ZZ.A02.A00(cls));
            } catch (Exception unused3) {
                return null;
            }
        } catch (Exception e) {
            throw new C82823wF(e);
        }
    }
}
