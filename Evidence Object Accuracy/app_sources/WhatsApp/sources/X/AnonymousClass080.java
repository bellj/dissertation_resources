package X;

import android.content.Context;

/* renamed from: X.080  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass080 {
    public final Context A00;
    public final C017007z A01;
    public final String A02;
    public final String A03;

    public AnonymousClass080(Context context, C017007z r7, String str) {
        String str2;
        String[] strArr = AnonymousClass0M8.A00;
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                str2 = null;
                break;
            }
            str2 = strArr[i];
            if (AnonymousClass081.A01(context, str2)) {
                break;
            }
            i++;
        }
        this.A03 = str2;
        this.A01 = r7;
        this.A00 = context;
        this.A02 = str;
    }
}
