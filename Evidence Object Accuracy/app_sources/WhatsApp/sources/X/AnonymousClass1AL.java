package X;

import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;

/* renamed from: X.1AL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AL {
    public final C14330lG A00;
    public final C15450nH A01;
    public final C16170oZ A02;
    public final C238013b A03;
    public final C17050qB A04;
    public final AnonymousClass01d A05;
    public final C14830m7 A06;
    public final C15890o4 A07;
    public final C14950mJ A08;
    public final C14850m9 A09;
    public final C16120oU A0A;
    public final AbstractC14440lR A0B;
    public final C254419k A0C;
    public final C21280xA A0D;

    public AnonymousClass1AL(C14330lG r1, C15450nH r2, C16170oZ r3, C238013b r4, C17050qB r5, AnonymousClass01d r6, C14830m7 r7, C15890o4 r8, C14950mJ r9, C14850m9 r10, C16120oU r11, AbstractC14440lR r12, C254419k r13, C21280xA r14) {
        this.A06 = r7;
        this.A09 = r10;
        this.A0B = r12;
        this.A00 = r1;
        this.A0A = r11;
        this.A02 = r3;
        this.A01 = r2;
        this.A08 = r9;
        this.A0C = r13;
        this.A0D = r14;
        this.A05 = r6;
        this.A03 = r4;
        this.A04 = r5;
        this.A07 = r8;
    }

    public static void A00(C14690ls r4, long j, boolean z) {
        try {
            FileOutputStream fileOutputStream = r4.A03;
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        } catch (Exception e) {
            if (!z || j < 1000) {
                StringBuilder sb = new StringBuilder();
                sb.append("pttutils/closevisualization/closevisualization ");
                sb.append(e.toString());
                Log.i(sb.toString());
                return;
            }
            Log.e("pttutils/closevisualization/closevisualization ", e);
        }
    }

    public void A01(AnonymousClass1KC r4, C30421Xi r5, File file) {
        if (file == null || !this.A09.A07(746)) {
            this.A02.A0L(r4, r5);
            if (file != null) {
                this.A0B.Ab2(new RunnableBRunnable0Shape10S0100000_I0_10(file, 0));
                return;
            }
            return;
        }
        this.A0B.Ab5(new C624237e(this.A02, r4, r5, file), new Void[0]);
    }

    public void A02(C14690ls r6, long j, boolean z) {
        try {
            try {
                r6.A01();
            } catch (Exception e) {
                if (!z || j < 1000) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("pttutils/stopaudiorecordandvisualization/stopaudiorecord ");
                    sb.append(e.toString());
                    Log.i(sb.toString());
                } else {
                    Log.e("pttutils/stopaudiorecordandvisualization/stopaudiorecord ", e);
                }
            }
        } finally {
            A00(r6, j, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r2.getCallState() == 0) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03(X.ActivityC000900k r7, X.AbstractC13860kS r8, X.AbstractC14640lm r9) {
        /*
            r6 = this;
            X.01d r0 = r6.A05
            android.telephony.TelephonyManager r2 = r0.A0N()
            r5 = 0
            if (r2 == 0) goto L_0x0015
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 31
            if (r1 >= r0) goto L_0x0053
            int r0 = r2.getCallState()
            if (r0 != 0) goto L_0x0062
        L_0x0015:
            boolean r0 = X.C21280xA.A00()
            if (r0 == 0) goto L_0x0022
            r0 = 2131888106(0x7f1207ea, float:1.9410838E38)
        L_0x001e:
            r8.Ado(r0)
        L_0x0021:
            return r5
        L_0x0022:
            java.lang.String r0 = "voicenote/startvoicenote"
            com.whatsapp.util.Log.i(r0)
            X.55t r1 = new X.55t
            r1.<init>(r7, r6)
            X.0qB r0 = r6.A04
            boolean r0 = r0.A04(r1)
            if (r0 == 0) goto L_0x0021
            X.0mJ r0 = r6.A08
            long r3 = r0.A01()
            X.0nH r1 = r6.A01
            X.0oW r0 = X.AbstractC15460nI.A1p
            int r0 = r1.A02(r0)
            int r0 = r0 << 10
            int r0 = r0 << 10
            long r1 = (long) r0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0066
            X.0oU r1 = r6.A0A
            r0 = 7
            X.C33471e8.A04(r7, r8, r1, r0)
            return r5
        L_0x0053:
            X.0o4 r0 = r6.A07     // Catch: SecurityException -> 0x0015
            boolean r0 = r0.A08()     // Catch: SecurityException -> 0x0015
            if (r0 == 0) goto L_0x0015
            int r0 = r2.getCallStateForSubscription()     // Catch: SecurityException -> 0x0015
            if (r0 != 0) goto L_0x0062
            goto L_0x0015
        L_0x0062:
            r0 = 2131888107(0x7f1207eb, float:1.941084E38)
            goto L_0x001e
        L_0x0066:
            X.13b r1 = r6.A03
            com.whatsapp.jid.UserJid r0 = com.whatsapp.jid.UserJid.of(r9)
            boolean r0 = r1.A0I(r0)
            if (r0 == 0) goto L_0x0078
            r0 = 106(0x6a, float:1.49E-43)
            X.C36021jC.A01(r7, r0)
            return r5
        L_0x0078:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1AL.A03(X.00k, X.0kS, X.0lm):boolean");
    }
}
