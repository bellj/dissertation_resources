package X;

/* renamed from: X.68P  reason: invalid class name */
/* loaded from: classes4.dex */
public abstract class AnonymousClass68P implements AnonymousClass17F {
    public final int A00 = 18;
    public final AbstractC15710nm A01;
    public final C17220qS A02;
    public final String A03 = "fb:graphql";

    public AnonymousClass68P(AbstractC15710nm r3, C17220qS r4) {
        this.A01 = r3;
        this.A02 = r4;
    }

    @Override // X.AnonymousClass17F
    public final void A9q(AbstractC116705Wm r13) {
        C17220qS r5 = this.A02;
        String A01 = r5.A01();
        C41141sy A0M = C117295Zj.A0M();
        A0M.A04(new AnonymousClass1W9("smax_id", this.A00));
        C41141sy.A01(A0M, "id", A01);
        C41141sy.A01(A0M, "xmlns", this.A03);
        C41141sy.A01(A0M, "type", "get");
        C41141sy.A01(A0M, "to", "s.whatsapp.net");
        C41141sy r1 = new C41141sy("supported_certificates");
        r1.A01 = "rsa2048".getBytes();
        C117295Zj.A1H(r1, A0M);
        C41141sy r3 = new C41141sy("auth_metadata");
        C117315Zl.A0X(r3, "timestamp", C117295Zj.A03(((C119895fH) this).A00));
        r3.A04(new AnonymousClass1W9("version", 1));
        C117295Zj.A1H(r3, A0M);
        r5.A0D(new AnonymousClass6DL(this, r13), A0M.A03(), A01, 264, 32000);
    }
}
