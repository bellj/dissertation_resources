package X;

import android.os.Handler;
import android.widget.TextView;
import androidx.biometric.FingerprintDialogFragment;

/* renamed from: X.0Yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07480Yh implements AnonymousClass02B {
    public final /* synthetic */ FingerprintDialogFragment A00;

    public C07480Yh(FingerprintDialogFragment fingerprintDialogFragment) {
        this.A00 = fingerprintDialogFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        int i;
        FingerprintDialogFragment fingerprintDialogFragment = this.A00;
        Handler handler = fingerprintDialogFragment.A05;
        Runnable runnable = fingerprintDialogFragment.A06;
        handler.removeCallbacks(runnable);
        int intValue = ((Number) obj).intValue();
        fingerprintDialogFragment.A1J(intValue);
        TextView textView = fingerprintDialogFragment.A03;
        if (textView != null) {
            if (intValue == 2) {
                i = fingerprintDialogFragment.A00;
            } else {
                i = fingerprintDialogFragment.A01;
            }
            textView.setTextColor(i);
        }
        handler.postDelayed(runnable, 2000);
    }
}
