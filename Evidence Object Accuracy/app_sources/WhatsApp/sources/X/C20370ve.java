package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.text.TextUtils;
import android.util.Pair;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.0ve  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20370ve {
    public static final String[] A0A = {"key_remote_jid", "key_from_me", "key_id", "id", "timestamp", "init_timestamp", "status", "error_code", "sender", "receiver", "type", "currency", "amount_1000", "credential_id", "methods", "bank_transaction_id", "request_key_id", "metadata", "country", "version", "future_data", "service_id", "background_id", "purchase_initiator"};
    public static final String[] A0B = {"message_row_id", "remote_jid_row_id", "key_id", "interop_id", "id", "timestamp", "init_timestamp", "status", "error_code", "sender_jid_row_id", "receiver_jid_row_id", "type", "currency_code", "amount_1000", "credential_id", "methods", "bank_transaction_id", "request_key_id", "metadata", "country", "version", "future_data", "service_id", "background_id", "purchase_initiator"};
    public AbstractC248317b A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C18460sU A03;
    public final C16490p7 A04;
    public final C21390xL A05;
    public final AnonymousClass102 A06;
    public final C241414j A07;
    public final C20340vb A08;
    public final C30931Zj A09 = C30931Zj.A00("PaymentTransactionStore", "database", "COMMON");

    public C20370ve(C15570nT r4, C14830m7 r5, C18460sU r6, C16490p7 r7, C21390xL r8, AnonymousClass102 r9, C241414j r10, C20340vb r11) {
        this.A02 = r5;
        this.A03 = r6;
        this.A01 = r4;
        this.A07 = r10;
        this.A05 = r8;
        this.A04 = r7;
        this.A08 = r11;
        this.A06 = r9;
    }

    public static Pair A00() {
        return new Pair("(type=? AND status=?)", new String[]{Integer.toString(8), Integer.toString(608)});
    }

    public static Pair A01() {
        String num = Integer.toString(12);
        return new Pair("((type=? AND status=?) OR (type=? AND (status=? OR status=?)))", new String[]{Integer.toString(20), num, Integer.toString(10), num, Integer.toString(19)});
    }

    public static Pair A02() {
        return Pair.create(new String[]{"19", "12", "17", "608", String.valueOf(1), String.valueOf(2), "20", "10", String.valueOf(6), String.valueOf(7), String.valueOf(8), String.valueOf(9), "100", "300", "40", "415", String.valueOf(15), String.valueOf(16)}, "((status!=?) AND (status!=?) AND (status!=?) AND (status!=?) AND (type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR (type=? AND (status=? OR status=? OR status=?))))");
    }

    public static Pair A03(Pair pair, Pair pair2, String str) {
        if (TextUtils.isEmpty((CharSequence) pair.first)) {
            return pair2;
        }
        if (TextUtils.isEmpty((CharSequence) pair2.first)) {
            return pair;
        }
        StringBuilder sb = new StringBuilder("(");
        sb.append((String) pair.first);
        sb.append(") ");
        sb.append(str);
        sb.append(" (");
        sb.append((String) pair2.first);
        sb.append(")");
        String obj = sb.toString();
        Object obj2 = pair.second;
        int length = ((String[]) obj2).length;
        String[] strArr = new String[((String[]) pair2.second).length + length];
        System.arraycopy(obj2, 0, strArr, 0, length);
        Object obj3 = pair2.second;
        System.arraycopy(obj3, 0, strArr, ((String[]) pair.second).length, ((String[]) obj3).length);
        return new Pair(obj, strArr);
    }

    public static final Pair A04(Pair pair, boolean z) {
        String str;
        if (z) {
            str = "currency_code";
        } else {
            str = "currency";
        }
        StringBuilder sb = new StringBuilder("(");
        sb.append(str);
        sb.append(" !=? OR ");
        sb.append("metadata");
        sb.append(" LIKE ?)");
        return A03(pair, new Pair(sb.toString(), new String[]{((AbstractC30781Yu) C30771Yt.A06).A04, "%money%"}), "AND");
    }

    public static final Pair A05(String str, String str2) {
        String[] strArr;
        String str3;
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (isEmpty && isEmpty2) {
            return null;
        }
        if (isEmpty || isEmpty2) {
            strArr = new String[1];
            if (!isEmpty) {
                strArr[0] = str;
                str3 = "key_id=?";
            } else {
                strArr[0] = str2;
                str3 = "id=?";
            }
        } else {
            strArr = new String[]{str, str2};
            str3 = "key_id=? OR id=?";
        }
        return new Pair(str3, strArr);
    }

    public static Pair A06(boolean z) {
        String str;
        if (z) {
            str = "(status IN (?, ?, ?, ?))";
        } else {
            str = "(status NOT IN (?, ?, ?, ?))";
        }
        return Pair.create(new String[]{"405", "106", "604", "408"}, str);
    }

    public static Pair A07(int[] iArr, int[] iArr2) {
        StringBuilder sb = new StringBuilder();
        int length = iArr.length;
        int length2 = iArr2.length;
        String[] strArr = new String[length + length2];
        for (int i = 0; i < length; i++) {
            sb.append("status=?");
            if (i != length - 1) {
                sb.append(" OR ");
            }
            strArr[i] = String.valueOf(iArr[i]);
        }
        StringBuilder sb2 = new StringBuilder();
        for (int i2 = 0; i2 < length2; i2++) {
            sb2.append("type=?");
            if (i2 != length2 - 1) {
                sb2.append(" OR ");
            }
            strArr[length + i2] = String.valueOf(iArr2[i2]);
        }
        StringBuilder sb3 = new StringBuilder("((");
        sb3.append(sb.toString());
        sb3.append(") AND (");
        sb3.append(sb2.toString());
        sb3.append("))");
        return Pair.create(strArr, sb3.toString());
    }

    public static final void A08(ContentValues contentValues, C16310on r5, AnonymousClass1IR r6) {
        r5.A03.A00("pay_transaction", contentValues, "id=?", new String[]{r6.A0K});
    }

    public static final void A09(C30941Zk r3, List list, List list2) {
        String num;
        C30951Zl r0 = r3.A00;
        AnonymousClass009.A05(r0);
        int i = r0.A00;
        if (i == 0) {
            list2.add("(type=? OR type=? OR type=?)");
            list.add(Integer.toString(2));
            list.add(Integer.toString(20));
        } else if (i == 2) {
            list2.add("(type=?)");
        } else {
            list2.add("(type=? OR type=?)");
            list.add(Integer.toString(2));
            num = Integer.toString(20);
            list.add(num);
        }
        num = Integer.toString(200);
        list.add(num);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01d3  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.ContentValues A0A(X.AnonymousClass1IR r9, X.AnonymousClass1IR r10) {
        /*
        // Method dump skipped, instructions count: 529
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0A(X.1IR, X.1IR):android.content.ContentValues");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01fb, code lost:
        if (r0 != null) goto L_0x01a4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x015b  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0197  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.ContentValues A0B(X.AnonymousClass1IR r9, X.AnonymousClass1IR r10) {
        /*
        // Method dump skipped, instructions count: 521
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0B(X.1IR, X.1IR):android.content.ContentValues");
    }

    public Pair A0C() {
        Pair A03 = A03(new Pair("(type=? AND status=?)", new String[]{Integer.toString(8), Integer.toString(602)}), A00(), "OR");
        Pair pair = new Pair(new String[0], null);
        return A03(A03, new Pair(pair.second, pair.first), "AND");
    }

    public Pair A0D() {
        Pair A01 = A01();
        Pair pair = new Pair(new String[0], null);
        return A03(A01, new Pair(pair.second, pair.first), "AND");
    }

    public final Pair A0E(int i) {
        String str;
        String rawString;
        if (i == 2) {
            str = "( sender_jid_row_id=? OR receiver_jid_row_id=? )";
            C15570nT r0 = this.A01;
            r0.A08();
            C27621Ig r02 = r0.A01;
            AnonymousClass009.A05(r02);
            Jid jid = r02.A0D;
            AnonymousClass009.A05(jid);
            rawString = Long.toString(this.A03.A01(jid));
        } else {
            str = "( sender=? OR receiver=? )";
            C15570nT r03 = this.A01;
            r03.A08();
            C27621Ig r04 = r03.A01;
            AnonymousClass009.A05(r04);
            Jid jid2 = r04.A0D;
            AnonymousClass009.A05(jid2);
            rawString = jid2.getRawString();
        }
        Pair pair = new Pair(new String[]{rawString, rawString}, str);
        StringBuilder sb = new StringBuilder("( type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR type=? OR (type=? AND ");
        sb.append((String) pair.second);
        sb.append(") OR (");
        sb.append("type");
        sb.append("=? AND ");
        sb.append("status");
        sb.append("!=? AND ");
        sb.append("status");
        sb.append("!=?) OR (");
        sb.append("type");
        sb.append("=? AND (");
        sb.append("status");
        sb.append("=? OR ");
        sb.append("status");
        sb.append("=? OR ");
        sb.append("status");
        sb.append("=?)) OR (");
        sb.append("type");
        sb.append("=? AND ");
        sb.append("status");
        sb.append("!=? AND ");
        sb.append("status");
        sb.append("!=? AND ");
        sb.append("status");
        sb.append("!=?))");
        String obj = sb.toString();
        Pair pair2 = new Pair(new String[0], null);
        String[] strArr = (String[]) pair2.first;
        Object obj2 = pair2.second;
        if (obj2 != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(obj);
            sb2.append(" AND ");
            sb2.append((String) obj2);
            obj = sb2.toString();
        }
        int i2 = !TextUtils.isEmpty(null) ? 1 : 0;
        if (i2 != 0) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(obj);
            sb3.append(" AND credential_id=?");
            obj = sb3.toString();
        }
        int length = strArr.length;
        String[] strArr2 = new String[i2 + 21 + length];
        int i3 = 0;
        strArr2[0] = Integer.toString(1);
        strArr2[1] = Integer.toString(2);
        strArr2[2] = Integer.toString(100);
        strArr2[3] = Integer.toString(6);
        strArr2[4] = Integer.toString(7);
        strArr2[5] = Integer.toString(8);
        strArr2[6] = Integer.toString(9);
        strArr2[7] = Integer.toString(1000);
        String[] strArr3 = (String[]) pair.first;
        strArr2[8] = strArr3[0];
        strArr2[9] = strArr3[1];
        strArr2[10] = Integer.toString(20);
        String num = Integer.toString(12);
        strArr2[11] = num;
        String num2 = Integer.toString(17);
        strArr2[12] = num2;
        strArr2[13] = Integer.toString(40);
        strArr2[14] = Integer.toString(415);
        strArr2[15] = Integer.toString(15);
        strArr2[16] = Integer.toString(16);
        strArr2[17] = Integer.toString(10);
        strArr2[18] = num;
        strArr2[19] = Integer.toString(19);
        strArr2[20] = num2;
        int i4 = 21;
        while (i3 < length) {
            strArr2[i4] = strArr[i3];
            i3++;
            i4++;
        }
        if (i2 != 0) {
            strArr2[i4] = null;
        }
        return new Pair(obj, strArr2);
    }

    public final Pair A0F(AbstractC14640lm r12, int i) {
        String str;
        Pair A0E = A0E(i);
        Pair A0D = A0D();
        String[] strArr = new String[((String[]) A0E.second).length + 1 + ((String[]) A0D.second).length];
        if (i == 1) {
            strArr[0] = r12.getRawString();
            str = "key_remote_jid=?";
        } else {
            long A01 = this.A03.A01(r12);
            if (A01 != -1) {
                strArr[0] = String.valueOf(A01);
                str = "remote_jid_row_id=?";
            } else {
                C30931Zj r2 = this.A09;
                StringBuilder sb = new StringBuilder("getPendingRequestsAndTransactionsQueryAndParams/no row id for jid/jid=");
                sb.append(r12.getRawString());
                r2.A05(sb.toString());
                return null;
            }
        }
        StringBuilder sb2 = new StringBuilder("(");
        sb2.append(str);
        sb2.append(" AND (");
        sb2.append((String) A0E.first);
        sb2.append(" OR ");
        sb2.append((String) A0D.first);
        sb2.append("))");
        String obj = sb2.toString();
        Object obj2 = A0E.second;
        System.arraycopy(obj2, 0, strArr, 1, ((String[]) obj2).length);
        Object obj3 = A0D.second;
        System.arraycopy(obj3, 0, strArr, ((String[]) A0E.second).length + 1, ((String[]) obj3).length);
        return new Pair(obj, strArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair A0G(X.C30941Zk r6) {
        /*
            r5 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            X.1Zm r0 = r6.A01
            if (r0 == 0) goto L_0x00a9
            java.lang.String[] r0 = r0.A01
            java.util.Collections.addAll(r2, r0)
            X.1Zm r0 = r6.A01
            java.lang.String r0 = r0.A00
        L_0x0017:
            r3.add(r0)
        L_0x001a:
            X.0nT r0 = r5.A01
            r0.A08()
            X.1Ig r0 = r0.A01
            X.AnonymousClass009.A05(r0)
            com.whatsapp.jid.Jid r0 = r0.A0D
            X.AnonymousClass009.A05(r0)
            java.lang.String r4 = r0.getRawString()
            boolean r0 = r6.A06
            r1 = 1
            if (r0 == 0) goto L_0x009d
            java.lang.String r0 = "(type=? OR type=? OR type=?)"
            r3.add(r0)
            java.lang.String r0 = java.lang.Integer.toString(r1)
            r2.add(r0)
            r0 = 10
            java.lang.String r0 = java.lang.Integer.toString(r0)
            r2.add(r0)
            r0 = 100
            java.lang.String r0 = java.lang.Integer.toString(r0)
            r2.add(r0)
        L_0x0050:
            java.lang.String r0 = "( sender=? OR receiver=?)"
            r3.add(r0)
            r2.add(r4)
            r2.add(r4)
            boolean r0 = r6.A02
            r4 = 0
            if (r0 == 0) goto L_0x0094
            android.util.Pair r1 = A06(r1)
        L_0x0064:
            java.lang.Object r0 = r1.first
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.util.Collections.addAll(r2, r0)
            java.lang.Object r0 = r1.second
            r3.add(r0)
        L_0x0070:
            java.lang.String r0 = "("
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = " AND "
            java.lang.String r0 = android.text.TextUtils.join(r0, r3)
            r1.append(r0)
            java.lang.String r0 = ")"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r0 = r2.toArray(r0)
            android.util.Pair r0 = android.util.Pair.create(r0, r1)
            return r0
        L_0x0094:
            boolean r0 = r6.A03
            if (r0 == 0) goto L_0x0070
            android.util.Pair r1 = A06(r4)
            goto L_0x0064
        L_0x009d:
            X.1Zl r0 = r6.A00
            if (r0 == 0) goto L_0x0050
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x0050
            A09(r6, r2, r3)
            goto L_0x0050
        L_0x00a9:
            boolean r0 = r6.A04
            if (r0 == 0) goto L_0x001a
            android.util.Pair r1 = A02()
            java.lang.Object r0 = r1.first
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.util.Collections.addAll(r2, r0)
            java.lang.Object r0 = r1.second
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0G(X.1Zk):android.util.Pair");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair A0H(X.C30941Zk r6) {
        /*
            r5 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            X.1Zm r0 = r6.A01
            if (r0 == 0) goto L_0x00c8
            java.lang.String[] r0 = r0.A01
            java.util.Collections.addAll(r2, r0)
            X.1Zm r0 = r6.A01
            java.lang.String r0 = r0.A00
        L_0x0017:
            r3.add(r0)
        L_0x001a:
            X.0nT r0 = r5.A01
            r0.A08()
            X.1Ig r0 = r0.A01
            X.AnonymousClass009.A05(r0)
            com.whatsapp.jid.Jid r1 = r0.A0D
            X.AnonymousClass009.A05(r1)
            X.0sU r0 = r5.A03
            long r0 = r0.A01(r1)
            java.lang.String r4 = java.lang.Long.toString(r0)
            boolean r0 = r6.A06
            r1 = 1
            if (r0 == 0) goto L_0x00bc
            java.lang.String r0 = "(type=? OR type=? OR type=?)"
            r3.add(r0)
            java.lang.String r0 = java.lang.Integer.toString(r1)
            r2.add(r0)
            r0 = 10
            java.lang.String r0 = java.lang.Integer.toString(r0)
            r2.add(r0)
            r0 = 100
            java.lang.String r0 = java.lang.Integer.toString(r0)
            r2.add(r0)
        L_0x0056:
            java.lang.String r0 = "( receiver_jid_row_id=? OR sender_jid_row_id=? OR (service_id=? AND (type=? OR type=? OR type=? OR type=?)))"
            r3.add(r0)
            r2.add(r4)
            r2.add(r4)
            java.lang.String r0 = "3"
            r2.add(r0)
            java.lang.String r0 = "6"
            r2.add(r0)
            java.lang.String r0 = "7"
            r2.add(r0)
            java.lang.String r0 = "8"
            r2.add(r0)
            java.lang.String r0 = "9"
            r2.add(r0)
            boolean r0 = r6.A02
            r4 = 0
            if (r0 == 0) goto L_0x00b3
            android.util.Pair r1 = A06(r1)
        L_0x0083:
            java.lang.Object r0 = r1.first
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.util.Collections.addAll(r2, r0)
            java.lang.Object r0 = r1.second
            r3.add(r0)
        L_0x008f:
            java.lang.String r0 = "("
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = " AND "
            java.lang.String r0 = android.text.TextUtils.join(r0, r3)
            r1.append(r0)
            java.lang.String r0 = ")"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String[] r0 = new java.lang.String[r4]
            java.lang.Object[] r0 = r2.toArray(r0)
            android.util.Pair r0 = android.util.Pair.create(r0, r1)
            return r0
        L_0x00b3:
            boolean r0 = r6.A03
            if (r0 == 0) goto L_0x008f
            android.util.Pair r1 = A06(r4)
            goto L_0x0083
        L_0x00bc:
            X.1Zl r0 = r6.A00
            if (r0 == 0) goto L_0x0056
            boolean r0 = r0.A01
            if (r0 == 0) goto L_0x0056
            A09(r6, r2, r3)
            goto L_0x0056
        L_0x00c8:
            boolean r0 = r6.A04
            if (r0 == 0) goto L_0x001a
            android.util.Pair r1 = A02()
            java.lang.Object r0 = r1.first
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.util.Collections.addAll(r2, r0)
            java.lang.Object r0 = r1.second
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0H(X.1Zk):android.util.Pair");
    }

    public final AnonymousClass1IR A0I(Cursor cursor) {
        if (A0g()) {
            return A0K(cursor);
        }
        return A0J(cursor);
    }

    public final AnonymousClass1IR A0J(Cursor cursor) {
        AnonymousClass1IR A03;
        int i;
        AbstractC16830pp AFY;
        AbstractC248317b r0;
        AbstractC14640lm A01 = AbstractC14640lm.A01(cursor.getString(cursor.getColumnIndexOrThrow("key_remote_jid")));
        String string = cursor.getString(cursor.getColumnIndexOrThrow("key_id"));
        boolean z = false;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("key_from_me")) == 1) {
            z = true;
        }
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("id"));
        long j = ((long) cursor.getInt(cursor.getColumnIndexOrThrow("init_timestamp"))) * 1000;
        long j2 = ((long) cursor.getInt(cursor.getColumnIndexOrThrow("timestamp"))) * 1000;
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow("status"));
        UserJid nullable = UserJid.getNullable(cursor.getString(cursor.getColumnIndexOrThrow("sender")));
        UserJid nullable2 = UserJid.getNullable(cursor.getString(cursor.getColumnIndexOrThrow("receiver")));
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow("type"));
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow("currency"));
        long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("amount_1000"));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow("credential_id"));
        String string5 = cursor.getString(cursor.getColumnIndexOrThrow("error_code"));
        String string6 = cursor.getString(cursor.getColumnIndexOrThrow("bank_transaction_id"));
        String string7 = cursor.getString(cursor.getColumnIndexOrThrow("methods"));
        String string8 = cursor.getString(cursor.getColumnIndexOrThrow("metadata"));
        String string9 = cursor.getString(cursor.getColumnIndexOrThrow("request_key_id"));
        String string10 = cursor.getString(cursor.getColumnIndexOrThrow("country"));
        if (TextUtils.isEmpty(string10)) {
            string10 = "IN";
        }
        int i4 = cursor.getInt(cursor.getColumnIndexOrThrow("version"));
        byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow("future_data"));
        int i5 = cursor.getInt(cursor.getColumnIndexOrThrow("service_id"));
        String string11 = cursor.getString(cursor.getColumnIndexOrThrow("background_id"));
        C30921Zi r11 = null;
        if (!TextUtils.isEmpty(string11)) {
            r11 = this.A08.A01(string11);
        }
        int i6 = cursor.getInt(cursor.getColumnIndexOrThrow("purchase_initiator"));
        C30931Zj r10 = this.A09;
        StringBuilder sb = new StringBuilder("readTransactionInfoByTransId got from db: id: ");
        sb.append(string2);
        sb.append(" timestamp: ");
        sb.append(j2);
        sb.append(" type: ");
        sb.append(i3);
        sb.append(" status: ");
        sb.append(i2);
        sb.append(" description:  peer: ");
        sb.append(nullable2);
        sb.append(" background_id: ");
        sb.append(string11);
        r10.A03(null, sb.toString());
        if (i3 != 5 || !TextUtils.isEmpty(string3)) {
            A03 = C31001Zq.A03(this.A06.A02(string3), nullable, nullable2, string3, string2, string4, string5, string6, string10, new BigDecimal(j3).scaleByPowerOfTen(-3), blob, i3, i2, i4, i5, i6, j, j2);
        } else {
            A03 = new AnonymousClass1IR(string10, 5, i4, j);
            A03.A0R = blob;
        }
        A03.A05(r11);
        A03.A0C = A01;
        if (A01 == null) {
            A03.A0P = true;
        }
        A03.A0Q = z;
        if (!TextUtils.isEmpty(string)) {
            A03.A0L = string;
        }
        if (!TextUtils.isEmpty(string9)) {
            A03.A0M = string9;
        }
        if (!TextUtils.isEmpty(string7)) {
            A03.A08(C31001Zq.A07(A03.A00(), string7));
        }
        if (!TextUtils.isEmpty(string8) && (r0 = this.A00) != null) {
            AbstractC16830pp AFY2 = r0.AFY(string10, string3);
            if (AFY2 != null) {
                A03.A0A = AFY2.AIk();
            }
            AbstractC30891Zf r3 = A03.A0A;
            if (r3 != null) {
                r3.A0W(string8, A03.A03);
                if (A03.A0E()) {
                    long A07 = A03.A0A.A07();
                    if (A07 > 0 && A07 < this.A02.A00()) {
                        int i7 = 16;
                        if (A03.A03 == 8) {
                            i7 = 607;
                        }
                        A03.A02 = i7;
                    }
                }
            }
        }
        if (A03.A01 == 0) {
            AbstractC248317b r32 = this.A00;
            if (r32 == null || (AFY = r32.AFY(A03.A0G, A03.A0I)) == null) {
                i = 0;
            } else {
                i = AFY.AGi();
            }
            A03.A01 = i;
        }
        StringBuilder sb2 = new StringBuilder("readTransactionFromCursor: ");
        sb2.append(A03);
        sb2.append(" countryData: ");
        sb2.append(A03.A0A);
        r10.A03(null, sb2.toString());
        return A03;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0203  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0236  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass1IR A0K(android.database.Cursor r50) {
        /*
        // Method dump skipped, instructions count: 698
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0K(android.database.Cursor):X.1IR");
    }

    public AnonymousClass1IR A0L(String str) {
        String str2;
        String[] strArr;
        boolean z = true;
        String[] strArr2 = {str};
        A0g();
        C16310on A01 = this.A04.get();
        try {
            C16330op r6 = A01.A03;
            if (A0g()) {
                str2 = "pay_transaction";
            } else {
                str2 = "pay_transactions";
            }
            if (A0g()) {
                strArr = A0B;
            } else {
                strArr = A0A;
            }
            Cursor A08 = r6.A08(str2, "request_key_id=?", null, null, strArr, strArr2);
            AnonymousClass1IR r3 = null;
            if (A08.moveToLast()) {
                try {
                    r3 = A0I(A08);
                } catch (AnonymousClass1MW e) {
                    this.A09.A0A("PaymentTransactionStore/readTransactionInfoByRequestMessageId/InvalidJidException - Cannot read TransactionInfo from a message with invalid JID", e);
                    A08.close();
                    A01.close();
                    return null;
                }
            }
            A08.close();
            A01.close();
            C30931Zj r2 = this.A09;
            StringBuilder sb = new StringBuilder("readTransactionInfoByRequestMessageId/");
            sb.append(str);
            sb.append("/");
            if (r3 == null) {
                z = false;
            }
            sb.append(z);
            r2.A06(sb.toString());
            return r3;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1IR A0M(java.lang.String r14) {
        /*
            r13 = this;
            java.lang.String r8 = "id=?"
            r3 = 1
            java.lang.String[] r12 = new java.lang.String[r3]
            r0 = 0
            r12[r0] = r14
            r13.A0g()
            X.0p7 r0 = r13.A04
            X.0on r5 = r0.get()
            X.0op r6 = r5.A03     // Catch: all -> 0x0070
            boolean r0 = r13.A0g()     // Catch: all -> 0x0070
            if (r0 == 0) goto L_0x002d
            java.lang.String r7 = "pay_transaction"
        L_0x001b:
            boolean r0 = r13.A0g()     // Catch: all -> 0x0070
            if (r0 == 0) goto L_0x002a
            java.lang.String[] r11 = X.C20370ve.A0B     // Catch: all -> 0x0070
        L_0x0023:
            r9 = 0
            r10 = r9
            android.database.Cursor r6 = r6.A08(r7, r8, r9, r10, r11, r12)     // Catch: all -> 0x0070
            goto L_0x0030
        L_0x002a:
            java.lang.String[] r11 = X.C20370ve.A0A     // Catch: all -> 0x0070
            goto L_0x0023
        L_0x002d:
            java.lang.String r7 = "pay_transactions"
            goto L_0x001b
        L_0x0030:
            boolean r0 = r6.moveToLast()     // Catch: all -> 0x0069
            if (r0 == 0) goto L_0x0043
            X.1IR r4 = r13.A0I(r6)     // Catch: 1MW -> 0x003b, all -> 0x0069
            goto L_0x0044
        L_0x003b:
            r2 = move-exception
            X.1Zj r1 = r13.A09     // Catch: all -> 0x0069
            java.lang.String r0 = "PaymentTransactionStore/readTransactionInfoByTransId/InvalidJidException - Cannot read TransactionInfo from a message with invalid JID"
            r1.A0A(r0, r2)     // Catch: all -> 0x0069
        L_0x0043:
            r4 = 0
        L_0x0044:
            r6.close()     // Catch: all -> 0x0070
            r5.close()
            X.1Zj r2 = r13.A09
            java.lang.String r0 = "readTransactionInfoByTransId/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r14)
            java.lang.String r0 = "/"
            r1.append(r0)
            if (r4 != 0) goto L_0x005e
            r3 = 0
        L_0x005e:
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            r2.A06(r0)
            return r4
        L_0x0069:
            r0 = move-exception
            if (r6 == 0) goto L_0x006f
            r6.close()     // Catch: all -> 0x006f
        L_0x006f:
            throw r0     // Catch: all -> 0x0070
        L_0x0070:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x0074
        L_0x0074:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0M(java.lang.String):X.1IR");
    }

    public AnonymousClass1IR A0N(String str, String str2) {
        if (A0g()) {
            return A0P(str, str2, -1);
        }
        return A0O(str, str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0078, code lost:
        if (r2 != null) goto L_0x007a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1IR A0O(java.lang.String r14, java.lang.String r15) {
        /*
            r13 = this;
            android.util.Pair r0 = A05(r14, r15)
            r9 = 0
            if (r0 != 0) goto L_0x0023
            X.1Zj r2 = r13.A09
            java.lang.String r0 = "getMessagePaymentInfoFromV1 got null query and params for message id: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r14)
            java.lang.String r0 = " trans id: "
            r1.append(r0)
            r1.append(r15)
            java.lang.String r0 = r1.toString()
            r2.A06(r0)
            return r9
        L_0x0023:
            java.lang.Object r8 = r0.first
            java.lang.String r8 = (java.lang.String) r8
            java.lang.Object r12 = r0.second
            java.lang.String[] r12 = (java.lang.String[]) r12
            X.0p7 r0 = r13.A04
            X.0on r5 = r0.get()
            X.0op r6 = r5.A03     // Catch: all -> 0x00b9
            java.lang.String r7 = "pay_transactions"
            java.lang.String[] r11 = X.C20370ve.A0A     // Catch: all -> 0x00b9
            r3 = r9
            r10 = r9
            android.database.Cursor r6 = r6.A08(r7, r8, r9, r10, r11, r12)     // Catch: all -> 0x00b9
            boolean r0 = r6.moveToLast()     // Catch: all -> 0x00b2
            if (r0 == 0) goto L_0x0057
            X.1IR r4 = r13.A0J(r6)     // Catch: 1MW -> 0x0048, all -> 0x00b2
            goto L_0x0058
        L_0x0048:
            r2 = move-exception
            X.1Zj r1 = r13.A09     // Catch: all -> 0x00b2
            java.lang.String r0 = "PaymentTransactionStore/getMessagePaymentInfoFromV1/InvalidJidException - Cannot get PaymentInfo from a message with invalid JID"
            r1.A0A(r0, r2)     // Catch: all -> 0x00b2
            r6.close()     // Catch: all -> 0x00b9
            r5.close()
            return r9
        L_0x0057:
            r4 = r9
        L_0x0058:
            r6.close()     // Catch: all -> 0x00b9
            r5.close()
            if (r4 == 0) goto L_0x0064
            X.1Zf r2 = r4.A0A
            if (r2 != 0) goto L_0x007a
        L_0x0064:
            X.17b r2 = r13.A00
            if (r2 == 0) goto L_0x0085
            if (r4 == 0) goto L_0x00ad
            java.lang.String r1 = r4.A0G
            java.lang.String r0 = r4.A0I
            X.0pp r0 = r2.AFY(r1, r0)
        L_0x0072:
            if (r0 == 0) goto L_0x0085
            X.1Zf r2 = r0.AIk()
            if (r2 == 0) goto L_0x0085
        L_0x007a:
            java.lang.String r1 = r2.A0E()
            if (r1 == 0) goto L_0x0085
            X.14j r0 = r13.A07
            r0.A0G(r2, r1)
        L_0x0085:
            X.1Zj r2 = r13.A09
            java.lang.String r0 = "PaymentTransactionStore/getMessagePaymentInfoFromV1/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r14)
            java.lang.String r0 = "/"
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " country data: "
            r1.append(r0)
            if (r4 == 0) goto L_0x00a2
            X.1Zf r9 = r4.A0A
        L_0x00a2:
            r1.append(r9)
            java.lang.String r0 = r1.toString()
            r2.A03(r3, r0)
            return r4
        L_0x00ad:
            X.0pp r0 = r2.AGf()
            goto L_0x0072
        L_0x00b2:
            r0 = move-exception
            if (r6 == 0) goto L_0x00b8
            r6.close()     // Catch: all -> 0x00b8
        L_0x00b8:
            throw r0     // Catch: all -> 0x00b9
        L_0x00b9:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x00bd
        L_0x00bd:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0O(java.lang.String, java.lang.String):X.1IR");
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public X.AnonymousClass1IR A0P(java.lang.String r13, java.lang.String r14, long r15) {
        /*
            r12 = this;
            r2 = 0
            r8 = 0
            r3 = -1
            int r0 = (r15 > r3 ? 1 : (r15 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x004c
            java.lang.String r0 = "message_row_id=?"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r0)
            X.1Zp r1 = new X.1Zp
            r1.<init>(r12)
            java.lang.String r0 = java.lang.Long.toString(r15)
            r1.add(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 != 0) goto L_0x0029
            java.lang.String r0 = " OR key_id=?"
            r3.append(r0)
            r1.add(r13)
        L_0x0029:
            boolean r0 = android.text.TextUtils.isEmpty(r14)
            if (r0 != 0) goto L_0x0037
            java.lang.String r0 = " OR id=?"
            r3.append(r0)
            r1.add(r14)
        L_0x0037:
            java.lang.String r7 = r3.toString()
            java.lang.String[] r0 = new java.lang.String[r2]
            java.lang.Object[] r11 = r1.toArray(r0)
            java.lang.String[] r11 = (java.lang.String[]) r11
            if (r7 == 0) goto L_0x00c9
        L_0x0045:
            X.0p7 r0 = r12.A04
            X.0on r4 = r0.get()
            goto L_0x006a
        L_0x004c:
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            r1 = 1
            if (r0 != 0) goto L_0x005d
            r0 = 2
            java.lang.String[] r11 = new java.lang.String[r0]
            r11[r2] = r13
            r11[r1] = r13
            java.lang.String r7 = "key_id=? OR interop_id=?"
            goto L_0x0045
        L_0x005d:
            boolean r0 = android.text.TextUtils.isEmpty(r14)
            if (r0 != 0) goto L_0x00c9
            java.lang.String[] r11 = new java.lang.String[r1]
            r11[r2] = r14
            java.lang.String r7 = "id=?"
            goto L_0x0045
        L_0x006a:
            X.0op r5 = r4.A03     // Catch: all -> 0x009a
            java.lang.String r6 = "pay_transaction"
            java.lang.String[] r10 = X.C20370ve.A0B     // Catch: all -> 0x009a
            r9 = r8
            android.database.Cursor r3 = r5.A08(r6, r7, r8, r9, r10, r11)     // Catch: all -> 0x009a
            boolean r0 = r3.moveToLast()     // Catch: all -> 0x0093
            if (r0 == 0) goto L_0x008f
            X.1IR r8 = r12.A0I(r3)     // Catch: 1MW -> 0x0080, all -> 0x0093
            goto L_0x008f
        L_0x0080:
            r2 = move-exception
            X.1Zj r1 = r12.A09     // Catch: all -> 0x0093
            java.lang.String r0 = "getMessagePaymentInfoV2/InvalidJidException - Cannot get PaymentInfo from a message with invalid JID"
            r1.A0A(r0, r2)     // Catch: all -> 0x0093
            r3.close()     // Catch: all -> 0x009a
            r4.close()
            return r8
        L_0x008f:
            r3.close()     // Catch: all -> 0x009a
            goto L_0x009f
        L_0x0093:
            r0 = move-exception
            if (r3 == 0) goto L_0x0099
            r3.close()     // Catch: all -> 0x0099
        L_0x0099:
            throw r0     // Catch: all -> 0x009a
        L_0x009a:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x009e
        L_0x009e:
            throw r0
        L_0x009f:
            r4.close()
            if (r8 == 0) goto L_0x00a8
            X.1Zf r2 = r8.A0A
            if (r2 != 0) goto L_0x00be
        L_0x00a8:
            X.17b r2 = r12.A00
            if (r2 == 0) goto L_0x00c9
            if (r8 == 0) goto L_0x00c9
            java.lang.String r1 = r8.A0G
            java.lang.String r0 = r8.A0I
            X.0pp r0 = r2.AFY(r1, r0)
            if (r0 == 0) goto L_0x00c9
            X.1Zf r2 = r0.AIk()
            if (r2 == 0) goto L_0x00c9
        L_0x00be:
            java.lang.String r1 = r2.A0E()
            if (r1 == 0) goto L_0x00c9
            X.14j r0 = r12.A07
            r0.A0G(r2, r1)
        L_0x00c9:
            X.1Zj r2 = r12.A09
            if (r8 != 0) goto L_0x00d3
            java.lang.String r0 = "IN- HANDLE_SEND_AGAIN PaymentTransactionStore#getMessagePaymentInfoV2 fetching from db, txn is null"
        L_0x00cf:
            r2.A06(r0)
            return r8
        L_0x00d3:
            java.lang.String r0 = "IN- HANDLE_SEND_AGAIN PaymentTransactionStore#getMessagePaymentInfoV2 fetching from db, and interop is "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            boolean r0 = r8.A0P
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20370ve.A0P(java.lang.String, java.lang.String, long):X.1IR");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 4, insn: 0x029f: IGET  (r1 I:X.1Zj) = (r4 I:X.0ve) X.0ve.A09 X.1Zj, block:B:100:0x029f
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public java.lang.String A0Q(
/*
[693] Method generation error in method: X.0ve.A0Q(X.0mz, boolean):java.lang.String, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r20v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public List A0R() {
        List list;
        synchronized (this) {
            if (A0g()) {
                Pair A0D = A0D();
                list = A0Y((String) A0D.first, (String[]) A0D.second, -1);
            } else {
                Pair A0D2 = A0D();
                list = A0X((String) A0D2.first, (String[]) A0D2.second, -1);
            }
        }
        return list;
    }

    public synchronized List A0S(int i) {
        List list;
        if (A0g()) {
            Pair pair = new Pair(new String[0], null);
            Pair A03 = A03(A03(A04(A01(), true), A00(), "OR"), new Pair((String) pair.second, (String[]) pair.first), "AND");
            list = A0Y((String) A03.first, (String[]) A03.second, i);
        } else {
            Pair pair2 = new Pair(new String[0], null);
            Pair A032 = A03(A03(A04(A01(), false), A00(), "OR"), new Pair((String) pair2.second, (String[]) pair2.first), "AND");
            list = A0X((String) A032.first, (String[]) A032.second, i);
        }
        return list;
    }

    public synchronized List A0T(int i) {
        ArrayList arrayList;
        int[] iArr = AnonymousClass1IR.A0T;
        int length = iArr.length;
        int[] iArr2 = AnonymousClass1IR.A0V;
        int length2 = iArr2.length;
        int[] iArr3 = AnonymousClass1IR.A0U;
        int length3 = iArr3.length;
        arrayList = new ArrayList(length + length2 + length3);
        for (int i2 : iArr) {
            arrayList.add(Integer.valueOf(i2));
        }
        for (int i3 : iArr2) {
            arrayList.add(Integer.valueOf(i3));
        }
        for (int i4 : iArr3) {
            arrayList.add(Integer.valueOf(i4));
        }
        return A0b((Integer[]) arrayList.toArray(new Integer[0]), new Integer[]{2, 1, 200, 100, 20, 10, 6, 7, 8}, i);
    }

    public final List A0U(AbstractC14640lm r12) {
        Pair A0F;
        C16310on A01;
        ArrayList arrayList;
        C30931Zj r1;
        String str;
        Pair A0F2;
        ArrayList arrayList2;
        if (A0g()) {
            if (r12 == null) {
                A0F2 = A0E(2);
            } else {
                A0F2 = A0F(r12, 2);
            }
            if (A0F2 == null) {
                r1 = this.A09;
                str = "readTransactionsV2/null queryPair";
            } else {
                String str2 = (String) A0F2.first;
                String[] strArr = (String[]) A0F2.second;
                A0g();
                A01 = this.A04.get();
                try {
                    Cursor A08 = A01.A03.A08("pay_transaction", str2, "init_timestamp DESC", "", A0B, strArr);
                    if (A08 != null) {
                        arrayList2 = new ArrayList(A08.getCount());
                        while (A08.moveToNext()) {
                            try {
                                arrayList2.add(A0I(A08));
                            } catch (AnonymousClass1MW e) {
                                this.A09.A0A("readTransactionsV2/InvalidJidException - Skipped transaction with invalid JID", e);
                            }
                        }
                        C30931Zj r4 = this.A09;
                        StringBuilder sb = new StringBuilder();
                        sb.append("readTransactionsV2 returned: ");
                        sb.append(arrayList2.size());
                        r4.A03(null, sb.toString());
                        A08.close();
                    } else {
                        arrayList2 = new ArrayList();
                    }
                    A01.close();
                    return arrayList2;
                } finally {
                }
            }
        } else {
            if (r12 == null) {
                A0F = A0E(1);
            } else {
                A0F = A0F(r12, 1);
            }
            if (A0F == null) {
                r1 = this.A09;
                str = "PaymentTransactionStore/readTransactions/null queryPair";
            } else {
                String str3 = (String) A0F.first;
                String[] strArr2 = (String[]) A0F.second;
                A01 = this.A04.get();
                try {
                    Cursor A082 = A01.A03.A08("pay_transactions", str3, "init_timestamp DESC", "", A0A, strArr2);
                    if (A082 != null) {
                        arrayList = new ArrayList(A082.getCount());
                        while (A082.moveToNext()) {
                            try {
                                arrayList.add(A0I(A082));
                            } catch (AnonymousClass1MW e2) {
                                this.A09.A0A("PaymentTransactionStore/readTransactions/InvalidJidException - Skipped transaction with invalid JID", e2);
                            }
                        }
                        C30931Zj r42 = this.A09;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("readTransactions returned: ");
                        sb2.append(arrayList.size());
                        r42.A03(null, sb2.toString());
                        A082.close();
                    } else {
                        arrayList = new ArrayList();
                    }
                    A01.close();
                    return arrayList;
                } finally {
                }
            }
        }
        r1.A05(str);
        return new ArrayList();
    }

    public List A0V(C30941Zk r12) {
        Pair A0G;
        C16310on A01;
        String str;
        String[] strArr;
        Cursor A08;
        if (A0g()) {
            A0G = A0H(r12);
        } else {
            A0G = A0G(r12);
        }
        String[] strArr2 = (String[]) A0G.first;
        String str2 = (String) A0G.second;
        try {
            A01 = this.A04.get();
            C16330op r4 = A01.A03;
            if (A0g()) {
                str = "pay_transaction";
            } else {
                str = "pay_transactions";
            }
            if (A0g()) {
                strArr = A0B;
            } else {
                strArr = A0A;
            }
            A08 = r4.A08(str, str2, "init_timestamp DESC", null, strArr, strArr2);
        } catch (Exception e) {
            this.A09.A0A("PaymentTransactionStore/readTransactionsWithFilters ", e);
        }
        if (A08 != null) {
            try {
                ArrayList arrayList = new ArrayList(A08.getCount());
                while (A08.moveToNext()) {
                    try {
                        arrayList.add(A0I(A08));
                    } catch (AnonymousClass1MW e2) {
                        this.A09.A0A("PaymentTransactionStore/readTransactionsWithFilters/InvalidJidException - Skipped transaction with invalid JID", e2);
                    }
                }
                C30931Zj r2 = this.A09;
                StringBuilder sb = new StringBuilder();
                sb.append("readTransactionsWithFilters returned: ");
                sb.append(arrayList.size());
                r2.A03(null, sb.toString());
                A08.close();
                A01.close();
                return arrayList;
            } catch (Throwable th) {
                try {
                    A08.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            A01.close();
            return new ArrayList();
        }
    }

    public synchronized List A0W(String str, Integer[] numArr, Integer[] numArr2, int i) {
        List list;
        String str2;
        String str3;
        String str4;
        String[] strArr;
        C16490p7 r3 = this.A04;
        r3.A04();
        if (!r3.A01) {
            list = Collections.emptyList();
        } else {
            if (numArr.length > 0) {
                str2 = String.format("(%s IN (\"%s\"))", "status", TextUtils.join("\",\"", numArr));
            } else {
                str2 = "";
            }
            if (numArr2.length > 0) {
                str3 = String.format("(%s IN (\"%s\"))", "type", TextUtils.join("\",\"", numArr2));
            } else {
                str3 = "";
            }
            String str5 = null;
            if (TextUtils.isEmpty(str2)) {
                str2 = null;
            }
            if (TextUtils.isEmpty(str3)) {
                str3 = str2;
            } else if (!TextUtils.isEmpty(str2)) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(" AND ");
                sb.append(str3);
                str3 = sb.toString();
            }
            if (!TextUtils.isEmpty(str)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("credential_id=");
                sb2.append(str);
                String obj = sb2.toString();
                if (TextUtils.isEmpty(str3)) {
                    str3 = obj;
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str3);
                    sb3.append(" AND ");
                    sb3.append(obj);
                    str3 = sb3.toString();
                }
            }
            if (TextUtils.isEmpty(str3)) {
                list = new ArrayList();
            } else {
                String format = String.format("(%s) AND (%s IS NOT NULL)", str3, "id");
                C30931Zj r2 = this.A09;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("pending txns query: ");
                sb4.append(format);
                r2.A03(null, sb4.toString());
                if (i > 0) {
                    str5 = Integer.toString(i);
                }
                A0g();
                try {
                    C16310on A01 = r3.get();
                    try {
                        C16330op r4 = A01.A03;
                        if (A0g()) {
                            str4 = "pay_transaction";
                        } else {
                            str4 = "pay_transactions";
                        }
                        if (A0g()) {
                            strArr = A0B;
                        } else {
                            strArr = A0A;
                        }
                        Cursor A08 = r4.A08(str4, format, "timestamp DESC", str5, strArr, null);
                        ArrayList arrayList = new ArrayList(A08.getCount());
                        while (A08.moveToNext()) {
                            try {
                                arrayList.add(A0I(A08));
                            } catch (AnonymousClass1MW e) {
                                r2.A0A("readPendingTransactions/InvalidJidException - Skipped pending transaction with invalid JID", e);
                            }
                        }
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("readPendingTransactions returned: ");
                        sb5.append(arrayList.size());
                        r2.A03(null, sb5.toString());
                        A08.close();
                        A01.close();
                        return arrayList;
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (IllegalStateException e2) {
                    r2.A0A("readPendingTransactions/IllegalStateException ", e2);
                    list = new ArrayList();
                }
            }
        }
        return list;
    }

    public final synchronized List A0X(String str, String[] strArr, int i) {
        ArrayList arrayList;
        String num = i > 0 ? Integer.toString(i) : "";
        try {
            C16310on A01 = this.A04.get();
            try {
                Cursor A08 = A01.A03.A08("pay_transactions", str, "init_timestamp DESC", num, A0A, strArr);
                arrayList = new ArrayList(A08.getCount());
                while (A08.moveToNext()) {
                    try {
                        arrayList.add(A0I(A08));
                    } catch (AnonymousClass1MW e) {
                        this.A09.A0A("PaymentTransactionStore/queryPaymentTransactionInfos/InvalidJidException - Skipped pending transaction with invalid JID", e);
                    }
                }
                C30931Zj r2 = this.A09;
                StringBuilder sb = new StringBuilder();
                sb.append("readPendingRequests returned: ");
                sb.append(arrayList.size());
                r2.A03(null, sb.toString());
                A08.close();
                A01.close();
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IllegalStateException e2) {
            this.A09.A0A("PaymentTransactionStore/queryPaymentTransactionInfos/IllegalStateException ", e2);
            return new ArrayList();
        }
        return arrayList;
    }

    public final synchronized List A0Y(String str, String[] strArr, int i) {
        String num;
        ArrayList arrayList;
        if (i > 0) {
            num = Integer.toString(i);
        } else {
            num = "";
        }
        A0g();
        try {
            C16310on A01 = this.A04.get();
            try {
                Cursor A08 = A01.A03.A08("pay_transaction", str, "init_timestamp DESC", num, A0B, strArr);
                arrayList = new ArrayList(A08.getCount());
                while (A08.moveToNext()) {
                    try {
                        arrayList.add(A0I(A08));
                    } catch (AnonymousClass1MW e) {
                        this.A09.A0A("queryPaymentTransactionInfosV2/InvalidJidException - Skipped pending transaction with invalid JID", e);
                    }
                }
                C30931Zj r2 = this.A09;
                StringBuilder sb = new StringBuilder();
                sb.append("readPendingRequests returned: ");
                sb.append(arrayList.size());
                r2.A03(null, sb.toString());
                A08.close();
                A01.close();
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (IllegalStateException e2) {
            this.A09.A0A("queryPaymentTransactionInfosV2/IllegalStateException ", e2);
            return new ArrayList();
        }
        return arrayList;
    }

    public List A0Z(List list) {
        String str;
        String[] strArr;
        StringBuilder sb = new StringBuilder("id IN (\"");
        sb.append(TextUtils.join("\",\"", list));
        sb.append("\")");
        String obj = sb.toString();
        A0g();
        C16310on A01 = this.A04.get();
        try {
            C16330op r4 = A01.A03;
            if (A0g()) {
                str = "pay_transaction";
            } else {
                str = "pay_transactions";
            }
            if (A0g()) {
                strArr = A0B;
            } else {
                strArr = A0A;
            }
            Cursor A08 = r4.A08(str, obj, null, "100", strArr, null);
            if (A08 != null) {
                ArrayList arrayList = new ArrayList(A08.getCount());
                while (A08.moveToNext()) {
                    try {
                        arrayList.add(A0I(A08));
                    } catch (AnonymousClass1MW e) {
                        this.A09.A0A("readTransactionsByIds/InvalidJidException - Skipped transaction with invalid JID", e);
                    }
                }
                C30931Zj r2 = this.A09;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("readTransactionsByIds returned: ");
                sb2.append(arrayList.size());
                r2.A06(sb2.toString());
                A08.close();
                A01.close();
                return arrayList;
            }
            A01.close();
            return new ArrayList();
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final synchronized List A0a(boolean z) {
        List<AnonymousClass1IR> list;
        ArrayList arrayList;
        C16490p7 r6;
        C16310on A02;
        long A00 = this.A02.A00();
        if (z) {
            int[] iArr = AnonymousClass1IR.A0T;
            int length = iArr.length;
            int[] iArr2 = AnonymousClass1IR.A0U;
            int length2 = iArr2.length;
            ArrayList arrayList2 = new ArrayList(length + length2);
            for (int i : iArr) {
                arrayList2.add(Integer.valueOf(i));
            }
            for (int i2 : iArr2) {
                arrayList2.add(Integer.valueOf(i2));
            }
            list = A0b((Integer[]) arrayList2.toArray(new Integer[0]), new Integer[]{2, 200, 20, 10}, -1);
        } else {
            list = A0T(-1);
        }
        arrayList = new ArrayList();
        try {
            r6 = this.A04;
            A02 = r6.A02();
        } catch (SQLiteDatabaseCorruptException e) {
            C30931Zj r2 = this.A09;
            StringBuilder sb = new StringBuilder();
            sb.append("PaymentTransactionStore/failPendingTransactions/failed: ");
            sb.append(e);
            r2.A05(sb.toString());
        }
        try {
            AnonymousClass1Lx A002 = A02.A00();
            for (AnonymousClass1IR r7 : list) {
                ContentValues contentValues = new ContentValues();
                Pair A05 = A05(r7.A0L, r7.A0K);
                if (A05 != null) {
                    contentValues.put("status", (Integer) 0);
                    contentValues.put("timestamp", Integer.valueOf((int) (A00 / 1000)));
                    C30931Zj r22 = this.A09;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("failed transaction/key_id=");
                    sb2.append(r7.A0L);
                    sb2.append(", transaction_id=");
                    sb2.append(r7.A0K);
                    r22.A06(sb2.toString());
                    if (A0f()) {
                        A02.A03.A00("pay_transaction", contentValues, (String) A05.first, (String[]) A05.second);
                    }
                    r6.A04();
                    if (r6.A05.A0E(A02)) {
                        A02.A03.A00("pay_transactions", contentValues, (String) A05.first, (String[]) A05.second);
                    }
                    arrayList.add(new AnonymousClass1IS(r7.A0C, r7.A0L, r7.A0Q));
                }
            }
            A002.A00();
            A002.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
        return arrayList;
    }

    public synchronized List A0b(Integer[] numArr, Integer[] numArr2, int i) {
        return A0W(null, numArr, numArr2, i);
    }

    public synchronized void A0c(AnonymousClass1IR r13) {
        C16490p7 r3;
        C16310on A02;
        long A00 = this.A02.A00();
        try {
            r3 = this.A04;
            A02 = r3.A02();
        } catch (SQLiteDatabaseCorruptException unused) {
            this.A09.A05("expirePendingRequest failed.");
        }
        try {
            AnonymousClass1Lx A002 = A02.A00();
            ContentValues contentValues = new ContentValues();
            Pair A05 = A05(r13.A0L, r13.A0K);
            contentValues.put("status", (Integer) 16);
            contentValues.put("timestamp", Integer.valueOf((int) (A00 / 1000)));
            C30931Zj r2 = this.A09;
            StringBuilder sb = new StringBuilder();
            sb.append("expirePendingRequest key id:");
            sb.append(r13.A0L);
            r2.A06(sb.toString());
            if (A0f()) {
                A08(contentValues, A02, r13);
            }
            r3.A04();
            if (r3.A05.A0E(A02)) {
                A02.A03.A00("pay_transactions", contentValues, (String) A05.first, (String[]) A05.second);
            }
            A002.A00();
            A002.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused2) {
            }
            throw th;
        }
    }

    public void A0d(AbstractC15340mz r4) {
        String str;
        if (r4.A0y == 0) {
            String str2 = "UNSET";
            if (str2.equals(r4.A0m)) {
                AnonymousClass1IR A0N = A0N(r4.A0z.A01, null);
                if (A0N == null && !C31001Zq.A08(r4.A0L)) {
                    A0Q(r4, false);
                }
                r4.A0L = A0N;
                if (!(A0N == null || (str = A0N.A0K) == null)) {
                    str2 = str;
                }
                r4.A0m = str2;
            }
        }
    }

    public void A0e(String str, int i, int i2, long j, long j2) {
        AnonymousClass1IR A0M;
        AbstractC16830pp AFY;
        if (!TextUtils.isEmpty(str) && i > 0 && j > 0 && j2 > 0 && i2 > 0 && (A0M = A0M(str)) != null) {
            AbstractC30891Zf r1 = A0M.A0A;
            if (!(r1 == null && ((AFY = this.A00.AFY(A0M.A0G, A0M.A0I)) == null || (r1 = AFY.AIk()) == null))) {
                r1.A0M(A0M.A03);
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("type", Integer.valueOf(i));
            contentValues.put("init_timestamp", Integer.valueOf((int) (j / 1000)));
            contentValues.put("status", Integer.valueOf(i2));
            contentValues.put("timestamp", Integer.valueOf((int) (j2 / 1000)));
            String[] strArr = {str};
            C16490p7 r5 = this.A04;
            C16310on A02 = r5.A02();
            try {
                if (A0f()) {
                    A02.A03.A00("pay_transaction", contentValues, "id=?", strArr);
                }
                r5.A04();
                if (r5.A05.A0E(A02)) {
                    int A00 = A02.A03.A00("pay_transactions", contentValues, "id=?", strArr);
                    C30931Zj r2 = this.A09;
                    StringBuilder sb = new StringBuilder();
                    sb.append("updateTransactionTypeById/");
                    sb.append(str);
                    sb.append("/");
                    sb.append(A00);
                    r2.A03(null, sb.toString());
                }
                A0f();
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }

    public boolean A0f() {
        return this.A03.A0C() && this.A05.A01("new_pay_transaction_ready", 0) == 1;
    }

    public boolean A0g() {
        return this.A03.A0C() && this.A05.A01("new_pay_transaction_ready", 0) == 1;
    }

    public boolean A0h(AnonymousClass1IR r4) {
        AnonymousClass1IR A0N = A0N(r4.A0L, r4.A0K);
        if (A0N == null) {
            return false;
        }
        r4.A06 = this.A02.A00();
        return A0j(r4, A0N, r4.A0L);
    }

    public synchronized boolean A0i(AnonymousClass1IR r9) {
        C30971Zn r5;
        AbstractC30891Zf r0 = r9.A0A;
        if (r0 != null) {
            r5 = r0.A00;
        } else {
            r5 = null;
        }
        if (r5 != null && this.A01.A0F(r9.A0E)) {
            UserJid of = UserJid.of(r9.A0D);
            if (of == null || r9.A0K == null) {
                this.A09.A05("insertOrUpdateIncentivePaymentContactInfo/ Receiver Jid or transaction id are null. Updating the incentive record in the payment contacts table failed.");
            } else {
                C241414j r4 = this.A07;
                AnonymousClass1ZO A05 = r4.A05(of);
                if (A05 == null || A05.A05 == null) {
                    AbstractC16830pp AGh = this.A00.AGh(C17930rd.A01(AnonymousClass1ZT.A01(C248917h.A04(of))).A03, null);
                    if (AGh != null) {
                        A05 = AGh.AIi();
                        if (A05 != null) {
                            A05.A05 = of;
                        }
                    } else if (A05 != null) {
                    }
                }
                HashSet hashSet = new HashSet();
                C30981Zo r02 = A05.A04;
                if (r02 != null) {
                    HashSet hashSet2 = (HashSet) r02.A00.get(r5.A02);
                    if (hashSet2 != null) {
                        hashSet = hashSet2;
                    }
                }
                try {
                    switch (r9.A02) {
                        case 401:
                        case 402:
                        case 403:
                        case 405:
                        case 410:
                        case 417:
                        case 420:
                            hashSet.add(Long.valueOf(Long.parseLong(r9.A0K)));
                            break;
                        case 404:
                        case 406:
                        case 407:
                        case 408:
                        case 409:
                        case 411:
                        case 412:
                        case 413:
                        case 414:
                        case 415:
                        case 416:
                        case 418:
                        case 419:
                        case 421:
                            hashSet.remove(Long.valueOf(Long.parseLong(r9.A0K)));
                            break;
                        default:
                            this.A09.A05("There's no valid transaction status. Updating the incentive record in the payment contacts table failed.");
                            break;
                    }
                    String str = r5.A02;
                    C30981Zo r03 = A05.A04;
                    if (r03 == null) {
                        r03 = new C30981Zo();
                        A05.A04 = r03;
                    }
                    r03.A00.put(str, hashSet);
                    return r4.A0J(A05);
                } catch (NumberFormatException e) {
                    this.A09.A0A("There was a problem parsing the paymentTransactionInfo.id", e);
                }
            }
        }
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 4, insn: 0x0256: IGET  (r1 I:X.1Zj) = (r4 I:X.0ve) X.0ve.A09 X.1Zj, block:B:64:0x0256
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public boolean A0j(
/*
[606] Method generation error in method: X.0ve.A0j(X.1IR, X.1IR, java.lang.String):boolean, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r19v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public boolean A0k(AnonymousClass1IR r12, AnonymousClass1IS r13, int i, int i2, long j) {
        boolean z;
        AbstractC30891Zf AIk;
        AbstractC16830pp AFY = this.A00.AFY(r12.A0G, r12.A0I);
        if (!(AFY == null || (AIk = AFY.AIk()) == null)) {
            synchronized (r12) {
                if (i > 0) {
                    if (r12.A02 != i) {
                        AbstractC30891Zf r0 = r12.A0A;
                        if (r0 == null) {
                            r12.A0A = AIk;
                            r0 = AIk;
                        }
                        r0.A0L(i);
                    }
                }
            }
            r12.A03(AIk, j);
            synchronized (r12) {
                if (i2 > 0) {
                    AbstractC30891Zf r02 = r12.A0A;
                    if (r02 == null) {
                        r12.A0A = AIk;
                        r02 = AIk;
                    }
                    r02.A0K(i2);
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(r12.A03));
        contentValues.put("status", Integer.valueOf(r12.A02));
        contentValues.put("timestamp", Integer.valueOf((int) (r12.A06 / 1000)));
        if (!TextUtils.isEmpty(r12.A0K)) {
            contentValues.put("id", r12.A0K);
        }
        if (!TextUtils.isEmpty(r12.A0H)) {
            contentValues.put("credential_id", r12.A0H);
        }
        if (!TextUtils.isEmpty(r12.A0J)) {
            contentValues.put("error_code", r12.A0J);
        }
        if (!TextUtils.isEmpty(r12.A0F)) {
            contentValues.put("bank_transaction_id", r12.A0F);
        }
        AbstractC30891Zf r03 = r12.A0A;
        if (r03 != null) {
            contentValues.put("metadata", r03.A0H());
        }
        boolean z2 = true;
        String str = r13.A01;
        String[] strArr = {str};
        try {
            C16490p7 r9 = this.A04;
            C16310on A02 = r9.A02();
            if (A0f()) {
                Pair A05 = A05(str, r12.A0K);
                z = false;
                if (A05 != null && A02.A03.A00("pay_transaction", contentValues, (String) A05.first, (String[]) A05.second) > 0) {
                    z = true;
                }
            } else {
                z = false;
            }
            r9.A04();
            if (r9.A05.A0E(A02)) {
                int A00 = A02.A03.A00("pay_transactions", contentValues, "key_id=?", strArr);
                C30931Zj r2 = this.A09;
                StringBuilder sb = new StringBuilder();
                sb.append("PaymentTransactionStore/insertMessagePaymentInfo/");
                sb.append(r13.A00);
                sb.append("/");
                sb.append(A00);
                r2.A03(null, sb.toString());
                if (A00 <= 0) {
                    z2 = false;
                }
                z = z2;
            }
            if (r12.A0A != null && z) {
                A0i(r12);
            }
            A02.close();
            return z;
        } catch (SQLiteDatabaseCorruptException e) {
            this.A09.A0A("PaymentTransactionStore/insertMessagePaymentInfo", e);
            return false;
        }
    }

    public boolean A0l(List list) {
        StringBuilder sb;
        String str;
        C30931Zj r10;
        String str2;
        ContentValues A0A2;
        ContentValues A0B2;
        int i;
        int i2;
        if (list == null || list.size() <= 0) {
            this.A09.A06("storeTransactions not storing transactions");
        } else {
            C16490p7 r9 = this.A04;
            C16310on A02 = r9.A02();
            try {
                AnonymousClass1Lx A00 = A02.A00();
                Iterator it = list.iterator();
                int i3 = 0;
                int i4 = 0;
                while (it.hasNext()) {
                    AnonymousClass1IR r4 = (AnonymousClass1IR) it.next();
                    if (!TextUtils.isEmpty(r4.A0K)) {
                        AnonymousClass1IR A0M = A0M(r4.A0K);
                        if (A0M == null || A0M.A0L(r4)) {
                            if (A0f() && (A0B2 = A0B(A0M, r4)) != null) {
                                String str3 = "id=?";
                                boolean isEmpty = TextUtils.isEmpty(r4.A0L);
                                int i5 = 1;
                                if (!isEmpty) {
                                    i5 = 2;
                                }
                                String[] strArr = new String[i5];
                                strArr[0] = r4.A0K;
                                if (!isEmpty) {
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(str3);
                                    sb2.append(" OR key_id=?");
                                    str3 = sb2.toString();
                                    strArr[1] = r4.A0L;
                                }
                                C16330op r11 = A02.A03;
                                long A002 = (long) r11.A00("pay_transaction", A0B2, str3, strArr);
                                long A022 = A002 != 1 ? r11.A02(A0B2, "pay_transaction") : -1;
                                if (A002 == 1 || A022 >= 0) {
                                    i3++;
                                }
                            }
                            r9.A04();
                            if (r9.A05.A0E(A02) && (A0A2 = A0A(A0M, r4)) != null) {
                                String str4 = "id=?";
                                boolean isEmpty2 = TextUtils.isEmpty(r4.A0L);
                                int i6 = 1;
                                if (!isEmpty2) {
                                    i6 = 2;
                                }
                                String[] strArr2 = new String[i6];
                                strArr2[0] = r4.A0K;
                                if (!isEmpty2) {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append(str4);
                                    sb3.append(" OR key_id=?");
                                    str4 = sb3.toString();
                                    strArr2[1] = r4.A0L;
                                }
                                C16330op r102 = A02.A03;
                                long A003 = (long) r102.A00("pay_transactions", A0A2, str4, strArr2);
                                long A023 = A003 != 1 ? r102.A02(A0A2, "pay_transactions") : -1;
                                if (A003 == 1 || A023 >= 0) {
                                    i4++;
                                } else {
                                    r10 = this.A09;
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append("could not update or insert transaction: ");
                                    sb4.append(r4.A0K);
                                    sb4.append(" update returned: ");
                                    sb4.append(A003);
                                    sb4.append(" insert returned: ");
                                    sb4.append(A023);
                                    str2 = sb4.toString();
                                }
                            }
                        } else {
                            C30931Zj r2 = this.A09;
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append("storeTransactions skipping store transaction with: ");
                            sb5.append(r4.A0K);
                            sb5.append(" as status is not updated  old ts: ");
                            sb5.append(A0M.A06);
                            sb5.append(" counter: ");
                            AbstractC30891Zf r0 = A0M.A0A;
                            if (r0 != null) {
                                i = r0.A05();
                            } else {
                                i = 0;
                            }
                            sb5.append(i);
                            sb5.append(" new ts: ");
                            sb5.append(r4.A06);
                            sb5.append(" counter: ");
                            AbstractC30891Zf r02 = r4.A0A;
                            if (r02 != null) {
                                i2 = r02.A05();
                            } else {
                                i2 = 0;
                            }
                            sb5.append(i2);
                            r2.A06(sb5.toString());
                            i4++;
                            i3++;
                        }
                    } else {
                        r10 = this.A09;
                        str2 = "could not update or insert transaction with empty transaction id";
                    }
                    r10.A06(str2);
                }
                A00.A00();
                A00.close();
                A02.close();
                if (!A0f()) {
                    i3 = i4;
                }
                int size = list.size();
                C30931Zj r22 = this.A09;
                if (i3 == size) {
                    sb = new StringBuilder();
                    str = "storeTransactions stored: ";
                } else {
                    sb = new StringBuilder("storeTransactions got: ");
                    sb.append(list.size());
                    str = " transactions but stored: ";
                }
                sb.append(str);
                sb.append(i3);
                r22.A06(sb.toString());
                if (i3 == list.size()) {
                    return true;
                }
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return false;
    }
}
