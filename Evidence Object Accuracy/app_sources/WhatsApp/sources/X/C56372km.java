package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;

/* renamed from: X.2km  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56372km extends AbstractC77963o9 {
    public final Context A00;

    @Override // X.AbstractC95064d1
    public final String A05() {
        return "com.google.android.gms.safetynet.internal.ISafetyNetService";
    }

    @Override // X.AbstractC95064d1
    public final String A06() {
        return "com.google.android.gms.safetynet.service.START";
    }

    @Override // X.AbstractC95064d1
    public final boolean A0A() {
        return true;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12200000;
    }

    public C56372km(Context context, Looper looper, AbstractC14980mM r10, AbstractC15000mO r11, AnonymousClass3BW r12) {
        super(context, looper, r10, r11, r12, 45);
        this.A00 = context;
    }

    @Override // X.AbstractC95064d1
    public final /* bridge */ /* synthetic */ IInterface A04(IBinder iBinder) {
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.safetynet.internal.ISafetyNetService");
        return !(queryLocalInterface instanceof C79913rP) ? new C79913rP(iBinder) : queryLocalInterface;
    }

    public final String A0C() {
        ApplicationInfo applicationInfo;
        Bundle bundle;
        try {
            Context context = this.A00;
            PackageManager packageManager = context.getPackageManager();
            if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128)) == null || (bundle = applicationInfo.metaData) == null)) {
                String str = (String) bundle.get("com.google.android.safetynet.ATTEST_API_KEY");
                if (str != null) {
                    return str;
                }
            }
            return "";
        } catch (PackageManager.NameNotFoundException unused) {
            return "";
        }
    }
}
