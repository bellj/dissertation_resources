package X;

import android.view.View;
import android.view.animation.Animation;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.3x8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83373x8 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ SelectionCheckView A01;

    public C83373x8(View view, SelectionCheckView selectionCheckView) {
        this.A01 = selectionCheckView;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.setVisibility(4);
    }
}
