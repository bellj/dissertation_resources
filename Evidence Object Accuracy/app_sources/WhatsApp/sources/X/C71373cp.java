package X;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3cp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71373cp implements Map, Serializable, AbstractC16910px {
    public static final C71373cp A00 = new C71373cp();
    public static final long serialVersionUID = 8246714829545688274L;

    @Override // java.util.Map
    public boolean containsKey(Object obj) {
        return false;
    }

    @Override // java.util.Map, java.lang.Object
    public int hashCode() {
        return 0;
    }

    @Override // java.util.Map
    public boolean isEmpty() {
        return true;
    }

    @Override // java.lang.Object
    public String toString() {
        return "{}";
    }

    @Override // java.util.Map
    public void clear() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Map
    public final /* bridge */ boolean containsValue(Object obj) {
        return false;
    }

    @Override // java.util.Map
    public final Set entrySet() {
        return C16900pw.A00;
    }

    @Override // java.util.Map, java.lang.Object
    public boolean equals(Object obj) {
        return (obj instanceof Map) && ((Map) obj).isEmpty();
    }

    @Override // java.util.Map
    public Object get(Object obj) {
        return null;
    }

    @Override // java.util.Map
    public final Set keySet() {
        return C16900pw.A00;
    }

    @Override // java.util.Map
    public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Map
    public void putAll(Map map) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    private final Object readResolve() {
        return A00;
    }

    @Override // java.util.Map
    public Object remove(Object obj) {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }

    @Override // java.util.Map
    public final int size() {
        return 0;
    }

    @Override // java.util.Map
    public final Collection values() {
        return AnonymousClass1WF.A00;
    }
}
