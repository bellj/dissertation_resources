package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1Wf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C30131Wf extends BroadcastReceiver {
    public final /* synthetic */ C247116o A00;

    public C30131Wf(C247116o r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        C247116o r1 = this.A00;
        if (r1.A02()) {
            Log.i("reload commerce translation metadata since locale changed");
            r1.A01();
        }
    }
}
