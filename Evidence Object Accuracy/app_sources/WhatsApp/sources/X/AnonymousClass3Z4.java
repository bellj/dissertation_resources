package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.3Z4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Z4 implements AbstractC21730xt {
    public final /* synthetic */ AnonymousClass1OE A00;
    public final /* synthetic */ C244315m A01;

    public AnonymousClass3Z4(AnonymousClass1OE r1, C244315m r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/sendInitLoginIq/onDeliveryFailure id=")));
        this.A00.APr("delivery failure", 3);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        C244315m.A00(r2, this.A00, str);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r13, String str) {
        String str2;
        AnonymousClass1OE r3 = this.A00;
        Log.i(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initLoginOnSuccess id=")));
        byte[] A01 = C244315m.A01(r13, r3, "ok_pub");
        byte[] A012 = C244315m.A01(r13, r3, "ok_key_signature");
        byte[] A013 = C244315m.A01(r13, r3, "hk_pub");
        byte[] A014 = C244315m.A01(r13, r3, "hk_key_signature");
        byte[] A015 = C244315m.A01(r13, r3, "ed_pub");
        byte[] A016 = C244315m.A01(r13, r3, "ed_key_signature");
        if (A01 != null && A012 != null && A013 != null && A014 != null && A015 != null && A016 != null) {
            AnonymousClass1V8 A0E = r13.A0E("count");
            int i = 1;
            if (A0E == null || A0E.A0I("value", null) == null) {
                Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initLoginOnSuccess/count was empty id=")));
                str2 = "count was empty";
            } else {
                try {
                    int parseInt = Integer.parseInt(A0E.A0I("value", null));
                    i = 2;
                    if (!C16550pE.A02(A01, A012)) {
                        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initLoginOnSuccess/ok_pub cannot be verified with ok_key_signature id=")));
                        str2 = "ok_pub cannot be verified with ok_key_signature";
                    } else if (!C16550pE.A02(A013, A014)) {
                        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initLoginOnSuccess/hk_pub cannot be verified with hk_key_signature id=")));
                        str2 = "hk_pub cannot be verified with hk_key_signature";
                    } else if (!C16550pE.A02(A015, A016)) {
                        Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initLoginOnSuccess/ed_pub cannot be verified with ed_key_signature id=")));
                        str2 = "ed_pub cannot be verified with ed_key_signature";
                    } else {
                        ((AnonymousClass1OF) r3).A00.A01();
                        C89654Ku r8 = new C89654Ku((NativeHolder) JniBridge.jvidispatchOOO(6, r3.A0D, A01));
                        C89644Kt r0 = new C89644Kt((NativeHolder) JniBridge.jvidispatchOO(27, r8.A00));
                        JniBridge.getInstance();
                        NativeHolder nativeHolder = r0.A00;
                        int jvidispatchIIO = (int) JniBridge.jvidispatchIIO(1, (long) 70, nativeHolder);
                        if (jvidispatchIIO != 0) {
                            r3.A08.APs(C12960it.A0W(jvidispatchIIO, "WESOpaqueClientCreateLoginStart failed with WESOpaqueStatusType="), 4, 1, -1, 0);
                            return;
                        }
                        JniBridge.getInstance();
                        byte[] bArr = (byte[]) JniBridge.jvidispatchOIO(0, (long) 71, nativeHolder);
                        synchronized (r3.A0C) {
                            r3.A06 = bArr;
                            r3.A05 = A015;
                            r3.A03 = r8;
                            r3.A01 = parseInt;
                            r3.A00 = 1;
                        }
                        r3.A00();
                        return;
                    }
                } catch (NumberFormatException unused) {
                    Log.e(C12960it.A0d(str, C12960it.A0k("EncryptedBackupProtocolHelper/initLoginOnSuccess/count is not numerical, id=")));
                    r3.APr("count is not numerical", 1);
                    return;
                }
            }
            r3.APr(str2, i);
        }
    }
}
