package X;

import android.content.Context;

/* renamed from: X.2y3  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2y3 extends C60752yZ {
    public boolean A00;

    public AnonymousClass2y3(Context context, AbstractC13890kV r2, AnonymousClass19D r3, AnonymousClass11P r4, C30421Xi r5) {
        super(context, r2, r3, r4, r5);
        A0Z();
    }

    @Override // X.AbstractC60572yF, X.AbstractC42681ve, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            C60872ym r2 = (C60872ym) this;
            AnonymousClass2P6 r3 = (AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent());
            AnonymousClass01J A08 = AnonymousClass1OY.A08(r3, r2);
            AnonymousClass1OY.A0L(A08, r2);
            AnonymousClass1OY.A0M(A08, r2);
            AnonymousClass1OY.A0K(A08, r2);
            AnonymousClass1OY.A0I(r3, A08, r2, AnonymousClass1OY.A09(A08, r2, AnonymousClass1OY.A0B(A08, r2)));
            AnonymousClass1OY.A0O(A08, r2);
            AnonymousClass1OY.A0N(A08, r2);
            r2.A01 = (C50532Px) r3.A02.get();
        }
    }
}
