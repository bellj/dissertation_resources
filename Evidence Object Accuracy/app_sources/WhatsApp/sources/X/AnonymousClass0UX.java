package X;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* renamed from: X.0UX  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UX {
    public AnonymousClass0J8 A00;
    public AnonymousClass0J9 A01;
    public boolean A02;

    public AnonymousClass0UX() {
        this(AnonymousClass0J8.screen, AnonymousClass0J9.Document);
    }

    public AnonymousClass0UX(AnonymousClass0J8 r2, AnonymousClass0J9 r3) {
        this.A00 = null;
        this.A01 = null;
        this.A02 = false;
        this.A00 = r2;
        this.A01 = r3;
    }

    public static int A00(AnonymousClass0I1 r4, List list, int i) {
        int i2 = 0;
        if (i >= 0) {
            Object obj = list.get(i);
            AbstractC12490i0 r0 = ((AnonymousClass0OO) r4).A00;
            if (obj == r0) {
                for (Object obj2 : r0.ABO()) {
                    if (obj2 != r4) {
                        i2++;
                    }
                }
            }
            return -1;
        }
        return i2;
    }

    public static List A01(AnonymousClass0I7 r9) {
        ArrayList arrayList = new ArrayList();
        while (!r9.A0D()) {
            String str = null;
            int i = r9.A01;
            String str2 = r9.A03;
            char charAt = str2.charAt(i);
            if ((charAt < 'A' || charAt > 'Z') && (charAt < 'a' || charAt > 'z')) {
                r9.A01 = i;
            } else {
                while (true) {
                    int A05 = r9.A05();
                    if (A05 < 65 || A05 > 90) {
                        if (A05 < 97 || A05 > 122) {
                            break;
                        }
                    }
                }
                str = str2.substring(i, r9.A01);
            }
            if (str == null) {
                break;
            }
            try {
                arrayList.add(AnonymousClass0J8.valueOf(str));
            } catch (IllegalArgumentException unused) {
            }
            if (!r9.A0E()) {
                break;
            }
        }
        return arrayList;
    }

    public static boolean A02(AnonymousClass0K2 r9, AnonymousClass0PE r10, AnonymousClass0I1 r11) {
        int size;
        ArrayList arrayList = new ArrayList();
        for (AbstractC12490i0 r0 = ((AnonymousClass0OO) r11).A00; r0 != null; r0 = ((AnonymousClass0OO) r0).A00) {
            arrayList.add(0, r0);
        }
        int size2 = arrayList.size() - 1;
        List list = r10.A01;
        if (list != null && list.size() == 1) {
            return A05(r9, (AnonymousClass0PJ) r10.A01.get(0), r11);
        }
        List list2 = r10.A01;
        if (list2 == null) {
            size = 0;
        } else {
            size = list2.size();
        }
        return A03(r9, r10, r11, arrayList, size - 1, size2);
    }

    public static boolean A03(AnonymousClass0K2 r4, AnonymousClass0PE r5, AnonymousClass0I1 r6, List list, int i, int i2) {
        AnonymousClass0PJ r1 = (AnonymousClass0PJ) r5.A01.get(i);
        if (A05(r4, r1, r6)) {
            AnonymousClass0JJ r12 = r1.A00;
            if (r12 == AnonymousClass0JJ.DESCENDANT) {
                if (i != 0) {
                    while (i2 >= 0) {
                        if (!A04(r4, r5, list, i - 1, i2)) {
                            i2--;
                        }
                    }
                }
                return true;
            } else if (r12 == AnonymousClass0JJ.CHILD) {
                return A04(r4, r5, list, i - 1, i2);
            } else {
                int A00 = A00(r6, list, i2);
                if (A00 > 0) {
                    return A03(r4, r5, (AnonymousClass0I1) ((AnonymousClass0OO) r6).A00.ABO().get(A00 - 1), list, i - 1, i2);
                }
            }
        }
        return false;
    }

    public static boolean A04(AnonymousClass0K2 r11, AnonymousClass0PE r12, List list, int i, int i2) {
        int i3 = i2;
        AnonymousClass0PJ r1 = (AnonymousClass0PJ) r12.A01.get(i);
        AnonymousClass0I1 r4 = (AnonymousClass0I1) list.get(i2);
        if (A05(r11, r1, r4)) {
            AnonymousClass0JJ r13 = r1.A00;
            if (r13 == AnonymousClass0JJ.DESCENDANT) {
                if (i == 0) {
                    return true;
                }
                while (i3 > 0) {
                    i3--;
                    if (A04(r11, r12, list, i - 1, i3)) {
                        return true;
                    }
                }
            } else if (r13 == AnonymousClass0JJ.CHILD) {
                return A04(r11, r12, list, i - 1, i2 - 1);
            } else {
                int A00 = A00(r4, list, i2);
                if (A00 > 0) {
                    return A03(r11, r12, (AnonymousClass0I1) ((AnonymousClass0OO) r4).A00.ABO().get(A00 - 1), list, i - 1, i3);
                }
            }
        }
        return false;
    }

    public static boolean A05(AnonymousClass0K2 r5, AnonymousClass0PJ r6, AnonymousClass0I1 r7) {
        boolean equals;
        List list;
        String str = r6.A01;
        if (str == null || str.equals(r7.A00().toLowerCase(Locale.US))) {
            List<AnonymousClass0NS> list2 = r6.A02;
            if (list2 != null) {
                for (AnonymousClass0NS r2 : list2) {
                    String str2 = r2.A01;
                    if (str2.equals("id")) {
                        equals = r2.A02.equals(r7.A03);
                        continue;
                    } else if (str2.equals("class") && (list = r7.A04) != null) {
                        equals = list.contains(r2.A02);
                        continue;
                    }
                    if (!equals) {
                        return false;
                    }
                }
            }
            List<AbstractC12120hP> list3 = r6.A03;
            if (list3 == null) {
                return true;
            }
            for (AbstractC12120hP r0 : list3) {
                if (!r0.ALK(r5, r7)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0131, code lost:
        if (r7.toString() != null) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0152, code lost:
        if (r12.A0G(")") == false) goto L_0x0133;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C05310Pb A06(X.AnonymousClass0I7 r12) {
        /*
        // Method dump skipped, instructions count: 686
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0UX.A06(X.0I7):X.0Pb");
    }
}
