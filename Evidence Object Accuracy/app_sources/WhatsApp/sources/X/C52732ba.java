package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C52732ba extends ArrayAdapter {
    public final Context A00;
    public final LayoutInflater A01;
    public final AnonymousClass130 A02;
    public final C15610nY A03;
    public final AnonymousClass1J1 A04;
    public final AnonymousClass018 A05;
    public final C14850m9 A06;
    public final AnonymousClass12F A07;

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getViewTypeCount() {
        return 3;
    }

    public C52732ba(Context context, AnonymousClass130 r3, C15610nY r4, AnonymousClass1J1 r5, AnonymousClass018 r6, C14850m9 r7, AnonymousClass12F r8, List list) {
        super(context, (int) R.layout.contact_picker_row, list);
        this.A00 = context;
        this.A06 = r7;
        this.A02 = r3;
        this.A03 = r4;
        this.A05 = r6;
        this.A07 = r8;
        this.A04 = r5;
        this.A01 = LayoutInflater.from(context);
    }

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public int getItemViewType(int i) {
        AnonymousClass5TX r0 = (AnonymousClass5TX) getItem(i);
        if (r0 == null) {
            return super.getItemViewType(i);
        }
        return r0.ADe();
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        AnonymousClass5TW r4;
        View view2 = view;
        AnonymousClass5TX r2 = (AnonymousClass5TX) getItem(i);
        if (r2 != null) {
            if (view == null) {
                int itemViewType = getItemViewType(i);
                if (itemViewType == 0) {
                    view2 = this.A01.inflate(R.layout.contact_picker_row, viewGroup, false);
                    C12980iv.A1B(view2, R.id.contactpicker_row_phone_type, 8);
                    Context context = this.A00;
                    C14850m9 r10 = this.A06;
                    r4 = new C68363Vc(context, view2, this.A03, this.A04, this.A05, r10, this.A07);
                } else if (itemViewType == 1) {
                    view2 = this.A01.inflate(R.layout.contact_picker_row, viewGroup, false);
                    C12980iv.A1B(view2, R.id.contactpicker_row_phone_type, 8);
                    r4 = new C68353Vb(view2, this.A02, this.A03, this.A07);
                } else if (itemViewType != 2) {
                    view2 = null;
                } else {
                    view2 = this.A01.inflate(R.layout.list_section, viewGroup, false);
                    r4 = new C68343Va(view2);
                }
                view2.setTag(r4);
            } else {
                r4 = (AnonymousClass5TW) view.getTag();
            }
            r4.ANG(r2);
            return view2;
        }
        return super.getView(i, view2, viewGroup);
    }
}
