package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.util.Log;

/* renamed from: X.12V  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass12V {
    public final C14900mE A00;
    public final C18160s0 A01;
    public final C18110rv A02;
    public final C18140ry A03;
    public final C20980wd A04;
    public final AbstractC14440lR A05;

    public AnonymousClass12V(C14900mE r2, C18160s0 r3, C18110rv r4, C18140ry r5, C20980wd r6, AbstractC14440lR r7) {
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r7, 2);
        C16700pc.A0E(r6, 4);
        C16700pc.A0E(r3, 6);
        this.A00 = r2;
        this.A05 = r7;
        this.A02 = r4;
        this.A04 = r6;
        this.A03 = r5;
        this.A01 = r3;
    }

    public final void A00(AnonymousClass1J7 r6) {
        Boolean A00 = this.A01.A00();
        if (A00 != null) {
            r6.AJ4(A00);
            return;
        }
        this.A05.Ab6(new RunnableBRunnable0Shape1S0300000_I0_1(this, new C42751vl(r6), new C42761vm(r6), 28));
    }

    public final boolean A01() {
        Boolean A00 = this.A01.A00();
        if (A00 != null) {
            return A00.booleanValue();
        }
        Log.i("AvatarRepository/unknown avatar config state, returning false.");
        return false;
    }
}
