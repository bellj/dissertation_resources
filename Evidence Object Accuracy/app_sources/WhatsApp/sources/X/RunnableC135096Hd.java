package X;

import android.view.animation.AccelerateDecelerateInterpolator;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import org.npci.commonlibrary.NPCIFragment;

/* renamed from: X.6Hd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135096Hd implements Runnable {
    public final /* synthetic */ NPCIFragment A00;

    public RunnableC135096Hd(NPCIFragment nPCIFragment) {
        this.A00 = nPCIFragment;
    }

    @Override // java.lang.Runnable
    public void run() {
        NPCIFragment nPCIFragment = this.A00;
        int i = nPCIFragment.A00;
        if (i != -1) {
            ArrayList arrayList = nPCIFragment.A0B;
            if (arrayList.get(i) instanceof C117795ag) {
                C117795ag A0s = C117305Zk.A0s(arrayList, i);
                AnonymousClass0QQ A01 = A0s.A01(A0s.A05, false);
                A01.A08(new AccelerateDecelerateInterpolator());
                A01.A01();
                A0s.Aez(AnonymousClass00T.A04(nPCIFragment.A0B(), R.drawable.ic_action_reload), new IDxCListenerShape2S0200000_3_I1(this, 52, A0s), nPCIFragment.A0I(R.string.npci_action_resend), 0, true, true);
            }
        }
    }
}
