package X;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/* renamed from: X.10Z  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10Z {
    public final C22260yn A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C15550nR A05;
    public final AnonymousClass10S A06;
    public final AnonymousClass10T A07;
    public final AnonymousClass10T A08;
    public final AnonymousClass10V A09;
    public final C18640sm A0A;
    public final AnonymousClass01d A0B;
    public final C14830m7 A0C;
    public final AnonymousClass018 A0D;
    public final C15650ng A0E;
    public final C15600nX A0F;
    public final AnonymousClass10Y A0G;
    public final C14850m9 A0H;
    public final C16120oU A0I;
    public final AnonymousClass12V A0J;
    public final C20750wG A0K;
    public final C22140ya A0L;
    public final AnonymousClass12U A0M;
    public final C20700wB A0N;
    public final AbstractC14440lR A0O;
    public final C14860mA A0P;

    public AnonymousClass10Z(C22260yn r2, C14330lG r3, C14900mE r4, C15570nT r5, C15550nR r6, AnonymousClass10S r7, AnonymousClass10T r8, AnonymousClass10V r9, C18640sm r10, AnonymousClass01d r11, C14830m7 r12, AnonymousClass018 r13, C15650ng r14, C15600nX r15, AnonymousClass10Y r16, C14850m9 r17, C16120oU r18, AnonymousClass12V r19, C20750wG r20, C22140ya r21, AnonymousClass12U r22, C20700wB r23, AbstractC14440lR r24, C14860mA r25) {
        this.A0H = r17;
        this.A02 = r4;
        this.A0O = r24;
        this.A01 = r3;
        this.A0M = r22;
        this.A0D = r13;
        this.A0B = r11;
        this.A0J = r19;
        this.A08 = r8;
        this.A0C = r12;
        this.A03 = r4;
        this.A04 = r5;
        this.A0I = r18;
        this.A0P = r25;
        this.A05 = r6;
        this.A00 = r2;
        this.A06 = r7;
        this.A0E = r14;
        this.A0N = r23;
        this.A0G = r16;
        this.A07 = r8;
        this.A09 = r9;
        this.A0K = r20;
        this.A0L = r21;
        this.A0F = r15;
        this.A0A = r10;
    }

    public C27611If A00(AbstractC14640lm r23, AnonymousClass1P4 r24, byte[] bArr, byte[] bArr2, boolean z) {
        C14830m7 r9 = this.A0C;
        C14900mE r3 = this.A03;
        C15570nT r4 = this.A04;
        C16120oU r13 = this.A0I;
        C14860mA r1 = this.A0P;
        C15550nR r5 = this.A05;
        AnonymousClass10S r6 = this.A06;
        C15650ng r10 = this.A0E;
        AnonymousClass10Y r12 = this.A0G;
        return new C27611If(r3, r4, r5, r6, this.A07, this.A09, r9, r10, this.A0F, r12, r13, r23, this, r24, this.A0L, r1, bArr, bArr2, z);
    }

    /* JADX INFO: finally extract failed */
    public C42801vt A01(File file) {
        byte[] bArr = new byte[(int) file.length()];
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            fileInputStream.read(bArr);
            fileInputStream.close();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            options.inDither = true;
            options.inPreferQualityOverSpeed = true;
            Bitmap bitmap = C37501mV.A06(new C41591tm(options, null, 96, 96, true), bArr).A02;
            if (bitmap != null) {
                Bitmap.Config config = bitmap.getConfig();
                if (config == null) {
                    config = Bitmap.Config.ARGB_8888;
                }
                Bitmap createBitmap = Bitmap.createBitmap(96, 96, config);
                Canvas canvas = new Canvas(createBitmap);
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setFilterBitmap(true);
                paint.setDither(true);
                canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), new Rect(0, 0, 96, 96), paint);
                bitmap.recycle();
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(this.A01.A0M("tmpt"));
                    createBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fileOutputStream);
                    fileOutputStream.close();
                } catch (IOException e) {
                    Log.e("profileinfo/sendphoto/cannot save thumb", e);
                }
                createBitmap.recycle();
                File A0M = this.A01.A0M("tmpt");
                byte[] bArr2 = new byte[(int) A0M.length()];
                FileInputStream fileInputStream2 = new FileInputStream(A0M);
                try {
                    fileInputStream2.read(bArr2);
                    fileInputStream2.close();
                    return new C42801vt(bArr, bArr2);
                } catch (Throwable th) {
                    try {
                        fileInputStream2.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } else {
                Log.e("profileinfo/sendphoto/cannot decode thumb");
                throw new FileNotFoundException();
            }
        } catch (Throwable th2) {
            try {
                fileInputStream.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public File A02(C15370n3 r4) {
        if (!(r4 instanceof AnonymousClass1VR)) {
            return this.A01.A0M("tmpi");
        }
        C14330lG r2 = this.A01;
        StringBuilder sb = new StringBuilder("tmpi");
        sb.append(((AnonymousClass1VR) r4).A00);
        return r2.A0M(sb.toString());
    }

    public void A03(Intent intent, ActivityC13810kN r5) {
        int intExtra = intent.getIntExtra("error_message_id", -1);
        if (intExtra > 0) {
            C14900mE r1 = this.A02;
            if (r5 != null) {
                r5.Ado(intExtra);
            } else {
                r1.A05(intExtra, 0);
            }
        }
    }

    public void A04(Intent intent, ActivityC13810kN r8, int i) {
        A05(intent, r8, r8, null, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r3 == null) goto L_0x0013;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A05(android.content.Intent r13, X.ActivityC13810kN r14, X.AbstractC13870kT r15, X.C15370n3 r16, int r17) {
        /*
            r12 = this;
            r10 = 0
            r4 = r14
            r9 = r16
            if (r13 == 0) goto L_0x0013
            android.net.Uri r3 = r13.getData()
            java.lang.String r0 = "webImageSource"
            java.lang.String r10 = r13.getStringExtra(r0)
            if (r3 != 0) goto L_0x0033
        L_0x0013:
            java.io.File r0 = r12.A02(r9)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x0027
            java.io.File r0 = r12.A02(r9)
            android.net.Uri r3 = android.net.Uri.fromFile(r0)
            if (r3 != 0) goto L_0x0033
        L_0x0027:
            java.lang.String r0 = "profileinfo/cropphoto/no-data"
            com.whatsapp.util.Log.e(r0)
            r0 = 2131888071(0x7f1207c7, float:1.9410767E38)
            r14.Ado(r0)
            return
        L_0x0033:
            X.01d r7 = r12.A0B
            X.018 r8 = r12.A0D
            X.10T r6 = r12.A08
            r0 = 0
            r5 = r15
            r11 = r17
            X.1vu r2 = new X.1vu
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            X.0lR r1 = r12.A0O
            java.lang.Void[] r0 = new java.lang.Void[r0]
            r1.Ab5(r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10Z.A05(android.content.Intent, X.0kN, X.0kT, X.0n3, int):void");
    }

    public void A06(ActivityC000800j r8, C15370n3 r9, int i) {
        A07(r8, r9, i, 1, true, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0147, code lost:
        if (r7 != false) goto L_0x0149;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x01e7, code lost:
        if (r10.A0K() == false) goto L_0x01e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x02b0, code lost:
        if (r7 != false) goto L_0x02b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0023, code lost:
        if (r10.A0K() == false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.ActivityC000800j r9, X.C15370n3 r10, int r11, int r12, boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 761
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10Z.A07(X.00j, X.0n3, int, int, boolean, boolean):void");
    }

    public void A08(C15370n3 r9) {
        if (!this.A0A.A0B()) {
            this.A03.A07(R.string.coldsync_no_network, 0);
            return;
        }
        C20750wG r1 = this.A0K;
        Jid A0B = r9.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        r1.A02(A00((AbstractC14640lm) A0B, null, null, null, false));
    }

    public void A09(C15370n3 r15) {
        int read;
        AnonymousClass10T r0 = this.A07;
        File A00 = r0.A00(r15);
        File A01 = r0.A01(r15);
        if (A00 != null && A00.exists() && A01 != null && A01.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(A01);
                FileInputStream fileInputStream2 = new FileInputStream(A00);
                int length = (int) A01.length();
                byte[] bArr = new byte[length];
                int i = 0;
                int i2 = 0;
                while (i2 < length && (read = fileInputStream.read(bArr, i2, length - i2)) != -1) {
                    i2 += read;
                }
                int length2 = (int) A00.length();
                byte[] bArr2 = new byte[length2];
                while (i < length2) {
                    int read2 = fileInputStream2.read(bArr2, i, length2 - i);
                    if (read2 == -1) {
                        break;
                    }
                    i += read2;
                }
                AbstractC14640lm r9 = (AbstractC14640lm) r15.A0B(AbstractC14640lm.class);
                AnonymousClass009.A05(r9);
                C27611If A002 = A00(r9, null, bArr2, bArr, false);
                A002.A04 = true;
                this.A0K.A02(A002);
                fileInputStream2.close();
                fileInputStream.close();
            } catch (IOException e) {
                StringBuilder sb = new StringBuilder("profileinfo/resend/jid ");
                sb.append(r15.A0D);
                sb.append("/failed");
                Log.e(sb.toString(), e);
            }
        }
    }

    public boolean A0A(C15370n3 r4) {
        File file = this.A07.A00.A04().A09;
        C14330lG.A03(file, false);
        return A0B(r4, C14330lG.A00(file, "tmpp"), false);
    }

    public boolean A0B(C15370n3 r12, File file, boolean z) {
        if (!this.A0A.A0B()) {
            this.A03.A07(R.string.coldsync_no_network, 0);
            return false;
        }
        try {
            C42801vt A01 = A01(file);
            C20750wG r1 = this.A0K;
            Jid A0B = r12.A0B(AbstractC14640lm.class);
            AnonymousClass009.A05(A0B);
            r1.A02(A00((AbstractC14640lm) A0B, null, A01.A00, A01.A01, z));
            return true;
        } catch (FileNotFoundException | IOException e) {
            this.A03.A07(R.string.error_load_image, 0);
            Log.e("profileinfo/sendphoto", e);
            return false;
        }
    }
}
