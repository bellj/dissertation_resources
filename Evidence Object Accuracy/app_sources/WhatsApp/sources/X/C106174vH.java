package X;

import android.content.Context;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* renamed from: X.4vH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106174vH implements AnonymousClass5VV {
    public AnonymousClass4A5 A00;
    public AnonymousClass4BK A01;
    public AnonymousClass4BH A02;
    public C73063fc A03;
    public C73063fc A04;
    public AbstractC93424a9 A05;
    public final Context A06;
    public final Path A07;
    public final RectF A08;
    public final AnonymousClass018 A09;
    public final AnonymousClass5VD A0A;
    public final AnonymousClass48L A0B;
    public final AbstractC16710pd A0C = new AnonymousClass1WL(new C113955Jq(this));

    public C106174vH(Context context, AnonymousClass018 r5, AnonymousClass5VD r6, AnonymousClass4BK r7, AnonymousClass4BH r8, AbstractC93424a9 r9) {
        C73063fc r1;
        this.A06 = context;
        this.A09 = r5;
        this.A0A = r6;
        this.A01 = r7;
        AnonymousClass48L r2 = new AnonymousClass48L();
        this.A0B = r2;
        this.A07 = new Path();
        this.A00 = AnonymousClass4A5.A02;
        this.A08 = new RectF();
        this.A04 = new C73063fc(context, C93014Yp.A00(context, r8), r2);
        if (r9 == null) {
            r1 = null;
        } else {
            r1 = new C73063fc(context, C93014Yp.A00(context, r8), r9);
        }
        this.A03 = r1;
        this.A02 = r8;
        this.A05 = r9;
    }

    public static void A00(Drawable drawable, C106174vH r5, float f) {
        RectF rectF = new RectF(r5.A08);
        float f2 = (float) 2;
        rectF.inset((rectF.width() / f2) * f, (rectF.height() / f2) * f);
        Rect rect = new Rect();
        rectF.roundOut(rect);
        drawable.setBounds(rect);
    }

    public final void A01() {
        AnonymousClass4BH r0 = this.A02;
        Context context = this.A06;
        AnonymousClass4V8 A00 = C93014Yp.A00(context, r0);
        AbstractC93424a9 r1 = this.A05;
        C73063fc r02 = null;
        if (r1 != null) {
            C16700pc.A0E(context, 1);
            r02 = new C73063fc(context, A00, r1);
        }
        this.A03 = r02;
        AnonymousClass48L r12 = this.A0B;
        C16700pc.A0F(r12, context);
        this.A04 = new C73063fc(context, A00, r12);
    }

    public final void A02() {
        float f = (float) ((AnonymousClass4YC) this.A0C.getValue()).A09.A00;
        C73063fc r10 = this.A03;
        if (r10 != null) {
            A00(r10, this, f);
            r10.setAlpha((int) (((double) 255) * (1.0d - ((double) f))));
        }
        C73063fc r2 = this.A04;
        A00(r2, this, (float) (1.0d - ((double) f)));
        r2.setAlpha((int) (((float) 255) * f));
        Path path = this.A07;
        path.reset();
        C73063fc r0 = this.A03;
        if (r0 != null) {
            path.addPath(r0.A05);
        }
        path.addPath(this.A04.A05);
    }

    @Override // X.AnonymousClass5VV
    public void AWD(AnonymousClass4YC r6) {
        AnonymousClass4A5 r0;
        double d = r6.A09.A00;
        if (d == 0.0d) {
            r0 = AnonymousClass4A5.A02;
        } else if (d == 1.0d) {
            r0 = AnonymousClass4A5.A01;
        } else {
            return;
        }
        this.A00 = r0;
    }

    @Override // X.AnonymousClass5VV
    public void AWE(AnonymousClass4YC r2) {
        A02();
        this.A0A.invalidate();
    }
}
