package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;

/* renamed from: X.4iv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98584iv implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        GoogleSignInOptions googleSignInOptions = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c != 5) {
                C95664e9.A0D(parcel, readInt);
            } else {
                googleSignInOptions = (GoogleSignInOptions) C95664e9.A07(parcel, GoogleSignInOptions.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new SignInConfiguration(googleSignInOptions, str);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new SignInConfiguration[i];
    }
}
