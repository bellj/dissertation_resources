package X;

import android.text.TextUtils;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.whatsapp.conversationslist.ConversationsFragment;
import java.util.ArrayList;

/* renamed from: X.1kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36901kp extends BaseAdapter implements Filterable {
    public AnonymousClass2LB A00 = new AnonymousClass2LB();
    public ArrayList A01 = new ArrayList();
    public final Filter A02 = new C52882bq(this);
    public final C15550nR A03;
    public final AnonymousClass5U4 A04;
    public final AnonymousClass018 A05;
    public final C15860o1 A06;
    public final /* synthetic */ ConversationsFragment A07;

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 2;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public C36901kp(C15550nR r2, ConversationsFragment conversationsFragment, AnonymousClass5U4 r4, AnonymousClass018 r5, C15860o1 r6) {
        this.A07 = conversationsFragment;
        this.A03 = r2;
        this.A05 = r5;
        this.A06 = r6;
        this.A04 = r4;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A07.A2F.size();
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        return this.A02;
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A07.A2F.get(i);
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) this.A07.A2F.get(i).hashCode();
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        return this.A07.A2F.get(i) instanceof C1103455e ? 1 : 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0133, code lost:
        if (r2.A07(442) != false) goto L_0x0135;
     */
    @Override // android.widget.Adapter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r54, android.view.View r55, android.view.ViewGroup r56) {
        /*
        // Method dump skipped, instructions count: 377
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36901kp.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean isEmpty() {
        if (super.isEmpty()) {
            return this.A07.A00 == 0 || !TextUtils.isEmpty(this.A00.A01);
        }
        return false;
    }
}
