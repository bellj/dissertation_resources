package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;

/* renamed from: X.0IA  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IA extends AnonymousClass03S {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public float A08;
    public float A09;
    public float A0A;
    public float A0B;
    public float A0C;
    public boolean A0D;
    public final Paint A0E = new Paint(1);

    public AnonymousClass0IA(AnonymousClass04Q r6) {
        super(r6);
        float f = super.A05;
        this.A08 = 12.0f * f;
        float f2 = 37.0f * f;
        this.A05 = f2;
        this.A00 = 0.5f * f;
        this.A0B = f * 2.0f;
        this.A07 = 5.0f * f;
        this.A09 = 8.0f * f;
        this.A0A = f * 3.0f;
        super.A03 = 5;
        super.A02 = 3.0f;
        this.A03 = ((f * 48.0f) - f2) / 2.0f;
    }

    @Override // X.AnonymousClass03S
    public int A00(float f, float f2) {
        float f3 = this.A04;
        float f4 = this.A05;
        float f5 = f3 - f4;
        if (f >= f5 && f <= f3) {
            float f6 = this.A06;
            if (f2 >= f6 && f2 <= f6 + f4) {
                this.A0D = true;
                return 2;
            }
        }
        float f7 = this.A03;
        if (f >= f5 - f7 && f <= f3 + f7) {
            float f8 = this.A06;
            if (f2 >= f8 - f7 && f2 <= f8 + f4 + f7) {
                this.A0D = true;
                return 1;
            }
        }
        this.A0D = false;
        return 0;
    }

    @Override // X.AnonymousClass03S
    public void A03(float f, float f2) {
        A01();
    }

    @Override // X.AnonymousClass03S
    public void A04(float f, float f2) {
        this.A0D = false;
        A01();
    }

    @Override // X.AnonymousClass03S
    public boolean A05(float f, float f2) {
        AnonymousClass04Q r6 = super.A09;
        Location location = r6.A0U.A00;
        if (location == null) {
            return true;
        }
        r6.A09(AnonymousClass0R9.A01(new AnonymousClass03T(location.getLatitude(), location.getLongitude()), 15.0f));
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r6 <= (r1 + r2)) goto L_0x0025;
     */
    @Override // X.AnonymousClass03S
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A06(float r5, float r6, float r7, float r8) {
        /*
            r4 = this;
            boolean r0 = r4.A0D
            r3 = 0
            if (r0 == 0) goto L_0x0025
            float r1 = r4.A04
            float r2 = r4.A05
            float r0 = r1 - r2
            int r0 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x001e
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x001e
            float r1 = r4.A06
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x001e
            float r1 = r1 + r2
            int r0 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0025
        L_0x001e:
            r4.A0D = r3
            r4.A01()
            r0 = 1
            return r0
        L_0x0025:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IA.A06(float, float, float, float):boolean");
    }

    @Override // X.AnonymousClass03S
    public void A08(Canvas canvas) {
        AnonymousClass04Q r1 = super.A09;
        float f = this.A08;
        float width = (((float) r1.A0E.getWidth()) - f) - ((float) r1.A05);
        this.A04 = width;
        float f2 = f + ((float) r1.A06);
        this.A06 = f2;
        float f3 = this.A05;
        float f4 = f3 / 2.0f;
        this.A01 = width - f4;
        this.A02 = f2 + f4;
        float f5 = this.A09;
        this.A0C = this.A0A + f5;
        Paint paint = this.A0E;
        paint.setStyle(Paint.Style.FILL);
        int i = -1;
        if (this.A0D) {
            i = -2236963;
        }
        paint.setColor(i);
        paint.setAlpha(230);
        float f6 = this.A04;
        float f7 = this.A06;
        canvas.drawRect(f6 - f3, f7, f6, f7 + f3, paint);
        paint.setColor(-7829368);
        canvas.drawCircle(this.A01, this.A02, this.A07, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(this.A0B);
        canvas.drawCircle(this.A01, this.A02, f5, paint);
        float f8 = this.A01;
        float f9 = this.A02;
        canvas.drawLine(f8, f9 - f5, f8, f9 - this.A0C, paint);
        float f10 = this.A01;
        float f11 = this.A02;
        canvas.drawLine(f10, f11 + f5, f10, f11 + this.A0C, paint);
        float f12 = this.A01;
        float f13 = this.A02;
        canvas.drawLine(f12 - f5, f13, f12 - this.A0C, f13, paint);
        float f14 = this.A01;
        float f15 = this.A02;
        canvas.drawLine(f14 + f5, f15, f14 + this.A0C, f15, paint);
        paint.setStrokeWidth(this.A00);
        paint.setColor(-3355444);
        float f16 = this.A04;
        float f17 = this.A06;
        canvas.drawRect(f16 - f3, f17, f16, f17 + f3, paint);
    }
}
