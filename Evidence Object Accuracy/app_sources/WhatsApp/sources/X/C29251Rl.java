package X;

import android.database.Cursor;

/* renamed from: X.1Rl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29251Rl {
    public final C14830m7 A00;
    public final AnonymousClass1RO A01;

    public C29251Rl(C14830m7 r1, AnonymousClass1RO r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public C29261Rm A00(C15980oF r13) {
        C16310on A01 = this.A01.get();
        try {
            Cursor A08 = A01.A03.A08("fast_ratchet_sender_keys", "group_id = ? AND sender_id = ? AND sender_type = ? AND device_id = ?", null, null, new String[]{"record"}, r13.A00());
            if (!A08.moveToNext()) {
                A08.close();
                A01.close();
                return null;
            }
            C29261Rm r0 = new C29261Rm(A08.getBlob(0), this.A00.A00() / 1000);
            A08.close();
            A01.close();
            return r0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
