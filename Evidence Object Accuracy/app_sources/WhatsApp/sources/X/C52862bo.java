package X;

import android.widget.Filter;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.2bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52862bo extends Filter {
    public final /* synthetic */ C54512gq A00;

    public /* synthetic */ C52862bo(C54512gq r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x0075 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x005b A[SYNTHETIC] */
    @Override // android.widget.Filter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.widget.Filter.FilterResults performFiltering(java.lang.CharSequence r11) {
        /*
            r10 = this;
            android.widget.Filter$FilterResults r7 = new android.widget.Filter$FilterResults
            r7.<init>()
            r6 = 0
            if (r11 == 0) goto L_0x00b2
            int r0 = r11.length()
            if (r0 != 0) goto L_0x001b
            X.2gq r0 = r10.A00
            java.util.List r0 = r0.A06
            r7.values = r0
            int r0 = r0.size()
        L_0x0018:
            r7.count = r0
            return r7
        L_0x001b:
            java.lang.String r0 = r11.toString()
            java.lang.String r3 = " "
            boolean r0 = r0.startsWith(r3)
            if (r0 != 0) goto L_0x00b2
            int r2 = r11.length()
            java.lang.String r1 = r11.toString()
            java.lang.String r0 = ""
            java.lang.String r0 = r1.replaceAll(r3, r0)
            int r0 = r0.length()
            int r2 = r2 - r0
            r5 = 1
            if (r2 > r5) goto L_0x00b2
            java.util.ArrayList r4 = X.C12960it.A0l()
            java.lang.String r0 = r11.toString()
            java.lang.String r3 = r0.toLowerCase()
            java.lang.String r1 = r11.toString()
            X.2gq r0 = r10.A00
            X.018 r8 = r0.A0E
            java.util.ArrayList r2 = X.C32751cg.A02(r8, r1)
            java.util.List r0 = r0.A06
            java.util.Iterator r9 = r0.iterator()
        L_0x005b:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x00aa
            X.0n3 r1 = X.C12970iu.A0a(r9)
            boolean r0 = r1.A0L()
            if (r0 == 0) goto L_0x0079
            java.lang.String r0 = X.C15610nY.A02(r1, r6)
        L_0x006f:
            boolean r0 = X.C32751cg.A03(r8, r0, r2, r5)
        L_0x0073:
            if (r0 == 0) goto L_0x005b
        L_0x0075:
            r4.add(r1)
            goto L_0x005b
        L_0x0079:
            java.lang.String r0 = r1.A0K
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0084
            java.lang.String r0 = r1.A0K
            goto L_0x006f
        L_0x0084:
            java.lang.String r0 = r1.A0U
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0095
            java.lang.String r0 = r1.A0U
            boolean r0 = X.C32751cg.A03(r8, r0, r2, r5)
            if (r0 == 0) goto L_0x0095
            goto L_0x0075
        L_0x0095:
            java.lang.Class<com.whatsapp.jid.UserJid> r0 = com.whatsapp.jid.UserJid.class
            com.whatsapp.jid.Jid r0 = r1.A0B(r0)
            X.0lm r0 = (X.AbstractC14640lm) r0
            if (r0 == 0) goto L_0x005b
            java.lang.String r0 = X.C248917h.A03(r0)
            if (r0 == 0) goto L_0x005b
            boolean r0 = r0.contains(r3)
            goto L_0x0073
        L_0x00aa:
            r7.values = r4
            int r0 = r4.size()
            goto L_0x0018
        L_0x00b2:
            java.util.ArrayList r0 = X.C12960it.A0l()
            r7.values = r0
            r7.count = r6
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C52862bo.performFiltering(java.lang.CharSequence):android.widget.Filter$FilterResults");
    }

    @Override // android.widget.Filter
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        int i;
        String str;
        Set set;
        Object obj = filterResults.values;
        if (obj instanceof List) {
            C54512gq r5 = this.A00;
            List list = (List) obj;
            r5.A07 = list;
            C71283cg r0 = r5.A04;
            if (r0 != null) {
                Collections.sort(list, r0);
            }
            List list2 = r5.A07;
            C71283cg r02 = r5.A04;
            if (!(r02 == null || (set = r02.A00) == null)) {
                Iterator it = list2.iterator();
                i = -1;
                while (it.hasNext()) {
                    if (!C15370n3.A07(C12970iu.A0a(it), set)) {
                        break;
                    }
                    i++;
                }
            }
            i = -1;
            r5.A01 = i;
            if (charSequence != null) {
                str = charSequence.toString().toLowerCase();
            } else {
                str = "";
            }
            r5.A05 = str;
            r5.A02();
        }
    }
}
