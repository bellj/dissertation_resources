package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

/* renamed from: X.07r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C016307r {
    public static void A00(ColorStateList colorStateList, ImageView imageView) {
        Drawable drawable;
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            C016207q.A02(colorStateList, imageView);
            if (i == 21 && (drawable = imageView.getDrawable()) != null && C016207q.A00(imageView) != null) {
                if (drawable.isStateful()) {
                    drawable.setState(imageView.getDrawableState());
                }
                imageView.setImageDrawable(drawable);
            }
        } else if (imageView instanceof AnonymousClass03Y) {
            ((AnonymousClass03Y) imageView).setSupportImageTintList(colorStateList);
        }
    }

    public static void A01(PorterDuff.Mode mode, ImageView imageView) {
        Drawable drawable;
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            C016207q.A03(mode, imageView);
            if (i == 21 && (drawable = imageView.getDrawable()) != null && C016207q.A00(imageView) != null) {
                if (drawable.isStateful()) {
                    drawable.setState(imageView.getDrawableState());
                }
                imageView.setImageDrawable(drawable);
            }
        } else if (imageView instanceof AnonymousClass03Y) {
            ((AnonymousClass03Y) imageView).setSupportImageTintMode(mode);
        }
    }
}
