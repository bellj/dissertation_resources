package X;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

/* renamed from: X.2Qs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50732Qs {
    public static final TimeInterpolator A00 = new DecelerateInterpolator();
    public static final TimeInterpolator A01 = new C016007o();
    public static final TimeInterpolator A02 = new C015907n();
    public static final TimeInterpolator A03 = new LinearInterpolator();
    public static final TimeInterpolator A04 = new AnonymousClass078();
}
