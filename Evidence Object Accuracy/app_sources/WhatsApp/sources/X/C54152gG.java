package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.2gG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54152gG extends AnonymousClass02L {
    public Map A00 = C12970iu.A11();

    public C54152gG() {
        super(new AnonymousClass0SC(new C74623iP()).A00());
    }

    public static C75683kF A00(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new AnonymousClass42U(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.conversation_icebreaker_welcome_message));
        }
        if (i == 2) {
            return new C61292zn(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.conversation_icebreaker_question));
        }
        if (i == 3) {
            return new C75683kF(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.conversation_icebreaker_divider));
        }
        String A0W = C12960it.A0W(i, "ItemAdapter/onCreateViewHolder type not handled - ");
        Log.e(A0W);
        throw C12960it.A0U(A0W);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        ((C75683kF) r2).A08((AnonymousClass4WS) A0E(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return A00(viewGroup, i);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4WS) A0E(i)).A00;
    }
}
