package X;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape0S0301000_I0;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.facebook.redex.RunnableBRunnable0Shape2S0300000_I0_2;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: X.0xC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21300xC {
    public static final Uri A09 = Uri.parse("");
    public AbstractC28651Ol A00;
    public ExecutorService A01;
    public final Handler A02 = new AnonymousClass22I(Looper.getMainLooper(), this);
    public final C14900mE A03;
    public final AnonymousClass11R A04;
    public final AnonymousClass11P A05;
    public final AnonymousClass01d A06;
    public final C16590pI A07;
    public final C21310xD A08;

    public C21300xC(C14900mE r9, AnonymousClass11R r10, AnonymousClass11P r11, AnonymousClass01d r12, C16590pI r13, C21310xD r14, AbstractC14440lR r15) {
        this.A07 = r13;
        this.A03 = r9;
        this.A06 = r12;
        this.A08 = r14;
        this.A05 = r11;
        this.A04 = r10;
        this.A01 = r15.A8a("AsyncAudioPlayer", new LinkedBlockingQueue(), 0, 1, 0, 60);
    }

    public void A00() {
        this.A01.submit(new RunnableBRunnable0Shape13S0100000_I0_13(this, 12));
    }

    public void A01(Uri uri) {
        if (uri.compareTo(A09) != 0) {
            Handler handler = this.A02;
            handler.removeMessages(99);
            handler.sendEmptyMessageDelayed(99, 10000);
            A00();
            if (!this.A08.A00) {
                Context context = this.A07.A00;
                if (this.A04.A00) {
                    AudioManager A0G = this.A06.A0G();
                    if (A0G == null || A0G.getStreamVolume(5) > 0) {
                        this.A01.submit(new RunnableBRunnable0Shape0S0301000_I0(this, context, uri, 3, 7));
                        return;
                    }
                    return;
                }
                this.A03.A0H(new RunnableBRunnable0Shape2S0300000_I0_2(this, context, uri, 2));
            }
        }
    }
}
