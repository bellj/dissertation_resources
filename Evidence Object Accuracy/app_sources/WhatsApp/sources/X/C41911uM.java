package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1uM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41911uM extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C41911uM A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public int A02;
    public int A03 = -1;
    public long A04;
    public AbstractC41941uP A05 = C56822m0.A02;

    static {
        C41911uM r0 = new C41911uM();
        A06 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A06;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C41911uM r2 = (C41911uM) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A02;
                int i3 = r2.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A02 = r7.Afp(i2, r2.A02, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                long j = this.A04;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A04 = r7.Afs(j, r2.A04, z3, z4);
                boolean z5 = false;
                if ((i & 4) == 4) {
                    z5 = true;
                }
                int i4 = this.A01;
                boolean z6 = false;
                if ((i3 & 4) == 4) {
                    z6 = true;
                }
                this.A01 = r7.Afp(i4, r2.A01, z5, z6);
                this.A05 = r7.Afq(this.A05, r2.A05);
                if (r7 == C463025i.A00) {
                    this.A00 |= r2.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r72.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 8) {
                            this.A00 |= 1;
                            this.A02 = r72.A02();
                        } else if (A03 == 16) {
                            this.A00 |= 2;
                            this.A04 = r72.A06();
                        } else if (A03 == 24) {
                            this.A00 |= 4;
                            this.A01 = r72.A02();
                        } else if (A03 == 32) {
                            AbstractC41941uP r22 = this.A05;
                            if (!((AnonymousClass1K7) r22).A00) {
                                r22 = AbstractC27091Fz.A0F(r22);
                                this.A05 = r22;
                            }
                            C56822m0 r23 = (C56822m0) r22;
                            r23.A02(r23.A00, r72.A02());
                        } else if (A03 == 34) {
                            int A04 = r72.A04(r72.A02());
                            AbstractC41941uP r1 = this.A05;
                            if (!((AnonymousClass1K7) r1).A00 && r72.A00() > 0) {
                                this.A05 = AbstractC27091Fz.A0F(r1);
                            }
                            while (r72.A00() > 0) {
                                C56822m0 r24 = (C56822m0) this.A05;
                                r24.A02(r24.A00, r72.A02());
                            }
                            r72.A03 = A04;
                            r72.A0B();
                        } else if (!A0a(r72, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r12 = new C28971Pt(e2.getMessage());
                        r12.unfinishedMessage = this;
                        throw new RuntimeException(r12);
                    }
                }
                break;
            case 3:
                ((AnonymousClass1K7) this.A05).A00 = false;
                return null;
            case 4:
                return new C41911uM();
            case 5:
                return new C81443u1();
            case 6:
                break;
            case 7:
                if (A07 == null) {
                    synchronized (C41911uM.class) {
                        if (A07 == null) {
                            A07 = new AnonymousClass255(A06);
                        }
                    }
                }
                return A07;
            default:
                throw new UnsupportedOperationException();
        }
        return A06;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2;
        int i3 = ((AbstractC27091Fz) this).A00;
        if (i3 != -1) {
            return i3;
        }
        int i4 = this.A00;
        if ((i4 & 1) == 1) {
            i = CodedOutputStream.A04(1, this.A02) + 0;
        } else {
            i = 0;
        }
        if ((i4 & 2) == 2) {
            i += CodedOutputStream.A06(2, this.A04);
        }
        if ((i4 & 4) == 4) {
            i += CodedOutputStream.A04(3, this.A01);
        }
        int i5 = 0;
        for (int i6 = 0; i6 < this.A05.size(); i6++) {
            C56822m0 r0 = (C56822m0) this.A05;
            r0.A01(i6);
            i5 += CodedOutputStream.A01(r0.A01[i6]);
        }
        int i7 = i + i5;
        if (!this.A05.isEmpty()) {
            int i8 = i7 + 1;
            if (i5 >= 0) {
                i2 = CodedOutputStream.A01(i5);
            } else {
                i2 = 10;
            }
            i7 = i8 + i2;
        }
        this.A03 = i5;
        int A00 = i7 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        AGd();
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0H(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A01);
        }
        if (this.A05.size() > 0) {
            codedOutputStream.A0C(34);
            codedOutputStream.A0C(this.A03);
        }
        for (int i = 0; i < this.A05.size(); i++) {
            C56822m0 r0 = (C56822m0) this.A05;
            r0.A01(i);
            codedOutputStream.A0C(r0.A01[i]);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
