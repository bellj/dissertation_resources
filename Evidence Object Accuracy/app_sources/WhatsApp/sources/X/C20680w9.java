package X;

/* renamed from: X.0w9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20680w9 {
    public final C15570nT A00;
    public final C16170oZ A01;
    public final C18850tA A02;

    public C20680w9(C15570nT r1, C16170oZ r2, C18850tA r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0039  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass1P4 r10, java.lang.String r11) {
        /*
            r9 = this;
            r6 = r11
            if (r10 != 0) goto L_0x004a
            X.0tA r2 = r9.A02
            X.11n r1 = r2.A0M
            java.lang.String r0 = "setting_pushName"
            java.lang.Object r0 = r1.A03(r0)
            X.1H3 r0 = (X.AnonymousClass1H3) r0
            if (r0 == 0) goto L_0x004a
            X.0m7 r0 = r0.A01
            long r7 = r0.A00()
            r4 = 0
            r5 = r4
            X.1gM r3 = new X.1gM
            r3.<init>(r4, r5, r6, r7)
            java.util.List r0 = java.util.Collections.singletonList(r3)
            java.util.Set r1 = r2.A0A(r0)
        L_0x0027:
            X.0nT r0 = r9.A00
            r0.A0C(r11)
            X.0tA r0 = r9.A02
            r0.A0O(r1)
            X.0oZ r1 = r9.A01
            X.0og r0 = r1.A0D
            boolean r0 = r0.A06
            if (r0 == 0) goto L_0x0049
            X.0qS r4 = r1.A19
            android.util.Pair r3 = android.util.Pair.create(r11, r10)
            r2 = 0
            r1 = 0
            r0 = 3
            android.os.Message r0 = android.os.Message.obtain(r2, r1, r0, r1, r3)
            r4.A08(r0, r1)
        L_0x0049:
            return
        L_0x004a:
            java.util.Set r1 = java.util.Collections.emptySet()
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20680w9.A00(X.1P4, java.lang.String):void");
    }
}
