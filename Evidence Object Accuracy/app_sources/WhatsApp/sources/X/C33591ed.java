package X;

import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.storage.StorageUsageActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.1ed  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33591ed implements AbstractC28051Kk {
    public final /* synthetic */ StorageUsageActivity A00;

    public C33591ed(StorageUsageActivity storageUsageActivity) {
        this.A00 = storageUsageActivity;
    }

    @Override // X.AbstractC28051Kk
    public void ANs(AnonymousClass4KZ r5) {
        Log.i("storage-usage-activity/fetch chats/completed");
        StorageUsageActivity storageUsageActivity = this.A00;
        ArrayList arrayList = r5.A00;
        storageUsageActivity.A0L = arrayList;
        StorageUsageActivity.A02(storageUsageActivity, arrayList, null, true);
        ((ActivityC13810kN) storageUsageActivity).A05.A0H(new RunnableBRunnable0Shape12S0100000_I0_12(this, 31));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        r3.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f4, code lost:
        if (r3 >= r7.size()) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f6, code lost:
        r1 = (X.C28021Kd) r7.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0100, code lost:
        if (X.C21600xg.A00(r1) != false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0102, code lost:
        r8.add(r1);
        ((java.util.List) r4.A01).add(java.lang.Integer.valueOf(r8.size() - 1));
        r3 = r3 + 1;
     */
    @Override // X.AbstractC28051Kk
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANt(X.C89454Ka r12) {
        /*
        // Method dump skipped, instructions count: 282
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C33591ed.ANt(X.4Ka):void");
    }

    @Override // X.AbstractC28051Kk
    public void AOw(C28011Kc r4, AbstractC14640lm r5) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape1S0300000_I0_1(this, r5, r4, 48));
    }
}
