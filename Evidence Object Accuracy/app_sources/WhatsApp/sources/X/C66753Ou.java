package X;

import com.whatsapp.businessdirectory.view.activity.DirectorySetNeighborhoodActivity;
import com.whatsapp.businessdirectory.viewmodel.DirectorySetNeighborhoodViewModel;
import java.util.ArrayList;

/* renamed from: X.3Ou  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66753Ou implements AnonymousClass07L {
    public final /* synthetic */ DirectorySetNeighborhoodActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66753Ou(DirectorySetNeighborhoodActivity directorySetNeighborhoodActivity) {
        this.A00 = directorySetNeighborhoodActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        DirectorySetNeighborhoodViewModel directorySetNeighborhoodViewModel = this.A00.A05;
        ArrayList A0l = C12960it.A0l();
        if (directorySetNeighborhoodViewModel.A03.A00() == null) {
            return true;
        }
        directorySetNeighborhoodViewModel.A06(str, ((AnonymousClass4T7) C12980iv.A0o(directorySetNeighborhoodViewModel.A08)).A05, A0l);
        directorySetNeighborhoodViewModel.A07(directorySetNeighborhoodViewModel.A04(A0l));
        return true;
    }
}
