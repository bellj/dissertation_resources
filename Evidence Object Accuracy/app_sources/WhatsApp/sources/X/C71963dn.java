package X;

import com.whatsapp.catalogsearch.view.fragment.CatalogSearchFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3dn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71963dn extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ CatalogSearchFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71963dn(CatalogSearchFragment catalogSearchFragment) {
        super(0);
        this.this$0 = catalogSearchFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        CatalogSearchFragment catalogSearchFragment = this.this$0;
        UserJid userJid = catalogSearchFragment.A0P;
        if (userJid == null) {
            throw C16700pc.A06("bizJid");
        }
        AbstractC14440lR r3 = catalogSearchFragment.A0Q;
        if (r3 != null) {
            C21770xx r2 = catalogSearchFragment.A0I;
            if (r2 != null) {
                AnonymousClass19Q r1 = catalogSearchFragment.A0K;
                if (r1 != null) {
                    return new AnonymousClass3EX(r2, r1, userJid, r3);
                }
                throw C16700pc.A06("catalogAnalyticManager");
            }
            throw C16700pc.A06("cartItemStore");
        }
        throw C16700pc.A06("waWorkers");
    }
}
