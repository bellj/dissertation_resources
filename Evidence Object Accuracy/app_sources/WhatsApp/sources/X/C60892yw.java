package X;

/* renamed from: X.2yw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60892yw extends AnonymousClass4UX {
    public C58552qq A00;
    public final C16590pI A01;
    public final AbstractC41521tf A02 = new C70293b3(this);
    public final AnonymousClass19O A03;

    public C60892yw(C16590pI r2, AnonymousClass19O r3) {
        this.A01 = r2;
        this.A03 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00c7, code lost:
        if (r2 != null) goto L_0x0034;
     */
    @Override // X.AnonymousClass4UX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(android.widget.FrameLayout r10, X.AnonymousClass1OY r11, X.AbstractC15340mz r12, X.C16470p4 r13) {
        /*
            r9 = this;
            r10.removeAllViews()
            android.content.Context r1 = r10.getContext()
            X.2qq r0 = new X.2qq
            r0.<init>(r1)
            r9.A00 = r0
            r10.addView(r0)
            X.1Z6 r2 = r13.A02
            r7 = 0
            r1 = 8
            r3 = r11
            r5 = r12
            if (r2 == 0) goto L_0x00c0
            java.lang.String r6 = r2.A01
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x00c0
            X.2qq r0 = r9.A00
            com.whatsapp.TextEmojiLabel r0 = r0.A01
            r0.setVisibility(r7)
            X.AnonymousClass009.A05(r6)
            X.2qq r0 = r9.A00
            com.whatsapp.TextEmojiLabel r4 = r0.A01
            r8 = 0
            r3.A17(r4, r5, r6, r7, r8)
        L_0x0034:
            java.lang.String r6 = r2.A00
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x00cb
            X.2qq r0 = r9.A00
            com.whatsapp.TextEmojiLabel r0 = r0.A00
            r0.setVisibility(r7)
            X.AnonymousClass009.A05(r6)
            X.2qq r0 = r9.A00
            com.whatsapp.TextEmojiLabel r4 = r0.A00
            r8 = 0
            r3.A17(r4, r5, r6, r7, r8)
        L_0x004e:
            X.1ZE r0 = r13.A05
            if (r0 == 0) goto L_0x00b0
            int r0 = r0.A00
            if (r0 == 0) goto L_0x00b0
            X.2qq r0 = r9.A00
            com.whatsapp.WaTextView r0 = r0.A03
            r0.setVisibility(r7)
            X.2qq r0 = r9.A00
            android.content.Context r3 = r0.getContext()
            r2 = 2131232453(0x7f0806c5, float:1.8081016E38)
            r0 = 2131100809(0x7f060489, float:1.781401E38)
            android.graphics.drawable.Drawable r3 = X.AnonymousClass2GE.A01(r3, r2, r0)
            X.0pI r0 = r9.A01
            android.content.res.Resources r2 = X.C16590pI.A00(r0)
            r0 = 2131891846(0x7f121686, float:1.9418424E38)
            java.lang.String r2 = r2.getString(r0)
            X.2qq r0 = r9.A00
            com.whatsapp.WaTextView r0 = r0.A03
            android.text.TextPaint r0 = r0.getPaint()
            java.lang.CharSequence r2 = X.C52252aV.A01(r0, r3, r2)
            X.2qq r0 = r9.A00
            com.whatsapp.WaTextView r0 = r0.A03
            r0.setText(r2)
        L_0x008d:
            X.0p3 r0 = r12.A0G()
            if (r0 == 0) goto L_0x00a8
            boolean r0 = r0.A05()
            if (r0 == 0) goto L_0x00a8
            X.19O r2 = r9.A03
            X.2qq r0 = r9.A00
            com.whatsapp.WaImageView r1 = r0.A02
            X.1tf r0 = r9.A02
            r2.A07(r1, r12, r0)
        L_0x00a4:
            r10.invalidate()
            return
        L_0x00a8:
            X.2qq r0 = r9.A00
            com.whatsapp.WaImageView r0 = r0.A02
            r0.setVisibility(r1)
            goto L_0x00a4
        L_0x00b0:
            X.2qq r0 = r9.A00
            com.whatsapp.WaTextView r2 = r0.A03
            r0 = 0
            r2.setText(r0)
            X.2qq r0 = r9.A00
            com.whatsapp.WaTextView r0 = r0.A03
            r0.setVisibility(r1)
            goto L_0x008d
        L_0x00c0:
            X.2qq r0 = r9.A00
            com.whatsapp.TextEmojiLabel r0 = r0.A01
            r0.setVisibility(r1)
            if (r2 == 0) goto L_0x00cb
            goto L_0x0034
        L_0x00cb:
            X.2qq r0 = r9.A00
            com.whatsapp.TextEmojiLabel r0 = r0.A00
            r0.setVisibility(r1)
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C60892yw.A00(android.widget.FrameLayout, X.1OY, X.0mz, X.0p4):void");
    }
}
