package X;

import java.nio.charset.Charset;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: X.3rv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80233rv extends AnonymousClass5I0<String> implements AnonymousClass5Z4, RandomAccess {
    public static final C80233rv A01;
    public static final AnonymousClass5Z4 A02;
    public final List A00;

    static {
        C80233rv r1 = new C80233rv(C12980iv.A0w(10));
        A01 = r1;
        ((AnonymousClass5I0) r1).A00 = false;
        A02 = r1;
    }

    public C80233rv(ArrayList arrayList) {
        this.A00 = arrayList;
    }

    public static String A00(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (!(obj instanceof AbstractC111925Bi)) {
            return new String((byte[]) obj, C93104Zc.A02);
        }
        AbstractC111925Bi r4 = (AbstractC111925Bi) obj;
        Charset charset = C93104Zc.A02;
        if (r4.A02() == 0) {
            return "";
        }
        C80273rz r42 = (C80273rz) r4;
        return new String(r42.zzb, r42.A03(), r42.A02(), charset);
    }

    @Override // X.AnonymousClass5Z6
    public final /* synthetic */ AnonymousClass5Z6 Agl(int i) {
        if (i >= size()) {
            ArrayList A0w = C12980iv.A0w(i);
            A0w.addAll(this.A00);
            return new C80233rv(A0w);
        }
        throw C72453ed.A0h();
    }

    @Override // X.AnonymousClass5Z4
    public final void Agp(AbstractC111925Bi r2) {
        A02();
        this.A00.add(r2);
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5Z4
    public final Object Ah7(int i) {
        return this.A00.get(i);
    }

    @Override // X.AnonymousClass5Z4
    public final List AhI() {
        return Collections.unmodifiableList(this.A00);
    }

    @Override // X.AnonymousClass5Z4
    public final AnonymousClass5Z4 AhK() {
        return super.A00 ? new AnonymousClass5I1(this) : this;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A02();
        this.A00.add(i, obj);
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List
    public final boolean addAll(int i, Collection collection) {
        A02();
        if (collection instanceof AnonymousClass5Z4) {
            collection = ((AnonymousClass5Z4) collection).AhI();
        }
        boolean addAll = this.A00.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        return addAll(size(), collection);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final void clear() {
        A02();
        this.A00.clear();
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        String str;
        int A012;
        List list = this.A00;
        Object obj = list.get(i);
        if (obj instanceof String) {
            return obj;
        }
        if (obj instanceof AbstractC111925Bi) {
            AbstractC111925Bi r7 = (AbstractC111925Bi) obj;
            Charset charset = C93104Zc.A02;
            if (r7.A02() == 0) {
                str = "";
            } else {
                C80273rz r0 = (C80273rz) r7;
                str = new String(r0.zzb, r0.A03(), r0.A02(), charset);
            }
            C80273rz r72 = (C80273rz) r7;
            int A03 = r72.A03();
            A012 = C94634cG.A00.A01(r72.zzb, A03, r72.A02() + A03);
        } else {
            byte[] bArr = (byte[]) obj;
            str = new String(bArr, C93104Zc.A02);
            A012 = C94634cG.A00.A01(bArr, 0, bArr.length);
        }
        if (A012 == 0) {
            list.set(i, str);
        }
        return str;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        A02();
        Object remove = this.A00.remove(i);
        ((AbstractList) this).modCount++;
        return A00(remove);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        A02();
        return A00(this.A00.set(i, obj));
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00.size();
    }
}
