package X;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.DialogFragment;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.facebook.redex.IDxObserverShape5S0200000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.fragment.NoviWithdrawCashReviewSheet;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

/* renamed from: X.5bG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118095bG extends AnonymousClass015 {
    public C14580lf A00;
    public AbstractC30791Yv A01;
    public AnonymousClass6F2 A02;
    public C1316263m A03;
    public C1309460p A04;
    public AbstractC1316063k A05;
    public C27691It A06 = C13000ix.A03();
    public C27691It A07 = C13000ix.A03();
    public C27691It A08 = C13000ix.A03();
    public C27691It A09 = C13000ix.A03();
    public C27691It A0A = C13000ix.A03();
    public C27691It A0B = C13000ix.A03();
    public C27691It A0C = C13000ix.A03();
    public final AnonymousClass016 A0D = C12980iv.A0T();
    public final AnonymousClass016 A0E = C12980iv.A0T();
    public final C16590pI A0F;
    public final AnonymousClass018 A0G;
    public final C17070qD A0H;
    public final C129865yQ A0I;
    public final AnonymousClass60Y A0J;
    public final AnonymousClass61F A0K;
    public final C130105yo A0L;
    public final C129685y8 A0M;
    public final C129675y7 A0N;
    public final C27691It A0O = C13000ix.A03();
    public final C27691It A0P = C13000ix.A03();
    public final C27691It A0Q = C13000ix.A03();
    public final C27691It A0R = C13000ix.A03();
    public final String A0S;
    public final String A0T;
    public final String A0U;
    public final String A0V;

    public AbstractC118095bG(C16590pI r2, AnonymousClass018 r3, C17070qD r4, C129865yQ r5, AnonymousClass60Y r6, AnonymousClass61F r7, C130105yo r8, C129685y8 r9, C129675y7 r10, String str, String str2, String str3, String str4) {
        this.A0F = r2;
        this.A0N = r10;
        this.A0G = r3;
        this.A0J = r6;
        this.A0H = r4;
        this.A0K = r7;
        this.A0M = r9;
        this.A0L = r8;
        this.A0I = r5;
        this.A0S = str4;
        this.A0U = str;
        this.A0T = str2;
        this.A0V = str3;
        this.A00 = C117305Zk.A0C(r4);
    }

    public static void A00(IDxObserverShape5S0200000_3_I1 iDxObserverShape5S0200000_3_I1, Object obj) {
        AbstractC118095bG r3 = (AbstractC118095bG) iDxObserverShape5S0200000_3_I1.A00;
        AbstractC001200n r2 = (AbstractC001200n) iDxObserverShape5S0200000_3_I1.A01;
        C130785zy r5 = (C130785zy) obj;
        r3.A09.A0B(Boolean.FALSE);
        if (r5.A06()) {
            Object obj2 = r5.A02;
            AnonymousClass009.A05(obj2);
            r3.A0B((C1309460p) obj2);
            r3.A09(r2);
            return;
        }
        C452120p r0 = r5.A00;
        AnonymousClass009.A05(r0);
        r3.A0A(r0);
    }

    public Bundle A04(boolean z) {
        CharSequence charSequence;
        AnonymousClass6F2 r3;
        AnonymousClass6F2 r0;
        Bundle A0D = C12970iu.A0D();
        C1309460p r02 = this.A04;
        AnonymousClass009.A05(r02);
        AnonymousClass6F2 r4 = r02.A00;
        AnonymousClass6F2 r11 = r02.A01;
        String str = this.A0T;
        AbstractC30791Yv r32 = r4.A00;
        Context context = this.A0F.A00;
        AnonymousClass018 r7 = this.A0G;
        CharSequence A0e = C117305Zk.A0e(context, r7, r32, r4.A01, 1);
        if (r11 != null) {
            charSequence = r11.A00.AA7(context, C12960it.A0X(context, r11.A06(r7), new Object[1], 0, R.string.novi_transaction_details_conversion_summary));
        } else {
            charSequence = null;
        }
        boolean z2 = this instanceof C123465nC;
        C1309460p r03 = this.A04;
        AnonymousClass009.A05(r03);
        AnonymousClass603 r04 = r03.A02;
        if (!z2) {
            r3 = r04.A02;
            r0 = r04.A03;
        } else {
            r3 = r04.A03;
            r0 = r04.A02;
        }
        C1315363d r5 = new C1315363d(A0e, charSequence, C1309460p.A01(context, r7, r3, r0), str);
        C1315663g A06 = A06();
        String str2 = this.A0V;
        AnonymousClass6F2 r05 = this.A04.A02.A02;
        CharSequence AA7 = r05.A00.AA7(context, r05.A06(r7));
        String string = context.getString(R.string.novi_withdraw_review_fee_title);
        AnonymousClass6F2 r06 = this.A04.A02.A01;
        CharSequence AA72 = r06.A00.AA7(context, r06.A06(r7));
        String str3 = this.A0U;
        AnonymousClass603 r07 = this.A04.A02;
        AnonymousClass6F2 r33 = r07.A03;
        BigDecimal subtract = r33.A01.A00.subtract(r07.A01.A01.A00);
        AbstractC30791Yv r34 = r33.A00;
        C1315563f r8 = new C1315563f(AA7, AA72, r34.AA7(context, r34.AAB(r7, subtract, 1)), str2, string, str3);
        A0D.putParcelable("withdraw-amount-data", r5);
        AnonymousClass009.A05(A06);
        A0D.putParcelable("withdraw-method-data", A06);
        A0D.putParcelable("withdraw-transaction-data", r8);
        if (z) {
            A0D.putInt("initial-button-state", 1);
        }
        return A0D;
    }

    public AnonymousClass017 A05() {
        C123475nD r3 = (C123475nD) this;
        String A0n = C12990iw.A0n();
        C30881Ze r0 = r3.A00;
        if (r0 == null) {
            return null;
        }
        C129705yA r1 = r3.A02;
        String str = r0.A0A;
        C1309460p r02 = r3.A04;
        AnonymousClass009.A05(r02);
        AnonymousClass603 r5 = r02.A02;
        AnonymousClass016 A0T = C12980iv.A0T();
        String str2 = AnonymousClass600.A03;
        C130155yt r32 = r1.A05;
        String A05 = r32.A05();
        long A00 = r1.A02.A00();
        String A0n2 = C12990iw.A0n();
        String str3 = r5.A04;
        JSONObject A002 = AnonymousClass61C.A00(r5.A03, r5.A02, A0n2, str, str3, str2, A05, A00);
        C130125yq r9 = r1.A06;
        C129585xx r7 = new C129585xx(r9, "DEPOSIT_DEBIT_CARD", A002);
        AnonymousClass61S[] r6 = new AnonymousClass61S[5];
        AnonymousClass61S.A04("action", "novi-submit-card-deposit", r6);
        AnonymousClass61S.A05("credential_id", str, r6, 1);
        C130155yt.A03(C117305Zk.A09(A0T, r1, 6), r32, C117295Zj.A0F(AnonymousClass61S.A00("card_deposit_signed_intent", C123465nC.A01(r9, r7, str3, A0n, r6)), r6, 4), 21);
        return A0T;
    }

    public C1315663g A06() {
        if (!(this instanceof C123465nC)) {
            C123475nD r3 = (C123475nD) this;
            C30881Ze r0 = r3.A00;
            AnonymousClass009.A05(r0);
            int A00 = C1311161i.A00(r0.A01);
            Context context = r3.A0F.A00;
            return new C1315663g(null, context.getResources().getString(R.string.novi_deposit_quote_payment_method_header), C1311161i.A05(context, r3.A00), A00, false, true);
        }
        C123465nC r5 = (C123465nC) this;
        int i = r5.A00;
        if (i == 1) {
            C1316163l r02 = r5.A02;
            AnonymousClass009.A05(r02);
            return new C1315663g(r02.A03, r02.A01, r02.A02, -1, false, true);
        } else if (i != 2) {
            return null;
        } else {
            Context context2 = r5.A0F.A00;
            String string = context2.getResources().getString(R.string.novi_withdraw_quote_payment_method_header);
            C30861Zc r03 = r5.A01;
            AnonymousClass009.A05(r03);
            return new C1315663g(null, string, C12990iw.A0o(context2.getResources(), C1311161i.A07(r03), new Object[1], 0, R.string.withdrawal_formatted_bank_account_name), R.drawable.ic_bank, false, false);
        }
    }

    public void A07(AbstractC001200n r7) {
        this.A00.A00(new AbstractC14590lg() { // from class: X.6EL
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                AnonymousClass1ZY r0;
                String A0X;
                AbstractC118095bG r72 = AbstractC118095bG.this;
                AbstractC28901Pl A00 = AnonymousClass61F.A00((List) obj);
                if (A00 != null && (r0 = A00.A08) != null) {
                    C1315463e r3 = ((C119805f8) r0).A02;
                    AnonymousClass63Y r02 = r3.A00;
                    AnonymousClass009.A05(r02);
                    AbstractC30791Yv r2 = r02.A00;
                    r72.A01 = r2;
                    if (((AbstractC30781Yu) r2).A00 != 1) {
                        AnonymousClass6F2 A0R = C117305Zk.A0R(r2, new BigDecimal(0), 1);
                        r72.A02 = A0R;
                        r72.A0D.A0A(A0R);
                    }
                    C1316263m r1 = r3.A01;
                    if (r1 == null) {
                        A0X = "";
                    } else {
                        AnonymousClass009.A05(r1);
                        A0X = C12960it.A0X(r72.A0F.A00, r1.A02.A06(r72.A0G), new Object[1], 0, R.string.novi_balance_with_value);
                    }
                    r72.A0O.A0A(new AnonymousClass4OZ(1, new C128105vZ(null, A0X, r72.A0F.A00.getString(R.string.novi_title), C130105yo.A00(r72.A0L), 0, 8, R.color.novi_service_subtitle_balance_color)));
                }
            }
        });
        C117305Zk.A1A(this.A0P, new C127015to(""), 2);
        C129685y8 r3 = this.A0M;
        C27691It A03 = C13000ix.A03();
        r3.A05.Ab2(new RunnableC135296Hx(r3, A03));
        C117295Zj.A0s(r7, A03, this, 137);
        C117305Zk.A1A(this.A0Q, new C127445uV(new IDxCListenerShape2S0200000_3_I1(this, 45, r7), this.A0F.A00.getString(R.string.novi_withdraw_review_btn_text), false), 2);
        AnonymousClass016 r4 = this.A0D;
        C117295Zj.A0s(r7, r4, this, 135);
        AnonymousClass02P r32 = new AnonymousClass02P();
        r32.A0D(r4, new AnonymousClass02B(C12970iu.A0E(), new Runnable(r4, r32) { // from class: X.6Ix
            public final /* synthetic */ AnonymousClass017 A00;
            public final /* synthetic */ AnonymousClass02P A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.A01.A0B(this.A00.A01());
            }
        }) { // from class: X.65i
            public final /* synthetic */ int A00 = 500;
            public final /* synthetic */ Handler A01;
            public final /* synthetic */ Runnable A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                Handler handler = this.A01;
                Runnable runnable = this.A02;
                int i = this.A00;
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, (long) i);
            }
        });
        C117305Zk.A19(r7, r32, this, 6);
        C117305Zk.A19(r7, this.A0N.A04, this, 7);
        C117295Zj.A0s(r7, this.A0K.A0G, this, 138);
    }

    public void A08(AbstractC001200n r6) {
        String str;
        C128365vz r3;
        String str2;
        AnonymousClass60Y r0;
        C1316163l r02;
        if (!(this instanceof C123465nC)) {
            AnonymousClass610 A03 = AnonymousClass610.A03("REVIEW_DEPOSIT_AMT_CLICK", "ADD_MONEY", "ENTER_AMOUNT");
            String string = this.A0F.A00.getString(R.string.novi_withdraw_review_btn_text);
            r3 = A03.A00;
            r3.A0L = string;
            A03.A07(this.A03, this.A04);
            r0 = this.A0J;
        } else {
            C123465nC r2 = (C123465nC) this;
            if (r2.A00 == 2) {
                str = "REVIEW_WD_AMT_CLICK";
            } else {
                str = "REVIEW_WD_CASH_AMT_CLICK";
            }
            AnonymousClass610 A032 = AnonymousClass610.A03(str, "WITHDRAW_MONEY", "ENTER_AMOUNT");
            String string2 = r2.A0F.A00.getString(R.string.novi_withdraw_review_btn_text);
            r3 = A032.A00;
            r3.A0L = string2;
            if (r2.A00 == 1) {
                str2 = "CASH";
            } else {
                str2 = "BANK";
            }
            r3.A0T = str2;
            A032.A08(((AbstractC118095bG) r2).A03, r2.A04);
            if (r2.A00 != 1 || (r02 = r2.A02) == null) {
                C30861Zc r03 = r2.A01;
                if (r03 != null) {
                    r3.A0S = r03.A0A;
                }
            } else {
                r3.A0h = r02.A04;
            }
            r0 = r2.A0J;
        }
        r0.A05(r3);
        A0D(new AnonymousClass6CX(r6, this), false);
    }

    public void A09(AbstractC001200n r8) {
        C1309460p r0 = this.A04;
        if (r0 != null && r0.A02.A01()) {
            AnonymousClass6F2 r2 = this.A04.A02.A02;
            if (r2.A01.A00.compareTo(r2.A00.AEV().A00) >= 0) {
                A08(r8);
            } else if (!(this.A01 instanceof C30801Yw)) {
                Context context = this.A0F.A00;
                C27691It r5 = this.A07;
                AnonymousClass603 r02 = this.A04.A02;
                AnonymousClass018 r1 = this.A0G;
                AnonymousClass6F2 r03 = r02.A02;
                r5.A0B(r03.A00.AA7(context, C12960it.A0X(context, r03.A06(r1), C12970iu.A1b(), 0, R.string.payments_send_payment_min_amount)));
                C117315Zl.A0Q(this.A06);
            }
        }
    }

    public void A0A(C452120p r3) {
        C129525xr r0 = (C129525xr) this.A0B.A01();
        if (r0 != null) {
            r0.A00();
        }
        this.A0I.A02(r3, null, null);
    }

    public void A0B(C1309460p r12) {
        String str;
        String str2;
        C1316163l r0;
        AnonymousClass6F2 r02;
        this.A04 = r12;
        boolean z = this instanceof C123465nC;
        if (!z) {
            AnonymousClass610 r4 = new AnonymousClass610("REVIEW_DEPOSIT_QUOTE", "ADD_MONEY");
            C128365vz r2 = r4.A00;
            r2.A0j = "ENTER_AMOUNT";
            r4.A07(this.A03, this.A04);
            AnonymousClass6F2 r03 = this.A02;
            if (r03 != null) {
                r2.A09 = Long.valueOf(r03.A01.A00.longValue());
            }
            this.A0J.A05(r2);
        } else {
            C123465nC r42 = (C123465nC) this;
            AnonymousClass6F2 r04 = r12.A01;
            if (r04 != null && r04.A01.A02()) {
                int i = r42.A00;
                if (i == 1) {
                    str = "REVIEW_WD_CASH_QUOTE";
                } else {
                    str = "REVIEW_WD_QUOTE";
                }
                AnonymousClass610 A03 = AnonymousClass610.A03(str, "WITHDRAW_MONEY", "REVIEW_TRANSACTION");
                if (i == 1) {
                    str2 = "CASH";
                } else {
                    str2 = "BANK";
                }
                C128365vz r1 = A03.A00;
                r1.A0T = str2;
                A03.A08(((AbstractC118095bG) r42).A03, r12);
                if (r42.A00 != 1 || (r0 = r42.A02) == null) {
                    C30861Zc r05 = r42.A01;
                    if (r05 != null) {
                        r1.A0S = r05.A0A;
                    }
                } else {
                    r1.A0h = r0.A04;
                }
                r42.A0J.A05(r1);
            }
        }
        if (!(this.A01 instanceof C30801Yw)) {
            Context context = this.A0F.A00;
            C27691It r8 = this.A0P;
            Object[] objArr = new Object[1];
            AnonymousClass603 r06 = r12.A02;
            if (!z) {
                r02 = r06.A03;
            } else {
                r02 = r06.A02;
            }
            C117305Zk.A1A(r8, new C127015to(C12960it.A0X(context, r02.A00.AAA(this.A0G, r02.A01, 1), objArr, 0, R.string.novi_transaction_details_conversion_summary)), 2);
        }
    }

    public void A0C(C130785zy r5) {
        C121035h9 r3;
        if (!(this instanceof C123465nC)) {
            C130605zg r1 = (C130605zg) r5.A02;
            if (r5.A06() && r1 != null && (r3 = r1.A02) != null) {
                this.A05 = null;
                HashMap A11 = C12970iu.A11();
                A11.put("deposit_transaction_id", r3.A05);
                A11.put("deposit_status", C31001Zq.A05(6, r3.A00.A00));
                this.A08.A0A(A11);
            } else if (r5.A01 == null || r1 == null) {
                A0A(r5.A00);
            } else {
                this.A05 = r1.A02;
            }
        } else {
            C123465nC r12 = (C123465nC) this;
            r12.A0E(r5, r12.A00);
        }
    }

    public void A0D(AbstractC136466Mq r4, boolean z) {
        this.A0B.A0B(new C129525xr(new AnonymousClass6MC(r4, this, z) { // from class: X.6DG
            public final /* synthetic */ AbstractC136466Mq A00;
            public final /* synthetic */ AbstractC118095bG A01;
            public final /* synthetic */ boolean A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass6MC
            public final DialogFragment ANO(Activity activity) {
                AbstractC118095bG r5 = this.A01;
                AbstractC136466Mq r42 = this.A00;
                boolean z2 = this.A02;
                PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
                NoviWithdrawCashReviewSheet noviWithdrawCashReviewSheet = new NoviWithdrawCashReviewSheet(r5.A0S);
                noviWithdrawCashReviewSheet.A06 = new AnonymousClass6CY(paymentBottomSheet, r42, noviWithdrawCashReviewSheet, r5);
                noviWithdrawCashReviewSheet.A0U(r5.A04(z2));
                paymentBottomSheet.A01 = noviWithdrawCashReviewSheet;
                return paymentBottomSheet;
            }
        }));
    }
}
