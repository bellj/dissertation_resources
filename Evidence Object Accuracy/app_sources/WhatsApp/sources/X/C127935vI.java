package X;

import android.content.Context;

/* renamed from: X.5vI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127935vI {
    public final Context A00;
    public final AnonymousClass102 A01;
    public final C120985h4 A02;
    public final C130205yy A03;
    public final C123745nn A04;

    public C127935vI(Context context, AnonymousClass102 r3, C130205yy r4, C123745nn r5) {
        this.A00 = context;
        this.A04 = r5;
        C120985h4 r0 = r5.A01;
        AnonymousClass009.A05(r0);
        this.A02 = r0;
        this.A03 = r4;
        this.A01 = r3;
    }
}
