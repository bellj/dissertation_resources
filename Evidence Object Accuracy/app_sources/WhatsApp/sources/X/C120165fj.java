package X;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;
import com.whatsapp.util.Log;

/* renamed from: X.5fj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120165fj extends AbstractC451020e {
    public final /* synthetic */ C128235vm A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120165fj(Context context, C14900mE r2, C18650sn r3, C128235vm r4) {
        super(context, r2, r3);
        this.A00 = r4;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r4) {
        C128235vm r0 = this.A00;
        r0.A07.A00(C117305Zk.A0L(), r0.A08);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r4) {
        C128235vm r0 = this.A00;
        r0.A07.A00(C117305Zk.A0L(), r0.A08);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r9) {
        AnonymousClass1V8 A0E;
        AnonymousClass1V8 A0c = C117305Zk.A0c(r9);
        if (A0c == null || (A0E = A0c.A0E("image")) == null) {
            C128235vm r0 = this.A00;
            r0.A07.A00(C117305Zk.A0L(), r0.A08);
            return;
        }
        C129015x2 r5 = this.A00.A07;
        String A0I = A0E.A0I("credential-id", null);
        A0E.A0I("image-content-id", null);
        String A0I2 = A0E.A0I("image-url", null);
        String A0I3 = A0E.A0I("image-label-color", null);
        AbstractC28901Pl r3 = r5.A01;
        String str = r3.A0A;
        if (A0I.equals(str)) {
            if (!TextUtils.isEmpty(A0I2)) {
                r5.A02.A0E = A0I2;
                ImageView imageView = r5.A00;
                C121875kP r02 = r5.A03.A08;
                if (imageView != null) {
                    r02.A04.A01(imageView, A0I2);
                } else {
                    C38721ob r1 = r02.A04;
                    int i = r1.A01;
                    r1.A02.A01(new AnonymousClass47R(i, A0I2, i), false);
                }
            }
            if (!TextUtils.isEmpty(A0I3)) {
                r5.A02.A0D = A0I3;
            }
            r5.A03.A07.A00().A03(null, r3);
            return;
        }
        StringBuilder A0k = C12960it.A0k("PAY: fetchCardArtImageContentDetails credentialIds don't match; request: ");
        A0k.append(str);
        A0k.append(" response: ");
        Log.w(C12960it.A0d(A0I, A0k));
    }
}
