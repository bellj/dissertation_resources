package X;

/* renamed from: X.28E  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass28E extends AnonymousClass1MS {
    public final C14900mE A00;
    public final C15550nR A01;
    public final C51482Va A02;
    public final AnonymousClass131 A03;
    public final boolean A04;
    public volatile boolean A05;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass28E(X.C14900mE r3, X.C15550nR r4, X.C51482Va r5, X.AnonymousClass131 r6, java.lang.String r7, boolean r8) {
        /*
            r2 = this;
            java.lang.String r1 = "contact-photos-"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.A02 = r5
            r2.A00 = r3
            r2.A01 = r4
            r2.A03 = r6
            r2.A04 = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass28E.<init>(X.0mE, X.0nR, X.2Va, X.131, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00af, code lost:
        r17.A00.A0H(new com.facebook.redex.RunnableBRunnable0Shape0S0500000_I0(r11, r12, r13, r17, r15, 0));
     */
    @Override // java.lang.Thread, java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 299
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass28E.run():void");
    }
}
