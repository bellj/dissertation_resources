package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.5JV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5JV extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ View $itemView;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5JV(View view) {
        super(0);
        this.$itemView = view;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return AnonymousClass028.A0D(this.$itemView, R.id.shimmer_category_list_item);
    }
}
