package X;

import java.util.List;

/* renamed from: X.27L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27L {
    public C41121sw A00;
    public AnonymousClass2CH A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final AnonymousClass16S A04;
    public final AnonymousClass16T A05;
    public final C233411h A06;
    public final C14830m7 A07;
    public final C16590pI A08;
    public final C14820m6 A09;
    public final C15990oG A0A;
    public final C18240s8 A0B;
    public final C22100yW A0C;
    public final C14850m9 A0D;
    public final C17220qS A0E;
    public final C22420z3 A0F;
    public final C22230yk A0G;
    public final AnonymousClass27W A0H;
    public final AnonymousClass1F2 A0I;
    public final AbstractC14440lR A0J;
    public final C14890mD A0K;
    public final C14860mA A0L;

    public AnonymousClass27L(AbstractC15710nm r2, C14900mE r3, AnonymousClass16S r4, AnonymousClass16T r5, C233411h r6, C14830m7 r7, C16590pI r8, C14820m6 r9, C15990oG r10, C18240s8 r11, C22100yW r12, C14850m9 r13, C17220qS r14, C22420z3 r15, C22230yk r16, AnonymousClass27W r17, AnonymousClass1F2 r18, AbstractC14440lR r19, C14890mD r20, C14860mA r21) {
        this.A08 = r8;
        this.A07 = r7;
        this.A0D = r13;
        this.A03 = r3;
        this.A02 = r2;
        this.A0J = r19;
        this.A0K = r20;
        this.A0L = r21;
        this.A0E = r14;
        this.A0B = r11;
        this.A06 = r6;
        this.A0A = r10;
        this.A09 = r9;
        this.A0G = r16;
        this.A0F = r15;
        this.A0C = r12;
        this.A04 = r4;
        this.A05 = r5;
        this.A0I = r18;
        this.A0H = r17;
    }

    public AbstractC41131sx A00() {
        C22100yW r10 = this.A0C;
        if (r10.A0L.A03()) {
            C41121sw r0 = this.A00;
            if (r0 != null) {
                return r0;
            }
            C14830m7 r6 = this.A07;
            C14900mE r2 = this.A03;
            AbstractC15710nm r1 = this.A02;
            AbstractC14440lR r14 = this.A0J;
            C17220qS r11 = this.A0E;
            C18240s8 r9 = this.A0B;
            C233411h r5 = this.A06;
            AnonymousClass27W r12 = this.A0H;
            C15990oG r8 = this.A0A;
            C41121sw r02 = new C41121sw(r1, r2, this.A04, this.A05, r5, r6, this.A09, r8, r9, r10, r11, r12, this.A0I, r14);
            this.A00 = r02;
            return r02;
        }
        if (this.A01 == null) {
            C14900mE r22 = this.A03;
            C16590pI r3 = this.A08;
            C14890mD r7 = this.A0K;
            C14860mA r82 = this.A0L;
            AnonymousClass2CH r13 = new AnonymousClass2CH(r22, r3, this.A0F, this.A0G, this.A0H, r7, r82);
            this.A01 = r13;
            C14860mA r03 = r13.A08;
            AnonymousClass1UU r23 = r13.A07;
            List list = r03.A0R;
            if (!list.contains(r23)) {
                list.add(r23);
            }
        }
        return this.A01;
    }
}
