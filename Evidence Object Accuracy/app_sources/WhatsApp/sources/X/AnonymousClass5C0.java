package X;

import java.security.cert.PolicyNode;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.5C0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5C0 implements PolicyNode {
    public int A00;
    public String A01;
    public PolicyNode A02;
    public List A03;
    public Set A04;
    public Set A05;
    public boolean A06;

    public AnonymousClass5C0(String str, PolicyNode policyNode, List list, Set set, Set set2, int i, boolean z) {
        this.A03 = list;
        this.A00 = i;
        this.A04 = set;
        this.A02 = policyNode;
        this.A05 = set2;
        this.A01 = str;
        this.A06 = z;
    }

    @Override // java.lang.Object
    public Object clone() {
        return A01();
    }

    @Override // java.security.cert.PolicyNode
    public Iterator getChildren() {
        return this.A03.iterator();
    }

    @Override // java.security.cert.PolicyNode
    public int getDepth() {
        return this.A00;
    }

    @Override // java.security.cert.PolicyNode
    public Set getExpectedPolicies() {
        return this.A04;
    }

    @Override // java.security.cert.PolicyNode
    public PolicyNode getParent() {
        return this.A02;
    }

    @Override // java.security.cert.PolicyNode
    public Set getPolicyQualifiers() {
        return this.A05;
    }

    @Override // java.security.cert.PolicyNode
    public String getValidPolicy() {
        return this.A01;
    }

    @Override // java.security.cert.PolicyNode
    public boolean isCritical() {
        return this.A06;
    }

    @Override // java.lang.Object
    public String toString() {
        return A00("");
    }

    public String A00(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append(this.A01);
        stringBuffer.append(" {\n");
        int i = 0;
        while (true) {
            List list = this.A03;
            if (i < list.size()) {
                stringBuffer.append(((AnonymousClass5C0) list.get(i)).A00(C12960it.A0d("    ", C12960it.A0j(str))));
                i++;
            } else {
                stringBuffer.append(str);
                stringBuffer.append("}\n");
                return stringBuffer.toString();
            }
        }
    }

    public AnonymousClass5C0 A01() {
        HashSet A12 = C12970iu.A12();
        Iterator it = this.A04.iterator();
        while (it.hasNext()) {
            A12.add(new String(C12970iu.A0x(it)));
        }
        HashSet A122 = C12970iu.A12();
        Iterator it2 = this.A05.iterator();
        while (it2.hasNext()) {
            A122.add(new String(C12970iu.A0x(it2)));
        }
        AnonymousClass5C0 r3 = new AnonymousClass5C0(new String(this.A01), null, C12960it.A0l(), A12, A122, this.A00, this.A06);
        for (AnonymousClass5C0 r0 : this.A03) {
            AnonymousClass5C0 A01 = r0.A01();
            A01.A02 = r3;
            r3.A03.add(A01);
            A01.A02 = r3;
        }
        return r3;
    }
}
