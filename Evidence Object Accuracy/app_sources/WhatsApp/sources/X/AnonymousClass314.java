package X;

/* renamed from: X.314  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass314 extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public String A07;

    public AnonymousClass314() {
        super(1656, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(8, this.A07);
        r3.Abe(5, this.A00);
        r3.Abe(4, this.A02);
        r3.Abe(3, this.A01);
        r3.Abe(7, this.A03);
        r3.Abe(6, this.A04);
        r3.Abe(1, this.A05);
        r3.Abe(2, this.A06);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStatusRowView {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "psaCampaigns", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowEntryMethod", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowIndex", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowSection", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowUnreadItemCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusRowViewCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusSessionId", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "statusViewerSessionId", this.A06);
        return C12960it.A0d("}", A0k);
    }
}
