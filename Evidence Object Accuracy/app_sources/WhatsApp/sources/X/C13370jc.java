package X;

import com.google.firebase.iid.FirebaseInstanceId;

/* renamed from: X.0jc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final /* synthetic */ class C13370jc implements AbstractC13110jA {
    public static final AbstractC13110jA A00 = new C13370jc();

    @Override // X.AbstractC13110jA
    public final Object A80(AbstractC13170jG r3) {
        return new C13360jb((FirebaseInstanceId) r3.A02(FirebaseInstanceId.class));
    }
}
