package X;

import android.content.Context;
import android.widget.ImageView;
import java.io.File;

/* renamed from: X.37N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37N extends AbstractC16350or {
    public final Context A00;
    public final ImageView A01;
    public final File A02;

    public AnonymousClass37N(Context context, ImageView imageView, File file) {
        this.A00 = context;
        this.A01 = imageView;
        this.A02 = file;
    }
}
