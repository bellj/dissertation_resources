package X;

import android.graphics.Bitmap;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.commerce.ui.viewmodel.WaGalaxyNavBarViewModel;

/* renamed from: X.546  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass546 implements AnonymousClass6MK {
    public final /* synthetic */ WaGalaxyNavBarViewModel A00;

    public AnonymousClass546(WaGalaxyNavBarViewModel waGalaxyNavBarViewModel) {
        this.A00 = waGalaxyNavBarViewModel;
    }

    @Override // X.AnonymousClass6MK
    public void APk() {
        Log.e("WaGalaxyImageViewModel/setupTopNavBar/Error while loading image");
    }

    @Override // X.AnonymousClass6MK
    public void AWv(Bitmap bitmap) {
        this.A00.A01.A0B(bitmap);
    }
}
