package X;

/* renamed from: X.0EO  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EO extends AnonymousClass015 {
    public static final AbstractC009404s A02 = new AnonymousClass0Yn();
    public C013506i A00 = new C013506i();
    public boolean A01 = false;

    @Override // X.AnonymousClass015
    public void A03() {
        C013506i r5 = this.A00;
        int i = r5.A00;
        for (int i2 = 0; i2 < i; i2++) {
            ((AnonymousClass0EN) r5.A02[i2]).A0C(true);
        }
        int i3 = r5.A00;
        Object[] objArr = r5.A02;
        for (int i4 = 0; i4 < i3; i4++) {
            objArr[i4] = null;
        }
        r5.A00 = 0;
    }
}
