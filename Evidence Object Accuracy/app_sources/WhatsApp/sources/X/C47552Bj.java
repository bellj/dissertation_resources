package X;

import com.whatsapp.group.GroupChatInfo;
import java.util.List;

/* renamed from: X.2Bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47552Bj extends C47562Bk {
    public final /* synthetic */ GroupChatInfo A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C47552Bj(C21320xE r7, GroupChatInfo groupChatInfo, C20710wC r9, C15580nU r10, C14860mA r11, List list) {
        super(r7, r9, r10, r11, list);
        this.A00 = groupChatInfo;
    }
}
