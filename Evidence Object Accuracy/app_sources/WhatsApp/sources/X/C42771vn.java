package X;

import android.content.Intent;

/* renamed from: X.1vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42771vn {
    public int A00;
    public int A01 = 1;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public boolean A05;
    public final int A06;
    public final int A07;
    public final Intent A08;

    public /* synthetic */ C42771vn(int i, int i2, Intent intent) {
        this.A08 = intent;
        this.A07 = i;
        this.A06 = i2;
    }

    public C42781vo A00() {
        C42781vo r1 = new C42781vo(this.A07, this.A06, this.A08);
        r1.A05 = this.A05;
        r1.A01 = this.A01;
        r1.A00 = this.A00;
        r1.A02 = this.A02;
        r1.A03 = this.A03;
        r1.A04 = this.A04;
        return r1;
    }
}
