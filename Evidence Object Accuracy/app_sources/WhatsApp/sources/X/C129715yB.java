package X;

import java.util.Arrays;

/* renamed from: X.5yB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C129715yB {
    public final long A00;
    public final long A01;
    public final long A02;
    public final AnonymousClass1V8 A03;
    public final AnonymousClass3EM A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;

    public C129715yB(AbstractC15710nm r31, AnonymousClass1V8 r32, C126365sl r33) {
        AnonymousClass1V8.A01(r32, "iq");
        AnonymousClass1V8 r7 = r33.A00;
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        this.A05 = (String) AnonymousClass3JT.A04(null, r32, String.class, A0j, A0k, AnonymousClass3JT.A04(null, r7, String.class, A0j, A0k, null, new String[]{"account", "action"}, false), new String[]{"account", "action"}, true);
        Class cls = Long.TYPE;
        this.A01 = C12980iv.A0G(AnonymousClass3JT.A04(null, r32, cls, A0j, A0k, null, new String[]{"account", "key-version"}, false));
        Long A0f = C117305Zk.A0f();
        this.A00 = C12980iv.A0G(AnonymousClass3JT.A04(null, r32, cls, A0f, A0k, null, new String[]{"account", "expiry-ts"}, false));
        this.A02 = C12980iv.A0G(AnonymousClass3JT.A04(null, r32, cls, A0f, A0k, null, new String[]{"account", "server-ts"}, false));
        this.A07 = (String) AnonymousClass3JT.A04(null, r32, String.class, 1L, 10000L, null, new String[]{"account", "purpose-enc-certificate"}, false);
        this.A06 = (String) AnonymousClass3JT.A04(null, r32, String.class, A0j, A0k, null, new String[]{"account", "data"}, false);
        this.A08 = (String) AnonymousClass3JT.A04(null, r32, String.class, A0j, A0k, null, new String[]{"account", "signature"}, false);
        this.A04 = (AnonymousClass3EM) AnonymousClass3JT.A05(r32, new AbstractC116095Uc(r31, r7) { // from class: X.6DU
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r4) {
                return new AnonymousClass3EM(this.A00, r4, this.A01);
            }
        }, new String[0]);
        this.A03 = r32;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C129715yB.class != obj.getClass()) {
                return false;
            }
            C129715yB r7 = (C129715yB) obj;
            if (!this.A05.equals(r7.A05) || this.A01 != r7.A01 || this.A00 != r7.A00 || this.A02 != r7.A02 || !this.A07.equals(r7.A07) || !this.A06.equals(r7.A06) || !this.A08.equals(r7.A08) || !this.A04.equals(r7.A04)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[8];
        objArr[0] = this.A05;
        C117315Zl.A0Z(objArr, this.A01);
        objArr[2] = Long.valueOf(this.A00);
        objArr[3] = Long.valueOf(this.A02);
        objArr[4] = this.A07;
        objArr[5] = this.A06;
        objArr[6] = this.A08;
        objArr[7] = this.A04;
        return Arrays.hashCode(objArr);
    }
}
