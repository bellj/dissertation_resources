package X;

import android.os.Bundle;

/* renamed from: X.5XN  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XN {
    AnonymousClass1UI AgM(AnonymousClass1UI v);

    AnonymousClass1UI AgO(AnonymousClass1UI v);

    void AgT();

    void AgW();

    void AgZ(Bundle bundle);

    void Aga(C56492ky v, AnonymousClass1UE v2, boolean z);

    void Agb(int i);

    boolean Agc();
}
