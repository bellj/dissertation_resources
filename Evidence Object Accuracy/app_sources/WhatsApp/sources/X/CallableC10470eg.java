package X;

import android.graphics.BitmapFactory;
import androidx.sharetarget.ShortcutInfoCompatSaverImpl;
import java.util.concurrent.Callable;

/* renamed from: X.0eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class CallableC10470eg implements Callable {
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A00;
    public final /* synthetic */ AnonymousClass0NL A01;

    public CallableC10470eg(ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, AnonymousClass0NL r2) {
        this.A00 = shortcutInfoCompatSaverImpl;
        this.A01 = r2;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        return BitmapFactory.decodeFile(this.A01.A01);
    }
}
