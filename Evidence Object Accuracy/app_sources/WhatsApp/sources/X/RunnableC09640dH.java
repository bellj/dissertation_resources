package X;

import java.util.List;

/* renamed from: X.0dH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09640dH implements Runnable {
    public final /* synthetic */ AbstractC06170Sl A00;
    public final /* synthetic */ List A01;

    public RunnableC09640dH(AbstractC06170Sl r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // java.lang.Runnable
    public void run() {
        for (AbstractC11430gH r2 : this.A01) {
            Object obj = this.A00.A00;
            AnonymousClass0Zt r22 = (AnonymousClass0Zt) r2;
            r22.A02 = obj;
            r22.A00(r22.A00, obj);
        }
    }
}
