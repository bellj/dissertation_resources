package X;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import com.whatsapp.gallerypicker.GalleryPickerFragment;

/* renamed from: X.3XC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XC implements AnonymousClass23E {
    public final /* synthetic */ AbstractC35611iN A00;
    public final /* synthetic */ View$OnClickListenerC55212hy A01;
    public final /* synthetic */ AnonymousClass23D A02;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void AQ8() {
    }

    public AnonymousClass3XC(AbstractC35611iN r1, View$OnClickListenerC55212hy r2, AnonymousClass23D r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23E
    public void A6L() {
        View$OnClickListenerC55212hy r0 = this.A01;
        ImageView imageView = r0.A02;
        imageView.setBackgroundColor(r0.A05.A01);
        imageView.setImageDrawable(null);
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        int i;
        View$OnClickListenerC55212hy r2 = this.A01;
        ImageView imageView = r2.A02;
        if (imageView.getTag() == this.A02) {
            GalleryPickerFragment galleryPickerFragment = r2.A05;
            if (galleryPickerFragment.A0B() == null) {
                return;
            }
            if (bitmap == MediaGalleryFragmentBase.A0U) {
                imageView.setScaleType(ImageView.ScaleType.CENTER);
                imageView.setBackgroundColor(galleryPickerFragment.A01);
                AbstractC35611iN r4 = this.A00;
                int type = r4.getType();
                if (type == 0) {
                    imageView.setBackgroundColor(galleryPickerFragment.A01);
                    i = R.drawable.ic_missing_thumbnail_picture;
                } else if (type == 1 || type == 2) {
                    imageView.setBackgroundColor(galleryPickerFragment.A01);
                    i = R.drawable.ic_missing_thumbnail_video;
                } else if (type != 3) {
                    imageView.setBackgroundColor(galleryPickerFragment.A01);
                    if (type != 4) {
                        imageView.setImageResource(0);
                        return;
                    } else {
                        imageView.setImageDrawable(C26511Dt.A03(galleryPickerFragment.A0B(), r4.AES(), null, false));
                        return;
                    }
                } else {
                    C12970iu.A18(galleryPickerFragment.A0p(), imageView, R.color.music_scrubber);
                    i = R.drawable.gallery_audio_item;
                }
                imageView.setImageResource(i);
                return;
            }
            C12990iw.A1E(imageView);
            imageView.setBackgroundResource(0);
            if (!z) {
                Drawable[] drawableArr = new Drawable[2];
                drawableArr[0] = galleryPickerFragment.A05;
                C12960it.A15(imageView, new BitmapDrawable(galleryPickerFragment.A02(), bitmap), drawableArr);
                return;
            }
            imageView.setImageBitmap(bitmap);
        }
    }
}
