package X;

import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09310ch implements Runnable {
    public final /* synthetic */ RecyclerView A00;

    public RunnableC09310ch(RecyclerView recyclerView) {
        this.A00 = recyclerView;
    }

    @Override // java.lang.Runnable
    public void run() {
        RecyclerView recyclerView = this.A00;
        AnonymousClass04Y r0 = recyclerView.A0R;
        if (r0 != null) {
            r0.A09();
        }
        recyclerView.A0o = false;
    }
}
