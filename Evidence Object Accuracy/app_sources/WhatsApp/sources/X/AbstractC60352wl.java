package X;

import android.content.Context;
import android.util.AttributeSet;

/* renamed from: X.2wl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC60352wl extends AnonymousClass2Ew {
    public boolean A00;

    public AbstractC60352wl(Context context) {
        super(context);
        A00();
    }

    public AbstractC60352wl(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC60352wl(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }
}
