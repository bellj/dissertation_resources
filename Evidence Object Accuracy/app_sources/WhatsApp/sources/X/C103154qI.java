package X;

import android.content.Context;
import com.whatsapp.location.GroupChatLiveLocationsActivity;

/* renamed from: X.4qI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103154qI implements AbstractC009204q {
    public final /* synthetic */ GroupChatLiveLocationsActivity A00;

    public C103154qI(GroupChatLiveLocationsActivity groupChatLiveLocationsActivity) {
        this.A00 = groupChatLiveLocationsActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
