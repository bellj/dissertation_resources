package X;

import android.util.SparseBooleanArray;

/* renamed from: X.4WQ  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4WQ {
    public final SparseBooleanArray A00 = new SparseBooleanArray();

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass4WQ)) {
            return false;
        }
        return this.A00.equals(((AnonymousClass4WQ) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
