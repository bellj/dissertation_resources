package X;

import java.util.concurrent.Callable;

/* renamed from: X.1nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CallableC38381nz implements Callable {
    public final /* synthetic */ C16150oX A00;
    public final /* synthetic */ C15650ng A01;
    public final /* synthetic */ AbstractC16130oV A02;

    public CallableC38381nz(C16150oX r1, C15650ng r2, AbstractC16130oV r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        this.A01.A07.A06(this.A00.A0F, this.A02.A0y);
        return Boolean.TRUE;
    }
}
