package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.1WB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1WB implements AnonymousClass1W6 {
    public final /* synthetic */ C247016n A00;
    public final /* synthetic */ AnonymousClass1WA A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;

    public AnonymousClass1WB(C247016n r1, AnonymousClass1WA r2, String str, String str2, String str3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = str;
        this.A04 = str2;
        this.A03 = str3;
    }

    @Override // X.AnonymousClass1W6
    public void AQu(UserJid userJid) {
        C247016n r2 = this.A00;
        r2.A02.A0a(userJid.getRawString());
        StringBuilder sb = new StringBuilder("Galaxy /onGetBusinessPublicKeyError/");
        sb.append(userJid.getRawString());
        Log.w(sb.toString());
        AnonymousClass1WA r0 = this.A01;
        if (r0 != null) {
            r0.A00();
        }
        r2.A00.AaV("galaxy-connection-public-key-error-response", "", false);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 1, insn: 0x015b: IGET  (r0 I:X.1WA) = (r1 I:X.1WB) X.1WB.A01 X.1WA, block:B:27:0x0156
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    @Override // X.AnonymousClass1W6
    public void AQv(
/*
[375] Method generation error in method: X.1WB.AQv(com.whatsapp.jid.UserJid, java.lang.String):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r23v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/
}
