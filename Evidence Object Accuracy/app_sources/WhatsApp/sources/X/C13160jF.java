package X;

import com.google.firebase.iid.Registrar;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;

/* renamed from: X.0jF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13160jF extends AbstractC13170jG {
    public static final AbstractC13190jI A04 = C13340jY.A00;
    public final C13270jQ A00;
    public final Map A01 = new HashMap();
    public final Map A02 = new HashMap();
    public final Map A03 = new HashMap();

    public C13160jF(Iterable iterable, Executor executor, C13080j7... r14) {
        C13270jQ r4 = new C13270jQ(executor);
        this.A00 = r4;
        ArrayList arrayList = new ArrayList();
        C13090j8 r1 = new C13090j8(C13270jQ.class, new Class[]{C13270jQ.class, AbstractC13280jR.class});
        r1.A02 = new C13100j9(r4);
        arrayList.add(r1.A00());
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            arrayList.addAll(((Registrar) it.next()).getComponents());
        }
        Collections.addAll(arrayList, r14);
        HashMap hashMap = new HashMap(arrayList.size());
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            C13080j7 r8 = (C13080j7) it2.next();
            C13380jd r6 = new C13380jd(r8);
            for (Class cls : r8.A04) {
                C13390je r2 = new C13390je(cls, !(r8.A01 == 0));
                if (!hashMap.containsKey(r2)) {
                    hashMap.put(r2, new HashSet());
                }
                Set set = (Set) hashMap.get(r2);
                if (set.isEmpty() || r2.A01) {
                    set.add(r6);
                } else {
                    throw new IllegalArgumentException(String.format("Multiple components provide %s.", cls));
                }
            }
        }
        for (Set<C13380jd> set2 : hashMap.values()) {
            for (C13380jd r62 : set2) {
                for (C13140jD r0 : r62.A00.A03) {
                    Set<C13380jd> set3 = (Set) hashMap.get(new C13390je(r0.A01, r0.A00 == 2));
                    if (set3 != null) {
                        for (C13380jd r12 : set3) {
                            r62.A01.add(r12);
                            r12.A02.add(r62);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Collection collection : hashMap.values()) {
            hashSet.addAll(collection);
        }
        HashSet hashSet2 = new HashSet();
        Iterator it3 = hashSet.iterator();
        while (it3.hasNext()) {
            C13380jd r13 = (C13380jd) it3.next();
            if (r13.A02.isEmpty()) {
                hashSet2.add(r13);
            }
        }
        int i = 0;
        while (!hashSet2.isEmpty()) {
            C13380jd r3 = (C13380jd) hashSet2.iterator().next();
            hashSet2.remove(r3);
            i++;
            for (C13380jd r15 : r3.A01) {
                Set set4 = r15.A02;
                set4.remove(r3);
                if (set4.isEmpty()) {
                    hashSet2.add(r15);
                }
            }
        }
        if (i == arrayList.size()) {
            Iterator it4 = arrayList.iterator();
            while (it4.hasNext()) {
                C13080j7 r22 = (C13080j7) it4.next();
                this.A01.put(r22, new C13200jJ(new AbstractC13190jI(r22, this) { // from class: X.0jf
                    public final C13080j7 A00;
                    public final C13160jF A01;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC13190jI
                    public Object get() {
                        C13160jF r32 = this.A01;
                        C13080j7 r23 = this.A00;
                        return r23.A02.A80(new C13320jW(r32, r23));
                    }
                }));
            }
            Map map = this.A01;
            for (Map.Entry entry : map.entrySet()) {
                C13080j7 r16 = (C13080j7) entry.getKey();
                if (r16.A01 == 0) {
                    Object value = entry.getValue();
                    for (Object obj : r16.A04) {
                        this.A02.put(obj, value);
                    }
                }
            }
            for (C13080j7 r42 : map.keySet()) {
                for (C13140jD r17 : r42.A03) {
                    if (r17.A00 == 1) {
                        Map map2 = this.A02;
                        Class cls2 = r17.A01;
                        if (!map2.containsKey(cls2)) {
                            throw new C81373tu(String.format("Unsatisfied dependency for component %s: %s", r42, cls2));
                        }
                    }
                }
            }
            HashMap hashMap2 = new HashMap();
            for (Map.Entry entry2 : this.A01.entrySet()) {
                C13080j7 r18 = (C13080j7) entry2.getKey();
                if (r18.A01 != 0) {
                    Object value2 = entry2.getValue();
                    for (Object obj2 : r18.A04) {
                        if (!hashMap2.containsKey(obj2)) {
                            hashMap2.put(obj2, new HashSet());
                        }
                        ((Set) hashMap2.get(obj2)).add(value2);
                    }
                }
            }
            for (Map.Entry entry3 : hashMap2.entrySet()) {
                this.A03.put(entry3.getKey(), new C13200jJ(new AbstractC13190jI((Set) entry3.getValue()) { // from class: X.0jg
                    public final Set A00;

                    {
                        this.A00 = r1;
                    }

                    @Override // X.AbstractC13190jI
                    public Object get() {
                        Set<C13200jJ> set5 = this.A00;
                        HashSet hashSet3 = new HashSet();
                        for (C13200jJ r02 : set5) {
                            hashSet3.add(r02.get());
                        }
                        return Collections.unmodifiableSet(hashSet3);
                    }
                }));
            }
            return;
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it5 = hashSet.iterator();
        while (it5.hasNext()) {
            C13380jd r19 = (C13380jd) it5.next();
            if (!r19.A02.isEmpty() && !r19.A01.isEmpty()) {
                arrayList2.add(r19.A00);
            }
        }
        throw new C81383tv(arrayList2);
    }
}
