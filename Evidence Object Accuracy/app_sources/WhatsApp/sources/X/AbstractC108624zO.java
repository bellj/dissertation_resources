package X;

/* renamed from: X.4zO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC108624zO implements AbstractC117125Yn {
    public static boolean zzey;
    public int zzex = 0;

    public static int A05(AnonymousClass5XT r3, Object obj) {
        AbstractC108624zO r4 = (AbstractC108624zO) obj;
        AbstractC79123q5 r2 = (AbstractC79123q5) r4;
        int i = r2.zzjq;
        if (i != -1) {
            return i;
        }
        int AhO = r3.AhO(r4);
        r2.zzjq = AhO;
        return AhO;
    }

    public static C94984cr A06(Object obj) {
        AbstractC79123q5 r5 = (AbstractC79123q5) obj;
        C94984cr r1 = r5.zzjp;
        if (r1 != C94984cr.A05) {
            return r1;
        }
        C94984cr r0 = new C94984cr(new int[8], new Object[8], 0, true);
        r5.zzjp = r0;
        return r0;
    }
}
