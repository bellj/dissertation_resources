package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.util.Map;

/* renamed from: X.57O  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass57O implements AbstractC28461Nh {
    public final /* synthetic */ AnonymousClass1DV A00;
    public final /* synthetic */ File A01;
    public final /* synthetic */ File A02;
    public final /* synthetic */ File A03;

    public AnonymousClass57O(AnonymousClass1DV r1, File file, File file2, File file3) {
        this.A00 = r1;
        this.A03 = file;
        this.A02 = file2;
        this.A01 = file3;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void AOt(long j) {
        this.A03.delete();
        this.A02.delete();
        this.A01.delete();
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        Log.e(C12960it.A0d(str, C12960it.A0k("MemoryExceptionsUploadHelper/Error: ")));
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
    }
}
