package X;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Window;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.Conversation;
import com.whatsapp.Main;
import com.whatsapp.authentication.AppAuthenticationActivity;
import com.whatsapp.messaging.MessageService;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.187  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass187 implements Application.ActivityLifecycleCallbacks {
    public int A00;
    public boolean A01;
    public boolean A02 = true;
    public boolean A03;
    public boolean A04;
    public final AnonymousClass186 A05;
    public final C16210od A06;
    public final AnonymousClass183 A07;
    public final C14900mE A08;
    public final C20640w5 A09;
    public final C22670zS A0A;
    public final AnonymousClass181 A0B;
    public final C19380u1 A0C;
    public final C18720su A0D;
    public final AnonymousClass01d A0E;
    public final AnonymousClass180 A0F;
    public final C22050yP A0G;
    public final C16120oU A0H;
    public final C22080yU A0I;
    public final C250717z A0J;
    public final C19890uq A0K;
    public final C21200x2 A0L;
    public final C15230mm A0M;
    public final AnonymousClass185 A0N;
    public final C22070yT A0O;
    public final C231210l A0P;
    public final AnonymousClass0yR A0Q;
    public final AbstractC14440lR A0R;

    public AnonymousClass187(AnonymousClass186 r2, C16210od r3, AnonymousClass183 r4, C14900mE r5, C20640w5 r6, C22670zS r7, AnonymousClass181 r8, C19380u1 r9, C18720su r10, AnonymousClass01d r11, AnonymousClass180 r12, C22050yP r13, C16120oU r14, C22080yU r15, C250717z r16, C19890uq r17, C21200x2 r18, C15230mm r19, AnonymousClass185 r20, C22070yT r21, C231210l r22, AnonymousClass0yR r23, AbstractC14440lR r24) {
        this.A0D = r10;
        this.A08 = r5;
        this.A0R = r24;
        this.A09 = r6;
        this.A0H = r14;
        this.A0B = r8;
        this.A0A = r7;
        this.A0C = r9;
        this.A0K = r17;
        this.A0M = r19;
        this.A0E = r11;
        this.A0Q = r23;
        this.A0L = r18;
        this.A0G = r13;
        this.A0O = r21;
        this.A0I = r15;
        this.A0N = r20;
        this.A06 = r3;
        this.A07 = r4;
        this.A0J = r16;
        this.A0P = r22;
        this.A0F = r12;
        this.A05 = r2;
        this.A00 = 0;
    }

    public final void A00(Activity activity, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(activity.getClass().getName());
        sb.append(".on");
        sb.append(str);
        Log.i(sb.toString());
        this.A0F.A02(activity, str2);
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityCreated(Activity activity, Bundle bundle) {
        A00(activity, "Create", "Create");
        if (this.A00 == 0) {
            this.A0M.A08 = true;
        }
        if (activity instanceof ActivityC000900k) {
            ((ActivityC000900k) activity).A0V().A0T.A01.add(new AnonymousClass04K(this.A07));
        }
        Window window = activity.getWindow();
        window.setCallback(new Window$CallbackC461424q(window.getCallback(), this.A0Q));
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityDestroyed(Activity activity) {
        A00(activity, "Destroy", "Destroy");
        this.A0J.A00();
        C22080yU r5 = this.A0I;
        StringBuilder sb = new StringBuilder("Activity_");
        sb.append(activity.getClass().getSimpleName());
        sb.append("_");
        sb.append(activity.hashCode());
        String obj = sb.toString();
        ConcurrentHashMap concurrentHashMap = r5.A05;
        if (!concurrentHashMap.containsKey(obj) && ((long) concurrentHashMap.size()) <= 100) {
            concurrentHashMap.put(obj, new C454221n(activity, obj, r5.A04, SystemClock.elapsedRealtime()));
            r5.A02.Ab4(new RunnableBRunnable0Shape8S0100000_I0_8(r5, 20), "MemoryLeakReporter.pruneRefs");
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityPaused(Activity activity) {
        if (!(activity instanceof Main)) {
            C15230mm r2 = this.A0M;
            StringBuilder sb = new StringBuilder("pause_");
            sb.append(activity.getClass());
            r2.A06(sb.toString());
        }
        if (!(activity instanceof Conversation)) {
            this.A0L.A00();
        }
        A00(activity, "Pause", "Pause");
        if (this.A03) {
            this.A0R.Ab6(new RunnableBRunnable0Shape0S0210000_I0(this, activity, 0, this.A04));
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityResumed(Activity activity) {
        AnonymousClass00E r0;
        boolean z;
        A00(activity, "Resume", "Resume");
        if (activity instanceof AbstractC13880kU) {
            r0 = ((AbstractC13880kU) activity).AGM();
        } else {
            r0 = AnonymousClass01V.A03;
        }
        if (r0.A00()) {
            z = true;
            if (new Random().nextBoolean()) {
                this.A04 = true;
            } else {
                this.A04 = false;
                this.A03 = true;
                return;
            }
        } else {
            this.A04 = false;
            z = false;
        }
        this.A03 = false;
        if (z) {
            this.A0R.Ab6(new RunnableBRunnable0Shape0S0210000_I0(this, activity, 0, z));
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        A00(activity, "SaveInstanceState", "Save");
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStarted(Activity activity) {
        A00(activity, "Start", "Start");
        if (this.A00 == 0 && !this.A01) {
            Log.i("app-init/application foregrounded");
            MessageService.A01(activity, this.A0P);
            C20640w5 r1 = this.A09;
            if (!r1.A03() && !r1.A02()) {
                this.A0K.A0C(1, true, false, false, false);
            }
            C19380u1 r3 = this.A0C;
            r3.A0D.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 3));
            C16210od r12 = this.A06;
            r12.A00 = true;
            for (AbstractC22060yS r0 : r12.A01()) {
                r0.AMG();
            }
        }
        int i = this.A00;
        boolean z = false;
        if (i == 0) {
            z = true;
        }
        this.A02 = z;
        this.A00 = i + 1;
        Window window = activity.getWindow();
        Window.Callback callback = window.getCallback();
        if (!(callback instanceof Window$CallbackC461424q)) {
            window.setCallback(new Window$CallbackC461424q(callback, this.A0Q));
        }
        C22670zS r4 = this.A0A;
        if (!r4.A02()) {
            C14820m6 r32 = r4.A03;
            if (r32.A00.getBoolean("privacy_fingerprint_enabled", false)) {
                Log.i("AppAuthManager/resetAppAuthSettingIfNecessary: no biometrics enrolled and setting was enabled");
                r32.A17(false);
                r4.A01(false);
            }
        }
    }

    @Override // android.app.Application.ActivityLifecycleCallbacks
    public void onActivityStopped(Activity activity) {
        C461124n r9;
        A00(activity, "Stop", "Stop");
        boolean isChangingConfigurations = activity.isChangingConfigurations();
        this.A01 = isChangingConfigurations;
        int i = this.A00 - 1;
        this.A00 = i;
        if (i == 0 && !isChangingConfigurations) {
            AnonymousClass180 r4 = this.A0F;
            r4.A03.execute(new RunnableBRunnable0Shape0S1100000_I0(24, "App backgrounded", r4));
            Log.i("app-init/application backgrounded");
            C15230mm r1 = this.A0M;
            r1.A06("app_session_ended");
            r1.A08 = false;
            C22050yP r42 = this.A0G;
            r42.A0K.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(r42, 0, this.A0E));
            if (!(activity instanceof AppAuthenticationActivity)) {
                C22670zS r2 = this.A0A;
                SharedPreferences sharedPreferences = r2.A03.A00;
                if (!sharedPreferences.getBoolean("fingerprint_authentication_needed", false)) {
                    Log.i("AppAuthManager/onApplicationBackground");
                    r2.A01(true);
                    sharedPreferences.edit().putLong("app_background_time", SystemClock.elapsedRealtime()).apply();
                }
            }
            AnonymousClass185 r10 = this.A0N;
            if ((r10.A03() || r10.A05.AJj(689639794)) && (r9 = r10.A00) != null) {
                if (r9.A02) {
                    Map map = r9.A06;
                    for (Map.Entry entry : map.entrySet()) {
                        C461724v r7 = new C461724v();
                        C461324p r11 = (C461324p) entry.getValue();
                        r7.A03 = Long.valueOf(r11.A03);
                        r7.A02 = (Integer) entry.getKey();
                        long j = r11.A03;
                        if (j > 0) {
                            double d = (double) j;
                            r7.A00 = Double.valueOf((r11.A01 * 60000.0d) / d);
                            r7.A01 = Double.valueOf((r11.A00 * 60000.0d) / d);
                        }
                        r9.A04.A07(r7);
                    }
                    map.clear();
                }
                r10.A01 = Boolean.FALSE;
                r10.A00 = null;
            }
            C19380u1 r3 = this.A0C;
            r3.A0D.execute(new RunnableBRunnable0Shape3S0100000_I0_3(r3, 2));
            List list = (List) this.A05.A00.get(0);
            if (list != null) {
                Iterator it = list.iterator();
                if (it.hasNext()) {
                    it.next();
                    throw new NullPointerException("onEvent");
                }
            }
            C16210od r12 = this.A06;
            r12.A00 = false;
            for (AbstractC22060yS r0 : r12.A01()) {
                r0.AMF();
            }
            this.A02 = true;
        }
    }
}
