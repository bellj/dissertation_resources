package X;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/* renamed from: X.3ii  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74743ii extends AnonymousClass0FE {
    public final /* synthetic */ C69233Yl A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74743ii(Context context, C69233Yl r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0FE
    public float A04(DisplayMetrics displayMetrics) {
        return 40.0f / TypedValue.applyDimension(1, 40.0f, displayMetrics);
    }
}
