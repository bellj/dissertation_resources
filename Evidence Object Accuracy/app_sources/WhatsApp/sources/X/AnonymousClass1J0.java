package X;

/* renamed from: X.1J0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1J0 extends AnonymousClass02K {
    @Override // X.AnonymousClass02K
    public boolean A00(Object obj, Object obj2) {
        return ((AbstractC27731Iz) obj).A9Y((AbstractC27731Iz) obj2);
    }

    @Override // X.AnonymousClass02K
    public boolean A01(Object obj, Object obj2) {
        AbstractC27731Iz r6 = (AbstractC27731Iz) obj;
        AbstractC27731Iz r7 = (AbstractC27731Iz) obj2;
        return r6.AGJ() == r7.AGJ() && r6.AHd() == r7.AHd();
    }
}
