package X;

import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.text.SeeMoreTextView;

/* renamed from: X.3gK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73493gK extends ClickableSpan {
    public final /* synthetic */ SeeMoreTextView A00;

    public C73493gK(SeeMoreTextView seeMoreTextView) {
        this.A00 = seeMoreTextView;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        SeeMoreTextView seeMoreTextView = this.A00;
        if (!seeMoreTextView.A03) {
            seeMoreTextView.A03 = true;
            seeMoreTextView.setText(seeMoreTextView.A01);
        }
    }
}
