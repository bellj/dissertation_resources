package X;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.2d2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53172d2 extends LinearLayout implements AnonymousClass004 {
    public C14820m6 A00;
    public C15600nX A01;
    public AnonymousClass11A A02;
    public GroupJid A03;
    public AnonymousClass2P7 A04;
    public boolean A05;
    public final View A06;
    public final AnonymousClass5UJ A07;

    public C53172d2(Context context, AbstractC37281lw r4) {
        super(context);
        if (!this.A05) {
            this.A05 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A00 = C12970iu.A0Z(A00);
            this.A02 = (AnonymousClass11A) A00.A8o.get();
            this.A01 = C12980iv.A0d(A00);
        }
        setOrientation(1);
        LinearLayout.inflate(getContext(), R.layout.community_home_invite_members, this);
        this.A07 = new AnonymousClass5UJ(r4, this) { // from class: X.57I
            public final /* synthetic */ AbstractC37281lw A00;
            public final /* synthetic */ C53172d2 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r3) {
                C53172d2 r0 = this.A01;
                AbstractC37281lw r1 = this.A00;
                GroupJid groupJid = r0.A03;
                if (groupJid != null && groupJid.equals(r3)) {
                    r1.ARV();
                }
            }
        };
        View A0D = AnonymousClass028.A0D(this, R.id.invite_members_row);
        this.A06 = A0D;
        AbstractView$OnClickListenerC34281fs.A02(A0D, this, context, 42);
        AbstractView$OnClickListenerC34281fs.A02(AnonymousClass028.A0D(this, R.id.invite_members_remove_button), this, r4, 43);
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A04;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A04 = r0;
        }
        return r0.generatedComponent();
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        AnonymousClass11A r0 = this.A02;
        r0.A00.add(this.A07);
    }

    @Override // android.view.View, android.view.ViewGroup
    public void onDetachedFromWindow() {
        AnonymousClass11A r0 = this.A02;
        r0.A00.remove(this.A07);
        super.onDetachedFromWindow();
    }
}
