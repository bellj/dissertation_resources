package X;

import java.util.Stack;

/* renamed from: X.5y7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129675y7 {
    public int A00;
    public C1316663q A01;
    public C128005vP A02;
    public final C125775rn A03 = new C125775rn();
    public final C117885au A04 = new C117885au();

    public C121385hl A00() {
        Stack stack = this.A03.A00;
        if (stack.size() == 0) {
            return null;
        }
        return (C121385hl) stack.peek();
    }

    public void A01(C1316663q r4, int i) {
        if (!r4.A02()) {
            this.A01 = r4;
            this.A00 = i;
            this.A04.A0B(new C127415uS(r4, "PENDING", i));
        }
    }
}
