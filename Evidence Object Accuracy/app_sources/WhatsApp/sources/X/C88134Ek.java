package X;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ProgressBar;

/* renamed from: X.4Ek  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C88134Ek {
    public static void A00(ProgressBar progressBar, int i) {
        if (progressBar != null && Build.VERSION.SDK_INT < 21) {
            Drawable indeterminateDrawable = progressBar.getIndeterminateDrawable();
            if (indeterminateDrawable != null) {
                indeterminateDrawable.setColorFilter(i, PorterDuff.Mode.MULTIPLY);
                progressBar.setIndeterminateDrawable(indeterminateDrawable);
            }
            Drawable progressDrawable = progressBar.getProgressDrawable();
            if (progressDrawable != null) {
                progressDrawable.setColorFilter(i, PorterDuff.Mode.MULTIPLY);
                progressBar.setProgressDrawable(progressDrawable);
            }
        }
    }
}
