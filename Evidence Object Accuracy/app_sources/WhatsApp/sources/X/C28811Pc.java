package X;

import android.text.TextUtils;

/* renamed from: X.1Pc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28811Pc {
    public final long A00;
    public final String A01;

    public C28811Pc(String str, long j) {
        this.A00 = j;
        this.A01 = str;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }
            C28811Pc r7 = (C28811Pc) obj;
            if (this.A00 != r7.A00 || !TextUtils.equals(this.A01, r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        long j = this.A00;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.A01;
        return i + (str != null ? str.hashCode() : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append(":");
        sb.append(AnonymousClass1US.A02(4, this.A01));
        return sb.toString();
    }
}
