package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.68O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68O implements AnonymousClass17B {
    public final C14830m7 A00;
    public final AnonymousClass179 A01;
    public final AnonymousClass17A A02;
    public final C16820po A03;
    public final C119885fG A04;
    public final String A05 = "CREATE_SHOPS_USER";

    public AnonymousClass68O(C14830m7 r3, AnonymousClass179 r4, AnonymousClass17A r5, C119885fG r6) {
        C16820po r1 = C17750rK.A00;
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
        this.A04 = r6;
        this.A03 = r1;
    }

    public final String A00(AnonymousClass1ZR r5, String str, String str2, String str3) {
        JSONObject A0a = C117295Zj.A0a();
        A0a.put("version", 1);
        A0a.put("operation", str2);
        A0a.put("timestamp", C117295Zj.A03(this.A00));
        A0a.put("client_pub_key", str);
        A0a.put("client_pub_key_type", "RSA 2048");
        A0a.put("password", str3);
        if (r5 != null) {
            Object obj = r5.A00;
            AnonymousClass009.A05(obj);
            A0a.put("fbid", String.valueOf(C12980iv.A0G(obj)));
        }
        return A0a.toString();
    }

    @Override // X.AnonymousClass17B
    public final void AZD(AnonymousClass3EB r12, AnonymousClass4VZ r13, Integer num, PublicKey publicKey, X509Certificate x509Certificate) {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
            KeyPair generateKeyPair = instance.generateKeyPair();
            PublicKey publicKey2 = generateKeyPair.getPublic();
            StringBuilder A0k = C12960it.A0k("-----BEGIN PUBLIC KEY-----\n");
            A0k.append(C117305Zk.A0n(publicKey2.getEncoded()));
            String A0d = C12960it.A0d("\n-----END PUBLIC KEY-----\n", A0k);
            SecureRandom secureRandom = new SecureRandom();
            StringBuilder A0h = C12960it.A0h();
            int i = 0;
            do {
                A0h.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}".charAt(secureRandom.nextInt(76)));
                i++;
            } while (i < 50);
            String obj = A0h.toString();
            this.A04.A00(new C119915fJ(r12, r12, this, obj, generateKeyPair), this.A01.A00(A00(null, A0d, this.A05, obj), x509Certificate), 20);
        } catch (NoSuchAlgorithmException | GeneralSecurityException | JSONException e) {
            r12.A01(e);
        }
    }

    @Override // X.AnonymousClass17B
    public final void AZF(C64063Ec r5, AnonymousClass3EB r6, AnonymousClass4VZ r7, Integer num, PublicKey publicKey, X509Certificate x509Certificate) {
        String A0K = C117315Zl.A0K(r5.A04);
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
            PublicKey publicKey2 = instance.generateKeyPair().getPublic();
            StringBuilder A0k = C12960it.A0k("-----BEGIN PUBLIC KEY-----\n");
            A0k.append(C117305Zk.A0n(publicKey2.getEncoded()));
            this.A04.A00(new C119905fI(r6, r6, this), this.A01.A00(A00(r5.A03, C12960it.A0d("\n-----END PUBLIC KEY-----\n", A0k), "DELETE_USER", A0K), x509Certificate), 19);
        } catch (NoSuchAlgorithmException | GeneralSecurityException | JSONException e) {
            r6.A01(e);
        }
    }

    @Override // X.AnonymousClass17B
    public final void AZG(C64063Ec r10, AnonymousClass3EB r11, AnonymousClass4VZ r12, Integer num, PublicKey publicKey, X509Certificate x509Certificate) {
        String A0K = C117315Zl.A0K(r10.A04);
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
            KeyPair generateKeyPair = instance.generateKeyPair();
            PublicKey publicKey2 = generateKeyPair.getPublic();
            StringBuilder A0k = C12960it.A0k("-----BEGIN PUBLIC KEY-----\n");
            A0k.append(C117305Zk.A0n(publicKey2.getEncoded()));
            this.A04.A00(new C119925fK(r10, r11, r11, this, generateKeyPair), this.A01.A00(A00(r10.A03, C12960it.A0d("\n-----END PUBLIC KEY-----\n", A0k), "GET_ACCESS_TOKEN", A0K), x509Certificate), 19);
        } catch (NoSuchAlgorithmException | GeneralSecurityException | JSONException e) {
            r11.A01(e);
        }
    }
}
