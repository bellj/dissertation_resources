package X;

import java.util.Arrays;

/* renamed from: X.1c4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32381c4 {
    public final AbstractC14640lm A00;
    public final AbstractC14640lm A01;
    public final boolean A02;

    public C32381c4(AbstractC14640lm r1, AbstractC14640lm r2, boolean z) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = z;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && (obj instanceof C32381c4)) {
                C32381c4 r5 = (C32381c4) obj;
                if (!C29941Vi.A00(this.A00, r5.A00) || !C29941Vi.A00(this.A01, r5.A01) || this.A02 != r5.A02) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A00, this.A01, Boolean.valueOf(this.A02)});
    }
}
