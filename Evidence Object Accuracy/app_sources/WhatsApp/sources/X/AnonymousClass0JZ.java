package X;

/* renamed from: X.0JZ  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0JZ {
    /* Fake field, exist only in values array */
    NONE,
    LEFT,
    TOP,
    RIGHT,
    BOTTOM,
    BASELINE,
    CENTER,
    CENTER_X,
    CENTER_Y
}
