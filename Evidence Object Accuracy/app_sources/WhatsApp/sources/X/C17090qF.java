package X;

/* renamed from: X.0qF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17090qF {
    public final C14330lG A00;
    public final C15570nT A01;
    public final C15810nw A02;
    public final C16590pI A03;
    public final C14950mJ A04;
    public final C20820wN A05;
    public final C16490p7 A06;
    public final C21600xg A07;
    public final C14850m9 A08;
    public final C16120oU A09;
    public final C15510nN A0A;
    public final AbstractC14440lR A0B;

    public C17090qF(C14330lG r1, C15570nT r2, C15810nw r3, C16590pI r4, C14950mJ r5, C20820wN r6, C16490p7 r7, C21600xg r8, C14850m9 r9, C16120oU r10, C15510nN r11, AbstractC14440lR r12) {
        this.A08 = r9;
        this.A03 = r4;
        this.A01 = r2;
        this.A0B = r12;
        this.A00 = r1;
        this.A09 = r10;
        this.A02 = r3;
        this.A04 = r5;
        this.A06 = r7;
        this.A07 = r8;
        this.A0A = r11;
        this.A05 = r6;
    }
}
