package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1vY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42641vY {
    public Uri A00;
    public C32731ce A01;
    public AbstractC14640lm A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;
    public Boolean A0E;
    public Boolean A0F;
    public Boolean A0G;
    public Integer A0H;
    public Integer A0I;
    public Integer A0J;
    public Long A0K;
    public Long A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public ArrayList A0Q;
    public ArrayList A0R;
    public List A0S;
    public final Context A0T;

    public C42641vY(Context context) {
        this.A0T = context;
    }

    public Intent A00() {
        Intent intent = new Intent();
        intent.setClassName(this.A0T.getPackageName(), "com.whatsapp.contact.picker.ContactPicker");
        Boolean bool = this.A0D;
        if (bool != null) {
            intent.putExtra("send", bool);
        }
        Boolean bool2 = this.A0F;
        if (bool2 != null) {
            intent.putExtra("skip_preview", bool2);
        }
        String str = this.A0N;
        if (str != null) {
            intent.putExtra("file_path", str);
        }
        ArrayList arrayList = this.A0R;
        if (arrayList != null) {
            intent.putExtra("message_types", arrayList);
        }
        Boolean bool3 = this.A03;
        if (bool3 != null) {
            intent.putExtra("block_contact", bool3);
        }
        ArrayList arrayList2 = this.A0Q;
        if (arrayList2 != null) {
            intent.putExtra("blocked_list", arrayList2);
        }
        String str2 = this.A0P;
        if (str2 != null) {
            intent.setType(str2);
        }
        String str3 = this.A0O;
        if (str3 != null) {
            intent.putExtra("android.intent.extra.TEXT", str3);
        }
        Boolean bool4 = this.A06;
        if (bool4 != null) {
            intent.putExtra("forward", bool4);
        }
        AbstractC14640lm r3 = this.A02;
        if (!(r3 == null && this.A0M == null)) {
            String str4 = this.A0M;
            if (str4 == null) {
                str4 = C15380n4.A03(r3);
            }
            intent.putExtra("forward_jid", str4);
        }
        List list = this.A0S;
        if (list != null) {
            intent.putExtra("jids", C15380n4.A06(list));
        }
        Long l = this.A0L;
        if (l != null) {
            intent.putExtra("forward_video_duration", l);
        }
        Integer num = this.A0J;
        if (num != null) {
            intent.putExtra("forward_text_length", num);
        }
        Integer num2 = this.A0H;
        if (num2 != null) {
            intent.putExtra("forward_messages_becoming_frequently_forwarded", num2);
        }
        Boolean bool5 = this.A09;
        if (bool5 != null) {
            intent.putExtra("is_forwarded", bool5);
        }
        Boolean bool6 = this.A07;
        if (bool6 != null) {
            intent.putExtra("forward_ctwa", bool6);
        }
        Boolean bool7 = this.A08;
        if (bool7 != null) {
            intent.putExtra("forward_highly_forwarded", bool7);
        }
        Boolean bool8 = this.A0E;
        if (bool8 != null) {
            intent.putExtra("set_group_icon", bool8);
        }
        Long l2 = this.A0K;
        if (l2 != null) {
            intent.putExtra("message_row_id", l2);
        }
        Boolean bool9 = this.A05;
        if (bool9 != null) {
            intent.putExtra("email_history", bool9);
        }
        Boolean bool10 = this.A04;
        if (bool10 != null) {
            intent.putExtra("call_picker", bool10);
        }
        Boolean bool11 = this.A0C;
        if (bool11 != null) {
            intent.putExtra("request_sync", bool11);
        }
        Boolean bool12 = this.A0B;
        if (bool12 != null) {
            intent.putExtra("request_out_contact_sync", bool12);
        }
        Uri uri = this.A00;
        if (uri != null) {
            intent.putExtra("android.intent.extra.STREAM", uri);
        }
        Boolean bool13 = this.A0G;
        if (bool13 != null) {
            intent.putExtra("status_chip_clicked", bool13);
        }
        C32731ce r1 = this.A01;
        if (r1 != null) {
            intent.putExtra("status_distribution", r1);
        }
        Boolean bool14 = this.A0A;
        if (bool14 != null) {
            intent.putExtra("multi_select_ddm", bool14);
        }
        Integer num3 = this.A0I;
        if (num3 != null) {
            intent.putExtra("dm_duration", num3);
        }
        return intent;
    }
}
