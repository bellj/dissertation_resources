package X;

import java.text.Collator;
import java.util.Comparator;

/* renamed from: X.3ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71293ch implements Comparator {
    public final C15610nY A00;
    public final C19990v2 A01;
    public final Collator A02;

    public C71293ch(C15610nY r3, AnonymousClass018 r4, C19990v2 r5) {
        this.A01 = r5;
        this.A00 = r3;
        Collator instance = Collator.getInstance(C12970iu.A14(r4));
        this.A02 = instance;
        instance.setDecomposition(1);
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        long j;
        long j2;
        C15370n3 r8 = (C15370n3) obj;
        C15370n3 r9 = (C15370n3) obj2;
        AbstractC14640lm r3 = (AbstractC14640lm) C15370n3.A03(r8, AbstractC14640lm.class);
        AbstractC14640lm r2 = (AbstractC14640lm) C15370n3.A03(r9, AbstractC14640lm.class);
        C19990v2 r1 = this.A01;
        if (r1.A0D(r3)) {
            j = r1.A05(r3);
        } else {
            j = 0;
        }
        if (r1.A0D(r2)) {
            j2 = r1.A05(r2);
        } else {
            j2 = 0;
        }
        if (j == 0 && j2 == 0) {
            Collator collator = this.A02;
            C15610nY r0 = this.A00;
            return collator.compare(r0.A04(r8), r0.A04(r9));
        } else if (j == 0) {
            return 1;
        } else {
            if (j2 == 0) {
                return -1;
            }
            if (j != j2) {
                return j < j2 ? 1 : -1;
            }
            C15610nY r02 = this.A00;
            return r02.A04(r8).compareTo(r02.A04(r9));
        }
    }
}
