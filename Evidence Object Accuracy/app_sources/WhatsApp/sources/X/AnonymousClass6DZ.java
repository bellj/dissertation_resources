package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.wabloks.ui.WaBloksActivity;
import java.util.List;
import java.util.Map;

/* renamed from: X.6DZ  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6DZ implements AnonymousClass5WG {
    public final /* synthetic */ AnonymousClass67N A00;
    public final /* synthetic */ WaBloksActivity A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ List A03;

    public AnonymousClass6DZ(AnonymousClass67N r1, WaBloksActivity waBloksActivity, String str, List list) {
        this.A00 = r1;
        this.A03 = list;
        this.A01 = waBloksActivity;
        this.A02 = str;
    }

    @Override // X.AnonymousClass5WG
    public void AQL(Map map) {
        C117295Zj.A0j(this.A01, this.A02);
    }

    @Override // X.AnonymousClass5WG
    public void AQM(AnonymousClass3M3 r12) {
        String str = r12.A04;
        if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(r12.A02) || !TextUtils.isEmpty(r12.A03)) {
            List list = this.A03;
            String str2 = r12.A00;
            String str3 = r12.A02;
            WaBloksActivity waBloksActivity = this.A01;
            ((C16170oZ) this.A00.A0K.get()).A0P(null, str2, str, str3, waBloksActivity.getString(R.string.thought_you_like_this), list, r12.A00());
            if (list.size() == 1) {
                waBloksActivity.startActivity(new C14960mK().A0i(waBloksActivity, (AbstractC14640lm) C12980iv.A0o(list)));
                return;
            }
            return;
        }
        C117295Zj.A0j(this.A01, this.A02);
    }
}
