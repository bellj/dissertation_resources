package X;

import android.os.Bundle;

/* renamed from: X.3FU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FU {
    public final Bundle A00;

    public AnonymousClass3FU(int i) {
        Bundle A0D = C12970iu.A0D();
        this.A00 = A0D;
        A0D.putInt("dialog_id", i);
    }

    public void A00() {
        this.A00.putBoolean("cancelable", false);
    }

    public void A01(String str) {
        this.A00.putString("negative_button", str);
    }

    public void A02(String str) {
        this.A00.putString("positive_button", str);
    }

    public void A03(String str) {
        this.A00.putString("title", str);
    }
}
