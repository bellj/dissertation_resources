package X;

/* renamed from: X.5F6  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5F6 implements AnonymousClass5VH {
    /* JADX INFO: finally extract failed */
    /*  JADX ERROR: IndexOutOfBoundsException in pass: ConstInlineVisitor
        java.lang.IndexOutOfBoundsException: Index: -1
        	at java.util.Collections$EmptyList.get(Collections.java:4456)
        	at jadx.core.dex.visitors.ConstInlineVisitor.needExplicitCast(ConstInlineVisitor.java:259)
        	at jadx.core.dex.visitors.ConstInlineVisitor.replaceArg(ConstInlineVisitor.java:240)
        	at jadx.core.dex.visitors.ConstInlineVisitor.replaceConst(ConstInlineVisitor.java:161)
        	at jadx.core.dex.visitors.ConstInlineVisitor.checkInsn(ConstInlineVisitor.java:105)
        	at jadx.core.dex.visitors.ConstInlineVisitor.process(ConstInlineVisitor.java:53)
        	at jadx.core.dex.visitors.ConstInlineVisitor.visit(ConstInlineVisitor.java:45)
        */
    @Override // X.AnonymousClass5VH
    public final java.lang.Object A7Q(X.AnonymousClass5WO r7, X.AnonymousClass5VI r8) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof X.C113645Ik
            if (r0 == 0) goto L_0x0022
            r5 = r7
            X.5Ik r5 = (X.C113645Ik) r5
            int r2 = r5.label
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r0 = r2 & r1
            if (r0 == 0) goto L_0x0022
            int r2 = r2 - r1
            r5.label = r2
        L_0x0012:
            java.lang.Object r4 = r5.result
            X.4A6 r3 = X.AnonymousClass4A6.A01
            int r0 = r5.label
            r2 = 1
            if (r0 == 0) goto L_0x0033
            if (r0 != r2) goto L_0x002c
            java.lang.Object r1 = r5.L$0
            X.5Eg r1 = (X.AbstractC112665Eg) r1
            goto L_0x0028
        L_0x0022:
            X.5Ik r5 = new X.5Ik
            r5.<init>(r7, r6)
            goto L_0x0012
        L_0x0028:
            X.C88174Eo.A00(r4)     // Catch: all -> 0x0059
            goto L_0x0053
        L_0x002c:
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x0033:
            X.C88174Eo.A00(r4)
            X.5X4 r0 = r5.ABi()
            X.5In r1 = new X.5In
            r1.<init>(r0, r8)
            r5.L$0 = r1     // Catch: all -> 0x0059
            r5.label = r2     // Catch: all -> 0x0059
            r0 = r6
            X.5L8 r0 = (X.AnonymousClass5L8) r0     // Catch: all -> 0x0059
            X.5ZQ r0 = r0.A00     // Catch: all -> 0x0059
            java.lang.Object r0 = r0.AJ5(r1, r5)     // Catch: all -> 0x0059
            if (r0 == r3) goto L_0x0050
            X.1WZ r0 = X.AnonymousClass1WZ.A00     // Catch: all -> 0x0059
        L_0x0050:
            if (r0 != r3) goto L_0x0053
            return r3
        L_0x0053:
            r1.A00()
            X.1WZ r0 = X.AnonymousClass1WZ.A00
            return r0
        L_0x0059:
            r0 = move-exception
            r1.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5F6.A7Q(X.5WO, X.5VI):java.lang.Object");
    }
}
