package X;

import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.15v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C245215v {
    public final C30621Yd A00 = new C30621Yd();
    public final ConcurrentHashMap A01 = new ConcurrentHashMap();

    public AnonymousClass1YM A00(AnonymousClass1YL r5, AbstractC15590nW r6) {
        AnonymousClass1YM r0;
        ConcurrentHashMap concurrentHashMap = this.A01;
        AnonymousClass1YM r02 = (AnonymousClass1YM) concurrentHashMap.get(r6);
        if (r02 != null) {
            return r02;
        }
        C30621Yd r1 = this.A00;
        Integer valueOf = Integer.valueOf(Math.abs(r6.hashCode()) % 128);
        ConcurrentHashMap concurrentHashMap2 = r1.A00;
        if (!concurrentHashMap2.containsKey(valueOf)) {
            concurrentHashMap2.putIfAbsent(valueOf, new Object());
        }
        Object obj = concurrentHashMap2.get(valueOf);
        AnonymousClass009.A05(obj);
        synchronized (obj) {
            r0 = (AnonymousClass1YM) concurrentHashMap.get(r6);
            if (r0 == null) {
                r0 = r5.A7t(r6);
                concurrentHashMap.put(r6, r0);
            }
        }
        return r0;
    }
}
