package X;

/* renamed from: X.5L7  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5L7 extends AnonymousClass5LD {
    public final C114215Kq A00;
    public final C06080Sc A01;

    public AnonymousClass5L7(C114215Kq r1, C06080Sc r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.C94524by
    public String toString() {
        return C16700pc.A08("ReceiveHasNext@", C72453ed.A0o(this));
    }
}
