package X;

import com.whatsapp.storage.StorageUsageActivity;

/* renamed from: X.4rN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103824rN implements AnonymousClass07L {
    public final /* synthetic */ StorageUsageActivity A00;

    public C103824rN(StorageUsageActivity storageUsageActivity) {
        this.A00 = storageUsageActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        StorageUsageActivity storageUsageActivity = this.A00;
        storageUsageActivity.A0K = str;
        storageUsageActivity.A0M = C32751cg.A02(((ActivityC13830kP) storageUsageActivity).A01, str);
        StorageUsageActivity.A02(storageUsageActivity, storageUsageActivity.A0L, null, false);
        return false;
    }

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        StorageUsageActivity storageUsageActivity = this.A00;
        storageUsageActivity.A0K = str;
        storageUsageActivity.A0M = C32751cg.A02(((ActivityC13830kP) storageUsageActivity).A01, str);
        StorageUsageActivity.A02(storageUsageActivity, storageUsageActivity.A0L, null, false);
        return false;
    }
}
