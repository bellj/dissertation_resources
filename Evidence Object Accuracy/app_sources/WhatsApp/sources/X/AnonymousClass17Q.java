package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

/* renamed from: X.17Q  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass17Q {
    public C27661Io A00;
    public String A01;
    public final AbstractC15710nm A02;
    public final AnonymousClass17K A03;
    public final C17220qS A04;
    public final AnonymousClass17M A05;
    public final AnonymousClass17L A06;
    public final AnonymousClass17P A07;
    public final AnonymousClass17O A08;
    public final C19750uc A09;
    public final AnonymousClass17N A0A;
    public final String A0B;
    public final Map A0C = new LinkedHashMap();
    public final Map A0D = new LinkedHashMap();

    public AnonymousClass17Q(AbstractC15710nm r2, AnonymousClass17K r3, C17220qS r4, AnonymousClass17M r5, AnonymousClass17L r6, AnonymousClass17P r7, AnonymousClass17O r8, C19750uc r9, AnonymousClass17N r10) {
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r4, 2);
        C16700pc.A0E(r5, 5);
        C16700pc.A0E(r9, 6);
        C16700pc.A0E(r6, 7);
        C16700pc.A0E(r3, 8);
        C16700pc.A0E(r7, 9);
        this.A02 = r2;
        this.A04 = r4;
        this.A0A = r10;
        this.A08 = r8;
        this.A05 = r5;
        this.A09 = r9;
        this.A06 = r6;
        this.A03 = r3;
        this.A07 = r7;
        String obj = UUID.randomUUID().toString();
        C16700pc.A0B(obj);
        this.A0B = obj;
    }

    public static final /* synthetic */ void A00(AbstractC19620uP r3, AnonymousClass17Q r4, Long l, int i) {
        r3.A02.A03(i, "iqResponse");
        r3.A01(i, "error_code", l.longValue());
        r3.A02(i, "session_id", r4.A0A.A00());
    }

    public static final /* synthetic */ void A01(AnonymousClass17Q r1, AnonymousClass3EH r2) {
        AbstractC116075Ua r12 = (AbstractC116075Ua) r1.A0D.get(r2.A01);
        if (r12 != null) {
            AnonymousClass1V8 r0 = r2.A00;
            C16700pc.A0B(r0);
            r12.AZa(r0);
        }
    }

    public final C27661Io A02() {
        C27661Io r0 = this.A00;
        if (r0 != null) {
            return r0;
        }
        C16700pc.A0K("fcsStateMachine");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final AnonymousClass3BG A03() {
        return new AnonymousClass3BG(new AnonymousClass4LW(new JSONObject(C17530qx.A02(new C17520qw("session-id", this.A0A.A00()), new C17520qw("bloks-versioning-id", "8bc4f82b25adef28e5962e5633432810078bf409e67f8f8a853f4d993d444666"))).toString()));
    }

    public final String A04() {
        String str = this.A01;
        if (str != null) {
            return str;
        }
        C16700pc.A0K("config");
        throw new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public final void A05(AnonymousClass5WA r16, EnumC869549q r17, AnonymousClass3FB r18) {
        String str;
        String str2;
        C16700pc.A0E(r17, 1);
        int hashCode = r18.hashCode();
        String A04 = A04();
        AnonymousClass17L r6 = this.A06;
        r6.A00(hashCode, A04);
        r6.A02(hashCode, "config_name", A04());
        C17220qS r8 = this.A04;
        String A01 = r8.A01();
        C16700pc.A0B(A01);
        int hashCode2 = r18.hashCode();
        String name = r17.name();
        r6.A02.A03(hashCode2, "iqRequest");
        if (name != null) {
            r6.A02(hashCode2, "iq_type", name);
        }
        r6.A02(hashCode2, "session_id", this.A0A.A00());
        switch (r17.ordinal()) {
            case 0:
                Map map = r18.A02;
                if (map != null) {
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    for (Map.Entry entry : map.entrySet()) {
                        if (entry.getValue() != null) {
                            linkedHashMap.put(entry.getKey(), entry.getValue());
                        }
                    }
                    str = new JSONObject(linkedHashMap).toString();
                } else {
                    str = null;
                }
                String str3 = r18.A00;
                AnonymousClass3BF r0 = new AnonymousClass3BF(new C63603Cf(new AnonymousClass3BE(r18.A01, str), A03(), str3), new AnonymousClass3CS(A01));
                r8.A09(new C620333p(r16, this, r18, r0), r0.A00, A01, 302, 32000);
                return;
            case 1:
                Map map2 = r18.A02;
                if (map2 != null) {
                    LinkedHashMap linkedHashMap2 = new LinkedHashMap();
                    for (Map.Entry entry2 : map2.entrySet()) {
                        if (entry2.getValue() != null) {
                            linkedHashMap2.put(entry2.getKey(), entry2.getValue());
                        }
                    }
                    str2 = new JSONObject(linkedHashMap2).toString();
                } else {
                    str2 = null;
                }
                String str4 = r18.A00;
                AnonymousClass3BH r02 = new AnonymousClass3BH(new C63603Cf(new AnonymousClass3BE(r18.A01, str2), A03(), str4), new AnonymousClass3CT(A01));
                r8.A09(new C620433q(r16, this, r18, r02), r02.A00, A01, 303, 32000);
                return;
            default:
                return;
        }
    }

    public final void A06(AnonymousClass5UY r14, AnonymousClass5UZ r15, AnonymousClass3FB r16, String str, Map map) {
        String str2 = r16.A00;
        C16700pc.A0E(str2, 0);
        this.A01 = str2;
        int hashCode = hashCode();
        String A04 = A04();
        AnonymousClass17P r6 = this.A07;
        r6.A00(hashCode, A04);
        r6.A02(hashCode, "config_name", A04());
        AnonymousClass17N r7 = this.A0A;
        r7.A00 = null;
        this.A05.A00(A04(), r7.A00(), str);
        this.A0C.put(AnonymousClass49S.A02.key, r7.A00());
        r7.A00();
        A04();
        C19750uc r2 = this.A09;
        String str3 = this.A0B;
        synchronized (r2) {
            C16700pc.A0E(str3, 0);
            r2.A00.put(str3, this);
        }
        EnumC869549q r3 = EnumC869549q.A01;
        r6.A02.A03(hashCode, "iqRequest");
        r6.A02(hashCode, "iq_type", "GET");
        r6.A02(hashCode, "session_id", r7.A00());
        A05(new C69193Yh(r14, r15, this, str, map, hashCode), r3, r16);
    }

    public final void A07(AnonymousClass3EI r20) {
        Map map;
        Map map2;
        Map map3;
        AnonymousClass4VP r9;
        AnonymousClass3E4 r0 = r20.A01.A05;
        if (r0 != null) {
            List<AnonymousClass4Wy> list = r0.A01.A02;
            ArrayList arrayList = new ArrayList();
            for (AnonymousClass4Wy r02 : list) {
                AbstractC115475Rr r1 = r02.A01;
                C16700pc.A0B(r1);
                LinkedHashMap linkedHashMap = null;
                linkedHashMap = null;
                String str = "$";
                if (r1 instanceof C71043cI) {
                    C71043cI r12 = (C71043cI) r1;
                    C64083Ee r5 = r12.A01;
                    String str2 = r5.A02;
                    C16700pc.A0B(str2);
                    String str3 = r12.A02;
                    C16700pc.A0B(str3);
                    String str4 = r5.A01;
                    if (str4 == null) {
                        str4 = str;
                    }
                    String str5 = r12.A04;
                    if (!(str5 == null || (map2 = (Map) C94734cS.A00(str5).A01(str, new AnonymousClass5T6[0])) == null)) {
                        C17520qw r4 = new C17520qw(AnonymousClass49S.A00.key, this.A0C);
                        if (map2.isEmpty()) {
                            Map singletonMap = Collections.singletonMap(r4.first, r4.second);
                            C16700pc.A0B(singletonMap);
                            linkedHashMap = singletonMap;
                        } else {
                            LinkedHashMap linkedHashMap2 = new LinkedHashMap(map2);
                            linkedHashMap2.put(r4.first, r4.second);
                            linkedHashMap = linkedHashMap2;
                        }
                    }
                    String str6 = r5.A06;
                    if (str6 == null) {
                        map = null;
                    } else {
                        map = (Map) C94734cS.A00(str6).A01(str, new AnonymousClass5T6[0]);
                    }
                    String str7 = r5.A05;
                    if (str7 == null) {
                        str7 = str;
                    } else if (str7.equals("")) {
                        str7 = null;
                    }
                    String str8 = r5.A04;
                    if (str8 != null) {
                        str = str8;
                    }
                    String str9 = r5.A03;
                    String str10 = r12.A03;
                    if (str10 == null) {
                        str10 = "replace";
                    }
                    r9 = new AnonymousClass46T(str2, str3, str4, str7, str, str9, str10, linkedHashMap, map);
                } else if (r1 instanceof C71033cH) {
                    C71033cH r13 = (C71033cH) r1;
                    C64083Ee r6 = r13.A01;
                    String str11 = r6.A02;
                    List<C64003Dv> list2 = r13.A03;
                    ArrayList arrayList2 = new ArrayList(C16760pi.A0D(list2));
                    for (C64003Dv r03 : list2) {
                        arrayList2.add(r03.A01);
                    }
                    ArrayList arrayList3 = new ArrayList(C16760pi.A0D(arrayList2));
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        String str12 = ((C63993Du) it.next()).A01;
                        C16700pc.A0B(str12);
                        arrayList3.add(str12);
                    }
                    String str13 = r13.A02;
                    String str14 = r6.A01;
                    if (str14 == null) {
                        str14 = str;
                    }
                    String str15 = r6.A04;
                    if (str15 != null) {
                        str = str15;
                    }
                    C16700pc.A0B(str11);
                    C16700pc.A0B(str13);
                    r9 = new AnonymousClass46R(str11, str13, str14, str, arrayList3);
                } else if (r1 instanceof C71013cF) {
                    C64083Ee r04 = ((C71013cF) r1).A01;
                    String str16 = r04.A02;
                    C16700pc.A0B(str16);
                    String str17 = r04.A01;
                    if (str17 == null) {
                        str17 = str;
                    }
                    String str18 = r04.A04;
                    if (str18 != null) {
                        str = str18;
                    }
                    r9 = new AnonymousClass46Q(str16, str17, str);
                } else if (r1 instanceof C71023cG) {
                    C71023cG r14 = (C71023cG) r1;
                    C64083Ee r62 = r14.A01;
                    String str19 = r62.A02;
                    C16700pc.A0B(str19);
                    String str20 = r62.A01;
                    if (str20 == null) {
                        str20 = str;
                    }
                    String str21 = r14.A02;
                    if (str21 == null) {
                        map3 = null;
                    } else {
                        map3 = (Map) C94734cS.A00(str21).A01(str, new AnonymousClass5T6[0]);
                    }
                    String str22 = r62.A05;
                    if (str22 == null) {
                        str22 = str;
                    } else if (str22.equals("")) {
                        str22 = null;
                    }
                    String str23 = r62.A04;
                    if (str23 != null) {
                        str = str23;
                    }
                    r9 = new AnonymousClass46S(str19, str20, str22, str, r62.A03, map3);
                }
                arrayList.add(r9 == 1 ? 1 : 0);
            }
            int A03 = C17540qy.A03(C16760pi.A0D(arrayList));
            if (A03 < 16) {
                A03 = 16;
            }
            LinkedHashMap linkedHashMap3 = new LinkedHashMap(A03);
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                Object next = it2.next();
                linkedHashMap3.put(((AnonymousClass4VP) next).A01, next);
            }
            this.A08.A00.putAll(linkedHashMap3);
            for (Map.Entry entry : linkedHashMap3.entrySet()) {
                C16700pc.A0E(entry.getValue(), 0);
            }
        }
    }
}
