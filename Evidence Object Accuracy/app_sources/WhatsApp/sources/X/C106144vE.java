package X;

import java.util.HashSet;

/* renamed from: X.4vE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106144vE implements AnonymousClass5SB {
    @Override // X.AnonymousClass5SB
    public boolean A9h(HashSet hashSet, String[] strArr) {
        for (String str : strArr) {
            if (hashSet.contains(str)) {
                return true;
            }
        }
        return false;
    }
}
