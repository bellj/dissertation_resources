package X;

import android.animation.Animator;
import android.app.Activity;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape0S0100000_I0;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.2Fc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48232Fc {
    public static final int A0A;
    public static final int A0B;
    public int A00;
    public View.OnClickListener A01 = new ViewOnClickCListenerShape0S0100000_I0(this, 13);
    public SearchView A02;
    public WaImageView A03;
    public boolean A04;
    public final Activity A05;
    public final View A06;
    public final AnonymousClass07L A07;
    public final Toolbar A08;
    public final AnonymousClass018 A09;

    static {
        int i = Build.VERSION.SDK_INT;
        int i2 = 250;
        int i3 = 220;
        if (i >= 21) {
            i3 = 250;
        }
        A0A = i3;
        if (i < 21) {
            i2 = 220;
        }
        A0B = i2;
    }

    public C48232Fc(Activity activity, View view, AnonymousClass07L r5, Toolbar toolbar, AnonymousClass018 r7) {
        this.A05 = activity;
        this.A09 = r7;
        this.A06 = view;
        this.A08 = toolbar;
        this.A07 = r5;
    }

    public static void A00(View view) {
        view.setBackgroundResource(R.drawable.search_background);
        if (Build.VERSION.SDK_INT < 21 && (view.getBackground() instanceof NinePatchDrawable)) {
            AnonymousClass2GE.A04(view.getBackground(), AnonymousClass00T.A00(view.getContext(), R.color.searchBackgroundLegacyTint));
        }
    }

    public void A01() {
        int width;
        int i;
        if (!A05()) {
            if (this.A02 == null) {
                View view = this.A06;
                A00(view);
                Activity activity = this.A05;
                activity.getLayoutInflater().inflate(R.layout.home_search_view_layout, (ViewGroup) view, true);
                SearchView searchView = (SearchView) AnonymousClass028.A0D(view, R.id.search_view);
                this.A02 = searchView;
                int i2 = R.string.search_hint;
                if (this.A04) {
                    i2 = R.string.search_hint_contactless;
                    ((ImageView) AnonymousClass028.A0D(searchView, R.id.search_close_btn)).setImageDrawable(null);
                }
                TextView textView = (TextView) AnonymousClass028.A0D(this.A02, R.id.search_src_text);
                textView.setTextColor(AnonymousClass00T.A00(activity, R.color.search_text_color));
                textView.setHintTextColor(AnonymousClass00T.A00(activity, R.color.hint_text));
                this.A02.setIconifiedByDefault(false);
                this.A02.setQueryHint(activity.getString(i2));
                SearchView searchView2 = this.A02;
                searchView2.A0B = this.A07;
                ((ImageView) searchView2.findViewById(R.id.search_mag_icon)).setImageDrawable(new C73093ff(AnonymousClass00T.A04(activity, R.drawable.ic_back), this));
                ImageView imageView = (ImageView) view.findViewById(R.id.search_back);
                SearchView searchView3 = this.A02;
                if (!(searchView3 == null || searchView3.getContext() == null)) {
                    imageView.setImageDrawable(new AnonymousClass2GF(AnonymousClass2GE.A01(this.A02.getContext(), R.drawable.ic_back, R.color.searchNavigateBackTint), this.A09));
                }
                imageView.setOnClickListener(new ViewOnClickCListenerShape0S0100000_I0(this, 12));
                if (this.A04) {
                    WaImageView waImageView = (WaImageView) AnonymousClass028.A0D(view, R.id.search_dialpad);
                    this.A03 = waImageView;
                    waImageView.setVisibility(0);
                    this.A03.setOnClickListener(this.A01);
                }
            }
            View view2 = this.A06;
            view2.setVisibility(0);
            if (Build.VERSION.SDK_INT < 21) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration((long) A0A);
                alphaAnimation.setAnimationListener(new C83183wp(this));
                view2.clearAnimation();
                view2.startAnimation(alphaAnimation);
                AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
                Toolbar toolbar = this.A08;
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) ((-toolbar.getHeight()) >> 3));
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(alphaAnimation2);
                animationSet.addAnimation(translateAnimation);
                animationSet.setDuration((long) A0B);
                toolbar.startAnimation(animationSet);
                toolbar.setVisibility(4);
            } else if (view2.isAttachedToWindow()) {
                View findViewById = this.A08.findViewById(R.id.menuitem_search);
                if (findViewById != null) {
                    int[] iArr = new int[2];
                    findViewById.getLocationInWindow(iArr);
                    if (!this.A09.A04().A06) {
                        width = (view2.getWidth() - iArr[0]) - (findViewById.getWidth() / 2);
                    } else {
                        width = iArr[0] + (findViewById.getWidth() / 2);
                    }
                } else {
                    width = view2.getWidth() / 2;
                }
                this.A00 = width;
                int max = Math.max(width, view2.getWidth() - this.A00);
                if (!this.A09.A04().A06) {
                    i = view2.getWidth() - this.A00;
                } else {
                    i = this.A00;
                }
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view2, i, view2.getHeight() / 2, 0.0f, (float) max);
                createCircularReveal.setDuration((long) A0A);
                createCircularReveal.addListener(new C72483eg(this));
                createCircularReveal.start();
            }
            if (C28391Mz.A03()) {
                C41691tw.A03(this.A05, R.color.lightStatusBarBackgroundColor);
            } else if (C28391Mz.A02()) {
                Activity activity2 = this.A05;
                activity2.getWindow().setStatusBarColor(AnonymousClass00T.A00(activity2, R.color.black_alpha_54));
            }
        }
    }

    public void A02(Bundle bundle) {
        CharSequence charSequence;
        if (bundle != null && (charSequence = bundle.getCharSequence("search_text")) != null) {
            A01();
            this.A00 = bundle.getInt("search_button_x_pos");
            this.A02.A0F(charSequence);
        }
    }

    public void A03(Bundle bundle) {
        if (this.A02 != null && A05()) {
            bundle.putCharSequence("search_text", this.A02.A0k.getText());
            bundle.putInt("search_button_x_pos", this.A00);
        }
    }

    public void A04(boolean z) {
        int i;
        if (A05()) {
            this.A02.A0F("");
            Toolbar toolbar = this.A08;
            toolbar.setVisibility(0);
            if (!z) {
                this.A02.setIconified(true);
                this.A06.setVisibility(4);
            } else if (Build.VERSION.SDK_INT >= 21) {
                int i2 = this.A00;
                View view = this.A06;
                int width = view.getWidth();
                int i3 = this.A00;
                int max = Math.max(i2, width - i3);
                if (i3 == 0) {
                    this.A00 = view.getWidth() >> 1;
                }
                if (!this.A09.A04().A06) {
                    i = view.getWidth() - this.A00;
                } else {
                    i = this.A00;
                }
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, i, view.getHeight() >> 1, (float) max, 0.0f);
                createCircularReveal.setDuration((long) A0B);
                createCircularReveal.addListener(new C72493eh(this));
                createCircularReveal.start();
            } else {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                long j = (long) A0B;
                alphaAnimation.setDuration(j);
                alphaAnimation.setAnimationListener(new C83193wq(this));
                this.A06.startAnimation(alphaAnimation);
                AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) ((-toolbar.getHeight()) / 4), 0.0f);
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(alphaAnimation2);
                animationSet.addAnimation(translateAnimation);
                animationSet.setDuration(j);
                toolbar.startAnimation(animationSet);
            }
            Activity activity = this.A05;
            C41691tw.A07(activity.getWindow(), false);
            C41691tw.A02(activity, R.color.primary);
        }
    }

    public boolean A05() {
        return this.A06.getVisibility() == 0;
    }
}
