package X;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import java.util.HashMap;

/* renamed from: X.2JO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2JO extends AnonymousClass2JJ {
    public static final String[] A00 = {"_id", "_data", "datetaken", "title", "mini_thumb_magic", "mime_type", "date_modified", "_size"};

    public AnonymousClass2JO(Uri uri, C16590pI r2, C19390u2 r3, String str, int i) {
        super(uri, r2, r3, str, i);
    }

    @Override // X.AbstractC35581iK
    public HashMap AAz() {
        String str;
        Uri build = this.A04.buildUpon().appendQueryParameter("distinct", "true").build();
        ContentResolver contentResolver = this.A03;
        String[] strArr = {"bucket_display_name", "bucket_id"};
        String str2 = this.A07;
        if (str2 != null) {
            StringBuilder sb = new StringBuilder("bucket_id = '");
            sb.append(str2);
            sb.append("'");
            str = sb.toString();
        } else {
            str = null;
        }
        Cursor query = MediaStore.Images.Media.query(contentResolver, build, strArr, str, null, A02());
        try {
            HashMap hashMap = new HashMap();
            if (query != null) {
                while (query.moveToNext()) {
                    hashMap.put(query.getString(1), query.getString(0));
                }
            }
            if (query != null) {
                query.close();
            }
            return hashMap;
        } catch (Throwable th) {
            if (query != null) {
                try {
                    query.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
    }
}
