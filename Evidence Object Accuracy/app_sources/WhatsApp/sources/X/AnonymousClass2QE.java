package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.2QE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QE {
    public C15580nU A00;
    public UserJid A01;
    public C32511cH A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Integer A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public List A0E;
}
