package X;

import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Locale;

/* renamed from: X.2AW  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2AW extends AbstractC33251dh {
    public AnonymousClass2AX A00;
    public final C15570nT A01;
    public final C15820nx A02;
    public final C19490uC A03;
    public final C16600pJ A04;

    public AnonymousClass2AW(C15570nT r7, AbstractC33221de r8, C33241dg r9, C15820nx r10, C17050qB r11, C19490uC r12, C21780xy r13, C16600pJ r14, AnonymousClass15D r15) {
        super(r8, r9, r11, r13, r15);
        this.A01 = r7;
        this.A03 = r12;
        this.A02 = r10;
        this.A04 = r14;
    }

    public final C47352Ah A06() {
        int i;
        C47352Ah r0;
        AbstractC33221de r5 = super.A00;
        long AKR = r5.AKR();
        boolean z = this instanceof C58762rL;
        if (z) {
            i = 16;
        } else if (!(((C58752rK) this) instanceof C58732rI)) {
            i = 20;
        } else {
            i = 0;
        }
        long j = AKR - ((long) i);
        InputStream ADW = r5.ADW();
        if (j >= 0) {
            while (j > 0) {
                try {
                    long skip = ADW.skip(j);
                    if (skip <= 0) {
                        if (ADW.read() == -1) {
                            break;
                        }
                        j--;
                    } else {
                        j -= skip;
                    }
                } catch (Throwable th) {
                    try {
                        ADW.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
        if (!z) {
            if (!(((C58752rK) this) instanceof C58732rI)) {
                byte[] bArr = new byte[16];
                byte[] bArr2 = new byte[4];
                int read = ADW.read(bArr);
                int read2 = ADW.read(bArr2);
                if (read == 16 && read2 == 4) {
                    r0 = new C47352Ah(bArr, bArr2);
                } else {
                    Log.e("Backup/BackupFileCrypt12/footer is null");
                }
            }
            r0 = null;
        } else {
            byte[] bArr3 = new byte[16];
            if (ADW.read(bArr3) == 16) {
                r0 = new C47352Ah(bArr3, null);
            } else {
                Log.e("backup/cannot read footer, footer is null");
                r0 = null;
            }
        }
        ADW.close();
        return r0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0147  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass2AX A07(java.io.InputStream r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 374
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2AW.A07(java.io.InputStream, boolean):X.2AX");
    }

    public EnumC16570pG A08() {
        if (!(this instanceof C58762rL)) {
            return EnumC16570pG.A04;
        }
        if (!(((C58762rL) this) instanceof C58742rJ)) {
            return EnumC16570pG.A06;
        }
        return EnumC16570pG.A07;
    }

    public InputStream A09() {
        int i;
        AbstractC33221de r1 = super.A00;
        BufferedInputStream bufferedInputStream = new BufferedInputStream(r1.ADW());
        long AKR = r1.AKR();
        if (this instanceof C58762rL) {
            i = 16;
        } else if (!(((C58752rK) this) instanceof C58732rI)) {
            i = 20;
        } else {
            i = 0;
        }
        long j = AKR - ((long) i);
        String.format(Locale.ENGLISH, "BackupFile/get-input-stream size-without-footer:%d footer-size:%d", Long.valueOf(j), Integer.valueOf(i));
        return new C37601mh(bufferedInputStream, j);
    }

    public final String A0A() {
        String str;
        C15570nT r2 = this.A01;
        r2.A08();
        if (r2.A00 == null) {
            str = "backup/BackupFileCrypt12/getUserJid MeManager.me is null";
        } else {
            r2.A08();
            C27631Ih r0 = r2.A05;
            if (r0 != null) {
                return r0.user;
            }
            str = "backup/BackupFileCrypt12/getUserJid MeManager.getMyJidObject() is null";
        }
        Log.e(str);
        return null;
    }
}
