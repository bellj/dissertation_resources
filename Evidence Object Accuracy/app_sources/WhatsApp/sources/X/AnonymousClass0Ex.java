package X;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0Ex  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Ex extends AnonymousClass0FE {
    public final /* synthetic */ AnonymousClass0FB A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0Ex(Context context, AnonymousClass0FB r2) {
        super(context);
        this.A00 = r2;
    }

    @Override // X.AnonymousClass0FE, X.AbstractC05520Pw
    public void A03(View view, C05160Om r8, C05480Ps r9) {
        AnonymousClass0FB r1 = this.A00;
        RecyclerView recyclerView = r1.A01;
        if (recyclerView != null) {
            int[] A04 = r1.A04(view, recyclerView.getLayoutManager());
            int i = A04[0];
            int i2 = A04[1];
            int ceil = (int) Math.ceil(((double) A07(Math.max(Math.abs(i), Math.abs(i2)))) / 0.3356d);
            if (ceil > 0) {
                DecelerateInterpolator decelerateInterpolator = ((AnonymousClass0FE) this).A04;
                r8.A02 = i;
                r8.A03 = i2;
                r8.A01 = ceil;
                r8.A05 = decelerateInterpolator;
                r8.A06 = true;
            }
        }
    }

    @Override // X.AnonymousClass0FE
    public float A04(DisplayMetrics displayMetrics) {
        return 100.0f / ((float) displayMetrics.densityDpi);
    }
}
