package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.2uW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59332uW extends AbstractC75723kJ {
    public final WaTextView A00;
    public final WaTextView A01;

    public C59332uW(View view) {
        super(view);
        this.A01 = C12960it.A0N(view, R.id.postcode_item_text);
        this.A00 = C12960it.A0N(view, R.id.postcode_item_location_name);
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r6) {
        C84673zh r62 = (C84673zh) r6;
        this.A01.setText(C12990iw.A0o(C12960it.A09(this.A0H), r62.A01, C12970iu.A1b(), 0, R.string.catalog_product_available_in_location));
        this.A00.setText(r62.A00);
    }
}
