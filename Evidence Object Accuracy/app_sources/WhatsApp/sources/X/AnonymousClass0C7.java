package X;

import android.graphics.drawable.Animatable;

/* renamed from: X.0C7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0C7 extends AbstractC05490Pt {
    public final Animatable A00;

    public AnonymousClass0C7(Animatable animatable) {
        this.A00 = animatable;
    }

    @Override // X.AbstractC05490Pt
    public void A01() {
        this.A00.start();
    }

    @Override // X.AbstractC05490Pt
    public void A02() {
        this.A00.stop();
    }
}
