package X;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

/* renamed from: X.2kj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56342kj extends AbstractC77873o0 {
    public final AnonymousClass5QZ A00;
    public final AbstractC94024b8 A01;
    public final C13690kA A02;

    public C56342kj(AnonymousClass5QZ r2, AbstractC94024b8 r3, C13690kA r4, int i) {
        super(i);
        this.A02 = r4;
        this.A01 = r3;
        this.A00 = r2;
        if (i == 2 && r3.A01) {
            throw C12970iu.A0f("Best-effort write calls cannot pass methods that should auto-resolve missing features.");
        }
    }

    @Override // X.AnonymousClass3HY
    public final void A01(Status status) {
        Exception r0;
        C13690kA r1 = this.A02;
        if (status.A02 != null) {
            r0 = new C77703ni(status);
        } else {
            r0 = new C630439z(status);
        }
        r1.A00(r0);
    }

    @Override // X.AnonymousClass3HY
    public final void A02(AnonymousClass3CU r6, boolean z) {
        C13690kA r2 = this.A02;
        r6.A01.put(r2, Boolean.valueOf(z));
        C13600jz r4 = r2.A00;
        r4.A03.A00(new AnonymousClass514(new C1091850s(r6, r2), C13650k5.A00));
        r4.A04();
    }

    @Override // X.AnonymousClass3HY
    public final void A03(C14970mL r4) {
        try {
            this.A01.A00(r4.A04, this.A02);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            A01(AnonymousClass3HY.A00(e2));
        } catch (RuntimeException e3) {
            this.A02.A00(e3);
        }
    }

    @Override // X.AnonymousClass3HY
    public final void A04(Exception exc) {
        this.A02.A00(exc);
    }

    @Override // X.AbstractC77873o0
    public final boolean A05(C14970mL r2) {
        return this.A01.A01;
    }

    @Override // X.AbstractC77873o0
    public final C78603pB[] A06(C14970mL r2) {
        return this.A01.A02;
    }
}
