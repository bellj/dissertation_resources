package X;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/* renamed from: X.0XU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XU implements AnonymousClass02O {
    @Override // X.AnonymousClass02O
    public Object apply(Object obj) {
        C006503b r7;
        List<AnonymousClass0PO> list = (List) obj;
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (AnonymousClass0PO r2 : list) {
            List list2 = r2.A04;
            if (list2 == null || list2.isEmpty()) {
                r7 = C006503b.A01;
            } else {
                r7 = (C006503b) r2.A04.get(0);
            }
            UUID fromString = UUID.fromString(r2.A03);
            arrayList.add(new C05450Pp(r2.A01, r7, r2.A02, r2.A05, fromString, r2.A00));
        }
        return arrayList;
    }
}
