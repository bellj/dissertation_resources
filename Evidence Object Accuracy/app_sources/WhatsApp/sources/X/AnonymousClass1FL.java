package X;

import android.content.SharedPreferences;
import java.util.Collections;

/* renamed from: X.1FL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FL {
    public SharedPreferences A00;
    public final C16630pM A01;

    public AnonymousClass1FL(C16630pM r1) {
        this.A01 = r1;
    }

    public void A00(C451220g r4, String str) {
        SharedPreferences sharedPreferences;
        synchronized (this) {
            sharedPreferences = this.A00;
            if (sharedPreferences == null) {
                sharedPreferences = this.A01.A01("com.whatsapp_payment_sync_preferences");
                this.A00 = sharedPreferences;
            }
        }
        sharedPreferences.edit().putString(r4.A01.A00(), str).apply();
        for (C451220g r0 : Collections.unmodifiableList(r4.A00)) {
            A00(r0, str);
        }
    }
}
