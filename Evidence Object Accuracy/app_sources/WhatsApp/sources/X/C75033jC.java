package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.picker.search.StickerSearchDialogFragment;

/* renamed from: X.3jC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75033jC extends AbstractC05270Ox {
    public final /* synthetic */ StickerSearchDialogFragment A00;

    public C75033jC(StickerSearchDialogFragment stickerSearchDialogFragment) {
        this.A00 = stickerSearchDialogFragment;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        if (i2 != 0) {
            this.A00.A05.A03();
        }
    }
}
