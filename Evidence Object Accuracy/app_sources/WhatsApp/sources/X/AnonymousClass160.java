package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.util.Log;

/* renamed from: X.160  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass160 {
    public final C20950wa A00;
    public final ExecutorC27271Gr A01;

    public AnonymousClass160(C20950wa r3, AbstractC14440lR r4) {
        this.A00 = r3;
        this.A01 = new ExecutorC27271Gr(r4, true);
    }

    public void A00(AbstractC15340mz r3) {
        C16460p3 A0G;
        if (AnonymousClass01I.A01()) {
            Log.w("thumbs are loaded on ui thread", new Throwable());
        }
        if (r3.A0G() != null) {
            A01(r3.A0G());
        }
        AbstractC15340mz A0E = r3.A0E();
        if (A0E != null && (A0G = A0E.A0G()) != null && !A0G.A05()) {
            A0G.A01(A0G.A06());
        }
    }

    public void A01(C16460p3 r3) {
        if (!r3.A05()) {
            byte[] A06 = r3.A06();
            if (A06 == null) {
                A06 = this.A00.A09(r3.A04);
            }
            r3.A01(A06);
        }
    }

    public void A02(C16460p3 r4, Runnable runnable) {
        if (r4.A05()) {
            runnable.run();
        } else {
            this.A01.execute(new RunnableBRunnable0Shape1S0300000_I0_1(this, r4, runnable, 35));
        }
    }

    public boolean A03(AbstractC15340mz r2) {
        if (r2 != null) {
            return (r2.A0G() != null && !r2.A0G().A05()) || A03(r2.A0E());
        }
        return false;
    }
}
