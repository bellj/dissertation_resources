package X;

import android.app.UiModeManager;
import android.content.Context;
import android.os.Handler;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.3mU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C76943mU extends AbstractC76533ln implements AbstractC116615Wd {
    public int A00;
    public long A01;
    public C100614mC A02;
    public AnonymousClass5Py A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final Context A08;
    public final C92474Wb A09;
    public final AnonymousClass5XY A0A;

    @Override // X.AbstractC117055Yb, X.AnonymousClass5WX
    public String getName() {
        return "MediaCodecAudioRenderer";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C76943mU(Context context, Handler handler, AnonymousClass3IX r7, AbstractC72373eU r8, AbstractC117015Xu r9, AnonymousClass5Xx... r10) {
        super(AbstractC116825Xa.A00, r9, 44100.0f, 1);
        C106534vr r3 = new C106534vr(r7, new C106554vt(r10));
        this.A08 = context.getApplicationContext();
        this.A0A = r3;
        this.A09 = new C92474Wb(handler, r8);
        r3.A0G = new C106524vq(this);
    }

    public C76943mU(Context context, Handler handler, AbstractC72373eU r6, AnonymousClass5XY r7, AbstractC117015Xu r8) {
        super(AbstractC116825Xa.A00, r8, 44100.0f, 1);
        this.A08 = context.getApplicationContext();
        this.A0A = r7;
        this.A09 = new C92474Wb(handler, r6);
        ((C106534vr) r7).A0G = new C106524vq(this);
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A07() {
        try {
            super.A07();
        } finally {
            if (this.A06) {
                this.A06 = false;
                this.A0A.reset();
            }
        }
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A08() {
        this.A06 = true;
        try {
            this.A0A.flush();
            try {
                super.A08();
                this.A09.A00(this.A0L);
            } catch (Throwable th) {
                this.A09.A00(this.A0L);
                throw th;
            }
        } catch (Throwable th2) {
            try {
                super.A08();
                this.A09.A00(this.A0L);
                throw th2;
            } catch (Throwable th3) {
                this.A09.A00(this.A0L);
                throw th3;
            }
        }
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A09(long j, boolean z) {
        super.A09(j, z);
        this.A0A.flush();
        this.A01 = j;
        this.A04 = true;
        this.A05 = true;
    }

    @Override // X.AbstractC76533ln, X.AbstractC106444vi
    public void A0A(boolean z, boolean z2) {
        super.A0A(z, z2);
        C92474Wb r3 = this.A09;
        AnonymousClass4U3 r2 = this.A0L;
        Handler handler = r3.A00;
        if (handler != null) {
            C12980iv.A18(handler, r3, r2, 4);
        }
        boolean z3 = ((AbstractC106444vi) this).A04.A00;
        C106534vr r32 = (C106534vr) this.A0A;
        if (z3) {
            C95314dV.A04(C12990iw.A1X(AnonymousClass3JZ.A01, 21));
            C95314dV.A04(r32.A0Q);
            if (!r32.A0X) {
                r32.A0X = true;
            } else {
                return;
            }
        } else if (r32.A0X) {
            r32.A0X = false;
        } else {
            return;
        }
        r32.flush();
    }

    @Override // X.AbstractC76533ln
    public AnonymousClass4XN A0C(C89864Lr r7) {
        AnonymousClass4XN A0C = super.A0C(r7);
        C92474Wb r4 = this.A09;
        C100614mC r3 = r7.A00;
        Handler handler = r4.A00;
        if (handler != null) {
            handler.post(new RunnableBRunnable0Shape3S0300000_I1(r4, r3, A0C, 1));
        }
        return A0C;
    }

    public final int A0W(C100614mC r3, C95494dp r4) {
        int i;
        UiModeManager uiModeManager;
        if (!"OMX.google.raw.decoder".equals(r4.A03) || (i = AnonymousClass3JZ.A01) >= 24 || (i == 23 && (uiModeManager = (UiModeManager) this.A08.getApplicationContext().getSystemService("uimode")) != null && uiModeManager.getCurrentModeType() == 4)) {
            return r3.A0A;
        }
        return -1;
    }

    public final void A0X() {
        long ACA = this.A0A.ACA(AJN());
        if (ACA != Long.MIN_VALUE) {
            if (!this.A05) {
                ACA = Math.max(this.A01, ACA);
            }
            this.A01 = ACA;
            this.A05 = false;
        }
    }

    @Override // X.AbstractC116615Wd
    public C94344be AFk() {
        return ((C106534vr) this.A0A).A08().A02;
    }

    @Override // X.AbstractC116615Wd
    public long AFr() {
        if (((AbstractC106444vi) this).A01 == 2) {
            A0X();
        }
        return this.A01;
    }

    @Override // X.AbstractC76533ln, X.AbstractC117055Yb
    public boolean AJx() {
        return this.A0A.AIH() || super.AJx();
    }

    @Override // X.AbstractC116615Wd
    public void AcX(C94344be r2) {
        this.A0A.AcX(r2);
    }
}
