package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import com.whatsapp.R;

/* renamed from: X.4o1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101744o1 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ScrollView A01;

    public ViewTreeObserver$OnGlobalLayoutListenerC101744o1(View view, ScrollView scrollView) {
        this.A01 = scrollView;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        float f;
        ScrollView scrollView = this.A01;
        boolean A01 = C93004Yo.A01(scrollView);
        View view = this.A00;
        if (A01) {
            f = view.getResources().getDimension(R.dimen.space_loose);
        } else {
            f = 0.0f;
        }
        view.setElevation(f);
        C12980iv.A1F(scrollView, this);
    }
}
