package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.R;
import java.util.Locale;
import org.chromium.net.UrlRequest;

/* renamed from: X.3Iz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65303Iz {
    public static int A00(AnonymousClass1JU r3) {
        String str;
        AnonymousClass1YI r2 = r3.A06;
        String str2 = r3.A07;
        if (str2 != null) {
            str = str2.toLowerCase(Locale.US);
        } else {
            str = null;
        }
        switch (r2.ordinal()) {
            case 1:
                return R.drawable.device_list_ic_chrome;
            case 2:
                return R.drawable.device_list_ic_firefox;
            case 3:
                return R.drawable.device_list_ic_ie;
            case 4:
                return R.drawable.device_list_ic_opera;
            case 5:
                return R.drawable.device_list_ic_safari;
            case 6:
                return R.drawable.device_list_ic_edge;
            case 7:
                if ("windows".equals(str)) {
                    return R.drawable.device_list_ic_windows;
                }
                if ("mac os".equals(str)) {
                    return R.drawable.device_list_ic_mac;
                }
                return R.drawable.device_list_ic_desktop_fallback;
            case 8:
            case 9:
                return R.drawable.device_list_ic_unknown_device;
            case 10:
            case 11:
            case 12:
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return R.drawable.device_list_ic_portal;
            default:
                return R.drawable.device_list_ic_unknown_browser;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(X.C14880mC r4) {
        /*
            boolean r0 = r4.A0C
            if (r0 == 0) goto L_0x0008
            r0 = 2131231369(0x7f080289, float:1.8078817E38)
            return r0
        L_0x0008:
            r3 = 2131231362(0x7f080282, float:1.8078803E38)
            java.lang.String r1 = r4.A08
            if (r1 == 0) goto L_0x0089
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r2 = r1.toLowerCase(r0)
        L_0x0015:
            java.lang.String r1 = r4.A07
            if (r1 == 0) goto L_0x0029
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r1.toLowerCase(r0)
            int r0 = r1.hashCode()
            switch(r0) {
                case -1361128838: goto L_0x002a;
                case -909897856: goto L_0x0031;
                case -849452327: goto L_0x003b;
                case 3356: goto L_0x0045;
                case 3108285: goto L_0x004f;
                case 105948115: goto L_0x0059;
                case 1557106716: goto L_0x0065;
                default: goto L_0x0026;
            }
        L_0x0026:
            r3 = 2131231371(0x7f08028b, float:1.8078821E38)
        L_0x0029:
            return r3
        L_0x002a:
            java.lang.String r0 = "chrome"
            boolean r0 = r1.equals(r0)
            goto L_0x0062
        L_0x0031:
            java.lang.String r0 = "safari"
            boolean r0 = r1.equals(r0)
            r3 = 2131231370(0x7f08028a, float:1.807882E38)
            goto L_0x0062
        L_0x003b:
            java.lang.String r0 = "firefox"
            boolean r0 = r1.equals(r0)
            r3 = 2131231365(0x7f080285, float:1.8078809E38)
            goto L_0x0062
        L_0x0045:
            java.lang.String r0 = "ie"
            boolean r0 = r1.equals(r0)
            r3 = 2131231366(0x7f080286, float:1.807881E38)
            goto L_0x0062
        L_0x004f:
            java.lang.String r0 = "edge"
            boolean r0 = r1.equals(r0)
            r3 = 2131231364(0x7f080284, float:1.8078807E38)
            goto L_0x0062
        L_0x0059:
            java.lang.String r0 = "opera"
            boolean r0 = r1.equals(r0)
            r3 = 2131231368(0x7f080288, float:1.8078815E38)
        L_0x0062:
            if (r0 != 0) goto L_0x0029
            goto L_0x0026
        L_0x0065:
            java.lang.String r0 = "desktop"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = "windows"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x007a
            r3 = 2131231373(0x7f08028d, float:1.8078825E38)
            return r3
        L_0x007a:
            java.lang.String r0 = "mac os"
            boolean r0 = r0.equals(r2)
            r3 = 2131231363(0x7f080283, float:1.8078805E38)
            if (r0 == 0) goto L_0x0029
            r3 = 2131231367(0x7f080287, float:1.8078813E38)
            return r3
        L_0x0089:
            r2 = 0
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C65303Iz.A01(X.0mC):int");
    }

    public static Intent A02(Context context) {
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(context.getPackageName(), "com.whatsapp.companiondevice.LinkedDevicesActivity");
        return A0A;
    }

    public static String A03(Context context, C14880mC r5) {
        int i;
        String str = r5.A07;
        if (str != null) {
            String lowerCase = str.toLowerCase(Locale.US);
            switch (lowerCase.hashCode()) {
                case -1361128838:
                    if (lowerCase.equals("chrome")) {
                        i = R.string.linked_device_name_platform_chrome;
                        return C12960it.A0X(context, r5.A08, new Object[1], 0, i);
                    }
                    break;
                case -909897856:
                    if (lowerCase.equals("safari")) {
                        i = R.string.linked_device_name_platform_safari;
                        return C12960it.A0X(context, r5.A08, new Object[1], 0, i);
                    }
                    break;
                case -849452327:
                    if (lowerCase.equals("firefox")) {
                        i = R.string.linked_device_name_platform_firefox;
                        return C12960it.A0X(context, r5.A08, new Object[1], 0, i);
                    }
                    break;
                case 3356:
                    if (lowerCase.equals("ie")) {
                        i = R.string.linked_device_name_platform_ie;
                        return C12960it.A0X(context, r5.A08, new Object[1], 0, i);
                    }
                    break;
                case 3108285:
                    if (lowerCase.equals("edge")) {
                        i = R.string.linked_device_name_platform_edge;
                        return C12960it.A0X(context, r5.A08, new Object[1], 0, i);
                    }
                    break;
                case 105948115:
                    if (lowerCase.equals("opera")) {
                        i = R.string.linked_device_name_platform_opera;
                        return C12960it.A0X(context, r5.A08, new Object[1], 0, i);
                    }
                    break;
            }
        }
        return r5.A08;
    }

    public static boolean A04(C15450nH r1) {
        return r1.A05(AbstractC15460nI.A0d) && r1.A05(AbstractC15460nI.A0c);
    }
}
