package X;

/* renamed from: X.2GJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GJ {
    public final C14330lG A00;
    public final C14900mE A01;
    public final C15450nH A02;
    public final C18720su A03;
    public final AnonymousClass1AX A04;
    public final AnonymousClass01d A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final C15890o4 A08;
    public final C14820m6 A09;
    public final AnonymousClass018 A0A;
    public final C14850m9 A0B;
    public final AnonymousClass2FO A0C;
    public final C16630pM A0D;
    public final C15690nk A0E;
    public final AbstractC14440lR A0F;
    public final C21260x8 A0G;
    public final C21280xA A0H;

    public AnonymousClass2GJ(C14330lG r2, C14900mE r3, C15450nH r4, C18720su r5, AnonymousClass1AX r6, AnonymousClass01d r7, C14830m7 r8, C16590pI r9, C15890o4 r10, C14820m6 r11, AnonymousClass018 r12, C14850m9 r13, AnonymousClass2FO r14, C16630pM r15, C15690nk r16, AbstractC14440lR r17, C21260x8 r18, C21280xA r19) {
        this.A07 = r9;
        this.A06 = r8;
        this.A03 = r5;
        this.A0B = r13;
        this.A01 = r3;
        this.A0F = r17;
        this.A00 = r2;
        this.A02 = r4;
        this.A0C = r14;
        this.A0G = r18;
        this.A0H = r19;
        this.A05 = r7;
        this.A0A = r12;
        this.A08 = r10;
        this.A09 = r11;
        this.A0E = r16;
        this.A04 = r6;
        this.A0D = r15;
    }

    public AnonymousClass1s8 A00(AnonymousClass01E r40, AbstractC49682Lt r41, AnonymousClass2JY r42) {
        C16590pI r0 = this.A07;
        C14830m7 r02 = this.A06;
        C18720su r03 = this.A03;
        C14850m9 r15 = this.A0B;
        C14900mE r13 = this.A01;
        AbstractC14440lR r12 = this.A0F;
        C14330lG r11 = this.A00;
        C15450nH r10 = this.A02;
        AnonymousClass2FO r9 = this.A0C;
        C21260x8 r8 = this.A0G;
        C21280xA r7 = this.A0H;
        AnonymousClass01d r6 = this.A05;
        AnonymousClass018 r5 = this.A0A;
        return new AnonymousClass1s8(r40, r11, r13, r10, r03, r41, this.A04, this, r6, r02, r0, this.A08, this.A09, r5, r15, r9, this.A0D, r42, this.A0E, r12, r8, r7);
    }
}
