package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.2ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52442ao extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(60);
    public final String A00;
    public final String A01;

    public /* synthetic */ C52442ao(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }

    public /* synthetic */ C52442ao(Parcelable parcelable, String str, String str2) {
        super(parcelable);
        if (!TextUtils.isEmpty(str2)) {
            this.A01 = str;
            this.A00 = str2;
            return;
        }
        this.A01 = null;
        this.A00 = null;
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }
}
