package X;

import com.whatsapp.payments.ui.NoviAmountEntryActivity;

/* renamed from: X.5bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118385bj extends AnonymousClass0Yo {
    public final /* synthetic */ NoviAmountEntryActivity A00;
    public final /* synthetic */ C128375w0 A01;

    public C118385bj(NoviAmountEntryActivity noviAmountEntryActivity, C128375w0 r2) {
        this.A01 = r2;
        this.A00 = noviAmountEntryActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123465nC.class)) {
            C128375w0 r3 = this.A01;
            C16590pI r4 = r3.A0B;
            C129675y7 r13 = r3.A0p;
            AnonymousClass018 r5 = r3.A0C;
            AnonymousClass60Y r8 = r3.A0c;
            C17070qD r6 = r3.A0V;
            AnonymousClass61F r9 = r3.A0d;
            C129685y8 r11 = r3.A0l;
            return new C123465nC(r4, r5, r6, new C129865yQ(r3.A01, this.A00, r3.A0S), r8, r9, r3.A0h, r11, r3.A0n, r13);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviWithdrawAmountEntryViewModel");
    }
}
