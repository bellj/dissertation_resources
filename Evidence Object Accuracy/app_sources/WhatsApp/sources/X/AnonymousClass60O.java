package X;

import android.text.TextUtils;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.60O  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60O {
    public long A00;
    public AnonymousClass20C A01;
    public AnonymousClass1ZR A02;
    @Deprecated
    public AnonymousClass1ZR A03;
    public AnonymousClass1ZR A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;

    public AnonymousClass60O(AnonymousClass102 r8, AnonymousClass1V8 r9) {
        AnonymousClass1V8 A0E = r9.A0E("amount");
        if (A0E == null) {
            String A0W = C117295Zj.A0W(r9, "amount");
            if (A0W != null) {
                this.A03 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A0W, "moneyStringValue");
            }
        } else {
            AnonymousClass1V8 A0E2 = A0E.A0E("money");
            if (A0E2 != null) {
                try {
                    AbstractC30791Yv A02 = r8.A02(C117295Zj.A0W(A0E2, "currency"));
                    C94074bD r2 = new C94074bD();
                    r2.A02 = A0E2.A07("value");
                    r2.A01 = A0E2.A04("offset");
                    r2.A03 = A02;
                    AnonymousClass20C A00 = r2.A00();
                    this.A01 = A00;
                    this.A03 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A00.A02.toString(), "moneyStringValue");
                } catch (Exception unused) {
                    Log.e("PAY: IndiaUpiMandateMetadata - and error occured while parsing the money node");
                }
            }
        }
        String A0I = r9.A0I("amount-rule", null);
        if (!TextUtils.isEmpty(A0I)) {
            this.A07 = A0I;
        }
        String A0I2 = r9.A0I("is-revocable", null);
        if (A0I2 != null) {
            this.A06 = A0I2;
        }
        String A0I3 = r9.A0I("end-ts", null);
        if (A0I3 != null) {
            this.A00 = C28421Nd.A01(A0I3, 0) * 1000;
        }
        String A0I4 = r9.A0I("seq-no", null);
        if (A0I4 != null) {
            this.A04 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A0I4, "upiSequenceNumber");
        }
        String A0I5 = r9.A0I("error-code", null);
        if (A0I5 != null) {
            this.A05 = A0I5;
        }
        String A0I6 = r9.A0I("mandate-update-info", null);
        if (A0I6 != null) {
            this.A02 = C117305Zk.A0I(C117305Zk.A0J(), String.class, A0I6, "upiMandateUpdateInfo");
        }
        String A0I7 = r9.A0I("status", null);
        this.A09 = A0I7 == null ? "INIT" : A0I7;
        String A0I8 = r9.A0I("action", null);
        this.A08 = A0I8 == null ? "UNKNOWN" : A0I8;
    }

    public AnonymousClass60O(String str) {
        Object obj;
        Object obj2;
        Object obj3;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject A05 = C13000ix.A05(str);
                AnonymousClass2SM A0J = C117305Zk.A0J();
                AnonymousClass1ZR r0 = this.A03;
                if (r0 == null) {
                    obj = null;
                } else {
                    obj = r0.A00;
                }
                this.A03 = C117305Zk.A0I(A0J, String.class, A05.optString("pendingAmount", (String) obj), "moneyStringValue");
                if (A05.optJSONObject("pendingMoney") != null) {
                    this.A01 = new C94074bD(A05.optJSONObject("pendingMoney")).A00();
                }
                this.A06 = A05.optString("isRevocable", this.A06);
                this.A00 = A05.optLong("mandateEndTs", this.A00);
                this.A07 = A05.optString("mandateAmountRule", this.A07);
                AnonymousClass2SM A0J2 = C117305Zk.A0J();
                AnonymousClass1ZR r02 = this.A04;
                if (r02 == null) {
                    obj2 = null;
                } else {
                    obj2 = r02.A00;
                }
                this.A04 = C117305Zk.A0I(A0J2, String.class, A05.optString("seqNum", (String) obj2), "upiMandateUpdateInfo");
                this.A05 = A05.optString("errorCode", this.A05);
                this.A09 = A05.optString("mandateUpdateStatus", this.A09);
                this.A08 = A05.optString("mandateUpdateAction", this.A08);
                AnonymousClass2SM A0J3 = C117305Zk.A0J();
                AnonymousClass1ZR r03 = this.A02;
                if (r03 == null) {
                    obj3 = null;
                } else {
                    obj3 = r03.A00;
                }
                this.A02 = C117305Zk.A0I(A0J3, String.class, A05.optString("mandateUpdateInfo", (String) obj3), "upiMandateUpdateInfo");
            } catch (JSONException e) {
                Log.w("PAY: IndiaUpiTransactionPendingUpdateMetadata threw: ", e);
            }
        }
    }

    public C30821Yy A00() {
        AnonymousClass1ZR r1 = this.A03;
        if (AnonymousClass1ZS.A03(r1)) {
            return null;
        }
        return C117305Zk.A0E(C30771Yt.A05, (String) r1.A00);
    }

    public boolean A01() {
        String str;
        String str2;
        String str3 = this.A08;
        if (!str3.equals("UNKNOWN")) {
            if (str3.equals("ACCEPT")) {
                str = this.A09;
                str2 = "PENDING";
            }
        } else {
            str = this.A09;
            str2 = "INIT";
        }
        return str.equals(str2);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("[ pendingAmount: ");
        AnonymousClass1ZR r2 = this.A03;
        if (C12970iu.A0s(r2, A0k) == null) {
            return "";
        }
        StringBuilder A0h = C12960it.A0h();
        C1309060l.A03(A0h, r2.toString());
        A0h.append(" errorCode: ");
        A0h.append(this.A05);
        A0h.append(" seqNum: ");
        A0h.append(this.A04);
        A0h.append(" mandateUpdateInfo: ");
        A0h.append(this.A02);
        A0h.append(" mandateUpdateAction: ");
        A0h.append(this.A08);
        A0h.append(" mandateUpdateStatus: ");
        A0h.append(this.A09);
        return C12960it.A0d("]", A0h);
    }
}
