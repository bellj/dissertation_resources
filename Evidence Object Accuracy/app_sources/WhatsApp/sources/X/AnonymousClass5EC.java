package X;

import javax.crypto.SecretKey;

/* renamed from: X.5EC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5EC implements SecretKey {
    public final AnonymousClass5WQ converter;
    public final char[] password;

    public AnonymousClass5EC(AnonymousClass5WQ r2, char[] cArr) {
        this.password = AnonymousClass1TT.A03(cArr);
        this.converter = r2;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        return "PBKDF2";
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        return this.converter.A7j(this.password);
    }

    @Override // java.security.Key
    public String getFormat() {
        return this.converter.AHM();
    }
}
