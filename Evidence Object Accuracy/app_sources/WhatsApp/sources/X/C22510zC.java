package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;

/* renamed from: X.0zC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22510zC {
    public final C22640zP A00;
    public final C14830m7 A01;
    public final C16590pI A02;
    public final C18360sK A03;
    public final C20650w6 A04;
    public final C19990v2 A05;
    public final C22630zO A06;
    public final C15860o1 A07;

    public C22510zC(C22640zP r1, C14830m7 r2, C16590pI r3, C18360sK r4, C20650w6 r5, C19990v2 r6, C22630zO r7, C15860o1 r8) {
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A04 = r5;
        this.A07 = r8;
        this.A06 = r7;
        this.A00 = r1;
        this.A03 = r4;
    }

    public void A00(C30461Xm r12) {
        AnonymousClass1PE A06;
        AnonymousClass1OT r0 = r12.A04;
        if (r0 != null && (A06 = this.A05.A06(C15380n4.A00(r0.A01))) != null) {
            if (A06.A0f) {
                this.A04.A05(A06.A05(), false);
            }
            C15860o1 r7 = this.A07;
            if (!r7.A08(A06.A05().getRawString()).A09()) {
                Context context = this.A02.A00;
                String A062 = A06.A06();
                C22630zO r02 = this.A06;
                context.getApplicationContext();
                CharSequence A0F = r02.A0F(r12);
                C15580nU A00 = this.A00.A0D.A00((C15580nU) ((GroupJid) A06.A05()));
                if (A00 != null) {
                    Intent A0H = C14960mK.A0H(context, A00);
                    C18360sK r5 = this.A03;
                    String A002 = C18360sK.A00(A00);
                    if (A002 != null) {
                        PendingIntent A003 = AnonymousClass1UY.A00(context, 6, A0H, 134217728);
                        C005602s A004 = C22630zO.A00(context);
                        A004.A0B(A062);
                        A004.A05(System.currentTimeMillis());
                        A004.A02(3);
                        A004.A0D(true);
                        A004.A0A(A062);
                        A004.A09(A0F);
                        A004.A09 = A003;
                        if (Build.VERSION.SDK_INT >= 26) {
                            String A0C = ((C33271dj) r7.A08(A00.getRawString())).A0C();
                            if (A0C != null) {
                                A004.A0J = A0C;
                                A004.A01 = 1;
                            } else {
                                return;
                            }
                        }
                        C18360sK.A01(A004, R.drawable.notifybar);
                        r5.A05(A002, 49, A004.A01());
                    }
                }
            }
        }
    }
}
