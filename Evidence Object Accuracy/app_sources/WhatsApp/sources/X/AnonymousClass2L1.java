package X;

import java.util.Map;

/* renamed from: X.2L1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2L1 extends AbstractC49422Kr {
    public final C14830m7 A00;
    public final C230810h A01;

    public AnonymousClass2L1(AbstractC15710nm r7, C14830m7 r8, C14850m9 r9, C16120oU r10, C450720b r11, C230810h r12, Map map) {
        super(r7, r9, r10, r11, map);
        this.A00 = r8;
        this.A01 = r12;
    }
}
