package X;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/* renamed from: X.1UR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UR extends ObjectInputStream {
    public final /* synthetic */ C14860mA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1UR(C14860mA r1, InputStream inputStream) {
        super(inputStream);
        this.A00 = r1;
    }

    @Override // java.io.ObjectInputStream
    public ObjectStreamClass readClassDescriptor() {
        ObjectStreamClass readClassDescriptor = super.readClassDescriptor();
        return !readClassDescriptor.getName().startsWith("java.") ? ObjectStreamClass.lookup(AnonymousClass1UQ.class) : readClassDescriptor;
    }
}
