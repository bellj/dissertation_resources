package X;

/* renamed from: X.58O  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass58O implements AnonymousClass5UW {
    public static final AnonymousClass58Z A02 = new AnonymousClass58Z();
    public final String A00;
    public final String A01;

    public AnonymousClass58O(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    @Override // X.AnonymousClass5UW
    public boolean A9g(AnonymousClass4V4 r3) {
        C16700pc.A0E(r3, 0);
        Object A00 = r3.A00(this.A01);
        if (A00 != null) {
            return A00.equals(this.A00);
        }
        return false;
    }
}
