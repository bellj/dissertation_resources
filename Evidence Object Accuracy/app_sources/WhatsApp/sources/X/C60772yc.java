package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.TemplateQuickReplyButtonsLayout;
import com.whatsapp.conversation.conversationrow.TemplateRowContentLayout;
import java.util.List;

/* renamed from: X.2yc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60772yc extends AbstractC60522yA {
    public final TemplateQuickReplyButtonsLayout A00 = ((TemplateQuickReplyButtonsLayout) findViewById(R.id.template_quick_reply_buttons));
    public final TemplateRowContentLayout A01 = ((TemplateRowContentLayout) findViewById(R.id.template_message_content));

    public C60772yc(Context context, AbstractC13890kV r4, AnonymousClass1XU r5) {
        super(context, r4, r5);
        ((C60782yd) this).A06.setTemplateImageRatio(true);
        A1P();
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public void A0s() {
        A1P();
        super.A0s();
    }

    @Override // X.C60782yd, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, (AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1P();
        }
    }

    public final void A1P() {
        List list;
        this.A01.A02(this);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A00;
        if (templateQuickReplyButtonsLayout != null) {
            AnonymousClass4KN r1 = this.A1c;
            AbstractC13890kV r0 = ((AbstractC28551Oa) this).A0a;
            if (r0 == null || !r0.AdX()) {
                list = null;
            } else {
                list = ((AbstractC28871Pi) ((AnonymousClass1X7) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O))).AH7().A04;
            }
            templateQuickReplyButtonsLayout.A01(r1, list);
        }
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_template_title_image_left;
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_template_title_image_left;
    }

    @Override // X.C60782yd, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_template_title_image_right;
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A00;
        if (templateQuickReplyButtonsLayout != null) {
            AnonymousClass1OY.A0G(templateQuickReplyButtonsLayout, this);
        }
    }

    @Override // X.AnonymousClass1OY, X.AbstractC28551Oa, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        TemplateQuickReplyButtonsLayout templateQuickReplyButtonsLayout = this.A00;
        if (templateQuickReplyButtonsLayout != null) {
            setMeasuredDimension(getMeasuredWidth(), AnonymousClass1OY.A05(this, templateQuickReplyButtonsLayout));
        }
    }
}
