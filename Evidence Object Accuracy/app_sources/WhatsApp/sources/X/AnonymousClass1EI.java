package X;

import com.whatsapp.wamsys.JniBridge;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1EI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EI {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15650ng A02;
    public final C18460sU A03;
    public final C16490p7 A04;
    public final AnonymousClass119 A05;
    public final AnonymousClass1EH A06;
    public final AnonymousClass1ED A07;
    public final JniBridge A08;
    public final Object A09 = new Object();

    public AnonymousClass1EI(AbstractC15710nm r2, C15570nT r3, C15650ng r4, C18460sU r5, C16490p7 r6, AnonymousClass119 r7, AnonymousClass1EH r8, AnonymousClass1ED r9, JniBridge jniBridge) {
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A08 = jniBridge;
        this.A02 = r4;
        this.A05 = r7;
        this.A07 = r9;
        this.A04 = r6;
        this.A06 = r8;
    }

    public static void A00(C27671Iq r9, C27711Iw r10) {
        ArrayList arrayList;
        List<C27701Iu> list = r9.A04;
        List list2 = r10.A05;
        if (list2.size() == 0) {
            arrayList = new ArrayList();
        } else {
            HashSet hashSet = new HashSet();
            for (C27701Iu r5 : list) {
                if (list2.contains(Long.valueOf(r5.A01)) && r5.A01 != -1) {
                    hashSet.add(r5.A02);
                }
            }
            arrayList = new ArrayList(hashSet);
        }
        r10.A01 = arrayList;
        r10.A04 = r9.A1D;
    }

    public final void A01(C27671Iq r10, C27711Iw r11, C27711Iw r12) {
        AbstractC14640lm A0B;
        List list = r10.A03;
        C16310on A02 = this.A04.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            if (list != null) {
                if (r11 != null) {
                    Iterator it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        AnonymousClass1Iv r2 = (AnonymousClass1Iv) it.next();
                        if ((r2.A0z.A02 && r11.A0z.A02) || ((A0B = r2.A0B()) != null && A0B.equals(r11.A0B()))) {
                            if (r2.A00 == ((AnonymousClass1Iv) r11).A00) {
                                it.remove();
                                break;
                            }
                        }
                    }
                }
                list.add(r12);
                r10.A14(list);
                this.A05.A04(r10);
                this.A02.A0n.A02(r10);
            } else if ((r10.A07 & 2) != 2) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(r12);
                if (r10.A03 == null) {
                    r10.A03 = arrayList;
                    r10.A14(arrayList);
                    r10.A07 = 2 | r10.A07;
                    C15650ng r1 = this.A02;
                    r1.A0V(r10);
                    this.A05.A04(r10);
                    r1.A0n.A02(r10);
                } else {
                    throw new IllegalStateException("FMessagePoll/setPollVotes re-assigning pollVotes");
                }
            }
            A00.A00();
            A00.close();
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
