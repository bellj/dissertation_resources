package X;

import android.net.Uri;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1Ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28721Ot {
    public static final AtomicLong A03 = new AtomicLong();
    public final Uri A00;
    public final AnonymousClass3H3 A01;
    public final Map A02;

    public C28721Ot(Uri uri, AnonymousClass3H3 r2, Map map) {
        this.A01 = r2;
        this.A00 = uri;
        this.A02 = map;
    }
}
