package X;

import android.database.sqlite.SQLiteDatabase;
import com.whatsapp.jobqueue.job.SyncdDeleteAllDataForNonMdUserJob;
import com.whatsapp.jobqueue.job.SyncdTableEmptyKeyCheckJob;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.0yB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21910yB extends AbstractC16280ok {
    public final C16590pI A00;
    public final C231410n A01;
    public final AnonymousClass1I2 A02;

    public C21910yB(AbstractC15710nm r9, C16590pI r10, C231410n r11, C14850m9 r12, AnonymousClass01H r13) {
        super(r10.A00, r9, "sync.db", new ReentrantReadWriteLock(), 45, r12.A07(781));
        this.A00 = r10;
        this.A01 = r11;
        this.A02 = new AnonymousClass1I2(r13);
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        return AnonymousClass1Tx.A01(super.A00(), this.A01);
    }

    public void A04() {
        ReentrantReadWriteLock reentrantReadWriteLock = this.A04;
        ReentrantReadWriteLock.WriteLock writeLock = reentrantReadWriteLock != null ? reentrantReadWriteLock.writeLock() : null;
        try {
            writeLock.lock();
            close();
            File databasePath = this.A00.A00.getDatabasePath("sync.db");
            if (!databasePath.delete()) {
                Log.w("sync-db-helper/failed to delete db");
            }
            AnonymousClass1Tx.A04(databasePath, "synd-db-helper");
        } finally {
            writeLock.unlock();
        }
    }

    public void A05(SQLiteDatabase sQLiteDatabase) {
        Log.i("sync-db-helper/reset");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS syncd_mutations");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS collection_versions");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS pending_mutations");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS peer_messages");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS msg_history_sync");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS crypto_info");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS peer_messages_bi_for_fanout_backfill_messages_trigger");
        sQLiteDatabase.execSQL("DROP INDEX IF EXISTS peer_messages_message_key_index");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS fanout_backfill_messages");
        sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS crypto_info_bi_for_missing_keys_trigger");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS missing_keys");
        sQLiteDatabase.execSQL("DROP INDEX IF EXISTS syncd_mutations_active_mutations_index");
        sQLiteDatabase.execSQL("DROP INDEX IF EXISTS syncd_mutations_active_mutations_chat_jid_index");
        sQLiteDatabase.execSQL("DROP INDEX IF EXISTS history_sync_companion_index");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS history_sync_companion");
        onCreate(sQLiteDatabase);
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            Log.i("sync-db-observer/onDbReset");
            ((AnonymousClass16V) it.next()).A01.A03(5);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE syncd_mutations(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, mutation_index TEXT UNIQUE NOT NULL, mutation_value BLOB, mutation_version INTEGER NOT NULL, collection_name TEXT NOT NULL, are_dependencies_missing BOOLEAN NOT NULL, mutation_mac BLOB, device_id INTEGER NOT NULL, epoch INTEGER NOT NULL, chat_jid TEXT, mutation_name TEXT )");
        sQLiteDatabase.execSQL("CREATE TABLE collection_versions (collection_name TEXT PRIMARY KEY, version INTEGER NOT NULL, lt_hash BLOB, dirty_version INTEGER NOT NULL DEFAULT -1 ) ");
        sQLiteDatabase.execSQL("CREATE TABLE pending_mutations(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, mutation_index TEXT UNIQUE NOT NULL, mutation_value BLOB, mutation_version INTEGER NOT NULL, operation BLOB NOT NULL, is_ready_to_sync BOOLEAN NOT NULL, collection_name TEXT, device_id INTEGER, epoch INTEGER, are_dependencies_missing BOOLEAN NOT NULL, mutation_name TEXT NOT NULL, chat_jid TEXT )");
        sQLiteDatabase.execSQL("CREATE TABLE peer_messages(_id INTEGER PRIMARY KEY AUTOINCREMENT,message_type INTEGER NOT NULL, key_remote_jid TEXT NOT NULL, key_from_me INTEGER, key_id TEXT NOT NULL, device_id TEXT, timestamp INTEGER, data TEXT, acked BOOLEAN )");
        sQLiteDatabase.execSQL("CREATE TABLE msg_history_sync(_id INTEGER PRIMARY KEY AUTOINCREMENT, device_id TEXT NOT NULL, sync_type INTEGER NOT NULL, last_processed_msg_row_id INTEGER, oldest_msg_row_id INTEGER, sent_msgs_count INTEGER, chunk_order INTEGER, sent_bytes INTEGER, last_chunk_timestamp INTEGER, status INTEGER, peer_msg_row_id INTEGER, oldest_message_to_sync_row_id INTEGER, session_id TEXT, md_reg_attempt_id TEXT )");
        sQLiteDatabase.execSQL("CREATE TABLE crypto_info (device_id INTEGER NOT NULL, epoch INTEGER NOT NULL, key_data BLOB NOT NULL, timestamp INTEGER NOT NULL, fingerprint BLOB NOT NULL, stale_timestamp INTEGER NOT NULL DEFAULT 0, PRIMARY KEY ( device_id , epoch ) )");
        sQLiteDatabase.execSQL("CREATE TABLE missing_keys (device_id INTEGER NOT NULL, epoch INTEGER NOT NULL, collection_name TEXT NOT NULL, PRIMARY KEY ( device_id , epoch , collection_name ) )");
        sQLiteDatabase.execSQL("CREATE TRIGGER crypto_info_bi_for_missing_keys_trigger AFTER INSERT ON crypto_info BEGIN DELETE FROM missing_keys WHERE device_id=new.device_id AND epoch=new.epoch; END");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS syncd_mutations_active_mutations_index ON syncd_mutations (are_dependencies_missing)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS syncd_mutations_active_mutations_chat_jid_index ON syncd_mutations (chat_jid, are_dependencies_missing)");
        sQLiteDatabase.execSQL("CREATE TABLE history_sync_companion (message_id TEXT PRIMARY KEY NOT NULL, sync_type INTEGER NOT NULL, chunk_order INTEGER NOT NULL, media_key BLOB, media_hash TEXT NOT NULL, media_enc_hash TEXT NOT NULL, file_size INTEGER NOT NULL, direct_path TEXT NOT NULL, local_path TEXT, start_time INTEGER)");
        sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_sync_companion_index ON history_sync_companion (sync_type,chunk_order)");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("sync-db-helper/onDowngrade oldVersion:");
        sb.append(i);
        sb.append(", newVersion:");
        sb.append(i2);
        Log.i(sb.toString());
        A05(sQLiteDatabase);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("sync-db-helper/onUpgrade oldVersion:");
        sb.append(i);
        sb.append(", newVersion:");
        sb.append(i2);
        Log.i(sb.toString());
        switch (i) {
            case 23:
                sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS crypto_info_bi_for_missing_keys_trigger");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS missing_keys");
                sQLiteDatabase.execSQL("CREATE TABLE missing_keys (device_id INTEGER NOT NULL, epoch INTEGER NOT NULL, collection_name TEXT NOT NULL, PRIMARY KEY ( device_id , epoch , collection_name ) )");
                sQLiteDatabase.execSQL("CREATE TRIGGER crypto_info_bi_for_missing_keys_trigger AFTER INSERT ON crypto_info BEGIN DELETE FROM missing_keys WHERE device_id=new.device_id AND epoch=new.epoch; END");
            case 24:
                sQLiteDatabase.execSQL("DROP TRIGGER IF EXISTS peer_messages_bi_for_fanout_backfill_messages_trigger");
                sQLiteDatabase.execSQL("DROP INDEX IF EXISTS peer_messages_message_key_index");
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS fanout_backfill_messages");
            case 25:
                sQLiteDatabase.execSQL("ALTER TABLE collection_versions ADD lt_hash BLOB");
                sQLiteDatabase.execSQL("ALTER TABLE syncd_mutations ADD mutation_mac BLOB");
            case 26:
                sQLiteDatabase.execSQL("ALTER TABLE msg_history_sync ADD oldest_message_to_sync_row_id INTEGER");
            case 27:
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD is_ready_to_sync BOOLEAN NOT NULL DEFAULT 1");
            case 28:
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD collection_name TEXT");
            case 29:
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD device_id INTEGER");
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD epoch INTEGER");
                sQLiteDatabase.execSQL("ALTER TABLE syncd_mutations ADD device_id INTEGER NOT NULL DEFAULT 0");
                sQLiteDatabase.execSQL("ALTER TABLE syncd_mutations ADD epoch INTEGER NOT NULL DEFAULT 0");
            case C25991Bp.A0S /* 30 */:
                sQLiteDatabase.execSQL("ALTER TABLE crypto_info ADD stale_timestamp INTEGER NOT NULL DEFAULT 0");
            case 31:
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD are_dependencies_missing BOOLEAN NOT NULL DEFAULT 0");
            case 32:
                sQLiteDatabase.execSQL("ALTER TABLE syncd_mutations ADD chat_jid TEXT");
            case 33:
                sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS syncd_mutations_active_mutations_index ON syncd_mutations (are_dependencies_missing)");
                sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS syncd_mutations_active_mutations_chat_jid_index ON syncd_mutations (chat_jid, are_dependencies_missing)");
            case 34:
                sQLiteDatabase.execSQL("ALTER TABLE collection_versions ADD dirty_version INTEGER NOT NULL DEFAULT -1");
            case 35:
                sQLiteDatabase.execSQL("ALTER TABLE peer_messages ADD acked BOOLEAN ");
            case 36:
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS encrypted_mutations");
            case 37:
                sQLiteDatabase.execSQL("ALTER TABLE syncd_mutations ADD mutation_name TEXT");
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD mutation_name TEXT NOT NULL DEFAULT ''");
            case 38:
            case 39:
            case 40:
                sQLiteDatabase.execSQL("ALTER TABLE msg_history_sync ADD session_id TEXT");
                sQLiteDatabase.execSQL("ALTER TABLE msg_history_sync ADD md_reg_attempt_id TEXT");
            case 41:
                sQLiteDatabase.execSQL("ALTER TABLE pending_mutations ADD chat_jid TEXT");
            case 42:
            case 43:
                sQLiteDatabase.execSQL("CREATE TABLE history_sync_companion (message_id TEXT PRIMARY KEY NOT NULL, sync_type INTEGER NOT NULL, chunk_order INTEGER NOT NULL, media_key BLOB, media_hash TEXT NOT NULL, media_enc_hash TEXT NOT NULL, file_size INTEGER NOT NULL, direct_path TEXT NOT NULL, local_path TEXT, start_time INTEGER)");
                sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS history_sync_companion_index ON history_sync_companion (sync_type,chunk_order)");
                break;
            case 44:
                break;
            default:
                StringBuilder sb2 = new StringBuilder("sync-db-helper/onUpgrade unknown oldVersion:");
                sb2.append(i);
                sb2.append(", newVersion:");
                sb2.append(i2);
                Log.e(sb2.toString());
                A05(sQLiteDatabase);
                break;
        }
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            AnonymousClass16V r2 = (AnonymousClass16V) it.next();
            StringBuilder sb3 = new StringBuilder("sync-db-observer/onDbReset(");
            sb3.append(i);
            sb3.append(", ");
            sb3.append(i2);
            sb3.append(")");
            Log.i(sb3.toString());
            if (i < 43 && 43 <= i2) {
                r2.A00.A00(new SyncdTableEmptyKeyCheckJob());
            }
            if (i < 45 && 45 <= i2) {
                r2.A00.A00(new SyncdDeleteAllDataForNonMdUserJob());
            }
        }
    }
}
