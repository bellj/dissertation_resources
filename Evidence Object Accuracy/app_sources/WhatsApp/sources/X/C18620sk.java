package X;

import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;

/* renamed from: X.0sk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18620sk implements AbstractC18630sl {
    public boolean A00;
    public final AnonymousClass10P A01;
    public final C22710zW A02;
    public final C17070qD A03;
    public final C30931Zj A04 = C30931Zj.A00("PaymentsLifecycleManager", "network", "COMMON");
    public final C22660zR A05;

    public C18620sk(C18650sn r4, AnonymousClass10P r5, C22710zW r6, C17070qD r7, C22660zR r8) {
        this.A05 = r8;
        this.A03 = r7;
        this.A02 = r6;
        this.A01 = r5;
        r4.A00 = this;
    }

    public synchronized void A00() {
        if (this.A00) {
            this.A04.A06("payments was already initialized");
        } else if (this.A02.A07()) {
            this.A04.A06("initializing payments");
            AnonymousClass10P r3 = this.A01;
            synchronized (r3) {
                r3.A00 = true;
                r3.A02.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(r3, 17));
            }
            this.A05.A03(this);
            this.A00 = true;
        }
    }

    public synchronized void A01(boolean z, boolean z2) {
        this.A04.A06("reinitializing payments");
        this.A00 = false;
        this.A03.A05(z, z2);
        this.A05.A04(this);
        AnonymousClass10P r3 = this.A01;
        synchronized (r3) {
            r3.A00 = false;
            r3.A02.A0H(new RunnableBRunnable0Shape9S0100000_I0_9(r3, 16));
        }
        A00();
    }

    @Override // X.AbstractC18630sl
    public void ASI(boolean z) {
        if (z) {
            this.A03.A05(true, false);
        }
    }
}
