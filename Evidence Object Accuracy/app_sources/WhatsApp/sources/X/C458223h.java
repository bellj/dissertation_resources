package X;

import android.os.SystemClock;

/* renamed from: X.23h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C458223h {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public boolean A04;
    public final long A05;
    public final long A06 = SystemClock.elapsedRealtime();
    public final boolean A07;

    public C458223h(long j, boolean z) {
        this.A07 = z;
        this.A05 = j;
    }
}
