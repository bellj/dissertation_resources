package X;

import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.1u0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41731u0 implements AbstractC21730xt {
    public final C41721tz A00;
    public final C17220qS A01;

    public C41731u0(C41721tz r1, C17220qS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        StringBuilder sb = new StringBuilder("PrivacySettingsProtocolHelper/onDeliveryFailure iqid=");
        sb.append(str);
        Log.w(sb.toString());
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r2, String str) {
        Log.e("PrivacySettingsProtocolHelper/onError");
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r10, String str) {
        AnonymousClass1V8 A0C = r10.A0C();
        AnonymousClass1V8.A01(A0C, "privacy");
        HashMap hashMap = new HashMap();
        AnonymousClass1V8[] r8 = A0C.A03;
        if (r8 != null) {
            for (AnonymousClass1V8 r6 : r8) {
                AnonymousClass1V8.A01(r6, "category");
                String A0I = r6.A0I("name", null);
                String A0I2 = r6.A0I("value", null);
                if ("error".equals(A0I2)) {
                    AnonymousClass1V8 A0C2 = r6.A0C();
                    AnonymousClass1V8.A01(A0C2, "error");
                    A0I2 = A0C2.A0I("code", null);
                }
                hashMap.put(A0I, A0I2);
            }
        }
        C41721tz r2 = this.A00;
        C37241lo r1 = r2.A00;
        if (r1 != null) {
            r1.A00(3);
        }
        r2.A01.A02(hashMap);
    }
}
