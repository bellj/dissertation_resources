package X;

/* renamed from: X.5Ky  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114295Ky extends AbstractC114305Kz implements AbstractC02790Dz {
    public final AbstractC02780Dy A00;

    public C114295Ky(AbstractC02780Dy r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC02790Dz
    public boolean A79(Throwable th) {
        C10710f4 r0 = super.A00;
        if (r0 != null) {
            return r0.A0i(th);
        }
        throw C16700pc.A06("job");
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        A0A((Throwable) obj);
        return AnonymousClass1WZ.A00;
    }
}
