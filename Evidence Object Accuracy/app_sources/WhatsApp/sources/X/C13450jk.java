package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/* renamed from: X.0jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13450jk {
    public final Context A00;
    public final SharedPreferences A01;
    public final C13460jl A02;
    public final Map A03 = new AnonymousClass00N();

    public C13450jk(Context context) {
        String str;
        boolean isEmpty;
        C13460jl r2 = new C13460jl();
        this.A00 = context;
        this.A01 = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.A02 = r2;
        File file = new File(AnonymousClass00T.A06(context), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    synchronized (this) {
                        isEmpty = this.A01.getAll().isEmpty();
                    }
                    if (!isEmpty) {
                        Log.i("FirebaseInstanceId", "App restored, clearing state");
                        A02();
                        FirebaseInstanceId.getInstance(C13030j1.A00()).A07();
                    }
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    if (valueOf.length() != 0) {
                        str = "Error creating file in no backup dir: ".concat(valueOf);
                    } else {
                        str = new String("Error creating file in no backup dir: ");
                    }
                    Log.d("FirebaseInstanceId", str);
                }
            }
        }
    }

    public static String A00(String str) {
        StringBuilder sb = new StringBuilder(String.valueOf("").length() + 3 + String.valueOf(str).length());
        sb.append("");
        sb.append("|S|");
        sb.append(str);
        return sb.toString();
    }

    public static String A01(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf("").length() + 4 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb.append("");
        sb.append("|T|");
        sb.append(str);
        sb.append("|");
        sb.append(str2);
        return sb.toString();
    }

    public final synchronized void A02() {
        this.A03.clear();
        Context context = this.A00;
        File A06 = AnonymousClass00T.A06(context);
        if (A06 == null || !A06.isDirectory()) {
            Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
            A06 = context.getFilesDir();
        }
        File[] listFiles = A06.listFiles();
        for (File file : listFiles) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
        this.A01.edit().clear().commit();
    }
}
