package X;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/* renamed from: X.4Yk  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4Yk {
    public static byte[] A00(byte[] bArr) {
        if (bArr != null) {
            try {
                ByteBuffer wrap = ByteBuffer.wrap(bArr);
                byte b = wrap.get();
                byte[] bArr2 = new byte[3];
                wrap.get(bArr2);
                int A00 = AnonymousClass3JS.A00(bArr2);
                byte[] bArr3 = new byte[A00];
                wrap.get(bArr3);
                if (AnonymousClass4GL.A00.contains(Byte.valueOf(b)) && A00 <= 16777215) {
                    return bArr3;
                }
                StringBuilder A0h = C12960it.A0h();
                A0h.append("Received an invalid handshake - type ");
                A0h.append((int) b);
                throw AnonymousClass1NR.A00(C12960it.A0e(" len ", A0h, A00), (byte) 80);
            } catch (BufferUnderflowException e) {
                throw AnonymousClass1NR.A01("Invalid handshake message", e, (byte) 80);
            }
        } else {
            throw AnonymousClass1NR.A00("Illegal argument - handshake is null", (byte) 80);
        }
    }

    public static byte[] A01(byte[] bArr, byte b) {
        int length;
        if (!AnonymousClass4GL.A00.contains(Byte.valueOf(b)) || bArr == null || (length = bArr.length) > 16777215) {
            StringBuilder A0k = C12960it.A0k("Illegal arguments -  type ");
            A0k.append((int) b);
            throw AnonymousClass1NR.A00(C12960it.A0e(" msg is null or bigger than", A0k, 16777215), (byte) 80);
        }
        ByteBuffer allocate = ByteBuffer.allocate(length + 4);
        allocate.put(b);
        allocate.put(AnonymousClass3JS.A05(length));
        allocate.put(bArr);
        return allocate.array();
    }
}
