package X;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.5Gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113125Gc implements AnonymousClass5VS, AbstractC117215Yz {
    public Collection A00;

    public C113125Gc(Collection collection) {
        this.A00 = C12980iv.A0x(collection);
    }

    @Override // X.AnonymousClass5VS
    public Collection AE7(AbstractC117205Yy r5) {
        if (r5 == null) {
            return C12980iv.A0x(this.A00);
        }
        ArrayList A0l = C12960it.A0l();
        for (Object obj : this.A00) {
            if (r5.ALJ(obj)) {
                A0l.add(obj);
            }
        }
        return A0l;
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return C12980iv.A0x(this.A00).iterator();
    }
}
