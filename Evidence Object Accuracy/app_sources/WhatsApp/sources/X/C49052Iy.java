package X;

import android.app.Activity;
import android.view.View;
import androidx.window.layout.WindowInfoTrackerImpl$windowLayoutInfo$1;

/* renamed from: X.2Iy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49052Iy implements AnonymousClass2ID {
    public static final AnonymousClass017 A05 = new AnonymousClass016();
    public C07550Zd A00;
    public boolean A01;
    public final Activity A02;
    public final AnonymousClass3PC A03 = new AnonymousClass3PC(this);
    public final C14820m6 A04;

    @Override // X.AnonymousClass2ID
    public void Abq(View view) {
    }

    @Override // X.AnonymousClass2ID
    public void onGlobalLayout() {
    }

    public C49052Iy(Activity activity, C14820m6 r5) {
        this.A02 = activity;
        this.A04 = r5;
        this.A01 = r5.A00.getBoolean("detect_device_foldable", false);
    }

    @Override // X.AnonymousClass2ID
    public AnonymousClass017 AB7() {
        return A05;
    }

    @Override // X.AnonymousClass2ID
    public AnonymousClass017 AB8() {
        return A05;
    }

    @Override // X.AnonymousClass2ID
    public void AWH(View view) {
        if (!this.A01) {
            Activity activity = this.A02;
            C07550Zd r5 = new C07550Zd(AbstractC12760iS.A00.A02(activity));
            this.A00 = r5;
            ExecutorC10620ev r4 = ExecutorC10620ev.A00;
            AnonymousClass3PC r3 = this.A03;
            C16700pc.A0E(activity, 0);
            r5.A03(r3, r4, C88254Ew.A00(new WindowInfoTrackerImpl$windowLayoutInfo$1(activity, (C07540Zc) r5.A00, null)));
        }
    }

    @Override // X.AnonymousClass2ID
    public void AWn() {
        C07550Zd r1 = this.A00;
        if (r1 != null) {
            r1.A02(this.A03);
            this.A00 = null;
        }
    }
}
