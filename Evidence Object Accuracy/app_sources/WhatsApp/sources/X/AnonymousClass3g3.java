package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.conversation.ConversationListView;

/* renamed from: X.3g3  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass3g3 extends Handler {
    public final /* synthetic */ ConversationListView A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass3g3(Looper looper, ConversationListView conversationListView) {
        super(looper);
        this.A00 = conversationListView;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        if (message.what == 0) {
            this.A00.A04();
        }
        this.A00.setTranscriptMode(0);
    }
}
