package X;

/* renamed from: X.588  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass588 implements AnonymousClass1FK {
    public final /* synthetic */ AnonymousClass1A8 A00;
    public final /* synthetic */ String A01;

    public AnonymousClass588(AnonymousClass1A8 r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r4) {
        AnonymousClass1A8.A00(null, this.A00, this.A01);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r4) {
        AnonymousClass1A8.A00(null, this.A00, this.A01);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        AnonymousClass1A8 r2 = this.A00;
        String str = this.A01;
        r2.A00.remove(str);
        r2.A01.remove(str);
    }
}
