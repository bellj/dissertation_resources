package X;

/* renamed from: X.5MK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MK extends AnonymousClass1TM {
    public AnonymousClass1TN A00;
    public AnonymousClass1TK A01;

    public AnonymousClass5MK(AbstractC114775Na r3) {
        if (r3.A0B() == 2) {
            this.A01 = AnonymousClass1TK.A00(AbstractC114775Na.A00(r3));
            this.A00 = AbstractC114775Na.A01(r3);
            return;
        }
        throw C12970iu.A0f(C12960it.A0f(C12960it.A0k("Bad sequence size: "), r3.A0B()));
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A01);
        return C94954co.A01(this.A00, A00);
    }
}
