package X;

import java.io.File;
import java.util.List;

/* renamed from: X.4Tv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91944Tv {
    public final byte A00;
    public final AnonymousClass02N A01;
    public final AbstractC15710nm A02;
    public final C002701f A03;
    public final C15650ng A04;
    public final C15660nh A05;
    public final AnonymousClass4XU A06;
    public final File A07;
    public final List A08 = C12960it.A0l();

    public C91944Tv(AnonymousClass02N r2, AbstractC15710nm r3, C002701f r4, C15650ng r5, C15660nh r6, AnonymousClass4XU r7, File file, byte b) {
        this.A02 = r3;
        this.A04 = r5;
        this.A05 = r6;
        this.A03 = r4;
        this.A00 = b;
        this.A07 = file;
        this.A01 = r2;
        this.A06 = r7;
    }
}
