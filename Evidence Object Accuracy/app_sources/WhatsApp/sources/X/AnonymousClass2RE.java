package X;

/* renamed from: X.2RE  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2RE extends AbstractC16110oT {
    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r1) {
    }

    public AnonymousClass2RE() {
        super(2374, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamServerDeviceCacheStale {");
        sb.append("}");
        return sb.toString();
    }
}
