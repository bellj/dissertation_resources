package X;

import android.content.BroadcastReceiver;

/* renamed from: X.1zg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44861zg extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass10K A00;

    public C44861zg(AnonymousClass10K r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0036, code lost:
        r2 = (X.AnonymousClass022) r5.get();
        ((X.C07760a2) r2.A06).A01.execute(new X.C03170Gl(r2, r3.A05));
     */
    @Override // android.content.BroadcastReceiver
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r7, android.content.Intent r8) {
        /*
            r6 = this;
            java.lang.String r0 = "gdrive-notification-manager/user-dismissed the notification"
            com.whatsapp.util.Log.i(r0)
            X.10K r4 = r6.A00
            X.0pI r0 = r4.A0J
            android.content.Context r0 = r0.A00
            r0.unregisterReceiver(r6)
            X.0xr r5 = r4.A0P     // Catch: ExecutionException | InterruptedException -> 0x004d
            java.lang.Object r0 = r5.get()     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.022 r0 = (X.AnonymousClass022) r0     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.1yX r0 = r0.A02()     // Catch: ExecutionException | InterruptedException -> 0x004d
            java.lang.Object r0 = r0.get()     // Catch: ExecutionException | InterruptedException -> 0x004d
            java.util.List r0 = (java.util.List) r0     // Catch: ExecutionException | InterruptedException -> 0x004d
            java.util.Iterator r2 = r0.iterator()     // Catch: ExecutionException | InterruptedException -> 0x004d
        L_0x0024:
            boolean r0 = r2.hasNext()     // Catch: ExecutionException | InterruptedException -> 0x004d
            if (r0 == 0) goto L_0x0053
            java.lang.Object r3 = r2.next()     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0Pp r3 = (X.C05450Pp) r3     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0Ji r1 = r3.A03     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0Ji r0 = X.EnumC03840Ji.RUNNING     // Catch: ExecutionException | InterruptedException -> 0x004d
            if (r1 != r0) goto L_0x0024
            java.lang.Object r2 = r5.get()     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.022 r2 = (X.AnonymousClass022) r2     // Catch: ExecutionException | InterruptedException -> 0x004d
            java.util.UUID r0 = r3.A05     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0Gl r1 = new X.0Gl     // Catch: ExecutionException | InterruptedException -> 0x004d
            r1.<init>(r2, r0)     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0gO r0 = r2.A06     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0a2 r0 = (X.C07760a2) r0     // Catch: ExecutionException | InterruptedException -> 0x004d
            X.0eu r0 = r0.A01     // Catch: ExecutionException | InterruptedException -> 0x004d
            r0.execute(r1)     // Catch: ExecutionException | InterruptedException -> 0x004d
            goto L_0x0053
        L_0x004d:
            r1 = move-exception
            java.lang.String r0 = "gdrive-notification-manager/couldn't cancel worker."
            com.whatsapp.util.Log.e(r0, r1)
        L_0x0053:
            r4.A03()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C44861zg.onReceive(android.content.Context, android.content.Intent):void");
    }
}
