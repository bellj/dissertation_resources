package X;

import android.view.ViewTreeObserver;
import com.whatsapp.calling.callhistory.group.GroupCallParticipantPickerSheet;

/* renamed from: X.4nb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101484nb implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ GroupCallParticipantPickerSheet A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101484nb(GroupCallParticipantPickerSheet groupCallParticipantPickerSheet) {
        this.A00 = groupCallParticipantPickerSheet;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        GroupCallParticipantPickerSheet groupCallParticipantPickerSheet = this.A00;
        C12980iv.A1F(groupCallParticipantPickerSheet.A03, this);
        if (!groupCallParticipantPickerSheet.A0A) {
            groupCallParticipantPickerSheet.A08.A0M(4);
        }
        groupCallParticipantPickerSheet.A0A = false;
    }
}
