package X;

import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.whatsapp.Conversation;

/* renamed from: X.3gz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73903gz extends Animation {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Conversation A01;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return false;
    }

    public C73903gz(Conversation conversation, int i) {
        this.A01 = conversation;
        this.A00 = i;
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = (int) (((float) this.A00) * f);
        Drawable background = this.A01.A0A.getBackground();
        if (background instanceof AnonymousClass2Zg) {
            AnonymousClass2Zg r1 = (AnonymousClass2Zg) background;
            r1.A00 = i;
            r1.invalidateSelf();
        }
    }
}
