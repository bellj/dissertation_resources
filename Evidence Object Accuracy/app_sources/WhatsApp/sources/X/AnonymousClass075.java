package X;

import java.util.Iterator;

/* renamed from: X.075  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass075 implements Iterator, AnonymousClass076 {
    public AnonymousClass05U A00;
    public boolean A01 = true;
    public final /* synthetic */ AnonymousClass03E A02;

    public AnonymousClass075(AnonymousClass03E r2) {
        this.A02 = r2;
    }

    @Override // X.AnonymousClass076
    public void Aea(AnonymousClass05U r3) {
        AnonymousClass05U r0 = this.A00;
        if (r3 == r0) {
            AnonymousClass05U r1 = r0.A01;
            this.A00 = r1;
            boolean z = false;
            if (r1 == null) {
                z = true;
            }
            this.A01 = z;
        }
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        AnonymousClass05U r0;
        if (this.A01) {
            r0 = this.A02.A02;
        } else {
            AnonymousClass05U r02 = this.A00;
            if (r02 == null) {
                return false;
            }
            r0 = r02.A00;
        }
        if (r0 != null) {
            return true;
        }
        return false;
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        AnonymousClass05U r0;
        if (this.A01) {
            this.A01 = false;
            r0 = this.A02.A02;
        } else {
            AnonymousClass05U r02 = this.A00;
            r0 = r02 != null ? r02.A00 : null;
        }
        this.A00 = r0;
        return r0;
    }
}
