package X;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import com.whatsapp.R;

/* renamed from: X.2Pf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50352Pf {
    public static final TypefaceSpan A02 = new TypefaceSpan("monospace");
    public final C16590pI A00;
    public final AnonymousClass018 A01;

    public C50352Pf(C16590pI r1, AnonymousClass018 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public Spanned A00(long j, long j2) {
        String A022;
        Spannable[] spannableArr;
        SpannableString spannableString;
        char c;
        C16590pI r1;
        int i;
        SpannableString spannableString2 = new SpannableString(this.A01.A0K().format(((double) j) / 100.0d));
        spannableString2.setSpan(A02, 0, spannableString2.length(), 33);
        if (j2 < 60000) {
            spannableString = new SpannableString(String.valueOf(j2 / 1000));
            r1 = this.A00;
            i = R.string.estimated_time_to_complete_file_transfer_seconds_message;
        } else if (j2 < 3600000) {
            spannableString = new SpannableString(String.valueOf(j2 / 60000));
            r1 = this.A00;
            i = R.string.estimated_time_to_complete_file_transfer_minutes_message;
        } else if (j2 < 43200000) {
            SpannableString spannableString3 = new SpannableString(String.valueOf(j2 / 3600000));
            spannableString = new SpannableString(String.valueOf((j2 % 3600000) / 60000));
            A022 = this.A00.A02(R.string.estimated_time_to_complete_file_transfer_hours_minutes_message);
            spannableArr = new Spannable[3];
            spannableArr[0] = spannableString2;
            spannableArr[1] = spannableString3;
            c = 2;
            spannableArr[c] = spannableString;
            return C42971wC.A02(A022, spannableArr);
        } else {
            A022 = this.A00.A02(R.string.estimated_time_to_complete_file_transfer_greater_than_twelve_hours);
            spannableArr = new Spannable[]{spannableString2};
            return C42971wC.A02(A022, spannableArr);
        }
        A022 = r1.A02(i);
        spannableArr = new Spannable[2];
        spannableArr[0] = spannableString2;
        c = 1;
        spannableArr[c] = spannableString;
        return C42971wC.A02(A022, spannableArr);
    }
}
