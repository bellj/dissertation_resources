package X;

import android.text.TextUtils;

/* renamed from: X.4HC  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HC {
    public static final String A00;
    public static final String[] A01;

    static {
        String[] strArr = {"_id", "chat_row_id", "from_me", "key_id", "sender_jid_row_id", "parent_message_row_id", "timestamp", "status", "message_add_on_type"};
        A01 = strArr;
        A00 = TextUtils.join(",", strArr);
    }
}
