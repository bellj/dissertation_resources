package X;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.UnsafeUtil;
import java.io.OutputStream;

/* renamed from: X.1sY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40921sY extends CodedOutputStream {
    public int A00;
    public int A01;
    public final int A02;
    public final OutputStream A03;
    public final byte[] A04;

    public C40921sY(OutputStream outputStream, int i) {
        if (i >= 0) {
            int max = Math.max(i, 20);
            this.A04 = new byte[max];
            this.A02 = max;
            if (outputStream != null) {
                this.A03 = outputStream;
                return;
            }
            throw new NullPointerException("out");
        }
        throw new IllegalArgumentException("bufferSize must be >= 0");
    }

    public final void A0O() {
        this.A03.write(this.A04, 0, this.A00);
        this.A00 = 0;
    }

    public final void A0P(int i) {
        byte[] bArr;
        int i2;
        int i3;
        byte[] bArr2;
        long j;
        if (CodedOutputStream.A02) {
            long j2 = CodedOutputStream.A00 + ((long) this.A00);
            long j3 = j2;
            while (true) {
                int i4 = i & -128;
                bArr2 = this.A04;
                j = 1 + j3;
                if (i4 == 0) {
                    break;
                }
                UnsafeUtil.A00(bArr2, (byte) ((i & 127) | 128), j3);
                i >>>= 7;
                j3 = j;
            }
            UnsafeUtil.A00(bArr2, (byte) i, j3);
            int i5 = (int) (j - j2);
            this.A00 += i5;
            i3 = this.A01 + i5;
        } else {
            while (true) {
                int i6 = i & -128;
                bArr = this.A04;
                i2 = this.A00;
                this.A00 = i2 + 1;
                if (i6 == 0) {
                    break;
                }
                bArr[i2] = (byte) ((i & 127) | 128);
                this.A01++;
                i >>>= 7;
            }
            bArr[i2] = (byte) i;
            i3 = this.A01 + 1;
        }
        this.A01 = i3;
    }

    public final void A0Q(int i) {
        if (this.A02 - this.A00 < i) {
            A0O();
        }
    }

    public final void A0R(long j) {
        byte[] bArr;
        int i;
        int i2;
        int i3;
        byte[] bArr2;
        long j2;
        int i4;
        long j3 = j;
        if (CodedOutputStream.A02) {
            long j4 = CodedOutputStream.A00 + ((long) this.A00);
            long j5 = j4;
            while (true) {
                int i5 = ((j3 & -128) > 0 ? 1 : ((j3 & -128) == 0 ? 0 : -1));
                bArr2 = this.A04;
                j2 = 1 + j5;
                i4 = (int) j3;
                if (i5 == 0) {
                    break;
                }
                UnsafeUtil.A00(bArr2, (byte) ((i4 & 127) | 128), j5);
                j3 >>>= 7;
                j5 = j2;
            }
            UnsafeUtil.A00(bArr2, (byte) i4, j5);
            int i6 = (int) (j2 - j4);
            this.A00 += i6;
            i3 = this.A01 + i6;
        } else {
            while (true) {
                int i7 = ((j3 & -128) > 0 ? 1 : ((j3 & -128) == 0 ? 0 : -1));
                bArr = this.A04;
                i = this.A00;
                this.A00 = i + 1;
                i2 = (int) j3;
                if (i7 == 0) {
                    break;
                }
                bArr[i] = (byte) ((i2 & 127) | 128);
                this.A01++;
                j3 >>>= 7;
            }
            bArr[i] = (byte) i2;
            i3 = this.A01 + 1;
        }
        this.A01 = i3;
    }
}
