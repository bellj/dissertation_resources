package X;

import com.whatsapp.WaEditText;

/* renamed from: X.66c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1322366c implements AbstractC116455Vm {
    public final /* synthetic */ WaEditText A00;
    public final /* synthetic */ C129745yE A01;

    public C1322366c(WaEditText waEditText, C129745yE r2) {
        this.A01 = r2;
        this.A00 = waEditText;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        WaEditText waEditText = this.A00;
        AnonymousClass009.A03(waEditText);
        C12960it.A0v(waEditText);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00, iArr, 0);
    }
}
