package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57412mz extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57412mz A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public C27081Fy A01;
    public C57622nM A02;
    public AnonymousClass1G8 A03;

    static {
        C57412mz r0 = new C57412mz();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        C82403vZ r1;
        AnonymousClass1G9 r12;
        AnonymousClass1G3 r13;
        switch (r5.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C57412mz r7 = (C57412mz) obj2;
                this.A01 = (C27081Fy) r6.Aft(this.A01, r7.A01);
                this.A03 = (AnonymousClass1G8) r6.Aft(this.A03, r7.A03);
                this.A02 = (C57622nM) r6.Aft(this.A02, r7.A02);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r62.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 18) {
                                if ((this.A00 & 1) == 1) {
                                    r13 = (AnonymousClass1G3) this.A01.A0T();
                                } else {
                                    r13 = null;
                                }
                                C27081Fy r0 = (C27081Fy) AbstractC27091Fz.A0H(r62, r72, C27081Fy.A0i);
                                this.A01 = r0;
                                if (r13 != null) {
                                    this.A01 = (C27081Fy) AbstractC27091Fz.A0C(r13, r0);
                                }
                                this.A00 |= 1;
                            } else if (A03 == 26) {
                                if ((this.A00 & 2) == 2) {
                                    r12 = (AnonymousClass1G9) this.A03.A0T();
                                } else {
                                    r12 = null;
                                }
                                AnonymousClass1G8 r02 = (AnonymousClass1G8) AbstractC27091Fz.A0H(r62, r72, AnonymousClass1G8.A05);
                                this.A03 = r02;
                                if (r12 != null) {
                                    this.A03 = (AnonymousClass1G8) AbstractC27091Fz.A0C(r12, r02);
                                }
                                this.A00 |= 2;
                            } else if (A03 == 34) {
                                if ((this.A00 & 4) == 4) {
                                    r1 = (C82403vZ) this.A02.A0T();
                                } else {
                                    r1 = null;
                                }
                                C57622nM r03 = (C57622nM) AbstractC27091Fz.A0H(r62, r72, C57622nM.A0B);
                                this.A02 = r03;
                                if (r1 != null) {
                                    this.A02 = (C57622nM) AbstractC27091Fz.A0C(r1, r03);
                                }
                                this.A00 |= 4;
                            } else if (!A0a(r62, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57412mz();
            case 5:
                return new C82333vS();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (C57412mz.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            C27081Fy r0 = this.A01;
            if (r0 == null) {
                r0 = C27081Fy.A0i;
            }
            i2 = AbstractC27091Fz.A08(r0, 2, 0);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass1G8 r02 = this.A03;
            if (r02 == null) {
                r02 = AnonymousClass1G8.A05;
            }
            i2 = AbstractC27091Fz.A08(r02, 3, i2);
        }
        if ((this.A00 & 4) == 4) {
            C57622nM r03 = this.A02;
            if (r03 == null) {
                r03 = C57622nM.A0B;
            }
            i2 = AbstractC27091Fz.A08(r03, 4, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            C27081Fy r0 = this.A01;
            if (r0 == null) {
                r0 = C27081Fy.A0i;
            }
            codedOutputStream.A0L(r0, 2);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass1G8 r02 = this.A03;
            if (r02 == null) {
                r02 = AnonymousClass1G8.A05;
            }
            codedOutputStream.A0L(r02, 3);
        }
        if ((this.A00 & 4) == 4) {
            C57622nM r03 = this.A02;
            if (r03 == null) {
                r03 = C57622nM.A0B;
            }
            codedOutputStream.A0L(r03, 4);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
