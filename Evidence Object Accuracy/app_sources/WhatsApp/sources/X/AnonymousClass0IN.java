package X;

import android.graphics.Bitmap;

/* renamed from: X.0IN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IN extends AnonymousClass0eL {
    public final /* synthetic */ Bitmap A00;
    public final /* synthetic */ AnonymousClass0IH A01;

    public AnonymousClass0IN(Bitmap bitmap, AnonymousClass0IH r2) {
        this.A01 = r2;
        this.A00 = bitmap;
    }

    @Override // X.AnonymousClass0eL, java.lang.Runnable
    public void run() {
        Bitmap bitmap = this.A00;
        if (bitmap != null) {
            AnonymousClass0I8 r1 = this.A01.A00;
            r1.A04 = bitmap;
            r1.A00 = (float) bitmap.getHeight();
            r1.A07();
            r1.A01();
        }
        this.A01.A00.A08 = false;
    }
}
