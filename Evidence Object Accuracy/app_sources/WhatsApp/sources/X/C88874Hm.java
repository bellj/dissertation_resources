package X;

/* renamed from: X.4Hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88874Hm {
    @Deprecated
    public static final AnonymousClass5QS A00 = AnonymousClass4HU.A00;
    public static final AbstractC77683ng A01;
    public static final AbstractC77683ng A02;
    public static final AnonymousClass4DN A03;
    public static final AnonymousClass4DN A04;
    public static final AnonymousClass1UE A05;
    public static final AnonymousClass1UE A06;
    @Deprecated
    public static final AnonymousClass1UE A07 = AnonymousClass4HU.A03;

    static {
        AnonymousClass4DN r5 = new AnonymousClass4DN();
        A03 = r5;
        AnonymousClass4DN r4 = new AnonymousClass4DN();
        A04 = r4;
        C77573nV r3 = new C77573nV();
        A01 = r3;
        C77583nW r2 = new C77583nW();
        A02 = r2;
        A05 = new AnonymousClass1UE(r3, r5, "Auth.CREDENTIALS_API");
        A06 = new AnonymousClass1UE(r2, r4, "Auth.GOOGLE_SIGN_IN_API");
    }
}
