package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57172mb extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57172mb A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public int A01 = 1;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;

    static {
        C57172mb r0 = new C57172mb();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57172mb r9 = (C57172mb) obj2;
                this.A02 = r8.Afr(this.A02, r9.A02);
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                int i2 = this.A01;
                int i3 = r9.A00;
                this.A01 = r8.Afp(i2, r9.A01, A1R, C12960it.A1R(i3));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A032 = r82.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 10) {
                            AnonymousClass1K6 r1 = this.A02;
                            if (!((AnonymousClass1K7) r1).A00) {
                                r1 = AbstractC27091Fz.A0G(r1);
                                this.A02 = r1;
                            }
                            r1.add((C57162ma) AbstractC27091Fz.A0H(r82, r92, C57162ma.A03));
                        } else if (A032 == 16) {
                            this.A00 |= 1;
                            this.A01 = r82.A02();
                        } else if (!A0a(r82, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A02);
                return null;
            case 4:
                return new C57172mb();
            case 5:
                return new C82013uw();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57172mb.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i2 = AbstractC27091Fz.A08((AnonymousClass1G1) this.A02.get(i3), 1, i2);
        }
        if ((this.A00 & 1) == 1) {
            i2 += CodedOutputStream.A03(2, this.A01);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        int i = 0;
        while (i < this.A02.size()) {
            i = AbstractC27091Fz.A06(codedOutputStream, this.A02, i, 1);
        }
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0E(2, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
