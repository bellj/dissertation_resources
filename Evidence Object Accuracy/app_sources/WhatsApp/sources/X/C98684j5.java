package X;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;

/* renamed from: X.4j5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98684j5 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String[] strArr = null;
        CursorWindow[] cursorWindowArr = null;
        Bundle bundle = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 1) {
                strArr = C95664e9.A0L(parcel, readInt);
            } else if (c == 2) {
                cursorWindowArr = (CursorWindow[]) C95664e9.A0K(parcel, CursorWindow.CREATOR, readInt);
            } else if (c == 3) {
                i2 = C95664e9.A02(parcel, readInt);
            } else if (c == 4) {
                bundle = C95664e9.A05(parcel, readInt);
            } else if (c != 1000) {
                C95664e9.A0D(parcel, readInt);
            } else {
                i = C95664e9.A02(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        DataHolder dataHolder = new DataHolder(bundle, cursorWindowArr, strArr, i, i2);
        dataHolder.A00();
        return dataHolder;
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataHolder[i];
    }
}
