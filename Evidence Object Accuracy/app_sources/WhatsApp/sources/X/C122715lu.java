package X;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122715lu extends AbstractC118835cS {
    public final View A00;
    public final ImageView A01;
    public final TextView A02;
    public final TextView A03;

    public C122715lu(View view) {
        super(view);
        this.A03 = C12960it.A0I(view, R.id.title);
        this.A02 = C12960it.A0I(view, R.id.subtitle);
        this.A01 = C12970iu.A0K(view, R.id.icon);
        this.A00 = AnonymousClass028.A0D(view, R.id.open_indicator);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r9, int i) {
        int i2;
        C122945mM r92 = (C122945mM) r9;
        TextView textView = this.A03;
        CharSequence charSequence = r92.A05;
        textView.setText(charSequence);
        int i3 = 8;
        int i4 = 0;
        textView.setVisibility(C13010iy.A00(TextUtils.isEmpty(charSequence) ? 1 : 0));
        ImageView imageView = this.A01;
        Drawable drawable = r92.A03;
        imageView.setImageDrawable(drawable);
        int i5 = 8;
        if (drawable != null) {
            i5 = 0;
        }
        imageView.setVisibility(i5);
        View view = this.A00;
        if (r92.A06) {
            i3 = 0;
        }
        view.setVisibility(i3);
        View view2 = this.A0H;
        view2.setOnClickListener(r92.A00);
        view2.setOnLongClickListener(r92.A01);
        if (r92.A00 == null && r92.A01 == null) {
            view2.setEnabled(false);
        }
        this.A02.setText(r92.A04);
        int paddingLeft = view2.getPaddingLeft();
        int paddingRight = view2.getPaddingRight();
        int paddingTop = view2.getPaddingTop();
        int paddingBottom = view2.getPaddingBottom();
        int i6 = r92.A02;
        if (i6 == 0) {
            paddingRight = (int) view2.getResources().getDimension(R.dimen.product_margin_16dp);
            paddingTop = paddingRight;
            i4 = paddingRight;
            paddingBottom = paddingRight;
        } else if (i6 != 1) {
            i4 = paddingLeft;
        } else {
            paddingTop = (int) view2.getResources().getDimension(R.dimen.conversation_row_margin);
            i2 = (int) view2.getResources().getDimension(R.dimen.novi_payment_transaction_detail_view_transaction_margin_left);
            paddingBottom = paddingTop;
            view2.setPadding(i4, paddingTop, paddingRight, paddingBottom);
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view2);
            A0H.leftMargin = i2;
            view2.setLayoutParams(A0H);
        }
        i2 = 0;
        view2.setPadding(i4, paddingTop, paddingRight, paddingBottom);
        ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view2);
        A0H.leftMargin = i2;
        view2.setLayoutParams(A0H);
    }
}
