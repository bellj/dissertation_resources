package X;

/* renamed from: X.19z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255919z {
    public Boolean A00 = null;
    public Boolean A01 = null;
    public Long A02 = null;
    public final C14830m7 A03;
    public final C16120oU A04;

    public C255919z(C14830m7 r2, C16120oU r3) {
        this.A03 = r2;
        this.A04 = r3;
    }

    public final void A00(int i) {
        C613930e r4 = new C613930e();
        r4.A02 = Integer.valueOf(i);
        r4.A03 = Long.valueOf(System.currentTimeMillis() - this.A03.A02);
        r4.A00 = this.A00;
        r4.A01 = this.A01;
        r4.A04 = this.A02;
        this.A04.A07(r4);
    }
}
