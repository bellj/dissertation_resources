package X;

import java.util.ArrayList;

/* renamed from: X.0YI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0YI implements AnonymousClass02g {
    public final int A00;
    public final int A01;
    public final String A02;
    public final /* synthetic */ AnonymousClass01F A03;

    public AnonymousClass0YI(AnonymousClass01F r1, String str, int i, int i2) {
        this.A03 = r1;
        this.A02 = str;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AnonymousClass02g
    public boolean AAG(ArrayList arrayList, ArrayList arrayList2) {
        int i;
        AnonymousClass01F r2 = this.A03;
        AnonymousClass01E r1 = r2.A06;
        if (r1 != null && this.A01 < 0 && this.A02 == null && r1.A0E().A0n()) {
            return false;
        }
        String str = this.A02;
        int i2 = this.A01;
        int i3 = this.A00;
        ArrayList arrayList3 = r2.A0E;
        if (arrayList3 == null) {
            return false;
        }
        if (str != null || i2 >= 0) {
            i = arrayList3.size() - 1;
            while (i >= 0) {
                C004902f r12 = (C004902f) arrayList3.get(i);
                if ((str == null || !str.equals(r12.A0A)) && (i2 < 0 || i2 != r12.A04)) {
                    i--;
                } else if ((i3 & 1) != 0) {
                    while (true) {
                        i--;
                        if (i < 0) {
                            break;
                        }
                        C004902f r13 = (C004902f) arrayList3.get(i);
                        if (str == null || !str.equals(r13.A0A)) {
                            if (i2 < 0 || i2 != r13.A04) {
                                break;
                            }
                        }
                    }
                }
            }
            return false;
        } else if ((i3 & 1) == 0) {
            int size = arrayList3.size() - 1;
            if (size < 0) {
                return false;
            }
            arrayList.add(arrayList3.remove(size));
            arrayList2.add(Boolean.TRUE);
            return true;
        } else {
            i = -1;
        }
        if (i == arrayList3.size() - 1) {
            return false;
        }
        for (int size2 = arrayList3.size() - 1; size2 > i; size2--) {
            arrayList.add(arrayList3.remove(size2));
            arrayList2.add(Boolean.TRUE);
        }
        return true;
    }
}
