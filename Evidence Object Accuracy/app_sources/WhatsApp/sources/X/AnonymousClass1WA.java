package X;

import android.app.Activity;

/* renamed from: X.1WA  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WA {
    public final Activity A00;
    public final C21010wg A01;
    public final C14900mE A02;
    public final C90904Pr A03;
    public final AbstractC14440lR A04;
    public final C19820uj A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final boolean A0A;

    public AnonymousClass1WA(Activity activity, C21010wg r3, C14900mE r4, C90904Pr r5, AbstractC14440lR r6, C19820uj r7, String str, String str2, String str3, String str4, boolean z) {
        C16700pc.A0E(activity, 1);
        C16700pc.A0E(str, 4);
        C16700pc.A0E(str2, 5);
        C16700pc.A0E(str3, 6);
        C16700pc.A0E(str4, 7);
        C16700pc.A0E(r7, 8);
        C16700pc.A0E(r3, 9);
        C16700pc.A0E(r4, 10);
        C16700pc.A0E(r6, 11);
        this.A00 = activity;
        this.A03 = r5;
        this.A0A = z;
        this.A08 = str;
        this.A07 = str2;
        this.A06 = str3;
        this.A09 = str4;
        this.A05 = r7;
        this.A01 = r3;
        this.A02 = r4;
        this.A04 = r6;
    }

    public void A00() {
        this.A02.A0I(new RunnableC55722jC(this.A00, this.A03, this, null, null, null, true));
    }
}
