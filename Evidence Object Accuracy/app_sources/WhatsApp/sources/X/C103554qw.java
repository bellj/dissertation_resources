package X;

import android.content.Context;
import com.whatsapp.status.playback.MyStatusesActivity;

/* renamed from: X.4qw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103554qw implements AbstractC009204q {
    public final /* synthetic */ MyStatusesActivity A00;

    public C103554qw(MyStatusesActivity myStatusesActivity) {
        this.A00 = myStatusesActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
