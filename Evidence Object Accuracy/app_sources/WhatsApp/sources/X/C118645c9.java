package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.payments.ui.PaymentTransactionHistoryActivity;
import com.whatsapp.payments.ui.widget.PeerPaymentTransactionRow;
import java.util.Calendar;
import java.util.List;

/* renamed from: X.5c9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118645c9 extends AnonymousClass02M implements AnonymousClass2TX {
    public C125815rr A00 = null;
    public List A01;
    public final int A02;
    public final Context A03;
    public final AnonymousClass018 A04;
    public final C15650ng A05;
    public final AnonymousClass1In A06;
    public final C30931Zj A07;
    public final AnonymousClass6LY A08;
    public final C129795yJ A09;
    public final AnonymousClass14X A0A;

    public C118645c9(Context context, AnonymousClass018 r3, C15650ng r4, AnonymousClass1In r5, C30931Zj r6, AnonymousClass6LY r7, C129795yJ r8, AnonymousClass14X r9, List list, int i) {
        this.A03 = context;
        this.A01 = C12980iv.A0x(list);
        this.A06 = r5;
        this.A07 = r6;
        this.A05 = r4;
        this.A04 = r3;
        this.A0A = r9;
        this.A09 = r8;
        this.A08 = r7;
        this.A02 = i;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A01.size();
    }

    public PeerPaymentTransactionRow A0E(ViewGroup viewGroup, int i) {
        return new PeerPaymentTransactionRow(this.A03, this.A06, this.A02);
    }

    /* renamed from: A0F */
    public void ANF(C118735cI r3, int i) {
        r3.A00.setText(((PaymentTransactionHistoryActivity) this.A08).A0W.get(i).toString());
    }

    @Override // X.AnonymousClass2TX
    public int ABl(int i) {
        return ((AnonymousClass6L2) ((PaymentTransactionHistoryActivity) this.A08).A0W.get(i)).count;
    }

    @Override // X.AnonymousClass2TX
    public int ADH() {
        return ((PaymentTransactionHistoryActivity) this.A08).A0W.size();
    }

    @Override // X.AnonymousClass2TX
    public long ADI(int i) {
        return -((Calendar) ((PaymentTransactionHistoryActivity) this.A08).A0W.get(i)).getTimeInMillis();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0099, code lost:
        if (r5.A01(((X.AnonymousClass1IR) r9.A01.get(r11)).A06).equals(r5.A01(((X.AnonymousClass1IR) r9.A01.get(r6)).A06)) != false) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a1, code lost:
        if (r6 == r9.A01.size()) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00a3, code lost:
        r2.findViewById(com.whatsapp.R.id.divider).setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00af, code lost:
        r8 = ((com.whatsapp.payments.ui.PaymentTransactionHistoryActivity) r9.A08).A0L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b5, code lost:
        if (r8 == null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00b7, code lost:
        r0 = r9.A05.A0E(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00bd, code lost:
        if (r0 == null) goto L_0x010c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00bf, code lost:
        r7 = r0.A0I();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00c3, code lost:
        r6 = r9.A0A;
        r5 = r6.A0L(r3);
        r4 = r9.A04;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d0, code lost:
        if (X.C32751cg.A03(r4, r7, r8, true) == false) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00d2, code lost:
        r2.A0C.A0G(((com.whatsapp.payments.ui.PaymentTransactionHistoryActivity) r10.A01.A08).A0L, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00e1, code lost:
        if (r9.A00 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e3, code lost:
        X.C117295Zj.A0o(r2, r9, r3, 26);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e8, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00ed, code lost:
        if (X.C32751cg.A03(r4, r5, r8, true) == false) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ef, code lost:
        r1 = ((com.whatsapp.payments.ui.PaymentTransactionHistoryActivity) r10.A01.A08).A0L;
        r0 = r2.A0D;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00f9, code lost:
        r0.A0G(r1, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00fd, code lost:
        r5 = r6.A0I(r3);
        r1 = ((com.whatsapp.payments.ui.PaymentTransactionHistoryActivity) r10.A01.A08).A0L;
        r0 = r2.A0B;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x010c, code lost:
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r10, int r11) {
        /*
        // Method dump skipped, instructions count: 270
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C118645c9.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ AnonymousClass03U AOh(ViewGroup viewGroup) {
        boolean z = this instanceof C121935kW;
        Context context = this.A03;
        LayoutInflater layoutInflater = AnonymousClass12P.A00(context).getLayoutInflater();
        if (!z) {
            return new C118735cI(C117305Zk.A04(context, layoutInflater, viewGroup, R.layout.transaction_history_section_row));
        }
        return new C121915kU(C117305Zk.A04(context, layoutInflater, viewGroup, R.layout.india_transaction_history_section_row));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 2000) {
            return new C118695cE(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_transaction_interop_shimmer));
        }
        return new C118755cK(A0E(viewGroup, i), this);
    }

    @Override // X.AnonymousClass2TX
    public boolean AWm(MotionEvent motionEvent, AnonymousClass03U r3, int i) {
        return false;
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        AnonymousClass1IR r2 = (AnonymousClass1IR) this.A01.get(i);
        return (r2.A03 != 1000 || !r2.A0P) ? 1000 : 2000;
    }
}
