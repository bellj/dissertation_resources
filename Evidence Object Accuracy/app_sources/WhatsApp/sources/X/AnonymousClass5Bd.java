package X;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import java.util.Locale;

/* renamed from: X.5Bd  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5Bd implements Comparable {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final int A07;
    public final int A08;
    public final C77343n8 A09;
    public final String A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;

    public AnonymousClass5Bd(C100614mC r8, C77343n8 r9, int i) {
        int i2;
        int i3;
        String[] strArr;
        String obj;
        int length;
        int i4;
        this.A09 = r9;
        String str = r8.A0S;
        this.A0A = (TextUtils.isEmpty(str) || TextUtils.equals(str, "und")) ? null : str;
        int i5 = 0;
        this.A0D = (i & 7) != 4 ? false : true;
        int i6 = 0;
        while (true) {
            AnonymousClass1Mr r1 = ((C100594mA) r9).A03;
            i2 = Integer.MAX_VALUE;
            if (i6 >= r1.size()) {
                i6 = Integer.MAX_VALUE;
                i3 = 0;
                break;
            }
            i3 = C77353n9.A00(r8, C12960it.A0g(r1, i6), false);
            if (i3 > 0) {
                break;
            }
            i6++;
        }
        this.A04 = i6;
        this.A05 = i3;
        this.A07 = Integer.bitCount(r8.A0D & ((C100594mA) r9).A01);
        boolean z = true;
        this.A0B = C12960it.A1S(r8.A0G & 1);
        int i7 = r8.A06;
        this.A01 = i7;
        this.A08 = r8.A0F;
        int i8 = r8.A05;
        this.A00 = i8;
        if ((i8 != -1 && i8 > r9.A00) || (i7 != -1 && i7 > r9.A01)) {
            z = false;
        }
        this.A0C = z;
        Configuration configuration = Resources.getSystem().getConfiguration();
        int i9 = AnonymousClass3JZ.A01;
        if (i9 >= 24) {
            strArr = AnonymousClass3JZ.A0I(configuration);
        } else {
            strArr = new String[1];
            Locale locale = configuration.locale;
            if (i9 >= 21) {
                obj = AnonymousClass3JZ.A0B(locale);
            } else {
                obj = locale.toString();
            }
            strArr[0] = obj;
        }
        int i10 = 0;
        while (true) {
            length = strArr.length;
            if (i10 >= length) {
                break;
            }
            strArr[i10] = AnonymousClass3JZ.A0A(strArr[i10]);
            i10++;
        }
        int i11 = 0;
        while (true) {
            if (i11 < length) {
                i4 = C77353n9.A00(r8, strArr[i11], false);
                if (i4 > 0) {
                    break;
                }
                i11++;
            } else {
                i11 = Integer.MAX_VALUE;
                i4 = 0;
                break;
            }
        }
        this.A02 = i11;
        this.A03 = i4;
        while (true) {
            AnonymousClass1Mr r2 = r9.A0E;
            if (i5 < r2.size()) {
                String str2 = r8.A0T;
                if (str2 != null && str2.equals(r2.get(i5))) {
                    i2 = i5;
                    break;
                }
                i5++;
            } else {
                break;
            }
        }
        this.A06 = i2;
    }

    /* renamed from: A00 */
    public int compareTo(AnonymousClass5Bd r8) {
        AbstractC112285Cu r6;
        AbstractC112285Cu reverse;
        AbstractC112285Cu r0;
        boolean z = this.A0C;
        if (!z || !this.A0D) {
            r6 = C77353n9.A03;
            reverse = r6.reverse();
        } else {
            reverse = C77353n9.A03;
            r6 = reverse;
        }
        AbstractC95284dR A00 = AbstractC112285Cu.A00(AbstractC112285Cu.A00(AbstractC95284dR.start().compareFalseFirst(this.A0D, r8.A0D), Integer.valueOf(this.A04), r8.A04).compare(this.A05, r8.A05).compare(this.A07, r8.A07).compareFalseFirst(z, r8.A0C), Integer.valueOf(this.A06), r8.A06);
        Integer valueOf = Integer.valueOf(this.A00);
        Integer valueOf2 = Integer.valueOf(r8.A00);
        if (this.A09.A0Q) {
            r0 = r6.reverse();
        } else {
            r0 = C77353n9.A04;
        }
        AbstractC95284dR compare = AbstractC112285Cu.A00(A00.compare(valueOf, valueOf2, r0).compareFalseFirst(this.A0B, r8.A0B), Integer.valueOf(this.A02), r8.A02).compare(this.A03, r8.A03).compare(Integer.valueOf(this.A01), Integer.valueOf(r8.A01), reverse).compare(Integer.valueOf(this.A08), Integer.valueOf(r8.A08), reverse);
        if (!AnonymousClass3JZ.A0H(this.A0A, r8.A0A)) {
            reverse = C77353n9.A04;
        }
        return compare.compare(valueOf, valueOf2, reverse).result();
    }
}
