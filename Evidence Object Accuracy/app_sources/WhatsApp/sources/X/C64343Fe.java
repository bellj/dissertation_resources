package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S0620000_I1;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape2S0500000_I1;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.ArrayList;
import java.util.Set;

/* renamed from: X.3Fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64343Fe {
    public int A00;
    public int A01;
    public C61102zP A02;
    public final Context A03;
    public final AnonymousClass18U A04;
    public final C22640zP A05;
    public final C91564Sf A06;
    public final C17990rj A07;
    public final AnonymousClass1BK A08;
    public final AnonymousClass19O A09;
    public final WebPagePreviewView A0A;

    public C64343Fe(Context context, AnonymousClass18U r4, C22640zP r5, C91564Sf r6, C17990rj r7, AnonymousClass1BK r8, AnonymousClass19O r9) {
        this.A03 = context;
        this.A04 = r4;
        this.A08 = r8;
        this.A09 = r9;
        this.A05 = r5;
        this.A0A = new WebPagePreviewView(context);
        this.A06 = r6;
        this.A02 = new C61102zP(AnonymousClass3GD.A00(context));
        this.A07 = r7;
    }

    public final float A00(AbstractC15340mz r6, AnonymousClass3IJ r7) {
        byte[] A17;
        if (r7.A01 == 4) {
            return 0.5625f;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        C32361c2 r0 = r6.A0N;
        if (r0 != null) {
            A17 = r0.A00;
            if (A17 == null) {
                A17 = r0.A0C;
            }
        } else {
            A17 = r6 instanceof C28861Ph ? ((C28861Ph) r6).A17() : new byte[0];
        }
        BitmapFactory.decodeByteArray(A17, 0, A17.length, options);
        float f = ((float) options.outHeight) / ((float) options.outWidth);
        if (f < 1.0f) {
            return Math.max(f, 0.5625f);
        }
        return Math.min(f, 1.0f);
    }

    public final void A01(AnonymousClass1OY r7, AbstractC15340mz r8, AnonymousClass3IJ r9, AbstractC35401hl r10, boolean z, boolean z2, boolean z3) {
        int i;
        Drawable A00;
        Set A002 = this.A08.A00(r8.A0C(), r8, r9.A03);
        if (A002 != null) {
            WebPagePreviewView webPagePreviewView = this.A0A;
            webPagePreviewView.A02();
            C53082ci r2 = ((AbstractC28551Oa) r7).A0I;
            if (r2 != null && AnonymousClass4AT.FORWARD == r2.A00) {
                r2.A01.setVisibility(8);
            }
            C12990iw.A1C(webPagePreviewView, this, r9, A002, 11);
        } else if (z) {
            A03(r8, r9, r10, z2);
        } else {
            AnonymousClass19O r4 = this.A09;
            if (C65043Hx.A01(r8, r4, z2)) {
                WebPagePreviewView webPagePreviewView2 = this.A0A;
                webPagePreviewView2.A00();
                if (z3) {
                    i = r9.A00;
                } else {
                    i = 0;
                }
                webPagePreviewView2.setImageLargeLogo(i);
                C32361c2 r22 = r8.A0N;
                ViewGroup.LayoutParams layoutParams = webPagePreviewView2.A07.getLayoutParams();
                if (r22 == null) {
                    layoutParams.width = -2;
                    webPagePreviewView2.A07.requestLayout();
                    r4.A07(webPagePreviewView2.A0K, r8, new C60722yU(this));
                } else {
                    layoutParams.width = -1;
                    webPagePreviewView2.A07.requestLayout();
                    int A04 = C12960it.A04(webPagePreviewView2, AnonymousClass3GD.A01(this.A03, 72));
                    webPagePreviewView2.setImageLargeThumbFrameHeight((int) (((float) A04) * A00(r8, r9)));
                    r4.A0C(webPagePreviewView2.A0K, r8, new C70323b6(this, A04), false);
                }
                AbstractView$OnClickListenerC34281fs.A03(webPagePreviewView2, this, r9, r8, 7);
            } else {
                WebPagePreviewView webPagePreviewView3 = this.A0A;
                webPagePreviewView3.A02();
                AbstractView$OnClickListenerC34281fs.A03(webPagePreviewView3, this, r9, r8, 7);
            }
        }
        FrameLayout frameLayout = (FrameLayout) this.A0A.findViewById(R.id.link_preview_frame);
        boolean z4 = r8.A0z.A02;
        Context context = this.A03;
        if (z4) {
            A00 = C92994Ym.A01(context);
        } else {
            A00 = C92994Ym.A00(context);
        }
        frameLayout.setForeground(A00);
    }

    public void A02(AnonymousClass1OY r21, AbstractC15340mz r22, AnonymousClass3IJ r23, AbstractC35401hl r24, boolean z, boolean z2, boolean z3, boolean z4) {
        C61102zP r0;
        ArrayList AGT;
        ArrayList AGT2;
        int A00 = AnonymousClass3GD.A00(this.A03);
        if (z4) {
            r0 = new AnonymousClass42M(A00);
        } else {
            r0 = new C61102zP(A00);
        }
        this.A02 = r0;
        A01(r21, r22, r23, r24, z, z2, z3);
        if (r22.A12(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH)) {
            C91564Sf r3 = this.A06;
            WebPagePreviewView webPagePreviewView = this.A0A;
            AbstractC13890kV r1 = ((AbstractC28551Oa) r21).A0a;
            if (r1 == null) {
                AGT2 = null;
            } else {
                AGT2 = r1.AGT();
            }
            C63313Bc r8 = new C63313Bc(r21, this, r22, r23, r24, z, z2, z3);
            boolean A07 = this.A05.A07();
            C32361c2 r12 = r22.A0N;
            if (r12 != null) {
                AnonymousClass2KT r5 = new AnonymousClass2KT(r3.A01, r12);
                r5.A01 = z;
                webPagePreviewView.setTag(new C92644Wt(r5, r22));
                webPagePreviewView.A0A(r5, AGT2, z2, A07);
                if (r12.A07 != null && r12.A00 == null) {
                    Set set = r3.A04;
                    String str = r22.A0z.A01;
                    if (!set.contains(str)) {
                        set.add(str);
                        r3.A03.Ab4(new RunnableBRunnable0Shape0S0620000_I1(r3, webPagePreviewView, r5, r22, AGT2, r8, 1, z2, A07), str);
                    }
                }
            }
        } else if (r22 instanceof C28861Ph) {
            WebPagePreviewView webPagePreviewView2 = this.A0A;
            C28861Ph r32 = (C28861Ph) r22;
            AbstractC13890kV r13 = ((AbstractC28551Oa) r21).A0a;
            if (r13 == null) {
                AGT = null;
            } else {
                AGT = r13.AGT();
            }
            boolean A01 = C65043Hx.A01(r22, this.A09, z2);
            boolean A072 = this.A05.A07();
            String str2 = r23.A02;
            String str3 = r23.A03;
            webPagePreviewView2.A0B(r32, str2, str3, str3, AGT, A01, z, A072);
        }
    }

    public final void A03(AbstractC15340mz r12, AnonymousClass3IJ r13, AbstractC35401hl r14, boolean z) {
        AbstractC41521tf r1;
        WebPagePreviewView webPagePreviewView = this.A0A;
        AnonymousClass2Bd.A02(r13, webPagePreviewView);
        AnonymousClass4NS r8 = new AnonymousClass4NS(this, r12);
        AnonymousClass1IS AC5 = r14.AC5();
        if (AC5 != null && AC5.equals(r12.A0z)) {
            r14.AcB(r8);
            int AC4 = r14.AC4();
            if (AC4 == 1) {
                webPagePreviewView.A05(1.0f, 0.0f, 0.0f, 0.0f);
            } else if (AC4 == 2) {
                webPagePreviewView.A05(0.0f, 1.0f, 1.0f, 0.67f);
            } else if (AC4 == 3) {
                webPagePreviewView.A05(0.0f, 1.0f, 0.0f, 0.0f);
            }
        }
        Bitmap[] bitmapArr = new Bitmap[1];
        webPagePreviewView.setOnClickListener(new ViewOnClickCListenerShape2S0500000_I1(this, r12, r13, r8, bitmapArr, 1));
        AnonymousClass19O r4 = this.A09;
        if (C65043Hx.A01(r12, r4, z)) {
            r1 = new C60732yV(this, bitmapArr);
        } else {
            int A04 = C12960it.A04(webPagePreviewView, AnonymousClass3GD.A01(this.A03, 72));
            webPagePreviewView.setVideoLargeThumbFrameHeight((int) (((float) A04) * A00(r12, r13)));
            r1 = new C70363bA(this, bitmapArr, A04);
        }
        if (r12.A0N != null) {
            r4.A0C(webPagePreviewView.A0C, r12, r1, true);
        } else if (r12 instanceof C28861Ph) {
            r4.A07(webPagePreviewView.A0C, r12, r1);
        }
    }
}
