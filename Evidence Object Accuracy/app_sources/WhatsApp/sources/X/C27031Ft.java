package X;

import android.net.Uri;
import android.os.Environment;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.1Ft  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27031Ft implements AbstractC15840nz {
    public final C15570nT A00;
    public final C15820nx A01;
    public final C15810nw A02;
    public final C17050qB A03;
    public final C16590pI A04;
    public final C15890o4 A05;
    public final C19490uC A06;
    public final C21780xy A07;
    public final C252318p A08;
    public final AbstractC15870o2 A09;
    public final C16600pJ A0A;
    public final AnonymousClass15D A0B;

    @Override // X.AbstractC15840nz
    public String AAp() {
        return "wallpaper-v2";
    }

    public C27031Ft(C15570nT r1, C15820nx r2, C15810nw r3, C17050qB r4, C16590pI r5, C15890o4 r6, C19490uC r7, C21780xy r8, C252318p r9, AbstractC15870o2 r10, C16600pJ r11, AnonymousClass15D r12) {
        this.A04 = r5;
        this.A0B = r12;
        this.A00 = r1;
        this.A02 = r3;
        this.A06 = r7;
        this.A01 = r2;
        this.A03 = r4;
        this.A05 = r6;
        this.A09 = r10;
        this.A0A = r11;
        this.A08 = r9;
        this.A07 = r8;
    }

    @Override // X.AbstractC15840nz
    public boolean A6K() {
        EnumC16570pG r13;
        String obj;
        C15820nx r8 = this.A01;
        if (r8.A04()) {
            r13 = EnumC16570pG.A07;
        } else {
            r13 = EnumC16570pG.A06;
        }
        this.A08.A00(r13);
        if (!this.A05.A0A(Environment.getExternalStorageState())) {
            StringBuilder sb = new StringBuilder("wallpaper/v2/backup/sdcard_unavailable ");
            sb.append(Environment.getExternalStorageState());
            Log.i(sb.toString());
            return false;
        }
        Set<String> AAi = this.A09.AAi();
        HashSet hashSet = new HashSet(AAi.size());
        for (String str : AAi) {
            Uri parse = Uri.parse(str);
            if (parse.getPath() != null) {
                File file = new File(parse.getPath());
                if (file.exists()) {
                    hashSet.add(file);
                }
            }
        }
        C15810nw r6 = this.A02;
        List<File> A02 = AbstractC15850o0.A02(r6);
        EnumC16570pG r5 = EnumC16570pG.A08;
        File A022 = r6.A02();
        if (r13 == r5) {
            obj = "Wallpapers";
        } else {
            StringBuilder sb2 = new StringBuilder("wallpapers.backup.crypt");
            sb2.append(r13.version);
            obj = sb2.toString();
        }
        File file2 = new File(A022, obj);
        for (File file3 : A02) {
            if (!file3.equals(file2) && file3.exists()) {
                C14350lI.A0N(file3);
            }
        }
        if (r5 == r13) {
            File file4 = new File(r6.A02(), "Wallpapers");
            if (!file4.exists()) {
                file4.mkdirs();
            }
            Iterator it = hashSet.iterator();
            while (it.hasNext()) {
                File file5 = (File) it.next();
                File file6 = new File(file4, file5.getName());
                File absoluteFile = file6.getAbsoluteFile();
                if (!absoluteFile.exists()) {
                    absoluteFile.mkdirs();
                }
                try {
                    FileChannel channel = new FileInputStream(file5).getChannel();
                    WritableByteChannel newChannel = Channels.newChannel(this.A03.A00(file6));
                    try {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("wallpaper/v2/backup/size ");
                        sb3.append(file5.length());
                        Log.i(sb3.toString());
                        C14350lI.A0H(channel, newChannel);
                        if (newChannel != null) {
                            newChannel.close();
                        }
                        if (channel != null) {
                            channel.close();
                        }
                    } catch (Throwable th) {
                        if (newChannel != null) {
                            try {
                                newChannel.close();
                            } catch (Throwable unused) {
                            }
                        }
                        throw th;
                        break;
                    }
                } catch (Exception e) {
                    Log.w("wallpaper/v2/backup/error ", e);
                }
            }
            return true;
        }
        try {
            AnonymousClass15D r14 = this.A0B;
            C15570nT r52 = this.A00;
            C19490uC r10 = this.A06;
            C17050qB r9 = this.A03;
            C16600pJ r12 = this.A0A;
            AbstractC37461mR A023 = C33231df.A00(r52, new C33211dd(file2), null, r8, r9, r10, this.A07, r12, r13, r14).A02(this.A04.A00);
            Iterator it2 = hashSet.iterator();
            while (it2.hasNext()) {
                File file7 = (File) it2.next();
                if (A023 == null) {
                    Log.e("wallpaper/v2/backup failed to create writer");
                    return true;
                }
                A023.AgB(file7);
            }
            if (A023 == null) {
                return true;
            }
            A023.close();
            return true;
        } catch (Exception e2) {
            Log.e("wallpaper/v2/backup failed", e2);
            return true;
        }
    }
}
