package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.4eG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95734eG implements Animator.AnimatorListener {
    public final /* synthetic */ AnimatorSet A00;
    public final /* synthetic */ MessageReplyActivity A01;
    public final /* synthetic */ int[] A02;

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }

    public C95734eG(AnimatorSet animatorSet, MessageReplyActivity messageReplyActivity, int[] iArr) {
        this.A01 = messageReplyActivity;
        this.A00 = animatorSet;
        this.A02 = iArr;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00.removeAllListeners();
        this.A01.A2k(AbstractC36671kL.A06(this.A02), 15);
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.removeAllListeners();
        this.A01.A2k(AbstractC36671kL.A06(this.A02), 15);
    }
}
