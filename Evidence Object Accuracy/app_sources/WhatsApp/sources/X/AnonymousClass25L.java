package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25L  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25L extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25L A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public String A01 = "";

    static {
        AnonymousClass25L r0 = new AnonymousClass25L();
        A02 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A01);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A01);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
