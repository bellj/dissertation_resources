package X;

import android.os.Bundle;
import android.os.Environment;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.10J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10J {
    public final C22730zY A00;
    public final AnonymousClass10N A01;
    public final C14820m6 A02;
    public final AtomicLong A03 = new AtomicLong(0);
    public final AtomicLong A04 = new AtomicLong(0);
    public final AtomicLong A05 = new AtomicLong(0);
    public final AtomicLong A06 = new AtomicLong(0);
    public final AtomicLong A07 = new AtomicLong(0);

    public AnonymousClass10J(C22730zY r4, AnonymousClass10N r5, C14820m6 r6) {
        this.A02 = r6;
        this.A00 = r4;
        this.A01 = r5;
    }

    public Bundle A00() {
        Bundle bundle = new Bundle();
        bundle.putLong("total_bytes_to_be_downloaded", this.A05.get());
        bundle.putLong("total_bytes_downloaded", this.A04.get());
        bundle.putLong("total_bytes_to_be_uploaded", this.A06.get());
        return bundle;
    }

    public C44781zX A01() {
        long j = this.A06.get();
        long j2 = this.A07.get();
        long j3 = this.A05.get();
        long j4 = this.A04.get();
        this.A03.get();
        return new C44781zX(j, j2, j3, j4);
    }

    public void A02(AnonymousClass10L r18) {
        AnonymousClass10N r4 = this.A01;
        r4.A03(r18);
        C14820m6 r6 = this.A02;
        if (!C44771zW.A0G(r6)) {
            C22730zY r2 = this.A00;
            if (!r2.A0c.get()) {
                if (r2.A0e.get() || C44771zW.A0H(r6)) {
                    if (!r2.A0i.get()) {
                        int i = r2.A02;
                        long j = this.A04.get();
                        long j2 = this.A05.get();
                        if (i == 0) {
                            r18.ASa(j, j2);
                        } else {
                            r18.ASW(j, j2);
                        }
                    } else if (!r2.A07) {
                        r18.ASX(this.A04.get(), this.A05.get());
                    } else if (!r2.A09) {
                        boolean equals = "unmounted".equals(Environment.getExternalStorageState());
                        long j3 = this.A04.get();
                        long j4 = this.A05.get();
                        if (equals) {
                            r18.ASZ(j3, j4);
                        } else {
                            r18.ASY(j3, j4);
                        }
                    } else {
                        AtomicLong atomicLong = this.A05;
                        if (atomicLong.get() > 0) {
                            r18.ASd(this.A04.get(), this.A03.get(), atomicLong.get());
                        } else {
                            r18.ASc();
                        }
                    }
                    int A03 = r6.A03();
                    Bundle A00 = A00();
                    for (AnonymousClass10L r0 : r4.A01()) {
                        r0.APx(A03, A00);
                    }
                    return;
                }
                if (C44771zW.A0I(r6)) {
                    StringBuilder sb = new StringBuilder("gdrive-service/observer/registered/error/");
                    sb.append(C44771zW.A04(r6.A03()));
                    Log.i(sb.toString());
                    return;
                }
                r4.A07(r6.A03(), A00());
            }
        }
        C22730zY r22 = this.A00;
        if (!r22.A0g.get()) {
            int i2 = r22.A01;
            long j5 = this.A07.get();
            long j6 = this.A06.get();
            if (i2 == 0) {
                r18.AN3(j5, j6);
            } else {
                r18.AMz(j5, j6);
            }
        } else if (!r22.A05) {
            r18.AN0(this.A07.get(), this.A06.get());
        } else if (!r22.A09) {
            boolean equals2 = "unmounted".equals(Environment.getExternalStorageState());
            long j7 = this.A07.get();
            long j8 = this.A06.get();
            if (equals2) {
                r18.AN2(j7, j8);
            } else {
                r18.AN1(j7, j8);
            }
        } else {
            AtomicLong atomicLong2 = this.A06;
            if (atomicLong2.get() > 0) {
                r18.AN6(this.A07.get(), atomicLong2.get());
            } else if (C44771zW.A0G(r6)) {
                r18.AN5();
            } else {
                r18.AVb();
            }
        }
        r4.A07(r6.A03(), A00());
    }
}
