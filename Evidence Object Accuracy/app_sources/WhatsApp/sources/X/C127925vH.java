package X;

import android.content.Context;

/* renamed from: X.5vH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127925vH {
    public final Context A00;
    public final AnonymousClass102 A01;
    public final C121035h9 A02;
    public final C130205yy A03;
    public final C123735nm A04;

    public C127925vH(Context context, AnonymousClass102 r3, C130205yy r4, C123735nm r5) {
        this.A00 = context;
        this.A04 = r5;
        C121035h9 r0 = r5.A00;
        AnonymousClass009.A05(r0);
        this.A02 = r0;
        this.A03 = r4;
        this.A01 = r3;
    }
}
