package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.lang.reflect.Method;

/* renamed from: X.0B8  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0B8 extends ViewGroup {
    public static Method A03;
    public View A00;
    public ViewGroup A01;
    public AnonymousClass0ZQ A02;

    @Override // android.view.ViewGroup, android.view.View
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    static {
        try {
            Class cls = Integer.TYPE;
            A03 = ViewGroup.class.getDeclaredMethod("invalidateChildInParentFast", cls, cls, Rect.class);
        } catch (NoSuchMethodException unused) {
        }
    }

    public AnonymousClass0B8(Context context, View view, ViewGroup viewGroup, AnonymousClass0ZQ r5) {
        super(context);
        this.A01 = viewGroup;
        this.A00 = view;
        setRight(viewGroup.getWidth());
        setBottom(viewGroup.getHeight());
        viewGroup.addView(this);
        this.A02 = r5;
    }

    public void A00(View view) {
        if (view.getParent() instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view.getParent();
            ViewGroup viewGroup2 = this.A01;
            if (!(viewGroup == viewGroup2 || viewGroup.getParent() == null || !AnonymousClass028.A0q(viewGroup))) {
                int[] iArr = new int[2];
                int[] iArr2 = new int[2];
                viewGroup.getLocationOnScreen(iArr);
                viewGroup2.getLocationOnScreen(iArr2);
                AnonymousClass028.A0X(view, iArr[0] - iArr2[0]);
                AnonymousClass028.A0Y(view, iArr[1] - iArr2[1]);
            }
            viewGroup.removeView(view);
            if (view.getParent() != null) {
                viewGroup.removeView(view);
            }
        }
        super.addView(view, getChildCount() - 1);
    }

    public void A01(View view) {
        super.removeView(view);
        if (getChildCount() == 0) {
            this.A01.removeView(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void dispatchDraw(Canvas canvas) {
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        this.A01.getLocationOnScreen(iArr);
        View view = this.A00;
        view.getLocationOnScreen(iArr2);
        canvas.translate((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
        canvas.clipRect(new Rect(0, 0, view.getWidth(), view.getHeight()));
        super.dispatchDraw(canvas);
    }

    @Override // android.view.ViewGroup, android.view.ViewParent
    public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
        ViewGroup viewGroup = this.A01;
        if (viewGroup == null) {
            return null;
        }
        rect.offset(iArr[0], iArr[1]);
        iArr[0] = 0;
        iArr[1] = 0;
        int[] iArr2 = new int[2];
        int[] iArr3 = new int[2];
        viewGroup.getLocationOnScreen(iArr2);
        this.A00.getLocationOnScreen(iArr3);
        int i = iArr3[1] - iArr2[1];
        rect.offset(new int[]{iArr3[0] - iArr2[0], i}[0], i);
        return super.invalidateChildInParent(iArr, rect);
    }

    @Override // android.graphics.drawable.Drawable.Callback, android.view.View
    public void invalidateDrawable(Drawable drawable) {
        invalidate(drawable.getBounds());
    }

    @Override // android.view.View
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable);
    }
}
