package X;

import android.os.Message;
import java.io.OutputStream;

/* renamed from: X.23V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass23V extends OutputStream {
    public final C18790t3 A00;
    public final OutputStream A01;
    public final Integer A02;
    public final Integer A03;

    public AnonymousClass23V(C18790t3 r1, OutputStream outputStream, Integer num, Integer num2) {
        this.A01 = outputStream;
        this.A02 = num;
        this.A00 = r1;
        this.A03 = num2;
    }

    public final void A00(int i) {
        AnonymousClass1N0 r1;
        Integer num = this.A02;
        if (num != null) {
            C18790t3 r3 = this.A00;
            int intValue = num.intValue();
            AnonymousClass1N1 r12 = r3.A00;
            boolean z = false;
            if (r12 != null) {
                z = true;
            }
            AnonymousClass009.A0F(z);
            if (i >= 0) {
                Message.obtain(r12, 2, intValue, i).sendToTarget();
                r3.A02();
            }
        }
        C18790t3 r13 = this.A00;
        int intValue2 = this.A03.intValue();
        AnonymousClass16B r5 = r13.A06;
        if (((long) i) >= 0 && (r1 = r5.A00) != null) {
            AnonymousClass009.A0F(true);
            Message.obtain(r1, 2, intValue2, i).sendToTarget();
            r5.A00();
        }
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01.close();
    }

    @Override // java.io.OutputStream, java.io.Flushable
    public void flush() {
        this.A01.flush();
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        this.A01.write(i);
        A00(1);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr) {
        this.A01.write(bArr);
        A00(bArr.length);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        this.A01.write(bArr, i, i2);
        A00(i2);
    }
}
