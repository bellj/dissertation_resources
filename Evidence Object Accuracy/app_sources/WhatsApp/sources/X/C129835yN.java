package X;

import android.content.Context;

/* renamed from: X.5yN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129835yN {
    public final Context A00;
    public final C16590pI A01;

    public C129835yN(C16590pI r2) {
        this.A01 = r2;
        this.A00 = r2.A00;
    }

    public C122945mM A00(CharSequence charSequence, int i) {
        return new C122945mM(null, this.A00.getString(i), charSequence, 0, false);
    }

    public C122945mM A01(CharSequence charSequence, int i, int i2) {
        return new C122945mM(C16590pI.A00(this.A01).getDrawable(i2), this.A00.getString(i), charSequence, 0, false);
    }
}
