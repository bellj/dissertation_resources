package X;

import android.animation.ValueAnimator;
import org.npci.commonlibrary.widget.FormItemEditText;

/* renamed from: X.61r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1312061r implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ FormItemEditText A00;

    public C1312061r(FormItemEditText formItemEditText) {
        this.A00 = formItemEditText;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        FormItemEditText formItemEditText = this.A00;
        formItemEditText.A09.setAlpha(((Number) valueAnimator.getAnimatedValue()).intValue());
        formItemEditText.invalidate();
    }
}
