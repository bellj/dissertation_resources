package X;

/* renamed from: X.6Au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133446Au implements AnonymousClass6MV {
    public final /* synthetic */ AbstractC136296Lz A00;
    public final /* synthetic */ AnonymousClass6M0 A01;
    public final /* synthetic */ AnonymousClass605 A02;

    public C133446Au(AbstractC136296Lz r1, AnonymousClass6M0 r2, AnonymousClass605 r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass6MV
    public void APo(C452120p r2) {
        this.A01.AVD(r2);
    }

    @Override // X.AnonymousClass6MV
    public void AVE(AnonymousClass6B7 r3) {
        this.A00.AVF(new C128545wH(r3));
    }
}
