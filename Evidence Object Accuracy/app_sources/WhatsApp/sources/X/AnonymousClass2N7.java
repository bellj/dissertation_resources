package X;

import java.util.Map;

/* renamed from: X.2N7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2N7 {
    public final int A00;
    public final String A01;
    public final Map A02;

    public AnonymousClass2N7(String str, int i, Map map) {
        this.A01 = str;
        this.A02 = map;
        this.A00 = i;
    }
}
