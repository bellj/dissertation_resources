package X;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.2fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53932fp extends AnonymousClass015 implements AbstractC33511eG {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final Map A03 = new LinkedHashMap();

    public boolean A04(int i) {
        Number number = (Number) this.A02.A01();
        return number != null && number.intValue() == i;
    }

    @Override // X.AbstractC33511eG
    public void ATJ(AnonymousClass1KZ r4) {
        Map map = this.A03;
        String str = r4.A0D;
        AnonymousClass017 r1 = (AnonymousClass017) map.get(str);
        if (r1 != null && r1.A01() != null) {
            r1.A0B(new C90434Nw(r4, str));
        }
    }

    @Override // X.AbstractC33511eG
    public void ATK(List list) {
        Map map = this.A03;
        map.clear();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1KZ r0 = (AnonymousClass1KZ) it.next();
            String str = r0.A0D;
            map.put(str, new AnonymousClass016(new C90434Nw(r0, str)));
        }
        C12960it.A1A(this.A02, 2);
    }

    @Override // X.AbstractC33511eG
    public void ATL() {
        C12960it.A1A(this.A02, 3);
    }

    @Override // X.AbstractC33511eG
    public void ATM(String str) {
        AnonymousClass017 r2 = (AnonymousClass017) this.A03.get(str);
        if (r2 != null && r2.A01() != null) {
            r2.A0B(new C90434Nw(null, str));
        }
    }
}
