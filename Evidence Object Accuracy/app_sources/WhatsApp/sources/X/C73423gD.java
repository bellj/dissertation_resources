package X;

import android.telephony.PhoneStateListener;
import com.whatsapp.util.Log;

/* renamed from: X.3gD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73423gD extends PhoneStateListener {
    public final /* synthetic */ AnonymousClass11P A00;

    public C73423gD(AnonymousClass11P r1) {
        this.A00 = r1;
    }

    @Override // android.telephony.PhoneStateListener
    public void onCallStateChanged(int i, String str) {
        String str2;
        if (i == 0) {
            str2 = "phone/state idle";
        } else if (i == 1) {
            Log.i("phone/state ringing");
            this.A00.A04();
            return;
        } else if (i == 2) {
            str2 = "phone/state offhook";
        } else {
            return;
        }
        Log.i(str2);
    }
}
