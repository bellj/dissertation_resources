package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.HomeActivity;
import com.whatsapp.util.Log;

/* renamed from: X.2MJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2MJ extends BroadcastReceiver {
    public final /* synthetic */ HomeActivity A00;
    public final /* synthetic */ Runnable A01;

    public AnonymousClass2MJ(HomeActivity homeActivity, Runnable runnable) {
        this.A00 = homeActivity;
        this.A01 = runnable;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Log.i("home/resume/unlocked received ACTION_USER_PRESENT");
        try {
            HomeActivity homeActivity = this.A00;
            homeActivity.unregisterReceiver(this);
            C14900mE r0 = ((ActivityC13810kN) homeActivity).A05;
            Runnable runnable = this.A01;
            r0.A0G(runnable);
            ((ActivityC13810kN) homeActivity).A05.A0J(runnable, 500);
            homeActivity.A07 = null;
        } catch (Exception e) {
            Log.e("home/resume/unlocked received ACTION_USER_PRESENT ", e);
        }
    }
}
