package X;

import android.app.Activity;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0201000_I1;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.whatsapp.R;
import com.whatsapp.ctwa.icebreaker.ui.IcebreakerBubbleView;

/* renamed from: X.238  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass238 {
    public View A00;
    public View A01;
    public ViewGroup A02;
    public ListView A03;
    public TextView A04;
    public RunnableBRunnable0Shape1S0201000_I1 A05;
    public ShimmerFrameLayout A06;
    public AnonymousClass3CH A07;
    public AnonymousClass239 A08;
    public IcebreakerBubbleView A09;
    public AnonymousClass2AS A0A;
    public AbstractC14640lm A0B;
    public C252718t A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public final int A0H;
    public final int A0I;
    public final Activity A0J;
    public final TextWatcher A0K = new AnonymousClass35z(this);
    public final C14900mE A0L;
    public final C18790t3 A0M;
    public final C22640zP A0N;
    public final AnonymousClass2IA A0O;
    public final C14310lE A0P;
    public final C32701cb A0Q;
    public final C254319j A0R;
    public final boolean A0S;

    public AnonymousClass238(Activity activity, C14900mE r4, C18790t3 r5, C22640zP r6, AnonymousClass2IA r7, C14310lE r8, C32701cb r9, C254319j r10, C253419a r11, C252718t r12) {
        this.A0J = activity;
        this.A0L = r4;
        this.A0M = r5;
        this.A0R = r10;
        this.A0N = r6;
        this.A0O = r7;
        this.A0P = r8;
        this.A0H = R.id.icebreaker_container;
        this.A0Q = r9;
        this.A0C = r12;
        this.A0F = false;
        C14850m9 r1 = r11.A00;
        this.A0S = r1.A07(863);
        this.A0I = r1.A07(1293) ? 2 : 1;
    }

    public void A00() {
        if (this.A0F) {
            int i = this.A0I;
            if (i == 2) {
                AnonymousClass2AS r1 = this.A0A;
                AnonymousClass009.A03(r1);
                r1.setVisibility(8);
                this.A08.A07.A01.setVisibility(0);
            } else {
                IcebreakerBubbleView icebreakerBubbleView = this.A09;
                AnonymousClass009.A03(icebreakerBubbleView);
                icebreakerBubbleView.setVisibility(8);
            }
            if (this.A0F) {
                this.A0F = false;
                if (i == 2) {
                    this.A02.removeView(this.A01);
                    this.A02.removeView(this.A0A);
                    this.A04.removeTextChangedListener(this.A0K);
                }
            }
        }
    }

    public boolean A01() {
        if (this.A0F && this.A0I == 2) {
            AnonymousClass2AS r2 = this.A0A;
            AnonymousClass009.A03(r2);
            for (int i = 0; i < this.A02.getChildCount(); i++) {
                if (this.A02.getChildAt(i) == r2 && r2.getVisibility() == 0) {
                    return true;
                }
            }
        }
        return false;
    }
}
