package X;

import X.AbstractC001200n;
import X.AnonymousClass02B;
import X.AnonymousClass074;
import X.C117885au;
import X.C12970iu;
import com.facebook.redex.IDxObserverShape5S0200000_3_I1;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.5au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117885au extends AnonymousClass016 {
    public WeakReference A00 = null;
    public final AtomicBoolean A01 = new AtomicBoolean(false);

    @Override // X.AnonymousClass017
    public void A05(AbstractC001200n r3, AnonymousClass02B r4) {
        r3.ADr().A00(new AnonymousClass054(r4, this) { // from class: com.whatsapp.payments.util.LastObserverLiveData$$ExternalSyntheticLambda0
            public final /* synthetic */ AnonymousClass02B A00;
            public final /* synthetic */ C117885au A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass054
            public final void AWQ(AnonymousClass074 r42, AbstractC001200n r5) {
                C117885au r2 = this.A01;
                AnonymousClass02B r1 = this.A00;
                if (r42 == AnonymousClass074.ON_RESUME) {
                    r2.A00 = C12970iu.A10(r1);
                }
            }
        });
        super.A05(r3, new IDxObserverShape5S0200000_3_I1(r4, 14, this));
    }

    @Override // X.AnonymousClass017
    public void A0A(Object obj) {
        this.A01.set(true);
        super.A0A(obj);
    }

    @Override // X.AnonymousClass017
    public void A0B(Object obj) {
        this.A01.set(true);
        super.A0B(obj);
    }
}
