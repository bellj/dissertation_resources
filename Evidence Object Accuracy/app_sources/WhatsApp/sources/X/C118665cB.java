package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;

/* renamed from: X.5cB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118665cB extends AbstractC018308n {
    public final int A00;
    public final Drawable A01;

    public C118665cB(Context context) {
        this.A01 = AnonymousClass00T.A04(context, R.drawable.service_row_divider);
        this.A00 = (int) TypedValue.applyDimension(1, 72.0f, context.getResources().getDisplayMetrics());
    }

    @Override // X.AbstractC018308n
    public void A02(Canvas canvas, C05480Ps r9, RecyclerView recyclerView) {
        int paddingLeft = recyclerView.getPaddingLeft() + this.A00;
        int width = recyclerView.getWidth() - recyclerView.getPaddingRight();
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View childAt = recyclerView.getChildAt(i);
            int bottom = childAt.getBottom() + C12970iu.A0H(childAt).bottomMargin;
            Drawable drawable = this.A01;
            drawable.setBounds(paddingLeft, bottom, width, drawable.getIntrinsicHeight() + bottom);
            drawable.draw(canvas);
        }
    }
}
