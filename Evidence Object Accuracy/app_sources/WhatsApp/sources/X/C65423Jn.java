package X;

import android.animation.ValueAnimator;

/* renamed from: X.3Jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65423Jn implements ValueAnimator.AnimatorUpdateListener {
    public int A00;
    public final /* synthetic */ AbstractC15160mf A01;

    public C65423Jn(AbstractC15160mf r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
        boolean z = AbstractC15160mf.A09;
        C34291fu r1 = this.A01.A05;
        if (z) {
            AnonymousClass028.A0Y(r1, A05 - this.A00);
        } else {
            r1.setTranslationY((float) A05);
        }
        this.A00 = A05;
    }
}
