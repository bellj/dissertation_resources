package X;

import android.content.Context;

/* renamed from: X.5eo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC119655eo extends AnonymousClass27X {
    public boolean A00;

    public AbstractC119655eo(Context context) {
        super(context, null, 0);
        A00();
    }

    @Override // X.AnonymousClass27Y
    public void A00() {
        if (!this.A00) {
            this.A00 = true;
            C117295Zj.A0y(AnonymousClass2P6.A00(generatedComponent()), this);
        }
    }
}
