package X;

/* renamed from: X.1to  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41611to extends AnonymousClass1MS {
    public final C14330lG A00;
    public final AnonymousClass018 A01;
    public final AnonymousClass19M A02;
    public final C22590zK A03;
    public final /* synthetic */ C41491tc A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C41611to(C14330lG r2, AnonymousClass018 r3, AnonymousClass19M r4, C22590zK r5, C41491tc r6) {
        super("MessageThumbsThread");
        this.A04 = r6;
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = r5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:80:0x015c, code lost:
        if (r7 == null) goto L_0x015e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0205 A[Catch: InterruptedException -> 0x0248, TryCatch #0 {InterruptedException -> 0x0248, blocks: (B:3:0x0005, B:4:0x0013, B:6:0x001d, B:8:0x0028, B:10:0x0037, B:12:0x003d, B:14:0x0043, B:16:0x0047, B:18:0x004d, B:20:0x0051, B:21:0x0059, B:23:0x005d, B:25:0x0063, B:27:0x0067, B:29:0x006b, B:31:0x0073, B:33:0x0079, B:34:0x0082, B:37:0x008c, B:39:0x0090, B:41:0x0094, B:43:0x0098, B:45:0x00a4, B:47:0x00b4, B:49:0x00ba, B:50:0x00c1, B:51:0x00c6, B:53:0x00ca, B:55:0x00d4, B:57:0x00ed, B:58:0x00f2, B:60:0x00f6, B:62:0x00ff, B:64:0x0107, B:66:0x010d, B:68:0x0117, B:71:0x011f, B:73:0x0123, B:77:0x0149, B:79:0x014f, B:81:0x015e, B:84:0x0164, B:86:0x016a, B:89:0x0173, B:91:0x0177, B:93:0x017d, B:95:0x018f, B:97:0x0195, B:98:0x01a9, B:99:0x01bb, B:101:0x01bf, B:103:0x01c3, B:105:0x01c9, B:106:0x01cd, B:107:0x01de, B:108:0x01ee, B:113:0x01f7, B:114:0x01fb, B:116:0x0205, B:117:0x0215, B:119:0x021b, B:120:0x0221, B:122:0x0227, B:123:0x0240), top: B:128:0x0005 }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0221 A[Catch: InterruptedException -> 0x0248, TryCatch #0 {InterruptedException -> 0x0248, blocks: (B:3:0x0005, B:4:0x0013, B:6:0x001d, B:8:0x0028, B:10:0x0037, B:12:0x003d, B:14:0x0043, B:16:0x0047, B:18:0x004d, B:20:0x0051, B:21:0x0059, B:23:0x005d, B:25:0x0063, B:27:0x0067, B:29:0x006b, B:31:0x0073, B:33:0x0079, B:34:0x0082, B:37:0x008c, B:39:0x0090, B:41:0x0094, B:43:0x0098, B:45:0x00a4, B:47:0x00b4, B:49:0x00ba, B:50:0x00c1, B:51:0x00c6, B:53:0x00ca, B:55:0x00d4, B:57:0x00ed, B:58:0x00f2, B:60:0x00f6, B:62:0x00ff, B:64:0x0107, B:66:0x010d, B:68:0x0117, B:71:0x011f, B:73:0x0123, B:77:0x0149, B:79:0x014f, B:81:0x015e, B:84:0x0164, B:86:0x016a, B:89:0x0173, B:91:0x0177, B:93:0x017d, B:95:0x018f, B:97:0x0195, B:98:0x01a9, B:99:0x01bb, B:101:0x01bf, B:103:0x01c3, B:105:0x01c9, B:106:0x01cd, B:107:0x01de, B:108:0x01ee, B:113:0x01f7, B:114:0x01fb, B:116:0x0205, B:117:0x0215, B:119:0x021b, B:120:0x0221, B:122:0x0227, B:123:0x0240), top: B:128:0x0005 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x011d  */
    @Override // java.lang.Thread, java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 591
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C41611to.run():void");
    }
}
