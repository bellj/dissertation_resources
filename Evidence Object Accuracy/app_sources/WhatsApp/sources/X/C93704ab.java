package X;

import java.net.DatagramSocket;
import java.nio.charset.Charset;

/* renamed from: X.4ab  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93704ab {
    public static final AnonymousClass4F2 A05 = new AnonymousClass4F2();
    public int A00 = 0;
    public DatagramSocket A01 = null;
    public Charset A02 = Charset.defaultCharset();
    public AnonymousClass4F2 A03 = A05;
    public boolean A04 = false;
}
