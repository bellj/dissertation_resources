package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3d5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ThreadFactoryC71533d5 implements ThreadFactory {
    public final String A00 = "GAC_Executor";
    public final ThreadFactory A01 = Executors.defaultThreadFactory();
    public final AtomicInteger A02 = new AtomicInteger();

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.A01.newThread(new RunnableBRunnable0Shape0S0100000_I0(runnable));
        String str = this.A00;
        int andIncrement = this.A02.getAndIncrement();
        StringBuilder A0t = C12980iv.A0t(str.length() + 13);
        A0t.append(str);
        A0t.append("[");
        A0t.append(andIncrement);
        newThread.setName(C12960it.A0d("]", A0t));
        return newThread;
    }
}
