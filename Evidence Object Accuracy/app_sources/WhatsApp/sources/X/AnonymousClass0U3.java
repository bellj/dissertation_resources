package X;

import android.os.Build;
import android.util.Log;
import android.util.Property;
import android.view.View;
import java.lang.reflect.Field;

/* renamed from: X.0U3  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U3 {
    public static Field A00;
    public static boolean A01;
    public static final Property A02 = new C02230An();
    public static final Property A03 = new C02220Am();
    public static final AnonymousClass0QJ A04;

    static {
        AnonymousClass0QJ r0;
        int i = Build.VERSION.SDK_INT;
        if (i >= 22) {
            r0 = new AnonymousClass0G8();
        } else if (i >= 21) {
            r0 = new AnonymousClass0G9();
        } else if (i >= 19) {
            r0 = new AnonymousClass0GA();
        } else {
            r0 = new AnonymousClass0QJ();
        }
        A04 = r0;
    }

    public static AbstractC11380gC A00(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new AnonymousClass0ZS(view);
        }
        return new AnonymousClass0ZR(view.getWindowToken());
    }

    public static void A01(View view, int i) {
        if (!A01) {
            try {
                Field declaredField = View.class.getDeclaredField("mViewFlags");
                A00 = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException unused) {
                Log.i("ViewUtils", "fetchViewFlagsField: ");
            }
            A01 = true;
        }
        Field field = A00;
        if (field != null) {
            try {
                A00.setInt(view, i | (field.getInt(view) & -13));
            } catch (IllegalAccessException unused2) {
            }
        }
    }
}
