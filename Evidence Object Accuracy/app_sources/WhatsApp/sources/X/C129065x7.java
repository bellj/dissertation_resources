package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5x7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129065x7 {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C16590pI A02;
    public final C17220qS A03;
    public final C18650sn A04;

    public C129065x7(AbstractC15710nm r1, C14900mE r2, C16590pI r3, C17220qS r4, C18650sn r5) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
        this.A03 = r4;
        this.A04 = r5;
    }

    public void A00(UserJid userJid, AnonymousClass6MU r13) {
        C17220qS r3 = this.A03;
        String A01 = r3.A01();
        C126375sm r10 = new C126375sm(userJid, new C128665wT(A01));
        C117295Zj.A1B(r3, new C120215fo(this.A02.A00, this.A01, this.A04, r13, this, r10), r10.A00, A01);
    }
}
