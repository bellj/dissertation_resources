package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape0S0300100_I0;
import com.google.firebase.iid.FirebaseInstanceId;

/* renamed from: X.0p5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C16480p5 extends BroadcastReceiver {
    public RunnableBRunnable0Shape0S0300100_I0 A00;

    public C16480p5(RunnableBRunnable0Shape0S0300100_I0 runnableBRunnable0Shape0S0300100_I0) {
        this.A00 = runnableBRunnable0Shape0S0300100_I0;
    }

    @Override // android.content.BroadcastReceiver
    public final void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        RunnableBRunnable0Shape0S0300100_I0 runnableBRunnable0Shape0S0300100_I0 = this.A00;
        if (runnableBRunnable0Shape0S0300100_I0 != null && (connectivityManager = (ConnectivityManager) runnableBRunnable0Shape0S0300100_I0.A00().getSystemService("connectivity")) != null && (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected()) {
            if (FirebaseInstanceId.A03()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.A02(this.A00, 0);
            this.A00.A00().unregisterReceiver(this);
            this.A00 = null;
        }
    }
}
