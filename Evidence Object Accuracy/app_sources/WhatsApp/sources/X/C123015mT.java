package X;

import android.content.Context;
import java.util.AbstractCollection;

/* renamed from: X.5mT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123015mT extends AbstractC125975s7 {
    public final String A00;

    public C123015mT(String str) {
        super(100);
        this.A00 = str;
    }

    public static void A00(Context context, AbstractCollection abstractCollection, int i) {
        abstractCollection.add(new C123015mT(context.getString(i)));
    }
}
