package X;

import com.whatsapp.payments.ui.IndiaUpiDeviceBindStepActivity;

/* renamed from: X.5il  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121595il extends AbstractActivityC121665jA {
    public boolean A00 = false;

    public AbstractActivityC121595il() {
        C117295Zj.A0p(this, 45);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            IndiaUpiDeviceBindStepActivity indiaUpiDeviceBindStepActivity = (IndiaUpiDeviceBindStepActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, indiaUpiDeviceBindStepActivity);
            ActivityC13810kN.A10(A1M, indiaUpiDeviceBindStepActivity);
            AbstractActivityC119235dO.A1S(r3, A1M, indiaUpiDeviceBindStepActivity, AbstractActivityC119235dO.A0l(A1M, ActivityC13790kL.A0S(r3, A1M, indiaUpiDeviceBindStepActivity, ActivityC13790kL.A0Y(A1M, indiaUpiDeviceBindStepActivity)), indiaUpiDeviceBindStepActivity));
            AbstractActivityC119235dO.A1Y(A1M, indiaUpiDeviceBindStepActivity);
            indiaUpiDeviceBindStepActivity.A0M = C117315Zl.A0F(A1M);
            indiaUpiDeviceBindStepActivity.A0F = (AnonymousClass17V) A1M.AEX.get();
            indiaUpiDeviceBindStepActivity.A0N = (C22120yY) A1M.ANn.get();
            indiaUpiDeviceBindStepActivity.A0R = (C130165yu) A1M.A2Z.get();
            indiaUpiDeviceBindStepActivity.A0I = (AnonymousClass69E) A1M.A9W.get();
            indiaUpiDeviceBindStepActivity.A07 = C12970iu.A0Y(A1M);
            indiaUpiDeviceBindStepActivity.A08 = C117305Zk.A0G(A1M);
            indiaUpiDeviceBindStepActivity.A09 = (C129925yW) A1M.AEW.get();
            indiaUpiDeviceBindStepActivity.A0D = (AnonymousClass162) A1M.AFE.get();
            indiaUpiDeviceBindStepActivity.A0L = r3.A0F();
            indiaUpiDeviceBindStepActivity.A0K = (C122205l5) A1M.A9Z.get();
            indiaUpiDeviceBindStepActivity.A0J = (C122195l4) A1M.A9V.get();
        }
    }
}
