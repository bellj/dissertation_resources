package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1EW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1EW {
    public final C15550nR A00;
    public final C14850m9 A01;
    public final AnonymousClass11G A02;
    public final AnonymousClass1EX A03;

    public AnonymousClass1EW(C15550nR r1, C14850m9 r2, AnonymousClass11G r3, AnonymousClass1EX r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static AnonymousClass1V8 A00(AnonymousClass1JX r4) {
        byte[] bArr;
        synchronized (r4) {
            bArr = r4.A0T;
            if (bArr == null) {
                bArr = r4.A01();
            }
        }
        return new AnonymousClass1V8("message", bArr, (AnonymousClass1W9[]) null);
    }

    public static final void A01(String str, String str2, List list) {
        if (!TextUtils.isEmpty(str2)) {
            list.add(new AnonymousClass1W9(str, str2));
        }
    }

    public void A02(List list, List list2) {
        AnonymousClass1W9 r1;
        String str;
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C41651ts r4 = (C41651ts) it.next();
            AbstractC14640lm r2 = r4.A00;
            boolean z = r2 instanceof UserJid;
            arrayList.clear();
            arrayList.add(new AnonymousClass1W9(r2, "jid"));
            if (!TextUtils.isEmpty(r4.A0J)) {
                String str2 = r4.A0J;
                AnonymousClass009.A05(str2);
                arrayList.add(new AnonymousClass1W9("notify", str2));
            }
            int i = ((AnonymousClass1JX) r4).A00;
            if (i == 0 || i == -1) {
                String str3 = r4.A0I;
                if (str3 != null) {
                    arrayList.add(new AnonymousClass1W9("name", str3));
                    if (!r4.A0O && z) {
                        arrayList.add(new AnonymousClass1W9("type", "out"));
                    }
                    String str4 = r4.A0K;
                    if (str4 != null) {
                        arrayList.add(new AnonymousClass1W9("short", str4));
                    }
                    if (r4.A0R) {
                        r1 = new AnonymousClass1W9("status_mute", "true");
                        arrayList.add(r1);
                    }
                }
                arrayList.add(new AnonymousClass1W9("dm_duration", r4.A02));
                arrayList.add(new AnonymousClass1W9("dm_t", r4.A06));
                list2.add(new AnonymousClass1V8("user", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[arrayList.size()])));
            } else {
                String str5 = r4.A0I;
                if (str5 != null) {
                    arrayList.add(new AnonymousClass1W9("name", str5));
                    if (!r4.A0O && z) {
                        arrayList.add(new AnonymousClass1W9("type", "out"));
                    }
                }
                String str6 = r4.A0K;
                if (str6 != null) {
                    arrayList.add(new AnonymousClass1W9("vname", str6));
                }
                if (r4.A0R) {
                    arrayList.add(new AnonymousClass1W9("status_mute", "true"));
                }
                if (i == 1) {
                    str = "0";
                } else if (i == 2) {
                    str = "1";
                } else if (i == 3) {
                    str = "2";
                } else {
                    arrayList.add(new AnonymousClass1W9("dm_duration", r4.A02));
                    arrayList.add(new AnonymousClass1W9("dm_t", r4.A06));
                    list2.add(new AnonymousClass1V8("user", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[arrayList.size()])));
                }
                arrayList.add(new AnonymousClass1W9("verify", str));
                if (r4.A0P) {
                    r1 = new AnonymousClass1W9("enterprise", "true");
                    arrayList.add(r1);
                    arrayList.add(new AnonymousClass1W9("dm_duration", r4.A02));
                    arrayList.add(new AnonymousClass1W9("dm_t", r4.A06));
                    list2.add(new AnonymousClass1V8("user", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[arrayList.size()])));
                } else {
                    arrayList.add(new AnonymousClass1W9("dm_duration", r4.A02));
                    arrayList.add(new AnonymousClass1W9("dm_t", r4.A06));
                    list2.add(new AnonymousClass1V8("user", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[arrayList.size()])));
                }
            }
        }
    }
}
