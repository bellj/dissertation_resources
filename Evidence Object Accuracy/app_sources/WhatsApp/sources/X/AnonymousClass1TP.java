package X;

import java.io.OutputStream;

/* renamed from: X.1TP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1TP {
    public OutputStream A00;

    public AnonymousClass1TP(OutputStream outputStream) {
        this.A00 = outputStream;
    }

    public AnonymousClass1TP A00() {
        return new AnonymousClass5N7(this.A00);
    }

    public AnonymousClass5N8 A01() {
        return new AnonymousClass5N8(this.A00);
    }

    public void A04(AnonymousClass1TL r1, boolean z) {
        r1.A08(this, z);
    }

    public final void A02(int i) {
        if (i > 127) {
            int i2 = i;
            int i3 = 1;
            while (true) {
                i2 >>>= 8;
                if (i2 == 0) {
                    break;
                }
                i3++;
            }
            OutputStream outputStream = this.A00;
            outputStream.write((byte) (i3 | 128));
            for (int i4 = (i3 - 1) << 3; i4 >= 0; i4 -= 8) {
                outputStream.write((byte) (i >> i4));
            }
            return;
        }
        this.A00.write((byte) i);
    }

    public final void A03(int i, int i2, boolean z) {
        if (!z) {
            return;
        }
        if (i2 < 31) {
            this.A00.write(i | i2);
            return;
        }
        OutputStream outputStream = this.A00;
        outputStream.write(31 | i);
        if (i2 < 128) {
            outputStream.write(i2);
            return;
        }
        byte[] bArr = new byte[5];
        int i3 = 4;
        bArr[4] = (byte) (i2 & 127);
        do {
            i2 >>= 7;
            i3--;
            bArr[i3] = (byte) ((i2 & 127) | 128);
        } while (i2 > 127);
        outputStream.write(bArr, i3, 5 - i3);
    }

    public final void A05(byte[] bArr, int i, int i2, boolean z) {
        A03(i, i2, z);
        int length = bArr.length;
        A02(length);
        this.A00.write(bArr, 0, length);
    }

    public final void A06(byte[] bArr, int i, boolean z) {
        if (z) {
            this.A00.write(i);
        }
        int length = bArr.length;
        A02(length);
        this.A00.write(bArr, 0, length);
    }

    public final void A07(AnonymousClass1TN[] r6, int i, boolean z) {
        if (z) {
            this.A00.write(i);
        }
        OutputStream outputStream = this.A00;
        outputStream.write(128);
        for (AnonymousClass1TN r0 : r6) {
            A04(r0.Aer(), true);
        }
        outputStream.write(0);
        outputStream.write(0);
    }
}
