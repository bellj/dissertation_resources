package X;

import android.database.Cursor;
import android.os.SystemClock;
import android.text.TextUtils;

/* renamed from: X.0v3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20000v3 implements AbstractC20010v4 {
    public final C14830m7 A00;
    public final AnonymousClass018 A01;
    public final C16510p9 A02;
    public final C20290vW A03;
    public final C15240mn A04;
    public final C16490p7 A05;

    @Override // X.AbstractC20010v4
    public Cursor AEK(AnonymousClass02N r2, String str) {
        return null;
    }

    public C20000v3(C14830m7 r1, AnonymousClass018 r2, C16510p9 r3, C20290vW r4, C15240mn r5, C16490p7 r6) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A05 = r6;
    }

    @Override // X.AbstractC20010v4
    public Cursor AEL(AnonymousClass02N r12, AbstractC14640lm r13, String str) {
        Cursor cursor;
        long uptimeMillis = SystemClock.uptimeMillis();
        try {
            C16310on A01 = this.A05.get();
            if (!TextUtils.isEmpty(str)) {
                C15250mo r2 = new C15250mo(this.A01);
                r2.A03(str);
                r2.A04 = r13;
                cursor = A01.A03.A07(r12, C34971h0.A09, new String[]{this.A04.A0B(r12, r2, null)});
            } else {
                cursor = A01.A03.A07(r12, C32301bw.A01, new String[]{String.valueOf(this.A02.A02(r13))});
            }
            A01.close();
            return cursor;
        } finally {
            this.A03.A00("KeptMessageStore/getKeptMessagesForJid", SystemClock.uptimeMillis() - uptimeMillis);
        }
    }
}
