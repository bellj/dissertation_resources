package X;

import X.AbstractC33621eg;
import X.AnonymousClass028;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.RunnableBRunnable0Shape0S0310000_I0;
import com.facebook.redex.RunnableBRunnable0Shape12S0100000_I0_12;
import com.facebook.redex.ViewOnClickCListenerShape0S0400000_I0;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.status.playback.StatusPlaybackActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackBaseFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.status.playback.page.StatusPlaybackPageItem$3;
import com.whatsapp.status.playback.widget.StatusPlaybackProgressView;
import com.whatsapp.text.ReadMoreTextView;
import com.whatsapp.ui.media.MediaCaptionTextView;
import com.whatsapp.util.Log;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1eg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC33621eg extends AbstractC33631eh {
    public BottomSheetBehavior A00;
    public C35571iH A01;
    public StatusPlaybackProgressView A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07 = true;
    public final Handler A08 = new Handler(Looper.getMainLooper());
    public final AnonymousClass12P A09;
    public final C14900mE A0A;
    public final C239613r A0B;
    public final C16170oZ A0C;
    public final AnonymousClass018 A0D;
    public final C15650ng A0E;
    public final C22810zg A0F;
    public final C18470sV A0G;
    public final C244415n A0H;
    public final AnonymousClass1CH A0I;
    public final C15860o1 A0J;
    public final AnonymousClass1BD A0K;
    public final C48252Fe A0L;
    public final AbstractC115405Rk A0M = new AnonymousClass59O(this);
    public final AnonymousClass1CY A0N;
    public final C28181Ma A0O = new C28181Ma(true);
    public final C28181Ma A0P = new C28181Ma(true);
    public final C28181Ma A0Q = new C28181Ma(true);
    public final AbstractC14440lR A0R;
    public final C17020q8 A0S;
    public final Runnable A0T = new RunnableBRunnable0Shape12S0100000_I0_12(this, 12);

    public abstract void A0H();

    public AbstractC33621eg(AnonymousClass12P r3, C14900mE r4, C239613r r5, C16170oZ r6, AnonymousClass018 r7, C15650ng r8, C22810zg r9, C18470sV r10, C244415n r11, AnonymousClass1CH r12, C15860o1 r13, AnonymousClass1BD r14, C48252Fe r15, AnonymousClass1CY r16, AbstractC14440lR r17, C17020q8 r18) {
        this.A0N = r16;
        this.A0A = r4;
        this.A0B = r5;
        this.A0R = r17;
        this.A0G = r10;
        this.A0C = r6;
        this.A09 = r3;
        this.A0H = r11;
        this.A0F = r9;
        this.A0D = r7;
        this.A0I = r12;
        this.A0E = r8;
        this.A0J = r13;
        this.A0S = r18;
        this.A0K = r14;
        this.A0L = r15;
    }

    @Override // X.AbstractC33631eh
    public void A00() {
        AnonymousClass2VE r0;
        super.A00();
        AbstractC33641ei A0A = A0A();
        if (A0A instanceof AnonymousClass35X) {
            r0 = ((AnonymousClass35X) A0A).A01;
        } else if (A0A instanceof AnonymousClass35U) {
            r0 = ((AnonymousClass35U) A0A).A00;
        } else {
            return;
        }
        if (r0 != null) {
            r0.A02.dismiss();
        }
    }

    @Override // X.AbstractC33631eh
    public void A01() {
        AnonymousClass2VE r0;
        super.A01();
        StatusPlaybackProgressView statusPlaybackProgressView = this.A02;
        if (statusPlaybackProgressView != null) {
            if (statusPlaybackProgressView.A03 == this.A0M) {
                statusPlaybackProgressView.A03 = null;
            }
            statusPlaybackProgressView.invalidate();
        }
        AbstractC33641ei A0A = A0A();
        if (!(A0A instanceof AnonymousClass35T)) {
            if (A0A instanceof AnonymousClass35X) {
                AnonymousClass35X r1 = (AnonymousClass35X) A0A;
                r1.A0H();
                r1.A0D();
                r0 = r1.A01;
            } else if (!(A0A instanceof AnonymousClass35S) && !(A0A instanceof AnonymousClass35V) && (A0A instanceof AnonymousClass35U)) {
                r0 = ((AnonymousClass35U) A0A).A00;
            } else {
                return;
            }
            if (r0 != null) {
                r0.A02.dismiss();
                return;
            }
            return;
        }
        AnonymousClass35T r12 = (AnonymousClass35T) A0A;
        AbstractC28651Ol r02 = r12.A00;
        if (r02 != null) {
            r02.A09();
            r12.A00.A06();
            r12.A00 = null;
        }
        r12.A01.removeMessages(0);
    }

    @Override // X.AbstractC33631eh
    public void A02() {
        super.A02();
        A0D();
        if (super.A04) {
            this.A0Q.A01();
            this.A0P.A01();
        }
    }

    @Override // X.AbstractC33631eh
    public void A03() {
        super.A03();
        A0E();
        if (super.A04) {
            this.A0Q.A03();
        }
    }

    @Override // X.AbstractC33631eh
    public void A04() {
        super.A04();
        AbstractC35561iG r0 = (AbstractC35561iG) this;
        this.A06 = r0.A0G.A0I(r0.A09);
        C28181Ma r2 = this.A0O;
        r2.A01 = 0;
        r2.A00 = 0;
        if (A0P(true)) {
            r2.A03();
        }
        A0H();
        A0F();
    }

    @Override // X.AbstractC33631eh
    public void A05() {
        super.A05();
        this.A0Q.A01();
        this.A0O.A01();
        StringBuilder sb = new StringBuilder("playbackPage/stopPlayback page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(this.A0L.A01);
        Log.i(sb.toString());
        this.A05 = false;
        this.A04 = false;
        StatusPlaybackProgressView statusPlaybackProgressView = this.A02;
        if (statusPlaybackProgressView.A03 == this.A0M) {
            statusPlaybackProgressView.A03 = null;
        }
        statusPlaybackProgressView.invalidate();
        A0A().A08();
        A0G();
        A0G();
    }

    @Override // X.AbstractC33631eh
    public void A08(Rect rect) {
        super.A08(rect);
        C35571iH A0B = A0B();
        A0B.A01.setPadding(rect.left, 0, rect.right, rect.bottom);
        A0B.A05.setPadding(rect.left, 0, rect.right, rect.bottom);
        BottomSheetBehavior bottomSheetBehavior = this.A00;
        View view = super.A00;
        AnonymousClass009.A03(view);
        bottomSheetBehavior.A0L(view.getContext().getResources().getDimensionPixelSize(R.dimen.status_footer_peek_height) + rect.bottom);
        ViewGroup viewGroup = A0B.A08;
        viewGroup.setPadding(rect.left, viewGroup.getPaddingTop(), rect.right, A0B.A08.getPaddingBottom());
        AbstractC33641ei A0A = A0A();
        if (A0A instanceof AnonymousClass35V) {
            AnonymousClass35V r7 = (AnonymousClass35V) A0A;
            int dimensionPixelSize = r7.A02().getResources().getDimensionPixelSize(R.dimen.status_text_h_padding);
            int dimensionPixelSize2 = r7.A02().getResources().getDimensionPixelSize(R.dimen.status_text_v_padding);
            if (r7.A0G) {
                r7.A0E.setPadding(rect.left, rect.top + dimensionPixelSize2, rect.right, dimensionPixelSize2 + rect.bottom);
                return;
            }
            r7.A06.setPadding(rect.left + dimensionPixelSize, rect.top + dimensionPixelSize2, rect.right + dimensionPixelSize, rect.bottom + dimensionPixelSize2);
            View view2 = r7.A00;
            if (view2 != null) {
                view2.setPadding(rect.left + dimensionPixelSize, rect.top + dimensionPixelSize2, dimensionPixelSize + rect.right, dimensionPixelSize2 + rect.bottom);
            }
        }
    }

    public View A09(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        String str;
        AnonymousClass1BD r6 = this.A0K;
        AbstractC15340mz r1 = ((AbstractC35561iG) this).A09;
        AnonymousClass1IS r2 = r1.A0z;
        int hashCode = r2.A01.hashCode();
        byte b = r1.A0y;
        if (b != 0) {
            if (b != 1) {
                if (b != 2) {
                    if (b != 3) {
                        if (b != 13) {
                            if (b != 15) {
                                if (b != 25) {
                                    switch (b) {
                                        case 27:
                                            break;
                                        case 28:
                                            break;
                                        case 29:
                                            break;
                                        default:
                                            str = "UNKNOWN";
                                            break;
                                    }
                                }
                            } else {
                                str = "DELETING";
                            }
                        }
                        str = "GIF";
                    }
                    str = "VIDEO";
                } else {
                    str = "VOICE";
                }
                boolean z = r2.A02;
                C21230x5 r22 = r6.A0C;
                r22.ALF(453119185, hashCode);
                r22.AL1("is_outgoing", 453119185, hashCode, z);
                r22.AL0("media_type", str, 453119185, hashCode);
                r22.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_CREATE_VIEW_START", hashCode);
                this.A02 = (StatusPlaybackProgressView) this.A0L.A01.A05().findViewById(R.id.playback_progress);
                View inflate = layoutInflater.inflate(R.layout.status_playback_page, (ViewGroup) null, false);
                C35571iH A0B = A0B();
                A0B.A03 = inflate.findViewById(R.id.content_sheet);
                A0B.A09 = (ViewGroup) inflate.findViewById(R.id.content);
                A0B.A07 = inflate.findViewById(R.id.click_handler);
                A0B.A0B = (TextView) inflate.findViewById(R.id.control_btn);
                A0B.A04 = inflate.findViewById(R.id.control_frame);
                A0B.A0F = (MediaCaptionTextView) inflate.findViewById(R.id.caption);
                A0B.A01 = inflate.findViewById(R.id.caption_container);
                A0B.A02 = inflate.findViewById(R.id.caption_padding);
                A0B.A08 = (ViewGroup) inflate.findViewById(R.id.bottom_sheet);
                A0B.A06 = inflate.findViewById(R.id.status_details_background);
                A0B.A00 = inflate.findViewById(R.id.cancel_btn);
                A0B.A0E = (CircularProgressBar) inflate.findViewById(R.id.progress_bar);
                A0B.A0C = (TextView) inflate.findViewById(R.id.error);
                A0B.A0A = (ViewGroup) inflate.findViewById(R.id.info);
                A0B.A0D = (TextView) inflate.findViewById(R.id.info_btn);
                A0B.A05 = inflate.findViewById(R.id.extra_padding);
                r22.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_CREATE_VIEW_END", hashCode);
                return inflate;
            }
            str = "IMAGE";
            boolean z = r2.A02;
            C21230x5 r22 = r6.A0C;
            r22.ALF(453119185, hashCode);
            r22.AL1("is_outgoing", 453119185, hashCode, z);
            r22.AL0("media_type", str, 453119185, hashCode);
            r22.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_CREATE_VIEW_START", hashCode);
            this.A02 = (StatusPlaybackProgressView) this.A0L.A01.A05().findViewById(R.id.playback_progress);
            View inflate = layoutInflater.inflate(R.layout.status_playback_page, (ViewGroup) null, false);
            C35571iH A0B = A0B();
            A0B.A03 = inflate.findViewById(R.id.content_sheet);
            A0B.A09 = (ViewGroup) inflate.findViewById(R.id.content);
            A0B.A07 = inflate.findViewById(R.id.click_handler);
            A0B.A0B = (TextView) inflate.findViewById(R.id.control_btn);
            A0B.A04 = inflate.findViewById(R.id.control_frame);
            A0B.A0F = (MediaCaptionTextView) inflate.findViewById(R.id.caption);
            A0B.A01 = inflate.findViewById(R.id.caption_container);
            A0B.A02 = inflate.findViewById(R.id.caption_padding);
            A0B.A08 = (ViewGroup) inflate.findViewById(R.id.bottom_sheet);
            A0B.A06 = inflate.findViewById(R.id.status_details_background);
            A0B.A00 = inflate.findViewById(R.id.cancel_btn);
            A0B.A0E = (CircularProgressBar) inflate.findViewById(R.id.progress_bar);
            A0B.A0C = (TextView) inflate.findViewById(R.id.error);
            A0B.A0A = (ViewGroup) inflate.findViewById(R.id.info);
            A0B.A0D = (TextView) inflate.findViewById(R.id.info_btn);
            A0B.A05 = inflate.findViewById(R.id.extra_padding);
            r22.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_CREATE_VIEW_END", hashCode);
            return inflate;
        }
        str = "TEXT";
        boolean z = r2.A02;
        C21230x5 r22 = r6.A0C;
        r22.ALF(453119185, hashCode);
        r22.AL1("is_outgoing", 453119185, hashCode, z);
        r22.AL0("media_type", str, 453119185, hashCode);
        r22.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_CREATE_VIEW_START", hashCode);
        this.A02 = (StatusPlaybackProgressView) this.A0L.A01.A05().findViewById(R.id.playback_progress);
        View inflate = layoutInflater.inflate(R.layout.status_playback_page, (ViewGroup) null, false);
        C35571iH A0B = A0B();
        A0B.A03 = inflate.findViewById(R.id.content_sheet);
        A0B.A09 = (ViewGroup) inflate.findViewById(R.id.content);
        A0B.A07 = inflate.findViewById(R.id.click_handler);
        A0B.A0B = (TextView) inflate.findViewById(R.id.control_btn);
        A0B.A04 = inflate.findViewById(R.id.control_frame);
        A0B.A0F = (MediaCaptionTextView) inflate.findViewById(R.id.caption);
        A0B.A01 = inflate.findViewById(R.id.caption_container);
        A0B.A02 = inflate.findViewById(R.id.caption_padding);
        A0B.A08 = (ViewGroup) inflate.findViewById(R.id.bottom_sheet);
        A0B.A06 = inflate.findViewById(R.id.status_details_background);
        A0B.A00 = inflate.findViewById(R.id.cancel_btn);
        A0B.A0E = (CircularProgressBar) inflate.findViewById(R.id.progress_bar);
        A0B.A0C = (TextView) inflate.findViewById(R.id.error);
        A0B.A0A = (ViewGroup) inflate.findViewById(R.id.info);
        A0B.A0D = (TextView) inflate.findViewById(R.id.info_btn);
        A0B.A05 = inflate.findViewById(R.id.extra_padding);
        r22.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_CREATE_VIEW_END", hashCode);
        return inflate;
    }

    public AbstractC33641ei A0A() {
        AbstractC35561iG r14 = (AbstractC35561iG) this;
        AbstractC33641ei r13 = r14.A00;
        if (r13 == null) {
            AnonymousClass1CZ r0 = r14.A0C;
            AbstractC15340mz r1 = r14.A09;
            C48242Fd r16 = new C48242Fd(r14);
            AnonymousClass1BD r15 = r14.A0A;
            AnonymousClass1J1 r5 = r14.A05;
            C16590pI r12 = r14.A06;
            Mp4Ops mp4Ops = r14.A02;
            AbstractC15710nm r10 = r14.A01;
            C18790t3 r9 = r14.A03;
            byte b = r1.A0y;
            if (b != 0) {
                if (b != 1) {
                    if (b != 2) {
                        if (b != 3) {
                            if (b != 13) {
                                if (b == 15) {
                                    r13 = new AnonymousClass35R(r0.A03, r0.A04, r0.A08, r0.A0A, r0.A0F, new C92784Xk(4500), r16);
                                    r14.A00 = r13;
                                } else if (b != 25) {
                                    switch (b) {
                                        case 27:
                                            break;
                                        case 28:
                                            break;
                                        case 29:
                                            break;
                                        default:
                                            C14850m9 r6 = r0.A0D;
                                            r13 = new AnonymousClass35S(r0.A03, r0.A04, r0.A06, r0.A08, r0.A0A, r6, r0.A0F, r1, r16);
                                            break;
                                    }
                                    r14.A00 = r13;
                                }
                            }
                            C14850m9 r17 = r0.A0D;
                            C14900mE r152 = r0.A03;
                            AbstractC14440lR r122 = r0.A0J;
                            C14330lG r11 = r0.A02;
                            AnonymousClass19M r102 = r0.A0C;
                            C15450nH r92 = r0.A05;
                            AnonymousClass12P r8 = r0.A00;
                            C244415n r7 = r0.A0E;
                            AnonymousClass01d r62 = r0.A08;
                            AnonymousClass018 r52 = r0.A0A;
                            AnonymousClass1CH r4 = r0.A0F;
                            AnonymousClass19O r3 = r0.A0I;
                            AnonymousClass1CI r2 = r0.A0K;
                            r13 = new AnonymousClass35W(r8, r11, r152, r92, r62, r52, r102, r17, r7, r4, r1, r16, r0.A0G, r0.A0H, r3, r122, r2);
                            r14.A00 = r13;
                        }
                        C14850m9 r18 = r0.A0D;
                        C14900mE r153 = r0.A03;
                        AbstractC14440lR r123 = r0.A0J;
                        C14330lG r112 = r0.A02;
                        AnonymousClass19M r103 = r0.A0C;
                        C15450nH r93 = r0.A05;
                        AnonymousClass12P r82 = r0.A00;
                        C244415n r72 = r0.A0E;
                        AnonymousClass01d r63 = r0.A08;
                        AnonymousClass018 r53 = r0.A0A;
                        AnonymousClass1CH r42 = r0.A0F;
                        AnonymousClass19O r32 = r0.A0I;
                        AnonymousClass1CI r22 = r0.A0K;
                        r13 = new AnonymousClass35X(r82, r112, r153, r93, r63, r53, r103, r18, r72, r42, r1, r16, r0.A0G, r0.A0H, r32, r123, r22);
                        r14.A00 = r13;
                    } else {
                        C14850m9 r64 = r0.A0D;
                        if (r64.A07(1875)) {
                            r13 = new AnonymousClass35T(r0.A04, r10, r0.A03, r5, r0.A08, r12, r0.A0A, r64, r0.A0F, r1, r16);
                            r14.A00 = r13;
                        }
                        C14850m9 r6 = r0.A0D;
                        r13 = new AnonymousClass35S(r0.A03, r0.A04, r0.A06, r0.A08, r0.A0A, r6, r0.A0F, r1, r16);
                        r14.A00 = r13;
                    }
                }
                C14850m9 r73 = r0.A0D;
                C14900mE r65 = r0.A03;
                r13 = new AnonymousClass35U(r0.A00, r65, r0.A08, r0.A0A, r73, r0.A0E, r0.A0F, r1, r16, r0.A0I);
                r14.A00 = r13;
            }
            C14850m9 r83 = r0.A0D;
            C14900mE r74 = r0.A03;
            AnonymousClass18U r66 = r0.A04;
            AnonymousClass01d r54 = r0.A08;
            AnonymousClass018 r43 = r0.A0A;
            AnonymousClass1CH r33 = r0.A0F;
            r13 = new AnonymousClass35V(r10, r74, r66, mp4Ops, r9, r0.A07, r54, r12, r43, r0.A0B, r83, r33, r1, r15, r16, r0.A0J);
            r14.A00 = r13;
        }
        return r13;
    }

    public C35571iH A0B() {
        if (this instanceof C35681iV) {
            return ((C35681iV) this).A0R();
        }
        C35571iH r0 = this.A01;
        if (r0 != null) {
            return r0;
        }
        C35571iH r02 = new C35571iH(this);
        this.A01 = r02;
        return r02;
    }

    public void A0C() {
        this.A03 = true;
        C48252Fe r1 = this.A0L;
        StatusPlaybackContactFragment statusPlaybackContactFragment = r1.A02;
        AnonymousClass1E9 r3 = statusPlaybackContactFragment.A0T;
        r3.A02.A01(new RunnableBRunnable0Shape0S0310000_I0(r3, statusPlaybackContactFragment.A0b, r1.A00, 10, true), 51);
    }

    public void A0D() {
        if (this.A05 && !this.A04) {
            StringBuilder sb = new StringBuilder("playbackPage/pausePlayback page=");
            sb.append(this);
            sb.append("; host=");
            sb.append(this.A0L.A01);
            Log.i(sb.toString());
            this.A04 = true;
            A0A().A04();
            this.A0P.A03();
        }
    }

    public void A0E() {
        ActivityC000900k r1;
        if (super.A04 && this.A05 && !super.A02 && this.A00.A0B == 4 && !this.A01.A0F.A0I() && !A0A().A0B()) {
            StringBuilder sb = new StringBuilder("playbackPage/resumePlayback page=");
            sb.append(this);
            sb.append("; host=");
            StatusPlaybackBaseFragment statusPlaybackBaseFragment = this.A0L.A01;
            sb.append(statusPlaybackBaseFragment);
            Log.i(sb.toString());
            if (statusPlaybackBaseFragment != null) {
                r1 = statusPlaybackBaseFragment.A0B();
            } else {
                r1 = null;
            }
            if (r1 instanceof StatusPlaybackActivity) {
                StatusPlaybackActivity statusPlaybackActivity = (StatusPlaybackActivity) r1;
                if (statusPlaybackActivity.A0L) {
                    statusPlaybackActivity.A0L = false;
                    new Handler().postDelayed(new RunnableBRunnable0Shape12S0100000_I0_12(this, 13), 1000);
                    return;
                }
            }
            this.A04 = false;
            A0A().A05();
            this.A0P.A01();
            A0G();
        }
    }

    public void A0F() {
        if (!super.A04 || this.A05 || !A0A().A0A()) {
            StringBuilder sb = new StringBuilder("playbackPage/startPlayback not possible page=");
            sb.append(this);
            sb.append("; host=");
            sb.append(this.A0L.A01);
            Log.w(sb.toString());
            return;
        }
        StringBuilder sb2 = new StringBuilder("playbackPage/startPlayback page=");
        sb2.append(this);
        sb2.append("; host=");
        sb2.append(this.A0L.A01);
        Log.i(sb2.toString());
        this.A05 = true;
        this.A04 = false;
        A0A().A07();
        this.A02.setProgressProvider(this.A0M);
        A0G();
        this.A0O.A01();
        this.A0Q.A03();
        if (super.A02 || !super.A03 || this.A00.A0B != 4 || this.A01.A0F.A0I() || A0A().A0B()) {
            A0D();
        }
    }

    public final void A0G() {
        AnonymousClass009.A03(super.A00);
        this.A08.removeCallbacks(this.A0T);
        if (this.A01.A08.getVisibility() != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(300);
            if (this.A01.A01.getVisibility() != 0) {
                this.A01.A01.startAnimation(alphaAnimation);
                this.A01.A01.setVisibility(0);
            }
            if (this.A01.A0F.getVisibility() == 0 && this.A01.A02.getVisibility() != 0) {
                this.A01.A02.startAnimation(alphaAnimation);
                this.A01.A02.setVisibility(0);
            }
            if (this.A01.A08.getVisibility() == 4) {
                this.A01.A08.startAnimation(alphaAnimation);
                this.A01.A08.setVisibility(0);
            }
            C33651en r4 = this.A0L.A01.A03;
            AnonymousClass009.A06(r4, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
            AnonymousClass009.A05(r4);
            View view = r4.A06;
            if (view.getVisibility() != 0) {
                AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation2.setDuration(300);
                view.startAnimation(alphaAnimation2);
                StatusPlaybackProgressView statusPlaybackProgressView = r4.A0E;
                statusPlaybackProgressView.startAnimation(alphaAnimation2);
                view.setVisibility(0);
                statusPlaybackProgressView.setVisibility(0);
                Button button = r4.A00;
                if (button != null) {
                    button.startAnimation(alphaAnimation2);
                    r4.A00.setVisibility(0);
                }
            }
            super.A00.setSystemUiVisibility(1792);
        }
    }

    public void A0I(int i) {
        C35571iH A0B = A0B();
        if (i == 4) {
            A0B.A06.setVisibility(8);
            A0B.A0A.setAlpha(1.0f);
            A0E();
        } else if (i != 3) {
            A0D();
        }
    }

    public void A0J(int i) {
        String str;
        StringBuilder sb = new StringBuilder("playbackPage/reportStatusExitStats exit-method=");
        switch (i) {
            case 1:
                str = "SWIPE_DOWN";
                break;
            case 2:
                str = "BACK_ARROW_TAP";
                break;
            case 3:
                str = "BACK_BUTTON_TAP";
                break;
            case 4:
                str = "STATUS_TIMEOUT";
                break;
            case 5:
                str = "STATUS_DISMISSED";
                break;
            case 6:
                str = "BACKWARD_SWIPE";
                break;
            case 7:
                str = "FORWARD_SWIPE";
                break;
            case 8:
                str = "BACKWARD_TAP";
                break;
            case 9:
                str = "FORWARD_TAP";
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        sb.append(str);
        sb.append("; page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(this.A0L.A01);
        Log.i(sb.toString());
    }

    public void A0K(View view) {
        StringBuilder sb = new StringBuilder("playbackPage/onViewCreated page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(this.A0L.A01);
        Log.i(sb.toString());
        AnonymousClass1BD r1 = this.A0K;
        AbstractC15340mz r4 = ((AbstractC35561iG) this).A09;
        int hashCode = r4.A0z.A01.hashCode();
        C21230x5 r2 = r1.A0C;
        r2.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_VIEW_CREATED_START", hashCode);
        C35571iH r8 = this.A01;
        AnonymousClass009.A05(r8);
        String str = null;
        r8.A0F.setOnClickListener(null);
        r8.A0F.setClickable(false);
        ((ReadMoreTextView) r8.A0F).A02 = new AbstractC116255Us(r8, this) { // from class: X.59g
            public final /* synthetic */ C35571iH A00;
            public final /* synthetic */ AbstractC33621eg A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116255Us
            public final boolean AO3() {
                AbstractC33621eg r22 = this.A01;
                this.A00.A0F.setExpanded(true);
                r22.A0D();
                r22.A0G();
                return true;
            }
        };
        this.A00 = new BottomSheetBehavior() { // from class: com.whatsapp.status.playback.page.StatusPlaybackPageItem$1
            public float A00;
            public boolean A01;

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
            public boolean A0C(MotionEvent motionEvent, View view2, CoordinatorLayout coordinatorLayout) {
                if ((!this.A01 && view2.isShown()) || motionEvent.getPointerCount() >= 2) {
                    return false;
                }
                boolean A0C = super.A0C(motionEvent, view2, coordinatorLayout);
                int actionMasked = motionEvent.getActionMasked();
                if (actionMasked == 0) {
                    this.A00 = motionEvent.getY();
                } else if (actionMasked == 2 && this.A00 < motionEvent.getY() && this.A0B == 4) {
                    return false;
                }
                return A0C;
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
            public boolean A0G(View view2, CoordinatorLayout coordinatorLayout, int i) {
                this.A01 = true;
                return super.A0G(view2, coordinatorLayout, i);
            }
        };
        BottomSheetBehavior bottomSheetBehavior = this.A00;
        ((AnonymousClass0B5) r8.A08.getLayoutParams()).A00(bottomSheetBehavior);
        bottomSheetBehavior.A0E = new C56752lc(this);
        StatusPlaybackPageItem$3 statusPlaybackPageItem$3 = new BottomSheetBehavior() { // from class: com.whatsapp.status.playback.page.StatusPlaybackPageItem$3
            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
            public boolean A0C(MotionEvent motionEvent, View view2, CoordinatorLayout coordinatorLayout) {
                if (AbstractC33621eg.this.A00.A0B == 3 || motionEvent.getPointerCount() >= 2 || !super.A0C(motionEvent, view2, coordinatorLayout)) {
                    return false;
                }
                return true;
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
            public boolean A0D(MotionEvent motionEvent, View view2, CoordinatorLayout coordinatorLayout) {
                if (AbstractC33621eg.this.A00.A0B == 3) {
                    return false;
                }
                try {
                    return super.A0D(motionEvent, view2, coordinatorLayout);
                } catch (IllegalArgumentException unused) {
                    return false;
                }
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
            public boolean A0G(View view2, CoordinatorLayout coordinatorLayout, int i) {
                super.A0G(view2, coordinatorLayout, i);
                AnonymousClass028.A0Y(view2, -view2.getTop());
                return true;
            }
        };
        ((AnonymousClass0B5) r8.A03.getLayoutParams()).A00(statusPlaybackPageItem$3);
        statusPlaybackPageItem$3.A0E = new C56782lf(r8, this);
        r8.A0E.setMax(100);
        r8.A09.addView(A0A().A03());
        if (!(A0A() instanceof AnonymousClass35V) && ((r4 instanceof AbstractC28871Pi) || !(r4 instanceof AbstractC16130oV) || (str = ((AbstractC16130oV) r4).A15()) == null)) {
            if (r4 instanceof C28861Ph) {
                str = ((C28861Ph) r4).A05;
            } else {
                str = null;
            }
        }
        r8.A0F.setCaptionText(AnonymousClass1US.A04(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, str));
        r8.A02.setVisibility(r8.A0F.getVisibility());
        PointF pointF = new PointF();
        AtomicLong atomicLong = new AtomicLong();
        r8.A07.setOnTouchListener(new View.OnTouchListener(pointF, this, atomicLong) { // from class: X.3NB
            public final /* synthetic */ PointF A00;
            public final /* synthetic */ AbstractC33621eg A01;
            public final /* synthetic */ AtomicLong A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                AbstractC33621eg r42 = this.A01;
                PointF pointF2 = this.A00;
                AtomicLong atomicLong2 = this.A02;
                if (r42.A00.A0B != 4 || r42.A0A().A0B()) {
                    return false;
                }
                if (motionEvent.getPointerCount() == 1) {
                    int action = motionEvent.getAction();
                    if (action == 0) {
                        if (r42.A0A() instanceof AnonymousClass35V) {
                            AnonymousClass35V r82 = (AnonymousClass35V) r42.A0A();
                            if (r82.A0G) {
                                boolean A0G = r82.A0G(motionEvent.getX(), motionEvent.getY(), false);
                                float x = motionEvent.getX();
                                float y = motionEvent.getY();
                                Rect A0J = C12980iv.A0J();
                                View view3 = r82.A00;
                                if (view3 != null) {
                                    view3.getGlobalVisibleRect(A0J);
                                }
                                if (A0J.contains((int) x, (int) y)) {
                                    return false;
                                }
                                if (A0G) {
                                    return true;
                                }
                            }
                        }
                        pointF2.set(motionEvent.getX(), motionEvent.getY());
                        atomicLong2.set(SystemClock.elapsedRealtime());
                        r42.A0D();
                        Handler handler = r42.A08;
                        Runnable runnable = r42.A0T;
                        handler.removeCallbacks(runnable);
                        handler.postDelayed(runnable, 500);
                    } else if (action == 1) {
                        if (r42.A0A() instanceof AnonymousClass35V) {
                            AnonymousClass35V r6 = (AnonymousClass35V) r42.A0A();
                            if (r6.A0G) {
                                boolean A0G2 = r6.A0G(motionEvent.getX(), motionEvent.getY(), true);
                                float x2 = motionEvent.getX();
                                float y2 = motionEvent.getY();
                                Rect A0J2 = C12980iv.A0J();
                                View view4 = r6.A00;
                                if (view4 != null) {
                                    view4.getGlobalVisibleRect(A0J2);
                                }
                                boolean contains = A0J2.contains((int) x2, (int) y2);
                                if (A0G2 || contains) {
                                    return true;
                                }
                            }
                        }
                        pointF2.set(motionEvent.getX(), motionEvent.getY());
                        r42.A0E();
                        view2.performClick();
                    }
                }
                if (motionEvent.getAction() != 3) {
                    return false;
                }
                r42.A0E();
                r42.A0G();
                return false;
            }
        });
        A0A().A03().setOnClickListener(new ViewOnClickCListenerShape0S0400000_I0(this, atomicLong, r8, pointF, 1));
        r2.ALC(453119185, "PLAYBACK_PAGE_ITEM_ON_VIEW_CREATED_END", hashCode);
        r2.AL6(453119185, hashCode, 2);
    }

    public void A0L(Integer num, int i, boolean z) {
        String str;
        StringBuilder sb = new StringBuilder("playbackPage/reportStatusEnterStats entry-method=");
        switch (i) {
            case 1:
                str = "DIRECT_TAP";
                break;
            case 2:
                str = "BACKWARD_SWIPE";
                break;
            case 3:
                str = "FORWARD_SWIPE";
                break;
            case 4:
                str = "BACKWARD_TAP";
                break;
            case 5:
                str = "FORWARD_TAP";
                break;
            case 6:
                str = "PREVIOUS_STATUS_TIMEOUT";
                break;
            case 7:
                str = "PREVIOUS_STATUS_DISMISSED";
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        sb.append(str);
        sb.append("; page=");
        sb.append(this);
        sb.append("; host=");
        sb.append(this.A0L.A01);
        Log.i(sb.toString());
    }

    public final void A0M(boolean z) {
        Drawable drawable;
        ViewGroup viewGroup = A0B().A0A;
        if (z) {
            View view = super.A00;
            AnonymousClass009.A03(view);
            drawable = AnonymousClass00T.A04(view.getContext(), R.drawable.ic_center_shadow);
        } else {
            drawable = null;
        }
        viewGroup.setBackground(drawable);
    }

    public final void A0N(boolean z, boolean z2) {
        C35571iH A0B = A0B();
        AnonymousClass009.A03(super.A00);
        if (A0B.A08.getVisibility() != 4 || A0B.A01.getVisibility() != 4) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(300);
            if (z) {
                A0B.A01.startAnimation(alphaAnimation);
                A0B.A01.setVisibility(4);
            }
            if (z2) {
                super.A00.setSystemUiVisibility(1798);
                if (A0B.A02.getVisibility() == 0) {
                    A0B.A02.startAnimation(alphaAnimation);
                    A0B.A02.setVisibility(4);
                }
            }
            if (A0B.A08.getVisibility() == 0) {
                A0B.A08.startAnimation(alphaAnimation);
                A0B.A08.setVisibility(4);
            }
            C33651en r6 = this.A0L.A01.A03;
            AnonymousClass009.A06(r6, "getViewHolder() is accessed before StatusPlaybackBaseFragment#onCreateView()");
            AnonymousClass009.A05(r6);
            View view = r6.A06;
            if (view.getVisibility() != 4) {
                AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
                alphaAnimation2.setDuration(300);
                view.startAnimation(alphaAnimation2);
                StatusPlaybackProgressView statusPlaybackProgressView = r6.A0E;
                statusPlaybackProgressView.startAnimation(alphaAnimation2);
                view.setVisibility(4);
                statusPlaybackProgressView.setVisibility(4);
                Button button = r6.A00;
                if (button != null) {
                    button.startAnimation(alphaAnimation2);
                    r6.A00.setVisibility(4);
                }
            }
        }
    }

    public boolean A0O() {
        if (!(A0A() instanceof AnonymousClass35V)) {
            AbstractC15340mz r2 = ((AbstractC35561iG) this).A09;
            String str = null;
            if (!(r2 instanceof AbstractC28871Pi) && (r2 instanceof AbstractC16130oV)) {
                str = ((AbstractC16130oV) r2).A15();
            }
            if (TextUtils.isEmpty(str)) {
                return true;
            }
        }
        return false;
    }

    public boolean A0P(boolean z) {
        StringBuilder sb;
        String str;
        AbstractC35561iG r0 = (AbstractC35561iG) this;
        AnonymousClass1CW r4 = r0.A0B;
        AbstractC15340mz r8 = r0.A09;
        int i = 0;
        if (!(r8 instanceof AbstractC16130oV)) {
            return false;
        }
        AbstractC16130oV r2 = (AbstractC16130oV) r8;
        if (r8.A12 >= -1 && r8.A0z.A02 && !((r2 instanceof AnonymousClass1X2) && C30041Vv.A15((AnonymousClass1X4) r2))) {
            return false;
        }
        C16150oX r1 = r2.A02;
        AnonymousClass009.A05(r1);
        if (r1.A0P || r1.A07 == 1 || r2.A08 == null) {
            return false;
        }
        StringBuilder sb2 = new StringBuilder("statusdownload/downloadifneeded ");
        sb2.append(z);
        sb2.append(" ");
        AnonymousClass1IS r5 = r2.A0z;
        sb2.append(r5.A01);
        sb2.append(" ");
        sb2.append(r2.A0B());
        Log.i(sb2.toString());
        if (z) {
            C22370yy r7 = r4.A02;
            for (AbstractC16130oV r82 : r7.A04()) {
                AnonymousClass1IS r9 = r82.A0z;
                if (C15380n4.A0N(r9.A00) && !r9.equals(r5)) {
                    r7.A0C(r82, false, false);
                    r4.A03.add(r82);
                    sb = new StringBuilder();
                    str = "statusdownload/cancel ";
                } else if (r9.equals(r5)) {
                    sb = new StringBuilder();
                    str = "statusdownload/is-current ";
                }
                sb.append(str);
                sb.append(r9.A01);
                sb.append(" ");
                sb.append(r82.A0B());
                Log.i(sb.toString());
            }
        } else if (r4.A00 != null) {
            r4.A03.add(r2);
            return true;
        } else if (C30041Vv.A0o(r2)) {
            i = 3;
        }
        r4.A00(r2, i);
        return true;
    }
}
