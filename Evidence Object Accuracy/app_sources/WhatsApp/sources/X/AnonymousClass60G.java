package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.60G  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass60G {
    public static final ArrayList A09;
    public static final ArrayList A0A;
    public static final ArrayList A0B;
    public final UserJid A00;
    public final AnonymousClass1V8 A01;
    public final AnonymousClass3EM A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;

    static {
        String[] strArr = new String[2];
        strArr[0] = "0";
        A0A = C12960it.A0m("1", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A09 = C12960it.A0m("1", strArr2, 1);
        String[] strArr3 = new String[2];
        strArr3[0] = "0";
        A0B = C12960it.A0m("1", strArr3, 1);
    }

    public AnonymousClass60G(AnonymousClass1V8 r26, C130485zU r27) {
        AnonymousClass1V8.A01(r26, "iq");
        AnonymousClass1V8 r2 = r27.A00;
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        AnonymousClass3JT.A04(null, r26, String.class, A0j, A0k, "upi-get-vpa", new String[]{"account", "action"}, false);
        AnonymousClass3JT.A04(null, r26, String.class, A0j, A0k, "1", new String[]{"account", "version"}, false);
        this.A00 = (UserJid) AnonymousClass3JT.A04(null, r26, UserJid.class, A0j, A0k, null, new String[]{"account", "user"}, false);
        this.A07 = (String) AnonymousClass3JT.A04(null, r26, String.class, 1L, 200L, null, new String[]{"account", "vpa"}, false);
        this.A08 = (String) AnonymousClass3JT.A04(null, r26, String.class, 1L, 100L, null, new String[]{"account", "vpa-id"}, false);
        this.A06 = (String) AnonymousClass3JT.A03(null, r26, String.class, 1L, 1000L, null, new String[]{"account", "user-name"}, false);
        this.A04 = AnonymousClass3JT.A09(r26, A0A, new String[]{"account", "nodal"});
        this.A03 = AnonymousClass3JT.A09(r26, A09, new String[]{"account", "nodal-allowed"});
        this.A05 = AnonymousClass3JT.A09(r26, A0B, new String[]{"account", "notif-allowed"});
        this.A02 = (AnonymousClass3EM) AnonymousClass3JT.A05(r26, new AbstractC116095Uc() { // from class: X.6DQ
            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r4) {
                return new AnonymousClass3EM(null, r4, AnonymousClass1V8.this);
            }
        }, new String[0]);
        this.A01 = r26;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass60G.class != obj.getClass()) {
                return false;
            }
            AnonymousClass60G r5 = (AnonymousClass60G) obj;
            if (!this.A04.equals(r5.A04) || !this.A03.equals(r5.A03) || !this.A05.equals(r5.A05) || !this.A00.equals(r5.A00) || !this.A07.equals(r5.A07) || !this.A08.equals(r5.A08) || !C29941Vi.A00(this.A06, r5.A06) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A04, this.A03, this.A05, this.A00, this.A07, this.A08, this.A06, this.A02});
    }
}
