package X;

import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.2Im  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48932Im implements AbstractC48942In {
    public final /* synthetic */ C48302Fl A00;

    public C48932Im(C48302Fl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC48942In
    public C59422uh A82(LatLng latLng, AnonymousClass2K1 r14, AnonymousClass1B4 r15, int i) {
        AnonymousClass01J r1 = this.A00.A03;
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        AnonymousClass018 r9 = (AnonymousClass018) r1.ANb.get();
        C16340oq r7 = (C16340oq) r1.A5z.get();
        C17170qN r8 = (C17170qN) r1.AMt.get();
        return new C59422uh(latLng, r2, (C14900mE) r1.A8X.get(), (C16430p0) r1.A5y.get(), r14, r15, r7, r8, r9, (C14850m9) r1.A04.get(), (AbstractC14440lR) r1.ANe.get());
    }
}
