package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.calling.callgrid.view.CallGrid;
import com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel;

/* renamed from: X.3j8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75003j8 extends AbstractC05270Ox {
    public final /* synthetic */ CallGrid A00;

    public C75003j8(CallGrid callGrid) {
        this.A00 = callGrid;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        CallGrid callGrid = this.A00;
        CallGridViewModel callGridViewModel = callGrid.A05;
        if (callGridViewModel != null && i == 0) {
            callGridViewModel.A0B(callGrid.getVisibleParticipantJids());
        }
    }
}
