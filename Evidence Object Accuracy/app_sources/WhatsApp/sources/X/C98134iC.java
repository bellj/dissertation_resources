package X;

import android.media.MediaPlayer;
import com.whatsapp.search.views.itemviews.MessageGifVideoPlayer;

/* renamed from: X.4iC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98134iC implements MediaPlayer.OnPreparedListener {
    public final /* synthetic */ MessageGifVideoPlayer A00;

    public C98134iC(MessageGifVideoPlayer messageGifVideoPlayer) {
        this.A00 = messageGifVideoPlayer;
    }

    @Override // android.media.MediaPlayer.OnPreparedListener
    public void onPrepared(MediaPlayer mediaPlayer) {
        MessageGifVideoPlayer messageGifVideoPlayer = this.A00;
        messageGifVideoPlayer.A0C = true;
        messageGifVideoPlayer.A03();
    }
}
