package X;

import android.content.ContentResolver;

/* renamed from: X.4SL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4SL {
    public final ContentResolver A00;
    public final C63613Cg A01;
    public final AnonymousClass3CB A02;
    public final C64553Fz A03;
    public final AnonymousClass4EZ A04;

    public AnonymousClass4SL(ContentResolver contentResolver, C63613Cg r2, AnonymousClass3CB r3, C64553Fz r4, AnonymousClass4EZ r5) {
        this.A01 = r2;
        this.A00 = contentResolver;
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
    }
}
