package X;

import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;

/* renamed from: X.54W  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass54W implements AnonymousClass5W0 {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ AnonymousClass3M8 A03;
    public final /* synthetic */ BusinessDirectorySearchQueryViewModel A04;
    public final /* synthetic */ String A05;

    public AnonymousClass54W(AnonymousClass3M8 r1, BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel, String str, int i, int i2, int i3) {
        this.A04 = businessDirectorySearchQueryViewModel;
        this.A03 = r1;
        this.A05 = str;
        this.A01 = i;
        this.A00 = i2;
        this.A02 = i3;
    }

    @Override // X.AnonymousClass5W0
    public void ASk() {
        this.A04.A0I(this.A03, this.A05, this.A01, this.A00, this.A02 + 1);
    }

    @Override // X.AnonymousClass5W0
    public void AUI() {
        this.A04.A0J(this.A03, this.A05, this.A01, this.A00, this.A02 + 1);
    }
}
