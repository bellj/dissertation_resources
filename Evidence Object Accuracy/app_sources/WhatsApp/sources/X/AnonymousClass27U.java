package X;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import com.whatsapp.R;

/* renamed from: X.27U  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass27U extends Dialog {
    public final int A00;
    public final Activity A01;
    public final AnonymousClass01d A02;
    public final C14830m7 A03;
    public final AnonymousClass018 A04;

    public AnonymousClass27U(Activity activity, AnonymousClass01d r3, C14830m7 r4, AnonymousClass018 r5, int i) {
        super(activity, R.style.FullScreenDialogNoFloating);
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = activity;
        this.A00 = i;
        this.A02 = r3;
    }

    @Override // android.app.Dialog
    public void onCreate(Bundle bundle) {
        C42941w9.A0B(getWindow(), this.A04);
        super.onCreate(bundle);
        Window window = getWindow();
        AnonymousClass009.A05(window);
        setContentView(window.getLayoutInflater().inflate(this.A00, (ViewGroup) null, false));
        getWindow().setLayout(-1, -1);
    }

    @Override // android.app.Dialog, android.view.KeyEvent.Callback
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return super.onKeyDown(i, keyEvent);
    }
}
