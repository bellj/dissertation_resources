package X;

import android.content.Context;
import android.os.Bundle;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* renamed from: X.5bH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118105bH extends AnonymousClass015 {
    public AnonymousClass1IR A00;
    public C27691It A01 = C13000ix.A03();
    public final C16590pI A02;
    public final AnonymousClass018 A03;
    public final C17070qD A04;
    public final C130095yn A05;
    public final AnonymousClass14X A06;
    public final AbstractC14440lR A07;
    public final String A08;

    public C118105bH(C16590pI r2, AnonymousClass018 r3, C17070qD r4, C130095yn r5, AnonymousClass14X r6, AbstractC14440lR r7, String str) {
        this.A02 = r2;
        this.A07 = r7;
        this.A06 = r6;
        this.A03 = r3;
        this.A04 = r4;
        this.A05 = r5;
        this.A08 = str;
    }

    public static /* synthetic */ void A00(C130785zy r10, C118105bH r11) {
        Object obj;
        C127495ua r1;
        String str;
        C1316463o r8;
        String string;
        if (!r10.A06() || (obj = r10.A02) == null) {
            Log.e("PAY: NoviCreateClaimViewModel/sendSubmitClaimRequest: request failed");
            C127495ua r12 = new C127495ua(2);
            r12.A02 = r10.A00;
            r11.A01.A0A(r12);
            return;
        }
        AnonymousClass1IR r3 = ((C125665rc) obj).A00;
        AbstractC1316063k r13 = ((C119825fA) r3.A0A).A01;
        String str2 = null;
        if (r13 instanceof AbstractC121025h8) {
            r8 = ((AbstractC121025h8) r13).A03;
            Context context = r11.A02.A00;
            string = context.getString(R.string.novi_claims_withdrawal_section_label);
            if (r13 instanceof C121015h7) {
                str2 = "Cash Withdrawal";
            } else if (r13 instanceof C121005h6) {
                String str3 = ((C121005h6) r13).A00.A00;
                str2 = C12960it.A0X(context, str3.substring(str3.length() - 4), new Object[1], 0, R.string.novi_claim_withdraw_bank_transfer_account_number);
            }
        } else if (r13 instanceof C120995h5) {
            r8 = ((C120995h5) r13).A01.A00;
            string = r11.A02.A00.getString(R.string.novi_claims_p2p_section_label);
            str2 = r11.A06.A0L(r3);
        } else {
            if (r13 instanceof C121035h9) {
                Context context2 = r11.A02.A00;
                context2.getString(R.string.novi_claims_deposit_section_label);
                AbstractC128515wE r32 = ((C121035h9) r13).A00.A02;
                if (r32 instanceof C120945h0) {
                    C120945h0 r33 = (C120945h0) r32;
                    context2.getString(R.string.novi_claim_deposit_account_number, C30881Ze.A08(r33.A00), r33.A03);
                    r1 = new C127495ua(2);
                    r11.A01.A0B(r1);
                }
                str = "PAY: NoviCreateClaimViewModel/processClaimsResponse: Payment method for deposit is not card ";
            } else {
                str = "PAY: NoviSubmitClaimActionCallback/onSuccess: transaction type should not be claimable";
            }
            Log.e(str);
            r1 = new C127495ua(2);
            r11.A01.A0B(r1);
        }
        if (r8 != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Bundle A0D = C12970iu.A0D();
            A0D.putString("novi_claim_id", r8.A03);
            A0D.putString("novi_claims_transaction_id", r3.A0K);
            A0D.putString("novi_claims_receiver_label", string);
            A0D.putString("novi_claims_receiver_name", str2);
            A0D.putString("novi_claims_amount", r3.A00().AAA(r11.A03, r3.A08, 0));
            A0D.putString("novi_claims_tramsaction_timestamp", simpleDateFormat.format(new Date(r3.A05)));
            A0D.putString("novi_claims_claim_timestamp", simpleDateFormat.format(new Date(r8.A01)));
            A0D.putString("novi_claims_addotional_information", r8.A02);
            r1 = new C127495ua(0);
            r1.A01 = A0D;
            r11.A01.A0B(r1);
        }
        r1 = new C127495ua(2);
        r11.A01.A0B(r1);
    }
}
