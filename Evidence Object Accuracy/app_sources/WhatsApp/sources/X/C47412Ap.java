package X;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaFrameLayout;
import com.whatsapp.conversation.conversationrow.ConversationRowVideo$RowVideoView;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape14S0100000_I0_1;
import java.io.File;
import java.util.Collections;
import java.util.List;

/* renamed from: X.2Ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47412Ap extends AbstractC47422Aq {
    public int A00;
    public int A01;
    public long A02 = 0;
    public View A03 = findViewById(R.id.control_frame);
    public View A04 = findViewById(R.id.text_and_date);
    public ViewGroup A05 = ((ViewGroup) findViewById(R.id.video_containter));
    public ImageView A06 = ((ImageView) findViewById(R.id.gif_attribution));
    public ImageView A07 = ((ImageView) findViewById(R.id.button_image));
    public TextView A08 = ((TextView) findViewById(R.id.control_btn));
    public CircularProgressBar A09 = ((CircularProgressBar) findViewById(R.id.progress_bar));
    public C14330lG A0A;
    public TextEmojiLabel A0B = ((TextEmojiLabel) findViewById(R.id.caption));
    public ConversationRowVideo$RowVideoView A0C = ((ConversationRowVideo$RowVideoView) findViewById(R.id.thumb));
    public AnonymousClass21S A0D;
    public AnonymousClass19L A0E;
    public Runnable A0F;
    public Runnable A0G;
    public boolean A0H = false;
    public final AbstractC41521tf A0I = new C70183as(this);
    public final AbstractView$OnClickListenerC34281fs A0J = new ViewOnClickCListenerShape14S0100000_I0_1(this, 12);

    public C47412Ap(Context context, AbstractC13890kV r4, AnonymousClass1XR r5) {
        super(context, r4, r5);
        TextEmojiLabel textEmojiLabel = this.A0B;
        if (textEmojiLabel != null) {
            textEmojiLabel.A07 = new C52162aM();
        }
        this.A09.setMax(100);
        this.A09.A0B = 0;
        A0X(true);
    }

    private void A0X(boolean z) {
        View.OnClickListener onClickListener;
        Drawable A00;
        int i;
        int i2;
        int i3;
        AbstractView$OnClickListenerC34281fs r4;
        AbstractC16130oV r1 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
        AnonymousClass1IS r5 = r1.A0z;
        hashCode();
        C16150oX r7 = r1.A02;
        AnonymousClass009.A05(r7);
        if (z) {
            this.A08.setTag(Collections.singletonList(r1));
        }
        TextView textView = this.A08;
        textView.setVisibility(0);
        A1R();
        if (z) {
            A1S(true);
        }
        ConversationRowVideo$RowVideoView conversationRowVideo$RowVideoView = this.A0C;
        conversationRowVideo$RowVideoView.setKeepRatio(true);
        AbstractC16130oV fMessage = getFMessage();
        if (C30041Vv.A12(fMessage)) {
            textView.setVisibility(8);
            View view = this.A03;
            CircularProgressBar circularProgressBar = this.A09;
            AbstractC42671vd.A0a(view, circularProgressBar, textView, this.A07, true, !z, false);
            conversationRowVideo$RowVideoView.setVisibility(0);
            if (r5.A02) {
                r4 = ((AbstractC42671vd) this).A0A;
            } else {
                r4 = null;
            }
            conversationRowVideo$RowVideoView.setOnClickListener(r4);
            this.A05.setOnClickListener(r4);
            View.OnClickListener onClickListener2 = ((AbstractC42671vd) this).A07;
            textView.setOnClickListener(onClickListener2);
            circularProgressBar.setOnClickListener(onClickListener2);
        } else if (C30041Vv.A13(fMessage)) {
            View view2 = this.A03;
            CircularProgressBar circularProgressBar2 = this.A09;
            ImageView imageView = this.A07;
            AbstractC42671vd.A0a(view2, circularProgressBar2, textView, imageView, false, false, false);
            imageView.setVisibility(0);
            imageView.setImageResource(R.drawable.ic_gif_thumb);
            textView.setVisibility(8);
            ViewGroup viewGroup = this.A05;
            viewGroup.setOnClickListener(((AbstractC42671vd) this).A0A);
            viewGroup.setContentDescription(viewGroup.getContext().getString(R.string.view_gif_content_description));
            View.OnClickListener viewOnClickCListenerShape0S0200000_I0 = new ViewOnClickCListenerShape0S0200000_I0(this, 21, r1);
            textView.setOnClickListener(viewOnClickCListenerShape0S0200000_I0);
            conversationRowVideo$RowVideoView.setOnClickListener(viewOnClickCListenerShape0S0200000_I0);
            conversationRowVideo$RowVideoView.setContentDescription(conversationRowVideo$RowVideoView.getContext().getString(R.string.play_gif_descr));
            AbstractC13890kV r3 = ((AbstractC28551Oa) this).A0a;
            if (r3 != null && r3.AdM(r5)) {
                A1Q();
            }
        } else {
            ImageView imageView2 = this.A07;
            imageView2.setVisibility(8);
            if (!C30041Vv.A11(getFMessage())) {
                textView.setText(R.string.retry);
                textView.setContentDescription(getContext().getString(R.string.retry));
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_upload, 0, 0, 0);
                textView.setOnClickListener(((AbstractC42671vd) this).A09);
                onClickListener = ((AbstractC42671vd) this).A0A;
            } else {
                A16(textView, Collections.singletonList(r1), r1.A01);
                textView.setContentDescription(textView.getContext().getString(R.string.button_download));
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.btn_download, 0, 0, 0);
                onClickListener = this.A0J;
                textView.setOnClickListener(onClickListener);
            }
            conversationRowVideo$RowVideoView.setOnClickListener(onClickListener);
            AbstractC42671vd.A0a(this.A03, this.A09, textView, imageView2, false, !z, false);
        }
        A0w();
        View.OnLongClickListener onLongClickListener = this.A1a;
        conversationRowVideo$RowVideoView.setOnLongClickListener(onLongClickListener);
        this.A05.setOnLongClickListener(onLongClickListener);
        WaFrameLayout waFrameLayout = (WaFrameLayout) AnonymousClass028.A0D(this, R.id.media_container);
        boolean z2 = r5.A02;
        Context context = getContext();
        if (z2) {
            A00 = C92994Ym.A01(context);
            i = R.color.bubble_color_outgoing;
            i2 = R.color.bubble_color_outgoing_pressed;
        } else {
            A00 = C92994Ym.A00(context);
            i = R.color.bubble_color_incoming;
            i2 = R.color.bubble_color_incoming_pressed;
        }
        waFrameLayout.A03 = i;
        waFrameLayout.A02 = i2;
        waFrameLayout.setForeground(A00);
        int A01 = C27531Hw.A01(getContext());
        int A002 = AnonymousClass19O.A00(r1, A01);
        if (A002 <= 0) {
            A002 = (A01 * 9) >> 4;
        }
        this.A00 = A002;
        this.A01 = A01;
        conversationRowVideo$RowVideoView.A02(A01, A002, true);
        this.A1O.A07(conversationRowVideo$RowVideoView, r1, this.A0I);
        if (r1.A00 == 0) {
            r1.A00 = C22200yh.A07(r7.A0F);
        }
        C16150oX r2 = ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A02;
        AnonymousClass009.A05(r2);
        int i4 = r2.A05;
        ImageView imageView3 = this.A06;
        if (i4 != 1) {
            i3 = R.drawable.ic_attributes_tenor;
            if (i4 != 2) {
                imageView3.setVisibility(8);
                A1M(this.A04, this.A0B, getFMessage().A15());
                hashCode();
                A1N(r1);
            }
        } else {
            i3 = R.drawable.ic_attributes_giphy;
        }
        imageView3.setImageResource(i3);
        imageView3.setVisibility(0);
        A1M(this.A04, this.A0B, getFMessage().A15());
        hashCode();
        A1N(r1);
    }

    @Override // X.AnonymousClass1OY
    public int A0n(int i) {
        if (!TextUtils.isEmpty(((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A15())) {
            return super.A0n(i);
        }
        return 0;
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A0X(false);
        A1H(false);
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        boolean exists;
        C14900mE r1;
        int i;
        String str;
        if (((AbstractC42671vd) this).A01 == null || RequestPermissionActivity.A0W(getContext(), ((AbstractC42671vd) this).A01)) {
            AbstractC16130oV r3 = (AbstractC16130oV) ((AbstractC28551Oa) this).A0O;
            C16150oX r6 = r3.A02;
            AnonymousClass009.A05(r6);
            AnonymousClass1IS r5 = r3.A0z;
            boolean z = r5.A02;
            if (z || r6.A0P) {
                boolean z2 = true;
                if (z && !r6.A0P && !r6.A0O && (str = r6.A0H) != null && C22200yh.A0I(this.A0A, str).exists()) {
                    r1 = ((AnonymousClass1OY) this).A0J;
                    i = R.string.cannot_play_gif_wait_until_processed;
                } else if (r6.A07 == 1) {
                    r1 = ((AnonymousClass1OY) this).A0J;
                    i = R.string.gallery_unsafe_video_removed;
                } else {
                    File file = r6.A0F;
                    if (file == null) {
                        exists = false;
                    } else {
                        exists = new File(Uri.fromFile(file).getPath()).exists();
                    }
                    StringBuilder sb = new StringBuilder("viewmessage/ from_me:");
                    sb.append(z);
                    sb.append(" type:");
                    sb.append((int) r3.A0y);
                    sb.append(" url:");
                    sb.append(C37611mi.A00(r3.A08));
                    sb.append(" file:");
                    sb.append(r6.A0F);
                    sb.append(" progress:");
                    sb.append(r6.A0C);
                    sb.append(" transferred:");
                    sb.append(r6.A0P);
                    sb.append(" transferring:");
                    sb.append(r6.A0a);
                    sb.append(" fileSize:");
                    sb.append(r6.A0A);
                    sb.append(" media_size:");
                    sb.append(r3.A01);
                    sb.append(" timestamp:");
                    sb.append(r3.A0I);
                    Log.i(sb.toString());
                    if (!exists) {
                        A1P();
                        return;
                    }
                    View findViewById = findViewById(R.id.media_container);
                    C64533Fx r0 = ((AbstractC28551Oa) this).A0b;
                    if (r0 == null || !r0.A08()) {
                        z2 = false;
                    }
                    AnonymousClass2TS r12 = new AnonymousClass2TS(getContext());
                    r12.A07 = z2;
                    AbstractC14640lm r02 = r5.A00;
                    AnonymousClass009.A05(r02);
                    r12.A03 = r02;
                    r12.A04 = r5;
                    Intent A00 = r12.A00();
                    if (findViewById != null) {
                        AbstractC454421p.A07(getContext(), A00, findViewById);
                    }
                    AbstractC454421p.A08(getContext(), A00, findViewById, new AnonymousClass2TT(getContext()), AbstractC42671vd.A0Z(r5.toString()));
                    return;
                }
                r1.A05(i, 1);
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != ((AbstractC28551Oa) this).A0O) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A0X(z2);
        }
    }

    public final void A1P() {
        Log.w("viewmessage/ no file");
        AbstractC15340mz r1 = ((AbstractC28551Oa) this).A0O;
        if (A1O()) {
            return;
        }
        if (((AbstractC28551Oa) this).A0b.A08()) {
            ActivityC13810kN r12 = (ActivityC13810kN) AbstractC35731ia.A01(getContext(), ActivityC13810kN.class);
            if (r12 != null) {
                ((AbstractC28551Oa) this).A0P.A01(r12);
                return;
            }
            return;
        }
        Context context = getContext();
        AnonymousClass1IS r0 = r1.A0z;
        getContext().startActivity(C14960mK.A0D(context, r0.A00, r0.hashCode()));
    }

    public final void A1Q() {
        C16150oX r0 = ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A02;
        AnonymousClass009.A05(r0);
        File file = r0.A0F;
        if (file == null || !new File(Uri.fromFile(file).getPath()).exists()) {
            A1P();
        } else if (this.A0G == null && this.A0F == null) {
            RunnableBRunnable0Shape5S0100000_I0_5 runnableBRunnable0Shape5S0100000_I0_5 = new RunnableBRunnable0Shape5S0100000_I0_5(this, 5);
            this.A0G = runnableBRunnable0Shape5S0100000_I0_5;
            ((AnonymousClass1OY) this).A0J.A0H(runnableBRunnable0Shape5S0100000_I0_5);
        }
    }

    public final void A1R() {
        Runnable runnable = this.A0G;
        if (runnable != null) {
            ((AnonymousClass1OY) this).A0J.A0G(runnable);
        }
        Runnable runnable2 = this.A0F;
        if (runnable2 != null) {
            ((AnonymousClass1OY) this).A0J.A0G(runnable2);
        }
        this.A0G = null;
        this.A0F = null;
    }

    public final void A1S(boolean z) {
        AnonymousClass21S r0 = this.A0D;
        if (r0 != null) {
            r0.hashCode();
            AnonymousClass21S r3 = this.A0D;
            ((AnonymousClass21T) r3).A04 = null;
            r3.A0B = null;
            if (z) {
                C47432Ar r1 = this.A0E.A00;
                AnonymousClass009.A01();
                if (r1.A07.remove(r3)) {
                    List list = r1.A06;
                    list.add(r3);
                    r3.hashCode();
                    list.size();
                } else {
                    StringBuilder sb = new StringBuilder("ExoPlayerVideoPlayerPoolManager/releaseVideoPlayerInstance/playerNotProvidedByPool videoPlayerId=");
                    sb.append(r3.hashCode());
                    Log.e(sb.toString());
                }
            }
            this.A0D = null;
        }
        this.A0C.setVisibility(0);
        this.A03.setVisibility(0);
    }

    @Override // X.AnonymousClass1OY
    public int getBroadcastDrawableId() {
        if (TextUtils.isEmpty(((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A15())) {
            return R.drawable.broadcast_status_icon_onmedia;
        }
        return R.drawable.broadcast_status_icon;
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_gif_left;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public AnonymousClass1XR getFMessage() {
        return (AnonymousClass1XR) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_gif_left;
    }

    @Override // X.AnonymousClass1OY
    public Drawable getKeepDrawable() {
        if (TextUtils.isEmpty(((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A15())) {
            return AnonymousClass2GE.A01(getContext(), R.drawable.keep, R.color.white);
        }
        return super.getKeepDrawable();
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        int A01 = AnonymousClass3GD.A01(getContext(), 72);
        int i = this.A00;
        int i2 = this.A01;
        return i > i2 ? (int) ((((float) A01) / ((float) i)) * ((float) i2)) : A01;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_gif_right;
    }

    @Override // X.AbstractC28551Oa
    public int getReactionsViewVerticalOverlap() {
        if (TextUtils.isEmpty(((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A15())) {
            return getResources().getDimensionPixelOffset(R.dimen.space_tight_halfStep);
        }
        return super.getReactionsViewVerticalOverlap();
    }

    @Override // X.AnonymousClass1OY
    public Drawable getStarDrawable() {
        if (TextUtils.isEmpty(((AbstractC16130oV) ((AbstractC28551Oa) this).A0O).A15())) {
            return AnonymousClass00T.A04(getContext(), R.drawable.message_star_media);
        }
        return super.getStarDrawable();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        hashCode();
        super.onAttachedToWindow();
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        AnonymousClass1IS r1 = ((AbstractC28551Oa) this).A0O.A0z;
        hashCode();
        super.onDetachedFromWindow();
        A1R();
        A1S(true);
        AbstractC13890kV r0 = ((AbstractC28551Oa) this).A0a;
        if (r0 != null) {
            r0.A8w(r1);
        }
    }

    @Override // android.view.View
    public void onFinishTemporaryDetach() {
        hashCode();
        super.onFinishTemporaryDetach();
    }

    @Override // android.view.View
    public void onStartTemporaryDetach() {
        AbstractC13890kV r2;
        AnonymousClass1X3 r7 = (AnonymousClass1X3) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        hashCode();
        super.onStartTemporaryDetach();
        if (this.A0D != null) {
            long j = this.A02;
            if (j > 0 && (r2 = ((AbstractC28551Oa) this).A0a) != null) {
                r2.AfW(r7, System.currentTimeMillis() - j);
                this.A02 = 0;
            }
        }
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof AnonymousClass1XR);
        super.setFMessage(r2);
    }
}
