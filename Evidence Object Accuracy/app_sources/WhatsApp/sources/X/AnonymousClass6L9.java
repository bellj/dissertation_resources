package X;

import java.util.TimerTask;
import org.npci.commonlibrary.NPCIFragment;

/* renamed from: X.6L9  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6L9 extends TimerTask {
    public final /* synthetic */ NPCIFragment A00;

    public AnonymousClass6L9(NPCIFragment nPCIFragment) {
        this.A00 = nPCIFragment;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        NPCIFragment nPCIFragment = this.A00;
        if (!nPCIFragment.A08.optString("resendOTPFeature", "false").equals("false") && !nPCIFragment.A0A) {
            nPCIFragment.A0B().runOnUiThread(new RunnableC135096Hd(nPCIFragment));
        }
    }
}
