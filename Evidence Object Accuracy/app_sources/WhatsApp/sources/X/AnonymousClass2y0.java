package X;

import android.content.Context;
import android.provider.ContactsContract;
import android.text.TextPaint;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.2y0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2y0 extends AnonymousClass1OY {
    public int A00;
    public C254919p A01;
    public AnonymousClass1AR A02;
    public AnonymousClass10A A03;
    public AnonymousClass130 A04;
    public AnonymousClass190 A05;
    public C20730wE A06;
    public C628438u A07;
    public C64343Fe A08;
    public C26171Ch A09;
    public C17990rj A0A;
    public C39271pZ A0B;
    public C30721Yo A0C;
    public boolean A0D;
    public boolean A0E;
    public final ImageView A0F;
    public final LinearLayout A0G;
    public final LinearLayout A0H;
    public final TextView A0I;
    public final TextView A0J;
    public final TextView A0K;
    public final TextView A0L;
    public final TextView A0M;
    public final TextView A0N;
    public final AnonymousClass1J1 A0O;
    public final C39091pH A0P;
    public final AbstractC35401hl A0Q;
    public final ArrayList A0R = C12960it.A0l();
    public final ArrayList A0S = C12960it.A0l();

    public AnonymousClass2y0(Context context, AnonymousClass1J1 r4, AbstractC13890kV r5, AbstractC15340mz r6, C39091pH r7) {
        super(context, r5, r6);
        A0Z();
        this.A0O = r4;
        this.A0P = r7;
        this.A0N = C12960it.A0J(this, R.id.vcard_text);
        this.A0I = C12960it.A0J(this, R.id.account_type);
        this.A0L = C12960it.A0J(this, R.id.description);
        this.A0F = C12970iu.A0L(this, R.id.picture);
        this.A0M = C12960it.A0J(this, R.id.msg_contact_btn);
        this.A0J = C12960it.A0J(this, R.id.action_contact_btn);
        this.A0G = (LinearLayout) findViewById(R.id.action_view_business_container);
        this.A0K = C12960it.A0J(this, R.id.action_view_business);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.contact_card);
        this.A0H = linearLayout;
        linearLayout.setOnLongClickListener(this.A1a);
        this.A0Q = C65213Iq.A00(context);
        A1M();
    }

    @Override // X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A0D) {
            this.A0D = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            this.A01 = (C254919p) A08.A6M.get();
            this.A02 = (AnonymousClass1AR) A08.ALM.get();
            this.A04 = C12990iw.A0Y(A08);
            this.A05 = (AnonymousClass190) A08.AIZ.get();
            this.A06 = (C20730wE) A08.A4J.get();
            this.A03 = (AnonymousClass10A) A08.A2W.get();
            this.A0A = A08.A2e();
            this.A09 = (C26171Ch) A08.A2a.get();
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, getFMessage());
        super.A1D(r2, z);
        if (z || A1X) {
            A1M();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b4, code lost:
        if (r7.A04 == false) goto L_0x00b6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A1M() {
        /*
            r16 = this;
            r5 = r16
            X.0mz r6 = r5.getFMessage()
            android.widget.TextView r1 = r5.A0N
            X.1IS r0 = r6.A0z
            r1.setTag(r0)
            X.1pZ r1 = r5.A0B
            if (r1 == 0) goto L_0x0016
            X.1pH r0 = r5.A0P
            r0.A03(r1)
        L_0x0016:
            X.1pH r1 = r5.A0P
            monitor-enter(r1)
            r3 = 0
            java.lang.Runnable r2 = r1.A01(r6, r3)     // Catch: all -> 0x00cc
            monitor-exit(r1)
            X.1pZ r2 = (X.C39271pZ) r2
            r5.A0B = r2
            X.3bK r1 = new X.3bK
            r1.<init>()
            X.0mE r0 = r5.A0J
            java.util.concurrent.Executor r0 = r0.A06
            r2.A01(r1, r0)
            android.content.Context r0 = r5.getContext()
            java.lang.String r0 = X.C47962Dl.A01(r0, r6)
            r5.A1N(r0)
            X.130 r2 = r5.A04
            android.widget.ImageView r1 = r5.A0F
            r0 = 2131231140(0x7f0801a4, float:1.8078353E38)
            r2.A05(r1, r0)
            r0 = 1024(0x400, float:1.435E-42)
            boolean r1 = r6.A12(r0)
            r0 = 2131366796(0x7f0a138c, float:1.8353496E38)
            if (r1 == 0) goto L_0x0050
            goto L_0x0067
        L_0x0050:
            android.view.ViewGroup r1 = X.C12980iv.A0P(r5, r0)
            if (r1 == 0) goto L_0x00cb
            X.3Fe r0 = r5.A08
            if (r0 == 0) goto L_0x0061
            com.whatsapp.webpagepreview.WebPagePreviewView r0 = r0.A0A
            r1.removeView(r0)
            r5.A08 = r3
        L_0x0061:
            r0 = 8
            r1.setVisibility(r0)
            return
        L_0x0067:
            android.view.ViewGroup r3 = X.C12980iv.A0P(r5, r0)
            if (r3 == 0) goto L_0x00cb
            X.3IJ r7 = X.AnonymousClass3IJ.A00(r6)
            r0 = 0
            r3.setVisibility(r0)
            X.3Fe r0 = r5.A08
            if (r0 != 0) goto L_0x00ab
            android.content.Context r9 = r5.getContext()
            X.18U r10 = r5.A0K
            X.1BK r14 = r5.A0t
            X.19O r15 = r5.A1O
            X.0zP r11 = r5.A0W
            X.0mE r4 = r5.A0J
            X.0lR r2 = r5.A1P
            X.0t3 r1 = r5.A0O
            X.14x r0 = r5.A0q
            X.4Sf r12 = new X.4Sf
            r12.<init>(r4, r1, r0, r2)
            X.0rj r13 = r5.A0A
            X.3Fe r8 = new X.3Fe
            r8.<init>(r9, r10, r11, r12, r13, r14, r15)
            r5.A08 = r8
            com.whatsapp.webpagepreview.WebPagePreviewView r2 = r8.A0A
            r1 = -1
            r0 = -2
            r3.addView(r2, r1, r0)
            X.3Fe r0 = r5.A08
            com.whatsapp.webpagepreview.WebPagePreviewView r1 = r0.A0A
            android.view.View$OnLongClickListener r0 = r5.A1a
            r1.setOnLongClickListener(r0)
        L_0x00ab:
            X.1hl r8 = r5.A0Q
            boolean r0 = r8 instanceof X.AnonymousClass2I3
            if (r0 == 0) goto L_0x00b6
            boolean r0 = r7.A04
            r9 = 1
            if (r0 != 0) goto L_0x00b7
        L_0x00b6:
            r9 = 0
        L_0x00b7:
            r5.A0E = r9
            X.3Fe r4 = r5.A08
            X.1Ch r0 = r5.A09
            boolean r10 = r0.A00(r6)
            X.1Ch r0 = r5.A09
            boolean r11 = r0.A01(r6)
            r12 = 0
            r4.A02(r5, r6, r7, r8, r9, r10, r11, r12)
        L_0x00cb:
            return
        L_0x00cc:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2y0.A1M():void");
    }

    public final void A1N(String str) {
        CharSequence A02;
        String A03 = AnonymousClass1US.A03(128, str);
        Context context = getContext();
        TextView textView = this.A0N;
        TextPaint paint = textView.getPaint();
        AnonymousClass19M r1 = this.A10;
        if (A03 == null) {
            A02 = null;
        } else {
            A02 = AbstractC36671kL.A02(context, paint, r1, new AnonymousClass56Y(), A03);
        }
        textView.setText(A0q(A02));
    }

    public final boolean A1O(C30721Yo r7) {
        boolean z;
        if (r7 != null) {
            List list = r7.A05;
            if (list != null) {
                Iterator it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((AnonymousClass1OY) this).A0L.A0F(((C30741Yq) it.next()).A01)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (list.size() > 0 && !z) {
                    return true;
                }
            }
            List<AnonymousClass4TL> list2 = r7.A02;
            if (list2 != null) {
                for (AnonymousClass4TL r0 : list2) {
                    if (r0.A01 == ContactsContract.CommonDataKinds.Email.class) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_contact_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_contact_left;
    }

    @Override // X.AbstractC28551Oa
    public int getMainChildMaxWidth() {
        if (this.A0E) {
            return AnonymousClass3GD.A02(this);
        }
        return 0;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_contact_right;
    }

    @Override // X.AnonymousClass1OY, android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        C628438u r1 = this.A07;
        if (r1 != null) {
            r1.A03(true);
            this.A07 = null;
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setText(this.A0N.getText());
    }

    @Override // android.view.View
    public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onPopulateAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.getText().add(this.A0N.getText());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (X.C30041Vv.A0r(r3) != false) goto L_0x000b;
     */
    @Override // X.AbstractC28551Oa
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setFMessage(X.AbstractC15340mz r3) {
        /*
            r2 = this;
            boolean r0 = r3 instanceof X.C30411Xh
            if (r0 != 0) goto L_0x000b
            boolean r1 = X.C30041Vv.A0r(r3)
            r0 = 0
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            r0 = 1
        L_0x000c:
            X.AnonymousClass009.A0F(r0)
            r2.A0O = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2y0.setFMessage(X.0mz):void");
    }
}
