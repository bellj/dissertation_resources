package X;

import android.os.Bundle;

/* renamed from: X.5bZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118285bZ extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C128345vx A01;

    public C118285bZ(Bundle bundle, C128345vx r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123555nL.class)) {
            C128345vx r1 = this.A01;
            C14830m7 r2 = r1.A0A;
            C14900mE r22 = r1.A01;
            C15570nT r23 = r1.A02;
            C16590pI r24 = r1.A0B;
            AbstractC14440lR r25 = r1.A0e;
            C241414j r26 = r1.A0H;
            AnonymousClass14X r27 = r1.A0d;
            C15550nR r28 = r1.A07;
            AnonymousClass01d r29 = r1.A09;
            AnonymousClass018 r210 = r1.A0C;
            C17070qD r211 = r1.A0V;
            C238013b r212 = r1.A06;
            C15650ng r15 = r1.A0D;
            AnonymousClass1AA r14 = r1.A05;
            AnonymousClass604 r13 = r1.A0b;
            C20300vX r12 = r1.A0E;
            C21860y6 r11 = r1.A0N;
            C22710zW r10 = r1.A0T;
            AnonymousClass102 r9 = r1.A0G;
            C14650lo r8 = r1.A04;
            C129925yW r7 = r1.A0L;
            AbstractC16870pt r6 = r1.A0Y;
            AnonymousClass17Z r5 = r1.A0Z;
            C20370ve r4 = r1.A0F;
            AnonymousClass1A7 r3 = r1.A0W;
            C243515e r213 = r1.A0R;
            return new C123555nL(this.A00, r22, r23, r8, r14, r212, r28, r29, r2, r24, r210, r15, r12, r4, r9, r26, r7, r1.A0M, r11, r213, r10, r211, r3, r6, r5, r13, r27, r25);
        }
        throw C12970iu.A0f("View model type mismatch");
    }
}
