package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.4vf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106414vf implements AnonymousClass5VW {
    public Timeline A00;
    public final Object A01;

    public C106414vf(Timeline timeline, Object obj) {
        this.A01 = obj;
        this.A00 = timeline;
    }

    @Override // X.AnonymousClass5VW
    public Timeline AHE() {
        return this.A00;
    }

    @Override // X.AnonymousClass5VW
    public Object AHN() {
        return this.A01;
    }
}
