package X;

/* renamed from: X.5FN  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FN implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FN(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r6) {
        appendable.append('\"');
        String obj2 = obj.toString();
        if (obj2 != null) {
            r6.A02.A9f(appendable, obj2);
        }
        appendable.append('\"');
    }
}
