package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import java.util.List;

/* renamed from: X.5c0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118555c0 extends AnonymousClass02M {
    public List A00 = C12960it.A0l();
    public final C15550nR A01;
    public final C15610nY A02;
    public final C14830m7 A03;
    public final AnonymousClass018 A04;
    public final C17070qD A05;
    public final AnonymousClass14X A06;

    public C118555c0(C15550nR r2, C15610nY r3, C14830m7 r4, AnonymousClass018 r5, C17070qD r6, AnonymousClass14X r7) {
        this.A03 = r4;
        this.A06 = r7;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public void ANH(AnonymousClass03U r15, int i) {
        C118805cP r152 = (C118805cP) r15;
        C127045tr r3 = (C127045tr) this.A00.get(i);
        AnonymousClass1IR r9 = r3.A00;
        WaTextView waTextView = r152.A02;
        View view = r152.A0H;
        waTextView.setText(C130345zG.A00(view.getContext(), r9.A03));
        C117695aS r1 = new C117695aS(view.getContext());
        C14830m7 r7 = r152.A05;
        AnonymousClass14X r12 = r152.A08;
        r1.A6W(new C128205vj(r152.A03, r152.A04, r7, r152.A06, r9, r152.A07, null, r12, false));
        r1.A02(false);
        LinearLayout linearLayout = r152.A00;
        linearLayout.addView(r1, linearLayout.indexOfChild(waTextView) + 1);
        C118525bx r13 = new C118525bx();
        RecyclerView recyclerView = r152.A01;
        recyclerView.setAdapter(r13);
        view.getContext();
        C12990iw.A1K(recyclerView);
        r13.A00 = r3.A01;
        r13.A02();
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C14830m7 r4 = this.A03;
        AnonymousClass14X r7 = this.A06;
        AnonymousClass018 r5 = this.A04;
        return new C118805cP(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.novi_text_input_transaction), this.A01, this.A02, r4, r5, this.A05, r7);
    }
}
