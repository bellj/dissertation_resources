package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.55a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1103055a implements AbstractC33171dZ {
    public final C15550nR A00;

    public C1103055a(C15550nR r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC33171dZ
    public boolean A9v(AbstractC14640lm r3) {
        return (r3 instanceof UserJid) && this.A00.A0a((UserJid) r3);
    }
}
