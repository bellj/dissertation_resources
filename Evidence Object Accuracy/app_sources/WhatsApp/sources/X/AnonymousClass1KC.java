package X;

import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.1KC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1KC implements AbstractC14470lU {
    public int A00;
    public C14460lT A01;
    public boolean A02;
    public byte[] A03;
    public final C14620lj A04;
    public final C14620lj A05;
    public final C14620lj A06;
    public final C14620lj A07 = new C14620lj();
    public final C14620lj A08 = new C14620lj();
    public final C14620lj A09 = new C14620lj();
    public final C14620lj A0A = new C14620lj();
    public final C14620lj A0B;
    public final C14620lj A0C;
    public final C14620lj A0D = new C14620lj();
    public final C14620lj A0E;
    public final C14620lj A0F;
    public final C14620lj A0G;
    public final C14620lj A0H;
    public final AbstractC14500lX A0I;
    public final C14430lQ A0J;
    public final C14450lS A0K;
    public final AnonymousClass1K9 A0L;
    public final AbstractC14590lg A0M;
    public final AbstractC14590lg A0N;
    public final AbstractC14590lg A0O;
    public final AbstractC14590lg A0P;
    public final AbstractC14590lg A0Q;
    public final Object A0R = new Object();
    public final AtomicBoolean A0S = new AtomicBoolean();
    public final AtomicBoolean A0T = new AtomicBoolean();
    public volatile String A0U;

    public AnonymousClass1KC(AbstractC14500lX r9, C14430lQ r10, C14450lS r11, AnonymousClass1K9 r12) {
        C14620lj r5 = new C14620lj();
        this.A0C = r5;
        C14620lj r4 = new C14620lj();
        this.A06 = r4;
        C14620lj r3 = new C14620lj();
        this.A0B = r3;
        C14620lj r2 = new C14620lj();
        this.A0F = r2;
        C14620lj r1 = new C14620lj();
        this.A0G = r1;
        this.A0H = new C14620lj();
        this.A0E = new C14620lj();
        this.A04 = new C14620lj();
        this.A05 = new C14620lj();
        this.A0N = r3;
        this.A0O = r5;
        this.A0M = r4;
        this.A0P = r2;
        this.A0Q = r1;
        this.A0L = r12;
        this.A0J = r10;
        this.A0K = r11;
        AnonymousClass1qQ r52 = r12.A01;
        int i = r52.A01;
        byte b = r52.A05.A00;
        int A00 = C33761f2.A00(b, i, false);
        synchronized (r11) {
            r11.A07 = Integer.valueOf(A00);
            r11.A0B = false;
        }
        synchronized (r11) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 4) {
                        switch (i) {
                            case 8:
                            case 9:
                            case 12:
                                r11.A00 = 4;
                                break;
                            case 10:
                                break;
                            case 11:
                                break;
                            default:
                                r11.A00 = 1;
                                break;
                        }
                    }
                    r11.A00 = 2;
                }
                r11.A00 = 3;
            } else {
                if (A00 != 3) {
                    if (A00 == 2) {
                    }
                    r11.A00 = 1;
                }
                r11.A00 = 2;
            }
        }
        boolean z = r12.A00.A0B;
        synchronized (r11) {
            r11.A0A = z;
        }
        Boolean valueOf = Boolean.valueOf(C30041Vv.A0I(b));
        synchronized (r11) {
            r11.A06 = valueOf;
        }
        File file = r52.A07;
        if (file != null) {
            this.A08.A04(new C39871qg(file, true));
        }
        this.A0I = r9;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C14380lL A00() {
        /*
            r30 = this;
            r1 = r30
            X.1K9 r0 = r1.A0L
            X.0lL r13 = r0.A00
            java.lang.String r0 = r1.A0U
            if (r0 == 0) goto L_0x0094
            java.lang.String r11 = r1.A0U
        L_0x000c:
            X.0lj r0 = r1.A08
            java.lang.Object r7 = r0.A00()
            X.1qg r7 = (X.C39871qg) r7
            X.0lj r0 = r1.A0D
            java.lang.Object r6 = r0.A00()
            X.1qS r6 = (X.AbstractC39731qS) r6
            java.io.File r8 = r13.A06
            java.lang.String r3 = r13.A08
            java.lang.String r12 = r13.A07
            long r1 = r13.A02
            int[] r10 = r13.A0E
            java.lang.String r4 = r13.A09
            int r5 = r13.A00
            r18 = 0
            if (r7 == 0) goto L_0x008f
            java.io.File r9 = r7.A01
            boolean r0 = r9.equals(r8)
            if (r0 != 0) goto L_0x008f
            long r1 = r7.A00
            r12 = r18
            r7 = 1
        L_0x003b:
            boolean r0 = r6 instanceof X.AnonymousClass45H
            if (r0 == 0) goto L_0x004c
            r0 = r6
            X.45H r0 = (X.AnonymousClass45H) r0
            int[] r3 = r0.A05
            boolean r0 = java.util.Arrays.equals(r3, r10)
            if (r0 != 0) goto L_0x004c
            r10 = r3
            r7 = 1
        L_0x004c:
            boolean r0 = r6 instanceof X.AnonymousClass45G
            if (r0 == 0) goto L_0x008d
            X.45G r6 = (X.AnonymousClass45G) r6
            int r8 = r6.A02
            if (r8 == r5) goto L_0x008d
            r7 = 1
        L_0x0057:
            boolean r0 = r11.equals(r4)
            if (r0 == 0) goto L_0x0060
            r11 = r4
            if (r7 == 0) goto L_0x008c
        L_0x0060:
            X.0lK r7 = r13.A05
            X.0lM r14 = r13.A03
            int r6 = r13.A01
            boolean r5 = r13.A0C
            boolean r4 = r13.A0A
            boolean r3 = r13.A0D
            boolean r0 = r13.A0B
            r15 = 0
            r22 = r6
            r23 = r8
            r24 = r1
            r26 = r5
            r27 = r4
            r28 = r3
            r29 = r0
            r20 = r11
            r21 = r10
            r19 = r12
            r17 = r9
            r16 = r7
            X.0lL r13 = new X.0lL
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r26, r27, r28, r29)
        L_0x008c:
            return r13
        L_0x008d:
            r8 = r5
            goto L_0x0057
        L_0x008f:
            r7 = 0
            r9 = r8
            r18 = r3
            goto L_0x003b
        L_0x0094:
            java.lang.String r11 = "optimistic"
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1KC.A00():X.0lL");
    }

    public AnonymousClass1qQ A01() {
        AnonymousClass1qQ r14 = this.A0L.A01;
        C39871qg r1 = (C39871qg) this.A08.A00();
        if (r1 == null) {
            return r14;
        }
        String str = r14.A08;
        File file = r1.A01;
        String str2 = r14.A0B;
        String str3 = r14.A09;
        String str4 = r14.A0A;
        C14370lK r12 = r14.A05;
        int i = r14.A00;
        int i2 = r14.A01;
        long j = r14.A02;
        long j2 = r14.A03;
        boolean z = r14.A0D;
        boolean z2 = r14.A0G;
        boolean z3 = r14.A0C;
        return new AnonymousClass1qQ(r14.A04, r12, r14.A06, file, str, str2, str3, str4, i, i2, j, j2, z, z2, z3, false, r14.A0F);
    }

    public void A02() {
        this.A09.A01();
        this.A08.A01();
        this.A07.A01();
        this.A0A.A01();
        this.A0C.A01();
        this.A0B.A01();
        this.A0D.A01();
        this.A0F.A01();
        this.A0G.A01();
        this.A0H.A01();
        this.A0E.A01();
        this.A06.A01();
    }

    public void A03(AbstractC14590lg r2, Executor executor) {
        this.A0A.A03(r2, executor);
    }

    @Override // X.AbstractC14470lU
    public String ADi() {
        StringBuilder sb = new StringBuilder("mediajob/");
        sb.append(this.A0J.A0D);
        return sb.toString();
    }
}
