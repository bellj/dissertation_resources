package X;

import java.util.Collection;

/* renamed from: X.2Rv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50912Rv {
    public static boolean containsAllImpl(Collection collection, Collection collection2) {
        for (Object obj : collection2) {
            if (!collection.contains(obj)) {
                return false;
            }
        }
        return true;
    }

    public static Collection filter(Collection collection, AnonymousClass28S r2) {
        if (collection instanceof C71713dO) {
            return ((C71713dO) collection).createCombined(r2);
        }
        return new C71713dO(collection, r2);
    }

    public static StringBuilder newStringBuilderForCollection(int i) {
        C28251Mi.checkNonnegative(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824L));
    }

    public static boolean safeContains(Collection collection, Object obj) {
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    public static Collection transform(Collection collection, AbstractC469028d r2) {
        return new C71693dM(collection, r2);
    }
}
