package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1zS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44731zS implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C99594kY();
    public int A00;
    public String A01;
    public String A02;
    public final List A03;
    public final boolean A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C44731zS(Parcel parcel) {
        this.A00 = parcel.readInt();
        this.A02 = parcel.readString();
        this.A01 = parcel.readString();
        this.A04 = parcel.readByte() != 0;
        this.A03 = parcel.createStringArrayList();
    }

    public C44731zS(String str, String str2, List list, int i, boolean z) {
        this.A00 = i;
        this.A04 = z;
        this.A03 = list;
        this.A02 = str;
        this.A01 = str2;
    }

    public static int A00(String str) {
        if (AnonymousClass1US.A0C(str) || "approved".equalsIgnoreCase(str)) {
            return 0;
        }
        if ("rejected".equalsIgnoreCase(str)) {
            return 2;
        }
        return "deleted".equalsIgnoreCase(str) ? 3 : 1;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        List list;
        if (this != obj) {
            if (obj instanceof C44731zS) {
                C44731zS r8 = (C44731zS) obj;
                if (this.A00 == r8.A00 && this.A04 == r8.A04 && C29941Vi.A00(this.A02, r8.A02) && C29941Vi.A00(this.A01, r8.A01)) {
                    List list2 = this.A03;
                    if (list2 != null && (list = r8.A03) != null && list2.size() == list.size()) {
                        for (int i = 0; i < list2.size(); i++) {
                            if (((String) list2.get(i)).equals(list.get(i))) {
                            }
                        }
                    } else if (list2 != r8.A03) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.deepHashCode(new Object[]{Integer.valueOf(this.A00), Boolean.valueOf(this.A04), this.A02, this.A01, this.A03});
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A01);
        parcel.writeByte(this.A04 ? (byte) 1 : 0);
        parcel.writeStringList(this.A03);
    }
}
