package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56922mB extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C56922mB A02;
    public static volatile AnonymousClass255 A03;
    public int A00;
    public String A01 = "";

    static {
        C56922mB r0 = new C56922mB();
        A02 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A02;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C56922mB r9 = (C56922mB) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A01;
                int i2 = r9.A00;
                this.A01 = r8.Afy(str, r9.A01, A1R, C12960it.A1R(i2));
                if (r8 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A032 = r82.A03();
                        if (A032 == 0) {
                            break;
                        } else if (A032 == 10) {
                            String A0A = r82.A0A();
                            this.A00 = 1 | this.A00;
                            this.A01 = A0A;
                        } else if (!A0a(r82, A032)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C56922mB();
            case 5:
                return new C81523u9();
            case 6:
                break;
            case 7:
                if (A03 == null) {
                    synchronized (C56922mB.class) {
                        if (A03 == null) {
                            A03 = AbstractC27091Fz.A09(A02);
                        }
                    }
                }
                return A03;
            default:
                throw C12970iu.A0z();
        }
        return A02;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A01, 0);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
