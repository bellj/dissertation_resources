package X;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.facebook.rendercore.RootHostView;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3gY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73633gY extends GestureDetector.SimpleOnGestureListener {
    public Bundle A00;
    public ActivityC000800j A01;
    public AnonymousClass01E A02;
    public RootHostView A03;
    public C64893Hi A04;
    public AnonymousClass1BZ A05;
    public C64173En A06;
    public AbstractC115835Tc A07;
    public AbstractC116545Vw A08;
    public HashMap A09;
    public Map A0A;

    public void A00() {
        AbstractC116545Vw r1;
        C64893Hi r2 = this.A04;
        if (r2 != null && (r1 = this.A08) != null) {
            r1.AJJ(Boolean.TRUE);
            RootHostView rootHostView = this.A03;
            if (rootHostView != null) {
                r2.A05(rootHostView);
            }
            this.A08.AJJ(Boolean.FALSE);
        }
    }

    public void A01(Bundle bundle, ActivityC000800j r4, AnonymousClass01E r5, C64173En r6, AbstractC115835Tc r7, AbstractC116545Vw r8, String str, HashMap hashMap) {
        this.A01 = r4;
        this.A02 = r5;
        this.A08 = r8;
        this.A07 = r7;
        AnonymousClass547 r1 = new AnonymousClass547(this);
        this.A00 = bundle;
        this.A09 = hashMap;
        this.A06 = r6;
        bundle.getBoolean("hot_reload");
        this.A05.AAv(r1, str, hashMap);
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
    }
}
