package X;

import java.io.Closeable;
import java.io.InputStream;

/* renamed from: X.5B9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5B9 implements Closeable {
    public C93884au A00 = C93884au.A01;
    public final C08870bz A01;

    public AnonymousClass5B9(C08870bz r2) {
        AnonymousClass0RA.A00(C08870bz.A01(r2));
        this.A01 = r2.clone();
    }

    public InputStream A00() {
        C08870bz r2;
        C08870bz r0 = this.A01;
        if (r0 != null) {
            r2 = r0.A03();
        } else {
            r2 = null;
        }
        if (r2 == null) {
            return null;
        }
        try {
            return new C03650Ip((AbstractC12930in) r2.A04());
        } finally {
            r2.close();
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        C08870bz r0 = this.A01;
        if (r0 != null) {
            r0.close();
        }
    }
}
