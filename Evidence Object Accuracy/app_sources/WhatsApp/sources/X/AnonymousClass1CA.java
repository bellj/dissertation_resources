package X;

import android.content.SharedPreferences;

/* renamed from: X.1CA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CA {
    public SharedPreferences A00;
    public final C16630pM A01;

    public AnonymousClass1CA(C16630pM r1) {
        this.A01 = r1;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("daily_metrics_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
