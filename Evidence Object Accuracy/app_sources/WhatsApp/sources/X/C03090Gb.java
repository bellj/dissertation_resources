package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0Gb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03090Gb extends AnonymousClass0Zt {
    public C03090Gb(Context context, AbstractC11500gO r3) {
        super(C05990Rt.A00(context, r3).A02);
    }

    @Override // X.AnonymousClass0Zt
    public boolean A01(C004401z r4) {
        return r4.A09.A03 == EnumC004001t.CONNECTED;
    }

    @Override // X.AnonymousClass0Zt
    public boolean A02(Object obj) {
        C05410Pl r5 = (C05410Pl) obj;
        int i = Build.VERSION.SDK_INT;
        boolean z = r5.A00;
        if (i < 26) {
            return !z;
        }
        if (!z || !r5.A03) {
            return true;
        }
        return false;
    }
}
