package X;

import android.content.Context;

/* renamed from: X.5xF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129145xF {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C18650sn A03;
    public final C18610sj A04;
    public final C30931Zj A05 = C117305Zk.A0V("PaymentStepUpWebviewAction", "network");

    public C129145xF(Context context, C14900mE r4, C18640sm r5, C18650sn r6, C18610sj r7) {
        this.A00 = context;
        this.A01 = r4;
        this.A04 = r7;
        this.A02 = r5;
        this.A03 = r6;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(X.AnonymousClass6MW r14, X.C32631cT r15, X.AnonymousClass1V8 r16) {
        /*
            r13 = this;
            r6 = 1
            X.1W9[] r2 = new X.AnonymousClass1W9[r6]
            java.lang.String r1 = r15.A02
            java.lang.String r0 = "step_up_id"
            boolean r5 = X.C12990iw.A1a(r0, r1, r2)
            java.lang.String r0 = "step_up"
            X.1V8 r4 = new X.1V8
            r4.<init>(r0, r2)
            X.1W9[] r3 = new X.AnonymousClass1W9[r6]
            X.24d r0 = r15.A00
            java.lang.String r1 = r0.A01
            java.lang.String r0 = "challenge_id"
            X.C12960it.A1M(r0, r1, r3, r5)
            java.lang.String r0 = "step_up_challenge"
            X.1V8 r2 = new X.1V8
            r2.<init>(r0, r3)
            r1 = 2
            if (r16 == 0) goto L_0x005a
            r0 = 3
            X.1V8[] r3 = new X.AnonymousClass1V8[r0]
            r3[r5] = r4
            r3[r6] = r2
            r3[r1] = r16
        L_0x0030:
            X.1W9[] r2 = new X.AnonymousClass1W9[r6]
            java.lang.String r1 = "action"
            java.lang.String r0 = "get-step-up-webview-url"
            X.C12960it.A1M(r1, r0, r2, r5)
            java.lang.String r0 = "account"
            X.1V8 r9 = new X.1V8
            r9.<init>(r0, r2, r3)
            r5 = r13
            X.0sj r7 = r13.A04
            android.content.Context r1 = r13.A00
            X.0mE r2 = r13.A01
            X.0sn r3 = r13.A03
            r6 = 14
            r4 = r14
            com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1 r0 = new com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r11 = 0
            java.lang.String r10 = "get"
            r8 = r0
            r7.A0F(r8, r9, r10, r11)
            return
        L_0x005a:
            X.1V8[] r3 = new X.AnonymousClass1V8[r1]
            r3[r5] = r4
            r3[r6] = r2
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C129145xF.A00(X.6MW, X.1cT, X.1V8):void");
    }
}
