package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0aS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08020aS implements AbstractC12850if, AbstractC12030hG, AbstractC12480hz, AbstractC12860ig {
    public Paint A00;
    public RectF A01;
    public AnonymousClass0QD A02;
    public List A03;
    public final Matrix A04;
    public final Path A05;
    public final RectF A06;
    public final AnonymousClass0AA A07;
    public final String A08;
    public final List A09;
    public final boolean A0A;

    public C08020aS(AnonymousClass0AA r5, C08170ah r6, AbstractC08070aX r7, String str, List list, boolean z) {
        this.A00 = new C020609t();
        this.A01 = new RectF();
        this.A04 = new Matrix();
        this.A05 = new Path();
        this.A06 = new RectF();
        this.A08 = str;
        this.A07 = r5;
        this.A0A = z;
        this.A09 = list;
        if (r6 != null) {
            AnonymousClass0QD r0 = new AnonymousClass0QD(r6);
            this.A02 = r0;
            r0.A03(r7);
            this.A02.A02(this);
        }
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            Object obj = list.get(size);
            if (obj instanceof AbstractC12020hF) {
                arrayList.add(obj);
            }
        }
        int size2 = arrayList.size();
        while (true) {
            size2--;
            if (size2 >= 0) {
                ((AbstractC12020hF) arrayList.get(size2)).A5W(list.listIterator(list.size()));
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C08020aS(X.AnonymousClass0AA r10, X.C08220am r11, X.AbstractC08070aX r12) {
        /*
            r9 = this;
            java.lang.String r6 = r11.A00
            boolean r8 = r11.A02
            java.util.List r2 = r11.A01
            int r0 = r2.size()
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>(r0)
            r1 = 0
        L_0x0010:
            int r0 = r2.size()
            r3 = r10
            r5 = r12
            if (r1 >= r0) goto L_0x002a
            java.lang.Object r0 = r2.get(r1)
            X.0hH r0 = (X.AbstractC12040hH) r0
            X.0hy r0 = r0.Aes(r10, r12)
            if (r0 == 0) goto L_0x0027
            r7.add(r0)
        L_0x0027:
            int r1 = r1 + 1
            goto L_0x0010
        L_0x002a:
            r1 = 0
        L_0x002b:
            int r0 = r2.size()
            if (r1 >= r0) goto L_0x0045
            java.lang.Object r4 = r2.get(r1)
            X.0hH r4 = (X.AbstractC12040hH) r4
            boolean r0 = r4 instanceof X.C08170ah
            if (r0 == 0) goto L_0x0042
            X.0ah r4 = (X.C08170ah) r4
        L_0x003d:
            r2 = r9
            r2.<init>(r3, r4, r5, r6, r7, r8)
            return
        L_0x0042:
            int r1 = r1 + 1
            goto L_0x002b
        L_0x0045:
            r4 = 0
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08020aS.<init>(X.0AA, X.0am, X.0aX):void");
    }

    public List A00() {
        if (this.A03 == null) {
            this.A03 = new ArrayList();
            int i = 0;
            while (true) {
                List list = this.A09;
                if (i >= list.size()) {
                    break;
                }
                Object obj = list.get(i);
                if (obj instanceof AbstractC12850if) {
                    this.A03.add(obj);
                }
                i++;
            }
        }
        return this.A03;
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r2, Object obj) {
        AnonymousClass0QD r0 = this.A02;
        if (r0 != null) {
            r0.A04(r2, obj);
        }
    }

    @Override // X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        boolean z;
        int intValue;
        if (!this.A0A) {
            Matrix matrix2 = this.A04;
            matrix2.set(matrix);
            AnonymousClass0QD r1 = this.A02;
            if (r1 != null) {
                matrix2.preConcat(r1.A00());
                AnonymousClass0QR r0 = r1.A02;
                if (r0 == null) {
                    intValue = 100;
                } else {
                    intValue = ((Number) r0.A03()).intValue();
                }
                i = (int) ((((((float) intValue) / 100.0f) * ((float) i)) / 255.0f) * 255.0f);
            }
            if (this.A07.A0C) {
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    List list = this.A09;
                    if (i2 >= list.size()) {
                        break;
                    } else if (!(list.get(i2) instanceof AbstractC12860ig) || (i3 = i3 + 1) < 2) {
                        i2++;
                    } else if (i != 255) {
                        z = true;
                        RectF rectF = this.A01;
                        rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
                        AAy(matrix2, rectF, true);
                        Paint paint = this.A00;
                        paint.setAlpha(i);
                        AnonymousClass0UV.A03(canvas, paint, rectF, 31);
                        i = 255;
                    }
                }
            }
            z = false;
            List list2 = this.A09;
            for (int size = list2.size() - 1; size >= 0; size--) {
                Object obj = list2.get(size);
                if (obj instanceof AbstractC12860ig) {
                    ((AbstractC12860ig) obj).A9C(canvas, matrix2, i);
                }
            }
            if (z) {
                canvas.restore();
            }
        }
    }

    @Override // X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        Matrix matrix2 = this.A04;
        matrix2.set(matrix);
        AnonymousClass0QD r0 = this.A02;
        if (r0 != null) {
            matrix2.preConcat(r0.A00());
        }
        RectF rectF2 = this.A06;
        rectF2.set(0.0f, 0.0f, 0.0f, 0.0f);
        List list = this.A09;
        int size = list.size();
        while (true) {
            size--;
            if (size >= 0) {
                AbstractC12470hy r1 = (AbstractC12470hy) list.get(size);
                if (r1 instanceof AbstractC12860ig) {
                    ((AbstractC12860ig) r1).AAy(matrix2, rectF2, z);
                    rectF.union(rectF2);
                }
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC12850if
    public Path AF0() {
        Matrix matrix = this.A04;
        matrix.reset();
        AnonymousClass0QD r0 = this.A02;
        if (r0 != null) {
            matrix.set(r0.A00());
        }
        Path path = this.A05;
        path.reset();
        if (!this.A0A) {
            List list = this.A09;
            int size = list.size();
            while (true) {
                size--;
                if (size < 0) {
                    break;
                }
                AbstractC12470hy r1 = (AbstractC12470hy) list.get(size);
                if (r1 instanceof AbstractC12850if) {
                    path.addPath(((AbstractC12850if) r1).AF0(), matrix);
                }
            }
        }
        return path;
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A07.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r4, C06430To r5, List list, int i) {
        String str = this.A08;
        if (r4.A02(str, i) || "__container".equals(str)) {
            if (!"__container".equals(str)) {
                C06430To r1 = new C06430To(r5);
                r1.A01.add(str);
                r5 = r1;
                if (r4.A01(str, i)) {
                    C06430To r0 = new C06430To(r1);
                    r0.A00 = this;
                    list.add(r0);
                }
            }
            if (r4.A03(str, i)) {
                int A00 = i + r4.A00(str, i);
                int i2 = 0;
                while (true) {
                    List list2 = this.A09;
                    if (i2 < list2.size()) {
                        AbstractC12470hy r12 = (AbstractC12470hy) list2.get(i2);
                        if (r12 instanceof AbstractC12480hz) {
                            ((AbstractC12480hz) r12).Aan(r4, r5, list, A00);
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        int size = list.size();
        List list3 = this.A09;
        ArrayList arrayList = new ArrayList(size + list3.size());
        arrayList.addAll(list);
        int size2 = list3.size();
        while (true) {
            size2--;
            if (size2 >= 0) {
                AbstractC12470hy r1 = (AbstractC12470hy) list3.get(size2);
                r1.Aby(arrayList, list3.subList(0, size2));
                arrayList.add(r1);
            } else {
                return;
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A08;
    }
}
