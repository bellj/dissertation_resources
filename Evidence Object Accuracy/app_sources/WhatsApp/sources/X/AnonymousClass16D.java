package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.16D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16D {
    public SharedPreferences A00;
    public SharedPreferences A01;
    public C16120oU A02 = null;
    public HashMap A03 = null;
    public final C14830m7 A04;
    public final C16630pM A05;

    public AnonymousClass16D(C14830m7 r2, C16630pM r3) {
        this.A04 = r2;
        this.A05 = r3;
    }

    public final SharedPreferences.Editor A00() {
        SharedPreferences sharedPreferences;
        synchronized (this) {
            sharedPreferences = this.A01;
            if (sharedPreferences == null) {
                sharedPreferences = this.A05.A01("psid_store");
                this.A01 = sharedPreferences;
            }
        }
        return sharedPreferences.edit();
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        try {
            sharedPreferences = this.A00;
            if (sharedPreferences == null) {
                sharedPreferences = this.A05.A01("ps_mini_buffer_sequence_number_store");
                this.A00 = sharedPreferences;
            }
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                throw th2;
            }
        }
        return sharedPreferences;
    }

    public synchronized String A02(int i) {
        String str;
        if (i == 0) {
            str = "00000000-0000-0000-0000-000000000000";
        } else {
            HashMap hashMap = this.A03;
            if (hashMap == null) {
                hashMap = A03();
                this.A03 = hashMap;
            }
            Integer valueOf = Integer.valueOf(i);
            if (hashMap.containsKey(valueOf)) {
                AnonymousClass1N3 r5 = (AnonymousClass1N3) this.A03.get(valueOf);
                if (r5 != null) {
                    int i2 = r5.A02;
                    if (i2 == -1) {
                        str = r5.A01;
                    } else {
                        long A00 = this.A04.A00() / 86400000;
                        if (A00 - r5.A00 >= ((long) i2)) {
                            String str2 = r5.A01;
                            r5.A01 = UUID.randomUUID().toString();
                            r5.A00 = A00;
                            this.A03.put(valueOf, r5);
                            A05(r5, valueOf);
                            A01().edit().remove(str2).apply();
                            A04(2, i, i2);
                        }
                        str = r5.A01;
                    }
                } else {
                    throw new IllegalArgumentException("Invalid psIdKey");
                }
            } else {
                throw new IllegalArgumentException("Invalid psIdKey");
            }
        }
        return str;
    }

    public synchronized HashMap A03() {
        SharedPreferences sharedPreferences = this.A01;
        if (sharedPreferences == null) {
            sharedPreferences = this.A05.A01("psid_store");
            this.A01 = sharedPreferences;
        }
        Map<String, ?> all = sharedPreferences.getAll();
        this.A03 = new HashMap();
        for (Map.Entry<String, ?> entry : all.entrySet()) {
            try {
                Integer valueOf = Integer.valueOf(entry.getKey());
                try {
                    JSONObject jSONObject = new JSONObject((String) entry.getValue());
                    String string = jSONObject.getString("psIdValue");
                    int i = jSONObject.getInt("rotationInDays");
                    long j = jSONObject.getLong("lastRotationTimeUtcDay");
                    jSONObject.getInt("beaconEvtNumber");
                    this.A03.put(valueOf, new AnonymousClass1N3(i, string, j));
                } catch (JSONException e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("psidstore/loadFromFile bad json ");
                    sb.append(e.toString());
                    Log.e(sb.toString());
                    A00().remove(entry.getKey()).apply();
                }
            } catch (NumberFormatException unused) {
                Log.e("psidstore/loadfromfile ps-id key is corrupted");
            }
        }
        return this.A03;
    }

    public final void A04(int i, int i2, int i3) {
        C16120oU r3 = this.A02;
        if (r3 != null) {
            AnonymousClass1N4 r2 = new AnonymousClass1N4();
            r2.A00 = Integer.valueOf(i);
            r2.A01 = Long.valueOf((long) i2);
            r2.A02 = Long.valueOf((long) i3);
            r3.A05(r2);
        }
    }

    public synchronized void A05(AnonymousClass1N3 r5, Integer num) {
        HashMap hashMap = this.A03;
        if (hashMap == null) {
            hashMap = A03();
            this.A03 = hashMap;
        }
        if (!hashMap.containsKey(num)) {
            throw new IllegalArgumentException("Invalid PS-ID key");
        } else if (r5 == null) {
            this.A03.remove(num);
            A00().remove(num.toString()).apply();
        } else {
            this.A03.put(num, r5);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("psIdValue", r5.A01);
                jSONObject.put("rotationInDays", r5.A02);
                jSONObject.put("lastRotationTimeUtcDay", r5.A00);
                jSONObject.put("beaconEvtNumber", 0);
                A00().putString(num.toString(), jSONObject.toString()).apply();
            } catch (JSONException e) {
                StringBuilder sb = new StringBuilder();
                sb.append("psidstore/updatePsIdRecord JEX ");
                sb.append(e.toString());
                Log.e(sb.toString());
            }
        }
    }

    public synchronized void A06(String str, ArrayList arrayList) {
        int i;
        String str2;
        int i2;
        HashMap hashMap = this.A03;
        if (hashMap == null) {
            hashMap = A03();
            this.A03 = hashMap;
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it = hashMap.keySet().iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            Integer num = (Integer) it.next();
            while (true) {
                if (i >= arrayList.size()) {
                    arrayList2.add(num);
                    break;
                } else if (num.intValue() != ((AnonymousClass1N5) arrayList.get(i)).A00) {
                    i++;
                }
            }
        }
        for (int i3 = 0; i3 < arrayList2.size(); i3++) {
            Integer num2 = (Integer) arrayList2.get(i3);
            AnonymousClass1N3 r0 = (AnonymousClass1N3) this.A03.get(num2);
            if (r0 != null) {
                i2 = r0.A02;
                A01().edit().remove(r0.A01).apply();
            } else {
                i2 = 0;
            }
            A05(null, num2);
            A04(3, num2.intValue(), i2);
        }
        long A00 = this.A04.A00();
        while (i < arrayList.size()) {
            AnonymousClass1N5 r6 = (AnonymousClass1N5) arrayList.get(i);
            if (!this.A03.containsKey(Integer.valueOf(r6.A00))) {
                if (str == null || r6.A00 != 113760892) {
                    str2 = UUID.randomUUID().toString();
                    A04(1, r6.A00, r6.A01);
                } else {
                    str2 = str;
                }
                AnonymousClass1N3 r2 = new AnonymousClass1N3(r6.A01, str2, A00 / 86400000);
                this.A03.put(Integer.valueOf(r6.A00), r2);
                A05(r2, Integer.valueOf(r6.A00));
            }
            i++;
        }
    }
}
