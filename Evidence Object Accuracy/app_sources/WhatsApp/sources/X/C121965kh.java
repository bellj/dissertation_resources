package X;

import com.whatsapp.R;

/* renamed from: X.5kh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121965kh extends AbstractC128945wv {
    public final int A00 = R.string.order_details_whatsapp_payment_option_subtitle;
    public final int A01 = R.string.order_details_whatsapp_payment_option_title;

    public C121965kh(C127005tn r4, boolean z) {
        super(r4, 0, z);
    }
}
