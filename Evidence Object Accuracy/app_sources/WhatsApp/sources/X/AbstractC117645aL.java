package X;

import android.content.Context;
import android.widget.FrameLayout;

/* renamed from: X.5aL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC117645aL extends FrameLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC117645aL(Context context) {
        super(context, null, 0);
        if (!this.A01) {
            this.A01 = true;
            C123675ne r2 = (C123675ne) this;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            r2.A00 = C12960it.A0Q(r1);
            r2.A01 = C117315Zl.A0H(r1);
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
