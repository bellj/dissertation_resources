package X;

/* renamed from: X.1b4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31761b4 {
    public C31281aI A00;

    public C31761b4(C31731b1 r5, int i) {
        AnonymousClass1G4 A0T = C31281aI.A04.A0T();
        A0T.A03();
        C31281aI r1 = (C31281aI) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = i;
        byte[] A00 = r5.A01.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C31281aI r12 = (C31281aI) A0T.A00;
        r12.A00 |= 2;
        r12.A03 = A01;
        byte[] bArr = r5.A00.A00;
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C31281aI r13 = (C31281aI) A0T.A00;
        r13.A00 |= 4;
        r13.A02 = A012;
        this.A00 = (C31281aI) A0T.A02();
    }

    public C31761b4(byte[] bArr) {
        this.A00 = (C31281aI) AbstractC27091Fz.A0E(C31281aI.A04, bArr);
    }

    public C31731b1 A00() {
        try {
            C31281aI r1 = this.A00;
            return new C31731b1(new C31721b0(r1.A02.A04()), C31481ac.A00(r1.A03.A04()));
        } catch (C31561ak e) {
            throw new AssertionError(e);
        }
    }
}
