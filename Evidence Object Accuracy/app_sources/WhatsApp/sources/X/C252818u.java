package X;

import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.18u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C252818u {
    public static final Map A05;
    public final AnonymousClass12P A00;
    public final C14900mE A01;
    public final AnonymousClass01d A02;
    public final AnonymousClass018 A03;
    public final C22650zQ A04;

    static {
        HashMap hashMap = new HashMap();
        A05 = hashMap;
        hashMap.put("terms-of-service", "https://www.whatsapp.com/legal/#terms-of-service");
        hashMap.put("privacy-policy", "https://www.whatsapp.com/legal/#privacy-policy");
        hashMap.put("end-to-end-encryption", "https://faq.whatsapp.com/general/28030015/");
        hashMap.put("facebook-companies", "https://www.facebook.com/help/111814505650678");
        hashMap.put("how-whatsapp-works-with-the-facebook-companies", "https://faq.whatsapp.com/general/26000112/");
        hashMap.put("privacy-policy-managing-and-deleting-your-information", "https://www.whatsapp.com/legal/#privacy-policy-managing-and-deleting-your-information");
        hashMap.put("privacy-policy-our-global-operations", "https://www.whatsapp.com/legal/#privacy-policy-our-global-operations");
        hashMap.put("terms-of-service-age", "https://www.whatsapp.com/legal/#terms-of-service-age");
        hashMap.put("cookies", "https://www.whatsapp.com/legal/#cookies");
    }

    public C252818u(AnonymousClass12P r1, C14900mE r2, AnonymousClass01d r3, AnonymousClass018 r4, C22650zQ r5) {
        this.A01 = r2;
        this.A04 = r5;
        this.A00 = r1;
        this.A02 = r3;
        this.A03 = r4;
    }

    public Uri A00(String str) {
        String str2;
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        AnonymousClass018 r2 = this.A03;
        buildUpon.appendQueryParameter("lg", r2.A06());
        buildUpon.appendQueryParameter("lc", r2.A05());
        if (this.A04.A04()) {
            str2 = "1";
        } else {
            str2 = "0";
        }
        buildUpon.appendQueryParameter("eea", str2);
        return buildUpon.build();
    }
}
