package X;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;
import com.whatsapp.WaTextView;

/* renamed from: X.2hv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC55182hv extends AnonymousClass03U {
    public final WaImageButton A00;
    public final WaTextView A01;

    public AbstractC55182hv(View view) {
        super(view);
        this.A00 = (WaImageButton) AnonymousClass028.A0D(view, R.id.category_icon);
        this.A01 = C12960it.A0N(view, R.id.category_name);
    }

    public void A08(int i) {
        WaImageButton waImageButton = this.A00;
        Drawable A03 = C015607k.A03(waImageButton.getBackground());
        C015607k.A0A(A03, i);
        if (Build.VERSION.SDK_INT >= 23) {
            A03 = AnonymousClass2GE.A02(this.A0H.getContext(), A03);
        }
        waImageButton.setBackground(A03);
    }
}
