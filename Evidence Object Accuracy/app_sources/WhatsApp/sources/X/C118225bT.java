package X;

/* renamed from: X.5bT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118225bT extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;

    public C118225bT(C128375w0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C117915ay.class)) {
            C128375w0 r0 = this.A00;
            return new C117915ay(r0.A0T, r0.A0V);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviServiceSelectionBottomSheet");
    }
}
