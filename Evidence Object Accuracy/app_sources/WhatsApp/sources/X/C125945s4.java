package X;

import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.5s4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125945s4 {
    public final Map A00;

    public C125945s4(String str) {
        HashMap A11 = C12970iu.A11();
        if (str != null) {
            try {
                JSONArray jSONArray = C13000ix.A05(str).getJSONArray("payment_options");
                for (int i = 0; i < jSONArray.length(); i++) {
                    C1315763h r1 = new C1315763h(jSONArray.getJSONObject(i));
                    A11.put(r1.A06, r1);
                }
            } catch (JSONException e) {
                Log.e(C12960it.A0d(e.getMessage(), C12960it.A0k("OrderDetailsPaymentOptions/parseOptions failed to parse payment options json: ")));
            }
        }
        this.A00 = A11;
    }
}
