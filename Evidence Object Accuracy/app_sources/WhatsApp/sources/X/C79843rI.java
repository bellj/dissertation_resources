package X;

import android.os.IBinder;

/* renamed from: X.3rI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79843rI extends C65873Li implements AnonymousClass5YK {
    public C79843rI(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }
}
