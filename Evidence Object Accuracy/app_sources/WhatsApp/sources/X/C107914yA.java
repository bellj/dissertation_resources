package X;

import android.os.Handler;

/* renamed from: X.4yA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107914yA implements AnonymousClass5QQ {
    public final Handler A00;

    public C107914yA(Handler handler) {
        this.A00 = handler;
    }

    public static void A00(Object obj, int i, Object obj2) {
        ((C107914yA) obj).A00.obtainMessage(i, obj2).sendToTarget();
    }
}
