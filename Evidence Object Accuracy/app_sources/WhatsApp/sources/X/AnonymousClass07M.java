package X;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import android.view.InflateException;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: X.07M  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07M extends AbstractC013706k implements Animatable {
    public Animator.AnimatorListener A00 = null;
    public ArgbEvaluator A01 = null;
    public Context A02;
    public AnonymousClass09w A03;
    public C020809x A04;
    public ArrayList A05 = null;
    public final Drawable.Callback A06 = new AnonymousClass0VD(this);

    public static boolean A06(int i) {
        return i >= 28 && i <= 31;
    }

    public AnonymousClass07M(Context context) {
        this.A02 = context;
        this.A03 = new AnonymousClass09w();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:192:0x000b */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:193:0x000b */
    /* JADX DEBUG: Multi-variable search result rejected for r1v5, resolved type: android.animation.PropertyValuesHolder[] */
    /* JADX DEBUG: Multi-variable search result rejected for r3v21, resolved type: android.animation.Animator[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01c6, code lost:
        if (r0 == null) goto L_0x0151;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x02df, code lost:
        if (r1 == null) goto L_0x02e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x02e7, code lost:
        if (r1 != null) goto L_0x02cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0322, code lost:
        if (r24 == null) goto L_0x0347;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0324, code lost:
        if (r21 == null) goto L_0x0347;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0326, code lost:
        r3 = new android.animation.Animator[r21.size()];
        r2 = r21.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0334, code lost:
        if (r2.hasNext() == false) goto L_0x0340;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x0336, code lost:
        r3[r11] = r2.next();
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0340, code lost:
        if (r30 != 0) goto L_0x0348;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x0342, code lost:
        r24.playTogether(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0347, code lost:
        return r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0348, code lost:
        r24.playSequentially(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x034d, code lost:
        return r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x015c, code lost:
        if (A06(r0.type) != false) goto L_0x015e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.animation.Animator A00(android.animation.AnimatorSet r24, android.content.Context r25, android.content.res.Resources.Theme r26, android.content.res.Resources r27, android.util.AttributeSet r28, org.xmlpull.v1.XmlPullParser r29, int r30) {
        /*
        // Method dump skipped, instructions count: 846
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07M.A00(android.animation.AnimatorSet, android.content.Context, android.content.res.Resources$Theme, android.content.res.Resources, android.util.AttributeSet, org.xmlpull.v1.XmlPullParser, int):android.animation.Animator");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:80:0x012e, code lost:
        if (r16 == 0) goto L_0x0130;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.animation.PropertyValuesHolder A01(android.content.res.TypedArray r14, java.lang.String r15, int r16, int r17, int r18) {
        /*
        // Method dump skipped, instructions count: 343
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07M.A01(android.content.res.TypedArray, java.lang.String, int, int, int):android.animation.PropertyValuesHolder");
    }

    public static ValueAnimator A02(ValueAnimator valueAnimator, Context context, Resources.Theme theme, Resources resources, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int resourceId;
        String string;
        String string2;
        String string3;
        String string4;
        PropertyValuesHolder propertyValuesHolder;
        PropertyValuesHolder propertyValuesHolder2;
        PropertyValuesHolder[] propertyValuesHolderArr;
        boolean z;
        int i6;
        int i7;
        ValueAnimator valueAnimator2 = valueAnimator;
        TypedArray A01 = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A02);
        TypedArray A012 = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A06);
        if (valueAnimator == null) {
            valueAnimator2 = new ValueAnimator();
        }
        if (!AnonymousClass06r.A02("duration", xmlPullParser)) {
            i = 300;
        } else {
            i = A01.getInt(1, 300);
        }
        long j = (long) i;
        if (!AnonymousClass06r.A02("startOffset", xmlPullParser)) {
            i2 = 0;
        } else {
            i2 = A01.getInt(2, 0);
        }
        long j2 = (long) i2;
        if (!AnonymousClass06r.A02("valueType", xmlPullParser)) {
            i3 = 4;
        } else {
            i3 = A01.getInt(7, 4);
        }
        if (AnonymousClass06r.A02("valueFrom", xmlPullParser) && AnonymousClass06r.A02("valueTo", xmlPullParser)) {
            if (i3 == 4) {
                TypedValue peekValue = A01.peekValue(5);
                boolean z2 = true;
                i3 = 0;
                if (peekValue != null) {
                    z = true;
                    i6 = peekValue.type;
                } else {
                    z = false;
                    i6 = 0;
                }
                TypedValue peekValue2 = A01.peekValue(6);
                if (peekValue2 != null) {
                    i7 = peekValue2.type;
                } else {
                    z2 = false;
                    i7 = 0;
                }
                if ((z && A06(i6)) || (z2 && A06(i7))) {
                    i3 = 3;
                }
            }
            PropertyValuesHolder A013 = A01(A01, "", i3, 5, 6);
            if (A013 != null) {
                valueAnimator2.setValues(A013);
            }
        }
        valueAnimator2.setDuration(j);
        valueAnimator2.setStartDelay(j2);
        if (!AnonymousClass06r.A02("repeatCount", xmlPullParser)) {
            i4 = 0;
        } else {
            i4 = A01.getInt(3, 0);
        }
        valueAnimator2.setRepeatCount(i4);
        if (!AnonymousClass06r.A02("repeatMode", xmlPullParser)) {
            i5 = 1;
        } else {
            i5 = A01.getInt(4, 1);
        }
        valueAnimator2.setRepeatMode(i5);
        if (A012 != null) {
            ObjectAnimator objectAnimator = (ObjectAnimator) valueAnimator2;
            if (!AnonymousClass06r.A02("pathData", xmlPullParser) || (string2 = A012.getString(1)) == null) {
                if (!AnonymousClass06r.A02("propertyName", xmlPullParser)) {
                    string = null;
                } else {
                    string = A012.getString(0);
                }
                objectAnimator.setPropertyName(string);
            } else {
                if (!AnonymousClass06r.A02("propertyXName", xmlPullParser)) {
                    string3 = null;
                } else {
                    string3 = A012.getString(2);
                }
                if (!AnonymousClass06r.A02("propertyYName", xmlPullParser)) {
                    string4 = null;
                } else {
                    string4 = A012.getString(3);
                }
                if (string3 == null && string4 == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(A012.getPositionDescription());
                    sb.append(" propertyXName or propertyYName is needed for PathData");
                    throw new InflateException(sb.toString());
                }
                Path A00 = C014306u.A00(string2);
                float f = 1.0f * 0.5f;
                PathMeasure pathMeasure = new PathMeasure(A00, false);
                ArrayList arrayList = new ArrayList();
                float f2 = 0.0f;
                arrayList.add(Float.valueOf(0.0f));
                float f3 = 0.0f;
                do {
                    f3 += pathMeasure.getLength();
                    arrayList.add(Float.valueOf(f3));
                } while (pathMeasure.nextContour());
                PathMeasure pathMeasure2 = new PathMeasure(A00, false);
                int min = Math.min(100, ((int) (f3 / f)) + 1);
                float[] fArr = new float[min];
                float[] fArr2 = new float[min];
                float[] fArr3 = new float[2];
                float f4 = f3 / ((float) (min - 1));
                int i8 = 0;
                int i9 = 0;
                while (true) {
                    propertyValuesHolder = null;
                    if (i8 >= min) {
                        break;
                    }
                    pathMeasure2.getPosTan(f2 - ((Number) arrayList.get(i9)).floatValue(), fArr3, null);
                    fArr[i8] = fArr3[0];
                    fArr2[i8] = fArr3[1];
                    f2 += f4;
                    int i10 = i9 + 1;
                    if (i10 < arrayList.size() && f2 > ((Number) arrayList.get(i10)).floatValue()) {
                        pathMeasure2.nextContour();
                        i9 = i10;
                    }
                    i8++;
                }
                if (string3 != null) {
                    propertyValuesHolder2 = PropertyValuesHolder.ofFloat(string3, fArr);
                } else {
                    propertyValuesHolder2 = null;
                }
                if (string4 != null) {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(string4, fArr2);
                }
                if (propertyValuesHolder2 == null) {
                    propertyValuesHolderArr = new PropertyValuesHolder[]{propertyValuesHolder};
                } else {
                    propertyValuesHolderArr = propertyValuesHolder == null ? new PropertyValuesHolder[]{propertyValuesHolder2} : new PropertyValuesHolder[]{propertyValuesHolder2, propertyValuesHolder};
                }
                objectAnimator.setValues(propertyValuesHolderArr);
            }
        }
        if (AnonymousClass06r.A02("interpolator", xmlPullParser) && (resourceId = A01.getResourceId(0, 0)) > 0) {
            valueAnimator2.setInterpolator(A03(context, resourceId));
        }
        A01.recycle();
        if (A012 != null) {
            A012.recycle();
        }
        return valueAnimator2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0105, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0108, code lost:
        return r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.view.animation.Interpolator A03(android.content.Context r6, int r7) {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass07M.A03(android.content.Context, int):android.view.animation.Interpolator");
    }

    public static AnonymousClass07M A04(Context context, int i) {
        int next;
        if (Build.VERSION.SDK_INT >= 24) {
            AnonymousClass07M r2 = new AnonymousClass07M(context);
            Drawable A04 = AnonymousClass00X.A04(context.getTheme(), context.getResources(), i);
            ((AbstractC013706k) r2).A00 = A04;
            A04.setCallback(r2.A06);
            r2.A04 = new C020809x(((AbstractC013706k) r2).A00.getConstantState());
            return r2;
        }
        try {
            XmlResourceParser xml = context.getResources().getXml(i);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            do {
                next = xml.next();
                if (next == 2) {
                    Resources resources = context.getResources();
                    Resources.Theme theme = context.getTheme();
                    AnonymousClass07M r0 = new AnonymousClass07M(context);
                    r0.inflate(resources, xml, asAttributeSet, theme);
                    return r0;
                }
            } while (next != 1);
            throw new XmlPullParserException("No start tag found");
        } catch (IOException | XmlPullParserException e) {
            Log.e("AnimatedVDCompat", "parser error", e);
            return null;
        }
    }

    public static void A05(AnimatedVectorDrawable animatedVectorDrawable, AnonymousClass03J r2) {
        animatedVectorDrawable.registerAnimationCallback(r2.A00());
    }

    public final void A07(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            for (int i = 0; i < childAnimations.size(); i++) {
                A07(childAnimations.get(i));
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                ArgbEvaluator argbEvaluator = this.A01;
                if (argbEvaluator == null) {
                    argbEvaluator = new ArgbEvaluator();
                    this.A01 = argbEvaluator;
                }
                objectAnimator.setEvaluator(argbEvaluator);
            }
        }
    }

    public void A08(AnonymousClass03J r3) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            A05((AnimatedVectorDrawable) drawable, r3);
            return;
        }
        ArrayList arrayList = this.A05;
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.A05 = arrayList;
        }
        if (!arrayList.contains(r3)) {
            this.A05.add(r3);
            Animator.AnimatorListener animatorListener = this.A00;
            if (animatorListener == null) {
                animatorListener = new AnonymousClass09A(this);
                this.A00 = animatorListener;
            }
            this.A03.A00.addListener(animatorListener);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean canApplyTheme() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A0E(drawable);
        }
        return false;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        AnonymousClass09w r1 = this.A03;
        r1.A02.draw(canvas);
        if (r1.A00.isStarted()) {
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getAlpha() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A00(drawable);
        }
        return this.A03.A02.getAlpha();
    }

    @Override // android.graphics.drawable.Drawable
    public int getChangingConfigurations() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | 0;
    }

    @Override // android.graphics.drawable.Drawable
    public ColorFilter getColorFilter() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A02(drawable);
        }
        return this.A03.A02.getColorFilter();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable.ConstantState getConstantState() {
        Drawable drawable = super.A00;
        if (drawable == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new C020809x(drawable.getConstantState());
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.A03.A02.getIntrinsicHeight();
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.A03.A02.getIntrinsicWidth();
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.A03.A02.getOpacity();
    }

    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    @Override // android.graphics.drawable.Drawable
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) {
        TypedArray obtainAttributes;
        Animator A00;
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A05(theme, resources, drawable, attributeSet, xmlPullParser);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    obtainAttributes = AnonymousClass06r.A01(theme, resources, attributeSet, AnonymousClass06q.A00);
                    int resourceId = obtainAttributes.getResourceId(0, 0);
                    if (resourceId != 0) {
                        C013606j A01 = C013606j.A01(theme, resources, resourceId);
                        A01.A04 = false;
                        A01.setCallback(this.A06);
                        AnonymousClass09w r2 = this.A03;
                        C013606j r1 = r2.A02;
                        if (r1 != null) {
                            r1.setCallback(null);
                        }
                        r2.A02 = A01;
                    }
                } else if ("target".equals(name)) {
                    obtainAttributes = resources.obtainAttributes(attributeSet, AnonymousClass06q.A01);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.A02;
                        if (context != null) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                A00 = AnimatorInflater.loadAnimator(context, resourceId2);
                            } else {
                                Resources resources2 = context.getResources();
                                Resources.Theme theme2 = context.getTheme();
                                XmlResourceParser xmlResourceParser = null;
                                try {
                                    try {
                                        xmlResourceParser = resources2.getAnimation(resourceId2);
                                        A00 = A00(null, context, theme2, resources2, Xml.asAttributeSet(xmlResourceParser), xmlResourceParser, 0);
                                        if (xmlResourceParser != null) {
                                            xmlResourceParser.close();
                                        }
                                    } catch (IOException e) {
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("Can't load animation resource ID #0x");
                                        sb.append(Integer.toHexString(resourceId2));
                                        Resources.NotFoundException notFoundException = new Resources.NotFoundException(sb.toString());
                                        notFoundException.initCause(e);
                                        throw notFoundException;
                                    } catch (XmlPullParserException e2) {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("Can't load animation resource ID #0x");
                                        sb2.append(Integer.toHexString(resourceId2));
                                        Resources.NotFoundException notFoundException2 = new Resources.NotFoundException(sb2.toString());
                                        notFoundException2.initCause(e2);
                                        throw notFoundException2;
                                    }
                                } catch (Throwable th) {
                                    if (xmlResourceParser != null) {
                                        xmlResourceParser.close();
                                    }
                                    throw th;
                                }
                            }
                            AnonymousClass09w r12 = this.A03;
                            A00.setTarget(r12.A02.A03.A08.A0E.get(string));
                            if (Build.VERSION.SDK_INT < 21) {
                                A07(A00);
                            }
                            ArrayList arrayList = r12.A03;
                            if (arrayList == null) {
                                arrayList = new ArrayList();
                                r12.A03 = arrayList;
                                r12.A01 = new AnonymousClass00N();
                            }
                            arrayList.add(A00);
                            r12.A01.put(A00, string);
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                } else {
                    continue;
                }
                obtainAttributes.recycle();
            }
            eventType = xmlPullParser.next();
        }
        AnonymousClass09w r0 = this.A03;
        AnimatorSet animatorSet = r0.A00;
        if (animatorSet == null) {
            animatorSet = new AnimatorSet();
            r0.A00 = animatorSet;
        }
        animatorSet.playTogether(r0.A03);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isAutoMirrored() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return C015607k.A0F(drawable);
        }
        return this.A03.A02.isAutoMirrored();
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return ((AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.A03.A00.isRunning();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean isStateful() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.A03.A02.isStateful();
    }

    @Override // android.graphics.drawable.Drawable
    public Drawable mutate() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    @Override // X.AbstractC013706k, android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        Drawable drawable = super.A00;
        if (drawable == null) {
            drawable = this.A03.A02;
        }
        drawable.setBounds(rect);
    }

    @Override // X.AbstractC013706k, android.graphics.drawable.Drawable
    public boolean onLevelChange(int i) {
        Drawable drawable = super.A00;
        if (drawable == null) {
            drawable = this.A03.A02;
        }
        return drawable.setLevel(i);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.A03.A02.setState(iArr);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.setAlpha(i);
        } else {
            this.A03.A02.setAlpha(i);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAutoMirrored(boolean z) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A0C(drawable, z);
        } else {
            this.A03.A02.setAutoMirrored(z);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.A03.A02.setColorFilter(colorFilter);
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTint(int i) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A0A(drawable, i);
        } else {
            this.A03.A02.setTint(i);
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A04(colorStateList, drawable);
        } else {
            this.A03.A02.setTintList(colorStateList);
        }
    }

    @Override // android.graphics.drawable.Drawable, X.AbstractC013806l
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            C015607k.A07(mode, drawable);
        } else {
            this.A03.A02.setTintMode(mode);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = super.A00;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.A03.A02.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
            return;
        }
        AnonymousClass09w r1 = this.A03;
        if (!r1.A00.isStarted()) {
            r1.A00.start();
            invalidateSelf();
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        Drawable drawable = super.A00;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.A03.A00.end();
        }
    }
}
