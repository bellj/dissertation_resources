package X;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.R;
import com.whatsapp.stickers.RemoveStickerFromFavoritesDialogFragment;
import com.whatsapp.stickers.StarOrRemoveFromRecentsStickerDialogFragment;
import com.whatsapp.stickers.StarStickerFromPickerDialogFragment;
import java.util.List;

/* renamed from: X.3Yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC69213Yj implements AnonymousClass5WC {
    public int A00;
    public int A01;
    public int A02;
    public GridLayoutManager A03;
    public RecyclerView A04;
    public C54392ge A05;
    public boolean A06 = false;
    public final int A07;
    public final Context A08;
    public final LayoutInflater A09;
    public final C14850m9 A0A;
    public final AnonymousClass4LI A0B = new AnonymousClass4LI(this);

    public abstract void A03(View view);

    public AbstractC69213Yj(Context context, LayoutInflater layoutInflater, C14850m9 r5, int i) {
        this.A0A = r5;
        this.A08 = context;
        this.A09 = layoutInflater;
        this.A07 = i;
        WindowManager A02 = AnonymousClass01d.A02(context);
        Point point = new Point();
        A02.getDefaultDisplay().getSize(point);
        int i2 = point.x / i;
        if (this.A00 != i2) {
            this.A00 = i2;
            GridLayoutManager gridLayoutManager = this.A03;
            if (gridLayoutManager != null) {
                gridLayoutManager.A1h(i2);
            }
            C54392ge r0 = this.A05;
            if (r0 != null) {
                r0.A02();
            }
        }
    }

    public C54392ge A00() {
        C54392ge r2 = this.A05;
        if (r2 == null) {
            if (this instanceof C622335s) {
                C622335s r0 = (C622335s) this;
                r2 = new C54392ge(r0.A08, r0.A05, r0.A07, C12990iw.A0j(), r0.A04.A04);
                r2.A02 = new AbstractC116235Uq() { // from class: X.3aG
                    @Override // X.AbstractC116235Uq
                    public final void AWf(AnonymousClass1KS r4) {
                        C622335s r22 = C622335s.this;
                        ActivityC13810kN.A0x(C12970iu.A0D(), r4, new StarStickerFromPickerDialogFragment(), r22);
                    }
                };
            } else if (this instanceof AnonymousClass35q) {
                AnonymousClass35q r02 = (AnonymousClass35q) this;
                r2 = new C54392ge(((AbstractC69213Yj) r02).A08, r02.A05, r02.A07, C12980iv.A0j(), null);
                r2.A02 = new AbstractC116235Uq() { // from class: X.3aF
                    @Override // X.AbstractC116235Uq
                    public final void AWf(AnonymousClass1KS r4) {
                        AnonymousClass35q r22 = AnonymousClass35q.this;
                        ActivityC13810kN.A0x(C12970iu.A0D(), r4, new RemoveStickerFromFavoritesDialogFragment(), r22);
                    }
                };
            } else if (this instanceof C622235r) {
                C622235r r03 = (C622235r) this;
                r2 = r03.A05;
                if (r2 == null) {
                    r2 = new C54392ge(((AbstractC69213Yj) r03).A08, r03.A0A, r03.A0B, C12970iu.A0h(), null);
                    r03.A05 = r2;
                    r2.A02 = new AbstractC116235Uq() { // from class: X.3aE
                        @Override // X.AbstractC116235Uq
                        public final void AWf(AnonymousClass1KS r4) {
                            C622235r r22 = C622235r.this;
                            ((ActivityC13810kN) AnonymousClass12P.A00(((AbstractC69213Yj) r22).A08)).Adm(StarOrRemoveFromRecentsStickerDialogFragment.A00(r4, r22.A0C));
                        }
                    };
                }
            } else if (!(this instanceof AnonymousClass35p)) {
                AnonymousClass35o r04 = (AnonymousClass35o) this;
                r2 = new C54392ge(r04.A08, r04.A00, r04.A01, 7, C12980iv.A0z(r04.A02.A03));
                r2.A02 = new AbstractC116235Uq() { // from class: X.3aC
                    @Override // X.AbstractC116235Uq
                    public final void AWf(AnonymousClass1KS r5) {
                        AnonymousClass35o r3 = AnonymousClass35o.this;
                        StarStickerFromPickerDialogFragment starStickerFromPickerDialogFragment = new StarStickerFromPickerDialogFragment();
                        Bundle A0D = C12970iu.A0D();
                        A0D.putParcelable("sticker", r5);
                        starStickerFromPickerDialogFragment.A0U(A0D);
                        ((ActivityC13810kN) AnonymousClass12P.A01(r3.A08, ActivityC13810kN.class)).Adm(starStickerFromPickerDialogFragment);
                    }
                };
            } else {
                AnonymousClass35p r05 = (AnonymousClass35p) this;
                r2 = new C54392ge(r05.A08, r05.A03, r05.A04, C12980iv.A0k(), r05.A01);
                r2.A02 = new AbstractC116235Uq() { // from class: X.3aD
                    @Override // X.AbstractC116235Uq
                    public final void AWf(AnonymousClass1KS r4) {
                        AnonymousClass35p r22 = AnonymousClass35p.this;
                        ActivityC13810kN.A0x(C12970iu.A0D(), r4, new StarStickerFromPickerDialogFragment(), r22);
                    }
                };
            }
            this.A05 = r2;
            boolean z = this.A06;
            r2.A04 = z;
            r2.A00 = C12980iv.A03(z ? 1 : 0);
        }
        return r2;
    }

    public void A01() {
        if (this instanceof C622335s) {
            C622335s r1 = (C622335s) this;
            r1.A00().A02();
            r1.A04();
        } else if (this instanceof AnonymousClass35q) {
            AnonymousClass35q r2 = (AnonymousClass35q) this;
            C235512c r3 = r2.A06;
            int i = r2.A04;
            r3.A0Y.Aaz(new C864447i(new AbstractC49382Kn() { // from class: X.3aB
                @Override // X.AbstractC49382Kn
                public final void AWd(List list) {
                    AnonymousClass35q r32 = AnonymousClass35q.this;
                    r32.A03 = list;
                    C54392ge A00 = r32.A00();
                    A00.A0E(r32.A03);
                    A00.A02();
                    if (r32.A00 != null) {
                        if (r32.A08) {
                            r32.A02.setText(R.string.avatar_picker_no_favorited_stickers);
                        }
                        int A0D = r32.A00().A0D();
                        View view = r32.A00;
                        int i2 = 8;
                        if (A0D == 0) {
                            i2 = 0;
                        }
                        view.setVisibility(i2);
                    }
                }
            }, r3, i), new Void[0]);
        } else if (this instanceof C622235r) {
            C622235r r0 = (C622235r) this;
            C22210yi r4 = r0.A09;
            r4.A09.execute(new RunnableBRunnable0Shape8S0200000_I0_8(r4, 0, new AbstractC49382Kn() { // from class: X.3aA
                @Override // X.AbstractC49382Kn
                public final void AWd(List list) {
                    C622235r r42 = C622235r.this;
                    C54392ge A00 = r42.A00();
                    r42.A06 = list;
                    A00.A0E(list);
                    A00.A02();
                    if (r42.A01 != null) {
                        int A0D = r42.A00().A0D();
                        View view = r42.A01;
                        int i2 = 8;
                        if (A0D == 0) {
                            i2 = 0;
                        }
                        view.setVisibility(i2);
                        boolean z = r42.A07;
                        TextView textView = r42.A03;
                        if (z) {
                            textView.setText(R.string.sticker_picker_no_sent_stickers);
                            r42.A02.setVisibility(4);
                        } else {
                            textView.setText(R.string.sticker_picker_no_recent_no_installed);
                            r42.A02.setVisibility(0);
                        }
                        r42.A04.setVisibility(0);
                        if (r42.A0C) {
                            r42.A03.setText(R.string.avatar_picker_no_sent_stickers);
                            r42.A02.setVisibility(4);
                        }
                    }
                }
            }));
        } else if (!(this instanceof AnonymousClass35p)) {
            AnonymousClass35o r22 = (AnonymousClass35o) this;
            r22.A00().A0E(C12980iv.A0z(r22.A02.A03));
            r22.A00().A02();
        } else {
            AnonymousClass35p r32 = (AnonymousClass35p) this;
            r32.A00().A02();
            if (r32.A00 != null) {
                int i2 = 0;
                int A09 = C12970iu.A09(r32.A01);
                View view = r32.A00;
                if (A09 != 0) {
                    i2 = 8;
                }
                view.setVisibility(i2);
            }
        }
    }

    public void A02(int i, int i2) {
        if (i != 0 && i2 != 0) {
            int dimensionPixelSize = i2 - this.A08.getResources().getDimensionPixelSize(R.dimen.picker_footer_height);
            if (dimensionPixelSize != this.A01) {
                this.A01 = dimensionPixelSize;
                int i3 = this.A07;
                int i4 = (i3 * 3) >> 2;
                int i5 = dimensionPixelSize % i3;
                if (i5 >= (i3 >> 2) && i5 <= i4) {
                    i4 = i5;
                }
                int max = Math.max(0, dimensionPixelSize - i4);
                this.A02 = (max % i3) / ((max / i3) + 1);
            }
            int i6 = i / this.A07;
            if (this.A00 != i6) {
                this.A00 = i6;
                GridLayoutManager gridLayoutManager = this.A03;
                if (gridLayoutManager != null) {
                    gridLayoutManager.A1h(i6);
                }
                C54392ge r0 = this.A05;
                if (r0 != null) {
                    r0.A02();
                }
            }
        }
    }

    @Override // X.AnonymousClass5WC
    public void AP2(View view, ViewGroup viewGroup, int i) {
        RecyclerView recyclerView = this.A04;
        if (recyclerView != null) {
            AnonymousClass0OK recycledViewPool = recyclerView.getRecycledViewPool();
            int i2 = 0;
            while (true) {
                SparseArray sparseArray = recycledViewPool.A01;
                if (i2 >= sparseArray.size()) {
                    break;
                }
                ((C04810Nd) sparseArray.valueAt(i2)).A03.clear();
                i2++;
            }
            recyclerView.setAdapter(null);
            this.A04 = null;
        }
        this.A03 = null;
        this.A05 = null;
    }

    @Override // X.AnonymousClass5WC
    public String getId() {
        if (this instanceof C622335s) {
            return ((C622335s) this).A04.A0D;
        }
        if (this instanceof AnonymousClass35q) {
            return "starred";
        }
        if (this instanceof C622235r) {
            return "recents";
        }
        if (!(this instanceof AnonymousClass35p)) {
            return "contextual_suggestion";
        }
        return C12960it.A0f(C12960it.A0k("reaction_"), ((AnonymousClass35p) this).A02);
    }
}
