package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Locale;

/* renamed from: X.0q8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17020q8 {
    public final C17050qB A00;
    public final C14830m7 A01;
    public final C16590pI A02;
    public final C15890o4 A03;
    public final C21320xE A04;
    public final C254519l A05;
    public final C254419k A06;

    public C17020q8(C17050qB r2, C14830m7 r3, C16590pI r4, C15890o4 r5, C21320xE r6, C254519l r7, C254419k r8) {
        this.A02 = r4;
        this.A01 = r3;
        this.A06 = r8;
        this.A00 = r2;
        this.A03 = r5;
        this.A04 = r6;
        this.A05 = r7;
        r6.A03(new C468127t(this));
    }

    public final File A00() {
        String str;
        Context context = this.A02.A00;
        if (context.getExternalCacheDir() == null) {
            str = "draftvoicenotecache/getcachedvoicenotesdirectory/external cache directory could not be accessed";
        } else {
            File file = new File(context.getExternalCacheDir().getAbsolutePath(), "Cached Voice Notes");
            if (file.exists() || file.mkdir()) {
                return file;
            }
            str = "draftvoicenotecache/getcachedvoicenotefile/problem creating directory ";
        }
        Log.e(str);
        return null;
    }

    public final File A01(AbstractC14640lm r6) {
        File A00 = A00();
        if (A00 == null) {
            return null;
        }
        return new File(A00, String.format(Locale.US, "%s.%s", r6.getRawString(), "opus"));
    }

    public final File A02(AbstractC14640lm r6) {
        File A00 = A00();
        if (A00 == null) {
            return null;
        }
        return new File(A00, String.format(Locale.US, "%s.%s", r6.getRawString(), "viz"));
    }

    public final File A03(AbstractC14640lm r6) {
        File A00 = A00();
        if (A00 != null) {
            return new File(A00, String.format(Locale.US, "%s.txt", r6.getRawString()));
        }
        Log.i("draftvoicenotecache/getquotedmessagefile/cached voice note directory is null");
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (X.C14350lI.A0M(r0) == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04(X.AbstractC14640lm r6) {
        /*
            r5 = this;
            java.lang.String r0 = "Chat jid cannot be null"
            X.AnonymousClass009.A06(r6, r0)
            java.io.File r0 = r5.A01(r6)
            java.io.File r1 = r5.A02(r6)
            if (r0 == 0) goto L_0x0016
            boolean r0 = X.C14350lI.A0M(r0)
            r2 = 1
            if (r0 != 0) goto L_0x0017
        L_0x0016:
            r2 = 0
        L_0x0017:
            if (r1 == 0) goto L_0x001c
            X.C14350lI.A0M(r1)
        L_0x001c:
            java.io.File r1 = r5.A03(r6)
            if (r1 == 0) goto L_0x002b
            boolean r0 = r1.exists()
            if (r0 == 0) goto L_0x002b
            r1.delete()
        L_0x002b:
            if (r2 == 0) goto L_0x005a
            X.19l r0 = r5.A05
            java.lang.Iterable r0 = r0.A01()
            java.util.Iterator r4 = r0.iterator()
        L_0x0037:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x005a
            java.lang.Object r3 = r4.next()
            X.0lp r3 = (X.C14660lp) r3
            com.whatsapp.Conversation r1 = r3.A00
            X.0lm r0 = r1.A2s
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0037
            X.0mE r2 = r1.A05
            r1 = 39
            com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0 r0 = new com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0
            r0.<init>(r3, r1)
            r2.A0H(r0)
            goto L_0x0037
        L_0x005a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17020q8.A04(X.0lm):void");
    }
}
