package X;

import com.whatsapp.wabloks.base.BkFcsPreloadingScreenFragment;
import com.whatsapp.wabloks.base.FdsContentFragmentManager;
import java.util.List;

/* renamed from: X.0rX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17870rX {
    public final C14900mE A00;
    public final C19750uc A01;

    public C17870rX(C14900mE r2, C19750uc r3) {
        C16700pc.A0E(r2, 1);
        C16700pc.A0E(r3, 2);
        this.A00 = r2;
        this.A01 = r3;
    }

    public final BkFcsPreloadingScreenFragment A00(AnonymousClass01F r4) {
        BkFcsPreloadingScreenFragment A00;
        List<AnonymousClass01E> A02 = r4.A0U.A02();
        C16700pc.A0B(A02);
        for (AnonymousClass01E r1 : A02) {
            if (r1 instanceof BkFcsPreloadingScreenFragment) {
                return (BkFcsPreloadingScreenFragment) r1;
            }
            if ((r1 instanceof FdsContentFragmentManager) && (A00 = A00(r1.A0E())) != null) {
                return A00;
            }
        }
        return null;
    }
}
