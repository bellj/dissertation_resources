package X;

import android.net.Uri;
import android.util.Base64;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: X.21z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C455221z extends AbstractC37791n0 {
    public String A00;
    public String A01;
    public final AnonymousClass14D A02;
    public final String A03;
    public final boolean A04;
    public final boolean A05;

    public C455221z(AnonymousClass14D r2, String str, String str2, String str3, String str4, boolean z, boolean z2) {
        super(str, str2, str3);
        if (!z) {
            this.A00 = super.A03;
        }
        this.A02 = r2;
        this.A04 = z;
        this.A05 = z2;
        this.A03 = str4;
    }

    public final Uri.Builder A02(C28481Nj r8) {
        byte[] bArr;
        String str = this.A01;
        if (str == null) {
            AnonymousClass14D r1 = this.A02;
            AnonymousClass009.A05(r1);
            byte[] decode = Base64.decode(super.A02, 2);
            byte[] bArr2 = r1.A00;
            if (bArr2 == null) {
                bArr = AnonymousClass1TT.A02(decode);
            } else if (decode == null) {
                bArr = (byte[]) bArr2.clone();
            } else {
                int length = bArr2.length;
                int length2 = decode.length;
                bArr = new byte[length + length2];
                System.arraycopy(bArr2, 0, bArr, 0, length);
                System.arraycopy(decode, 0, bArr, length, length2);
            }
            try {
                str = AnonymousClass1US.A0A(Base64.encodeToString(MessageDigest.getInstance("SHA-256").digest(bArr), 2));
                this.A01 = str;
            } catch (NoSuchAlgorithmException e) {
                throw new AssertionError(e);
            }
        }
        boolean z = false;
        if (str != null) {
            z = true;
        }
        AnonymousClass009.A0A("Upload token has not been set", z);
        Uri.Builder A01 = A01(r8);
        A01.appendQueryParameter("token", this.A01);
        if (this.A05) {
            A01.appendQueryParameter("_nc_rmr", "1");
        }
        return A01;
    }

    @Override // X.AbstractC37701mr
    public String AAJ(C15450nH r4, C28481Nj r5, boolean z) {
        Uri.Builder A02 = A02(r5);
        if (this.A04) {
            A02.appendQueryParameter("stream", "1");
        }
        String str = this.A03;
        if (str != null) {
            A02.appendQueryParameter("type", str);
        }
        return A02.build().toString();
    }
}
