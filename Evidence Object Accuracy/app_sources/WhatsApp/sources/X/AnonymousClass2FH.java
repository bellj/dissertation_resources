package X;

import android.app.Activity;
import android.app.Application;
import com.whatsapp.calling.di.ActivityModule;
import com.whatsapp.gallery.MediaPickerFragmentModule;
import com.whatsapp.gallery.di.GalleryModule;
import com.whatsapp.stickers.di.StickersModule;

/* renamed from: X.2FH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2FH implements AnonymousClass005 {
    public final Activity A00;
    public final AnonymousClass005 A01;
    public final Object A02 = new Object();
    public volatile Object A03;

    public AnonymousClass2FH(Activity activity) {
        this.A00 = activity;
        this.A01 = new C48702Hh((ActivityC001000l) activity);
    }

    @Override // X.AnonymousClass005
    public Object generatedComponent() {
        if (this.A03 == null) {
            synchronized (this.A02) {
                if (this.A03 == null) {
                    Activity activity = this.A00;
                    if (activity.getApplication() instanceof AnonymousClass005) {
                        C48722Hj r0 = (C48722Hj) ((AbstractC48712Hi) AnonymousClass027.A00(AbstractC48712Hi.class, this.A01));
                        C48732Hk r02 = new C48732Hk(r0.A03, r0.A04);
                        r02.A00 = activity;
                        AnonymousClass01J r7 = r02.A02;
                        this.A03 = new AnonymousClass2FL(activity, r02.A01, r7, new ActivityModule(), new MediaPickerFragmentModule(), new GalleryModule(), new StickersModule());
                    } else if (Application.class.equals(activity.getApplication().getClass())) {
                        throw new IllegalStateException("Hilt Activity must be attached to an @HiltAndroidApp Application. Did you forget to specify your Application's class name in your manifest's <application />'s android:name attribute?");
                    } else {
                        StringBuilder sb = new StringBuilder("Hilt Activity must be attached to an @AndroidEntryPoint Application. Found: ");
                        sb.append(activity.getApplication().getClass());
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
        }
        return this.A03;
    }
}
