package X;

import android.graphics.drawable.Drawable;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;

/* renamed from: X.1qE  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1qE {
    public final int A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass1KS A03;
    public final String A04;
    public final boolean A05;
    public final boolean A06;

    public AnonymousClass1qE(AnonymousClass1KS r1, String str, int i, int i2, int i3, boolean z, boolean z2) {
        this.A03 = r1;
        this.A04 = str;
        this.A02 = i;
        this.A00 = i2;
        this.A05 = z;
        this.A01 = i3;
        this.A06 = z2;
    }

    public void A00(Drawable drawable, C14900mE r6) {
        if (!(this instanceof C39651qH)) {
            AnonymousClass1qI r3 = (AnonymousClass1qI) this;
            if (drawable != null) {
                drawable.setBounds(0, 0, r3.A02, ((AnonymousClass1qE) r3).A00);
                r3.A01.AWc(drawable);
                return;
            }
            return;
        }
        AnonymousClass009.A05(r6);
        r6.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(this, 2, drawable));
    }
}
