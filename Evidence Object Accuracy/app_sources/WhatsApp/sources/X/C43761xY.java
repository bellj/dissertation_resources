package X;

import android.app.Activity;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;

/* renamed from: X.1xY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43761xY {
    public boolean A00;
    public final Activity A01;
    public final AbstractC15710nm A02;
    public final C14900mE A03;
    public final AnonymousClass1P3 A04;
    public final C238013b A05;
    public final AnonymousClass1P2 A06;
    public final C43661xO A07;
    public final AnonymousClass10S A08;
    public final C14820m6 A09;
    public final C17220qS A0A;
    public final AnonymousClass1P4 A0B;
    public final C14860mA A0C;
    public final Runnable A0D = new RunnableBRunnable0Shape3S0100000_I0_3(this, 16);
    public final Runnable A0E = new RunnableBRunnable0Shape3S0100000_I0_3(this, 17);
    public final boolean A0F;

    public C43761xY(Activity activity, AbstractC15710nm r4, C14900mE r5, AnonymousClass1P3 r6, C238013b r7, AnonymousClass1P2 r8, C43661xO r9, AnonymousClass10S r10, C14820m6 r11, C17220qS r12, AnonymousClass1P4 r13, C14860mA r14, boolean z) {
        this.A01 = activity;
        this.A03 = r5;
        this.A02 = r4;
        this.A0C = r14;
        this.A0A = r12;
        this.A05 = r7;
        this.A08 = r10;
        this.A09 = r11;
        this.A04 = r6;
        this.A0B = r13;
        this.A0F = z;
        this.A06 = r8;
        this.A07 = r9;
    }
}
