package X;

import java.nio.ByteBuffer;

/* renamed from: X.4vo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC106504vo implements AnonymousClass5Xx {
    public C94084bE A00;
    public C94084bE A01;
    public C94084bE A02;
    public C94084bE A03;
    public ByteBuffer A04;
    public ByteBuffer A05;
    public boolean A06;

    public AbstractC106504vo() {
        ByteBuffer byteBuffer = AnonymousClass5Xx.A00;
        this.A04 = byteBuffer;
        this.A05 = byteBuffer;
        C94084bE r0 = C94084bE.A04;
        this.A02 = r0;
        this.A03 = r0;
        this.A00 = r0;
        this.A01 = r0;
    }

    public final ByteBuffer A00(int i) {
        if (this.A04.capacity() < i) {
            this.A04 = C72453ed.A0x(i);
        } else {
            this.A04.clear();
        }
        ByteBuffer byteBuffer = this.A04;
        this.A05 = byteBuffer;
        return byteBuffer;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0071, code lost:
        if (r1 != 4) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0097, code lost:
        if (r0 == 0) goto L_0x00b4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:69:? A[RETURN, SYNTHETIC] */
    @Override // X.AnonymousClass5Xx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C94084bE A7V(X.C94084bE r8) {
        /*
            r7 = this;
            r7.A02 = r8
            r2 = r7
            boolean r0 = r7 instanceof X.C76663m0
            if (r0 != 0) goto L_0x007d
            boolean r0 = r7 instanceof X.C76673m1
            if (r0 != 0) goto L_0x008e
            boolean r0 = r7 instanceof X.C76643ly
            if (r0 != 0) goto L_0x0053
            boolean r0 = r7 instanceof X.C76683m2
            if (r0 != 0) goto L_0x0040
            X.3lz r2 = (X.C76653lz) r2
            int[] r6 = r2.A01
            if (r6 == 0) goto L_0x00b4
            int r0 = r8.A02
            r5 = 2
            if (r0 != r5) goto L_0x003a
            int r4 = r8.A01
            int r3 = r6.length
            boolean r2 = X.C12980iv.A1V(r4, r3)
            r1 = 0
        L_0x0026:
            if (r1 >= r3) goto L_0x00a0
            r0 = r6[r1]
            if (r0 >= r4) goto L_0x0034
            boolean r0 = X.C12980iv.A1V(r0, r1)
            r2 = r2 | r0
            int r1 = r1 + 1
            goto L_0x0026
        L_0x0034:
            X.4Bk r0 = new X.4Bk
            r0.<init>(r8)
            throw r0
        L_0x003a:
            X.4Bk r0 = new X.4Bk
            r0.<init>(r8)
            throw r0
        L_0x0040:
            int r1 = r8.A02
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            if (r1 == r0) goto L_0x0070
            r0 = 805306368(0x30000000, float:4.656613E-10)
            if (r1 == r0) goto L_0x0070
            r0 = 4
            if (r1 == r0) goto L_0x00b4
            X.4Bk r0 = new X.4Bk
            r0.<init>(r8)
            throw r0
        L_0x0053:
            int r1 = r8.A02
            r2 = 2
            r0 = 3
            if (r1 == r0) goto L_0x0073
            if (r1 == r2) goto L_0x00b4
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            if (r1 == r0) goto L_0x0073
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            if (r1 == r0) goto L_0x0073
            r0 = 805306368(0x30000000, float:4.656613E-10)
            if (r1 == r0) goto L_0x0073
            r0 = 4
            if (r1 == r0) goto L_0x0073
            X.4Bk r0 = new X.4Bk
            r0.<init>(r8)
            throw r0
        L_0x0070:
            r2 = 4
            if (r1 == r2) goto L_0x00b4
        L_0x0073:
            int r1 = r8.A03
            int r0 = r8.A01
            X.4bE r8 = new X.4bE
            r8.<init>(r1, r0, r2)
            goto L_0x00a9
        L_0x007d:
            X.3m0 r2 = (X.C76663m0) r2
            int r1 = r8.A02
            r0 = 2
            if (r1 != r0) goto L_0x00b7
            r0 = 1
            r2.A05 = r0
            int r0 = r2.A03
            if (r0 != 0) goto L_0x00a9
            int r0 = r2.A02
            goto L_0x0097
        L_0x008e:
            X.3m1 r2 = (X.C76673m1) r2
            int r1 = r8.A02
            r0 = 2
            if (r1 != r0) goto L_0x009a
            boolean r0 = r2.A05
        L_0x0097:
            if (r0 == 0) goto L_0x00b4
            goto L_0x00a9
        L_0x009a:
            X.4Bk r0 = new X.4Bk
            r0.<init>(r8)
            throw r0
        L_0x00a0:
            if (r2 == 0) goto L_0x00b4
            int r0 = r8.A03
            X.4bE r8 = new X.4bE
            r8.<init>(r0, r3, r5)
        L_0x00a9:
            r7.A03 = r8
            boolean r0 = r7.AJD()
            if (r0 != 0) goto L_0x00b3
            X.4bE r8 = X.C94084bE.A04
        L_0x00b3:
            return r8
        L_0x00b4:
            X.4bE r8 = X.C94084bE.A04
            goto L_0x00a9
        L_0x00b7:
            X.4Bk r0 = new X.4Bk
            r0.<init>(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC106504vo.A7V(X.4bE):X.4bE");
    }

    @Override // X.AnonymousClass5Xx
    public ByteBuffer AEn() {
        ByteBuffer byteBuffer = this.A05;
        this.A05 = AnonymousClass5Xx.A00;
        return byteBuffer;
    }

    @Override // X.AnonymousClass5Xx
    public boolean AJD() {
        if (!(this instanceof C76673m1)) {
            return C12960it.A1X(this.A03, C94084bE.A04);
        }
        return ((C76673m1) this).A05;
    }

    @Override // X.AnonymousClass5Xx
    public boolean AJN() {
        return this.A06 && this.A05 == AnonymousClass5Xx.A00;
    }

    @Override // X.AnonymousClass5Xx
    public final void AZj() {
        this.A06 = true;
        if (this instanceof C76663m0) {
            C76663m0 r4 = (C76663m0) this;
            if (r4.A05) {
                int i = r4.A00;
                if (i > 0) {
                    r4.A04 += (long) (i / ((AbstractC106504vo) r4).A00.A00);
                }
                r4.A00 = 0;
            }
        } else if (this instanceof C76673m1) {
            C76673m1 r42 = (C76673m1) this;
            int i2 = r42.A01;
            if (i2 > 0) {
                r42.A02(r42.A07, i2);
            }
            if (!r42.A06) {
                r42.A04 += (long) (r42.A02 / r42.A00);
            }
        }
    }

    @Override // X.AnonymousClass5Xx
    public final void flush() {
        this.A05 = AnonymousClass5Xx.A00;
        this.A06 = false;
        this.A00 = this.A02;
        this.A01 = this.A03;
        if (this instanceof C76663m0) {
            C76663m0 r4 = (C76663m0) this;
            if (r4.A05) {
                r4.A05 = false;
                int i = r4.A02;
                int i2 = ((AbstractC106504vo) r4).A00.A00;
                r4.A06 = new byte[i * i2];
                r4.A01 = r4.A03 * i2;
            }
            r4.A00 = 0;
        } else if (this instanceof C76673m1) {
            C76673m1 r42 = (C76673m1) this;
            if (r42.A05) {
                C94084bE r2 = ((AbstractC106504vo) r42).A00;
                int i3 = r2.A00;
                r42.A00 = i3;
                long j = r42.A09;
                long j2 = (long) r2.A03;
                int A0W = ((int) C72453ed.A0W(j, j2)) * i3;
                if (r42.A07.length != A0W) {
                    r42.A07 = new byte[A0W];
                }
                int A0W2 = ((int) C72453ed.A0W(r42.A0A, j2)) * i3;
                r42.A02 = A0W2;
                if (r42.A08.length != A0W2) {
                    r42.A08 = new byte[A0W2];
                }
            }
            r42.A03 = 0;
            r42.A04 = 0;
            r42.A01 = 0;
            r42.A06 = false;
        } else if (this instanceof C76653lz) {
            C76653lz r43 = (C76653lz) this;
            r43.A00 = r43.A01;
        }
    }

    @Override // X.AnonymousClass5Xx
    public final void reset() {
        flush();
        this.A04 = AnonymousClass5Xx.A00;
        C94084bE r0 = C94084bE.A04;
        this.A02 = r0;
        this.A03 = r0;
        this.A00 = r0;
        this.A01 = r0;
        if (this instanceof C76663m0) {
            ((C76663m0) this).A06 = AnonymousClass3JZ.A0A;
        } else if (this instanceof C76673m1) {
            C76673m1 r1 = (C76673m1) this;
            r1.A05 = false;
            r1.A02 = 0;
            byte[] bArr = AnonymousClass3JZ.A0A;
            r1.A07 = bArr;
            r1.A08 = bArr;
        } else if (this instanceof C76653lz) {
            C76653lz r12 = (C76653lz) this;
            r12.A00 = null;
            r12.A01 = null;
        }
    }
}
