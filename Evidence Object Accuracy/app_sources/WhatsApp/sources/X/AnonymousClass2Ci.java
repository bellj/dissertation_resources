package X;

import java.util.HashSet;

/* renamed from: X.2Ci  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Ci {
    public final C242914y A00;
    public final C22830zi A01;
    public final AnonymousClass1IS A02;
    public final HashSet A03;
    public final boolean A04;

    public AnonymousClass2Ci(C242914y r1, C22830zi r2, AnonymousClass1IS r3, HashSet hashSet, boolean z) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = hashSet;
        this.A02 = r3;
        this.A04 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005a, code lost:
        if (r2 != null) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Collection A00() {
        /*
            r4 = this;
            java.util.HashSet r2 = r4.A03
            if (r2 == 0) goto L_0x0052
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0052
            java.lang.Class<com.whatsapp.jid.DeviceJid> r0 = com.whatsapp.jid.DeviceJid.class
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            X.C15380n4.A0C(r0, r2, r3)
        L_0x0014:
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0042
            boolean r0 = r4.A04
            X.14y r2 = r4.A00
            X.1IS r1 = r4.A02
            if (r0 == 0) goto L_0x0045
            X.0lm r1 = r1.A00
            boolean r0 = r1 instanceof X.AbstractC15590nW
            if (r0 == 0) goto L_0x0043
            X.0nW r1 = (X.AbstractC15590nW) r1
        L_0x002a:
            X.AnonymousClass009.A05(r1)
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0042
            X.0nX r0 = r2.A03
            X.1YM r1 = r0.A02(r1)
            X.0nT r0 = r2.A01
            java.util.Set r0 = r1.A0C(r0)
        L_0x003f:
            r3.retainAll(r0)
        L_0x0042:
            return r3
        L_0x0043:
            r1 = 0
            goto L_0x002a
        L_0x0045:
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0042
            X.0zi r0 = r2.A04
            java.util.Set r0 = r0.A00(r1)
            goto L_0x003f
        L_0x0052:
            X.0zi r1 = r4.A01
            X.1IS r0 = r4.A02
            java.util.Set r3 = r1.A00(r0)
            if (r2 == 0) goto L_0x0042
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Ci.A00():java.util.Collection");
    }
}
