package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1Rs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29321Rs {
    public final NativeHolder A00;

    public C29321Rs(NativeHolder nativeHolder) {
        this.A00 = nativeHolder;
    }

    public C29321Rs(byte[] bArr, long j) {
        JniBridge.getInstance();
        this.A00 = new C29321Rs((NativeHolder) JniBridge.jvidispatchOIO(18, j, bArr)).A00;
    }
}
