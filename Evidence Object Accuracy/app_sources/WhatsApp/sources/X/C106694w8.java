package X;

/* renamed from: X.4w8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106694w8 implements AbstractC116785Ww {
    public boolean A00;
    public final C107134wq A01 = new C107134wq(null);
    public final C95304dT A02 = C95304dT.A05(2786);

    @Override // X.AbstractC116785Ww
    public void AIa(AbstractC14070ko r6) {
        this.A01.A8b(r6, new C92824Xo(Integer.MIN_VALUE, 0, 1));
        r6.A9V();
        C106904wT.A00(r6, -9223372036854775807L);
    }

    @Override // X.AbstractC116785Ww
    public int AZn(AnonymousClass5Yf r6, AnonymousClass4IG r7) {
        C95304dT r4 = this.A02;
        int read = r6.read(r4.A02, 0, 2786);
        if (read == -1) {
            return -1;
        }
        r4.A0S(0);
        r4.A0R(read);
        if (!this.A00) {
            this.A01.A04 = 0;
            this.A00 = true;
        }
        this.A01.A7a(r4);
        return 0;
    }

    @Override // X.AbstractC116785Ww
    public void AbQ(long j, long j2) {
        this.A00 = false;
        this.A01.AbP();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0039, code lost:
        if ((r7 - r3) >= 8192) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0030, code lost:
        r12.Aaj();
        r7 = r7 + 1;
     */
    @Override // X.AbstractC116785Ww
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean Ae5(X.AnonymousClass5Yf r12) {
        /*
            r11 = this;
            r2 = 10
            X.4dT r5 = X.C95304dT.A05(r2)
            r4 = 0
            r3 = 0
        L_0x0008:
            X.C95304dT.A06(r12, r5, r2)
            r5.A0S(r4)
            int r1 = r5.A0D()
            r0 = 4801587(0x494433, float:6.728456E-39)
            if (r1 == r0) goto L_0x007c
            r12.Aaj()
            r12.A5r(r3)
            r7 = r3
        L_0x001e:
            r6 = 0
        L_0x001f:
            byte[] r1 = r5.A02
            r0 = 6
            r12.AZ4(r1, r4, r0)
            r5.A0S(r4)
            int r1 = r5.A0F()
            r0 = 2935(0xb77, float:4.113E-42)
            if (r1 == r0) goto L_0x003f
            r12.Aaj()
            int r7 = r7 + 1
            int r1 = r7 - r3
            r0 = 8192(0x2000, float:1.14794E-41)
            if (r1 >= r0) goto L_0x008c
            r12.A5r(r7)
            goto L_0x001e
        L_0x003f:
            r0 = 1
            int r6 = r6 + r0
            r10 = 4
            if (r6 < r10) goto L_0x0045
            return r0
        L_0x0045:
            byte[] r9 = r5.A02
            int r1 = r9.length
            r0 = 6
            if (r1 < r0) goto L_0x008c
            r0 = 5
            byte r0 = r9[r0]
            r1 = r0 & 248(0xf8, float:3.48E-43)
            r8 = 3
            int r1 = r1 >> r8
            r0 = 10
            r2 = 1
            if (r1 <= r0) goto L_0x006f
            r0 = 2
            byte r0 = r9[r0]
            r0 = r0 & 7
            int r1 = r0 << 8
            byte r0 = r9[r8]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r0 = r0 | r1
            int r0 = r0 + r2
            int r1 = r0 << 1
        L_0x0066:
            r0 = -1
            if (r1 == r0) goto L_0x008c
            int r0 = r1 + -6
            r12.A5r(r0)
            goto L_0x001f
        L_0x006f:
            byte r2 = r9[r10]
            r0 = r2 & 192(0xc0, float:2.69E-43)
            int r1 = r0 >> 6
            r0 = r2 & 63
            int r1 = X.C93124Ze.A00(r1, r0)
            goto L_0x0066
        L_0x007c:
            r0 = 3
            r5.A0T(r0)
            int r1 = r5.A0B()
            int r0 = r1 + 10
            int r3 = r3 + r0
            r12.A5r(r1)
            goto L_0x0008
        L_0x008c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106694w8.Ae5(X.5Yf):boolean");
    }
}
