package X;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/* renamed from: X.00N  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00N<K, V> extends AnonymousClass00O<K, V> implements Map<K, V> {
    public AbstractC008904n A00;

    public AnonymousClass00N() {
    }

    public AnonymousClass00N(int i) {
        super(i);
    }

    public AnonymousClass00N(AnonymousClass00O r1) {
        if (r1 != null) {
            A08(r1);
        }
    }

    @Override // java.util.Map
    public Set entrySet() {
        AbstractC008904n r1 = this.A00;
        if (r1 == null) {
            r1 = new C02470Ck(this);
            this.A00 = r1;
        }
        C10410ea r0 = r1.A00;
        if (r0 != null) {
            return r0;
        }
        C10410ea r02 = new C10410ea(r1);
        r1.A00 = r02;
        return r02;
    }

    @Override // java.util.Map
    public Set keySet() {
        AbstractC008904n r1 = this.A00;
        if (r1 == null) {
            r1 = new C02470Ck(this);
            this.A00 = r1;
        }
        C009004o r0 = r1.A01;
        if (r0 != null) {
            return r0;
        }
        C009004o r02 = new C009004o(r1);
        r1.A01 = r02;
        return r02;
    }

    @Override // java.util.Map
    public void putAll(Map map) {
        A07(super.A00 + map.size());
        for (Map.Entry<K, V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override // java.util.Map
    public Collection values() {
        AbstractC008904n r1 = this.A00;
        if (r1 == null) {
            r1 = new C02470Ck(this);
            this.A00 = r1;
        }
        C10280eM r0 = r1.A02;
        if (r0 != null) {
            return r0;
        }
        C10280eM r02 = new C10280eM(r1);
        r1.A02 = r02;
        return r02;
    }
}
