package X;

import java.io.IOException;

/* renamed from: X.1df  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33231df {
    public static AbstractC33251dh A00(C15570nT r3, AbstractC33221de r4, C33241dg r5, C15820nx r6, C17050qB r7, C19490uC r8, C21780xy r9, C16600pJ r10, EnumC16570pG r11, AnonymousClass15D r12) {
        int i = r11.version;
        if (i == EnumC16570pG.A08.version) {
            return new AnonymousClass2AU(r4, r7, r9, r12);
        }
        if (i == EnumC16570pG.A04.version) {
            return new C58752rK(r3, r4, r5, r6, r7, r8, r9, r10, r12);
        }
        if (i == EnumC16570pG.A05.version) {
            return new C58732rI(r3, r4, r5, r6, r7, r8, r9, r10, r12);
        }
        if (i == EnumC16570pG.A06.version) {
            return new C58762rL(r3, r4, r5, r6, r7, r8, r9, r10, r12);
        }
        if (i == EnumC16570pG.A07.version) {
            return new C58742rJ(r3, r4, r5, r6, r7, r8, r9, r10, r12);
        }
        StringBuilder sb = new StringBuilder("BackupFile/verify-integrity/unknown-version: ");
        sb.append(r11);
        sb.append(" ");
        sb.append(r4);
        r10.A00(sb.toString(), 4);
        throw new IOException("BackupFile/verify-integrity/unknown-version");
    }
}
