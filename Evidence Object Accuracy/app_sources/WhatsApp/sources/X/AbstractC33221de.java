package X;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;

/* renamed from: X.1de  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC33221de {
    boolean A8j();

    boolean A9l();

    C32871cs ACp(C17050qB v);

    FileInputStream AD0();

    String ADG(MessageDigest messageDigest, long j);

    InputStream ADW();

    OutputStream AEq();

    long AKN();

    long AKR();
}
