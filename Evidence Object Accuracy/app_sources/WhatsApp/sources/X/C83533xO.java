package X;

import android.view.animation.Animation;
import com.whatsapp.R;

/* renamed from: X.3xO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83533xO extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ Animation A00;
    public final /* synthetic */ C14680lr A01;
    public final /* synthetic */ boolean A02;

    public C83533xO(Animation animation, C14680lr r2, boolean z) {
        this.A01 = r2;
        this.A02 = z;
        this.A00 = animation;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        boolean z = this.A02;
        C14680lr r1 = this.A01;
        int i = R.drawable.ic_pause_draft_preview;
        if (z) {
            i = R.drawable.ic_resume_draft_preview;
        }
        r1.A01(i);
        r1.A09.startAnimation(this.A00);
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
        this.A01.A09.setClickable(false);
    }
}
