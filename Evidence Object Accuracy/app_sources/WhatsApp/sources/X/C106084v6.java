package X;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: X.4v6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106084v6 implements AbstractC11600gY {
    public final AbstractC12220hZ A00;
    public final AbstractC12220hZ A01;
    public final AbstractC12250hc A02;
    public final C88914Hy A03;
    public final AnonymousClass4UM A04;
    public final C105914up A05;
    public final ExecutorService A06;
    public final ScheduledExecutorService A07;

    public C106084v6(AbstractC12220hZ r1, AbstractC12220hZ r2, AbstractC12250hc r3, C88914Hy r4, AnonymousClass4UM r5, C105914up r6, ExecutorService executorService, ScheduledExecutorService scheduledExecutorService) {
        this.A03 = r4;
        this.A07 = scheduledExecutorService;
        this.A06 = executorService;
        this.A02 = r3;
        this.A04 = r5;
        this.A05 = r6;
        this.A00 = r1;
        this.A01 = r2;
    }
}
