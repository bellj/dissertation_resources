package X;

import android.net.Uri;

/* renamed from: X.3Sy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67803Sy implements AbstractC116335Va, AnonymousClass5Q9 {
    public long A00 = -1;
    public long A01;
    public AnonymousClass5X6 A02;
    public AnonymousClass3H3 A03;
    public boolean A04 = true;
    public boolean A05;
    public final long A06 = C28721Ot.A03.getAndIncrement();
    public final Uri A07;
    public final AbstractC14070ko A08;
    public final AnonymousClass4IG A09 = new AnonymousClass4IG();
    public final AnonymousClass5QB A0A;
    public final C67753St A0B;
    public final AnonymousClass4MC A0C;
    public volatile boolean A0D;
    public final /* synthetic */ C14060kn A0E;

    public C67803Sy(Uri uri, AbstractC14070ko r6, AnonymousClass5QB r7, C14060kn r8, AnonymousClass2BW r9, AnonymousClass4MC r10) {
        this.A0E = r8;
        this.A07 = uri;
        this.A0B = new C67753St(r9);
        this.A0A = r7;
        this.A08 = r6;
        this.A0C = r10;
        AnonymousClass3D9 r1 = new AnonymousClass3D9();
        r1.A04 = this.A07;
        r1.A03 = 0;
        r1.A00 = 6;
        r1.A05 = C14060kn.A0c;
        this.A03 = r1.A00();
    }
}
