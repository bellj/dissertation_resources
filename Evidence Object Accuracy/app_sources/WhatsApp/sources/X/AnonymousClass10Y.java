package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.SystemClock;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.10Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10Y {
    public final AbstractC15710nm A00;
    public final C14830m7 A01;
    public final C16370ot A02;
    public final C16510p9 A03;
    public final C19990v2 A04;
    public final C20290vW A05;
    public final C18460sU A06;
    public final C21620xi A07;
    public final C20850wQ A08;
    public final C16490p7 A09;
    public final C20930wY A0A;
    public final C14850m9 A0B;
    public final C22450z6 A0C;

    public AnonymousClass10Y(AbstractC15710nm r1, C14830m7 r2, C16370ot r3, C16510p9 r4, C19990v2 r5, C20290vW r6, C18460sU r7, C21620xi r8, C20850wQ r9, C16490p7 r10, C20930wY r11, C14850m9 r12, C22450z6 r13) {
        this.A01 = r2;
        this.A0B = r12;
        this.A06 = r7;
        this.A03 = r4;
        this.A00 = r1;
        this.A04 = r5;
        this.A0C = r13;
        this.A02 = r3;
        this.A0A = r11;
        this.A05 = r6;
        this.A07 = r8;
        this.A09 = r10;
        this.A08 = r9;
    }

    public AbstractC15340mz A00(AbstractC14640lm r7) {
        if (r7 == null) {
            Log.e("msgstore/last/message/jid is null");
            return null;
        }
        C19990v2 r1 = this.A04;
        if (r1.A06(r7) == null) {
            StringBuilder sb = new StringBuilder("msgstore/last/message/no chat for ");
            sb.append(r7);
            Log.w(sb.toString());
            return null;
        }
        AnonymousClass1PE A06 = r1.A06(r7);
        if (A06 == null) {
            return null;
        }
        long j = A06.A0M;
        if (j == 1) {
            return null;
        }
        AbstractC15340mz r0 = A06.A0Z;
        if (r0 != null) {
            return r0;
        }
        AbstractC15340mz A04 = A04(r7, j);
        A06.A0Z = A04;
        return A04;
    }

    public AbstractC15340mz A01(AbstractC14640lm r9) {
        AbstractC15340mz r4;
        AbstractC15340mz r2 = null;
        if (r9 == null) {
            Log.e("msgstore/last/message/jid is null");
        } else {
            C19990v2 r0 = this.A04;
            AnonymousClass1PE A06 = r0.A06(r9);
            if (A06 == null) {
                StringBuilder sb = new StringBuilder("msgstore/last/message/no chat for ");
                sb.append(r9);
                Log.w(sb.toString());
                return null;
            }
            r2 = A06.A0a;
            if (r2 == null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                AnonymousClass1PE A062 = r0.A06(r9);
                if (A062 != null) {
                    long j = A062.A0T;
                    if (j != 1) {
                        r4 = A04(r9, j);
                        this.A05.A00("LastMessageStore/getLastChatsListDisplayedMessageFromDb", SystemClock.uptimeMillis() - uptimeMillis);
                        A06.A0a = r4;
                        return r4;
                    }
                }
                r4 = null;
                A06.A0a = r4;
                return r4;
            }
        }
        return r2;
    }

    public AbstractC15340mz A02(AbstractC14640lm r9) {
        long uptimeMillis = SystemClock.uptimeMillis();
        AbstractC15340mz r5 = null;
        if (r9 == null) {
            return null;
        }
        String[] strArr = {String.valueOf(this.A03.A02(r9))};
        C16310on A01 = this.A09.get();
        try {
            Cursor A09 = A01.A03.A09(C32301bw.A0I, strArr);
            if (A09 != null) {
                if (A09.moveToNext()) {
                    r5 = this.A02.A02(A09, r9, false, true);
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("msgstore/last-raw/db no message for ");
                    sb.append(r9);
                    Log.w(sb.toString());
                }
                A09.close();
            } else {
                Log.e("msgstore/last-raw/db/cursor is null");
            }
            A01.close();
            this.A05.A00("LastMessageStore/getLastMessageRaw", SystemClock.uptimeMillis() - uptimeMillis);
            return r5;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public AbstractC15340mz A03(AbstractC14640lm r10, int i) {
        long uptimeMillis = SystemClock.uptimeMillis();
        AbstractC15340mz r6 = null;
        if (r10 != null) {
            String[] strArr = {String.valueOf(this.A03.A02(r10)), String.valueOf(i)};
            C16310on A01 = this.A09.get();
            try {
                Cursor A09 = A01.A03.A09(C32301bw.A0F, strArr);
                if (A09 == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("msgstore/get/nth no message: ");
                    sb.append(r10);
                    sb.append(" ");
                    sb.append(i);
                    Log.i(sb.toString());
                } else {
                    if (A09.moveToLast()) {
                        r6 = this.A02.A02(A09, r10, false, true);
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("msgstore/get/nth can't get message: ");
                        sb2.append(r10);
                        sb2.append(" ");
                        sb2.append(i);
                        Log.w(sb2.toString());
                    }
                    this.A05.A00("LastMessageStore/getNthLastMessage", SystemClock.uptimeMillis() - uptimeMillis);
                    A09.close();
                }
                A01.close();
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return r6;
    }

    public final AbstractC15340mz A04(AbstractC14640lm r10, long j) {
        AbstractC15340mz A00 = this.A02.A00(j);
        if (!C15380n4.A0O(r10) || !(A00 instanceof AnonymousClass1XB)) {
            return A00;
        }
        AnonymousClass1XB r2 = (AnonymousClass1XB) A00;
        if (r2.A00 != 2) {
            return A00;
        }
        Log.i("msgstore/initialize/update-group-create-failed-msg");
        AnonymousClass1XB A002 = C22140ya.A00(this.A00, r2.A0z, null, 3, r2.A0I);
        A002.A0l(r2.A0I());
        A002.A0u(((C30461Xm) r2).A01);
        this.A07.A02(A002);
        return A002;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r0 != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList A05(X.AbstractC14640lm r12, int r13) {
        /*
            r11 = this;
            long r9 = android.os.SystemClock.uptimeMillis()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r8 = 1
            if (r13 != r8) goto L_0x0062
            X.0mz r2 = r11.A01(r12)
            if (r2 == 0) goto L_0x0051
            X.1IS r0 = r2.A0z
            boolean r0 = r0.A02
            if (r0 == 0) goto L_0x0025
            boolean r0 = X.C22450z6.A00(r2)
            if (r0 != 0) goto L_0x0025
            boolean r0 = r2 instanceof X.AnonymousClass1XB
            if (r0 != 0) goto L_0x0052
            r0 = 0
        L_0x0023:
            if (r0 == 0) goto L_0x0062
        L_0x0025:
            boolean r0 = r2 instanceof X.C30381Xe
            if (r0 != 0) goto L_0x0062
            boolean r0 = r2 instanceof X.C30361Xc
            if (r0 != 0) goto L_0x0062
            X.0m9 r1 = r11.A0B
            r0 = 1818(0x71a, float:2.548E-42)
            boolean r0 = r1.A07(r0)
            if (r0 == 0) goto L_0x003b
            boolean r0 = r2 instanceof X.C30331Wz
            if (r0 != 0) goto L_0x0062
        L_0x003b:
            boolean r0 = r2 instanceof X.C30421Xi
            if (r0 == 0) goto L_0x004e
            int r0 = r2.A08
            if (r0 != r8) goto L_0x004e
            r0 = r2
            X.0oV r0 = (X.AbstractC16130oV) r0
            X.0oX r0 = r0.A02
            if (r0 == 0) goto L_0x0051
            boolean r0 = r0.A0P
            if (r0 == 0) goto L_0x0051
        L_0x004e:
            r4.add(r2)
        L_0x0051:
            return r4
        L_0x0052:
            r0 = r2
            X.1XB r0 = (X.AnonymousClass1XB) r0
            int r0 = r0.A00
            java.util.Set r1 = X.C32201bm.A01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            boolean r0 = r1.contains(r0)
            goto L_0x0023
        L_0x0062:
            X.0p7 r0 = r11.A09
            X.0on r5 = r0.get()
            X.0wY r0 = r11.A0A     // Catch: all -> 0x00b1
            boolean r0 = r0.A02()     // Catch: all -> 0x00b1
            if (r0 == 0) goto L_0x008f
            java.lang.String r7 = X.AnonymousClass1Y7.A05     // Catch: all -> 0x00b1
        L_0x0072:
            X.0op r6 = r5.A03     // Catch: all -> 0x00b1
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch: all -> 0x00b1
            r2 = 0
            X.0p9 r0 = r11.A03     // Catch: all -> 0x00b1
            long r0 = r0.A02(r12)     // Catch: all -> 0x00b1
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch: all -> 0x00b1
            r3[r2] = r0     // Catch: all -> 0x00b1
            java.lang.String r0 = java.lang.String.valueOf(r13)     // Catch: all -> 0x00b1
            r3[r8] = r0     // Catch: all -> 0x00b1
            android.database.Cursor r1 = r6.A09(r7, r3)     // Catch: all -> 0x00b1
            goto L_0x0092
        L_0x008f:
            java.lang.String r7 = X.AnonymousClass1Y7.A06     // Catch: all -> 0x00b1
            goto L_0x0072
        L_0x0092:
            r11.A07(r1, r12, r4)     // Catch: all -> 0x00aa
            if (r1 == 0) goto L_0x009a
            r1.close()     // Catch: all -> 0x00b1
        L_0x009a:
            r5.close()
            X.0vW r3 = r11.A05
            long r1 = android.os.SystemClock.uptimeMillis()
            long r1 = r1 - r9
            java.lang.String r0 = "LastMessageStore/getLastMessagesForNotification"
            r3.A00(r0, r1)
            return r4
        L_0x00aa:
            r0 = move-exception
            if (r1 == 0) goto L_0x00b0
            r1.close()     // Catch: all -> 0x00b0
        L_0x00b0:
            throw r0     // Catch: all -> 0x00b1
        L_0x00b1:
            r0 = move-exception
            r5.close()     // Catch: all -> 0x00b5
        L_0x00b5:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass10Y.A05(X.0lm, int):java.util.ArrayList");
    }

    public List A06(AbstractC14640lm r11, int i) {
        long j;
        ArrayList arrayList = new ArrayList();
        try {
            C16310on A01 = this.A09.get();
            try {
                Cursor A09 = A01.A03.A09(C32301bw.A08, new String[]{String.valueOf(this.A03.A02(r11)), String.valueOf(i)});
                AnonymousClass1PE A06 = this.A04.A06(r11);
                if (A06 != null) {
                    j = A06.A0P;
                } else {
                    j = -1;
                }
                while (A09.moveToNext()) {
                    AbstractC15340mz A02 = this.A02.A02(A09, r11, false, true);
                    if (A02 != null) {
                        boolean z = false;
                        if (A02.A12 <= j) {
                            z = true;
                        }
                        arrayList.add(Pair.create(A02, Boolean.valueOf(z)));
                    }
                }
                A09.close();
                A01.close();
                return arrayList;
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e(e);
            this.A08.A02();
            return arrayList;
        } catch (IllegalStateException e2) {
            Log.i("msgstore/getlastsignificantincomingmessages/IllegalStateException ", e2);
            return arrayList;
        }
    }

    public final void A07(Cursor cursor, AbstractC14640lm r5, ArrayList arrayList) {
        if (cursor != null) {
            while (cursor.moveToNext()) {
                try {
                    try {
                        try {
                            AbstractC15340mz A02 = this.A02.A02(cursor, r5, false, true);
                            if (A02 != null) {
                                if (!(A02 instanceof C30421Xi) || A02.A08 != 1) {
                                    if (this.A0B.A07(1818) && (A02 instanceof C30331Wz)) {
                                    }
                                    arrayList.add(A02);
                                } else {
                                    C16150oX r0 = ((AbstractC16130oV) ((C30421Xi) A02)).A02;
                                    if (r0 != null && r0.A0P) {
                                        arrayList.add(A02);
                                    }
                                }
                            }
                        } catch (SQLiteDatabaseCorruptException e) {
                            Log.e(e);
                            this.A08.A02();
                        }
                    } catch (IllegalStateException e2) {
                        Log.i("msgstore/getlastmessagesfornotification/IllegalStateException ", e2);
                    }
                } finally {
                    cursor.close();
                }
            }
        }
    }
}
