package X;

/* renamed from: X.30k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C614530k extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;
    public Long A03;
    public Long A04;

    public C614530k() {
        super(3522, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A03);
        r3.Abe(3, this.A04);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamPrivacyHighlightDaily {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dialogAppearCount", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "dialogSelectCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "narrativeAppearCount", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "privacyHighlightCategory", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "privacyHighlightSurface", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
