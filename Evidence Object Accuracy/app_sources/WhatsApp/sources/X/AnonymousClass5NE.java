package X;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/* renamed from: X.5NE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NE extends AnonymousClass1TL {
    public byte[] A00;

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return new AnonymousClass5MD(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    public final SimpleDateFormat A0C() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(A0E() ? "yyyyMMddHHmmss.SSSz" : A0G() ? "yyyyMMddHHmmssz" : A0F() ? "yyyyMMddHHmmz" : "yyyyMMddHHz");
        simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        return simpleDateFormat;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Date A0D() {
        /*
            r5 = this;
            byte[] r0 = r5.A00
            java.lang.String r2 = X.AnonymousClass1T7.A02(r0)
            java.lang.String r4 = "Z"
            boolean r0 = r2.endsWith(r4)
            r3 = 0
            if (r0 == 0) goto L_0x004c
            boolean r0 = r5.A0E()
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "yyyyMMddHHmmss.SSS'Z'"
        L_0x0017:
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            r1.<init>(r0)
        L_0x001c:
            java.util.SimpleTimeZone r0 = new java.util.SimpleTimeZone
            r0.<init>(r3, r4)
            r1.setTimeZone(r0)
        L_0x0024:
            boolean r0 = r5.A0E()
            if (r0 == 0) goto L_0x002e
            java.lang.String r2 = A00(r2)
        L_0x002e:
            java.util.Date r0 = r1.parse(r2)
            java.util.Date r0 = X.AnonymousClass3GX.A00(r0)
            return r0
        L_0x0037:
            boolean r0 = r5.A0G()
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = "yyyyMMddHHmmss'Z'"
            goto L_0x0017
        L_0x0040:
            boolean r0 = r5.A0F()
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = "yyyyMMddHHmm'Z'"
            goto L_0x0017
        L_0x0049:
            java.lang.String r0 = "yyyyMMddHH'Z'"
            goto L_0x0017
        L_0x004c:
            r0 = 45
            int r0 = r2.indexOf(r0)
            if (r0 > 0) goto L_0x0087
            r0 = 43
            int r0 = r2.indexOf(r0)
            if (r0 > 0) goto L_0x0087
            boolean r0 = r5.A0E()
            if (r0 == 0) goto L_0x0072
            java.lang.String r0 = "yyyyMMddHHmmss.SSS"
        L_0x0064:
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            r1.<init>(r0)
            java.util.TimeZone r0 = java.util.TimeZone.getDefault()
            java.lang.String r4 = r0.getID()
            goto L_0x001c
        L_0x0072:
            boolean r0 = r5.A0G()
            if (r0 == 0) goto L_0x007b
            java.lang.String r0 = "yyyyMMddHHmmss"
            goto L_0x0064
        L_0x007b:
            boolean r0 = r5.A0F()
            if (r0 == 0) goto L_0x0084
            java.lang.String r0 = "yyyyMMddHHmm"
            goto L_0x0064
        L_0x0084:
            java.lang.String r0 = "yyyyMMddHH"
            goto L_0x0064
        L_0x0087:
            java.lang.String r2 = r5.A0B()
            java.text.SimpleDateFormat r1 = r5.A0C()
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5NE.A0D():java.util.Date");
    }

    public boolean A0E() {
        int i = 0;
        while (true) {
            byte[] bArr = this.A00;
            if (i == bArr.length) {
                return false;
            }
            if (bArr[i] == 46 && i == 14) {
                return true;
            }
            i++;
        }
    }

    public boolean A0F() {
        return A0H(10) && A0H(11);
    }

    public boolean A0G() {
        return A0H(12) && A0H(13);
    }

    public final boolean A0H(int i) {
        byte b;
        byte[] bArr = this.A00;
        return bArr.length > i && (b = bArr[i]) >= 48 && b <= 57;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    public AnonymousClass5NE(byte[] bArr) {
        if (bArr.length >= 4) {
            this.A00 = bArr;
            if (!A0H(0) || !A0H(1) || !A0H(2) || !A0H(3)) {
                throw C12970iu.A0f("illegal characters in GeneralizedTime string");
            }
            return;
        }
        throw C12970iu.A0f("GeneralizedTime string too short");
    }

    public static final String A00(String str) {
        StringBuilder A0h;
        String str2;
        String substring = str.substring(14);
        int i = 1;
        while (i < substring.length() && '0' <= (r1 = substring.charAt(i)) && r1 <= '9') {
            i++;
        }
        int i2 = i - 1;
        if (i2 > 3) {
            A0h = C12960it.A0h();
            str2 = substring.substring(0, 4);
        } else if (i2 == 1) {
            A0h = C12960it.A0h();
            C72453ed.A1O(substring, A0h, 0, i);
            str2 = "00";
        } else if (i2 != 2) {
            return str;
        } else {
            A0h = C12960it.A0h();
            C72453ed.A1O(substring, A0h, 0, i);
            str2 = "0";
        }
        A0h.append(str2);
        String A0d = C12960it.A0d(substring.substring(i), A0h);
        StringBuilder A0h2 = C12960it.A0h();
        C72453ed.A1O(str, A0h2, 0, 14);
        return C12960it.A0d(A0d, A0h2);
    }

    public static AnonymousClass5NE A01(Object obj) {
        if (obj == null || (obj instanceof AnonymousClass5NE)) {
            return (AnonymousClass5NE) obj;
        }
        if (obj instanceof byte[]) {
            try {
                return (AnonymousClass5NE) AnonymousClass1TL.A03((byte[]) obj);
            } catch (Exception e) {
                throw C12970iu.A0f(C12960it.A0d(e.toString(), C12960it.A0k("encoding error in getInstance: ")));
            }
        } else {
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("illegal object in getInstance: ")));
        }
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        int length = (!(this instanceof AnonymousClass5MD) ? this.A00 : ((AnonymousClass5MD) this).A0I()).length;
        return AnonymousClass1TQ.A00(length) + 1 + length;
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A07() {
        return !(this instanceof AnonymousClass5MD) ? new AnonymousClass5MD(this.A00) : this;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(!(this instanceof AnonymousClass5MD) ? this.A00 : ((AnonymousClass5MD) this).A0I(), 24, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NE)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NE) r3).A00);
    }

    public String A0B() {
        StringBuilder sb;
        String str;
        StringBuilder A0j;
        String str2;
        String num;
        String num2;
        String A0d;
        String str3;
        String str4;
        String A02 = AnonymousClass1T7.A02(this.A00);
        int length = A02.length();
        int i = length - 1;
        if (A02.charAt(i) == 'Z') {
            A0j = C12960it.A0h();
            C72453ed.A1O(A02, A0j, 0, i);
            A0d = "GMT+00:00";
        } else {
            int i2 = length - 6;
            char charAt = A02.charAt(i2);
            if ((charAt == '-' || charAt == '+') && A02.indexOf("GMT") == i2 - 3) {
                return A02;
            }
            int i3 = length - 5;
            char charAt2 = A02.charAt(i3);
            if (charAt2 == '-' || charAt2 == '+') {
                sb = C12960it.A0h();
                C72453ed.A1O(A02, sb, 0, i3);
                sb.append("GMT");
                int i4 = i3 + 3;
                C72453ed.A1O(A02, sb, i3, i4);
                sb.append(":");
                str = A02.substring(i4);
            } else {
                int i5 = length - 3;
                char charAt3 = A02.charAt(i5);
                if (charAt3 == '-' || charAt3 == '+') {
                    sb = C12960it.A0h();
                    C72453ed.A1O(A02, sb, 0, i5);
                    sb.append("GMT");
                    sb.append(A02.substring(i5));
                    str = ":00";
                } else {
                    A0j = C12960it.A0j(A02);
                    TimeZone timeZone = TimeZone.getDefault();
                    int rawOffset = timeZone.getRawOffset();
                    if (rawOffset < 0) {
                        rawOffset = -rawOffset;
                        str2 = "-";
                    } else {
                        str2 = "+";
                    }
                    int i6 = rawOffset / 3600000;
                    int i7 = (rawOffset - (((i6 * 60) * 60) * 1000)) / 60000;
                    try {
                        if (timeZone.useDaylightTime()) {
                            if (A0E()) {
                                A02 = A00(A02);
                            }
                            SimpleDateFormat A0C = A0C();
                            StringBuilder A0j2 = C12960it.A0j(A02);
                            A0j2.append("GMT");
                            A0j2.append(str2);
                            if (i6 < 10) {
                                str3 = C12960it.A0W(i6, "0");
                            } else {
                                str3 = Integer.toString(i6);
                            }
                            A0j2.append(str3);
                            A0j2.append(":");
                            if (i7 < 10) {
                                str4 = C12960it.A0W(i7, "0");
                            } else {
                                str4 = Integer.toString(i7);
                            }
                            if (timeZone.inDaylightTime(A0C.parse(C12960it.A0d(str4, A0j2)))) {
                                int i8 = -1;
                                if (str2.equals("+")) {
                                    i8 = 1;
                                }
                                i6 += i8;
                            }
                        }
                    } catch (ParseException unused) {
                    }
                    StringBuilder A0j3 = C12960it.A0j("GMT");
                    A0j3.append(str2);
                    if (i6 < 10) {
                        num = C12960it.A0W(i6, "0");
                    } else {
                        num = Integer.toString(i6);
                    }
                    A0j3.append(num);
                    A0j3.append(":");
                    if (i7 < 10) {
                        num2 = C12960it.A0W(i7, "0");
                    } else {
                        num2 = Integer.toString(i7);
                    }
                    A0d = C12960it.A0d(num2, A0j3);
                }
            }
            return C12960it.A0d(str, sb);
        }
        return C12960it.A0d(A0d, A0j);
    }
}
