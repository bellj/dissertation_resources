package X;

import java.util.Queue;

/* renamed from: X.2uq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59502uq extends C37171lc {
    public final AbstractC116555Vx A00;
    public final Queue A01;

    public C59502uq(AbstractC116555Vx r2, Queue queue) {
        super(AnonymousClass39o.A06);
        this.A01 = queue;
        this.A00 = r2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return C29941Vi.A00(this.A01, ((C59502uq) obj).A01);
    }

    @Override // X.C37171lc
    public int hashCode() {
        Object[] objArr = new Object[3];
        C12960it.A1O(objArr, super.hashCode());
        objArr[1] = this.A01;
        return C12980iv.A0B(this.A00, objArr, 2);
    }
}
