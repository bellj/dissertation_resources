package X;

import java.util.Comparator;

/* renamed from: X.0eQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10320eQ implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        C04940Nq r3 = (C04940Nq) obj;
        C04940Nq r4 = (C04940Nq) obj2;
        int i = r3.A01 - r4.A01;
        return i == 0 ? r3.A02 - r4.A02 : i;
    }
}
