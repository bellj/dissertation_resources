package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RatingBar;
import com.whatsapp.R;

/* renamed from: X.0Bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02380Bx extends RatingBar {
    public final AnonymousClass0SU A00;

    public C02380Bx(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, R.attr.ratingBarStyle);
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass0SU r0 = new AnonymousClass0SU(this);
        this.A00 = r0;
        r0.A01(attributeSet, R.attr.ratingBarStyle);
    }

    @Override // android.widget.RatingBar, android.widget.AbsSeekBar, android.widget.ProgressBar, android.view.View
    public synchronized void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        Bitmap bitmap = this.A00.A00;
        if (bitmap != null) {
            setMeasuredDimension(View.resolveSizeAndState(bitmap.getWidth() * getNumStars(), i, 0), getMeasuredHeight());
        }
    }
}
