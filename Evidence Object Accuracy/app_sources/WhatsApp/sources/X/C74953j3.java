package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3j3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74953j3 extends AbstractC018308n {
    public int A00 = 0;
    public int A01;
    public final int A02;

    public C74953j3(int i, int i2) {
        this.A02 = i;
        this.A01 = i2;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r6, RecyclerView recyclerView) {
        int A00 = RecyclerView.A00(view);
        if (A00 == 0) {
            rect.set(0, this.A01, 0, 0);
        } else if (A00 == this.A02 - 1) {
            rect.set(0, 0, 0, this.A00);
        }
    }
}
