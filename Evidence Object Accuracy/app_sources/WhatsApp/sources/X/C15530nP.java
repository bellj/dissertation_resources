package X;

import android.content.UriMatcher;

/* renamed from: X.0nP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15530nP {
    public final UriMatcher A00;
    public final AnonymousClass01H A01;

    public C15530nP(C15570nT r10, C15550nR r11, C15610nY r12, C15600nX r13, C15620nZ r14, C15560nS r15) {
        this.A01 = new C002601e(null, new AnonymousClass01N(r11, r12, r13, r14, r15) { // from class: X.5EW
            public final /* synthetic */ C15550nR A01;
            public final /* synthetic */ C15610nY A02;
            public final /* synthetic */ C15600nX A03;
            public final /* synthetic */ C15620nZ A04;
            public final /* synthetic */ C15560nS A05;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
                this.A05 = r6;
                this.A04 = r5;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new C15540nQ(C15570nT.this, this.A01, this.A02, this.A03, this.A04, this.A05);
            }
        });
        UriMatcher uriMatcher = new UriMatcher(-1);
        this.A00 = uriMatcher;
        uriMatcher.addURI("com.whatsapp.provider.instrumentation", "contacts", 1);
    }
}
