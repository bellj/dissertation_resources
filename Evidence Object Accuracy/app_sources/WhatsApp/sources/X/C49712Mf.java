package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Mf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49712Mf extends AbstractC28131Kt {
    public final UserJid A00;
    public final UserJid A01;
    public final UserJid A02;

    public C49712Mf(UserJid userJid, UserJid userJid2, UserJid userJid3, String str) {
        super(null, str);
        this.A00 = userJid;
        this.A01 = userJid2;
        this.A02 = userJid3;
    }
}
