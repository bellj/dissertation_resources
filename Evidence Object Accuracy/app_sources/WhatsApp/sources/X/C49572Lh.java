package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Lh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49572Lh extends AnimatorListenerAdapter {
    public final /* synthetic */ HomeActivity A00;

    public C49572Lh(HomeActivity homeActivity) {
        this.A00 = homeActivity;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        super.onAnimationEnd(animator);
        this.A00.A08.setVisibility(0);
    }
}
