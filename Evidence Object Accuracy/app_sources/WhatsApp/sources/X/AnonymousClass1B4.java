package X;

import android.net.TrafficStats;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

/* renamed from: X.1B4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1B4 {
    public final C18790t3 A00;
    public final AnonymousClass1B2 A01;
    public final AnonymousClass1B6 A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C18800t4 A05;
    public final C19930uu A06;
    public final AbstractC14440lR A07;

    public AnonymousClass1B4(C18790t3 r1, AnonymousClass1B2 r2, AnonymousClass1B6 r3, C18640sm r4, C14830m7 r5, C18800t4 r6, C19930uu r7, AbstractC14440lR r8) {
        this.A04 = r5;
        this.A07 = r8;
        this.A06 = r7;
        this.A00 = r1;
        this.A05 = r6;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
    }

    public AnonymousClass4N2 A00(String str, String str2, JSONObject jSONObject, boolean z) {
        InputStream inflaterInputStream;
        int i;
        JSONObject jSONObject2 = null;
        if (!this.A03.A0B()) {
            i = -1;
        } else {
            TrafficStats.setThreadStatsTag(19);
            if (!str2.startsWith("https://")) {
                StringBuilder sb = new StringBuilder();
                sb.append("https://");
                sb.append(str2);
                str2 = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append(AnonymousClass029.A08);
            sb2.append(str);
            sb2.append("?");
            sb2.append("access_token");
            sb2.append("=");
            sb2.append(AnonymousClass029.A0B);
            sb2.append("|");
            sb2.append(AnonymousClass029.A09);
            URL url = new URL(sb2.toString());
            URLConnection openConnection = url.openConnection();
            if (openConnection instanceof HttpsURLConnection) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) openConnection;
                if (z) {
                    httpsURLConnection.setSSLSocketFactory(this.A05.A00());
                }
                httpsURLConnection.setRequestMethod("POST");
                httpsURLConnection.setConnectTimeout(15000);
                httpsURLConnection.setReadTimeout(30000);
                httpsURLConnection.setUseCaches(false);
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                httpsURLConnection.setRequestProperty("Content-Type", A01("Content-Type"));
                httpsURLConnection.setRequestProperty("Accept-Encoding", A01("Accept-Encoding"));
                httpsURLConnection.setRequestProperty("User-Agent", A01("User-Agent"));
                C18790t3 r7 = this.A00;
                AnonymousClass23V r2 = new AnonymousClass23V(r7, httpsURLConnection.getOutputStream(), null, 19);
                String obj = jSONObject.toString();
                if (TextUtils.isEmpty(obj)) {
                    i = 3;
                } else {
                    r2.write(obj.getBytes(AnonymousClass01V.A08));
                    r2.flush();
                    long currentTimeMillis = System.currentTimeMillis();
                    httpsURLConnection.connect();
                    int responseCode = httpsURLConnection.getResponseCode();
                    AnonymousClass1B2 r9 = this.A01;
                    Integer valueOf = Integer.valueOf(responseCode);
                    Long valueOf2 = Long.valueOf(System.currentTimeMillis() - currentTimeMillis);
                    C855843j r22 = new C855843j();
                    r22.A02 = r9.A00;
                    r22.A03 = "HttpsUrlConnection";
                    r22.A00 = valueOf2;
                    r22.A04 = str;
                    r22.A01 = Long.valueOf((long) valueOf.intValue());
                    r9.A03.A07(r22);
                    if (responseCode / 100 == 2) {
                        String contentEncoding = httpsURLConnection.getContentEncoding();
                        InputStream r1 = new AnonymousClass1oJ(r7, httpsURLConnection.getInputStream(), null, 19);
                        if ("gzip".equalsIgnoreCase(contentEncoding)) {
                            inflaterInputStream = new GZIPInputStream(r1);
                        } else {
                            if ("deflate".equalsIgnoreCase(contentEncoding)) {
                                inflaterInputStream = new InflaterInputStream(r1);
                            }
                            jSONObject2 = AnonymousClass1P1.A02(r1);
                        }
                        r1 = inflaterInputStream;
                        jSONObject2 = AnonymousClass1P1.A02(r1);
                    }
                    httpsURLConnection.disconnect();
                    return new AnonymousClass4N2(jSONObject2, responseCode);
                }
            } else {
                StringBuilder sb3 = new StringBuilder("Failed to create a HTTPS connection with ");
                sb3.append(url.toString());
                throw new IOException(sb3.toString());
            }
        }
        return new AnonymousClass4N2(null, i);
    }

    public final String A01(String str) {
        switch (str.hashCode()) {
            case -1844712829:
                if (str.equals("User-Agent")) {
                    return this.A06.A01();
                }
                break;
            case -1099743112:
                if (str.equals("Accept-Encoding")) {
                    return "gzip";
                }
                break;
            case 949037134:
                if (str.equals("Content-Type")) {
                    return "application/json";
                }
                break;
        }
        StringBuilder sb = new StringBuilder("DirectoryGraphAPIImpl/getResponseFamily unsupported header name: ");
        sb.append(str);
        Log.e(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("DirectoryGraphAPIImpl/getResponseFamily unsupported header name: ");
        sb2.append(str);
        throw new IllegalStateException(sb2.toString());
    }
}
