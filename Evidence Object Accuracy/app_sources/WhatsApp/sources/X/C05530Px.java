package X;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import com.whatsapp.R;

/* renamed from: X.0Px  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05530Px {
    public int A00 = 8388611;
    public View A01;
    public PopupWindow.OnDismissListener A02;
    public AnonymousClass0XP A03;
    public AbstractC12280hf A04;
    public boolean A05;
    public final int A06;
    public final int A07;
    public final Context A08;
    public final PopupWindow.OnDismissListener A09 = new AnonymousClass0X5(this);
    public final AnonymousClass07H A0A;
    public final boolean A0B;

    public C05530Px(Context context, View view, AnonymousClass07H r4, int i, int i2, boolean z) {
        this.A08 = context;
        this.A0A = r4;
        this.A01 = view;
        this.A0B = z;
        this.A06 = i;
        this.A07 = i2;
    }

    public AnonymousClass0XP A00() {
        AnonymousClass0XP r3 = this.A03;
        if (r3 == null) {
            Context context = this.A08;
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            Point point = new Point();
            if (Build.VERSION.SDK_INT >= 17) {
                defaultDisplay.getRealSize(point);
            } else {
                defaultDisplay.getSize(point);
            }
            if (Math.min(point.x, point.y) >= context.getResources().getDimensionPixelSize(R.dimen.abc_cascading_menus_min_smallest_width)) {
                r3 = new AnonymousClass0CM(context, this.A01, this.A06, this.A07, this.A0B);
            } else {
                r3 = new AnonymousClass0CN(context, this.A01, this.A0A, this.A06, this.A07, this.A0B);
            }
            r3.A06(this.A0A);
            r3.A05(this.A09);
            r3.A04(this.A01);
            r3.Abr(this.A04);
            r3.A07(this.A05);
            r3.A01(this.A00);
            this.A03 = r3;
        }
        return r3;
    }

    public void A01() {
        AnonymousClass0XP r0 = this.A03;
        if (r0 != null && r0.AK4()) {
            this.A03.dismiss();
        }
    }

    public void A02() {
        this.A03 = null;
        PopupWindow.OnDismissListener onDismissListener = this.A02;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r1 == false) goto L_0x000b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A03() {
        /*
            r3 = this;
            X.0XP r0 = r3.A03
            if (r0 == 0) goto L_0x000b
            boolean r1 = r0.AK4()
            r0 = 1
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            r2 = 1
            if (r0 != 0) goto L_0x001f
            android.view.View r0 = r3.A01
            r1 = 0
            if (r0 != 0) goto L_0x0015
            return r1
        L_0x0015:
            X.0XP r0 = r3.A00()
            r0.A08(r1)
            r0.Ade()
        L_0x001f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C05530Px.A03():boolean");
    }
}
