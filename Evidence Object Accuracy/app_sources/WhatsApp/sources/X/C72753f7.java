package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.transition.TransitionValues;

/* renamed from: X.3f7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72753f7 extends AnimatorListenerAdapter {
    public final /* synthetic */ TransitionValues A00;
    public final /* synthetic */ C52342ae A01;

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
    }

    public C72753f7(TransitionValues transitionValues, C52342ae r2) {
        this.A01 = r2;
        this.A00 = transitionValues;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A00.view.setAlpha(1.0f);
    }
}
