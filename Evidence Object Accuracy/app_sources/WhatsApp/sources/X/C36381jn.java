package X;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.WaTextView;

/* renamed from: X.1jn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36381jn extends AnonymousClass03U {
    public final Activity A00;
    public final ImageView A01;
    public final AnonymousClass12P A02;
    public final WaTextView A03;
    public final WaTextView A04;
    public final C22640zP A05;
    public final C15550nR A06;
    public final AnonymousClass1J1 A07;
    public final AbstractC14440lR A08;

    public C36381jn(Activity activity, View view, AnonymousClass12P r6, C22640zP r7, C15550nR r8, AnonymousClass1J1 r9, AbstractC14440lR r10) {
        super(view);
        this.A08 = r10;
        this.A02 = r6;
        this.A06 = r8;
        this.A07 = r9;
        this.A05 = r7;
        View view2 = this.A0H;
        this.A01 = (ImageView) AnonymousClass028.A0D(view2, R.id.community_icon);
        WaTextView waTextView = (WaTextView) AnonymousClass028.A0D(view2, R.id.community_subject);
        this.A04 = waTextView;
        this.A03 = (WaTextView) AnonymousClass028.A0D(view2, R.id.community_new);
        this.A00 = activity;
        C27531Hw.A06(waTextView);
    }
}
