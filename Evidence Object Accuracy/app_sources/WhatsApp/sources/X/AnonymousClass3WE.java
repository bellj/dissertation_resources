package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.community.JoinGroupBottomSheetFragment;

/* renamed from: X.3WE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3WE implements AnonymousClass28F {
    public final /* synthetic */ JoinGroupBottomSheetFragment A00;
    public final /* synthetic */ C15370n3 A01;

    public AnonymousClass3WE(JoinGroupBottomSheetFragment joinGroupBottomSheetFragment, C15370n3 r2) {
        this.A00 = joinGroupBottomSheetFragment;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            Adv(imageView);
        }
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        imageView.setImageDrawable(C12970iu.A0C(imageView.getContext(), this.A00.A0T.A01(this.A01)));
    }
}
