package X;

import android.view.View;

/* renamed from: X.0WH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WH implements View.OnClickListener {
    public final /* synthetic */ C05150Ol A00;

    public AnonymousClass0WH(C05150Ol r1) {
        this.A00 = r1;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        AnonymousClass09V r0 = this.A00.A03.A00.A05;
        if (r0 != null) {
            r0.dismiss();
        }
    }
}
