package X;

/* renamed from: X.2xB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xB extends C852541v {
    public final /* synthetic */ AnonymousClass2xA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2xB(AnonymousClass2xA r1, C15610nY r2, AnonymousClass018 r3) {
        super(r2, r3);
        this.A00 = r1;
    }

    @Override // X.C852541v, X.C71333cl
    public int A00(C15370n3 r3, C15370n3 r4) {
        C29951Vj r1 = r3.A0F;
        C29951Vj r0 = r4.A0F;
        if (r1 == null) {
            if (r0 != null) {
                return -1;
            }
        } else if (r0 == null) {
            return 1;
        } else {
            C15580nU r12 = r1.A01;
            C15580nU r02 = r0.A01;
            if (r12 != null) {
                if (r02 == null) {
                    return 1;
                }
            } else if (r02 != null) {
                return -1;
            }
        }
        return super.A00(r3, r4);
    }
}
