package X;

/* renamed from: X.0dk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09930dk implements Runnable {
    public final /* synthetic */ RunnableC10250eH A00;
    public final /* synthetic */ AnonymousClass040 A01;
    public final /* synthetic */ AbstractFutureC44231yX A02;

    public RunnableC09930dk(RunnableC10250eH r1, AnonymousClass040 r2, AbstractFutureC44231yX r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.A02.get();
            C06390Tk A00 = C06390Tk.A00();
            String str = RunnableC10250eH.A0J;
            RunnableC10250eH r2 = this.A00;
            A00.A02(str, String.format("Starting work for %s", r2.A08.A0G), new Throwable[0]);
            AbstractFutureC44231yX A01 = r2.A03.A01();
            r2.A0D = A01;
            this.A01.A08(A01);
        } catch (Throwable th) {
            this.A01.A0A(th);
        }
    }
}
