package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;
import java.util.Locale;

/* renamed from: X.5g1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120335g1 extends AbstractC124175oj {
    public final int A00;
    public final int A01;
    public final Context A02;
    public final C14900mE A03;
    public final C15570nT A04;
    public final C18640sm A05;
    public final C14830m7 A06;
    public final AnonymousClass102 A07;
    public final C17220qS A08;
    public final AnonymousClass60Z A09;
    public final C18650sn A0A;
    public final C18610sj A0B;
    public final C17070qD A0C;
    public final C30931Zj A0D = C117305Zk.A0V("BaseTokenAddCardAction", "network");
    public final C129385xd A0E;
    public final C129945yY A0F;
    public final C128775we A0G;
    public final C18590sh A0H;
    public final AbstractC14440lR A0I;
    public final Boolean A0J;
    public final String A0K;
    public final String A0L;

    public C120335g1(Context context, C14900mE r10, C15570nT r11, C18640sm r12, C14830m7 r13, AnonymousClass102 r14, C17220qS r15, AnonymousClass60Z r16, C18650sn r17, C18600si r18, C18610sj r19, C17070qD r20, C129385xd r21, C129945yY r22, C128775we r23, C18590sh r24, AnonymousClass1BY r25, C22120yY r26, AbstractC14440lR r27, Boolean bool, String str, String str2, int i, int i2) {
        super(r12, r18, r19, r25, r26);
        this.A03 = r10;
        this.A0I = r27;
        this.A0H = r24;
        this.A0C = r20;
        this.A0B = r19;
        this.A0A = r17;
        this.A0K = str;
        this.A0L = str2;
        this.A00 = i;
        this.A01 = i2;
        this.A06 = r13;
        this.A02 = context;
        this.A04 = r11;
        this.A08 = r15;
        this.A0F = r22;
        this.A0E = r21;
        this.A07 = r14;
        this.A09 = r16;
        this.A05 = r12;
        this.A0J = bool;
        this.A0G = r23;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        String str;
        C30931Zj r1;
        String str2;
        AnonymousClass01T r0 = (AnonymousClass01T) obj;
        String str3 = (String) r0.A00;
        C452120p r3 = (C452120p) r0.A01;
        if (str3 == null) {
            Log.i(C12960it.A0b("PAY: BrazilAddCardAction token error: ", r3));
            this.A0G.A00(null, r3, null, false);
            return;
        }
        Log.i("PAY: BrazilAddCardAction sendAddCard token success");
        String A0j = C117305Zk.A0j(this.A04, this.A06);
        C17220qS r12 = this.A08;
        String A01 = r12.A01();
        String A012 = this.A0H.A01();
        Locale locale = Locale.US;
        Object[] A1b = C12970iu.A1b();
        int i = this.A00;
        C12960it.A1P(A1b, i, 0);
        String format = String.format(locale, "%02d", A1b);
        int i2 = this.A01;
        String num = Integer.toString(i2);
        Boolean bool = this.A0J;
        if (bool == null || bool.booleanValue()) {
            str = "1";
        } else {
            str = "0";
        }
        C129385xd r8 = this.A0E;
        String str4 = this.A0K;
        C128075vW r02 = r8.A00;
        C126205sV r14 = null;
        if (r02 != null && r02.A00 == 5) {
            String str5 = r02.A02;
            r02.A02 = null;
            String A00 = r8.A0H.A00(5);
            String A06 = r8.A0E.A06();
            StringBuilder A0j2 = C12960it.A0j(str5);
            A0j2.append(A00);
            A0j2.append(A06);
            A0j2.append(str4.replaceAll("\\s", ""));
            A0j2.append(i);
            Locale locale2 = Locale.US;
            Object[] A1b2 = C12970iu.A1b();
            C12960it.A1P(A1b2, i2 % 100, 0);
            String A0d = C12960it.A0d(String.format(locale2, "%02d", A1b2), A0j2);
            AnonymousClass60Z r03 = r8.A0C;
            String A04 = r03.A04(A0d);
            if (A04 == null) {
                r1 = r8.A01;
                str2 = "device_signature is null";
            } else {
                String A05 = r03.A05(A0d);
                if (A05 == null) {
                    r1 = r8.A01;
                    str2 = "wallet_signature is null";
                } else if (str5 == null) {
                    r1 = r8.A01;
                    str2 = "challenge_id is null";
                } else {
                    r14 = new C126205sV(A04, A05, str5);
                }
            }
            r1.A05(str2);
        }
        C117325Zm.A05(r12, new IDxRCallbackShape2S0100000_3_I1(this.A02, this.A03, this.A0A, this, 0), new C130445zQ(r14, new AnonymousClass3CT(A01), A012, A0j, format, num, str3, str).A00, A01);
    }
}
