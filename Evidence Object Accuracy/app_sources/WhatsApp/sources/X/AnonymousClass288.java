package X;

/* renamed from: X.288  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass288 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public String A02;

    public AnonymousClass288() {
        super(2692, new AnonymousClass00E(10, 1000, 1000000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A01);
        r3.Abe(5, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamActivityStats {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "featureName", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "memoryKbsConsumed", this.A01);
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "memoryStatStage", obj);
        sb.append("}");
        return sb.toString();
    }
}
