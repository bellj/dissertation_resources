package X;

import android.text.StaticLayout;
import android.widget.TextView;

/* renamed from: X.08L  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08L extends AnonymousClass08M {
    @Override // X.AnonymousClass08M, X.AnonymousClass08N
    public void A00(StaticLayout.Builder builder, TextView textView) {
        builder.setTextDirection(textView.getTextDirectionHeuristic());
    }

    @Override // X.AnonymousClass08N
    public boolean A01(TextView textView) {
        return textView.isHorizontallyScrollable();
    }
}
