package X;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.3dV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71783dV extends ScheduledThreadPoolExecutor {
    public final /* synthetic */ C29631Ua A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71783dV(C29631Ua r2, ThreadFactory threadFactory) {
        super(1, threadFactory);
        this.A00 = r2;
    }

    @Override // java.util.concurrent.ThreadPoolExecutor
    public void afterExecute(Runnable runnable, Throwable th) {
        super.afterExecute(runnable, th);
        if (runnable instanceof Future) {
            Future future = (Future) runnable;
            if (future.isDone()) {
                try {
                    future.get();
                } catch (InterruptedException unused) {
                } catch (ExecutionException e) {
                    th = e.getCause();
                }
            }
        }
        if (th != null) {
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
        }
    }
}
