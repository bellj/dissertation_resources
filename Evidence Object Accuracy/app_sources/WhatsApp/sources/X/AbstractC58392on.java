package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.2on  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC58392on extends AbstractC53142cy {
    public TextEmojiLabel A00;
    public WaImageView A01;
    public WaTextView A02;
    public AnonymousClass01d A03;
    public AnonymousClass018 A04;
    public boolean A05;

    public abstract int getRootLayoutID();

    public AbstractC58392on(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AbstractC58392on(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.A05 = true;
        A01(attributeSet);
    }

    /* JADX INFO: finally extract failed */
    public void A01(AttributeSet attributeSet) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = C12980iv.A0I(this).obtainStyledAttributes(attributeSet, AnonymousClass2GZ.A0B, 0, 0);
            try {
                int resourceId = obtainStyledAttributes.getResourceId(4, 0);
                i = obtainStyledAttributes.getResourceId(0, 0);
                i2 = obtainStyledAttributes.getResourceId(1, 0);
                i3 = obtainStyledAttributes.getColor(2, 0);
                i4 = obtainStyledAttributes.getColor(5, 0);
                this.A05 = obtainStyledAttributes.getBoolean(3, true);
                obtainStyledAttributes.recycle();
                i5 = resourceId;
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        } else {
            i4 = 0;
            i = 0;
            i2 = 0;
            i3 = 0;
        }
        View inflate = C12960it.A0E(this).inflate(getRootLayoutID(), (ViewGroup) this, true);
        this.A02 = C12960it.A0N(inflate, R.id.list_item_title);
        this.A00 = C12970iu.A0T(inflate, R.id.list_item_description);
        this.A01 = C12980iv.A0X(inflate, R.id.list_item_icon);
        if (i5 != 0) {
            this.A02.setText(i5);
        }
        if (i != 0) {
            this.A00.setText(i);
            this.A00.setText(i);
        }
        if (i4 != 0) {
            this.A02.setTextColor(i4);
        }
        if (i3 != 0) {
            setIconColor(i3);
        }
        if (i2 != 0) {
            setIcon(i2);
        }
    }

    public void setDescription(Spanned spanned) {
        this.A00.setText(spanned);
        C52162aM.A00(this.A00);
        AbstractC28491Nn.A04(this.A00, this.A03);
    }

    public void setDescription(String str) {
        this.A00.setText(str);
    }

    public void setIcon(int i) {
        Drawable drawable;
        if (getResources().getXml(i) != null) {
            drawable = C013606j.A01(null, getResources(), i);
            AnonymousClass2GF r1 = new AnonymousClass2GF(drawable, this.A04);
            r1.A00 = this.A05;
            this.A01.setImageDrawable(r1);
        }
        drawable = AnonymousClass00T.A04(getContext(), i);
        AnonymousClass2GF r1 = new AnonymousClass2GF(drawable, this.A04);
        r1.A00 = this.A05;
        this.A01.setImageDrawable(r1);
    }

    public void setIconColor(int i) {
        C016307r.A00(ColorStateList.valueOf(i), this.A01);
    }

    public void setIconVisible(boolean z) {
        this.A01.setVisibility(C12960it.A02(z ? 1 : 0));
    }

    public void setTitle(String str) {
        this.A02.setText(str);
    }
}
