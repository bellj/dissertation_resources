package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/* renamed from: X.0Tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06440Tp {
    public static boolean A0D = true;
    public static BitmapFactory.Options A0E;
    public static final Bitmap A0F = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
    public static final C06090Sd A0G = new C06090Sd(20);
    public static final C06090Sd A0H = new C06090Sd(32);
    public int A00 = -1;
    public int A01 = -1;
    public int A02 = -1;
    public int A03 = -1;
    public int A04 = -1;
    public long A05 = -1;
    public Bitmap A06;
    public BitmapFactory.Options A07;
    public C06440Tp A08 = null;
    public C06440Tp A09 = null;
    public final AnonymousClass0eL A0A = new AnonymousClass0IK(this);
    public final C06440Tp[] A0B = new C06440Tp[4];
    public volatile int A0C = 0;

    public C06440Tp(int i, int i2) {
        this.A01 = i;
        this.A00 = i2;
        if (!A0D) {
            BitmapFactory.Options options = A0E;
            if (options == null) {
                options = new BitmapFactory.Options();
                A0E = options;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
            }
            this.A07 = options;
            return;
        }
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        this.A07 = options2;
        options2.inSampleSize = 1;
        options2.inPreferredConfig = Bitmap.Config.RGB_565;
        options2.inMutable = true;
    }

    public static C06440Tp A00(byte[] bArr, int i) {
        Bitmap bitmap;
        C06440Tp r5 = new C06440Tp(-1, -1);
        if (A0D) {
            BitmapFactory.Options options = r5.A07;
            if (options.inBitmap == null) {
                options.inBitmap = (Bitmap) A0H.A00();
            }
        }
        try {
            BitmapFactory.Options options2 = r5.A07;
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, i, options2);
            r5.A06 = bitmap;
            if (A0D) {
                options2.inBitmap = null;
            }
        } catch (IllegalArgumentException unused) {
            C06160Sk.A06.A00();
            A0D = false;
            BitmapFactory.Options options3 = r5.A07;
            options3.inBitmap.recycle();
            options3.inBitmap = null;
            A0H.A01();
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, i, options3);
            r5.A06 = bitmap;
        }
        if (bitmap == null) {
            r5.A02();
            return null;
        }
        r5.A01 = bitmap.getWidth();
        r5.A00 = r5.A06.getHeight();
        return r5;
    }

    public synchronized Bitmap A01() {
        return this.A06;
    }

    public void A02() {
        this.A01 = -1;
        this.A00 = -1;
        for (int i = 0; i < 4; i++) {
            this.A0B[i] = null;
        }
        A03();
        this.A0C = 0;
        this.A08 = null;
        this.A09 = null;
        this.A02 = -1;
        this.A03 = -1;
        this.A04 = -1;
        this.A05 = -1;
    }

    public final synchronized void A03() {
        Bitmap bitmap = this.A06;
        if (!(bitmap == null || bitmap == A0F)) {
            if (bitmap.getConfig() == Bitmap.Config.ARGB_8888) {
                A0G.A02(this.A06);
            } else if (A0D) {
                A0H.A02(this.A06);
            } else {
                this.A06.recycle();
            }
        }
        this.A06 = null;
    }

    public synchronized String toString() {
        StringBuilder sb;
        String str;
        sb = new StringBuilder();
        sb.append(C06440Tp.class.getSimpleName());
        sb.append(" {x=");
        sb.append(this.A02);
        sb.append(", y=");
        sb.append(this.A03);
        sb.append(", zoom=");
        sb.append(this.A04);
        sb.append(", status=");
        sb.append(this.A0C);
        sb.append("}");
        if (this.A06 == null) {
            str = "x";
        } else {
            str = "o";
        }
        sb.append(str);
        return sb.toString();
    }
}
