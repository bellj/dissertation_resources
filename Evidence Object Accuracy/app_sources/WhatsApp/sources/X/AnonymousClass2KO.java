package X;

import com.whatsapp.util.Log;

/* renamed from: X.2KO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2KO {
    public final /* synthetic */ C41121sw A00;

    public AnonymousClass2KO(C41121sw r1) {
        this.A00 = r1;
    }

    public void A00(int i) {
        StringBuilder sb = new StringBuilder("CompanionDeviceQrHandler/onError code=");
        sb.append(i);
        Log.i(sb.toString());
        C41121sw r3 = this.A00;
        C49222Ju r1 = r3.A01;
        if (r1 != null) {
            r1.A02 = false;
        }
        C22100yW r2 = r3.A0D;
        synchronized (r2.A0O) {
            r2.A01 = null;
        }
        r3.A00 = null;
        r3.A0F.APl(i);
    }
}
