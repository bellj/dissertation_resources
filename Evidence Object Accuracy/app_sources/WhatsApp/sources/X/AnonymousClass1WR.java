package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.1WR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1WR implements Iterator, AbstractC16910px {
    public int A00 = -1;
    public Object A01;
    public final Iterator A02;
    public final /* synthetic */ AnonymousClass1WQ A03;

    @Override // java.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public AnonymousClass1WR(AnonymousClass1WQ r2) {
        this.A03 = r2;
        this.A02 = r2.A01.iterator();
    }

    public final void A00() {
        int i;
        while (true) {
            Iterator it = this.A02;
            if (!it.hasNext()) {
                i = 0;
                break;
            }
            Object next = it.next();
            AnonymousClass1WQ r2 = this.A03;
            if (((Boolean) r2.A00.AJ4(next)).booleanValue() == r2.A02) {
                this.A01 = next;
                i = 1;
                break;
            }
        }
        this.A00 = i;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        if (this.A00 == -1) {
            A00();
        }
        return this.A00 == 1;
    }

    @Override // java.util.Iterator
    public Object next() {
        if (this.A00 == -1) {
            A00();
        }
        if (this.A00 != 0) {
            Object obj = this.A01;
            this.A01 = null;
            this.A00 = -1;
            return obj;
        }
        throw new NoSuchElementException();
    }
}
