package X;

import com.whatsapp.polls.PollResultsViewModel;
import java.util.Comparator;

/* renamed from: X.5Ch  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112165Ch implements Comparator {
    public final /* synthetic */ PollResultsViewModel A00;

    public C112165Ch(PollResultsViewModel pollResultsViewModel) {
        this.A00 = pollResultsViewModel;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return AnonymousClass048.A00(((C27701Iu) obj2).A00, ((C27701Iu) obj).A00);
    }
}
