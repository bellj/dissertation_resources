package X;

import android.os.CountDownTimer;
import android.widget.ProgressBar;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;

/* renamed from: X.2Zt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51942Zt extends CountDownTimer {
    public final /* synthetic */ long A00;
    public final /* synthetic */ VerifyPhoneNumber A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51942Zt(VerifyPhoneNumber verifyPhoneNumber, long j, long j2) {
        super(j, 1000);
        this.A01 = verifyPhoneNumber;
        this.A00 = j2;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        VerifyPhoneNumber verifyPhoneNumber = this.A01;
        verifyPhoneNumber.A0C = null;
        if (verifyPhoneNumber.A0f.A03()) {
            verifyPhoneNumber.A0f.A02(true);
        } else {
            verifyPhoneNumber.A0M.setEnabled(true);
        }
        verifyPhoneNumber.A0I.setProgress(100);
        if (verifyPhoneNumber.A0f.A03()) {
            verifyPhoneNumber.A0I.setVisibility(8);
        }
        verifyPhoneNumber.A0K.setText(C12960it.A0X(verifyPhoneNumber, Integer.valueOf(VerifyPhoneNumber.A1D), new Object[1], 0, R.string.verify_description_bottom));
        verifyPhoneNumber.A11 = false;
        verifyPhoneNumber.A2q();
        String A2i = verifyPhoneNumber.A2i();
        if (A2i != null) {
            Log.i("verifyphonenumber/countdowntimer/done/try-savedcode");
            verifyPhoneNumber.A01 = 0;
            String str = verifyPhoneNumber.A0x;
            String str2 = verifyPhoneNumber.A0y;
            AnonymousClass009.A05(str2);
            verifyPhoneNumber.A3L(verifyPhoneNumber.A0q, AnonymousClass4AY.TAPPED_LINK, A2i, str, str2, "sms", null);
        }
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        ProgressBar progressBar = this.A01.A0I;
        long j2 = this.A00;
        progressBar.setProgress((int) ((((double) (j2 - j)) * 100.0d) / ((double) j2)));
    }
}
