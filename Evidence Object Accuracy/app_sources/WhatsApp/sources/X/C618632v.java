package X;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.whatsapp.R;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;

/* renamed from: X.32v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618632v extends AbstractC56692lQ {
    public float A00;
    public float A01;
    public float A02;
    public int A03 = 2;
    public long A04;
    public SensorManager A05 = this.A09.A0D();
    public Location A06;
    public Display A07 = this.A09.A0O().getDefaultDisplay();
    public C35961j4 A08;
    public AnonymousClass01d A09;
    public boolean A0A;
    public boolean A0B;
    public final SensorEventListener A0C = new AnonymousClass3LQ(this);
    public final AbstractC116405Vh A0D = new C1090350d(this);
    public final float[] A0E = new float[3];
    public final float[] A0F = new float[16];
    public final float[] A0G = new float[3];

    public C618632v(Context context, GoogleMapOptions googleMapOptions) {
        super(context, googleMapOptions);
        A06(new AbstractC115755Su() { // from class: X.50i
            @Override // X.AbstractC115755Su
            public final void ASP(C35961j4 r3) {
                C618632v r1 = C618632v.this;
                if (r1.A08 == null) {
                    r1.A08 = r3;
                    r1.setLocationMode(r1.A03);
                }
            }
        });
    }

    public static LatLng A00(LatLng latLng, double d) {
        double d2 = 50.0d / 6378137.0d;
        double radians = Math.toRadians(d);
        double radians2 = Math.toRadians(latLng.A00);
        double radians3 = Math.toRadians(latLng.A01);
        double cos = Math.cos(d2);
        double sin = Math.sin(d2);
        double sin2 = Math.sin(radians2);
        double cos2 = sin * Math.cos(radians2);
        double cos3 = (cos * sin2) + (Math.cos(radians) * cos2);
        return new LatLng(Math.toDegrees(Math.asin(cos3)), Math.toDegrees(radians3 + Math.atan2(cos2 * Math.sin(radians), cos - (sin2 * cos3))));
    }

    public C35961j4 A07(AbstractC115755Su r2) {
        AnonymousClass009.A01();
        C35961j4 r0 = this.A08;
        if (r0 != null) {
            r2.ASP(r0);
            return this.A08;
        }
        A06(r2);
        return null;
    }

    public void A08() {
        SensorManager sensorManager = this.A05;
        if (sensorManager != null) {
            Sensor defaultSensor = sensorManager.getDefaultSensor(11);
            this.A0B = C12960it.A1W(defaultSensor);
            if (defaultSensor != null) {
                sensorManager.registerListener(this.A0C, defaultSensor, 1);
            }
        }
    }

    public void A09() {
        int i = this.A03;
        if (i != 0) {
            if (i == 1) {
                setLocationMode(0);
                return;
            } else if (i != 2) {
                return;
            }
        }
        setLocationMode(1);
    }

    public void A0A(int i) {
        ImageView imageView;
        int i2;
        if (this instanceof C618232r) {
            C618232r r2 = (C618232r) this;
            int i3 = 8;
            if (i == 0) {
                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = r2.A00;
                AbstractView$OnCreateContextMenuListenerC35851ir r0 = groupChatLiveLocationsActivity2.A0M;
                r0.A0u = true;
                r0.A0s = true;
                View view = r0.A0U;
                if (r0.A0m == null) {
                    i3 = 0;
                }
                view.setVisibility(i3);
                imageView = groupChatLiveLocationsActivity2.A04;
                i2 = R.drawable.btn_compass_mode_tilt;
            } else if (i == 1) {
                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity22 = r2.A00;
                AbstractView$OnCreateContextMenuListenerC35851ir r02 = groupChatLiveLocationsActivity22.A0M;
                r02.A0u = true;
                r02.A0s = true;
                View view2 = r02.A0U;
                if (r02.A0m == null) {
                    i3 = 0;
                }
                view2.setVisibility(i3);
                imageView = groupChatLiveLocationsActivity22.A04;
                i2 = R.drawable.btn_myl_active;
            } else if (i == 2) {
                GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity23 = r2.A00;
                groupChatLiveLocationsActivity23.A04.setImageResource(R.drawable.btn_myl);
                groupChatLiveLocationsActivity23.A0M.A0s = false;
                return;
            } else {
                return;
            }
            imageView.setImageResource(i2);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.A03 != 2) {
            this.A03 = 2;
            A0A(2);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public int getLocationMode() {
        return this.A03;
    }

    public void setLocationMode(int i) {
        LatLng latLng;
        LatLng latLng2;
        int i2;
        C35961j4 r2 = this.A08;
        if (r2 != null) {
            CameraPosition A02 = r2.A02();
            if (i != 0) {
                if (i == 1) {
                    Location location = this.A06;
                    if (location != null) {
                        latLng2 = new LatLng(location.getLatitude(), this.A06.getLongitude());
                        this.A03 = 1;
                        i2 = 1;
                    } else {
                        latLng2 = A02.A03;
                        this.A03 = 2;
                        i2 = 2;
                    }
                    A0A(i2);
                    AnonymousClass3EO r1 = new AnonymousClass3EO();
                    C13020j0.A02(latLng2, "location must not be null.");
                    r1.A03 = latLng2;
                    r1.A00 = A02.A02;
                    r1.A01 = 0.0f;
                    r1.A02 = 0.0f;
                    r2.A09(C65193Io.A00(r1.A00()));
                    return;
                } else if (i == 2) {
                    this.A03 = 2;
                    A0A(2);
                    return;
                } else {
                    return;
                }
            } else if (this.A0B) {
                this.A01 = A02.A02;
                i = 0;
                A0A(0);
                Location location2 = this.A06;
                if (location2 != null) {
                    latLng = new LatLng(location2.getLatitude(), this.A06.getLongitude());
                } else {
                    latLng = A02.A03;
                }
                float f = this.A00;
                LatLng A00 = A00(latLng, (double) f);
                AnonymousClass3EO r4 = new AnonymousClass3EO();
                r4.A01 = Math.max(Math.min(this.A02, 67.5f), 0.0f);
                r4.A02 = f;
                r4.A00 = Math.max(this.A01, 15.0f);
                C13020j0.A02(A00, "location must not be null.");
                r4.A03 = A00;
                CameraPosition A002 = r4.A00();
                this.A0A = true;
                r2.A0B(C65193Io.A00(A002), this.A0D);
            } else {
                return;
            }
        }
        this.A03 = i;
    }

    public void setMyLocation(Location location) {
        this.A06 = location;
    }
}
