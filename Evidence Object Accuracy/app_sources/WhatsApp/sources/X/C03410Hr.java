package X;

import android.graphics.Matrix;

/* renamed from: X.0Hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03410Hr extends AbstractC03420Hs implements AbstractC11530gR {
    public Matrix A00;
    public C08910c3 A01;
    public C08910c3 A02;
    public C08910c3 A03;
    public C08910c3 A04;
    public Boolean A05;
    public Boolean A06;
    public String A07;

    @Override // X.AnonymousClass0OO
    public String A00() {
        return "pattern";
    }
}
