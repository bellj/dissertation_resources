package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2gz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54602gz extends AbstractC018308n {
    public final /* synthetic */ C63443Bp A00;

    public C54602gz(C63443Bp r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r9, RecyclerView recyclerView) {
        C63443Bp r5 = this.A00;
        if (r5.A00 != 0) {
            int A00 = RecyclerView.A00(view);
            int i = r5.A00;
            int i2 = A00 % i;
            int i3 = (r5.A04 - (r5.A01 * i)) / (i + 1);
            rect.left = i3 - ((i2 * i3) / i);
            rect.right = ((i2 + 1) * i3) / i;
            if (A00 < i) {
                rect.top = r5.A02;
            }
            rect.bottom = r5.A02;
        }
    }
}
