package X;

/* renamed from: X.5t4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126555t4 {
    public final AnonymousClass1V8 A00;

    public C126555t4(AnonymousClass3CS r6, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy.A01(A0M, "xmlns", "w:pay");
        C41141sy A0O = C117295Zj.A0O(A0M);
        C41141sy.A01(A0O, "action", "upi-sign-qr-code");
        if (C117305Zk.A1X(str, 1, false)) {
            C41141sy.A01(A0O, "qr-code", str);
        }
        this.A00 = C117295Zj.A0I(A0O, A0M, r6);
    }
}
