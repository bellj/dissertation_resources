package X;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import androidx.work.impl.background.systemjob.SystemJobService;

/* renamed from: X.0Tc  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Tc {
    public static final String A01 = C06390Tk.A01("SystemJobInfoConverter");
    public final ComponentName A00;

    public AnonymousClass0Tc(Context context) {
        this.A00 = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    public static JobInfo.TriggerContentUri A00(AnonymousClass0P9 r3) {
        return new JobInfo.TriggerContentUri(r3.A00, r3.A01 ? 1 : 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ca, code lost:
        if (r4 >= 26) goto L_0x00cc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.job.JobInfo A01(X.C004401z r11, int r12) {
        /*
        // Method dump skipped, instructions count: 289
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0Tc.A01(X.01z, int):android.app.job.JobInfo");
    }
}
