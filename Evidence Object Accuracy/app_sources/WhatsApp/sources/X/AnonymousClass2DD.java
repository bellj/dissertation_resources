package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.2DD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DD extends AbstractC28131Kt {
    public final int A00;
    public final List A01;
    public final Map A02;
    public final Map A03;

    public AnonymousClass2DD(C34021fS r1, String str, List list, Map map, Map map2, int i) {
        super(r1, str);
        this.A01 = list;
        this.A00 = i;
        this.A02 = map;
        this.A03 = map2;
    }
}
