package X;

import android.net.Uri;
import com.whatsapp.contact.picker.ContactPickerFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.38v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C628538v extends AbstractC16350or {
    public final Uri A00;
    public final C253318z A01;
    public final C32701cb A02;
    public final C17220qS A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final WeakReference A07;
    public final boolean A08;

    public C628538v(Uri uri, ContactPickerFragment contactPickerFragment, C253318z r4, C32701cb r5, C17220qS r6, String str, String str2, String str3, boolean z) {
        this.A07 = C12970iu.A10(contactPickerFragment);
        this.A06 = str;
        this.A08 = z;
        this.A02 = r5;
        this.A03 = r6;
        this.A01 = r4;
        this.A05 = str2;
        this.A04 = str3;
        this.A00 = uri;
    }
}
