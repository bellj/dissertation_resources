package X;

/* renamed from: X.3y6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C83913y6 extends AbstractC87954Dr {
    public final boolean A00;
    public final boolean A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C83913y6) {
                C83913y6 r5 = (C83913y6) obj;
                if (!(this.A01 == r5.A01 && this.A00 == r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        boolean z = this.A01;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        if (!this.A00) {
            i = 0;
        }
        return i5 + i;
    }

    public C83913y6() {
        this(false, false);
    }

    public C83913y6(boolean z, boolean z2) {
        this.A01 = z;
        this.A00 = z2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AvatarDetails(isDeleting=");
        A0k.append(this.A01);
        A0k.append(", deleteError=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
