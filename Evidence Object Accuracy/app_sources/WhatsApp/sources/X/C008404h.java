package X;

import android.content.Context;
import java.util.Arrays;

/* renamed from: X.04h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008404h extends AnonymousClass030 {
    public boolean A00 = true;
    public final AnonymousClass0SQ A01;
    public final long[] A02;
    public final long[] A03;

    public C008404h(Context context) {
        this.A01 = AnonymousClass0SQ.A00(context);
        this.A02 = new long[8];
        this.A03 = new long[8];
    }

    public static void A00(C008304g r4, long[] jArr, int i) {
        r4.mobileBytesTx += jArr[i | 3];
        r4.mobileBytesRx += jArr[i | 2];
        r4.wifiBytesTx += jArr[i | 1];
        r4.wifiBytesRx += jArr[i | 0];
    }

    @Override // X.AnonymousClass030
    public AnonymousClass032 A01() {
        return new C008304g();
    }

    @Override // X.AnonymousClass030
    public /* bridge */ /* synthetic */ boolean A02(AnonymousClass032 r12) {
        boolean z;
        C008304g r122 = (C008304g) r12;
        synchronized (this) {
            if (this.A00) {
                AnonymousClass0SQ r9 = this.A01;
                long[] jArr = this.A02;
                if (r9.A02(jArr)) {
                    long[] jArr2 = this.A03;
                    int i = 0;
                    while (true) {
                        int length = jArr.length;
                        if (i >= length) {
                            System.arraycopy(jArr, 0, jArr2, 0, length);
                            z = true;
                            break;
                        } else if (jArr[i] < jArr2[i]) {
                            Arrays.toString(jArr2);
                            Arrays.toString(jArr);
                            z = false;
                            break;
                        } else {
                            i++;
                        }
                    }
                    this.A00 = z;
                    if (z) {
                        boolean A01 = r9.A01();
                        r122.mobileBytesTx = 0;
                        r122.mobileBytesRx = 0;
                        r122.wifiBytesTx = 0;
                        r122.wifiBytesRx = 0;
                        A00(r122, jArr, 0);
                        if (A01) {
                            A00(r122, jArr, 4);
                        }
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
