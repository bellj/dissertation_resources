package X;

/* renamed from: X.39w  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass39w {
    A02(0),
    A04(1),
    A03(2),
    A01(3);
    
    public final int mIntValue;

    AnonymousClass39w(int i) {
        this.mIntValue = i;
    }

    public static AnonymousClass39w A00(int i) {
        if (i == 0) {
            return A02;
        }
        if (i == 1) {
            return A04;
        }
        if (i == 2) {
            return A03;
        }
        if (i == 3) {
            return A01;
        }
        throw C12970iu.A0f(C12960it.A0W(i, "Unknown enum value: "));
    }
}
