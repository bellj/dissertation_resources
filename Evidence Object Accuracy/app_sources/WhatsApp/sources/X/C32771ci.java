package X;

import java.util.Arrays;

/* renamed from: X.1ci  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32771ci {
    public final String A00;
    public final byte[] A01;

    public C32771ci(String str, byte[] bArr) {
        this.A00 = str;
        this.A01 = bArr;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C32771ci.class != obj.getClass()) {
                return false;
            }
            C32771ci r5 = (C32771ci) obj;
            if (!Arrays.equals(this.A01, r5.A01) || !C29941Vi.A00(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int hashCode = (Arrays.hashCode(this.A01) + 31) * 31;
        String str = this.A00;
        return hashCode + (str == null ? 0 : str.hashCode());
    }
}
