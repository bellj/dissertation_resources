package X;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.res.Configuration;

/* renamed from: X.0V2  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0V2 implements ComponentCallbacks {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AnonymousClass0ZY A01;

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
    }

    public AnonymousClass0V2(Activity activity, AnonymousClass0ZY r2) {
        this.A01 = r2;
        this.A00 = activity;
    }

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        AnonymousClass0ZY r0 = this.A01;
        AbstractC11940h7 r2 = r0.A00;
        if (r2 != null) {
            Activity activity = this.A00;
            r2.AYW(activity, r0.A00(activity));
        }
    }
}
