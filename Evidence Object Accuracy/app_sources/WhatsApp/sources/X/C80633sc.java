package X;

import android.view.View;
import com.whatsapp.emoji.EmojiEditTextBottomSheetDialogFragment;

/* renamed from: X.3sc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80633sc extends AnonymousClass2UH {
    public final /* synthetic */ EmojiEditTextBottomSheetDialogFragment A00;

    @Override // X.AnonymousClass2UH
    public void A00(View view, float f) {
    }

    public C80633sc(EmojiEditTextBottomSheetDialogFragment emojiEditTextBottomSheetDialogFragment) {
        this.A00 = emojiEditTextBottomSheetDialogFragment;
    }

    @Override // X.AnonymousClass2UH
    public void A01(View view, int i) {
        if (i == 4 || i == 5) {
            this.A00.A1B();
        }
    }
}
