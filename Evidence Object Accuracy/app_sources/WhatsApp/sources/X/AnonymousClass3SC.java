package X;

import android.graphics.Point;
import com.whatsapp.location.GroupChatLiveLocationsActivity;

/* renamed from: X.3SC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SC implements AbstractC12500i1 {
    public final /* synthetic */ GroupChatLiveLocationsActivity A00;

    public AnonymousClass3SC(GroupChatLiveLocationsActivity groupChatLiveLocationsActivity) {
        this.A00 = groupChatLiveLocationsActivity;
    }

    @Override // X.AbstractC12500i1
    public void ANe() {
        this.A00.A0W = false;
    }

    @Override // X.AbstractC12500i1
    public void AQZ() {
        GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = this.A00;
        groupChatLiveLocationsActivity.A0W = false;
        AnonymousClass04Q r2 = groupChatLiveLocationsActivity.A05;
        AnonymousClass009.A05(r2);
        AbstractView$OnCreateContextMenuListenerC35851ir r1 = groupChatLiveLocationsActivity.A0L;
        C30751Yr r0 = r1.A0o;
        if (r0 != null) {
            AnonymousClass03T r4 = new AnonymousClass03T(r0.A00, r0.A01);
            Point A04 = r2.A0R.A04(r4);
            int i = A04.x;
            if (i <= 0 || A04.y <= 0 || i >= groupChatLiveLocationsActivity.A0K.getWidth() || A04.y >= groupChatLiveLocationsActivity.A0K.getHeight()) {
                groupChatLiveLocationsActivity.A0W = true;
                groupChatLiveLocationsActivity.A05.A0B(AnonymousClass0R9.A01(r4, groupChatLiveLocationsActivity.A00 * 2.0f), this, 1500);
            }
        } else if (!r1.A0u && groupChatLiveLocationsActivity.A0X) {
            groupChatLiveLocationsActivity.A0X = false;
            groupChatLiveLocationsActivity.A2i(true);
        }
    }
}
