package X;

import android.content.SharedPreferences;

/* renamed from: X.0qC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17060qC {
    public final C16120oU A00;
    public final C16630pM A01;
    public final C21710xr A02;

    public C17060qC(C16120oU r1, C16630pM r2, C21710xr r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(String str) {
        SharedPreferences A01 = this.A01.A01("ntp-scheduler");
        synchronized (this) {
            A01.edit().putInt(str, A01.getInt(str, 0) + 1).apply();
        }
    }
}
