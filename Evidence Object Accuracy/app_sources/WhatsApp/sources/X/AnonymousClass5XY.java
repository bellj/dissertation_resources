package X;

import java.nio.ByteBuffer;

/* renamed from: X.5XY  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5XY {
    void A7W(C100614mC v, int[] iArr, int i);

    long ACA(boolean z);

    int AD7(C100614mC v);

    boolean AHx(ByteBuffer byteBuffer, int i, long j);

    boolean AIH();

    void AcX(C94344be v);

    void flush();

    void reset();
}
