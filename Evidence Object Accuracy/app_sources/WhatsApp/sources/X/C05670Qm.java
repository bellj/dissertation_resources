package X;

import android.os.Build;
import android.view.ViewGroup;

/* renamed from: X.0Qm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05670Qm {
    public static int A00(ViewGroup.MarginLayoutParams marginLayoutParams) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass0T4.A00(marginLayoutParams);
        }
        return marginLayoutParams.rightMargin;
    }

    public static int A01(ViewGroup.MarginLayoutParams marginLayoutParams) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass0T4.A01(marginLayoutParams);
        }
        return marginLayoutParams.leftMargin;
    }
}
