package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2EB  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2EB {
    public final byte A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final Jid A04;
    public final Jid A05;
    public final UserJid A06;
    public final C29211Rh A07;
    public final C29211Rh A08;
    public final String A09;
    public final String A0A;
    public final byte[] A0B;
    public final byte[] A0C;

    public AnonymousClass2EB(Jid jid, Jid jid2, UserJid userJid, C29211Rh r4, C29211Rh r5, String str, String str2, byte[] bArr, byte[] bArr2, byte b, int i, long j, long j2) {
        this.A04 = jid;
        this.A0A = str;
        this.A05 = jid2;
        this.A06 = userJid;
        this.A03 = j;
        this.A01 = i;
        this.A0C = bArr;
        this.A0B = bArr2;
        this.A00 = b;
        this.A08 = r4;
        this.A07 = r5;
        this.A02 = j2;
        this.A09 = str2;
    }
}
