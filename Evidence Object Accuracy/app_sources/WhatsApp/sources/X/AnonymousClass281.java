package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.281  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass281 {
    public final UserJid A00;
    public final String A01;
    public final String A02;
    public final String A03;

    public AnonymousClass281(UserJid userJid, String str, String str2, String str3) {
        this.A01 = str;
        this.A02 = str2;
        this.A03 = str3;
        this.A00 = userJid;
    }
}
