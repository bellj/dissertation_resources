package X;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.lang.ref.Reference;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0zF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22540zF {
    public final C14330lG A00;
    public final C22590zK A01;
    public final AbstractC14440lR A02;
    public final ConcurrentHashMap A03 = new ConcurrentHashMap();

    public C22540zF(C14330lG r2, C22590zK r3, AbstractC14440lR r4) {
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r3;
    }

    public void A00(ImageView imageView, C30921Zi r11, int i, int i2) {
        Drawable drawable;
        StringBuilder sb = new StringBuilder("PaymentBackgroundMetadata{id='");
        sb.append(r11.A0F);
        sb.append("',fileSize=");
        sb.append(r11.A0E);
        sb.append(",width=");
        sb.append(r11.A0D);
        sb.append(",height=");
        sb.append(r11.A09);
        sb.append(",mimetype='");
        sb.append(r11.A0G);
        sb.append("'}_");
        sb.append(i);
        sb.append("_");
        sb.append(i2);
        String obj = sb.toString();
        imageView.setTag(obj);
        Drawable drawable2 = imageView.getDrawable();
        Reference reference = (Reference) this.A03.get(obj);
        if (reference != null) {
            drawable = (Drawable) reference.get();
        } else {
            drawable = null;
        }
        if (drawable2 != null && drawable2.equals(drawable)) {
            return;
        }
        if (drawable != null) {
            imageView.setImageDrawable(drawable);
        } else {
            this.A02.Aaz(new C626137x(imageView, r11, this, obj, i, i2), new Void[0]);
        }
    }
}
