package X;

import android.os.IBinder;
import com.google.android.gms.maps.internal.IUiSettingsDelegate;

/* renamed from: X.3rH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79833rH extends C65873Li implements IUiSettingsDelegate {
    public C79833rH(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }
}
