package X;

/* renamed from: X.3Gi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64643Gi {
    public final AnonymousClass4QJ A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public C64643Gi(AnonymousClass4QJ r1, String str, String str2, String str3, boolean z) {
        this.A01 = str;
        this.A03 = str2;
        this.A02 = str3;
        this.A04 = z;
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009d, code lost:
        if (r5 != null) goto L_0x001b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00e3 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C64643Gi A00(android.content.Context r8, X.AnonymousClass1BK r9, X.AbstractC15340mz r10, int r11) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64643Gi.A00(android.content.Context, X.1BK, X.0mz, int):X.3Gi");
    }
}
