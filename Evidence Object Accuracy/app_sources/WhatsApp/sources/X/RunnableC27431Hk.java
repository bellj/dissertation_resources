package X;

import android.content.Context;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.1Hk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC27431Hk implements Runnable, Future {
    public final C16590pI A00;
    public final /* synthetic */ C21750xv A01;

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        return false;
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        return false;
    }

    public RunnableC27431Hk(C16590pI r1, C21750xv r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Future
    public Object get() {
        return null;
    }

    @Override // java.util.concurrent.Future
    public /* bridge */ /* synthetic */ Object get(long j, TimeUnit timeUnit) {
        C21750xv r1 = this.A01;
        if (r1.A02.await(j, timeUnit)) {
            return r1.A03.get();
        }
        throw new TimeoutException();
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        return this.A01.A02.getCount() == 0;
    }

    @Override // java.lang.Runnable
    public void run() {
        Set set;
        Context context = this.A00.A00;
        Set A00 = C21750xv.A00(context, "primary-task-killer", C21750xv.A04);
        Set A002 = C21750xv.A00(context, "secondary-task-killer", C21750xv.A05);
        C21750xv r4 = this.A01;
        AtomicReference atomicReference = r4.A03;
        Set set2 = null;
        if (A00 != null) {
            set = Collections.unmodifiableSet(A00);
        } else {
            set = null;
        }
        if (A002 != null) {
            set2 = Collections.unmodifiableSet(A002);
        }
        atomicReference.set(new C457022s(set, set2));
        r4.A02.countDown();
    }
}
