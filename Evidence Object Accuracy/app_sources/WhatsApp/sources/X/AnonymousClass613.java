package X;

import android.content.Context;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.613  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass613 {
    public static CharSequence A00(Context context, AnonymousClass018 r4, AnonymousClass1IR r5) {
        return A01(context, r4, new AnonymousClass6F2(r5.A00(), r5.A08));
    }

    public static CharSequence A01(Context context, AnonymousClass018 r4, AnonymousClass6F2 r5) {
        return C117305Zk.A0e(context, r4, r5.A00, r5.A01, 0);
    }

    public static List A02(AnonymousClass102 r29, UserJid userJid, List list) {
        ArrayList A0l = C12960it.A0l();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1IR A09 = C117315Zl.A09(it);
            A0l.add(A09);
            if (A09.A01 == 3 && A09.A0G() && !A09.A0H()) {
                AbstractC30891Zf r0 = A09.A0A;
                AnonymousClass009.A05(r0);
                AbstractC1316063k r02 = ((C119825fA) r0).A01;
                AnonymousClass009.A05(r02);
                C121035h9 r2 = ((C120995h5) r02).A00;
                if (r2 != null) {
                    C1315063a r4 = r2.A00;
                    if (r4.A00 == 405) {
                        AbstractC30791Yv A02 = r29.A02(r2.A03());
                        String A03 = r2.A03();
                        C30821Yy r10 = r4.A03.A02.A01;
                        String str = r2.A05;
                        long j = A09.A05;
                        long j2 = ((AbstractC1316063k) r2).A01;
                        String str2 = A09.A0G;
                        AnonymousClass1IR r8 = new AnonymousClass1IR(A02, r10, userJid, null, A03, str, null, null, null, null, str2, 6, 405, AnonymousClass20N.A00(str2), 3, 0, j, j2);
                        C119825fA r6 = new C119825fA();
                        r6.A01 = r2;
                        r8.A08 = r2.A02();
                        r8.A0I = A03;
                        r8.A06 = ((AbstractC1316063k) r2).A00 / 1000;
                        r8.A0A = r6;
                        A0l.add(r8);
                    }
                }
            }
        }
        return A0l;
    }

    public static List A03(List list) {
        ArrayList A0l = C12960it.A0l();
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                AnonymousClass1IR A09 = C117315Zl.A09(it);
                AbstractC30891Zf r0 = A09.A0A;
                AnonymousClass009.A05(r0);
                AbstractC1316063k r1 = ((C119825fA) r0).A01;
                if (r1 instanceof AbstractC121025h8) {
                    AbstractC121025h8 r12 = (AbstractC121025h8) r1;
                    if (r12.A03 == null && r12.A05 == null) {
                    }
                }
                A0l.add(A09);
            }
        }
        return A0l;
    }
}
