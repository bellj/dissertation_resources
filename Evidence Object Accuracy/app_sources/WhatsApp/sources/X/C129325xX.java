package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.5xX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129325xX {
    public AbstractC30791Yv A00;
    public C30821Yy A01;
    public UserJid A02;
    public C1315463e A03;
    public AnonymousClass6F2 A04;
    public AnonymousClass6F2 A05;
    public AnonymousClass6F2 A06;
    public AnonymousClass6F2 A07;
    public C1315863i A08;
    public AnonymousClass63X A09;
    public String A0A;
    public boolean A0B;

    public C128095vY A00() {
        C1315463e r14 = this.A03;
        AnonymousClass009.A05(r14);
        C1315863i r15 = this.A08;
        AnonymousClass009.A05(r15);
        UserJid userJid = this.A02;
        AnonymousClass009.A05(userJid);
        C30821Yy r5 = this.A01;
        AnonymousClass009.A05(r5);
        AbstractC30791Yv r4 = this.A00;
        AnonymousClass009.A05(r4);
        AnonymousClass6F2 r8 = this.A07;
        AnonymousClass009.A05(r8);
        AnonymousClass6F2 r7 = this.A05;
        AnonymousClass009.A05(r7);
        C1315263c r11 = new C1315263c(new C1316363n(this.A06, r8, r15.A03.A01));
        AnonymousClass63Z r9 = new AnonymousClass63Z(userJid, new C1316363n(this.A04, r7, r15.A02.A01), null);
        C126845tX r2 = new C126845tX(new AnonymousClass6F2(r4, r5), this.A0B);
        return new C128095vY(userJid, r14, r15, new C1315163b(null, null, r15.A05, r9, null, r11, ""), this.A09, r2, this.A0A);
    }
}
