package X;

import android.util.SparseArray;

/* renamed from: X.5rR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125555rR {
    public final AnonymousClass28D A00;

    public C125555rR(AnonymousClass28D r5) {
        AnonymousClass28D r2 = new AnonymousClass28D(13535);
        this.A00 = r2;
        AnonymousClass28D r0 = (AnonymousClass28D) r5.A02.get(35);
        if (r0 != null) {
            AnonymousClass3JI A01 = AnonymousClass3JI.A01(r0);
            SparseArray sparseArray = r2.A02;
            sparseArray.put(35, A01);
            sparseArray.put(38, r5.A0G(38));
            sparseArray.put(36, r5.A0J(36, ""));
            sparseArray.put(46, r5.A0J(46, ""));
            sparseArray.put(48, r5.A0J(48, ""));
            sparseArray.put(45, r5.A0G(45));
            return;
        }
        throw C12960it.A0U("Null content for BottomSheet");
    }
}
