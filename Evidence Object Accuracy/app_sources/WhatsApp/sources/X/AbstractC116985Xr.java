package X;

/* renamed from: X.5Xr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116985Xr {
    public static final AnonymousClass1TK A00;
    public static final AnonymousClass1TK A01;
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03;
    public static final AnonymousClass1TK A04;
    public static final AnonymousClass1TK A05;
    public static final AnonymousClass1TK A06;
    public static final AnonymousClass1TK A07;
    public static final AnonymousClass1TK A08;
    public static final AnonymousClass1TK A09;
    public static final AnonymousClass1TK A0A;
    public static final AnonymousClass1TK A0B;
    public static final AnonymousClass1TK A0C;
    public static final AnonymousClass1TK A0D;
    public static final AnonymousClass1TK A0E;
    public static final AnonymousClass1TK A0F;
    public static final AnonymousClass1TK A0G;
    public static final AnonymousClass1TK A0H;
    public static final AnonymousClass1TK A0I;
    public static final AnonymousClass1TK A0J;
    public static final AnonymousClass1TK A0K;
    public static final AnonymousClass1TK A0L;
    public static final AnonymousClass1TK A0M;
    public static final AnonymousClass1TK A0N;
    public static final AnonymousClass1TK A0O;
    public static final AnonymousClass1TK A0P;
    public static final AnonymousClass1TK A0Q;
    public static final AnonymousClass1TK A0R;
    public static final AnonymousClass1TK A0S;
    public static final AnonymousClass1TK A0T;
    public static final AnonymousClass1TK A0U;
    public static final AnonymousClass1TK A0V;

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.2.643.2.2");
        A00 = A12;
        A0I = AnonymousClass1TL.A02("9", A12);
        A0J = AnonymousClass1TL.A02("10", A12);
        A0V = AnonymousClass1TL.A02("13.0", A12);
        A0T = AnonymousClass1TL.A02("13.1", A12);
        A01 = AnonymousClass1TL.A02("21", A12);
        A0U = AnonymousClass1TL.A02("31.0", A12);
        A0P = AnonymousClass1TL.A02("31.1", A12);
        A0Q = AnonymousClass1TL.A02("31.2", A12);
        A0R = AnonymousClass1TL.A02("31.3", A12);
        A0S = AnonymousClass1TL.A02("31.4", A12);
        A0A = AnonymousClass1TL.A02("20", A12);
        A02 = AnonymousClass1TL.A02("19", A12);
        A0M = AnonymousClass1TL.A02("4", A12);
        A0L = AnonymousClass1TL.A02("3", A12);
        A0K = AnonymousClass1TL.A02("30.1", A12);
        A0B = AnonymousClass1TL.A02("32.2", A12);
        A0C = AnonymousClass1TL.A02("32.3", A12);
        A0D = AnonymousClass1TL.A02("32.4", A12);
        A0E = AnonymousClass1TL.A02("32.5", A12);
        A0F = AnonymousClass1TL.A02("33.1", A12);
        A0G = AnonymousClass1TL.A02("33.2", A12);
        A0H = AnonymousClass1TL.A02("33.3", A12);
        A04 = AnonymousClass1TL.A02("35.1", A12);
        A05 = AnonymousClass1TL.A02("35.2", A12);
        A06 = AnonymousClass1TL.A02("35.3", A12);
        A08 = AnonymousClass1TL.A02("36.0", A12);
        A09 = AnonymousClass1TL.A02("36.1", A12);
        A0O = AnonymousClass1TL.A02("36.0", A12);
        A0N = AnonymousClass1TL.A02("36.1", A12);
        A07 = AnonymousClass1TL.A02("96", A12);
        A03 = AnonymousClass1TL.A02("98", A12);
    }
}
