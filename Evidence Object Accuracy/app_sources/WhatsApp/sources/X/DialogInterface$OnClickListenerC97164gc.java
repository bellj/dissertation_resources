package X;

import android.content.DialogInterface;
import com.whatsapp.documentpicker.DocumentPickerActivity;

/* renamed from: X.4gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class DialogInterface$OnClickListenerC97164gc implements DialogInterface.OnClickListener {
    public final /* synthetic */ DocumentPickerActivity.SendDocumentsConfirmationDialogFragment A00;
    public final /* synthetic */ boolean A01;

    public /* synthetic */ DialogInterface$OnClickListenerC97164gc(DocumentPickerActivity.SendDocumentsConfirmationDialogFragment sendDocumentsConfirmationDialogFragment, boolean z) {
        this.A00 = sendDocumentsConfirmationDialogFragment;
        this.A01 = z;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        ActivityC000900k A0B;
        DocumentPickerActivity.SendDocumentsConfirmationDialogFragment sendDocumentsConfirmationDialogFragment = this.A00;
        if (this.A01 && (A0B = sendDocumentsConfirmationDialogFragment.A0B()) != null) {
            A0B.finish();
        }
    }
}
