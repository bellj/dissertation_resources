package X;

/* renamed from: X.3t8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80893t8<E> extends AbstractC80943tD<E> {
    public static final long serialVersionUID = 0;

    public C80893t8(int i) {
        super(3);
    }

    public static C80893t8 create() {
        return create(3);
    }

    public static C80893t8 create(int i) {
        return new C80893t8(3);
    }

    @Override // X.AbstractC80943tD
    public C95614e4 newBackingMap(int i) {
        return new C95614e4(3);
    }
}
