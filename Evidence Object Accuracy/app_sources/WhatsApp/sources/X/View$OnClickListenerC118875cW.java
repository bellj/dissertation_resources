package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankAccountPickerActivity;

/* renamed from: X.5cW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class View$OnClickListenerC118875cW extends AnonymousClass03U implements View.OnClickListener {
    public final ImageView A00;
    public final RadioButton A01;
    public final TextView A02;
    public final TextView A03;
    public final TextView A04;
    public final C125835rt A05;

    public View$OnClickListenerC118875cW(View view, C125835rt r3) {
        super(view);
        this.A00 = C12970iu.A0K(view, R.id.provider_icon);
        this.A03 = C12960it.A0I(view, R.id.account_number);
        this.A02 = C12960it.A0I(view, R.id.account_name);
        this.A04 = C12960it.A0I(view, R.id.account_type);
        this.A01 = (RadioButton) AnonymousClass028.A0D(view, R.id.radio_button);
        this.A05 = r3;
        view.setOnClickListener(this);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        C125835rt r1 = this.A05;
        int i = this.A06;
        if (i == -1) {
            i = super.A05;
        }
        IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity = r1.A00;
        if (!indiaUpiBankAccountPickerActivity.A0V && (!((C128025vR) indiaUpiBankAccountPickerActivity.A0T.get(i)).A05)) {
            if (indiaUpiBankAccountPickerActivity.A0T.size() == 1) {
                indiaUpiBankAccountPickerActivity.A31();
                return;
            }
            ((C128025vR) indiaUpiBankAccountPickerActivity.A0T.get(indiaUpiBankAccountPickerActivity.A01)).A00 = false;
            ((C128025vR) indiaUpiBankAccountPickerActivity.A0T.get(i)).A00 = true;
            AnonymousClass02M r12 = indiaUpiBankAccountPickerActivity.A0B.A0N;
            if (r12 != null) {
                r12.A03(indiaUpiBankAccountPickerActivity.A01);
                indiaUpiBankAccountPickerActivity.A01 = i;
                indiaUpiBankAccountPickerActivity.A0B.A0N.A03(i);
            }
        }
    }
}
