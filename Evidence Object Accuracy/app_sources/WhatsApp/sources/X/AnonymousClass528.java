package X;

/* renamed from: X.528  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass528 implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r5, AbstractC94534c0 r6, AnonymousClass4RG r7) {
        C82993wW r2;
        C82973wU r0;
        String str;
        boolean z = r5 instanceof C82993wW;
        boolean A00 = C72473ef.A00(z ? 1 : 0);
        boolean z2 = r6 instanceof C82993wW;
        if (!(C72473ef.A00(z2 ? 1 : 0) ^ A00)) {
            return false;
        }
        if (A00) {
            if (!z) {
                throw C82843wH.A00("Expected regexp node");
            }
            r2 = (C82993wW) r5;
            if ((r6 instanceof C82973wU) || (r6 instanceof C83013wY)) {
                r0 = r6.A06();
                str = r0.A01;
            } else {
                if (r6 instanceof C82953wS) {
                    str = ((C82953wS) r6).toString();
                }
                str = "";
            }
        } else if (!z2) {
            throw C82843wH.A00("Expected regexp node");
        } else {
            r2 = (C82993wW) r6;
            if ((r5 instanceof C82973wU) || (r5 instanceof C83013wY)) {
                r0 = r5.A06();
                str = r0.A01;
            } else {
                if (r5 instanceof C82953wS) {
                    str = ((C82953wS) r5).toString();
                }
                str = "";
            }
        }
        return r2.A02.matcher(str).matches();
    }
}
