package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteFullException;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.132  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass132 {
    public final C14830m7 A00;
    public final C16370ot A01;
    public final C16510p9 A02;
    public final C21620xi A03;
    public final AnonymousClass12I A04;
    public final C20850wQ A05;
    public final C16490p7 A06;
    public final AnonymousClass134 A07;
    public final Map A08;
    public final AtomicBoolean A09;

    public AnonymousClass132(C14830m7 r2, C16370ot r3, C16510p9 r4, C21620xi r5, AnonymousClass12I r6, C20850wQ r7, C16490p7 r8, AnonymousClass134 r9) {
        this.A00 = r2;
        this.A02 = r4;
        this.A07 = r9;
        this.A01 = r3;
        this.A04 = r6;
        this.A03 = r5;
        this.A06 = r8;
        this.A05 = r7;
        this.A08 = r5.A02;
        this.A09 = r5.A03;
    }

    public int A00(AbstractC14640lm r4) {
        int i = 0;
        if (r4 != null) {
            Iterator it = A01().iterator();
            while (it.hasNext()) {
                if (r4.equals(((AbstractC15340mz) it.next()).A0z.A00)) {
                    i++;
                }
            }
        }
        return i;
    }

    public ArrayList A01() {
        long A00 = this.A00.A00();
        if (!this.A09.get()) {
            A02();
        }
        Map map = this.A03.A02;
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            if (((AbstractC15340mz) ((Map.Entry) it.next()).getValue()).A0I + 86400000 < A00) {
                it.remove();
            }
        }
        StringBuilder sb = new StringBuilder("msgstore/unsendmessages/cached:");
        sb.append(map.size());
        Log.i(sb.toString());
        ArrayList arrayList = new ArrayList(map.size());
        for (Object obj : map.values()) {
            arrayList.add(obj);
        }
        Collections.sort(arrayList, new C45041zy());
        return arrayList;
    }

    public final void A02() {
        C16310on A01;
        int i;
        AtomicBoolean atomicBoolean = this.A09;
        synchronized (atomicBoolean) {
            if (!atomicBoolean.get()) {
                ArrayList arrayList = new ArrayList();
                C28181Ma r7 = new C28181Ma(false);
                r7.A04("unsentmsgstore/unsendmessages");
                long A04 = this.A07.A04(this.A00.A00() - 86400000);
                try {
                    try {
                        try {
                            A01 = this.A06.get();
                        } catch (SQLiteDatabaseCorruptException e) {
                            Log.e(e);
                            this.A05.A02();
                        }
                    } catch (IllegalStateException e2) {
                        Log.i("unsentmsgstore/unsent/IllegalStateException ", e2);
                    }
                    try {
                        Cursor A09 = A01.A03.A09(C32301bw.A0O, new String[]{String.valueOf(A04)});
                        if (A09 == null) {
                            A01.close();
                        } else {
                            int columnIndexOrThrow = A09.getColumnIndexOrThrow("chat_row_id");
                            while (A09.moveToNext()) {
                                AbstractC14640lm A05 = this.A02.A05((long) A09.getInt(columnIndexOrThrow));
                                if (A05 == null) {
                                    Log.w("unsentmsgstore/unsent/jid is null!");
                                } else {
                                    AbstractC15340mz A02 = this.A01.A02(A09, A05, false, true);
                                    if (A02 == null) {
                                        Log.w("unsentmsgstore/unsent/can't read message from cursor.");
                                    } else {
                                        byte b = A02.A0y;
                                        if (!(b == 8 || b == 10 || b == 7 || ((i = A02.A0C) == 7 && C15380n4.A0J(A02.A0z.A00)))) {
                                            if (!A02.A0r || C15380n4.A0F(A05)) {
                                                StringBuilder sb = new StringBuilder();
                                                sb.append("unsentmsgstore/unsent/add key=");
                                                sb.append(A02.A0z.A01);
                                                sb.append(" type=");
                                                sb.append((int) b);
                                                sb.append(" status=");
                                                sb.append(i);
                                                Log.i(sb.toString());
                                                arrayList.add(A02);
                                            }
                                        }
                                    }
                                }
                            }
                            A09.close();
                            A01.close();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("unsentmsgstore/unsent ");
                            sb2.append(arrayList.size());
                            sb2.append(" | time spent:");
                            sb2.append(r7.A01());
                            Log.i(sb2.toString());
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                AbstractC15340mz r2 = (AbstractC15340mz) it.next();
                                this.A08.put(r2.A0z, r2);
                            }
                            if (!atomicBoolean.compareAndSet(false, true)) {
                                Log.e("unsent messages cache initialization failed to change the related flag");
                            }
                        }
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (SQLiteFullException e3) {
                    this.A04.A00(0);
                    throw e3;
                }
            }
        }
    }

    public boolean A03() {
        if (!this.A09.get()) {
            A02();
        }
        C21620xi r1 = this.A03;
        long A00 = this.A00.A00();
        Map map = r1.A02;
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            if (((AbstractC15340mz) ((Map.Entry) it.next()).getValue()).A0I + 86400000 < A00) {
                it.remove();
            }
        }
        return !map.isEmpty();
    }
}
