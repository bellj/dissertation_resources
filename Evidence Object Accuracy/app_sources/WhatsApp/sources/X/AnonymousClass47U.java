package X;

import android.media.MediaPlayer;
import com.whatsapp.util.Log;
import com.whatsapp.util.OpusPlayer;
import java.io.File;
import java.io.IOException;

/* renamed from: X.47U  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass47U extends AbstractC28651Ol {
    public final OpusPlayer A00;

    @Override // X.AbstractC28651Ol
    public void A0B(MediaPlayer.OnErrorListener onErrorListener) {
    }

    @Override // X.AbstractC28651Ol
    public void A0C(AnonymousClass4KR r1) {
    }

    @Override // X.AbstractC28651Ol
    public boolean A0E(AbstractC15710nm r2, float f) {
        return false;
    }

    public AnonymousClass47U(File file, int i) {
        this.A00 = new OpusPlayer(file.getAbsolutePath(), i);
    }

    @Override // X.AbstractC28651Ol
    public int A02() {
        try {
            return (int) this.A00.getCurrentPosition();
        } catch (IOException e) {
            Log.e(e);
            return 0;
        }
    }

    @Override // X.AbstractC28651Ol
    public int A03() {
        try {
            return (int) this.A00.getLength();
        } catch (IOException e) {
            Log.e(e);
            return 0;
        }
    }

    @Override // X.AbstractC28651Ol
    public void A04() {
        try {
            this.A00.pause();
        } catch (IOException e) {
            Log.e(e);
        }
    }

    @Override // X.AbstractC28651Ol
    public void A05() {
        this.A00.prepare();
    }

    @Override // X.AbstractC28651Ol
    public void A06() {
        this.A00.close();
    }

    @Override // X.AbstractC28651Ol
    public void A07() {
        this.A00.resume();
    }

    @Override // X.AbstractC28651Ol
    public void A08() {
        this.A00.start();
    }

    @Override // X.AbstractC28651Ol
    public void A09() {
        try {
            this.A00.stop();
        } catch (IOException e) {
            Log.e(e);
        }
    }

    @Override // X.AbstractC28651Ol
    public void A0A(int i) {
        this.A00.seek((long) i);
    }

    @Override // X.AbstractC28651Ol
    public boolean A0D() {
        try {
            return this.A00.isPlaying();
        } catch (IOException e) {
            Log.e(e);
            return false;
        }
    }
}
