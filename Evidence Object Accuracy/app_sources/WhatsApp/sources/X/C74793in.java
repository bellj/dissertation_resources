package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;

/* renamed from: X.3in  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74793in extends AnonymousClass02L {
    public final CategoryThumbnailLoader A00;
    public final AnonymousClass1J7 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74793in(CategoryThumbnailLoader categoryThumbnailLoader, AnonymousClass1J7 r3) {
        super(C74653iS.A00);
        C16700pc.A0E(categoryThumbnailLoader, 1);
        this.A00 = categoryThumbnailLoader;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC75663kD r22 = (AbstractC75663kD) r2;
        C16700pc.A0E(r22, 0);
        Object A0E = A0E(i);
        C16700pc.A0B(A0E);
        r22.A08((AnonymousClass4K7) A0E);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        C16700pc.A0E(viewGroup, 0);
        if (i == 0) {
            View inflate = C12960it.A0E(viewGroup).inflate(R.layout.list_item_catalog_category, viewGroup, false);
            C16700pc.A0B(inflate);
            return new C850340w(inflate, this.A00, this.A01);
        } else if (i == 1) {
            View inflate2 = C12960it.A0E(viewGroup).inflate(R.layout.list_item_shimmer_category, viewGroup, false);
            C16700pc.A0B(inflate2);
            return new C850540y(inflate2);
        } else if (i == 6) {
            View inflate3 = C12960it.A0E(viewGroup).inflate(R.layout.list_item_all_category_search_context, viewGroup, false);
            C16700pc.A0B(inflate3);
            return new C850140u(inflate3, this.A01);
        } else if (i == 7) {
            View inflate4 = C12960it.A0E(viewGroup).inflate(R.layout.header_category_search_context, viewGroup, false);
            C16700pc.A0B(inflate4);
            return new C849940s(inflate4);
        } else {
            throw C12970iu.A0f(C16700pc.A08("Invalid item viewtype: ", Integer.valueOf(i)));
        }
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass4K7) A0E(i)).A00;
    }
}
