package X;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.3qM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79293qM extends AbstractC79223qF {
    public long A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final ByteBuffer A04;
    public final ByteBuffer A05;

    public C79293qM(ByteBuffer byteBuffer) {
        this.A04 = byteBuffer;
        this.A05 = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
        long A05 = C95624e5.A02.A05(byteBuffer, C95624e5.A01);
        this.A01 = A05;
        long position = ((long) byteBuffer.position()) + A05;
        long limit = A05 + ((long) byteBuffer.limit());
        this.A02 = limit;
        this.A03 = limit - 10;
        this.A00 = position;
    }
}
