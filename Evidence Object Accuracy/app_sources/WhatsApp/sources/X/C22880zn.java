package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.ConditionVariable;
import android.os.Environment;
import com.facebook.redex.RunnableBRunnable0Shape0S0000000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.0zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22880zn {
    public final C14900mE A00;
    public final C15570nT A01;
    public final C237913a A02;
    public final C18330sH A03;
    public final C15820nx A04;
    public final C22730zY A05;
    public final C26551Dx A06;
    public final AnonymousClass10J A07;
    public final C22150yc A08;
    public final C19850um A09;
    public final C238013b A0A;
    public final C22740zZ A0B;
    public final C232210v A0C;
    public final C237412v A0D;
    public final C18850tA A0E;
    public final C20020v5 A0F;
    public final C230910i A0G;
    public final C16590pI A0H;
    public final C15890o4 A0I;
    public final C14820m6 A0J;
    public final C15990oG A0K;
    public final C15880o3 A0L;
    public final C20850wQ A0M;
    public final C16490p7 A0N;
    public final C26041Bu A0O;
    public final C21760xw A0P;
    public final C246216f A0Q;
    public final C241414j A0R;
    public final C231710q A0S;
    public final C16030oK A0T;
    public final C14420lP A0U;
    public final C22370yy A0V;
    public final C19890uq A0W;
    public final C20660w7 A0X;
    public final C20220vP A0Y;
    public final C18620sk A0Z;
    public final C18350sJ A0a;
    public final C26531Dv A0b;
    public final C15860o1 A0c;
    public final AnonymousClass105 A0d;
    public final C26541Dw A0e;
    public final C231910s A0f;
    public final C15830ny A0g;
    public final C22990zy A0h;
    public final C23000zz A0i;
    public final AnonymousClass12O A0j;
    public final AbstractC14440lR A0k;
    public final C232110u A0l;
    public final C22870zm A0m;
    public final C14860mA A0n;
    public final C245816b A0o;
    public final C21710xr A0p;
    public final AnonymousClass01H A0q;
    public final AnonymousClass01H A0r;
    public final CopyOnWriteArrayList A0s = new CopyOnWriteArrayList();

    public C22880zn(C14900mE r2, C15570nT r3, C237913a r4, C18330sH r5, C15820nx r6, C22730zY r7, C26551Dx r8, AnonymousClass10J r9, C22150yc r10, C19850um r11, C238013b r12, C22740zZ r13, C232210v r14, C237412v r15, C18850tA r16, C20020v5 r17, C230910i r18, C16590pI r19, C15890o4 r20, C14820m6 r21, C15990oG r22, C15880o3 r23, C20850wQ r24, C16490p7 r25, C26041Bu r26, C21760xw r27, C246216f r28, C241414j r29, C231710q r30, C16030oK r31, C14420lP r32, C22370yy r33, C19890uq r34, C20660w7 r35, C20220vP r36, C18620sk r37, C18350sJ r38, C26531Dv r39, C15860o1 r40, AnonymousClass105 r41, C26541Dw r42, C231910s r43, C15830ny r44, C22990zy r45, C23000zz r46, AnonymousClass12O r47, AbstractC14440lR r48, C232110u r49, C22870zm r50, C14860mA r51, C245816b r52, C21710xr r53, AnonymousClass01H r54, AnonymousClass01H r55) {
        this.A0H = r19;
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
        this.A0k = r48;
        this.A0b = r39;
        this.A0n = r51;
        this.A0X = r35;
        this.A0r = r54;
        this.A0E = r16;
        this.A0m = r50;
        this.A0R = r29;
        this.A0e = r42;
        this.A0W = r34;
        this.A0p = r53;
        this.A0A = r12;
        this.A04 = r6;
        this.A0g = r44;
        this.A0o = r52;
        this.A0F = r17;
        this.A0c = r40;
        this.A0P = r27;
        this.A08 = r10;
        this.A0U = r32;
        this.A0K = r22;
        this.A0j = r47;
        this.A03 = r5;
        this.A0L = r23;
        this.A0i = r46;
        this.A09 = r11;
        this.A06 = r8;
        this.A0Y = r36;
        this.A0N = r25;
        this.A0a = r38;
        this.A0h = r45;
        this.A0I = r20;
        this.A0J = r21;
        this.A0Q = r28;
        this.A0G = r18;
        this.A0S = r30;
        this.A0V = r33;
        this.A0f = r43;
        this.A0l = r49;
        this.A0T = r31;
        this.A0d = r41;
        this.A0C = r14;
        this.A0Z = r37;
        this.A0O = r26;
        this.A0D = r15;
        this.A05 = r7;
        this.A0M = r24;
        this.A07 = r9;
        this.A0B = r13;
        this.A0q = r55;
    }

    public void A00() {
        this.A0a.A0A(6);
        Iterator it = this.A0s.iterator();
        while (it.hasNext()) {
            ((AnonymousClass108) it.next()).ASA();
        }
        this.A0V.A05();
        this.A0Y.A0D(false);
        this.A09.A09();
        ConditionVariable conditionVariable = new ConditionVariable(false);
        AnonymousClass214 r9 = new AnonymousClass214(conditionVariable, this);
        ConditionVariable conditionVariable2 = new ConditionVariable(false);
        AnonymousClass215 r2 = new AnonymousClass215(conditionVariable2, this, r9);
        Context context = this.A0H.A00;
        context.bindService(C14960mK.A0U(context, null), r2, 1);
        C15570nT r22 = this.A01;
        r22.A08();
        if (r22.A00 != null) {
            Intent A0U = C14960mK.A0U(context, "action_delete");
            A0U.putExtra("account_name", this.A0J.A09());
            r22.A08();
            C27631Ih r0 = r22.A05;
            AnonymousClass009.A05(r0);
            A0U.putExtra("jid_user", r0.user);
            this.A0k.Ab2(new RunnableBRunnable0Shape0S0300000_I0(conditionVariable2, context, A0U, 12));
        } else {
            Log.i("deleteacctconfirm/app.me is null/no google drive backup deletion");
        }
        A03(context);
        this.A03.A01();
        this.A0X.A05();
        this.A0W.A0G(false);
        this.A0k.Aaz(new AnonymousClass216(context, conditionVariable2, conditionVariable, this, r9), new Void[0]);
    }

    public void A01() {
        Context context = this.A0H.A00;
        AnonymousClass047.A00(context, R.xml.preferences_account);
        AnonymousClass047.A00(context, R.xml.preferences_chat);
        AnonymousClass047.A00(context, R.xml.preferences_chat_history);
        AnonymousClass047.A00(context, R.xml.preferences_contacts);
        AnonymousClass047.A00(context, R.xml.preferences_data_usage);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x01b2 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02() {
        /*
        // Method dump skipped, instructions count: 740
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22880zn.A02():void");
    }

    public final void A03(Context context) {
        File filesDir = context.getFilesDir();
        filesDir.toString();
        for (int i = 0; i < context.fileList().length; i++) {
        }
        C14350lI.A0C(filesDir);
        this.A0k.Ab2(new RunnableBRunnable0Shape0S0000000_I0(1));
        C20020v5 r2 = this.A0F;
        synchronized (r2) {
            r2.A04().edit().clear().commit();
            AnonymousClass1EB r1 = r2.A07;
            r1.A02().edit().clear().commit();
            r1.A01().edit().clear().commit();
        }
        String externalStorageState = Environment.getExternalStorageState();
        StringBuilder sb = new StringBuilder("deleteacctconfirm/externalmedia-state ");
        sb.append(externalStorageState);
        Log.i(sb.toString());
        if (this.A0I.A0A(externalStorageState)) {
            this.A0L.A0F();
        }
    }

    public final void A04(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append("_preferences");
        SharedPreferences sharedPreferences = context.getSharedPreferences(sb.toString(), 0);
        if (!sharedPreferences.edit().clear().commit()) {
            Log.e("deleteacctconfirm/cleanup/clear failed");
        }
        A01();
        if (!sharedPreferences.edit().putString("version", "2.22.17.70").commit()) {
            Log.e("deleteacctconfirm/cleanup/setversion failed");
        }
    }
}
