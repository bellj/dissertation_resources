package X;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.gallery.GalleryFragmentBase;
import com.whatsapp.gallery.LinksGalleryFragment;
import java.util.Calendar;

/* renamed from: X.31a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C616131a extends AbstractC54432gi implements AnonymousClass2TX {
    public final /* synthetic */ LinksGalleryFragment A00;

    public C616131a(LinksGalleryFragment linksGalleryFragment) {
        this.A00 = linksGalleryFragment;
    }

    @Override // X.AnonymousClass2TX
    public int ABl(int i) {
        return ((AnonymousClass2WL) ((GalleryFragmentBase) this.A00).A0I.get(i)).count;
    }

    @Override // X.AnonymousClass2TX
    public int ADH() {
        return ((GalleryFragmentBase) this.A00).A0I.size();
    }

    @Override // X.AnonymousClass2TX
    public long ADI(int i) {
        return -((Calendar) ((GalleryFragmentBase) this.A00).A0I.get(i)).getTimeInMillis();
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ void ANF(AnonymousClass03U r3, int i) {
        ((C75263jZ) r3).A00.setText(((GalleryFragmentBase) this.A00).A0I.get(i).toString());
    }

    @Override // X.AnonymousClass2TX
    public /* bridge */ /* synthetic */ AnonymousClass03U AOh(ViewGroup viewGroup) {
        LinksGalleryFragment linksGalleryFragment = this.A00;
        View inflate = linksGalleryFragment.A0B().getLayoutInflater().inflate(R.layout.media_gallery_section_row, viewGroup, false);
        inflate.setClickable(false);
        C12970iu.A18(linksGalleryFragment.A0p(), inflate, R.color.gallery_separator);
        return new C75263jZ(inflate);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LinksGalleryFragment linksGalleryFragment = this.A00;
        View A0F = C12960it.A0F(linksGalleryFragment.A0B().getLayoutInflater(), viewGroup, R.layout.link_media_item);
        ((FrameLayout) A0F.findViewById(R.id.link_preview_frame)).setForeground(AnonymousClass00T.A04(linksGalleryFragment.A0p(), R.drawable.selector_orange_gradient));
        AnonymousClass2GF.A01(linksGalleryFragment.A0p(), C12970iu.A0L(A0F, R.id.chevron), ((GalleryFragmentBase) linksGalleryFragment).A05, R.drawable.chevron);
        return new C55162ht(A0F, linksGalleryFragment);
    }

    @Override // X.AnonymousClass2TX
    public boolean AWm(MotionEvent motionEvent, AnonymousClass03U r3, int i) {
        return false;
    }
}
