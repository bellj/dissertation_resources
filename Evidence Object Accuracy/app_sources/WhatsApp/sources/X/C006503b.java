package X;

import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: X.03b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C006503b {
    public static final C006503b A01 = new C006403a().A00();
    public static final String A02 = C06390Tk.A01("Data");
    public Map A00;

    public C006503b() {
    }

    public C006503b(C006503b r3) {
        this.A00 = new HashMap(r3.A00);
    }

    public C006503b(Map map) {
        this.A00 = new HashMap(map);
    }

    public static C006503b A00(byte[] bArr) {
        Throwable th;
        ObjectInputStream objectInputStream;
        Throwable e;
        if (bArr.length <= 10240) {
            HashMap hashMap = new HashMap();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                objectInputStream = new ObjectInputStream(byteArrayInputStream);
                try {
                    try {
                        for (int readInt = objectInputStream.readInt(); readInt > 0; readInt--) {
                            hashMap.put(objectInputStream.readUTF(), objectInputStream.readObject());
                        }
                        try {
                            objectInputStream.close();
                        } catch (IOException e2) {
                            Log.e(A02, "Error in Data#fromByteArray: ", e2);
                        }
                    } catch (IOException | ClassNotFoundException e3) {
                        e = e3;
                        String str = A02;
                        Log.e(str, "Error in Data#fromByteArray: ", e);
                        if (objectInputStream != null) {
                            try {
                                objectInputStream.close();
                            } catch (IOException e4) {
                                Log.e(str, "Error in Data#fromByteArray: ", e4);
                            }
                        }
                        byteArrayInputStream.close();
                        return new C006503b(hashMap);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (objectInputStream != null) {
                        try {
                            objectInputStream.close();
                        } catch (IOException e5) {
                            Log.e(A02, "Error in Data#fromByteArray: ", e5);
                        }
                    }
                    try {
                        byteArrayInputStream.close();
                        throw th;
                    } catch (IOException e6) {
                        Log.e(A02, "Error in Data#fromByteArray: ", e6);
                        throw th;
                    }
                }
            } catch (IOException | ClassNotFoundException e7) {
                e = e7;
                objectInputStream = null;
            } catch (Throwable th3) {
                th = th3;
                byteArrayInputStream.close();
                throw th;
            }
            try {
                byteArrayInputStream.close();
            } catch (IOException e8) {
                Log.e(A02, "Error in Data#fromByteArray: ", e8);
            }
            return new C006503b(hashMap);
        }
        throw new IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
    }

    public static byte[] A01(C006503b r7) {
        Throwable th;
        IOException e;
        ObjectOutputStream objectOutputStream;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream2 = null;
        try {
            try {
                objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            } catch (IOException e2) {
                e = e2;
            }
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            Map map = r7.A00;
            objectOutputStream.writeInt(map.size());
            for (Map.Entry entry : map.entrySet()) {
                objectOutputStream.writeUTF((String) entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            try {
                objectOutputStream.close();
            } catch (IOException e3) {
                Log.e(A02, "Error in Data#toByteArray: ", e3);
            }
            try {
                byteArrayOutputStream.close();
            } catch (IOException e4) {
                Log.e(A02, "Error in Data#toByteArray: ", e4);
            }
            if (byteArrayOutputStream.size() <= 10240) {
                return byteArrayOutputStream.toByteArray();
            }
            throw new IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
        } catch (IOException e5) {
            e = e5;
            objectOutputStream2 = objectOutputStream;
            String str = A02;
            Log.e(str, "Error in Data#toByteArray: ", e);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (objectOutputStream2 != null) {
                try {
                    objectOutputStream2.close();
                } catch (IOException e6) {
                    Log.e(str, "Error in Data#toByteArray: ", e6);
                }
            }
            try {
                byteArrayOutputStream.close();
                return byteArray;
            } catch (IOException e7) {
                Log.e(str, "Error in Data#toByteArray: ", e7);
                return byteArray;
            }
        } catch (Throwable th3) {
            th = th3;
            objectOutputStream2 = objectOutputStream;
            if (objectOutputStream2 != null) {
                try {
                    objectOutputStream2.close();
                } catch (IOException e8) {
                    Log.e(A02, "Error in Data#toByteArray: ", e8);
                }
            }
            try {
                byteArrayOutputStream.close();
                throw th;
            } catch (IOException e9) {
                Log.e(A02, "Error in Data#toByteArray: ", e9);
                throw th;
            }
        }
    }

    public int A02(String str, int i) {
        Object obj = this.A00.get(str);
        return obj instanceof Integer ? ((Number) obj).intValue() : i;
    }

    public String A03(String str) {
        Object obj = this.A00.get(str);
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }

    public boolean equals(Object obj) {
        boolean equals;
        if (this != obj) {
            if (obj != null && C006503b.class == obj.getClass()) {
                Map map = this.A00;
                Set keySet = map.keySet();
                Map map2 = ((C006503b) obj).A00;
                if (keySet.equals(map2.keySet())) {
                    for (Object obj2 : keySet) {
                        Object obj3 = map.get(obj2);
                        Object obj4 = map2.get(obj2);
                        if (obj3 != null) {
                            if (obj4 != null) {
                                if (!(obj3 instanceof Object[]) || !(obj4 instanceof Object[])) {
                                    equals = obj3.equals(obj4);
                                } else {
                                    equals = Arrays.deepEquals((Object[]) obj3, (Object[]) obj4);
                                }
                                if (!equals) {
                                }
                            }
                        } else if (obj3 == obj4) {
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A00.hashCode() * 31;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Data {");
        Map map = this.A00;
        if (!map.isEmpty()) {
            for (String str : map.keySet()) {
                sb.append(str);
                sb.append(" : ");
                Object obj = map.get(str);
                if (obj instanceof Object[]) {
                    sb.append(Arrays.toString((Object[]) obj));
                } else {
                    sb.append(obj);
                }
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }
}
