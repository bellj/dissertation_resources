package X;

import com.whatsapp.jid.Jid;
import java.util.List;

/* renamed from: X.1fU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34041fU extends AbstractC28131Kt {
    public final Jid A00;
    public final List A01;

    public C34041fU(Jid jid, C34021fS r2, String str, List list) {
        super(r2, str);
        this.A00 = jid;
        this.A01 = list;
    }
}
