package X;

/* renamed from: X.42z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C854842z extends AbstractC16110oT {
    public Integer A00;

    public C854842z() {
        super(3138, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamConversationsNuxUiAction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "uiActionSource", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
