package X;

import java.util.List;

/* renamed from: X.3wh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC83103wh extends AnonymousClass4YR {
    public boolean A08(C94394bk r5, Object obj, String str) {
        if (obj != null) {
            C92364Vp r1 = r5.A01;
            if (obj instanceof List) {
                return true;
            }
            if (A07() && !r1.A03.contains(EnumC87084Ad.SUPPRESS_EXCEPTIONS)) {
                Object[] A1a = C12980iv.A1a();
                A1a[0] = toString();
                A1a[1] = obj;
                throw new C82803wD(String.format("Filter: %s can only be applied to arrays. Current context is: %s", A1a));
            }
        } else if (A07() && !r5.A01.A03.contains(EnumC87084Ad.SUPPRESS_EXCEPTIONS)) {
            StringBuilder A0k = C12960it.A0k("The path ");
            A0k.append(str);
            throw new C82803wD(C12960it.A0d(" is null", A0k));
        }
        return false;
    }
}
