package X;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import java.util.Map;

/* renamed from: X.35K  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35K extends AbstractC55342iF {
    public final AbstractC14640lm A00;
    public final C21990yJ A01;
    public final AnonymousClass4OK A02;
    public final AbstractC14440lR A03;
    public final List A04;
    public final List A05;
    public final List A06;
    public final Map A07 = C12970iu.A11();
    public final boolean A08;

    public AnonymousClass35K(Context context, Resources resources, AbstractC14640lm r4, C21990yJ r5, AnonymousClass4OK r6, AbstractC14440lR r7, List list, List list2, List list3, boolean z) {
        super(context, resources);
        this.A03 = r7;
        this.A01 = r5;
        this.A02 = r6;
        this.A04 = list;
        this.A06 = list2;
        this.A05 = list3;
        this.A00 = r4;
        this.A08 = z;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        int size = this.A04.size();
        List list = this.A06;
        return list != null ? size + list.size() : size;
    }

    @Override // X.AbstractC55342iF, X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        super.A0D(viewGroup, obj, i);
        C12980iv.A1M((AbstractC16350or) this.A07.remove(Integer.valueOf(i)));
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return C12970iu.A1Z(view, obj);
    }

    public final void A0F(AnonymousClass35E r6, int i) {
        r6.A02.setVisibility(0);
        r6.A03.setVisibility(0);
        r6.A01.setVisibility(8);
        AnonymousClass38N r2 = new AnonymousClass38N(r6.getContext(), (Uri) this.A04.get(i), this.A01, new C63673Cm(r6, this, i));
        C12980iv.A1M((AbstractC16350or) this.A07.put(Integer.valueOf(i), r2));
        C12960it.A1E(r2, this.A03);
    }
}
