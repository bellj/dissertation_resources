package X;

import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.whatsapp.R;
import com.whatsapp.appwidget.WidgetProvider;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.3OQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3OQ implements RemoteViewsService.RemoteViewsFactory {
    public final Context A00;
    public final C22670zS A01;
    public final C15550nR A02;
    public final C15610nY A03;
    public final C14830m7 A04;
    public final AnonymousClass018 A05;
    public final C22630zO A06;
    public final ArrayList A07 = C12960it.A0l();

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public int getViewTypeCount() {
        return 1;
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public boolean hasStableIds() {
        return true;
    }

    public AnonymousClass3OQ(Context context, C22670zS r3, C15550nR r4, C15610nY r5, C14830m7 r6, AnonymousClass018 r7, C22630zO r8) {
        this.A00 = context;
        this.A04 = r6;
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A05 = r7;
        this.A06 = r8;
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public int getCount() {
        return this.A07.size();
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public RemoteViews getViewAt(int i) {
        ArrayList arrayList = this.A07;
        if (i >= arrayList.size()) {
            return null;
        }
        RemoteViews remoteViews = new RemoteViews(this.A00.getPackageName(), (int) R.layout.widget_row);
        AnonymousClass4SW r5 = (AnonymousClass4SW) arrayList.get(i);
        remoteViews.setTextViewText(R.id.heading, r5.A02);
        remoteViews.setTextViewText(R.id.content, r5.A01);
        remoteViews.setTextViewText(R.id.date, r5.A04);
        remoteViews.setContentDescription(R.id.date, r5.A03);
        Intent A0A = C12970iu.A0A();
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", C15380n4.A03(r5.A00));
        A0A.putExtras(A0D);
        remoteViews.setOnClickFillInIntent(R.id.widget_row, A0A);
        return remoteViews;
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public void onCreate() {
        Log.i("widgetviewsfactory/oncreate");
        onDataSetChanged();
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public void onDataSetChanged() {
        Log.i("widgetviewsfactory/ondatasetchanged");
        long clearCallingIdentity = Binder.clearCallingIdentity();
        try {
            ArrayList arrayList = WidgetProvider.A0A;
            ArrayList arrayList2 = this.A07;
            arrayList2.clear();
            if (arrayList != null && this.A01.A06()) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    AbstractC15340mz A0f = C12980iv.A0f(it);
                    AnonymousClass4SW r4 = new AnonymousClass4SW();
                    C15550nR r1 = this.A02;
                    AbstractC14640lm r0 = A0f.A0z.A00;
                    C15370n3 A0B = r1.A0B(r0);
                    r4.A00 = r0;
                    r4.A02 = AbstractC32741cf.A02(this.A03.A04(A0B));
                    r4.A01 = this.A06.A0D(A0B, A0f, false, false);
                    C14830m7 r2 = this.A04;
                    AnonymousClass018 r3 = this.A05;
                    r4.A04 = C38131nZ.A0A(r3, r2.A02(A0f.A0I), false);
                    r4.A03 = C38131nZ.A0A(r3, r2.A02(A0f.A0I), true);
                    arrayList2.add(r4);
                }
            }
        } finally {
            Binder.restoreCallingIdentity(clearCallingIdentity);
        }
    }

    @Override // android.widget.RemoteViewsService.RemoteViewsFactory
    public void onDestroy() {
        Log.i("widgetviewsfactory/ondestroy");
    }
}
