package X;

/* renamed from: X.4WP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4WP {
    public final long A00;

    public /* synthetic */ AnonymousClass4WP(long j) {
        this.A00 = j;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof AnonymousClass4WP) && this.A00 == ((AnonymousClass4WP) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((C72453ed.A0A(((int) (0 ^ (0 >>> 32))) * 31, this.A00) + 0) * 31) + 0) * 31) + 0;
    }
}
