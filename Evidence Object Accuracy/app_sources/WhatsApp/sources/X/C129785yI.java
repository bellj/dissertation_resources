package X;

/* renamed from: X.5yI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C129785yI {
    public final String A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C129785yI) && C16700pc.A0O(this.A00, ((C129785yI) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public C129785yI(String str) {
        this.A00 = str;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Device(paymentDeviceId=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
