package X;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.IndiaUpiDebitCardVerificationActivity;
import com.whatsapp.payments.ui.IndiaUpiForgotPinDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;
import com.whatsapp.payments.ui.IndiaUpiPauseMandateActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity;
import com.whatsapp.payments.ui.IndiaUpiPinSetUpCompletedActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5j9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121655j9 extends AbstractActivityC121545iU implements AbstractC124835qC, AnonymousClass6N0, AnonymousClass1P3, AbstractC136456Mp, AnonymousClass6M1, AbstractC136306Ma {
    public AnonymousClass130 A00;
    public C15890o4 A01;
    public AbstractC28901Pl A02;
    public C241414j A03;
    public AnonymousClass1IR A04;
    public C248217a A05;
    public AnonymousClass18S A06;
    public C18620sk A07;
    public PaymentBottomSheet A08 = new PaymentBottomSheet();
    public AnonymousClass60W A09;
    public C14920mG A0A;
    public String A0B;
    public String A0C;
    public List A0D;
    public boolean A0E;
    public boolean A0F;
    public final AnonymousClass4UZ A0G = new C120095fc(this);
    public final C30931Zj A0H = C117295Zj.A0G("IndiaUpiBaseRequestPaymentActivity");

    @Override // X.AbstractC124835qC
    public String ACf(AbstractC28901Pl r2, int i) {
        return null;
    }

    @Override // X.AbstractC124835qC
    public void AMM(ViewGroup viewGroup) {
    }

    @Override // X.AnonymousClass6N0
    public void ATc(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void ATg(PaymentBottomSheet paymentBottomSheet, int i) {
    }

    @Override // X.AnonymousClass6N0
    public void AXm(PaymentBottomSheet paymentBottomSheet) {
    }

    @Override // X.AnonymousClass6N0
    public void AXo(PaymentBottomSheet paymentBottomSheet) {
    }

    @Override // X.AbstractC124835qC
    public boolean AdO(AbstractC28901Pl r2, int i) {
        return false;
    }

    @Override // X.AbstractC124835qC
    public boolean AdU(AbstractC28901Pl r2) {
        return true;
    }

    @Override // X.AbstractC124835qC
    public boolean AdV() {
        return false;
    }

    @Override // X.AbstractC124835qC
    public void Adj(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    public Intent A3H() {
        Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentsAccountSetupActivity.class);
        A0D.putExtra("extra_setup_mode", 2);
        A0D.putExtra("extra_payments_entry_type", 6);
        A0D.putExtra("extra_is_first_payment_method", true);
        A0D.putExtra("extra_skip_value_props_display", false);
        return A0D;
    }

    public void A3I() {
        if (!this.A01.A08()) {
            RequestPermissionActivity.A0B(this);
            return;
        }
        int A01 = this.A09.A01();
        if (A01 == 1) {
            A2I(new AnonymousClass2GV() { // from class: X.66a
                @Override // X.AnonymousClass2GV
                public final void AO1() {
                    AbstractActivityC121655j9 r2 = AbstractActivityC121655j9.this;
                    r2.A2G(C14960mK.A00(r2), true);
                }
            }, R.string.payment_sending_failed, R.string.upi_unable_to_send_payment_without_phone_state_permission, R.string.change_number_title);
        } else if (A01 != 2) {
            C119755f3 r0 = (C119755f3) this.A02.A08;
            if (r0 == null || !"OD_UNSECURED".equals(r0.A0B) || this.A0F) {
                ((AbstractActivityC121545iU) this).A09.A00();
            } else {
                Ado(R.string.upi_unsecure_overdraft_account_non_merchant_payment_error);
            }
        } else {
            new AlertDialog.Builder(this).setTitle(R.string.payment_cant_process).setMessage(R.string.upi_unable_to_send_payment_with_incorrect_stored_sim_id).setPositiveButton(R.string.upi_add_payment_method, new IDxCListenerShape10S0100000_3_I1(this, 17)).setNegativeButton(R.string.upi_cancel_payment, new IDxCListenerShape10S0100000_3_I1(this, 18)).setCancelable(false).show();
        }
    }

    public void A3J(AbstractC28901Pl r17, HashMap hashMap) {
        AbstractC28901Pl r7 = r17;
        IndiaUpiPauseMandateActivity indiaUpiPauseMandateActivity = (IndiaUpiPauseMandateActivity) this;
        indiaUpiPauseMandateActivity.A2C(R.string.register_wait_message);
        C117945b1 r11 = indiaUpiPauseMandateActivity.A06;
        long A02 = IndiaUpiPauseMandateActivity.A02(indiaUpiPauseMandateActivity.A02);
        long A022 = IndiaUpiPauseMandateActivity.A02(indiaUpiPauseMandateActivity.A01);
        if (r17 == null) {
            r7 = r11.A00;
        }
        C120565gO r9 = r11.A0B;
        AnonymousClass1IR r6 = r11.A01;
        String str = r11.A03;
        C133246Aa r8 = new AbstractC136236Lt(A02, A022) { // from class: X.6Aa
            public final /* synthetic */ long A00;
            public final /* synthetic */ long A01;

            {
                this.A00 = r2;
                this.A01 = r4;
            }

            @Override // X.AbstractC136236Lt
            public final void AVD(C452120p r82) {
                C117945b1 r2 = C117945b1.this;
                long j = this.A00;
                long j2 = this.A01;
                if (r82 == null) {
                    r2.A0D.Ab2(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000f: INVOKE  
                          (wrap: X.0lR : 0x0008: IGET  (r0v2 X.0lR A[REMOVE]) = (r2v0 'r2' X.5b1) X.5b1.A0D X.0lR)
                          (wrap: X.6Je : 0x000c: CONSTRUCTOR  (r1v1 X.6Je A[REMOVE]) = (r2v0 'r2' X.5b1), (r3v0 'j' long), (r5v0 'j2' long) call: X.6Je.<init>(X.5b1, long, long):void type: CONSTRUCTOR)
                         type: INTERFACE call: X.0lR.Ab2(java.lang.Runnable):void in method: X.6Aa.AVD(X.20p):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x000c: CONSTRUCTOR  (r1v1 X.6Je A[REMOVE]) = (r2v0 'r2' X.5b1), (r3v0 'j' long), (r5v0 'j2' long) call: X.6Je.<init>(X.5b1, long, long):void type: CONSTRUCTOR in method: X.6Aa.AVD(X.20p):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 23 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6Je, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 29 more
                        */
                    /*
                        this = this;
                        X.5b1 r2 = X.C117945b1.this
                        long r3 = r7.A00
                        long r5 = r7.A01
                        if (r8 != 0) goto L_0x0013
                        X.0lR r0 = r2.A0D
                        X.6Je r1 = new X.6Je
                        r1.<init>(r2, r3, r5)
                        r0.Ab2(r1)
                        return
                    L_0x0013:
                        r0 = 3
                        X.5vl r1 = new X.5vl
                        r1.<init>(r0)
                        r1.A04 = r8
                        X.1It r0 = r2.A02
                        r0.A0A(r1)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C133246Aa.AVD(X.20p):void");
                }
            };
            Log.i("PAY: pausePayeeMandate called");
            ArrayList A0l = C12960it.A0l();
            C117295Zj.A1M("action", "upi-pause-mandate", A0l);
            r9.A02(r6, A0l);
            C120565gO.A01(null, (C119835fB) r6.A0A, str, A0l, true);
            C120565gO.A00(r7, hashMap, A0l);
            AnonymousClass1V8[] A03 = r9.A03(r6);
            A0l.add(new AnonymousClass1W9("pause-start-ts", A02 / 1000));
            A0l.add(new AnonymousClass1W9("pause-end-ts", A022 / 1000));
            C120475gF r1 = r9.A03;
            if (r1 != null) {
                r1.A00("U66", A0l);
            }
            C64513Fv r72 = ((C126705tJ) r9).A00;
            if (r72 != null) {
                r72.A04("upi-pause-mandate");
            }
            C117305Zk.A1H(((C126705tJ) r9).A01, new C120735gf(r9.A00, r9.A01, r9.A02, r72, r8, r9), C117295Zj.A0L(A0l, A03));
        }

        public final void A3K(AnonymousClass1IR r5) {
            AbstractC30891Zf r2 = r5.A0A;
            AnonymousClass009.A05(r2);
            C119835fB r22 = (C119835fB) r2;
            String str = r22.A0J;
            if (r22.A0B != null) {
                this.A0H.A06("skipping verifyReceiver for mandates");
                this.A0C = str;
                this.A0B = (String) C117295Zj.A0R(r22.A07);
                A3L(this.A08);
                return;
            }
            ((AbstractActivityC121685jC) this).A0P.A02().AF8().Afg(C117295Zj.A0E(str), new AnonymousClass6Ln(str) { // from class: X.691
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // X.AnonymousClass6Ln
                public final void AVN(UserJid userJid, AnonymousClass1ZR r12, AnonymousClass1ZR r13, AnonymousClass1ZR r14, C452120p r15, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
                    boolean z6;
                    AbstractActivityC121655j9 r4 = AbstractActivityC121655j9.this;
                    String str4 = this.A01;
                    r4.AaN();
                    if (!z || r15 != null) {
                        Object[] A1b = C12970iu.A1b();
                        A1b[0] = r4.getString(R.string.india_upi_payment_id_name);
                        r4.Adr(A1b, 0, R.string.payment_id_cannot_verify_error_text_default);
                        return;
                    }
                    r4.A0B = (String) C117295Zj.A0R(r12);
                    r4.A0C = str4;
                    r4.A0F = z2;
                    if (z3) {
                        boolean z7 = r4 instanceof IndiaUpiMandatePaymentActivity;
                        AnonymousClass18S r3 = r4.A06;
                        AnonymousClass1ZR A0E = C117295Zj.A0E(str4);
                        if (!z7) {
                            z6 = false;
                        } else {
                            z6 = true;
                        }
                        r3.A00(r4, r4, null, A0E, z6, false);
                        return;
                    }
                    r4.A3L(r4.A08);
                }
            });
        }

        public void A3L(PaymentBottomSheet paymentBottomSheet) {
            ConfirmPaymentFragment A00 = ConfirmPaymentFragment.A00(this.A02, null, !this.A0F ? 1 : 0);
            A00.A0L = this;
            A00.A0M = this;
            paymentBottomSheet.A01 = A00;
            Adl(paymentBottomSheet, "ConfirmPaymentFragment");
        }

        public void A3M(PaymentBottomSheet paymentBottomSheet) {
            AbstractC28901Pl r2 = this.A02;
            Bundle A0D = C12970iu.A0D();
            A0D.putParcelable("extra_bank_account", r2);
            IndiaUpiForgotPinDialogFragment indiaUpiForgotPinDialogFragment = new IndiaUpiForgotPinDialogFragment();
            indiaUpiForgotPinDialogFragment.A0U(A0D);
            indiaUpiForgotPinDialogFragment.A04 = this;
            paymentBottomSheet.A01 = indiaUpiForgotPinDialogFragment;
            Adl(paymentBottomSheet, "IndiaUpiForgotPinDialogFragment");
        }

        public void A3N(PaymentBottomSheet paymentBottomSheet) {
            AbstractC28901Pl r2 = this.A02;
            Bundle A0D = C12970iu.A0D();
            A0D.putParcelable("extra_bank_account", r2);
            IndiaUpiPinPrimerDialogFragment indiaUpiPinPrimerDialogFragment = new IndiaUpiPinPrimerDialogFragment();
            indiaUpiPinPrimerDialogFragment.A0U(A0D);
            indiaUpiPinPrimerDialogFragment.A04 = this;
            paymentBottomSheet.A01 = indiaUpiPinPrimerDialogFragment;
            Adl(paymentBottomSheet, "IndiaUpiPinPrimerDialogFragment");
        }

        public void A3O(PaymentBottomSheet paymentBottomSheet, String str) {
            if (this instanceof IndiaUpiMandatePaymentActivity) {
                paymentBottomSheet.A00 = null;
            }
            A2M(str);
        }

        @Override // X.AbstractC124835qC
        public void A6F(ViewGroup viewGroup) {
            AnonymousClass60R r3;
            if (!(this instanceof IndiaUpiMandatePaymentActivity)) {
                View inflate = getLayoutInflater().inflate(R.layout.confirm_payment_total_amount_row, viewGroup, true);
                if (this.A04 != null) {
                    C12960it.A0I(inflate, R.id.amount).setText(((AbstractActivityC121545iU) this).A02.A02("INR").AAA(((AbstractActivityC121545iU) this).A01, this.A04.A08, 0));
                    return;
                }
                return;
            }
            IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity = (IndiaUpiMandatePaymentActivity) this;
            View inflate2 = indiaUpiMandatePaymentActivity.getLayoutInflater().inflate(R.layout.confirm_payment_mandate_extra_row, viewGroup, true);
            TextView A0I = C12960it.A0I(inflate2, R.id.date_value);
            TextView A0I2 = C12960it.A0I(inflate2, R.id.frequency_value);
            TextView A0I3 = C12960it.A0I(inflate2, R.id.total_value);
            AnonymousClass1IR r5 = indiaUpiMandatePaymentActivity.A01.A07;
            AbstractC30891Zf r1 = r5.A0A;
            if ((r1 instanceof C119835fB) && (r3 = ((C119835fB) r1).A0B) != null) {
                A0I.setText(indiaUpiMandatePaymentActivity.A03.A03(r3.A01));
                A0I2.setText(indiaUpiMandatePaymentActivity.A03.A05(r3.A0E));
                A0I3.setText(indiaUpiMandatePaymentActivity.A03.A04(r5.A08, r3.A0F));
            }
        }

        @Override // X.AbstractC124835qC
        public String ABX(AbstractC28901Pl r2, int i) {
            int i2;
            if (!(this instanceof IndiaUpiMandatePaymentActivity)) {
                i2 = R.string.payments_send_payment_text;
            } else {
                i2 = R.string.upi_mandate_bottom_cta;
            }
            return getString(i2);
        }

        @Override // X.AbstractC124835qC
        public String ACJ(AbstractC28901Pl r2) {
            return getString(R.string.payments_send_payment_using);
        }

        @Override // X.AbstractC124835qC
        public String ACK(AbstractC28901Pl r4) {
            return C1311161i.A02(this, ((AbstractActivityC121545iU) this).A01, r4, ((AbstractActivityC121685jC) this).A0P, false);
        }

        @Override // X.AbstractC124835qC
        public String AEO(AbstractC28901Pl r6) {
            AnonymousClass1ZR A04 = ((AbstractActivityC121665jA) this).A0B.A04();
            if (AnonymousClass1ZS.A02(A04)) {
                return null;
            }
            return C12960it.A0X(this, AnonymousClass1ZS.A01(A04), C12970iu.A1b(), 0, R.string.india_upi_payment_id_with_upi_label);
        }

        @Override // X.AbstractC124835qC
        public void AMN(ViewGroup viewGroup) {
            View inflate = getLayoutInflater().inflate(R.layout.confirm_dialog_title, viewGroup, true);
            C117295Zj.A0n(C117295Zj.A07(this, inflate, C12960it.A0I(inflate, R.id.text), R.string.confirm_payment_bottom_sheet_default_title), this, 29);
        }

        @Override // X.AbstractC124835qC
        public void AMP(ViewGroup viewGroup) {
            View inflate = getLayoutInflater().inflate(R.layout.india_upi_confirm_payment_recipient_row, viewGroup, true);
            ImageView A0K = C12970iu.A0K(inflate, R.id.payment_recipient_profile_pic);
            TextView A0I = C12960it.A0I(inflate, R.id.payment_recipient_name);
            TextView A0I2 = C12960it.A0I(inflate, R.id.payment_recipient_vpa);
            AnonymousClass028.A0D(inflate, R.id.expand_receiver_details_button).setVisibility(0);
            C117295Zj.A0n(inflate, this, 30);
            this.A00.A05(A0K, R.drawable.avatar_contact);
            A0I.setText(this.A0B);
            A0I2.setText(C12960it.A0X(this, this.A0C, new Object[1], 0, R.string.india_upi_payment_id_with_upi_label));
        }

        @Override // X.AbstractC136306Ma
        public void AOG() {
            this.A08.A1K();
        }

        @Override // X.AnonymousClass6N0
        public void AOW(View view, View view2, AnonymousClass1ZO r8, AbstractC28901Pl r9, PaymentBottomSheet paymentBottomSheet) {
            A3O(this.A08, "ConfirmPaymentFragment");
            String[] split = ((AbstractActivityC121665jA) this).A0C.A05().split(";");
            int length = split.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (split[i].equalsIgnoreCase(this.A02.A0A)) {
                    this.A0E = true;
                    break;
                } else {
                    i++;
                }
            }
            C119755f3 r0 = (C119755f3) this.A02.A08;
            if (r0 == null || !C12970iu.A1Y(r0.A05.A00) || this.A0E) {
                A3I();
                return;
            }
            PaymentBottomSheet paymentBottomSheet2 = new PaymentBottomSheet();
            this.A08 = paymentBottomSheet2;
            A3M(paymentBottomSheet2);
        }

        @Override // X.AbstractC136306Ma
        public void AOd() {
            Intent A0D = C12990iw.A0D(this, IndiaUpiDebitCardVerificationActivity.class);
            C117315Zl.A0M(A0D, this.A02);
            A2v(A0D);
            A2E(A0D, 1016);
        }

        @Override // X.AbstractC136456Mp
        public void AOf() {
            A3O(this.A08, "IndiaUpiForgotPinDialogFragment");
            C18600si r2 = ((AbstractActivityC121665jA) this).A0C;
            StringBuilder A0h = C12960it.A0h();
            A0h.append(r2.A05());
            A0h.append(";");
            r2.A0H(C12960it.A0d(this.A02.A0A, A0h));
            this.A0E = true;
            A3I();
        }

        @Override // X.AbstractC124835qC
        public void AQh(ViewGroup viewGroup, AbstractC28901Pl r3) {
            AbstractActivityC119235dO.A0p(getLayoutInflater(), viewGroup, this);
        }

        @Override // X.AbstractC136456Mp
        public void AQj() {
            Intent A02 = IndiaUpiPinPrimerFullSheetActivity.A02(this, (C30861Zc) this.A02, true);
            A2v(A02);
            A2E(A02, 1017);
        }

        @Override // X.AbstractC136456Mp
        public void AQk() {
            this.A08.A1K();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
            if (r2.A02 == null) goto L_0x006b;
         */
        @Override // X.AnonymousClass6MS
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void ARr(X.C452120p r9, java.lang.String r10) {
            /*
            // Method dump skipped, instructions count: 270
            */
            throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121655j9.ARr(X.20p, java.lang.String):void");
        }

        @Override // X.AnonymousClass6N0
        public void ATW(PaymentBottomSheet paymentBottomSheet, int i) {
            PaymentMethodsListPickerFragment A00 = PaymentMethodsListPickerFragment.A00(this.A0D);
            A00.A08 = new C121855kA(this);
            A00.A06 = this;
            C117315Zl.A0P(A00, paymentBottomSheet.A01, paymentBottomSheet);
        }

        @Override // X.AnonymousClass6M1
        public void ATY(AbstractC28901Pl r1) {
            this.A02 = r1;
        }

        @Override // X.AnonymousClass6N0
        public void ATZ(AbstractC28901Pl r2, PaymentMethodRow paymentMethodRow) {
            if (this instanceof IndiaUpiMandatePaymentActivity) {
                this.A02 = r2;
            }
        }

        @Override // X.AnonymousClass1P3
        public void AVM(boolean z) {
            if (z) {
                A3L(this.A08);
            }
        }

        @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
        public void onActivityResult(int i, int i2, Intent intent) {
            PaymentBottomSheet paymentBottomSheet;
            String str;
            if (i != 155) {
                switch (i) {
                    case 1015:
                        return;
                    case 1016:
                        if (i2 == -1 && intent != null) {
                            AbstractC28901Pl r0 = (AbstractC28901Pl) intent.getParcelableExtra("extra_bank_account");
                            if (r0 != null) {
                                this.A02 = r0;
                            }
                            C18600si r2 = ((AbstractActivityC121665jA) this).A0C;
                            StringBuilder A0h = C12960it.A0h();
                            A0h.append(r2.A05());
                            A0h.append(";");
                            r2.A0H(C12960it.A0d(this.A02.A0A, A0h));
                            paymentBottomSheet = this.A08;
                            str = "IndiaUpiPinPrimerDialogFragment";
                            break;
                        } else {
                            return;
                        }
                        break;
                    case 1017:
                        if (i2 == -1) {
                            C18600si r22 = ((AbstractActivityC121665jA) this).A0C;
                            StringBuilder A0h2 = C12960it.A0h();
                            A0h2.append(r22.A05());
                            A0h2.append(";");
                            r22.A0H(C12960it.A0d(this.A02.A0A, A0h2));
                            paymentBottomSheet = this.A08;
                            str = "IndiaUpiForgotPinDialogFragment";
                            break;
                        } else {
                            return;
                        }
                    case 1018:
                        if (TextUtils.isEmpty(this.A0B)) {
                            A2C(R.string.register_wait_message);
                            A3K(this.A04);
                            return;
                        }
                        A3L(this.A08);
                        return;
                    default:
                        super.onActivityResult(i, i2, intent);
                        return;
                }
                A3O(paymentBottomSheet, str);
                AbstractC28901Pl r3 = this.A02;
                Intent A0D = C12990iw.A0D(this, IndiaUpiPinSetUpCompletedActivity.class);
                C117315Zl.A0M(A0D, r3);
                A0D.putExtra("on_settings_page", false);
                A2E(A0D, 1018);
            } else if (i2 == -1) {
                A3I();
            }
        }

        @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            this.A05.A03(this.A0G);
        }

        @Override // X.AbstractActivityC121545iU, android.app.Activity
        public Dialog onCreateDialog(int i) {
            if (i != 34) {
                return super.onCreateDialog(i);
            }
            C004802e A0S = C12980iv.A0S(this);
            A0S.A06(R.string.payments_change_of_receiver_not_allowed);
            C12970iu.A1I(A0S);
            A0S.A04(new IDxDListenerShape14S0100000_3_I1(this, 9));
            return A0S.create();
        }

        @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
        public void onDestroy() {
            super.onDestroy();
            this.A05.A04(this.A0G);
        }
    }
