package X;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122425lR extends AbstractC118825cR {
    public Button A00;
    public ImageView A01;
    public TextView A02;

    public C122425lR(View view) {
        super(view);
        this.A01 = C12970iu.A0K(view, R.id.warning_icon);
        this.A02 = C12960it.A0I(view, R.id.warning_message);
        this.A00 = (Button) AnonymousClass028.A0D(view, R.id.cta_button);
    }
}
