package X;

import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.GalleryPickerFragment;
import java.util.ArrayList;

/* renamed from: X.2gY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54332gY extends AnonymousClass02M {
    public final ArrayList A00 = C12960it.A0l();
    public final /* synthetic */ GalleryPickerFragment A01;

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        return (long) i;
    }

    public C54332gY(GalleryPickerFragment galleryPickerFragment) {
        this.A01 = galleryPickerFragment;
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        if (r8 != 9) goto L_0x0047;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0057  */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void ANH(X.AnonymousClass03U r10, int r11) {
        /*
            r9 = this;
            X.2hy r10 = (X.View$OnClickListenerC55212hy) r10
            java.util.ArrayList r0 = r9.A00
            java.lang.Object r7 = r0.get(r11)
            X.4TC r7 = (X.AnonymousClass4TC) r7
            r10.A00 = r7
            android.widget.TextView r3 = r10.A03
            com.whatsapp.gallerypicker.GalleryPickerFragment r6 = r10.A05
            X.018 r0 = r6.A0C
            java.text.NumberFormat r2 = r0.A0J()
            int r0 = r7.A00
            long r0 = (long) r0
            java.lang.String r0 = r2.format(r0)
            r3.setText(r0)
            com.whatsapp.TextEmojiLabel r2 = r10.A04
            java.lang.String r1 = r7.A05
            r0 = 0
            r2.A0G(r0, r1)
            android.widget.ImageView r2 = r10.A01
            int r8 = r7.A02
            if (r8 == 0) goto L_0x0080
            r0 = 1
            if (r8 == r0) goto L_0x007c
            r0 = 2
            if (r8 == r0) goto L_0x0047
            r0 = 4
            if (r8 == r0) goto L_0x0080
            r0 = 5
            if (r8 == r0) goto L_0x007c
            r0 = 6
            if (r8 == r0) goto L_0x0047
            r0 = 7
            if (r8 == r0) goto L_0x0080
            r1 = 9
            r0 = 2131231552(0x7f080340, float:1.8079188E38)
            if (r8 == r1) goto L_0x004a
        L_0x0047:
            r0 = 2131231550(0x7f08033e, float:1.8079184E38)
        L_0x004a:
            r2.setImageResource(r0)
            android.widget.ImageView r5 = r10.A02
            java.lang.Object r4 = r5.getTag()
            X.23D r4 = (X.AnonymousClass23D) r4
            if (r4 == 0) goto L_0x0084
            java.lang.String r3 = r4.AH5()
            java.lang.StringBuilder r2 = X.C12960it.A0h()
            r2.append(r8)
            java.lang.String r1 = "-"
            r2.append(r1)
            java.lang.String r0 = r7.A04
            r2.append(r0)
            r2.append(r1)
            int r0 = r7.A01
            java.lang.String r0 = X.C12960it.A0f(r2, r0)
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0084
            return
        L_0x007c:
            r0 = 2131231551(0x7f08033f, float:1.8079186E38)
            goto L_0x004a
        L_0x0080:
            r0 = 2131231549(0x7f08033d, float:1.8079182E38)
            goto L_0x004a
        L_0x0084:
            X.22x r0 = r6.A0H
            r0.A01(r4)
            X.1iN r0 = r7.A03
            X.3X5 r2 = new X.3X5
            r2.<init>(r0, r7, r10)
            X.3XC r1 = new X.3XC
            r1.<init>(r0, r10, r2)
            r5.setTag(r2)
            X.22x r0 = r6.A0H
            r0.A02(r2, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54332gY.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        GalleryPickerFragment galleryPickerFragment = this.A01;
        return new View$OnClickListenerC55212hy(C12980iv.A0O(galleryPickerFragment.A04(), R.layout.gallery_picker_item), galleryPickerFragment);
    }
}
