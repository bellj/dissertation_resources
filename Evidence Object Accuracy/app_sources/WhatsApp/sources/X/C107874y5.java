package X;

import android.content.Context;

/* renamed from: X.4y5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107874y5 implements AbstractC47452At {
    public final Context A00;
    public final AbstractC47452At A01;

    public C107874y5(Context context) {
        C107864y4 r1 = new C107864y4();
        r1.A00 = null;
        this.A00 = context.getApplicationContext();
        this.A01 = r1;
    }

    public C107874y5(Context context, String str) {
        C107864y4 r1 = new C107864y4();
        r1.A00 = str;
        this.A00 = context.getApplicationContext();
        this.A01 = r1;
    }

    @Override // X.AbstractC47452At
    public /* bridge */ /* synthetic */ AnonymousClass2BW A8D() {
        return new C67793Sx(this.A00, this.A01.A8D());
    }
}
