package X;

/* renamed from: X.14N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14N {
    public static AnonymousClass14N A00;

    public static synchronized AnonymousClass14N A00() {
        AnonymousClass14N r0;
        synchronized (AnonymousClass14N.class) {
            r0 = A00;
            if (r0 == null) {
                r0 = new AnonymousClass14N();
                A00 = r0;
            }
        }
        return r0;
    }
}
