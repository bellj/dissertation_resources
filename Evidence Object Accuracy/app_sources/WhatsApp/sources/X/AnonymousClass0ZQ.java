package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.0ZQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZQ implements AbstractC12420ht {
    public AnonymousClass0B8 A00;

    public AnonymousClass0ZQ() {
    }

    public AnonymousClass0ZQ(Context context, View view, ViewGroup viewGroup) {
        this.A00 = new AnonymousClass0B8(context, view, viewGroup, this);
    }

    @Override // X.AbstractC12420ht
    public void A5b(View view) {
        this.A00.A00(view);
    }

    @Override // X.AbstractC12420ht
    public void AaF(View view) {
        this.A00.A01(view);
    }
}
