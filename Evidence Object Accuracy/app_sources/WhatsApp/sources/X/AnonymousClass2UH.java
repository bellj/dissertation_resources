package X;

import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.calling.views.VoipCallControlBottomSheetDragIndicator;
import com.whatsapp.camera.DragBottomSheetIndicator;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.doodle.DoodleView;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* renamed from: X.2UH  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2UH {
    public void A00(View view, float f) {
        View findViewById;
        int i;
        AbstractView$OnCreateContextMenuListenerC35851ir r2;
        int i2;
        float f2;
        float f3;
        int i3;
        int i4;
        int i5;
        if (this instanceof C56782lf) {
            C56782lf r22 = (C56782lf) this;
            int i6 = (f > 0.95f ? 1 : (f == 0.95f ? 0 : -1));
            AbstractC33621eg r3 = r22.A02;
            if (i6 > 0) {
                r3.A0G();
            } else {
                r3.A0N(false, false);
            }
            View view2 = r22.A01.A03;
            float f4 = 1.0f;
            if (f < 0.5f) {
                f4 = 2.0f * f;
            }
            view2.setAlpha(f4);
            AbstractC116205Un r5 = (AbstractC116205Un) r3.A0L.A01.A0B();
            if (r5 != null) {
                ActivityC000800j r52 = (ActivityC000800j) r5;
                float f5 = 0.0f;
                if (f >= 0.9f) {
                    f5 = 1.0f - ((1.0f - f) * 10.0f);
                }
                View findViewById2 = r52.findViewById(R.id.video_playback_container_overlay);
                findViewById2.setVisibility(0);
                findViewById2.setBackgroundColor(((int) (f5 * 255.0f)) << 24);
                r52.findViewById(R.id.video_playback_container).setVisibility(4);
            }
            if (!r22.A00) {
                r3.A0A().A09(true);
                r22.A00 = true;
            }
        } else if (this instanceof C56752lc) {
            AbstractC33621eg r4 = ((C56752lc) this).A00;
            C35571iH A0B = r4.A0B();
            A0B.A06.setVisibility(0);
            A0B.A06.setAlpha(f);
            A0B.A0A.setAlpha(1.0f - ((float) Math.pow((double) f, 4.0d)));
            if (r4.A0O()) {
                int i7 = (f > 0.0f ? 1 : (f == 0.0f ? 0 : -1));
                boolean z = r4.A07;
                if (i7 != 0) {
                    if (z) {
                        r4.A07 = false;
                        r4.A0M(false);
                    }
                } else if (!z) {
                    r4.A07 = true;
                    r4.A0M(true);
                }
            }
            r4.A0G();
        } else if (this instanceof C56742lb) {
            C56742lb r23 = (C56742lb) this;
            AnonymousClass21U r42 = r23.A00;
            if (r42.A07() && r42.A0F) {
                r23.A01(view, 1);
                r42.A0F = false;
            } else if (r42.A08() && r42.A0G) {
                r23.A01(view, 1);
                r42.A0G = false;
            }
            AnonymousClass21Y r0 = (AnonymousClass21Y) r42.A0V.A01.A0B();
            if (r0 != null) {
                ((MediaComposerActivity) r0).A0e.A04.setAlpha(1.0f - f);
            }
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) r42.A0Q.getLayoutManager();
            if (linearLayoutManager != null) {
                float f6 = (0.35f * f) + 0.65f;
                float f7 = (0.45f * f) + 0.55f;
                int A1C = linearLayoutManager.A1C();
                for (int A1A = linearLayoutManager.A1A(); A1A <= A1C; A1A++) {
                    View A0C = linearLayoutManager.A0C(A1A);
                    if (A0C != null) {
                        A0C.setScaleX(f6);
                        A0C.setScaleY(f6);
                        if (A1A == r42.A01 && (findViewById = A0C.findViewById(R.id.selection_check)) != null) {
                            findViewById.setScaleX(f7);
                            findViewById.setScaleY(f7);
                        }
                    }
                }
            }
            float f8 = 1.0f - (f * r42.A00);
            View view3 = r42.A06;
            view3.setScaleX(f8);
            view3.setScaleY(f8);
            DoodleView doodleView = r42.A09.A0H;
            doodleView.setScaleX(f8);
            doodleView.setScaleY(f8);
        } else if (this instanceof C56732la) {
            float height = ((float) view.getHeight()) * f;
            AbstractView$OnCreateContextMenuListenerC35851ir r24 = ((C56732la) this).A00;
            BottomSheetBehavior bottomSheetBehavior = r24.A0a;
            if (bottomSheetBehavior.A0M) {
                i = -1;
            } else {
                i = bottomSheetBehavior.A09;
            }
            AbstractView$OnCreateContextMenuListenerC35851ir.A02(r24, ((float) i) + height, false);
        } else if (this instanceof AnonymousClass2lZ) {
            AnonymousClass2lZ r43 = (AnonymousClass2lZ) this;
            if (f >= 0.0f) {
                int height2 = view.getHeight();
                r2 = r43.A00;
                BottomSheetBehavior bottomSheetBehavior2 = r2.A0Z;
                if (bottomSheetBehavior2.A0M) {
                    i3 = -1;
                } else {
                    i3 = bottomSheetBehavior2.A09;
                }
                f3 = ((float) (height2 - i3)) * f;
                f2 = (float) i3;
            } else {
                if (!Float.isNaN(f)) {
                    r2 = r43.A00;
                    BottomSheetBehavior bottomSheetBehavior3 = r2.A0Z;
                    if (bottomSheetBehavior3.A0M) {
                        i2 = -1;
                    } else {
                        i2 = bottomSheetBehavior3.A09;
                    }
                    f2 = (float) i2;
                    f3 = f2 * f;
                }
                r43.A00.A0g.setOffset(f);
            }
            r2.A0N(f2 + f3, false);
            r43.A00.A0g.setOffset(f);
        } else if (this instanceof C56772le) {
            View A0D = AnonymousClass028.A0D(view, R.id.topHandle);
            if (((double) f) > 0.7d && f < 1.0f) {
                A0D.setAlpha(1.0f - f);
            } else if (f == 1.0f) {
                A0D.setAlpha(0.0f);
                A0D.setVisibility(8);
            }
        } else if (!(this instanceof AnonymousClass2UG)) {
            C64523Fw r25 = ((C56722lY) this).A00;
            VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2 = r25.A0G;
            voipCallControlBottomSheetV2.A1N(f);
            VoipCallControlBottomSheetDragIndicator voipCallControlBottomSheetDragIndicator = voipCallControlBottomSheetV2.A0L;
            if (voipCallControlBottomSheetDragIndicator != null) {
                voipCallControlBottomSheetDragIndicator.setSlideOffset(f);
            }
            if (f <= 0.0f) {
                if (r25.A01 > 0) {
                    i5 = view.getTop() - r25.A01;
                } else {
                    BottomSheetBehavior bottomSheetBehavior4 = r25.A0F;
                    if (bottomSheetBehavior4.A0M) {
                        i4 = -1;
                    } else {
                        i4 = bottomSheetBehavior4.A09;
                    }
                    i5 = (int) (((float) (-i4)) * f);
                }
                voipCallControlBottomSheetV2.A1P(i5, f);
                r25.A01 = view.getTop();
            }
        } else {
            AnonymousClass2UG r44 = (AnonymousClass2UG) this;
            AnonymousClass1s8 r1 = r44.A02;
            float f9 = 1.0f - f;
            r1.A03.setAlpha(f9);
            r1.A0E.A00.setBackgroundColor(((int) (255.0f * f)) << 24);
            C49632Lo r26 = r44.A03;
            if (r26.A06.getConfiguration().orientation != 2) {
                RecyclerView recyclerView = r26.A09;
                recyclerView.setVisibility(0);
                recyclerView.setAlpha(f9);
            }
            r26.A08.setAlpha(f);
            DragBottomSheetIndicator dragBottomSheetIndicator = r26.A02;
            if (dragBottomSheetIndicator != null) {
                dragBottomSheetIndicator.setOffset(f);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0328, code lost:
        if (r18 == 2) goto L_0x032a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0043, code lost:
        if (r18 != 5) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008f, code lost:
        if (r10.A0F.A0B != 4) goto L_0x0091;
     */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0253  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x0327  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0330 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:300:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.view.View r17, int r18) {
        /*
        // Method dump skipped, instructions count: 1416
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2UH.A01(android.view.View, int):void");
    }
}
