package X;

/* renamed from: X.0J6  reason: invalid class name */
/* loaded from: classes.dex */
public enum AnonymousClass0J6 {
    NONE,
    /* Fake field, exist only in values array */
    ADD,
    INVERT,
    /* Fake field, exist only in values array */
    LUMA,
    /* Fake field, exist only in values array */
    LUMA_INVERTED,
    /* Fake field, exist only in values array */
    UNKNOWN
}
