package X;

/* renamed from: X.21M  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass21M extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public Integer A03;
    public Integer A04;
    public Integer A05;
    public Long A06;
    public Long A07;
    public Long A08;

    public AnonymousClass21M() {
        super(2244, new AnonymousClass00E(1, 1, 100), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(6, this.A02);
        r3.Abe(3, this.A06);
        r3.Abe(1, this.A03);
        r3.Abe(2, this.A07);
        r3.Abe(11, this.A08);
        r3.Abe(10, this.A00);
        r3.Abe(4, this.A04);
        r3.Abe(9, this.A05);
        r3.Abe(5, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        StringBuilder sb = new StringBuilder("WamPsBufferUpload {");
        Integer num = this.A02;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "applicationState", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psBufferUploadHttpResponseCode", this.A06);
        Integer num2 = this.A03;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psBufferUploadResult", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psBufferUploadT", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psDitheredT", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psForceUpload", this.A00);
        Integer num3 = this.A04;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psTokenNotReadyReason", obj3);
        Integer num4 = this.A05;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psUploadReason", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "waConnectedToChatd", this.A01);
        sb.append("}");
        return sb.toString();
    }
}
