package X;

/* renamed from: X.30c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C613730c extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Integer A02;
    public String A03;

    public C613730c() {
        super(1844, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(4, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStickerPackDownload {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPackDownloadOrigin", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPackIsAvatar", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stickerPackIsFirstParty", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "waAvatarSessionId", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
