package X;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URLConnection;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.WeakHashMap;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

/* renamed from: X.4dG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95174dG {
    public static Map A00 = Collections.synchronizedMap(new WeakHashMap());

    public static Collection A00(URI uri, CertificateFactory certificateFactory) {
        Hashtable hashtable = new Hashtable();
        hashtable.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
        hashtable.put("java.naming.provider.url", uri.toString());
        try {
            byte[] bArr = (byte[]) new InitialDirContext(hashtable).getAttributes("").get("certificateRevocationList;binary").get();
            if (bArr != null && bArr.length != 0) {
                return certificateFactory.generateCRLs(new ByteArrayInputStream(bArr));
            }
            throw new CRLException(C12960it.A0b("no CRL returned from: ", uri));
        } catch (NamingException e) {
            throw new CRLException(C12960it.A0d(uri.toString(), C12960it.A0k("issue connecting to: ")), e);
        }
    }

    public static synchronized C113115Gb A01(URI uri, CertificateFactory certificateFactory, Date date) {
        Collection<? extends CRL> generateCRLs;
        C113115Gb r3;
        synchronized (C95174dG.class) {
            Map map = A00;
            WeakReference weakReference = (WeakReference) map.get(uri);
            if (!(weakReference == null || (r3 = (C113115Gb) weakReference.get()) == null)) {
                for (X509CRL x509crl : C12980iv.A0x(r3.A00)) {
                    Date nextUpdate = x509crl.getNextUpdate();
                    if (nextUpdate == null || !nextUpdate.before(date)) {
                    }
                }
                return r3;
            }
            if (uri.getScheme().equals("ldap")) {
                generateCRLs = A00(uri, certificateFactory);
            } else {
                URLConnection openConnection = uri.toURL().openConnection();
                openConnection.setConnectTimeout(15000);
                openConnection.setReadTimeout(15000);
                InputStream inputStream = openConnection.getInputStream();
                generateCRLs = certificateFactory.generateCRLs(inputStream);
                inputStream.close();
            }
            C113115Gb r1 = new C113115Gb(new C113125Gc(generateCRLs));
            map.put(uri, C12970iu.A10(r1));
            return r1;
        }
    }
}
