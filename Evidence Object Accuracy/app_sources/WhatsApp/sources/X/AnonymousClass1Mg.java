package X;

import com.whatsapp.jid.UserJid;
import java.util.AbstractCollection;

/* renamed from: X.1Mg  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Mg {
    public int A00;
    public long A01 = 0;
    public long A02 = 0;
    public long A03 = 0;
    public C47822Cw A04;
    public String A05;
    public String A06;
    public String A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;
    public boolean A0D;
    public boolean A0E;
    public boolean A0F;
    public boolean A0G;
    public boolean A0H;
    public boolean A0I;
    public final C15370n3 A0J;
    public final UserJid A0K;
    public final String A0L;

    public AnonymousClass1Mg(C15370n3 r3) {
        String str;
        this.A0K = (UserJid) r3.A0B(UserJid.class);
        this.A0J = r3;
        C28811Pc r0 = r3.A0C;
        if (r0 != null) {
            str = r0.A01;
        } else {
            str = null;
        }
        this.A0L = str;
        if (r3.A0f) {
            this.A03 = r3.A0B;
        }
    }

    public AnonymousClass1Mg(UserJid userJid, String str) {
        this.A0K = userJid;
        this.A0L = str;
        this.A0J = null;
    }

    public static void A00(AnonymousClass1Mg r0, AbstractCollection abstractCollection) {
        abstractCollection.add(r0.A01());
    }

    public C42101uf A01() {
        boolean z = this.A0K instanceof AnonymousClass1MU;
        if (z) {
            this.A0D = true;
        }
        boolean z2 = this.A0E;
        boolean z3 = true;
        if (!z) {
            z3 = false;
        }
        this.A0E = (!z3) & z2;
        return new C42101uf(this);
    }
}
