package X;

import java.util.Map;

/* renamed from: X.5fS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC120005fS extends AnonymousClass12X {
    /* renamed from: A01 */
    public String A00(C91064Qh r4, Map map) {
        if (!map.containsKey(190)) {
            return null;
        }
        r4.A00 = 8;
        return ((AnonymousClass3H5) map.get(190)).A01;
    }
}
