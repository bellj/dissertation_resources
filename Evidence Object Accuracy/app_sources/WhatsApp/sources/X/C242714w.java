package X;

import android.database.Cursor;

/* renamed from: X.14w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C242714w {
    public final C16490p7 A00;

    public C242714w(C16490p7 r1) {
        this.A00 = r1;
    }

    public AnonymousClass1V1 A00(AbstractC15340mz r10) {
        AnonymousClass1V1 r6 = new AnonymousClass1V1(r10);
        if (r10.A11 == -1) {
            return r6;
        }
        C16310on A01 = this.A00.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT duration, campaign_id, first_seen_timestamp, action_link_url, action_link_button_title FROM message_status_psa_campaign WHERE message_row_id = ?", new String[]{Long.toString(r10.A11)});
            if (A09 != null) {
                if (A09.moveToNext()) {
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("campaign_id");
                    int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("duration");
                    int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("first_seen_timestamp");
                    int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("action_link_url");
                    int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("action_link_button_title");
                    r6.A04 = A09.getString(columnIndexOrThrow);
                    r6.A00 = A09.getLong(columnIndexOrThrow2);
                    r6.A01 = A09.getLong(columnIndexOrThrow3);
                    r6.A03 = A09.getString(columnIndexOrThrow4);
                    r6.A02 = A09.getString(columnIndexOrThrow5);
                }
                A09.close();
            }
            A01.close();
            return r6;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
