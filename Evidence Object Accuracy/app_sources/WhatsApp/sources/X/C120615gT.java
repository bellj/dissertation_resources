package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.5gT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120615gT extends C120895gv {
    public final /* synthetic */ AnonymousClass60I A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120615gT(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, AnonymousClass60I r11) {
        super(context, r8, r9, r10, "upi-get-accounts");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        Log.i(C12960it.A0b("PAY: IndiaUpiGetBankAccountsAction: sendGetBankAccounts: onRequestError: ", r3));
        AnonymousClass6MR r1 = this.A00.A02;
        if (r1 != null) {
            r1.AN8(r3, null);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        Log.i(C12960it.A0b("PAY: IndiaUpiGetBankAccountsAction: sendGetBankAccounts: onResponseError: ", r3));
        AnonymousClass6MR r1 = this.A00.A02;
        if (r1 != null) {
            r1.AN8(r3, null);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r3) {
        super.A04(r3);
        C12960it.A1E(new C124075oP(this, r3), this.A00.A0I);
    }
}
