package X;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3JI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JI {
    public final C93634aU A00;
    public final AnonymousClass28D A01;
    public final String A02;

    public AnonymousClass3JI(AnonymousClass28D r2, String str, List list, List list2, List list3, Map map) {
        this.A00 = new C93634aU(list, list3, list2, map);
        this.A01 = r2;
        this.A02 = str;
    }

    public static AnonymousClass3JI A00(C14230l4 r6, AnonymousClass28D r7) {
        Object obj;
        if (r6 == null || r7.A01 != 13901) {
            List list = Collections.EMPTY_LIST;
            return new AnonymousClass3JI(r7, null, list, list, list, Collections.EMPTY_MAP);
        }
        AbstractC14200l1 A0G = r7.A0G(35);
        if (A0G != null) {
            try {
                obj = AnonymousClass3AG.A00(C14220l3.A01, A0G, r6);
            } catch (C87444Bn e) {
                C28691Op.A01("BloksParseResult", "Exception executing Parse Embedded expression", e);
                obj = A01(new AnonymousClass28D(13320));
            }
            return (AnonymousClass3JI) obj;
        }
        throw C12970iu.A0f("ParseResultWrapper doesn't have a parse result!");
    }

    public static AnonymousClass3JI A01(AnonymousClass28D r7) {
        List list = Collections.EMPTY_LIST;
        return new AnonymousClass3JI(r7, null, list, list, list, Collections.EMPTY_MAP);
    }

    public static AnonymousClass3JI A02(C63433Bo r7) {
        String str;
        Map A03 = A03(r7.A05);
        AnonymousClass28D r1 = r7.A00;
        List list = r7.A04;
        List list2 = r7.A03;
        List list3 = r7.A06;
        if (list3 == null) {
            list3 = Collections.emptyList();
        }
        C89064In r0 = r7.A01;
        if (r0 != null) {
            str = r0.A00;
        } else {
            str = null;
        }
        return new AnonymousClass3JI(r1, str, list, list2, list3, A03);
    }

    public static Map A03(List list) {
        if (list == null) {
            return Collections.EMPTY_MAP;
        }
        HashMap hashMap = new HashMap(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C64573Gb r1 = (C64573Gb) it.next();
            hashMap.put(r1.A01, r1);
        }
        return hashMap;
    }

    public static Map A04(List list, Map map) {
        HashMap A11 = C12970iu.A11();
        HashMap hashMap = new HashMap(map);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C93344a1 r2 = (C93344a1) it.next();
            if (map.containsKey(r2.A01)) {
                A11.put(r2.A00, hashMap.remove(r2.A01));
            }
        }
        A11.putAll(hashMap);
        return A11;
    }

    public AnonymousClass28D A05() {
        return this.A01;
    }
}
