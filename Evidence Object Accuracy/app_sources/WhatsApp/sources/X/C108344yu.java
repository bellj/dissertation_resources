package X;

import android.os.Bundle;
import java.util.concurrent.locks.Lock;

/* renamed from: X.4yu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108344yu implements AbstractC116625We {
    public final /* synthetic */ C108354yv A00;

    public /* synthetic */ C108344yu(C108354yv r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116625We
    public final void AgN(C56492ky r3) {
        C108354yv r0 = this.A00;
        Lock lock = r0.A0D;
        lock.lock();
        try {
            r0.A03 = r3;
            C108354yv.A00(r0);
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AbstractC116625We
    public final void AgP(Bundle bundle) {
        C108354yv r2 = this.A00;
        Lock lock = r2.A0D;
        lock.lock();
        try {
            r2.A03 = C56492ky.A04;
            C108354yv.A00(r2);
        } finally {
            lock.unlock();
        }
    }

    @Override // X.AbstractC116625We
    public final void AgS(int i, boolean z) {
        C108354yv r3 = this.A00;
        Lock lock = r3.A0D;
        lock.lock();
        try {
            if (r3.A04) {
                r3.A04 = false;
                r3.A08.AgS(i, false);
                r3.A03 = null;
                r3.A02 = null;
            } else {
                r3.A04 = true;
                r3.A09.onConnectionSuspended(i);
            }
        } finally {
            lock.unlock();
        }
    }
}
