package X;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import android.webkit.MimeTypeMap;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/* renamed from: X.0yh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22200yh {
    public static int A00 = -1;
    public static String A01;
    public static final BitmapFactory.Options A02;
    public static final DecimalFormat A03 = new DecimalFormat("0000", new DecimalFormatSymbols(Locale.US));

    static {
        BitmapFactory.Options options = new BitmapFactory.Options();
        A02 = options;
        options.inDither = true;
        options.inInputShareable = true;
        options.inPurgeable = true;
    }

    public static byte A04(String str) {
        if (str == null) {
            return -1;
        }
        if (str.startsWith("audio")) {
            return 2;
        }
        if (str.startsWith("video")) {
            return 3;
        }
        if (str.startsWith("image")) {
            return 1;
        }
        if (str.startsWith("text/x-vcard") || str.startsWith("text/vcard")) {
            return 4;
        }
        return str.startsWith("text") ? (byte) 0 : 9;
    }

    public static int A05(ContentResolver contentResolver, Uri uri) {
        File A032 = C14350lI.A03(uri);
        int i = 0;
        if (A032 != null) {
            i = new AnonymousClass03i(A032.getPath()).A07(1);
        } else if ("content".equals(uri.getScheme())) {
            String[] strArr = {"_data", "orientation"};
            Uri build = uri.buildUpon().query(null).build();
            if (contentResolver != null) {
                try {
                    Cursor query = contentResolver.query(build, strArr, null, null, null);
                    if (query != null) {
                        try {
                            if (!query.moveToFirst()) {
                                Log.e("sample_rotate_image/cursor_is_empty");
                            } else if (query.getColumnCount() == 2) {
                                String string = query.getString(0);
                                if (string != null) {
                                    i = new AnonymousClass03i(string).A07(1);
                                } else {
                                    int i2 = query.getInt(1);
                                    if (i2 == 90) {
                                        i = 6;
                                    } else if (i2 == 180) {
                                        i = 3;
                                    } else if (i2 == 270) {
                                        i = 8;
                                    }
                                }
                            } else {
                                Log.e("sample_rotate_image/no_orientation_info");
                            }
                        } finally {
                            query.close();
                        }
                    }
                } catch (Exception e) {
                    Log.w("sample_rotate_image/query_orientation_info", e);
                }
            } else {
                Log.w("media-file-utils/get-exiff-orientation cr=null");
            }
        }
        StringBuilder sb = new StringBuilder("sample_rotate_image/orientation ");
        sb.append(i);
        Log.i(sb.toString());
        return i;
    }

    public static int A06(C14330lG r8, C14370lK r9, String str, int i, int i2) {
        File A0B = r8.A0B(r9.A00, i, i2);
        int i3 = 0;
        int i4 = -1;
        if (A0B.exists()) {
            File[] listFiles = A0B.listFiles();
            if (listFiles != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(r9.A01);
                sb.append("-");
                sb.append(str);
                sb.append("-WA");
                String obj = sb.toString();
                int length = listFiles.length;
                while (i3 < length) {
                    String name = listFiles[i3].getName();
                    if (name.startsWith(obj) && name.length() > 19) {
                        try {
                            int parseInt = Integer.parseInt(name.substring(15, 19));
                            if (parseInt > i4) {
                                i4 = parseInt;
                            }
                        } catch (NumberFormatException e) {
                            StringBuilder sb2 = new StringBuilder("mediafileutils/findlargestfileindex/nfe:");
                            sb2.append(name);
                            Log.i(sb2.toString(), e);
                        }
                    }
                    i3++;
                }
                i3 = length;
            } else {
                StringBuilder sb3 = new StringBuilder("mediafileutils/findlargestfileindex/no files for mmsType:");
                sb3.append(r9);
                Log.i(sb3.toString());
            }
        }
        StringBuilder sb4 = new StringBuilder("mediafileutils/findlargestfileindex mmsType:");
        sb4.append(r9);
        sb4.append(" fileIndex:");
        sb4.append(i4);
        sb4.append(" total:");
        sb4.append(i3);
        Log.i(sb4.toString());
        return i4;
    }

    public static int A07(File file) {
        long A08 = A08(file);
        int i = (int) (A08 / 1000);
        if (i != 0 || A08 == 0) {
            return i;
        }
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        if (r1 != null) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long A08(java.io.File r5) {
        /*
            r2 = 0
            if (r5 == 0) goto L_0x005c
            boolean r0 = r5.exists()
            if (r0 == 0) goto L_0x005c
            java.lang.String r1 = r5.getName()
            java.lang.String r0 = ".opus"
            boolean r0 = r1.endsWith(r0)
            if (r0 == 0) goto L_0x0032
            r1 = 0
            r0 = 3
            X.1Ol r1 = X.AbstractC28651Ol.A00(r1, r1, r5, r0)     // Catch: IOException -> 0x002c, all -> 0x0025
            r1.A05()     // Catch: IOException -> 0x002c, all -> 0x0025
            int r0 = r1.A03()     // Catch: IOException -> 0x002c, all -> 0x0025
            long r2 = (long) r0     // Catch: IOException -> 0x002c, all -> 0x0025
            goto L_0x002e
        L_0x0025:
            r0 = move-exception
            if (r1 == 0) goto L_0x002b
            r1.A06()
        L_0x002b:
            throw r0
        L_0x002c:
            if (r1 == 0) goto L_0x005c
        L_0x002e:
            r1.A06()
            return r2
        L_0x0032:
            X.C38911ou.A03(r5)     // Catch: IOException -> 0x0036
            goto L_0x0055
        L_0x0036:
            X.1ox r4 = new X.1ox     // Catch: Exception -> 0x0056
            r4.<init>()     // Catch: Exception -> 0x0056
            java.lang.String r0 = r5.getAbsolutePath()     // Catch: all -> 0x0050
            r4.setDataSource(r0)     // Catch: all -> 0x0050
            r0 = 9
            java.lang.String r0 = r4.extractMetadata(r0)     // Catch: all -> 0x0050
            long r0 = java.lang.Long.parseLong(r0)     // Catch: all -> 0x0050
            r4.close()     // Catch: Exception -> 0x0056
            return r0
        L_0x0050:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0054
        L_0x0054:
            throw r0     // Catch: Exception -> 0x0056
        L_0x0055:
            return r2
        L_0x0056:
            r1 = move-exception
            java.lang.String r0 = "getmediadurationseconds"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x005c:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22200yh.A08(java.io.File):long");
    }

    public static Bitmap A09(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 21) {
            return bitmap;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, A02);
    }

    public static Bitmap A0A(Bitmap bitmap, float f, int i) {
        Rect rect;
        if (bitmap == null || bitmap.isRecycled()) {
            return null;
        }
        bitmap.getWidth();
        bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        float f2 = (float) i;
        RectF rectF = new RectF(0.0f, 0.0f, f2, f2);
        int width = (bitmap.getWidth() - bitmap.getHeight()) >> 1;
        if (width > 0) {
            rect = new Rect(width, 0, bitmap.getWidth() - width, bitmap.getHeight());
        } else {
            rect = new Rect(0, -width, bitmap.getWidth(), bitmap.getHeight() + width);
        }
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-1);
        if (f >= 0.0f) {
            canvas.drawRoundRect(rectF, f, f, paint);
        } else {
            canvas.drawArc(rectF, 0.0f, 360.0f, true, paint);
        }
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rectF, paint);
        return createBitmap;
    }

    public static Bitmap A0B(Bitmap bitmap, Matrix matrix, int i, int i2) {
        Bitmap bitmap2 = bitmap;
        if (matrix != null) {
            Log.i("sample_rotate_image/rotate");
            try {
                Bitmap createBitmap = Bitmap.createBitmap(bitmap2, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                if (bitmap2 != createBitmap) {
                    bitmap2.recycle();
                }
                bitmap2 = createBitmap;
            } catch (OutOfMemoryError e) {
                Log.e("sample_rotate_image/rotate/out-of-memory");
                bitmap2.recycle();
                throw e;
            }
        }
        if (i2 <= 0 || (bitmap2.getWidth() <= i2 && bitmap2.getHeight() <= i2)) {
            return bitmap2;
        }
        StringBuilder sb = new StringBuilder("sample_rotate_image/scale/");
        sb.append(bitmap2.getWidth());
        sb.append(" | ");
        sb.append(bitmap2.getHeight());
        Log.i(sb.toString());
        float f = (float) i;
        float max = Math.max(((float) bitmap2.getWidth()) / f, ((float) bitmap2.getHeight()) / f);
        Rect rect = new Rect(0, 0, (int) (((float) bitmap2.getWidth()) / max), (int) (((float) bitmap2.getHeight()) / max));
        rect.right = Math.max(rect.right, 1);
        rect.bottom = Math.max(rect.bottom, 1);
        Rect rect2 = new Rect(0, 0, bitmap2.getWidth(), bitmap2.getHeight());
        Bitmap.Config config = bitmap2.getConfig();
        try {
            int width = rect.width();
            int height = rect.height();
            if (config == null) {
                config = Bitmap.Config.ARGB_8888;
            }
            Bitmap createBitmap2 = Bitmap.createBitmap(width, height, config);
            Canvas canvas = new Canvas(createBitmap2);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setFilterBitmap(true);
            paint.setDither(true);
            canvas.drawBitmap(bitmap2, rect2, rect, paint);
            bitmap2.recycle();
            return createBitmap2;
        } catch (OutOfMemoryError e2) {
            Log.e("sample_rotate_image/scale/out-of-memory");
            bitmap2.recycle();
            throw e2;
        }
    }

    public static Matrix A0C(int i) {
        Matrix matrix;
        float[] fArr;
        Matrix matrix2;
        float f;
        switch (i) {
            case 2:
                matrix = new Matrix();
                fArr = new float[]{-1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
                matrix.setValues(fArr);
                return matrix;
            case 3:
                matrix2 = new Matrix();
                f = 180.0f;
                matrix2.setRotate(f);
                return matrix2;
            case 4:
                matrix = new Matrix();
                fArr = new float[]{1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
                matrix.setValues(fArr);
                return matrix;
            case 5:
                matrix = new Matrix();
                fArr = new float[]{0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
                matrix.setValues(fArr);
                return matrix;
            case 6:
                matrix2 = new Matrix();
                f = 90.0f;
                matrix2.setRotate(f);
                return matrix2;
            case 7:
                matrix = new Matrix();
                fArr = new float[]{0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
                matrix.setValues(fArr);
                return matrix;
            case 8:
                matrix2 = new Matrix();
                f = 270.0f;
                matrix2.setRotate(f);
                return matrix2;
            default:
                return null;
        }
    }

    public static Matrix A0D(ContentResolver contentResolver, Uri uri) {
        int parseInt;
        if (uri == null || TextUtils.isEmpty(uri.toString())) {
            StringBuilder sb = new StringBuilder("No file ");
            sb.append(uri);
            throw new FileNotFoundException(sb.toString());
        }
        Matrix A0C = A0C(A05(contentResolver, uri));
        if (uri.getQueryParameter("flip-h") != null) {
            Matrix matrix = new Matrix();
            matrix.setValues(new float[]{-1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f});
            if (A0C == null) {
                A0C = matrix;
            } else {
                A0C.postConcat(matrix);
            }
        }
        if (uri.getQueryParameter("flip-v") != null) {
            Matrix matrix2 = new Matrix();
            matrix2.setValues(new float[]{1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f});
            if (A0C == null) {
                A0C = matrix2;
            } else {
                A0C.postConcat(matrix2);
            }
        }
        String queryParameter = uri.getQueryParameter("rotation");
        if (!(queryParameter == null || (parseInt = Integer.parseInt(queryParameter)) == 0)) {
            if (A0C == null) {
                A0C = new Matrix();
            }
            A0C.postRotate((float) parseInt);
        }
        return A0C;
    }

    public static Pair A0E(File file) {
        int A07;
        Integer valueOf;
        int i;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        if (file.exists()) {
            try {
                A07 = new AnonymousClass03i(file.getAbsolutePath()).A07(1);
            } catch (IOException e) {
                Log.e("failure retrieving exif, io exception", e);
            }
            if (A07 != 6 || A07 == 8) {
                valueOf = Integer.valueOf(options.outHeight);
                i = options.outWidth;
            } else {
                valueOf = Integer.valueOf(options.outWidth);
                i = options.outHeight;
            }
            return new Pair(valueOf, Integer.valueOf(i));
        }
        A07 = 0;
        if (A07 != 6) {
        }
        valueOf = Integer.valueOf(options.outHeight);
        i = options.outWidth;
        return new Pair(valueOf, Integer.valueOf(i));
    }

    public static File A0F(C14330lG r6, C16630pM r7, C14370lK r8, File file, int i) {
        StringBuilder sb = new StringBuilder(".");
        sb.append(C14350lI.A07(file.getAbsolutePath()));
        return A0G(r6, r7, r8, sb.toString(), i, 3);
    }

    public static File A0G(C14330lG r8, C16630pM r9, C14370lK r10, String str, int i, int i2) {
        String obj;
        File file = null;
        for (int i3 = 0; i3 < 100; i3++) {
            StringBuilder sb = new StringBuilder();
            synchronized (C22200yh.class) {
                A0Q(r8, r9);
                SharedPreferences.Editor edit = r9.A01(AnonymousClass01V.A07).edit();
                String format = new SimpleDateFormat("yyyyMMdd", Locale.US).format(new Date());
                if (!format.equals(A01)) {
                    A00 = 0;
                    A01 = format;
                    edit.putString("file_date", format);
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(r10.A01);
                sb2.append("-");
                sb2.append(A01);
                sb2.append("-WA");
                sb2.append(A03.format((long) A00));
                obj = sb2.toString();
                int i4 = A00 + 1;
                A00 = i4;
                edit.putInt("file_index", i4);
                edit.apply();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("mediafileutils/readablefilename/");
                sb3.append(obj);
                Log.i(sb3.toString());
            }
            sb.append(obj);
            sb.append(str);
            file = new File(r8.A0B(r10.A00, i, i2), sb.toString());
            if (!file.exists()) {
                break;
            }
        }
        return file;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009b, code lost:
        if (r12 == X.C14370lK.A0T) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e9, code lost:
        if (r12 == X.C14370lK.A0P) goto L_0x00eb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File A0H(X.C14330lG r10, X.C16630pM r11, X.C14370lK r12, java.lang.String r13, java.lang.String r14, int r15, int r16, boolean r17, boolean r18, boolean r19, boolean r20) {
        /*
        // Method dump skipped, instructions count: 298
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22200yh.A0H(X.0lG, X.0pM, X.0lK, java.lang.String, java.lang.String, int, int, boolean, boolean, boolean, boolean):java.io.File");
    }

    public static File A0I(C14330lG r2, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".doodle");
        return r2.A0M(sb.toString());
    }

    public static File A0J(File file, String str) {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            return file2;
        }
        for (int i = 1; i <= 100; i++) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("-");
            sb.append(i);
            File file3 = new File(file, sb.toString());
            if (!file3.exists()) {
                return file3;
            }
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("-");
        sb2.append(UUID.randomUUID().toString());
        return new File(file, sb2.toString());
    }

    public static FileInputStream A0K(File file) {
        for (int i = 0; i < 2; i++) {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException e) {
                if (!e.getMessage().contains("Permission denied")) {
                    throw e;
                } else if (i == 1) {
                    throw e;
                }
            }
        }
        throw new AssertionError("Unreachable code");
    }

    public static String A0L() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String A0M(Uri uri) {
        int lastIndexOf;
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment == null || (lastIndexOf = lastPathSegment.lastIndexOf(46)) == -1) {
            return "";
        }
        return lastPathSegment.substring(lastIndexOf + 1);
    }

    public static String A0N(Uri uri, AnonymousClass01d r2) {
        ContentResolver A0C = r2.A0C();
        if (A0C == null) {
            Log.w("media-file-utils/get-media-mime cr=null");
        } else {
            String type = A0C.getType(uri);
            if (type != null) {
                return type;
            }
        }
        return A0O(A0M(uri));
    }

    public static String A0O(String str) {
        Locale locale = Locale.US;
        String lowerCase = str.toLowerCase(locale);
        switch (lowerCase.hashCode()) {
            case 96323:
                if (lowerCase.equals("aac")) {
                    return "audio/aac";
                }
                break;
            case 96710:
                if (lowerCase.equals("amr")) {
                    return "audio/amr";
                }
                break;
            case 98822:
                if (lowerCase.equals("csv")) {
                    return "text/csv";
                }
                break;
            case 99640:
                if (lowerCase.equals("doc")) {
                    return "application/msword";
                }
                break;
            case 106458:
                if (lowerCase.equals("m4a")) {
                    return "audio/mp4";
                }
                break;
            case 108272:
                if (lowerCase.equals("mp3")) {
                    return "audio/mpeg";
                }
                break;
            case 108273:
                if (lowerCase.equals("mp4")) {
                    return "video/mp4";
                }
                break;
            case 110834:
                if (lowerCase.equals("pdf")) {
                    return "application/pdf";
                }
                break;
            case 111220:
                if (lowerCase.equals("ppt")) {
                    return "application/vnd.ms-powerpoint";
                }
                break;
            case 113252:
                if (lowerCase.equals("rtf")) {
                    return "application/rtf";
                }
                break;
            case 115312:
                if (lowerCase.equals("txt")) {
                    return "text/plain";
                }
                break;
            case 117484:
                if (lowerCase.equals("wav")) {
                    return "audio/x-wav";
                }
                break;
            case 117835:
                if (lowerCase.equals("wma")) {
                    return "audio/x-ms-wma";
                }
                break;
            case 118783:
                if (lowerCase.equals("xls")) {
                    return "application/vnd.ms-excel";
                }
                break;
            case 3088960:
                if (lowerCase.equals("docx")) {
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                }
                break;
            case 3418175:
                if (lowerCase.equals("opus")) {
                    return "audio/ogg; codecs=opus";
                }
                break;
            case 3447940:
                if (lowerCase.equals("pptx")) {
                    return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                }
                break;
            case 3682393:
                if (lowerCase.equals("xlsx")) {
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }
                break;
        }
        String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(str.toLowerCase(locale));
        return mimeTypeFromExtension == null ? "application/octet-stream" : mimeTypeFromExtension;
    }

    public static void A0P(Context context, Uri uri) {
        context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE").setData(uri));
    }

    public static synchronized void A0Q(C14330lG r13, C16630pM r14) {
        synchronized (C22200yh.class) {
            if (A00 < 0) {
                SharedPreferences A012 = r14.A01(AnonymousClass01V.A07);
                A00 = A012.getInt("file_index", -1);
                String string = A012.getString("file_date", null);
                A01 = string;
                if (A00 < 0 || TextUtils.isEmpty(string)) {
                    String format = new SimpleDateFormat("yyyyMMdd", Locale.US).format(new Date());
                    A01 = format;
                    long currentTimeMillis = System.currentTimeMillis();
                    A00 = -1;
                    C14370lK r2 = C14370lK.A05;
                    int max = Math.max(-1, A06(r13, r2, format, 0, 1));
                    A00 = max;
                    int max2 = Math.max(max, A06(r13, r2, A01, 0, 2));
                    A00 = max2;
                    int max3 = Math.max(max2, A06(r13, r2, A01, 0, 3));
                    A00 = max3;
                    String str = A01;
                    C14370lK r22 = C14370lK.A0I;
                    int max4 = Math.max(max3, A06(r13, r22, str, 1, 1));
                    A00 = max4;
                    int max5 = Math.max(max4, A06(r13, r22, A01, 1, 2));
                    A00 = max5;
                    int max6 = Math.max(max5, A06(r13, r22, A01, 1, 3));
                    A00 = max6;
                    String str2 = A01;
                    C14370lK r23 = C14370lK.A0X;
                    int max7 = Math.max(max6, A06(r13, r23, str2, 0, 1));
                    A00 = max7;
                    int max8 = Math.max(max7, A06(r13, r23, A01, 0, 2));
                    A00 = max8;
                    int max9 = Math.max(max8, A06(r13, r23, A01, 0, 3));
                    A00 = max9;
                    String str3 = A01;
                    C14370lK r24 = C14370lK.A0B;
                    int max10 = Math.max(max9, A06(r13, r24, str3, 0, 1));
                    A00 = max10;
                    int max11 = Math.max(max10, A06(r13, r24, A01, 0, 2));
                    A00 = max11;
                    int max12 = Math.max(max11, A06(r13, r24, A01, 0, 3));
                    A00 = max12;
                    String str4 = A01;
                    C14370lK r25 = C14370lK.A08;
                    int max13 = Math.max(max12, A06(r13, r25, str4, 0, 1));
                    A00 = max13;
                    int max14 = Math.max(max13, A06(r13, r25, A01, 0, 2));
                    A00 = max14;
                    int max15 = Math.max(max14, A06(r13, r25, A01, 0, 3));
                    A00 = max15;
                    A00 = max15 + 1;
                    SharedPreferences.Editor edit = A012.edit();
                    edit.putInt("file_index", A00);
                    edit.putString("file_date", A01);
                    edit.apply();
                    StringBuilder sb = new StringBuilder();
                    sb.append("mediafileutils/initfilecounter file_index:");
                    sb.append(A00);
                    sb.append(" | file_date:");
                    sb.append(A01);
                    sb.append(" |  time:");
                    sb.append(System.currentTimeMillis() - currentTimeMillis);
                    Log.i(sb.toString());
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("mediafileutils/initfilecounter file_index:");
                    sb2.append(A00);
                    sb2.append(" | file_date:");
                    sb2.append(A01);
                    Log.i(sb2.toString());
                }
            }
        }
    }

    public static void A0R(C16150oX r2, File file) {
        Pair A0E = A0E(file);
        r2.A08 = ((Number) A0E.first).intValue();
        r2.A06 = ((Number) A0E.second).intValue();
    }

    public static boolean A0S(C17050qB r3, File file, File file2) {
        String str;
        StringBuilder sb;
        String str2;
        File A012 = r3.A01(file);
        if (file2.renameTo(A012)) {
            if (!file.renameTo(file2)) {
                boolean renameTo = A012.renameTo(file2);
                boolean delete = file.delete();
                if (!renameTo) {
                    if (!delete) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("failed to delete ");
                        sb2.append(file.getAbsolutePath());
                        Log.e(sb2.toString());
                    }
                    if (!A012.delete()) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("failed to delete ");
                        sb3.append(A012.getAbsolutePath());
                        Log.e(sb3.toString());
                    }
                    str = "restore input file failed";
                    Log.e(str);
                    return false;
                } else if (delete) {
                    return false;
                } else {
                    sb = new StringBuilder();
                    sb.append("failed to delete ");
                    str2 = file.getAbsolutePath();
                }
            } else if (A012.delete()) {
                return true;
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("failed to delete ");
                sb4.append(A012.getAbsolutePath());
                Log.e(sb4.toString());
                return true;
            }
        } else if (A012.delete()) {
            return false;
        } else {
            sb = new StringBuilder();
            sb.append("failed to delete ");
            str2 = A012.getAbsolutePath();
        }
        sb.append(str2);
        str = sb.toString();
        Log.e(str);
        return false;
    }
}
