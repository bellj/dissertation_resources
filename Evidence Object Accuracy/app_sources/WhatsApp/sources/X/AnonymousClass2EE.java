package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.2EE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EE {
    public AnonymousClass016 A00;
    public AnonymousClass016 A01;
    public final int A02;
    public final int A03;
    public final C14650lo A04;
    public final AnonymousClass1CK A05;
    public final AnonymousClass1CL A06;
    public final C19870uo A07;
    public final C17220qS A08;
    public final C19840ul A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass2EE(C14650lo r4, AnonymousClass1CK r5, AnonymousClass1CL r6, C16590pI r7, C19870uo r8, C17220qS r9, C19840ul r10, AbstractC14440lR r11) {
        this.A0A = r11;
        this.A05 = r5;
        this.A09 = r10;
        this.A08 = r9;
        this.A06 = r6;
        this.A04 = r4;
        this.A07 = r8;
        Context context = r7.A00;
        this.A03 = context.getResources().getDimensionPixelSize(R.dimen.order_message_thumbnail_width);
        this.A02 = context.getResources().getDimensionPixelSize(R.dimen.order_message_thumbnail_height);
    }
}
