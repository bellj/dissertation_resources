package X;

/* renamed from: X.1Cc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26121Cc extends AbstractC26131Cd {
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final C17070qD A02;
    public final AnonymousClass17Z A03;
    public final AbstractC14440lR A04;

    public C26121Cc(C14830m7 r1, C14820m6 r2, C14850m9 r3, C21860y6 r4, C22710zW r5, C17070qD r6, AnonymousClass17Z r7, AbstractC14440lR r8) {
        super(r3, r4, r5);
        this.A00 = r1;
        this.A04 = r8;
        this.A02 = r6;
        this.A01 = r2;
        this.A03 = r7;
    }

    public final void A03(int i) {
        this.A01.A00.edit().putInt("payments_incentive_banner_total_days", i).apply();
    }
}
