package X;

/* renamed from: X.1Q0  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q0 implements Comparable {
    public final long A00;
    public final String A01;

    public AnonymousClass1Q0(String str, long j) {
        this.A00 = j;
        this.A01 = str;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        AnonymousClass1Q0 r6 = (AnonymousClass1Q0) obj;
        long j = this.A00;
        long j2 = r6.A00;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        return this.A01.compareTo(r6.A01);
    }
}
