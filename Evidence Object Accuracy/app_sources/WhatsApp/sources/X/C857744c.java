package X;

import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;
import java.util.Set;

/* renamed from: X.44c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C857744c extends AbstractC33331dp {
    public final /* synthetic */ MediaAlbumActivity A00;

    public C857744c(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        this.A00.A08.notifyDataSetChanged();
    }
}
