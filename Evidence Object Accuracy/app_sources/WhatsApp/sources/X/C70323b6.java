package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.webpagepreview.WebPagePreviewView;

/* renamed from: X.3b6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70323b6 implements AbstractC41521tf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C64343Fe A01;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70323b6(C64343Fe r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r6) {
        C64343Fe r0 = this.A01;
        WebPagePreviewView webPagePreviewView = r0.A0A;
        if (bitmap != null) {
            webPagePreviewView.setImageLargeThumbWithBitmap(bitmap);
        } else {
            webPagePreviewView.setImageLargeThumbWithBackground(AnonymousClass00T.A00(r0.A03, R.color.primary_surface));
        }
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        this.A01.A0A.setImageLargeThumbWithBackground(-7829368);
    }
}
