package X;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.TypedValue;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;

/* renamed from: X.2wB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60182wB extends AbstractC75743kL {
    public final View A00;

    public C60182wB(View view, ParticipantsListViewModel participantsListViewModel) {
        super(view, participantsListViewModel);
        this.A00 = AnonymousClass028.A0D(view, R.id.add_participant_icon);
    }

    @Override // X.AbstractC75743kL
    public void A09(C92194Ux r9) {
        View view = this.A0H;
        view.setClickable(true);
        C12960it.A0z(view, this, 49);
        Context context = view.getContext();
        View view2 = this.A00;
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        if (context != null) {
            TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.voipAddParticipantButtonColor, typedValue, true);
            shapeDrawable.getPaint().setColor(typedValue.data);
        }
        view2.setBackground(shapeDrawable);
        C65293Iy.A03(view, context.getString(R.string.voip_joinable_add_participant_description), context.getString(R.string.voip_joinable_add_participant_click_action_description));
    }
}
