package X;

import android.os.HandlerThread;
import android.util.Log;
import android.util.SparseArray;
import com.facebook.profilo.logger.BufferLogger;
import com.facebook.profilo.mmapbuf.core.MmapBufferManager;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceArray;

/* renamed from: X.1ST  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1ST {
    public static final ThreadLocal A0A = new C71673dK();
    public static volatile AnonymousClass1ST A0B;
    public HandlerC29471Sw A00;
    public final SparseArray A01;
    public final AnonymousClass1Se A02;
    public final AbstractC29391Sf A03;
    public final MmapBufferManager A04;
    public final File A05;
    public final String A06 = "main";
    public final AtomicInteger A07;
    public final AtomicReference A08;
    public final AtomicReferenceArray A09;

    public AnonymousClass1ST(SparseArray sparseArray, AnonymousClass1Sg r4, AnonymousClass1Se r5, AbstractC29391Sf r6, MmapBufferManager mmapBufferManager, File file) {
        this.A01 = sparseArray;
        this.A08 = new AtomicReference(r4);
        this.A04 = mmapBufferManager;
        this.A05 = file;
        this.A03 = r6;
        this.A09 = new AtomicReferenceArray(2);
        this.A07 = new AtomicInteger(0);
        this.A02 = r5;
    }

    public final C29441Sr A00(int i, long j) {
        if (this.A07.get() == 0) {
            return null;
        }
        int i2 = 0;
        do {
            C29441Sr r3 = (C29441Sr) this.A09.get(i2);
            if (!(r3 == null || (r3.A01 & i) == 0)) {
                long j2 = r3.A05;
                if (r3.A0B == null && j2 == j) {
                    return r3;
                }
            }
            i2++;
        } while (i2 < 2);
        return null;
    }

    public final C29441Sr A01(long j) {
        if (this.A07.get() == 0) {
            return null;
        }
        int i = 0;
        do {
            C29441Sr r3 = (C29441Sr) this.A09.get(i);
            if (r3 != null && r3.A06 == j) {
                return r3;
            }
            i++;
        } while (i < 2);
        return null;
    }

    public final void A02() {
        C89744Ld r2;
        HandlerThread handlerThread;
        if (this.A00 == null) {
            AnonymousClass1Se r3 = this.A02;
            synchronized (C89744Ld.class) {
                r2 = C89744Ld.A01;
                if (r2 == null) {
                    r2 = new C89744Ld();
                    C89744Ld.A01 = r2;
                }
            }
            synchronized (r2) {
                if (r2.A00 == null) {
                    HandlerThread handlerThread2 = new HandlerThread("Prflo:TraceCtl");
                    r2.A00 = handlerThread2;
                    handlerThread2.start();
                }
                handlerThread = r2.A00;
            }
            this.A00 = new HandlerC29471Sw(handlerThread.getLooper(), r3, this.A03);
        }
    }

    public final void A03(int i, int i2, int i3, long j) {
        C29441Sr A00 = A00(i, j);
        if (A00 != null) {
            A05(A00);
            StringBuilder sb = new StringBuilder("STOP PROFILO_TRACEID: ");
            long j2 = A00.A06;
            sb.append(AnonymousClass034.A00(j2));
            Log.w("Profilo/TraceControl", sb.toString());
            synchronized (this) {
                A02();
                if (i2 == 0) {
                    BufferLogger.writeStandardEntry(A00.A09, 6, 37, 0, 0, 0, 0, j2);
                    this.A00.A01(new C29441Sr(A00, i3));
                } else if (i2 == 1) {
                    BufferLogger.writeStandardEntry(A00.A09, 6, 61, 0, 0, 0, 0, j2);
                    this.A00.A02(A00);
                }
            }
        }
    }

    public void A04(long j, int i) {
        C29441Sr A01 = A01(j);
        if (A01 != null && A01.A06 == j) {
            A05(A01);
            synchronized (this) {
                A02();
                this.A00.A01(new C29441Sr(A01, i));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0011, code lost:
        r2 = r5.A07;
        r1 = r2.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
        if (r2.compareAndSet(r1, (1 << r4) ^ r1) == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(X.C29441Sr r6) {
        /*
            r5 = this;
            r4 = 0
        L_0x0001:
            r0 = 2
            r3 = 1
            if (r4 >= r0) goto L_0x0021
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r5.A09
            r0 = 0
            boolean r0 = r1.compareAndSet(r4, r6, r0)
            if (r0 != 0) goto L_0x0011
            int r4 = r4 + 1
            goto L_0x0001
        L_0x0011:
            java.util.concurrent.atomic.AtomicInteger r2 = r5.A07
            int r1 = r2.get()
            int r0 = r3 << r4
            r0 = r0 ^ r1
            boolean r0 = r2.compareAndSet(r1, r0)
            if (r0 == 0) goto L_0x0011
            return
        L_0x0021:
            java.lang.String r1 = "Profilo/TraceControl"
            java.lang.String r0 = "Could not reset Trace Context to null"
            android.util.Log.w(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ST.A05(X.1Sr):void");
    }
}
