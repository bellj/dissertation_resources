package X;

/* renamed from: X.2C2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2C2 implements AbstractC18870tC, AbstractC47642Bu {
    public AnonymousClass1LC A00;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void AR9() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    public AnonymousClass2C2(C16240og r1) {
        r1.A05(this);
    }

    @Override // X.AbstractC18870tC
    public void ARC() {
        AnonymousClass1LC r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AbstractC47642Bu
    public void AcI(AnonymousClass1LC r1) {
        this.A00 = r1;
    }
}
