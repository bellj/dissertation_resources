package X;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.1UI  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1UI extends BasePendingResult implements AnonymousClass1UL {
    public final AnonymousClass4DN A00;
    public final AnonymousClass1UE A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass1UI(AnonymousClass1UE r2, AnonymousClass1U8 r3) {
        super(r3);
        C13020j0.A02(r3, "GoogleApiClient must not be null");
        C13020j0.A02(r2, "Api must not be null");
        this.A00 = r2.A01;
        this.A01 = r2;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [X.5YT, android.os.IBinder] */
    /* JADX WARN: Type inference failed for: r5v0, types: [X.5YT, android.os.IBinder] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.AnonymousClass5QV r21) {
        /*
        // Method dump skipped, instructions count: 309
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1UI.A07(X.5QV):void");
    }

    public final void A08(AnonymousClass5QV r8) {
        try {
            A07(r8);
        } catch (DeadObjectException e) {
            A09(new Status(null, null, e.getLocalizedMessage(), 1, 8));
            throw e;
        } catch (RemoteException e2) {
            A09(new Status(null, null, e2.getLocalizedMessage(), 1, 8));
        }
    }

    public final void A09(Status status) {
        boolean z = false;
        if (status.A01 <= 0) {
            z = true;
        }
        C13020j0.A03("Failed result must not be success", !z);
        A05(A02(status));
    }
}
