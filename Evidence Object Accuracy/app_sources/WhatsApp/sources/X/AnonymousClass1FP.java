package X;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Pair;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.whatsapp.push.RegistrationIntentService;
import com.whatsapp.util.Log;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1FP  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FP implements AbstractC15920o8 {
    public final C15450nH A00;
    public final AnonymousClass1FO A01;
    public final C14830m7 A02;
    public final C16590pI A03;
    public final C22350yw A04;
    public final C21680xo A05;
    public final AnonymousClass16G A06;
    public final C22050yP A07;
    public final C20660w7 A08;
    public final C251318f A09;
    public final AbstractC14440lR A0A;

    public AnonymousClass1FP(C15450nH r1, AnonymousClass1FO r2, C14830m7 r3, C16590pI r4, C22350yw r5, C21680xo r6, AnonymousClass16G r7, C22050yP r8, C20660w7 r9, C251318f r10, AbstractC14440lR r11) {
        this.A03 = r4;
        this.A02 = r3;
        this.A0A = r11;
        this.A08 = r9;
        this.A00 = r1;
        this.A05 = r6;
        this.A09 = r10;
        this.A07 = r8;
        this.A04 = r5;
        this.A01 = r2;
        this.A06 = r7;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{6, 27, 87, 159, 174, 208, 18};
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        int i2;
        SharedPreferences.Editor editor;
        boolean z;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor2;
        String str;
        AnonymousClass01b r9;
        String str2;
        if (i == 6) {
            Bundle bundle = (Bundle) message.obj;
            String string = bundle.getString("gcmToken");
            String string2 = bundle.getString("fbnsToken");
            String string3 = bundle.getString("mutedChatsHash");
            StringBuilder sb = new StringBuilder("AppMessagingXmppHandler/received client config from server; gcmToken=");
            int i3 = 0;
            if (string != null) {
                i2 = string.length();
            } else {
                i2 = 0;
            }
            sb.append(i2);
            sb.append(" bytes; fbnsToken=");
            if (string2 != null) {
                i3 = string2.length();
            }
            sb.append(i3);
            sb.append(" bytes: mutedChatsHash=");
            sb.append(string3);
            Log.i(sb.toString());
            RegistrationIntentService.A02(this.A03.A00, string, string3);
            C251318f r3 = this.A09;
            if (r3.A01()) {
                r3.A00(string2);
                return true;
            } else if (TextUtils.isEmpty(string2)) {
                return true;
            } else {
                C20660w7 r2 = this.A08;
                if (!r2.A01.A06) {
                    return true;
                }
                r2.A06.A08(Message.obtain(null, 0, 263, 0), false);
                return true;
            }
        } else if (i == 18) {
            return true;
        } else {
            if (i == 27) {
                int i4 = message.arg2;
                StringBuilder sb2 = new StringBuilder("AppMessagingXmppHandler/clientConfigError/");
                sb2.append(i4);
                Log.e(sb2.toString());
                if (i4 != 404) {
                    return true;
                }
                RegistrationIntentService.A02(this.A03.A00, null, null);
                C251318f r22 = this.A09;
                if (!r22.A01()) {
                    return true;
                }
                r22.A00(null);
                return true;
            } else if (i != 87) {
                if (i == 159) {
                    long j = ((Bundle) message.obj).getLong("timestampMs");
                    AnonymousClass1FO r8 = this.A01;
                    if (1675268253000L < j) {
                        return true;
                    }
                    SharedPreferences sharedPreferences2 = r8.A01.A00;
                    long j2 = sharedPreferences2.getLong("client_expiration_time", 0);
                    long A00 = r8.A00.A00() + TimeUnit.DAYS.toMillis(3);
                    if (j2 == 0 || (j < j2 && j2 > A00)) {
                        long max = Math.max(j, A00);
                        StringBuilder sb3 = new StringBuilder("wa-shared-prefs/set-client-expiration-time/");
                        sb3.append(max);
                        sb3.append(" ");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Calendar instance = Calendar.getInstance();
                        instance.setTimeInMillis(max);
                        sb3.append(simpleDateFormat.format(instance.getTime()));
                        Log.i(sb3.toString());
                        editor = sharedPreferences2.edit().putLong("client_expiration_time", max);
                    } else if (j2 <= 0 || j != -1) {
                        return true;
                    } else {
                        Log.i("wa-shared-prefs/clear-client-expiration-time");
                        editor = sharedPreferences2.edit().remove("client_expiration_time");
                    }
                    editor.apply();
                    return true;
                } else if (i == 174) {
                    AnonymousClass1V8 r32 = (AnonymousClass1V8) message.obj;
                    int A002 = C28421Nd.A00(r32.A0I("version", null), 0);
                    int A003 = C28421Nd.A00(r32.A0I("protocol", null), 1);
                    HashMap hashMap = new HashMap();
                    for (AnonymousClass1V8 r5 : r32.A0J("prop")) {
                        hashMap.put(r5.A0H("name"), r5.A0I("value", null));
                    }
                    if (A003 == 2) {
                        this.A00.A04(this.A02, r32.A0I("hash", null), r32.A0I("key", null), hashMap, A002, A003, C28421Nd.A01(r32.A0I("refresh", null), 86400) * 1000);
                    } else {
                        this.A00.A04(this.A02, null, null, hashMap, A002, 1, 86400000);
                    }
                    this.A0A.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(this, 38));
                    return true;
                } else if (i != 208) {
                    return false;
                } else {
                    AnonymousClass1V8 r6 = (AnonymousClass1V8) message.obj;
                    C28421Nd.A00(r6.A0I("protocol", null), 1);
                    String A0I = r6.A0I("ab_key", null);
                    String A0I2 = r6.A0I("hash", null);
                    long A01 = C28421Nd.A01(r6.A0I("refresh", null), 86400) * 1000;
                    int A05 = r6.A05("refresh_id", 0);
                    SparseArray sparseArray = new SparseArray();
                    List<AnonymousClass1V8> A0J = r6.A0J("prop");
                    SparseIntArray sparseIntArray = new SparseIntArray();
                    for (AnonymousClass1V8 r82 : A0J) {
                        if (!TextUtils.isEmpty(r82.A0I("config_code", null))) {
                            sparseArray.append(r82.A06(r82.A0H("config_code"), "config_code"), Pair.create(r82.A0H("config_value"), r82.A0I("config_expo_key", null)));
                        } else {
                            sparseIntArray.append(r82.A06(r82.A0H("event_code"), "event_code"), r82.A06(r82.A0H("sampling_weight"), "sampling_weight"));
                        }
                    }
                    C21680xo r7 = this.A05;
                    C14830m7 r23 = this.A02;
                    synchronized (r7) {
                        SharedPreferences sharedPreferences3 = r7.A01;
                        SharedPreferences.Editor edit = sharedPreferences3.edit();
                        if (!TextUtils.isEmpty(A0I2) && C14850m9.A0B) {
                            SharedPreferences A012 = r7.A07.A01("ab-props-backup");
                            A012.edit().clear().apply();
                            Map<String, ?> all = sharedPreferences3.getAll();
                            if (!all.isEmpty()) {
                                SharedPreferences.Editor edit2 = A012.edit();
                                for (Map.Entry<String, ?> entry : all.entrySet()) {
                                    String key = entry.getKey();
                                    Object value = entry.getValue();
                                    if (value != null) {
                                        Class<?> cls = value.getClass();
                                        if (cls.equals(Boolean.class)) {
                                            edit2.putBoolean(key, ((Boolean) value).booleanValue());
                                        } else if (cls.equals(Float.class)) {
                                            edit2.putFloat(key, ((Float) value).floatValue());
                                        } else if (cls.equals(Integer.class)) {
                                            edit2.putInt(key, ((Integer) value).intValue());
                                        } else if (cls.equals(Long.class)) {
                                            edit2.putLong(key, ((Long) value).longValue());
                                        } else if (cls.equals(String.class)) {
                                            edit2.putString(key, (String) value);
                                        } else if (Set.class.isAssignableFrom(cls)) {
                                            edit2.putStringSet(key, (Set) value);
                                        }
                                    }
                                }
                                edit2.commit();
                            }
                        }
                        long max2 = Math.max(A01, 600000L);
                        edit.putLong("ab_props:sys:refresh", max2);
                        if (!TextUtils.isEmpty(A0I2)) {
                            HashMap hashMap2 = new HashMap();
                            Set<String> stringSet = sharedPreferences3.getStringSet("ab_props:sys:last_exposure_keys", null);
                            sharedPreferences3.getAll();
                            edit.clear();
                            edit.putLong("ab_props:sys:refresh", max2);
                            edit.putStringSet("ab_props:sys:last_exposure_keys", stringSet);
                            C22040yO r24 = r7.A00;
                            AnonymousClass009.A05(r24);
                            synchronized (r24) {
                                r24.A01 = false;
                            }
                            z = true;
                            C21210x3 r13 = r7.A05;
                            for (AbstractC18320sG r33 : r13.A01()) {
                                try {
                                    if (r33 instanceof AnonymousClass121) {
                                        AnonymousClass121 r14 = (AnonymousClass121) r33;
                                        r14.A00 = AnonymousClass2RM.A00(r14.A02);
                                    } else if (r33 instanceof C20310vY) {
                                        C20310vY r142 = (C20310vY) r33;
                                        r142.A00 = r142.A02.A07(544);
                                    } else if (r33 instanceof C34261fq) {
                                        C34261fq r143 = (C34261fq) r33;
                                        r143.A00 = r143.A01.A02.A07(736);
                                    } else if (r33 instanceof AnonymousClass120) {
                                        AnonymousClass120 r144 = (AnonymousClass120) r33;
                                        r144.A00 = r144.A03.A02(1228);
                                    } else if (r33 instanceof C18710st) {
                                        C18710st r145 = (C18710st) r33;
                                        r145.A01 = r145.A03.A08();
                                        String A03 = r145.A05.A03(1846);
                                        if (A03 != null) {
                                            r145.A00 = A03;
                                        }
                                    } else if (r33 instanceof C18310sF) {
                                        C18310sF r146 = (C18310sF) r33;
                                        r146.A00 = r146.A02.A07(932);
                                    }
                                } catch (Exception e) {
                                    r13.A05(r33, e);
                                }
                            }
                            for (int i5 = 0; i5 < sparseArray.size(); i5++) {
                                int keyAt = sparseArray.keyAt(i5);
                                Pair pair = (Pair) sparseArray.valueAt(i5);
                                String str3 = (String) pair.second;
                                if (r7.A04(edit, (String) pair.first, keyAt) && !TextUtils.isEmpty(str3)) {
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append(keyAt);
                                    sb4.append("_expo_key");
                                    edit.putString(sb4.toString(), str3);
                                }
                            }
                            Set<String> stringSet2 = sharedPreferences3.getStringSet("ab_props:sys:last_exposure_keys", null);
                            if (stringSet2 == null) {
                                r9 = new AnonymousClass01b(0);
                            } else {
                                r9 = new AnonymousClass01b(stringSet2);
                            }
                            AnonymousClass01b r52 = new AnonymousClass01b(0);
                            for (int i6 = 0; i6 < sparseArray.size(); i6++) {
                                Pair pair2 = (Pair) sparseArray.valueAt(i6);
                                if (pair2 == null) {
                                    str2 = null;
                                } else {
                                    str2 = (String) pair2.second;
                                }
                                if (r9.contains(str2)) {
                                    r52.add(str2);
                                }
                            }
                            C14850m9 r25 = r7.A04;
                            r25.A05(edit, r52);
                            r25.A09.clear();
                            hashMap2.isEmpty();
                        } else {
                            z = false;
                        }
                        edit.putString("ab_props:sys:config_key", A0I);
                        AnonymousClass15Y r34 = r7.A06;
                        r34.A02(A0I, 4473, 0);
                        r34.A02(A0I, 4473, 1);
                        if (!TextUtils.isEmpty(A0I2)) {
                            edit.putString("ab_props:sys:config_hash", A0I2);
                        }
                        edit.putLong("ab_props:sys:last_refresh_time", r23.A00());
                        edit.putInt("ab_props:sys:last_version", A05);
                        edit.apply();
                        if (z) {
                            C21210x3 r92 = r7.A05;
                            for (AbstractC18320sG r83 : r92.A01()) {
                                try {
                                    if (r83 instanceof AnonymousClass121) {
                                        AnonymousClass121 r53 = (AnonymousClass121) r83;
                                        if (!r53.A00 && AnonymousClass2RM.A00(r53.A02)) {
                                            r53.A03.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(r53, 27));
                                        }
                                    } else if (r83 instanceof C20310vY) {
                                        C20310vY r54 = (C20310vY) r83;
                                        if (!r54.A00 && r54.A02.A07(544)) {
                                            r54.A03.Ab2(new RunnableBRunnable0Shape9S0100000_I0_9(r54, 23));
                                        }
                                    } else if (r83 instanceof AnonymousClass16C) {
                                        AnonymousClass16C r26 = (AnonymousClass16C) r83;
                                        r26.A01.Ab2(new RunnableBRunnable0Shape8S0100000_I0_8(r26.A00, 44));
                                    } else if (r83 instanceof C22770zc) {
                                        C22770zc r11 = (C22770zc) r83;
                                        Runnable runnable = r11.A00;
                                        if (runnable != null) {
                                            r11.A05.AaP(runnable);
                                        }
                                        r11.A00 = r11.A05.AbK(r11.A09, "AbPropsTamperManager/ap-props-hash-update", 500);
                                    } else if (r83 instanceof C34261fq) {
                                        C34261fq r55 = (C34261fq) r83;
                                        C241614l r4 = r55.A01;
                                        if (!r4.A02.A07(736) && r55.A00) {
                                            C233711k r56 = r4.A01;
                                            synchronized ("syncd_bootstrapped_mutations") {
                                                Set A02 = r56.A02();
                                                A02.remove("setting_unarchiveChats");
                                                r56.A01().edit().putStringSet("syncd_bootstrapped_mutations", A02).apply();
                                            }
                                        }
                                    } else if (r83 instanceof C20070vA) {
                                        C18850tA r57 = ((C20070vA) r83).A00;
                                        r57.A0e.Ab2(new RunnableBRunnable0Shape4S0100000_I0_4(r57, 18));
                                    } else if (r83 instanceof AnonymousClass2RN) {
                                        AnonymousClass2RN r58 = (AnonymousClass2RN) r83;
                                        r58.A00.A06.execute(new RunnableBRunnable0Shape4S0100000_I0_4(r58, 16));
                                    } else if (r83 instanceof AnonymousClass120) {
                                        AnonymousClass120 r10 = (AnonymousClass120) r83;
                                        Log.i("CommunityAbPropsObserver/onAfterABPropsChanged");
                                        long A022 = (long) r10.A03.A02(1228);
                                        if (r10.A00 == -1 || A022 != -1) {
                                            SharedPreferences sharedPreferences4 = r10.A01.A00;
                                            if (!sharedPreferences4.getBoolean("groups_type_discovered", false)) {
                                                long j3 = A022 * 1000;
                                                long j4 = sharedPreferences4.getLong("client_version_upgrade_timestamp", -1);
                                                if (j3 == 0 || j4 < j3) {
                                                    editor2 = sharedPreferences4.edit().putBoolean("groups_type_discovered", true);
                                                    editor2.apply();
                                                } else {
                                                    str = "CommunityAbPropsObserver/fetch needed";
                                                }
                                            }
                                        } else {
                                            str = "CommunityAbPropsObserver/force fetch";
                                        }
                                        Log.i(str);
                                        C20710wC r35 = r10.A04;
                                        if (r35.A10 || !r10.A02.A00) {
                                            Log.i("CommunityAbPropsObserver/onAfterABPropsChanged/skip");
                                        } else {
                                            r35.A0F(0, false, false);
                                            editor2 = r10.A01.A00.edit().putBoolean("groups_type_discovered", true);
                                            editor2.apply();
                                        }
                                    } else if (!(r83 instanceof C18710st)) {
                                        C18310sF r42 = (C18310sF) r83;
                                        if (r42.A00 && !r42.A02.A07(932)) {
                                            r42.A01.A0L();
                                        }
                                    } else {
                                        C18710st r59 = (C18710st) r83;
                                        r59.A06.Ab2(new RunnableBRunnable0Shape3S0100000_I0_3(r59, 4));
                                    }
                                } catch (Exception e2) {
                                    r92.A05(r83, e2);
                                }
                            }
                        }
                        r7.A03(0);
                        r7.A02(0);
                        File file = new File(r7.A03.A00.getFilesDir(), "crash_counter");
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                    if (TextUtils.isEmpty(A0I2)) {
                        return true;
                    }
                    AnonymousClass16G r36 = this.A06;
                    synchronized (r36) {
                        sharedPreferences = r36.A00;
                        if (sharedPreferences == null) {
                            sharedPreferences = r36.A01.A02("field-stats-events-sampling");
                            r36.A00 = sharedPreferences;
                        }
                    }
                    SharedPreferences.Editor edit3 = sharedPreferences.edit();
                    edit3.clear();
                    for (int i7 = 0; i7 < sparseIntArray.size(); i7++) {
                        int keyAt2 = sparseIntArray.keyAt(i7);
                        edit3.putInt(String.valueOf(keyAt2), sparseIntArray.valueAt(i7));
                    }
                    edit3.apply();
                    return true;
                }
            } else if (!((Boolean) message.obj).booleanValue()) {
                return true;
            } else {
                this.A04.A00();
                return true;
            }
        }
    }
}
