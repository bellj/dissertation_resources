package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.1D3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1D3 implements AbstractC16990q5 {
    public final C14830m7 A00;
    public final C237712y A01;
    public final C25641Ae A02;
    public final C19780uf A03;
    public final C15600nX A04;
    public final AnonymousClass1D2 A05;
    public final C238513g A06;
    public final AnonymousClass11B A07;
    public final C241114g A08;
    public final AnonymousClass10U A09;
    public final C21400xM A0A;
    public final C15670ni A0B;
    public final C22110yX A0C;

    public AnonymousClass1D3(C14830m7 r1, C237712y r2, C25641Ae r3, C19780uf r4, C15600nX r5, AnonymousClass1D2 r6, C238513g r7, AnonymousClass11B r8, C241114g r9, AnonymousClass10U r10, C21400xM r11, C15670ni r12, C22110yX r13) {
        this.A0C = r13;
        this.A0B = r12;
        this.A09 = r10;
        this.A0A = r11;
        this.A08 = r9;
        this.A03 = r4;
        this.A06 = r7;
        this.A02 = r3;
        this.A04 = r5;
        this.A07 = r8;
        this.A01 = r2;
        this.A05 = r6;
        this.A00 = r1;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC16990q5
    public void AOo() {
        C15670ni r1 = this.A0B;
        Log.i("SharedMediaIdsStore/deleteOutdatedRecords");
        C16310on A02 = r1.A00.A02();
        try {
            A02.A03.A01("shared_media_ids", "expiration_timestamp <?", new String[]{String.valueOf(System.currentTimeMillis())});
            A02.close();
            C22110yX r3 = this.A0C;
            List<AnonymousClass1JU> A08 = r3.A02.A08();
            HashSet hashSet = new HashSet(A08.size());
            for (AnonymousClass1JU r0 : A08) {
                hashSet.add(r0.A05);
            }
            C22090yV r5 = r3.A03;
            AnonymousClass009.A00();
            HashSet hashSet2 = new HashSet();
            A02 = r5.A00.get();
            try {
                Cursor A09 = A02.A03.A09("SELECT DISTINCT device_id FROM peer_messages", null);
                while (A09.moveToNext()) {
                    DeviceJid nullable = DeviceJid.getNullable(A09.getString(A09.getColumnIndexOrThrow("device_id")));
                    if (nullable != null) {
                        hashSet2.add(nullable);
                    }
                }
                A09.close();
                A02.close();
                hashSet2.removeAll(hashSet);
                Iterator it = hashSet2.iterator();
                while (it.hasNext()) {
                    r5.A05((DeviceJid) it.next());
                }
                if (!hashSet2.isEmpty()) {
                    r3.A00.AaV("orphan-peer-messages", String.valueOf(hashSet2.size()), false);
                }
            } finally {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
            }
        } catch (Throwable th) {
            throw th;
        }
    }

    @Override // X.AbstractC16990q5
    public void AOp() {
        this.A03.A03();
        AnonymousClass10U r4 = this.A09;
        r4.A04.A01(new RunnableBRunnable0Shape6S0100000_I0_6(r4, 9), 40);
        C20590w0 r1 = this.A04.A08;
        try {
            C16310on A02 = r1.A08.A02();
            A02.A03.A01("group_participants_history", "timestamp < ?", new String[]{String.valueOf((r1.A04.A00() - 2592000000L) / 1000)});
            A02.close();
        } catch (SQLiteException e) {
            Log.e("msgstore/clear-old-participant-history/db-not-accessible", e);
        }
        C238513g r12 = this.A06;
        C16310on A022 = r12.A03.A02();
        try {
            A022.A03.A01("receipt_orphaned", "timestamp < ?", new String[]{String.valueOf((r12.A00.A00() - 5184000000L) / 1000)});
            A022.close();
            C241114g r11 = this.A08;
            C14830m7 r6 = this.A00;
            long A00 = r6.A00() - AnonymousClass4GS.A00;
            int i = 500;
            while (true) {
                try {
                    C16310on A023 = r11.A04.A02();
                    AnonymousClass1Lx A002 = A023.A00();
                    C16330op r13 = A023.A03;
                    Cursor A09 = r13.A09("SELECT message_row_id FROM message_streaming_sidecar WHERE timestamp < ? LIMIT ?", new String[]{String.valueOf(A00), String.valueOf(200)});
                    StringBuilder sb = new StringBuilder();
                    sb.append("SidecarMessageStore/deleteStreamingSidecarOlderThan num messages to update:");
                    sb.append(A09.getCount());
                    Log.i(sb.toString());
                    boolean z = false;
                    if (A09.getCount() >= 200) {
                        z = true;
                    }
                    int i2 = 0;
                    while (A09.moveToNext()) {
                        long j = A09.getLong(A09.getColumnIndexOrThrow("message_row_id"));
                        AbstractC15340mz A003 = r11.A01.A00(j);
                        if (A003 instanceof AbstractC16130oV) {
                            C16150oX r0 = ((AbstractC16130oV) A003).A02;
                            AnonymousClass009.A05(r0);
                            r0.A0M = false;
                            r11.A03.A00(A003, A003.A0z);
                            r11.A02.A07(A003);
                        }
                        i2 += r13.A01("message_streaming_sidecar", "message_row_id = ?", new String[]{String.valueOf(j)});
                    }
                    A002.A00();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("SidecarMessageStore/deleteStreamingSidecarOlderThan deleting rows:");
                    sb2.append(i2);
                    Log.i(sb2.toString());
                    A09.close();
                    A002.close();
                    A023.close();
                    i--;
                    if (!z || i <= 0) {
                        break;
                    }
                } catch (Exception e2) {
                    Log.e("SidecarMessageStore/deleteStreamingSidecarOlderThan", e2);
                }
            }
            AnonymousClass1D2 r7 = this.A05;
            long A004 = r7.A00.A00() - 2678400000L;
            while (true) {
                try {
                    C16310on A024 = r7.A05.A02();
                    AnonymousClass1Lx A005 = A024.A00();
                    C16330op r8 = A024.A03;
                    int i3 = 0;
                    Cursor A092 = r8.A09("SELECT message_row_id FROM mms_thumbnail_metadata WHERE insert_timestamp < ? LIMIT ?", new String[]{String.valueOf(A004), String.valueOf(200)});
                    ArrayList arrayList = new ArrayList();
                    while (A092.moveToNext()) {
                        arrayList.add(Long.valueOf(A092.getLong(0)));
                    }
                    A092.close();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("MmsThumbnailMetadataDeletionManager/deleteMmsThumbnailMetadataOlderThan num messages to update:");
                    sb3.append(arrayList.size());
                    Log.i(sb3.toString());
                    boolean z2 = false;
                    if (arrayList.size() > 0) {
                        z2 = true;
                    }
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        long longValue = ((Long) it.next()).longValue();
                        AbstractC15340mz A006 = r7.A01.A00(longValue);
                        if (A006 != null) {
                            AnonymousClass15Q r9 = r7.A03;
                            AbstractC14640lm r2 = A006.A0z.A00;
                            Map map = r9.A03;
                            if (map.get(r2) == null && map.get(null) == null) {
                                A006.A0i(null);
                                r7.A02.A0t(A006, -1);
                            }
                        }
                        i3 += r8.A01("mms_thumbnail_metadata", "message_row_id = ?", new String[]{String.valueOf(longValue)});
                    }
                    A005.A00();
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("MmsThumbnailMetadataDeletionManager/deleteMmsThumbnailMetadataOlderThan deleting rows:");
                    sb4.append(i3);
                    Log.i(sb4.toString());
                    if (i3 == 0) {
                        z2 = false;
                    }
                    A005.close();
                    A024.close();
                    if (!z2) {
                        break;
                    }
                } catch (Exception e3) {
                    Log.e("MmsThumbnailMetadataDeletionManager/deleteMmsThumbnailMetadataOlderThan", e3);
                }
            }
            C237712y r112 = this.A01;
            try {
                C16310on A025 = r112.A02.A02();
                C16330op r122 = A025.A03;
                Cursor A08 = r122.A08("conversion_tuples", null, null, null, new String[]{"jid_row_id", "data", "source", "biz_count", "has_user_sent_last_message", "last_interaction"}, new String[0]);
                while (A08.moveToNext()) {
                    if (System.currentTimeMillis() - A08.getLong(5) > AnonymousClass22F.A06) {
                        r112.A00(r122, A08.getInt(0));
                    }
                }
                A08.close();
                A025.close();
            } catch (Exception e4) {
                Log.e("conversionTupleMessageStore/getConversionTuple error accessing db", e4);
            }
            C21400xM r42 = this.A0A;
            Log.i("MessageAddOnManager/deleteOldOrphanedMessageAddOns");
            long A007 = r42.A04.A00() - (((long) r42.A00) * 86400000);
            C16310on A026 = r42.A0F.A03.A02();
            try {
                A026.A03.A01("message_add_on_orphan", "message_add_on_orphan.timestamp < ?", new String[]{String.valueOf(A007)});
                A026.close();
                this.A02.A00();
                AnonymousClass11B r5 = this.A07;
                String[] strArr = {String.valueOf(r6.A00() - 5184000000L)};
                A022 = r5.A01.A02();
                try {
                    A022.A03.A01("group_past_participant_user", "timestamp < ? ", strArr);
                } finally {
                    try {
                        A022.close();
                    } catch (Throwable unused) {
                    }
                }
            } finally {
            }
        } finally {
            try {
                A022.close();
            } catch (Throwable unused2) {
            }
        }
    }
}
