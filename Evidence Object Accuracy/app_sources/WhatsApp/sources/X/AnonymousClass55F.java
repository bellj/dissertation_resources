package X;

import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.conversation.conversationrow.WaveformVisualizerView;

/* renamed from: X.55F  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass55F implements AbstractC116065Tz {
    public final /* synthetic */ C60752yZ A00;

    public /* synthetic */ AnonymousClass55F(C60752yZ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116065Tz
    public final void AYS(byte[] bArr) {
        ConversationRowAudioPreview conversationRowAudioPreview = this.A00.A06;
        if (conversationRowAudioPreview != null) {
            WaveformVisualizerView waveformVisualizerView = conversationRowAudioPreview.A03;
            waveformVisualizerView.A02 = bArr;
            waveformVisualizerView.invalidate();
        }
    }
}
