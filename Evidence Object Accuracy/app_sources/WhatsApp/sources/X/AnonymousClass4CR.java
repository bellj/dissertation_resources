package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.4CR  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4CR extends IllegalStateException {
    public final long positionMs;
    public final Timeline timeline;
    public final int windowIndex;

    public AnonymousClass4CR(Timeline timeline, int i, long j) {
        this.timeline = timeline;
        this.windowIndex = i;
        this.positionMs = j;
    }
}
