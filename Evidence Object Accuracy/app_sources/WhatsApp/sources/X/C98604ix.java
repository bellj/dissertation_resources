package X;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* renamed from: X.4ix  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98604ix implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        ArrayList arrayList = null;
        Account account = null;
        String str = null;
        String str2 = null;
        ArrayList arrayList2 = null;
        String str3 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 1:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 2:
                    arrayList = C95664e9.A0B(parcel, Scope.CREATOR, readInt);
                    break;
                case 3:
                    account = (Account) C95664e9.A07(parcel, Account.CREATOR, readInt);
                    break;
                case 4:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 5:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 6:
                    z3 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 7:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case '\b':
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case '\t':
                    arrayList2 = C95664e9.A0B(parcel, C78283of.CREATOR, readInt);
                    break;
                case '\n':
                    str3 = C95664e9.A08(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        HashMap A11 = C12970iu.A11();
        if (arrayList2 != null) {
            Iterator it = arrayList2.iterator();
            while (it.hasNext()) {
                C78283of r1 = (C78283of) it.next();
                A11.put(Integer.valueOf(r1.A00), r1);
            }
        }
        return new GoogleSignInOptions(account, str, str2, str3, arrayList, A11, i, z, z2, z3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInOptions[i];
    }
}
