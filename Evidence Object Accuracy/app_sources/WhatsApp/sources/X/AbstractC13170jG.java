package X;

import java.util.Set;

/* renamed from: X.0jG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC13170jG {
    public AbstractC13190jI A00() {
        if (!(this instanceof C13320jW)) {
            C13200jJ r0 = (C13200jJ) ((C13160jF) this).A03.get(AnonymousClass00P.class);
            if (r0 == null) {
                return C13160jF.A04;
            }
            return r0;
        }
        C13320jW r1 = (C13320jW) this;
        if (r1.A05.contains(AnonymousClass00P.class)) {
            return r1.A00.A00();
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", AnonymousClass00P.class));
    }

    public AbstractC13190jI A01(Class cls) {
        if (!(this instanceof C13320jW)) {
            C13020j0.A02(cls, "Null interface requested.");
            return (AbstractC13190jI) ((C13160jF) this).A02.get(cls);
        }
        C13320jW r1 = (C13320jW) this;
        if (r1.A02.contains(cls)) {
            return r1.A00.A01(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    public Object A02(Class cls) {
        if (!(this instanceof C13320jW)) {
            AbstractC13190jI A01 = A01(cls);
            if (A01 == null) {
                return null;
            }
            return A01.get();
        }
        C13320jW r1 = (C13320jW) this;
        if (r1.A01.contains(cls)) {
            Object A02 = r1.A00.A02(cls);
            if (cls.equals(AbstractC13280jR.class)) {
                return new C13330jX((AbstractC13280jR) A02, r1.A03);
            }
            return A02;
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }

    public Set A03() {
        if (!(this instanceof C13320jW)) {
            return (Set) A00().get();
        }
        C13320jW r1 = (C13320jW) this;
        if (r1.A04.contains(AnonymousClass00P.class)) {
            return r1.A00.A03();
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", AnonymousClass00P.class));
    }
}
