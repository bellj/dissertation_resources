package X;

import android.content.Context;
import android.os.SystemClock;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.Executor;

/* renamed from: X.0ER  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0ER extends AnonymousClass0QL {
    public final Executor A00;
    public volatile RunnableC10240eG A01;
    public volatile RunnableC10240eG A02;

    public abstract Object A06();

    public void A07() {
    }

    public void A0B(Object obj) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0ER(Context context) {
        super(context);
        Executor executor = RunnableC10240eG.A09;
        this.A00 = executor;
    }

    @Override // X.AnonymousClass0QL
    @Deprecated
    public void A05(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.A05(str, fileDescriptor, printWriter, strArr);
        if (this.A02 != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.A02);
            printWriter.print(" waiting=");
            printWriter.println(false);
        }
        if (this.A01 != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.A01);
            printWriter.print(" waiting=");
            printWriter.println(false);
        }
    }

    public void A08() {
        if (this.A01 == null && this.A02 != null) {
            RunnableC10240eG r3 = this.A02;
            Executor executor = this.A00;
            if (r3.A05 != AnonymousClass0JG.PENDING) {
                int[] iArr = C04300Le.A00;
                int ordinal = r3.A05.ordinal();
                int i = iArr[ordinal];
                if (ordinal == 1) {
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                } else if (i != 2) {
                    throw new IllegalStateException("We should never reach this state");
                } else {
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
                }
            } else {
                r3.A05 = AnonymousClass0JG.RUNNING;
                executor.execute(r3.A02);
            }
        }
    }

    public void A09() {
        A00();
        this.A02 = new RunnableC10240eG(this);
        A08();
    }

    public void A0A(RunnableC10240eG r2, Object obj) {
        A0B(obj);
        if (this.A01 == r2) {
            if (this.A04) {
                if (this.A06) {
                    A09();
                } else {
                    this.A03 = true;
                }
            }
            SystemClock.uptimeMillis();
            this.A01 = null;
            A08();
        }
    }
}
