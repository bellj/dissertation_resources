package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;
import java.util.List;

/* renamed from: X.35r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622235r extends AbstractC69213Yj {
    public View A00;
    public View A01;
    public TextView A02;
    public TextView A03;
    public WaImageView A04;
    public C54392ge A05;
    public List A06;
    public boolean A07;
    public final AnonymousClass242 A08;
    public final C22210yi A09;
    public final AnonymousClass1AB A0A;
    public final AbstractC116245Ur A0B;
    public final boolean A0C;

    public C622235r(Context context, LayoutInflater layoutInflater, C14850m9 r4, AnonymousClass242 r5, C22210yi r6, AnonymousClass1AB r7, AbstractC116245Ur r8, int i) {
        super(context, layoutInflater, r4, i);
        this.A08 = r5;
        this.A09 = r6;
        this.A0A = r7;
        this.A0B = r8;
        this.A0C = r6.A0A;
    }

    @Override // X.AbstractC69213Yj
    public void A03(View view) {
        View findViewById = view.findViewById(R.id.empty);
        this.A01 = findViewById;
        findViewById.setVisibility(4);
        TextView A0J = C12960it.A0J(view, R.id.get_stickers_button);
        this.A02 = A0J;
        C27531Hw.A06(A0J);
        C12960it.A11(this.A02, this, 28);
        this.A03 = C12960it.A0J(view, R.id.empty_text);
        this.A04 = (WaImageView) view.findViewById(R.id.empty_image);
        this.A00 = view.findViewById(R.id.sticker_avatar_upsell);
    }

    @Override // X.AbstractC69213Yj, X.AnonymousClass5WC
    public void AP2(View view, ViewGroup viewGroup, int i) {
        super.AP2(view, viewGroup, i);
        C54392ge r1 = this.A05;
        if (r1 != null) {
            r1.A03 = null;
        }
        this.A01 = null;
    }
}
