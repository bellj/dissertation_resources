package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/* renamed from: X.2ZW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZW extends Drawable {
    public float A00;
    public float A01 = 1.0f;
    public float A02;
    public int A03;
    public boolean A04;
    public final int A05;
    public final Paint A06;
    public final Drawable A07;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZW(Context context, int i) {
        Drawable drawable;
        Paint A0A = C12960it.A0A();
        this.A06 = A0A;
        A0A.setStyle(Paint.Style.FILL);
        this.A00 = (C12960it.A01(context) * 3.0f) / 4.0f;
        this.A05 = (int) (C12960it.A01(context) * 48.0f);
        if (i != 0) {
            drawable = AnonymousClass00T.A04(context, i);
        } else {
            drawable = null;
        }
        this.A07 = drawable;
    }

    public void A00(float f, int i) {
        this.A02 = f;
        this.A03 = i;
        this.A01 = 1.0f;
        invalidateSelf();
    }

    public void A01(int i) {
        this.A03 = i;
        this.A01 = 1.0f;
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        float min = (float) ((Math.min(bounds.width(), bounds.height()) * 7) >> 4);
        if (this.A04) {
            Paint paint = this.A06;
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(this.A03);
            canvas.drawCircle((float) bounds.centerX(), (float) bounds.centerY(), min, paint);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(-1);
            canvas.drawCircle((float) bounds.centerX(), (float) bounds.centerY(), (this.A02 * 1.2f) / 2.0f, paint);
            if (Color.red(this.A03) > 240 && Color.green(this.A03) > 240 && Color.blue(this.A03) > 240) {
                C12990iw.A13(paint);
                paint.setStrokeWidth(this.A00);
                int min2 = 255 - ((Math.min(Math.min(Color.red(this.A03), Color.green(this.A03)), Color.blue(this.A03)) - 240) * 3);
                paint.setColor(Color.argb(255, min2, min2, min2));
                canvas.drawCircle((float) bounds.centerX(), (float) bounds.centerY(), (this.A02 * 1.2f) / 2.0f, paint);
                return;
            }
            return;
        }
        if (this.A03 != 0) {
            Paint paint2 = this.A06;
            paint2.setStyle(Paint.Style.FILL);
            paint2.setColor(this.A03);
            canvas.drawCircle((float) bounds.centerX(), (float) bounds.centerY(), this.A01 * min, paint2);
        }
        Drawable drawable = this.A07;
        if (drawable != null) {
            drawable.setBounds(bounds.centerX() - (drawable.getIntrinsicWidth() >> 1), bounds.centerY() - (drawable.getIntrinsicHeight() >> 1), bounds.centerX() + (drawable.getIntrinsicWidth() >> 1), bounds.centerY() + (drawable.getIntrinsicHeight() >> 1));
            drawable.draw(canvas);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        return this.A05;
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        return this.A05;
    }
}
