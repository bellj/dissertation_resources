package X;

/* renamed from: X.2uv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59552uv extends C37171lc {
    public final AnonymousClass4NA A00;
    public final String A01;

    public C59552uv(AnonymousClass4NA r2, String str) {
        super(AnonymousClass39o.A0U);
        this.A01 = str;
        this.A00 = r2;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A01.equals(((C59552uv) obj).A01);
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A01.hashCode();
    }
}
