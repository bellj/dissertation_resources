package X;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;
import com.whatsapp.payments.ui.IndiaUpiCheckBalanceActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentMethodSelectionActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;
import java.io.Serializable;
import java.util.List;

/* renamed from: X.6CU  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CU implements AbstractC1311861p {
    public final /* synthetic */ AbstractActivityC121525iS A00;
    public final /* synthetic */ PaymentMethodsListPickerFragment A01;

    @Override // X.AbstractC1311861p
    public List A9w(List list) {
        return null;
    }

    @Override // X.AbstractC1311861p
    public boolean AdV() {
        return false;
    }

    @Override // X.AbstractC1311861p
    public boolean AdZ() {
        return false;
    }

    @Override // X.AbstractC1311861p
    public void Adj(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ void onCreate() {
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ void onDestroy() {
    }

    public AnonymousClass6CU(AbstractActivityC121525iS r1, PaymentMethodsListPickerFragment paymentMethodsListPickerFragment) {
        this.A00 = r1;
        this.A01 = paymentMethodsListPickerFragment;
    }

    @Override // X.AbstractC1311861p
    public /* synthetic */ int AAc() {
        return R.string.payments_settings_add_new_account;
    }

    @Override // X.AbstractC1311861p
    public View AAd(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        AbstractActivityC121525iS r1 = this.A00;
        if (r1.A0g.size() == 0) {
            return null;
        }
        View inflate = layoutInflater.inflate(R.layout.india_upi_payment_bottomsheet_view_balance_row, (ViewGroup) null);
        C117295Zj.A0m(inflate, R.id.check_balance_icon, AnonymousClass00T.A00(r1, R.color.settings_icon));
        return inflate;
    }

    @Override // X.AbstractC1311861p
    public View AD5(LayoutInflater layoutInflater, FrameLayout frameLayout) {
        return C12960it.A0F(layoutInflater, frameLayout, R.layout.powered_by_upi_logo);
    }

    @Override // X.AbstractC1311861p
    public int AEM(AbstractC28901Pl r4) {
        AbstractActivityC121525iS r2 = this.A00;
        if (AbstractActivityC119235dO.A1k(r4, r2) || !r4.equals(r2.A0B)) {
            return 0;
        }
        return R.drawable.countrypicker_checkmark;
    }

    @Override // X.AbstractC1311861p
    public String AEP(AbstractC28901Pl r3) {
        AbstractActivityC121525iS r1 = this.A00;
        if (AbstractActivityC119235dO.A1k(r3, r1)) {
            return r1.getString(R.string.payment_method_unavailable);
        }
        return null;
    }

    @Override // X.AbstractC1311861p
    public String AEQ(AbstractC28901Pl r5) {
        AbstractActivityC121525iS r3 = this.A00;
        return C1311161i.A02(r3, ((AbstractActivityC121545iU) r3).A01, r5, ((AbstractActivityC121685jC) r3).A0P, false);
    }

    @Override // X.AbstractC1311861p
    public View AFP(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        AbstractActivityC121525iS r0 = this.A00;
        ((AbstractActivityC121665jA) r0).A0D.AKg(C12980iv.A0i(), null, "available_payment_methods_prompt", r0.A0d);
        return null;
    }

    @Override // X.AbstractC1311861p
    public void ALx() {
        AbstractActivityC121525iS r3 = this.A00;
        r3.A3O(57, "available_payment_methods_prompt");
        Intent A0D = C12990iw.A0D(r3, IndiaUpiBankPickerActivity.class);
        A0D.putExtra("extra_payments_entry_type", 6);
        A0D.putExtra("extra_is_first_payment_method", !AbstractActivityC119235dO.A1l(r3));
        A0D.putExtra("extra_skip_value_props_display", AbstractActivityC119235dO.A1l(r3));
        r3.startActivityForResult(A0D, 1008);
    }

    @Override // X.AbstractC1311861p
    public void AM1() {
        Intent intent;
        PaymentBottomSheet paymentBottomSheet = (PaymentBottomSheet) this.A01.A09();
        if (paymentBottomSheet != null) {
            paymentBottomSheet.A1K();
        }
        AbstractActivityC121525iS r3 = this.A00;
        if (r3.A0g.size() == 1) {
            C119755f3 r0 = (C119755f3) C117315Zl.A08(r3.A0g, 0).A08;
            if (r0 == null || C12970iu.A1Y(r0.A05.A00)) {
                AbstractC28901Pl A08 = C117315Zl.A08(r3.A0g, 0);
                intent = C12990iw.A0D(r3, IndiaUpiCheckBalanceActivity.class);
                C117315Zl.A0M(intent, A08);
            } else {
                C36021jC.A01(r3, 29);
                return;
            }
        } else {
            List list = r3.A0g;
            intent = C12990iw.A0D(r3, IndiaUpiPaymentMethodSelectionActivity.class);
            intent.putExtra("bank_accounts", (Serializable) list);
        }
        r3.startActivityForResult(intent, 1015);
        r3.A3O(62, "available_payment_methods_prompt");
    }

    @Override // X.AbstractC1311861p
    public void AMp() {
        this.A00.A3O(1, "available_payment_methods_prompt");
    }

    @Override // X.AbstractC1311861p
    public boolean AdN(AbstractC28901Pl r2) {
        return AbstractActivityC119235dO.A1k(r2, this.A00);
    }
}
