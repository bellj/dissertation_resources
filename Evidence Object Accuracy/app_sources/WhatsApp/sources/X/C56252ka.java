package X;

import android.text.TextUtils;
import java.util.HashMap;

/* renamed from: X.2ka  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56252ka extends AbstractC64703Go {
    public String A00;
    public String A01;
    public String A02;
    public String A03;

    public final void A03(C56252ka r2) {
        if (!TextUtils.isEmpty(this.A00)) {
            r2.A00 = this.A00;
        }
        if (!TextUtils.isEmpty(this.A01)) {
            r2.A01 = this.A01;
        }
        if (!TextUtils.isEmpty(this.A02)) {
            r2.A02 = this.A02;
        }
        if (!TextUtils.isEmpty(this.A03)) {
            r2.A03 = this.A03;
        }
    }

    public final String toString() {
        HashMap A11 = C12970iu.A11();
        A11.put("appName", this.A00);
        A11.put("appVersion", this.A01);
        A11.put("appId", this.A02);
        return AbstractC64703Go.A01("appInstallerId", this.A03, A11);
    }
}
