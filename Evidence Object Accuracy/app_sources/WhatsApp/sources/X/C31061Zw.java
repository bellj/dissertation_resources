package X;

import android.util.Pair;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.List;

/* renamed from: X.1Zw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31061Zw {
    public static final InetSocketAddress A00;
    public static final InetSocketAddress A01;

    static {
        try {
            A00 = new InetSocketAddress(InetAddress.getByAddress(new byte[]{8, 8, 8, 8}), 53);
            A01 = new InetSocketAddress(InetAddress.getByAddress(new byte[]{8, 8, 4, 4}), 53);
        } catch (UnknownHostException e) {
            throw new Error(e);
        }
    }

    public static Pair A00(byte[] bArr, int i) {
        int length;
        if (i < 0 || i >= (length = bArr.length)) {
            throw new UnknownHostException("offset is outside of the data array");
        }
        int i2 = -1;
        CharsetDecoder newDecoder = Charset.forName("UTF8").newDecoder();
        CodingErrorAction codingErrorAction = CodingErrorAction.REPORT;
        newDecoder.onMalformedInput(codingErrorAction);
        newDecoder.onUnmappableCharacter(codingErrorAction);
        StringBuilder sb = new StringBuilder();
        while (true) {
            byte b = bArr[i];
            int i3 = i + 1;
            if (((b >> 6) & 3) != 3) {
                if (b == 0) {
                    break;
                }
                i = i3 + b;
                if (i < length) {
                    try {
                        sb.append(newDecoder.decode(ByteBuffer.wrap(bArr, i3, b)).toString());
                        sb.append(".");
                    } catch (CharacterCodingException unused) {
                        throw new UnknownHostException("failed to parse canonical name");
                    }
                } else {
                    throw new UnknownHostException("failed to parse canonical name");
                }
            } else {
                int i4 = (b & 63) << 8;
                if (i3 < length) {
                    i2 = i4 + bArr[i3];
                } else {
                    throw new UnknownHostException("offset is outside of the data array, when getting a pointer");
                }
            }
        }
        return new Pair(sb, Integer.valueOf(i2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0216, code lost:
        if (r4.getMessage().contains("libcore.io.ErrnoException: connect failed: EINVAL") == false) goto L_0x0218;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x017e  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A01(java.lang.String r19, int r20) {
        /*
        // Method dump skipped, instructions count: 606
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C31061Zw.A01(java.lang.String, int):java.util.List");
    }

    public static void A02(AnonymousClass3JN r15, AnonymousClass3JN r16, String str, List list, byte[] bArr, int i) {
        short length;
        C63503Bv r2 = r16.A01;
        if (r2.A01 != r15.A01.A01) {
            StringBuilder sb = new StringBuilder("received response with unexpected id while trying to resolve ");
            sb.append(str);
            throw new UnknownHostException(sb.toString());
        } else if (!r2.A0A) {
            StringBuilder sb2 = new StringBuilder("did not receive response from server while trying to resolve ");
            sb2.append(str);
            throw new UnknownHostException(sb2.toString());
        } else if (r2.A0B) {
            StringBuilder sb3 = new StringBuilder("received truncated response while trying to resolve ");
            sb3.append(str);
            throw new UnknownHostException(sb3.toString());
        } else if (r2.A06 == 0) {
            long currentTimeMillis = System.currentTimeMillis();
            AnonymousClass4TB r9 = null;
            int i2 = 0;
            while (true) {
                AnonymousClass4TB[] r1 = r16.A02;
                if (i2 < r1.length) {
                    AnonymousClass4TB r14 = r1[i2];
                    if (r14.A03 == 1) {
                        if (r14.A04 == 5) {
                            r9 = r14;
                        } else if (r14.A04 != 1 && r14.A04 != 28) {
                            StringBuilder sb4 = new StringBuilder("unexpected type returned while trying to resolve ");
                            sb4.append(str);
                            throw new UnknownHostException(sb4.toString());
                        } else if (r14.A04 == 1 && ((short) r14.A05.length) != 4) {
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append("unexpected record length returned while trying to resolve ");
                            sb5.append(str);
                            throw new UnknownHostException(sb5.toString());
                        } else if (r14.A04 != 28 || (length = (short) r14.A05.length) == 16) {
                            AnonymousClass3IM r13 = r14.A02;
                            StringBuilder sb6 = new StringBuilder();
                            for (String str2 : r13.A02) {
                                sb6.append(str2);
                                sb6.append('.');
                            }
                            short s = r13.A01;
                            if (s != 0) {
                                for (String str3 : AnonymousClass3IM.A00(r16.A00, s + 0).A02) {
                                    sb6.append(str3);
                                    sb6.append('.');
                                }
                            }
                            if (sb6.length() > 0) {
                                sb6.setLength(sb6.length() - 1);
                            }
                            list.add(new C31071Zx(InetAddress.getByAddress(sb6.toString(), r14.A05), (((long) r14.A01) * 1000) + currentTimeMillis));
                            list.get(list.size() - 1);
                        } else {
                            StringBuilder sb7 = new StringBuilder();
                            sb7.append("unexpected record length returned while trying to resolve ");
                            sb7.append(str);
                            sb7.append(" ");
                            sb7.append((int) length);
                            throw new UnknownHostException(sb7.toString());
                        }
                        i2++;
                    } else {
                        StringBuilder sb8 = new StringBuilder("unexpected class returned while trying to resolve ");
                        sb8.append(str);
                        throw new UnknownHostException(sb8.toString());
                    }
                } else if (list.isEmpty() && r9 != null) {
                    Pair A002 = A00(r9.A05, 0);
                    StringBuilder sb9 = (StringBuilder) A002.first;
                    int intValue = ((Number) A002.second).intValue();
                    if (intValue != -1) {
                        sb9.append((CharSequence) A00(bArr, intValue).first);
                    }
                    String obj = sb9.toString();
                    if (!obj.equals(str)) {
                        list.addAll(A01(obj, i + 1));
                        return;
                    }
                    return;
                } else {
                    return;
                }
            }
        } else {
            StringBuilder sb10 = new StringBuilder("error code was set in response while trying to resolve ");
            sb10.append(str);
            throw new UnknownHostException(sb10.toString());
        }
    }
}
