package X;

import java.util.List;

/* renamed from: X.1Jd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27771Jd {
    public AnonymousClass1JR A00;
    public AnonymousClass1K3 A01;
    public Integer A02;
    public List A03;
    public byte[] A04;
    public byte[] A05;
    public final String A06;
    public final List A07;
    public final byte[] A08;

    public C27771Jd(String str, List list, byte[] bArr) {
        this.A06 = str;
        this.A07 = list;
        this.A08 = bArr;
    }

    public C27781Je A00() {
        String str;
        List list;
        AnonymousClass1K4 r0;
        if (this.A03 == null && this.A01 == null) {
            str = this.A06;
            list = this.A07;
            r0 = null;
        } else {
            AnonymousClass1G4 A0T = AnonymousClass1K4.A09.A0T();
            List list2 = this.A03;
            if (list2 != null) {
                A0T.A03();
                AnonymousClass1K4 r2 = (AnonymousClass1K4) A0T.A00;
                AnonymousClass1K6 r1 = r2.A04;
                if (!((AnonymousClass1K7) r1).A00) {
                    r1 = AbstractC27091Fz.A0G(r1);
                    r2.A04 = r1;
                }
                AnonymousClass1G5.A01(list2, r1);
            } else {
                AnonymousClass1K3 r02 = this.A01;
                if (r02 != null) {
                    A0T.A03();
                    AnonymousClass1K4 r12 = (AnonymousClass1K4) A0T.A00;
                    r12.A06 = r02;
                    r12.A00 |= 2;
                }
            }
            if (this.A00 != null) {
                AnonymousClass1G4 A0T2 = C27921Jt.A02.A0T();
                byte[] bArr = this.A00.A00;
                AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
                A0T2.A03();
                C27921Jt r13 = (C27921Jt) A0T2.A00;
                r13.A00 |= 1;
                r13.A01 = A01;
                A0T.A03();
                AnonymousClass1K4 r14 = (AnonymousClass1K4) A0T.A00;
                r14.A07 = (C27921Jt) A0T2.A02();
                r14.A00 |= 16;
            }
            byte[] bArr2 = this.A05;
            if (bArr2 != null) {
                AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
                A0T.A03();
                AnonymousClass1K4 r15 = (AnonymousClass1K4) A0T.A00;
                r15.A00 |= 4;
                r15.A03 = A012;
            }
            byte[] bArr3 = this.A04;
            if (bArr3 != null) {
                AbstractC27881Jp A013 = AbstractC27881Jp.A01(bArr3, 0, bArr3.length);
                A0T.A03();
                AnonymousClass1K4 r16 = (AnonymousClass1K4) A0T.A00;
                r16.A00 |= 8;
                r16.A02 = A013;
            }
            Integer num = this.A02;
            if (num != null) {
                int intValue = num.intValue();
                A0T.A03();
                AnonymousClass1K4 r17 = (AnonymousClass1K4) A0T.A00;
                r17.A00 |= 64;
                r17.A01 = intValue;
            }
            str = this.A06;
            list = this.A07;
            r0 = (AnonymousClass1K4) A0T.A02();
        }
        return new C27781Je(r0, str, list, this.A08);
    }
}
