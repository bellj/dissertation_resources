package X;

import java.util.List;

/* renamed from: X.3HB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HB {
    public boolean A00;
    public final C71183cW A01;
    public final List A02 = C12960it.A0l();
    public final int[] A03 = new int[5];

    public AnonymousClass3HB(C71183cW r2) {
        this.A01 = r2;
    }

    public static boolean A00(int[] iArr) {
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = iArr[i];
            if (i3 == 0) {
                break;
            }
            i2 += i3;
            i++;
            if (i >= 5) {
                if (i2 >= 7) {
                    float f = ((float) i2) / 7.0f;
                    float f2 = f / 2.0f;
                    if (C12980iv.A02(iArr, f, 0) >= f2 || C12980iv.A02(iArr, f, 1) >= f2 || C12980iv.A02(iArr, f * 3.0f, 2) >= 3.0f * f2 || C12980iv.A02(iArr, f, 3) >= f2 || C12980iv.A02(iArr, f, 4) >= f2) {
                        break;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean A01() {
        List<C82593vs> list = this.A02;
        int size = list.size();
        float f = 0.0f;
        int i = 0;
        float f2 = 0.0f;
        for (C82593vs r2 : list) {
            if (r2.A01 >= 2) {
                i++;
                f2 += r2.A00;
            }
        }
        if (i >= 3) {
            float f3 = f2 / ((float) size);
            for (C82593vs r0 : list) {
                f += Math.abs(r0.A00 - f3);
            }
            if (f <= f2 * 0.05f) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x015f, code lost:
        if (r0 >= r3) goto L_0x0167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0161, code lost:
        r13[3] = r0 + 1;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0167, code lost:
        if (r2 == r14) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x016b, code lost:
        if (r13[3] < r3) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x016f, code lost:
        if (r2 >= r14) goto L_0x0181;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0175, code lost:
        if (r12.A03(r2, r6) == false) goto L_0x0181;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0177, code lost:
        r0 = r13[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0179, code lost:
        if (r0 >= r3) goto L_0x0181;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x017b, code lost:
        r13[4] = r0 + 1;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0181, code lost:
        r4 = r13[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0183, code lost:
        if (r4 >= r3) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0185, code lost:
        r0 = r13[0] + r13[1];
        r3 = r13[2];
        r15 = r13[3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0197, code lost:
        if ((X.C12980iv.A05(((r0 + r3) + r15) + r4, r10) * 5) >= r10) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x019d, code lost:
        if (A00(r13) == false) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x019f, code lost:
        r2 = ((float) ((r2 - r4) - r15)) - (((float) r3) / 2.0f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        if (r12.A03(r2, r5) != false) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0049, code lost:
        r0 = r13[1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004b, code lost:
        if (r0 > r3) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        r13[1] = r0 + 1;
        r5 = r5 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0052, code lost:
        if (r5 < 0) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0057, code lost:
        if (r13[1] <= r3) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x02cf, code lost:
        r0 = r11.A01;
        r6 = r0 + 1;
        r4 = (float) r0;
        r3 = (r4 * ((X.AnonymousClass3HP) r11).A00) + r2;
        r2 = (float) r6;
        r7.set(r8, new X.C82593vs(r3 / r2, ((r4 * ((X.AnonymousClass3HP) r11).A01) + r5) / r2, ((r4 * r11.A00) + r10) / r2, r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x02ee, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0059, code lost:
        r5 = Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0072, code lost:
        if (r12.A03(r2, r5) == false) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0074, code lost:
        r0 = r13[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0076, code lost:
        if (r0 > r3) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0078, code lost:
        r13[0] = r0 + 1;
        r5 = r5 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        if (r5 < 0) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        if (r13[0] > r3) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0084, code lost:
        r4 = r20 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0086, code lost:
        if (r4 >= r7) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008c, code lost:
        if (r12.A03(r2, r4) == false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008e, code lost:
        r13[2] = r13[2] + 1;
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0096, code lost:
        if (r4 != r7) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x009a, code lost:
        if (r4 >= r7) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a0, code lost:
        if (r12.A03(r2, r4) != false) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a2, code lost:
        r0 = r13[3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00a4, code lost:
        if (r0 >= r3) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00a6, code lost:
        r13[3] = r0 + 1;
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ac, code lost:
        if (r4 == r7) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00b0, code lost:
        if (r13[3] < r3) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00b4, code lost:
        if (r4 >= r7) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ba, code lost:
        if (r12.A03(r2, r4) == false) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00bc, code lost:
        r0 = r13[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00be, code lost:
        if (r0 >= r3) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00c0, code lost:
        r13[4] = r0 + 1;
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00c6, code lost:
        r5 = r13[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00c8, code lost:
        if (r5 >= r3) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00ca, code lost:
        r6 = r13[2];
        r14 = r13[3];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00de, code lost:
        if ((X.C12980iv.A05((((r13[0] + r13[1]) + r6) + r14) + r5, r10) * 5) >= (r10 << 1)) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00e4, code lost:
        if (A00(r13) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00e6, code lost:
        r5 = ((float) ((r4 - r5) - r14)) - (((float) r6) / 2.0f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0105, code lost:
        if (r12.A03(r4, r6) != false) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0107, code lost:
        r0 = r13[1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0109, code lost:
        if (r0 > r3) goto L_0x0113;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x010b, code lost:
        r13[1] = r0 + 1;
        r4 = r4 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0110, code lost:
        if (r4 < 0) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0115, code lost:
        if (r13[1] <= r3) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0117, code lost:
        r2 = Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x012d, code lost:
        if (r12.A03(r4, r6) == false) goto L_0x013b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x012f, code lost:
        r0 = r13[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0131, code lost:
        if (r0 > r3) goto L_0x013b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0133, code lost:
        r13[0] = r0 + 1;
        r4 = r4 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0138, code lost:
        if (r4 < 0) goto L_0x013b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x013b, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x013e, code lost:
        if (r13[0] <= r3) goto L_0x0141;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0141, code lost:
        if (r2 >= r14) goto L_0x0151;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0147, code lost:
        if (r12.A03(r2, r6) == false) goto L_0x0151;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0149, code lost:
        r13[2] = r13[2] + 1;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0151, code lost:
        if (r2 != r14) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0155, code lost:
        if (r2 >= r14) goto L_0x0167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x015b, code lost:
        if (r12.A03(r2, r6) != false) goto L_0x0167;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x015d, code lost:
        r0 = r13[3];
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A02(int[] r19, int r20, int r21) {
        /*
        // Method dump skipped, instructions count: 763
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HB.A02(int[], int, int):boolean");
    }
}
