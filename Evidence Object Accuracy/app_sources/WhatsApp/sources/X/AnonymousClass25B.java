package X;

/* renamed from: X.25B  reason: invalid class name */
/* loaded from: classes2.dex */
public enum AnonymousClass25B {
    IS_INITIALIZED,
    VISIT,
    MERGE_FROM_STREAM,
    MAKE_IMMUTABLE,
    NEW_MUTABLE_INSTANCE,
    NEW_BUILDER,
    GET_DEFAULT_INSTANCE,
    GET_PARSER
}
