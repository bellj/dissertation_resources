package X;

import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.1pw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39461pw {
    public static String A00(String str, String str2, String str3, String str4) {
        Uri.Builder path = Uri.parse("https://static.whatsapp.net").buildUpon().path("downloadable");
        path.appendQueryParameter("category", str);
        if (str2 != null) {
            path.appendQueryParameter("locale", str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            path.appendQueryParameter("existing_id", str3);
        }
        if (!TextUtils.isEmpty(str4)) {
            path.appendQueryParameter("version", str4);
        }
        return path.build().toString();
    }

    public static String A01(Map map) {
        Uri.Builder path = Uri.parse("https://static.whatsapp.net").buildUpon().path("downloadable");
        for (Map.Entry entry : map.entrySet()) {
            path.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
        }
        return path.build().toString();
    }

    public static void A02(String str) {
        StringBuilder sb = new StringBuilder("DownloadableUtils/reportCriticalEventIfBeta");
        sb.append(str);
        Log.e(sb.toString());
    }

    public static boolean A03(String str) {
        char[] charArray = str.toCharArray();
        for (char c : charArray) {
            if (!(Character.isLetterOrDigit(c) || c == '-' || c == '_')) {
                return false;
            }
        }
        return true;
    }
}
