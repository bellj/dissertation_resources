package X;

import android.content.Context;
import android.text.TextUtils;

/* renamed from: X.0Oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05040Oa {
    public Context A00;
    public AnonymousClass0SX A01;
    public String A02;
    public boolean A03;

    public C05040Oa(Context context) {
        this.A00 = context;
    }

    public C04820Ne A00() {
        if (this.A01 != null) {
            Context context = this.A00;
            if (context == null) {
                throw new IllegalArgumentException("Must set a non-null context to create the configuration.");
            } else if (!this.A03 || !TextUtils.isEmpty(this.A02)) {
                return new C04820Ne(context, this.A01, this.A02, this.A03);
            } else {
                throw new IllegalArgumentException("Must set a non-null database name to a configuration that uses the no backup directory.");
            }
        } else {
            throw new IllegalArgumentException("Must set a callback to create the configuration.");
        }
    }
}
