package X;

import androidx.fragment.app.DialogFragment;

/* renamed from: X.0kS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC13860kS {
    boolean AJN();

    void AaN();

    void Adl(DialogFragment dialogFragment, String str);

    void Adm(DialogFragment dialogFragment);

    void Ado(int i);

    @Deprecated
    void Adp(String str);

    void Adq(AnonymousClass2GV v, Object[] objArr, int i, int i2, int i3);

    void Adr(Object[] objArr, int i, int i2);

    void Ady(int i, int i2);

    void AfX(String str);
}
