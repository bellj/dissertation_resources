package X;

import java.io.Serializable;

/* renamed from: X.3cR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71133cR implements Serializable {
    public static final long serialVersionUID = 1;
    public AnonymousClass4AP countryCodeSource_ = AnonymousClass4AP.FROM_NUMBER_WITH_PLUS_SIGN;
    public int countryCode_ = 0;
    public String extension_ = "";
    public boolean hasCountryCode;
    public boolean hasCountryCodeSource;
    public boolean hasExtension;
    public boolean hasItalianLeadingZero;
    public boolean hasNationalNumber;
    public boolean hasPreferredDomesticCarrierCode;
    public boolean hasRawInput;
    public boolean hasSecondLeadingZero;
    public boolean italianLeadingZero_ = false;
    public long nationalNumber_ = 0;
    public String preferredDomesticCarrierCode_ = "";
    public String rawInput_ = "";
    public boolean secondLeadingZero_ = false;

    public void A00(C71133cR r4) {
        if (r4.hasCountryCode) {
            int i = r4.countryCode_;
            this.hasCountryCode = true;
            this.countryCode_ = i;
        }
        if (r4.hasNationalNumber) {
            long j = r4.nationalNumber_;
            this.hasNationalNumber = true;
            this.nationalNumber_ = j;
        }
        if (r4.hasExtension) {
            String str = r4.extension_;
            this.hasExtension = true;
            this.extension_ = str;
        }
        if (r4.hasItalianLeadingZero) {
            boolean z = r4.italianLeadingZero_;
            this.hasItalianLeadingZero = true;
            this.italianLeadingZero_ = z;
        }
        if (r4.hasRawInput) {
            String str2 = r4.rawInput_;
            this.hasRawInput = true;
            this.rawInput_ = str2;
        }
        if (r4.hasCountryCodeSource) {
            AnonymousClass4AP r1 = r4.countryCodeSource_;
            this.hasCountryCodeSource = true;
            this.countryCodeSource_ = r1;
        }
        if (r4.hasPreferredDomesticCarrierCode) {
            String str3 = r4.preferredDomesticCarrierCode_;
            this.hasPreferredDomesticCarrierCode = true;
            this.preferredDomesticCarrierCode_ = str3;
        }
        if (r4.hasSecondLeadingZero) {
            boolean z2 = r4.secondLeadingZero_;
            this.hasSecondLeadingZero = true;
            this.secondLeadingZero_ = z2;
        }
    }

    public boolean A01(C71133cR r7) {
        if (r7 == null) {
            return false;
        }
        if (this == r7) {
            return true;
        }
        if (this.countryCode_ == r7.countryCode_ && this.nationalNumber_ == r7.nationalNumber_ && this.extension_.equals(r7.extension_) && this.italianLeadingZero_ == r7.italianLeadingZero_ && this.rawInput_.equals(r7.rawInput_) && this.countryCodeSource_ == r7.countryCodeSource_ && this.preferredDomesticCarrierCode_.equals(r7.preferredDomesticCarrierCode_) && this.hasPreferredDomesticCarrierCode == r7.hasPreferredDomesticCarrierCode && this.secondLeadingZero_ == r7.secondLeadingZero_) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return (obj instanceof C71133cR) && A01((C71133cR) obj);
    }

    @Override // java.lang.Object
    public int hashCode() {
        int A08 = ((C12990iw.A08(Long.valueOf(this.nationalNumber_), (2173 + this.countryCode_) * 53) * 53) + this.extension_.hashCode()) * 53;
        int i = 1231;
        int i2 = 1237;
        if (this.italianLeadingZero_) {
            i2 = 1231;
        }
        int A082 = ((C12990iw.A08(this.countryCodeSource_, (((A08 + i2) * 53) + this.rawInput_.hashCode()) * 53) * 53) + this.preferredDomesticCarrierCode_.hashCode()) * 53;
        int i3 = 1237;
        if (this.hasPreferredDomesticCarrierCode) {
            i3 = 1231;
        }
        int i4 = (A082 + i3) * 53;
        if (!this.secondLeadingZero_) {
            i = 1237;
        }
        return i4 + i;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("Country Code: ");
        A0k.append(this.countryCode_);
        A0k.append(" National Number: ");
        A0k.append(this.nationalNumber_);
        if (this.hasItalianLeadingZero && this.italianLeadingZero_) {
            A0k.append(" Leading Zero: true");
        }
        if (this.hasExtension) {
            A0k.append(" Extension: ");
            A0k.append(this.extension_);
        }
        if (this.hasCountryCodeSource) {
            A0k.append(" Country Code Source: ");
            A0k.append(this.countryCodeSource_);
        }
        if (this.hasPreferredDomesticCarrierCode) {
            A0k.append(" Preferred Domestic Carrier Code: ");
            A0k.append(this.preferredDomesticCarrierCode_);
        }
        if (this.hasSecondLeadingZero && this.secondLeadingZero_) {
            A0k.append(" Second Leading Zero: true");
        }
        return A0k.toString();
    }
}
