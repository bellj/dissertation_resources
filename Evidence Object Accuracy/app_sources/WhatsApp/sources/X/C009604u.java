package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.04u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C009604u extends ViewGroup.MarginLayoutParams {
    public int A00;

    public C009604u() {
        super(-2, -2);
        this.A00 = 0;
        this.A00 = 21;
    }

    public C009604u(int i, int i2) {
        super(i, i2);
        this.A00 = 0;
        this.A00 = 8388627;
    }

    public C009604u(C009604u r2) {
        super((ViewGroup.MarginLayoutParams) r2);
        this.A00 = 0;
        this.A00 = r2.A00;
    }

    public C009604u(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.A00 = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass07O.A01);
        this.A00 = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
    }

    public C009604u(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
        this.A00 = 0;
    }
}
