package X;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2RZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2RZ implements AbstractC50792Ra {
    public final /* synthetic */ AnonymousClass1UK A00;
    public final /* synthetic */ AnonymousClass2RW A01;
    public final /* synthetic */ AnonymousClass2RY A02;
    public final /* synthetic */ C13690kA A03;

    public AnonymousClass2RZ(AnonymousClass1UK r1, AnonymousClass2RW r2, AnonymousClass2RY r3, C13690kA r4) {
        this.A00 = r1;
        this.A03 = r4;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC50792Ra
    public final void AON(Status status) {
        Exception r1;
        if (status.A01 <= 0) {
            AnonymousClass1UK r5 = this.A00;
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            BasePendingResult basePendingResult = (BasePendingResult) r5;
            C13020j0.A04("Result has already been consumed.", !basePendingResult.A0B);
            try {
                if (!basePendingResult.A09.await(0, timeUnit)) {
                    basePendingResult.A06(Status.A0A);
                }
            } catch (InterruptedException unused) {
                basePendingResult.A06(Status.A08);
            }
            boolean z = false;
            if (basePendingResult.A09.getCount() == 0) {
                z = true;
            }
            C13020j0.A04("Result is not ready.", z);
            this.A03.A01(this.A01.A7h(BasePendingResult.A00(basePendingResult)));
            return;
        }
        C13690kA r2 = this.A03;
        if (status.A02 != null) {
            r1 = new C77703ni(status);
        } else {
            r1 = new C630439z(status);
        }
        r2.A00.A07(r1);
    }
}
