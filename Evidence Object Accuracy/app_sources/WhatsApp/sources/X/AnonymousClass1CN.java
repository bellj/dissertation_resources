package X;

import android.content.Context;
import com.whatsapp.R;
import java.io.File;

/* renamed from: X.1CN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CN {
    public final C38721ob A00;

    public AnonymousClass1CN(C14900mE r10, C18790t3 r11, C16590pI r12, C18810t5 r13) {
        Context context = r12.A00;
        C38771og r3 = new C38771og(r10, r11, r13, new File(context.getCacheDir(), "biz_directory_cache"), "directory-image");
        r3.A00 = context.getResources().getDimensionPixelSize(R.dimen.avatar_size_big);
        r3.A01 = 16777216;
        r3.A05 = true;
        this.A00 = r3.A00();
    }
}
