package X;

/* renamed from: X.4CS  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CS extends IllegalStateException {
    public Throwable cause;

    public AnonymousClass4CS(String str, Throwable th) {
        super(str);
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
