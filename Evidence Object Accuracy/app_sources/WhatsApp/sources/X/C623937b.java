package X;

import android.graphics.Matrix;
import android.graphics.Rect;
import java.lang.ref.WeakReference;

/* renamed from: X.37b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C623937b extends AbstractC16350or {
    public final Matrix A00;
    public final Rect A01;
    public final Rect A02;
    public final WeakReference A03;

    public C623937b(Matrix matrix, Rect rect, Rect rect2, AbstractC115325Rc r5) {
        this.A02 = rect;
        this.A01 = rect2;
        this.A00 = matrix;
        this.A03 = C12970iu.A10(r5);
    }
}
