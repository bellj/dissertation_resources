package X;

import org.json.JSONObject;

/* renamed from: X.2Rx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50932Rx {
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final boolean A04;

    public C50932Rx(AnonymousClass1V8 r3, long j, long j2) {
        this.A03 = j;
        this.A01 = r3.A06(r3.A0H("redeemed_count"), "redeemed_count");
        this.A00 = r3.A06(r3.A0H("reserved_count"), "reserved_count");
        this.A04 = "1".equalsIgnoreCase(r3.A0H("is_eligible"));
        this.A02 = j2;
    }

    public C50932Rx(String str) {
        JSONObject jSONObject = new JSONObject(str);
        this.A03 = jSONObject.getLong("offer_id");
        this.A04 = jSONObject.getBoolean("is_eligible");
        this.A00 = jSONObject.getInt("pending_count");
        this.A01 = jSONObject.getInt("redeemed_count");
        this.A02 = jSONObject.getLong("last_sync_time_ms");
    }
}
