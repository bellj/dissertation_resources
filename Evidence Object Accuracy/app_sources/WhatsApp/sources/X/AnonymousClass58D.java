package X;

import com.whatsapp.util.Log;

/* renamed from: X.58D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass58D implements AnonymousClass2S1 {
    public final /* synthetic */ AnonymousClass2S2 A00;

    public AnonymousClass58D(AnonymousClass2S2 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2S1
    public void APk() {
        Log.e("PAY: PaymentIncentiveManager/syncIncentiveData/refreshUserClaimInfo failed");
        this.A00.A00.APk();
    }

    @Override // X.AnonymousClass2S1
    public void AX0(C50932Rx r3) {
        AnonymousClass2S2 r0 = this.A00;
        r0.A00.AWz(r0.A01.A00());
    }
}
