package X;

import com.whatsapp.polls.PollVoterViewModel;
import java.util.Comparator;

/* renamed from: X.5Ci  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112175Ci implements Comparator {
    public final /* synthetic */ PollVoterViewModel A00;

    public C112175Ci(PollVoterViewModel pollVoterViewModel) {
        this.A00 = pollVoterViewModel;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return AnonymousClass048.A00(((C27701Iu) obj2).A00, ((C27701Iu) obj).A00);
    }
}
