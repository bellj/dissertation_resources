package X;

import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;

/* renamed from: X.5KJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KJ extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $handler;
    public final /* synthetic */ AnonymousClass28G this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KJ(AnonymousClass28G r2, AnonymousClass1J7 r3) {
        super(1);
        this.this$0 = r2;
        this.$handler = r3;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        AnonymousClass28G r3 = this.this$0;
        r3.A00.A0H(new RunnableBRunnable0Shape3S0300000_I1(obj, this.$handler, r3, 17));
        return AnonymousClass1WZ.A00;
    }
}
