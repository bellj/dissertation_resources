package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.2uE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59242uE extends AnonymousClass32K {
    public final C63913Dk A00;
    public final C19830uk A01;
    public final AnonymousClass4Tm A02;
    public final C59062tu A03;
    public final C18640sm A04;
    public final C19840ul A05;

    public C59242uE(C63913Dk r9, C19810ui r10, C17560r0 r11, AnonymousClass4RQ r12, C17570r1 r13, C19830uk r14, AnonymousClass4Tm r15, C59062tu r16, C18640sm r17, C19840ul r18, AbstractC14440lR r19) {
        super(r10, r11, r12, r13, r19, 4);
        this.A03 = r16;
        this.A01 = r14;
        this.A02 = r15;
        this.A05 = r18;
        this.A04 = r17;
        this.A00 = r9;
    }

    @Override // X.AnonymousClass44L
    public void A00(AnonymousClass3H5 r4, JSONObject jSONObject, int i) {
        A04();
        Log.e(C12960it.A0W(i, "GetCollectionsGraphQLService/onErrorResponse/error - "));
        if (!A03(this.A02.A05, r4.A00, true)) {
            this.A00.A00(i);
        }
    }

    public final void A04() {
        AnonymousClass4Tm r1 = this.A02;
        if (r1.A06 == null) {
            int i = r1.A02;
            C19840ul r12 = this.A05;
            if (i == 0) {
                r12.A02("collection_management_view_tag");
                return;
            }
            AnonymousClass1Q5 A00 = C19840ul.A00(r12);
            if (A00 != null) {
                A00.A07("datasource_collections");
            }
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        A04();
        Log.e("GetCollectionsGraphQLService/onDeliveryFailure");
        if (!A03(this.A02.A05, -1, false)) {
            this.A00.A00(-1);
        }
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("GetCollectionsGraphQLService/onDirectConnectionError, jid = ")));
        this.A00.A00(422);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        A02();
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        A04();
        Log.e("GetCollectionsGraphQLService/onError/error - 0");
        if (!A03(this.A02.A05, 0, false)) {
            this.A00.A00(0);
        }
    }
}
