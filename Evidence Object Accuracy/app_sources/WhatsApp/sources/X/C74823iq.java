package X;

import com.whatsapp.StickyHeadersRecyclerView;

/* renamed from: X.3iq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74823iq extends AnonymousClass0QE {
    public final /* synthetic */ StickyHeadersRecyclerView A00;

    public C74823iq(StickyHeadersRecyclerView stickyHeadersRecyclerView) {
        this.A00 = stickyHeadersRecyclerView;
    }

    @Override // X.AnonymousClass0QE
    public void A00() {
        StickyHeadersRecyclerView stickyHeadersRecyclerView = this.A00;
        stickyHeadersRecyclerView.A02 = -1;
        stickyHeadersRecyclerView.A00 = -1;
    }
}
