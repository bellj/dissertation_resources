package X;

/* renamed from: X.5wQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128635wQ {
    public final /* synthetic */ C117925az A00;

    public /* synthetic */ C128635wQ(C117925az r1) {
        this.A00 = r1;
    }

    public final void A00(C30821Yy r7, C30821Yy r8, C452120p r9) {
        C117925az r5 = this.A00;
        AnonymousClass016 r1 = r5.A01;
        C127115ty r0 = new C127115ty();
        r0.A01 = true;
        r1.A0B(r0);
        if (r9 != null || r7 == null || r8 == null) {
            r5.A06.A04(C12960it.A0d(r9.A09, C12960it.A0k("error: ")));
            C127755v0 r12 = new C127755v0(1);
            r12.A00 = r9;
            r5.A07.A0B(r12);
            return;
        }
        C127755v0 r4 = new C127755v0(0);
        AbstractC30791Yv r3 = C30771Yt.A05;
        AnonymousClass018 r2 = r5.A03;
        r4.A01 = r3.AAA(r2, r7, 0);
        r4.A02 = r3.AAA(r2, r8, 0);
        r5.A07.A0B(r4);
    }
}
