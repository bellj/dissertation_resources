package X;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0101000_I0;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/* renamed from: X.0mL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C14970mL implements AbstractC14980mM, AbstractC15000mO, AbstractC15020mQ {
    public int A00 = 0;
    public C56492ky A01 = null;
    public boolean A02;
    public final int A03;
    public final AbstractC72443eb A04;
    public final AnonymousClass4XF A05;
    public final AnonymousClass3CU A06;
    public final BinderC56702lR A07;
    public final List A08 = new ArrayList();
    public final Map A09 = new HashMap();
    public final Queue A0A = new LinkedList();
    public final Set A0B = new HashSet();
    public final /* synthetic */ C65863Lh A0C;

    public C14970mL(AnonymousClass2RP r13, C65863Lh r14) {
        this.A0C = r14;
        Handler handler = r14.A06;
        Looper looper = handler.getLooper();
        AnonymousClass4RB A00 = r13.A00();
        AnonymousClass01b r11 = A00.A00;
        AnonymousClass3BW r6 = new AnonymousClass3BW(A00.A01, A00.A02, A00.A03, null, r11);
        AbstractC77683ng r1 = r13.A04.A00;
        C13020j0.A01(r1);
        AbstractC72443eb A002 = r1.A00(r13.A01, looper, this, this, r6, r13.A03);
        String str = r13.A09;
        if (str != null && (A002 instanceof AbstractC95064d1)) {
            ((AbstractC95064d1) A002).A0R = str;
        }
        this.A04 = A002;
        this.A05 = r13.A06;
        this.A06 = new AnonymousClass3CU();
        this.A03 = r13.A00;
        if (A002.Aae()) {
            Context context = r14.A05;
            AnonymousClass4RB A003 = r13.A00();
            AnonymousClass01b r112 = A003.A00;
            this.A07 = new BinderC56702lR(context, handler, new AnonymousClass3BW(A003.A01, A003.A02, A003.A03, null, r112));
        }
    }

    public final void A00() {
        Queue queue = this.A0A;
        ArrayList arrayList = new ArrayList(queue);
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            AnonymousClass3HY r1 = (AnonymousClass3HY) arrayList.get(i);
            if (this.A04.isConnected()) {
                if (A0C(r1)) {
                    queue.remove(r1);
                }
            } else {
                return;
            }
        }
    }

    public final void A01() {
        Handler handler = this.A0C.A06;
        C13020j0.A00(handler);
        this.A01 = null;
        A06(C56492ky.A04);
        if (this.A02) {
            AnonymousClass4XF r1 = this.A05;
            handler.removeMessages(11, r1);
            handler.removeMessages(9, r1);
            this.A02 = false;
        }
        Iterator it = this.A09.values().iterator();
        if (it.hasNext()) {
            it.next();
            throw new NullPointerException("zaa");
        }
        A00();
        A02();
    }

    public final void A02() {
        C65863Lh r4 = this.A0C;
        Handler handler = r4.A06;
        AnonymousClass4XF r1 = this.A05;
        handler.removeMessages(12, r1);
        handler.sendMessageDelayed(handler.obtainMessage(12, r1), r4.A00);
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [X.5Yh, X.3eb] */
    public final void A03() {
        C65863Lh r5 = this.A0C;
        C13020j0.A00(r5.A06);
        AbstractC72443eb r4 = this.A04;
        if (!r4.isConnected() && !r4.AJG()) {
            try {
                int A00 = r5.A08.A00(r5.A05, r4);
                if (A00 != 0) {
                    C56492ky r7 = new C56492ky(A00, null);
                    String name = r4.getClass().getName();
                    String obj = r7.toString();
                    StringBuilder sb = new StringBuilder(name.length() + 35 + obj.length());
                    sb.append("The service for ");
                    sb.append(name);
                    sb.append(" is not available: ");
                    sb.append(obj);
                    Log.w("GoogleApiManager", sb.toString());
                    A07(r7, null);
                    return;
                }
                AnonymousClass3T1 r3 = new AnonymousClass3T1(r4, this.A05, r5);
                if (r4.Aae()) {
                    BinderC56702lR r9 = this.A07;
                    C13020j0.A01(r9);
                    AnonymousClass5Yh r0 = r9.A01;
                    if (r0 != null) {
                        r0.A8y();
                    }
                    AnonymousClass3BW r11 = r9.A05;
                    r11.A00 = Integer.valueOf(System.identityHashCode(r9));
                    AbstractC77683ng r6 = r9.A04;
                    Context context = r9.A02;
                    Handler handler = r9.A03;
                    r9.A01 = r6.A00(context, handler.getLooper(), r9, r9, r11, r11.A01);
                    r9.A00 = r3;
                    Set set = r9.A06;
                    if (set == null || set.isEmpty()) {
                        handler.post(new RunnableBRunnable0Shape14S0100000_I1(r9, 7));
                    } else {
                        AbstractC95064d1 r1 = (AbstractC95064d1) r9.A01;
                        r1.A7X(new C108394yz(r1));
                    }
                }
                try {
                    r4.A7X(r3);
                } catch (SecurityException e) {
                    A07(new C56492ky(10), e);
                }
            } catch (IllegalStateException e2) {
                A07(new C56492ky(10), e2);
            }
        }
    }

    public final void A04() {
        C13020j0.A00(this.A0C.A06);
        Status status = C65863Lh.A0G;
        A08(status);
        this.A06.A00(status, false);
        for (C92544Wi r2 : (C92544Wi[]) this.A09.keySet().toArray(new C92544Wi[0])) {
            A0A(new C56332ki(r2, new C13690kA()));
        }
        A06(new C56492ky(4));
        if (this.A04.isConnected()) {
            AnonymousClass4IQ r3 = new AnonymousClass4IQ(this);
            r3.A00.A0C.A06.post(new RunnableBRunnable0Shape14S0100000_I1(r3, 6));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007e A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A05(int r8) {
        /*
            r7 = this;
            X.3Lh r5 = r7.A0C
            android.os.Handler r4 = r5.A06
            X.C13020j0.A00(r4)
            r0 = 0
            r7.A01 = r0
            r0 = 1
            r7.A02 = r0
            X.3CU r6 = r7.A06
            X.3eb r0 = r7.A04
            X.4d1 r0 = (X.AbstractC95064d1) r0
            java.lang.String r2 = r0.A0S
            java.lang.String r0 = "The connection to Google Play services was lost"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r3 = 1
            if (r8 != r3) goto L_0x0078
            java.lang.String r0 = " due to service disconnection."
        L_0x0021:
            r1.append(r0)
        L_0x0024:
            if (r2 == 0) goto L_0x002e
            java.lang.String r0 = " Last reason for disconnect: "
            r1.append(r0)
            r1.append(r2)
        L_0x002e:
            r2 = 20
            java.lang.String r1 = r1.toString()
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status
            r0.<init>(r2, r1)
            r6.A00(r0, r3)
            X.4XF r3 = r7.A05
            r0 = 9
            android.os.Message r2 = android.os.Message.obtain(r4, r0, r3)
            r0 = 5000(0x1388, double:2.4703E-320)
            r4.sendMessageDelayed(r2, r0)
            r0 = 11
            android.os.Message r2 = android.os.Message.obtain(r4, r0, r3)
            r0 = 120000(0x1d4c0, double:5.9288E-319)
            r4.sendMessageDelayed(r2, r0)
            X.3Gx r0 = r5.A08
            android.util.SparseIntArray r0 = r0.A01
            r0.clear()
            java.util.Map r0 = r7.A09
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x007e
            r1.next()
            java.lang.String r1 = "zac"
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>(r1)
            throw r0
        L_0x0078:
            r0 = 3
            if (r8 != r0) goto L_0x0024
            java.lang.String r0 = " due to dead object exception."
            goto L_0x0021
        L_0x007e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14970mL.A05(int):void");
    }

    public final void A06(C56492ky r4) {
        Set set = this.A0B;
        Iterator it = set.iterator();
        if (it.hasNext()) {
            it.next();
            if (C13300jT.A00(r4, C56492ky.A04)) {
                AbstractC95064d1 r1 = (AbstractC95064d1) this.A04;
                if (!r1.isConnected() || r1.A0B == null) {
                    throw new RuntimeException("Failed to connect when checking package");
                }
            }
            throw new NullPointerException("zac");
        }
        set.clear();
    }

    public final void A07(C56492ky r9, Exception exc) {
        Status A00;
        AnonymousClass5Yh r0;
        C65863Lh r5 = this.A0C;
        Handler handler = r5.A06;
        C13020j0.A00(handler);
        BinderC56702lR r02 = this.A07;
        if (!(r02 == null || (r0 = r02.A01) == null)) {
            r0.A8y();
        }
        C13020j0.A00(handler);
        this.A01 = null;
        r5.A08.A01.clear();
        A06(r9);
        if ((this.A04 instanceof C77943o7) && r9.A01 != 24) {
            r5.A04 = true;
            handler.sendMessageDelayed(handler.obtainMessage(19), 300000);
        }
        int i = r9.A01;
        if (i == 4) {
            A00 = C65863Lh.A0H;
        } else {
            Queue queue = this.A0A;
            if (queue.isEmpty()) {
                this.A01 = r9;
                return;
            } else if (exc != null) {
                C13020j0.A00(handler);
                A09(null, exc, false);
                return;
            } else {
                boolean z = r5.A0E;
                AnonymousClass4XF r1 = this.A05;
                if (z) {
                    A09(C65863Lh.A00(r9, r1), null, true);
                    if (!queue.isEmpty() && !A0B(r9) && !r5.A07(r9, this.A03)) {
                        if (i == 18) {
                            this.A02 = true;
                        }
                        if (this.A02) {
                            handler.sendMessageDelayed(Message.obtain(handler, 9, r1), 5000);
                            return;
                        }
                    } else {
                        return;
                    }
                }
                A00 = C65863Lh.A00(r9, r1);
            }
        }
        A08(A00);
    }

    public final void A08(Status status) {
        C13020j0.A00(this.A0C.A06);
        A09(status, null, false);
    }

    public final void A09(Status status, Exception exc, boolean z) {
        C13020j0.A00(this.A0C.A06);
        boolean z2 = false;
        boolean z3 = true;
        if (status != null) {
            z3 = false;
        }
        if (exc == null) {
            z2 = true;
        }
        if (z3 != z2) {
            Iterator it = this.A0A.iterator();
            while (it.hasNext()) {
                AnonymousClass3HY r2 = (AnonymousClass3HY) it.next();
                if (!z || r2.A00 == 2) {
                    if (status != null) {
                        r2.A01(status);
                    } else {
                        r2.A04(exc);
                    }
                    it.remove();
                }
            }
            return;
        }
        throw new IllegalArgumentException("Status XOR exception should be null");
    }

    public final void A0A(AnonymousClass3HY r3) {
        C13020j0.A00(this.A0C.A06);
        if (!this.A04.isConnected()) {
            this.A0A.add(r3);
            C56492ky r1 = this.A01;
            if (r1 == null || !r1.A00()) {
                A03();
            } else {
                A07(r1, null);
            }
        } else if (A0C(r3)) {
            A02();
        } else {
            this.A0A.add(r3);
        }
    }

    public final boolean A0B(C56492ky r7) {
        synchronized (C65863Lh.A0I) {
            C65863Lh r2 = this.A0C;
            if (r2.A01 == null || !r2.A0A.contains(this.A05)) {
                return false;
            }
            DialogInterface$OnCancelListenerC56312kg r4 = r2.A01;
            AnonymousClass4MG r3 = new AnonymousClass4MG(r7, this.A03);
            if (r4.A04.compareAndSet(null, r3)) {
                r4.A00.post(new RunnableBRunnable0Shape10S0200000_I1(r4, 14, r3));
            }
            return true;
        }
    }

    public final boolean A0C(AnonymousClass3HY r20) {
        AbstractC77873o0 r7;
        C78603pB[] A06;
        int length;
        C78603pB[] r14;
        if (!(r20 instanceof AbstractC77873o0) || (A06 = (r7 = (AbstractC77873o0) r20).A06(this)) == null || (length = A06.length) == 0) {
            AnonymousClass3CU r2 = this.A06;
            AbstractC72443eb r1 = this.A04;
            r20.A02(r2, r1.Aae());
            try {
                r20.A03(this);
                return true;
            } catch (DeadObjectException unused) {
                onConnectionSuspended(1);
                AbstractC95064d1 r12 = (AbstractC95064d1) r1;
                r12.A0S = "DeadObjectException thrown while running ApiCallRunner.";
                r12.A8y();
                return true;
            }
        } else {
            AbstractC72443eb r9 = this.A04;
            C56472kw r0 = ((AbstractC95064d1) r9).A0Q;
            if (r0 == null) {
                r14 = null;
            } else {
                r14 = r0.A03;
            }
            int i = 0;
            if (r14 == null) {
                r14 = new C78603pB[0];
            }
            int length2 = r14.length;
            AnonymousClass00N r4 = new AnonymousClass00N(length2);
            for (C78603pB r15 : r14) {
                String str = r15.A02;
                long j = r15.A01;
                if (j == -1) {
                    j = (long) r15.A00;
                }
                r4.put(str, Long.valueOf(j));
            }
            do {
                C78603pB r3 = A06[i];
                Number number = (Number) r4.get(r3.A02);
                if (number != null) {
                    long longValue = number.longValue();
                    long j2 = r3.A01;
                    if (j2 == -1) {
                        j2 = (long) r3.A00;
                    }
                    if (longValue >= j2) {
                        i++;
                    }
                }
                String name = r9.getClass().getName();
                String str2 = r3.A02;
                long j3 = r3.A01;
                if (j3 == -1) {
                    j3 = (long) r3.A00;
                }
                StringBuilder sb = new StringBuilder(name.length() + 77 + String.valueOf(str2).length());
                sb.append(name);
                sb.append(" could not execute call because it requires feature (");
                sb.append(str2);
                sb.append(", ");
                sb.append(j3);
                sb.append(").");
                Log.w("GoogleApiManager", sb.toString());
                C65863Lh r42 = this.A0C;
                if (!r42.A0E || !r7.A05(this)) {
                    r7.A04(new AnonymousClass5HQ(r3));
                    return true;
                }
                AnonymousClass3F2 r72 = new AnonymousClass3F2(r3, this.A05);
                List list = this.A08;
                int indexOf = list.indexOf(r72);
                if (indexOf >= 0) {
                    Object obj = list.get(indexOf);
                    Handler handler = r42.A06;
                    handler.removeMessages(15, obj);
                    handler.sendMessageDelayed(Message.obtain(handler, 15, obj), 5000);
                    return false;
                }
                list.add(r72);
                Handler handler2 = r42.A06;
                handler2.sendMessageDelayed(Message.obtain(handler2, 15, r72), 5000);
                handler2.sendMessageDelayed(Message.obtain(handler2, 16, r72), 120000);
                C56492ky r13 = new C56492ky(2, null);
                if (A0B(r13)) {
                    return false;
                }
                r42.A07(r13, this.A03);
                return false;
            } while (i < length);
            AnonymousClass3CU r2 = this.A06;
            AbstractC72443eb r1 = this.A04;
            r20.A02(r2, r1.Aae());
            r20.A03(this);
            return true;
        }
    }

    @Override // X.AbstractC14990mN
    public final void onConnected(Bundle bundle) {
        Looper myLooper = Looper.myLooper();
        Handler handler = this.A0C.A06;
        if (myLooper == handler.getLooper()) {
            A01();
        } else {
            handler.post(new RunnableBRunnable0Shape0S0100000_I0(this, 4));
        }
    }

    @Override // X.AbstractC15010mP
    public final void onConnectionFailed(C56492ky r2) {
        A07(r2, null);
    }

    @Override // X.AbstractC14990mN
    public final void onConnectionSuspended(int i) {
        Looper myLooper = Looper.myLooper();
        Handler handler = this.A0C.A06;
        if (myLooper == handler.getLooper()) {
            A05(i);
        } else {
            handler.post(new RunnableBRunnable0Shape0S0101000_I0(this, i, 0));
        }
    }
}
