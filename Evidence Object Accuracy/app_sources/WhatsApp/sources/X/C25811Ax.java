package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1Ax  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25811Ax {
    public final C14650lo A00;
    public final AnonymousClass19Q A01;
    public final C14850m9 A02;
    public final AbstractC14440lR A03;

    public C25811Ax(C14650lo r1, AnonymousClass19Q r2, C14850m9 r3, AbstractC14440lR r4) {
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = r4;
        this.A02 = r3;
    }

    public final void A00(UserJid userJid, Boolean bool, Integer num, Integer num2, String str, String str2, int i, int i2) {
        if (this.A02.A07(1514)) {
            this.A03.Ab2(new Runnable(userJid, bool, num, num2, str, str2, i, i2) { // from class: X.2jK
                public final /* synthetic */ int A00;
                public final /* synthetic */ int A01;
                public final /* synthetic */ UserJid A03;
                public final /* synthetic */ Boolean A04;
                public final /* synthetic */ Integer A05;
                public final /* synthetic */ Integer A06;
                public final /* synthetic */ String A07;
                public final /* synthetic */ String A08;

                {
                    this.A03 = r2;
                    this.A07 = r6;
                    this.A00 = r8;
                    this.A05 = r4;
                    this.A06 = r5;
                    this.A04 = r3;
                    this.A08 = r7;
                    this.A01 = r9;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    Long l;
                    C25811Ax r5 = C25811Ax.this;
                    UserJid userJid2 = this.A03;
                    String str3 = this.A07;
                    int i3 = this.A00;
                    Integer num3 = this.A05;
                    Integer num4 = this.A06;
                    Boolean bool2 = this.A04;
                    String str4 = this.A08;
                    int i4 = this.A01;
                    C30141Wg A00 = r5.A00.A09.A00(userJid2);
                    if (A00 != null) {
                        if (A00.A0H) {
                            str3 = null;
                        }
                        int intValue = num4.intValue();
                        AnonymousClass19Q r6 = r5.A01;
                        Integer valueOf = Integer.valueOf(i3);
                        if (num3 != null) {
                            l = C12980iv.A0l(num3.intValue());
                        } else {
                            l = null;
                        }
                        Long A0l = C12980iv.A0l(intValue);
                        Integer valueOf2 = Integer.valueOf(i4);
                        AnonymousClass31B r1 = new AnonymousClass31B();
                        r1.A06 = str3;
                        r1.A07 = userJid2.getRawString();
                        r1.A08 = r6.A00;
                        r1.A01 = valueOf;
                        r1.A03 = l;
                        r1.A04 = A0l;
                        r1.A00 = bool2;
                        r1.A09 = str4;
                        r1.A05 = C12980iv.A0l(r6.A08.getAndIncrement());
                        r1.A02 = valueOf2;
                        r6.A04.A07(r1);
                    }
                }
            });
        }
    }

    public void A01(UserJid userJid, String str, int i, int i2, int i3, boolean z) {
        A00(userJid, Boolean.valueOf(z), Integer.valueOf(i3), Integer.valueOf(i), str, null, i2, 1);
    }
}
