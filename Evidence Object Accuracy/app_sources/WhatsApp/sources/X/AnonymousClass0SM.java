package X;

import java.nio.FloatBuffer;

/* renamed from: X.0SM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0SM {
    public static final FloatBuffer A07;
    public static final FloatBuffer A08;
    public static final FloatBuffer A09;
    public static final FloatBuffer A0A;
    public static final FloatBuffer A0B;
    public static final FloatBuffer A0C;
    public static final float[] A0D;
    public static final float[] A0E;
    public static final float[] A0F;
    public static final float[] A0G;
    public static final float[] A0H;
    public static final float[] A0I;
    public int A00 = 2;
    public int A01 = 8;
    public int A02 = (A0D.length / 2);
    public int A03 = 8;
    public EnumC03740Iy A04;
    public FloatBuffer A05 = A08;
    public FloatBuffer A06 = A07;

    static {
        float[] fArr = {0.0f, 0.57735026f, -0.5f, -0.28867513f, 0.5f, -0.28867513f};
        A0H = fArr;
        float[] fArr2 = {0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};
        A0I = fArr2;
        A0B = AnonymousClass0UM.A02(fArr);
        A0C = AnonymousClass0UM.A02(fArr2);
        float[] fArr3 = {-0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f};
        A0F = fArr3;
        float[] fArr4 = {0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f};
        A0G = fArr4;
        A09 = AnonymousClass0UM.A02(fArr3);
        A0A = AnonymousClass0UM.A02(fArr4);
        float[] fArr5 = {-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f};
        A0D = fArr5;
        float[] fArr6 = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};
        A0E = fArr6;
        A07 = AnonymousClass0UM.A02(fArr5);
        A08 = AnonymousClass0UM.A02(fArr6);
    }

    public AnonymousClass0SM(EnumC03740Iy r3) {
        this.A04 = r3;
    }

    public String toString() {
        EnumC03740Iy r2 = this.A04;
        if (r2 == null) {
            return "[Drawable2d: ...]";
        }
        StringBuilder sb = new StringBuilder("[Drawable2d: ");
        sb.append(r2);
        sb.append("]");
        return sb.toString();
    }
}
