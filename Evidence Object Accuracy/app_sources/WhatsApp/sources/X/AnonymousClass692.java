package X;

import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity;

/* renamed from: X.692  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass692 implements AnonymousClass6Ln {
    public final /* synthetic */ IndiaUpiSendPaymentActivity A00;
    public final /* synthetic */ Runnable A01;

    public /* synthetic */ AnonymousClass692(IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity, Runnable runnable) {
        this.A00 = indiaUpiSendPaymentActivity;
        this.A01 = runnable;
    }

    @Override // X.AnonymousClass6Ln
    public final void AVN(UserJid userJid, AnonymousClass1ZR r12, AnonymousClass1ZR r13, AnonymousClass1ZR r14, C452120p r15, String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        int i;
        int i2;
        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity = this.A00;
        Runnable runnable = this.A01;
        ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0i = z2;
        indiaUpiSendPaymentActivity.AaN();
        if (z && r15 == null) {
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A06 = r12;
            ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A0M = str;
            ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0C = userJid;
            indiaUpiSendPaymentActivity.A0p = str2;
            indiaUpiSendPaymentActivity.A0r = z5;
            indiaUpiSendPaymentActivity.A0t = z2;
            if (z3) {
                ((AbstractActivityC121525iS) indiaUpiSendPaymentActivity).A0J.A00(indiaUpiSendPaymentActivity, new AnonymousClass1P3(runnable) { // from class: X.66u
                    public final /* synthetic */ Runnable A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // X.AnonymousClass1P3
                    public final void AVM(boolean z6) {
                        IndiaUpiSendPaymentActivity indiaUpiSendPaymentActivity2 = IndiaUpiSendPaymentActivity.this;
                        Runnable runnable2 = this.A01;
                        if (z6) {
                            runnable2.run();
                        } else if (indiaUpiSendPaymentActivity2.A09) {
                            indiaUpiSendPaymentActivity2.A00 = 3;
                            indiaUpiSendPaymentActivity2.A3b();
                        } else {
                            C36021jC.A01(indiaUpiSendPaymentActivity2, 22);
                        }
                    }
                }, userJid, ((AbstractActivityC121665jA) indiaUpiSendPaymentActivity).A08, true, false);
            } else {
                runnable.run();
            }
        } else if (indiaUpiSendPaymentActivity.A09) {
            if (r15 == null || !((i2 = r15.A00) == -2 || i2 == 6 || i2 == 7)) {
                i = 4;
            } else {
                i = 2;
            }
            indiaUpiSendPaymentActivity.A00 = i;
            indiaUpiSendPaymentActivity.A3b();
        } else {
            Object[] A1b = C12970iu.A1b();
            A1b[0] = indiaUpiSendPaymentActivity.getString(R.string.india_upi_payment_id_name);
            indiaUpiSendPaymentActivity.Adr(A1b, 0, R.string.payment_id_cannot_verify_error_text_default);
        }
    }
}
