package X;

import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.whatsapp.util.Log;

/* renamed from: X.0mJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14950mJ {
    public static boolean A00() {
        try {
            return Environment.isExternalStorageRemovable();
        } catch (Exception e) {
            Log.w("StorageUtils/checkifremovable/error ", e);
            return true;
        }
    }

    public long A01() {
        long j;
        long j2;
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if (Build.VERSION.SDK_INT >= 18) {
                j = statFs.getAvailableBlocksLong();
                j2 = statFs.getBlockSizeLong();
            } else {
                j = (long) statFs.getAvailableBlocks();
                j2 = (long) statFs.getBlockSize();
            }
            return j * j2;
        } catch (ArithmeticException | IllegalArgumentException e) {
            Log.w("StorageUtils/avail-external-storage/error/", e);
            return 0;
        }
    }

    public long A02() {
        long availableBlocks;
        long blockSize;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            availableBlocks = statFs.getAvailableBlocksLong();
            blockSize = statFs.getBlockSizeLong();
        } else {
            availableBlocks = (long) statFs.getAvailableBlocks();
            blockSize = (long) statFs.getBlockSize();
        }
        return availableBlocks * blockSize;
    }

    public long A03() {
        long j;
        long j2;
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if (Build.VERSION.SDK_INT >= 18) {
                j = statFs.getBlockCountLong();
                j2 = statFs.getBlockSizeLong();
            } else {
                j = (long) statFs.getBlockCount();
                j2 = (long) statFs.getBlockSize();
            }
            return j * j2;
        } catch (IllegalArgumentException e) {
            Log.w("StorageUtils/total-external-storage/error/illegal-arg", e);
            return 0;
        }
    }

    public long A04() {
        long blockCount;
        long blockSize;
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        if (Build.VERSION.SDK_INT >= 18) {
            blockCount = statFs.getBlockCountLong();
            blockSize = statFs.getBlockSizeLong();
        } else {
            blockCount = (long) statFs.getBlockCount();
            blockSize = (long) statFs.getBlockSize();
        }
        return blockCount * blockSize;
    }
}
