package X;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.4yi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108224yi implements AbstractC50792Ra {
    public final /* synthetic */ BasePendingResult A00;
    public final /* synthetic */ AnonymousClass3CU A01;

    public C108224yi(BasePendingResult basePendingResult, AnonymousClass3CU r2) {
        this.A01 = r2;
        this.A00 = basePendingResult;
    }

    @Override // X.AbstractC50792Ra
    public final void AON(Status status) {
        this.A01.A00.remove(this.A00);
    }
}
