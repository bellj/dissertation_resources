package X;

import android.content.Context;

/* renamed from: X.4MJ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4MJ {
    public final Context A00;
    public final Context A01;

    public AnonymousClass4MJ(Context context) {
        C13020j0.A01(context);
        Context applicationContext = context.getApplicationContext();
        C13020j0.A02(applicationContext, "Application context can't be null");
        this.A00 = applicationContext;
        this.A01 = applicationContext;
    }
}
