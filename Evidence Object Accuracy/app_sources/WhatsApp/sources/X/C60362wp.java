package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.mediaview.PhotoView;
import java.util.Iterator;

/* renamed from: X.2wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60362wp extends AbstractC58382oj {
    public final /* synthetic */ MediaComposerActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60362wp(AnonymousClass01F r1, MediaComposerActivity mediaComposerActivity) {
        super(r1);
        this.A00 = mediaComposerActivity;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00.A2i().size();
    }

    @Override // X.AbstractC58382oj, X.AnonymousClass01A
    public void A0A(ViewGroup viewGroup) {
        super.A0A(viewGroup);
        MediaComposerActivity mediaComposerActivity = this.A00;
        if (mediaComposerActivity.A2e() < 0 && !mediaComposerActivity.A2i().isEmpty()) {
            mediaComposerActivity.A2q(0);
        }
        Iterator A0o = ActivityC13810kN.A0o(mediaComposerActivity);
        while (A0o.hasNext()) {
            AnonymousClass01E r2 = (AnonymousClass01E) A0o.next();
            if (r2 instanceof MediaComposerFragment) {
                MediaComposerFragment mediaComposerFragment = (MediaComposerFragment) r2;
                mediaComposerFragment.A1E(mediaComposerActivity.A18);
                if (mediaComposerActivity.A0x && mediaComposerFragment.A00.equals(mediaComposerActivity.AAj())) {
                    mediaComposerFragment.A1B();
                    mediaComposerFragment.A1A();
                }
            }
        }
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ int A0F(Object obj) {
        int indexOf = this.A00.A2i().indexOf(((MediaComposerFragment) ((AnonymousClass01E) obj)).A00);
        if (indexOf < 0) {
            return -2;
        }
        return A0L(indexOf);
    }

    @Override // X.AbstractC58382oj
    public void A0K(ViewGroup viewGroup, AnonymousClass01E r4, int i) {
        PhotoView photoView;
        if (r4 instanceof ImageComposerFragment) {
            ((ImageComposerFragment) r4).A06.A00();
        } else {
            View view = r4.A0A;
            if (!(view == null || (photoView = (PhotoView) view.findViewById(R.id.photo)) == null)) {
                photoView.A01();
            }
        }
        super.A0I(viewGroup, r4, i);
    }

    public final int A0L(int i) {
        MediaComposerActivity mediaComposerActivity = this.A00;
        return !C28141Kv.A01(mediaComposerActivity.A0M) ? C12990iw.A09(mediaComposerActivity.A2i(), i) - 1 : i;
    }
}
