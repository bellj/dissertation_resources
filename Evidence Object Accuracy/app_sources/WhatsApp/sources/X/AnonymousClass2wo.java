package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.StatusPlaybackActivity;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import com.whatsapp.status.playback.fragment.StatusPlaybackFragment;

/* renamed from: X.2wo  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2wo extends AbstractC58382oj {
    public final /* synthetic */ StatusPlaybackActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2wo(AnonymousClass01F r1, StatusPlaybackActivity statusPlaybackActivity) {
        super(r1);
        this.A00 = statusPlaybackActivity;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        StatusPlaybackActivity statusPlaybackActivity = this.A00;
        AnonymousClass3EE r1 = statusPlaybackActivity.A0D;
        if (r1 == null || !statusPlaybackActivity.A0J) {
            return 0;
        }
        return r1.A00.size();
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ int A0F(Object obj) {
        AnonymousClass01E r5 = (AnonymousClass01E) obj;
        if (r5 instanceof StatusPlaybackFragment) {
            StatusPlaybackActivity statusPlaybackActivity = this.A00;
            AnonymousClass3EE r1 = statusPlaybackActivity.A0D;
            UserJid userJid = ((StatusPlaybackContactFragment) ((StatusPlaybackFragment) r5)).A0P;
            AnonymousClass009.A05(userJid);
            int A00 = r1.A00(userJid.getRawString());
            if (A00 >= 0 && A00 < statusPlaybackActivity.A0D.A00.size()) {
                return A00;
            }
        }
        return -2;
    }
}
