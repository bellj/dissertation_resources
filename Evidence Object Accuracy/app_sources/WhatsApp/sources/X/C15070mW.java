package X;

import android.content.SharedPreferences;

/* renamed from: X.0mW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C15070mW extends AbstractC15040mS {
    public long A00 = -1;
    public SharedPreferences A01;
    public long A02;
    public final C64563Ga A03 = new C64563Ga(this, ((Number) C88904Hw.A0F.A00()).longValue());

    public C15070mW(C14160kx r4) {
        super(r4);
    }

    public final long A0H() {
        C14170ky.A00();
        A0G();
        long j = this.A02;
        if (j == 0) {
            j = this.A01.getLong("first_run", 0);
            if (j == 0) {
                j = System.currentTimeMillis();
                SharedPreferences.Editor edit = this.A01.edit();
                edit.putLong("first_run", j);
                if (!edit.commit()) {
                    A0A("Failed to commit first run time");
                }
            }
            this.A02 = j;
        }
        return j;
    }
}
