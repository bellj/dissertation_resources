package X;

import com.whatsapp.registration.report.BanReportViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.1yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44321yj implements AbstractC44311yh {
    public final /* synthetic */ BanReportViewModel A00;

    public C44321yj(BanReportViewModel banReportViewModel) {
        this.A00 = banReportViewModel;
    }

    @Override // X.AbstractC44311yh
    public void APk() {
        Log.i("BanReportViewModel/export-report/on-error");
        this.A00.A02.A0B(1);
    }

    @Override // X.AbstractC44311yh
    public void AUd(String str) {
        StringBuilder sb = new StringBuilder("BanReportViewModel/export-report/on-ready-to-export :: ");
        sb.append(str);
        Log.i(sb.toString());
        this.A00.A01.A0A(str);
    }
}
