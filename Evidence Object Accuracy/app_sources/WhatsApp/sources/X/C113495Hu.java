package X;

import java.util.AbstractCollection;
import java.util.Iterator;

/* renamed from: X.5Hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113495Hu extends AbstractCollection<V> {
    public final /* synthetic */ AnonymousClass5I4 this$0;

    public C113495Hu(AnonymousClass5I4 r1) {
        this.this$0 = r1;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public void clear() {
        this.this$0.clear();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return this.this$0.valuesIterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public int size() {
        return this.this$0.size();
    }
}
