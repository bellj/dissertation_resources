package X;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.2ga  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54352ga extends AnonymousClass02M {
    public int A00 = -1;
    public AnonymousClass4KS A01;
    public final List A02 = C12960it.A0l();

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.size();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v3, types: [android.view.View] */
    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r7, int i) {
        TextEmojiLabel textEmojiLabel;
        AbstractC75243jX r72 = (AbstractC75243jX) r7;
        if (getItemViewType(i) == 0) {
            AnonymousClass42P r73 = (AnonymousClass42P) r72;
            String str = ((C93354a2) this.A02.get(i)).A01;
            boolean isEmpty = TextUtils.isEmpty(str);
            View view = r73.A0H;
            if (isEmpty) {
                view.setVisibility(8);
                return;
            }
            view.setVisibility(0);
            r73.A00.A0G(null, str);
            return;
        }
        AnonymousClass1ZB r4 = ((C93354a2) this.A02.get(i)).A00;
        C61172zX r74 = (C61172zX) r72;
        if (r4 == null) {
            textEmojiLabel = r74.A0H;
        } else {
            r74.A00.setChecked(C12960it.A1V(r74.A00(), r74.A03.A00));
            r74.A0H.setVisibility(0);
            r74.A02.A0G(null, r4.A02);
            String str2 = r4.A00;
            boolean isEmpty2 = TextUtils.isEmpty(str2);
            TextEmojiLabel textEmojiLabel2 = r74.A01;
            textEmojiLabel = textEmojiLabel2;
            if (!isEmpty2) {
                textEmojiLabel2.A0G(null, str2);
                textEmojiLabel2.setVisibility(0);
                return;
            }
        }
        textEmojiLabel.setVisibility(8);
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater A0E = C12960it.A0E(viewGroup);
        if (i == 0) {
            return new AnonymousClass42P(A0E.inflate(R.layout.select_list_bottom_sheet_section_item, viewGroup, false), this);
        }
        return new C61172zX(A0E.inflate(R.layout.select_list_bottom_sheet_item, viewGroup, false), this);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return (!TextUtils.isEmpty(((C93354a2) this.A02.get(i)).A01) ? 1 : 0) ^ 1;
    }
}
