package X;

import X.C49632Lo;
import android.content.res.Resources;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.camera.CameraBottomSheetBehavior;
import com.whatsapp.camera.DragBottomSheetIndicator;

/* renamed from: X.2Lo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49632Lo {
    public View A00;
    public View A01;
    public DragBottomSheetIndicator A02;
    public AnonymousClass2UD A03;
    public C457522x A04;
    public AbstractC16350or A05;
    public final Resources A06;
    public final View A07;
    public final View A08;
    public final RecyclerView A09;
    public final BottomSheetBehavior A0A = new CameraBottomSheetBehavior() { // from class: com.whatsapp.camera.bottomsheet.CameraBottomSheetController$3
        public boolean A00;

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
        public boolean A0C(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
            if (!this.A00 || !super.A0C(motionEvent, view, coordinatorLayout)) {
                return false;
            }
            return motionEvent.getPointerCount() < 2 || motionEvent.getY() > ((float) C49632Lo.this.A07.getTop());
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
        public boolean A0D(MotionEvent motionEvent, View view, CoordinatorLayout coordinatorLayout) {
            return this.A00 && super.A0D(motionEvent, view, coordinatorLayout);
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
        public boolean A0G(View view, CoordinatorLayout coordinatorLayout, int i) {
            this.A00 = true;
            return super.A0G(view, coordinatorLayout, i);
        }
    };
    public final boolean A0B;

    public C49632Lo(View view, boolean z) {
        this.A07 = view;
        if (!z) {
            this.A01 = AnonymousClass028.A0D(view, R.id.swipe_up_hint);
            this.A00 = AnonymousClass028.A0D(view, R.id.drag_indicator_layout);
            this.A02 = (DragBottomSheetIndicator) AnonymousClass028.A0D(view, R.id.drag_indicator);
        }
        this.A09 = (RecyclerView) AnonymousClass028.A0D(view, R.id.recent_media);
        this.A08 = AnonymousClass028.A0D(view, R.id.gallery_container);
        this.A06 = view.getResources();
        this.A0B = z;
        View view2 = this.A08;
        view2.setVisibility(8);
        view2.setAlpha(0.0f);
        this.A0A.A0L(this.A06.getDimensionPixelSize(this.A0B ? R.dimen.new_camera_gallery_peek_height : R.dimen.camera_gallery_peek_height));
        ((AnonymousClass0B5) this.A07.getLayoutParams()).A00(this.A0A);
    }

    public void A00() {
        AnonymousClass2UD r0 = this.A03;
        if (r0 != null) {
            r0.A02();
        }
    }

    public void A01() {
        boolean z = false;
        if (this.A0A.A0B == 4) {
            z = true;
        }
        int i = 0;
        RecyclerView recyclerView = this.A09;
        if (!z) {
            recyclerView.setVisibility(4);
            recyclerView.setAlpha(0.0f);
            View view = this.A00;
            if (view != null) {
                view.setVisibility(4);
            }
            View view2 = this.A08;
            view2.setVisibility(0);
            view2.setAlpha(1.0f);
            return;
        }
        recyclerView.setVisibility(0);
        recyclerView.setAlpha(1.0f);
        View view3 = this.A00;
        if (view3 != null) {
            if (!A04()) {
                i = 8;
            }
            view3.setVisibility(i);
        }
        View view4 = this.A08;
        view4.setVisibility(4);
        view4.setAlpha(0.0f);
    }

    public void A02() {
        View view = this.A07;
        if (view.getVisibility() != 4) {
            view.setVisibility(4);
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
            animationSet.addAnimation(new AlphaAnimation(1.0f, 0.0f));
            animationSet.addAnimation(translateAnimation);
            animationSet.setDuration(300);
            view.startAnimation(animationSet);
        }
    }

    public void A03(boolean z) {
        if (this.A0A.A0B != 4) {
            return;
        }
        if (!z || this.A06.getConfiguration().orientation == 2) {
            A02();
            return;
        }
        this.A07.setVisibility(0);
        A01();
    }

    public boolean A04() {
        if (this.A0B) {
            return true;
        }
        AnonymousClass2UD r0 = this.A03;
        return r0 != null && r0.A0D() >= 12;
    }
}
