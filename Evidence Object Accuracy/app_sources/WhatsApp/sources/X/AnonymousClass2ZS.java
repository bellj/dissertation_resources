package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;

/* renamed from: X.2ZS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZS extends Drawable {
    public final /* synthetic */ Drawable A00;
    public final /* synthetic */ ViewGroup A01;
    public final /* synthetic */ MessageDetailsActivity A02;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZS(Drawable drawable, ViewGroup viewGroup, MessageDetailsActivity messageDetailsActivity) {
        this.A02 = messageDetailsActivity;
        this.A00 = drawable;
        this.A01 = viewGroup;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Drawable drawable = this.A00;
        int intrinsicHeight = drawable.getIntrinsicHeight();
        int intrinsicWidth = drawable.getIntrinsicWidth();
        ViewGroup viewGroup = this.A01;
        int width = viewGroup.getWidth();
        int height = viewGroup.getHeight();
        int i = width * intrinsicHeight;
        int i2 = height * intrinsicWidth;
        if (i > i2) {
            height = i / intrinsicWidth;
        } else {
            width = i2 / intrinsicHeight;
        }
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
    }
}
