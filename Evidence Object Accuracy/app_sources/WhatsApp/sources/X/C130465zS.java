package X;

import java.util.ArrayList;

/* renamed from: X.5zS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130465zS {
    public static final ArrayList A01;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "BANK";
        A01 = C12960it.A0m("CARD", strArr, 1);
    }

    public C130465zS(String str, byte[] bArr) {
        C41141sy r4 = new C41141sy("credential");
        if (AnonymousClass3JT.A0E(str, 1, 100, false)) {
            C41141sy.A01(r4, "key_type", str);
        }
        AnonymousClass3JT.A0C(bArr, -9007199254740991L, 9007199254740991L);
        r4.A01 = bArr;
        r4.A0A("CARD", "type", A01);
        this.A00 = r4.A03();
    }
}
