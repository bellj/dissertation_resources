package X;

import com.whatsapp.payments.ui.NoviPayHubTransactionHistoryActivity;

/* renamed from: X.69R  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass69R implements AbstractC35651iS {
    public final /* synthetic */ NoviPayHubTransactionHistoryActivity A00;

    public AnonymousClass69R(NoviPayHubTransactionHistoryActivity noviPayHubTransactionHistoryActivity) {
        this.A00 = noviPayHubTransactionHistoryActivity;
    }

    @Override // X.AbstractC35651iS
    public void ATe(AnonymousClass1IR r3) {
        if (r3.A01 == 3) {
            this.A00.A2f();
        }
    }

    @Override // X.AbstractC35651iS
    public void ATf(AnonymousClass1IR r3) {
        if (r3.A01 == 3) {
            this.A00.A2f();
        }
    }
}
