package X;

/* renamed from: X.0oF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15980oF {
    public final C15950oC A00;
    public final String A01;

    public C15980oF(C15950oC r1, String str) {
        this.A01 = str;
        this.A00 = r1;
    }

    public String[] A00() {
        C15950oC r2 = this.A00;
        return new String[]{this.A01, r2.A02, String.valueOf(r2.A01), String.valueOf(r2.A00)};
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C15980oF)) {
            return false;
        }
        C15980oF r4 = (C15980oF) obj;
        if (!this.A01.equals(r4.A01) || !this.A00.equals(r4.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.A01.hashCode() ^ this.A00.hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003a, code lost:
        if (r0 == null) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r7 = this;
            java.lang.String r4 = r7.A01
            java.lang.String r5 = "@"
            boolean r0 = r4.contains(r5)
            r3 = 4
            if (r0 == 0) goto L_0x003c
            int r1 = r4.indexOf(r5)
            r0 = 0
            java.lang.String r6 = r4.substring(r0, r1)
            int r0 = r1 + 1
            java.lang.String r2 = r4.substring(r0)
            java.lang.String r0 = "g.us"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0057
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r0 = 15
            java.lang.String r0 = X.AnonymousClass1US.A02(r0, r6)
        L_0x002d:
            r1.append(r0)
            r1.append(r5)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            if (r0 != 0) goto L_0x0040
        L_0x003c:
            java.lang.String r0 = X.AnonymousClass1US.A02(r3, r4)
        L_0x0040:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = "::"
            r1.append(r0)
            X.0oC r0 = r7.A00
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            return r0
        L_0x0057:
            java.lang.String r0 = "broadcast"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x003c
            X.1VX r0 = X.AnonymousClass1VX.A00
            java.lang.String r0 = r0.getRawString()
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x0081
            X.0oD r0 = X.C15960oD.A00
            java.lang.String r0 = r0.getRawString()
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x0081
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r0 = X.AnonymousClass1US.A02(r3, r6)
            goto L_0x002d
        L_0x0081:
            r0 = r4
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15980oF.toString():java.lang.String");
    }
}
