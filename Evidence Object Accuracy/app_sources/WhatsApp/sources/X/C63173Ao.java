package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Ao  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63173Ao {
    public static AnonymousClass2Lx A00(AnonymousClass1G3 r7, C28891Pk r8) {
        AnonymousClass2M1 r1;
        int i;
        AnonymousClass2Ly r0 = r7.A05().A03;
        if (r0 == null) {
            r0 = AnonymousClass2Ly.A07;
        }
        AnonymousClass2Lx r4 = (AnonymousClass2Lx) r0.A0T();
        String str = r8.A01;
        AnonymousClass2Ly r12 = (AnonymousClass2Ly) AnonymousClass1G4.A00(r4);
        r12.A00 |= 32;
        r12.A04 = str;
        String str2 = r8.A02;
        if (!TextUtils.isEmpty(str2)) {
            AnonymousClass2Ly r13 = (AnonymousClass2Ly) AnonymousClass1G4.A00(r4);
            r13.A00 |= 64;
            r13.A05 = str2;
        }
        List<C30761Ys> list = r8.A04;
        if (list != null && !list.isEmpty()) {
            ArrayList A0l = C12960it.A0l();
            for (C30761Ys r6 : list) {
                AnonymousClass1G4 A0T = AnonymousClass2M1.A04.A0T();
                int i2 = r6.A03;
                if (i2 == 1) {
                    AnonymousClass1G4 A0T2 = AnonymousClass2mU.A03.A0T();
                    String str3 = r6.A05;
                    AnonymousClass2mU r14 = (AnonymousClass2mU) AnonymousClass1G4.A00(A0T2);
                    r14.A00 |= 2;
                    r14.A02 = str3;
                    String str4 = r6.A04;
                    AnonymousClass2mU r15 = (AnonymousClass2mU) AnonymousClass1G4.A00(A0T2);
                    r15.A00 |= 1;
                    r15.A01 = str4;
                    r1 = (AnonymousClass2M1) AnonymousClass1G4.A00(A0T);
                    r1.A03 = A0T2.A02();
                    i = 1;
                } else if (i2 == 3) {
                    AnonymousClass1G4 A0T3 = C57102mT.A03.A0T();
                    String str5 = r6.A04;
                    C57102mT r16 = (C57102mT) AnonymousClass1G4.A00(A0T3);
                    r16.A00 |= 1;
                    r16.A01 = str5;
                    String str6 = r6.A05;
                    C57102mT r17 = (C57102mT) AnonymousClass1G4.A00(A0T3);
                    r17.A00 |= 2;
                    r17.A02 = str6;
                    r1 = (AnonymousClass2M1) AnonymousClass1G4.A00(A0T);
                    r1.A03 = A0T3.A02();
                    i = 3;
                } else if (i2 == 2) {
                    AnonymousClass1G4 A0T4 = AnonymousClass2M3.A03.A0T();
                    String str7 = r6.A04;
                    AnonymousClass2M3 r18 = (AnonymousClass2M3) AnonymousClass1G4.A00(A0T4);
                    r18.A00 |= 1;
                    r18.A01 = str7;
                    String str8 = r6.A05;
                    AnonymousClass2M3 r19 = (AnonymousClass2M3) AnonymousClass1G4.A00(A0T4);
                    r19.A00 |= 2;
                    r19.A02 = str8;
                    AbstractC27091Fz A02 = A0T4.A02();
                    r1 = (AnonymousClass2M1) AnonymousClass1G4.A00(A0T);
                    r1.A03 = A02;
                    i = 2;
                } else {
                    int i3 = r6.A02;
                    AnonymousClass2M1 r110 = (AnonymousClass2M1) AnonymousClass1G4.A00(A0T);
                    r110.A00 |= 8;
                    r110.A02 = i3;
                    A0l.add(A0T.A02());
                }
                r1.A01 = i;
                int i3 = r6.A02;
                AnonymousClass2M1 r110 = (AnonymousClass2M1) AnonymousClass1G4.A00(A0T);
                r110.A00 |= 8;
                r110.A02 = i3;
                A0l.add(A0T.A02());
            }
            AnonymousClass2Ly r2 = (AnonymousClass2Ly) AnonymousClass1G4.A00(r4);
            AnonymousClass1K6 r111 = r2.A02;
            if (!((AnonymousClass1K7) r111).A00) {
                r111 = AbstractC27091Fz.A0G(r111);
                r2.A02 = r111;
            }
            AnonymousClass1G5.A01(A0l, r111);
        }
        return r4;
    }
}
