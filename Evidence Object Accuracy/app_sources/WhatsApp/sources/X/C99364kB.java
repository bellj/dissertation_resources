package X;

import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99364kB implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        IBinder iBinder = null;
        IntentFilter[] intentFilterArr = null;
        String str = null;
        String str2 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                iBinder = C95664e9.A06(parcel, readInt);
            } else if (c == 3) {
                intentFilterArr = (IntentFilter[]) C95664e9.A0K(parcel, IntentFilter.CREATOR, readInt);
            } else if (c != 4) {
                str2 = C95664e9.A09(parcel, str2, c, 5, readInt);
            } else {
                str = C95664e9.A08(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78553p6(iBinder, str, str2, intentFilterArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78553p6[i];
    }
}
