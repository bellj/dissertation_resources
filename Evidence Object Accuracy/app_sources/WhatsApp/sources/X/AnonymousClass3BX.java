package X;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.3BX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BX {
    public int A00;
    public int A01;
    public C43161wW A02;
    public final String A03;
    public final String A04;
    public final ConcurrentMap A05;
    public volatile AtomicBoolean A06 = new AtomicBoolean();

    public AnonymousClass3BX(AnonymousClass5XB r2) {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        this.A05 = concurrentHashMap;
        concurrentHashMap.put(r2, r2);
        this.A03 = r2.getId();
        this.A04 = r2.AHT();
        this.A00 = r2.AE9();
        this.A01 = r2.AEB();
    }
}
