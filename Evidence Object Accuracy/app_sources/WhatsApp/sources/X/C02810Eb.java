package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Eb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02810Eb extends C02280As {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(19);
    public String A00;

    public C02810Eb(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readString();
    }

    public C02810Eb(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.AbsSavedState, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A00);
    }
}
