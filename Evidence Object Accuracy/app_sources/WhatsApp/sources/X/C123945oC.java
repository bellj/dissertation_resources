package X;

import android.view.View;
import com.whatsapp.payments.ui.widget.PaymentView;

/* renamed from: X.5oC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123945oC extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ PaymentView A00;

    public C123945oC(PaymentView paymentView) {
        this.A00 = paymentView;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        this.A00.A0z.A01(2);
    }
}
