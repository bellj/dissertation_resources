package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.whatsapp.util.Log;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0z1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22400z1 {
    public final C14830m7 A00;
    public final C14820m6 A01;
    public final C16370ot A02;
    public final AnonymousClass151 A03;
    public final C14850m9 A04;
    public final C14410lO A05;
    public final C252418q A06;
    public final AbstractC14440lR A07;
    public final AtomicBoolean A08 = new AtomicBoolean(false);

    public C22400z1(C14830m7 r3, C14820m6 r4, C16370ot r5, AnonymousClass151 r6, C14850m9 r7, C14410lO r8, C252418q r9, AbstractC14440lR r10) {
        this.A00 = r3;
        this.A04 = r7;
        this.A07 = r10;
        this.A05 = r8;
        this.A02 = r5;
        this.A01 = r4;
        this.A06 = r9;
        this.A03 = r6;
    }

    public final void A00(LinkedList linkedList, Random random) {
        int size;
        C14430lQ A02;
        StringBuilder sb = new StringBuilder("ThumbnailAutoDownload/enqueueThumbnailAutodownloadsToRetry num remaining:");
        sb.append(linkedList.size());
        Log.i(sb.toString());
        int i = 0;
        while (!linkedList.isEmpty()) {
            C252418q r6 = this.A06;
            synchronized (r6) {
                size = ((AbstractC14550lc) r6).A01.size();
            }
            if (i >= 20 || size - i > 5) {
                linkedList.size();
                this.A07.AbK(new RunnableBRunnable0Shape1S0300000_I0_1(this, linkedList, random, 9), "ThumbnailAutoDownload/enqueue", (long) (random.nextInt(5000) + 5000));
                return;
            }
            AbstractC15340mz A00 = this.A02.A00(((Number) linkedList.pop()).longValue());
            if (A00 != null && !A00.A0z.A02 && A01(A00)) {
                if (A00 instanceof AbstractC16130oV) {
                    C16150oX r0 = ((AbstractC16130oV) A00).A02;
                    AnonymousClass009.A05(r0);
                    String str = r0.A0I;
                    if (!(str == null || (A02 = this.A05.A0K.A02(str, 3)) == null)) {
                        long A002 = this.A00.A00();
                        long j = A02.A06;
                        long j2 = A002 - j;
                        if (j2 < 86400000 && j2 < (j - A02.A0C) * 2) {
                        }
                    }
                }
                r6.A06(A00, 1);
                i++;
            }
        }
        this.A08.set(false);
    }

    public final boolean A01(AbstractC15340mz r5) {
        if (r5.A0T == null) {
            return false;
        }
        C14850m9 r2 = this.A04;
        if ((!r2.A07(250) || !(r5 instanceof C16440p1)) && !C38451o7.A00(this.A00, this.A01, r2, r5) && !(r5 instanceof C28861Ph)) {
            return false;
        }
        return true;
    }
}
