package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;

/* renamed from: X.5xN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129225xN {
    public final Context A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final AnonymousClass102 A03;
    public final C17220qS A04;
    public final C18650sn A05;
    public final AbstractC136206Lq A06;
    public final String A07;

    public C129225xN(Context context, AbstractC15710nm r2, C14900mE r3, AnonymousClass102 r4, C17220qS r5, C18650sn r6, AbstractC136206Lq r7, String str) {
        this.A00 = context;
        this.A07 = str;
        this.A02 = r3;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = r7;
    }

    public void A00() {
        C17220qS r7 = this.A04;
        String A01 = r7.A01();
        C126265sb r4 = new C126265sb(new AnonymousClass3CS(A01), this.A07);
        r7.A09(new IDxRCallbackShape0S0200000_3_I1(this.A00, this.A02, this.A05, r4, this, 3), r4.A00, A01, 204, C26061Bw.A0L);
    }
}
