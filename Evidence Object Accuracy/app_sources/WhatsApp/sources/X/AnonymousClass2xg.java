package X;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.whatsapp.mentions.MentionPickerView;

/* renamed from: X.2xg  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2xg extends AbstractC83763xr {
    public int A00;
    public ValueAnimator A01;
    public View A02;
    public View A03;
    public AnonymousClass01d A04;
    public C14850m9 A05;

    public double getAvailableScreenHeightPercentage() {
        return 0.5d;
    }

    public abstract View getContentView();

    public AnonymousClass2xg(Context context) {
        super(context);
    }

    public AnonymousClass2xg(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public final void A03() {
        if (this.A02 != null) {
            MentionPickerView mentionPickerView = (MentionPickerView) this;
            ViewGroup.LayoutParams layoutParams = mentionPickerView.A00.getLayoutParams();
            layoutParams.width = this.A02.getWidth();
            mentionPickerView.A00.setLayoutParams(layoutParams);
        }
    }

    public void A04(int i) {
        int i2;
        if (i != this.A00) {
            this.A00 = i;
            ValueAnimator valueAnimator = this.A01;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            if (C12960it.A1T(getVisibility())) {
                i2 = getHeight();
            } else {
                i2 = 0;
            }
            int[] A07 = C13000ix.A07();
            A07[0] = i2;
            A07[1] = i;
            ValueAnimator ofInt = ValueAnimator.ofInt(A07);
            this.A01 = ofInt;
            ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.3Jh
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                    AnonymousClass2xg r2 = AnonymousClass2xg.this;
                    int A05 = C12960it.A05(valueAnimator2.getAnimatedValue());
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) r2.getLayoutParams();
                    layoutParams.height = A05;
                    r2.setLayoutParams(layoutParams);
                }
            });
            this.A01.addListener(new AnonymousClass2YQ(this, i));
            this.A01.setDuration(250L);
            this.A01.start();
        }
    }

    public void A05(int i, int i2) {
        int i3;
        if (i != 0) {
            if (this.A05.A07(571)) {
                i3 = i2 * Math.min(2, i);
            } else {
                int actionBarSize = getActionBarSize();
                Display defaultDisplay = this.A04.A0O().getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                int i4 = point.y;
                int[] iArr = new int[2];
                getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                View view = this.A03;
                if (view == null) {
                    view = (View) getParent().getParent();
                    this.A03 = view;
                }
                view.getLocationOnScreen(iArr2);
                double d = (double) i2;
                int min = Math.min((int) (((((double) i4) * 0.5d) - ((double) actionBarSize)) - ((double) ((int) (1.25d * d)))), ((iArr[1] + getHeight()) - iArr2[1]) - ((int) (0.6d * d))) / i2;
                i3 = i > min ? (min * i2) + ((int) (d * 0.5d)) : i * i2;
            }
            if (i3 != 0) {
                A04(i3);
                return;
            }
        }
        if (getVisibility() == 0) {
            A04(0);
        }
    }

    private int getActionBarSize() {
        TypedArray obtainStyledAttributes = C12980iv.A0I(this).obtainStyledAttributes(new int[]{16843499});
        try {
            return (int) obtainStyledAttributes.getDimension(0, 0.0f);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @Override // android.view.View
    public void onConfigurationChanged(Configuration configuration) {
        if (this.A02 != null && getVisibility() == 0) {
            C12980iv.A1H(getViewTreeObserver(), this, 3);
        }
    }

    public void setAnchorWidthView(View view) {
        this.A02 = view;
        A03();
    }

    public void setConstraintParentView(View view) {
        this.A03 = view;
    }
}
