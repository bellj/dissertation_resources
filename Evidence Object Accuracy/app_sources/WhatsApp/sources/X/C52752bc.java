package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.group.NewGroup;
import java.util.List;

/* renamed from: X.2bc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52752bc extends ArrayAdapter {
    public final LayoutInflater A00;
    public final /* synthetic */ NewGroup A01;

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public long getItemId(int i) {
        return (long) (i << 10);
    }

    @Override // android.widget.Adapter, android.widget.BaseAdapter
    public boolean hasStableIds() {
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52752bc(Context context, NewGroup newGroup, List list) {
        super(context, (int) R.layout.selected_contact, list);
        this.A01 = newGroup;
        this.A00 = LayoutInflater.from(newGroup);
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public int getCount() {
        return this.A01.A0W.size();
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A01.A0W.get(i);
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        NewGroup newGroup = this.A01;
        C15370n3 r2 = (C15370n3) newGroup.A0W.get(i);
        AnonymousClass009.A05(r2);
        if (view == null) {
            view = C12960it.A0F(this.A00, viewGroup, R.layout.selected_contact);
        }
        C12960it.A0I(view, R.id.contact_name).setText(newGroup.A09.A04(r2));
        AnonymousClass028.A0D(view, R.id.close).setVisibility(8);
        ImageView A0K = C12970iu.A0K(view, R.id.contact_row_photo);
        newGroup.A0C.A06(A0K, r2);
        AnonymousClass028.A0a(A0K, 2);
        AnonymousClass23N.A03(view, R.string.new_group_contact_content_description);
        return view;
    }
}
