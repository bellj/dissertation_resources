package X;

import android.graphics.Bitmap;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3VA  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3VA implements AnonymousClass2E5 {
    public final /* synthetic */ C14310lE A00;
    public final /* synthetic */ UserJid A01;

    public /* synthetic */ AnonymousClass3VA(C14310lE r1, UserJid userJid) {
        this.A00 = r1;
        this.A01 = userJid;
    }

    @Override // X.AnonymousClass2E5
    public final void AS6(Bitmap bitmap, C68203Um r7, boolean z) {
        C14310lE r4 = this.A00;
        UserJid userJid = this.A01;
        if (r4.A01 != null) {
            AbstractC14440lR r2 = r4.A0S;
            r2.Ab6(new RunnableBRunnable0Shape3S0200000_I0_3(r4, 13, bitmap));
            r2.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(r4, 15, userJid));
        }
    }
}
