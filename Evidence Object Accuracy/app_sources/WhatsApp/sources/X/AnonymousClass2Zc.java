package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.2Zc  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Zc extends Drawable implements Drawable.Callback {
    public int A00;
    public int A01;
    public final Drawable A02;
    public final Drawable A03;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public AnonymousClass2Zc(Context context) {
        Drawable A0C = C12970iu.A0C(context, R.drawable.carousel_scrollbar_track);
        this.A03 = A0C;
        A0C.setCallback(this);
        Drawable A0C2 = C12970iu.A0C(context, R.drawable.carousel_scrollbar_thumb);
        this.A02 = A0C2;
        A0C2.setCallback(this);
    }

    public final void A00() {
        Rect bounds = getBounds();
        this.A03.setBounds(bounds);
        Drawable drawable = this.A02;
        int i = bounds.left + this.A01;
        drawable.setBounds(i, bounds.top, i + this.A00, bounds.bottom);
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        this.A03.draw(canvas);
        this.A02.draw(canvas);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @Override // android.graphics.drawable.Drawable
    public void onBoundsChange(Rect rect) {
        A00();
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.A03.setAlpha(i);
        this.A02.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.A03.setColorFilter(colorFilter);
        this.A02.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable.Callback
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
