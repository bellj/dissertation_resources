package X;

import android.graphics.Bitmap;
import android.net.Uri;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.3Wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68853Wz implements AnonymousClass23D {
    public final /* synthetic */ ImageComposerFragment A00;

    public C68853Wz(ImageComposerFragment imageComposerFragment) {
        this.A00 = imageComposerFragment;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(((MediaComposerFragment) this.A00).A00, A0h);
        return C12960it.A0d("-original", A0h);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        try {
            ImageComposerFragment imageComposerFragment = this.A00;
            Uri.Builder buildUpon = Uri.fromFile(ActivityC13790kL.A0T(imageComposerFragment).A05()).buildUpon();
            if (((MediaComposerFragment) imageComposerFragment).A00.getQueryParameter("flip-h") != null) {
                buildUpon.appendQueryParameter("flip-h", ((MediaComposerFragment) imageComposerFragment).A00.getQueryParameter("flip-h"));
            }
            Uri build = buildUpon.build();
            C15450nH r2 = ((MediaComposerFragment) imageComposerFragment).A04;
            C22190yg r1 = ((MediaComposerFragment) imageComposerFragment).A0L;
            int A02 = r2.A02(AbstractC15460nI.A1T);
            return r1.A07(build, A02, A02);
        } catch (C39351pj | IOException e) {
            Log.e("ImageComposerFragment/loadOriginalBitmap", e);
            return null;
        }
    }
}
