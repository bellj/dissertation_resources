package X;

import java.util.Set;

/* renamed from: X.3o9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC77963o9 extends AbstractC95064d1 implements AbstractC72443eb, AbstractC115565Sb {
    public final AnonymousClass3BW A00;
    public final Set A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AbstractC77963o9(android.content.Context r13, android.os.Looper r14, X.AbstractC14990mN r15, X.AbstractC15010mP r16, X.AnonymousClass3BW r17, int r18) {
        /*
            r12 = this;
            r4 = r13
            X.4cl r9 = X.C94924cl.A00(r13)
            X.29i r6 = X.C471729i.A00
            X.C13020j0.A01(r15)
            r1 = r16
            X.C13020j0.A01(r1)
            X.4yx r7 = new X.4yx
            r7.<init>(r15)
            X.4yy r8 = new X.4yy
            r8.<init>(r1)
            r0 = r17
            java.lang.String r10 = r0.A03
            r3 = r12
            r5 = r14
            r11 = r18
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
            r12.A00 = r0
            java.util.Set r2 = r0.A06
            java.util.Iterator r1 = r2.iterator()
        L_0x002c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0043
            java.lang.Object r0 = r1.next()
            boolean r0 = r2.contains(r0)
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = "Expanding scopes is not permitted, use implied scopes instead"
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x0043:
            r12.A01 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC77963o9.<init>(android.content.Context, android.os.Looper, X.0mN, X.0mP, X.3BW, int):void");
    }
}
