package X;

/* renamed from: X.0DQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DQ extends C07420Xx {
    public final Object A00 = new Object();

    public AnonymousClass0DQ(int i) {
        super(i);
    }

    @Override // X.C07420Xx, X.AbstractC12350hm
    public Object A5a() {
        Object A5a;
        synchronized (this.A00) {
            A5a = super.A5a();
        }
        return A5a;
    }

    @Override // X.C07420Xx, X.AbstractC12350hm
    public boolean Aa6(Object obj) {
        boolean Aa6;
        synchronized (this.A00) {
            Aa6 = super.Aa6(obj);
        }
        return Aa6;
    }
}
