package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.google.android.material.chip.Chip;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54322gX extends AnonymousClass02M {
    public List A00;
    public final AnonymousClass4J4 A01;

    public C54322gX(AnonymousClass4J4 r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r2, int i) {
        AbstractC75733kK r22 = (AbstractC75733kK) r2;
        r22.A08();
        r22.A09((AnonymousClass3E9) this.A00.get(i));
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater A0E;
        int i2;
        if (i == 1) {
            A0E = C12960it.A0E(viewGroup);
            i2 = R.layout.search_history_icon;
        } else if (i == 2) {
            return new C60082vr((Chip) C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.search_history_chip), AnonymousClass2FL.A00(this.A01.A00.A01));
        } else if (i == 3) {
            A0E = C12960it.A0E(viewGroup);
            i2 = R.layout.search_history_see_all;
        } else {
            throw C12960it.A0U(C12960it.A0W(i, "SearchHistoryItemAdapter/onCreateViewHolder unhandled view type: "));
        }
        return new C848940f(C12960it.A0F(A0E, viewGroup, i2));
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return ((AnonymousClass3E9) this.A00.get(i)).A02;
    }
}
