package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122415lQ extends AbstractC118825cR {
    public View A00;
    public TextView A01;
    public TextView A02;

    public C122415lQ(View view) {
        super(view);
        this.A02 = C12960it.A0I(view, R.id.date);
        this.A01 = C12960it.A0I(view, R.id.amount);
        this.A00 = AnonymousClass028.A0D(view, R.id.divider);
    }
}
