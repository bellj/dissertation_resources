package X;

import android.view.View;
import android.widget.AdapterView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.status.StatusesFragment;

/* renamed from: X.36R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36R extends AbstractC102384p3 {
    public final /* synthetic */ StatusesFragment A00;

    public AnonymousClass36R(StatusesFragment statusesFragment) {
        this.A00 = statusesFragment;
    }

    @Override // X.AbstractC102384p3
    public void A00(AdapterView adapterView, View view, int i, long j) {
        C55172hu r4 = (C55172hu) view.getTag();
        if (r4 == null) {
            return;
        }
        if (r4.A01 == C29831Uv.A00 && r4.A00 == 0) {
            StatusesFragment statusesFragment = this.A00;
            statusesFragment.A07.A08();
            statusesFragment.A1F();
            return;
        }
        StatusesFragment statusesFragment2 = this.A00;
        statusesFragment2.A0v(C14960mK.A0F(statusesFragment2.A0p(), r4.A01, Boolean.FALSE));
        AnonymousClass1BD r1 = statusesFragment2.A0k;
        UserJid userJid = r4.A01;
        C33701ew r0 = statusesFragment2.A0q;
        r1.A04(userJid, null, null, statusesFragment2.A1A(), r0.A02, r0.A03, r0.A01, r0.A05);
    }
}
