package X;

import android.hardware.Camera;
import java.util.concurrent.Callable;

/* renamed from: X.6KV  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KV implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass661 A01;

    public AnonymousClass6KV(AnonymousClass661 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        int i;
        AnonymousClass661 r4 = this.A01;
        if (r4.isConnected()) {
            r4.A01 = this.A00;
            if (r4.A0Z == null) {
                r4.A0Y.setDisplayOrientation(r4.A04(r4.A01));
            } else {
                boolean z = AnonymousClass66P.A0C;
                Camera camera = r4.A0Y;
                if (z) {
                    i = 0;
                } else {
                    i = r4.A01;
                }
                camera.setDisplayOrientation(r4.A04(i));
                r4.A0Z.A02(C117325Zm.A00(r4.A01));
            }
            AbstractC130685zo A05 = r4.A05();
            C129845yO r0 = (C129845yO) A05.A03(AbstractC130685zo.A0m);
            r4.A0A(r0.A02, r0.A01);
            return new C127255uC(new C127245uB(r4.ABG(), A05, r4.A00));
        }
        throw new C136076Kx("Can not update preview display rotation");
    }
}
