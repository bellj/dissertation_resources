package X;

/* renamed from: X.6J0  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6J0 implements Runnable {
    public final /* synthetic */ C129455xk A00;
    public final /* synthetic */ AnonymousClass661 A01;
    public final /* synthetic */ AnonymousClass60J A02;

    public AnonymousClass6J0(C129455xk r1, AnonymousClass661 r2, AnonymousClass60J r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass60J r5 = this.A02;
        C125525rO r4 = AnonymousClass60J.A0Z;
        byte[] bArr = (byte[]) r5.A01(r4);
        if (bArr == null || bArr.length == 0) {
            this.A00.A00(C12990iw.A0m("Photo taking returned no jpeg data!"));
            return;
        }
        C129455xk r3 = this.A00;
        AnonymousClass643.A00(r3.A00, new Object[]{r3.A01, r5.A01(r4), r5}, 6);
        AnonymousClass60C A00 = AnonymousClass60C.A00();
        A00.A02(0, A00.A03);
    }
}
