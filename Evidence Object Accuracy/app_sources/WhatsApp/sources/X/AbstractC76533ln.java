package X;

import android.media.MediaCodec;
import android.media.MediaCryptoException;
import android.media.MediaFormat;
import android.os.Bundle;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: X.3ln  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC76533ln extends AbstractC106444vi {
    public static final byte[] A15 = {0, 0, 1, 103, 66, -64, 11, -38, 37, -112, 0, 0, 1, 104, -50, 15, 19, 32, 0, 0, 1, 101, -120, -124, 13, -50, 113, 24, -96, 0, 47, -65, 28, 49, -61, 39, 93, 120};
    public float A00;
    public float A01;
    public float A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public long A0A;
    public long A0B;
    public long A0C;
    public long A0D;
    public long A0E;
    public long A0F;
    public MediaFormat A0G;
    public AnonymousClass3A1 A0H;
    public C100614mC A0I;
    public C100614mC A0J;
    public C100614mC A0K;
    public AnonymousClass4U3 A0L;
    public AnonymousClass5Q2 A0M;
    public AnonymousClass5Q2 A0N;
    public AnonymousClass4P3 A0O;
    public AnonymousClass5XG A0P;
    public C95494dp A0Q;
    public AnonymousClass4CL A0R;
    public ByteBuffer A0S;
    public ArrayDeque A0T;
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;
    public boolean A0k;
    public boolean A0l;
    public boolean A0m;
    public boolean A0n;
    public boolean A0o;
    public boolean A0p;
    public boolean A0q;
    public boolean A0r;
    public final float A0s;
    public final MediaCodec.BufferInfo A0t;
    public final C76763mA A0u = new C76763mA(0);
    public final C76763mA A0v = new C76763mA(2);
    public final C76763mA A0w = new C76763mA(0);
    public final C76753m9 A0x;
    public final AbstractC116825Xa A0y;
    public final AbstractC117015Xu A0z;
    public final AnonymousClass4R9 A10;
    public final ArrayList A11;
    public final long[] A12;
    public final long[] A13;
    public final long[] A14;

    public void A0Q(C76763mA r1) {
    }

    public AbstractC76533ln(AbstractC116825Xa r7, AbstractC117015Xu r8, float f, int i) {
        super(i);
        this.A0y = r7;
        this.A0z = r8;
        this.A0s = f;
        C76753m9 r4 = new C76753m9();
        this.A0x = r4;
        this.A10 = new AnonymousClass4R9();
        this.A11 = C12960it.A0l();
        this.A0t = new MediaCodec.BufferInfo();
        this.A01 = 1.0f;
        this.A02 = 1.0f;
        this.A0F = -9223372036854775807L;
        this.A13 = new long[10];
        this.A12 = new long[10];
        this.A14 = new long[10];
        this.A0E = -9223372036854775807L;
        this.A0D = -9223372036854775807L;
        r4.A01(0);
        ((C76763mA) r4).A01.order(ByteOrder.nativeOrder());
        A0H();
    }

    private void A00() {
        int i = this.A04;
        if (i == 1) {
            A0K();
        } else if (i == 2) {
            A0K();
            A0M();
            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
        } else if (i != 3) {
            this.A0o = true;
            A0F();
        } else {
            A0E();
            A0L();
        }
    }

    public static boolean A01(IllegalStateException illegalStateException) {
        return illegalStateException instanceof MediaCodec.CodecException;
    }

    @Override // X.AbstractC106444vi
    public void A07() {
        try {
            A0I();
            A0E();
        } finally {
            this.A0N = null;
        }
    }

    @Override // X.AbstractC106444vi
    public void A08() {
        this.A0J = null;
        this.A0E = -9223372036854775807L;
        this.A0D = -9223372036854775807L;
        this.A09 = 0;
        if (this.A0N == null && this.A0M == null) {
            A0R();
        } else {
            A07();
        }
    }

    @Override // X.AbstractC106444vi
    public void A09(long j, boolean z) {
        int i;
        this.A0l = false;
        this.A0o = false;
        this.A0p = false;
        if (this.A0V) {
            this.A0x.clear();
            this.A0v.clear();
            this.A0W = false;
        } else if (A0R()) {
            A0L();
        }
        AnonymousClass4R9 r2 = this.A10;
        synchronized (r2) {
            i = r2.A01;
        }
        if (i > 0) {
            this.A0r = true;
        }
        synchronized (r2) {
            r2.A00 = 0;
            r2.A01 = 0;
            Arrays.fill(r2.A03, (Object) null);
        }
        int i2 = this.A09;
        if (i2 != 0) {
            this.A0D = this.A12[i2 - 1];
            this.A0E = this.A13[i2 - 1];
            this.A09 = 0;
        }
    }

    @Override // X.AbstractC106444vi
    public void A0A(boolean z, boolean z2) {
        this.A0L = new AnonymousClass4U3();
    }

    public float A0B(C100614mC[] r7, float f) {
        if (!(this instanceof C76933mT)) {
            int i = -1;
            for (C100614mC r0 : r7) {
                int i2 = r0.A0F;
                if (i2 != -1) {
                    i = Math.max(i, i2);
                }
            }
            if (i == -1) {
                return -1.0f;
            }
            return f * ((float) i);
        }
        float f2 = -1.0f;
        for (C100614mC r02 : r7) {
            float f3 = r02.A01;
            if (f3 != -1.0f) {
                f2 = Math.max(f2, f3);
            }
        }
        if (f2 != -1.0f) {
            return f2 * f;
        }
        return -1.0f;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        if (X.AnonymousClass3JZ.A01 >= 23) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x006c, code lost:
        if (A0T() == false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00bb, code lost:
        if (r8 != false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00c1, code lost:
        if (A0S() == false) goto L_0x00c8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0119  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass4XN A0C(X.C89864Lr r16) {
        /*
        // Method dump skipped, instructions count: 295
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76533ln.A0C(X.4Lr):X.4XN");
    }

    public List A0D(C100614mC r6, AbstractC117015Xu r7) {
        Object obj;
        if (this instanceof C76933mT) {
            return C76933mT.A04(r6, r7, false, ((C76933mT) this).A0U);
        }
        C76943mU r0 = (C76943mU) this;
        String str = r6.A0T;
        if (str == null) {
            return Collections.emptyList();
        }
        if (((C106534vr) r0.A0A).AD7(r6) != 0) {
            List A03 = C95604e3.A03("audio/raw", false, false);
            if (!A03.isEmpty() && (obj = A03.get(0)) != null) {
                return Collections.singletonList(obj);
            }
        }
        ArrayList A0x = C12980iv.A0x(r7.ACR(str, false, false));
        Collections.sort(A0x, new AnonymousClass5CY(new C107384xF(r6)));
        if ("audio/eac3-joc".equals(str)) {
            A0x = C12980iv.A0x(A0x);
            A0x.addAll(r7.ACR("audio/eac3", false, false));
        }
        return Collections.unmodifiableList(A0x);
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [X.5XG, X.5Q2] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0E() {
        /*
            r6 = this;
            r5 = 0
            X.5XG r0 = r6.A0P     // Catch: all -> 0x004a
            if (r0 == 0) goto L_0x0042
            X.4x8 r0 = (X.C107314x8) r0     // Catch: all -> 0x004a
            r0.A00 = r5     // Catch: all -> 0x004a
            r0.A01 = r5     // Catch: all -> 0x004a
            android.media.MediaCodec r0 = r0.A02     // Catch: all -> 0x004a
            r0.release()     // Catch: all -> 0x004a
            X.4U3 r1 = r6.A0L     // Catch: all -> 0x004a
            int r0 = r1.A01     // Catch: all -> 0x004a
            int r0 = r0 + 1
            r1.A01 = r0     // Catch: all -> 0x004a
            X.4dp r0 = r6.A0Q     // Catch: all -> 0x004a
            java.lang.String r4 = r0.A03     // Catch: all -> 0x004a
            r1 = r6
            boolean r0 = r6 instanceof X.C76933mT     // Catch: all -> 0x004a
            if (r0 != 0) goto L_0x0033
            X.3mU r1 = (X.C76943mU) r1     // Catch: all -> 0x004a
            X.4Wb r1 = r1.A09     // Catch: all -> 0x004a
            android.os.Handler r3 = r1.A00     // Catch: all -> 0x004a
            if (r3 == 0) goto L_0x0042
            r0 = 0
            com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1 r2 = new com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1     // Catch: all -> 0x004a
            r2.<init>(r0, r4, r1)     // Catch: all -> 0x004a
        L_0x002f:
            r3.post(r2)     // Catch: all -> 0x004a
            goto L_0x0042
        L_0x0033:
            X.3mT r1 = (X.C76933mT) r1     // Catch: all -> 0x004a
            X.4ME r1 = r1.A0Z     // Catch: all -> 0x004a
            android.os.Handler r3 = r1.A00     // Catch: all -> 0x004a
            if (r3 == 0) goto L_0x0042
            r0 = 1
            com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1 r2 = new com.facebook.redex.RunnableBRunnable0Shape1S1100000_I1     // Catch: all -> 0x004a
            r2.<init>(r0, r4, r1)     // Catch: all -> 0x004a
            goto L_0x002f
        L_0x0042:
            r6.A0P = r5
            r6.A0M = r5
            r6.A0H()
            return
        L_0x004a:
            r0 = move-exception
            r6.A0P = r5
            r6.A0M = r5
            r6.A0H()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76533ln.A0E():void");
    }

    public void A0F() {
        if (this instanceof C76943mU) {
            C76943mU r3 = (C76943mU) this;
            try {
                C106534vr r1 = (C106534vr) r3.A0A;
                if (!r1.A0R && r1.A0D != null && r1.A0F()) {
                    r1.A09();
                    r1.A0R = true;
                }
            } catch (AnonymousClass4C4 e) {
                throw r3.A01(e.format, e, e.isRecoverable);
            }
        }
    }

    public void A0G() {
        this.A07 = -1;
        this.A0u.A01 = null;
        this.A08 = -1;
        this.A0S = null;
        this.A0A = -9223372036854775807L;
        this.A0j = false;
        this.A0i = false;
        this.A0Y = false;
        this.A0q = false;
        this.A0m = false;
        this.A0n = false;
        this.A11.clear();
        this.A0B = -9223372036854775807L;
        this.A0C = -9223372036854775807L;
        AnonymousClass4P3 r2 = this.A0O;
        if (r2 != null) {
            r2.A01 = 0;
            r2.A00 = 0;
            r2.A02 = false;
        }
        this.A05 = 0;
        this.A04 = 0;
        this.A06 = this.A0k ? 1 : 0;
    }

    public void A0H() {
        A0G();
        this.A0H = null;
        this.A0O = null;
        this.A0T = null;
        this.A0Q = null;
        this.A0I = null;
        this.A0G = null;
        this.A0h = false;
        this.A0X = false;
        this.A00 = -1.0f;
        this.A03 = 0;
        this.A0Z = false;
        this.A0e = false;
        this.A0g = false;
        this.A0b = false;
        this.A0c = false;
        this.A0a = false;
        this.A0f = false;
        this.A0d = false;
        this.A0k = false;
        this.A06 = 0;
    }

    public final void A0I() {
        this.A0U = false;
        this.A0x.clear();
        this.A0v.clear();
        this.A0W = false;
        this.A0V = false;
    }

    public final void A0J() {
        if (this.A0i) {
            this.A05 = 1;
            this.A04 = 3;
            return;
        }
        A0E();
        A0L();
    }

    public final void A0K() {
        try {
            ((C107314x8) this.A0P).A02.flush();
        } finally {
            A0G();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01e1, code lost:
        if ("AXON 7 mini".equals(r3) == false) goto L_0x01f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0264, code lost:
        if (r14.A09 == -1) goto L_0x0266;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x047a, code lost:
        if ("OMX.MTK.VIDEO.DECODER.AVC".equals(r6) != false) goto L_0x047d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x04c5, code lost:
        if ("c2.android.aac.decoder".equals(r6) == false) goto L_0x04c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x04ea, code lost:
        if ("stvm8".equals(r3) == false) goto L_0x04fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x04fa, code lost:
        if ("OMX.amlogic.avc.decoder.awesome.secure".equals(r6) == false) goto L_0x04fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x050c, code lost:
        if ("OMX.google.aac.decoder".equals(r6) == false) goto L_0x050e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x01ed A[Catch: Exception -> 0x0669, Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #2 {Exception -> 0x0669, blocks: (B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:365:0x0664, B:366:0x0668), top: B:397:0x0105 }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x03be A[Catch: Exception -> 0x0669, Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #2 {Exception -> 0x0669, blocks: (B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:365:0x0664, B:366:0x0668), top: B:397:0x0105 }] */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x046b A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x0483  */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x04be A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x04ce A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x04da A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x0505 A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x0515 A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x0565 A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x057a A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:325:0x05c1  */
    /* JADX WARNING: Removed duplicated region for block: B:335:0x05db A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:338:0x05e7 A[Catch: Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #3 {Exception -> 0x0679, blocks: (B:51:0x00cd, B:54:0x00d8, B:55:0x00e0, B:58:0x00e8, B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:192:0x03f2, B:194:0x0400, B:196:0x0408, B:198:0x0412, B:200:0x041a, B:202:0x0422, B:207:0x0430, B:209:0x0438, B:211:0x0440, B:213:0x044a, B:215:0x0452, B:217:0x045a, B:221:0x0465, B:223:0x046b, B:225:0x0473, B:228:0x047d, B:231:0x0485, B:233:0x048d, B:238:0x049a, B:240:0x04a4, B:242:0x04ac, B:247:0x04b8, B:249:0x04be, B:252:0x04c8, B:254:0x04ce, B:258:0x04da, B:260:0x04e4, B:262:0x04ec, B:264:0x04f4, B:268:0x04ff, B:270:0x0505, B:273:0x050f, B:275:0x0515, B:277:0x051d, B:279:0x0527, B:281:0x0531, B:283:0x0539, B:285:0x0541, B:287:0x0549, B:289:0x0551, B:293:0x055c, B:295:0x0565, B:297:0x0569, B:299:0x0571, B:303:0x057a, B:307:0x0586, B:311:0x0592, B:313:0x059a, B:315:0x05a2, B:317:0x05ac, B:319:0x05b6, B:327:0x05c4, B:333:0x05d1, B:335:0x05db, B:336:0x05e2, B:338:0x05e7, B:339:0x05f0, B:341:0x05fd, B:343:0x0605, B:344:0x0613, B:346:0x061b, B:347:0x062d, B:349:0x063a, B:351:0x0644, B:353:0x0648, B:355:0x064c, B:356:0x064e, B:358:0x0651, B:361:0x065a, B:364:0x0660, B:365:0x0664, B:366:0x0668, B:369:0x066d, B:370:0x0678), top: B:399:0x00cd, outer: #4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:408:0x0613 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:409:0x05fd A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x019b A[Catch: Exception -> 0x0669, Exception -> 0x0679, 4CL -> 0x06f1, LOOP:2: B:84:0x0195->B:86:0x019b, LOOP_END, TryCatch #2 {Exception -> 0x0669, blocks: (B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:365:0x0664, B:366:0x0668), top: B:397:0x0105 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01b4 A[Catch: Exception -> 0x0669, Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #2 {Exception -> 0x0669, blocks: (B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:365:0x0664, B:366:0x0668), top: B:397:0x0105 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01bb A[Catch: Exception -> 0x0669, Exception -> 0x0679, 4CL -> 0x06f1, TryCatch #2 {Exception -> 0x0669, blocks: (B:59:0x0105, B:61:0x0113, B:65:0x0126, B:67:0x0130, B:68:0x013a, B:69:0x013d, B:71:0x0145, B:73:0x014d, B:75:0x0157, B:77:0x0161, B:79:0x0169, B:83:0x0174, B:84:0x0195, B:86:0x019b, B:89:0x01b4, B:92:0x01bb, B:100:0x01d1, B:102:0x01db, B:104:0x01e3, B:106:0x01ed, B:107:0x01f4, B:111:0x0200, B:114:0x0225, B:116:0x022d, B:120:0x023f, B:122:0x0245, B:124:0x0249, B:125:0x0255, B:127:0x025d, B:129:0x0261, B:132:0x0267, B:133:0x027b, B:135:0x0280, B:138:0x02a9, B:139:0x02bc, B:141:0x02cf, B:142:0x02f9, B:144:0x0308, B:145:0x030c, B:147:0x031d, B:149:0x0323, B:151:0x0329, B:152:0x0335, B:153:0x0337, B:155:0x0342, B:158:0x0348, B:167:0x0361, B:172:0x037c, B:175:0x0387, B:177:0x038d, B:178:0x0397, B:180:0x03a7, B:181:0x03ac, B:183:0x03be, B:185:0x03c6, B:186:0x03ca, B:190:0x03df, B:191:0x03eb, B:365:0x0664, B:366:0x0668), top: B:397:0x0105 }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0L() {
        /*
        // Method dump skipped, instructions count: 1786
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76533ln.A0L():void");
    }

    public final void A0M() {
        try {
            throw C12980iv.A0n("sessionId");
        } catch (MediaCryptoException e) {
            throw A01(this.A0J, e, false);
        }
    }

    public void A0N(long j) {
        while (true) {
            int i = this.A09;
            if (i != 0) {
                long[] jArr = this.A14;
                if (j >= jArr[0]) {
                    long[] jArr2 = this.A13;
                    this.A0E = jArr2[0];
                    long[] jArr3 = this.A12;
                    this.A0D = jArr3[0];
                    int i2 = i - 1;
                    this.A09 = i2;
                    System.arraycopy(jArr2, 1, jArr2, 0, i2);
                    System.arraycopy(jArr3, 1, jArr3, 0, this.A09);
                    System.arraycopy(jArr, 1, jArr, 0, this.A09);
                    if (!(this instanceof C76933mT)) {
                        ((C106534vr) ((C76943mU) this).A0A).A0V = true;
                    } else {
                        ((C76933mT) this).A0X();
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r6v10 */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0O(long r10) {
        /*
            r9 = this;
            X.4R9 r5 = r9.A10
            monitor-enter(r5)
            r6 = 0
        L_0x0004:
            int r8 = r5.A01     // Catch: all -> 0x006d
            if (r8 <= 0) goto L_0x0027
            long[] r0 = r5.A02     // Catch: all -> 0x006d
            int r7 = r5.A00     // Catch: all -> 0x006d
            r0 = r0[r7]     // Catch: all -> 0x006d
            long r3 = r10 - r0
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 < 0) goto L_0x0027
            r2 = 1
            java.lang.Object[] r1 = r5.A03     // Catch: all -> 0x006d
            r6 = r1[r7]     // Catch: all -> 0x006d
            r0 = 0
            r1[r7] = r0     // Catch: all -> 0x006d
            int r7 = r7 + r2
            int r0 = r1.length     // Catch: all -> 0x006d
            int r7 = r7 % r0
            r5.A00 = r7     // Catch: all -> 0x006d
            int r8 = r8 - r2
            r5.A01 = r8     // Catch: all -> 0x006d
            goto L_0x0004
        L_0x0027:
            monitor-exit(r5)
            X.4mC r6 = (X.C100614mC) r6
            if (r6 != 0) goto L_0x0057
            boolean r0 = r9.A0h
            if (r0 == 0) goto L_0x0057
            monitor-enter(r5)
            int r4 = r5.A01     // Catch: all -> 0x0050
            if (r4 != 0) goto L_0x0036
            goto L_0x0053
        L_0x0036:
            r3 = 1
            boolean r0 = X.C12960it.A1U(r4)
            X.C95314dV.A04(r0)     // Catch: all -> 0x0050
            java.lang.Object[] r2 = r5.A03     // Catch: all -> 0x0050
            int r1 = r5.A00     // Catch: all -> 0x0050
            r6 = r2[r1]     // Catch: all -> 0x0050
            r0 = 0
            r2[r1] = r0     // Catch: all -> 0x0050
            int r1 = r1 + r3
            int r0 = r2.length     // Catch: all -> 0x0050
            int r1 = r1 % r0
            r5.A00 = r1     // Catch: all -> 0x0050
            int r4 = r4 - r3
            r5.A01 = r4     // Catch: all -> 0x0050
            goto L_0x0054
        L_0x0050:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0053:
            r6 = 0
        L_0x0054:
            monitor-exit(r5)
            X.4mC r6 = (X.C100614mC) r6
        L_0x0057:
            r1 = 0
            if (r6 == 0) goto L_0x0064
            r9.A0K = r6
        L_0x005c:
            android.media.MediaFormat r0 = r9.A0G
            r9.A0P(r0, r6)
            r9.A0h = r1
        L_0x0063:
            return
        L_0x0064:
            boolean r0 = r9.A0h
            if (r0 == 0) goto L_0x0063
            X.4mC r6 = r9.A0K
            if (r6 == 0) goto L_0x0063
            goto L_0x005c
        L_0x006d:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76533ln.A0O(long):void");
    }

    public void A0P(MediaFormat mediaFormat, C100614mC r10) {
        boolean z;
        int integer;
        int integer2;
        int i;
        int i2;
        if (!(this instanceof C76933mT)) {
            C76943mU r3 = (C76943mU) this;
            C100614mC r0 = r3.A02;
            int[] iArr = null;
            if (r0 != null) {
                r10 = r0;
            } else if (r3.A0P != null) {
                if ("audio/raw".equals(r10.A0T)) {
                    i = r10.A0B;
                } else if (AnonymousClass3JZ.A01 >= 24 && mediaFormat.containsKey("pcm-encoding")) {
                    i = mediaFormat.getInteger("pcm-encoding");
                } else if (mediaFormat.containsKey("v-bits-per-sample")) {
                    i = AnonymousClass3JZ.A01(mediaFormat.getInteger("v-bits-per-sample"));
                } else {
                    i = 2;
                }
                C93844ap A00 = C93844ap.A00();
                A00.A0R = "audio/raw";
                A00.A09 = i;
                A00.A05 = r10.A07;
                A00.A06 = r10.A08;
                A00.A04 = mediaFormat.getInteger("channel-count");
                A00.A0D = mediaFormat.getInteger("sample-rate");
                C100614mC r5 = new C100614mC(A00);
                if (r3.A07 && r5.A06 == 6 && (i2 = r10.A06) < 6) {
                    iArr = new int[i2];
                    for (int i3 = 0; i3 < i2; i3++) {
                        iArr[i3] = i3;
                    }
                }
                r10 = r5;
            }
            try {
                r3.A0A.A7W(r10, iArr, 0);
            } catch (AnonymousClass4CG e) {
                throw r3.A01(e.format, e, false);
            }
        } else {
            C76933mT r52 = (C76933mT) this;
            AnonymousClass5XG r02 = ((AbstractC76533ln) r52).A0P;
            if (r02 != null) {
                ((C107314x8) r02).A02.setVideoScalingMode(r52.A0B);
            }
            if (r52.A0U) {
                r52.A06 = r10.A0I;
                integer2 = r10.A09;
            } else {
                if (!mediaFormat.containsKey("crop-right") || !mediaFormat.containsKey("crop-left") || !mediaFormat.containsKey("crop-bottom") || !mediaFormat.containsKey("crop-top")) {
                    z = false;
                    integer = mediaFormat.getInteger("width");
                } else {
                    z = true;
                    integer = (mediaFormat.getInteger("crop-right") - mediaFormat.getInteger("crop-left")) + 1;
                }
                r52.A06 = integer;
                if (z) {
                    integer2 = (mediaFormat.getInteger("crop-bottom") - mediaFormat.getInteger("crop-top")) + 1;
                } else {
                    integer2 = mediaFormat.getInteger("height");
                }
            }
            r52.A04 = integer2;
            float f = r10.A02;
            r52.A00 = f;
            if (AnonymousClass3JZ.A01 >= 21) {
                int i4 = r10.A0E;
                if (i4 == 90 || i4 == 270) {
                    int i5 = r52.A06;
                    r52.A06 = integer2;
                    r52.A04 = i5;
                    r52.A00 = 1.0f / f;
                }
            } else {
                r52.A05 = r10.A0E;
            }
            C94494bu r4 = r52.A0Y;
            r4.A00 = r10.A01;
            C92394Vs r32 = r4.A0E;
            r32.A03.A00();
            r32.A02.A00();
            r32.A04 = false;
            r32.A01 = -9223372036854775807L;
            r32.A00 = 0;
            r4.A05();
        }
    }

    public boolean A0R() {
        if (this.A0P != null) {
            if (this.A04 == 3 || this.A0e || ((this.A0g && !this.A0X) || (this.A0b && this.A0j))) {
                A0E();
                return true;
            }
            A0K();
        }
        return false;
    }

    public final boolean A0S() {
        if (this.A0i) {
            this.A05 = 1;
            if (this.A0e || this.A0b) {
                this.A04 = 3;
                return false;
            }
            this.A04 = 2;
            return true;
        }
        A0M();
        throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
    }

    public final boolean A0T() {
        if (AnonymousClass3JZ.A01 >= 23) {
            float A0B = A0B(super.A08, this.A02);
            float f = this.A00;
            if (f != A0B) {
                if (A0B == -1.0f) {
                    A0J();
                    return false;
                } else if (f != -1.0f || A0B > this.A0s) {
                    Bundle A0D = C12970iu.A0D();
                    A0D.putFloat("operating-rate", A0B);
                    this.A0P.AcS(A0D);
                    this.A00 = A0B;
                }
            }
        }
        return true;
    }

    public boolean A0U(C100614mC r6, AnonymousClass5XG r7, ByteBuffer byteBuffer, int i, int i2, int i3, long j, long j2, long j3, boolean z, boolean z2) {
        C76943mU r3 = (C76943mU) this;
        if (r3.A02 != null && (i2 & 2) != 0) {
            ((C107314x8) r7).A02.releaseOutputBuffer(i, false);
            return true;
        } else if (z) {
            if (r7 != null) {
                ((C107314x8) r7).A02.releaseOutputBuffer(i, false);
            }
            r3.A0L.A08 += i3;
            ((C106534vr) r3.A0A).A0V = true;
            return true;
        } else {
            try {
                if (!r3.A0A.AHx(byteBuffer, i3, j3)) {
                    return false;
                }
                if (r7 != null) {
                    ((C107314x8) r7).A02.releaseOutputBuffer(i, false);
                }
                r3.A0L.A06 += i3;
                return true;
            } catch (AnonymousClass4C3 e) {
                throw r3.A01(e.format, e, e.isRecoverable);
            } catch (AnonymousClass4C4 e2) {
                throw r3.A01(r6, e2, e2.isRecoverable);
            }
        }
    }

    public final boolean A0V(boolean z) {
        C89864Lr r4 = super.A0A;
        r4.A01 = null;
        r4.A00 = null;
        C76763mA r3 = this.A0w;
        r3.clear();
        int A00 = A00(r4, r3, z);
        if (A00 == -5) {
            A0C(r4);
            return true;
        } else if (A00 != -4 || !AnonymousClass4YO.A00(r3)) {
            return false;
        } else {
            this.A0l = true;
            A00();
            return false;
        }
    }

    @Override // X.AbstractC117055Yb
    public boolean AJN() {
        if (!(this instanceof C76943mU)) {
            return this.A0o;
        }
        C76943mU r1 = (C76943mU) this;
        if (!r1.A0o) {
            return false;
        }
        C106534vr r12 = (C106534vr) r1.A0A;
        if (r12.A0D != null) {
            return r12.A0R && !r12.AIH();
        }
        return true;
    }

    @Override // X.AbstractC117055Yb
    public boolean AJx() {
        boolean AJx;
        if (this.A0J == null) {
            return false;
        }
        if (AIJ()) {
            AJx = super.A06;
        } else {
            AJx = super.A05.AJx();
        }
        if (AJx || this.A08 >= 0) {
            return true;
        }
        long j = this.A0A;
        if (j == -9223372036854775807L || SystemClock.elapsedRealtime() >= j) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0261, code lost:
        A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0353, code lost:
        if (r37.A06 != 2) goto L_0x035a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x0355, code lost:
        r3.clear();
        r37.A06 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x035a, code lost:
        r37.A0l = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x035e, code lost:
        if (r37.A0i != false) goto L_0x05e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0360, code lost:
        A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:321:0x05e8, code lost:
        if (r37.A0d != false) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:322:0x05ea, code lost:
        r37.A0j = true;
        ((X.C107314x8) r37.A0P).A02.queueInputBuffer(r37.A07, 0, 0, 0, 4);
        r37.A07 = -1;
        r3.A01 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:327:0x0620, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:329:0x0627, code lost:
        throw A01(r37.A0J, r2, false);
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007d A[Catch: IllegalStateException -> 0x0635, DONT_GENERATE, TryCatch #6 {IllegalStateException -> 0x0635, blocks: (B:7:0x0012, B:9:0x0016, B:11:0x001a, B:13:0x001e, B:16:0x0026, B:18:0x002f, B:19:0x0034, B:21:0x0041, B:23:0x0069, B:24:0x0071, B:26:0x0075, B:27:0x0077, B:28:0x007a, B:29:0x007c, B:30:0x007d, B:32:0x0080, B:34:0x0084, B:35:0x008f, B:37:0x0093, B:39:0x0097, B:42:0x00a4, B:43:0x00b6, B:50:0x00c8, B:52:0x00ce, B:53:0x00d1, B:55:0x00d5, B:56:0x00de, B:58:0x00e7, B:59:0x00ea, B:60:0x00ed, B:62:0x00f1, B:63:0x00f4, B:65:0x00f8, B:67:0x00fc, B:70:0x0102, B:72:0x0106, B:73:0x010f, B:75:0x0113, B:77:0x0119, B:80:0x011e, B:81:0x0124, B:83:0x012f, B:85:0x0135, B:93:0x0148, B:94:0x014e, B:96:0x0159, B:98:0x015f, B:99:0x0166, B:101:0x016a, B:103:0x016e, B:106:0x0175, B:108:0x0179, B:109:0x0186, B:111:0x018a, B:113:0x018e, B:114:0x01bd, B:116:0x01c4, B:117:0x01c9, B:119:0x01f9, B:121:0x0206, B:124:0x020f, B:126:0x021f, B:128:0x0229, B:130:0x0231, B:131:0x0233, B:133:0x023e, B:136:0x0249, B:138:0x024d, B:139:0x0252, B:140:0x0257, B:142:0x025b, B:144:0x0261, B:145:0x0264, B:147:0x0269, B:149:0x026e, B:151:0x0272, B:153:0x0276, B:155:0x0284, B:156:0x0291, B:158:0x0296, B:160:0x029a, B:161:0x02b4, B:162:0x02b8, B:164:0x02bc, B:165:0x02e2, B:167:0x02ed, B:170:0x02f8, B:173:0x02fd, B:175:0x0307, B:176:0x0319, B:177:0x031b, B:179:0x0334, B:184:0x033e, B:186:0x0342, B:187:0x0347, B:188:0x034b, B:190:0x0351, B:192:0x0355, B:193:0x035a, B:195:0x0360, B:196:0x0365, B:198:0x0369, B:200:0x0372, B:202:0x0379, B:203:0x037d, B:205:0x0388, B:207:0x038c, B:209:0x0390, B:210:0x0398, B:211:0x039d, B:214:0x03a3, B:215:0x03ad, B:217:0x03b6, B:218:0x03c7, B:220:0x03cf, B:221:0x03d1, B:223:0x03d7, B:225:0x03dd, B:226:0x03e1, B:228:0x03ef, B:230:0x03f6, B:231:0x03ff, B:232:0x0401, B:234:0x040c, B:235:0x0415, B:237:0x0419, B:238:0x041d, B:240:0x041f, B:242:0x0427, B:243:0x0432, B:244:0x0443, B:248:0x0449, B:250:0x044d, B:254:0x045a, B:267:0x04bf, B:268:0x04c0, B:269:0x04c2, B:271:0x04c8, B:272:0x04ce, B:274:0x04de, B:275:0x04e1, B:277:0x04e5, B:279:0x04ec, B:281:0x04f7, B:283:0x0508, B:284:0x050a, B:285:0x050d, B:286:0x0512, B:288:0x0519, B:289:0x051f, B:292:0x0527, B:294:0x053f, B:295:0x0553, B:296:0x0567, B:297:0x057a, B:299:0x0586, B:300:0x0595, B:302:0x0599, B:304:0x05a1, B:306:0x05a7, B:308:0x05b2, B:309:0x05b4, B:311:0x05bf, B:313:0x05cb, B:314:0x05cf, B:317:0x05d5, B:319:0x05df, B:320:0x05e6, B:322:0x05ea, B:323:0x0604, B:325:0x061b, B:326:0x061f, B:328:0x0621, B:329:0x0627, B:333:0x062e, B:334:0x0634, B:255:0x045f, B:257:0x0463, B:260:0x047c, B:262:0x0483, B:264:0x049c, B:265:0x04a8, B:266:0x04ae, B:331:0x0629), top: B:361:0x0012 }] */
    /* JADX WARNING: Removed duplicated region for block: B:381:0x023e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:387:0x010f A[ADDED_TO_REGION, SYNTHETIC] */
    @Override // X.AbstractC117055Yb
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AaS(long r38, long r40) {
        /*
        // Method dump skipped, instructions count: 1660
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76533ln.AaS(long, long):void");
    }

    @Override // X.AbstractC106444vi, X.AbstractC117055Yb
    public void AcY(float f, float f2) {
        this.A01 = f;
        this.A02 = f2;
        if (this.A0P != null && this.A04 != 3 && super.A01 != 0) {
            A0T();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (X.C106644w2.class.equals(r3) != false) goto L_0x002a;
     */
    @Override // X.AnonymousClass5WX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int Aeb(X.C100614mC r12) {
        /*
        // Method dump skipped, instructions count: 276
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC76533ln.Aeb(X.4mC):int");
    }
}
