package X;

import java.util.ConcurrentModificationException;
import java.util.Map;

/* renamed from: X.00O  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00O {
    public static int A03;
    public static int A04;
    public static Object[] A05;
    public static Object[] A06;
    public int A00;
    public int[] A01;
    public Object[] A02;

    public AnonymousClass00O() {
        this.A01 = AnonymousClass00R.A00;
        this.A02 = AnonymousClass00R.A02;
        this.A00 = 0;
    }

    public AnonymousClass00O(int i) {
        if (i == 0) {
            this.A01 = AnonymousClass00R.A00;
            this.A02 = AnonymousClass00R.A02;
        } else {
            A00(i);
        }
        this.A00 = 0;
    }

    private void A00(int i) {
        if (i == 8) {
            synchronized (AnonymousClass00O.class) {
                Object[] objArr = A06;
                if (objArr != null) {
                    this.A02 = objArr;
                    A06 = (Object[]) objArr[0];
                    this.A01 = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    A04--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (AnonymousClass00O.class) {
                Object[] objArr2 = A05;
                if (objArr2 != null) {
                    this.A02 = objArr2;
                    A05 = (Object[]) objArr2[0];
                    this.A01 = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    A03--;
                    return;
                }
            }
        }
        this.A01 = new int[i];
        this.A02 = new Object[i << 1];
    }

    public static void A01(int[] iArr, Object[] objArr, int i) {
        int length = iArr.length;
        if (length == 8) {
            synchronized (AnonymousClass00O.class) {
                int i2 = A04;
                if (i2 < 10) {
                    objArr[0] = A06;
                    objArr[1] = iArr;
                    for (int i3 = (i << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    A06 = objArr;
                    A04 = i2 + 1;
                }
            }
        } else if (length == 4) {
            synchronized (AnonymousClass00O.class) {
                int i4 = A03;
                if (i4 < 10) {
                    objArr[0] = A05;
                    objArr[1] = iArr;
                    for (int i5 = (i << 1) - 1; i5 >= 2; i5--) {
                        objArr[i5] = null;
                    }
                    A05 = objArr;
                    A03 = i4 + 1;
                }
            }
        }
    }

    public int A02() {
        int i = this.A00;
        if (i == 0) {
            return -1;
        }
        int[] iArr = this.A01;
        try {
            int A00 = AnonymousClass00R.A00(iArr, i, 0);
            if (A00 >= 0) {
                Object[] objArr = this.A02;
                if (objArr[A00 << 1] != null) {
                    int i2 = A00 + 1;
                    while (i2 < i && iArr[i2] == 0) {
                        if (objArr[i2 << 1] == null) {
                            return i2;
                        }
                        i2++;
                    }
                    do {
                        A00--;
                        if (A00 < 0 || iArr[A00] != 0) {
                            return i2 ^ -1;
                        }
                    } while (objArr[A00 << 1] != null);
                    return A00;
                }
            }
            return A00;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    public int A03(Object obj) {
        return obj == null ? A02() : A05(obj, obj.hashCode());
    }

    public int A04(Object obj) {
        int i = this.A00 << 1;
        Object[] objArr = this.A02;
        int i2 = 1;
        if (obj == null) {
            while (i2 < i) {
                if (objArr[i2] == null) {
                    return i2 >> 1;
                }
                i2 += 2;
            }
            return -1;
        }
        while (i2 < i) {
            if (obj.equals(objArr[i2])) {
                return i2 >> 1;
            }
            i2 += 2;
        }
        return -1;
    }

    public int A05(Object obj, int i) {
        int i2 = this.A00;
        if (i2 == 0) {
            return -1;
        }
        try {
            int A00 = AnonymousClass00R.A00(this.A01, i2, i);
            if (A00 < 0 || obj.equals(this.A02[A00 << 1])) {
                return A00;
            }
            int i3 = A00 + 1;
            while (i3 < i2 && this.A01[i3] == i) {
                if (obj.equals(this.A02[i3 << 1])) {
                    return i3;
                }
                i3++;
            }
            do {
                A00--;
                if (A00 < 0 || this.A01[A00] != i) {
                    return i3 ^ -1;
                }
            } while (!obj.equals(this.A02[A00 << 1]));
            return A00;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    public Object A06(int i) {
        Object[] objArr = this.A02;
        int i2 = i << 1;
        Object obj = objArr[i2 + 1];
        int i3 = this.A00;
        int i4 = 0;
        if (i3 <= 1) {
            A01(this.A01, objArr, i3);
            this.A01 = AnonymousClass00R.A00;
            this.A02 = AnonymousClass00R.A02;
        } else {
            int i5 = i3 - 1;
            int[] iArr = this.A01;
            int length = iArr.length;
            int i6 = 8;
            if (length <= 8 || i3 >= length / 3) {
                if (i < i5) {
                    int i7 = i + 1;
                    int i8 = i5 - i;
                    System.arraycopy(iArr, i7, iArr, i, i8);
                    Object[] objArr2 = this.A02;
                    System.arraycopy(objArr2, i7 << 1, objArr2, i2, i8 << 1);
                }
                Object[] objArr3 = this.A02;
                int i9 = i5 << 1;
                objArr3[i9] = null;
                objArr3[i9 + 1] = null;
            } else {
                if (i3 > 8) {
                    i6 = i3 + (i3 >> 1);
                }
                A00(i6);
                if (i3 == this.A00) {
                    if (i > 0) {
                        System.arraycopy(iArr, 0, this.A01, 0, i);
                        System.arraycopy(objArr, 0, this.A02, 0, i2);
                    }
                    if (i < i5) {
                        int i10 = i + 1;
                        int i11 = i5 - i;
                        System.arraycopy(iArr, i10, this.A01, i, i11);
                        System.arraycopy(objArr, i10 << 1, this.A02, i2, i11 << 1);
                    }
                } else {
                    throw new ConcurrentModificationException();
                }
            }
            i4 = i5;
        }
        if (i3 == this.A00) {
            this.A00 = i4;
            return obj;
        }
        throw new ConcurrentModificationException();
    }

    public void A07(int i) {
        int i2 = this.A00;
        int[] iArr = this.A01;
        if (iArr.length < i) {
            Object[] objArr = this.A02;
            A00(i);
            if (this.A00 > 0) {
                System.arraycopy(iArr, 0, this.A01, 0, i2);
                System.arraycopy(objArr, 0, this.A02, 0, i2 << 1);
            }
            A01(iArr, objArr, i2);
        }
        if (this.A00 != i2) {
            throw new ConcurrentModificationException();
        }
    }

    public void A08(AnonymousClass00O r6) {
        int i = r6.A00;
        A07(this.A00 + i);
        if (this.A00 != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                Object[] objArr = r6.A02;
                int i3 = i2 << 1;
                put(objArr[i3], objArr[i3 + 1]);
            }
        } else if (i > 0) {
            System.arraycopy(r6.A01, 0, this.A01, 0, i);
            System.arraycopy(r6.A02, 0, this.A02, 0, i << 1);
            this.A00 = i;
        }
    }

    public void clear() {
        int i = this.A00;
        if (i > 0) {
            int[] iArr = this.A01;
            Object[] objArr = this.A02;
            this.A01 = AnonymousClass00R.A00;
            this.A02 = AnonymousClass00R.A02;
            this.A00 = 0;
            A01(iArr, objArr, i);
        }
        if (this.A00 > 0) {
            throw new ConcurrentModificationException();
        }
    }

    public boolean containsKey(Object obj) {
        return A03(obj) >= 0;
    }

    public boolean containsValue(Object obj) {
        return A04(obj) >= 0;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass00O) {
                AnonymousClass00O r7 = (AnonymousClass00O) obj;
                if (size() == r7.size()) {
                    for (int i = 0; i < this.A00; i++) {
                        Object[] objArr = this.A02;
                        Object obj2 = objArr[i << 1];
                        Object obj3 = objArr[(i << 1) + 1];
                        Object obj4 = r7.get(obj2);
                        if (obj3 == null) {
                            if (obj4 == null && r7.containsKey(obj2)) {
                            }
                            return false;
                        } else if (!obj3.equals(obj4)) {
                            return false;
                        }
                    }
                }
                return false;
            }
            if (obj instanceof Map) {
                Map map = (Map) obj;
                if (size() == map.size()) {
                    for (int i2 = 0; i2 < this.A00; i2++) {
                        Object[] objArr2 = this.A02;
                        Object obj5 = objArr2[i2 << 1];
                        Object obj6 = objArr2[(i2 << 1) + 1];
                        Object obj7 = map.get(obj5);
                        if (obj6 == null) {
                            if (obj7 == null && map.containsKey(obj5)) {
                            }
                            return false;
                        } else if (!obj6.equals(obj7)) {
                            return false;
                        }
                    }
                }
                return false;
            }
            return false;
        }
        return true;
    }

    public Object get(Object obj) {
        return getOrDefault(obj, null);
    }

    public Object getOrDefault(Object obj, Object obj2) {
        int A032 = A03(obj);
        return A032 >= 0 ? this.A02[(A032 << 1) + 1] : obj2;
    }

    public int hashCode() {
        int[] iArr = this.A01;
        Object[] objArr = this.A02;
        int i = this.A00;
        int i2 = 1;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            Object obj = objArr[i2];
            i4 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i3];
            i3++;
            i2 += 2;
        }
        return i4;
    }

    public boolean isEmpty() {
        return this.A00 <= 0;
    }

    public Object put(Object obj, Object obj2) {
        int hashCode;
        int A052;
        int i = this.A00;
        if (obj == null) {
            A052 = A02();
            hashCode = 0;
        } else {
            hashCode = obj.hashCode();
            A052 = A05(obj, hashCode);
        }
        if (A052 >= 0) {
            int i2 = (A052 << 1) + 1;
            Object[] objArr = this.A02;
            Object obj3 = objArr[i2];
            objArr[i2] = obj2;
            return obj3;
        }
        int i3 = A052 ^ -1;
        int[] iArr = this.A01;
        if (i >= iArr.length) {
            int i4 = 4;
            if (i >= 8) {
                i4 = (i >> 1) + i;
            } else if (i >= 4) {
                i4 = 8;
            }
            Object[] objArr2 = this.A02;
            A00(i4);
            if (i == this.A00) {
                int[] iArr2 = this.A01;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr2, 0, this.A02, 0, objArr2.length);
                }
                A01(iArr, objArr2, i);
            } else {
                throw new ConcurrentModificationException();
            }
        }
        if (i3 < i) {
            int[] iArr3 = this.A01;
            int i5 = i3 + 1;
            System.arraycopy(iArr3, i3, iArr3, i5, i - i3);
            Object[] objArr3 = this.A02;
            System.arraycopy(objArr3, i3 << 1, objArr3, i5 << 1, (this.A00 - i3) << 1);
        }
        int i6 = this.A00;
        if (i == i6) {
            int[] iArr4 = this.A01;
            if (i3 < iArr4.length) {
                iArr4[i3] = hashCode;
                Object[] objArr4 = this.A02;
                int i7 = i3 << 1;
                objArr4[i7] = obj;
                objArr4[i7 + 1] = obj2;
                this.A00 = i6 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public Object putIfAbsent(Object obj, Object obj2) {
        Object obj3 = get(obj);
        return obj3 == null ? put(obj, obj2) : obj3;
    }

    public Object remove(Object obj) {
        int A032 = A03(obj);
        if (A032 >= 0) {
            return A06(A032);
        }
        return null;
    }

    public boolean remove(Object obj, Object obj2) {
        int A032 = A03(obj);
        if (A032 < 0) {
            return false;
        }
        Object obj3 = this.A02[(A032 << 1) + 1];
        if (obj2 != obj3 && (obj2 == null || !obj2.equals(obj3))) {
            return false;
        }
        A06(A032);
        return true;
    }

    public Object replace(Object obj, Object obj2) {
        int A032 = A03(obj);
        if (A032 < 0) {
            return null;
        }
        int i = (A032 << 1) + 1;
        Object[] objArr = this.A02;
        Object obj3 = objArr[i];
        objArr[i] = obj2;
        return obj3;
    }

    public boolean replace(Object obj, Object obj2, Object obj3) {
        int A032 = A03(obj);
        if (A032 < 0) {
            return false;
        }
        int i = (A032 << 1) + 1;
        Object obj4 = this.A02[i];
        if (obj4 != obj2 && (obj2 == null || !obj2.equals(obj4))) {
            return false;
        }
        this.A02[i] = obj3;
        return true;
    }

    public int size() {
        return this.A00;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.A00 * 28);
        sb.append('{');
        for (int i = 0; i < this.A00; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            Object obj = this.A02[i << 1];
            if (obj != this) {
                sb.append(obj);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object obj2 = this.A02[(i << 1) + 1];
            if (obj2 != this) {
                sb.append(obj2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}
