package X;

import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.2wH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60242wH extends AnonymousClass2SU {
    public final /* synthetic */ VoipActivityV2 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C60242wH(AnonymousClass2A6 r3, UserJid userJid, VoipActivityV2 voipActivityV2) {
        super(r3, userJid, voipActivityV2.A1t, "display");
        this.A00 = voipActivityV2;
    }

    public final boolean A0A() {
        UserJid userJid;
        VideoCallParticipantView videoCallParticipantView;
        MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = this.A00.A0x;
        if (maximizedParticipantVideoDialogFragment != null) {
            UserJid userJid2 = this.A04;
            AnonymousClass2SU r0 = maximizedParticipantVideoDialogFragment.A08;
            if (r0 != null) {
                userJid = r0.A04;
            } else {
                userJid = null;
            }
            if (userJid2.equals(userJid) && (videoCallParticipantView = super.A00) != null && videoCallParticipantView.A03 != 7) {
                return true;
            }
        }
        return false;
    }
}
