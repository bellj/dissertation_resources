package X;

/* renamed from: X.43L  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43L extends AbstractC16110oT {
    public Long A00;
    public String A01;

    public AnonymousClass43L() {
        super(2442, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamExitReasonsSummary {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "exitReasonsCensus", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "lastReportedExitTime", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
