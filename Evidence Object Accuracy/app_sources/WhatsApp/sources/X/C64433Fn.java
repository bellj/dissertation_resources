package X;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import com.whatsapp.blocklist.BlockConfirmationDialogFragment;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.support.ReportSpamDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.3Fn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64433Fn {
    public C15370n3 A00;
    public boolean A01;
    public final int A02 = 902;
    public final int A03 = 21;
    public final ActivityC000800j A04;
    public final AbstractC13860kS A05;
    public final C238013b A06;
    public final C22700zV A07;
    public final C14820m6 A08;
    public final C19990v2 A09;
    public final C15600nX A0A;
    public final C21250x7 A0B;
    public final C22100yW A0C;
    public final C22230yk A0D;
    public final Runnable A0E;
    public final Runnable A0F;

    public C64433Fn(ActivityC000800j r3, AbstractC13860kS r4, C238013b r5, C22700zV r6, C14820m6 r7, C19990v2 r8, C15600nX r9, C21250x7 r10, C22100yW r11, C22230yk r12, Runnable runnable, Runnable runnable2) {
        this.A04 = r3;
        this.A09 = r8;
        this.A0B = r10;
        this.A0D = r12;
        this.A06 = r5;
        this.A07 = r6;
        this.A08 = r7;
        this.A0C = r11;
        this.A0A = r9;
        this.A05 = r4;
        this.A0E = runnable;
        this.A0F = runnable2;
    }

    public SpannableStringBuilder A00(String str) {
        Spanned fromHtml = Html.fromHtml(str);
        SpannableStringBuilder A0J = C12990iw.A0J(fromHtml);
        URLSpan[] uRLSpanArr = (URLSpan[]) fromHtml.getSpans(0, fromHtml.length(), URLSpan.class);
        if (uRLSpanArr != null) {
            for (URLSpan uRLSpan : uRLSpanArr) {
                if ("group-privacy-settings".equals(uRLSpan.getURL())) {
                    int spanStart = A0J.getSpanStart(uRLSpan);
                    int spanEnd = A0J.getSpanEnd(uRLSpan);
                    int spanFlags = A0J.getSpanFlags(uRLSpan);
                    A0J.removeSpan(uRLSpan);
                    A0J.setSpan(new C58222oL(this.A04, this), spanStart, spanEnd, spanFlags);
                }
            }
        }
        return A0J;
    }

    public final String A01(int i) {
        C15370n3 r0 = this.A00;
        if (!(r0 == null || r0.A0B(C15580nU.class) == null)) {
            if (i == 0) {
                return "group_spam_banner_report";
            }
            C15370n3 r02 = this.A00;
            if (r02 == null) {
                Log.e("Contact is unexpected null");
                return "left_group_spam_banner_report";
            }
            GroupJid groupJid = (GroupJid) r02.A0B(C15580nU.class);
            if (groupJid == null || !this.A0A.A0C(groupJid)) {
                return "left_group_spam_banner_report";
            }
        }
        return null;
    }

    public void A02() {
        AbstractC14640lm A01 = C15370n3.A01(this.A00);
        C21250x7 r2 = this.A0B;
        r2.A02(A01, C12980iv.A0j(), this.A01);
        r2.A06(A01, 1);
        if (this.A09.A06(A01) != null) {
            this.A0D.A04(A01, 9, 0, 0);
        }
        this.A0F.run();
    }

    public void A03(int i) {
        String str;
        UserJid A04 = C15370n3.A04(this.A00);
        C238013b r2 = this.A06;
        if (!r2.A0I(A04)) {
            this.A0B.A02(A04, C12970iu.A0h(), this.A01);
            if (this.A00.A0J()) {
                boolean A1V = C12960it.A1V(i, 1);
                ActivityC000800j r3 = this.A04;
                String str2 = "biz_spam_banner_block";
                if (this.A01) {
                    str2 = "triggered_block";
                }
                r3.startActivityForResult(C14960mK.A0S(r3, A04, str2, false, true, A1V), this.A02);
                return;
            }
            if (i == 1) {
                str = "1_1_spam_banner_block";
            } else {
                str = "1_1_old_spam_banner_block";
            }
            if (this.A01) {
                str = "triggered_block";
            }
            this.A05.Adm(BlockConfirmationDialogFragment.A00(A04, str, true, false, true));
            return;
        }
        r2.A0B(this.A04, this.A00, false);
    }

    public void A04(int i) {
        String str;
        AbstractC14640lm A01 = C15370n3.A01(this.A00);
        if (A01 instanceof C15580nU) {
            str = A01(i);
            AnonymousClass009.A05(str);
        } else {
            str = "1_1_spam_banner_report";
        }
        C21250x7 r2 = this.A0B;
        r2.A02(A01, C12970iu.A0g(), this.A01);
        r2.A06(A01, -2);
        this.A0C.A05().A00(new AbstractC14590lg(A01, str) { // from class: X.3be
            public final /* synthetic */ AbstractC14640lm A01;
            public final /* synthetic */ String A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                C64433Fn r22 = C64433Fn.this;
                AbstractC14640lm r3 = this.A01;
                String str2 = this.A02;
                Boolean bool = (Boolean) obj;
                AbstractC13860kS r1 = r22.A05;
                if (!r1.AJN()) {
                    if (r22.A01) {
                        str2 = "triggered_block";
                    }
                    r1.Adm(ReportSpamDialogFragment.A00(r3, null, null, str2, 1, bool.booleanValue(), true, true, true));
                }
            }
        });
    }
}
