package X;

import java.net.URL;

/* renamed from: X.0IL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0IL extends AnonymousClass0eL {
    public final /* synthetic */ C05250Ov A00;
    public final /* synthetic */ URL A01;

    public AnonymousClass0IL(C05250Ov r1, URL url) {
        this.A00 = r1;
        this.A01 = url;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a7  */
    @Override // X.AnonymousClass0eL, java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
            X.0Ov r6 = r7.A00
            java.net.URL r0 = r7.A01
            java.io.InputStream r5 = r0.openStream()     // Catch: ArrayIndexOutOfBoundsException -> 0x0009, IOException -> 0x003d
            goto L_0x000d
        L_0x0009:
            java.io.InputStream r5 = r0.openStream()     // Catch: IOException -> 0x003d
        L_0x000d:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch: IOException -> 0x003d
            r4.<init>()     // Catch: IOException -> 0x003d
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r0]     // Catch: IOException -> 0x003d
        L_0x0016:
            int r2 = r5.read(r3)     // Catch: IOException -> 0x002e, all -> 0x0036
            if (r2 < 0) goto L_0x0026
            r1 = 0
            java.lang.String r0 = new java.lang.String     // Catch: IOException -> 0x002e, all -> 0x0036
            r0.<init>(r3, r1, r2)     // Catch: IOException -> 0x002e, all -> 0x0036
            r4.append(r0)     // Catch: IOException -> 0x002e, all -> 0x0036
            goto L_0x0016
        L_0x0026:
            r5.close()     // Catch: IOException -> 0x0029, IOException -> 0x003d
        L_0x0029:
            java.lang.String r1 = r4.toString()     // Catch: IOException -> 0x003d
            goto L_0x003f
        L_0x002e:
            java.lang.String r1 = ""
            if (r5 == 0) goto L_0x003f
            r5.close()     // Catch: IOException -> 0x003f, IOException -> 0x003d
            goto L_0x003f
        L_0x0036:
            r0 = move-exception
            if (r5 == 0) goto L_0x003c
            r5.close()     // Catch: IOException -> 0x003c, IOException -> 0x003d
        L_0x003c:
            throw r0     // Catch: IOException -> 0x003d
        L_0x003d:
            java.lang.String r1 = ""
        L_0x003f:
            java.lang.String r3 = ""
            boolean r0 = r1.equals(r3)
            if (r0 != 0) goto L_0x00ba
            java.lang.Class<X.0Je> r0 = X.EnumC03800Je.class
            java.util.EnumSet r5 = java.util.EnumSet.noneOf(r0)
            java.lang.String r0 = "\"payload\":["
            int r0 = r1.indexOf(r0)
            int r0 = r0 + 11
            java.lang.String r2 = r1.substring(r0)
            r0 = 93
            int r1 = r2.indexOf(r0)
            r0 = -1
            if (r1 == r0) goto L_0x00ba
            r4 = 0
            java.lang.String r1 = r2.substring(r4, r1)
            java.lang.String r0 = "[^A-Za-z0-9,]"
            java.lang.String r0 = r1.replaceAll(r0, r3)
            java.lang.String r1 = r0.trim()
            java.lang.String r0 = ","
            java.lang.String[] r3 = r1.split(r0)
            int r2 = r3.length
        L_0x0078:
            if (r4 >= r2) goto L_0x00b5
            r1 = r3[r4]
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r1 = r1.toLowerCase(r0)
            int r0 = r1.hashCode()
            switch(r0) {
                case -1081373969: goto L_0x00aa;
                case 110345: goto L_0x009f;
                case 3198960: goto L_0x0094;
                case 1807548271: goto L_0x0091;
                default: goto L_0x0089;
            }
        L_0x0089:
            X.0Je r0 = X.EnumC03800Je.UNKNOWN
        L_0x008b:
            r5.add(r0)
            int r4 = r4 + 1
            goto L_0x0078
        L_0x0091:
            java.lang.String r0 = "openstreetmap"
            goto L_0x00a1
        L_0x0094:
            java.lang.String r0 = "here"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0089
            X.0Je r0 = X.EnumC03800Je.HERE
            goto L_0x008b
        L_0x009f:
            java.lang.String r0 = "osm"
        L_0x00a1:
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0089
            X.0Je r0 = X.EnumC03800Je.OSM
            goto L_0x008b
        L_0x00aa:
            java.lang.String r0 = "mapbox"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0089
            X.0Je r0 = X.EnumC03800Je.MAPBOX
            goto L_0x008b
        L_0x00b5:
            X.04M r0 = r6.A06
            r0.setCurrentAttribution(r5)
        L_0x00ba:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IL.run():void");
    }
}
