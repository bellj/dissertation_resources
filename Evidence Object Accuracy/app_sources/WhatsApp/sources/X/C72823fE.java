package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3fE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72823fE extends AnimatorListenerAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C69653a1 A01;
    public final /* synthetic */ boolean A02;

    public C72823fE(C69653a1 r1, int i, boolean z) {
        this.A01 = r1;
        this.A02 = z;
        this.A00 = i;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.A01.setVisibility(this.A00);
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        if (this.A02) {
            this.A01.A01.setVisibility(0);
        }
    }
}
