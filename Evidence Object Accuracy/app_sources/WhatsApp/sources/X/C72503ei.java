package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72503ei extends AnimatorListenerAdapter {
    public final /* synthetic */ AbstractC28551Oa A00;

    public C72503ei(AbstractC28551Oa r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        C72453ed.A1D(this.A00);
    }
}
