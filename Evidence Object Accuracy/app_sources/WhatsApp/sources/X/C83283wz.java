package X;

import android.view.animation.Animation;

/* renamed from: X.3wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83283wz extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ C92254Vd A00;

    public C83283wz(C92254Vd r1) {
        this.A00 = r1;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        C92254Vd r2 = this.A00;
        r2.A01.setVisibility(8);
        r2.A02.ATC(0.0f);
    }
}
