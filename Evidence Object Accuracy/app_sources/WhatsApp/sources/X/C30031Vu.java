package X;

import android.database.Cursor;

/* renamed from: X.1Vu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30031Vu {
    public Cursor A00;
    public final long A01;
    public final long A02;

    public C30031Vu(Cursor cursor, long j, long j2) {
        this.A01 = j;
        this.A02 = j2;
        this.A00 = cursor;
    }
}
