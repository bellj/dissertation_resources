package X;

import com.whatsapp.payments.ui.BrazilOrderDetailsActivity;
import com.whatsapp.payments.ui.BrazilPaymentActivity;

/* renamed from: X.68D  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68D implements AnonymousClass1QA {
    public final /* synthetic */ C30821Yy A00;
    public final /* synthetic */ C133966Cu A01;
    public final /* synthetic */ AbstractC16390ow A02;
    public final /* synthetic */ String A03;

    public AnonymousClass68D(C30821Yy r1, C133966Cu r2, AbstractC16390ow r3, String str) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = str;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1QA
    public void AWX() {
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A01.A00;
        brazilOrderDetailsActivity.AaN();
        String str = this.A03;
        if (str == null) {
            brazilOrderDetailsActivity.AVp(this.A00);
        } else {
            brazilOrderDetailsActivity.A07.A00(new Runnable(this.A00, this) { // from class: X.6I3
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ AnonymousClass68D A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AnonymousClass68D r0 = this.A01;
                    r0.A01.A00.AVp(this.A00);
                }
            }, str);
        }
    }

    @Override // X.AnonymousClass1QA
    public void AWZ() {
        AnonymousClass1ZD r0;
        BrazilOrderDetailsActivity brazilOrderDetailsActivity = this.A01.A00;
        brazilOrderDetailsActivity.AaN();
        AbstractC16390ow r1 = this.A02;
        C16470p4 ABf = r1.ABf();
        if (ABf != null && (r0 = ABf.A01) != null && r0.A04.A02 != null) {
            C117295Zj.A0f(brazilOrderDetailsActivity, brazilOrderDetailsActivity.getResources(), C117295Zj.A0V(((BrazilPaymentActivity) brazilOrderDetailsActivity).A04, "HH:mm", r1.ABf().A01.A04.A02.A00), C12970iu.A1b());
        }
    }
}
