package X;

import java.util.List;

/* renamed from: X.3mw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77223mw extends AbstractC76783mE {
    public final float A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final C95304dT A04 = new C95304dT();
    public final String A05;
    public final boolean A06;

    public C77223mw(List list) {
        super("Tx3gDecoder");
        String str = "sans-serif";
        boolean z = true;
        if (list.size() == 1 && (C72463ee.A0b(list, 0).length == 48 || C72463ee.A0b(list, 0).length == 53)) {
            byte[] A0b = C72463ee.A0b(list, 0);
            this.A03 = A0b[24];
            this.A02 = ((A0b[26] & 255) << 24) | ((A0b[27] & 255) << 16) | ((A0b[28] & 255) << 8) | (A0b[29] & 255);
            this.A05 = "Serif".equals(new String(A0b, 43, A0b.length - 43, C88814Hf.A05)) ? "serif" : str;
            int i = A0b[25] * 20;
            this.A01 = i;
            z = (A0b[0] & 32) == 0 ? false : z;
            this.A06 = z;
            if (z) {
                this.A00 = Math.max(0.0f, Math.min(((float) ((A0b[11] & 255) | ((A0b[10] & 255) << 8))) / ((float) i), 0.95f));
            } else {
                this.A00 = 0.85f;
            }
        } else {
            this.A02 = -1;
            this.A05 = str;
            this.A00 = 0.85f;
            this.A01 = -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A01(android.text.SpannableStringBuilder r6, int r7, int r8, int r9, int r10, int r11) {
        /*
            if (r7 == r8) goto L_0x002b
            r5 = r11 | 33
            r0 = r7 & 1
            r4 = 0
            r3 = 1
            boolean r2 = X.C12960it.A1S(r0)
            r0 = r7 & 2
            boolean r1 = X.C12960it.A1S(r0)
            if (r2 == 0) goto L_0x0039
            if (r1 == 0) goto L_0x0017
            r3 = 3
        L_0x0017:
            android.text.style.StyleSpan r0 = new android.text.style.StyleSpan
            r0.<init>(r3)
            r6.setSpan(r0, r9, r10, r5)
        L_0x001f:
            r0 = r7 & 4
            if (r0 == 0) goto L_0x002c
            android.text.style.UnderlineSpan r0 = new android.text.style.UnderlineSpan
            r0.<init>()
            r6.setSpan(r0, r9, r10, r5)
        L_0x002b:
            return
        L_0x002c:
            if (r2 != 0) goto L_0x002b
            if (r1 != 0) goto L_0x002b
            android.text.style.StyleSpan r0 = new android.text.style.StyleSpan
            r0.<init>(r4)
            r6.setSpan(r0, r9, r10, r5)
            return
        L_0x0039:
            if (r1 == 0) goto L_0x001f
            r3 = 2
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77223mw.A01(android.text.SpannableStringBuilder, int, int, int, int, int):void");
    }
}
