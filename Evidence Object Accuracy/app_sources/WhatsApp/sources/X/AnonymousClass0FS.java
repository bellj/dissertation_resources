package X;

/* renamed from: X.0FS  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FS extends AbstractC02880Ff {
    public final /* synthetic */ C07740a0 A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`out_of_quota_policy`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FS(AnonymousClass0QN r1, C07740a0 r2) {
        super(r1);
        this.A00 = r2;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:62:0x0190 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:89:0x0153 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:76:0x01b1 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v0 */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r5v2, types: [java.io.OutputStream, java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r5v3 */
    /* JADX WARN: Type inference failed for: r5v4 */
    /* JADX WARN: Type inference failed for: r5v5 */
    /* JADX WARN: Type inference failed for: r5v6, types: [java.io.OutputStream, java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r3v9, types: [java.util.Iterator] */
    /* JADX WARN: Type inference failed for: r5v7 */
    /* JADX WARN: Type inference failed for: r5v8 */
    /* JADX WARN: Type inference failed for: r5v9 */
    /* JADX WARN: Type inference failed for: r5v10 */
    /* JADX WARN: Type inference failed for: r5v11 */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:89|50|(2:96|52)|(5:98|53|(2:56|54)|100|57)|93|68|71|(2:73|74)) */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01a2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01a3, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01b1, code lost:
        if (r3 == null) goto L_0x01bb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0153 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // X.AbstractC02880Ff
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(X.AbstractC12830ic r14, java.lang.Object r15) {
        /*
        // Method dump skipped, instructions count: 498
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0FS.A03(X.0ic, java.lang.Object):void");
    }
}
