package X;

/* renamed from: X.4ZV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4ZV {
    public static final Class A00;
    public static final boolean A01;

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0011, code lost:
        if (java.lang.Class.forName("org.robolectric.Robolectric") == null) goto L_0x0013;
     */
    static {
        /*
            java.lang.String r0 = "libcore.io.Memory"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch: all -> 0x0007
            goto L_0x0008
        L_0x0007:
            r0 = 0
        L_0x0008:
            X.AnonymousClass4ZV.A00 = r0
            java.lang.String r0 = "org.robolectric.Robolectric"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch: all -> 0x0013
            r0 = 1
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            r0 = 0
        L_0x0014:
            X.AnonymousClass4ZV.A01 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4ZV.<clinit>():void");
    }

    public static boolean A00() {
        return A00 != null && !A01;
    }
}
