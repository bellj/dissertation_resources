package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.3Lv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65983Lv implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(51);
    public final String A00;
    public final String A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C65983Lv(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A00 = C12990iw.A0p(parcel);
    }

    public C65983Lv(String str, String str2) {
        this.A01 = str;
        this.A00 = str2;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("PhoneNumberSelectionInfo{phoneNumberLabel='");
        char A00 = C12990iw.A00(this.A01, A0k);
        A0k.append(", phoneNumber='");
        A0k.append(this.A00);
        A0k.append(A00);
        return C12970iu.A0v(A0k);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }
}
