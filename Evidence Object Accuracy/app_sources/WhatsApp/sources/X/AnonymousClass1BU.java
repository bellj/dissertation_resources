package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/* renamed from: X.1BU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BU implements AbstractC231810r {
    public final AnonymousClass19M A00;
    public final C231710q A01;
    public final AnonymousClass1BT A02;
    public final Map A03;
    public final Map A04;
    public final Map A05 = new HashMap();

    public AnonymousClass1BU(AnonymousClass19M r4, C231710q r5, AnonymousClass1BT r6) {
        this.A00 = r4;
        this.A02 = r6;
        this.A01 = r5;
        for (AbstractC470728v r1 : AnonymousClass3GW.A00()) {
            A00(r1, this.A05);
        }
        this.A03 = new HashMap();
        this.A04 = new HashMap();
    }

    public static void A00(AbstractC470728v r5, Map map) {
        C37471mS[] ACh = r5.ACh();
        for (C37471mS r1 : ACh) {
            Collection collection = (Collection) map.get(r1);
            if (collection == null) {
                collection = new LinkedHashSet();
                map.put(r1, collection);
            }
            collection.add(r5);
        }
    }

    public synchronized void A01(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            A00((AbstractC470728v) it.next(), this.A04);
        }
    }

    @Override // X.AbstractC231810r
    public void A7B() {
        this.A01.A7B();
    }

    @Override // X.AbstractC231810r
    public /* bridge */ /* synthetic */ Collection A9t(String str, Object[] objArr, int i, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (AbstractC470728v r1 : this.A02.A02()) {
            if (r1 instanceof C470828w) {
                A00(r1, hashMap);
            }
            arrayList.addAll(Arrays.asList(r1.ACh()));
        }
        List<C37471mS> A01 = this.A01.A01(str, arrayList, new ArrayList(), null, i, false);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        ArrayList arrayList2 = new ArrayList(4);
        arrayList2.add(hashMap);
        arrayList2.add(this.A03);
        arrayList2.add(this.A04);
        arrayList2.add(this.A05);
        synchronized (this) {
            Iterator it = arrayList2.iterator();
            while (it.hasNext()) {
                Map map = (Map) it.next();
                for (C37471mS r0 : A01) {
                    Collection<AbstractC470728v> collection = (Collection) map.get(r0);
                    if (collection != null) {
                        for (AbstractC470728v r12 : collection) {
                            if (objArr == null || !C37871n9.A01(r12, objArr)) {
                                if (r12 instanceof C470828w) {
                                    linkedHashSet2.add(r12);
                                } else {
                                    linkedHashSet.add(r12);
                                }
                            }
                        }
                    }
                }
            }
        }
        for (C37471mS r2 : A01) {
            linkedHashSet.add(new AnonymousClass3YL(r2, this.A00));
        }
        LinkedHashSet linkedHashSet3 = linkedHashSet2;
        if (z) {
            linkedHashSet3 = linkedHashSet;
        }
        LinkedHashSet linkedHashSet4 = new LinkedHashSet(linkedHashSet3);
        if (z) {
            linkedHashSet = linkedHashSet2;
        }
        linkedHashSet4.addAll(linkedHashSet);
        return linkedHashSet4;
    }

    @Override // X.AbstractC231810r
    public void AcF(boolean z) {
        this.A01.AcF(z);
    }

    @Override // X.AbstractC231810r
    public int getCount() {
        return this.A01.getCount();
    }
}
