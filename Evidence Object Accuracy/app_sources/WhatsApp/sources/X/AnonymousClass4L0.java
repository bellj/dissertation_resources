package X;

import java.util.regex.Pattern;

/* renamed from: X.4L0  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4L0 {
    public final Pattern A00;

    public AnonymousClass4L0(String str) {
        StringBuilder A0k = C12960it.A0k("(?:WhatsApp|");
        A0k.append(Pattern.quote(str));
        this.A00 = Pattern.compile(C12960it.A0d(").*?([0-9]{3})-([0-9]{3})", A0k));
    }
}
