package X;

/* renamed from: X.4W4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4W4 {
    public final int A00;
    public final int A01 = 7;
    public final int A02;
    public final int A03;
    public final String A04;
    public final String A05;
    public final String A06;

    public AnonymousClass4W4(String str, String str2, String str3, int i, int i2, int i3) {
        this.A00 = i;
        this.A05 = str;
        this.A03 = i2;
        this.A02 = i3;
        this.A06 = str2;
        this.A04 = str3;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamsysMMSUploadRequest{mediaType=");
        A0k.append(this.A00);
        A0k.append(", filePath='");
        A0k.append(this.A05);
        A0k.append('\'');
        A0k.append(", uploadType=");
        A0k.append(this.A03);
        A0k.append(", uploadOptions=");
        A0k.append(this.A02);
        A0k.append(", transferOptions=");
        A0k.append(this.A01);
        A0k.append(", loggingId='");
        A0k.append(this.A06);
        A0k.append('\'');
        A0k.append(", connBlockJSONStr='");
        A0k.append(this.A04);
        A0k.append('\'');
        return C12970iu.A0v(A0k);
    }
}
