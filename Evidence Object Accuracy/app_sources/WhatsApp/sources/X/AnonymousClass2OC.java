package X;

import android.content.Context;

/* renamed from: X.2OC  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2OC {
    public String A00(Context context) {
        if (this instanceof AnonymousClass2OD) {
            AnonymousClass2OD r6 = (AnonymousClass2OD) this;
            Object[] objArr = r6.A01;
            int length = objArr.length;
            Object[] objArr2 = new Object[length];
            for (int i = 0; i < length; i++) {
                Object obj = objArr[i];
                if (obj instanceof AnonymousClass2OC) {
                    objArr2[i] = ((AnonymousClass2OC) obj).A00(context);
                } else {
                    objArr2[i] = obj;
                }
            }
            return context.getString(r6.A00, objArr2);
        } else if (!(this instanceof AnonymousClass2OB)) {
            return ((AnonymousClass2OE) this).A00;
        } else {
            AnonymousClass2OB r62 = (AnonymousClass2OB) this;
            Object[] objArr3 = r62.A02;
            int length2 = objArr3.length;
            Object[] objArr4 = new Object[length2];
            for (int i2 = 0; i2 < length2; i2++) {
                Object obj2 = objArr3[i2];
                if (obj2 instanceof AnonymousClass2OC) {
                    objArr4[i2] = ((AnonymousClass2OC) obj2).A00(context);
                } else {
                    objArr4[i2] = obj2;
                }
            }
            return context.getResources().getQuantityString(r62.A01, r62.A00, objArr4);
        }
    }
}
