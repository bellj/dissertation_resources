package X;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SendDisableLiveLocationJob;
import com.whatsapp.jobqueue.job.SendFinalLiveLocationNotificationJob;
import com.whatsapp.jobqueue.job.SendLiveLocationKeyJob;
import com.whatsapp.location.FinalLiveLocationBroadcastReceiver;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/* renamed from: X.0oK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16030oK {
    public int A00 = 0;
    public long A01;
    public long A02;
    public long A03 = 0;
    public long A04;
    public C27131Gd A05;
    public AbstractC18860tB A06;
    public C30751Yr A07;
    public Long A08 = null;
    public Map A09;
    public Map A0A;
    public final Handler A0B = new Handler();
    public final C15570nT A0C;
    public final C20670w8 A0D;
    public final C16240og A0E;
    public final AnonymousClass13T A0F;
    public final C15550nR A0G;
    public final AnonymousClass10S A0H;
    public final C18230s7 A0I;
    public final AnonymousClass01d A0J;
    public final C14830m7 A0K;
    public final C16590pI A0L;
    public final C14820m6 A0M;
    public final C15990oG A0N;
    public final C18240s8 A0O;
    public final C21320xE A0P;
    public final AnonymousClass12H A0Q;
    public final AnonymousClass13N A0R;
    public final AnonymousClass15N A0S;
    public final C17220qS A0T;
    public final C22230yk A0U;
    public final Object A0V = new Object();
    public final Object A0W = new Object();
    public final Object A0X = new Object();
    public final Runnable A0Y = new RunnableBRunnable0Shape7S0100000_I0_7(this, 39);
    public final Runnable A0Z = new RunnableBRunnable0Shape7S0100000_I0_7(this, 38);
    public final HashSet A0a = new HashSet();
    public final List A0b = new ArrayList();
    public final List A0c = new ArrayList();
    public final Map A0d = new HashMap();
    public final Map A0e = new HashMap();
    public final Map A0f = new HashMap();
    public final Map A0g = new HashMap();
    public final Map A0h = new HashMap();
    public volatile AnonymousClass01H A0i;

    public static boolean A01(long j, long j2) {
        return j == 0 || j > j2;
    }

    public C16030oK(C15570nT r6, C20670w8 r7, C16240og r8, AnonymousClass13T r9, C15550nR r10, AnonymousClass10S r11, C18230s7 r12, AnonymousClass01d r13, C14830m7 r14, C16590pI r15, C14820m6 r16, C15990oG r17, C18240s8 r18, C21320xE r19, AnonymousClass12H r20, AnonymousClass13N r21, AnonymousClass15N r22, C17220qS r23, C22230yk r24) {
        this.A0L = r15;
        this.A0K = r14;
        this.A0I = r12;
        this.A0C = r6;
        this.A0D = r7;
        this.A0T = r23;
        this.A0G = r10;
        this.A0O = r18;
        this.A0J = r13;
        this.A0U = r24;
        this.A0H = r11;
        this.A0Q = r20;
        this.A0E = r8;
        this.A0R = r21;
        this.A0N = r17;
        this.A0F = r9;
        this.A0M = r16;
        this.A0S = r22;
        this.A0P = r19;
        C35811im r0 = new C35811im(this);
        this.A05 = r0;
        r11.A03(r0);
        AnonymousClass1hN r02 = new AnonymousClass1hN(this);
        this.A06 = r02;
        r20.A03(r02);
        SharedPreferences sharedPreferences = r16.A00;
        this.A04 = sharedPreferences.getLong("live_location_sharing_session_total_time", 0);
        this.A02 = sharedPreferences.getLong("live_location_sharing_session_start_time", 0);
        this.A01 = sharedPreferences.getLong("live_location_sharing_session_end_time", 0);
    }

    public static final C30751Yr A00(UserJid userJid, C27081Fy r6, C30341Xa r7) {
        C35771ii r3 = r6.A0P;
        if (r3 == null) {
            r3 = C35771ii.A0B;
        }
        C30751Yr r62 = new C30751Yr(userJid);
        r62.A00 = r3.A00;
        r62.A01 = r3.A01;
        r62.A03 = r3.A03;
        int i = r3.A04;
        if ((i & 8) == 8) {
            float f = r3.A02;
            if (!Float.isNaN(f)) {
                r62.A02 = f;
            }
        }
        r62.A04 = r3.A05;
        if ((i & 128) == 128 && r7 != null) {
            r62.A05 = r7.A0I + (((long) r3.A06) * 1000);
        }
        return r62;
    }

    public final long A02() {
        long j;
        synchronized (this.A0X) {
            SharedPreferences sharedPreferences = this.A0M.A00;
            long j2 = sharedPreferences.getLong("live_location_sequence_number", -1);
            long A01 = this.A0K.A01() * 1000;
            if (A01 > j2) {
                StringBuilder sb = new StringBuilder();
                sb.append("LocationSharingManager/getNextSequenceNumber; got a new sequence number; currentSequenceNumber=");
                sb.append(A01);
                Log.i(sb.toString());
                j2 = A01;
            }
            j = j2 + 1;
            sharedPreferences.edit().putLong("live_location_sequence_number", j).apply();
        }
        return j;
    }

    public long A03(AbstractC14640lm r4) {
        long j;
        synchronized (this.A0X) {
            C35211hS r0 = (C35211hS) A0B().get(r4);
            j = r0 != null ? r0.A01 : -1;
        }
        return j;
    }

    public long A04(C30341Xa r5) {
        long j;
        C35221hT r1;
        synchronized (this.A0W) {
            Map A0A = A0A();
            AnonymousClass1IS r2 = r5.A0z;
            Map map = (Map) A0A.get(r2.A00);
            j = (map == null || (r1 = (C35221hT) map.get(r5.A0C())) == null || !r2.equals(r1.A02)) ? -1 : r1.A00;
        }
        return j;
    }

    public long A05(C30341Xa r5) {
        long j;
        AnonymousClass1IS r3 = r5.A0z;
        AbstractC14640lm r1 = r3.A00;
        AnonymousClass009.A05(r1);
        synchronized (this.A0X) {
            C35211hS r12 = (C35211hS) A0B().get(r1);
            j = (r12 == null || !r12.A02.equals(r3)) ? -1 : r12.A01;
        }
        return j;
    }

    public final C30341Xa A06(AnonymousClass1IS r4) {
        AbstractC15340mz A03 = ((C15650ng) this.A0i.get()).A0K.A03(r4);
        if (!(A03 instanceof C30341Xa) || A03.A13) {
            return null;
        }
        return (C30341Xa) A03;
    }

    public ArrayList A07(AbstractC14640lm r9) {
        ArrayList arrayList;
        C30751Yr r0;
        synchronized (this.A0W) {
            Map map = (Map) A0A().get(r9);
            long A00 = this.A0K.A00();
            arrayList = new ArrayList();
            if (map != null) {
                for (C35221hT r4 : map.values()) {
                    if (A01(r4.A00, A00) && (r0 = (C30751Yr) this.A0h.get(r4.A01)) != null) {
                        arrayList.add(r0);
                    }
                }
            }
        }
        return arrayList;
    }

    public List A08() {
        ArrayList arrayList;
        synchronized (this.A0X) {
            A0B();
            Set A0C = A0C();
            A0C.removeAll(this.A0a);
            StringBuilder sb = new StringBuilder();
            sb.append("LocationSharingManager/getJidsNeedingSenderKey; jids.size=");
            sb.append(A0C.size());
            Log.i(sb.toString());
            arrayList = new ArrayList(A0C);
        }
        return arrayList;
    }

    public List A09(AbstractC14640lm r9) {
        ArrayList arrayList = new ArrayList();
        synchronized (this.A0W) {
            Map map = (Map) A0A().get(r9);
            long A00 = this.A0K.A00();
            if (map != null) {
                for (C35221hT r4 : map.values()) {
                    if (A01(r4.A00, A00)) {
                        arrayList.add(r4.A01);
                    }
                }
            }
        }
        return arrayList;
    }

    public final Map A0A() {
        Map map;
        C35831ip r0;
        synchronized (this.A0W) {
            if (this.A09 == null) {
                Map map2 = this.A0h;
                AnonymousClass15N r5 = this.A0S;
                long currentTimeMillis = System.currentTimeMillis();
                HashMap hashMap = new HashMap();
                try {
                    C16310on A01 = r5.A00.get();
                    try {
                        Cursor A08 = A01.A03.A08("location_cache", null, null, null, C35821io.A00, null);
                        if (A08 == null) {
                            Log.e("LocationSharingStore/getAllUserLocations/unable to get user location cache");
                            A01.close();
                        } else {
                            while (A08.moveToNext()) {
                                UserJid nullable = UserJid.getNullable(A08.getString(0));
                                if (nullable == null) {
                                    r0 = null;
                                } else {
                                    r0 = new C35831ip(A08, nullable);
                                }
                                if (r0 != null) {
                                    C30751Yr r1 = r0.A00;
                                    hashMap.put(r1.A06, r1);
                                }
                            }
                            A08.close();
                            A01.close();
                            StringBuilder sb = new StringBuilder("LocationSharingStore/getAllUserLocations/returned ");
                            sb.append(hashMap.size());
                            sb.append(" user locations sharer | time: ");
                            sb.append(System.currentTimeMillis() - currentTimeMillis);
                            Log.i(sb.toString());
                        }
                        map2.putAll(hashMap);
                        HashSet hashSet = new HashSet(map2.keySet());
                        this.A09 = new HashMap();
                        r5.A01(this.A0K.A00(), false);
                        List<C35801il> A00 = r5.A00(0, false);
                        HashMap hashMap2 = new HashMap();
                        for (C35801il r3 : A00) {
                            AbstractC14640lm r12 = r3.A01;
                            if (!hashMap2.containsKey(r12)) {
                                hashMap2.put(r12, new HashMap());
                            }
                            UserJid userJid = r3.A02;
                            ((Map) hashMap2.get(r12)).put(userJid, new C35221hT(userJid, r3.A03, r3.A00));
                        }
                        HashSet hashSet2 = new HashSet();
                        for (AbstractC14640lm r32 : hashMap2.keySet()) {
                            if (this.A0G.A09(r32) != null) {
                                if (!this.A09.containsKey(r32)) {
                                    this.A09.put(r32, new HashMap());
                                }
                                Map map3 = (Map) hashMap2.get(r32);
                                AnonymousClass009.A05(map3);
                                Map map4 = (Map) this.A09.get(r32);
                                AnonymousClass009.A05(map4);
                                for (UserJid userJid2 : map3.keySet()) {
                                    map4.put(userJid2, (C35221hT) map3.get(userJid2));
                                    hashSet.remove(userJid2);
                                }
                            } else {
                                hashSet2.add(r32);
                            }
                        }
                        if (!hashSet2.isEmpty()) {
                            r5.A04(hashSet2, false);
                        }
                        if (!hashSet.isEmpty()) {
                            r5.A05(hashSet);
                        }
                        A0M();
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (Exception e) {
                    Log.e("LocationSharingStore/getAllUserLocations/error getting user locations", e);
                    throw new RuntimeException(e);
                }
            }
            map = this.A09;
        }
        return map;
    }

    public final Map A0B() {
        Map map;
        synchronized (this.A0X) {
            if (this.A0A == null) {
                this.A0A = new HashMap();
                C14830m7 r8 = this.A0K;
                long A00 = r8.A00();
                AnonymousClass15N r7 = this.A0S;
                r7.A01(A00 - 604800000, true);
                List<C35801il> A002 = r7.A00(r8.A00(), true);
                HashMap hashMap = new HashMap(A002.size());
                for (C35801il r9 : A002) {
                    AbstractC14640lm r10 = r9.A01;
                    if (!hashMap.containsKey(r10)) {
                        hashMap.put(r10, new C35211hS(r9.A03, null, r9.A00));
                    }
                    Object obj = hashMap.get(r10);
                    AnonymousClass009.A05(obj);
                    if (r9.A03.equals(((C35211hS) obj).A02)) {
                        ((C35211hS) hashMap.get(r10)).A03.add(r9.A02);
                    }
                }
                HashSet hashSet = new HashSet();
                for (AbstractC14640lm r3 : hashMap.keySet()) {
                    if (this.A0G.A09(r3) != null) {
                        this.A0A.put(r3, (C35211hS) hashMap.get(r3));
                    } else {
                        hashSet.add(r3);
                    }
                }
                if (!hashSet.isEmpty()) {
                    r7.A04(hashSet, true);
                }
                HashSet hashSet2 = this.A0a;
                long currentTimeMillis = System.currentTimeMillis();
                HashSet hashSet3 = new HashSet();
                try {
                    C16310on A01 = r7.A00.get();
                    try {
                        Cursor A08 = A01.A03.A08("location_key_distribution", "sent_to_server = ?", null, null, new String[]{"jid"}, new String[]{"1"});
                        if (A08 == null) {
                            Log.e("LocationSharingStore/getAllLocationSharers/unable to read location key distribution table");
                            A01.close();
                        } else {
                            while (A08.moveToNext()) {
                                UserJid nullable = UserJid.getNullable(A08.getString(0));
                                if (nullable != null) {
                                    hashSet3.add(nullable);
                                }
                            }
                            A08.close();
                            A01.close();
                            StringBuilder sb = new StringBuilder("LocationSharingStore/getAllLocationReceiverHasKey/returned ");
                            sb.append(hashSet3.size());
                            sb.append(" location receivers has key | time: ");
                            sb.append(System.currentTimeMillis() - currentTimeMillis);
                            Log.i(sb.toString());
                        }
                        hashSet2.addAll(hashSet3);
                        HashSet hashSet4 = new HashSet(hashSet2);
                        hashSet4.removeAll(A0C());
                        if (!hashSet4.isEmpty()) {
                            A0H();
                        }
                        A0N();
                    } catch (Throwable th) {
                        try {
                            A01.close();
                        } catch (Throwable unused) {
                        }
                        throw th;
                    }
                } catch (Exception e) {
                    Log.e("LocationSharingStore/getAllLocationReceiverHasKey/error reading database", e);
                    throw new RuntimeException(e);
                }
            }
            map = this.A0A;
        }
        return map;
    }

    public final Set A0C() {
        Map A0B = A0B();
        HashSet hashSet = new HashSet();
        long A00 = this.A0K.A00();
        for (C35211hS r4 : A0B.values()) {
            if (A01(r4.A01, A00)) {
                hashSet.addAll(r4.A03);
            }
        }
        return hashSet;
    }

    public void A0D() {
        Log.i("LocationSharingManager/cancelShareLocation");
        synchronized (this.A0X) {
            Iterator it = new ArrayList(A0B().keySet()).iterator();
            while (it.hasNext()) {
                A0P((AbstractC14640lm) it.next());
            }
        }
    }

    public void A0E() {
        if (A0c()) {
            boolean z = true;
            Object obj = this.A0V;
            synchronized (obj) {
                if ((this.A00 & 1) != 1) {
                    z = false;
                }
            }
            if (!z) {
                Context context = this.A0L.A00;
                LocationSharingService.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.START_LOCATION_REPORTING").putExtra("duration", 40000L));
                synchronized (obj) {
                    this.A00 = 1 | this.A00;
                }
            }
        }
    }

    public void A0F() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.A0X) {
            Map A0B = A0B();
            A0B.size();
            long A00 = this.A0K.A00();
            for (Map.Entry entry : A0B.entrySet()) {
                long j = ((C35211hS) entry.getValue()).A01;
                if (j != 0 && j <= A00) {
                    arrayList.add((AbstractC14640lm) entry.getKey());
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            A0P((AbstractC14640lm) it.next());
        }
        A0N();
    }

    public void A0G() {
        Log.i("LocationSharingManager/onStopLocationReporting");
        synchronized (this.A0V) {
            this.A00 = -3 & this.A00;
        }
        Context context = this.A0L.A00;
        AnonymousClass1Tv.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.STOP_LOCATION_REPORTING"));
    }

    public void A0H() {
        Log.i("LocationSharingManager/removeMyLocationSenderKey");
        C15570nT r0 = this.A0C;
        r0.A08();
        C27481Hq r02 = r0.A04;
        AnonymousClass009.A05(r02);
        C15950oC A02 = C15940oB.A02(r02);
        synchronized (this.A0X) {
            C18240s8 r2 = this.A0O;
            r2.A00.execute(new RunnableBRunnable0Shape5S0200000_I0_5(this, 30, A02));
            this.A0a.clear();
            this.A0g.clear();
            try {
                C16310on A022 = this.A0S.A00.A02();
                try {
                    int A01 = A022.A03.A01("location_key_distribution", null, null);
                    A022.close();
                    StringBuilder sb = new StringBuilder("LocationSharingStore/deleteAllLocationReceiverHasKey/deleted ");
                    sb.append(A01);
                    sb.append(" rows");
                    Log.i(sb.toString());
                } catch (Throwable th) {
                    try {
                        A022.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Exception e) {
                Log.e("LocationSharingStore/deleteAllLocationReceiverHasKey/delete failed", e);
                throw new RuntimeException(e);
            }
        }
        AnonymousClass13T r03 = this.A0F;
        r03.A00.A01(new C32041bW());
    }

    public void A0I() {
        C30751Yr r5;
        int i;
        synchronized (this) {
            r5 = this.A07;
        }
        if (r5 != null) {
            if (A0c()) {
                synchronized (this.A0X) {
                    boolean z = false;
                    for (C35211hS r11 : A0B().values()) {
                        long j = r11.A01;
                        AnonymousClass1IS r6 = r11.A02;
                        C30341Xa A06 = A06(r6);
                        if (!(A06 == null || j == 0)) {
                            C30751Yr r9 = r11.A00;
                            if (r9 == null) {
                                r9 = A06.A02;
                                if (r9 != null) {
                                    r11.A00 = r9;
                                    z = true;
                                } else {
                                    long j2 = r5.A05;
                                    if (j >= j2 && j <= j2 + 240000) {
                                        r9 = new C30751Yr(r5.A06);
                                        r11.A00 = r9;
                                        r9.A00(r5);
                                        this.A0D.A00(new SendFinalLiveLocationNotificationJob(r6, r5, (int) ((r5.A05 - A06.A0I) / 1000)));
                                        z = true;
                                    }
                                }
                            }
                            if (r9.A05 + C26061Bw.A0L < j) {
                                long j3 = r5.A05;
                                if (j >= j3 && j <= j3 + C26061Bw.A0L) {
                                    r9.A00(r5);
                                    this.A0D.A00(new SendFinalLiveLocationNotificationJob(r6, r5, (int) ((r5.A05 - A06.A0I) / 1000)));
                                    z = true;
                                }
                            }
                        }
                    }
                    if (z) {
                        A0N();
                    }
                }
                if (!A0c()) {
                    synchronized (this.A0V) {
                        i = -2 & this.A00;
                        this.A00 = i;
                    }
                    if (i == 0) {
                        Context context = this.A0L.A00;
                        AnonymousClass1Tv.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.STOP_LOCATION_REPORTING"));
                    }
                }
            }
            C27081Fy A04 = this.A0R.A04(r5, null);
            long max = Math.max(0L, (this.A0K.A00() - r5.A05) / 1000);
            try {
                C18240s8 r0 = this.A0O;
                Object obj = r0.A00.submit(new Callable(A04) { // from class: X.1iq
                    public final /* synthetic */ C27081Fy A01;

                    {
                        this.A01 = r2;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        C16030oK r4 = C16030oK.this;
                        C27081Fy r3 = this.A01;
                        C15570nT r02 = r4.A0C;
                        r02.A08();
                        C27481Hq r03 = r02.A04;
                        AnonymousClass009.A05(r03);
                        return new C15930o9(r4.A0N.A0C(new C15980oF(C15940oB.A02(r03), C15960oD.A00.getRawString()), r3.A02()).A02, 2, 3);
                    }
                }).get();
                if (this.A0E.A06) {
                    StringBuilder sb = new StringBuilder("sendmethods/sendLocation elapsed=");
                    sb.append(max);
                    Log.i(sb.toString());
                    this.A0T.A08(Message.obtain(null, 0, 84, 0, Pair.create(Long.valueOf(max), obj)), false);
                }
            } catch (InterruptedException | ExecutionException e) {
                Log.e("LocationSharingManager/encryptAndSendLocation error", e);
            }
        } else {
            Log.w("LocationSharingManager/sendLatestLocation/try to send location, but no location available");
        }
    }

    public final void A0J() {
        Context context = this.A0L.A00;
        PendingIntent A01 = AnonymousClass1UY.A01(context, 0, new Intent(context, FinalLiveLocationBroadcastReceiver.class), 536870912);
        if (A01 != null) {
            AlarmManager A04 = this.A0J.A04();
            if (A04 != null) {
                A04.cancel(A01);
            } else {
                Log.w("LocationSharingManager/cancelFinalLiveLocationUpdateAlarm/AlarmManager is null");
            }
            A01.cancel();
        }
    }

    public final void A0K() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.A0W) {
            Map A0A = A0A();
            A0A.size();
            long A00 = this.A0K.A00();
            for (Map.Entry entry : A0A.entrySet()) {
                for (C35221hT r5 : ((Map) entry.getValue()).values()) {
                    long j = r5.A00;
                    if (j != 0 && j <= A00) {
                        arrayList.add(Pair.create((AbstractC14640lm) entry.getKey(), r5.A01));
                    }
                }
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            A0Q((AbstractC14640lm) pair.first, (UserJid) pair.second);
        }
        A0M();
    }

    public final void A0L() {
        synchronized (this.A0X) {
            Set A0C = A0C();
            HashSet hashSet = new HashSet(this.A0a);
            hashSet.removeAll(A0C);
            if (!hashSet.isEmpty()) {
                A0H();
            }
        }
    }

    public final void A0M() {
        Long l;
        Handler handler = this.A0B;
        Runnable runnable = this.A0Y;
        handler.removeCallbacks(runnable);
        synchronized (this.A0W) {
            l = null;
            for (Map map : A0A().values()) {
                for (C35221hT r7 : map.values()) {
                    if (l == null || r7.A00 < l.longValue()) {
                        l = Long.valueOf(r7.A00);
                    }
                }
            }
        }
        if (l != null) {
            long A00 = this.A0K.A00();
            long longValue = l.longValue();
            if (longValue > A00) {
                handler.postDelayed(runnable, longValue - A00);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003c, code lost:
        if (r0 < r20.longValue()) goto L_0x003e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ba A[Catch: all -> 0x016b, TRY_ENTER, TryCatch #1 {, blocks: (B:4:0x0012, B:5:0x0026, B:7:0x002c, B:9:0x0034, B:11:0x003e, B:13:0x0046, B:15:0x004e, B:18:0x0056, B:20:0x005a, B:25:0x0066, B:27:0x006f, B:31:0x0079, B:33:0x0081, B:35:0x0089, B:36:0x008b, B:37:0x008e, B:38:0x00a5, B:62:0x0138, B:63:0x015b, B:45:0x00ba, B:48:0x00cc, B:49:0x00d4, B:50:0x00e5, B:51:0x00e8, B:52:0x00f3, B:54:0x0101, B:57:0x0109, B:58:0x010d, B:60:0x011a, B:61:0x0133), top: B:72:0x0012 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0101 A[Catch: all -> 0x016b, TryCatch #1 {, blocks: (B:4:0x0012, B:5:0x0026, B:7:0x002c, B:9:0x0034, B:11:0x003e, B:13:0x0046, B:15:0x004e, B:18:0x0056, B:20:0x005a, B:25:0x0066, B:27:0x006f, B:31:0x0079, B:33:0x0081, B:35:0x0089, B:36:0x008b, B:37:0x008e, B:38:0x00a5, B:62:0x0138, B:63:0x015b, B:45:0x00ba, B:48:0x00cc, B:49:0x00d4, B:50:0x00e5, B:51:0x00e8, B:52:0x00f3, B:54:0x0101, B:57:0x0109, B:58:0x010d, B:60:0x011a, B:61:0x0133), top: B:72:0x0012 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0105  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0N() {
        /*
        // Method dump skipped, instructions count: 368
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16030oK.A0N():void");
    }

    public void A0O(Location location) {
        C30751Yr A05 = this.A0R.A05(location);
        synchronized (this) {
            C30751Yr r0 = this.A07;
            if (r0 == null || A05.A05 > r0.A05) {
                this.A07 = A05;
            }
        }
    }

    public void A0P(AbstractC14640lm r7) {
        StringBuilder sb = new StringBuilder("LocationSharingManager/cancelShareLocation; jid=");
        sb.append(r7);
        Log.i(sb.toString());
        synchronized (this.A0X) {
            C35211hS r5 = (C35211hS) A0B().remove(r7);
            if (r5 != null) {
                C30341Xa A06 = A06(r5.A02);
                if (A06 != null) {
                    A0Z(A06);
                }
                this.A0S.A06(Collections.singletonList(r5), this.A0K.A00());
                Set A0C = A0C();
                Iterator it = r5.A03.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!A0C.contains((UserJid) it.next())) {
                            A0H();
                            break;
                        }
                    } else {
                        break;
                    }
                }
                long A02 = A02();
                for (AnonymousClass13Q r0 : this.A0b) {
                    r0.AWr(r7);
                }
                A0N();
                this.A0B.post(new RunnableBRunnable0Shape5S0200000_I0_5(this, 36, r7));
                if (!A0d()) {
                    A0G();
                }
                this.A0D.A00(new SendDisableLiveLocationJob(r7, A02));
            }
        }
    }

    public final void A0Q(AbstractC14640lm r7, UserJid userJid) {
        StringBuilder sb = new StringBuilder("LocationSharingManager/onReceiveStopSharing; jid=");
        sb.append(r7);
        sb.append("; participant=");
        sb.append(userJid);
        Log.i(sb.toString());
        synchronized (this.A0W) {
            Map A0A = A0A();
            Map map = (Map) A0A.get(r7);
            if (map != null) {
                UserJid userJid2 = userJid;
                if (userJid == null) {
                    userJid2 = r7;
                }
                A0V((C35221hT) map.remove(userJid2));
                AnonymousClass15N r2 = this.A0S;
                AnonymousClass009.A05(r7);
                if (userJid != null) {
                    r2.A02(r7, Collections.singletonList(userJid), false);
                } else {
                    r2.A04(Collections.singletonList(r7), false);
                }
                if (map.isEmpty()) {
                    A0A.remove(r7);
                }
            }
            A0b(A0A);
        }
        for (AnonymousClass13P r0 : this.A0c) {
            r0.AUi(r7, userJid);
        }
        A0M();
        this.A0B.post(new RunnableBRunnable0Shape5S0200000_I0_5(this, 31, r7));
    }

    public void A0R(AbstractC14640lm r4, String str) {
        StringBuilder sb = new StringBuilder("LocationSharingManager/cancelShareLocation; msgId=");
        sb.append(str);
        sb.append("; jid=");
        sb.append(r4);
        Log.i(sb.toString());
        synchronized (this.A0X) {
            if (((C35211hS) A0B().get(r4)) == null) {
                C30341Xa A06 = A06(new AnonymousClass1IS(r4, str, true));
                if (A06 != null) {
                    A0Z(A06);
                }
                return;
            }
            A0P(r4);
        }
    }

    public void A0S(AbstractC14640lm r9, List list) {
        boolean z;
        StringBuilder sb = new StringBuilder("LocationSharingManager/onParticipantsLeftGroup; gjid=");
        sb.append(r9);
        sb.append("; participants.size=");
        sb.append(list.size());
        Log.i(sb.toString());
        synchronized (this.A0X) {
            Map A0B = A0B();
            C35211hS r6 = (C35211hS) A0B.get(r9);
            z = false;
            if (r6 != null) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    UserJid userJid = (UserJid) it.next();
                    List list2 = r6.A03;
                    if (list2.contains(userJid)) {
                        list2.remove(userJid);
                        z = true;
                    }
                }
                if (r6.A03.isEmpty()) {
                    C35211hS r0 = (C35211hS) A0B.remove(r9);
                    AnonymousClass009.A05(r0);
                    C30341Xa A06 = A06(r0.A02);
                    if (A06 != null) {
                        A0Z(A06);
                    }
                }
                this.A0S.A02(r9, list, true);
                A0L();
            }
        }
        if (z) {
            A0N();
            this.A0B.post(new RunnableBRunnable0Shape5S0200000_I0_5(this, 35, r9));
        }
        synchronized (this.A0W) {
            Iterator it2 = list.iterator();
            while (it2.hasNext()) {
                A0Q(r9, (UserJid) it2.next());
            }
        }
    }

    public void A0T(C15580nU r3) {
        Map map;
        StringBuilder sb = new StringBuilder("LocationSharingManager/onMeLeftGroup; gjid=");
        sb.append(r3);
        Log.i(sb.toString());
        A0P(r3);
        synchronized (this.A0W) {
            map = (Map) A0A().remove(r3);
        }
        if (map != null) {
            for (C35221hT r0 : map.values()) {
                A0Q(r3, r0.A01);
            }
        }
    }

    public void A0U(UserJid userJid, byte[] bArr, int i) {
        if (i > 4) {
            StringBuilder sb = new StringBuilder("LocationSharingManager/sendLocationKeyRetryRequest/reached max retry; remote_resource=");
            sb.append(userJid);
            sb.append("; retryCount=");
            sb.append(i);
            Log.w(sb.toString());
            return;
        }
        synchronized (this.A0W) {
            boolean z = false;
            for (Map.Entry entry : A0A().entrySet()) {
                Iterator it = ((Map) entry.getValue()).keySet().iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (userJid.equals((UserJid) it.next())) {
                            z = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (!z) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("LocationSharingManager/sendLocationKeyRetryRequest/should not receive location updates from this user; jid=");
                sb2.append(userJid);
                Log.i(sb2.toString());
                return;
            }
            Map map = this.A0d;
            synchronized (map) {
                long A00 = this.A0K.A00();
                Pair pair = (Pair) map.get(userJid);
                if (pair != null) {
                    long longValue = A00 - ((Long) pair.first).longValue();
                    if (longValue < 60000 && ((Integer) pair.second).intValue() >= i) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("LocationSharingManager/sendLocationKeyRetryRequest/retry too soon; remote_resource=");
                        sb3.append(userJid);
                        sb3.append("; timeElapsed=");
                        sb3.append(longValue);
                        Log.i(sb3.toString());
                        return;
                    }
                }
                StringBuilder sb4 = new StringBuilder();
                sb4.append("LocationSharingManager/sendLocationKeyRetryRequest/send; remote_resource=");
                sb4.append(userJid);
                sb4.append("; retryCount=");
                sb4.append(i);
                Log.i(sb4.toString());
                map.put(userJid, Pair.create(Long.valueOf(A00), Integer.valueOf(i)));
                C17220qS r3 = this.A0T;
                Message obtain = Message.obtain(null, 0, 125, 0);
                obtain.getData().putParcelable("jid", userJid);
                obtain.getData().putByteArray("registrationId", bArr);
                obtain.getData().putInt("retryCount", i);
                r3.A06(obtain);
            }
        }
    }

    public final void A0V(C35221hT r2) {
        C30341Xa A06;
        if (r2 != null && (A06 = A06(r2.A02)) != null) {
            A0Z(A06);
        }
    }

    public void A0W(AnonymousClass13Q r3) {
        List list = this.A0b;
        if (!list.contains(r3)) {
            list.add(r3);
        }
    }

    public void A0X(AnonymousClass13P r3) {
        List list = this.A0c;
        if (!list.contains(r3)) {
            list.add(r3);
        }
    }

    public void A0Y(C30751Yr r7, C30341Xa r8) {
        UserJid of;
        AnonymousClass1IS r1 = r8.A0z;
        AbstractC14640lm r5 = r1.A00;
        boolean z = r1.A02;
        if (z) {
            of = null;
        } else if (C15380n4.A0J(r5)) {
            of = UserJid.of(r8.A0B());
        } else {
            of = UserJid.of(r5);
        }
        String str = r1.A01;
        StringBuilder sb = new StringBuilder("LocationSharingManager/storeFinalLiveLocation/jid=");
        sb.append(r5);
        sb.append("; fromMe=");
        sb.append(z);
        sb.append("; msgId=");
        sb.append(str);
        sb.append("; participant=");
        sb.append(of);
        sb.append("; location.time=");
        sb.append(r7.A05);
        Log.i(sb.toString());
        r8.A02 = r7;
        ((C15650ng) this.A0i.get()).A0a(r8, 18);
    }

    public final void A0Z(C30341Xa r5) {
        int A00 = (int) ((this.A0K.A00() - r5.A0I) / 1000);
        if (A00 < r5.A00) {
            r5.A00 = A00;
            if (((AnonymousClass1XP) r5).A02 == 1) {
                ((AnonymousClass1XP) r5).A02 = 0;
            }
            ((C15650ng) this.A0i.get()).A0a(r5, 19);
        }
    }

    public void A0a(C30341Xa r20, long j) {
        UserJid of;
        StringBuilder sb = new StringBuilder("LocationSharingManager/onReceiveSharing; message.key.remote_jid=");
        AnonymousClass1IS r7 = r20.A0z;
        AbstractC14640lm r14 = r7.A00;
        sb.append(r14);
        sb.append("; message.remote_resource=");
        sb.append(r20.A0B());
        sb.append("; expiration=");
        sb.append(j);
        sb.append("; message.sequenceNumber=");
        sb.append(r20.A01);
        Log.i(sb.toString());
        AbstractC14640lm A0B = r20.A0B();
        if (A0B == null) {
            of = UserJid.of(r14);
        } else {
            of = UserJid.of(A0B);
        }
        synchronized (this.A0W) {
            Map A0A = A0A();
            Pair create = Pair.create(r14, of);
            Map map = this.A0f;
            Long l = (Long) map.get(create);
            if (l == null || l.longValue() < r20.A01) {
                map.remove(create);
                if (!A0A.containsKey(r14)) {
                    A0A.put(r14, new HashMap());
                }
                Map map2 = (Map) A0A.get(r14);
                AnonymousClass009.A05(map2);
                A0V((C35221hT) map2.get(of));
                AnonymousClass009.A05(of);
                map2.put(of, new C35221hT(of, r7, j));
                Map map3 = this.A0h;
                if (!map3.containsKey(of)) {
                    AnonymousClass009.A05(of);
                    map3.put(of, new C30751Yr(of));
                }
                C30751Yr r8 = (C30751Yr) map3.get(of);
                AnonymousClass009.A05(r8);
                long j2 = r8.A05;
                long j3 = r20.A0I;
                if (j2 <= j3) {
                    r8.A00 = ((AnonymousClass1XP) r20).A00;
                    r8.A01 = ((AnonymousClass1XP) r20).A01;
                    r8.A05 = j3;
                    this.A0S.A03(r8);
                }
                AnonymousClass15N r3 = this.A0S;
                AnonymousClass009.A05(r14);
                AnonymousClass009.A05(of);
                r3.A07(Collections.singletonList(new C35801il(r14, of, new AnonymousClass1IS(r14, r7.A01, false), j)));
                A0Y(r8, r20);
                for (AnonymousClass13P r0 : this.A0c) {
                    r0.AUh(r14, of);
                }
                A0M();
                this.A0B.post(new RunnableBRunnable0Shape5S0200000_I0_5(this, 34, r14));
                return;
            }
            Log.i("LocationSharingManager/onReceiveSharing; received message with old sequence number; not set receiving");
        }
    }

    public final void A0b(Map map) {
        HashSet hashSet = new HashSet(this.A0h.keySet());
        for (Map map2 : map.values()) {
            for (C35221hT r0 : map2.values()) {
                hashSet.remove(r0.A01);
            }
        }
        if (!hashSet.isEmpty()) {
            this.A0S.A05(hashSet);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (r3 <= r7) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0024, code lost:
        if (r6 != null) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0c() {
        /*
            r9 = this;
            X.0m7 r0 = r9.A0K
            long r7 = r0.A00()
            java.lang.Object r5 = r9.A0X
            monitor-enter(r5)
            java.lang.Long r6 = r9.A08     // Catch: all -> 0x003d
            monitor-exit(r5)     // Catch: all -> 0x003d
            if (r6 == 0) goto L_0x001c
            long r3 = r6.longValue()
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0026
            int r0 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r0 > 0) goto L_0x0026
        L_0x001c:
            java.lang.String r0 = "LocationSharingManager/hasExpiringLocationReceivers/triggered clearing"
            com.whatsapp.util.Log.i(r0)
            r9.A0F()
            if (r6 == 0) goto L_0x003b
        L_0x0026:
            long r1 = r6.longValue()
            int r0 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r0 < 0) goto L_0x003b
            monitor-enter(r5)
            long r2 = r9.A03     // Catch: all -> 0x0033
            monitor-exit(r5)     // Catch: all -> 0x0033
            goto L_0x0036
        L_0x0033:
            r0 = move-exception
            monitor-exit(r5)     // Catch: all -> 0x0033
            throw r0
        L_0x0036:
            int r1 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            r0 = 1
            if (r1 <= 0) goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            return r0
        L_0x003d:
            r0 = move-exception
            monitor-exit(r5)     // Catch: all -> 0x003d
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16030oK.A0c():boolean");
    }

    public boolean A0d() {
        boolean z;
        synchronized (this.A0X) {
            Map A0B = A0B();
            long A00 = this.A0K.A00();
            Iterator it = A0B.values().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (A01(((C35211hS) it.next()).A01, A00)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        }
        return z;
    }

    public boolean A0e() {
        synchronized (this.A0X) {
            List<UserJid> A08 = A08();
            Map map = this.A0g;
            A08.removeAll(map.keySet());
            if (A08.isEmpty()) {
                return false;
            }
            for (UserJid userJid : A08) {
                map.put(userJid, 0);
            }
            this.A0D.A00(new SendLiveLocationKeyJob(A08));
            return true;
        }
    }

    public boolean A0f(AbstractC14640lm r6) {
        synchronized (this.A0X) {
            C35211hS r0 = (C35211hS) A0B().get(r6);
            if (r0 != null) {
                if (A01(r0.A01, this.A0K.A00())) {
                    return true;
                }
                A0P(r6);
            }
            return false;
        }
    }

    public boolean A0g(UserJid userJid, int i) {
        if (i > 4) {
            StringBuilder sb = new StringBuilder("LocationSharingManager/shouldUserGetLocationKeyRetry/reached max retry; remote_resource=");
            sb.append(userJid);
            sb.append("; retryCount=");
            sb.append(i);
            Log.w(sb.toString());
            return false;
        }
        synchronized (this.A0X) {
            if (A0C().contains(userJid)) {
                long A00 = this.A0K.A00();
                Pair pair = (Pair) this.A0e.get(userJid);
                if (pair != null) {
                    long longValue = A00 - ((Long) pair.first).longValue();
                    if (longValue < 60000 && ((Integer) pair.second).intValue() >= i) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("LocationSharingManager/shouldUserGetLocationKeyRetry/retry too soon; remote_resource=");
                        sb2.append(userJid);
                        sb2.append("; timeElapsed=");
                        sb2.append(longValue);
                        Log.i(sb2.toString());
                    }
                }
                return true;
            }
            return false;
        }
    }

    public boolean A0h(C30751Yr r13) {
        boolean z;
        C30341Xa A06;
        synchronized (this.A0W) {
            Map map = this.A0h;
            UserJid userJid = r13.A06;
            C30751Yr r4 = (C30751Yr) map.get(userJid);
            z = false;
            if (r4 == null || r4.A05 <= r13.A05) {
                for (Map map2 : A0A().values()) {
                    C35221hT r9 = (C35221hT) map2.get(userJid);
                    if (!(r9 == null || r13.A05 > r9.A00 || (A06 = A06(r9.A02)) == null)) {
                        A0Y(r13, A06);
                        z = true;
                    }
                }
                if (z) {
                    if (r4 == null) {
                        map.put(userJid, r13);
                    } else {
                        r4.A00(r13);
                    }
                    this.A0S.A03(r13);
                }
            }
        }
        return z;
    }
}
