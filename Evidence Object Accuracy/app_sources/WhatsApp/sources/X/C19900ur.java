package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.Random;

/* renamed from: X.0ur  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19900ur extends AbstractC18220s6 {
    public final C18230s7 A00;
    public final C14830m7 A01;
    public final C14820m6 A02;
    public final C19890uq A03;
    public final Random A04;

    public C19900ur(Context context, C18230s7 r2, C14830m7 r3, C14820m6 r4, C19890uq r5, Random random) {
        super(context);
        this.A01 = r3;
        this.A04 = random;
        this.A00 = r2;
        this.A03 = r5;
        this.A02 = r4;
    }

    public final void A02() {
        long A00 = this.A01.A00();
        SharedPreferences sharedPreferences = this.A02.A00;
        if (!sharedPreferences.contains("last_heartbeat_login")) {
            long nextInt = A00 - (((long) this.A04.nextInt(86400)) * 1000);
            sharedPreferences.edit().putLong("last_heartbeat_login", nextInt).apply();
            StringBuilder sb = new StringBuilder("no last heartbeat known; setting to ");
            sb.append(C38121nY.A02(nextInt));
            Log.i(sb.toString());
        }
        long j = sharedPreferences.getLong("last_heartbeat_login", 0);
        if (j <= A00) {
            long j2 = 86400000 + j;
            if (j2 >= A00) {
                long elapsedRealtime = (j2 - A00) + SystemClock.elapsedRealtime();
                StringBuilder sb2 = new StringBuilder("HeartbeatWakeupAction; elapsedRealTimeHeartbeatLogin=");
                sb2.append(C38121nY.A02(elapsedRealtime));
                Log.i(sb2.toString());
                if (!this.A00.A02(A00("com.whatsapp.action.HEARTBEAT_WAKEUP", 134217728), 2, elapsedRealtime)) {
                    Log.w("HeartbeatWakeupAction; AlarmManager is null");
                    return;
                }
                return;
            }
        }
        StringBuilder sb3 = new StringBuilder("HeartbeatWakeupAction/last heart beat login=");
        sb3.append(j);
        sb3.append(" server time=");
        sb3.append(A00);
        sb3.append(" client time=");
        sb3.append(System.currentTimeMillis());
        sb3.append(" interval=");
        sb3.append(86400);
        Log.i(sb3.toString());
        A03(null);
    }

    public final void A03(Intent intent) {
        StringBuilder sb = new StringBuilder("HeartbeatWakeupAction; intent=");
        sb.append(intent);
        Log.i(sb.toString());
        long A00 = this.A01.A00();
        this.A03.A0C(0, false, true, true, true);
        StringBuilder sb2 = new StringBuilder("HeartbeatWakeupAction/setting last heart beat login time: ");
        sb2.append(A00);
        Log.i(sb2.toString());
        this.A02.A00.edit().putLong("last_heartbeat_login", A00).apply();
        A02();
    }
}
