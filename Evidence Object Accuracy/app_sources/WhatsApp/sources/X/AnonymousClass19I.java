package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.19I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19I {
    public final C64533Fx A00;
    public final C64533Fx A01;
    public final C64533Fx A02;
    public final AnonymousClass19H A03;

    public AnonymousClass19I(C16590pI r2, AnonymousClass018 r3, AnonymousClass19H r4) {
        this.A03 = r4;
        this.A00 = new AnonymousClass42C(r2, r3);
        this.A01 = new C64533Fx(r2, r3);
        this.A02 = new AnonymousClass42B(r2, r3);
    }

    public void A00(Context context) {
        this.A00.A00.setColor(AnonymousClass00T.A00(context, R.color.conversationRowSelectionColor));
        this.A01.A00.setColor(AnonymousClass00T.A00(context, R.color.conversationRowSelectionColor));
        this.A02.A00.setColor(AnonymousClass00T.A00(context, R.color.conversationRowSelectionColor));
    }
}
