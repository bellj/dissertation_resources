package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.stickers.StickerStoreMyTabFragment;
import java.util.List;

/* renamed from: X.2VT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VT extends C38671oW {
    public final /* synthetic */ StickerStoreMyTabFragment A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2VT(StickerStoreMyTabFragment stickerStoreMyTabFragment, List list) {
        super(stickerStoreMyTabFragment, list);
        this.A00 = stickerStoreMyTabFragment;
    }

    @Override // X.C38671oW, X.AnonymousClass02M
    public int A0D() {
        int A0D = super.A0D();
        return A0D > 0 ? A0D + 1 : A0D;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0023, code lost:
        if (r4.A04.size() != 0) goto L_0x0025;
     */
    @Override // X.C38671oW, X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r10, int r11) {
        /*
            r9 = this;
            int r0 = r9.getItemViewType(r11)
            if (r0 != 0) goto L_0x00d2
            X.2hq r10 = (X.C55132hq) r10
            super.ANH(r10, r11)
            java.util.List r0 = r9.A00
            java.lang.Object r4 = r0.get(r11)
            X.1KZ r4 = (X.AnonymousClass1KZ) r4
            com.whatsapp.CircularProgressBar r2 = r10.A0H
            boolean r0 = r4.A0O
            r8 = 8
            r6 = 0
            if (r0 == 0) goto L_0x0025
            java.util.List r0 = r4.A04
            int r1 = r0.size()
            r0 = 0
            if (r1 == 0) goto L_0x0027
        L_0x0025:
            r0 = 8
        L_0x0027:
            r2.setVisibility(r0)
            android.widget.ImageView r2 = r10.A06
            r0 = 2131232764(0x7f0807fc, float:1.8081646E38)
            r2.setImageResource(r0)
            android.content.Context r1 = r2.getContext()
            r0 = 2131100322(0x7f0602a2, float:1.7813022E38)
            android.content.res.ColorStateList r0 = X.AnonymousClass00T.A03(r1, r0)
            X.C016307r.A00(r0, r2)
            android.widget.ImageView r5 = r10.A07
            r0 = 2131232768(0x7f080800, float:1.8081655E38)
            r5.setImageResource(r0)
            android.widget.ImageView r3 = r10.A08
            r0 = 2131232110(0x7f08056e, float:1.808032E38)
            r3.setImageResource(r0)
            boolean r0 = r4.A01()
            r7 = 4
            if (r0 == 0) goto L_0x00dc
            r2.setVisibility(r7)
            r5.setVisibility(r7)
            r3.setVisibility(r7)
            android.view.View r0 = r10.A04
            r0.setVisibility(r6)
            boolean r1 = r4.A05
            android.widget.TextView r0 = r10.A0E
            if (r1 == 0) goto L_0x00d3
            r0.setVisibility(r7)
            android.widget.ProgressBar r0 = r10.A0A
            r0.setVisibility(r6)
        L_0x0073:
            com.whatsapp.stickers.StickerStoreMyTabFragment r7 = r9.A00
            r0 = 2131892035(0x7f121743, float:1.9418807E38)
            java.lang.String r0 = r7.A0I(r0)
            r2.setContentDescription(r0)
            r1 = 32
            com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0 r0 = new com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0
            r0.<init>(r4, r1, r9)
            r2.setOnClickListener(r0)
            r0 = 2131892050(0x7f121752, float:1.9418837E38)
            java.lang.String r0 = r7.A0I(r0)
            r5.setContentDescription(r0)
            r0 = 1
            r5.setLongClickable(r0)
            r1 = 1
            com.facebook.redex.ViewOnClickCListenerShape0S0000000_I0 r0 = new com.facebook.redex.ViewOnClickCListenerShape0S0000000_I0
            r0.<init>(r1)
            r5.setOnClickListener(r0)
            X.3N8 r0 = new X.3N8
            r0.<init>(r10)
            r5.setOnTouchListener(r0)
            r4 = 2131886138(0x7f12003a, float:1.9406846E38)
            X.2VX[] r2 = new X.AnonymousClass2VX[r1]
            r1 = 32
            X.2VX r0 = new X.2VX
            r0.<init>(r1, r4)
            r2[r6] = r0
            X.2VY r0 = new X.2VY
            r0.<init>(r2)
            X.AnonymousClass028.A0g(r5, r0)
            r0 = 2131892032(0x7f121740, float:1.94188E38)
            java.lang.String r0 = r7.A0I(r0)
            r3.setContentDescription(r0)
            r1 = 47
            com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2 r0 = new com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2
            r0.<init>(r9, r1)
            r3.setOnClickListener(r0)
        L_0x00d2:
            return
        L_0x00d3:
            r0.setVisibility(r6)
            android.widget.ProgressBar r0 = r10.A0A
            r0.setVisibility(r7)
            goto L_0x0073
        L_0x00dc:
            android.view.View r0 = r10.A04
            r0.setVisibility(r7)
            java.lang.String r1 = r4.A0D
            java.lang.String r0 = "meta-avatar"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00f6
            r2.setVisibility(r7)
            r5.setVisibility(r8)
            r3.setVisibility(r6)
            goto L_0x0073
        L_0x00f6:
            r3.setVisibility(r8)
            r2.setVisibility(r6)
            r5.setVisibility(r6)
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2VT.ANH(X.03U, int):void");
    }

    @Override // X.C38671oW, X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i != 1) {
            return super.AOl(viewGroup, i);
        }
        StickerStoreMyTabFragment stickerStoreMyTabFragment = this.A00;
        return new C75363jj(LayoutInflater.from(stickerStoreMyTabFragment.A0p()).inflate(R.layout.sticker_store_my_drag_footer, viewGroup, false), stickerStoreMyTabFragment);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return A0D() - 1 != i ? 0 : 1;
    }
}
