package X;

import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;

/* renamed from: X.3Dp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63953Dp {
    public final int A00 = 32;
    public final String A01 = "SHA-256";

    public byte[] A00(byte[] bArr, byte[] bArr2) {
        String replace = this.A01.replace("-", "");
        try {
            Mac instance = Mac.getInstance(C12960it.A0d(replace, C12960it.A0j("Hmac")));
            C12990iw.A1S(C12960it.A0d(replace, C12960it.A0j("Hmac")), instance, bArr);
            return instance.doFinal(bArr2);
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] A01(byte[] bArr, byte[] bArr2, int i) {
        String replace = this.A01.replace("-", "");
        try {
            int ceil = (int) Math.ceil(((double) i) / ((double) this.A00));
            byte[] bArr3 = new byte[0];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (int i2 = 1; i2 < ceil + 1; i2++) {
                Mac instance = Mac.getInstance(C12960it.A0d(replace, C12960it.A0j("Hmac")));
                C12990iw.A1S(C12960it.A0d(replace, C12960it.A0j("Hmac")), instance, bArr);
                instance.update(bArr3);
                if (bArr2 != null) {
                    instance.update(bArr2);
                }
                instance.update((byte) i2);
                bArr3 = instance.doFinal();
                int min = Math.min(i, bArr3.length);
                byteArrayOutputStream.write(bArr3, 0, min);
                i -= min;
            }
            return byteArrayOutputStream.toByteArray();
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
