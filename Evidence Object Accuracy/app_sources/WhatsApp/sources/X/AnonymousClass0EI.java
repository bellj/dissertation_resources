package X;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0EI  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0EI extends AbstractC06480Tu {
    @Override // X.AbstractC06480Tu
    public Object A02(Object obj) {
        if (obj != null) {
            return ((Transition) obj).clone();
        }
        return null;
    }

    @Override // X.AbstractC06480Tu
    public Object A03(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition((Transition) obj);
        return transitionSet;
    }

    @Override // X.AbstractC06480Tu
    public Object A04(Object obj, Object obj2, Object obj3) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition == null) {
            transition = null;
            if (transition2 != null) {
                transition = transition2;
            }
        } else if (transition2 != null) {
            transition = new TransitionSet().addTransition(transition).addTransition(transition2).setOrdering(1);
        }
        if (transition3 == null) {
            return transition;
        }
        TransitionSet transitionSet = new TransitionSet();
        if (transition != null) {
            transitionSet.addTransition(transition);
        }
        transitionSet.addTransition(transition3);
        return transitionSet;
    }

    @Override // X.AbstractC06480Tu
    public Object A05(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.addTransition((Transition) obj);
        }
        transitionSet.addTransition((Transition) obj2);
        return transitionSet;
    }

    @Override // X.AbstractC06480Tu
    public void A06(Rect rect, Object obj) {
        ((Transition) obj).setEpicenterCallback(new C02140Ae(rect, this));
    }

    @Override // X.AbstractC06480Tu
    public void A07(View view, Object obj) {
        ((Transition) obj).addTarget(view);
    }

    @Override // X.AbstractC06480Tu
    public void A08(View view, Object obj) {
        if (view != null) {
            Rect rect = new Rect();
            AbstractC06480Tu.A00(view, rect);
            ((Transition) obj).setEpicenterCallback(new C02130Ad(rect, this));
        }
    }

    @Override // X.AbstractC06480Tu
    public void A09(View view, Object obj, ArrayList arrayList) {
        ((Transition) obj).addListener(new AnonymousClass0Vv(view, this, arrayList));
    }

    @Override // X.AbstractC06480Tu
    public void A0A(View view, Object obj, ArrayList arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> targets = transitionSet.getTargets();
        targets.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            AbstractC06480Tu.A01((View) arrayList.get(i), targets);
        }
        targets.add(view);
        arrayList.add(view);
        A0E(transitionSet, arrayList);
    }

    @Override // X.AbstractC06480Tu
    public void A0B(ViewGroup viewGroup, Object obj) {
        TransitionManager.beginDelayedTransition(viewGroup, (Transition) obj);
    }

    @Override // X.AbstractC06480Tu
    public void A0C(AnonymousClass02N r2, AnonymousClass01E r3, Object obj, Runnable runnable) {
        ((Transition) obj).addListener(new C06920Vu(this, runnable));
    }

    @Override // X.AbstractC06480Tu
    public void A0D(Object obj, Object obj2, Object obj3, Object obj4, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3) {
        ((Transition) obj).addListener(new AnonymousClass0Vw(this, obj2, obj4, arrayList, arrayList3));
    }

    @Override // X.AbstractC06480Tu
    public void A0E(Object obj, ArrayList arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            int i = 0;
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int transitionCount = transitionSet.getTransitionCount();
                while (i < transitionCount) {
                    A0E(transitionSet.getTransitionAt(i), arrayList);
                    i++;
                }
                return;
            }
            List<Integer> targetIds = transition.getTargetIds();
            if (targetIds == null || targetIds.isEmpty()) {
                List<String> targetNames = transition.getTargetNames();
                if (targetNames == null || targetNames.isEmpty()) {
                    List<Class> targetTypes = transition.getTargetTypes();
                    if (targetTypes == null || targetTypes.isEmpty()) {
                        List<View> targets = transition.getTargets();
                        if (targets == null || targets.isEmpty()) {
                            int size = arrayList.size();
                            while (i < size) {
                                transition.addTarget((View) arrayList.get(i));
                                i++;
                            }
                        }
                    }
                }
            }
        }
    }

    @Override // X.AbstractC06480Tu
    public void A0F(Object obj, ArrayList arrayList, ArrayList arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.getTargets().clear();
            transitionSet.getTargets().addAll(arrayList2);
            A0H(transitionSet, arrayList, arrayList2);
        }
    }

    @Override // X.AbstractC06480Tu
    public boolean A0G(Object obj) {
        return obj instanceof Transition;
    }

    public void A0H(Object obj, ArrayList arrayList, ArrayList arrayList2) {
        List<View> targets;
        Transition transition = (Transition) obj;
        int i = 0;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int transitionCount = transitionSet.getTransitionCount();
            while (i < transitionCount) {
                A0H(transitionSet.getTransitionAt(i), arrayList, arrayList2);
                i++;
            }
            return;
        }
        List<Integer> targetIds = transition.getTargetIds();
        if (targetIds == null || targetIds.isEmpty()) {
            List<String> targetNames = transition.getTargetNames();
            if (targetNames == null || targetNames.isEmpty()) {
                List<Class> targetTypes = transition.getTargetTypes();
                if ((targetTypes == null || targetTypes.isEmpty()) && (targets = transition.getTargets()) != null && targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
                    if (arrayList2 != null) {
                        int size = arrayList2.size();
                        while (i < size) {
                            transition.addTarget((View) arrayList2.get(i));
                            i++;
                        }
                    }
                    int size2 = arrayList.size();
                    while (true) {
                        size2--;
                        if (size2 >= 0) {
                            transition.removeTarget((View) arrayList.get(size2));
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }
}
