package X;

import android.os.Bundle;

/* renamed from: X.5ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118295ba extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C128355vy A01;

    public C118295ba(Bundle bundle, C128355vy r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123575nN.class)) {
            C128355vy r1 = this.A01;
            C14830m7 r2 = r1.A09;
            C14900mE r22 = r1.A00;
            C15570nT r23 = r1.A01;
            C16590pI r24 = r1.A0A;
            AbstractC14440lR r25 = r1.A0k;
            C15450nH r26 = r1.A02;
            C14850m9 r27 = r1.A0I;
            C241414j r28 = r1.A0H;
            C17220qS r29 = r1.A0J;
            AnonymousClass14X r210 = r1.A0i;
            C15550nR r211 = r1.A06;
            AnonymousClass01d r212 = r1.A08;
            AnonymousClass018 r213 = r1.A0C;
            C18590sh r214 = r1.A0h;
            C17070qD r215 = r1.A0W;
            C238013b r216 = r1.A05;
            C15650ng r217 = r1.A0D;
            AnonymousClass1AA r218 = r1.A04;
            C1310060v r219 = r1.A0g;
            AnonymousClass604 r220 = r1.A0f;
            C20300vX r221 = r1.A0E;
            AnonymousClass69E r222 = r1.A0c;
            C22980zx r223 = r1.A0j;
            C21860y6 r224 = r1.A0P;
            C1308460e r225 = r1.A0M;
            C18610sj r226 = r1.A0T;
            C22710zW r15 = r1.A0V;
            AnonymousClass102 r14 = r1.A0G;
            C14650lo r13 = r1.A03;
            C129925yW r12 = r1.A0K;
            AnonymousClass17Z r11 = r1.A0a;
            C20370ve r10 = r1.A0F;
            AbstractC16870pt r9 = r1.A0Y;
            AnonymousClass68Z r8 = r1.A0L;
            C1329668y r7 = r1.A0N;
            C18650sn r6 = r1.A0Q;
            AnonymousClass1A7 r5 = r1.A0X;
            C243515e r4 = r1.A0S;
            C20380vf r3 = r1.A0O;
            C121265hX r227 = r1.A0e;
            return new C123575nN(this.A00, r22, r23, r26, r13, r218, r216, r211, r212, r2, r24, r213, r217, r221, r10, r14, r28, r27, r29, r12, r8, r225, r7, r3, r224, r6, r4, r226, r15, r215, r5, r9, r1.A0Z, r11, r222, r227, r220, r219, r214, r210, r223, r25);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
