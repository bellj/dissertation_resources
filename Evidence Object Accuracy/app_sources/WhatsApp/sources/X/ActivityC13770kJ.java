package X;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;

/* renamed from: X.0kJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ActivityC13770kJ extends AbstractActivityC13780kK {
    public ListAdapter A00;
    public ListView A01;
    public boolean A02 = false;
    public final Handler A03 = new Handler(Looper.getMainLooper());
    public final Runnable A04 = new RunnableBRunnable0Shape1S0100000_I0_1(this, 33);

    public static void A0M(AbstractActivityC36551k4 r1, AnonymousClass01J r2, C249317l r3) {
        ((ActivityC13790kL) r1).A08 = r3;
        r1.A0G = (AnonymousClass12U) r2.AJd.get();
        r1.A0D = (C21270x9) r2.A4A.get();
        r1.A09 = (C15550nR) r2.A45.get();
        r1.A0B = (C15610nY) r2.AMe.get();
        r1.A06 = (C238013b) r2.A1Z.get();
        r1.A0F = (AnonymousClass12F) r2.AJM.get();
        r1.A07 = (C22330yu) r2.A3I.get();
        r1.A08 = (AnonymousClass116) r2.A3z.get();
        r1.A0E = (C244215l) r2.A8y.get();
        r1.A0A = (AnonymousClass10S) r2.A46.get();
    }

    public static void A0N(AnonymousClass01J r1, AbstractActivityC36611kC r2) {
        r2.A0N = (C21270x9) r1.A4A.get();
        r2.A0J = (C15550nR) r1.A45.get();
        r2.A0L = (C15610nY) r1.AMe.get();
        r2.A0F = (C238013b) r1.A1Z.get();
        r2.A0K = (AnonymousClass10S) r1.A46.get();
        r2.A0U = (AnonymousClass12F) r1.AJM.get();
        r2.A0I = (AnonymousClass116) r1.A3z.get();
        r2.A0S = (AnonymousClass018) r1.ANb.get();
        r2.A0G = (C22330yu) r1.A3I.get();
        r2.A0T = (C244215l) r1.A8y.get();
        r2.A0R = (AnonymousClass118) r1.A42.get();
    }

    public static void A0O(AnonymousClass01J r1, AbstractActivityC36611kC r2, C249317l r3) {
        ((ActivityC13790kL) r2).A08 = r3;
        r2.A0B = (AnonymousClass1AR) r1.ALM.get();
        r2.A0C = (C16170oZ) r1.AM4.get();
    }

    @Override // X.ActivityC000800j
    public void A1Z() {
        View findViewById = findViewById(16908292);
        ListView listView = (ListView) findViewById(16908298);
        this.A01 = listView;
        if (listView != null) {
            if (findViewById != null) {
                listView.setEmptyView(findViewById);
            }
            if (this.A02) {
                A2f(this.A00);
            }
            this.A03.post(this.A04);
            this.A02 = true;
            return;
        }
        throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
    }

    public ListView A2e() {
        if (this.A01 == null) {
            setContentView(17367060);
        }
        ListView listView = this.A01;
        AnonymousClass009.A03(listView);
        return listView;
    }

    public void A2f(ListAdapter listAdapter) {
        synchronized (this) {
            if (this.A01 == null) {
                setContentView(17367060);
            }
            this.A00 = listAdapter;
            this.A01.setAdapter(listAdapter);
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        this.A03.removeCallbacks(this.A04);
        super.onDestroy();
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        if (this.A01 == null) {
            setContentView(17367060);
        }
        super.onRestoreInstanceState(bundle);
    }
}
