package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3D7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3D7 {
    public int A00;
    public int A01 = -1;
    public final C36161jQ A02;
    public final String A03;
    public final /* synthetic */ AnonymousClass2VG A04;

    public AnonymousClass3D7(AnonymousClass2VG r2, String str, List list, int i) {
        this.A04 = r2;
        this.A00 = i;
        this.A03 = str;
        this.A02 = new C36161jQ(list);
    }

    public boolean A00(AnonymousClass2VH r9) {
        C36161jQ r2 = this.A02;
        int indexOf = C12980iv.A0z(r2).indexOf(r9);
        if (!C12980iv.A0z(r2).remove(r9)) {
            return false;
        }
        this.A01 = indexOf;
        r2.A0B(r2.A01());
        AnonymousClass2VG r7 = this.A04;
        if (C12980iv.A0z(r2).size() != 0) {
            return true;
        }
        ArrayList A0l = C12960it.A0l();
        C36161jQ r5 = r7.A05;
        int i = 1;
        for (AnonymousClass3D7 r22 : C12980iv.A0z(r5)) {
            if (this.A03.equals(r22.A03)) {
                r7.A00 = i;
            } else {
                r22.A00 = i;
                A0l.add(r22);
                i++;
            }
        }
        r5.A0B(A0l);
        return true;
    }
}
