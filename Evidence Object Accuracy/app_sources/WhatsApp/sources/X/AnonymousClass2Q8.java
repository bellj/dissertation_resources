package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.location.LocationSharingService;

/* renamed from: X.2Q8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Q8 extends C36261ja {
    public final /* synthetic */ C36261ja A00;
    public final /* synthetic */ AnonymousClass13O A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2Q8(AbstractC14640lm r1, C36261ja r2, AnonymousClass13O r3, boolean z) {
        super(r1, z);
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.C36261ja
    public void A01(int i) {
        this.A00.A01(i);
    }

    @Override // X.C36261ja
    public void A02(int i) {
        super.A02(i);
        C36261ja r0 = this.A00;
        r0.A02(i);
        AnonymousClass13O r2 = this.A01;
        if (r2.A04.A0f(r0.A00)) {
            Context context = r2.A02.A00;
            LocationSharingService.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.ACTION_START_LOCATION_UPDATES_FOR_WEB").putExtra("duration", (long) i));
        }
    }
}
