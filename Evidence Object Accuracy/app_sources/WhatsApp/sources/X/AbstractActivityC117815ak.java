package X;

import java.util.AbstractList;
import org.json.JSONObject;
import org.npci.commonlibrary.GetCredential;
import org.npci.commonlibrary.NPCIFragment;

/* renamed from: X.5ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC117815ak extends ActivityC000800j implements AnonymousClass004 {
    public boolean A00 = false;
    public final Object A01 = C12970iu.A0l();
    public volatile AnonymousClass2FH A02;

    public AbstractActivityC117815ak() {
        C117295Zj.A0p(this, 122);
    }

    public static String A02(String str, GetCredential getCredential, int i) {
        return ((JSONObject) getCredential.A07.get(i)).optString(str, "");
    }

    public static String A03(AbstractList abstractList, NPCIFragment nPCIFragment, int i) {
        nPCIFragment.A09.put("credential", ((AbstractC136546My) abstractList.get(i)).getInputValue());
        return ((GetCredential) nPCIFragment.A01).A0A.A04.A00(nPCIFragment.A09);
    }

    public static C128835wk A09(NPCIFragment nPCIFragment) {
        C128045vT r3 = ((GetCredential) nPCIFragment.A01).A0A;
        C128835wk r0 = r3.A01;
        if (r0 != null) {
            return r0;
        }
        C129045x5 r02 = r3.A05;
        C130495zV r2 = r02.A03;
        r3.A00 = r2;
        C128835wk r03 = new C128835wk(r02.A00, r2);
        r3.A01 = r03;
        return r03;
    }

    @Override // X.ActivityC001000l, X.AbstractC001800t
    public AbstractC009404s ACV() {
        return C48572Gu.A00(this, super.ACV());
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A02 == null) {
            synchronized (this.A01) {
                if (this.A02 == null) {
                    this.A02 = new AnonymousClass2FH(this);
                }
            }
        }
        return this.A02.generatedComponent();
    }
}
