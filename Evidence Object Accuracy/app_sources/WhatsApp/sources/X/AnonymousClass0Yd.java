package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0Yd  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Yd implements AnonymousClass02B {
    public final /* synthetic */ BiometricFragment A00;

    public AnonymousClass0Yd(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        CharSequence charSequence = (CharSequence) obj;
        if (charSequence != null) {
            BiometricFragment biometricFragment = this.A00;
            if (biometricFragment.A1K()) {
                biometricFragment.A01.A04(2);
                biometricFragment.A01.A05(charSequence);
            }
            AnonymousClass0EP r2 = biometricFragment.A01;
            AnonymousClass016 r0 = r2.A08;
            if (r0 == null) {
                r0 = new AnonymousClass016();
                r2.A08 = r0;
            }
            AnonymousClass0EP.A00(r0, null);
        }
    }
}
