package X;

import android.content.SharedPreferences;
import android.os.Message;
import com.facebook.redex.RunnableBRunnable0Shape13S0100000_I0_13;
import com.whatsapp.util.Log;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.0mG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14920mG {
    public static final long A0D = TimeUnit.SECONDS.toMillis(35);
    public static final long[] A0E;
    public SharedPreferences A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C20670w8 A03;
    public final C18640sm A04;
    public final C14830m7 A05;
    public final AnonymousClass122 A06;
    public final AnonymousClass124 A07;
    public final C20660w7 A08;
    public final C16630pM A09;
    public final AbstractC14440lR A0A;
    public final List A0B = new LinkedList();
    public volatile String A0C;

    static {
        TimeUnit timeUnit = TimeUnit.HOURS;
        TimeUnit timeUnit2 = TimeUnit.DAYS;
        A0E = new long[]{timeUnit.toMillis(6), timeUnit.toMillis(12), timeUnit2.toMillis(1), timeUnit2.toMillis(1), timeUnit2.toMillis(3), timeUnit2.toMillis(7)};
    }

    public C14920mG(AbstractC15710nm r2, C14900mE r3, C20670w8 r4, C18640sm r5, C14830m7 r6, AnonymousClass122 r7, AnonymousClass124 r8, C20660w7 r9, C16630pM r10, AbstractC14440lR r11) {
        this.A05 = r6;
        this.A02 = r3;
        this.A01 = r2;
        this.A0A = r11;
        this.A08 = r9;
        this.A06 = r7;
        this.A03 = r4;
        this.A07 = r8;
        this.A09 = r10;
        this.A04 = r5;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A09.A01(AnonymousClass01V.A07);
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public String A01() {
        if (this.A0C == null) {
            synchronized (this) {
                if (this.A0C == null) {
                    String string = A00().getString("two_factor_auth_code", null);
                    String str = "";
                    if (string == null) {
                        this.A0C = str;
                    } else {
                        boolean z = A00().getBoolean("two_factor_auth_using_encryption", false);
                        if (!z || (string = A02(string)) != null) {
                            str = string;
                        } else {
                            this.A01.AaV("TwoFactorAuthManager/loadCodeInMemory/EncryptedCodeFailure", null, false);
                        }
                        this.A0C = str;
                        StringBuilder sb = new StringBuilder("TwoFactorAuthManager/loadCodeInMemory/isUsingEncryption: ");
                        sb.append(z);
                        Log.i(sb.toString());
                    }
                }
            }
        }
        String str2 = this.A0C;
        AnonymousClass009.A05(str2);
        return str2;
    }

    public final String A02(String str) {
        byte[] A01;
        try {
            C31091Zz A00 = AnonymousClass122.A00(new JSONArray(str));
            if (A00 == null || (A01 = this.A07.A01(A00, AnonymousClass029.A0M)) == null) {
                return null;
            }
            return new String(A01, AnonymousClass01V.A0A);
        } catch (JSONException e) {
            Log.w("TwoFactorAuthManager/decryptCode/cannot create Json", e);
            return null;
        }
    }

    public void A03(String str, String str2) {
        if (!this.A04.A0B()) {
            Log.i("twofactorauthmanager/set-two-factor-auth-settings no internet connection, cancelling");
            this.A02.A0I(new RunnableBRunnable0Shape13S0100000_I0_13(this, 4));
            return;
        }
        Log.i("twofactorauthmanager/set-two-factor-auth-settings");
        C20660w7 r1 = this.A08;
        if (r1.A01.A06) {
            Log.i("sendmethods/send-set-two-factor-auth");
            C17220qS r3 = r1.A06;
            Message obtain = Message.obtain(null, 0, 111, 0);
            obtain.getData().putString("code", str);
            obtain.getData().putString("email", str2);
            r3.A08(obtain, false);
        }
    }

    public void A04(boolean z) {
        int max;
        int i = A00().getInt("two_factor_auth_nag_interval", 0);
        if (z) {
            max = Math.min(i + 1, 5);
        } else {
            max = Math.max(i - 1, 0);
        }
        A00().edit().putLong("two_factor_auth_nag_time", this.A05.A00()).putInt("two_factor_auth_nag_interval", max).putBoolean("two_factor_auth_last_code_correctness", z).apply();
    }
}
