package X;

/* renamed from: X.2S2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2S2 {
    public final /* synthetic */ AnonymousClass2S7 A00;
    public final /* synthetic */ AnonymousClass17Z A01;
    public final /* synthetic */ boolean A02;

    public AnonymousClass2S2(AnonymousClass2S7 r1, AnonymousClass17Z r2, boolean z) {
        this.A01 = r2;
        this.A02 = z;
        this.A00 = r1;
    }

    public void A00(C50942Ry r5) {
        if (!this.A02) {
            this.A00.AWz(this.A01.A00());
        } else if (r5 != null) {
            this.A01.A07(new AnonymousClass58D(this), r5.A08.A01);
        }
    }
}
