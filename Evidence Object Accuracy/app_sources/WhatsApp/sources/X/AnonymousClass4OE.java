package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.4OE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4OE {
    public final View A00;
    public final TextView A01;

    public AnonymousClass4OE(View view) {
        this.A00 = view;
        this.A01 = C12960it.A0I(view, R.id.qr_tab_title);
    }
}
