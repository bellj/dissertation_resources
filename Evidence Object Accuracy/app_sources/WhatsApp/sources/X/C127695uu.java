package X;

import java.math.BigDecimal;

/* renamed from: X.5uu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127695uu {
    public final int A00;
    public final AbstractC30791Yv A01;
    public final BigDecimal A02;
    public final boolean A03;

    public C127695uu(AbstractC30791Yv r1, BigDecimal bigDecimal, int i, boolean z) {
        this.A02 = bigDecimal;
        this.A00 = i;
        this.A03 = z;
        this.A01 = r1;
    }

    public static AnonymousClass6F2 A00(AbstractC30791Yv r1, AnonymousClass6F2 r2, BigDecimal bigDecimal, boolean z) {
        return r2.A05(new C127695uu(r1, bigDecimal, z ? 1 : 0, z));
    }

    public static AnonymousClass6F2 A01(AnonymousClass6F2 r2, AnonymousClass63Y r3, BigDecimal bigDecimal, int i, boolean z) {
        return r2.A05(new C127695uu(r3.A02, bigDecimal, i, z));
    }
}
