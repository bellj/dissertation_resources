package X;

/* renamed from: X.3F4  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3F4 {
    public final long A00;
    public final String A01;

    public AnonymousClass3F4(String str, long j) {
        this.A01 = str;
        this.A00 = j;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3F4.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3F4 r7 = (AnonymousClass3F4) obj;
            if (this.A00 != r7.A00 || !this.A01.equals(r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(Long.valueOf(this.A00), A1a);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("{ path = ");
        A0k.append(this.A01);
        A0k.append(", ");
        A0k.append("size");
        A0k.append(" = ");
        A0k.append(this.A00);
        return C12960it.A0d(" } \n", A0k);
    }
}
