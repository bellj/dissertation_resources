package X;

import android.text.Spannable;
import java.util.List;
import org.json.JSONException;

/* renamed from: X.3AP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3AP {
    public static void A00(C004602b r12, C14260l7 r13, AnonymousClass28D r14, AbstractC21000wf r15) {
        try {
            String A0I = r14.A0I(43);
            List A0M = r14.A0M(42);
            List A0M2 = r14.A0M(35);
            List A0M3 = r14.A0M(38);
            List A0M4 = r14.A0M(41);
            Spannable AD8 = r15.AD8(r12.getContext(), r13.A00, new AnonymousClass024(r13, r14) { // from class: X.3PE
                public final /* synthetic */ C14260l7 A00;
                public final /* synthetic */ AnonymousClass28D A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass024
                public final void accept(Object obj) {
                    AnonymousClass28D r3 = this.A01;
                    C14260l7 r2 = this.A00;
                    AbstractC14200l1 A0G = r3.A0G(40);
                    if (A0G != null) {
                        C28701Oq.A01(r2, r3, C14210l2.A02(obj), A0G);
                    }
                }
            }, A0I, A0M, A0M2, A0M3, A0M4);
            if (AD8 != null) {
                r12.setText(AD8);
            }
        } catch (JSONException e) {
            C28691Op.A00("WaTextWithEntitiesComponentBinder", C12960it.A0b("bind/exception parsing formatted string: ", e));
        }
        String A0I2 = r14.A0I(44);
        if (A0I2 != null) {
            try {
                r12.setGravity(AnonymousClass3JW.A07(A0I2));
            } catch (AnonymousClass491 e2) {
                C28691Op.A01("WaTextWithEntitiesComponentBinder", "Failed to parse textAlign", e2);
            }
        }
        String A0I3 = r14.A0I(45);
        if (A0I3 != null) {
            try {
                r12.setTextSize(AnonymousClass3JW.A03(A0I3));
            } catch (AnonymousClass491 e3) {
                C28691Op.A01("WaTextWithEntitiesComponentBinder", "Failed to parse text size", e3);
            }
        }
        String A0I4 = r14.A0I(36);
        if (A0I4 != null) {
            try {
                r12.setLineHeight((int) AnonymousClass3JW.A01(A0I4));
            } catch (AnonymousClass491 e4) {
                C28691Op.A01("WaTextWithEntitiesComponentBinder", "Failed to parse line height pixel value", e4);
            }
        }
    }
}
