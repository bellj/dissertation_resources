package X;

import com.whatsapp.jid.UserJid;
import java.util.List;

/* renamed from: X.3EQ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EQ {
    public final UserJid A00;
    public final String A01;
    public final String A02;
    public final List A03;

    public AnonymousClass3EQ(UserJid userJid, String str, String str2, List list) {
        this.A00 = userJid;
        this.A03 = list;
        this.A02 = str;
        this.A01 = str2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EQ.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EQ r5 = (AnonymousClass3EQ) obj;
            if (!this.A00.equals(r5.A00) || !this.A03.equals(r5.A03)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A00;
        return C12960it.A06(this.A03, A1a);
    }
}
