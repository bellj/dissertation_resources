package X;

import android.os.Build;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import com.whatsapp.R;
import com.whatsapp.search.views.TokenizedSearchInput;

/* renamed from: X.2ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53602ei extends AnonymousClass04v {
    public final /* synthetic */ TokenizedSearchInput A00;

    public C53602ei(TokenizedSearchInput tokenizedSearchInput) {
        this.A00 = tokenizedSearchInput;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        super.A01(view, accessibilityEvent);
        TokenizedSearchInput tokenizedSearchInput = this.A00;
        if (AnonymousClass23N.A05(tokenizedSearchInput.A09.A0P()) && accessibilityEvent.getEventType() == 1) {
            TokenizedSearchInput.A00(view, tokenizedSearchInput);
        }
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r6) {
        super.A06(view, r6);
        TokenizedSearchInput tokenizedSearchInput = this.A00;
        if (AnonymousClass23N.A05(tokenizedSearchInput.A09.A0P())) {
            if (Build.VERSION.SDK_INT < 23) {
                r6.A0F(tokenizedSearchInput.getContext().getString(R.string.accessibility_role_button));
            }
            String name = Button.class.getName();
            AccessibilityNodeInfo accessibilityNodeInfo = r6.A02;
            accessibilityNodeInfo.setClassName(name);
            accessibilityNodeInfo.setCheckable(false);
            accessibilityNodeInfo.setClickable(true);
            r6.A09(new C007804a(16, tokenizedSearchInput.getContext().getString(R.string.accessibility_action_click_remove)));
        }
    }
}
