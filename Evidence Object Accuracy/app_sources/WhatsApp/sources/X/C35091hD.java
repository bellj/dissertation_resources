package X;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.whatsapp.util.Log;

/* renamed from: X.1hD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35091hD extends AbstractC35031h7 {
    @Override // X.AbstractC35031h7
    public void A02(Context context, int i) {
        if (AnonymousClass01U.A00(context, "com.huawei.android.launcher") > 63006) {
            Bundle bundle = new Bundle();
            bundle.putString("package", context.getPackageName());
            bundle.putString("class", "com.whatsapp.Main");
            bundle.putInt("badgenumber", i);
            try {
                context.getContentResolver().call(Uri.parse("content://com.huawei.android.launcher.settings/badge/"), "change_badge", (String) null, bundle);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder("Unexpected exception, launcher version = ");
                sb.append(AnonymousClass01U.A00(context, "com.huawei.android.launcher"));
                Log.e(sb.toString(), e);
            }
        }
    }
}
