package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4ji  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99074ji implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        C78313oi[] r4 = null;
        long j = 0;
        int i = 0;
        boolean z = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                j = C95664e9.A04(parcel, readInt);
            } else if (c == 3) {
                r4 = (C78313oi[]) C95664e9.A0K(parcel, C78313oi.CREATOR, readInt);
            } else if (c != 4) {
                z = C95664e9.A0H(parcel, c, 5, readInt, z);
            } else {
                C95664e9.A0F(parcel, readInt, 4);
                i = parcel.readInt();
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78393oq(r4, i, j, z);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78393oq[i];
    }
}
