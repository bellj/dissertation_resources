package X;

import com.google.protobuf.CodedOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import org.chromium.net.UrlRequest;

/* renamed from: X.1Fz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC27091Fz extends AnonymousClass1G0 {
    public int A00 = -1;
    public AnonymousClass256 unknownFields = AnonymousClass256.A04;

    public static int A02(int i, int i2, int i3) {
        return i3 + CodedOutputStream.A04(i, i2);
    }

    public static int A03(int i, int i2, int i3) {
        return i3 + CodedOutputStream.A02(i, i2);
    }

    public static int A04(int i, String str, int i2) {
        return i2 + CodedOutputStream.A07(i, str);
    }

    public static int A05(AbstractC27881Jp r0, int i, int i2) {
        return i2 + CodedOutputStream.A09(r0, i);
    }

    public static int A06(CodedOutputStream codedOutputStream, List list, int i, int i2) {
        codedOutputStream.A0L((AnonymousClass1G1) list.get(i), i2);
        return i + 1;
    }

    public static int A07(AbstractC27091Fz r1, int i) {
        int A00 = i + r1.unknownFields.A00();
        r1.A00 = A00;
        return A00;
    }

    public static int A08(AnonymousClass1G1 r0, int i, int i2) {
        return i2 + CodedOutputStream.A0A(r0, i);
    }

    public static AnonymousClass255 A09(AbstractC27091Fz r1) {
        return new AnonymousClass255(r1);
    }

    public static AbstractC27091Fz A0A(AbstractC27881Jp r5, AbstractC27091Fz r6) {
        AnonymousClass254 A00 = AnonymousClass259.A00();
        try {
            C27861Jn r52 = (C27861Jn) r5;
            byte[] bArr = r52.bytes;
            int A05 = r52.A05();
            int A03 = r52.A03();
            AnonymousClass253 r2 = new AnonymousClass253(bArr, A05, A03);
            try {
                r2.A04(A03);
                AbstractC27091Fz A0B = A0B(r2, A00, r6);
                try {
                    r2.A0C(0);
                    A0P(A0B);
                    A0P(A0B);
                    return A0B;
                } catch (C28971Pt e) {
                    e.unfinishedMessage = A0B;
                    throw e;
                }
            } catch (C28971Pt e2) {
                throw new IllegalArgumentException(e2);
            }
        } catch (C28971Pt e3) {
            throw e3;
        }
    }

    public static AbstractC27091Fz A0B(AnonymousClass253 r2, AnonymousClass254 r3, AbstractC27091Fz r4) {
        AbstractC27091Fz r1 = (AbstractC27091Fz) r4.A0V(AnonymousClass25B.NEW_MUTABLE_INSTANCE, null, null);
        try {
            r1.A0V(AnonymousClass25B.MERGE_FROM_STREAM, r2, r3);
            r1.A0W();
            return r1;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof C28971Pt) {
                throw e.getCause();
            }
            throw e;
        }
    }

    public static AbstractC27091Fz A0C(AnonymousClass1G4 r0, AbstractC27091Fz r1) {
        r0.A04(r1);
        return r0.A01();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        if (r2 >= 64) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        r0 = r7.read();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0030, code lost:
        if (r0 == -1) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0034, code lost:
        if ((r0 & 128) == 0) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0036, code lost:
        r2 = r2 + 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        throw new X.C28971Pt("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0048, code lost:
        throw new X.C28971Pt("CodedInputStream encountered a malformed varint.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC27091Fz A0D(X.AbstractC27091Fz r6, java.io.InputStream r7) {
        /*
            X.254 r5 = X.AnonymousClass259.A00()
            int r4 = r7.read()     // Catch: IOException -> 0x006d
            r3 = -1
            if (r4 != r3) goto L_0x000c
            goto L_0x0064
        L_0x000c:
            r0 = r4 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0051
            r4 = r4 & 127(0x7f, float:1.78E-43)
            r2 = 7
        L_0x0013:
            int r1 = r7.read()     // Catch: IOException -> 0x006d
            if (r1 == r3) goto L_0x0049
            r0 = r1 & 127(0x7f, float:1.78E-43)
            int r0 = r0 << r2
            r4 = r4 | r0
            r0 = r1 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0051
            int r2 = r2 + 7
            r0 = 32
            if (r2 >= r0) goto L_0x0028
            goto L_0x0013
        L_0x0028:
            r0 = 64
            if (r2 >= r0) goto L_0x0041
            int r0 = r7.read()     // Catch: IOException -> 0x006d
            if (r0 == r3) goto L_0x0039
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0051
            int r2 = r2 + 7
            goto L_0x0028
        L_0x0039:
            java.lang.String r1 = "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length."
            X.1Pt r0 = new X.1Pt     // Catch: IOException -> 0x006d
            r0.<init>(r1)     // Catch: IOException -> 0x006d
            throw r0     // Catch: IOException -> 0x006d
        L_0x0041:
            java.lang.String r1 = "CodedInputStream encountered a malformed varint."
            X.1Pt r0 = new X.1Pt     // Catch: IOException -> 0x006d
            r0.<init>(r1)     // Catch: IOException -> 0x006d
            throw r0     // Catch: IOException -> 0x006d
        L_0x0049:
            java.lang.String r1 = "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length."
            X.1Pt r0 = new X.1Pt     // Catch: IOException -> 0x006d
            r0.<init>(r1)     // Catch: IOException -> 0x006d
            throw r0     // Catch: IOException -> 0x006d
        L_0x0051:
            X.25A r0 = new X.25A
            r0.<init>(r7, r4)
            X.253 r2 = new X.253
            r2.<init>(r0)
            X.1Fz r1 = A0B(r2, r5, r6)
            r0 = 0
            r2.A0C(r0)     // Catch: 1Pt -> 0x0069
            goto L_0x0065
        L_0x0064:
            r1 = 0
        L_0x0065:
            A0P(r1)
            return r1
        L_0x0069:
            r0 = move-exception
            r0.unfinishedMessage = r1
            throw r0
        L_0x006d:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            X.1Pt r0 = new X.1Pt
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC27091Fz.A0D(X.1Fz, java.io.InputStream):X.1Fz");
    }

    public static AbstractC27091Fz A0E(AbstractC27091Fz r4, byte[] bArr) {
        AnonymousClass254 A00 = AnonymousClass259.A00();
        try {
            int length = bArr.length;
            AnonymousClass253 r0 = new AnonymousClass253(bArr, 0, length);
            try {
                r0.A04(length);
                AbstractC27091Fz A0B = A0B(r0, A00, r4);
                try {
                    r0.A0C(0);
                    A0P(A0B);
                    return A0B;
                } catch (C28971Pt e) {
                    e.unfinishedMessage = A0B;
                    throw e;
                }
            } catch (C28971Pt e2) {
                throw new IllegalArgumentException(e2);
            }
        } catch (C28971Pt e3) {
            throw e3;
        }
    }

    public static AbstractC41941uP A0F(AbstractC41941uP r2) {
        int size = r2.size();
        int i = size << 1;
        if (size == 0) {
            i = 10;
        }
        return r2.ALY(i);
    }

    public static AnonymousClass1K6 A0G(AnonymousClass1K6 r2) {
        int size = r2.size();
        int i = size << 1;
        if (size == 0) {
            i = 10;
        }
        return r2.ALZ(i);
    }

    public static AnonymousClass1G1 A0H(AnonymousClass253 r1, AnonymousClass254 r2, AbstractC27091Fz r3) {
        return r1.A09(r2, r3.A0U());
    }

    public static Object A0I(Object obj, Method method, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw cause;
            } else if (cause instanceof Error) {
                throw cause;
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    public static RuntimeException A0J(C28971Pt r1, AnonymousClass1G1 r2) {
        r1.unfinishedMessage = r2;
        return new RuntimeException(r1);
    }

    public static RuntimeException A0K(AnonymousClass1G1 r2, Throwable th) {
        C28971Pt r1 = new C28971Pt(th.getMessage());
        r1.unfinishedMessage = r2;
        return new RuntimeException(r1);
    }

    public static String A0L(AbstractC27881Jp r5) {
        String str;
        AbstractC27881Jp r3 = new AnonymousClass258(r5).A00;
        StringBuilder sb = new StringBuilder(r3.A03());
        for (int i = 0; i < r3.A03(); i++) {
            int A02 = r3.A02(i);
            if (A02 == 34) {
                str = "\\\"";
            } else if (A02 == 39) {
                str = "\\'";
            } else if (A02 != 92) {
                switch (A02) {
                    case 7:
                        str = "\\a";
                        break;
                    case 8:
                        str = "\\b";
                        break;
                    case 9:
                        str = "\\t";
                        break;
                    case 10:
                        str = "\\n";
                        break;
                    case 11:
                        str = "\\v";
                        break;
                    case 12:
                        str = "\\f";
                        break;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        str = "\\r";
                        break;
                    default:
                        if (A02 < 32 || A02 > 126) {
                            sb.append('\\');
                            sb.append((char) (((A02 >>> 6) & 3) + 48));
                            sb.append((char) (((A02 >>> 3) & 7) + 48));
                            A02 = (A02 & 7) + 48;
                        }
                        sb.append((char) A02);
                        continue;
                        break;
                }
            } else {
                str = "\\\\";
            }
            sb.append(str);
        }
        return sb.toString();
    }

    public static final String A0M(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    public static void A0N(CodedOutputStream codedOutputStream, AbstractC27091Fz r2) {
        r2.unknownFields.A02(codedOutputStream);
    }

    public static void A0O(CodedOutputStream codedOutputStream, Object obj, int i) {
        codedOutputStream.A0L((AnonymousClass1G0) obj, i);
    }

    public static void A0P(AbstractC27091Fz r2) {
        if (r2 != null && !r2.A0Z()) {
            C28971Pt r0 = new C28971Pt(new AnonymousClass257().getMessage());
            r0.unfinishedMessage = r2;
            throw r0;
        }
    }

    public static void A0Q(AnonymousClass1G1 r12, StringBuilder sb, int i) {
        boolean z;
        int intValue;
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        TreeSet treeSet = new TreeSet();
        Method[] declaredMethods = r12.getClass().getDeclaredMethods();
        for (Method method : declaredMethods) {
            hashMap2.put(method.getName(), method);
            if (method.getParameterTypes().length == 0) {
                hashMap.put(method.getName(), method);
                if (method.getName().startsWith("get")) {
                    treeSet.add(method.getName());
                }
            }
        }
        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            String replaceFirst = ((String) it.next()).replaceFirst("get", "");
            if (replaceFirst.endsWith("List") && !replaceFirst.endsWith("OrBuilderList")) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(replaceFirst.substring(0, 1).toLowerCase());
                sb2.append(replaceFirst.substring(1, replaceFirst.length() - 4));
                String obj = sb2.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("get");
                sb3.append(replaceFirst);
                Method method2 = (Method) hashMap.get(sb3.toString());
                if (method2 != null) {
                    A0S(A0I(r12, method2, new Object[0]), A0M(obj), sb, i);
                }
            }
            StringBuilder sb4 = new StringBuilder("set");
            sb4.append(replaceFirst);
            if (hashMap2.get(sb4.toString()) != null) {
                if (replaceFirst.endsWith("Bytes")) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("get");
                    sb5.append(replaceFirst.substring(0, replaceFirst.length() - 5));
                    if (hashMap.containsKey(sb5.toString())) {
                    }
                }
                StringBuilder sb6 = new StringBuilder();
                sb6.append(replaceFirst.substring(0, 1).toLowerCase());
                sb6.append(replaceFirst.substring(1));
                String obj2 = sb6.toString();
                StringBuilder sb7 = new StringBuilder();
                sb7.append("get");
                sb7.append(replaceFirst);
                Method method3 = (Method) hashMap.get(sb7.toString());
                StringBuilder sb8 = new StringBuilder("has");
                sb8.append(replaceFirst);
                Method method4 = (Method) hashMap.get(sb8.toString());
                if (method3 != null) {
                    Object A0I = A0I(r12, method3, new Object[0]);
                    if (method4 == null) {
                        if (A0I instanceof Boolean) {
                            z = !((Boolean) A0I).booleanValue();
                        } else {
                            if (A0I instanceof Integer) {
                                intValue = ((Number) A0I).intValue();
                            } else if (A0I instanceof Float) {
                                intValue = (((Number) A0I).floatValue() > 0.0f ? 1 : (((Number) A0I).floatValue() == 0.0f ? 0 : -1));
                            } else if (A0I instanceof Double) {
                                intValue = (((Number) A0I).doubleValue() > 0.0d ? 1 : (((Number) A0I).doubleValue() == 0.0d ? 0 : -1));
                            } else if (A0I instanceof String) {
                                z = A0I.equals("");
                            } else if (A0I instanceof AbstractC27881Jp) {
                                z = A0I.equals(AbstractC27881Jp.A01);
                            } else {
                                if (A0I instanceof AnonymousClass1G1) {
                                    if (A0I == ((AnonymousClass1G2) A0I).ACU()) {
                                    }
                                } else if (A0I instanceof Enum) {
                                    intValue = ((Enum) A0I).ordinal();
                                }
                                A0S(A0I, A0M(obj2), sb, i);
                            }
                            if (intValue != 0) {
                                A0S(A0I, A0M(obj2), sb, i);
                            }
                        }
                        if (!z) {
                            A0S(A0I, A0M(obj2), sb, i);
                        }
                    } else if (((Boolean) A0I(r12, method4, new Object[0])).booleanValue()) {
                        A0S(A0I, A0M(obj2), sb, i);
                    }
                }
            }
        }
        AnonymousClass256 r3 = ((AbstractC27091Fz) r12).unknownFields;
        if (r3 != null) {
            for (int i2 = 0; i2 < r3.count; i2++) {
                A0S(r3.A03[i2], String.valueOf(r3.A02[i2] >>> 3), sb, i);
            }
        }
    }

    public static void A0R(Object obj) {
        ((AnonymousClass1K7) obj).A00 = false;
    }

    public static final void A0S(Object obj, String str, StringBuilder sb, int i) {
        String str2;
        String A0L;
        if (obj instanceof List) {
            for (Object obj2 : (List) obj) {
                A0S(obj2, str, sb, i);
            }
            return;
        }
        sb.append('\n');
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(' ');
        }
        sb.append(str);
        if (obj instanceof String) {
            sb.append(": \"");
            A0L = A0L(new C27861Jn(((String) obj).getBytes(C27851Jm.A03)));
        } else if (obj instanceof AbstractC27881Jp) {
            sb.append(": \"");
            A0L = A0L((AbstractC27881Jp) obj);
        } else {
            if (obj instanceof AbstractC27091Fz) {
                sb.append(" {");
                A0Q((AnonymousClass1G0) obj, sb, i + 2);
                sb.append("\n");
                for (int i3 = 0; i3 < i; i3++) {
                    sb.append(' ');
                }
                str2 = "}";
            } else {
                sb.append(": ");
                str2 = obj.toString();
            }
            sb.append(str2);
            return;
        }
        sb.append(A0L);
        sb.append('\"');
    }

    public final AnonymousClass1G4 A0T() {
        AnonymousClass1G4 r0 = (AnonymousClass1G4) A0V(AnonymousClass25B.NEW_BUILDER, null, null);
        r0.A04(this);
        return r0;
    }

    public final AnonymousClass255 A0U() {
        return (AnonymousClass255) A0V(AnonymousClass25B.GET_PARSER, null, null);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 6241
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:66)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public java.lang.Object A0V(X.AnonymousClass25B r19, java.lang.Object r20, java.lang.Object r21) {
        /*
        // Method dump skipped, instructions count: 21882
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC27091Fz.A0V(X.25B, java.lang.Object, java.lang.Object):java.lang.Object");
    }

    public void A0W() {
        A0V(AnonymousClass25B.MAKE_IMMUTABLE, null, null);
        this.unknownFields.A01 = false;
    }

    public void A0X(int i, int i2) {
        AnonymousClass256 r4 = this.unknownFields;
        if (r4 == AnonymousClass256.A04) {
            r4 = new AnonymousClass256(new int[8], new Object[8], 0, true);
            this.unknownFields = r4;
        }
        if (r4.A01) {
            r4.A01((i << 3) | 0, Long.valueOf((long) i2));
            return;
        }
        throw new UnsupportedOperationException();
    }

    public void A0Y(AbstractC462925h r3, AbstractC27091Fz r4) {
        A0V(AnonymousClass25B.VISIT, r3, r4);
        this.unknownFields = r3.Afz(this.unknownFields, r4.unknownFields);
    }

    public final boolean A0Z() {
        return A0V(AnonymousClass25B.IS_INITIALIZED, Boolean.TRUE, null) != null;
    }

    public boolean A0a(AnonymousClass253 r6, int i) {
        if ((i & 7) == 4) {
            return false;
        }
        AnonymousClass256 r4 = this.unknownFields;
        if (r4 == AnonymousClass256.A04) {
            r4 = new AnonymousClass256(new int[8], new Object[8], 0, true);
            this.unknownFields = r4;
        }
        return r4.A03(r6, i);
    }

    @Override // X.AnonymousClass1G2
    public /* bridge */ /* synthetic */ AnonymousClass1G1 ACU() {
        return (AnonymousClass1G0) A0V(AnonymousClass25B.GET_DEFAULT_INSTANCE, null, null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!A0V(AnonymousClass25B.GET_DEFAULT_INSTANCE, null, null).getClass().isInstance(obj)) {
            return false;
        }
        try {
            A0Y(AnonymousClass275.A01, (AbstractC27091Fz) obj);
            return true;
        } catch (C113275Gw unused) {
            return false;
        }
    }

    public int hashCode() {
        int i = super.A00;
        if (i != 0) {
            return i;
        }
        AnonymousClass276 r0 = new AnonymousClass276();
        A0Y(r0, this);
        int i2 = r0.A00;
        super.A00 = i2;
        return i2;
    }

    public String toString() {
        String obj = super.toString();
        StringBuilder sb = new StringBuilder("# ");
        sb.append(obj);
        A0Q(this, sb, 0);
        return sb.toString();
    }
}
