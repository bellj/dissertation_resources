package X;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.api.Status;

/* renamed from: X.4yq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108304yq implements AnonymousClass5XN {
    public final C108364yw A00;

    public C108304yq(C108364yw r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5XN
    public final void AgT() {
    }

    @Override // X.AnonymousClass5XN
    public final void AgZ(Bundle bundle) {
    }

    @Override // X.AnonymousClass5XN
    public final void Aga(C56492ky r1, AnonymousClass1UE r2, boolean z) {
    }

    @Override // X.AnonymousClass5XN
    public final AnonymousClass1UI AgM(AnonymousClass1UI r1) {
        AgO(r1);
        return r1;
    }

    @Override // X.AnonymousClass5XN
    public final AnonymousClass1UI AgO(AnonymousClass1UI r6) {
        try {
            C108364yw r4 = this.A00;
            C77733nl r3 = r4.A05;
            C93414a8 r1 = r3.A0C;
            r1.A01.add(r6);
            r6.A0A.set(r1.A00);
            AnonymousClass4DN r2 = r6.A00;
            AbstractC72443eb r12 = (AbstractC72443eb) r3.A0H.get(r2);
            C13020j0.A02(r12, "Appropriate Api was not requested.");
            if (r12.isConnected() || !r4.A0A.containsKey(r2)) {
                r6.A08(r12);
                return r6;
            }
            r6.A09(new Status(17, null));
            return r6;
        } catch (DeadObjectException unused) {
            C108364yw r0 = this.A00;
            C72463ee.A0R(r0.A06, new C77823nv(this, this));
            return r6;
        }
    }

    @Override // X.AnonymousClass5XN
    public final void AgW() {
    }

    @Override // X.AnonymousClass5XN
    public final void Agb(int i) {
        C108364yw r1 = this.A00;
        r1.A00(null);
        r1.A07.AgS(i, false);
    }

    @Override // X.AnonymousClass5XN
    public final boolean Agc() {
        this.A00.A00(null);
        return true;
    }
}
