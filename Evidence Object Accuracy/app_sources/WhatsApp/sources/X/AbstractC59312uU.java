package X;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.jid.UserJid;
import java.util.Date;

/* renamed from: X.2uU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC59312uU extends AbstractC75723kJ {
    public final int A00;
    public final int A01;
    public final int A02;
    public final FrameLayout A03;
    public final ImageView A04;
    public final TextView A05;
    public final TextEmojiLabel A06;
    public final TextEmojiLabel A07;
    public final C37071lG A08;
    public final AnonymousClass1lM A09;
    public final AnonymousClass018 A0A;
    public final UserJid A0B;
    public final Date A0C = new Date();

    public AbstractC59312uU(View view, C37071lG r3, AnonymousClass1lM r4, AnonymousClass018 r5, UserJid userJid) {
        super(view);
        this.A0B = userJid;
        this.A0A = r5;
        this.A03 = (FrameLayout) view.findViewById(R.id.catalog_item_view);
        this.A04 = C12970iu.A0L(view, R.id.catalog_list_product_image);
        TextEmojiLabel A0U = C12970iu.A0U(view, R.id.catalog_list_product_title);
        this.A07 = A0U;
        this.A02 = A0U.getTextColors().getDefaultColor();
        TextView A0J = C12960it.A0J(view, R.id.catalog_list_product_price);
        this.A05 = A0J;
        this.A01 = A0J.getTextColors().getDefaultColor();
        TextEmojiLabel A0U2 = C12970iu.A0U(view, R.id.catalog_list_product_description);
        this.A06 = A0U2;
        this.A00 = A0U2.getTextColors().getDefaultColor();
        this.A08 = r3;
        this.A09 = r4;
    }
}
