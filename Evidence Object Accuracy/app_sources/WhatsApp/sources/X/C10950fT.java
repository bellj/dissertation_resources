package X;

import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* renamed from: X.0fT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10950fT extends FutureTask {
    public final /* synthetic */ RunnableC10240eG A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C10950fT(RunnableC10240eG r1, Callable callable) {
        super(callable);
        this.A00 = r1;
    }

    @Override // java.util.concurrent.FutureTask
    public void done() {
        try {
            Object obj = get();
            RunnableC10240eG r1 = this.A00;
            if (!r1.A04.get()) {
                r1.A00(obj);
            }
        } catch (InterruptedException e) {
            Log.w("AsyncTask", e);
        } catch (CancellationException unused) {
            RunnableC10240eG r2 = this.A00;
            if (!r2.A04.get()) {
                r2.A00(null);
            }
        } catch (ExecutionException e2) {
            throw new RuntimeException("An error occurred while executing doInBackground()", e2.getCause());
        } catch (Throwable th) {
            throw new RuntimeException("An error occurred while executing doInBackground()", th);
        }
    }
}
