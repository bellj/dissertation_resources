package X;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

/* renamed from: X.4aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93784aj {
    public static final AbstractC77683ng A09;
    public static final AnonymousClass4DN A0A;
    @Deprecated
    public static final AnonymousClass1UE A0B;
    public int A00 = -1;
    public EnumC87344Bd A01;
    public String A02;
    public final int A03;
    public final Context A04;
    public final AnonymousClass5QT A05;
    public final AnonymousClass5QU A06;
    public final AbstractC115095Qe A07;
    public final String A08;

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A0A = r3;
        C77603nY r2 = new C77603nY();
        A09 = r2;
        A0B = new AnonymousClass1UE(r2, r3, "ClearcutLogger.API");
    }

    public C93784aj(Context context) {
        C77713nj r8 = new C77713nj(context);
        C108454z5 r7 = C108454z5.A00;
        C108154ya r6 = new C108154ya(context);
        EnumC87344Bd r4 = EnumC87344Bd.A01;
        this.A01 = r4;
        this.A04 = context;
        this.A08 = context.getPackageName();
        int i = 0;
        try {
            i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.wtf("ClearcutLogger", "This can't happen.", e);
        }
        this.A03 = i;
        this.A00 = -1;
        this.A02 = "VISION";
        this.A06 = r8;
        this.A07 = r7;
        this.A01 = r4;
        this.A05 = r6;
    }
}
