package X;

/* renamed from: X.5KV  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KV extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public final /* synthetic */ AnonymousClass5X4[] $elements;
    public final /* synthetic */ AnonymousClass5BP $index;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5KV(AnonymousClass5BP r2, AnonymousClass5X4[] r3) {
        super(2);
        this.$elements = r3;
        this.$index = r2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: X.5X4[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AnonymousClass5ZQ
    public /* bridge */ /* synthetic */ Object AJ5(Object obj, Object obj2) {
        C16700pc.A0E(obj2, 1);
        AnonymousClass5X4[] r3 = this.$elements;
        AnonymousClass5BP r2 = this.$index;
        int i = r2.element;
        r2.element = i + 1;
        r3[i] = obj2;
        return AnonymousClass1WZ.A00;
    }
}
