package X;

/* renamed from: X.5F1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5F1 implements AnonymousClass5WP {
    public final boolean A00 = true;

    @Override // X.AnonymousClass5WP
    public AnonymousClass5L9 ADu() {
        return null;
    }

    @Override // X.AnonymousClass5WP
    public boolean AJD() {
        return this.A00;
    }

    public String toString() {
        String str;
        StringBuilder A0k = C12960it.A0k("Empty{");
        if (this.A00) {
            str = "Active";
        } else {
            str = "New";
        }
        A0k.append(str);
        return C12970iu.A0v(A0k);
    }
}
