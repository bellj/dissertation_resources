package X;

/* renamed from: X.3EW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EW {
    public final long A00;
    public final AnonymousClass1V8 A01;
    public final String A02;
    public final String A03;

    public AnonymousClass3EW(AnonymousClass1V8 r22) {
        AnonymousClass1V8.A01(r22, "error");
        this.A00 = C12980iv.A0G(AnonymousClass3JT.A04(null, r22, Long.TYPE, C12970iu.A0j(), C12970iu.A0k(), null, new String[]{"code"}, false));
        this.A03 = (String) AnonymousClass3JT.A04(null, r22, String.class, 1L, 1000L, null, new String[]{"text"}, false);
        this.A02 = (String) AnonymousClass3JT.A03(null, r22, String.class, 1L, 10000L, null, new String[]{"params"}, false);
        this.A01 = r22;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3EW.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3EW r7 = (AnonymousClass3EW) obj;
            if (this.A00 != r7.A00 || !this.A03.equals(r7.A03) || !C29941Vi.A00(this.A02, r7.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(this.A00);
        objArr[1] = this.A03;
        return C12980iv.A0B(this.A02, objArr, 2);
    }
}
