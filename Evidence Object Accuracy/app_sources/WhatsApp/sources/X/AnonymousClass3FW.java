package X;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3FW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3FW {
    public final String A00;
    public final JSONObject A01 = new JSONObject();

    public AnonymousClass3FW(String str, AnonymousClass3FW... r5) {
        this.A00 = str;
        for (AnonymousClass3FW r0 : r5) {
            A00(r0);
        }
    }

    public void A00(AnonymousClass3FW r6) {
        try {
            String str = r6.A00;
            if (str == null) {
                JSONObject jSONObject = r6.A01;
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String A0x = C12970iu.A0x(keys);
                    this.A01.put(A0x, jSONObject.get(A0x));
                }
                return;
            }
            this.A01.put(str, r6.A01);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void A01(String str, String str2) {
        try {
            this.A01.put(str, str2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void A02(String str, boolean z) {
        try {
            this.A01.put(str, z);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            String str = this.A00;
            if (str != null) {
                jSONObject.put(str, this.A01);
            } else {
                jSONObject = this.A01;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }
}
