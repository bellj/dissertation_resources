package X;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.3EO  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EO {
    public float A00;
    public float A01;
    public float A02;
    public LatLng A03;

    public CameraPosition A00() {
        return new CameraPosition(this.A03, this.A00, this.A01, this.A02);
    }
}
