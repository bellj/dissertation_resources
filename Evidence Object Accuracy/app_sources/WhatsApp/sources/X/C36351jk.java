package X;

import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1jk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36351jk implements AbstractC36361jl {
    public final C14830m7 A00;
    public final C21320xE A01;
    public final C15650ng A02;
    public final C20710wC A03;
    public final C63323Bd A04;
    public final C22140ya A05;
    public final C14860mA A06;

    public C36351jk(C14830m7 r2, C21320xE r3, C15650ng r4, C20710wC r5, C63323Bd r6, C22140ya r7, C14860mA r8) {
        this.A00 = r2;
        this.A06 = r8;
        this.A02 = r4;
        this.A03 = r5;
        this.A05 = r7;
        this.A01 = r3;
        this.A04 = r6;
        List list = r6.A06;
        if (list != null) {
            Arrays.deepToString(list.toArray());
        }
    }

    @Override // X.AbstractC36361jl
    public void APl(int i) {
        C63323Bd r0 = this.A04;
        AnonymousClass1JV r4 = r0.A02;
        String str = r0.A05;
        List list = r0.A06;
        int i2 = r0.A00;
        AnonymousClass1P4 r2 = r0.A03;
        StringBuilder sb = new StringBuilder("groupmgr/request failed : ");
        sb.append(i);
        sb.append(" | ");
        sb.append(r4);
        sb.append(" | ");
        sb.append(14);
        Log.e(sb.toString());
        this.A03.A0y.remove(r4);
        int i3 = 2003;
        if (i != 406) {
            i3 = 2004;
            if (i != 429) {
                i3 = 2002;
                if (i != 500) {
                    i3 = 2001;
                }
            }
        }
        C20710wC.A02(i3, str);
        this.A02.A0S(this.A05.A03(r4, str, list, 3, i2, this.A00.A00()));
        if (r2 != null) {
            this.A06.A0H(r2.A01, i);
        }
        this.A01.A09(r4, false);
    }

    @Override // X.AbstractC36361jl
    public void AWy(C15580nU r5, C47572Bl r6) {
        StringBuilder sb = new StringBuilder("groupmgr/request success : ");
        sb.append(r5);
        sb.append(" | ");
        sb.append(14);
        Log.i(sb.toString());
        C63323Bd r3 = this.A04;
        AnonymousClass1P4 r0 = r3.A03;
        if (r0 != null) {
            this.A06.A0H(r0.A01, 200);
        }
        this.A01.A09(r3.A02, false);
    }

    @Override // X.AbstractC36361jl
    public void AXX() {
        C63323Bd r0 = this.A04;
        AnonymousClass1JV r5 = r0.A02;
        String str = r0.A05;
        List list = r0.A06;
        int i = r0.A00;
        AnonymousClass1P4 r3 = r0.A03;
        Log.i("groupmgr/group_request/timeout/type: 14");
        this.A03.A0y.remove(r5);
        this.A02.A0S(this.A05.A03(r5, str, list, 3, i, this.A00.A00()));
        if (r3 != null) {
            this.A06.A0H(r3.A01, 500);
        }
        this.A01.A09(r5, false);
    }
}
