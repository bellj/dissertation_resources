package X;

import android.content.Context;
import android.graphics.drawable.Drawable;

/* renamed from: X.4vM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106224vM implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        ((AnonymousClass2k6) obj).setForegroundCompat(((C55992k9) obj2).A05);
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        Drawable drawable = ((C55992k9) obj).A05;
        Drawable drawable2 = ((C55992k9) obj2).A05;
        if (drawable == null) {
            if (drawable2 != null) {
                return true;
            }
            return false;
        } else if (drawable2 == null || drawable.equals(drawable2)) {
            return false;
        } else {
            return true;
        }
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        ((AnonymousClass2k6) obj).setForegroundCompat(null);
    }
}
