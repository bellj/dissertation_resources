package X;

import java.util.List;

/* renamed from: X.1cN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32571cN {
    public final C32561cM A00;
    public final C15580nU A01;
    public final AnonymousClass1P4 A02;
    public final String A03;
    public final List A04;

    public C32571cN(C32561cM r1, C15580nU r2, AnonymousClass1P4 r3, String str, List list) {
        this.A03 = str;
        this.A01 = r2;
        this.A04 = list;
        this.A00 = r1;
        this.A02 = r3;
    }
}
