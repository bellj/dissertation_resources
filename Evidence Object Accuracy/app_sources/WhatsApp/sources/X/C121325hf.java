package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5hf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121325hf extends AbstractC130285z6 {
    public String A00;
    public List A01 = C12960it.A0l();

    public C121325hf(AnonymousClass1V8 r7, String str) {
        this.A00 = str;
        try {
            Iterator it = r7.A0J("required_document").iterator();
            while (it.hasNext()) {
                AnonymousClass1V8 A0d = C117305Zk.A0d(it);
                A0d.A0H("family_name");
                List A0J = A0d.A0J("option");
                ArrayList A0l = C12960it.A0l();
                Iterator it2 = A0J.iterator();
                while (it2.hasNext()) {
                    AnonymousClass1V8 A0d2 = C117305Zk.A0d(it2);
                    C12970iu.A1T(A0d2.A0H("name"), A0d2.A0H("type"), A0l);
                }
                this.A01.add(new C125735rj(A0l));
            }
        } catch (AnonymousClass1V9 unused) {
            Log.e("PAY: DocUploadChallenge/parseDocUploadOptions failed.");
        }
    }
}
