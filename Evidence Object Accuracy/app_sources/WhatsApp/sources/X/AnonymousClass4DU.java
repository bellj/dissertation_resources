package X;

/* renamed from: X.4DU  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4DU {
    public static int A04(int i) {
        return C80253rx.A00(i << 3);
    }

    public static int A05(int i) {
        return C80253rx.A00((i >> 31) ^ (i << 1));
    }

    public static int A06(long j) {
        return C80253rx.A01((j >> 63) ^ (j << 1));
    }

    public static void A07(C80253rx r1, int i, int i2) {
        r1.A05((i << 3) | i2);
    }
}
