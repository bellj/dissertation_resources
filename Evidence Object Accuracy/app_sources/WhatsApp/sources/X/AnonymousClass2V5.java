package X;

import com.whatsapp.viewsharedcontacts.ViewSharedContactArrayActivity;

/* renamed from: X.2V5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2V5 {
    public AbstractC14640lm A00;
    public final long A01;
    public final C15370n3 A02;
    public final /* synthetic */ ViewSharedContactArrayActivity A03;

    public /* synthetic */ AnonymousClass2V5(C15370n3 r1, AbstractC14640lm r2, ViewSharedContactArrayActivity viewSharedContactArrayActivity, long j) {
        this.A03 = viewSharedContactArrayActivity;
        this.A02 = r1;
        this.A01 = j;
        this.A00 = r2;
    }
}
