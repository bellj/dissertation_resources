package X;

import android.os.Bundle;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.PhotoView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.2eO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53422eO extends AbstractC000600h {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ C454321o A01;

    public C53422eO(Bundle bundle, C454321o r2) {
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AbstractC000600h
    public void A03(List list, Map map) {
        Bundle bundle;
        ArrayList<String> stringArrayList;
        PhotoView A19;
        MediaViewBaseFragment mediaViewBaseFragment = this.A01.A01;
        Object A1C = mediaViewBaseFragment.A1C(mediaViewBaseFragment.A09.getCurrentItem());
        if (A1C != null && (bundle = this.A00) != null && (stringArrayList = bundle.getStringArrayList("visible_shared_elements")) != null && stringArrayList.contains(AbstractC28561Ob.A0W(A1C)) && (A19 = mediaViewBaseFragment.A19(A1C)) != null) {
            Object A1B = mediaViewBaseFragment.A1B();
            AnonymousClass009.A05(A1B);
            list.remove(AbstractC28561Ob.A0W(A1B));
            list.add(AbstractC28561Ob.A0W(A1C));
            map.put(AbstractC28561Ob.A0W(A1C), A19);
        }
    }
}
