package X;

/* renamed from: X.2uj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59442uj extends AbstractC16320oo {
    public final double A00;
    public final double A01;
    public final double A02;
    public final String A03;

    public C59442uj(AbstractC15710nm r15, C14900mE r16, C16430p0 r17, C48122Ek r18, AnonymousClass2K1 r19, AnonymousClass1B4 r20, C16340oq r21, C17170qN r22, AnonymousClass018 r23, C14850m9 r24, AbstractC14440lR r25) {
        super(r15, r16, r17, r19, r20, r21, r22, r23, r24, r25);
        this.A00 = r18.A02().doubleValue();
        this.A01 = r18.A03().doubleValue();
        this.A02 = r18.A05.doubleValue();
        this.A03 = r18.A07;
    }
}
