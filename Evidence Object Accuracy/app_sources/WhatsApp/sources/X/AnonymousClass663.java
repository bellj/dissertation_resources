package X;

import android.graphics.Point;

/* renamed from: X.663  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass663 implements AbstractC136156Lf {
    public final /* synthetic */ AnonymousClass643 A00;

    public AnonymousClass663(AnonymousClass643 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136156Lf
    public void AQe(Point point, EnumC124565pk r8) {
        Object[] objArr;
        int i;
        int i2;
        AnonymousClass643 r5 = this.A00;
        AnonymousClass4K4 r4 = r5.A0B;
        if (r4 != null) {
            int A01 = C117315Zl.A01(r8, C125165qo.A00);
            if (A01 != 1) {
                if (A01 != 2) {
                    if (A01 == 3 || A01 == 4) {
                        i2 = 13;
                    } else if (A01 == 5) {
                        i2 = 14;
                    } else {
                        return;
                    }
                    AnonymousClass643.A00(r5, r4, i2);
                    return;
                } else if (point != null) {
                    objArr = new Object[]{r4, point};
                    i = 12;
                } else {
                    return;
                }
            } else if (point != null) {
                objArr = new Object[]{r4, point};
                i = 11;
            } else {
                return;
            }
            AnonymousClass643.A00(r5, objArr, i);
        }
    }
}
