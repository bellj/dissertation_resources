package X;

import android.app.Activity;
import android.content.Context;
import android.view.View;

/* renamed from: X.2oO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C58252oO extends AbstractC52172aN {
    public final C15570nT A00;
    public final C15370n3 A01;

    public C58252oO(Context context, C15570nT r2, C15370n3 r3, int i) {
        super(context, i);
        this.A01 = r3;
        this.A00 = r2;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        C15570nT r1 = this.A00;
        C15370n3 r3 = this.A01;
        if (!r1.A0F(r3.A0D)) {
            Activity A02 = AnonymousClass12P.A02(view);
            A02.startActivity(new C14960mK().A0h(A02, r3, 14));
        }
    }
}
