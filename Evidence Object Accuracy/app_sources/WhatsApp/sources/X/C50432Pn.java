package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2Pn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50432Pn implements AbstractC50412Pl {
    public final AnonymousClass19Q A00;
    public final AnonymousClass19T A01;
    public final C16590pI A02;
    public final C15650ng A03;

    public C50432Pn(AnonymousClass19Q r1, AnonymousClass19T r2, C16590pI r3, C15650ng r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC50412Pl
    public void AZA(Context context, AbstractC15340mz r12, C16470p4 r13, int i) {
        String str;
        AnonymousClass1ZC r5 = r13.A04;
        if (r5 != null && r13.A00 == 2) {
            AnonymousClass19Q r3 = this.A00;
            r3.A00(10);
            AnonymousClass1Z6 r0 = r13.A02;
            if (r0 != null) {
                str = r0.A01;
            } else {
                str = null;
            }
            Intent intent = new Intent();
            intent.setClassName(context.getPackageName(), "com.whatsapp.biz.catalog.view.activity.ProductListActivity");
            intent.putExtra("message_content", r5);
            intent.putExtra("message_title", str);
            context.startActivity(intent);
            UserJid userJid = r5.A00;
            String str2 = r5.A01.A01;
            AnonymousClass19T r2 = this.A01;
            r2.A0M.add(new AnonymousClass3V1(r2, new AnonymousClass4Q7(this, r12, r13), str2));
            if (!r2.A07(new AnonymousClass4TA(userJid, 0, 0, str2, r3.A00, false))) {
                StringBuilder sb = new StringBuilder("ProductHeaderIntegrityChecker/AsyncCallback/Failed to get product status, reason - ");
                sb.append("Failed to send product request");
                Log.e(sb.toString());
            }
        }
    }
}
