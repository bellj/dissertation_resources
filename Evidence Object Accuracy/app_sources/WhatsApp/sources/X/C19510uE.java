package X;

import android.os.CancellationSignal;
import android.os.SystemClock;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0uE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19510uE {
    public final C15570nT A00;
    public final C16240og A01;
    public final C19500uD A02;
    public final C14830m7 A03;
    public final C19480uB A04;
    public final C19490uC A05;

    public C19510uE(C15570nT r1, C16240og r2, C19500uD r3, C14830m7 r4, C19480uB r5, C19490uC r6) {
        this.A03 = r4;
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = r6;
        this.A01 = r2;
        this.A02 = r3;
    }

    public static void A00(CancellationSignal cancellationSignal, CountDownLatch countDownLatch) {
        long elapsedRealtime = SystemClock.elapsedRealtime() + C26061Bw.A0L;
        while (true) {
            cancellationSignal.throwIfCanceled();
            if (countDownLatch.getCount() != 0 && SystemClock.elapsedRealtime() <= elapsedRealtime) {
                countDownLatch.await(500, TimeUnit.MILLISECONDS);
            } else {
                return;
            }
        }
    }
}
