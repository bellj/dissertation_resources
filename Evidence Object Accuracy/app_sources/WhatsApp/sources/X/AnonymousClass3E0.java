package X;

/* renamed from: X.3E0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3E0 {
    public final AnonymousClass1V8 A00;
    public final C63983Dt A01;

    public AnonymousClass3E0(AbstractC15710nm r2, AnonymousClass1V8 r3) {
        AnonymousClass1V8.A01(r3, "pay");
        this.A01 = (C63983Dt) AnonymousClass3JT.A01(r2, r3, 9);
        this.A00 = r3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass3E0.class != obj.getClass()) {
            return false;
        }
        return this.A01.equals(((AnonymousClass3E0) obj).A01);
    }

    public int hashCode() {
        return C12970iu.A08(this.A01, C12970iu.A1b());
    }
}
