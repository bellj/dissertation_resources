package X;

/* renamed from: X.54t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1102354t implements AnonymousClass27K {
    public long A00;
    public String A01;
    public final C233411h A02;
    public final C14830m7 A03;
    public final C16120oU A04;
    public final AbstractC14440lR A05;

    public C1102354t(C233411h r1, C14830m7 r2, C16120oU r3, AbstractC14440lR r4) {
        this.A03 = r2;
        this.A05 = r4;
        this.A04 = r3;
        this.A02 = r1;
    }

    @Override // X.AnonymousClass27K
    public void AKd(int i) {
        AKe(1, -1);
    }

    @Override // X.AnonymousClass27K
    public void AKe(int i, long j) {
        long j2 = this.A00;
        this.A05.Ab2(new RunnableC76243lK(this, this.A01, i, j2, j));
    }

    @Override // X.AnonymousClass27K
    public void AKn() {
        long j = this.A00;
        this.A05.Ab2(new RunnableC76243lK(this, this.A01, 4, j, 0));
    }

    @Override // X.AnonymousClass27K
    public void AUp(String str) {
        this.A00 = C12980iv.A0D(this.A03.A00());
        this.A01 = str;
    }
}
