package X;

/* renamed from: X.1BE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1BE {
    public final AnonymousClass10U A00;
    public final C242714w A01;
    public final C18470sV A02;
    public final C15860o1 A03;
    public final AnonymousClass1BD A04;

    public AnonymousClass1BE(AnonymousClass10U r1, C242714w r2, C18470sV r3, C15860o1 r4, AnonymousClass1BD r5) {
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
    }

    public C628238s A00(AbstractC33021d9 r8) {
        C18470sV r3 = this.A02;
        C15860o1 r4 = this.A03;
        return new C628238s(this.A00, this.A01, r3, r4, this.A04, r8);
    }
}
