package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/* renamed from: X.4xJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107424xJ implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(23);
    public final List A00;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107424xJ(List list) {
        boolean z;
        this.A00 = list;
        if (!list.isEmpty()) {
            long j = ((C100504m1) list.get(0)).A01;
            for (int i = 1; i < list.size(); i++) {
                if (((C100504m1) list.get(i)).A02 < j) {
                    z = true;
                    break;
                } else {
                    j = ((C100504m1) list.get(i)).A01;
                }
            }
        }
        z = false;
        C95314dV.A03(!z);
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || C107424xJ.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((C107424xJ) obj).A00);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.A00.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        return C12970iu.A0s(this.A00, C12960it.A0k("SlowMotion: segments="));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.A00);
    }
}
