package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.5kS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121905kS extends AbstractC121845k9 {
    public final /* synthetic */ C123475nD A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C121905kS(Context context, AnonymousClass018 r2, C17070qD r3, AnonymousClass61F r4, C123475nD r5) {
        super(context, r2, r3, r4);
        this.A00 = r5;
    }

    @Override // X.AbstractC1311861p
    public void ALx() {
        C123475nD r4 = this.A00;
        AnonymousClass60Y r3 = r4.A0J;
        AnonymousClass610 A03 = AnonymousClass610.A03("ADD_NEW_FI_CLICK", "ADD_MONEY", "REVIEW_TRANSACTION");
        String string = r4.A0F.A00.getString(R.string.novi_deposit_add_card_row_text);
        C128365vz r1 = A03.A00;
        r1.A0L = string;
        r1.A0i = "PAYMENT_METHODS";
        r3.A05(r1);
        C12960it.A1A(r4.A03, 125);
    }
}
