package X;

import android.view.View;
import android.view.ViewGroup;

/* renamed from: X.3kU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC75803kU extends AnonymousClass01A {
    public abstract Object A0G(ViewGroup viewGroup, int i);

    public abstract void A0I(ViewGroup viewGroup, Object obj, int i);

    public abstract boolean A0J(View view, Object obj);

    @Override // X.AnonymousClass01A
    @Deprecated
    public final int A02(Object obj) {
        return A0F(obj);
    }

    @Override // X.AnonymousClass01A
    @Deprecated
    public final Object A05(ViewGroup viewGroup, int i) {
        return A0G(viewGroup, i);
    }

    @Override // X.AnonymousClass01A
    @Deprecated
    public final void A0C(ViewGroup viewGroup, Object obj, int i) {
        A0H(viewGroup, obj, i);
    }

    @Override // X.AnonymousClass01A
    @Deprecated
    public final void A0D(ViewGroup viewGroup, Object obj, int i) {
        A0I(viewGroup, obj, i);
    }

    @Override // X.AnonymousClass01A
    @Deprecated
    public final boolean A0E(View view, Object obj) {
        return A0J(view, obj);
    }

    public int A0F(Object obj) {
        return -1;
    }

    public void A0H(ViewGroup viewGroup, Object obj, int i) {
    }
}
