package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.1UQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1UQ<K, V> extends LinkedHashMap<K, V> {
    public AnonymousClass1UQ() {
        super(100);
    }

    @Override // java.util.LinkedHashMap
    public boolean removeEldestEntry(Map.Entry entry) {
        return size() > 100;
    }
}
