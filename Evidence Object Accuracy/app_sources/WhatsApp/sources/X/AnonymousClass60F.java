package X;

import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.60F  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass60F {
    public static final ArrayList A09;
    public final long A00;
    public final AnonymousClass1V8 A01;
    public final AnonymousClass3EM A02;
    public final Long A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;

    static {
        String[] strArr = new String[3];
        strArr[0] = "BANK";
        strArr[1] = "PREPAID";
        A09 = C12960it.A0m("SUPPORT", strArr, 2);
    }

    public AnonymousClass60F(AbstractC15710nm r25, AnonymousClass1V8 r26, C126275sc r27) {
        AnonymousClass1V8.A01(r26, "iq");
        AnonymousClass1V8 r4 = r27.A00;
        Long A0j = C12970iu.A0j();
        Long A0k = C12970iu.A0k();
        AnonymousClass3JT.A04(null, r26, String.class, A0j, A0k, "br-get-merchant-status", new String[]{"account", "action"}, false);
        AnonymousClass3JT.A04(null, r26, String.class, A0j, A0k, "1", new String[]{"account", "status"}, false);
        this.A00 = C12980iv.A0G(AnonymousClass3JT.A04(null, r26, Long.TYPE, C117305Zk.A0f(), A0k, null, new String[]{"account", "verify-id"}, false));
        this.A03 = (Long) AnonymousClass3JT.A03(null, r26, Long.class, 1L, 100L, null, new String[]{"account", "bank-code"}, false);
        this.A04 = (String) AnonymousClass3JT.A03(null, r26, String.class, 1L, 1000L, null, new String[]{"account", "bank-name"}, false);
        this.A06 = (String) AnonymousClass3JT.A03(null, r26, String.class, 1L, 10L, null, new String[]{"account", "masked-account-number"}, false);
        this.A05 = (String) AnonymousClass3JT.A03(null, r26, String.class, 4L, 4L, null, new String[]{"account", "last4"}, false);
        this.A07 = (String) AnonymousClass3JT.A03(null, r26, String.class, 1L, 100L, null, new String[]{"account", "support-phone-number"}, false);
        this.A08 = AnonymousClass3JT.A09(r26, A09, new String[]{"account", "verify-type"});
        this.A02 = (AnonymousClass3EM) AnonymousClass3JT.A05(r26, new AbstractC116095Uc(r25, r4) { // from class: X.6DS
            public final /* synthetic */ AbstractC15710nm A00;
            public final /* synthetic */ AnonymousClass1V8 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC116095Uc
            public final Object A63(AnonymousClass1V8 r42) {
                return new AnonymousClass3EM(this.A00, r42, this.A01);
            }
        }, new String[0]);
        this.A01 = r26;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass60F.class != obj.getClass()) {
                return false;
            }
            AnonymousClass60F r7 = (AnonymousClass60F) obj;
            if (!this.A08.equals(r7.A08) || this.A00 != r7.A00 || !C29941Vi.A00(this.A03, r7.A03) || !C29941Vi.A00(this.A04, r7.A04) || !C29941Vi.A00(this.A06, r7.A06) || !C29941Vi.A00(this.A05, r7.A05) || !C29941Vi.A00(this.A07, r7.A07) || !this.A02.equals(r7.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[8];
        objArr[0] = this.A08;
        C117315Zl.A0Z(objArr, this.A00);
        objArr[2] = this.A03;
        objArr[3] = this.A04;
        objArr[4] = this.A06;
        objArr[5] = this.A05;
        objArr[6] = this.A07;
        objArr[7] = this.A02;
        return Arrays.hashCode(objArr);
    }
}
