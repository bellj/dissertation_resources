package X;

import android.view.WindowInsets;

/* renamed from: X.0Do  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02690Do extends C02700Dp {
    public C02690Do(C018408o r1, WindowInsets windowInsets) {
        super(r1, windowInsets);
    }

    @Override // X.C06250St
    public C06190Sn A06() {
        return C06190Sn.A00(this.A03.getDisplayCutout());
    }

    @Override // X.C06250St
    public C018408o A07() {
        return C018408o.A02(this.A03.consumeDisplayCutout());
    }

    @Override // X.C02710Dq, X.C06250St
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C02690Do)) {
            return false;
        }
        C02710Dq r4 = (C02710Dq) obj;
        if (!AnonymousClass08r.A00(this.A03, r4.A03) || !AnonymousClass08r.A00(((C02710Dq) this).A00, r4.A00)) {
            return false;
        }
        return true;
    }

    @Override // X.C06250St
    public int hashCode() {
        return this.A03.hashCode();
    }
}
