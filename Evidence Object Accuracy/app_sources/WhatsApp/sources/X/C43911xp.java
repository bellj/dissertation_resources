package X;

/* renamed from: X.1xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43911xp extends C43851xh {
    public final C43881xk A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;

    public C43911xp(C43881xk r1, String str, String str2, String str3, String str4, String str5) {
        super(str4);
        this.A04 = str;
        this.A03 = str2;
        this.A02 = str3;
        this.A01 = str5;
        this.A00 = r1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UserNoticeBanner{text='");
        sb.append(this.A04);
        sb.append('\'');
        sb.append(", lightUrl='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append(", lightIconFile='");
        sb.append(super.A01);
        sb.append('\'');
        sb.append(", darkUrl='");
        sb.append(this.A02);
        sb.append('\'');
        sb.append(", darkIconFile='");
        sb.append(super.A00);
        sb.append('\'');
        sb.append(", iconDescription='");
        sb.append(super.A02);
        sb.append('\'');
        sb.append(", action='");
        sb.append(this.A01);
        sb.append('\'');
        sb.append(", timing=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
