package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.protocol.VoipStanzaChildNode;

/* renamed from: X.3BV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BV {
    public final int A00;
    public final Jid A01;
    public final String A02;
    public final String A03;
    public final boolean A04;
    public final VoipStanzaChildNode[] A05;

    public AnonymousClass3BV(Jid jid, String str, String str2, VoipStanzaChildNode[] voipStanzaChildNodeArr, int i, boolean z) {
        if (C15380n4.A0I(jid)) {
            this.A02 = str;
            this.A01 = jid;
            this.A03 = str2;
            this.A00 = i;
            this.A04 = z;
            this.A05 = voipStanzaChildNodeArr;
            return;
        }
        throw C12970iu.A0f(C12960it.A0b("CallIncomingAck:Wrong jid type: ", jid));
    }
}
