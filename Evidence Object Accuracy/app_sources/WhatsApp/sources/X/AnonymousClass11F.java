package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/* renamed from: X.11F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11F {
    public final C14850m9 A00;

    public AnonymousClass11F(C14850m9 r1) {
        this.A00 = r1;
    }

    public Drawable A00(Resources.Theme theme, Resources resources, AbstractC469028d r5, int i) {
        if (this.A00.A07(1257)) {
            return new AnonymousClass2ZZ(theme, resources, r5, i);
        }
        return new C51842Za(theme, resources, r5, i);
    }
}
