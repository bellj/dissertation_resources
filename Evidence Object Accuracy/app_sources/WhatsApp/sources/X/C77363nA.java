package X;

import java.io.IOException;

/* renamed from: X.3nA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77363nA extends AnonymousClass495 {
    public C77363nA(AnonymousClass3H3 r2, IOException iOException) {
        super(r2, iOException, "Cleartext HTTP traffic not permitted. See https://exoplayer.dev/issues/cleartext-not-permitted");
    }
}
