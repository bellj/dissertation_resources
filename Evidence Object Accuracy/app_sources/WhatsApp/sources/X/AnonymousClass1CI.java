package X;

/* renamed from: X.1CI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CI {
    public AnonymousClass21S A00;
    public final C14900mE A01;
    public final C16170oZ A02;
    public final C15610nY A03;
    public final C18640sm A04;
    public final AnonymousClass01d A05;
    public final C14830m7 A06;
    public final C16590pI A07;
    public final AnonymousClass018 A08;
    public final C14950mJ A09;
    public final C20830wO A0A;
    public final C22180yf A0B;
    public final C16120oU A0C;
    public final AnonymousClass109 A0D;
    public final AnonymousClass1CH A0E;
    public final C22370yy A0F;
    public final AbstractC14440lR A0G;

    public AnonymousClass1CI(C14900mE r2, C16170oZ r3, C15610nY r4, C18640sm r5, AnonymousClass01d r6, C14830m7 r7, C16590pI r8, AnonymousClass018 r9, C14950mJ r10, C20830wO r11, C22180yf r12, C16120oU r13, AnonymousClass109 r14, AnonymousClass1CH r15, C22370yy r16, AbstractC14440lR r17) {
        this.A07 = r8;
        this.A06 = r7;
        this.A01 = r2;
        this.A0G = r17;
        this.A0C = r13;
        this.A02 = r3;
        this.A09 = r10;
        this.A0B = r12;
        this.A05 = r6;
        this.A03 = r4;
        this.A08 = r9;
        this.A0E = r15;
        this.A0D = r14;
        this.A0F = r16;
        this.A04 = r5;
        this.A0A = r11;
    }
}
