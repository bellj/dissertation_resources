package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import com.whatsapp.inappsupport.ui.ContactUsActivity;

/* renamed from: X.3Ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66473Ns implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ ScrollView A01;
    public final /* synthetic */ ContactUsActivity A02;

    public ViewTreeObserver$OnPreDrawListenerC66473Ns(View view, ScrollView scrollView, ContactUsActivity contactUsActivity) {
        this.A02 = contactUsActivity;
        this.A00 = view;
        this.A01 = scrollView;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        View view = this.A00;
        int height = view.getHeight();
        ScrollView scrollView = this.A01;
        scrollView.setPadding(scrollView.getPaddingLeft(), scrollView.getPaddingTop(), scrollView.getPaddingRight(), scrollView.getPaddingBottom() + height);
        C12980iv.A1G(view, this);
        return false;
    }
}
