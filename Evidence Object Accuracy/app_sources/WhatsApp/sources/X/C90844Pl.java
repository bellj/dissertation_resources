package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.4Pl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90844Pl {
    public final float A00;
    public final C16590pI A01;
    public final C14850m9 A02;

    public C90844Pl(C16590pI r5, C14850m9 r6) {
        this.A02 = r6;
        this.A01 = r5;
        Context context = r5.A00;
        this.A00 = (float) (context.getResources().getDimensionPixelSize(R.dimen.product_catalog_list_thumb_size) + (context.getResources().getDimensionPixelSize(R.dimen.product_catalog_list_thumb_margin_vertical) << 1));
    }
}
