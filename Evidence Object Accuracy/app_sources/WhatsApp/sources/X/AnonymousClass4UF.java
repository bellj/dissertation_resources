package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.components.SelectionCheckView;

/* renamed from: X.4UF  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4UF {
    public final View A00;
    public final View A01;
    public final ImageView A02;
    public final ImageView A03;
    public final ImageView A04;
    public final ImageView A05;
    public final ImageView A06;
    public final LinearLayout A07;
    public final TextView A08;
    public final TextView A09;
    public final TextView A0A;
    public final TextView A0B;
    public final TextEmojiLabel A0C;
    public final C28801Pb A0D;
    public final SelectionCheckView A0E;

    public AnonymousClass4UF(View view, View view2, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, ImageView imageView5, LinearLayout linearLayout, TextView textView, TextView textView2, TextView textView3, TextView textView4, TextEmojiLabel textEmojiLabel, C28801Pb r14, SelectionCheckView selectionCheckView) {
        this.A05 = imageView;
        this.A03 = imageView2;
        this.A01 = view;
        this.A0D = r14;
        this.A0C = textEmojiLabel;
        this.A0B = textView;
        this.A02 = imageView3;
        this.A06 = imageView4;
        this.A08 = textView2;
        this.A0E = selectionCheckView;
        this.A09 = textView3;
        this.A07 = linearLayout;
        this.A0A = textView4;
        this.A04 = imageView5;
        this.A00 = view2;
    }
}
