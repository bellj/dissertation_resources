package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0100000_I0_5;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.0q3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16970q3 {
    public final C15570nT A00;
    public final C241614l A01;
    public final C14820m6 A02;
    public final C19990v2 A03;
    public final C15680nj A04;
    public final C16120oU A05;
    public final C22230yk A06;
    public final AbstractC14440lR A07;

    public C16970q3(C15570nT r1, C241614l r2, C14820m6 r3, C19990v2 r4, C15680nj r5, C16120oU r6, C22230yk r7, AbstractC14440lR r8) {
        this.A00 = r1;
        this.A07 = r8;
        this.A03 = r4;
        this.A05 = r6;
        this.A06 = r7;
        this.A02 = r3;
        this.A04 = r5;
        this.A01 = r2;
    }

    public static void A00(Context context, View view, C16170oZ r7, AbstractC14640lm r8) {
        C34271fr A00 = C34271fr.A00(view, context.getString(R.string.archived_chats_stay_muted), 0);
        A00.A07(context.getString(R.string.unarchive_button), new ViewOnClickCListenerShape4S0200000_I0(r7, 20, r8));
        A00.A06(AnonymousClass00T.A00(context, R.color.snackbarButton));
        ((TextView) A00.A05.findViewById(R.id.snackbar_text)).setSingleLine(false);
        A00.A03();
    }

    public static boolean A01(C14820m6 r2) {
        SharedPreferences sharedPreferences = r2.A00;
        return sharedPreferences.getBoolean("archive_v2_enabled", false) && !sharedPreferences.getBoolean("notify_new_message_for_archived_chats", false);
    }

    public static boolean A02(C14820m6 r2) {
        SharedPreferences sharedPreferences = r2.A00;
        return sharedPreferences.getBoolean("archive_v2_enabled", false) && !sharedPreferences.getBoolean("notify_new_message_for_archived_chats", false);
    }

    public static boolean A03(C14820m6 r0, C19990v2 r1, AbstractC14640lm r2) {
        return A01(r0) && r1.A0E(r2);
    }

    public void A04() {
        this.A00.A08();
        this.A02.A00.edit().putBoolean("archive_v2_enabled", true).apply();
        A06(false);
    }

    public void A05(boolean z) {
        this.A00.A08();
        this.A07.Ab2(new RunnableBRunnable0Shape0S0110000_I0(this, 5, z));
        A06(z);
        C16120oU r2 = this.A05;
        C34301fv r1 = new C34301fv();
        r1.A00 = Boolean.valueOf(!z);
        r2.A07(r1);
    }

    public final void A06(boolean z) {
        this.A02.A00.edit().putBoolean("notify_new_message_for_archived_chats", z).apply();
        this.A07.Ab2(new RunnableBRunnable0Shape5S0100000_I0_5(this.A01, 16));
    }
}
