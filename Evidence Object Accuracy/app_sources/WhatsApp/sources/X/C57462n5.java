package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57462n5 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57462n5 A05;
    public static volatile AnonymousClass255 A06;
    public int A00;
    public int A01;
    public C43261wh A02;
    public String A03 = "";
    public String A04 = "";

    static {
        C57462n5 r0 = new C57462n5();
        A05 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        C81603uH r1;
        switch (r7.ordinal()) {
            case 0:
                return A05;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57462n5 r9 = (C57462n5) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A04;
                int i2 = r9.A00;
                this.A04 = r8.Afy(str, r9.A04, A1R, C12960it.A1R(i2));
                this.A03 = r8.Afy(this.A03, r9.A03, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A02 = (C43261wh) r8.Aft(this.A02, r9.A02);
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 8, 8);
                int i4 = this.A01;
                int i5 = r9.A00;
                this.A01 = r8.Afp(i4, r9.A01, A1V, C12960it.A1V(i5 & 8, 8));
                if (r8 == C463025i.A00) {
                    this.A00 = i3 | i5;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                AnonymousClass254 r92 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        if (A03 == 0) {
                            break;
                        } else if (A03 == 10) {
                            String A0A = r82.A0A();
                            this.A00 = 1 | this.A00;
                            this.A04 = A0A;
                        } else if (A03 == 18) {
                            String A0A2 = r82.A0A();
                            this.A00 |= 2;
                            this.A03 = A0A2;
                        } else if (A03 == 26) {
                            if ((this.A00 & 4) == 4) {
                                r1 = (C81603uH) this.A02.A0T();
                            } else {
                                r1 = null;
                            }
                            C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r82, r92, C43261wh.A0O);
                            this.A02 = r0;
                            if (r1 != null) {
                                this.A02 = (C43261wh) AbstractC27091Fz.A0C(r1, r0);
                            }
                            this.A00 |= 4;
                        } else if (A03 == 32) {
                            this.A00 |= 8;
                            this.A01 = r82.A02();
                        } else if (!A0a(r82, A03)) {
                            break;
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57462n5();
            case 5:
                return new C82353vU();
            case 6:
                break;
            case 7:
                if (A06 == null) {
                    synchronized (C57462n5.class) {
                        if (A06 == null) {
                            A06 = AbstractC27091Fz.A09(A05);
                        }
                    }
                }
                return A06;
            default:
                throw C12970iu.A0z();
        }
        return A05;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A04, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A03, i2);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A02;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 3, i2);
        }
        if ((this.A00 & 8) == 8) {
            i2 = AbstractC27091Fz.A02(4, this.A01, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A04);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A03);
        }
        if ((this.A00 & 4) == 4) {
            C43261wh r0 = this.A02;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
