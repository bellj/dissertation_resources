package X;

/* renamed from: X.4YO  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YO {
    public int flags;

    public static boolean A00(AnonymousClass4YO r1) {
        return r1.getFlag(4);
    }

    public final void addFlag(int i) {
        this.flags = i | this.flags;
    }

    public void clear() {
        this.flags = 0;
    }

    public final void clearFlag(int i) {
        this.flags = (i ^ -1) & this.flags;
    }

    public final boolean getFlag(int i) {
        return C12960it.A1V(this.flags & i, i);
    }

    public final boolean hasSupplementalData() {
        return C12960it.A1V(this.flags & 268435456, 268435456);
    }

    public final boolean isDecodeOnly() {
        return C12960it.A1V(this.flags & Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    public final boolean isEndOfStream() {
        return A00(this);
    }

    public final boolean isKeyFrame() {
        return C12960it.A1V(this.flags & 1, 1);
    }

    public final void setFlags(int i) {
        this.flags = i;
    }
}
