package X;

/* renamed from: X.5MI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MI extends AnonymousClass1TM {
    public AnonymousClass5MG A00;
    public AnonymousClass5MV A01;

    public AnonymousClass5MI(AbstractC114775Na r3) {
        AnonymousClass1TN A00 = AbstractC114775Na.A00(r3);
        this.A00 = A00 instanceof AnonymousClass5MG ? (AnonymousClass5MG) A00 : A00 != null ? new AnonymousClass5MG(AnonymousClass5ND.A00(A00)) : null;
        if (r3.A0B() == 2) {
            AbstractC114775Na A05 = AbstractC114775Na.A05((AnonymousClass5NU) r3.A0D(1), true);
            this.A01 = A05 != null ? new AnonymousClass5MV(AbstractC114775Na.A04((Object) A05)) : null;
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        A00.A06(this.A00);
        AnonymousClass5MV r2 = this.A01;
        if (r2 != null) {
            C94954co.A02(r2, A00, 0, true);
        }
        return new AnonymousClass5NZ(A00);
    }
}
