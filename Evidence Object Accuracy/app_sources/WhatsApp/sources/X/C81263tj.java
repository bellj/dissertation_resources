package X;

import com.google.common.collect.Multisets;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.3tj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81263tj extends Multisets.ElementSet<E> {
    public final /* synthetic */ AbstractC113525Hx this$0;

    public C81263tj() {
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public C81263tj(AbstractC113525Hx r1) {
        this();
        this.this$0 = r1;
    }

    public void clear() {
        multiset().clear();
    }

    public boolean contains(Object obj) {
        return multiset().contains(obj);
    }

    public boolean containsAll(Collection collection) {
        return multiset().containsAll(collection);
    }

    public boolean isEmpty() {
        return multiset().isEmpty();
    }

    public Iterator iterator() {
        return this.this$0.elementIterator();
    }

    public AnonymousClass5Z2 multiset() {
        return this.this$0;
    }

    public boolean remove(Object obj) {
        return C12960it.A1U(multiset().remove(obj, Integer.MAX_VALUE));
    }

    public int size() {
        return multiset().entrySet().size();
    }
}
