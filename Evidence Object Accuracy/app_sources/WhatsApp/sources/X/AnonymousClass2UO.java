package X;

import android.content.res.Resources;
import android.view.animation.Animation;
import com.whatsapp.R;

/* renamed from: X.2UO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UO extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ AnonymousClass2U3 A00;
    public final /* synthetic */ boolean A01;

    public AnonymousClass2UO(AnonymousClass2U3 r1, boolean z) {
        this.A00 = r1;
        this.A01 = z;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        AnonymousClass2U3 r3 = this.A00;
        boolean z = this.A01;
        Resources resources = r3.A03.getContext().getResources();
        int i = R.string.switch_to_front_camera;
        if (z) {
            i = R.string.switch_to_back_camera;
        }
        r3.A0C.setContentDescription(resources.getString(i));
    }
}
