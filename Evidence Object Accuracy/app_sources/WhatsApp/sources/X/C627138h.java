package X;

import android.os.SystemClock;
import java.lang.ref.WeakReference;

/* renamed from: X.38h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C627138h extends AbstractC16350or {
    public final long A00 = SystemClock.elapsedRealtime();
    public final C16170oZ A01;
    public final AnonymousClass1P3 A02;
    public final C254119h A03;
    public final C15370n3 A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final WeakReference A08;
    public final WeakReference A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;

    public C627138h(ActivityC000800j r4, AbstractC13860kS r5, C16170oZ r6, AnonymousClass1P3 r7, C254119h r8, C15370n3 r9, String str, String str2, String str3, boolean z, boolean z2) {
        this.A0B = z;
        this.A0C = z2;
        this.A08 = C12970iu.A10(r4);
        this.A09 = C12970iu.A10(r5);
        this.A01 = r6;
        this.A03 = r8;
        this.A0A = true;
        this.A04 = r9;
        this.A06 = str;
        this.A07 = str2;
        this.A05 = str3;
        this.A02 = r7;
    }
}
