package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

/* renamed from: X.1DR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DR {
    public final AbstractC15710nm A00;
    public final C16590pI A01;
    public final C16120oU A02;

    public AnonymousClass1DR(AbstractC15710nm r1, C16590pI r2, C16120oU r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public static void A00(Context context, String str, String str2, Map map, Map map2) {
        if (str != null && map.containsKey(str)) {
            A01(context, (File) map.get(str), "tombstone", str2, map2);
        }
    }

    public static boolean A01(Context context, File file, String str, String str2, Map map) {
        try {
            File A04 = C14350lI.A04(file, new File(context.getCacheDir(), "crash_upload"), file.getName());
            StringBuilder sb = new StringBuilder();
            if (A04 == null) {
                sb.append(str2);
                sb.append("/compress/empty; exit");
                Log.w(sb.toString());
                return false;
            }
            sb.append(str2);
            sb.append("/upload/attachment file: ");
            sb.append(A04.getAbsolutePath());
            Log.i(sb.toString());
            map.put(str, A04.getPath());
            return true;
        } catch (IOException e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append("/compress/fail; exit");
            Log.w(sb2.toString(), e);
            return false;
        }
    }

    public void A02(HashSet hashSet, Map map, boolean z) {
        this.A00.A03(hashSet, map, z, false, false, false);
        for (String str : map.values()) {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
        }
    }
}
