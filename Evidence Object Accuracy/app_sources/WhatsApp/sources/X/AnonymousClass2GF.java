package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/* renamed from: X.2GF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2GF extends AnonymousClass2GG {
    public boolean A00 = true;
    public final AnonymousClass018 A01;

    public AnonymousClass2GF(Drawable drawable, AnonymousClass018 r3) {
        super(drawable, r3.A04().A06);
        this.A01 = r3;
    }

    public static AnonymousClass2GF A00(Context context, AnonymousClass018 r2, int i) {
        return new AnonymousClass2GF(AnonymousClass00T.A04(context, i), r2);
    }

    public static void A01(Context context, ImageView imageView, AnonymousClass018 r3, int i) {
        imageView.setImageDrawable(new AnonymousClass2GF(AnonymousClass00T.A04(context, i), r3));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r2.A01.A04().A06 == false) goto L_0x000f;
     */
    @Override // X.AnonymousClass2GG, android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r3) {
        /*
            r2 = this;
            boolean r0 = r2.A00
            if (r0 == 0) goto L_0x000f
            X.018 r0 = r2.A01
            X.1Kv r0 = r0.A04()
            boolean r0 = r0.A06
            r1 = 1
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r1 = 0
        L_0x0010:
            boolean r0 = r2.A00
            if (r0 == r1) goto L_0x0019
            r2.A00 = r1
            r2.invalidateSelf()
        L_0x0019:
            super.draw(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2GF.draw(android.graphics.Canvas):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r2.A01.A04().A06 == false) goto L_0x000f;
     */
    @Override // X.AnonymousClass2GG, android.graphics.drawable.InsetDrawable, android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getPadding(android.graphics.Rect r3) {
        /*
            r2 = this;
            boolean r0 = r2.A00
            if (r0 == 0) goto L_0x000f
            X.018 r0 = r2.A01
            X.1Kv r0 = r0.A04()
            boolean r0 = r0.A06
            r1 = 1
            if (r0 != 0) goto L_0x0010
        L_0x000f:
            r1 = 0
        L_0x0010:
            boolean r0 = r2.A00
            if (r0 == r1) goto L_0x0019
            r2.A00 = r1
            r2.invalidateSelf()
        L_0x0019:
            boolean r0 = super.getPadding(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2GF.getPadding(android.graphics.Rect):boolean");
    }
}
