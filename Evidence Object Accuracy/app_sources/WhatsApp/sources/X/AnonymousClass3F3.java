package X;

/* renamed from: X.3F3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3F3 {
    public final int A00;
    public final C16120oU A01;

    public AnonymousClass3F3(C16120oU r1, int i) {
        this.A00 = i;
        this.A01 = r1;
    }

    public void A00() {
        AnonymousClass30L r1 = new AnonymousClass30L();
        r1.A01 = Integer.valueOf(this.A00);
        r1.A02 = C12970iu.A0h();
        r1.A00 = Boolean.FALSE;
        this.A01.A07(r1);
    }

    public void A01() {
        AnonymousClass30L r1 = new AnonymousClass30L();
        r1.A01 = Integer.valueOf(this.A00);
        r1.A02 = C12990iw.A0j();
        r1.A00 = Boolean.FALSE;
        this.A01.A07(r1);
    }

    public void A02(boolean z) {
        AnonymousClass30L r1 = new AnonymousClass30L();
        r1.A01 = Integer.valueOf(this.A00);
        r1.A02 = C12970iu.A0g();
        r1.A00 = Boolean.valueOf(z);
        this.A01.A07(r1);
    }
}
