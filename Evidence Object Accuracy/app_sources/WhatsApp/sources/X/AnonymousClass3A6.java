package X;

import android.util.Log;

@Deprecated
/* renamed from: X.3A6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3A6 {
    public static void A00(String str, Object obj) {
        C56582lF r0 = C56582lF.A00;
        if (r0 != null) {
            r0.A0C(str, obj);
            return;
        }
        if (obj != null) {
            String str2 = (String) obj;
            StringBuilder A0t = C12980iv.A0t(str.length() + 1 + str2.length());
            A0t.append(str);
            A0t.append(":");
            str = C12960it.A0d(str2, A0t);
        }
        Log.e((String) C88904Hw.A0K.A00(), str);
    }
}
