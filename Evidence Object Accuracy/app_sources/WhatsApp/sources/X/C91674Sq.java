package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4Sq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class C91674Sq {
    public final /* synthetic */ UserJid A00;
    public final /* synthetic */ AbstractC38141na A01;
    public final /* synthetic */ AnonymousClass5WB A02;
    public final /* synthetic */ AnonymousClass3FP A03;
    public final /* synthetic */ String A04;

    public /* synthetic */ C91674Sq(UserJid userJid, AbstractC38141na r2, AnonymousClass5WB r3, AnonymousClass3FP r4, String str) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = userJid;
        this.A04 = str;
    }
}
