package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.2e9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53362e9 extends AnonymousClass07N {
    public final Context A00;
    public final AnonymousClass2JG A01;
    public final AbstractC15340mz A02;
    public final C248016x A03;
    public final AnonymousClass1BD A04;
    public final AnonymousClass4LL A05;
    public final AnonymousClass01H A06;

    public C53362e9(Context context, View view, AnonymousClass2JG r9, AbstractC15340mz r10, C248016x r11, AnonymousClass1BD r12, AnonymousClass4LL r13, AnonymousClass01H r14) {
        super(context, view);
        this.A00 = context;
        this.A01 = r9;
        this.A03 = r11;
        this.A04 = r12;
        this.A05 = r13;
        this.A02 = r10;
        this.A06 = r14;
        AnonymousClass07H r5 = super.A04;
        ArrayList A0l = C12960it.A0l();
        C12970iu.A1T(Integer.valueOf((int) R.id.menuitem_forward), context.getString(R.string.menuitem_status_forward), A0l);
        C12970iu.A1T(Integer.valueOf((int) R.id.menuitem_share_status_third_party), context.getString(R.string.menuitem_status_share), A0l);
        if (this.A03.A01()) {
            C12970iu.A1T(Integer.valueOf((int) R.id.menuitem_share_status_facebook), context.getString(R.string.menuitem_status_share_with_fb), A0l);
        }
        C12970iu.A1T(Integer.valueOf((int) R.id.menuitem_delete), context.getString(R.string.menuitem_status_delete), A0l);
        for (int i = 0; i < A0l.size(); i++) {
            AnonymousClass01T r1 = (AnonymousClass01T) A0l.get(i);
            Object obj = r1.A00;
            AnonymousClass009.A05(obj);
            Object obj2 = r1.A01;
            AnonymousClass009.A05(obj2);
            r5.add(0, ((Number) obj).intValue(), 0, (String) obj2);
        }
    }
}
