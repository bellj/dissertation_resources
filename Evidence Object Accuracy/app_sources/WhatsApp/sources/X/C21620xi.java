package X;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21620xi {
    public final C19990v2 A00;
    public final AnonymousClass1YX A01 = new AnonymousClass1YX();
    public final Map A02 = new ConcurrentHashMap();
    public final AtomicBoolean A03 = new AtomicBoolean();

    public C21620xi(C19990v2 r2) {
        this.A00 = r2;
    }

    public void A00(AnonymousClass1YZ r4) {
        Collection<AnonymousClass1PE> values;
        AnonymousClass1YX r2 = this.A01;
        synchronized (r2) {
            for (AbstractC15340mz r0 : r2.A01.A05().values()) {
                r4.AfJ(r0);
            }
            for (Map.Entry entry : r2.A02.entrySet()) {
                AbstractC15340mz r02 = (AbstractC15340mz) ((WeakReference) entry.getValue()).get();
                if (r02 != null) {
                    r4.AfJ(r02);
                }
            }
        }
        for (AbstractC15340mz r03 : this.A02.values()) {
            r4.AfJ(r03);
        }
        C19990v2 r1 = this.A00;
        synchronized (r1) {
            values = r1.A0B().values();
        }
        for (AnonymousClass1PE r04 : values) {
            AbstractC15340mz r05 = r04.A0a;
            if (r05 != null) {
                r4.AfJ(r05);
            }
        }
    }

    public void A01(AbstractC14640lm r7) {
        AnonymousClass1YX r5 = this.A01;
        synchronized (r5) {
            C006202y r3 = r5.A01;
            Iterator it = new HashSet(r3.A05().keySet()).iterator();
            while (it.hasNext()) {
                AnonymousClass1IS r1 = (AnonymousClass1IS) it.next();
                if (r7.equals(r1.A00)) {
                    r3.A07(r1);
                }
            }
            ArrayList arrayList = new ArrayList();
            Map map = r5.A02;
            for (AnonymousClass1IS r12 : map.keySet()) {
                if (r7.equals(r12.A00)) {
                    arrayList.add(r12);
                }
            }
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                map.remove((AnonymousClass1IS) it2.next());
            }
        }
        Map map2 = this.A02;
        Iterator it3 = new HashSet(map2.keySet()).iterator();
        while (it3.hasNext()) {
            AnonymousClass1IS r13 = (AnonymousClass1IS) it3.next();
            if (r7.equals(r13.A00)) {
                map2.remove(r13);
            }
        }
        AnonymousClass1PE A06 = this.A00.A06(r7);
        if (A06 != null) {
            A06.A0a = null;
            A06.A0Z = null;
        }
    }

    public void A02(AbstractC15340mz r5) {
        AnonymousClass1YX r0 = this.A01;
        AnonymousClass1IS r3 = r5.A0z;
        r0.A00(r5, r3);
        C19990v2 r2 = this.A00;
        synchronized (r2) {
            AnonymousClass1PE A06 = r2.A06(r3.A00);
            if (A06 != null) {
                AbstractC15340mz r02 = A06.A0a;
                if (r02 != null && r02.A0z.equals(r3)) {
                    A06.A0a = r5;
                }
                AbstractC15340mz r03 = A06.A0Z;
                if (r03 != null && r03.A0z.equals(r3)) {
                    A06.A0Z = r5;
                }
            }
        }
    }

    public void A03(AnonymousClass1IS r5) {
        AnonymousClass1YX r1 = this.A01;
        synchronized (r1) {
            r1.A02.remove(r5);
            r1.A01.A07(r5);
        }
        this.A02.remove(r5);
        C19990v2 r3 = this.A00;
        synchronized (r3) {
            AnonymousClass1PE A06 = r3.A06(r5.A00);
            if (A06 != null) {
                AbstractC15340mz r0 = A06.A0a;
                if (r0 != null && r0.A0z.equals(r5)) {
                    A06.A0a = null;
                }
                AbstractC15340mz r02 = A06.A0Z;
                if (r02 != null && r02.A0z.equals(r5)) {
                    A06.A0Z = null;
                }
                AnonymousClass1YY r03 = A06.A0c;
                if (r03 != null && r03.A01.A0z.equals(r5)) {
                    A06.A0c = null;
                }
            }
        }
    }
}
