package X;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.whatsapp.util.Log;
import com.whatsapp.workers.ntp.NtpSyncWorker;

/* renamed from: X.0vU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20270vU extends AbstractC18220s6 {
    public final AnonymousClass01d A00;
    public final C14830m7 A01;
    public final C16590pI A02;
    public final C20250vS A03;
    public final C14850m9 A04;
    public final C17060qC A05;

    public C20270vU(Context context, AnonymousClass01d r2, C14830m7 r3, C16590pI r4, C20250vS r5, C14850m9 r6, C17060qC r7) {
        super(context);
        this.A01 = r3;
        this.A04 = r6;
        this.A03 = r5;
        this.A02 = r4;
        this.A00 = r2;
        this.A05 = r7;
    }

    public final void A02(Intent intent) {
        PowerManager.WakeLock wakeLock;
        StringBuilder sb = new StringBuilder("NtpAction#updateNtp; intent=");
        sb.append(intent);
        Log.i(sb.toString());
        PowerManager A0I = this.A00.A0I();
        if (A0I == null) {
            Log.w("NtpAction/updateNtp pm=null");
            wakeLock = null;
        } else {
            wakeLock = C39151pN.A00(A0I, "NtpAction#updateNtp", 1);
            wakeLock.setReferenceCounted(false);
            wakeLock.acquire(300000);
        }
        try {
            C17060qC r3 = this.A05;
            NtpSyncWorker.A00(this.A02.A00, this.A01, this.A03, r3);
        } finally {
            if (wakeLock != null) {
                wakeLock.release();
            }
        }
    }
}
