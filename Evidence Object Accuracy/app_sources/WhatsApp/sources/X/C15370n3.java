package X;

import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.0n3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15370n3 implements Cloneable {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public long A07;
    public long A08 = -1;
    public long A09;
    public long A0A;
    public long A0B;
    public C28811Pc A0C;
    public Jid A0D;
    public UserJid A0E;
    public C29951Vj A0F;
    public AnonymousClass1PD A0G;
    public Integer A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public String A0N;
    public String A0O;
    public String A0P;
    public String A0Q;
    public String A0R;
    public String A0S;
    public String A0T;
    public String A0U;
    public Locale A0V;
    public boolean A0W;
    public boolean A0X = true;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e;
    @Deprecated
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j;

    public C15370n3(C28811Pc r3, String str, String str2, int i) {
        this.A0C = r3;
        this.A0K = str;
        this.A0H = Integer.valueOf(i);
        this.A0P = str2;
    }

    public C15370n3(Jid jid) {
        this.A0D = jid;
        this.A0f = true;
        this.A0C = null;
        if (C15380n4.A0J(jid)) {
            this.A0G = AnonymousClass1PD.A04;
        }
    }

    public C15370n3(Jid jid, String str, String str2, String str3, int i, long j, boolean z) {
        int length;
        this.A0D = jid;
        this.A0f = z;
        this.A0K = str2;
        if ((j > 0 || j == -2) && str != null && 5 <= (length = str.length()) && length <= 20) {
            this.A0C = new C28811Pc(str, j);
        }
        this.A0H = Integer.valueOf(i);
        this.A0P = str3;
    }

    public static int A00(C15370n3 r1, Object obj) {
        if (obj == AnonymousClass1JA.A05) {
            return r1.A04;
        }
        return r1.A05;
    }

    public static AbstractC14640lm A01(C15370n3 r1) {
        Jid A0B = r1.A0B(AbstractC14640lm.class);
        AnonymousClass009.A05(A0B);
        return (AbstractC14640lm) A0B;
    }

    public static AbstractC14640lm A02(C15370n3 r1) {
        return (AbstractC14640lm) r1.A0B(AbstractC14640lm.class);
    }

    public static Jid A03(C15370n3 r0, Class cls) {
        Jid A0B = r0.A0B(cls);
        AnonymousClass009.A05(A0B);
        return A0B;
    }

    public static UserJid A04(C15370n3 r1) {
        Jid A0B = r1.A0B(UserJid.class);
        AnonymousClass009.A05(A0B);
        return (UserJid) A0B;
    }

    public static UserJid A05(C15370n3 r1) {
        return (UserJid) r1.A0B(UserJid.class);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004d, code lost:
        if (android.text.TextUtils.equals(r4.A0D(), r2.A0D()) == false) goto L_0x004f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A06(X.AbstractC29911Vf r8, java.util.List r9) {
        /*
            r7 = 0
            if (r9 == 0) goto L_0x00c0
            java.util.Iterator r6 = r9.iterator()
        L_0x0007:
            r7 = 0
        L_0x0008:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x00c0
            java.lang.Object r2 = r6.next()
            X.0n3 r2 = (X.C15370n3) r2
            X.0n3 r4 = r8.A00
            if (r2 == r4) goto L_0x0024
            com.whatsapp.jid.Jid r1 = r4.A0D
            if (r1 != 0) goto L_0x0026
            java.lang.String r0 = "wacontact/updatecontact/invalid"
            com.whatsapp.util.Log.e(r0)
        L_0x0022:
            if (r7 == 0) goto L_0x0007
        L_0x0024:
            r7 = 1
            goto L_0x0008
        L_0x0026:
            com.whatsapp.jid.Jid r0 = r2.A0D
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0022
            boolean r0 = r8 instanceof X.C29921Vg
            if (r0 != 0) goto L_0x00ad
            boolean r0 = r8 instanceof X.C29931Vh
            if (r0 != 0) goto L_0x0073
            java.lang.String r1 = r4.A0K
            java.lang.String r0 = r2.A0K
            boolean r0 = android.text.TextUtils.equals(r1, r0)
            if (r0 == 0) goto L_0x004f
            java.lang.String r1 = r4.A0D()
            java.lang.String r0 = r2.A0D()
            boolean r0 = android.text.TextUtils.equals(r1, r0)
            r5 = 0
            if (r0 != 0) goto L_0x0050
        L_0x004f:
            r5 = 1
        L_0x0050:
            java.lang.String r0 = r4.A0K
            r2.A0K = r0
            java.lang.String r0 = r4.A0I
            r2.A0I = r0
            java.lang.String r1 = r4.A0D()
            boolean r0 = r2 instanceof X.C29861Va
            if (r0 != 0) goto L_0x006d
            r2.A0T = r1
        L_0x0062:
            java.util.Locale r0 = r4.A0V
            r2.A0V = r0
            int r0 = r4.A06
            r2.A06 = r0
        L_0x006a:
            if (r5 == 0) goto L_0x0022
            goto L_0x0024
        L_0x006d:
            java.lang.String r0 = "Setting verified name for ServerContact not allowed"
            X.AnonymousClass009.A07(r0)
            goto L_0x0062
        L_0x0073:
            boolean r0 = r4.A0X
            r2.A0X = r0
            int r3 = r4.A04
            if (r3 <= 0) goto L_0x007f
            int r0 = r2.A04
            if (r0 != r3) goto L_0x009f
        L_0x007f:
            int r1 = r4.A05
            if (r1 <= 0) goto L_0x0087
            int r0 = r2.A05
            if (r0 != r1) goto L_0x009f
        L_0x0087:
            if (r3 != 0) goto L_0x008d
            int r0 = r2.A04
            if (r0 != 0) goto L_0x009f
        L_0x008d:
            if (r1 != 0) goto L_0x0093
            int r0 = r2.A05
            if (r0 != 0) goto L_0x009f
        L_0x0093:
            if (r3 >= 0) goto L_0x0099
            int r0 = r2.A04
            if (r0 > 0) goto L_0x009f
        L_0x0099:
            if (r1 >= 0) goto L_0x00ab
            int r0 = r2.A05
            if (r0 <= 0) goto L_0x00ab
        L_0x009f:
            r5 = 1
        L_0x00a0:
            r2.A04 = r3
            int r0 = r4.A05
            r2.A05 = r0
            long r0 = r4.A0A
            r2.A0A = r0
            goto L_0x006a
        L_0x00ab:
            r5 = 0
            goto L_0x00a0
        L_0x00ad:
            java.lang.String r1 = r4.A0R
            java.lang.String r0 = r2.A0R
            boolean r0 = android.text.TextUtils.equals(r1, r0)
            r5 = r0 ^ 1
            java.lang.String r0 = r4.A0R
            r2.A0R = r0
            long r0 = r4.A0B
            r2.A0B = r0
            goto L_0x006a
        L_0x00c0:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15370n3.A06(X.1Vf, java.util.List):boolean");
    }

    public static boolean A07(C15370n3 r1, Set set) {
        return set.contains(r1.A0B(UserJid.class));
    }

    public long A08() {
        if (!(this instanceof C29861Va)) {
            return this.A08;
        }
        return -2;
    }

    public C15370n3 A09() {
        try {
            Object clone = super.clone();
            if (clone instanceof C15370n3) {
                return (C15370n3) clone;
            }
        } catch (CloneNotSupportedException unused) {
        }
        return null;
    }

    public Jid A0A() {
        return this.A0D;
    }

    public Jid A0B(Class cls) {
        if (cls.isInstance(this.A0D)) {
            return (Jid) cls.cast(this.A0D);
        }
        return null;
    }

    public String A0C() {
        C28811Pc r3 = this.A0C;
        if (r3 == null) {
            return C15380n4.A03(this.A0D);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(r3.A00);
        sb.append(":");
        sb.append(r3.A01);
        return sb.toString();
    }

    public String A0D() {
        if (!(this instanceof C29861Va)) {
            return this.A0T;
        }
        return ((C29861Va) this).A00.A09(R.string.whatsapp_name);
    }

    public String A0E(float f, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(A0C());
        sb.append("_");
        sb.append(i);
        sb.append("_");
        sb.append(f);
        return sb.toString();
    }

    public void A0F(long j) {
        if (!(this instanceof C29861Va)) {
            this.A08 = j;
            return;
        }
        StringBuilder sb = new StringBuilder("Attempting to set the id of the server contact to=");
        sb.append(j);
        Log.e(sb.toString());
    }

    public void A0G(AbstractC15710nm r8, C29951Vj r9) {
        String str;
        if (r9 != null && r9.A00 == 2 && r9.A01 == null) {
            Locale locale = Locale.US;
            Object[] objArr = new Object[1];
            Jid jid = this.A0D;
            if (jid != null) {
                str = jid.toString();
            } else {
                str = "unknown@unknown";
            }
            if (!(jid instanceof C15580nU) && !(jid instanceof AnonymousClass1JV)) {
                str = String.format(locale, "[obfuscated]@%s", str.substring(str.indexOf("@") + 1));
            }
            objArr[0] = str;
            String format = String.format(locale, "(manage_community_groups) contact/community_info Detected subgroup '%s' without parent info", objArr);
            Log.e(format);
            r8.AaV("missing_parent_info", format, true);
            AnonymousClass009.A07("subgroup has to have a linked parent group jid");
        }
        this.A0F = r9;
    }

    public boolean A0H() {
        return A0J() && this.A06 == 3;
    }

    public boolean A0I() {
        C28811Pc r0 = this.A0C;
        return r0 != null && !TextUtils.isEmpty(r0.A01);
    }

    public boolean A0J() {
        int i;
        return (A0D() == null || (i = this.A06) == 0 || i == -1) ? false : true;
    }

    public boolean A0K() {
        if (this instanceof AnonymousClass1VR) {
            return true;
        }
        Jid jid = this.A0D;
        if (jid != null) {
            return C15380n4.A0J(jid);
        }
        StringBuilder sb = new StringBuilder("row_id=");
        sb.append(A08());
        sb.append(" jid=");
        sb.append((Object) "(null)");
        sb.append(" key=");
        C28811Pc r2 = this.A0C;
        if (r2 == null) {
            sb.append("(null)");
        } else {
            sb.append(r2.A00);
            sb.append("-");
            sb.append(r2.A01);
        }
        sb.append(" phone=");
        sb.append(this.A0H);
        sb.append(" iswa=");
        sb.append(this.A0f);
        if (A08() == -1) {
            return false;
        }
        StringBuilder sb2 = new StringBuilder("problematic contact:");
        sb2.append(sb.toString());
        Log.e(sb2.toString());
        return false;
    }

    public boolean A0L() {
        if (!(this instanceof C29861Va)) {
            return A0J() && A0H();
        }
        return true;
    }

    public boolean A0M() {
        if (!(this instanceof C29861Va)) {
            return A0K() && this.A0Y;
        }
        return true;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && obj.getClass() == getClass()) {
            C15370n3 r5 = (C15370n3) obj;
            if (C29941Vi.A00(this.A0D, r5.A0D)) {
                C28811Pc r1 = this.A0C;
                C28811Pc r0 = r5.A0C;
                if (r1 != null) {
                    return r1.equals(r0);
                }
                if (r0 != null) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        C28811Pc r0 = this.A0C;
        if (r0 != null) {
            return r0.hashCode();
        }
        Jid jid = this.A0D;
        if (jid != null) {
            return jid.hashCode();
        }
        return 0;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("row_id=");
        sb.append(A08());
        sb.append(" jid=");
        Jid jid = this.A0D;
        if (jid == null) {
            jid = "(null)";
        }
        sb.append(jid);
        sb.append(" key=");
        C28811Pc r2 = this.A0C;
        if (r2 == null) {
            sb.append("(null)");
        } else {
            sb.append(r2.A00);
            sb.append("-");
            sb.append(AnonymousClass1US.A02(4, r2.A01));
        }
        sb.append(" phone=");
        sb.append(this.A0H);
        sb.append(" iswa=");
        sb.append(this.A0f);
        if (A0K()) {
            sb.append(" status=");
            sb.append(this.A0R);
        }
        return sb.toString();
    }
}
