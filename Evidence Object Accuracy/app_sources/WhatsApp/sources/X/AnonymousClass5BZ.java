package X;

/* renamed from: X.5BZ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BZ implements Comparable {
    public final int A00;
    public final AnonymousClass4UD A01;

    public AnonymousClass5BZ(AnonymousClass4UD r1, int i) {
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return AnonymousClass048.A00(this.A00, ((AnonymousClass5BZ) obj).A00);
    }
}
