package X;

/* renamed from: X.5zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC130685zo {
    public static final C125475rJ A00 = A02(11);
    public static final C125475rJ A01 = A02(48);
    public static final C125475rJ A02 = A02(55);
    public static final C125475rJ A03 = A02(49);
    public static final C125475rJ A04 = A02(50);
    public static final C125475rJ A05 = A02(51);
    public static final C125475rJ A06 = A02(12);
    public static final C125475rJ A07 = A02(47);
    public static final C125475rJ A08 = A02(13);
    public static final C125475rJ A09 = A02(46);
    public static final C125475rJ A0A = A02(10);
    public static final C125475rJ A0B = A02(37);
    public static final C125475rJ A0C = A02(9);
    public static final C125475rJ A0D = A02(30);
    public static final C125475rJ A0E = A02(32);
    public static final C125475rJ A0F = A02(31);
    public static final C125475rJ A0G = A02(42);
    public static final C125475rJ A0H = A02(27);
    public static final C125475rJ A0I = A02(22);
    public static final C125475rJ A0J = A02(6);
    public static final C125475rJ A0K = A02(45);
    public static final C125475rJ A0L = A02(53);
    public static final C125475rJ A0M = A02(0);
    public static final C125475rJ A0N = A02(56);
    public static final C125475rJ A0O = A02(1);
    public static final C125475rJ A0P = A02(44);
    public static final C125475rJ A0Q = A02(7);
    public static final C125475rJ A0R = A02(5);
    public static final C125475rJ A0S = A02(57);
    public static final C125475rJ A0T = A02(2);
    public static final C125475rJ A0U = A02(4);
    public static final C125475rJ A0V = A02(60);
    public static final C125475rJ A0W = A02(3);
    public static final C125475rJ A0X = A02(14);
    public static final C125475rJ A0Y = A02(15);
    public static final C125475rJ A0Z = A02(36);
    public static final C125475rJ A0a = A02(54);
    public static final C125475rJ A0b = A02(38);
    public static final C125475rJ A0c = A02(21);
    public static final C125475rJ A0d = A02(16);
    public static final C125475rJ A0e = A02(17);
    public static final C125475rJ A0f = A02(41);
    public static final C125475rJ A0g = A02(34);
    public static final C125475rJ A0h = A02(52);
    public static final C125475rJ A0i = A02(18);
    public static final C125475rJ A0j = A02(39);
    public static final C125475rJ A0k = A02(19);
    public static final C125475rJ A0l = A02(40);
    public static final C125475rJ A0m = A02(33);
    public static final C125475rJ A0n = A02(8);
    public static final C125475rJ A0o = A02(23);
    public static final C125475rJ A0p = A02(26);
    public static final C125475rJ A0q = A02(20);
    public static final C125475rJ A0r = A02(59);
    public static final C125475rJ A0s = A02(35);
    public static final C125475rJ A0t = A02(24);
    public static final C125475rJ A0u = A02(58);
    public static final C125475rJ A0v = A02(25);

    public static C125475rJ A02(int i) {
        return new C125475rJ(i);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Failed to find switch 'out' block
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:819)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:858)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:858)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:161)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:732)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:732)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:737)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:156)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:95)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:52)
        */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x01a9  */
    public java.lang.Object A03(X.C125475rJ r8) {
        /*
        // Method dump skipped, instructions count: 756
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC130685zo.A03(X.5rJ):java.lang.Object");
    }
}
