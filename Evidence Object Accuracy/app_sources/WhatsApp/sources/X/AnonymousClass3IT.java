package X;

import android.os.Build;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3IT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3IT {
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public boolean A05;
    public boolean A06;
    public final AnonymousClass4PG A07;
    public final AbstractC115095Qe A08;
    public final List A09;
    public final Map A0A;

    public AnonymousClass3IT(AnonymousClass3IT r6) {
        this.A07 = r6.A07;
        this.A08 = r6.A08;
        this.A00 = r6.A00;
        this.A01 = r6.A01;
        this.A02 = r6.A02;
        this.A03 = r6.A03;
        this.A04 = r6.A04;
        this.A09 = C12980iv.A0x(r6.A09);
        this.A0A = new HashMap(r6.A0A.size());
        Iterator A0n = C12960it.A0n(r6.A0A);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            AbstractC64703Go A00 = A00((Class) A15.getKey());
            ((AbstractC64703Go) A15.getValue()).A02(A00);
            this.A0A.put(A15.getKey(), A00);
        }
    }

    public AnonymousClass3IT(AnonymousClass4PG r3, AbstractC115095Qe r4) {
        C13020j0.A01(r4);
        this.A07 = r3;
        this.A08 = r4;
        this.A03 = 1800000;
        this.A04 = 3024000000L;
        this.A0A = C12970iu.A11();
        this.A09 = C12960it.A0l();
    }

    public static AbstractC64703Go A00(Class cls) {
        try {
            return (AbstractC64703Go) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            if (e instanceof InstantiationException) {
                throw new IllegalArgumentException("dataType doesn't have default constructor", e);
            } else if (e instanceof IllegalAccessException) {
                throw new IllegalArgumentException("dataType default constructor is not accessible", e);
            } else if (Build.VERSION.SDK_INT < 19 || !(e instanceof ReflectiveOperationException)) {
                throw new RuntimeException(e);
            } else {
                throw new IllegalArgumentException("Linkage exception", e);
            }
        }
    }

    public final AbstractC64703Go A01(Class cls) {
        Map map = this.A0A;
        AbstractC64703Go r0 = (AbstractC64703Go) map.get(cls);
        if (r0 != null) {
            return r0;
        }
        AbstractC64703Go A00 = A00(cls);
        map.put(cls, A00);
        return A00;
    }

    public final void A02(AbstractC64703Go r4) {
        C13020j0.A01(r4);
        Class<?> cls = r4.getClass();
        if (cls.getSuperclass() == AbstractC64703Go.class) {
            r4.A02(A01(cls));
            return;
        }
        throw new IllegalArgumentException();
    }
}
