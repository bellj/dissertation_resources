package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.3xX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83623xX extends AbstractC52172aN {
    public final /* synthetic */ C253118x A00;
    public final /* synthetic */ Runnable A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83623xX(Context context, C253118x r2, Runnable runnable, int i) {
        super(context, i);
        this.A00 = r2;
        this.A01 = runnable;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        this.A01.run();
    }
}
