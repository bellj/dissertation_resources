package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4kL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99464kL implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        long j = 0;
        String str2 = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                str = C95664e9.A08(parcel, readInt);
            } else if (c == 3) {
                str2 = C95664e9.A08(parcel, readInt);
            } else if (c != 4) {
                C95664e9.A0D(parcel, readInt);
            } else {
                j = C95664e9.A04(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78343ol(str, str2, j);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78343ol[i];
    }
}
