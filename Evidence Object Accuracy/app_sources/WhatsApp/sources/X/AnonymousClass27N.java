package X;

import java.util.Map;

/* renamed from: X.27N  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass27N {
    boolean AK9();

    void Aab();

    void Aao();

    boolean Aee();

    void Af2();

    void setQrDecodeHints(Map map);

    void setQrScannerCallback(AnonymousClass27Z v);
}
