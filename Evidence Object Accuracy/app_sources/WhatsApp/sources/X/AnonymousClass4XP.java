package X;

/* renamed from: X.4XP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XP {
    public int A00;
    public int A01;
    public int A02;
    public C95034cy A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;

    public AnonymousClass4XP(C95034cy r1) {
        this.A03 = r1;
    }

    public void A00(int i) {
        this.A04 |= C12960it.A1U(i);
        this.A01 += i;
    }

    public void A01(int i) {
        boolean z = true;
        if (!this.A06 || this.A00 == 4) {
            this.A04 = true;
            this.A06 = true;
            this.A00 = i;
            return;
        }
        if (i != 4) {
            z = false;
        }
        C95314dV.A03(z);
    }
}
