package X;

/* renamed from: X.0FL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FL extends AnonymousClass0Z4 {
    public final AnonymousClass0Z3 A00;
    public final AnonymousClass0Z4 A01;

    public AnonymousClass0FL(AnonymousClass0Z4 r2) {
        this.A01 = r2;
        this.A00 = new AnonymousClass0Z3(r2);
    }

    @Override // X.AnonymousClass0Z4
    public Object A00(Object obj, Object obj2) {
        return this.A01.A00(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4
    public boolean A01(Object obj, Object obj2) {
        return this.A01.A01(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4
    public boolean A02(Object obj, Object obj2) {
        return this.A01.A02(obj, obj2);
    }

    @Override // X.AnonymousClass0Z4, X.AbstractC12640iF
    public void ANr(Object obj, int i, int i2) {
        this.A00.ANr(obj, i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ARP(int i, int i2) {
        this.A00.ARP(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void ASr(int i, int i2) {
        this.A00.ASr(i, i2);
    }

    @Override // X.AbstractC12640iF
    public void AUs(int i, int i2) {
        this.A00.AUs(i, i2);
    }

    @Override // X.AnonymousClass0Z4, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return this.A01.compare(obj, obj2);
    }
}
