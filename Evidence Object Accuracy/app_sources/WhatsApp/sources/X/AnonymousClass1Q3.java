package X;

import android.app.ActivityManager;
import android.util.Pair;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import java.io.Serializable;

/* renamed from: X.1Q3  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q3 implements AnonymousClass1Q2 {
    public final AnonymousClass01d A00;
    public final C243315c A01;
    public final String A02 = "memory_stats";

    @Override // X.AnonymousClass1Q2
    public boolean AK1() {
        return true;
    }

    public AnonymousClass1Q3(AnonymousClass01d r2, C243315c r3) {
        this.A00 = r2;
        this.A01 = r3;
    }

    public void A00(C28631Oi r6, String str, long j) {
        try {
            String str2 = this.A02;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("_at_start");
            Long l = (Long) ((Serializable) r6.A0A.get(new Pair(str2, sb.toString())));
            if (l != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append("_delta");
                r6.A02(Long.valueOf(j - l.longValue()), str2, sb2.toString());
            }
        } catch (RuntimeException unused) {
        }
    }

    @Override // X.AbstractC28621Oh
    public final String ADy() {
        return this.A02;
    }

    @Override // X.AbstractC28621Oh
    public void APh(C28631Oi r8) {
        ActivityManager A03 = this.A00.A03();
        if (A03 != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            A03.getMemoryInfo(memoryInfo);
            long j = (memoryInfo.availMem / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS;
            String str = this.A02;
            r8.A02(Long.valueOf(j), str, "avail_mem");
            r8.A02(Long.valueOf((memoryInfo.threshold / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS), str, "low_mem");
            r8.A02(Long.valueOf((memoryInfo.totalMem / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS), str, "total_mem");
            A00(r8, "avail_mem", (memoryInfo.availMem / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS);
        }
        Runtime runtime = this.A01.A00;
        A00(r8, "java_heap", ((runtime.totalMemory() - runtime.freeMemory()) / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS);
    }

    @Override // X.AbstractC28621Oh
    public void AWI(C28631Oi r6) {
        ActivityManager A03 = this.A00.A03();
        if (A03 != null) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            A03.getMemoryInfo(memoryInfo);
            long j = (memoryInfo.availMem / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS;
            String str = this.A02;
            StringBuilder sb = new StringBuilder();
            sb.append("avail_mem");
            sb.append("_at_start");
            r6.A02(Long.valueOf(j), str, sb.toString());
        }
        Runtime runtime = this.A01.A00;
        long freeMemory = ((runtime.totalMemory() - runtime.freeMemory()) / SearchActionVerificationClientService.MS_TO_NS) * SearchActionVerificationClientService.MS_TO_NS;
        String str2 = this.A02;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("java_heap");
        sb2.append("_at_start");
        r6.A02(Long.valueOf(freeMemory), str2, sb2.toString());
    }
}
