package X;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.TypedValue;
import android.view.Window;
import android.widget.ImageView;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.R;

/* renamed from: X.1tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41691tw {
    public static int A00(Context context, int i, int i2) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i, typedValue, true);
        try {
            return AnonymousClass00T.A00(context, typedValue.resourceId);
        } catch (Resources.NotFoundException unused) {
            return AnonymousClass00T.A00(context, i2);
        }
    }

    public static AnonymousClass062 A01(Context context) {
        AnonymousClass062 r3;
        Configuration configuration = new Configuration();
        configuration.uiMode = (new Configuration().uiMode & -49) | 16;
        if (context instanceof AnonymousClass062) {
            r3 = (AnonymousClass062) context;
        } else {
            r3 = new AnonymousClass062(context, (Resources.Theme) null);
        }
        r3.A01(configuration);
        return r3;
    }

    public static void A02(Activity activity, int i) {
        Window window;
        if (Build.VERSION.SDK_INT >= 21 && (window = activity.getWindow()) != null) {
            window.addFlags(Integer.MIN_VALUE);
            window.clearFlags(67108864);
            window.setStatusBarColor(AnonymousClass00T.A00(activity, i));
        }
    }

    public static void A03(Activity activity, int i) {
        A05(activity.getBaseContext(), activity.getWindow(), i);
    }

    public static void A04(Activity activity, int i, int i2) {
        if (C28391Mz.A02()) {
            boolean z = true;
            if (i2 != 1 || A08(activity)) {
                z = false;
            } else if (!C28391Mz.A04()) {
                return;
            }
            Window window = activity.getWindow();
            if (window != null) {
                window.setNavigationBarColor(AnonymousClass00T.A00(activity, i));
                if (Build.VERSION.SDK_INT >= 27) {
                    int systemUiVisibility = window.getDecorView().getSystemUiVisibility();
                    int i3 = systemUiVisibility & -17;
                    if (z) {
                        i3 = systemUiVisibility | 16;
                    }
                    window.getDecorView().setSystemUiVisibility(i3);
                }
            }
        }
    }

    public static void A05(Context context, Window window, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            boolean z = !A08(context);
            if (window != null) {
                window.addFlags(Integer.MIN_VALUE);
                window.clearFlags(67108864);
                window.setStatusBarColor(AnonymousClass00T.A00(context, i));
                A07(window, z);
            }
        }
    }

    public static void A06(Context context, ImageView imageView) {
        AnonymousClass2GE.A07(imageView, A00(context, R.attr.settingsIconColor, R.color.settings_icon));
    }

    public static void A07(Window window, boolean z) {
        if (Build.VERSION.SDK_INT >= 23) {
            int systemUiVisibility = window.getDecorView().getSystemUiVisibility();
            int i = systemUiVisibility & -8193;
            if (z) {
                i = systemUiVisibility | DefaultCrypto.BUFFER_SIZE;
            }
            window.getDecorView().setSystemUiVisibility(i);
        }
    }

    public static boolean A08(Context context) {
        return (context.getResources().getConfiguration().uiMode & 48) == 32;
    }
}
