package X;

import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;

/* renamed from: X.3E9  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3E9 {
    public C48142Em A00;
    public BusinessDirectorySearchQueryViewModel A01;
    public final int A02;

    public AnonymousClass3E9(BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel, int i) {
        this.A02 = i;
        this.A01 = businessDirectorySearchQueryViewModel;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass3E9 r5 = (AnonymousClass3E9) obj;
            if (this.A02 != r5.A02 || !C29941Vi.A00(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, this.A02);
        return C12960it.A06(this.A00, A1a);
    }
}
