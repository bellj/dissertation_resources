package X;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: X.52L  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52L implements AnonymousClass5RD {
    public final int A00 = 400;
    public final Deque A01 = new LinkedList();
    public final Map A02 = new ConcurrentHashMap();
    public final ReentrantLock A03 = new ReentrantLock();

    public final void A00(String str) {
        ReentrantLock reentrantLock = this.A03;
        reentrantLock.lock();
        try {
            Deque deque = this.A01;
            deque.removeFirstOccurrence(str);
            deque.addFirst(str);
        } finally {
            reentrantLock.unlock();
        }
    }

    public String toString() {
        return this.A02.toString();
    }
}
