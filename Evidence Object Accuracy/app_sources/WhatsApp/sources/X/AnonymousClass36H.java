package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.ctwa.bizpreview.BusinessPreviewMediaCardGrid;
import com.whatsapp.ui.media.MediaCard;
import com.whatsapp.ui.media.MediaCardGrid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.36H  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass36H extends AbstractC83653xd {
    public View A00;
    public HorizontalScrollView A01;
    public ImageView A02;
    public ImageView A03;
    public LinearLayout A04;
    public RelativeLayout A05;
    public TextView A06;
    public TextView A07;
    public TextView A08;
    public TextView A09;
    public TextView A0A;
    public AnonymousClass018 A0B;
    public AbstractC116275Uu A0C;

    public int getThumbnailIconGravity() {
        return 3;
    }

    public abstract int getThumbnailPixelSize();

    public int getThumbnailTextGravity() {
        return 5;
    }

    public AnonymousClass36H(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AnonymousClass36H(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08(attributeSet);
    }

    public static ThumbnailButton A01(View view, int i) {
        AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(i, i);
        ThumbnailButton thumbnailButton = new ThumbnailButton(view.getContext());
        thumbnailButton.setBackgroundResource(R.drawable.catalog_product_placeholder_background);
        thumbnailButton.setLayoutParams(layoutParams);
        thumbnailButton.A02 = view.getResources().getDimension(R.dimen.catalog_item_image_radius);
        return thumbnailButton;
    }

    public AnonymousClass2x5 A03(ViewGroup.LayoutParams layoutParams, AnonymousClass4TJ r4, int i) {
        AnonymousClass2x5 r1 = new AnonymousClass2x5(getContext());
        C12990iw.A1E(r1);
        r1.setLayoutParams(layoutParams);
        r1.A00 = (float) (i / 6);
        r1.A04 = getThumbnailTextGravity();
        r1.A01 = getThumbnailIconGravity();
        AnonymousClass028.A0k(r1, r4.A05);
        String str = r4.A04;
        if (str != null) {
            r1.A0A = str;
        }
        String str2 = r4.A03;
        if (str2 != null) {
            r1.setContentDescription(str2);
        }
        Drawable drawable = r4.A00;
        if (drawable != null) {
            r1.A08 = drawable;
        }
        if (r4.A01 != null) {
            C12960it.A11(r1, r4, 33);
        }
        r4.A02.AQS(r1, i);
        return r1;
    }

    public void A04() {
        this.A04.setVisibility(0);
        this.A00.setVisibility(8);
        this.A05.setVisibility(8);
        this.A06.setVisibility(8);
    }

    public void A05() {
        this.A04.setVisibility(8);
        this.A00.setVisibility(0);
        this.A05.setVisibility(0);
        this.A06.setVisibility(8);
    }

    public void A06(int i) {
        C74083hH r1;
        GridView gridView;
        if (this instanceof MediaCardGrid) {
            MediaCardGrid mediaCardGrid = (MediaCardGrid) this;
            ArrayList A0l = C12960it.A0l();
            mediaCardGrid.A03 = A0l;
            for (int i2 = 0; i2 < i; i2++) {
                ThumbnailButton A01 = A01(mediaCardGrid, mediaCardGrid.getThumbnailPixelSize());
                A0l = mediaCardGrid.A03;
                A0l.add(A01);
            }
            r1 = new C74083hH(A0l);
            mediaCardGrid.A02 = r1;
            gridView = mediaCardGrid.A00;
        } else if (!(this instanceof MediaCard)) {
            BusinessPreviewMediaCardGrid businessPreviewMediaCardGrid = (BusinessPreviewMediaCardGrid) this;
            businessPreviewMediaCardGrid.A05 = C12960it.A0l();
            int thumbnailPixelSize = businessPreviewMediaCardGrid.getThumbnailPixelSize();
            for (int i3 = 0; i3 < businessPreviewMediaCardGrid.A00; i3++) {
                businessPreviewMediaCardGrid.A05.add(A01(businessPreviewMediaCardGrid, thumbnailPixelSize));
            }
            r1 = new C74083hH(businessPreviewMediaCardGrid.A05);
            businessPreviewMediaCardGrid.A04 = r1;
            gridView = businessPreviewMediaCardGrid.A01;
        } else {
            MediaCard mediaCard = (MediaCard) this;
            int thumbnailPixelSize2 = mediaCard.getThumbnailPixelSize();
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(thumbnailPixelSize2, thumbnailPixelSize2);
            int dimensionPixelSize = mediaCard.getResources().getDimensionPixelSize(R.dimen.medium_thumbnail_padding);
            layoutParams.setMargins(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
            for (int i4 = 0; i4 < i; i4++) {
                LinearLayout linearLayout = mediaCard.A00;
                ThumbnailButton thumbnailButton = new ThumbnailButton(mediaCard.getContext());
                thumbnailButton.setBackgroundResource(R.drawable.catalog_product_placeholder_background);
                thumbnailButton.setLayoutParams(layoutParams);
                linearLayout.addView(thumbnailButton);
            }
            ((AnonymousClass36H) mediaCard).A01.setVisibility(0);
            return;
        }
        gridView.setAdapter((ListAdapter) r1);
    }

    public void A07(int i, int i2) {
        if (i < 0) {
            i = this.A00.getPaddingLeft();
        }
        if (i2 < 0) {
            i2 = this.A00.getPaddingRight();
        }
        View view = this.A00;
        view.setPadding(i, view.getPaddingTop(), i2, this.A00.getPaddingBottom());
        TextView textView = this.A06;
        textView.setPadding(i, textView.getPaddingTop(), i2, this.A06.getPaddingBottom());
    }

    /* JADX INFO: finally extract failed */
    public void A08(AttributeSet attributeSet) {
        C12960it.A0E(this).inflate(R.layout.media_card, (ViewGroup) this, true);
        this.A0A = C12960it.A0I(this, R.id.media_card_title);
        this.A08 = C12960it.A0I(this, R.id.media_card_empty_title);
        this.A09 = C12960it.A0I(this, R.id.media_card_info);
        this.A07 = C12960it.A0I(this, R.id.media_card_empty_info);
        this.A00 = AnonymousClass028.A0D(this, R.id.title_container);
        this.A01 = (HorizontalScrollView) AnonymousClass028.A0D(this, R.id.media_card_scroller);
        this.A06 = C12960it.A0I(this, R.id.media_card_error);
        this.A05 = (RelativeLayout) AnonymousClass028.A0D(this, R.id.media_card_thumb_container);
        this.A04 = (LinearLayout) AnonymousClass028.A0D(this, R.id.media_card_empty);
        this.A02 = C12970iu.A0K(this, R.id.branding_img);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = C12980iv.A0I(this).obtainStyledAttributes(attributeSet, AnonymousClass2QN.A0C, 0, 0);
            try {
                String A0E = this.A0B.A0E(obtainStyledAttributes, 1);
                String A0E2 = this.A0B.A0E(obtainStyledAttributes, 0);
                obtainStyledAttributes.recycle();
                this.A0A.setText(A0E);
                AnonymousClass028.A0l(this.A0A, true);
                this.A08.setText(A0E);
                setMediaInfo(A0E2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
        }
    }

    public void A09(List list, int i) {
        C74083hH r0;
        if (list.size() == 0) {
            A04();
            return;
        }
        A05();
        int thumbnailPixelSize = getThumbnailPixelSize();
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.medium_thumbnail_padding);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(thumbnailPixelSize, thumbnailPixelSize);
        layoutParams.setMargins(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        if (this.A03 == null) {
            ImageView imageView = new ImageView(getContext());
            this.A03 = imageView;
            imageView.setBackgroundResource(R.drawable.selector_orange_gradient);
            this.A03.setLayoutParams(layoutParams);
            AnonymousClass2GF.A01(getContext(), this.A03, this.A0B, R.drawable.group_info_chevron_right);
            C12960it.A0r(getContext(), this.A03, R.string.more);
            this.A03.setScaleType(ImageView.ScaleType.CENTER);
            if (this.A0C != null) {
                C12960it.A11(this.A03, this, 39);
            }
        }
        if (this instanceof MediaCardGrid) {
            MediaCardGrid mediaCardGrid = (MediaCardGrid) this;
            ArrayList arrayList = mediaCardGrid.A03;
            if (arrayList == null) {
                mediaCardGrid.A03 = C12960it.A0l();
            } else {
                arrayList.clear();
            }
            int size = list.size();
            if (size > 3) {
                size = 6;
                if (size < 6) {
                    size = 3;
                }
            }
            for (int i2 = 0; i2 < size; i2++) {
                AnonymousClass2x5 A03 = mediaCardGrid.A03(new AbsListView.LayoutParams(thumbnailPixelSize, thumbnailPixelSize), (AnonymousClass4TJ) list.get(i2), thumbnailPixelSize);
                ((ThumbnailButton) A03).A02 = mediaCardGrid.getResources().getDimension(R.dimen.catalog_item_image_radius);
                mediaCardGrid.A03.add(A03);
            }
            if (mediaCardGrid.A02 == null) {
                C74083hH r1 = new C74083hH(mediaCardGrid.A03);
                mediaCardGrid.A02 = r1;
                mediaCardGrid.A00.setAdapter((ListAdapter) r1);
            }
            r0 = mediaCardGrid.A02;
        } else if (!(this instanceof MediaCard)) {
            BusinessPreviewMediaCardGrid businessPreviewMediaCardGrid = (BusinessPreviewMediaCardGrid) this;
            ArrayList arrayList2 = businessPreviewMediaCardGrid.A05;
            if (arrayList2 == null) {
                businessPreviewMediaCardGrid.A05 = C12960it.A0l();
            } else {
                arrayList2.clear();
            }
            int min = Math.min(businessPreviewMediaCardGrid.A00, list.size());
            for (int i3 = 0; i3 < min; i3++) {
                AnonymousClass4TJ r12 = (AnonymousClass4TJ) list.get(i3);
                AnonymousClass2x5 A032 = businessPreviewMediaCardGrid.A03(new AbsListView.LayoutParams(thumbnailPixelSize, thumbnailPixelSize), r12, thumbnailPixelSize);
                if (r12.A01 != null) {
                    C12960it.A14(A032, businessPreviewMediaCardGrid, r12, 3);
                }
                ((ThumbnailButton) A032).A02 = businessPreviewMediaCardGrid.getResources().getDimension(R.dimen.catalog_item_image_radius);
                businessPreviewMediaCardGrid.A05.add(A032);
            }
            if (businessPreviewMediaCardGrid.A04 == null) {
                C74083hH r13 = new C74083hH(businessPreviewMediaCardGrid.A05);
                businessPreviewMediaCardGrid.A04 = r13;
                businessPreviewMediaCardGrid.A01.setAdapter((ListAdapter) r13);
            }
            r0 = businessPreviewMediaCardGrid.A04;
        } else {
            MediaCard mediaCard = (MediaCard) this;
            mediaCard.A00.removeAllViews();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                mediaCard.A00.addView(mediaCard.A03(layoutParams, (AnonymousClass4TJ) it.next(), thumbnailPixelSize));
            }
            C42941w9.A0E(mediaCard.A00, mediaCard.A0B);
            C42941w9.A0D(((AnonymousClass36H) mediaCard).A01, mediaCard.A0B);
            if (list.size() >= i) {
                mediaCard.A00.addView(((AnonymousClass36H) mediaCard).A03);
            }
            ((AnonymousClass36H) mediaCard).A01.setVisibility(0);
            return;
        }
        r0.notifyDataSetChanged();
    }

    public String getError() {
        if (this.A06.getVisibility() == 0) {
            return C12980iv.A0q(this.A06);
        }
        return null;
    }

    public void setCatalogBrandingDrawable(Drawable drawable) {
        ImageView imageView = this.A02;
        int i = 8;
        if (drawable != null) {
            i = 0;
        }
        imageView.setVisibility(i);
        this.A02.setImageDrawable(drawable);
    }

    public void setError(String str) {
        this.A06.setText(str);
        this.A06.setVisibility(0);
    }

    public void setMediaInfo(String str) {
        this.A09.setText(str);
        this.A07.setText(str);
        if (!TextUtils.isEmpty(str)) {
            C42941w9.A0F(this.A09, this.A0B);
            C42941w9.A0F(this.A07, this.A0B);
        }
    }

    public void setSeeMoreClickListener(AbstractC116275Uu r3) {
        this.A0C = r3;
        ImageView imageView = this.A03;
        if (imageView != null) {
            C12960it.A11(imageView, r3, 38);
        }
        C12960it.A11(this.A0A, r3, 35);
        C12960it.A11(this.A09, r3, 37);
        C12960it.A11(this.A08, r3, 34);
        C12960it.A11(this.A07, r3, 36);
    }

    public void setSeeMoreColor(int i) {
        this.A09.setTextColor(i);
    }

    public void setTitle(String str) {
        this.A0A.setText(str);
        this.A08.setText(str);
    }

    public void setTitleTextColor(int i) {
        this.A0A.setTextColor(i);
    }

    public void setTopShadowVisibility(int i) {
        int i2;
        int paddingLeft = getPaddingLeft();
        if (i == 0) {
            i2 = C12960it.A09(this).getDimensionPixelSize(R.dimen.info_screen_card_spacing);
        } else {
            i2 = 0;
        }
        C12990iw.A1A(this, paddingLeft, i2);
    }
}
