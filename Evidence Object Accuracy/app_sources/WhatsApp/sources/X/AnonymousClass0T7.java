package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EdgeEffect;

/* renamed from: X.0T7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0T7 {
    public static float A00(EdgeEffect edgeEffect) {
        try {
            return edgeEffect.getDistance();
        } catch (Throwable unused) {
            return 0.0f;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:6:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: android.widget.EdgeEffect */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: android.widget.EdgeEffect */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v3, types: [float] */
    public static float A01(EdgeEffect edgeEffect, float f, float f2) {
        try {
            edgeEffect = edgeEffect.onPullDistance(f, f2);
            return edgeEffect;
        } catch (Throwable unused) {
            edgeEffect.onPull(f, f2);
            return 0.0f;
        }
    }

    public static EdgeEffect A02(Context context, AttributeSet attributeSet) {
        try {
            return new EdgeEffect(context, attributeSet);
        } catch (Throwable unused) {
            return new EdgeEffect(context);
        }
    }
}
