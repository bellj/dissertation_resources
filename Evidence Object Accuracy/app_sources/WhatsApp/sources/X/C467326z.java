package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.26z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C467326z extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C467326z A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public AnonymousClass229 A01;
    public AnonymousClass229 A02;

    static {
        C467326z r0 = new C467326z();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r5, Object obj, Object obj2) {
        int i;
        C467226y r1;
        switch (r5.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r6 = (AbstractC462925h) obj;
                C467326z r7 = (C467326z) obj2;
                this.A01 = (AnonymousClass229) r6.Aft(this.A01, r7.A01);
                this.A02 = (AnonymousClass229) r6.Aft(this.A02, r7.A02);
                if (r6 == C463025i.A00) {
                    this.A00 |= r7.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r62 = (AnonymousClass253) obj;
                AnonymousClass254 r72 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A032 = r62.A03();
                        int i2 = 1;
                        if (A032 == 0) {
                            break;
                        } else {
                            if (A032 == 10) {
                                C467226y r12 = (this.A00 & 1) == 1 ? (C467226y) this.A01.A0T() : null;
                                AnonymousClass229 r0 = (AnonymousClass229) r62.A09(r72, AnonymousClass229.A0A.A0U());
                                this.A01 = r0;
                                if (r12 != null) {
                                    r12.A04(r0);
                                    this.A01 = (AnonymousClass229) r12.A01();
                                }
                                i = this.A00;
                            } else if (A032 == 18) {
                                i2 = 2;
                                if ((this.A00 & 2) == 2) {
                                    r1 = (C467226y) this.A02.A0T();
                                } else {
                                    r1 = null;
                                }
                                AnonymousClass229 r02 = (AnonymousClass229) r62.A09(r72, AnonymousClass229.A0A.A0U());
                                this.A02 = r02;
                                if (r1 != null) {
                                    r1.A04(r02);
                                    this.A02 = (AnonymousClass229) r1.A01();
                                }
                                i = this.A00;
                            } else if (!A0a(r62, A032)) {
                                break;
                            }
                            this.A00 = i | i2;
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r13 = new C28971Pt(e2.getMessage());
                        r13.unfinishedMessage = this;
                        throw new RuntimeException(r13);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C467326z();
            case 5:
                return new AnonymousClass270();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C467326z.class) {
                        if (A04 == null) {
                            A04 = new AnonymousClass255(A03);
                        }
                    }
                }
                return A04;
            default:
                throw new UnsupportedOperationException();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            AnonymousClass229 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            i2 = 0 + CodedOutputStream.A0A(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass229 r02 = this.A02;
            if (r02 == null) {
                r02 = AnonymousClass229.A0A;
            }
            i2 += CodedOutputStream.A0A(r02, 2);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            AnonymousClass229 r0 = this.A01;
            if (r0 == null) {
                r0 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r0, 1);
        }
        if ((this.A00 & 2) == 2) {
            AnonymousClass229 r02 = this.A02;
            if (r02 == null) {
                r02 = AnonymousClass229.A0A;
            }
            codedOutputStream.A0L(r02, 2);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
