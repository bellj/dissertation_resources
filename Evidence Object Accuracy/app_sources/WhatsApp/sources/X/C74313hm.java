package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.whatsapp.Conversation;

/* renamed from: X.3hm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74313hm extends AnonymousClass04v {
    public final /* synthetic */ Conversation A00;

    public C74313hm(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        super.A01(view, accessibilityEvent);
        if (((AbstractActivityC13750kH) this.A00).A0I.A0B()) {
            accessibilityEvent.setFromIndex(-1);
            accessibilityEvent.setToIndex(-1);
            accessibilityEvent.setItemCount(-1);
        }
    }
}
