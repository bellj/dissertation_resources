package X;

/* renamed from: X.4V4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4V4 {
    public String A00;

    public AnonymousClass4V4(String str) {
        this.A00 = str;
    }

    public Object A00(String str) {
        C16700pc.A0E(str, 0);
        try {
            return C94734cS.A00(this.A00).A01(str, new AnonymousClass5T6[0]);
        } catch (C82803wD unused) {
            return null;
        }
    }
}
