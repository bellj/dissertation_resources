package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.ConditionVariable;
import android.os.IBinder;

/* renamed from: X.2Kc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ServiceConnectionC49272Kc implements ServiceConnection {
    public final /* synthetic */ ConditionVariable A00;
    public final /* synthetic */ C18350sJ A01;

    public ServiceConnectionC49272Kc(ConditionVariable conditionVariable, C18350sJ r2) {
        this.A01 = r2;
        this.A00 = conditionVariable;
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.A00.open();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        this.A00.close();
    }
}
