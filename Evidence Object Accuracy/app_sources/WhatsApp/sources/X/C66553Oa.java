package X;

import android.content.Context;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.3Oa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66553Oa implements AbstractC009204q {
    public final /* synthetic */ ViewProfilePhoto.SavePhoto A00;

    public C66553Oa(ViewProfilePhoto.SavePhoto savePhoto) {
        this.A00 = savePhoto;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        ViewProfilePhoto.SavePhoto savePhoto = this.A00;
        if (!savePhoto.A02) {
            savePhoto.A02 = true;
            AnonymousClass01J r1 = ((AnonymousClass2FL) ((AnonymousClass2FJ) savePhoto.generatedComponent())).A1E;
            savePhoto.A01 = C12970iu.A0R(r1);
            savePhoto.A00 = (C14330lG) r1.A7B.get();
        }
    }
}
