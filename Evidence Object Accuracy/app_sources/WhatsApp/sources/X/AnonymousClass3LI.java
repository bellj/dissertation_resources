package X;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;

/* renamed from: X.3LI  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3LI implements ServiceConnection {
    public volatile C79663qy A00;
    public volatile boolean A01;
    public final /* synthetic */ C15100mZ A02;

    public AnonymousClass3LI(C15100mZ r1) {
        this.A02 = r1;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:11|(1:13)(1:14)|37|15|(3:20|(1:22)(1:23)|25)|39|24|25) */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0051 A[Catch: all -> 0x0081, TryCatch #1 {, blocks: (B:7:0x0016, B:25:0x007c, B:26:0x007f, B:6:0x000f, B:9:0x001b, B:11:0x0027, B:13:0x002f, B:14:0x0032, B:15:0x0037, B:16:0x003f, B:18:0x0048, B:20:0x0051, B:22:0x0055, B:23:0x006c, B:24:0x006f), top: B:35:0x000d }] */
    @Override // android.content.ServiceConnection
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r6, android.os.IBinder r7) {
        /*
            r5 = this;
            java.lang.String r2 = "AnalyticsServiceConnection.onServiceConnected"
            android.os.Looper r1 = android.os.Looper.getMainLooper()
            android.os.Looper r0 = android.os.Looper.myLooper()
            if (r1 != r0) goto L_0x0089
            monitor-enter(r5)
            if (r7 != 0) goto L_0x001a
            X.0mZ r1 = r5.A02     // Catch: all -> 0x0081
            java.lang.String r0 = "Service connected with null binder"
            r1.A08(r0)     // Catch: all -> 0x0081
            r5.notifyAll()     // Catch: all -> 0x0086
            goto L_0x007f
        L_0x001a:
            r4 = 0
            java.lang.String r2 = r7.getInterfaceDescriptor()     // Catch: RemoteException -> 0x0047, all -> 0x0081
            java.lang.String r1 = "com.google.android.gms.analytics.internal.IAnalyticsService"
            boolean r0 = r1.equals(r2)     // Catch: RemoteException -> 0x0047, all -> 0x0081
            if (r0 == 0) goto L_0x003f
            android.os.IInterface r2 = r7.queryLocalInterface(r1)     // Catch: RemoteException -> 0x0047, all -> 0x0081
            boolean r0 = r2 instanceof X.C79663qy     // Catch: RemoteException -> 0x0047, all -> 0x0081
            if (r0 == 0) goto L_0x0032
            X.3qy r2 = (X.C79663qy) r2     // Catch: RemoteException -> 0x0047, all -> 0x0081
            goto L_0x0037
        L_0x0032:
            X.3qy r2 = new X.3qy     // Catch: RemoteException -> 0x0047, all -> 0x0081
            r2.<init>(r7)     // Catch: RemoteException -> 0x0047, all -> 0x0081
        L_0x0037:
            X.0mZ r3 = r5.A02     // Catch: RemoteException -> 0x0048, all -> 0x0081
            java.lang.String r0 = "Bound to IAnalyticsService interface"
            r3.A09(r0)     // Catch: RemoteException -> 0x0048, all -> 0x0081
            goto L_0x004f
        L_0x003f:
            X.0mZ r3 = r5.A02     // Catch: RemoteException -> 0x0047, all -> 0x0081
            java.lang.String r0 = "Got binder with a wrong descriptor"
            r3.A0C(r0, r2)     // Catch: RemoteException -> 0x0047, all -> 0x0081
            goto L_0x006f
        L_0x0047:
            r2 = r4
        L_0x0048:
            X.0mZ r3 = r5.A02     // Catch: all -> 0x0081
            java.lang.String r0 = "Service connect failed to get IAnalyticsService"
            r3.A08(r0)     // Catch: all -> 0x0081
        L_0x004f:
            if (r2 == 0) goto L_0x006f
            boolean r0 = r5.A01     // Catch: all -> 0x0081
            if (r0 != 0) goto L_0x006c
            java.lang.String r0 = "onServiceConnected received after the timeout limit"
            r3.A0A(r0)     // Catch: all -> 0x0081
            X.0kx r0 = r3.A00     // Catch: all -> 0x0081
            X.0ky r0 = r0.A03     // Catch: all -> 0x0081
            X.C13020j0.A01(r0)     // Catch: all -> 0x0081
            com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1 r1 = new com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1     // Catch: all -> 0x0081
            r1.<init>(r5, r2)     // Catch: all -> 0x0081
            X.0kz r0 = r0.A03     // Catch: all -> 0x0081
            r0.submit(r1)     // Catch: all -> 0x0081
            goto L_0x007c
        L_0x006c:
            r5.A00 = r2     // Catch: all -> 0x0081
            goto L_0x007c
        L_0x006f:
            X.3IW r2 = X.AnonymousClass3IW.A00()     // Catch: IllegalArgumentException -> 0x007c, all -> 0x0081
            X.0kx r0 = r3.A00     // Catch: IllegalArgumentException -> 0x007c, all -> 0x0081
            android.content.Context r1 = r0.A00     // Catch: IllegalArgumentException -> 0x007c, all -> 0x0081
            X.3LI r0 = r3.A01     // Catch: IllegalArgumentException -> 0x007c, all -> 0x0081
            r2.A01(r1, r0)     // Catch: IllegalArgumentException -> 0x007c, all -> 0x0081
        L_0x007c:
            r5.notifyAll()     // Catch: all -> 0x0086
        L_0x007f:
            monitor-exit(r5)     // Catch: all -> 0x0086
            return
        L_0x0081:
            r0 = move-exception
            r5.notifyAll()     // Catch: all -> 0x0086
            throw r0     // Catch: all -> 0x0086
        L_0x0086:
            r0 = move-exception
            monitor-exit(r5)     // Catch: all -> 0x0086
            throw r0
        L_0x0089:
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3LI.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    @Override // android.content.ServiceConnection
    public final void onServiceDisconnected(ComponentName componentName) {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            C14170ky r2 = ((C15050mT) this.A02).A00.A03;
            C13020j0.A01(r2);
            r2.A03.submit(new RunnableBRunnable0Shape10S0200000_I1(componentName, 18, this));
            return;
        }
        throw C12960it.A0U("AnalyticsServiceConnection.onServiceDisconnected");
    }
}
