package X;

import java.util.NoSuchElementException;

/* renamed from: X.28c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C468928c extends AnonymousClass1I5 {
    public boolean done;
    public final /* synthetic */ Object val$value;

    public C468928c(Object obj) {
        this.val$value = obj;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return !this.done;
    }

    @Override // java.util.Iterator
    public Object next() {
        if (!this.done) {
            this.done = true;
            return this.val$value;
        }
        throw new NoSuchElementException();
    }
}
