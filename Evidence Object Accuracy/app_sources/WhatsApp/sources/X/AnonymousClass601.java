package X;

import java.util.List;
import java.util.Set;

/* renamed from: X.601  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass601 {
    public static Set A03;
    public int A00 = -1;
    public C127685ut A01;
    public final Object A02;

    static {
        Class[] clsArr = new Class[5];
        clsArr[0] = Boolean.class;
        clsArr[1] = Integer.class;
        clsArr[2] = Double.class;
        clsArr[3] = Long.class;
        A03 = C12970iu.A13(String.class, clsArr, 4);
    }

    public AnonymousClass601(C127685ut r3, Object obj) {
        if (A03.contains(obj.getClass())) {
            this.A01 = r3;
            this.A02 = obj;
            List list = r3.A02;
            this.A00 = list.size();
            list.add(this);
            return;
        }
        throw C12970iu.A0f("Only primitive types allowed");
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A01.A00);
        return C12960it.A0f(A0h, this.A00);
    }
}
