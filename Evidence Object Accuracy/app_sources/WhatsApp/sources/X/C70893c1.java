package X;

import com.whatsapp.util.Log;

/* renamed from: X.3c1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70893c1 implements AbstractC47972Dm {
    public final /* synthetic */ AnonymousClass2xZ A00;

    @Override // X.AbstractC47972Dm
    public void ANV() {
    }

    public C70893c1(AnonymousClass2xZ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC47972Dm
    public void ANX(AnonymousClass1YT r4) {
        AnonymousClass2xZ r2 = this.A00;
        AbstractC14640lm r1 = r2.A0R;
        if (r1.equals(r4.A04)) {
            Log.i(C12960it.A0b("groupconversationmenu/onCallLogUpdated groupJid: ", r1));
            if (!C29941Vi.A00(r4.A06, r2.A03)) {
                r2.A03 = r4.A06;
                ((AbstractC36781kZ) r2).A01.invalidateOptionsMenu();
            }
            if (r2.A03 == null) {
                r4 = null;
            }
            r2.A00 = r4;
        }
    }
}
