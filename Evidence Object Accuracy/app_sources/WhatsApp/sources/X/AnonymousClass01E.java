package X;

import X.AbstractC001200n;
import X.AnonymousClass01E;
import X.AnonymousClass074;
import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.01E  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass01E implements AbstractC001200n, AbstractC001400p, AbstractC001500q, AbstractC001800t, ComponentCallbacks, View.OnCreateContextMenuListener, AbstractC002000v {
    public static final Object A0m = new Object();
    public int A00;
    public int A01;
    public int A02;
    public int A03 = -1;
    public int A04;
    public Bundle A05;
    public Bundle A06;
    public Bundle A07;
    public SparseArray A08;
    public LayoutInflater A09;
    public View A0A;
    public ViewGroup A0B;
    public AnonymousClass0O9 A0C;
    public AnonymousClass01E A0D;
    public AnonymousClass01E A0E;
    public AnonymousClass05V A0F;
    public AnonymousClass01F A0G = new AnonymousClass05Y();
    public AnonymousClass01F A0H;
    public AnonymousClass0Yp A0I;
    public AnonymousClass05I A0J = AnonymousClass05I.RESUMED;
    public C009804x A0K = new C009804x(this);
    public AnonymousClass016 A0L = new AnonymousClass016();
    public AbstractC009404s A0M = null;
    public C010004z A0N = new C010004z(this);
    public Boolean A0O = null;
    public Boolean A0P;
    public Runnable A0Q = new RunnableC018208m(this);
    public String A0R;
    public String A0S = null;
    public String A0T = UUID.randomUUID().toString();
    public boolean A0U;
    public boolean A0V;
    public boolean A0W;
    public boolean A0X;
    public boolean A0Y;
    public boolean A0Z;
    public boolean A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;
    public boolean A0e = true;
    public boolean A0f;
    public boolean A0g;
    public boolean A0h;
    public boolean A0i;
    public boolean A0j = true;
    public final ArrayList A0k = new ArrayList();
    public final AtomicInteger A0l = new AtomicInteger();

    @Deprecated
    public void A0j(int i, String[] strArr, int[] iArr) {
    }

    public boolean A0o(MenuItem menuItem) {
        return false;
    }

    public void A0w(Bundle bundle) {
    }

    public void A0x(Menu menu) {
    }

    public void A0y(Menu menu, MenuInflater menuInflater) {
    }

    public boolean A0z(MenuItem menuItem) {
        return false;
    }

    public void A17(Bundle bundle, View view) {
    }

    public final int A00() {
        AnonymousClass01E r0;
        AnonymousClass05I r1 = this.A0J;
        if (r1 == AnonymousClass05I.INITIALIZED || (r0 = this.A0D) == null) {
            return r1.ordinal();
        }
        return Math.min(r1.ordinal(), r0.A00());
    }

    public final Context A01() {
        Context A0p = A0p();
        if (A0p != null) {
            return A0p;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" not attached to a context.");
        throw new IllegalStateException(sb.toString());
    }

    public final Resources A02() {
        return A01().getResources();
    }

    public final Bundle A03() {
        Bundle bundle = this.A05;
        if (bundle != null) {
            return bundle;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" does not have any arguments.");
        throw new IllegalStateException(sb.toString());
    }

    public final LayoutInflater A04() {
        LayoutInflater layoutInflater = this.A09;
        if (layoutInflater != null) {
            return layoutInflater;
        }
        LayoutInflater A0q = A0q(null);
        this.A09 = A0q;
        return A0q;
    }

    public final View A05() {
        View view = this.A0A;
        if (view != null) {
            return view;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" did not return a View from onCreateView() or this was called before onCreateView().");
        throw new IllegalStateException(sb.toString());
    }

    public final AnonymousClass05L A06(AnonymousClass05J r8, AnonymousClass05K r9) {
        AnonymousClass0XV r4 = new AnonymousClass0XV(this);
        if (this.A03 <= 1) {
            AtomicReference atomicReference = new AtomicReference();
            C05070Od r1 = new C05070Od(r8, r9, r4, this, atomicReference);
            if (this.A03 >= 0) {
                r1.A00();
            } else {
                this.A0k.add(r1);
            }
            return new AnonymousClass0C2(r9, this, atomicReference);
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" is attempting to registerForActivityResult after being created. Fragments must call registerForActivityResult() before they are created (i.e. initialization, onAttach(), or onCreate()).");
        throw new IllegalStateException(sb.toString());
    }

    public final AnonymousClass0O9 A07() {
        AnonymousClass0O9 r0 = this.A0C;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass0O9 r02 = new AnonymousClass0O9();
        this.A0C = r02;
        return r02;
    }

    @Deprecated
    public final AnonymousClass01E A08() {
        String str;
        AnonymousClass01E r0 = this.A0E;
        if (r0 != null) {
            return r0;
        }
        AnonymousClass01F r1 = this.A0H;
        if (r1 == null || (str = this.A0S) == null) {
            return null;
        }
        return r1.A09(str);
    }

    public final AnonymousClass01E A09() {
        AnonymousClass01E r0 = this.A0D;
        if (r0 != null) {
            return r0;
        }
        Context A0p = A0p();
        StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        if (A0p == null) {
            sb.append(this);
            sb.append(" is not attached to any Fragment or host");
            throw new IllegalStateException(sb.toString());
        }
        sb.append(this);
        sb.append(" is not a child Fragment, it is directly attached to ");
        sb.append(A0p());
        throw new IllegalStateException(sb.toString());
    }

    public AnonymousClass01E A0A(String str) {
        if (str.equals(this.A0T)) {
            return this;
        }
        return this.A0G.A0U.A00(str);
    }

    public final ActivityC000900k A0B() {
        AnonymousClass05V r0 = this.A0F;
        if (r0 == null) {
            return null;
        }
        return (ActivityC000900k) r0.A00;
    }

    public final ActivityC000900k A0C() {
        ActivityC000900k A0B = A0B();
        if (A0B != null) {
            return A0B;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" not attached to an activity.");
        throw new IllegalStateException(sb.toString());
    }

    public AnonymousClass05W A0D() {
        return new AnonymousClass0EF(this);
    }

    public final AnonymousClass01F A0E() {
        if (this.A0F != null) {
            return this.A0G;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" has not been attached yet.");
        throw new IllegalStateException(sb.toString());
    }

    public final AnonymousClass01F A0F() {
        AnonymousClass01F r0 = this.A0H;
        if (r0 != null) {
            return r0;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" not associated with a fragment manager.");
        throw new IllegalStateException(sb.toString());
    }

    public AbstractC001200n A0G() {
        AnonymousClass0Yp r0 = this.A0I;
        if (r0 != null) {
            return r0;
        }
        throw new IllegalStateException("Can't access the Fragment View's LifecycleOwner when getView() is null i.e., before onCreateView() or after onDestroyView()");
    }

    public final CharSequence A0H(int i) {
        return A01().getResources().getText(i);
    }

    public final String A0I(int i) {
        return A01().getResources().getString(i);
    }

    public final String A0J(int i, Object... objArr) {
        return A01().getResources().getString(i, objArr);
    }

    public void A0K() {
        this.A0V = true;
        AnonymousClass05V r0 = this.A0F;
        if (r0 != null && r0.A00 != null) {
            this.A0V = false;
            this.A0V = true;
        }
    }

    public void A0L() {
        onLowMemory();
        for (AnonymousClass01E r0 : this.A0G.A0U.A02()) {
            if (r0 != null) {
                r0.A0L();
            }
        }
    }

    public void A0M() {
        if (!this.A0Z) {
            this.A0Z = true;
            if (A0c() && !this.A0a) {
                this.A0F.A04.A0b();
            }
        }
    }

    public void A0N() {
        ViewGroup viewGroup;
        if (this.A0C != null && A07().A0F) {
            if (this.A0F == null) {
                A07().A0F = false;
            } else if (Looper.myLooper() != this.A0F.A02.getLooper()) {
                this.A0F.A02.postAtFrontOfQueue(new RunnableC09230cZ(this));
            } else {
                AnonymousClass0O9 r1 = this.A0C;
                if (r1 != null) {
                    r1.A0F = false;
                }
                if (this.A0A != null && (viewGroup = this.A0B) != null && this.A0H != null) {
                    AbstractC06180Sm A01 = AbstractC06180Sm.A01(viewGroup);
                    A01.A04();
                    this.A0F.A02.post(new RunnableC09540d5(this, A01));
                }
            }
        }
    }

    public void A0O(int i, int i2, int i3, int i4) {
        if (this.A0C != null || i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
            A07().A01 = i;
            A07().A02 = i2;
            A07().A04 = i3;
            A07().A05 = i4;
        }
    }

    @Deprecated
    public void A0P(Intent intent, int i, Bundle bundle) {
        if (this.A0F != null) {
            AnonymousClass01F A0F = A0F();
            if (A0F.A03 != null) {
                A0F.A0D.addLast(new C06790Vc(this.A0T, i));
                if (!(intent == null || bundle == null)) {
                    intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundle);
                }
                A0F.A03.A00(null, intent);
                return;
            }
            AnonymousClass05V r1 = A0F.A07;
            if (i == -1) {
                r1.A01.startActivity(intent, bundle);
                return;
            }
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }

    public void A0Q(Configuration configuration) {
        onConfigurationChanged(configuration);
        for (AnonymousClass01E r0 : this.A0G.A0U.A02()) {
            if (r0 != null) {
                r0.A0Q(configuration);
            }
        }
    }

    public void A0R(Bundle bundle) {
        this.A0G.A0G();
        this.A03 = 1;
        this.A0V = false;
        if (Build.VERSION.SDK_INT >= 19) {
            this.A0K.A00(new AnonymousClass054() { // from class: androidx.fragment.app.Fragment$5
                @Override // X.AnonymousClass054
                public void AWQ(AnonymousClass074 r2, AbstractC001200n r3) {
                    View view;
                    if (r2 == AnonymousClass074.ON_STOP && (view = AnonymousClass01E.this.A0A) != null) {
                        view.cancelPendingInputEvents();
                    }
                }
            });
        }
        this.A0N.A00(bundle);
        A16(bundle);
        this.A0d = true;
        if (this.A0V) {
            this.A0K.A04(AnonymousClass074.ON_CREATE);
            return;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onCreate()");
        throw new C02150Af(sb.toString());
    }

    public void A0S(Bundle bundle) {
        A0w(bundle);
        this.A0N.A01(bundle);
        Parcelable A04 = this.A0G.A04();
        if (A04 != null) {
            bundle.putParcelable(ActivityC000900k.A05, A04);
        }
    }

    public void A0T(Bundle bundle) {
        Parcelable parcelable;
        if (bundle != null && (parcelable = bundle.getParcelable(ActivityC000900k.A05)) != null) {
            this.A0G.A0Q(parcelable);
            AnonymousClass01F r2 = this.A0G;
            r2.A0P = false;
            r2.A0Q = false;
            r2.A0A.A01 = false;
            r2.A0N(1);
        }
    }

    public void A0U(Bundle bundle) {
        AnonymousClass01F r0 = this.A0H;
        if (r0 == null || !r0.A0m()) {
            this.A05 = bundle;
            return;
        }
        throw new IllegalStateException("Fragment already added and state has been saved");
    }

    public void A0V(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        this.A0G.A0G();
        this.A0f = true;
        this.A0I = new AnonymousClass0Yp(this, AHb());
        View A10 = A10(bundle, layoutInflater, viewGroup);
        this.A0A = A10;
        AnonymousClass0Yp r0 = this.A0I;
        if (A10 != null) {
            r0.A00();
            this.A0A.setTag(R.id.view_tree_lifecycle_owner, this.A0I);
            this.A0A.setTag(R.id.view_tree_view_model_store_owner, this.A0I);
            this.A0A.setTag(R.id.view_tree_saved_state_registry_owner, this.A0I);
            this.A0L.A0B(this.A0I);
        } else if (r0.A00 != null) {
            throw new IllegalStateException("Called getViewLifecycleOwner() but onCreateView() returned null");
        } else {
            this.A0I = null;
        }
    }

    public void A0W(Menu menu) {
        if (!this.A0a) {
            this.A0G.A0R(menu);
        }
    }

    @Deprecated
    public void A0X(AnonymousClass01E r4, int i) {
        AnonymousClass01F r0;
        AnonymousClass01F r1 = this.A0H;
        if (r4 != null) {
            r0 = r4.A0H;
        } else {
            r0 = null;
        }
        if (r1 == null || r0 == null || r1 == r0) {
            for (AnonymousClass01E r12 = r4; r12 != null; r12 = r12.A08()) {
                if (r12.equals(this)) {
                    StringBuilder sb = new StringBuilder("Setting ");
                    sb.append(r4);
                    sb.append(" as the target of ");
                    sb.append(this);
                    sb.append(" would create a target cycle");
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            if (r4 == null) {
                this.A0S = null;
            } else if (this.A0H == null || r4.A0H == null) {
                this.A0S = null;
                this.A0E = r4;
                this.A04 = i;
                return;
            } else {
                this.A0S = r4.A0T;
            }
            this.A0E = null;
            this.A04 = i;
            return;
        }
        StringBuilder sb2 = new StringBuilder("Fragment ");
        sb2.append(r4);
        sb2.append(" must share the same FragmentManager to be set as a target fragment");
        throw new IllegalArgumentException(sb2.toString());
    }

    public void A0Y(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.A02));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.A01));
        printWriter.print(" mTag=");
        printWriter.println(this.A0R);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.A03);
        printWriter.print(" mWho=");
        printWriter.print(this.A0T);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.A00);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.A0U);
        printWriter.print(" mRemoving=");
        printWriter.print(this.A0g);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.A0Y);
        printWriter.print(" mInLayout=");
        printWriter.println(this.A0c);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.A0a);
        printWriter.print(" mDetached=");
        printWriter.print(this.A0X);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.A0e);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.A0Z);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.A0i);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.A0j);
        if (this.A0H != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.A0H);
        }
        if (this.A0F != null) {
            printWriter.print(str);
            printWriter.print("mHost=");
            printWriter.println(this.A0F);
        }
        if (this.A0D != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.A0D);
        }
        if (this.A05 != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.A05);
        }
        if (this.A06 != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.A06);
        }
        if (this.A08 != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.A08);
        }
        if (this.A07 != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewRegistryState=");
            printWriter.println(this.A07);
        }
        AnonymousClass01E A08 = A08();
        if (A08 != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(A08);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.A04);
        }
        printWriter.print(str);
        printWriter.print("mPopDirection=");
        AnonymousClass0O9 r0 = this.A0C;
        if (r0 == null) {
            z = false;
        } else {
            z = r0.A0G;
        }
        printWriter.println(z);
        AnonymousClass0O9 r02 = this.A0C;
        if (!(r02 == null || r02.A01 == 0)) {
            printWriter.print(str);
            printWriter.print("getEnterAnim=");
            AnonymousClass0O9 r03 = this.A0C;
            if (r03 == null) {
                i4 = 0;
            } else {
                i4 = r03.A01;
            }
            printWriter.println(i4);
        }
        AnonymousClass0O9 r04 = this.A0C;
        if (!(r04 == null || r04.A02 == 0)) {
            printWriter.print(str);
            printWriter.print("getExitAnim=");
            AnonymousClass0O9 r05 = this.A0C;
            if (r05 == null) {
                i3 = 0;
            } else {
                i3 = r05.A02;
            }
            printWriter.println(i3);
        }
        AnonymousClass0O9 r06 = this.A0C;
        if (!(r06 == null || r06.A04 == 0)) {
            printWriter.print(str);
            printWriter.print("getPopEnterAnim=");
            AnonymousClass0O9 r07 = this.A0C;
            if (r07 == null) {
                i2 = 0;
            } else {
                i2 = r07.A04;
            }
            printWriter.println(i2);
        }
        AnonymousClass0O9 r08 = this.A0C;
        if (!(r08 == null || r08.A05 == 0)) {
            printWriter.print(str);
            printWriter.print("getPopExitAnim=");
            AnonymousClass0O9 r09 = this.A0C;
            if (r09 == null) {
                i = 0;
            } else {
                i = r09.A05;
            }
            printWriter.println(i);
        }
        if (this.A0B != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.A0B);
        }
        if (this.A0A != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.A0A);
        }
        if (A0p() != null) {
            new AnonymousClass0Q3(this, AHb()).A03(str, fileDescriptor, printWriter, strArr);
        }
        printWriter.print(str);
        StringBuilder sb = new StringBuilder("Child ");
        sb.append(this.A0G);
        sb.append(":");
        printWriter.println(sb.toString());
        AnonymousClass01F r2 = this.A0G;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("  ");
        r2.A0h(sb2.toString(), fileDescriptor, printWriter, strArr);
    }

    public void A0Z(boolean z) {
        for (AnonymousClass01E r0 : this.A0G.A0U.A02()) {
            if (r0 != null) {
                r0.A0Z(z);
            }
        }
    }

    public void A0a(boolean z) {
        for (AnonymousClass01E r0 : this.A0G.A0U.A02()) {
            if (r0 != null) {
                r0.A0a(z);
            }
        }
    }

    public void A0b(boolean z) {
        if (this.A0e != z) {
            this.A0e = z;
            if (this.A0Z && A0c() && !this.A0a) {
                this.A0F.A04.A0b();
            }
        }
    }

    public final boolean A0c() {
        return this.A0F != null && this.A0U;
    }

    public final boolean A0d() {
        AnonymousClass01E r0;
        if (this.A0e) {
            return this.A0H == null || (r0 = this.A0D) == null || r0.A0d();
        }
        return false;
    }

    public final boolean A0e() {
        View view;
        return A0c() && !this.A0a && (view = this.A0A) != null && view.getWindowToken() != null && this.A0A.getVisibility() == 0;
    }

    public boolean A0f(Menu menu) {
        boolean z = false;
        if (this.A0a) {
            return false;
        }
        if (this.A0Z && this.A0e) {
            z = true;
            A0x(menu);
        }
        return z | this.A0G.A0o(menu);
    }

    public boolean A0g(Menu menu, MenuInflater menuInflater) {
        boolean z = false;
        if (this.A0a) {
            return false;
        }
        if (this.A0Z && this.A0e) {
            z = true;
            A0y(menu, menuInflater);
        }
        return z | this.A0G.A0p(menu, menuInflater);
    }

    public boolean A0h(MenuItem menuItem) {
        if (this.A0a) {
            return false;
        }
        if (A0o(menuItem)) {
            return true;
        }
        return this.A0G.A0q(menuItem);
    }

    public boolean A0i(MenuItem menuItem) {
        if (this.A0a) {
            return false;
        }
        if (!this.A0Z || !this.A0e || !A0z(menuItem)) {
            return this.A0G.A0r(menuItem);
        }
        return true;
    }

    public void A0k(Bundle bundle) {
        this.A0V = true;
    }

    public void A0l() {
        this.A0V = true;
    }

    @Deprecated
    public void A0m(Bundle bundle) {
        this.A0V = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0031, code lost:
        if (r6 != false) goto L_0x0033;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0n(boolean r6) {
        /*
            r5 = this;
            boolean r0 = r5.A0j
            r4 = 5
            if (r0 != 0) goto L_0x002a
            if (r6 == 0) goto L_0x002a
            int r0 = r5.A03
            if (r0 >= r4) goto L_0x002a
            X.01F r3 = r5.A0H
            if (r3 == 0) goto L_0x002a
            boolean r0 = r5.A0c()
            if (r0 == 0) goto L_0x002a
            boolean r0 = r5.A0d
            if (r0 == 0) goto L_0x002a
            X.0Ts r2 = r3.A0D(r5)
            X.01E r1 = r2.A02
            boolean r0 = r1.A0W
            if (r0 == 0) goto L_0x002a
            boolean r0 = r3.A0M
            if (r0 == 0) goto L_0x0041
            r0 = 1
            r3.A0N = r0
        L_0x002a:
            r5.A0j = r6
            int r0 = r5.A03
            if (r0 >= r4) goto L_0x0033
            r0 = 1
            if (r6 == 0) goto L_0x0034
        L_0x0033:
            r0 = 0
        L_0x0034:
            r5.A0W = r0
            android.os.Bundle r0 = r5.A06
            if (r0 == 0) goto L_0x0040
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r6)
            r5.A0P = r0
        L_0x0040:
            return
        L_0x0041:
            r0 = 0
            r1.A0W = r0
            r2.A04()
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01E.A0n(boolean):void");
    }

    public Context A0p() {
        AnonymousClass05V r0 = this.A0F;
        if (r0 == null) {
            return null;
        }
        return r0.A01;
    }

    public LayoutInflater A0q(Bundle bundle) {
        AnonymousClass05V r0 = this.A0F;
        if (r0 != null) {
            ActivityC000900k r1 = r0.A04;
            LayoutInflater cloneInContext = r1.getLayoutInflater().cloneInContext(r1);
            C013106e.A01(this.A0G.A0S, cloneInContext);
            return cloneInContext;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }

    public void A0r() {
        this.A0V = true;
    }

    public void A0s() {
        this.A0V = true;
    }

    @Deprecated
    public void A0t(int i, int i2, Intent intent) {
        if (AnonymousClass01F.A01(2)) {
            StringBuilder sb = new StringBuilder("Fragment ");
            sb.append(this);
            sb.append(" received the following in onActivityResult(): requestCode: ");
            sb.append(i);
            sb.append(" resultCode: ");
            sb.append(i2);
            sb.append(" data: ");
            sb.append(intent);
            Log.v("FragmentManager", sb.toString());
        }
    }

    @Deprecated
    public void A0u(Activity activity) {
        this.A0V = true;
    }

    public void A0v(Intent intent) {
        AnonymousClass05V r0 = this.A0F;
        if (r0 != null) {
            r0.A01.startActivity(intent, null);
            return;
        }
        StringBuilder sb = new StringBuilder("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }

    public View A10(Bundle bundle, LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return null;
    }

    public void A11() {
        this.A0V = true;
    }

    public void A12() {
        this.A0V = true;
    }

    public void A13() {
        this.A0V = true;
    }

    public void A14() {
        this.A0V = true;
    }

    public void A15(Context context) {
        Activity activity;
        this.A0V = true;
        AnonymousClass05V r0 = this.A0F;
        if (r0 != null && (activity = r0.A00) != null) {
            this.A0V = false;
            A0u(activity);
        }
    }

    public void A16(Bundle bundle) {
        this.A0V = true;
        A0T(bundle);
        AnonymousClass01F r2 = this.A0G;
        if (r2.A00 < 1) {
            r2.A0P = false;
            r2.A0Q = false;
            r2.A0A.A01 = false;
            r2.A0N(1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r2 == null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0023, code lost:
        if (X.AnonymousClass01F.A01(3) == false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        r1 = new java.lang.StringBuilder("Could not find Application instance from Context ");
        r1.append(A01().getApplicationContext());
        r1.append(", you will not be able to use AndroidViewModel with the default ViewModelProvider.Factory");
        android.util.Log.d("FragmentManager", r1.toString());
     */
    @Override // X.AbstractC001800t
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC009404s ACV() {
        /*
            r3 = this;
            X.01F r0 = r3.A0H
            if (r0 == 0) goto L_0x0056
            X.04s r1 = r3.A0M
            if (r1 != 0) goto L_0x004e
            r2 = 0
            android.content.Context r0 = r3.A01()
            android.content.Context r1 = r0.getApplicationContext()
        L_0x0011:
            boolean r0 = r1 instanceof android.content.ContextWrapper
            if (r0 == 0) goto L_0x001e
            boolean r0 = r1 instanceof android.app.Application
            if (r0 == 0) goto L_0x004f
            r2 = r1
            android.app.Application r2 = (android.app.Application) r2
            if (r2 != 0) goto L_0x0045
        L_0x001e:
            r0 = 3
            boolean r0 = X.AnonymousClass01F.A01(r0)
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "Could not find Application instance from Context "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            android.content.Context r0 = r3.A01()
            android.content.Context r0 = r0.getApplicationContext()
            r1.append(r0)
            java.lang.String r0 = ", you will not be able to use AndroidViewModel with the default ViewModelProvider.Factory"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "FragmentManager"
            android.util.Log.d(r0, r1)
        L_0x0045:
            android.os.Bundle r0 = r3.A05
            X.05E r1 = new X.05E
            r1.<init>(r2, r0, r3)
            r3.A0M = r1
        L_0x004e:
            return r1
        L_0x004f:
            android.content.ContextWrapper r1 = (android.content.ContextWrapper) r1
            android.content.Context r1 = r1.getBaseContext()
            goto L_0x0011
        L_0x0056:
            java.lang.String r1 = "Can't access ViewModels from detached fragment"
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass01E.ACV():X.04s");
    }

    @Override // X.AbstractC001200n
    public AbstractC009904y ADr() {
        return this.A0K;
    }

    @Override // X.AbstractC001500q
    public final AnonymousClass058 AGO() {
        return this.A0N.A00;
    }

    @Override // X.AbstractC001400p
    public AnonymousClass05C AHb() {
        AnonymousClass01F r2 = this.A0H;
        if (r2 == null) {
            throw new IllegalStateException("Can't access ViewModels from detached fragment");
        } else if (A00() != 1) {
            HashMap hashMap = r2.A0A.A04;
            AnonymousClass05C r1 = (AnonymousClass05C) hashMap.get(this.A0T);
            if (r1 != null) {
                return r1;
            }
            AnonymousClass05C r12 = new AnonymousClass05C();
            hashMap.put(this.A0T, r12);
            return r12;
        } else {
            throw new IllegalStateException("Calling getViewModelStore() before a Fragment reaches onCreate() when using setMaxLifecycle(INITIALIZED) is not supported");
        }
    }

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        this.A0V = true;
    }

    @Override // android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        A0C().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
        this.A0V = true;
    }

    @Deprecated
    public void startActivityForResult(Intent intent, int i) {
        A0P(intent, i, null);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append(getClass().getSimpleName());
        sb.append("{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("}");
        sb.append(" (");
        sb.append(this.A0T);
        int i = this.A02;
        if (i != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(i));
        }
        String str = this.A0R;
        if (str != null) {
            sb.append(" tag=");
            sb.append(str);
        }
        sb.append(")");
        return sb.toString();
    }
}
