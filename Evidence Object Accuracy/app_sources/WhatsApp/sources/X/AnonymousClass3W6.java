package X;

/* renamed from: X.3W6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3W6 implements AbstractC115925Tl {
    public final /* synthetic */ C44341yl A00;

    public AnonymousClass3W6(C44341yl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC115925Tl
    public C68433Vj A7w(AbstractC49242Jy r14, AbstractC49252Jz r15) {
        C44341yl r1 = this.A00;
        AnonymousClass01J r3 = r1.A03;
        AbstractC14440lR A0T = C12960it.A0T(r3);
        C15890o4 A0Y = C12970iu.A0Y(r3);
        AnonymousClass1CO r6 = (AnonymousClass1CO) r3.AHw.get();
        C44351ym r2 = r1.A02;
        return new C68433Vj(C12990iw.A0W(r3), new AnonymousClass3ER((AbstractC48942In) r2.A0f.get(), (AnonymousClass1B5) r2.A1O.A5t.get()), r6, (AnonymousClass1B0) r3.A2K.get(), (AnonymousClass1B3) r3.AI0.get(), r14, r15, A0Y, A0T);
    }
}
