package X;

import android.view.ViewGroup;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.InviteViaLinkView;
import java.util.HashSet;

/* renamed from: X.2gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54442gj extends AnonymousClass02M {
    public AnonymousClass024 A00 = new AnonymousClass024() { // from class: X.4rV
        @Override // X.AnonymousClass024
        public final void accept(Object obj) {
        }
    };
    public AnonymousClass3FF A01;
    public final AnonymousClass0QI A02;
    public final C15570nT A03;
    public final C237913a A04;
    public final C92624Wr A05 = new C92624Wr(0, null);
    public final AnonymousClass3DP A06;
    public final C15610nY A07;
    public final AnonymousClass1J1 A08;
    public final C15580nU A09;
    public final AnonymousClass12F A0A;

    public /* synthetic */ C54442gj(C15570nT r4, C237913a r5, AnonymousClass3DP r6, C15610nY r7, AnonymousClass1J1 r8, C15580nU r9, AnonymousClass12F r10) {
        this.A04 = r5;
        this.A03 = r4;
        this.A07 = r7;
        this.A08 = r8;
        this.A0A = r10;
        this.A09 = r9;
        this.A06 = r6;
        this.A02 = new AnonymousClass0QI(new C55272i7(this, r4, r7), C92624Wr.class);
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        AnonymousClass0QI r2 = this.A02;
        int i2 = ((C92624Wr) r2.A02(i)).A00;
        if (i2 == 1) {
            i2 = ((AnonymousClass3FF) ((C92624Wr) r2.A02(i)).A01).A03.hashCode();
        }
        return (long) i2;
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A02.A03;
    }

    public void A0E(AnonymousClass1JO r6) {
        HashSet A12 = C12970iu.A12();
        AnonymousClass3FF r0 = this.A01;
        if (r0 != null && r0.A00()) {
            A12.add(this.A05);
        }
        C103844rP r3 = new AnonymousClass02O() { // from class: X.4rP
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                return new C92624Wr(1, obj);
            }
        };
        HashSet A122 = C12970iu.A12();
        for (Object obj : r6.A00) {
            A122.add(r3.apply(obj));
        }
        A12.addAll(new HashSet(AnonymousClass1JO.A00(A122).A00));
        this.A02.A05(A12);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003b, code lost:
        if ((r1 ^ r4) == false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
        if (r2 != false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0096, code lost:
        if (r3.A00() == false) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0025, code lost:
        if (r7.A02.A0F(r9.A03) == false) goto L_0x0027;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00c9  */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ANH(X.AnonymousClass03U r11, int r12) {
        /*
        // Method dump skipped, instructions count: 221
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C54442gj.ANH(X.03U, int):void");
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new C55072hk(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.community_member_row_container), this.A03, this.A04, this.A07, this.A08, this.A0A);
        }
        C15580nU r3 = this.A09;
        InviteViaLinkView inviteViaLinkView = (InviteViaLinkView) C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.invite_via_link_button);
        inviteViaLinkView.setupOnClick(r3, (ActivityC13810kN) AnonymousClass12P.A02(viewGroup), null);
        TextView A0J = C12960it.A0J(inviteViaLinkView, R.id.invite_via_link_text);
        if (A0J != null) {
            A0J.setText(R.string.community_invite_via_link);
        }
        return new C75233jW(inviteViaLinkView, this);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        AnonymousClass3FF r0;
        return (i != 0 || (r0 = this.A01) == null || !r0.A00()) ? 1 : 0;
    }
}
