package X;

import android.os.Build;
import android.text.TextUtils;
import android.view.View;

/* renamed from: X.0KD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KD {
    public static void A00(View view, CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 26) {
            view.setTooltipText(charSequence);
            return;
        }
        AnonymousClass0WR r0 = AnonymousClass0WR.A0A;
        if (r0 != null && r0.A05 == view) {
            AnonymousClass0WR.A00(null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            AnonymousClass0WR r1 = AnonymousClass0WR.A09;
            if (r1 != null && r1.A05 == view) {
                r1.A01();
            }
            view.setOnLongClickListener(null);
            view.setLongClickable(false);
            view.setOnHoverListener(null);
            return;
        }
        new AnonymousClass0WR(view, charSequence);
    }
}
