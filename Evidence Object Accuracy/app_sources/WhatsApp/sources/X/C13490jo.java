package X;

import java.io.PrintStream;

/* renamed from: X.0jo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13490jo {
    public static final AbstractC13500jp A00;

    static {
        AbstractC13500jp r0;
        Integer num;
        try {
            try {
                num = (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
            } catch (Exception e) {
                PrintStream printStream = System.err;
                printStream.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
                e.printStackTrace(printStream);
            }
        } catch (Throwable th) {
            PrintStream printStream2 = System.err;
            String name = C79633qu.class.getName();
            StringBuilder sb = new StringBuilder(name.length() + 133);
            sb.append("An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy ");
            sb.append(name);
            sb.append("will be used. The error is: ");
            printStream2.println(sb.toString());
            th.printStackTrace(printStream2);
            r0 = new C79633qu();
        }
        if (num != null && num.intValue() >= 19) {
            r0 = new C79643qv();
            A00 = r0;
        }
        if (!Boolean.getBoolean("com.google.devtools.build.android.desugar.runtime.twr_disable_mimic")) {
            r0 = new C79653qw();
        } else {
            r0 = new C79633qu();
        }
        A00 = r0;
    }
}
