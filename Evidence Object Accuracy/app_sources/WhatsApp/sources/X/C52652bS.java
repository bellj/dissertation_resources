package X;

import android.net.Uri;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/* renamed from: X.2bS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52652bS extends WebViewClient {
    public final /* synthetic */ AnonymousClass12P A00;
    public final /* synthetic */ AnonymousClass39H A01;

    public C52652bS(AnonymousClass12P r1, AnonymousClass39H r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.webkit.WebViewClient
    public void onReceivedError(WebView webView, int i, String str, String str2) {
        this.A01.A0E("WebViewClient error", true);
    }

    @Override // android.webkit.WebViewClient
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        this.A01.A0E("WebViewClient error", true);
    }

    @Override // android.webkit.WebViewClient
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith("y://error")) {
            this.A01.A0E("iFrame api script error", true);
        }
        if (str.startsWith("https://")) {
            this.A00.Ab9(webView.getContext(), Uri.parse(str));
        }
        return true;
    }
}
