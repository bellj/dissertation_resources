package X;

import com.whatsapp.jid.UserJid;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Yw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69343Yw implements AbstractC32461cC {
    public final /* synthetic */ C468027s A00;

    public C69343Yw(C468027s r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC32461cC
    public void APl(int i) {
        C468027s r2 = this.A00;
        C63373Bi r3 = new C63373Bi(r2.A0J, null, null, null, null, null, 0, 2, 0);
        AnonymousClass016 r1 = r2.A0R;
        if (i != -1) {
            r1.A0A(new C92634Ws(r3, i));
            r2.A05(5);
            return;
        }
        throw C12960it.A0U("Error code expected but default success code '-1' was provided.");
    }

    @Override // X.AbstractC32461cC
    public void AR7(C15580nU r14, C15580nU r15, UserJid userJid, AnonymousClass1PD r17, String str, String str2, String str3, Map map, int i, int i2, long j, long j2) {
        C468027s r1 = this.A00;
        List A0B = r1.A0I.A0B(map);
        r1.A0J = r14;
        if (r15 != null) {
            r1.A0N = C12960it.A1U(r1.A04.A01(r15).size());
        }
        r1.A06(new C63373Bi(r14, userJid, r17, str, str3, A0B, i, i2, j));
    }
}
