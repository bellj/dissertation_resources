package X;

import android.content.Context;

/* renamed from: X.4Yp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93014Yp {
    public static final AnonymousClass4V8 A00(Context context, AnonymousClass4BH r2) {
        C16700pc.A0F(r2, context);
        float f = A01(r2).A00(context).A01;
        switch (r2.ordinal()) {
            case 0:
                return new AnonymousClass48D(f);
            case 1:
            case 2:
                return new AnonymousClass48G(f);
            case 3:
                return new AnonymousClass48F(f);
            case 4:
                return new AnonymousClass48E(f);
            default:
                throw new C113285Gx();
        }
    }

    public static final AnonymousClass4BQ A01(AnonymousClass4BH r1) {
        C16700pc.A0E(r1, 0);
        switch (r1.ordinal()) {
            case 0:
                return AnonymousClass4BQ.A02;
            case 1:
                return AnonymousClass4BQ.A05;
            case 2:
                return AnonymousClass4BQ.A04;
            case 3:
                return AnonymousClass4BQ.A03;
            case 4:
                return AnonymousClass4BQ.A01;
            default:
                throw new C113285Gx();
        }
    }
}
