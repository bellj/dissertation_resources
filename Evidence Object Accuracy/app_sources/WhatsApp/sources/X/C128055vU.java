package X;

import android.content.Context;

/* renamed from: X.5vU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128055vU {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final C18650sn A03;
    public final C18610sj A04;
    public final C129565xv A05;
    public final C30931Zj A06 = C117305Zk.A0V("PaymentComplianceAction", "network");

    public C128055vU(Context context, C14900mE r4, C18640sm r5, C18650sn r6, C18610sj r7, C129565xv r8) {
        this.A00 = context;
        this.A01 = r4;
        this.A05 = r8;
        this.A04 = r7;
        this.A02 = r5;
        this.A03 = r6;
    }
}
