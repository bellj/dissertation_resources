package X;

import android.text.TextUtils;

/* renamed from: X.0ra  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17900ra {
    public C17930rd A00;
    public boolean A01;
    public final C15570nT A02;
    public final AbstractC17860rW A03;
    public final AnonymousClass162 A04;
    public final C30931Zj A05 = C30931Zj.A00("PaymentsCountryManager", "infra", "COMMON");

    public C17900ra(C15570nT r4, AbstractC17860rW r5, AnonymousClass162 r6) {
        this.A02 = r4;
        this.A04 = r6;
        this.A03 = r5;
    }

    public synchronized AbstractC30791Yv A00() {
        AbstractC30791Yv r0;
        if (!this.A01) {
            A02();
        }
        C17930rd r02 = this.A00;
        if (r02 != null) {
            r0 = r02.A02;
        } else {
            r0 = null;
        }
        return r0;
    }

    public synchronized C17930rd A01() {
        if (!this.A01) {
            A02();
        }
        return this.A00;
    }

    public final synchronized void A02() {
        C17930rd r1;
        C30931Zj r4 = this.A05;
        r4.A03(null, "tryInitFromMock: no mockedCountry");
        C15570nT r9 = this.A02;
        r9.A08();
        C27631Ih r0 = r9.A05;
        if (r0 != null) {
            String str = r0.user;
            C17930rd[] r7 = C17930rd.A0I;
            int length = r7.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    r1 = C17930rd.A0F;
                    break;
                }
                r1 = r7[i];
                if (str.startsWith(r1.A04)) {
                    break;
                }
                i++;
            }
            C17930rd r72 = C17930rd.A0F;
            if (r1 != r72) {
                A03(r1);
            } else {
                C17930rd[] r3 = C17930rd.A0G;
                int length2 = r3.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length2) {
                        break;
                    } else if (str.startsWith(r3[i2].A04)) {
                        r9.A08();
                        String A04 = C248917h.A04(r9.A05);
                        String str2 = "";
                        if (TextUtils.isEmpty(str2)) {
                            str2 = AnonymousClass1ZT.A01(A04);
                        }
                        if (TextUtils.isEmpty(A04) || TextUtils.isEmpty(str2)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("phoneNumber:");
                            sb.append(A04);
                            sb.append(" countryCode:");
                            sb.append(str2);
                            r4.A06(sb.toString());
                        } else {
                            C17930rd A01 = C17930rd.A01(str2);
                            if (A01 == r72) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("not enabled with unsupported country code: ");
                                sb2.append(str2);
                                r4.A03(null, sb2.toString());
                            } else {
                                A03(A01);
                            }
                        }
                    } else {
                        i2++;
                    }
                }
            }
            this.A01 = true;
        }
        this.A00 = null;
        this.A01 = true;
    }

    public final void A03(C17930rd r4) {
        this.A00 = r4;
        C30931Zj r2 = this.A05;
        StringBuilder sb = new StringBuilder("init enabled for country: ");
        sb.append(r4.A03);
        sb.append(" and default currency: ");
        sb.append(((AbstractC30781Yu) r4.A02).A04);
        r2.A06(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000f, code lost:
        if (r1 == false) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A04() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.A01     // Catch: all -> 0x0014
            if (r0 != 0) goto L_0x0008
            r2.A02()     // Catch: all -> 0x0014
        L_0x0008:
            X.0rd r0 = r2.A00     // Catch: all -> 0x0014
            if (r0 == 0) goto L_0x0011
            boolean r1 = r0.A06     // Catch: all -> 0x0014
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            monitor-exit(r2)
            return r0
        L_0x0014:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C17900ra.A04():boolean");
    }
}
