package X;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.whatsapp.util.Log;

/* renamed from: X.1YG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YG extends AbstractC16280ok {
    public final Context A00;
    public final C231410n A01;

    public AnonymousClass1YG(Context context, AbstractC15710nm r9, C231410n r10, C14850m9 r11) {
        super(context, r9, "companion_devices.db", null, 9, r11.A07(781));
        this.A00 = context;
        this.A01 = r10;
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        return AnonymousClass1Tx.A01(super.A00(), this.A01);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS devices");
        sQLiteDatabase.execSQL("CREATE TABLE devices (_id INTEGER PRIMARY KEY AUTOINCREMENT,device_id TEXT,device_os TEXT,platform_type INTEGER,last_active INTEGER,login_time INTEGER,logout_time INTEGER,adv_key_index INTEGER,full_sync_required INTEGER,place_name TEXT);");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS companion_device_jid_index ON devices(device_id);");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS devices_history");
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("CompanionDeviceDbHelper/downgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        onCreate(sQLiteDatabase);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("CompanionDeviceDbHelper/upgrade from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(i2);
        Log.i(sb.toString());
        switch (i) {
            case 1:
                sQLiteDatabase.execSQL("ALTER TABLE devices ADD platform_type INTEGER");
            case 2:
                sQLiteDatabase.execSQL("ALTER TABLE devices ADD login_time INTEGER");
            case 3:
                sQLiteDatabase.execSQL("ALTER TABLE devices ADD adv_key_index INTEGER NOT NULL DEFAULT 0");
            case 4:
                sQLiteDatabase.execSQL("ALTER TABLE devices ADD full_sync_required INTEGER NOT NULL DEFAULT 0");
            case 5:
                sQLiteDatabase.execSQL("ALTER TABLE devices ADD place_name TEXT");
            case 6:
                sQLiteDatabase.execSQL("ALTER TABLE devices ADD logout_time INTEGER NOT NULL DEFAULT 0");
                break;
            case 7:
            case 8:
                break;
            default:
                Log.e("CompanionDeviceDbHelper/upgrade unknown old version");
                onCreate(sQLiteDatabase);
                return;
        }
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS devices_history");
    }
}
