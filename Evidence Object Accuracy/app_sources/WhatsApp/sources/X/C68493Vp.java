package X;

import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: X.3Vp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68493Vp implements AnonymousClass2K1 {
    public final float A00;
    public final LatLng A01;
    public final AnonymousClass1P6 A02;
    public final String A03;
    public final String A04;
    public final /* synthetic */ AnonymousClass3ER A05;

    public C68493Vp(LatLng latLng, AnonymousClass1P6 r2, AnonymousClass3ER r3, String str, String str2, float f) {
        this.A05 = r3;
        this.A01 = latLng;
        this.A04 = str;
        this.A00 = f;
        this.A03 = str2;
        this.A02 = r2;
    }

    @Override // X.AnonymousClass2K1
    public void APl(int i) {
        this.A02.ARL(i);
    }

    @Override // X.AnonymousClass2K1
    public /* bridge */ /* synthetic */ void AX4(Object obj) {
        int i;
        C89304Jl r7 = (C89304Jl) obj;
        LatLng latLng = this.A01;
        double d = latLng.A00;
        double d2 = latLng.A01;
        ArrayList A01 = C65013Hu.A01(d, d2, 14);
        AnonymousClass009.A05(A01);
        Number number = (Number) C12980iv.A0o(A01);
        Number number2 = (Number) A01.get(1);
        int i2 = 14;
        StringBuilder A0h = C12960it.A0h();
        do {
            char c = '0';
            long j = (long) (1 << (i2 - 1));
            if ((number.longValue() & j) != 0) {
                c = (char) 49;
            }
            if ((number2.longValue() & j) != 0) {
                c = (char) (((char) (c + 1)) + 1);
            }
            A0h.append(c);
            i2--;
        } while (i2 > 0);
        String obj2 = A0h.toString();
        Map map = r7.A00;
        boolean containsKey = map.containsKey(obj2);
        AnonymousClass1P6 r0 = this.A02;
        if (containsKey) {
            i = C12960it.A05(map.get(obj2));
        } else {
            i = 10;
        }
        String str = this.A03;
        ArrayList A012 = C65013Hu.A01(d, d2, i);
        AnonymousClass009.A05(A012);
        LatLng A02 = AnonymousClass1U5.A02(C65013Hu.A02(i, C12980iv.A0G(A012.get(0)), C12980iv.A0G(A012.get(1))));
        long A0G = C12980iv.A0G(A012.get(0));
        long A0G2 = C12980iv.A0G(A012.get(1));
        long j2 = (long) (2 << (i - 1));
        double d3 = 360.0d / ((double) j2);
        ArrayList A0l = C12960it.A0l();
        A0l.add(Double.valueOf(C65013Hu.A00(A0G2, j2, true)));
        A0l.add(Double.valueOf((((double) A0G) * d3) - 180.0d));
        A0l.add(Double.valueOf(C65013Hu.A00(A0G2, j2, false)));
        A0l.add(Double.valueOf((d3 * ((double) (A0G + 1))) - 180.0d));
        String str2 = this.A04;
        double d4 = (double) this.A00;
        r0.ARM(new C48122Ek(Double.valueOf(((double) (AnonymousClass3AR.A00(AnonymousClass1U5.A02(A0l), new LatLng(((Number) A0l.get(2)).doubleValue(), ((Number) A0l.get(3)).doubleValue())) / 2.0f)) + d4), Double.valueOf(d), Double.valueOf(d2), Double.valueOf(A02.A00), Double.valueOf(A02.A01), Double.valueOf(d4), str2, str));
    }
}
