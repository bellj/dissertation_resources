package X;

/* renamed from: X.1Ms  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28321Ms<K> extends AbstractC17940re<K> {
    public final transient AnonymousClass1Mr list;
    public final transient AbstractC17190qP map;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return true;
    }

    public C28321Ms(AbstractC17190qP r1, AnonymousClass1Mr r2) {
        this.map = r1;
        this.list = r2;
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf
    public AnonymousClass1Mr asList() {
        return this.list;
    }

    @Override // X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection
    public boolean contains(Object obj) {
        return this.map.get(obj) != null;
    }

    @Override // X.AbstractC17950rf
    public int copyIntoArray(Object[] objArr, int i) {
        return asList().copyIntoArray(objArr, i);
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public AnonymousClass1I5 iterator() {
        return asList().iterator();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public int size() {
        return this.map.size();
    }
}
