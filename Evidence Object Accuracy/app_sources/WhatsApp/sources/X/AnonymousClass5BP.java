package X;

import java.io.Serializable;

/* renamed from: X.5BP  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BP implements Serializable {
    public int element;

    @Override // java.lang.Object
    public String toString() {
        return String.valueOf(this.element);
    }
}
