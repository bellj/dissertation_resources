package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.69a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329869a implements AbstractC43531xB {
    public final C18600si A00;

    public C1329869a(C18600si r1) {
        this.A00 = r1;
    }

    public static final void A00(AnonymousClass102 r7, AnonymousClass1V8 r8, AnonymousClass1V8 r9, ArrayList arrayList, int i) {
        int length;
        int i2 = 0;
        if (i == 2) {
            AnonymousClass1V8[] r6 = r9.A03;
            if (r6 != null) {
                int length2 = r6.length;
                while (i2 < length2) {
                    AnonymousClass1V8 r2 = r6[i2];
                    if (r2 != null) {
                        if ("bank".equals(r2.A00)) {
                            C119755f3 r0 = new C119755f3();
                            r0.A01(r7, r8, 2);
                            r0.A01(r7, r2, 2);
                            arrayList.add(r0);
                        } else {
                            String str = r2.A00;
                            if ("psp".equals(str) || "psp-routing".equals(str)) {
                                C119715ez r02 = new C119715ez();
                                r02.A01(r7, r2, 2);
                                arrayList.add(r02);
                            }
                        }
                    }
                    i2++;
                }
            }
        } else if (i == 4) {
            AnonymousClass1V8[] r3 = r9.A03;
            if (r3 != null && (length = r3.length) > 0) {
                do {
                    AnonymousClass1V8 r1 = r3[i2];
                    if (r1 != null) {
                        C119755f3 r03 = new C119755f3();
                        r03.A01(r7, r1, 4);
                        arrayList.add(r03);
                    }
                    i2++;
                } while (i2 < length);
            }
        } else if (i != 5) {
            StringBuilder A0k = C12960it.A0k("PAY: IndiaProtoParser got action: ");
            A0k.append(i);
            Log.i(C12960it.A0d("; nothing to do", A0k));
        } else {
            C119715ez r04 = new C119715ez();
            r04.A01(r7, r9, 5);
            arrayList.add(r04);
        }
    }

    @Override // X.AbstractC43531xB
    public /* synthetic */ int AH0() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00ad A[SYNTHETIC] */
    @Override // X.AbstractC43531xB
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList AYv(X.AnonymousClass102 r10, X.AnonymousClass1V8 r11) {
        /*
            r9 = this;
            X.1V8 r4 = X.C117305Zk.A0c(r11)
            java.util.ArrayList r3 = X.C12960it.A0l()
            if (r4 != 0) goto L_0x0010
            java.lang.String r0 = "PAY: IndiaProtoParser empty account node"
            com.whatsapp.util.Log.w(r0)
        L_0x000f:
            return r3
        L_0x0010:
            java.lang.String r0 = "wa-support-phone-number"
            r2 = 0
            java.lang.String r1 = r4.A0I(r0, r2)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0022
            X.0si r0 = r9.A00
            r0.A0I(r1)
        L_0x0022:
            java.lang.String r0 = "action"
            java.lang.String r1 = r4.A0I(r0, r2)
            java.lang.String r0 = "upi-batch"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x005a
            r7 = 1
        L_0x0031:
            java.lang.String r6 = "psp-config"
            r8 = 2
            r5 = 0
            r0 = 1
            if (r7 == r0) goto L_0x0099
            r0 = 10
            if (r7 == r0) goto L_0x0099
            if (r7 != r8) goto L_0x00ce
            A00(r10, r4, r4, r3, r7)
            X.1V8[] r2 = r4.A03
            if (r2 == 0) goto L_0x000f
        L_0x0045:
            int r0 = r2.length
            if (r5 >= r0) goto L_0x000f
            r1 = r2[r5]
            if (r1 == 0) goto L_0x0057
            java.lang.String r0 = r1.A00
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0057
            A00(r10, r4, r1, r3, r7)
        L_0x0057:
            int r5 = r5 + 1
            goto L_0x0045
        L_0x005a:
            java.lang.String r0 = "upi-get-banks"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0064
            r7 = 2
            goto L_0x0031
        L_0x0064:
            java.lang.String r0 = "upi-register-vpa"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x006e
            r7 = 4
            goto L_0x0031
        L_0x006e:
            java.lang.String r0 = "upi-list-keys"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0078
            r7 = 5
            goto L_0x0031
        L_0x0078:
            java.lang.String r0 = "upi-check-mpin"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x0082
            r7 = 6
            goto L_0x0031
        L_0x0082:
            java.lang.String r0 = "pay-precheck"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x008d
            r7 = 8
            goto L_0x0031
        L_0x008d:
            java.lang.String r0 = "upi-get-psp-routing-and-list-keys"
            boolean r0 = r0.equalsIgnoreCase(r1)
            r7 = 0
            if (r0 == 0) goto L_0x0031
            r7 = 10
            goto L_0x0031
        L_0x0099:
            X.1V8[] r7 = r4.A03
            if (r7 == 0) goto L_0x000f
        L_0x009d:
            int r0 = r7.length
            if (r5 >= r0) goto L_0x000f
            r2 = r7[r5]
            if (r2 == 0) goto L_0x00ad
            java.lang.String r1 = r2.A00
            int r0 = r1.hashCode()
            switch(r0) {
                case -384112062: goto L_0x00c4;
                case 3288564: goto L_0x00b7;
                case 93503927: goto L_0x00b0;
                default: goto L_0x00ad;
            }
        L_0x00ad:
            int r5 = r5 + 1
            goto L_0x009d
        L_0x00b0:
            java.lang.String r0 = "banks"
            boolean r0 = r1.equals(r0)
            goto L_0x00c8
        L_0x00b7:
            java.lang.String r0 = "keys"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00ad
            r0 = 5
            A00(r10, r4, r2, r3, r0)
            goto L_0x00ad
        L_0x00c4:
            boolean r0 = r1.equals(r6)
        L_0x00c8:
            if (r0 == 0) goto L_0x00ad
            A00(r10, r4, r2, r3, r8)
            goto L_0x00ad
        L_0x00ce:
            A00(r10, r4, r4, r3, r7)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1329869a.AYv(X.102, X.1V8):java.util.ArrayList");
    }

    @Override // X.AbstractC43531xB
    public /* synthetic */ C14580lf AYw(AnonymousClass1V8 r2) {
        throw C12980iv.A0u("Asynchronous parsing is not supported in Sync Mode");
    }
}
