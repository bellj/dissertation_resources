package X;

import android.net.Uri;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.whatsapp.support.faq.FaqItemActivity;

/* renamed from: X.2bT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52662bT extends WebViewClient {
    public final /* synthetic */ FaqItemActivity A00;

    public C52662bT(FaqItemActivity faqItemActivity) {
        this.A00 = faqItemActivity;
    }

    public final boolean A00(Uri uri) {
        Class ABa;
        if (!"internal".equals(uri.getScheme())) {
            return false;
        }
        FaqItemActivity faqItemActivity = this.A00;
        if (!"ombudsman".equals(uri.getHost()) || (ABa = faqItemActivity.A04.A02().ABa()) == null) {
            return true;
        }
        faqItemActivity.startActivity(C12990iw.A0D(faqItemActivity, ABa));
        return true;
    }

    @Override // android.webkit.WebViewClient
    public void onPageFinished(WebView webView, String str) {
        AnonymousClass3FC r0 = this.A00.A05;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // android.webkit.WebViewClient
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        return A00(webResourceRequest.getUrl());
    }

    @Override // android.webkit.WebViewClient
    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return A00(Uri.parse(str));
    }
}
