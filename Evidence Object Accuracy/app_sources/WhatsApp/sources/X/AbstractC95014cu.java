package X;

import java.util.Map;
import java.util.Set;

/* renamed from: X.4cu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC95014cu {
    public static final void A03(C78633pE r2, Object obj, StringBuilder sb) {
        String obj2;
        int i = r2.A02;
        if (i == 11) {
            Class cls = r2.A06;
            C13020j0.A01(cls);
            obj2 = cls.cast(obj).toString();
        } else if (i == 7) {
            obj2 = "\"";
            sb.append(obj2);
            sb.append(AnonymousClass4ZT.A00((String) obj));
        } else {
            sb.append(obj);
            return;
        }
        sb.append(obj2);
    }

    public Object A04(C78633pE r6) {
        if (this instanceof C78943pn) {
            C78943pn r1 = (C78943pn) this;
            int i = r6.A04;
            if (i == 1) {
                return Integer.valueOf(r1.A05);
            }
            if (i == 2) {
                return r1.A03;
            }
            if (i == 3) {
                return Integer.valueOf(r1.A00);
            }
            if (i == 4) {
                return r1.A04;
            }
            throw C12960it.A0U(C12960it.A0e("Unknown SafeParcelable id=", C12980iv.A0t(37), i));
        } else if (this instanceof C78933pm) {
            C78933pm r12 = (C78933pm) this;
            int i2 = r6.A04;
            if (i2 == 1) {
                return Integer.valueOf(r12.A04);
            }
            if (i2 == 2) {
                return r12.A00;
            }
            if (i2 == 3) {
                return r12.A02;
            }
            if (i2 == 4) {
                return r12.A01;
            }
            throw C12960it.A0U(C12960it.A0e("Unknown SafeParcelable id=", C12980iv.A0t(37), i2));
        } else if (this instanceof C78923pl) {
            C78923pl r0 = (C78923pl) this;
            int i3 = r6.A04;
            switch (i3) {
                case 1:
                    return Integer.valueOf(r0.A05);
                case 2:
                    return r0.A00;
                case 3:
                    return r0.A01;
                case 4:
                    return r0.A02;
                case 5:
                    return r0.A03;
                case 6:
                    return r0.A04;
                default:
                    throw C12960it.A0U(C12960it.A0e("Unknown SafeParcelable id=", C12980iv.A0t(37), i3));
            }
        } else if (!(this instanceof C78913pk)) {
            String str = r6.A07;
            boolean z = ((AbstractC78953po) this) instanceof C78763pV;
            if (r6.A06 != null) {
                if (!z) {
                    try {
                        char upperCase = Character.toUpperCase(str.charAt(0));
                        String substring = str.substring(1);
                        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(substring) + 4);
                        A0t.append("get");
                        A0t.append(upperCase);
                        A0t.append(substring);
                        return getClass().getMethod(A0t.toString(), new Class[0]).invoke(this, new Object[0]);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    throw C12980iv.A0u("Converting to JSON does not require this method.");
                }
            } else if (!z) {
                return null;
            } else {
                throw C12980iv.A0u("Converting to JSON does not require this method.");
            }
        } else {
            C78913pk r13 = (C78913pk) this;
            int i4 = r6.A04;
            if (i4 == 1) {
                return Integer.valueOf(r13.A03);
            }
            if (i4 == 2) {
                return r13.A02;
            }
            if (i4 == 4) {
                return r13.A01;
            }
            throw C12960it.A0U(C12960it.A0e("Unknown SafeParcelable id=", C12980iv.A0t(37), i4));
        }
    }

    public Map A05() {
        if (this instanceof C78763pV) {
            C78763pV r1 = (C78763pV) this;
            C78443ov r0 = r1.A04;
            if (r0 == null) {
                return null;
            }
            String str = r1.A05;
            C13020j0.A01(str);
            return (Map) r0.A02.get(str);
        } else if (this instanceof C78943pn) {
            return C78943pn.A07;
        } else {
            if (this instanceof C78933pm) {
                return C78933pm.A06;
            }
            if (!(this instanceof C78923pl)) {
                return C78913pk.A05;
            }
            return C78923pl.A06;
        }
    }

    public boolean A06(C78633pE r3) {
        Set set;
        if (this instanceof C78943pn) {
            set = ((C78943pn) this).A06;
        } else if (this instanceof C78933pm) {
            set = ((C78933pm) this).A05;
        } else if (this instanceof C78923pl) {
            return true;
        } else {
            if (this instanceof C78913pk) {
                set = ((C78913pk) this).A04;
            } else if (r3.A03 == 11) {
                if (r3.A0A) {
                    throw C12980iv.A0u("Concrete type arrays not supported");
                }
                throw C12980iv.A0u("Concrete types not supported");
            } else if (!(((AbstractC78953po) this) instanceof C78763pV)) {
                return false;
            } else {
                throw C12980iv.A0u("Converting to JSON does not require this method.");
            }
        }
        return AbstractC78753pU.A00(set, r3.A04);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0044, code lost:
        if (r2.A02.containsKey(r6) != false) goto L_0x0046;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b1  */
    @Override // java.lang.Object
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r10 = this;
            java.util.Map r5 = r10.A05()
            r0 = 100
            java.lang.StringBuilder r3 = X.C12980iv.A0t(r0)
            java.util.Iterator r9 = X.C72453ed.A10(r5)
        L_0x000e:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x00c3
            java.lang.String r8 = X.C12970iu.A0x(r9)
            java.lang.Object r7 = r5.get(r8)
            X.3pE r7 = (X.C78633pE) r7
            boolean r0 = r10.A06(r7)
            if (r0 == 0) goto L_0x000e
            java.lang.Object r6 = r10.A04(r7)
            X.5Qd r2 = r7.A00
            if (r2 == 0) goto L_0x0046
            X.3pN r2 = (X.C78693pN) r2
            java.lang.Number r6 = (java.lang.Number) r6
            android.util.SparseArray r1 = r2.A01
            int r0 = r6.intValue()
            java.lang.Object r1 = r1.get(r0)
            if (r1 != 0) goto L_0x00c1
            java.util.HashMap r0 = r2.A02
            java.lang.String r6 = "gms_unknown"
            boolean r0 = r0.containsKey(r6)
            if (r0 == 0) goto L_0x00c1
        L_0x0046:
            int r0 = r3.length()
            java.lang.String r4 = ","
            if (r0 != 0) goto L_0x00bd
            java.lang.String r0 = "{"
            r3.append(r0)
        L_0x0053:
            java.lang.String r1 = "\""
            r3.append(r1)
            r3.append(r8)
            java.lang.String r0 = "\":"
            r3.append(r0)
            if (r6 != 0) goto L_0x0068
            java.lang.String r0 = "null"
        L_0x0064:
            r3.append(r0)
            goto L_0x000e
        L_0x0068:
            int r0 = r7.A03
            switch(r0) {
                case 8: goto L_0x0098;
                case 9: goto L_0x009f;
                case 10: goto L_0x00b6;
                default: goto L_0x006d;
            }
        L_0x006d:
            boolean r0 = r7.A09
            if (r0 == 0) goto L_0x0093
            java.util.AbstractList r6 = (java.util.AbstractList) r6
            java.lang.String r0 = "["
            r3.append(r0)
            int r2 = r6.size()
            r1 = 0
        L_0x007d:
            if (r1 >= r2) goto L_0x0090
            if (r1 <= 0) goto L_0x0084
            r3.append(r4)
        L_0x0084:
            java.lang.Object r0 = r6.get(r1)
            if (r0 == 0) goto L_0x008d
            A03(r7, r0, r3)
        L_0x008d:
            int r1 = r1 + 1
            goto L_0x007d
        L_0x0090:
            java.lang.String r0 = "]"
            goto L_0x0064
        L_0x0093:
            A03(r7, r6, r3)
            goto L_0x000e
        L_0x0098:
            r3.append(r1)
            byte[] r6 = (byte[]) r6
            r0 = 0
            goto L_0x00a6
        L_0x009f:
            r3.append(r1)
            byte[] r6 = (byte[]) r6
            r0 = 10
        L_0x00a6:
            if (r6 != 0) goto L_0x00b1
            r0 = 0
        L_0x00a9:
            r3.append(r0)
            r3.append(r1)
            goto L_0x000e
        L_0x00b1:
            java.lang.String r0 = android.util.Base64.encodeToString(r6, r0)
            goto L_0x00a9
        L_0x00b6:
            java.util.HashMap r6 = (java.util.HashMap) r6
            X.AnonymousClass4DP.A00(r3, r6)
            goto L_0x000e
        L_0x00bd:
            r3.append(r4)
            goto L_0x0053
        L_0x00c1:
            r6 = r1
            goto L_0x0046
        L_0x00c3:
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x00d0
            java.lang.String r0 = "}"
        L_0x00cb:
            java.lang.String r0 = X.C12960it.A0d(r0, r3)
            return r0
        L_0x00d0:
            java.lang.String r0 = "{}"
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC95014cu.toString():java.lang.String");
    }
}
