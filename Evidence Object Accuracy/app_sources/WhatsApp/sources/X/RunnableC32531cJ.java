package X;

import com.facebook.redex.RunnableBRunnable0Shape6S0100000_I0_6;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Arrays;
import java.util.List;

/* renamed from: X.1cJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC32531cJ extends AbstractC32541cK implements Runnable, AbstractC32491cF {
    public int A00;
    public C15580nU A01;
    public final C21320xE A02;
    public final C20710wC A03;
    public final AnonymousClass1P4 A04;
    public final C14860mA A05;
    public final String A06;
    public final List A07;
    public final boolean A08;

    public RunnableC32531cJ(C21320xE r11, C20710wC r12, C15580nU r13, AnonymousClass1P4 r14, C14860mA r15, String str, List list, int i) {
        this(r11, r12, r13, r14, r15, str, list, i, true);
    }

    public RunnableC32531cJ(C21320xE r2, C20710wC r3, C15580nU r4, AnonymousClass1P4 r5, C14860mA r6, String str, List list, int i, boolean z) {
        Log.a(r4 != null);
        this.A05 = r6;
        this.A03 = r3;
        this.A02 = r2;
        this.A01 = r4;
        this.A06 = str;
        this.A07 = list;
        this.A00 = i;
        this.A04 = r5;
        this.A08 = z;
        if (list != null) {
            Arrays.deepToString(list.toArray());
        }
    }

    public void A01(Integer num) {
        C63633Ci r4;
        int i;
        UserJid userJid;
        Integer num2;
        AnonymousClass3F3 r3;
        AnonymousClass30L r2;
        int i2;
        GroupChatInfo groupChatInfo;
        if (!(this instanceof C36181jS)) {
            if (this instanceof AnonymousClass32V) {
                AnonymousClass32V r5 = (AnonymousClass32V) this;
                AnonymousClass3FT r1 = r5.A00;
                r1.A01.A28();
                r4 = r1.A00;
                if (r4 != null) {
                    i = 2;
                    if (num != null) {
                        userJid = r5.A01;
                        num2 = r5.A02;
                    } else {
                        UserJid userJid2 = r5.A01;
                        Integer num3 = r5.A02;
                        AnonymousClass3EZ r32 = r4.A02;
                        if (r32.A02.A0F(userJid2) && num3 != null) {
                            int intValue = num3.intValue();
                            if (intValue == 3) {
                                r3 = r32.A04;
                                r2 = new AnonymousClass30L();
                                r2.A01 = Integer.valueOf(r3.A00);
                                i2 = 1;
                            } else if (intValue == 4) {
                                r3 = r32.A04;
                                r2 = new AnonymousClass30L();
                                r2.A01 = Integer.valueOf(r3.A00);
                                i2 = 4;
                            } else {
                                return;
                            }
                            r2.A02 = i2;
                            r2.A00 = Boolean.FALSE;
                            r3.A01.A07(r2);
                            return;
                        }
                        return;
                    }
                } else {
                    return;
                }
            } else if (!(this instanceof AnonymousClass32U)) {
                if (this instanceof AnonymousClass32S) {
                    groupChatInfo = ((AnonymousClass32S) this).A00;
                } else if (this instanceof AnonymousClass32R) {
                    groupChatInfo = ((AnonymousClass32R) this).A00;
                } else if (this instanceof C47552Bj) {
                    groupChatInfo = ((C47552Bj) this).A00;
                } else if (this instanceof AnonymousClass32T) {
                    AnonymousClass32T r12 = (AnonymousClass32T) this;
                    C14900mE r22 = r12.A01.A00;
                    if (r12.A00 != 1) {
                        r22.A03();
                        return;
                    }
                    return;
                } else if (this instanceof AnonymousClass32W) {
                    AnonymousClass32W r33 = (AnonymousClass32W) this;
                    if (r33.A03) {
                        C16170oZ r23 = r33.A01.A01;
                        Jid A0B = r33.A02.A0B(AbstractC14640lm.class);
                        AnonymousClass009.A05(A0B);
                        r23.A0J((AbstractC14640lm) A0B, true, true);
                    }
                    AnonymousClass1MF r13 = r33.A00;
                    if (r13 != null) {
                        r13.AR8(r33.A02);
                        return;
                    }
                    return;
                } else {
                    return;
                }
                groupChatInfo.A28();
                return;
            } else {
                AnonymousClass32U r24 = (AnonymousClass32U) this;
                AnonymousClass3FT r14 = r24.A00;
                r14.A01.A28();
                r4 = r14.A00;
                if (r4 != null) {
                    i = 1;
                    if (num != null) {
                        userJid = r24.A01;
                        num2 = r24.A02;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            r4.A00(userJid, num2, i);
            return;
        }
        C36181jS r42 = (C36181jS) this;
        C36191jT r15 = r42.A00;
        if (!r15.A01.isFinishing()) {
            r15.A00.postDelayed(new RunnableBRunnable0Shape6S0100000_I0_6(r42, 43), 300);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ba, code lost:
        if (r11 != 403) goto L_0x00bc;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
    @Override // X.AbstractC32491cF
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Aaw(int r11) {
        /*
        // Method dump skipped, instructions count: 278
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC32531cJ.Aaw(int):void");
    }

    @Override // java.lang.Runnable
    public void run() {
        super.A01.cancel();
        StringBuilder sb = new StringBuilder("groupmgr/request success/");
        sb.append(this.A00);
        Log.i(sb.toString());
        A01(null);
    }
}
