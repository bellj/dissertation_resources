package X;

/* renamed from: X.3D6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3D6 {
    public final String A00;
    public final String A01;
    public final String A02;
    public final String A03;
    public final String A04;

    public AnonymousClass3D6(String str, String str2, String str3, String str4) {
        AnonymousClass009.A05(str);
        this.A01 = str;
        this.A04 = str2;
        AnonymousClass009.A05(str3);
        this.A00 = str3;
        AnonymousClass009.A05(str4);
        this.A03 = str4;
        StringBuilder A0h = C12960it.A0h();
        for (int i = 0; i < str4.length(); i++) {
            A0h.appendCodePoint((str4.charAt(i) + 61926) - 65);
        }
        this.A02 = A0h.toString();
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A01);
        A0h.append(" ");
        A0h.append(this.A00);
        A0h.append(" ");
        A0h.append(this.A04);
        A0h.append(" ");
        return C12960it.A0d(this.A02, A0h);
    }
}
