package X;

import android.util.Property;
import androidx.appcompat.widget.SwitchCompat;

/* renamed from: X.0Ag  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02160Ag extends Property {
    public C02160Ag() {
        super(Float.class, "thumbPos");
    }

    @Override // android.util.Property
    public Object get(Object obj) {
        return Float.valueOf(((SwitchCompat) obj).A00);
    }

    @Override // android.util.Property
    public void set(Object obj, Object obj2) {
        ((SwitchCompat) obj).setThumbPosition(((Number) obj2).floatValue());
    }
}
