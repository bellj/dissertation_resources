package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.0CP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CP extends C05530Px {
    public final /* synthetic */ AnonymousClass0XQ A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0CP(Context context, View view, AnonymousClass07H r12, AnonymousClass0XQ r13) {
        super(context, view, r12, R.attr.actionOverflowMenuStyle, 0, true);
        this.A00 = r13;
        super.A00 = 8388613;
        AnonymousClass0XK r1 = r13.A0N;
        this.A04 = r1;
        AnonymousClass0XP r0 = this.A03;
        if (r0 != null) {
            r0.Abr(r1);
        }
    }

    @Override // X.C05530Px
    public void A02() {
        AnonymousClass0XQ r1 = this.A00;
        AnonymousClass07H r0 = r1.A0A;
        if (r0 != null) {
            r0.close();
        }
        r1.A0H = null;
        super.A02();
    }
}
