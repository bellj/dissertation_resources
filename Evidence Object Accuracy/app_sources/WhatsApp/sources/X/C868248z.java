package X;

import java.io.IOException;

/* renamed from: X.48z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C868248z extends IOException {
    public C868248z(IOException iOException) {
        super(iOException);
    }

    public C868248z(IOException iOException, String str) {
        super(str, iOException);
    }
}
