package X;

import java.io.Serializable;
import java.util.Comparator;

/* renamed from: X.5Cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112315Cx implements Comparator, Serializable {
    public final float average;

    public /* synthetic */ C112315Cx(float f) {
        this.average = f;
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        float f = ((C82593vs) obj2).A00;
        float f2 = this.average;
        return Float.compare(Math.abs(f - f2), Math.abs(((C82593vs) obj).A00 - f2));
    }
}
