package X;

import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.3FX  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3FX {
    public AnonymousClass4LQ A00;
    public boolean A01;

    public abstract void A01();

    public abstract void A02();

    public abstract boolean A03();

    public final void A00() {
        boolean A03 = A03();
        if (this.A01 != A03) {
            this.A01 = A03;
            AnonymousClass2Nu r3 = this.A00.A00;
            Log.i(C12960it.A0b("voip/audio_route/HeadsetMonitor ", r3));
            CallInfo callInfo = Voip.getCallInfo();
            if (callInfo == null || callInfo.callState == Voip.CallState.NONE) {
                Log.e("voip/audio_route/headsetPlugReceiver ignored, not in any call");
                return;
            }
            r3.A08(callInfo, null);
            if (A03) {
                r3.A05 = true;
                Log.i("voip/audio_route/headset Plugged");
                if (r3.A00 == 1) {
                    r3.A06(callInfo);
                    r3.A0A(callInfo, false);
                    return;
                }
                return;
            }
            Log.i("voip/audio_route/headset Unplugged");
            r3.A07(callInfo, null);
        }
    }
}
