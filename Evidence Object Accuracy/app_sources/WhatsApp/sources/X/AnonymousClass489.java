package X;

import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.489  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass489 extends AbstractC29111Qx {
    public final /* synthetic */ AnonymousClass4W4 A00;
    public final /* synthetic */ AnonymousClass1RK A01;
    public final /* synthetic */ AnonymousClass1RK A02;

    public AnonymousClass489(AnonymousClass4W4 r1, AnonymousClass1RK r2, AnonymousClass1RK r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC29111Qx
    public void A01() {
        try {
            AnonymousClass1RK r14 = this.A02;
            AnonymousClass4W4 r4 = this.A00;
            int i = r4.A00;
            String str = r4.A05;
            int i2 = r4.A03;
            int i3 = r4.A02;
            JniBridge.jvidispatchIIIIIOOOO((long) i, (long) i2, (long) i3, (long) r4.A01, str, r4.A06, r4.A04, r14);
        } catch (Exception unused) {
            Log.e("Error : Could not enqueue upload request in wa-msys.");
            A02(23);
        }
    }
}
