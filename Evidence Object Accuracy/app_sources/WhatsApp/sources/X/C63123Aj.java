package X;

/* renamed from: X.3Aj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63123Aj {
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005b, code lost:
        if (r7 == 43) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0003, code lost:
        if (r17 == false) goto L_0x0005;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass31R A00(byte r7, int r8, int r9, long r10, long r12, long r14, boolean r16, boolean r17, boolean r18, boolean r19, boolean r20) {
        /*
            if (r16 == 0) goto L_0x0005
            r1 = 0
            if (r17 != 0) goto L_0x0006
        L_0x0005:
            r1 = 1
        L_0x0006:
            java.lang.String r0 = "Status can't be view once."
            X.AnonymousClass009.A0B(r0, r1)
            X.31R r2 = new X.31R
            r2.<init>()
            r5 = 0
            java.lang.Long r1 = java.lang.Long.valueOf(r5)
            r2.A0C = r1
            r2.A0D = r1
            r2.A09 = r1
            r2.A07 = r1
            r2.A08 = r1
            r2.A0A = r1
            r2.A0E = r1
            r2.A0G = r1
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r2.A03 = r0
            r2.A0B = r1
            if (r16 == 0) goto L_0x0090
            r0 = 1
        L_0x0030:
            java.lang.Long r3 = java.lang.Long.valueOf(r0)
            r2.A0I = r3
            long r0 = (long) r8
            long r3 = r3.longValue()
            long r0 = r0 - r3
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.A06 = r0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r17)
            r2.A02 = r0
            r0 = 1
            if (r7 == r0) goto L_0x008e
            r0 = 3
            if (r7 == r0) goto L_0x005d
            r0 = 13
            r1 = 11
            if (r7 == r0) goto L_0x005e
            r0 = 42
            if (r7 == r0) goto L_0x008e
            r0 = 43
            r1 = 1
            if (r7 != r0) goto L_0x005e
        L_0x005d:
            r1 = 3
        L_0x005e:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
            r2.A05 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)
            r2.A04 = r0
            if (r18 == 0) goto L_0x0078
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r19)
            r2.A00 = r0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r20)
            r2.A01 = r0
        L_0x0078:
            java.lang.Long r0 = java.lang.Long.valueOf(r14)
            r2.A0H = r0
            int r0 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x008d
            int r0 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x008d
            long r12 = r12 - r10
            java.lang.Long r0 = java.lang.Long.valueOf(r12)
            r2.A0F = r0
        L_0x008d:
            return r2
        L_0x008e:
            r1 = 2
            goto L_0x005e
        L_0x0090:
            r0 = 0
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63123Aj.A00(byte, int, int, long, long, long, boolean, boolean, boolean, boolean, boolean):X.31R");
    }
}
