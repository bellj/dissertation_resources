package X;

/* renamed from: X.10R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10R extends AbstractC17250qV {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C18780t0 A02;
    public final C22280yp A03;

    public AnonymousClass10R(AbstractC15710nm r8, C14900mE r9, C18780t0 r10, C17220qS r11, C17230qT r12, C22280yp r13, AbstractC14440lR r14) {
        super(r8, r11, r12, r14, new int[]{234}, true);
        this.A01 = r9;
        this.A00 = r8;
        this.A03 = r13;
        this.A02 = r10;
    }
}
