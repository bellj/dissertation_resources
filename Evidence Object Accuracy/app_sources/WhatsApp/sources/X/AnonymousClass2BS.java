package X;

import android.util.SparseArray;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2BS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2BS implements AnonymousClass1N9 {
    public AnonymousClass1NA A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass23W A03;
    public final AnonymousClass16F A04;
    public final RandomAccessFile A05;

    public AnonymousClass2BS(AnonymousClass23W r1, AnonymousClass16F r2, RandomAccessFile randomAccessFile, int i, int i2) {
        this.A05 = randomAccessFile;
        this.A03 = r1;
        this.A02 = i;
        this.A01 = i2;
        this.A04 = r2;
    }

    public AnonymousClass1NA A00(int i) {
        RandomAccessFile randomAccessFile = this.A05;
        AnonymousClass1NA r3 = new AnonymousClass1NA(this.A03, this.A04, randomAccessFile, i, this.A01);
        try {
            r3.A02();
            return r3;
        } catch (AnonymousClass2CB e) {
            StringBuilder sb = new StringBuilder("InMemorySingleEventBufferManager/readEventBufferFromDisk: error in reading event buffer");
            sb.append(e.toString());
            Log.e(sb.toString());
            return r3;
        }
    }

    @Override // X.AnonymousClass1N9
    public boolean A6w() {
        AnonymousClass1NA A00;
        if (!(this instanceof AnonymousClass2BQ)) {
            A00 = A00((this.A03.A01 + 1) % this.A02);
        } else {
            AnonymousClass2BQ r2 = (AnonymousClass2BQ) this;
            if (!r2.A01.A01()) {
                return true;
            }
            A00 = r2.A00(1);
        }
        return A00.A05();
    }

    @Override // X.AnonymousClass1N9
    public void A7C() {
        AnonymousClass1NA r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
    }

    @Override // X.AnonymousClass1N9
    public AnonymousClass1NA A8e() {
        return this.A00;
    }

    @Override // X.AnonymousClass1N9
    public void A9F(List list) {
        int i;
        if (this instanceof AnonymousClass2BQ) {
            AnonymousClass2BQ r4 = (AnonymousClass2BQ) this;
            C47632Bt r9 = r4.A01;
            Iterator it = list.iterator();
            while (it.hasNext()) {
                int intValue = ((Number) it.next()).intValue();
                if (intValue < 1000) {
                    int i2 = intValue >> 3;
                    int i3 = intValue % 8;
                    Iterator it2 = r9.A03.entrySet().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            C47682By r1 = (C47682By) ((Map.Entry) it2.next()).getValue();
                            if (i2 == r1.A05) {
                                r1.A03(i3);
                                break;
                            }
                        }
                    }
                } else if (r9.A01.A07(1070)) {
                    int i4 = intValue - 1000;
                    r9.A05[i4].A03(0);
                    StringBuilder sb = new StringBuilder();
                    sb.append(r9.A02);
                    sb.append("wampsid");
                    sb.append(intValue);
                    File file = new File(sb.toString());
                    try {
                        r9.A06[i4].close();
                        file.delete();
                    } catch (IOException e) {
                        StringBuilder sb2 = new StringBuilder("privatestatsuploadqueue/dropSentData phase 2 files io exception failure");
                        sb2.append(e.toString());
                        Log.e(sb2.toString());
                    }
                }
            }
            int i5 = 0;
            do {
                boolean[] zArr = r9.A07;
                if (zArr[i5]) {
                    HashMap hashMap = r9.A03;
                    Iterator it3 = hashMap.entrySet().iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        Map.Entry entry = (Map.Entry) it3.next();
                        C47682By r12 = (C47682By) entry.getValue();
                        Object key = entry.getKey();
                        if (r12.A05 == i5) {
                            int i6 = 0;
                            int i7 = 0;
                            while (true) {
                                i = r12.A01;
                                if (i6 >= i) {
                                    break;
                                }
                                if (((AnonymousClass2C3) r12.A04.get(i6)).A01 <= 8) {
                                    i7++;
                                }
                                i6++;
                            }
                            if (i7 == i) {
                                try {
                                    r12.A07.close();
                                } catch (IOException e2) {
                                    StringBuilder sb3 = new StringBuilder("queuefile/flush failed to close file ");
                                    sb3.append(e2.getMessage());
                                    Log.e(sb3.toString());
                                }
                                zArr[i5] = false;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append(r9.A02);
                                sb4.append("wampsid");
                                sb4.append(i5);
                                try {
                                    new File(sb4.toString()).delete();
                                } catch (Exception e3) {
                                    StringBuilder sb5 = new StringBuilder("psuploadqueue/dropSentData failed to delete closed queue file ");
                                    sb5.append(e3.toString());
                                    Log.e(sb5.toString());
                                }
                                hashMap.remove(key);
                            }
                        }
                    }
                }
                i5++;
            } while (i5 < 8);
            r9.A08.A00.edit().putBoolean("events_ps_phase3_migration_done", true).apply();
            AnonymousClass1NA A00 = r4.A00(1);
            if (r4.A00) {
                ByteBuffer A01 = A00.A04.A01();
                if (A01.limit() > 0) {
                    r4.A01(A01, false);
                }
                r4.A00 = false;
            }
            try {
                A00.A00();
                A00.A01();
            } catch (IOException e4) {
                StringBuilder sb6 = new StringBuilder("privatestatseventbuffermanager/dropsentdata: ioexception while flushing back buffer");
                sb6.append(e4.toString());
                Log.e(sb6.toString());
            }
        } else if (AIB()) {
            Iterator it4 = list.iterator();
            while (it4.hasNext()) {
                int intValue2 = ((Number) it4.next()).intValue();
                if (intValue2 != this.A03.A01) {
                    AnonymousClass1NA A002 = A00(intValue2);
                    if (!A002.A05()) {
                        A002.A00();
                        try {
                            A002.A01();
                        } catch (IOException e5) {
                            StringBuilder sb7 = new StringBuilder("InMemorySingleEventBufferManager/dropSentData: error while event buffer flush");
                            sb7.append(e5.toString());
                            Log.e(sb7.toString());
                        }
                    }
                }
            }
        } else {
            throw new Error("InMemorySingleEventBufferManager/dropSentData: Tried to drop empty buffer");
        }
    }

    @Override // X.AnonymousClass1N9
    public boolean AA2() {
        try {
            this.A00.A01();
            return true;
        } catch (IOException unused) {
            Log.e("InMemorySingleEventBufferManager/flushEventBuffers: error while event buffer flush");
            return false;
        }
    }

    @Override // X.AnonymousClass1N9
    public SparseArray ACP() {
        Object e;
        StringBuilder sb;
        String str;
        boolean z;
        if (!(this instanceof AnonymousClass2BQ)) {
            SparseArray sparseArray = new SparseArray();
            AnonymousClass23W r3 = this.A03;
            int i = r3.A01;
            while (true) {
                i = (i + 1) % this.A02;
                if (i == r3.A01) {
                    return sparseArray;
                }
                AnonymousClass1NA A00 = A00(i);
                if (!A00.A05()) {
                    ByteBuffer byteBuffer = A00.A04.A05;
                    sparseArray.put(i, Arrays.copyOf(byteBuffer.array(), byteBuffer.position()));
                }
            }
        } else {
            C47632Bt r6 = ((AnonymousClass2BQ) this).A01;
            SparseArray sparseArray2 = new SparseArray();
            Iterator it = r6.A03.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C47682By r7 = (C47682By) ((Map.Entry) it.next()).getValue();
                if (r7.A05()) {
                    SparseArray A002 = r7.A00();
                    for (int i2 = 0; i2 < A002.size(); i2++) {
                        int keyAt = A002.keyAt(i2);
                        sparseArray2.put((r7.A05 << 3) + keyAt, A002.get(keyAt));
                    }
                }
            }
            if (r6.A01.A07(1070) && !r6.A08.A00.getBoolean("events_ps_phase3_migration_done", false)) {
                for (int i3 = 0; i3 < 3; i3++) {
                    int i4 = i3 + 1000;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(r6.A02);
                    sb2.append("wampsid");
                    sb2.append(i4);
                    File file = new File(sb2.toString());
                    if (file.exists()) {
                        try {
                            RandomAccessFile[] randomAccessFileArr = r6.A06;
                            randomAccessFileArr[i3] = new RandomAccessFile(file, "rwd");
                            C47682By[] r72 = r6.A05;
                            r72[i3] = new C47682By(r6.A0A, randomAccessFileArr[i3], i3);
                            RandomAccessFile randomAccessFile = r72[i3].A07;
                            randomAccessFile.seek(0);
                            byte[] bArr = C47682By.A08;
                            byte[] bArr2 = new byte[bArr.length];
                            randomAccessFile.read(bArr2);
                            if (Arrays.equals(bArr2, bArr)) {
                                z = true;
                            } else {
                                Log.e("privatestatsuploadqueue/initfromqueuefile invalid queue file");
                                z = false;
                            }
                            if (!z) {
                                try {
                                    randomAccessFileArr[i3].close();
                                    file.delete();
                                } catch (IOException | SecurityException e2) {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("privatestatsuploadqueue/getdatatosend failed to delete the corrupted phase2 queue file ");
                                    sb3.append(e2.toString());
                                    Log.e(sb3.toString());
                                }
                            } else {
                                r72[i3].A01();
                                if (r72[i3].A05()) {
                                    SparseArray A003 = r72[i3].A00();
                                    int keyAt2 = A003.keyAt(0);
                                    sparseArray2.put(i4 + keyAt2, A003.get(keyAt2));
                                }
                            }
                        } catch (FileNotFoundException e3) {
                            e = e3;
                            sb = new StringBuilder();
                            str = "privatestatsuploadqueue/getdatatosend phase 2 file not file not found ";
                            sb.append(str);
                            sb.append(e.toString());
                            Log.e(sb.toString());
                        } catch (IOException e4) {
                            e = e4;
                            sb = new StringBuilder();
                            str = "privatestatsuploadqueue/getdatatosend phase 2 files io exception";
                            sb.append(str);
                            sb.append(e.toString());
                            Log.e(sb.toString());
                        }
                    }
                }
            }
            return sparseArray2;
        }
    }

    @Override // X.AnonymousClass1N9
    public int AEc(int i) {
        return (i + 1) % this.A02;
    }

    @Override // X.AnonymousClass1N9
    public boolean AIB() {
        if (this instanceof AnonymousClass2BQ) {
            return ((AnonymousClass2BQ) this).A01.A01();
        }
        for (int i = 0; i < this.A02; i++) {
            if (!(i == this.A03.A01 || A00(i).A05())) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AnonymousClass1N9
    public void AIm() {
        AnonymousClass23W r1 = this.A03;
        AnonymousClass23X[] r0 = r1.A05;
        int i = r1.A01;
        AnonymousClass23X r6 = r0[i];
        AnonymousClass1NA r02 = new AnonymousClass1NA(r1, this.A04, this.A05, i, this.A01);
        this.A00 = r02;
        try {
            r02.A02();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
            long j = r6.A04;
            if (j > seconds) {
                StringBuilder sb = new StringBuilder("InMemorySingleEventBufferManager/init event from file: current event buffer timestamp is ");
                sb.append(j - seconds);
                sb.append(" seconds in the future");
                Log.w(sb.toString());
                r6.A04 = seconds;
            }
            Locale locale = Locale.US;
            Object[] objArr = new Object[5];
            AnonymousClass1NA r2 = this.A00;
            if (r2.A04()) {
                objArr[0] = Integer.valueOf(r2.A01);
                objArr[1] = Integer.valueOf(r2.A00);
                objArr[2] = Integer.valueOf(r2.A03.A00.size());
                objArr[3] = Integer.valueOf(this.A00.A04.A05.position());
                AnonymousClass1NA r22 = this.A00;
                objArr[4] = Long.valueOf(r22.A05.A05[r22.A02].A04);
                Log.i(String.format(locale, "InMemorySingleEventBufferManager/initfromfile: opened existing wam file: record_count = %d, event_count = %d, attribute_count = %d, size = %d, create_ts = %d", objArr));
                return;
            }
            throw new UnsupportedOperationException("No record count available for rotated buffers");
        } catch (AnonymousClass2CB e) {
            throw new AnonymousClass2BT(e.toString());
        }
    }

    @Override // X.AnonymousClass1N9
    public void AIx() {
        RandomAccessFile randomAccessFile = this.A05;
        AnonymousClass23W r1 = this.A03;
        this.A00 = new AnonymousClass1NA(r1, this.A04, randomAccessFile, r1.A01, this.A01);
    }

    @Override // X.AnonymousClass1N9
    public void AKX() {
        this.A00 = A00(this.A03.A01);
    }

    @Override // X.AnonymousClass1N9
    public void Aau() {
        if (this instanceof AnonymousClass2BQ) {
            AnonymousClass2BQ r3 = (AnonymousClass2BQ) this;
            AnonymousClass1NA r2 = ((AnonymousClass2BS) r3).A00;
            ByteBuffer A01 = r2.A04.A01();
            if (A01.limit() != 0) {
                r3.A01(A01, true);
                try {
                    r2.A00();
                    r2.A01();
                } catch (IOException e) {
                    e.toString();
                }
            }
        }
    }
}
