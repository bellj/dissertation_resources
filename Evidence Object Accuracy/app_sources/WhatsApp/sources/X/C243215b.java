package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/* renamed from: X.15b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C243215b implements AnonymousClass15U {
    public final int A00;
    public final long A01;
    public final AnonymousClass15S A02;
    public final Random A03 = new Random();
    public final Set A04;

    public C243215b(AnonymousClass15S r6, AnonymousClass15R r7) {
        this.A02 = r6;
        this.A01 = r7.A00();
        HashSet hashSet = new HashSet();
        for (int i : AnonymousClass15U.A00) {
            hashSet.add(Integer.valueOf(i));
        }
        this.A04 = Collections.unmodifiableSet(hashSet);
        this.A00 = this.A03.nextInt(901) + 100;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002a, code lost:
        if ((java.lang.Math.abs((r10.A01 ^ ((long) r5.A00.A02(225))) ^ ((long) r11)) % r3) != 0) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A00(int r11) {
        /*
            r10 = this;
            X.15S r5 = r10.A02
            X.1OB r0 = r5.A01(r11)
            long r3 = r0.A01
            r8 = 0
            r7 = 0
            int r0 = (r3 > r8 ? 1 : (r3 == r8 ? 0 : -1))
            if (r0 == 0) goto L_0x002c
            r1 = 1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x002d
            X.0m9 r1 = r5.A00
            r0 = 225(0xe1, float:3.15E-43)
            int r0 = r1.A02(r0)
            long r5 = r10.A01
            long r0 = (long) r0
            long r5 = r5 ^ r0
            long r0 = (long) r11
            long r5 = r5 ^ r0
            long r1 = java.lang.Math.abs(r5)
            long r1 = r1 % r3
            int r0 = (r1 > r8 ? 1 : (r1 == r8 ? 0 : -1))
            if (r0 == 0) goto L_0x002d
        L_0x002c:
            return r7
        L_0x002d:
            java.util.Set r1 = r10.A04
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            boolean r1 = r1.contains(r0)
            r0 = 1
            if (r1 == 0) goto L_0x0046
            java.util.Random r1 = r10.A03
            int r0 = r10.A00
            int r0 = r1.nextInt(r0)
            if (r0 != 0) goto L_0x002c
            r7 = 1
            return r7
        L_0x0046:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C243215b.A00(int):boolean");
    }
}
