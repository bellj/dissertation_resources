package X;

import java.io.Closeable;

/* renamed from: X.0io  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12940io extends Closeable {
    void A6P(int i, byte[] bArr);

    void A6R(int i, double d);

    void A6S(int i, long j);

    void A6T(int i);

    void A6U(int i, String str);
}
