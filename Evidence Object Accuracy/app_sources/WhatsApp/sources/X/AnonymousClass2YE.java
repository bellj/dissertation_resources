package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2YE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YE extends AnimatorListenerAdapter {
    public final /* synthetic */ C53232dO A00;

    public AnonymousClass2YE(C53232dO r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        C53232dO r3 = this.A00;
        r3.setAlpha(0.0f);
        for (int i = 0; i < r3.getChildCount(); i++) {
            r3.A0A[i] = r3.getChildAt(i).getMeasuredWidth();
        }
        C53232dO.A01(r3, 8);
    }
}
