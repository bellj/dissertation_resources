package X;

import android.os.Bundle;
import android.os.Message;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2NA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2NA extends AbstractC35941j2 {
    public final /* synthetic */ UserJid A00;
    public final /* synthetic */ AnonymousClass2L8 A01;

    public AnonymousClass2NA(UserJid userJid, AnonymousClass2L8 r2) {
        this.A01 = r2;
        this.A00 = userJid;
    }

    @Override // X.AbstractC35941j2
    public void A00(int i) {
        C450720b r2 = this.A01.A0H;
        UserJid userJid = this.A00;
        StringBuilder sb = new StringBuilder("xmpp/reader/on-get-biz-vname-cert-error jid=");
        sb.append(userJid);
        sb.append(" code=");
        sb.append(i);
        Log.i(sb.toString());
        AbstractC450820c r3 = r2.A00;
        Message obtain = Message.obtain(null, 0, 110, 0);
        Bundle data = obtain.getData();
        data.putParcelable("jid", userJid);
        data.putInt("errorCode", i);
        r3.AYY(obtain);
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r18) {
        Jid jid;
        Object obj;
        AnonymousClass1V8 A0E = r18.A0E("verified_name");
        AnonymousClass009.A05(A0E);
        AnonymousClass2L8 r14 = this.A01;
        Jid A0A = A0E.A0A(r14.A04, UserJid.class, "jid");
        if (A0A != null || (jid = this.A00) == null) {
            jid = A0A;
        }
        String A0I = A0E.A0I("v", null);
        String A0I2 = A0E.A0I("verified_level", null);
        long A08 = A0E.A08("serial", 0);
        C32141bg r6 = new C32141bg(A0E.A0I("host_storage", null), A0E.A0I("actual_actors", null), A0E.A0I("privacy_mode_ts", null));
        if (!"1".equals(A0I) || A0I2 == null) {
            StringBuilder sb = new StringBuilder("unknown vname cert payload version or vlevel: v=");
            sb.append(A0I);
            sb.append(" vlevel=");
            sb.append(A0I2);
            Log.w(sb.toString());
            return;
        }
        byte[] bArr = A0E.A01;
        int A00 = AnonymousClass2SI.A00(A0I2);
        C450720b r1 = r14.A0H;
        StringBuilder sb2 = new StringBuilder("xmpp/reader/on-get-biz-vname-cert jid=");
        sb2.append(jid);
        sb2.append(" certBlob=[");
        if (bArr != null) {
            obj = Integer.valueOf(bArr.length);
        } else {
            obj = "null";
        }
        sb2.append(obj);
        sb2.append("] vlevel=");
        sb2.append(A00);
        sb2.append(". privactMode=");
        sb2.append(r6);
        Log.i(sb2.toString());
        AbstractC450820c r4 = r1.A00;
        Message obtain = Message.obtain(null, 0, 109, 0);
        Bundle data = obtain.getData();
        data.putParcelable("jid", jid);
        data.putByteArray("certBlob", bArr);
        data.putLong("serial", A08);
        data.putInt("vlevel", A00);
        data.putInt("host_storage", r6.hostStorage);
        data.putInt("actual_actors", r6.actualActors);
        data.putLong("privacy_mode_ts", r6.privacyModeTs);
        r4.AYY(obtain);
    }
}
