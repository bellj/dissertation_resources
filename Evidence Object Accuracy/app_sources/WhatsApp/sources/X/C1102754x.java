package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.calling.callhistory.group.GroupCallLogActivity;

/* renamed from: X.54x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1102754x implements AnonymousClass28F {
    public final /* synthetic */ GroupCallLogActivity A00;

    public C1102754x(GroupCallLogActivity groupCallLogActivity) {
        this.A00 = groupCallLogActivity;
    }

    @Override // X.AnonymousClass28F
    public void Adh(Bitmap bitmap, ImageView imageView, boolean z) {
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            Adv(imageView);
        }
    }

    @Override // X.AnonymousClass28F
    public void Adv(ImageView imageView) {
        imageView.setImageResource(R.drawable.avatar_contact);
    }
}
