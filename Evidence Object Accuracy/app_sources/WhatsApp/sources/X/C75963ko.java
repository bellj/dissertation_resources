package X;

/* renamed from: X.3ko  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75963ko extends AbstractRunnableC47782Cq {
    public final /* synthetic */ C64183Eo A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C75963ko(C64183Eo r2) {
        super("StreamingUploadDataTask_ask_for_data");
        this.A00 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        C64183Eo r0 = this.A00;
        r0.A03.canHandleStreamingUploadUpdate(r0.A02.mTaskIdentifier);
    }
}
