package X;

import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.whatsapp.R;

/* renamed from: X.2Uv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51462Uv extends AnonymousClass04v {
    public final /* synthetic */ C36821kh A00;

    public C51462Uv(C36821kh r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass04v
    public void A01(View view, AccessibilityEvent accessibilityEvent) {
        if (accessibilityEvent.getEventType() == 32768) {
            C36821kh r1 = this.A00;
            if (r1.getContext() != null && view.getTag() != null) {
                AnonymousClass23N.A00(r1.getContext(), r1.A04, r1.getContext().getString(R.string.ax_label_call_banner_timer, C38131nZ.A06(r1.A05, ((Number) view.getTag()).longValue())));
            }
        }
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r4) {
        super.A06(view, r4);
        r4.A02.setContentDescription("");
    }
}
