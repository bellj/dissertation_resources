package X;

import android.transition.Transition;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.3xw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83813xw extends AbstractC100714mM {
    public final /* synthetic */ ViewProfilePhoto A00;

    public C83813xw(ViewProfilePhoto viewProfilePhoto) {
        this.A00 = viewProfilePhoto;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
        super.onTransitionStart(transition);
        ViewProfilePhoto viewProfilePhoto = this.A00;
        View findViewById = viewProfilePhoto.findViewById(R.id.picture);
        View findViewById2 = viewProfilePhoto.findViewById(R.id.picture_animation);
        findViewById.setVisibility(4);
        findViewById2.setVisibility(0);
    }
}
