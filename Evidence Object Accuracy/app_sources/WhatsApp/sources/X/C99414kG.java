package X;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;

/* renamed from: X.4kG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99414kG implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        int i = 0;
        ParcelFileDescriptor parcelFileDescriptor = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                i = C95664e9.A02(parcel, readInt);
            } else if (c != 3) {
                C95664e9.A0D(parcel, readInt);
            } else {
                parcelFileDescriptor = (ParcelFileDescriptor) C95664e9.A07(parcel, ParcelFileDescriptor.CREATOR, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78223oZ(parcelFileDescriptor, i);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78223oZ[i];
    }
}
