package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.util.Log;

/* renamed from: X.2Fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48332Fp implements AbstractC21730xt {
    public C48352Fr A00;
    public final AbstractC15710nm A01;
    public final C14900mE A02;
    public final C15570nT A03;
    public final C18640sm A04;
    public final C17220qS A05;

    public C48332Fp(AbstractC15710nm r1, C14900mE r2, C15570nT r3, C18640sm r4, C17220qS r5) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A05 = r5;
        this.A04 = r4;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("DeleteBusinessActivityReportProtocolHelper/delivery-error");
        if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(this, 15));
        }
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r5, String str) {
        Log.e("DeleteBusinessActivityReport/onError");
        if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(this, 14));
        }
        AbstractC15710nm r3 = this.A01;
        StringBuilder sb = new StringBuilder("error_code=");
        sb.append(C41151sz.A00(r5));
        r3.AaV("DeleteBusinessActivityReport/delete business activity error", sb.toString(), true);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r4, String str) {
        if (this.A00 != null) {
            this.A02.A0I(new RunnableBRunnable0Shape11S0100000_I0_11(this, 16));
        }
    }
}
