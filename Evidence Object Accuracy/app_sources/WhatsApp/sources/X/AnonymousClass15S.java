package X;

import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.15S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15S {
    public static final AnonymousClass1OB A06 = new AnonymousClass1OB(0, 0, false);
    public final C14850m9 A00;
    public final AbstractC20460vn A01;
    public volatile Boolean A02;
    public volatile Boolean A03;
    public volatile Long A04;
    public volatile ConcurrentHashMap A05;

    public AnonymousClass15S(C14850m9 r1, AbstractC20460vn r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static final void A00(ConcurrentHashMap concurrentHashMap, JSONArray jSONArray) {
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONArray jSONArray2 = jSONArray.getJSONArray(i);
                int i2 = jSONArray2.getInt(0);
                int i3 = jSONArray2.getInt(1);
                long max = Math.max(jSONArray2.getLong(2), 0L);
                long optLong = jSONArray2.optLong(3);
                boolean z = false;
                if (3 == i3) {
                    z = true;
                }
                concurrentHashMap.put(Integer.valueOf(i2), new AnonymousClass1OB(max, optLong, z));
            }
        }
    }

    public final AnonymousClass1OB A01(int i) {
        if (this.A05 == null) {
            synchronized (this) {
                if (this.A05 == null) {
                    ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
                    try {
                        C14850m9 r5 = this.A00;
                        JSONArray jSONArray = r5.A04(226).getJSONArray("sampling");
                        if (jSONArray.length() == 0) {
                            JSONObject A04 = r5.A04(1716);
                            JSONObject A042 = r5.A04(1717);
                            A00(concurrentHashMap, A04.getJSONArray("sampling"));
                            A00(concurrentHashMap, A042.getJSONArray("sampling"));
                        } else {
                            A00(concurrentHashMap, jSONArray);
                        }
                    } catch (Exception e) {
                        this.A01.A9c(e.getMessage());
                        concurrentHashMap.clear();
                    }
                    this.A05 = concurrentHashMap;
                }
            }
        }
        ConcurrentHashMap concurrentHashMap2 = this.A05;
        Integer valueOf = Integer.valueOf(i);
        AnonymousClass1OB r1 = (AnonymousClass1OB) concurrentHashMap2.get(valueOf);
        if (r1 == null) {
            r1 = (AnonymousClass1OB) this.A05.get(Integer.valueOf(i >> 16));
            if (r1 == null) {
                r1 = A06;
            }
            this.A05.put(valueOf, r1);
        }
        return r1;
    }

    public final void A02() {
        if (this.A03 == null || this.A04 == null) {
            synchronized (this) {
                if (this.A03 == null || this.A04 == null) {
                    C14850m9 r1 = this.A00;
                    this.A03 = Boolean.valueOf(r1.A07(397));
                    this.A04 = Long.valueOf((long) r1.A02(398));
                }
            }
        }
    }

    public boolean A03() {
        if (this.A02 == null) {
            synchronized (this) {
                if (this.A02 == null) {
                    this.A02 = Boolean.valueOf(this.A00.A07(212));
                }
            }
        }
        return this.A02.booleanValue();
    }
}
