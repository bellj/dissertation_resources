package X;

import com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel;
import com.whatsapp.util.Log;

/* renamed from: X.3YW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YW implements AnonymousClass5XP {
    public final /* synthetic */ GoogleMigrateImporterViewModel A00;

    public AnonymousClass3YW(GoogleMigrateImporterViewModel googleMigrateImporterViewModel) {
        this.A00 = googleMigrateImporterViewModel;
    }

    @Override // X.AnonymousClass5XP
    public void ANg() {
        Log.i("GoogleMigrateImporterViewModel/onCancellationCompleted()");
        this.A00.A07(C12970iu.A0g());
    }

    @Override // X.AnonymousClass5XP
    public void ANh() {
        Log.i("GoogleMigrateImporterViewModel/onCancellationStarted()");
        this.A00.A06(7);
    }

    @Override // X.AnonymousClass5XP
    public void AOQ(boolean z) {
        StringBuilder A0k = C12960it.A0k("GoogleMigrateImporterViewModel/onComplete()/success = ");
        A0k.append(z);
        C12960it.A1F(A0k);
        if (z) {
            GoogleMigrateImporterViewModel googleMigrateImporterViewModel = this.A00;
            googleMigrateImporterViewModel.A06(5);
            C12970iu.A1Q(googleMigrateImporterViewModel.A03, 100);
        }
    }

    @Override // X.AnonymousClass5XP
    public void APl(int i) {
        Log.i(C12960it.A0W(i, "GoogleMigrateImporterViewModel/onError()/errorCode = "));
        this.A00.A06(AnonymousClass4EQ.A00(i));
    }

    @Override // X.AnonymousClass5XP
    public void ARJ() {
        GoogleMigrateImporterViewModel googleMigrateImporterViewModel = this.A00;
        googleMigrateImporterViewModel.A06(3);
        C12970iu.A1Q(googleMigrateImporterViewModel.A03, -1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if (r3 == 101) goto L_0x0012;
     */
    @Override // X.AnonymousClass5XP
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AU8(int r3) {
        /*
            r2 = this;
            java.lang.String r0 = "GoogleMigrateImporterViewModel/onPrepareBeforeRetryCompleted()"
            com.whatsapp.util.Log.i(r0)
            r0 = 301(0x12d, float:4.22E-43)
            if (r3 == r0) goto L_0x0012
            r0 = 104(0x68, float:1.46E-43)
            if (r3 == r0) goto L_0x0012
            r0 = 101(0x65, float:1.42E-43)
            r1 = 0
            if (r3 != r0) goto L_0x0013
        L_0x0012:
            r1 = 1
        L_0x0013:
            com.whatsapp.migration.android.view.GoogleMigrateImporterViewModel r0 = r2.A00
            if (r1 == 0) goto L_0x001b
            r0.A04()
            return
        L_0x001b:
            r0.A05()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3YW.AU8(int):void");
    }

    @Override // X.AnonymousClass5XP
    public void AU9() {
        Log.i("GoogleMigrateImporterViewModel/onPrepareBeforeRetryStarted()");
        this.A00.A06(17);
    }

    @Override // X.AnonymousClass5XP
    public void AUL(int i) {
        Log.i(C12960it.A0W(i, "GoogleMigrateImporterViewModel/onProgress(); progress="));
        GoogleMigrateImporterViewModel googleMigrateImporterViewModel = this.A00;
        googleMigrateImporterViewModel.A06(3);
        C12970iu.A1Q(googleMigrateImporterViewModel.A03, i);
    }
}
