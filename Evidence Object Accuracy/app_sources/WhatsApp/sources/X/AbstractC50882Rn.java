package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2Rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC50882Rn extends TextEmojiLabel {
    public boolean A00;

    public AbstractC50882Rn(Context context) {
        super(context);
        A08();
    }

    public AbstractC50882Rn(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A08();
    }

    public AbstractC50882Rn(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A08();
    }
}
