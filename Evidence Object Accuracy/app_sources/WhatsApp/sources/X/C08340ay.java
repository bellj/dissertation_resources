package X;

/* renamed from: X.0ay  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08340ay implements AbstractC12050hI {
    public static final C08340ay A00 = new C08340ay();
    public static final C05850Rf A01 = C05850Rf.A00("t", "f", "s", "j", "tr", "lh", "ls", "fc", "sc", "sw", "of");

    @Override // X.AbstractC12050hI
    public Object AYt(AbstractC08850bx r15, float f) {
        EnumC03730Ix r2 = EnumC03730Ix.CENTER;
        r15.A0A();
        String str = null;
        EnumC03730Ix r3 = r2;
        String str2 = null;
        float f2 = 0.0f;
        int i = 0;
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i2 = 0;
        int i3 = 0;
        float f5 = 0.0f;
        boolean z = true;
        while (r15.A0H()) {
            switch (r15.A04(A01)) {
                case 0:
                    str = r15.A08();
                    break;
                case 1:
                    str2 = r15.A08();
                    break;
                case 2:
                    f2 = (float) r15.A02();
                    break;
                case 3:
                    int A03 = r15.A03();
                    r3 = r2;
                    if (A03 <= 2 && A03 >= 0) {
                        r3 = EnumC03730Ix.values()[A03];
                        break;
                    }
                    break;
                case 4:
                    i = r15.A03();
                    break;
                case 5:
                    f3 = (float) r15.A02();
                    break;
                case 6:
                    f4 = (float) r15.A02();
                    break;
                case 7:
                    i2 = AnonymousClass0UD.A01(r15);
                    break;
                case 8:
                    i3 = AnonymousClass0UD.A01(r15);
                    break;
                case 9:
                    f5 = (float) r15.A02();
                    break;
                case 10:
                    z = r15.A0I();
                    break;
                default:
                    r15.A0D();
                    r15.A0E();
                    break;
            }
        }
        r15.A0C();
        return new C05240Ou(r3, str, str2, f2, f3, f4, f5, i, i2, i3, z);
    }
}
