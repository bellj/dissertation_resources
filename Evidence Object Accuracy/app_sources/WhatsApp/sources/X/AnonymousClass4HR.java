package X;

import java.util.HashMap;

/* renamed from: X.4HR  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4HR {
    public static final HashMap A00;
    public static final HashMap A01;
    public static final HashMap A02;

    static {
        HashMap A11 = C12970iu.A11();
        A02 = A11;
        HashMap A112 = C12970iu.A11();
        A01 = A112;
        HashMap A113 = C12970iu.A11();
        A00 = A113;
        A11.put("IN", A112);
        A11.put("BR", A113);
        A112.put("stable_release", 2);
        C12960it.A1K("stable_release", A113, 1);
        A113.put("feature_bip", 2);
    }
}
