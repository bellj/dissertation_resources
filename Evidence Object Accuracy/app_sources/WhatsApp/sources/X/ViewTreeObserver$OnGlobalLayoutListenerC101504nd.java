package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4nd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101504nd implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ AnonymousClass2Ew A00;

    public ViewTreeObserver$OnGlobalLayoutListenerC101504nd(AnonymousClass2Ew r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        AnonymousClass2Ew r1 = this.A00;
        r1.A07();
        C12980iv.A1F(r1.A0F, this);
        AnonymousClass2Ew.A00(r1);
    }
}
