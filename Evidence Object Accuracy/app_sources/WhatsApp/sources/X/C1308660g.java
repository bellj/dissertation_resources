package X;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.os.Build;

/* renamed from: X.60g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1308660g {
    public static int A00(CameraManager cameraManager, CaptureRequest.Builder builder, C119085cr r7, AbstractC130695zp r8, String str, int i) {
        CaptureRequest.Key key;
        if (r7 == null || r8 == null) {
            throw C12960it.A0U("Trying to update builder after camera closed.");
        }
        int i2 = 0;
        if (i == 0) {
            int A05 = C12960it.A05(r7.A03(AbstractC130685zo.A0C));
            if (A05 == 4) {
                if (C117295Zj.A1S(AbstractC130695zp.A08, r8)) {
                    i2 = 4;
                }
            } else if (A05 == 3) {
                if (C117295Zj.A1S(AbstractC130695zp.A09, r8)) {
                    i2 = 3;
                }
            } else if (A05 == 1) {
                if (C117295Zj.A1S(AbstractC130695zp.A07, r8)) {
                    i2 = 1;
                }
            } else if (A05 == 0 && C117295Zj.A1S(AbstractC130695zp.A0I, r8)) {
                float A02 = C72453ed.A02(r7.A03(AbstractC130685zo.A0a));
                if (A02 >= C1309160m.A00(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE, cameraManager, str)) {
                    builder.set(CaptureRequest.LENS_FOCUS_DISTANCE, Float.valueOf(A02));
                }
            }
            key = CaptureRequest.CONTROL_AF_MODE;
        } else if (i == 1) {
            int A052 = C12960it.A05(r7.A03(AbstractC130685zo.A0t));
            if (A052 == -1) {
                return A052;
            }
            if (A052 == 1 && C117295Zj.A1S(AbstractC130695zp.A0A, r8)) {
                i2 = 1;
            }
            key = CaptureRequest.CONTROL_AWB_MODE;
        } else {
            StringBuilder A0k = C12960it.A0k("Illegal key: ");
            A0k.append(i);
            throw C12960it.A0U(C12960it.A0d(" for the updateBuilderAndReturn method", A0k));
        }
        builder.set(key, Integer.valueOf(i2));
        return i2;
    }

    public static void A01(CaptureRequest.Builder builder) {
        if (Build.VERSION.SDK_INT >= 21) {
            builder.set(CaptureRequest.CONTROL_CAPTURE_INTENT, 3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:111:0x026e, code lost:
        if (X.C12960it.A05(r8.A03(r1)) != 3) goto L_0x0270;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0067, code lost:
        if (r2 != 0) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01f0, code lost:
        if (X.C12960it.A05(r8.A03(r1)) != 3) goto L_0x01f2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0248  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(android.hardware.camera2.CaptureRequest.Builder r7, X.C119085cr r8, X.AbstractC130695zp r9, int r10) {
        /*
        // Method dump skipped, instructions count: 820
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1308660g.A02(android.hardware.camera2.CaptureRequest$Builder, X.5cr, X.5zp, int):void");
    }
}
