package X;

import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.mediaview.PhotoView;
import com.whatsapp.videoplayback.ExoPlaybackControlView;

/* renamed from: X.33Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33Y extends AnonymousClass3NJ {
    public final /* synthetic */ MediaViewFragment A00;
    public final /* synthetic */ PhotoView A01;
    public final /* synthetic */ ExoPlaybackControlView A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass33Y(MediaViewFragment mediaViewFragment, PhotoView photoView, PhotoView photoView2, AbstractC16130oV r4, ExoPlaybackControlView exoPlaybackControlView) {
        super(photoView, r4);
        this.A00 = mediaViewFragment;
        this.A01 = photoView2;
        this.A02 = exoPlaybackControlView;
    }
}
