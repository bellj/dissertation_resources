package X;

/* renamed from: X.4zM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108604zM implements AbstractC116375Ve {
    public AbstractC116375Ve[] A00;

    public C108604zM(AbstractC116375Ve... r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116375Ve
    public final boolean Ags(Class cls) {
        for (AbstractC116375Ve r0 : this.A00) {
            if (r0.Ags(cls)) {
                return true;
            }
        }
        return false;
    }

    @Override // X.AbstractC116375Ve
    public final AbstractC115145Qj Ah5(Class cls) {
        AbstractC116375Ve[] r4 = this.A00;
        for (AbstractC116375Ve r1 : r4) {
            if (r1.Ags(cls)) {
                return r1.Ah5(cls);
            }
        }
        throw C12980iv.A0u(C12960it.A0c(cls.getName(), "No factory is available for message type: "));
    }
}
