package X;

import java.io.File;

/* renamed from: X.0wN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20820wN {
    public C16490p7 A00;
    public final C22320yt A01;

    public C20820wN(C22320yt r1, C16490p7 r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public Long A00() {
        C16490p7 r2 = this.A00;
        r2.A04();
        File file = r2.A07;
        if (!file.exists()) {
            return null;
        }
        r2.A04();
        return Long.valueOf(file.length());
    }
}
