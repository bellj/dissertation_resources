package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.text.ParseException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.4Qs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C91174Qs {
    public final IvParameterSpec A00;
    public final SecretKeySpec A01;
    public final SecretKeySpec A02;

    public C91174Qs(byte[] bArr) {
        try {
            byte[][] A02 = C31241aE.A02(bArr, 32, 32, 16);
            this.A01 = new SecretKeySpec(A02[0], "AES");
            this.A02 = new SecretKeySpec(A02[1], DefaultCrypto.HMAC_SHA256);
            this.A00 = new IvParameterSpec(A02[2]);
        } catch (ParseException e) {
            throw new AssertionError(e);
        }
    }
}
