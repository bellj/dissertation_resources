package X;

import android.content.Context;

/* renamed from: X.2k5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55982k5 extends AnonymousClass2k6 implements AnonymousClass5SE {
    public AnonymousClass3H1 A00;
    public final AnonymousClass3JO A01 = new AnonymousClass3JO(this);

    public C55982k5(Context context) {
        super(context, null);
    }

    @Override // android.view.View
    public void offsetLeftAndRight(int i) {
        if (i != 0) {
            super.offsetLeftAndRight(i);
        }
    }

    @Override // android.view.View
    public void offsetTopAndBottom(int i) {
        if (i != 0) {
            super.offsetTopAndBottom(i);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.A01.A07();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.A01.A08();
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        AnonymousClass3H1 r0 = this.A00;
        if (r0 == null) {
            setMeasuredDimension(0, 0);
        } else {
            setMeasuredDimension(r0.A03.A04.width(), this.A00.A03.A04.height());
        }
    }

    @Override // X.AnonymousClass5SE
    public void setRenderTree(AnonymousClass3H1 r2) {
        if (this.A00 != r2) {
            if (r2 == null) {
                this.A01.A09();
            }
            this.A00 = r2;
            requestLayout();
        }
    }

    @Override // android.view.View
    public void setTranslationX(float f) {
        if (f != getTranslationX()) {
            super.setTranslationX(f);
        }
    }

    @Override // android.view.View
    public void setTranslationY(float f) {
        if (f != getTranslationY()) {
            super.setTranslationY(f);
        }
    }
}
