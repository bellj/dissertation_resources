package X;

import com.facebook.profilo.core.TriggerRegistry;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: X.1SU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SU {
    public static final int A02 = TriggerRegistry.A00.A02("WhatsApp");
    public final ArrayList A00;
    public final boolean A01;

    public AnonymousClass1SU() {
        boolean z = false;
        ArrayList arrayList = new ArrayList(Arrays.asList("stack_trace", "qpl", "system_counters", "high_freq_main_thread_counters"));
        this.A00 = arrayList;
        if (!"x86_64".equals(AnonymousClass1HP.A02())) {
            arrayList.add("atrace");
        }
        z = Math.random() < 0.5d ? true : z;
        this.A01 = z;
        if (z) {
            arrayList.add("wall_time_stack_trace");
        }
    }
}
