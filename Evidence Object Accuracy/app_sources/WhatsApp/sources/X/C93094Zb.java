package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/* renamed from: X.4Zb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93094Zb {
    public static final AnonymousClass4PO A00;
    public static final ByteBuffer A01;
    public static final Charset A02 = Charset.forName("ISO-8859-1");
    public static final Charset A03 = Charset.forName(DefaultCrypto.UTF_8);
    public static final byte[] A04;

    static {
        byte[] bArr = new byte[0];
        A04 = bArr;
        A01 = ByteBuffer.wrap(bArr);
        AnonymousClass4PO r2 = new AnonymousClass4PO(bArr);
        try {
            int i = r2.A00 + r2.A01;
            r2.A00 = i;
            int i2 = i - 0;
            if (i2 > 0) {
                int i3 = i2 - 0;
                r2.A01 = i3;
                r2.A00 = i - i3;
            } else {
                r2.A01 = 0;
            }
            A00 = r2;
        } catch (C868048x e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Object A00(Object obj, Object obj2) {
        AbstractC79123q5 r3 = (AbstractC79123q5) ((AbstractC117125Yn) obj);
        C79133q6 r2 = (C79133q6) r3.A05(5);
        r2.A00();
        AbstractC79123q5 r1 = r2.A00;
        C72453ed.A0d(r1).AhG(r1, r3);
        AbstractC117125Yn r4 = (AbstractC117125Yn) obj2;
        if (r2.AhB().getClass().isInstance(r4)) {
            r2.A00();
            AbstractC79123q5 r12 = r2.A00;
            C72453ed.A0d(r12).AhG(r12, (AbstractC79123q5) ((AbstractC108624zO) r4));
            return r2.AhC();
        }
        throw C12970iu.A0f("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
