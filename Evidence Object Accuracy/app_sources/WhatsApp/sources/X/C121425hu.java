package X;

import android.content.Intent;
import android.os.Parcelable;

/* renamed from: X.5hu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121425hu extends AbstractC128555wI {
    public C30861Zc A00;
    public C1316163l A01;
    public final int A02;
    public final AnonymousClass60Y A03;

    public C121425hu(Intent intent, AnonymousClass60Y r4) {
        int intExtra = intent.getIntExtra("withdrawal_type", -1);
        this.A02 = intExtra;
        if (intExtra != -1) {
            if (intExtra == 1) {
                Parcelable parcelableExtra = intent.getParcelableExtra("withdraw_store_info");
                AnonymousClass009.A06(parcelableExtra, "Store info must be passed with intent extras for cash withdrawal type");
                this.A01 = (C1316163l) parcelableExtra;
            } else {
                Parcelable parcelableExtra2 = intent.getParcelableExtra("bank_for_withdrawal");
                AnonymousClass009.A06(parcelableExtra2, "Bank must be passed with intent extras for bank withdrawal type");
                this.A00 = (C30861Zc) parcelableExtra2;
            }
            this.A03 = r4;
            return;
        }
        throw C12960it.A0U("Withdrawal type must be set using intent creation");
    }
}
