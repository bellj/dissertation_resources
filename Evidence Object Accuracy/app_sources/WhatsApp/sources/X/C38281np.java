package X;

import java.util.Collections;
import java.util.Set;

/* renamed from: X.1np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38281np implements AbstractC34921gv {
    @Override // X.AbstractC34921gv
    public String AEZ() {
        return "m";
    }

    @Override // X.AbstractC34921gv
    public Set AEH(AbstractC15340mz r2) {
        if (r2.A0v) {
            return Collections.singleton("s");
        }
        return null;
    }

    @Override // X.AbstractC34921gv
    public C34941gx AEa(C15250mo r4) {
        Boolean bool = r4.A08;
        if (bool == null) {
            return null;
        }
        boolean booleanValue = bool.booleanValue();
        boolean z = true;
        if (!booleanValue) {
            z = false;
        }
        boolean booleanValue2 = Boolean.valueOf(z).booleanValue();
        C34941gx r1 = new C34941gx();
        Set singleton = Collections.singleton("s");
        if (booleanValue2) {
            r1.A00 = singleton;
            return r1;
        }
        r1.A01 = singleton;
        return r1;
    }
}
