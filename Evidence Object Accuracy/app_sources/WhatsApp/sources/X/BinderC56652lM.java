package X;

import android.os.IInterface;
import android.os.Parcel;

/* renamed from: X.2lM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class BinderC56652lM extends AbstractBinderC73293fz implements IInterface {
    public final /* synthetic */ AbstractC115745St A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinderC56652lM(AbstractC115745St r2) {
        super("com.google.android.gms.maps.internal.IOnMarkerClickListener");
        this.A00 = r2;
    }

    @Override // X.AbstractBinderC73293fz
    public final boolean A00(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        boolean ASR = this.A00.ASR(new C36311jg(AbstractBinderC79903rO.A00(parcel.readStrongBinder())));
        parcel2.writeNoException();
        parcel2.writeInt(ASR ? 1 : 0);
        return true;
    }
}
