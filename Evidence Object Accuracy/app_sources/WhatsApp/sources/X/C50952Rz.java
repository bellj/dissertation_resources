package X;

import java.util.Arrays;
import org.json.JSONObject;

/* renamed from: X.2Rz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50952Rz {
    public final int A00;
    public final long A01;

    public C50952Rz(int i, long j) {
        this.A00 = i;
        this.A01 = j;
    }

    public C50952Rz(String str) {
        JSONObject jSONObject = new JSONObject(str);
        this.A00 = jSONObject.getInt("update_count");
        this.A01 = jSONObject.getLong("id");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C50952Rz)) {
            return false;
        }
        C50952Rz r7 = (C50952Rz) obj;
        if (r7.A01 == this.A01 && r7.A00 == this.A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.A01), Integer.valueOf(this.A00)});
    }
}
