package X;

/* renamed from: X.3y5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C83903y5 extends AbstractC87954Dr {
    public final boolean A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof C83903y5) && this.A00 == ((C83903y5) obj).A00);
    }

    public int hashCode() {
        boolean z = this.A00;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    public C83903y5() {
        this(false);
    }

    public C83903y5(boolean z) {
        this.A00 = z;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("NewUser(isDeleted=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
