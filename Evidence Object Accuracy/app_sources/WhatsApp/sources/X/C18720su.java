package X;

import android.os.Handler;
import android.os.HandlerThread;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0su  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18720su extends AbstractC18440sS implements AbstractC18730sv {
    public static final int A0O = ((int) ((AnonymousClass01V.A00 / 1024) / 16));
    public Handler A00;
    public C006202y A01;
    public AnonymousClass1O4 A02;
    public AnonymousClass1O4 A03;
    public AnonymousClass1O4 A04;
    public AnonymousClass1O6 A05;
    public AnonymousClass1O6 A06;
    public Map A07 = new HashMap();
    public final AbstractC15710nm A08;
    public final AnonymousClass169 A09;
    public final C16590pI A0A;
    public final AbstractC14440lR A0B;
    public final Object A0C = new Object();
    public final Object A0D = new Object();
    public final Object A0E = new Object();
    public final Object A0F = new Object();
    public final Object A0G = new Object();
    public final Object A0H = new Object();
    public final Object A0I = new Object();
    public final Object A0J = new Object();
    public final Object A0K = new Object();
    public final List A0L = new ArrayList();
    public volatile AnonymousClass1O4 A0M;
    public volatile AnonymousClass1O4 A0N;

    public C18720su(AbstractC15710nm r2, AnonymousClass169 r3, C16590pI r4, AnonymousClass14A r5, AbstractC14440lR r6) {
        super(r5);
        this.A0A = r4;
        this.A08 = r2;
        this.A0B = r6;
        this.A09 = r3;
    }

    public static /* synthetic */ Handler A00(C18720su r3) {
        Handler handler;
        synchronized (r3) {
            handler = r3.A00;
            if (handler == null) {
                HandlerThread handlerThread = new HandlerThread("cache-cleaner", 10);
                handlerThread.start();
                handler = new AnonymousClass1O3(handlerThread.getLooper(), r3);
                r3.A00 = handler;
            }
        }
        return handler;
    }

    public AnonymousClass1O4 A01() {
        AnonymousClass1O4 r1;
        synchronized (this.A0D) {
            r1 = this.A03;
            if (r1 == null) {
                r1 = new AnonymousClass1O5(this, (int) (AnonymousClass01V.A00 / 8192));
                this.A03 = r1;
            }
        }
        return r1;
    }

    public AnonymousClass1O4 A02() {
        AnonymousClass1O4 r1;
        synchronized (this.A0H) {
            r1 = this.A04;
            if (r1 == null) {
                r1 = new AnonymousClass1O7(this, (int) ((AnonymousClass01V.A00 / 1024) / 6));
                this.A04 = r1;
            }
        }
        return r1;
    }

    public AnonymousClass1O4 A03() {
        if (this.A0M == null) {
            synchronized (this.A0I) {
                if (this.A0M == null) {
                    this.A0M = new AnonymousClass1O8(this, A0O);
                }
            }
        }
        return this.A0M;
    }

    public AnonymousClass1O4 A04() {
        if (this.A0N == null) {
            synchronized (this.A0J) {
                if (this.A0N == null) {
                    this.A0N = new AnonymousClass1O4(25);
                }
            }
        }
        return this.A0N;
    }

    public AnonymousClass1O6 A05() {
        AnonymousClass1O6 r2;
        synchronized (this.A0F) {
            r2 = this.A05;
            if (r2 == null) {
                r2 = new AnonymousClass1O6(this.A08, this.A0A, this.A0B, "gif_content_obj_store", 32);
                this.A05 = r2;
            }
        }
        return r2;
    }

    public AnonymousClass1O6 A06() {
        AnonymousClass1O6 r2;
        synchronized (this.A0G) {
            r2 = this.A06;
            if (r2 == null) {
                r2 = new AnonymousClass1O6(this.A08, this.A0A, this.A0B, "gif_preview_obj_store", 256);
                this.A06 = r2;
            }
        }
        return r2;
    }

    public List A07(int i) {
        List list;
        synchronized (this.A0E) {
            Map map = this.A07;
            Integer valueOf = Integer.valueOf(i);
            list = map.containsKey(valueOf) ? (List) ((WeakReference) map.get(valueOf)).get() : null;
        }
        return list;
    }

    public void A08() {
        synchronized (this.A0E) {
            this.A07.clear();
        }
    }

    @Override // X.AbstractC18730sv
    public void AKm(String str) {
        Runtime runtime = Runtime.getRuntime();
        synchronized (this.A0H) {
            AnonymousClass1O4 r0 = this.A04;
            if (r0 != null) {
                r0.A00.A01();
                this.A04.A00.A00();
            }
        }
        synchronized (this.A0D) {
            AnonymousClass1O4 r02 = this.A03;
            if (r02 != null) {
                r02.A00.A01();
                this.A03.A00.A00();
            }
        }
        synchronized (this.A0G) {
            AnonymousClass1O6 r03 = this.A06;
            if (r03 != null) {
                r03.A05.A01();
                this.A06.A05.A00();
            }
        }
        synchronized (this.A0F) {
            AnonymousClass1O6 r04 = this.A05;
            if (r04 != null) {
                r04.A05.A01();
                this.A05.A05.A00();
            }
        }
        synchronized (this.A0K) {
            C006202y r05 = this.A01;
            if (r05 != null) {
                r05.A01();
                this.A01.A00();
            }
        }
        synchronized (this.A0I) {
            if (this.A0M != null) {
                this.A0M.A00.A01();
                this.A0M.A00.A00();
            }
        }
        synchronized (this.A0J) {
            if (this.A0N != null) {
                this.A0N.A00.A01();
                this.A0N.A00.A00();
            }
        }
        synchronized (this.A0C) {
            AnonymousClass1O4 r06 = this.A02;
            if (r06 != null) {
                r06.A00.A01();
                this.A02.A00.A00();
            }
        }
        AnonymousClass169 r1 = this.A09;
        synchronized (r1) {
            r1.A01.size();
        }
        runtime.freeMemory();
        runtime.totalMemory();
        runtime.maxMemory();
    }
}
