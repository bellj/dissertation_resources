package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;

/* renamed from: X.06h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C013406h {
    public TypedValue A00;
    public final Context A01;
    public final TypedArray A02;

    public C013406h(Context context, TypedArray typedArray) {
        this.A01 = context;
        this.A02 = typedArray;
    }

    public static C013406h A00(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new C013406h(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    public ColorStateList A01(int i) {
        int resourceId;
        ColorStateList A00;
        TypedArray typedArray = this.A02;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0 || (A00 = C015007d.A00(this.A01, resourceId)) == null) {
            return typedArray.getColorStateList(i);
        }
        return A00;
    }

    public Drawable A02(int i) {
        int resourceId;
        TypedArray typedArray = this.A02;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0) {
            return typedArray.getDrawable(i);
        }
        return C012005t.A01().A04(this.A01, resourceId);
    }

    public Drawable A03(int i) {
        int resourceId;
        Drawable A05;
        TypedArray typedArray = this.A02;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0) {
            return null;
        }
        C011905s A01 = C011905s.A01();
        Context context = this.A01;
        synchronized (A01) {
            A05 = A01.A00.A05(context, resourceId, true);
        }
        return A05;
    }

    public void A04() {
        this.A02.recycle();
    }
}
