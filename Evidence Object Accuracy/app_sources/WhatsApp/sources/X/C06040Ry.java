package X;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: X.0Ry  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06040Ry {
    public static final Executor A06 = new ExecutorC10580er();
    public int A00;
    public List A01;
    public List A02 = Collections.emptyList();
    public final C04770Mz A03;
    public final AbstractC12640iF A04;
    public final Executor A05;

    public C06040Ry(C04770Mz r2, AbstractC12640iF r3) {
        this.A04 = r3;
        this.A03 = r2;
        this.A05 = A06;
    }
}
