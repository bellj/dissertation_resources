package X;

import android.util.Log;
import java.util.Set;

/* renamed from: X.0R5  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0R5 {
    public static void A00(String str) {
        Set set = C04450Lt.A00;
        if (!set.contains(str)) {
            Log.w("LOTTIE", str, null);
            set.add(str);
        }
    }

    public static void A01(String str, Throwable th) {
        Set set = C04450Lt.A00;
        if (!set.contains(str)) {
            Log.w("LOTTIE", str, th);
            set.add(str);
        }
    }
}
