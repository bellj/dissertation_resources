package X;

import android.view.animation.Animation;
import com.whatsapp.identity.IdentityVerificationActivity;

/* renamed from: X.3wx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83263wx extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ IdentityVerificationActivity A00;

    public C83263wx(IdentityVerificationActivity identityVerificationActivity) {
        this.A00 = identityVerificationActivity;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        this.A00.A03.setVisibility(8);
    }
}
