package X;

/* renamed from: X.3Fk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64403Fk {
    public final AnonymousClass3HZ A00;
    public final int[] A01;

    public C64403Fk(AnonymousClass3HZ r6, int[] iArr) {
        int length = iArr.length;
        if (length != 0) {
            this.A00 = r6;
            if (length <= 1 || iArr[0] != 0) {
                this.A01 = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.A01 = new int[]{0};
                return;
            }
            int i2 = length - i;
            int[] iArr2 = new int[i2];
            this.A01 = iArr2;
            System.arraycopy(iArr, i, iArr2, 0, i2);
            return;
        }
        throw new IllegalArgumentException();
    }

    public int A00(int i) {
        int i2;
        if (i == 0) {
            int[] iArr = this.A01;
            return iArr[(iArr.length - 1) - 0];
        }
        int[] iArr2 = this.A01;
        if (i == 1) {
            i2 = 0;
            for (int i3 : iArr2) {
                i2 ^= i3;
            }
        } else {
            i2 = iArr2[0];
            int length = iArr2.length;
            for (int i4 = 1; i4 < length; i4++) {
                i2 = this.A00.A01(i, i2) ^ iArr2[i4];
            }
        }
        return i2;
    }

    public C64403Fk A01(int i) {
        if (i == 0) {
            return this.A00.A04;
        }
        if (i == 1) {
            return this;
        }
        int[] iArr = this.A01;
        int length = iArr.length;
        int[] iArr2 = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr2[i2] = this.A00.A01(iArr[i2], i);
        }
        return new C64403Fk(this.A00, iArr2);
    }

    public C64403Fk A02(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.A00.A04;
        } else {
            int[] iArr = this.A01;
            int length = iArr.length;
            int[] iArr2 = new int[i + length];
            for (int i3 = 0; i3 < length; i3++) {
                iArr2[i3] = this.A00.A01(iArr[i3], i2);
            }
            return new C64403Fk(this.A00, iArr2);
        }
    }

    public C64403Fk A03(C64403Fk r10) {
        AnonymousClass3HZ r8 = this.A00;
        if (r8.equals(r10.A00)) {
            int[] iArr = this.A01;
            if (iArr[0] == 0) {
                return r10;
            }
            int[] iArr2 = r10.A01;
            if (iArr2[0] == 0) {
                return this;
            }
            int[] iArr3 = iArr;
            if (iArr.length <= iArr2.length) {
                iArr3 = iArr2;
                iArr2 = iArr;
            }
            int length = iArr3.length;
            int[] iArr4 = new int[length];
            int length2 = length - iArr2.length;
            System.arraycopy(iArr3, 0, iArr4, 0, length2);
            for (int i = length2; i < length; i++) {
                iArr4[i] = iArr2[i - length2] ^ iArr3[i];
            }
            return new C64403Fk(r8, iArr4);
        }
        throw C12970iu.A0f("GenericGFPolys do not have same GenericGF field");
    }

    public C64403Fk A04(C64403Fk r13) {
        AnonymousClass3HZ r10 = this.A00;
        if (r10.equals(r13.A00)) {
            int[] iArr = this.A01;
            if (iArr[0] != 0) {
                int[] iArr2 = r13.A01;
                if (iArr2[0] != 0) {
                    int length = iArr.length;
                    int length2 = iArr2.length;
                    int[] iArr3 = new int[(length + length2) - 1];
                    for (int i = 0; i < length; i++) {
                        int i2 = iArr[i];
                        for (int i3 = 0; i3 < length2; i3++) {
                            int i4 = i + i3;
                            iArr3[i4] = iArr3[i4] ^ r10.A01(i2, iArr2[i3]);
                        }
                    }
                    return new C64403Fk(r10, iArr3);
                }
            }
            return r10.A04;
        }
        throw C12970iu.A0f("GenericGFPolys do not have same GenericGF field");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0066 A[EDGE_INSN: B:34:0x0066->B:30:0x0066 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r7 = this;
            int[] r5 = r7.A01
            int r0 = r5.length
            int r6 = r0 + -1
            int r0 = r6 << 3
            java.lang.StringBuilder r4 = X.C12980iv.A0t(r0)
            r3 = r6
        L_0x000c:
            if (r3 < 0) goto L_0x0066
            int r0 = r6 - r3
            r1 = r5[r0]
            if (r1 == 0) goto L_0x0039
            if (r1 >= 0) goto L_0x0054
            java.lang.String r0 = " - "
            r4.append(r0)
            int r1 = -r1
        L_0x001c:
            r2 = 1
            if (r3 == 0) goto L_0x0021
            if (r1 == r2) goto L_0x0032
        L_0x0021:
            X.3HZ r0 = r7.A00
            if (r1 == 0) goto L_0x0060
            int[] r0 = r0.A06
            r1 = r0[r1]
            if (r1 != 0) goto L_0x0046
            r0 = 49
        L_0x002d:
            r4.append(r0)
        L_0x0030:
            if (r3 == 0) goto L_0x0066
        L_0x0032:
            if (r3 != r2) goto L_0x003c
            r0 = 120(0x78, float:1.68E-43)
            r4.append(r0)
        L_0x0039:
            int r3 = r3 + -1
            goto L_0x000c
        L_0x003c:
            java.lang.String r0 = "x^"
            r4.append(r0)
            r4.append(r3)
            goto L_0x0039
        L_0x0046:
            if (r1 != r2) goto L_0x004b
            r0 = 97
            goto L_0x002d
        L_0x004b:
            java.lang.String r0 = "a^"
            r4.append(r0)
            r4.append(r1)
            goto L_0x0030
        L_0x0054:
            int r0 = r4.length()
            if (r0 <= 0) goto L_0x001c
            java.lang.String r0 = " + "
            r4.append(r0)
            goto L_0x001c
        L_0x0060:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x0066:
            java.lang.String r0 = r4.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64403Fk.toString():java.lang.String");
    }
}
