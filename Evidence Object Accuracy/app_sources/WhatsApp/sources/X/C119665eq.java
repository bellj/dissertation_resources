package X;

import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import java.util.Collection;

/* renamed from: X.5eq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119665eq extends C27131Gd {
    public final /* synthetic */ PaymentGroupParticipantPickerActivity A00;

    public C119665eq(PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity) {
        this.A00 = paymentGroupParticipantPickerActivity;
    }

    @Override // X.C27131Gd
    public void A04(Collection collection) {
        this.A00.A0F.notifyDataSetChanged();
    }
}
