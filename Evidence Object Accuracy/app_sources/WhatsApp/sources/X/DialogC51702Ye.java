package X;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.R;
import com.whatsapp.WaButton;
import com.whatsapp.WaImageView;
import com.whatsapp.mediacomposer.doodle.ColorPickerComponent;
import com.whatsapp.mediacomposer.doodle.penmode.PenModeView;

/* renamed from: X.2Ye  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC51702Ye extends Dialog {
    public int A00;
    public View A01;
    public ViewGroup A02;
    public WaButton A03;
    public WaImageView A04;
    public WaImageView A05;
    public ColorPickerComponent A06;
    public AnonymousClass2ZW A07;
    public PenModeView A08;
    public final int A09;
    public final int A0A;
    public final int A0B = getContext().getResources().getDimensionPixelSize(R.dimen.doodle_pen_size_thin);
    public final View.OnLayoutChangeListener A0C;
    public final C48792Hu A0D;
    public final AnonymousClass3FS A0E;
    public final boolean A0F;
    public final int[] A0G;

    public DialogC51702Ye(Activity activity, C48792Hu r10, C89514Kg r11, C47322Ae r12, C63583Cd r13, int[] iArr, boolean z) {
        super(activity, R.style.DoodlePenDialog);
        int dimensionPixelSize = getContext().getResources().getDimensionPixelSize(R.dimen.doodle_pen_size_medium);
        this.A09 = dimensionPixelSize;
        this.A0A = getContext().getResources().getDimensionPixelSize(R.dimen.doodle_pen_size_thick);
        this.A00 = -1;
        this.A0C = new View.OnLayoutChangeListener() { // from class: X.3Mf
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                DialogC51702Ye r3 = DialogC51702Ye.this;
                Window window = r3.getWindow();
                if (window != null) {
                    int[] iArr2 = new int[2];
                    r3.A02.getLocationOnScreen(iArr2);
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) r3.A02.getLayoutParams();
                    int rotation = window.getWindowManager().getDefaultDisplay().getRotation();
                    if (r3.A00 != rotation) {
                        r3.A00 = rotation;
                        layoutParams.rightMargin = 0;
                        layoutParams.leftMargin = 0;
                        layoutParams.topMargin = 0;
                        layoutParams.bottomMargin = 0;
                        if (rotation == 0) {
                            layoutParams.topMargin = r3.A0G[1] - iArr2[1];
                        } else if (rotation == 1) {
                            layoutParams.leftMargin = r3.A0G[0] - iArr2[0];
                        } else if (rotation == 2) {
                            layoutParams.bottomMargin = r3.A0G[1] - iArr2[1];
                        } else if (rotation == 3) {
                            layoutParams.rightMargin = r3.A0G[0] - iArr2[0];
                        }
                        r3.A02.setLayoutParams(layoutParams);
                    }
                }
            }
        };
        this.A0D = r10;
        this.A0E = new AnonymousClass3FS(r11, r12, new C63573Cc(r12, this), r13, r10.A00, AnonymousClass00T.A00(getContext(), R.color.color_picker_default_blur_color), dimensionPixelSize);
        this.A0G = iArr;
        this.A0F = z;
    }

    @Override // android.app.Dialog
    public void onBackPressed() {
        if (!this.A0E.A03) {
            super.onBackPressed();
        }
    }

    @Override // android.app.Dialog
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            setContentView(R.layout.doodle_pen_wave2);
            this.A01 = findViewById(R.id.doodle_pen_root);
            this.A03 = (WaButton) findViewById(R.id.wave2_pen_dialog_done);
            this.A04 = (WaImageView) findViewById(R.id.wave2_pen_dialog_pen);
            this.A05 = (WaImageView) findViewById(R.id.wave2_pen_dialog_undo);
            this.A02 = (ViewGroup) findViewById(R.id.canvas);
            this.A06 = (ColorPickerComponent) findViewById(R.id.wave2_pen_dialog_color_picker_component);
            this.A08 = (PenModeView) findViewById(R.id.pen_mode_view);
            window.setLayout(-1, -1);
            window.setFlags(EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            if (Build.VERSION.SDK_INT >= 28) {
                window.getAttributes().layoutInDisplayCutoutMode = 1;
            }
            this.A01.addOnLayoutChangeListener(this.A0C);
            C12960it.A0x(this.A03, this, 44);
            this.A02.setOnTouchListener(new View.OnTouchListener() { // from class: X.3N5
                @Override // android.view.View.OnTouchListener
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    AnonymousClass3FS r4 = DialogC51702Ye.this.A0E;
                    r4.A08.A00.invalidate();
                    r4.A09.A00(motionEvent, (float) r4.A02, r4.A01, r4.A04);
                    return true;
                }
            });
            AnonymousClass2ZW r1 = new AnonymousClass2ZW(getContext(), R.drawable.ic_cam_draw);
            this.A07 = r1;
            this.A04.setImageDrawable(r1);
            this.A06.A00();
            this.A06.A04(null, new AnonymousClass3YF(this), null);
            C12960it.A0x(this.A05, this, 43);
            this.A05.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.3Mp
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    AnonymousClass3FS r3 = DialogC51702Ye.this.A0E;
                    if (r3.A03) {
                        return true;
                    }
                    r3.A08.A00.invalidate();
                    C63583Cd r2 = r3.A0B;
                    C454721t r12 = r2.A01;
                    if (!r12.A03.A00.isEmpty()) {
                        r12.A02();
                        C454921v r0 = r2.A00;
                        r0.A01();
                        r0.A02();
                    }
                    r3.A02(true);
                    return true;
                }
            });
            this.A08.A00 = new C1110157t(this);
            AnonymousClass3FS r2 = this.A0E;
            int i = r2.A05;
            r2.A01 = i;
            r2.A0A.A01.A07.A01(i);
            r2.A01(2, r2.A06);
            r2.A02(false);
            if (!this.A0F) {
                PenModeView penModeView = this.A08;
                AnonymousClass028.A0D(penModeView, R.id.pen_mode_blur).setVisibility(8);
                AnonymousClass028.A0D(penModeView, R.id.pen_mode_blur_space).setVisibility(8);
            }
        }
    }
}
