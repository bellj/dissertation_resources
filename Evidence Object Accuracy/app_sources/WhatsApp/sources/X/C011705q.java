package X;

import android.content.res.Resources;
import java.lang.Thread;

/* renamed from: X.05q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C011705q implements Thread.UncaughtExceptionHandler {
    public final /* synthetic */ Thread.UncaughtExceptionHandler A00;

    public C011705q(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.A00 = uncaughtExceptionHandler;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public void uncaughtException(Thread thread, Throwable th) {
        String message;
        if (!(th instanceof Resources.NotFoundException) || (message = th.getMessage()) == null || (!message.contains("drawable") && !message.contains("Drawable"))) {
            this.A00.uncaughtException(thread, th);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(th.getMessage());
        sb.append(". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
        Resources.NotFoundException notFoundException = new Resources.NotFoundException(sb.toString());
        notFoundException.initCause(th.getCause());
        notFoundException.setStackTrace(th.getStackTrace());
        this.A00.uncaughtException(thread, notFoundException);
    }
}
