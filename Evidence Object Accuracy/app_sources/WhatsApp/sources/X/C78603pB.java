package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3pB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C78603pB extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98864jN();
    @Deprecated
    public final int A00;
    public final long A01;
    public final String A02;

    public C78603pB(int i, String str, long j) {
        this.A02 = str;
        this.A00 = i;
        this.A01 = j;
    }

    public C78603pB(String str, long j) {
        this.A02 = str;
        this.A01 = j;
        this.A00 = -1;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj instanceof C78603pB) {
            C78603pB r9 = (C78603pB) obj;
            String str = this.A02;
            String str2 = r9.A02;
            if (str == null ? str2 == null : str.equals(str2)) {
                long j = this.A01;
                if (j == -1) {
                    j = (long) this.A00;
                }
                long j2 = r9.A01;
                if (j2 == -1) {
                    j2 = (long) r9.A00;
                }
                if (j == j2) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A02;
        long j = this.A01;
        if (j == -1) {
            j = (long) this.A00;
        }
        return C12960it.A06(Long.valueOf(j), A1a);
    }

    @Override // java.lang.Object
    public final String toString() {
        C13290jS r5 = new C13290jS(this);
        r5.A00(this.A02, "name");
        long j = this.A01;
        if (j == -1) {
            j = (long) this.A00;
        }
        r5.A00(Long.valueOf(j), "version");
        return r5.toString();
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0D(parcel, this.A02, 1, false);
        int i2 = this.A00;
        C95654e8.A07(parcel, 2, i2);
        long j = this.A01;
        if (j == -1) {
            j = (long) i2;
        }
        C95654e8.A08(parcel, 3, j);
        C95654e8.A06(parcel, A00);
    }
}
