package X;

/* renamed from: X.5sb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126265sb {
    public final AnonymousClass1V8 A00;

    public C126265sb(AnonymousClass3CS r6, String str) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "br-get-payout-banks");
        if (C117305Zk.A1Y(str, false)) {
            C41141sy.A01(A0N, "provider", str);
        }
        this.A00 = C117295Zj.A0I(A0N, A0M, r6);
    }
}
