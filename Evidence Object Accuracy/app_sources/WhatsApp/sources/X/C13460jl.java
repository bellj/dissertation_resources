package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;

/* renamed from: X.0jl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13460jl {
    public static final C13470jm A00(Context context, C13470jm r12, boolean z) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Writing ID to properties file");
        }
        Properties properties = new Properties();
        properties.setProperty("id", r12.A01);
        properties.setProperty("cre", String.valueOf(r12.A00));
        File A04 = A04(context);
        try {
            A04.createNewFile();
            RandomAccessFile randomAccessFile = new RandomAccessFile(A04, "rw");
            FileChannel channel = randomAccessFile.getChannel();
            channel.lock();
            if (z && channel.size() > 0) {
                try {
                    channel.position(0L);
                    r12 = A03(channel);
                } catch (C13480jn | IOException e) {
                    if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        String valueOf = String.valueOf(e);
                        StringBuilder sb = new StringBuilder(valueOf.length() + 58);
                        sb.append("Tried reading ID before writing new one, but failed with: ");
                        sb.append(valueOf);
                        Log.d("FirebaseInstanceId", sb.toString());
                    }
                }
                channel.close();
                randomAccessFile.close();
                return r12;
            }
            channel.truncate(0L);
            properties.store(Channels.newOutputStream(channel), (String) null);
            channel.close();
            randomAccessFile.close();
            return r12;
        } catch (IOException e2) {
            String valueOf2 = String.valueOf(e2);
            StringBuilder sb2 = new StringBuilder(valueOf2.length() + 21);
            sb2.append("Failed to write key: ");
            sb2.append(valueOf2);
            Log.w("FirebaseInstanceId", sb2.toString());
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C13470jm A01(android.content.SharedPreferences r5) {
        /*
            java.lang.String r0 = "cre"
            java.lang.String r0 = X.C13450jk.A00(r0)
            r4 = 0
            java.lang.String r0 = r5.getString(r0, r4)
            if (r0 == 0) goto L_0x0012
            long r1 = java.lang.Long.parseLong(r0)     // Catch: NumberFormatException -> 0x0012
            goto L_0x0014
        L_0x0012:
            r1 = 0
        L_0x0014:
            java.lang.String r0 = "id"
            java.lang.String r0 = X.C13450jk.A00(r0)
            java.lang.String r3 = r5.getString(r0, r4)
            if (r3 != 0) goto L_0x0036
            java.lang.String r0 = "|P|"
            java.lang.String r0 = X.C13450jk.A00(r0)
            java.lang.String r0 = r5.getString(r0, r4)
            if (r0 != 0) goto L_0x002e
            return r4
        L_0x002e:
            java.security.PublicKey r0 = A05(r0)
            java.lang.String r3 = X.C13430ji.A01(r0)
        L_0x0036:
            X.0jm r0 = new X.0jm
            r0.<init>(r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13460jl.A01(android.content.SharedPreferences):X.0jm");
    }

    public static final C13470jm A02(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            FileChannel channel = fileInputStream.getChannel();
            channel.lock(0, Long.MAX_VALUE, true);
            C13470jm A03 = A03(channel);
            channel.close();
            fileInputStream.close();
            return A03;
        } catch (Throwable th) {
            try {
                throw th;
            } catch (Throwable th2) {
                try {
                    fileInputStream.close();
                    throw th2;
                } catch (Throwable th3) {
                    C13490jo.A00.A00(th, th3);
                    throw th2;
                }
            }
        }
    }

    public static C13470jm A03(FileChannel fileChannel) {
        Properties properties = new Properties();
        properties.load(Channels.newInputStream(fileChannel));
        try {
            long parseLong = Long.parseLong(properties.getProperty("cre"));
            String property = properties.getProperty("id");
            if (property == null) {
                String property2 = properties.getProperty("pub");
                if (property2 != null) {
                    property = C13430ji.A01(A05(property2));
                } else {
                    throw new C13480jn();
                }
            }
            return new C13470jm(property, parseLong);
        } catch (NumberFormatException e) {
            throw new C13480jn(e);
        }
    }

    public static File A04(Context context) {
        String obj;
        if (TextUtils.isEmpty("")) {
            obj = "com.google.InstanceId.properties";
        } else {
            try {
                String encodeToString = Base64.encodeToString("".getBytes(DefaultCrypto.UTF_8), 11);
                StringBuilder sb = new StringBuilder(String.valueOf(encodeToString).length() + 33);
                sb.append("com.google.InstanceId_");
                sb.append(encodeToString);
                sb.append(".properties");
                obj = sb.toString();
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        File A06 = AnonymousClass00T.A06(context);
        if (A06 == null || !A06.isDirectory()) {
            Log.w("FirebaseInstanceId", "noBackupFilesDir doesn't exist, using regular files directory instead");
            A06 = context.getFilesDir();
        }
        return new File(A06, obj);
    }

    public static PublicKey A05(String str) {
        try {
            try {
                return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 8)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(valueOf.length() + 19);
                sb.append("Invalid key stored ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                throw new C13480jn(e);
            }
        } catch (IllegalArgumentException e2) {
            throw new C13480jn(e2);
        }
    }

    public static final void A06(Context context, C13470jm r4) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        try {
            if (r4.equals(A01(sharedPreferences))) {
                return;
            }
        } catch (C13480jn unused) {
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Writing key to shared preferences");
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(C13450jk.A00("id"), r4.A01);
        edit.putString(C13450jk.A00("cre"), String.valueOf(r4.A00));
        edit.commit();
    }

    public final C13470jm A07(Context context) {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA");
            instance.initialize(EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
            C13470jm r4 = new C13470jm(C13430ji.A01(instance.generateKeyPair().getPublic()), System.currentTimeMillis());
            C13470jm A00 = A00(context, r4, true);
            if (A00 == null || A00.equals(r4)) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "Generated new key");
                }
                A06(context, r4);
                return r4;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Loaded key after generating new one, using loaded one");
            }
            return A00;
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
