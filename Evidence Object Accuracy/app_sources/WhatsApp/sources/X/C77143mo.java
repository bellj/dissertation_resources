package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3mo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77143mo extends AbstractC107394xG {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(26);
    public final long A00;
    public final long A01;
    public final byte[] A02;

    public /* synthetic */ C77143mo(Parcel parcel) {
        this.A01 = parcel.readLong();
        this.A00 = parcel.readLong();
        this.A02 = parcel.createByteArray();
    }

    public C77143mo(byte[] bArr, long j, long j2) {
        this.A01 = j2;
        this.A00 = j;
        this.A02 = bArr;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeByteArray(this.A02);
    }
}
