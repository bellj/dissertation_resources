package X;

import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.jid.UserJid;
import java.lang.ref.WeakReference;

@Deprecated
/* renamed from: X.37S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37S extends AbstractC16350or {
    public final C18470sV A00;
    public final UserJid A01;
    public final C15860o1 A02;
    public final WeakReference A03;

    public AnonymousClass37S(ContactInfoActivity contactInfoActivity, C18470sV r3, UserJid userJid, C15860o1 r5) {
        this.A00 = r3;
        this.A02 = r5;
        this.A01 = userJid;
        this.A03 = C12970iu.A10(contactInfoActivity);
    }
}
