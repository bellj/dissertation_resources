package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;
import com.whatsapp.contact.picker.BidiContactListView;

/* renamed from: X.2LJ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass2LJ extends ListView implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AnonymousClass2LJ(Context context) {
        super(context);
        A01();
    }

    public AnonymousClass2LJ(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public AnonymousClass2LJ(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public void A01() {
        if (this instanceof BidiContactListView) {
            BidiContactListView bidiContactListView = (BidiContactListView) this;
            if (!bidiContactListView.A02) {
                bidiContactListView.A02 = true;
                AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) bidiContactListView.generatedComponent())).A06;
                bidiContactListView.A01 = (C252718t) r1.A9K.get();
                bidiContactListView.A00 = (AnonymousClass018) r1.ANb.get();
            }
        } else if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
