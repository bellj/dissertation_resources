package X;

import android.content.Context;

/* renamed from: X.5xJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129185xJ {
    public final Context A00;
    public final C15550nR A01;
    public final C15610nY A02;
    public final C14830m7 A03;
    public final AnonymousClass018 A04;
    public final AnonymousClass14X A05;

    public C129185xJ(Context context, C15550nR r2, C15610nY r3, C14830m7 r4, AnonymousClass018 r5, AnonymousClass14X r6) {
        this.A03 = r4;
        this.A00 = context;
        this.A05 = r6;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
    }

    public AbstractC130195yx A00(int i) {
        if (!(i == 1 || i == 2 || i == 3 || i == 4)) {
            if (i == 6) {
                return new C123735nm(this.A00, this.A04, this.A05);
            } else if (i == 7) {
                return new C123745nn(this.A00, this.A04, this.A05);
            } else if (i == 8) {
                C14830m7 r4 = this.A03;
                return new C123765np(this.A00, r4, this.A04, this.A05);
            } else if (!(i == 10 || i == 20 || i == 30 || i == 40 || i == 100 || i == 200 || i == 300)) {
                return null;
            }
        }
        C14830m7 r42 = this.A03;
        Context context = this.A00;
        AnonymousClass14X r6 = this.A05;
        return new C123755no(context, this.A01, this.A02, r42, this.A04, r6);
    }
}
