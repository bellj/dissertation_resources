package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.1IR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IR implements Parcelable {
    public static final int[] A0S = {702, 701};
    public static final int[] A0T = {101, 102, 103, 104};
    public static final int[] A0U = {11, 12, 608};
    public static final int[] A0V = {401, 402, 403, 404, 410, 411, 413, 601, 602, 603};
    public static final Parcelable.Creator CREATOR = new AnonymousClass3Ll();
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public long A06;
    public AbstractC30791Yv A07;
    @Deprecated
    public C30821Yy A08;
    public AnonymousClass20C A09;
    public AbstractC30891Zf A0A;
    public C30921Zi A0B;
    public AbstractC14640lm A0C;
    public UserJid A0D;
    public UserJid A0E;
    public String A0F;
    public String A0G;
    public String A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public ArrayList A0N;
    public boolean A0O = true;
    public boolean A0P;
    public boolean A0Q;
    public byte[] A0R;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004b, code lost:
        if (r23 == 40) goto L_0x004d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1IR(X.AbstractC30791Yv r12, X.C30821Yy r13, com.whatsapp.jid.UserJid r14, com.whatsapp.jid.UserJid r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, int r23, int r24, int r25, int r26, int r27, long r28, long r30) {
        /*
            r11 = this;
            r3 = r17
            r4 = r11
            r11.<init>()
            r0 = 1
            r11.A0O = r0
            r2 = r23
            if (r23 != 0) goto L_0x000e
            r0 = 0
        L_0x000e:
            X.AnonymousClass009.A0F(r0)
            r11.A03 = r2
            r11.A0E = r14
            r11.A0D = r15
            r0 = r16
            r11.A0I = r0
            r11.A08 = r13
            r0 = r28
            r11.A05 = r0
            r0 = r22
            r11.A0G = r0
            r0 = r25
            r11.A04 = r0
            r0 = r26
            r11.A01 = r0
            r11.A07 = r12
            r0 = r27
            r11.A00 = r0
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 == 0) goto L_0x004d
            r3 = r21
            boolean r0 = android.text.TextUtils.isEmpty(r21)
            if (r0 != 0) goto L_0x0050
            r0 = 10
            if (r2 == r0) goto L_0x004d
            r0 = 20
            if (r2 == r0) goto L_0x004d
            r0 = 40
            if (r2 != r0) goto L_0x0050
        L_0x004d:
            r11.A06(r3)
        L_0x0050:
            r7 = r20
            r5 = r18
            r6 = r19
            r9 = r30
            r8 = r24
            r4.A07(r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.<init>(X.1Yv, X.1Yy, com.whatsapp.jid.UserJid, com.whatsapp.jid.UserJid, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, int, int, int, long, long):void");
    }

    public AnonymousClass1IR(String str, int i, int i2, long j) {
        this.A03 = i;
        this.A05 = j;
        this.A0G = str;
        this.A04 = i2;
        this.A01 = 0;
        this.A0I = "XXX";
        this.A07 = C30771Yt.A06;
    }

    public synchronized AbstractC30791Yv A00() {
        return this.A07;
    }

    public synchronized C30921Zi A01() {
        return this.A0B;
    }

    public synchronized Boolean A02() {
        Boolean bool;
        int i = this.A03;
        if (i == 1 || i == 100 || i == 20) {
            bool = Boolean.TRUE;
        } else if (i == 2 || i == 200 || i == 10) {
            bool = Boolean.FALSE;
        } else {
            bool = null;
        }
        return bool;
    }

    public synchronized void A03(AbstractC30891Zf r4, long j) {
        if (j > 0) {
            AbstractC30891Zf r0 = this.A0A;
            if (r0 == null) {
                this.A0A = r4;
                r0 = r4;
            }
            r0.A0N(j);
        }
    }

    public synchronized void A04(AnonymousClass1IR r10) {
        if (!TextUtils.isEmpty(r10.A0K)) {
            A06(r10.A0K);
        }
        this.A05 = r10.A05;
        this.A0E = r10.A0E;
        this.A0D = r10.A0D;
        this.A08 = r10.A08;
        this.A0I = r10.A0I;
        this.A03 = r10.A03;
        this.A0G = r10.A0G;
        this.A07 = r10.A07;
        this.A04 = r10.A04;
        this.A0R = r10.A0R;
        this.A0L = r10.A0L;
        this.A0Q = r10.A0Q;
        this.A0C = r10.A0C;
        this.A0M = r10.A0M;
        this.A0A = r10.A0A;
        this.A0B = r10.A0B;
        A07(this.A0H, this.A0J, this.A0F, this.A02, this.A06);
    }

    public synchronized void A05(C30921Zi r2) {
        this.A0B = r2;
    }

    public synchronized void A06(String str) {
        AnonymousClass009.A04(str);
        this.A0K = str;
        this.A0O = true;
    }

    public synchronized void A07(String str, String str2, String str3, int i, long j) {
        this.A02 = i;
        if (j >= 0) {
            this.A06 = j;
            this.A0O = true;
            this.A0H = str;
            this.A0J = str2;
            this.A0F = str3;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("PAY: PaymentTransaction update called with invalid timestamp: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    public synchronized void A08(ArrayList arrayList) {
        if (arrayList != null) {
            if (arrayList.size() > 0) {
                this.A0N = arrayList;
            }
        }
    }

    public synchronized void A09(boolean z) {
        this.A0O = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0035, code lost:
        if (r1 != 200) goto L_0x0037;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0A() {
        /*
            r7 = this;
            monitor-enter(r7)
            int r1 = r7.A03     // Catch: all -> 0x0068
            r6 = 0
            r5 = 1
            if (r1 == r5) goto L_0x0008
            goto L_0x0017
        L_0x0008:
            int[] r4 = X.AnonymousClass1IR.A0V     // Catch: all -> 0x0068
            int r3 = r4.length     // Catch: all -> 0x0068
            r2 = 0
        L_0x000c:
            if (r2 >= r3) goto L_0x0066
            r1 = r4[r2]     // Catch: all -> 0x0068
            int r0 = r7.A02     // Catch: all -> 0x0068
            if (r1 == r0) goto L_0x0037
            int r2 = r2 + 1
            goto L_0x000c
        L_0x0017:
            r0 = 2
            if (r1 == r0) goto L_0x0057
            r0 = 3
            if (r1 == r0) goto L_0x0066
            r0 = 6
            if (r1 == r0) goto L_0x0048
            r0 = 7
            if (r1 == r0) goto L_0x0057
            r0 = 8
            if (r1 == r0) goto L_0x0039
            r0 = 10
            if (r1 == r0) goto L_0x0039
            r0 = 20
            if (r1 == r0) goto L_0x0039
            r0 = 100
            if (r1 == r0) goto L_0x0008
            r0 = 200(0xc8, float:2.8E-43)
            if (r1 == r0) goto L_0x0057
        L_0x0037:
            monitor-exit(r7)
            return r6
        L_0x0039:
            int[] r4 = X.AnonymousClass1IR.A0U     // Catch: all -> 0x0068
            int r3 = r4.length     // Catch: all -> 0x0068
            r2 = 0
        L_0x003d:
            if (r2 >= r3) goto L_0x0066
            r1 = r4[r2]     // Catch: all -> 0x0068
            int r0 = r7.A02     // Catch: all -> 0x0068
            if (r1 == r0) goto L_0x0037
            int r2 = r2 + 1
            goto L_0x003d
        L_0x0048:
            int[] r4 = X.AnonymousClass1IR.A0S     // Catch: all -> 0x0068
            int r3 = r4.length     // Catch: all -> 0x0068
            r2 = 0
        L_0x004c:
            if (r2 >= r3) goto L_0x0066
            r1 = r4[r2]     // Catch: all -> 0x0068
            int r0 = r7.A02     // Catch: all -> 0x0068
            if (r1 == r0) goto L_0x0037
            int r2 = r2 + 1
            goto L_0x004c
        L_0x0057:
            int[] r4 = X.AnonymousClass1IR.A0T     // Catch: all -> 0x0068
            int r3 = r4.length     // Catch: all -> 0x0068
            r2 = 0
        L_0x005b:
            if (r2 >= r3) goto L_0x0066
            r1 = r4[r2]     // Catch: all -> 0x0068
            int r0 = r7.A02     // Catch: all -> 0x0068
            if (r1 == r0) goto L_0x0037
            int r2 = r2 + 1
            goto L_0x005b
        L_0x0066:
            monitor-exit(r7)
            return r5
        L_0x0068:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.A0A():boolean");
    }

    public synchronized boolean A0B() {
        return this.A0O;
    }

    public synchronized boolean A0C() {
        boolean z;
        if (this.A03 == 10) {
            int i = this.A02;
            if (i == 12 || i == 19) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    public synchronized boolean A0D() {
        boolean z;
        if (A0F()) {
            int i = this.A02;
            if (i == 12 || i == 19 || i == 20) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r2 == 602) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0E() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.A0D()     // Catch: all -> 0x0015
            if (r0 != 0) goto L_0x0012
            int r2 = r3.A02     // Catch: all -> 0x0015
            r0 = 608(0x260, float:8.52E-43)
            if (r2 == r0) goto L_0x0012
            r1 = 602(0x25a, float:8.44E-43)
            r0 = 0
            if (r2 != r1) goto L_0x0013
        L_0x0012:
            r0 = 1
        L_0x0013:
            monitor-exit(r3)
            return r0
        L_0x0015:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.A0E():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0012, code lost:
        if (r2 == 30) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0F() {
        /*
            r3 = this;
            monitor-enter(r3)
            int r2 = r3.A03     // Catch: all -> 0x0017
            r0 = 20
            if (r2 == r0) goto L_0x0014
            r0 = 40
            if (r2 == r0) goto L_0x0014
            r0 = 10
            if (r2 == r0) goto L_0x0014
            r1 = 30
            r0 = 0
            if (r2 != r1) goto L_0x0015
        L_0x0014:
            r0 = 1
        L_0x0015:
            monitor-exit(r3)
            return r0
        L_0x0017:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.A0F():boolean");
    }

    public synchronized boolean A0G() {
        boolean z;
        int i = this.A03;
        z = true;
        if (!(i == 2 || i == 200 || i == 1 || i == 100 || i == 3)) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        if (r3.A03 == 30) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0H() {
        /*
            r3 = this;
            monitor-enter(r3)
            int r1 = r3.A02     // Catch: all -> 0x001d
            r0 = 106(0x6a, float:1.49E-43)
            if (r1 == r0) goto L_0x001a
            r0 = 405(0x195, float:5.68E-43)
            if (r1 == r0) goto L_0x001a
            r0 = 604(0x25c, float:8.46E-43)
            if (r1 == r0) goto L_0x001a
            r0 = 703(0x2bf, float:9.85E-43)
            if (r1 == r0) goto L_0x001a
            int r2 = r3.A03     // Catch: all -> 0x001d
            r1 = 30
            r0 = 0
            if (r2 != r1) goto L_0x001b
        L_0x001a:
            r0 = 1
        L_0x001b:
            monitor-exit(r3)
            return r0
        L_0x001d:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.A0H():boolean");
    }

    public synchronized boolean A0I() {
        return C31001Zq.A0A(this.A0G, this.A04);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r1 != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0J() {
        /*
            r2 = this;
            monitor-enter(r2)
            X.1Yy r0 = r2.A08     // Catch: all -> 0x0013
            if (r0 == 0) goto L_0x0010
            X.1Zf r0 = r2.A0A     // Catch: all -> 0x0013
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0.A0Y(r2)     // Catch: all -> 0x0013
            r0 = 0
            if (r1 == 0) goto L_0x0011
        L_0x0010:
            r0 = 1
        L_0x0011:
            monitor-exit(r2)
            return r0
        L_0x0013:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.A0J():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:52:0x007f, code lost:
        if (r9.A0X() == false) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0088, code lost:
        if (r12 > r10) goto L_0x008a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002b A[Catch: all -> 0x008d, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x0009, B:9:0x0011, B:13:0x001a, B:15:0x0020, B:20:0x002b, B:25:0x0038, B:27:0x003e, B:28:0x0046, B:31:0x004d, B:36:0x005a, B:37:0x005e, B:51:0x007b), top: B:62:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007b A[Catch: all -> 0x008d, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x0009, B:9:0x0011, B:13:0x001a, B:15:0x0020, B:20:0x002b, B:25:0x0038, B:27:0x003e, B:28:0x0046, B:31:0x004d, B:36:0x005a, B:37:0x005e, B:51:0x007b), top: B:62:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0083 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0K(X.AbstractC30891Zf r17, int r18, long r19) {
        /*
            r16 = this;
            r5 = r16
            monitor-enter(r5)
            long r1 = r5.A06     // Catch: all -> 0x008d
            X.1Zf r9 = r5.A0A     // Catch: all -> 0x008d
            if (r9 == 0) goto L_0x000e
            int r8 = r9.A05()     // Catch: all -> 0x008d
            goto L_0x000f
        L_0x000e:
            r8 = 0
        L_0x000f:
            if (r17 == 0) goto L_0x0016
            int r7 = r17.A05()     // Catch: all -> 0x008d
            goto L_0x0017
        L_0x0016:
            r7 = 0
        L_0x0017:
            r6 = 0
            if (r17 == 0) goto L_0x0025
            X.5YY r0 = r17.A0A()     // Catch: all -> 0x008d
            if (r0 == 0) goto L_0x0026
            java.lang.String r4 = r0.ABW()     // Catch: all -> 0x008d
            goto L_0x0027
        L_0x0025:
            r0 = r6
        L_0x0026:
            r4 = r6
        L_0x0027:
            r14 = 0
            if (r0 == 0) goto L_0x0030
            long r12 = r0.AHR()     // Catch: all -> 0x008d
            goto L_0x0032
        L_0x0030:
            r12 = 0
        L_0x0032:
            if (r9 == 0) goto L_0x0035
            goto L_0x0038
        L_0x0035:
            r10 = 0
            goto L_0x0046
        L_0x0038:
            X.5YY r0 = r9.A0A()     // Catch: all -> 0x008d
            if (r0 == 0) goto L_0x0035
            java.lang.String r6 = r0.ABW()     // Catch: all -> 0x008d
            long r10 = r0.AHR()     // Catch: all -> 0x008d
        L_0x0046:
            int r3 = r5.A01     // Catch: all -> 0x008d
            r0 = 3
            if (r3 != r0) goto L_0x0057
            if (r9 == 0) goto L_0x0052
            long r1 = r9.A09()     // Catch: all -> 0x008d
            goto L_0x0054
        L_0x0052:
            r1 = 0
        L_0x0054:
            if (r17 == 0) goto L_0x005e
            goto L_0x005a
        L_0x0057:
            r14 = r19
            goto L_0x005e
        L_0x005a:
            long r14 = r17.A09()     // Catch: all -> 0x008d
        L_0x005e:
            int r3 = r5.A03     // Catch: all -> 0x008d
            r0 = 5
            if (r3 == r0) goto L_0x0067
            r0 = 1000(0x3e8, float:1.401E-42)
            if (r3 != r0) goto L_0x006b
        L_0x0067:
            r0 = r18
            if (r3 != r0) goto L_0x008a
        L_0x006b:
            int r0 = (r14 > r1 ? 1 : (r14 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x008a
            int r0 = (r14 > r1 ? 1 : (r14 == r1 ? 0 : -1))
            if (r0 != 0) goto L_0x0079
            if (r7 != 0) goto L_0x0077
            if (r8 == 0) goto L_0x008a
        L_0x0077:
            if (r7 > r8) goto L_0x008a
        L_0x0079:
            if (r9 == 0) goto L_0x0081
            boolean r0 = r9.A0X()     // Catch: all -> 0x008d
            if (r0 != 0) goto L_0x008a
        L_0x0081:
            if (r4 == 0) goto L_0x0085
            if (r4 != r6) goto L_0x008a
        L_0x0085:
            int r1 = (r12 > r10 ? 1 : (r12 == r10 ? 0 : -1))
            r0 = 0
            if (r1 <= 0) goto L_0x008b
        L_0x008a:
            r0 = 1
        L_0x008b:
            monitor-exit(r5)
            return r0
        L_0x008d:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1IR.A0K(X.1Zf, int, long):boolean");
    }

    public synchronized boolean A0L(AnonymousClass1IR r5) {
        return A0K(r5.A0A, r5.A03, r5.A06);
    }

    public synchronized boolean A0M(UserJid userJid) {
        boolean z;
        if (userJid != null) {
            if (!userJid.equals(this.A0E)) {
                z = false;
                if (userJid.equals(this.A0D)) {
                }
            }
        }
        z = true;
        return z;
    }

    @Override // java.lang.Object
    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append("id: ");
        sb.append(this.A0K);
        sb.append(" key_remote_id: ");
        sb.append(this.A0C);
        sb.append(" key_from_me: ");
        sb.append(this.A0Q);
        sb.append(" key_id: ");
        sb.append(this.A0L);
        sb.append(" status: ");
        sb.append(this.A02);
        sb.append(" type: ");
        sb.append(this.A03);
        sb.append(" updateTs: ");
        sb.append(this.A06);
        sb.append(" initTs: ");
        sb.append(this.A05);
        sb.append(" error_code: ");
        sb.append(this.A0J);
        sb.append(" sender: ");
        sb.append(this.A0E);
        sb.append(" receiver: ");
        sb.append(this.A0D);
        sb.append(" credential_id: ");
        sb.append(this.A0H);
        sb.append(" methods: ");
        sb.append(this.A0N);
        sb.append(" reqMsgKeyId: ");
        sb.append(this.A0M);
        sb.append(" metadata: ");
        sb.append(this.A0A);
        sb.append(" country: ");
        sb.append(this.A0G);
        sb.append(" version: ");
        sb.append(this.A04);
        sb.append(" interop: ");
        sb.append(this.A0P);
        sb.append(" background: ");
        sb.append(this.A0B);
        sb.append(" purchase_initiator: ");
        sb.append(this.A00);
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public synchronized void writeToParcel(Parcel parcel, int i) {
        int i2;
        String str;
        parcel.writeInt(this.A03);
        parcel.writeInt(this.A02);
        parcel.writeLong(this.A06);
        parcel.writeString(this.A0I);
        C30821Yy r0 = this.A08;
        if (r0 != null) {
            i2 = r0.A00.scale();
        } else {
            i2 = 0;
        }
        parcel.writeInt(i2);
        C30821Yy r02 = this.A08;
        String str2 = null;
        if (r02 != null) {
            str = r02.A00.toString();
        } else {
            str = null;
        }
        parcel.writeString(str);
        parcel.writeString(this.A0K);
        UserJid userJid = this.A0E;
        parcel.writeString(userJid == null ? null : userJid.getRawString());
        UserJid userJid2 = this.A0D;
        if (userJid2 != null) {
            str2 = userJid2.getRawString();
        }
        parcel.writeString(str2);
        parcel.writeString(this.A0L);
        parcel.writeString(this.A0H);
        parcel.writeString(this.A0J);
        parcel.writeString(this.A0F);
        parcel.writeList(this.A0N);
        parcel.writeString(C15380n4.A03(this.A0C));
        int i3 = 1;
        int i4 = 0;
        if (this.A0Q) {
            i4 = 1;
        }
        parcel.writeInt(i4);
        parcel.writeLong(this.A05);
        parcel.writeString(this.A0M);
        parcel.writeString(this.A0G);
        parcel.writeInt(this.A04);
        if (!this.A0P) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeParcelable(this.A0B, 0);
        parcel.writeInt(this.A00);
        byte[] bArr = this.A0R;
        if (bArr != null) {
            parcel.writeInt(bArr.length);
            parcel.writeByteArray(this.A0R);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.A01);
        parcel.writeParcelable(this.A0A, 0);
        this.A07.writeToParcel(parcel, i);
    }
}
