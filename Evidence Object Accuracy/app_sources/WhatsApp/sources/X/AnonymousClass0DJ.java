package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

/* renamed from: X.0DJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DJ extends C06400Tl {
    public static final Class A00;
    public static final Constructor A01;
    public static final Method A02;
    public static final Method A03;

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.lang.Class */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.lang.reflect.Method */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.reflect.Method */
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: java.lang.Class */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.lang.reflect.Method */
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: java.lang.reflect.Method */
    /* JADX WARN: Multi-variable type inference failed */
    static {
        Class cls;
        Method method;
        Method method2;
        Constructor<?> constructor = null;
        try {
            Class<?> cls2 = Class.forName("android.graphics.FontFamily");
            Constructor<?> constructor2 = cls2.getConstructor(new Class[0]);
            Class<?> cls3 = Integer.TYPE;
            Method method3 = cls2.getMethod("addFontWeightStyle", ByteBuffer.class, cls3, List.class, cls3, Boolean.TYPE);
            constructor = constructor2;
            method2 = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(cls2, 1).getClass());
            method = method3;
            cls = cls2;
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e("TypefaceCompatApi24Impl", e.getClass().getName(), e);
            cls = constructor;
            method2 = constructor;
            method = constructor;
        }
        A01 = constructor;
        A00 = cls;
        A02 = method;
        A03 = method2;
    }

    public static boolean A00() {
        if (A02 != null) {
            return true;
        }
        Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        return false;
    }

    public static boolean A01(Object obj, ByteBuffer byteBuffer, int i, int i2, boolean z) {
        try {
            return ((Boolean) A02.invoke(obj, byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Boolean.valueOf(z))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return false;
        }
    }

    @Override // X.C06400Tl
    public Typeface A04(Context context, Resources resources, AnonymousClass0MQ r18, int i) {
        Object obj;
        MappedByteBuffer mappedByteBuffer;
        try {
            obj = A01.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            obj = null;
        }
        if (obj != null) {
            C05020Ny[] r6 = r18.A00;
            for (C05020Ny r8 : r6) {
                int i2 = r8.A00;
                File A002 = C06500Tw.A00(context);
                if (A002 != null) {
                    try {
                        if (C06500Tw.A02(resources, A002, i2)) {
                            try {
                                FileInputStream fileInputStream = new FileInputStream(A002);
                                try {
                                    FileChannel channel = fileInputStream.getChannel();
                                    mappedByteBuffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                                    fileInputStream.close();
                                } catch (Throwable th) {
                                    try {
                                        fileInputStream.close();
                                    } catch (Throwable unused2) {
                                    }
                                    throw th;
                                    break;
                                }
                            } catch (IOException unused3) {
                                mappedByteBuffer = null;
                            }
                            if (mappedByteBuffer != null && A01(obj, mappedByteBuffer, r8.A01, r8.A02, r8.A05)) {
                            }
                        }
                    } finally {
                        A002.delete();
                    }
                }
                mappedByteBuffer = null;
                if (mappedByteBuffer != null) {
                }
            }
            try {
                Object newInstance = Array.newInstance(A00, 1);
                Array.set(newInstance, 0, obj);
                return (Typeface) A03.invoke(null, newInstance);
            } catch (IllegalAccessException | InvocationTargetException unused4) {
                return null;
            }
        }
        return null;
    }

    @Override // X.C06400Tl
    public Typeface A06(Context context, CancellationSignal cancellationSignal, C04920No[] r13, int i) {
        Object obj;
        Object obj2;
        Typeface typeface;
        try {
            obj2 = A01.newInstance(new Object[0]);
            obj = obj2;
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            obj2 = null;
            obj = null;
        }
        if (obj2 != null) {
            AnonymousClass00O r9 = new AnonymousClass00O();
            int length = r13.length;
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    C04920No r5 = r13[i2];
                    Uri uri = r5.A03;
                    ByteBuffer byteBuffer = (ByteBuffer) r9.get(uri);
                    if (byteBuffer == null) {
                        byteBuffer = C06500Tw.A01(context, uri);
                        r9.put(uri, byteBuffer);
                    }
                    if (byteBuffer == null || !A01(obj, byteBuffer, r5.A01, r5.A02, r5.A04)) {
                        break;
                    }
                    i2++;
                } else {
                    try {
                        Object newInstance = Array.newInstance(A00, 1);
                        Array.set(newInstance, 0, obj);
                        typeface = (Typeface) A03.invoke(null, newInstance);
                    } catch (IllegalAccessException | InvocationTargetException unused2) {
                        typeface = null;
                    }
                    if (typeface != null) {
                        return Typeface.create(typeface, i);
                    }
                }
            }
        }
        return null;
    }
}
