package X;

import android.util.Log;

/* renamed from: X.0R6  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0R6 {
    public static void A00(String str) {
        if (Log.isLoggable("InstallReferrerClient", 2)) {
            Log.v("InstallReferrerClient", str);
        }
    }

    public static void A01(String str) {
        if (Log.isLoggable("InstallReferrerClient", 5)) {
            Log.w("InstallReferrerClient", str);
        }
    }
}
