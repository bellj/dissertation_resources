package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* renamed from: X.2en  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53652en extends AbstractC015707l {
    public static final Parcelable.Creator CREATOR = new C98494im();
    public CharSequence A00;
    public boolean A01;

    public C53652en(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        this.A00 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.A01 = C12970iu.A1W(parcel.readInt());
    }

    public C53652en(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("TextInputLayout.SavedState{");
        A0k.append(Integer.toHexString(System.identityHashCode(this)));
        A0k.append(" error=");
        A0k.append((Object) this.A00);
        return C12960it.A0d("}", A0k);
    }

    @Override // X.AbstractC015707l, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        TextUtils.writeToParcel(this.A00, parcel, i);
        parcel.writeInt(this.A01 ? 1 : 0);
    }
}
