package X;

import android.os.Bundle;

/* renamed from: X.3Ow  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66773Ow implements AnonymousClass07L {
    public final /* synthetic */ AbstractActivityC35431hr A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66773Ow(AbstractActivityC35431hr r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        AbstractActivityC35431hr r3 = this.A00;
        r3.A0O = str;
        r3.A0P = C32751cg.A02(((ActivityC13830kP) r3).A01, str);
        Bundle A0D = C12970iu.A0D();
        A0D.putString("query", str);
        r3.A0W().A00(A0D, r3);
        return false;
    }
}
