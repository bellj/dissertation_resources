package X;

import android.content.Context;
import android.net.SSLSessionCache;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.1NU  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1NU extends SSLSocketFactory {
    public SSLSocketFactory A00;
    public final SSLSessionCache A01;
    public final AnonymousClass14I A02;
    public final SSLContext A03;
    public volatile String[] A04;

    public AnonymousClass1NU(Context context, AnonymousClass14I r6) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            this.A03 = instance;
            this.A02 = r6;
            instance.getClientSessionContext().setSessionTimeout(86400);
            instance.getClientSessionContext().setSessionCacheSize(24);
            File cacheDir = context.getCacheDir();
            SSLSessionCache sSLSessionCache = null;
            if (cacheDir != null && cacheDir.exists()) {
                try {
                    sSLSessionCache = new SSLSessionCache(new File(cacheDir, "SSLSessionCache"));
                } catch (IOException unused) {
                }
            }
            this.A01 = sSLSessionCache;
        } catch (NoSuchAlgorithmException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("TLS");
            sb.append(" algorithm not available for SSLContext: ");
            Log.w(sb.toString(), e);
            throw new RuntimeException(e);
        }
    }

    public final synchronized SSLSocketFactory A00() {
        SSLSocketFactory sSLSocketFactory;
        sSLSocketFactory = this.A00;
        if (sSLSocketFactory == null) {
            SSLContext sSLContext = this.A03;
            SSLSessionCache sSLSessionCache = this.A01;
            if (this instanceof AnonymousClass1NW) {
                AnonymousClass1NW r2 = (AnonymousClass1NW) this;
                try {
                    sSLContext.init(null, AnonymousClass1NW.A02, null);
                    if (sSLSessionCache != null) {
                        AnonymousClass3I7.A01(sSLSessionCache, sSLContext);
                    }
                    sSLSocketFactory = new C71813dY(sSLSessionCache, r2, sSLContext, sSLContext.getSocketFactory());
                    this.A00 = sSLSocketFactory;
                } catch (KeyManagementException e) {
                    Log.e(e);
                    throw new RuntimeException(e);
                }
            } else if (!(this instanceof AnonymousClass2UX)) {
                try {
                    sSLContext.init(null, AnonymousClass1NT.A02, null);
                    if (sSLSessionCache != null) {
                        AnonymousClass3I7.A01(sSLSessionCache, sSLContext);
                    }
                    sSLSocketFactory = sSLContext.getSocketFactory();
                    this.A00 = sSLSocketFactory;
                } catch (KeyManagementException e2) {
                    Log.e(e2);
                    throw new RuntimeException(e2);
                }
            } else {
                try {
                    sSLContext.init(null, AnonymousClass2UX.A00, null);
                    sSLSocketFactory = sSLContext.getSocketFactory();
                    this.A00 = sSLSocketFactory;
                } catch (KeyManagementException e3) {
                    Log.e(e3);
                    throw new RuntimeException(e3);
                }
            }
        }
        return sSLSocketFactory;
    }

    public void A01(Socket socket) {
        if (socket instanceof SSLSocket) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            String[] strArr = this.A04;
            if (strArr == null) {
                String[] supportedProtocols = sSLSocket.getSupportedProtocols();
                ArrayList arrayList = new ArrayList();
                if (supportedProtocols != null) {
                    for (String str : supportedProtocols) {
                        if (str != null && str.startsWith("TLS")) {
                            arrayList.add(str);
                        }
                    }
                }
                strArr = (String[]) arrayList.toArray(new String[0]);
                this.A04 = strArr;
            }
            if (strArr.length > 0) {
                sSLSocket.setEnabledProtocols(strArr);
            }
        }
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket() {
        Socket createSocket = A00().createSocket();
        A01(createSocket);
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(String str, int i) {
        Socket createSocket = A00().createSocket(str, i);
        A01(createSocket);
        if (createSocket instanceof SSLSocket) {
            this.A02.A00(str, createSocket);
        }
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        Socket createSocket = A00().createSocket(str, i, inetAddress, i2);
        A01(createSocket);
        if (createSocket instanceof SSLSocket) {
            this.A02.A00(str, createSocket);
        }
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(InetAddress inetAddress, int i) {
        Socket createSocket = A00().createSocket(inetAddress, i);
        A01(createSocket);
        if (createSocket instanceof SSLSocket) {
            this.A02.A00(inetAddress.getHostName(), createSocket);
        }
        return createSocket;
    }

    @Override // javax.net.SocketFactory
    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        Socket createSocket = A00().createSocket(inetAddress, i, inetAddress2, i2);
        A01(createSocket);
        if (createSocket instanceof SSLSocket) {
            this.A02.A00(inetAddress.getHostName(), createSocket);
        }
        return createSocket;
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        Socket createSocket = A00().createSocket(socket, str, i, z);
        A01(createSocket);
        if (createSocket instanceof SSLSocket) {
            this.A02.A00(str, createSocket);
        }
        return createSocket;
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public String[] getDefaultCipherSuites() {
        return A00().getDefaultCipherSuites();
    }

    @Override // javax.net.ssl.SSLSocketFactory
    public String[] getSupportedCipherSuites() {
        return A00().getSupportedCipherSuites();
    }
}
