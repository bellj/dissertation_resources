package X;

import X.AbstractC001200n;
import X.AbstractC009904y;
import X.AbstractC010305c;
import X.AnonymousClass051;
import X.AnonymousClass06T;
import X.AnonymousClass074;
import X.AnonymousClass0XA;
import java.util.ArrayDeque;
import java.util.Iterator;

/* renamed from: X.051  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass051 {
    public final Runnable A00;
    public final ArrayDeque A01;

    public AnonymousClass051() {
        this(null);
    }

    public AnonymousClass051(Runnable runnable) {
        this.A01 = new ArrayDeque();
        this.A00 = runnable;
    }

    public void A00() {
        Iterator descendingIterator = this.A01.descendingIterator();
        while (descendingIterator.hasNext()) {
            AbstractC010305c r1 = (AbstractC010305c) descendingIterator.next();
            if (r1.A01) {
                r1.A00();
                return;
            }
        }
        Runnable runnable = this.A00;
        if (runnable != null) {
            runnable.run();
        }
    }

    public void A01(AbstractC010305c r4, AbstractC001200n r5) {
        AbstractC009904y ADr = r5.ADr();
        if (((C009804x) ADr).A02 != AnonymousClass05I.DESTROYED) {
            r4.A00.add(new Object(r4, this, ADr) { // from class: androidx.activity.OnBackPressedDispatcher$LifecycleOnBackPressedCancellable
                public AnonymousClass06T A00;
                public final AbstractC010305c A01;
                public final AbstractC009904y A02;
                public final /* synthetic */ AnonymousClass051 A03;

                {
                    this.A03 = r2;
                    this.A02 = r3;
                    this.A01 = r1;
                    r3.A00(this);
                }

                @Override // X.AnonymousClass054
                public void AWQ(AnonymousClass074 r52, AbstractC001200n r6) {
                    if (r52 == AnonymousClass074.ON_START) {
                        AnonymousClass051 r3 = this.A03;
                        AbstractC010305c r2 = this.A01;
                        r3.A01.add(r2);
                        AnonymousClass0XA r1 = new AnonymousClass0XA(r2, r3);
                        r2.A00.add(r1);
                        this.A00 = r1;
                    } else if (r52 == AnonymousClass074.ON_STOP) {
                        AnonymousClass06T r0 = this.A00;
                        if (r0 != null) {
                            r0.cancel();
                        }
                    } else if (r52 == AnonymousClass074.ON_DESTROY) {
                        cancel();
                    }
                }

                @Override // X.AnonymousClass06T
                public void cancel() {
                    this.A02.A01(this);
                    this.A01.A00.remove(this);
                    AnonymousClass06T r0 = this.A00;
                    if (r0 != null) {
                        r0.cancel();
                        this.A00 = null;
                    }
                }
            });
        }
    }
}
