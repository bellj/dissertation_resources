package X;

import android.text.TextUtils;
import android.util.Base64;
import java.io.UnsupportedEncodingException;

/* renamed from: X.5zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130775zx {
    public final C15570nT A00;
    public final C14830m7 A01;
    public final C18610sj A02;

    public C130775zx(C15570nT r1, C14830m7 r2, C18610sj r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: java.lang.String[] */
    /* JADX WARN: Multi-variable type inference failed */
    public static byte[] A00(Boolean bool, Object obj, String str, String str2, byte[] bArr, Object[] objArr, long j) {
        String str3;
        Object obj2;
        if (bool == null) {
            str3 = null;
        } else {
            str3 = bool.booleanValue() ? "PIN" : "BIO";
        }
        int length = objArr.length;
        int i = length + 6;
        Object[] objArr2 = new Object[i];
        C12990iw.A1P(str, str3, objArr2);
        objArr2[2] = obj;
        objArr2[3] = Long.valueOf(j);
        C117305Zk.A1M(str2, bArr, objArr2);
        System.arraycopy(objArr, 0, objArr2, 6, length);
        String[] strArr = new String[i];
        for (int i2 = 0; i2 < i; i2++) {
            if (objArr2[i2] == null) {
                obj2 = "";
            } else {
                boolean z = objArr2[i2] instanceof String;
                obj2 = objArr2[i2];
                if (z) {
                    continue;
                } else if ((obj2 instanceof Long) || (objArr2[i2] instanceof Integer)) {
                    obj2 = String.valueOf(objArr2[i2]);
                } else if (objArr2[i2] instanceof byte[]) {
                    obj2 = Base64.encodeToString((byte[]) objArr2[i2], 2);
                } else {
                    throw C12970iu.A0f(C30931Zj.A01("PinActions", "getPinNode: should only accept long, int, byte[], and String args"));
                }
            }
            strArr[i2] = obj2;
        }
        try {
            return C117315Zl.A0a(TextUtils.join("|", strArr));
        } catch (UnsupportedEncodingException e) {
            C117305Zk.A1N("PinActions", C12960it.A0b("constructPayload: UTF-8 not supported: ", e));
            throw new Error(e);
        }
    }
}
