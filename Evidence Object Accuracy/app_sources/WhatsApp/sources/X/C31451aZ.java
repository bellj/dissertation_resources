package X;

/* renamed from: X.1aZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31451aZ {
    public C31461aa A00;

    public C31451aZ(C31461aa r1) {
        this.A00 = r1;
    }

    public C31451aZ(C31491ad r9, AbstractC31681aw r10, byte[][] bArr, int i, int i2) {
        AnonymousClass1G4 A0T = C31471ab.A03.A0T();
        byte[] A00 = r9.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C31471ab r1 = (C31471ab) A0T.A00;
        r1.A00 |= 1;
        r1.A02 = A01;
        if (r10 instanceof C31691ax) {
            byte[] bArr2 = ((C31721b0) r10.A00()).A00;
            AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
            A0T.A03();
            C31471ab r12 = (C31471ab) A0T.A00;
            r12.A00 |= 2;
            r12.A01 = A012;
        }
        AnonymousClass1G4 A0T2 = C31461aa.A04.A0T();
        A0T2.A03();
        C31461aa r13 = (C31461aa) A0T2.A00;
        r13.A00 |= 1;
        r13.A01 = i;
        A0T2.A03();
        C31461aa r14 = (C31461aa) A0T2.A00;
        r14.A03 = (C31471ab) A0T.A02();
        r14.A00 |= 2;
        int length = bArr.length;
        int[] A002 = AnonymousClass4FQ.A00(i2, length);
        for (int i3 = 0; i3 < length; i3++) {
            C466726t r3 = (C466726t) AnonymousClass25E.A03.A0T();
            r3.A05(A002[i3]);
            byte[] bArr3 = bArr[i3];
            r3.A06(AbstractC27881Jp.A01(bArr3, 0, bArr3.length));
            AbstractC27091Fz A02 = r3.A02();
            A0T2.A03();
            C31461aa r2 = (C31461aa) A0T2.A00;
            AnonymousClass1K6 r15 = r2.A02;
            if (!((AnonymousClass1K7) r15).A00) {
                r15 = AbstractC27091Fz.A0G(r15);
                r2.A02 = r15;
            }
            r15.add(A02);
        }
        this.A00 = (C31461aa) A0T2.A02();
    }

    public void A00(C31501ae r10) {
        byte[][] bArr = r10.A01;
        int i = r10.A00;
        int length = bArr.length;
        int[] A00 = AnonymousClass4FQ.A00(i, length);
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        ((C31461aa) A0T.A00).A02 = AnonymousClass277.A01;
        for (int i2 = 0; i2 < length; i2++) {
            C466726t r3 = (C466726t) AnonymousClass25E.A03.A0T();
            r3.A05(A00[i2]);
            byte[] bArr2 = bArr[i2];
            r3.A06(AbstractC27881Jp.A01(bArr2, 0, bArr2.length));
            AbstractC27091Fz A02 = r3.A02();
            A0T.A03();
            C31461aa r2 = (C31461aa) A0T.A00;
            AnonymousClass1K6 r1 = r2.A02;
            if (!((AnonymousClass1K7) r1).A00) {
                r1 = AbstractC27091Fz.A0G(r1);
                r2.A02 = r1;
            }
            r1.add(A02);
        }
        this.A00 = (C31461aa) A0T.A02();
    }
}
