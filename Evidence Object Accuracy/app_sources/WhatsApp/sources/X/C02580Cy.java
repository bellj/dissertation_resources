package X;

/* renamed from: X.0Cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02580Cy extends C07270Xi {
    public int A00;

    public C02580Cy(AbstractC07280Xj r2) {
        super(r2);
        EnumC03760Ja r0;
        if (r2 instanceof AnonymousClass0D3) {
            r0 = EnumC03760Ja.HORIZONTAL_DIMENSION;
        } else {
            r0 = EnumC03760Ja.VERTICAL_DIMENSION;
        }
        this.A04 = r0;
    }
}
