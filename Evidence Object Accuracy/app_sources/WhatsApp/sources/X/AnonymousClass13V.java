package X;

import android.os.Message;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import com.whatsapp.jid.DeviceJid;

/* renamed from: X.13V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13V implements AbstractC15920o8 {
    public final C18920tH A00;
    public final C17230qT A01;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{212};
    }

    public AnonymousClass13V(C18920tH r1, C17230qT r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        if (i != 212) {
            return false;
        }
        Object obj = message.obj;
        AnonymousClass009.A05(obj);
        AnonymousClass1OT r4 = (AnonymousClass1OT) obj;
        AnonymousClass1V4 A00 = this.A01.A00(1, r4.A00);
        if (A00 != null) {
            A00.A02(3);
        }
        C18920tH r2 = this.A00;
        DeviceJid of = DeviceJid.of(r4.A01);
        String str = r4.A07;
        if (of == null || TextUtils.isEmpty(str)) {
            return true;
        }
        r2.A0I.Ab2(new RunnableBRunnable0Shape0S1300000_I0(r2, of, r4, str, 3));
        return true;
    }
}
