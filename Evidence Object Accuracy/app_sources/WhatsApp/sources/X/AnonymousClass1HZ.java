package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;

/* renamed from: X.1HZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1HZ extends BroadcastReceiver {
    public final /* synthetic */ AnonymousClass117 A00;

    public AnonymousClass1HZ(AnonymousClass117 r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.A00.A05.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, 34, context));
    }
}
