package X;

import android.graphics.Rect;

/* renamed from: X.0Pa  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05300Pa {
    public final C05400Pk A00;

    public C05300Pa(Rect rect) {
        this.A00 = new C05400Pk(rect);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !C05300Pa.class.equals(obj.getClass())) {
            return false;
        }
        return C16700pc.A0O(this.A00, ((C05300Pa) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("WindowMetrics { bounds: ");
        C05400Pk r0 = this.A00;
        sb.append(new Rect(r0.A01, r0.A03, r0.A02, r0.A00));
        sb.append(" }");
        return sb.toString();
    }
}
