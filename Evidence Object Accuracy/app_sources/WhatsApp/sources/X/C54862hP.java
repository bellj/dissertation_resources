package X;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.2hP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54862hP extends AbstractC05270Ox {
    public static final ViewGroup.MarginLayoutParams A01;
    public AnonymousClass4VX A00;

    static {
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-1, -1);
        marginLayoutParams.setMargins(0, 0, 0, 0);
        A01 = marginLayoutParams;
    }

    public C54862hP(AnonymousClass4VX r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        View A0C;
        ViewGroup.MarginLayoutParams marginLayoutParams;
        int height;
        int top;
        int i3;
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int A1A = linearLayoutManager.A1A();
        if (A1A != -1 && (A0C = linearLayoutManager.A0C(A1A)) != null) {
            if (A0C.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                marginLayoutParams = C12970iu.A0H(A0C);
            } else {
                marginLayoutParams = A01;
            }
            if (linearLayoutManager.A01 == 0) {
                height = A0C.getWidth() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
                if (AnonymousClass028.A05(((AnonymousClass02H) linearLayoutManager).A07) == 0) {
                    top = A0C.getLeft();
                    i3 = marginLayoutParams.leftMargin;
                } else {
                    top = height - A0C.getRight();
                    i3 = marginLayoutParams.rightMargin;
                }
            } else {
                height = marginLayoutParams.bottomMargin + A0C.getHeight() + marginLayoutParams.topMargin;
                top = A0C.getTop();
                i3 = marginLayoutParams.topMargin;
            }
            int i4 = -(top - i3);
            float f = 0.0f;
            if (height != 0) {
                f = ((float) i4) / ((float) height);
            }
            AnonymousClass4VX r4 = this.A00;
            C14210l2 r2 = new C14210l2();
            C14260l7 r3 = r4.A00;
            r2.A05(r3, 0);
            r2.A05(Integer.valueOf(A1A), 1);
            r2.A05(C64983Hr.A00((double) f), 2);
            C28701Oq.A01(r3, r4.A01, C14210l2.A01(r2, Integer.valueOf(i4), 3), r4.A02);
        }
    }
}
