package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.util.Base64;
import android.util.JsonReader;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService;
import com.whatsapp.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.crypto.AEADBadTagException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: X.1Bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C25981Bo {
    public static final int A0E = 1000;
    public static final int A0F = 131072;
    public static final String A0G = "import/complete/all";
    public static final String A0H = "import/complete/enc_metadata";
    public static final String A0I = "import/complete/file_list";
    public static final String A0J = "import/complete/files";
    public static final String A0K = "import/metadata/data_id";
    public static final String A0L = "import/metadata/key";
    public static final String A0M = "import/metadata/key-jid";
    public static final String A0N = "import/metadata/key/account_hash";
    public static final String A0O = "import/metadata/key/proto_version";
    public static final String A0P = "import/metadata/key/server_salt";
    public static final String A0Q = "import/metadata/scheme";
    public static final String A0R = "import/metadata/source_id";
    public static final String A0S = "migration/prefetcher";
    public static final String A0T = "xpm/file-prefetcher";
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C16590pI A02;
    public final C26011Br A03;
    public final C26091Bz A04;
    public final AnonymousClass1C0 A05;
    public final AnonymousClass1C1 A06;
    public final C26081By A07;
    public final C26001Bq A08;
    public final C19510uE A09;
    public final C26071Bx A0A;
    public final C15740np A0B;
    public final AnonymousClass01H A0C;
    public final AtomicBoolean A0D = new AtomicBoolean(false);

    public C25981Bo(AbstractC15710nm r3, C15570nT r4, C16590pI r5, C19510uE r6, C26071Bx r7, C26001Bq r8, C15740np r9, AnonymousClass01H r10, C26081By r11, C26011Br r12, C26091Bz r13, AnonymousClass1C0 r14, AnonymousClass1C1 r15) {
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
        this.A08 = r8;
        this.A09 = r6;
        this.A0A = r7;
        this.A0C = r10;
        this.A0B = r9;
        this.A03 = r12;
        this.A07 = r11;
        this.A04 = r13;
        this.A05 = r14;
        this.A06 = r15;
    }

    private OutputStream A00(OutputStream outputStream, String str, String str2) {
        if (str2 == null) {
            return outputStream;
        }
        String A00 = this.A06.A00(A0L);
        if (A00 != null) {
            byte[] decode = Base64.decode(A00, 2);
            byte[] decode2 = Base64.decode(str2, 2);
            try {
                Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                instance.init(2, new SecretKeySpec(decode, "AES"), new IvParameterSpec(decode2));
                return new CipherOutputStream(outputStream, instance);
            } catch (GeneralSecurityException e) {
                StringBuilder sb = new StringBuilder("Failed to decrypt: ");
                sb.append(str);
                throw new AnonymousClass2GB(sb.toString(), e, 105);
            }
        } else {
            throw new IOException("Cannot create decryption stream due to a missing key.");
        }
    }

    private void A01() {
        A0I("migration/messages_export.zip");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x014f, code lost:
        if (r8 == null) goto L_0x0154;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A02(android.os.CancellationSignal r18, byte[] r19) {
        /*
        // Method dump skipped, instructions count: 389
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C25981Bo.A02(android.os.CancellationSignal, byte[]):void");
    }

    public static void A03(String str, Throwable th) {
        if (Build.VERSION.SDK_INT >= 19) {
            Throwable cause = th.getCause();
            if (cause instanceof AEADBadTagException) {
                StringBuilder sb = new StringBuilder("Failed to decrypt: ");
                sb.append(str);
                throw new AnonymousClass2GB(sb.toString(), cause, 104);
            }
        }
    }

    public File A04() {
        return new File(this.A02.A00.getFilesDir(), A0S);
    }

    public File A05(String str) {
        File canonicalFile = A04().getCanonicalFile();
        File canonicalFile2 = new File(canonicalFile, str).getCanonicalFile();
        for (File parentFile = canonicalFile2.getParentFile(); parentFile != null; parentFile = parentFile.getParentFile()) {
            if (canonicalFile.equals(parentFile)) {
                return canonicalFile2;
            }
        }
        StringBuilder sb = new StringBuilder("Remote path '");
        sb.append(str);
        sb.append("' escaped prefetch sandbox: '");
        sb.append(canonicalFile2);
        sb.append("'");
        throw new SecurityException(sb.toString());
    }

    public File A06(String str) {
        C26081By r0 = this.A07;
        AnonymousClass009.A05(str);
        C16310on A00 = r0.A00.A00();
        try {
            boolean z = true;
            Cursor A09 = A00.A03.A09("SELECT prefetched_file_path, prefetched FROM prefetched_files WHERE remote_file_path = ?", new String[]{str});
            if (!A09.moveToFirst()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unknown remote file: ");
                sb.append(str);
                throw new FileNotFoundException(sb.toString());
            } else if (A09.getCount() <= 1) {
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("prefetched");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("prefetched_file_path");
                long j = A09.getLong(columnIndexOrThrow);
                boolean z2 = false;
                if (j == -1) {
                    z2 = true;
                }
                if (j != 1) {
                    z = false;
                }
                String string = A09.getString(columnIndexOrThrow2);
                if (!z || TextUtils.isEmpty(string)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("File was not prefetched: ");
                    sb2.append(str);
                    sb2.append(", prefetch failed: ");
                    sb2.append(z2);
                    throw new FileNotFoundException(sb2.toString());
                }
                File file = new File(string);
                A09.close();
                A00.close();
                if (file.exists()) {
                    return file;
                }
                StringBuilder sb3 = new StringBuilder("Not found in file prefetcher sandbox: ");
                sb3.append(str);
                throw new FileNotFoundException(sb3.toString());
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Multiple prefetched files match: ");
                sb4.append(A09.getCount());
                throw new FileNotFoundException(sb4.toString());
            }
        } catch (Throwable th) {
            try {
                A00.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A07() {
        this.A06.A02(A0G);
    }

    public void A08() {
        this.A0D.get();
    }

    public void A09() {
        AnonymousClass1C1 r4 = this.A06;
        synchronized (r4) {
            r4.A00 = null;
            C16310on A01 = r4.A04.A00.A01();
            A01.A03.A01("properties", null, null);
            A01.close();
        }
        AnonymousClass1C0 r2 = this.A05;
        synchronized (r2) {
            synchronized (r2) {
                AnonymousClass1C3 r0 = r2.A00;
                if (r0 != null) {
                    r0.close();
                    r2.A00 = null;
                }
            }
            C14350lI.A0C(A04());
        }
        r2.A01.deleteDatabase("migration_prefetcher.db");
        Log.i("FilePrefetcher/removeDatabase/deleted");
        C14350lI.A0C(A04());
    }

    public void A0A(CancellationSignal cancellationSignal) {
        byte[] bArr;
        String str;
        Log.i("xpm/file-prefetcher/importEncryptionKey(); ");
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r02 = r0.A05;
        if (r02 != null) {
            String rawString = r02.getRawString();
            String A00 = this.A06.A00(A0M);
            if (!rawString.equals(A00)) {
                StringBuilder sb = new StringBuilder("xpm/file-prefetcher/importEncryptionKey(); key was cached for a different jid, removing (old jid: ");
                sb.append(A00);
                sb.append(") ");
                Log.i(sb.toString());
                this.A09.A04.A00.clear();
                this.A06.A01(A0M, null);
                this.A06.A01(A0L, null);
            }
            if (this.A06.A00(A0L) != null) {
                str = "xpm/file-prefetcher/importEncryptionKey(); encryption key has already been fetched from the server, skipping.";
            } else {
                String A002 = this.A06.A00(A0O);
                String A003 = this.A06.A00(A0N);
                String A004 = this.A06.A00(A0P);
                if (A002 == null || A003 == null || A004 == null) {
                    StringBuilder sb2 = new StringBuilder("Missing key identifier: version=");
                    sb2.append(A002);
                    sb2.append(", account_hash=");
                    sb2.append(A003);
                    sb2.append(", server_salt=");
                    sb2.append(A004);
                    throw new AnonymousClass498(sb2.toString());
                }
                StringBuilder sb3 = new StringBuilder("xpm/file-prefetcher/importEncryptionKey(); key, version=");
                sb3.append(A002);
                sb3.append(", account_hash=");
                sb3.append(A003);
                sb3.append(", server_salt=");
                sb3.append(A004);
                Log.i(sb3.toString());
                C19510uE r9 = this.A09;
                C15570nT r14 = r9.A00;
                r14.A08();
                C27631Ih r1 = r14.A05;
                if (r1 != null) {
                    byte[] decode = Base64.decode(A003, 2);
                    byte[] decode2 = Base64.decode(A004, 2);
                    CountDownLatch countDownLatch = new CountDownLatch(1);
                    r9.A02.A00(new RunnableBRunnable0Shape4S0100000_I0_4(countDownLatch, 23), A002, decode2, decode);
                    try {
                        C19510uE.A00(cancellationSignal, countDownLatch);
                        if (countDownLatch.getCount() <= 0) {
                            r14.A08();
                            C27631Ih r142 = r14.A05;
                            if (r142 == null) {
                                throw new AnonymousClass2GB(301, "User was logged out while waiting for encryption key.");
                            } else if (r142.equals(r1)) {
                                C32821cn r03 = new C32821cn(A002, new byte[0], decode2, decode, new byte[0]);
                                C32761ch r12 = (C32761ch) r9.A05.A01.A00.get(new C32771ci(r03.A00, r03.A04));
                                if (r12 == null || !Arrays.equals(r12.A01, decode) || (bArr = r12.A02) == null) {
                                    throw new AnonymousClass2GD(101, "Key not found.");
                                }
                                AnonymousClass2F8 r13 = new AnonymousClass2F8(r142, A002, Base64.encodeToString(decode, 2), Base64.encodeToString(decode2, 2), Base64.encodeToString(bArr, 2), System.currentTimeMillis());
                                this.A06.A01(A0L, r13.A03);
                                this.A06.A01(A0M, r13.A01.getRawString());
                                str = "xpm/file-prefetcher/importEncryptionKey(); encryption key imported successfully";
                            } else {
                                throw new AnonymousClass2GB(301, "User changed while waiting for encryption key.");
                            }
                        } else if (r9.A01.A04 == 2) {
                            throw new AnonymousClass2GD(103, "Failed to fetch keys, timed out.");
                        } else {
                            throw new AnonymousClass2GD(102, "Not connected to server, cannot fetch keys.");
                        }
                    } catch (InterruptedException e) {
                        throw new AnonymousClass2GD("Failed to fetch keys, interrupted.", e);
                    }
                } else {
                    throw new AnonymousClass2GB(301, "Cannot fetch encryption key when user is not logged in.");
                }
            }
            Log.i(str);
            return;
        }
        throw new AnonymousClass2GB(301, "Cannot identify current logged in user.");
    }

    public void A0B(CancellationSignal cancellationSignal) {
        if (this.A06.A02(A0J)) {
            Log.i("xpm/file-prefetcher/importAllFiles(); file were already imported, skipping.");
            return;
        }
        byte[] bArr = new byte[A0F];
        while (this.A07.A00() > 0) {
            cancellationSignal.throwIfCanceled();
            A02(cancellationSignal, bArr);
        }
        this.A06.A01(A0J, Boolean.toString(true));
    }

    public void A0C(CancellationSignal cancellationSignal) {
        ZipEntry nextEntry;
        ZipEntry nextEntry2;
        Log.i("xpm/file-prefetcher/importEncryptionMetadata(); ");
        C15570nT r0 = this.A01;
        r0.A08();
        C27631Ih r02 = r0.A05;
        if (r02 != null) {
            String A00 = AnonymousClass2FC.A00(r02);
            C71103cO A02 = this.A03.A02();
            try {
                C16310on A01 = this.A07.A00.A01();
                AnonymousClass1Lx A002 = A01.A00();
                try {
                    int i = 0;
                    if (this.A06.A02(A0H)) {
                        Log.i("xpm/file-prefetcher/importEncryptionMetadata(); encryption metadata is already imported, skipping.");
                        A002.close();
                        A01.close();
                        A02.close();
                    } else if (A0J()) {
                        ParcelFileDescriptor A003 = this.A03.A00("migration/enc.zip");
                        FileInputStream fileInputStream = new FileInputStream(A003.getFileDescriptor());
                        ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
                        do {
                            nextEntry = zipInputStream.getNextEntry();
                            if (nextEntry == null) {
                                throw new FileNotFoundException("metadata.json was not found in zip file.");
                            }
                        } while (!"metadata.json".equals(nextEntry.getName()));
                        JsonReader jsonReader = new JsonReader(new InputStreamReader(zipInputStream));
                        try {
                            jsonReader.beginObject();
                            String str = null;
                            String str2 = null;
                            AnonymousClass2FG r13 = null;
                            String str3 = null;
                            while (jsonReader.hasNext()) {
                                String nextName = jsonReader.nextName();
                                if ("scheme".equals(nextName)) {
                                    str = jsonReader.nextString();
                                } else if ("data_id".equals(nextName)) {
                                    str2 = jsonReader.nextString();
                                } else if ("source_id".equals(nextName)) {
                                    str3 = jsonReader.nextString();
                                } else if ("key_id".equals(nextName)) {
                                    jsonReader.beginObject();
                                    Integer num = null;
                                    String str4 = null;
                                    String str5 = null;
                                    while (jsonReader.hasNext()) {
                                        String nextName2 = jsonReader.nextName();
                                        switch (nextName2.hashCode()) {
                                            case -758621230:
                                                if (!nextName2.equals("server_salt")) {
                                                    jsonReader.skipValue();
                                                    break;
                                                } else {
                                                    str5 = jsonReader.nextString();
                                                    break;
                                                }
                                            case 351608024:
                                                if (!nextName2.equals("version")) {
                                                    jsonReader.skipValue();
                                                    break;
                                                } else {
                                                    num = Integer.valueOf(jsonReader.nextInt());
                                                    break;
                                                }
                                            case 1091060704:
                                                if (!nextName2.equals("account_hash")) {
                                                    jsonReader.skipValue();
                                                    break;
                                                } else {
                                                    str4 = jsonReader.nextString();
                                                    break;
                                                }
                                            default:
                                                jsonReader.skipValue();
                                                break;
                                        }
                                    }
                                    jsonReader.endObject();
                                    if (num == null) {
                                        throw new IOException("Invalid key info: version is missing.");
                                    } else if (str4 == null) {
                                        throw new IOException("Invalid key info: account hash is missing.");
                                    } else if (str5 != null) {
                                        r13 = new AnonymousClass2FG(num.toString(), str4, str5);
                                    } else {
                                        throw new IOException("Invalid key info: server salt is missing.");
                                    }
                                } else {
                                    jsonReader.skipValue();
                                }
                            }
                            jsonReader.endObject();
                            if (str == null) {
                                throw new IOException("Invalid metadata file: scheme is missing.");
                            } else if (str2 == null) {
                                throw new IOException("Invalid metadata file: data id is missing.");
                            } else if (r13 != null) {
                                AnonymousClass2FC r10 = new AnonymousClass2FC(r13, str, str2, str3);
                                jsonReader.close();
                                zipInputStream.close();
                                String str6 = r10.A03;
                                if (str6 == null || str6.equals(A00)) {
                                    String str7 = r10.A02;
                                    if (str7.equals("AES-GCM-v1")) {
                                        this.A06.A01(A0Q, str7);
                                        this.A06.A01(A0K, r10.A01);
                                        this.A06.A01(A0R, str6);
                                        AnonymousClass1C1 r3 = this.A06;
                                        AnonymousClass2FG r5 = r10.A00;
                                        r3.A01(A0O, r5.A02);
                                        this.A06.A01(A0N, r5.A00);
                                        this.A06.A01(A0P, r5.A01);
                                        fileInputStream.close();
                                        A003.close();
                                        ParcelFileDescriptor A004 = this.A03.A00("migration/enc.zip");
                                        FileInputStream fileInputStream2 = new FileInputStream(A004.getFileDescriptor());
                                        C26071Bx r52 = this.A0A;
                                        ZipInputStream zipInputStream2 = new ZipInputStream(fileInputStream2);
                                        do {
                                            nextEntry2 = zipInputStream2.getNextEntry();
                                            if (nextEntry2 == null) {
                                                throw new FileNotFoundException("metadata.json was not found in zip file.");
                                            }
                                        } while (!"metadata.json".equals(nextEntry2.getName()));
                                        C619533g r7 = new C619533g(new JsonReader(new InputStreamReader(zipInputStream2)), r52.A00, zipInputStream2);
                                        while (r7.A02()) {
                                            cancellationSignal.throwIfCanceled();
                                            C90444Nx r9 = (C90444Nx) r7.A00();
                                            C26081By r2 = this.A07;
                                            ContentValues contentValues = new ContentValues();
                                            contentValues.put("remote_file_path", r9.A01);
                                            contentValues.put("enc_iv", r9.A00);
                                            C16310on A012 = r2.A00.A01();
                                            A012.A03.A02(contentValues, "encrypted_files");
                                            A012.close();
                                            i++;
                                        }
                                        r7.close();
                                        fileInputStream2.close();
                                        A004.close();
                                        this.A06.A01(A0H, Boolean.toString(true));
                                        A002.A00();
                                        A002.close();
                                        A01.close();
                                        A02.close();
                                        StringBuilder sb = new StringBuilder("xpm/file-prefetcher/importEncryptionMetadata(); imported ");
                                        sb.append(i);
                                        sb.append(" encrypted file metadata entries.");
                                        Log.i(sb.toString());
                                        return;
                                    }
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Unsupported: ");
                                    sb2.append(str7);
                                    throw new AnonymousClass2GB(100, sb2.toString());
                                }
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("Source id mismatch: current=");
                                sb3.append(str6);
                                sb3.append(", expected=");
                                sb3.append(A00);
                                throw new AnonymousClass2GB(106, sb3.toString());
                            } else {
                                throw new IOException("Invalid metadata file: key info is missing.");
                            }
                        } catch (Throwable th) {
                            try {
                                jsonReader.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } else {
                        throw new AnonymousClass2GB(200, "Missing encryption metadata: migration/enc.zip");
                    }
                } catch (Throwable th2) {
                    try {
                        A002.close();
                    } catch (Throwable unused2) {
                    }
                    throw th2;
                }
            } catch (Throwable th3) {
                try {
                    A02.close();
                } catch (Throwable unused3) {
                }
                throw th3;
            }
        } else {
            throw new AnonymousClass2GB(301, "Cannot identify current logged in user.");
        }
    }

    public void A0D(CancellationSignal cancellationSignal) {
        Log.i("xpm/file-prefetcher/importFileList(); ");
        C16310on A01 = this.A07.A00.A01();
        try {
            AnonymousClass1Lx A00 = A01.A00();
            if (this.A06.A02(A0I)) {
                Log.i("xpm/file-prefetcher/importFileList(); file list was already imported, skipping.");
                A00.close();
                A01.close();
                return;
            }
            C26011Br r5 = this.A03;
            try {
                AnonymousClass351 A012 = r5.A01();
                try {
                    ParcelFileDescriptor AD1 = ((IAppDataReaderService) A012.A00()).AD1();
                    C619433f r52 = new C619433f(AD1, new JsonReader(new BufferedReader(new FileReader(AD1.getFileDescriptor()))), r5.A06.A00.A01.A3V());
                    A012.close();
                    int i = 0;
                    while (r52.A02()) {
                        cancellationSignal.throwIfCanceled();
                        AnonymousClass3F4 r1 = (AnonymousClass3F4) r52.A00();
                        C26081By r2 = this.A07;
                        ContentValues contentValues = new ContentValues();
                        String str = r1.A01;
                        contentValues.put("remote_file_path", str);
                        contentValues.put("file_size", Long.valueOf(r1.A00));
                        contentValues.putNull("prefetched_file_path");
                        contentValues.put("required", (Integer) 0);
                        contentValues.put("prefetched", (Integer) 0);
                        C16310on A013 = r2.A00.A01();
                        long A02 = A013.A03.A02(contentValues, "prefetched_files");
                        A013.close();
                        if (A02 < 0) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("xpm/file-prefetcher/importFileList(); failed to import metadata for ");
                            sb.append(str);
                            Log.e(sb.toString());
                            this.A00.AaV("xpm-file-prefetcher-cannot-add-metadata", str, false);
                        } else {
                            i++;
                        }
                    }
                    r52.close();
                    this.A06.A01(A0I, Boolean.toString(true));
                    A00.A00();
                    A00.close();
                    A01.close();
                    StringBuilder sb2 = new StringBuilder("xpm/file-prefetcher/importFileList(); imported ");
                    sb2.append(i);
                    sb2.append(" entries.");
                    Log.i(sb2.toString());
                } catch (Throwable th) {
                    try {
                        A012.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            } catch (Exception e) {
                throw new IOException(e);
            }
        } catch (Throwable th2) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th2;
        }
    }

    public void A0E(CancellationSignal cancellationSignal) {
        if (!this.A0D.getAndSet(true)) {
            try {
                A0F(cancellationSignal);
            } finally {
                this.A0D.set(false);
            }
        } else {
            Log.e("xpm/file-prefetcher/prefetchAllFiles()concurrent prefetch requested, not supported");
            throw new IllegalStateException("Multiple concurrent operations are not supported.");
        }
    }

    public void A0F(CancellationSignal cancellationSignal) {
        Log.i("xpm/file-prefetcher/prefetchAllFilesLocked()");
        if (this.A06.A02(A0G)) {
            Log.i("xpm/file-prefetcher/prefetchAllFilesLocked() already marked as completed, nothing to do.");
            return;
        }
        try {
            C71103cO A02 = this.A03.A02();
            if (A0J()) {
                A0C(cancellationSignal);
                A0A(cancellationSignal);
            }
            A0D(cancellationSignal);
            A01();
            A0B(cancellationSignal);
            this.A0C.get();
            this.A03.A03();
            this.A06.A01(A0G, Boolean.toString(true));
            A02.close();
        } catch (OperationCanceledException e) {
            Log.w("xpm/file-prefetcher/prefetchAllFilesLocked()cancelled");
            throw e;
        }
    }

    public void A0G(CancellationSignal cancellationSignal, File file, String str, byte[] bArr) {
        this.A0B.A03(cancellationSignal, A06(str), file, bArr);
    }

    public void A0H(CancellationSignal cancellationSignal, String str, String str2, byte[] bArr) {
        File A05 = A05(str);
        File parentFile = A05.getParentFile();
        if (parentFile != null) {
            parentFile.mkdirs();
        }
        try {
            ParcelFileDescriptor A00 = this.A03.A00(str);
            FileInputStream fileInputStream = new FileInputStream(A00.getFileDescriptor());
            FileOutputStream fileOutputStream = new FileOutputStream(A05);
            OutputStream A002 = A00(fileOutputStream, str, str2);
            C15740np.A01(cancellationSignal, fileInputStream, A002, bArr);
            if (A002 != null) {
                A002.close();
            }
            fileOutputStream.close();
            fileInputStream.close();
            A00.close();
            C26081By r0 = this.A07;
            String canonicalPath = A05.getCanonicalPath();
            C16310on A01 = r0.A00.A01();
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("prefetched", (Integer) 1);
                contentValues.put("prefetched_file_path", canonicalPath);
                A01.A03.A00("prefetched_files", contentValues, "remote_file_path = ?", new String[]{str});
                A01.close();
                this.A0C.get();
                if (!str.equals("migration/metadata.json")) {
                    try {
                        AnonymousClass351 A012 = this.A03.A01();
                        C67603Sd r1 = (C67603Sd) ((IAppDataReaderService) A012.A00());
                        Parcel obtain = Parcel.obtain();
                        Parcel obtain2 = Parcel.obtain();
                        try {
                            obtain.writeInterfaceToken("com.google.android.apps.pixelmigrate.migrate.ios.appdatareader.IAppDataReaderService");
                            obtain.writeString(str);
                            r1.A00.transact(3, obtain, obtain2, 0);
                            obtain2.readException();
                            A012.close();
                        } finally {
                            obtain2.recycle();
                            obtain.recycle();
                        }
                    } catch (Exception e) {
                        throw new IOException(str, e);
                    }
                }
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (OperationCanceledException e2) {
            A05.delete();
            StringBuilder sb = new StringBuilder("xpm/file-prefetcher/importFile(); cancelled while importing ");
            sb.append(str);
            Log.i(sb.toString());
            throw e2;
        }
    }

    public void A0I(String str) {
        C16310on A01 = this.A07.A00.A01();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("required", (Integer) 1);
            int A00 = A01.A03.A00("prefetched_files", contentValues, "remote_file_path = ?", new String[]{str});
            A01.close();
            if (A00 <= 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" is not present in remote sandbox.");
                throw new AnonymousClass2GB(200, sb.toString());
            }
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A0J() {
        try {
            ParcelFileDescriptor A00 = this.A03.A00("migration/enc.zip");
            if (A00 != null) {
                A00.close();
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
