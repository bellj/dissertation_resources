package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.19Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19Q {
    public String A00;
    public final C15570nT A01;
    public final C22700zV A02;
    public final C14850m9 A03;
    public final C16120oU A04;
    public final AnonymousClass00E A05;
    public final HashSet A06 = new HashSet(Arrays.asList(31, 28, 30, 29, 32, 37, 33, 12, 4, 27, 35, 36, 34));
    public final AtomicInteger A07 = new AtomicInteger();
    public final AtomicInteger A08 = new AtomicInteger();

    public AnonymousClass19Q(C15570nT r6, C22700zV r7, C14850m9 r8, C16120oU r9) {
        this.A03 = r8;
        this.A01 = r6;
        this.A04 = r9;
        this.A02 = r7;
        this.A05 = AbstractC16110oT.DEFAULT_SAMPLING_RATE;
    }

    public void A00(int i) {
        this.A00 = UUID.randomUUID().toString();
        this.A08.set(1);
        this.A07.set(i);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v5, resolved type: X.31Q */
    /* JADX DEBUG: Multi-variable search result rejected for r1v7, resolved type: X.31H */
    /* JADX WARN: Multi-variable type inference failed */
    public final void A01(UserJid userJid, Boolean bool, Boolean bool2, Integer num, Integer num2, Long l, String str, String str2, String str3, String str4, int i) {
        AnonymousClass31O r1;
        AnonymousClass00E r12 = this.A05;
        boolean A01 = r12.A01(this.A00);
        int i2 = r12.A03 * 1;
        boolean A0F = this.A01.A0F(userJid);
        HashSet hashSet = this.A06;
        Integer valueOf = Integer.valueOf(i);
        boolean contains = hashSet.contains(valueOf);
        if (A0F) {
            if (contains || A01) {
                AnonymousClass31H r13 = new AnonymousClass31H();
                r13.A02 = num;
                r13.A06 = this.A00;
                r13.A09 = str;
                r13.A04 = num2;
                r13.A05 = l;
                r13.A08 = str2;
                r13.A01 = bool;
                r13.A00 = bool2;
                r13.A07 = str3;
                r13.A0A = str4;
                int i3 = this.A07.get();
                r1 = r13;
                if (i3 != 0) {
                    r13.A03 = Integer.valueOf(i3);
                    r1 = r13;
                }
            } else {
                return;
            }
        } else if (!contains && !A01) {
            return;
        } else {
            if (this.A03.A07(904)) {
                AnonymousClass31Q r14 = new AnonymousClass31Q();
                r14.A08 = Long.valueOf((long) this.A08.getAndIncrement());
                r14.A05 = valueOf;
                r14.A0B = this.A00;
                r14.A0E = str;
                r14.A06 = num2;
                r14.A09 = userJid.getRawString();
                r14.A07 = l;
                r14.A0D = str2;
                r14.A02 = bool;
                r14.A00 = bool2;
                r14.A0C = str3;
                r14.A0F = str4;
                int i4 = this.A07.get();
                if (i4 != 0) {
                    r14.A04 = Integer.valueOf(i4);
                }
                if (!A01) {
                    r14.A01 = Boolean.TRUE;
                }
                r14.A03 = Integer.valueOf(C65003Ht.A00(this.A02.A00(userJid)));
                r1 = r14;
            } else {
                AnonymousClass31O r15 = new AnonymousClass31O();
                r15.A05 = valueOf;
                r15.A0A = this.A00;
                r15.A0D = str;
                r15.A06 = num2;
                r15.A08 = userJid.getRawString();
                r15.A07 = l;
                r15.A0C = str2;
                r15.A02 = bool;
                r15.A00 = bool2;
                r15.A0B = str3;
                r15.A0E = str4;
                int i5 = this.A07.get();
                if (i5 != 0) {
                    r15.A04 = Integer.valueOf(i5);
                }
                if (!A01) {
                    r15.A01 = Boolean.TRUE;
                }
                r15.A03 = Integer.valueOf(C65003Ht.A00(this.A02.A00(userJid)));
                r15.A0D = null;
                r15.A08 = null;
                r15.A0C = null;
                r1 = r15;
            }
        }
        if (hashSet.contains(valueOf)) {
            i2 = 1;
        }
        this.A04.A08(r1, i2);
    }

    public void A02(UserJid userJid, Integer num, Long l, String str, int i) {
        A01(userJid, null, null, num, null, l, str, null, null, null, i);
    }

    public void A03(UserJid userJid, Integer num, String str, int i) {
        A02(userJid, num, null, str, i);
    }
}
