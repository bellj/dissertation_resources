package X;

import android.view.View;
import com.whatsapp.voipcalling.VoipCallControlBottomSheetV2;

/* renamed from: X.3NE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NE implements View.OnTouchListener {
    public float A00;
    public float A01;
    public final /* synthetic */ VoipCallControlBottomSheetV2 A02;

    public AnonymousClass3NE(VoipCallControlBottomSheetV2 voipCallControlBottomSheetV2) {
        this.A02 = voipCallControlBottomSheetV2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x009e, code lost:
        if (r6.A02(r5) != false) goto L_0x00a0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048  */
    @Override // android.view.View.OnTouchListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r10, android.view.MotionEvent r11) {
        /*
            r9 = this;
            com.whatsapp.voipcalling.VoipCallControlBottomSheetV2 r3 = r9.A02
            X.3Fw r0 = r3.A0F
            r2 = 1
            if (r0 == 0) goto L_0x00a0
            boolean r6 = r0.A08()
            X.2CT r0 = r3.A0W
            if (r0 == 0) goto L_0x00c9
            com.whatsapp.voipcalling.VoipActivityV2 r1 = r0.A00
            boolean r0 = r1.A1m
            if (r0 != 0) goto L_0x0025
            com.whatsapp.contact.picker.ContactPickerFragment r0 = r1.A18
            if (r0 != 0) goto L_0x0025
            com.whatsapp.calling.callgrid.viewmodel.CallGridViewModel r0 = r1.A0p
            if (r0 == 0) goto L_0x00c9
            X.016 r0 = r0.A07
            java.lang.Object r0 = r0.A01()
            if (r0 == 0) goto L_0x00c9
        L_0x0025:
            r5 = 1
        L_0x0026:
            float r1 = r11.getX()
            float r4 = r11.getY()
            X.00k r0 = r3.A0B()
            if (r0 == 0) goto L_0x0046
            if (r6 != 0) goto L_0x0046
            int r0 = com.whatsapp.voipcalling.VoipCallControlBottomSheetV2.A00(r3)
            float r0 = (float) r0
            float r0 = r0 + r4
            r11.setLocation(r1, r0)
            X.00k r0 = r3.A0B()
            r0.dispatchTouchEvent(r11)
        L_0x0046:
            if (r5 != 0) goto L_0x00a0
            int r0 = r11.getAction()
            if (r0 != 0) goto L_0x0052
            r9.A00 = r1
            r9.A01 = r4
        L_0x0052:
            com.whatsapp.calling.views.VoipCallControlBottomSheetDragIndicator r0 = r3.A0L
            if (r0 == 0) goto L_0x00a1
            int r0 = r11.getAction()
            if (r0 == r2) goto L_0x0062
            int r0 = r11.getAction()
            if (r0 != 0) goto L_0x00a1
        L_0x0062:
            com.whatsapp.calling.views.VoipCallControlBottomSheetDragIndicator r6 = r3.A0L
            android.view.View r0 = r3.A06
            int r0 = r0.getTop()
            float r7 = (float) r0
            float r7 = r7 - r4
            int r5 = r11.getAction()
            int r0 = r6.A02
            if (r0 != 0) goto L_0x0081
            android.content.res.Resources r8 = r6.getResources()
            r0 = 2131165419(0x7f0700eb, float:1.7945055E38)
            int r0 = r8.getDimensionPixelSize(r0)
            r6.A02 = r0
        L_0x0081:
            int r0 = r6.getLeft()
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 < 0) goto L_0x00a1
            int r0 = r6.getRight()
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x00a1
            int r0 = r6.A02
            float r0 = (float) r0
            int r0 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x00a1
            boolean r0 = r6.A02(r5)
            if (r0 == 0) goto L_0x00a1
        L_0x00a0:
            return r2
        L_0x00a1:
            int r0 = r11.getAction()
            if (r0 != r2) goto L_0x00a0
            float r0 = r9.A00
            float r1 = r1 - r0
            float r0 = java.lang.Math.abs(r1)
            float r1 = r3.A02
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x00a0
            float r0 = r9.A01
            float r4 = r4 - r0
            float r0 = java.lang.Math.abs(r4)
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x00a0
            com.whatsapp.calling.controls.viewmodel.BottomSheetViewModel r0 = r3.A0H
            boolean r0 = r0.A01
            if (r0 != 0) goto L_0x00a0
            r3.A1M()
            return r2
        L_0x00c9:
            r5 = 0
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3NE.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
