package X;

import android.widget.ImageView;
import com.whatsapp.util.Log;

/* renamed from: X.6Db  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134036Db implements AnonymousClass5WK {
    public final /* synthetic */ ImageView A00;
    public final /* synthetic */ AbstractC28901Pl A01;
    public final /* synthetic */ C129945yY A02;
    public final /* synthetic */ boolean A03;

    @Override // X.AnonymousClass5WK
    public void AXT() {
    }

    public C134036Db(ImageView imageView, AbstractC28901Pl r2, C129945yY r3, boolean z) {
        this.A02 = r3;
        this.A03 = z;
        this.A01 = r2;
        this.A00 = imageView;
    }

    @Override // X.AnonymousClass5WK
    public void ARw() {
        StringBuilder A0k = C12960it.A0k("PAY: Failed to display card art, empty image shown. Re-fetch ");
        boolean z = this.A03;
        A0k.append(z);
        Log.w(A0k.toString());
        if (z) {
            this.A02.A01(this.A00, this.A01);
        }
    }
}
