package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.payments.IDxRCallbackShape0S0200000_3_I1;
import com.whatsapp.payments.limitation.NoviPayLimitationsBloksActivity;
import com.whatsapp.payments.ui.NoviPayBloksActivity;
import com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity;
import com.whatsapp.payments.ui.NoviServiceSelectionBottomSheet;
import com.whatsapp.payments.ui.stepup.NoviPayStepUpBloksActivity;
import com.whatsapp.util.Log;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.61a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1310561a {
    public static int[] A00;

    static {
        int[] iArr = new int[7];
        iArr[0] = R.string.novi_camera_permission_need_both;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.novi_camera_permission_need_both_previous_decline_v30;
        if (i < 30) {
            i2 = R.string.novi_camera_permission_need_both_previous_decline;
        }
        iArr[1] = i2;
        iArr[2] = R.string.novi_camera_permission_need_storage;
        int i3 = R.string.novi_camera_permission_need_storage_previous_decline_v30;
        if (i < 30) {
            i3 = R.string.novi_camera_permission_need_storage_previous_decline;
        }
        iArr[3] = i3;
        iArr[4] = R.string.novi_camera_permission_need_camera;
        iArr[5] = R.string.novi_camera_permission_need_camera_previous_decline;
        iArr[6] = R.string.novi_camera_permission_granted_before;
        A00 = iArr;
    }

    public static double A00(double d) {
        double ceil;
        double pow = Math.pow(10.0d, (double) 2);
        double d2 = d * pow;
        if (d2 >= 0.0d) {
            ceil = Math.floor(d2);
        } else {
            ceil = Math.ceil(d2);
        }
        return ceil / pow;
    }

    public static String A01(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (str.equals("novi.wallet_core.rc_stable")) {
                return "18055553720@s.whatsapp.net";
            }
            if (str.equals("novi.wallet_core.rc")) {
                return "12015553742@s.whatsapp.net";
            }
        }
        return "18556501554@s.whatsapp.net";
    }

    public static Locale A02(C16590pI r1) {
        AnonymousClass06D r12 = C04100Kj.A00(C12980iv.A0H(r1.A00)).A00;
        AnonymousClass009.A0E(C12960it.A1U(r12.size()));
        return r12.AAS(0);
    }

    public static void A03(Activity activity, C15890o4 r5, C130105yo r6) {
        Intent A0A = RequestPermissionActivity.A0A(activity, r5, A00, 30, true);
        if (A0A == null) {
            if (!C12980iv.A1W(r6.A02(), "wavi_seen_camera_permission_education")) {
                A0A = C12990iw.A0D(activity, NoviPayBloksActivity.class);
                A0A.putExtra("screen_name", "novipay_p_camera_permission_granted_before_interstitial");
            } else {
                return;
            }
        }
        activity.startActivityForResult(A0A, 30);
    }

    public static void A04(Context context, C14900mE r10, C18650sn r11, C18610sj r12, AbstractC136196Lo r13, C130105yo r14) {
        String string = r14.A02().getString("resource_encryption_key", "");
        if (!string.isEmpty()) {
            try {
                JSONObject A05 = C13000ix.A05(string);
                C130785zy r0 = new C130785zy(null, new C37891nB(Base64.decode(A05.getString("key"), 0), Base64.decode(A05.getString("mac"), 0), Base64.decode(A05.getString("iv"), 0)));
                r0.A01 = null;
                r13.AV8(r0);
                return;
            } catch (JSONException unused) {
                Log.e("PAY: NoviSharedPreferences/retrieveResourceEncryptionKey failed retrieve resource key");
            }
        }
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[2];
        C12960it.A1M("action", "novi-get-resource-decryption-key", r3, 0);
        C12960it.A1M("client_request_id", C12990iw.A0n(), r3, 1);
        r12.A0F(new IDxRCallbackShape0S0200000_3_I1(context, r10, r11, r14, r13, 17), C117315Zl.A0G(r3), "get", C125195qr.A00);
    }

    public static void A05(Context context, String str) {
        Intent A04 = C117295Zj.A04(str);
        if (context.getPackageManager().queryIntentActivities(A04, 0).size() > 0) {
            context.startActivity(A04);
            return;
        }
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage("com.novi.wallet");
        if (launchIntentForPackage != null) {
            context.startActivity(launchIntentForPackage);
        }
    }

    public static void A06(ActivityC13790kL r5, C126025sD r6) {
        Intent intent;
        Class cls = NoviPayBloksActivity.class;
        String str = r6.A00;
        switch (str.hashCode()) {
            case 852389990:
                if (str.equals("limitationInterstitial")) {
                    intent = C12990iw.A0D(r5, NoviPayLimitationsBloksActivity.class);
                    intent.putExtra("limitation_origin", 3);
                    break;
                } else {
                    return;
                }
            case 1069198737:
                if (!str.equals("createAccount")) {
                    return;
                }
                intent = C12990iw.A0D(r5, cls);
                break;
            case 1182488422:
                if (str.equals("addPaymentMethod")) {
                    intent = C12990iw.A0D(r5, NoviPayHubAddPaymentMethodActivity.class);
                    intent.putExtra("extra_funding_category", "balance_top_up");
                    break;
                } else {
                    return;
                }
            case 1299163445:
                if (str.equals("loginScreen")) {
                    intent = C12990iw.A0D(r5, cls);
                    intent.putExtra("screen_name", "novipay_p_login_password");
                    intent.putExtra("login_entry_point", 5);
                    break;
                } else {
                    return;
                }
            case 1622260550:
                if (str.equals("servicesDialog")) {
                    r5.Adl(new NoviServiceSelectionBottomSheet(), str);
                    return;
                }
                return;
            case 1629551059:
                if (str.equals("stepUpScreen")) {
                    cls = NoviPayStepUpBloksActivity.class;
                    intent = C12990iw.A0D(r5, cls);
                    break;
                } else {
                    return;
                }
            default:
                return;
        }
        r5.startActivity(intent);
    }
}
