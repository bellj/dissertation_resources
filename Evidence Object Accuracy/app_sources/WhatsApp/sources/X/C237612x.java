package X;

import android.content.Context;
import android.os.Build;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import com.whatsapp.jid.UserJid;

/* renamed from: X.12x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C237612x {
    public final AnonymousClass2OH A00;

    public C237612x(AnonymousClass01d r3, C16590pI r4, C14850m9 r5) {
        AnonymousClass2OH r0;
        if (Build.VERSION.SDK_INT >= 28) {
            r0 = new AnonymousClass2OH(r3, r4, r5);
        } else {
            r0 = null;
        }
        this.A00 = r0;
    }

    public int A00() {
        AnonymousClass2OH A04 = A04();
        A04.A02();
        return A04.A05.size();
    }

    public int A01() {
        AnonymousClass2OH r0;
        if (Build.VERSION.SDK_INT < 28 || (r0 = this.A00) == null) {
            return 0;
        }
        return r0.A05();
    }

    public Connection A02(ConnectionRequest connectionRequest, boolean z) {
        return A04().A06(connectionRequest, z);
    }

    public C52142aJ A03(String str) {
        return A04().A07(str);
    }

    public final AnonymousClass2OH A04() {
        if (Build.VERSION.SDK_INT >= 28) {
            AnonymousClass2OH r0 = this.A00;
            AnonymousClass009.A05(r0);
            return r0;
        }
        throw new RuntimeException("Requires API level 28");
    }

    public void A05() {
        A04().A08();
    }

    public void A06(ConnectionRequest connectionRequest) {
        A04().A0A(connectionRequest);
    }

    public void A07(ConnectionRequest connectionRequest) {
        A04().A0B(connectionRequest);
    }

    public void A08(AnonymousClass2ON r2) {
        A04().A03(r2);
    }

    public void A09(AnonymousClass2ON r2) {
        A04().A04(r2);
    }

    public void A0A(String str) {
        A04().A0E(str);
    }

    public void A0B(String str, String str2) {
        A04().A0G(str, str2);
    }

    public boolean A0C() {
        AnonymousClass2OH A04 = A04();
        A04.A06 = A04.A04.A07(1641);
        return A04.A06;
    }

    public boolean A0D() {
        AnonymousClass2OH A04 = A04();
        boolean A07 = A04.A04.A07(1642);
        A04.A01 = A07;
        return A07;
    }

    public boolean A0E(Context context, UserJid userJid) {
        return A04().A0H(context, userJid);
    }

    public boolean A0F(UserJid userJid, String str, String str2, boolean z) {
        return A04().A0I(userJid, str, str2, z);
    }

    public boolean A0G(UserJid userJid, String str, String str2, boolean z, boolean z2) {
        return A04().A0J(userJid, str, str2, z, z2);
    }
}
