package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.biz.catalog.view.CatalogCarouselDetailImageView;
import com.whatsapp.components.button.ThumbnailButton;
import java.util.Map;

/* renamed from: X.380  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass380 extends AbstractC16350or {
    public int A00;
    public Bitmap A01;
    public View A02;
    public ThumbnailButton A03;
    public String A04;
    public Map A05;
    public final /* synthetic */ CatalogCarouselDetailImageView A06;

    public AnonymousClass380(Bitmap bitmap, View view, CatalogCarouselDetailImageView catalogCarouselDetailImageView, ThumbnailButton thumbnailButton, String str, Map map, int i) {
        this.A06 = catalogCarouselDetailImageView;
        this.A02 = view;
        this.A03 = thumbnailButton;
        this.A01 = bitmap;
        this.A00 = i;
        this.A05 = map;
        this.A04 = str;
    }
}
