package X;

/* renamed from: X.4HO  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4HO {
    public static final String[] A00 = {"Camera:MicroVideoOffset", "GCamera:MicroVideoOffset"};
    public static final String[] A01 = {"Camera:MotionPhotoPresentationTimestampUs", "GCamera:MotionPhotoPresentationTimestampUs", "Camera:MicroVideoPresentationTimestampUs", "GCamera:MicroVideoPresentationTimestampUs"};
    public static final String[] A02 = {"Camera:MotionPhoto", "GCamera:MotionPhoto", "Camera:MicroVideo", "GCamera:MicroVideo"};
}
