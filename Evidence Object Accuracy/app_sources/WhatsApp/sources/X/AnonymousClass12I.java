package X;

import android.content.Context;
import android.text.format.Formatter;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.12I  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12I {
    public CopyOnWriteArrayList A00 = new CopyOnWriteArrayList();

    public void A00(int i) {
        Iterator it = this.A00.iterator();
        while (it.hasNext()) {
            C27241Go r6 = (C27241Go) it.next();
            if (i == 0) {
                Context context = r6.A00;
                C29671Ue.A00(context, r6.A01, context.getString(R.string.error_report_db_or_disk_is_full, Formatter.formatFileSize(context, r6.A02.A02())));
            } else if (i == 1) {
                long A02 = r6.A02.A02();
                if (A02 < 10485760) {
                    Context context2 = r6.A00;
                    String formatFileSize = Formatter.formatFileSize(context2, A02);
                    StringBuilder sb = new StringBuilder();
                    sb.append("errorreporter/diskio/diskspace ");
                    sb.append(formatFileSize);
                    Log.i(sb.toString());
                    if (formatFileSize != null) {
                        C18360sK r3 = r6.A01;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(context2.getString(R.string.error_msgstore_db_diskio));
                        sb2.append(" ");
                        sb2.append(context2.getString(R.string.error_possible_cause_is_low_disk_space, formatFileSize));
                        C29671Ue.A00(context2, r3, sb2.toString());
                    }
                }
                Context context3 = r6.A00;
                C29671Ue.A00(context3, r6.A01, context3.getString(R.string.error_msgstore_db_diskio));
            } else if (i == 2) {
                Context context4 = r6.A00;
                C29671Ue.A00(context4, r6.A01, context4.getString(R.string.msg_store_lost_due_to_previous_error));
            } else if (i == 3) {
                Context context5 = r6.A00;
                C29671Ue.A00(context5, r6.A01, context5.getString(R.string.error_unable_to_open_msgstoredb));
            } else if (i == 4) {
                Context context6 = r6.A00;
                C29671Ue.A00(context6, r6.A01, context6.getString(R.string.error_unable_to_update_readonly_msgstoredb));
            } else {
                continue;
            }
        }
    }
}
