package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3f4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72723f4 extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass2CF A00;

    public C72723f4(AnonymousClass2CF r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2CF r1 = this.A00;
        r1.setVisibility(4);
        r1.setAlpha(1.0f);
        r1.A05();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        super.onAnimationStart(animator);
        this.A00.A0d = false;
    }
}
