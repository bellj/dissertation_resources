package X;

/* renamed from: X.3mF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76793mF extends AnonymousClass4XC {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C76793mF(X.C95424dg r17, int r18, long r19, long r21) {
        /*
            r16 = this;
            r2 = r17
            X.4w4 r3 = new X.4w4
            r3.<init>()
            r0 = r18
            X.4w7 r4 = new X.4w7
            r4.<init>(r2, r0)
            long r6 = r2.A02()
            long r8 = r2.A09
            int r0 = r2.A04
            if (r0 <= 0) goto L_0x0034
            long r14 = (long) r0
            int r0 = r2.A06
            long r0 = (long) r0
            long r14 = r14 + r0
            r0 = 2
            long r14 = r14 / r0
            r0 = 1
        L_0x0022:
            long r14 = r14 + r0
            int r1 = r2.A06
            r0 = 6
            int r5 = java.lang.Math.max(r0, r1)
            r2 = r16
            r12 = r21
            r10 = r19
            r2.<init>(r3, r4, r5, r6, r8, r10, r12, r14)
            return
        L_0x0034:
            int r1 = r2.A05
            int r0 = r2.A03
            if (r1 != r0) goto L_0x004b
            if (r1 <= 0) goto L_0x004b
            long r14 = (long) r1
        L_0x003d:
            int r0 = r2.A02
            long r0 = (long) r0
            long r14 = r14 * r0
            int r0 = r2.A00
            long r0 = (long) r0
            long r14 = r14 * r0
            r0 = 8
            long r14 = r14 / r0
            r0 = 64
            goto L_0x0022
        L_0x004b:
            r14 = 4096(0x1000, double:2.0237E-320)
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C76793mF.<init>(X.4dg, int, long, long):void");
    }
}
