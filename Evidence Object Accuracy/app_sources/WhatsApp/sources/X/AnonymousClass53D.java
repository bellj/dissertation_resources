package X;

import com.whatsapp.backup.google.viewmodel.SettingsGoogleDriveViewModel;

/* renamed from: X.53D  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass53D implements AnonymousClass10M {
    public final C25721Am A00;
    public final SettingsGoogleDriveViewModel A01;

    public AnonymousClass53D(C25721Am r1, SettingsGoogleDriveViewModel settingsGoogleDriveViewModel) {
        this.A00 = r1;
        this.A01 = settingsGoogleDriveViewModel;
    }

    @Override // X.AnonymousClass10M
    public void AMu() {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A01;
        settingsGoogleDriveViewModel.A0d.A0A(new C84203ya());
    }

    @Override // X.AnonymousClass10M
    public void AMv() {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A01;
        settingsGoogleDriveViewModel.A0d.A0A(new C84213yb());
    }

    @Override // X.AnonymousClass10M
    public void AMw(long j, long j2) {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A01;
        settingsGoogleDriveViewModel.A0d.A0A(new C84253yf(j, j2));
    }

    @Override // X.AnonymousClass10M
    public void AMx() {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A01;
        settingsGoogleDriveViewModel.A0d.A0A(new C84223yc());
    }

    @Override // X.AnonymousClass10M
    public void AMy(boolean z) {
        SettingsGoogleDriveViewModel settingsGoogleDriveViewModel = this.A01;
        settingsGoogleDriveViewModel.A0d.A0A(new C84243ye(z));
    }
}
