package X;

import java.util.concurrent.Callable;

/* renamed from: X.6KN  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6KN implements Callable {
    public final /* synthetic */ C119035cm A00;

    public AnonymousClass6KN(C119035cm r1) {
        this.A00 = r1;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C119035cm r4 = this.A00;
        C1308060a r3 = r4.A02;
        r3.A02();
        C130185yw r2 = r3.A01;
        if (r2 != null) {
            r2.A05(r4.A00, r4.A03);
        }
        AnonymousClass61Q r22 = r3.A02;
        if (r22 == null) {
            return null;
        }
        r22.A0F(r4.A04, true);
        return null;
    }
}
