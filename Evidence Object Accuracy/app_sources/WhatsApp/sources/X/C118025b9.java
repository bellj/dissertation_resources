package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.5b9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118025b9 extends AnonymousClass015 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final C14830m7 A02;
    public final C17070qD A03;
    public final AnonymousClass17Z A04;
    public final AbstractC14440lR A05;

    public C118025b9(C14830m7 r2, C17070qD r3, AnonymousClass17Z r4, AbstractC14440lR r5) {
        this.A02 = r2;
        this.A05 = r5;
        this.A03 = r3;
        this.A04 = r4;
    }

    public final boolean A04(AbstractC38191ng r6, AnonymousClass2S0 r7) {
        if (r6 == null) {
            return false;
        }
        int A00 = r7.A00(TimeUnit.MILLISECONDS.toSeconds(this.A02.A00()));
        C14850m9 r2 = r6.A07;
        if (!C117305Zk.A1U(r2) || A00 != 1) {
            return false;
        }
        C50942Ry r1 = r7.A01;
        C50932Rx r3 = r7.A02;
        if (r1 == null || r3 == null || !C117305Zk.A1U(r2) || r1.A03 <= r3.A01 + r3.A00 || !r3.A04) {
            return false;
        }
        return true;
    }
}
