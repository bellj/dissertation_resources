package X;

import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.5bN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118165bN extends AnonymousClass015 implements AnonymousClass6Ln {
    public C1308460e A00;
    public final AnonymousClass016 A01 = C12980iv.A0T();
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03 = C12980iv.A0T();
    public final AnonymousClass69E A04;

    public C118165bN(AnonymousClass69E r2) {
        this.A04 = r2;
    }

    @Override // X.AnonymousClass6Ln
    public void AVN(UserJid userJid, AnonymousClass1ZR r12, AnonymousClass1ZR r13, AnonymousClass1ZR r14, C452120p r15, String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        AnonymousClass60V A04;
        int i;
        this.A01.A0B(Boolean.FALSE);
        C128195vi r1 = new C128195vi(userJid, r12, r13, r14, str, str2, z5, z2);
        if (z) {
            if (r15 == null) {
                r1.A06 = C12960it.A1S(z3 ? 1 : 0);
                this.A02.A0B(r1);
                return;
            }
        } else if (r15 == null) {
            i = R.string.payment_invalid_vpa_error_text;
            if (z4) {
                i = R.string.payment_invalid_upi_number_error_text;
            }
            A04 = new AnonymousClass60V(i);
            this.A03.A0B(A04);
        }
        A04 = this.A04.A04(this.A00.A04, r15.A00);
        if (A04.A01 == null) {
            i = R.string.payments_generic_error;
            A04 = new AnonymousClass60V(i);
        }
        this.A03.A0B(A04);
    }
}
