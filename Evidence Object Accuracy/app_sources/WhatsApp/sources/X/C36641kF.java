package X;

import com.whatsapp.countrygating.viewmodel.CountryGatingViewModel;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.tosgating.viewmodel.ToSGatingViewModel;

/* renamed from: X.1kF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36641kF extends AnonymousClass015 {
    public C15370n3 A00;
    public AbstractC14640lm A01;
    public final AnonymousClass016 A02 = new AnonymousClass016();
    public final AnonymousClass2Dn A03;
    public final C22330yu A04;
    public final C22640zP A05;
    public final C15550nR A06;
    public final C27131Gd A07;
    public final AnonymousClass10S A08;
    public final C14310lE A09;
    public final CountryGatingViewModel A0A;
    public final C19990v2 A0B;
    public final C20830wO A0C;
    public final C15600nX A0D;
    public final C14850m9 A0E;
    public final C20710wC A0F;
    public final AnonymousClass5UJ A0G;
    public final AnonymousClass11A A0H;
    public final AbstractC33331dp A0I;
    public final C244215l A0J;
    public final AnonymousClass1E5 A0K;
    public final ToSGatingViewModel A0L;
    public final ExecutorC27271Gr A0M;
    public final C27691It A0N = new C27691It();
    public final C27691It A0O = new C27691It();
    public final C27691It A0P = new C27691It();

    public C36641kF(C22330yu r11, C22640zP r12, C15550nR r13, AnonymousClass10S r14, C14310lE r15, CountryGatingViewModel countryGatingViewModel, C19990v2 r17, C20830wO r18, C15600nX r19, C15370n3 r20, C14850m9 r21, C20710wC r22, AnonymousClass11A r23, C244215l r24, AnonymousClass1E5 r25, AbstractC14640lm r26, ToSGatingViewModel toSGatingViewModel, AnonymousClass168 r28) {
        C36471jw r5 = new C36471jw(this);
        this.A07 = r5;
        AnonymousClass41Y r4 = new AnonymousClass41Y(this);
        this.A03 = r4;
        C858044f r3 = new C858044f(this);
        this.A0I = r3;
        AnonymousClass57C r2 = new AnonymousClass5UJ() { // from class: X.57C
            @Override // X.AnonymousClass5UJ
            public final void ALi(AbstractC14640lm r32) {
                C36641kF r1 = C36641kF.this;
                AnonymousClass009.A05(r32);
                if (r32.equals(r1.A01)) {
                    r1.A04();
                }
            }
        };
        this.A0G = r2;
        this.A0E = r21;
        this.A0B = r17;
        this.A06 = r13;
        this.A08 = r14;
        this.A0K = r25;
        this.A04 = r11;
        this.A05 = r12;
        this.A0J = r24;
        this.A0H = r23;
        this.A01 = r26;
        this.A09 = r15;
        this.A0A = countryGatingViewModel;
        this.A0L = toSGatingViewModel;
        this.A0C = r18;
        this.A0D = r19;
        this.A0F = r22;
        this.A00 = r20;
        this.A0M = new ExecutorC27271Gr(r28, false);
        r14.A03(r5);
        r11.A03(r4);
        r24.A03(r3);
        if (r20.A0K()) {
            r23.A00.add(r2);
        }
    }

    @Override // X.AnonymousClass015
    public void A03() {
        this.A08.A04(this.A07);
        this.A04.A04(this.A03);
        this.A0J.A04(this.A0I);
        if (this.A00.A0K()) {
            AnonymousClass11A r0 = this.A0H;
            r0.A00.remove(this.A0G);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0042, code lost:
        if (r5.A0C((com.whatsapp.jid.GroupJid) r0) != false) goto L_0x0044;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A04() {
        /*
            r19 = this;
            r3 = r19
            X.0n3 r1 = r3.A00
            java.lang.Class<X.0lm> r0 = X.AbstractC14640lm.class
            com.whatsapp.jid.Jid r2 = r1.A0B(r0)
            X.0lm r2 = (X.AbstractC14640lm) r2
            X.0wO r0 = r3.A0C
            X.0n3 r0 = r0.A01(r2)
            r3.A00 = r0
            boolean r0 = r0.A0K()
            if (r0 == 0) goto L_0x0021
            X.0n3 r1 = r3.A00
            java.lang.Class<X.0nU> r0 = X.C15580nU.class
            r1.A0B(r0)
        L_0x0021:
            X.0nU r2 = X.C15580nU.A02(r2)
            X.0n3 r0 = r3.A00
            boolean r0 = r0.A0K()
            r4 = 1
            if (r0 == 0) goto L_0x0044
            X.0nX r5 = r3.A0D
            X.0n3 r1 = r3.A00
            java.lang.Class<X.0nU> r0 = X.C15580nU.class
            com.whatsapp.jid.Jid r0 = r1.A0B(r0)
            X.AnonymousClass009.A05(r0)
            com.whatsapp.jid.GroupJid r0 = (com.whatsapp.jid.GroupJid) r0
            boolean r0 = r5.A0C(r0)
            r10 = 1
            if (r0 == 0) goto L_0x0045
        L_0x0044:
            r10 = 0
        L_0x0045:
            X.0wC r1 = r3.A0F
            X.0n3 r0 = r3.A00
            boolean r16 = r1.A0X(r0)
            X.0n3 r0 = r3.A00
            int r9 = r1.A05(r0)
            X.0n3 r0 = r3.A00
            boolean r17 = r1.A0Y(r0)
            X.1E5 r1 = r3.A0K
            X.0n3 r0 = r3.A00
            boolean r18 = r1.A00(r0)
            X.0v2 r0 = r3.A0B
            int r8 = r0.A02(r2)
            X.0zP r0 = r3.A05
            boolean r0 = r0.A07()
            if (r0 == 0) goto L_0x0090
            if (r2 == 0) goto L_0x0090
            r0 = 3
            if (r8 == r0) goto L_0x0077
            r0 = 2
            if (r8 != r0) goto L_0x0090
        L_0x0077:
            if (r16 != 0) goto L_0x0090
            if (r18 != 0) goto L_0x0090
            if (r9 == r4) goto L_0x0090
            X.1Gr r1 = r3.A0M
            X.2j8 r0 = new X.2j8
            r4 = r0
            r5 = r3
            r6 = r2
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r17
            r4.<init>(r6, r7, r8, r9, r10)
            r1.execute(r0)
            return
        L_0x0090:
            r14 = 0
            X.016 r1 = r3.A02
            X.0n3 r5 = r3.A00
            r6 = 0
            boolean r11 = r5.A0Y
            com.whatsapp.countrygating.viewmodel.CountryGatingViewModel r4 = r3.A0A
            X.0lm r0 = r3.A01
            com.whatsapp.jid.UserJid r2 = com.whatsapp.jid.UserJid.of(r0)
            boolean r12 = r4.A04(r2)
            com.whatsapp.tosgating.viewmodel.ToSGatingViewModel r0 = r3.A0L
            boolean r13 = r0.A04(r2)
            boolean r15 = r3.A05()
            r7 = r6
            X.2V7 r4 = new X.2V7
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
            r1.A0B(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36641kF.A04():void");
    }

    public boolean A05() {
        C14850m9 r0 = this.A0E;
        AbstractC14640lm r1 = this.A01;
        if (C41861uH.A00(r0, r1)) {
            return true;
        }
        CountryGatingViewModel countryGatingViewModel = this.A0A;
        UserJid of = UserJid.of(r1);
        if (countryGatingViewModel.A04(of) || this.A0L.A04(of)) {
            return true;
        }
        if (!this.A00.A0K()) {
            return false;
        }
        Jid A0B = this.A00.A0B(C15580nU.class);
        AnonymousClass009.A05(A0B);
        GroupJid groupJid = (GroupJid) A0B;
        if (!this.A0D.A0C(groupJid) || this.A0F.A0Z(this.A00, groupJid)) {
            return true;
        }
        return false;
    }
}
