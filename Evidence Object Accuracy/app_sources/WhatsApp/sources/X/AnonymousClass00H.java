package X;

import java.io.OutputStream;
import java.io.PrintStream;

/* renamed from: X.00H  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass00H extends PrintStream {
    @Override // java.io.PrintStream, java.io.OutputStream, java.io.Closeable, java.io.FilterOutputStream, java.lang.AutoCloseable
    public void close() {
    }

    public AnonymousClass00H(OutputStream outputStream) {
        super(outputStream);
    }
}
