package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.2ZQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZQ extends Drawable {
    public final Paint A00 = C12960it.A0A();
    public final /* synthetic */ AnonymousClass3FR A01;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public AnonymousClass2ZQ(AnonymousClass3FR r2) {
        this.A01 = r2;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        AnonymousClass3FR r3 = this.A01;
        if (r3.A00 > 0.0f) {
            int A00 = AnonymousClass00T.A00(r3.A0B.getContext(), R.color.conversationRowGlowColor);
            int i = (A00 & 16777215) | (((int) (((float) (A00 >> 24)) * r3.A00)) << 24);
            Paint paint = this.A00;
            paint.setColor(i);
            canvas.drawRect(getBounds(), paint);
        }
    }
}
