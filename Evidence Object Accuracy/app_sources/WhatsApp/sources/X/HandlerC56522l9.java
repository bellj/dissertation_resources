package X;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;

/* renamed from: X.2l9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC56522l9 extends HandlerC472529t {
    public HandlerC56522l9() {
        super(Looper.getMainLooper());
    }

    public HandlerC56522l9(Looper looper) {
        super(looper);
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            try {
                throw C12980iv.A0n("onResult");
            } catch (RuntimeException e) {
                throw e;
            }
        } else if (i != 2) {
            StringBuilder A0t = C12980iv.A0t(45);
            A0t.append("Don't know how to handle message: ");
            A0t.append(i);
            Log.wtf("BasePendingResult", A0t.toString(), new Exception());
        } else {
            ((BasePendingResult) message.obj).A06(Status.A0A);
        }
    }
}
