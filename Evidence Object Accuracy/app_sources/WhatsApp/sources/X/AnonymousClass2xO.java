package X;

import android.view.View;

/* renamed from: X.2xO  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2xO extends AbstractC104124rr {
    public final /* synthetic */ AnonymousClass0QQ A00;
    public final /* synthetic */ AnonymousClass3DA A01;
    public final /* synthetic */ C55262i6 A02;

    public AnonymousClass2xO(AnonymousClass0QQ r1, AnonymousClass3DA r2, C55262i6 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMC(View view) {
        this.A00.A09(null);
        view.setAlpha(1.0f);
        view.setTranslationX(0.0f);
        view.setTranslationY(0.0f);
        C55262i6 r3 = this.A02;
        AnonymousClass3DA r2 = this.A01;
        r3.A03(r2.A05);
        C12980iv.A1K(r3, r2.A05, r3.A02);
    }

    @Override // X.AbstractC104124rr, X.AbstractC12530i4
    public void AMD(View view) {
    }
}
