package X;

import android.content.Context;

/* renamed from: X.0iL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC12690iL {
    boolean A7P(AnonymousClass07H v, C07340Xp v2);

    boolean A9n(AnonymousClass07H v, C07340Xp v2);

    boolean AA1();

    void AIn(Context context, AnonymousClass07H v);

    void AOF(AnonymousClass07H v, boolean z);

    boolean AWs(AnonymousClass0CK v);

    void Abr(AbstractC12280hf v);

    void AfQ(boolean z);
}
