package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5y4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129645y4 {
    public final C16590pI A00;
    public final AnonymousClass600 A01;
    public final C130125yq A02;
    public final AnonymousClass61F A03;
    public final C130105yo A04;

    public C129645y4(C16590pI r1, AnonymousClass600 r2, C130125yq r3, AnonymousClass61F r4, C130105yo r5) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
    }

    public String A00() {
        C130105yo r2 = this.A04;
        String A0p = C12980iv.A0p(r2.A02(), "uuid");
        if (AnonymousClass1US.A0C(A0p)) {
            A0p = C12990iw.A0n();
            C12970iu.A1D(C130105yo.A01(r2), "uuid", A0p);
        }
        AnonymousClass009.A05(A0p);
        String str = AnonymousClass600.A03;
        String obj = C1310561a.A02(this.A00).toString();
        String str2 = this.A03.A06;
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("app_install_uuid", A0p);
            A0a.put("risk_period_uuid", str);
            A0a.put("device_locale", obj);
            if (!AnonymousClass1US.A0C(str2)) {
                A0a.put("auth_token", str2);
            }
        } catch (JSONException unused) {
            Log.e("PAY: NoviGraphQLActionManager/createCompositeHeaderJson failed to construct JSON");
        }
        return Base64.encodeToString(C16050oM.A05(new byte[]{1}, this.A02.A05(A0a.toString().getBytes(), null)), 2);
    }

    public String A01(C127595uk r9) {
        byte[] A03 = AnonymousClass61L.A03(this.A03.A06.getBytes());
        if (A03 == null) {
            return null;
        }
        byte[][] bArr = new byte[2];
        byte[] bArr2 = new byte[1];
        bArr2[0] = 1;
        bArr[0] = bArr2;
        C130125yq r2 = this.A02;
        JSONObject A0a = C117295Zj.A0a();
        try {
            Long l = r9.A00;
            if (l != null) {
                A0a.put("doc_id", l);
            } else {
                A0a.put("doc_string", r9.A02);
            }
            String str = r9.A03;
            if (str != null) {
                A0a.put("variables", str);
            }
            A0a.put("doc_name", r9.A01);
        } catch (JSONException unused) {
            Log.e("PAY: NoviGraphQlInput/toJson() can't construct json");
        }
        bArr[1] = r2.A05(A0a.toString().getBytes(), A03);
        return Base64.encodeToString(C16050oM.A05(bArr), 2);
    }
}
