package X;

import androidx.core.view.inputmethod.EditorInfoCompat;

/* renamed from: X.1gK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34551gK extends AnonymousClass1JQ {
    public final boolean A00;

    public C34551gK(AnonymousClass1JR r10, String str, long j, boolean z) {
        super(C27791Jf.A03, r10, str, "regular_low", 7, j, false);
        this.A00 = z;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass1G4 A0T = C34541gJ.A02.A0T();
        boolean z = this.A00;
        A0T.A03();
        C34541gJ r1 = (C34541gJ) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = z;
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        A01.A03();
        C27831Jk r2 = (C27831Jk) A01.A00;
        r2.A0O = (C34541gJ) A0T.A02();
        r2.A00 |= EditorInfoCompat.IME_FLAG_NO_PERSONALIZED_LEARNING;
        return A01;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("TimeFormatMutation{rowId=");
        sb.append(this.A07);
        sb.append(", is24HourFormat=");
        sb.append(this.A00);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
