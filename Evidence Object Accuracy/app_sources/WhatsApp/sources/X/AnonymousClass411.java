package X;

import java.util.List;
import java.util.Map;

/* renamed from: X.411  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass411 extends AnonymousClass4K8 {
    public final List A00;
    public final Map A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass411) {
                AnonymousClass411 r5 = (AnonymousClass411) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass411(List list, Map map) {
        super(list);
        C16700pc.A0G(list, map);
        this.A00 = list;
        this.A01 = map;
    }

    public int hashCode() {
        return C12990iw.A08(this.A01, this.A00.hashCode() * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("CategoryGroupsWithChildItems(catalogCategoryGroups=");
        A0k.append(this.A00);
        A0k.append(", parentCategoryToChildItemMap=");
        A0k.append(this.A01);
        return C12970iu.A0u(A0k);
    }
}
