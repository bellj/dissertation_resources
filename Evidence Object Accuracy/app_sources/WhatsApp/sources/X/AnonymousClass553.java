package X;

/* renamed from: X.553  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass553 implements AbstractC116005Tt {
    public final C15370n3 A00;

    public AnonymousClass553(C15370n3 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116005Tt
    public C15370n3 ABZ() {
        return this.A00;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("ContactNotInAddressBookListItem{wacontact=");
        A0k.append(this.A00);
        return C12970iu.A0v(A0k);
    }
}
