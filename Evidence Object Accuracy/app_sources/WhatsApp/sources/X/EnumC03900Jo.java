package X;

/* renamed from: X.0Jo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03900Jo {
    AUTO(0),
    ENABLED(1),
    DISABLED(2);
    
    public final String value;

    EnumC03900Jo(int i) {
        this.value = r2;
    }

    public static EnumC03900Jo A00(String str) {
        EnumC03900Jo[] values = values();
        for (EnumC03900Jo r1 : values) {
            if (r1.toString().equals(str)) {
                return r1;
            }
        }
        StringBuilder sb = new StringBuilder("Error finding DragToDismiss enum value for: ");
        sb.append(str);
        C28691Op.A00("CdsOpenScreenConfig", sb.toString());
        return AUTO;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.value;
    }
}
