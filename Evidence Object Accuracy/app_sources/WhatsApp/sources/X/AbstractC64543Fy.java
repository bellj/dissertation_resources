package X;

import android.content.res.TypedArray;

/* renamed from: X.3Fy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC64543Fy {
    public final AnonymousClass3C3 A00 = new AnonymousClass3C3();

    public AbstractC64543Fy A00(TypedArray typedArray) {
        if (typedArray.hasValue(3)) {
            AnonymousClass3C3 r1 = this.A00;
            r1.A0H = typedArray.getBoolean(3, r1.A0H);
        }
        if (typedArray.hasValue(0)) {
            AnonymousClass3C3 r12 = this.A00;
            r12.A0G = typedArray.getBoolean(0, r12.A0G);
        }
        if (typedArray.hasValue(1)) {
            A02(typedArray.getFloat(1, 0.3f));
        }
        if (typedArray.hasValue(11)) {
            A03(typedArray.getFloat(11, 1.0f));
        }
        if (typedArray.hasValue(7)) {
            AnonymousClass3C3 r5 = this.A00;
            long j = (long) typedArray.getInt(7, (int) r5.A0D);
            if (j >= 0) {
                r5.A0D = j;
            } else {
                throw C12970iu.A0f(C12970iu.A0w(C12960it.A0k("Given a negative duration: "), j));
            }
        }
        if (typedArray.hasValue(14)) {
            AnonymousClass3C3 r13 = this.A00;
            r13.A0A = typedArray.getInt(14, r13.A0A);
        }
        if (typedArray.hasValue(15)) {
            AnonymousClass3C3 r52 = this.A00;
            long j2 = (long) typedArray.getInt(15, (int) r52.A0E);
            if (j2 >= 0) {
                r52.A0E = j2;
            } else {
                throw C12970iu.A0f(C12970iu.A0w(C12960it.A0k("Given a negative repeat delay: "), j2));
            }
        }
        if (typedArray.hasValue(16)) {
            AnonymousClass3C3 r14 = this.A00;
            r14.A0B = typedArray.getInt(16, r14.A0B);
        }
        if (typedArray.hasValue(5)) {
            AnonymousClass3C3 r2 = this.A00;
            int i = typedArray.getInt(5, r2.A06);
            if (i != 1) {
                int i2 = 2;
                if (i != 2) {
                    i2 = 3;
                    if (i != 3) {
                        i2 = 0;
                    }
                }
                r2.A06 = i2;
            } else {
                r2.A06 = 1;
            }
        }
        if (typedArray.hasValue(17)) {
            AnonymousClass3C3 r15 = this.A00;
            if (typedArray.getInt(17, r15.A0C) != 1) {
                r15.A0C = 0;
            } else {
                r15.A0C = 1;
            }
        }
        if (typedArray.hasValue(6)) {
            AnonymousClass3C3 r22 = this.A00;
            float f = typedArray.getFloat(6, r22.A00);
            if (f >= 0.0f) {
                r22.A00 = f;
            } else {
                StringBuilder A0k = C12960it.A0k("Given invalid dropoff value: ");
                A0k.append(f);
                throw C12970iu.A0f(A0k.toString());
            }
        }
        if (typedArray.hasValue(9)) {
            AnonymousClass3C3 r23 = this.A00;
            int dimensionPixelSize = typedArray.getDimensionPixelSize(9, r23.A08);
            if (dimensionPixelSize >= 0) {
                r23.A08 = dimensionPixelSize;
            } else {
                throw C12970iu.A0f(C12960it.A0W(dimensionPixelSize, "Given invalid width: "));
            }
        }
        if (typedArray.hasValue(8)) {
            AnonymousClass3C3 r24 = this.A00;
            int dimensionPixelSize2 = typedArray.getDimensionPixelSize(8, r24.A07);
            if (dimensionPixelSize2 >= 0) {
                r24.A07 = dimensionPixelSize2;
            } else {
                throw C12970iu.A0f(C12960it.A0W(dimensionPixelSize2, "Given invalid height: "));
            }
        }
        if (typedArray.hasValue(13)) {
            AnonymousClass3C3 r25 = this.A00;
            float f2 = typedArray.getFloat(13, r25.A02);
            if (f2 >= 0.0f) {
                r25.A02 = f2;
            } else {
                StringBuilder A0k2 = C12960it.A0k("Given invalid intensity value: ");
                A0k2.append(f2);
                throw C12970iu.A0f(A0k2.toString());
            }
        }
        if (typedArray.hasValue(19)) {
            AnonymousClass3C3 r26 = this.A00;
            float f3 = typedArray.getFloat(19, r26.A04);
            if (f3 >= 0.0f) {
                r26.A04 = f3;
            } else {
                StringBuilder A0k3 = C12960it.A0k("Given invalid width ratio: ");
                A0k3.append(f3);
                throw C12970iu.A0f(A0k3.toString());
            }
        }
        if (typedArray.hasValue(10)) {
            AnonymousClass3C3 r27 = this.A00;
            float f4 = typedArray.getFloat(10, r27.A01);
            if (f4 >= 0.0f) {
                r27.A01 = f4;
            } else {
                StringBuilder A0k4 = C12960it.A0k("Given invalid height ratio: ");
                A0k4.append(f4);
                throw C12970iu.A0f(A0k4.toString());
            }
        }
        if (typedArray.hasValue(18)) {
            AnonymousClass3C3 r16 = this.A00;
            r16.A03 = typedArray.getFloat(18, r16.A03);
        }
        return this;
    }

    public AnonymousClass3C3 A01() {
        AnonymousClass3C3 r3 = this.A00;
        int i = r3.A0C;
        int[] iArr = r3.A0K;
        if (i != 1) {
            int i2 = r3.A05;
            iArr[0] = i2;
            int i3 = r3.A09;
            iArr[1] = i3;
            iArr[2] = i3;
            iArr[3] = i2;
        } else {
            int i4 = r3.A09;
            iArr[0] = i4;
            iArr[1] = i4;
            int i5 = r3.A05;
            iArr[2] = i5;
            iArr[3] = i5;
        }
        float[] fArr = r3.A0J;
        if (i != 1) {
            float f = r3.A02;
            float f2 = 1.0f - f;
            float f3 = r3.A00;
            fArr[0] = Math.max((f2 - f3) / 2.0f, 0.0f);
            fArr[1] = Math.max((f2 - 0.001f) / 2.0f, 0.0f);
            float f4 = f + 1.0f;
            fArr[2] = Math.min((f4 + 0.001f) / 2.0f, 1.0f);
            fArr[3] = Math.min((f4 + f3) / 2.0f, 1.0f);
            return r3;
        }
        fArr[0] = 0.0f;
        float f5 = r3.A02;
        fArr[1] = Math.min(f5, 1.0f);
        fArr[2] = Math.min(f5 + r3.A00, 1.0f);
        fArr[3] = 1.0f;
        return r3;
    }

    public void A02(float f) {
        AnonymousClass3C3 r3 = this.A00;
        r3.A05 = (((int) (Math.min(1.0f, Math.max(0.0f, f)) * 255.0f)) << 24) | (r3.A05 & 16777215);
    }

    public void A03(float f) {
        AnonymousClass3C3 r3 = this.A00;
        r3.A09 = (((int) (Math.min(1.0f, Math.max(0.0f, f)) * 255.0f)) << 24) | (r3.A09 & 16777215);
    }
}
