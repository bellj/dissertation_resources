package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.1ZB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZB implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100334lk();
    public final String A00;
    public final String A01;
    public final String A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZB(Parcel parcel) {
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
        this.A00 = parcel.readString();
    }

    public AnonymousClass1ZB(String str, String str2, String str3) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = str3;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeString(this.A00);
    }
}
