package X;

import android.view.View;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import java.util.ArrayList;

/* renamed from: X.0QU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0QU {
    public int A00 = Integer.MIN_VALUE;
    public int A01 = Integer.MIN_VALUE;
    public int A02 = 0;
    public ArrayList A03 = new ArrayList();
    public final int A04;
    public final /* synthetic */ StaggeredGridLayoutManager A05;

    public AnonymousClass0QU(StaggeredGridLayoutManager staggeredGridLayoutManager, int i) {
        this.A05 = staggeredGridLayoutManager;
        this.A04 = i;
    }

    public int A00() {
        if (this.A05.A0F) {
            return A04(this.A03.size() - 1, -1);
        }
        return A04(0, this.A03.size());
    }

    public int A01() {
        if (this.A05.A0F) {
            return A04(0, this.A03.size());
        }
        return A04(this.A03.size() - 1, -1);
    }

    public int A02(int i) {
        int i2 = this.A00;
        if (i2 != Integer.MIN_VALUE) {
            return i2;
        }
        if (this.A03.size() == 0) {
            return i;
        }
        A06();
        return this.A00;
    }

    public int A03(int i) {
        int i2 = this.A01;
        if (i2 != Integer.MIN_VALUE) {
            return i2;
        }
        if (this.A03.size() == 0) {
            return i;
        }
        A07();
        return this.A01;
    }

    public int A04(int i, int i2) {
        AbstractC06220Sq r8 = this.A05.A07;
        int A06 = r8.A06();
        int A02 = r8.A02();
        int i3 = -1;
        if (i2 > i) {
            i3 = 1;
        }
        while (i != i2) {
            View view = (View) this.A03.get(i);
            int A0B = r8.A0B(view);
            int A08 = r8.A08(view);
            boolean z = false;
            boolean z2 = false;
            if (A0B <= A02) {
                z2 = true;
            }
            if (A08 >= A06) {
                z = true;
            }
            if (z2 && z && (A0B < A06 || A08 > A02)) {
                return AnonymousClass02H.A02(view);
            }
            i += i3;
        }
        return -1;
    }

    public View A05(int i, int i2) {
        View view = null;
        ArrayList arrayList = this.A03;
        int size = arrayList.size();
        int i3 = size - 1;
        if (i2 != -1) {
            while (i3 >= 0) {
                View view2 = (View) arrayList.get(i3);
                StaggeredGridLayoutManager staggeredGridLayoutManager = this.A05;
                if (staggeredGridLayoutManager.A0F && AnonymousClass02H.A02(view2) >= i) {
                    break;
                } else if (staggeredGridLayoutManager.A0F || AnonymousClass02H.A02(view2) > i) {
                    if (!view2.hasFocusable()) {
                        break;
                    }
                    i3--;
                    view = view2;
                } else {
                    return view;
                }
            }
        } else {
            int i4 = 0;
            while (i4 < size) {
                View view3 = (View) arrayList.get(i4);
                StaggeredGridLayoutManager staggeredGridLayoutManager2 = this.A05;
                if ((staggeredGridLayoutManager2.A0F && AnonymousClass02H.A02(view3) <= i) || ((!staggeredGridLayoutManager2.A0F && AnonymousClass02H.A02(view3) >= i) || !view3.hasFocusable())) {
                    break;
                }
                i4++;
                view = view3;
            }
        }
        return view;
    }

    public void A06() {
        C06840Vh A00;
        int i;
        ArrayList arrayList = this.A03;
        View view = (View) arrayList.get(arrayList.size() - 1);
        AnonymousClass0FA r2 = (AnonymousClass0FA) view.getLayoutParams();
        StaggeredGridLayoutManager staggeredGridLayoutManager = this.A05;
        this.A00 = staggeredGridLayoutManager.A07.A08(view);
        if (r2.A01 && (A00 = staggeredGridLayoutManager.A09.A00(r2.A00())) != null && A00.A00 == 1) {
            int i2 = this.A00;
            int i3 = this.A04;
            int[] iArr = A00.A03;
            if (iArr == null) {
                i = 0;
            } else {
                i = iArr[i3];
            }
            this.A00 = i2 + i;
        }
    }

    public void A07() {
        C06840Vh A00;
        int i;
        View view = (View) this.A03.get(0);
        AnonymousClass0FA r2 = (AnonymousClass0FA) view.getLayoutParams();
        StaggeredGridLayoutManager staggeredGridLayoutManager = this.A05;
        this.A01 = staggeredGridLayoutManager.A07.A0B(view);
        if (r2.A01 && (A00 = staggeredGridLayoutManager.A09.A00(r2.A00())) != null && A00.A00 == -1) {
            int i2 = this.A01;
            int i3 = this.A04;
            int[] iArr = A00.A03;
            if (iArr == null) {
                i = 0;
            } else {
                i = iArr[i3];
            }
            this.A01 = i2 - i;
        }
    }

    public void A08() {
        this.A03.clear();
        this.A01 = Integer.MIN_VALUE;
        this.A00 = Integer.MIN_VALUE;
        this.A02 = 0;
    }

    public void A09() {
        ArrayList arrayList = this.A03;
        int size = arrayList.size();
        View view = (View) arrayList.remove(size - 1);
        AnonymousClass0FA r1 = (AnonymousClass0FA) view.getLayoutParams();
        r1.A00 = null;
        int i = ((AnonymousClass0B6) r1).A00.A00;
        if (!((i & 8) == 0 && (i & 2) == 0)) {
            this.A02 -= this.A05.A07.A09(view);
        }
        if (size == 1) {
            this.A01 = Integer.MIN_VALUE;
        }
        this.A00 = Integer.MIN_VALUE;
    }

    public void A0A() {
        ArrayList arrayList = this.A03;
        View view = (View) arrayList.remove(0);
        AnonymousClass0FA r1 = (AnonymousClass0FA) view.getLayoutParams();
        r1.A00 = null;
        if (arrayList.size() == 0) {
            this.A00 = Integer.MIN_VALUE;
        }
        int i = ((AnonymousClass0B6) r1).A00.A00;
        if (!((i & 8) == 0 && (i & 2) == 0)) {
            this.A02 -= this.A05.A07.A09(view);
        }
        this.A01 = Integer.MIN_VALUE;
    }

    public void A0B(View view) {
        AnonymousClass0FA r3 = (AnonymousClass0FA) view.getLayoutParams();
        r3.A00 = this;
        ArrayList arrayList = this.A03;
        arrayList.add(view);
        this.A00 = Integer.MIN_VALUE;
        if (arrayList.size() == 1) {
            this.A01 = Integer.MIN_VALUE;
        }
        int i = ((AnonymousClass0B6) r3).A00.A00;
        if ((i & 8) != 0 || (i & 2) != 0) {
            this.A02 += this.A05.A07.A09(view);
        }
    }

    public void A0C(View view) {
        AnonymousClass0FA r3 = (AnonymousClass0FA) view.getLayoutParams();
        r3.A00 = this;
        ArrayList arrayList = this.A03;
        arrayList.add(0, view);
        this.A01 = Integer.MIN_VALUE;
        if (arrayList.size() == 1) {
            this.A00 = Integer.MIN_VALUE;
        }
        int i = ((AnonymousClass0B6) r3).A00.A00;
        if ((i & 8) != 0 || (i & 2) != 0) {
            this.A02 += this.A05.A07.A09(view);
        }
    }
}
