package X;

import android.view.View;

/* renamed from: X.3b5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70313b5 implements AbstractC41521tf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AbstractActivityC33001d7 A01;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    @Override // X.AbstractC41521tf
    public /* synthetic */ void Adu(View view) {
    }

    public C70313b5(AbstractActivityC33001d7 r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return (int) (((float) this.A00) * C12960it.A01(this.A01));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002d, code lost:
        if (r1 != 29) goto L_0x002f;
     */
    @Override // X.AbstractC41521tf
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Adg(android.graphics.Bitmap r7, android.view.View r8, X.AbstractC15340mz r9) {
        /*
            r6 = this;
            r3 = r8
            X.2x5 r3 = (X.AnonymousClass2x5) r3
            if (r7 != 0) goto L_0x00b6
            r2 = 0
            r3.A02 = r2
            byte r1 = r9.A0y
            r5 = 1
            if (r1 == r5) goto L_0x008e
            r0 = 2
            if (r1 == r0) goto L_0x0059
            r0 = 3
            if (r1 == r0) goto L_0x0055
            r0 = 9
            if (r1 == r0) goto L_0x0049
            r0 = 13
            if (r1 == r0) goto L_0x0055
            r0 = 23
            if (r1 == r0) goto L_0x008e
            r0 = 25
            if (r1 == r0) goto L_0x008e
            r0 = 26
            if (r1 == r0) goto L_0x0049
            r0 = 28
            if (r1 == r0) goto L_0x0055
            r0 = 29
            if (r1 == r0) goto L_0x0055
        L_0x002f:
            r3.setImageResource(r2)
        L_0x0032:
            X.1d7 r0 = r6.A01
            java.util.HashSet r2 = r0.A0R
            X.1IS r1 = r9.A0z
            boolean r0 = r2.contains(r1)
            if (r0 != 0) goto L_0x0048
            r2.add(r1)
            android.view.animation.AlphaAnimation r0 = X.C12970iu.A0J()
            X.C12990iw.A1B(r8, r0)
        L_0x0048:
            return
        L_0x0049:
            android.content.Context r1 = r8.getContext()
            r0 = r9
            X.0p1 r0 = (X.C16440p1) r0
            android.graphics.drawable.Drawable r0 = X.C26511Dt.A02(r1, r0)
            goto L_0x00b1
        L_0x0055:
            r2 = 2131232392(0x7f080688, float:1.8080892E38)
            goto L_0x002f
        L_0x0059:
            android.graphics.drawable.Drawable[] r4 = new android.graphics.drawable.Drawable[r0]
            android.content.Context r1 = r8.getContext()
            r0 = 2131100485(0x7f060345, float:1.7813353E38)
            android.graphics.drawable.ColorDrawable r0 = X.C12980iv.A0L(r1, r0)
            r4[r2] = r0
            android.content.Context r1 = r8.getContext()
            r0 = 2131231564(0x7f08034c, float:1.8079213E38)
            android.graphics.drawable.Drawable r2 = X.AnonymousClass00T.A04(r1, r0)
            android.content.res.Resources r1 = r8.getResources()
            r0 = 2131167003(0x7f07071b, float:1.7948267E38)
            int r1 = r1.getDimensionPixelSize(r0)
            android.graphics.drawable.InsetDrawable r0 = new android.graphics.drawable.InsetDrawable
            r0.<init>(r2, r1)
            r4[r5] = r0
            android.graphics.drawable.LayerDrawable r0 = new android.graphics.drawable.LayerDrawable
            r0.<init>(r4)
            r3.setImageDrawable(r0)
            goto L_0x0032
        L_0x008e:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER
            r3.setScaleType(r0)
            X.1d7 r0 = r6.A01
            android.content.res.Resources r1 = r0.getResources()
            r0 = 2131100327(0x7f0602a7, float:1.7813032E38)
            int r0 = r1.getColor(r0)
            r3.setBackgroundColor(r0)
            android.content.Context r2 = r8.getContext()
            r1 = 2131231276(0x7f08022c, float:1.8078628E38)
            r0 = 2131100328(0x7f0602a8, float:1.7813034E38)
            android.graphics.drawable.Drawable r0 = X.AnonymousClass2GE.A01(r2, r1, r0)
        L_0x00b1:
            r3.setImageDrawable(r0)
            goto L_0x0032
        L_0x00b6:
            r3.setImageBitmap(r7)
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C70313b5.Adg(android.graphics.Bitmap, android.view.View, X.0mz):void");
    }
}
