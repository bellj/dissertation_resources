package X;

import android.content.Context;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.search.views.MessageThumbView;

/* renamed from: X.34t  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34t extends AnonymousClass34g {
    public AnonymousClass018 A00;
    public boolean A01;
    public final WaTextView A02 = C12960it.A0N(this, R.id.media_time);
    public final MessageThumbView A03;

    @Override // X.AnonymousClass34g
    public float getRatio() {
        return 1.0f;
    }

    public AnonymousClass34t(Context context) {
        super(context);
        A01();
        MessageThumbView messageThumbView = (MessageThumbView) AnonymousClass028.A0D(this, R.id.thumb_view);
        this.A03 = messageThumbView;
        C12960it.A0r(context, messageThumbView, R.string.gif_preview_description);
    }

    @Override // X.AbstractC74163hQ
    public void A01() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            ((AbstractC84543zT) this).A01 = C12960it.A0S(A00);
            this.A00 = C12960it.A0R(A00);
        }
    }

    @Override // X.AnonymousClass34g
    public int getMark() {
        return R.drawable.msg_status_gif;
    }

    public void setMessage(AnonymousClass1XR r3) {
        super.setMessage((AbstractC16130oV) r3);
        MessageThumbView messageThumbView = this.A03;
        messageThumbView.setVisibility(0);
        messageThumbView.A00 = ((AbstractC84543zT) this).A00;
        messageThumbView.setMessage(r3);
        WaTextView waTextView = this.A02;
        C12990iw.A1G(waTextView);
        waTextView.setVisibility(8);
    }
}
