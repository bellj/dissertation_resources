package X;

/* renamed from: X.4rs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C104134rs implements AbstractC11760go {
    public final /* synthetic */ C34291fu A00;

    public C104134rs(C34291fu r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC11760go
    public void onTouchExplorationStateChanged(boolean z) {
        this.A00.setClickableOrFocusableBasedOnAccessibility(z);
    }
}
