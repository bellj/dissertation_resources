package X;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: X.0UH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0UH {
    public final int A00;
    public final int A01;
    public final long A02;
    public final byte[] A03;

    public AnonymousClass0UH(long j, byte[] bArr, int i, int i2) {
        this.A00 = i;
        this.A01 = i2;
        this.A02 = j;
        this.A03 = bArr;
    }

    public static AnonymousClass0UH A00(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append((char) 0);
        byte[] bytes = sb.toString().getBytes(AnonymousClass03i.A0K);
        return new AnonymousClass0UH(-1, bytes, 2, bytes.length);
    }

    public static AnonymousClass0UH A01(ByteOrder byteOrder, int[] iArr) {
        int i = AnonymousClass03i.A0s[3];
        int length = iArr.length;
        ByteBuffer wrap = ByteBuffer.wrap(new byte[i * length]);
        wrap.order(byteOrder);
        for (int i2 : iArr) {
            wrap.putShort((short) i2);
        }
        return new AnonymousClass0UH(-1, wrap.array(), 3, length);
    }

    public static AnonymousClass0UH A02(ByteOrder byteOrder, long[] jArr) {
        int i = AnonymousClass03i.A0s[4];
        int length = jArr.length;
        ByteBuffer wrap = ByteBuffer.wrap(new byte[i * length]);
        wrap.order(byteOrder);
        for (long j : jArr) {
            wrap.putInt((int) j);
        }
        return new AnonymousClass0UH(-1, wrap.array(), 4, length);
    }

    public static AnonymousClass0UH A03(ByteOrder byteOrder, AnonymousClass0OJ[] r9) {
        int i = AnonymousClass03i.A0s[5];
        int length = r9.length;
        ByteBuffer wrap = ByteBuffer.wrap(new byte[i * length]);
        wrap.order(byteOrder);
        for (AnonymousClass0OJ r3 : r9) {
            wrap.putInt((int) r3.A01);
            wrap.putInt((int) r3.A00);
        }
        return new AnonymousClass0UH(-1, wrap.array(), 5, length);
    }

    public int A04(ByteOrder byteOrder) {
        Object A05 = A05(byteOrder);
        if (A05 == null) {
            throw new NumberFormatException("NULL can't be converted to a integer value");
        } else if (A05 instanceof String) {
            return Integer.parseInt((String) A05);
        } else {
            if (A05 instanceof long[]) {
                long[] jArr = (long[]) A05;
                if (jArr.length == 1) {
                    return (int) jArr[0];
                }
                throw new NumberFormatException("There are more than one component");
            } else if (A05 instanceof int[]) {
                int[] iArr = (int[]) A05;
                if (iArr.length == 1) {
                    return iArr[0];
                }
                throw new NumberFormatException("There are more than one component");
            } else {
                throw new NumberFormatException("Couldn't find a integer value");
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 6, insn: 0x017f: MOVE  (r12 I:??[OBJECT, ARRAY]) = (r6 I:??[OBJECT, ARRAY]), block:B:120:0x017f
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public java.lang.Object A05(
/*
[424] Method generation error in method: X.0UH.A05(java.nio.ByteOrder):java.lang.Object, file: classes.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r14v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public String A06(ByteOrder byteOrder) {
        Object A05 = A05(byteOrder);
        if (A05 != null) {
            if (A05 instanceof String) {
                return (String) A05;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (!(A05 instanceof long[])) {
                if (!(A05 instanceof int[])) {
                    if (!(A05 instanceof double[])) {
                        if (A05 instanceof AnonymousClass0OJ[]) {
                            AnonymousClass0OJ[] r7 = (AnonymousClass0OJ[]) A05;
                            while (true) {
                                int length = r7.length;
                                if (i >= length) {
                                    break;
                                }
                                AnonymousClass0OJ r2 = r7[i];
                                sb.append(r2.A01);
                                sb.append('/');
                                sb.append(r2.A00);
                                i++;
                                if (i != length) {
                                    sb.append(",");
                                }
                            }
                        }
                    } else {
                        double[] dArr = (double[]) A05;
                        while (true) {
                            int length2 = dArr.length;
                            if (i >= length2) {
                                break;
                            }
                            sb.append(dArr[i]);
                            i++;
                            if (i != length2) {
                                sb.append(",");
                            }
                        }
                    }
                } else {
                    int[] iArr = (int[]) A05;
                    while (true) {
                        int length3 = iArr.length;
                        if (i >= length3) {
                            break;
                        }
                        sb.append(iArr[i]);
                        i++;
                        if (i != length3) {
                            sb.append(",");
                        }
                    }
                }
            } else {
                long[] jArr = (long[]) A05;
                while (true) {
                    int length4 = jArr.length;
                    if (i >= length4) {
                        break;
                    }
                    sb.append(jArr[i]);
                    i++;
                    if (i != length4) {
                        sb.append(",");
                    }
                }
            }
            return sb.toString();
        }
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("(");
        sb.append(AnonymousClass03i.A13[this.A00]);
        sb.append(", data length:");
        sb.append(this.A03.length);
        sb.append(")");
        return sb.toString();
    }
}
