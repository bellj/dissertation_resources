package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.0tI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18930tI implements AbstractC18940tJ {
    public C27751Jb A00 = null;
    public final C15570nT A01;
    public final C232310w A02;
    public final C18920tH A03;
    public final C234311q A04;
    public final C233411h A05;
    public final C233711k A06;
    public final C233311g A07;
    public final C234011n A08;
    public final C234211p A09;
    public final C234111o A0A;
    public final C233511i A0B;
    public final Object A0C = new Object();

    public C18930tI(C15570nT r2, C232310w r3, C18920tH r4, C234311q r5, C233411h r6, C233711k r7, C233311g r8, C234011n r9, C234211p r10, C234111o r11, C233511i r12) {
        this.A01 = r2;
        this.A07 = r8;
        this.A08 = r9;
        this.A05 = r6;
        this.A03 = r4;
        this.A0B = r12;
        this.A0A = r11;
        this.A06 = r7;
        this.A02 = r3;
        this.A09 = r10;
        this.A04 = r5;
    }

    public void A00() {
        this.A01.A08();
        synchronized (this.A0C) {
            Log.i("SyncdBootstrapManager/bootstrapNewFeatures");
            C233711k r7 = this.A06;
            if (r7.A01().getInt("syncd_bootstrap_state", 0) == 4) {
                if (r7.A02().isEmpty()) {
                    r7.A07(new HashSet(Arrays.asList("star", "mute", "archive", "contact", "deleteMessageForMe", "setting_pushName", "setting_locale", "markChatAsRead", "sentinel")));
                }
                Set A02 = r7.A02();
                C234011n r5 = this.A08;
                Set<String> A04 = r5.A04();
                A04.removeAll(A02);
                if (!A04.isEmpty()) {
                    ArrayList arrayList = new ArrayList();
                    for (String str : A04) {
                        AbstractC250017s r2 = (AbstractC250017s) r5.A03(str);
                        if (r2 != null) {
                            arrayList.addAll(r2.A01(true));
                            StringBuilder sb = new StringBuilder();
                            sb.append("SyncdBootstrapManager/bootstrapNewFeatures adding mutations for ");
                            sb.append(r2.getClass().getCanonicalName());
                            Log.i(sb.toString());
                        } else {
                            Log.e("SyncdBootstrapManager/bootstrapNewFeatures handler not found");
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        r5.A07(arrayList);
                    }
                    r7.A07(A04);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
        // Method dump skipped, instructions count: 288
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18930tI.A01():void");
    }

    public synchronized void A02(C27751Jb r2) {
        this.A00 = r2;
    }

    public void A03(List list) {
        C27751Jb r5;
        AnonymousClass1K4 r4;
        synchronized (this) {
            r5 = this.A00;
        }
        if (r5 != null) {
            synchronized (r5) {
                Iterator it = list.iterator();
                long j = 0;
                while (it.hasNext()) {
                    C27781Je r42 = (C27781Je) it.next();
                    if (r42 != null && "critical_unblock_low".equals(r42.A01)) {
                        for (Object obj : r42.A02) {
                            if (obj instanceof C34501gF) {
                                j++;
                            }
                        }
                    }
                }
                r5.A00 = j;
                Iterator it2 = list.iterator();
                long j2 = 0;
                while (it2.hasNext()) {
                    C27781Je r0 = (C27781Je) it2.next();
                    if (!(r0 == null || (r4 = r0.A00) == null)) {
                        j2 += (long) r4.AGd();
                        if ((r4.A00 & 2) == 2) {
                            AnonymousClass1K3 r02 = r4.A06;
                            if (r02 == null) {
                                r02 = AnonymousClass1K3.A07;
                            }
                            j2 += r02.A01;
                        }
                    }
                }
                r5.A01 = j2;
            }
            StringBuilder sb = new StringBuilder("SyncdBootstrapManager/syncdRequestPrepared: ");
            sb.append(r5);
            Log.i(sb.toString());
            int i = this.A06.A01().getInt("syncd_bootstrap_state", 0);
            C233411h r3 = this.A05;
            AnonymousClass1JT r2 = r5.A02;
            int i2 = 2;
            if (i == 1) {
                i2 = 1;
            }
            r3.A0D(r2, i2, true);
        }
    }

    @Override // X.AbstractC18940tJ
    public void AXL() {
        Log.i("SyncdBootstrapManager/onSyncdFailed");
        this.A09.A02(false);
    }

    @Override // X.AbstractC18940tJ
    public void AXM() {
        A01();
    }
}
