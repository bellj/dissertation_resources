package X;

import java.io.File;

/* renamed from: X.1pU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39221pU extends AbstractC39111pJ {
    public AnonymousClass1KB A00;
    public String A01;
    public final File A02;
    public final String A03;

    public C39221pU(C39741qT r9, AbstractC14470lU r10, C39781qX r11, C39771qW r12, AbstractC39761qV r13, AnonymousClass1KB r14, File file, File file2, String str, String str2) {
        super(r9, r10, r11, r12, r13, file2);
        this.A02 = file;
        this.A03 = str;
        this.A00 = r14;
        this.A01 = str2;
    }
}
