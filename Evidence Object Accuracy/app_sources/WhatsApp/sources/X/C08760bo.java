package X;

import com.facebook.common.time.AwakeTimeSinceBootClock;

/* renamed from: X.0bo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08760bo implements AnonymousClass5T4 {
    public final /* synthetic */ AnonymousClass0UA A00;
    public final /* synthetic */ AnonymousClass5T4 A01;

    public C08760bo(AnonymousClass0UA r1, AnonymousClass5T4 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass5T4
    public void AV7(AbstractC92144Us r4) {
        AnonymousClass0UA r2 = this.A00;
        if (r2.A09.getAndSet(r4) != r4) {
            try {
                r2.A08.set(r4.A00());
                r2.A07.set(AwakeTimeSinceBootClock.INSTANCE.now());
            } finally {
                this.A01.AV7(r4);
            }
        }
    }
}
