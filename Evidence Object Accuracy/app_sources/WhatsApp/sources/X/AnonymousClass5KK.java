package X;

/* renamed from: X.5KK  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KK extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public AnonymousClass5KK() {
        super(2);
    }

    @Override // X.AnonymousClass5ZQ
    public Object AJ5(Object obj, Object obj2) {
        String str = (String) obj;
        C16700pc.A0F(str, obj2);
        if (str.length() == 0) {
            return obj2.toString();
        }
        return C12960it.A0Z(obj2, ", ", C12960it.A0j(str));
    }
}
