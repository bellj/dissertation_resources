package X;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import com.whatsapp.R;

/* renamed from: X.2CF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2CF extends View implements AnonymousClass004 {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public float A07;
    public float A08;
    public float A09;
    public float A0A;
    public float A0B;
    public float A0C;
    public float A0D;
    public int A0E;
    public int A0F;
    public int A0G;
    public int A0H;
    public int A0I;
    public int A0J;
    public int A0K;
    public int A0L;
    public int A0M;
    public int A0N;
    public int A0O;
    public AnimatorSet A0P;
    public Bitmap A0Q;
    public Bitmap A0R;
    public Bitmap A0S;
    public Matrix A0T;
    public Paint A0U;
    public Paint A0V;
    public Paint A0W;
    public Paint A0X;
    public RectF A0Y;
    public RectF A0Z;
    public AnonymousClass2P7 A0a;
    public boolean A0b;
    public boolean A0c;
    public boolean A0d;

    public AnonymousClass2CF(Context context) {
        super(context, null, 0);
        if (!this.A0b) {
            this.A0b = true;
            generatedComponent();
        }
        this.A0c = false;
        this.A0I = getResources().getDimensionPixelSize(R.dimen.bouncing_lock_size_collapsed);
        this.A0M = getResources().getDimensionPixelSize(R.dimen.bouncing_lock_height_expanded);
        this.A0H = getResources().getDimensionPixelSize(R.dimen.bouncing_lock_border_size);
        this.A0F = getResources().getDimensionPixelSize(R.dimen.bouncing_lock_arrow_bounce_displacement);
        this.A0J = this.A0I >> 1;
        this.A0O = AnonymousClass00T.A00(getContext(), R.color.bouncing_lock_lock_icon_unlocked);
        this.A0N = AnonymousClass00T.A00(getContext(), R.color.bouncing_lock_icon_locked);
        this.A0G = AnonymousClass00T.A00(getContext(), R.color.bouncing_lock_arrow_color);
        this.A0Y = new RectF();
        int A00 = AnonymousClass00T.A00(getContext(), R.color.bouncing_lock_background_color);
        Paint paint = new Paint(1);
        this.A0V = paint;
        paint.setStyle(Paint.Style.FILL);
        this.A0V.setColor(A00);
        this.A0Z = new RectF();
        int A002 = AnonymousClass00T.A00(getContext(), R.color.bouncing_lock_border_color);
        Paint paint2 = new Paint(1);
        this.A0W = paint2;
        paint2.setColor(A002);
        this.A0W.setStyle(Paint.Style.STROKE);
        this.A0W.setStrokeWidth((float) this.A0H);
        Paint paint3 = new Paint(1);
        this.A0X = paint3;
        paint3.setFilterBitmap(true);
        Paint paint4 = new Paint(1);
        this.A0U = paint4;
        paint4.setFilterBitmap(true);
        this.A0U.setColorFilter(new PorterDuffColorFilter(this.A0G, PorterDuff.Mode.SRC_IN));
        this.A0S = BitmapFactory.decodeResource(getResources(), R.drawable.ic_ptt_lock_shackle);
        this.A0R = BitmapFactory.decodeResource(getResources(), R.drawable.ic_ptt_lock_body);
        this.A0Q = BitmapFactory.decodeResource(getResources(), R.drawable.ic_ptt_lock_arrow);
        float height = ((float) this.A0S.getHeight()) * 0.39f;
        this.A05 = height;
        this.A0B = height;
        float height2 = (float) ((this.A0I >> 1) - (this.A0S.getHeight() >> 1));
        this.A0D = height2;
        float height3 = height2 + (((float) this.A0S.getHeight()) * 0.9f);
        this.A0C = height3;
        this.A0A = height3 + ((float) this.A0R.getHeight()) + ((float) getResources().getDimensionPixelSize(R.dimen.bouncing_lock_arrow_margin_top));
        this.A01 = this.A0B;
        this.A03 = -2.5f;
        this.A06 = (float) (this.A0M + this.A0Q.getHeight());
        float height4 = (float) (this.A0M + this.A0S.getHeight());
        this.A08 = height4;
        this.A07 = height4 + (((float) this.A0S.getHeight()) * 0.9f);
        this.A0T = new Matrix();
        A05();
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation((float) getResources().getDimensionPixelSize(R.dimen.bouncing_lock_elevation));
            setClipToOutline(false);
            setOutlineProvider(new C73793go(this));
        }
    }

    public static /* synthetic */ void A00(ValueAnimator valueAnimator, AnonymousClass2CF r3) {
        float floatValue = ((Number) valueAnimator.getAnimatedValue()).floatValue();
        float f = r3.A06;
        r3.A00 = f + ((r3.A0A - f) * floatValue);
        float f2 = r3.A08;
        r3.A04 = f2 + ((r3.A0D - f2) * floatValue);
        float f3 = r3.A07;
        r3.A02 = f3 + ((r3.A0C - f3) * floatValue);
        r3.A0K = (int) (Math.min(1.0f, floatValue * 1.5f) * 255.0f);
        r3.postInvalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            r3.invalidateOutline();
        }
    }

    public static /* synthetic */ void A01(ValueAnimator valueAnimator, AnonymousClass2CF r4) {
        r4.A0X.setColorFilter(new PorterDuffColorFilter(((Number) valueAnimator.getAnimatedValue()).intValue(), PorterDuff.Mode.SRC_IN));
        r4.postInvalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            r4.invalidateOutline();
        }
    }

    public static /* synthetic */ void A02(ValueAnimator valueAnimator, AnonymousClass2CF r3) {
        float floatValue = ((Number) valueAnimator.getAnimatedValue()).floatValue();
        r3.A00 = (r3.A0A - (((float) r3.A0F) * floatValue)) - Math.abs(r3.A0B - r3.A01);
        r3.postInvalidate();
        if (Build.VERSION.SDK_INT >= 21) {
            r3.invalidateOutline();
        }
    }

    public void A03() {
        if (this.A0d) {
            A04();
            int alpha = (int) ((getAlpha() / 1.0f) * 200.0f);
            animate().setListener(null).cancel();
            animate().alpha(0.0f).setDuration((long) alpha).setListener(new C72723f4(this)).start();
        }
    }

    public void A04() {
        if (this.A0c) {
            AnimatorSet animatorSet = this.A0P;
            if (animatorSet != null) {
                animatorSet.end();
                this.A0P.removeAllListeners();
            }
            this.A0P = null;
        }
    }

    public final void A05() {
        this.A01 = this.A0B;
        this.A00 = this.A06;
        this.A04 = this.A08;
        this.A02 = this.A07;
        this.A0K = 0;
        this.A0L = this.A0M;
        this.A0E = 255;
        this.A09 = 0.0f;
        setTranslationY(0.0f);
        setScaleX(1.0f);
        setScaleY(1.0f);
        this.A0X.setColorFilter(new PorterDuffColorFilter(this.A0O, PorterDuff.Mode.SRC_IN));
        if (getMeasuredHeight() != 0 && getMeasuredWidth() != 0) {
            setPivotY((float) (getMeasuredHeight() >> 1));
            setPivotX((float) (getMeasuredWidth() >> 1));
        }
    }

    public final void A06() {
        if (!this.A0c) {
            this.A0P = new AnimatorSet();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            ofFloat.setDuration(800L);
            ofFloat.setRepeatMode(2);
            ofFloat.setRepeatCount(-1);
            ofFloat.setInterpolator(new DecelerateInterpolator());
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4ee
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AnonymousClass2CF r3 = AnonymousClass2CF.this;
                    float A00 = C12960it.A00(valueAnimator);
                    r3.A03 = (2.5f * A00) - 1.75f;
                    float height = ((float) r3.A0S.getHeight()) * 0.39f * A00;
                    r3.A01 = r3.A0B - height;
                    r3.A02 = r3.A0C - height;
                }
            });
            ValueAnimator ofFloat2 = ValueAnimator.ofFloat(0.0f, 1.0f);
            ofFloat2.setDuration(400L);
            ofFloat2.setRepeatMode(2);
            ofFloat2.setRepeatCount(-1);
            ofFloat2.setInterpolator(new AccelerateInterpolator());
            ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4ec
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AnonymousClass2CF.A02(valueAnimator, AnonymousClass2CF.this);
                }
            });
            this.A0P.playTogether(ofFloat, ofFloat2);
            this.A0P.addListener(new C72853fH(this));
            this.A0P.start();
        }
    }

    public final void A07(Runnable runnable, long j) {
        if (!this.A0d) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            ofFloat.setDuration(j);
            ofFloat.setInterpolator(new DecelerateInterpolator());
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.4eb
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AnonymousClass2CF.A00(valueAnimator, AnonymousClass2CF.this);
                }
            });
            ofFloat.addListener(new C72803fC(this, runnable));
            ofFloat.start();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A0a;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A0a = r0;
        }
        return r0.generatedComponent();
    }

    public int getCollapsedHeightPx() {
        return this.A0I;
    }

    public int getExpandedHeightPx() {
        return this.A0M;
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        A04();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = this.A0V;
        paint.setAlpha(this.A0K);
        RectF rectF = this.A0Y;
        rectF.left = 0.0f;
        float f = (float) ((int) this.A01);
        rectF.top = f;
        rectF.right = 0.0f + ((float) this.A0I);
        rectF.bottom = f + ((float) this.A0L);
        float f2 = (float) this.A0J;
        canvas.drawRoundRect(rectF, f2, f2, paint);
        if (Build.VERSION.SDK_INT < 21) {
            RectF rectF2 = this.A0Z;
            float f3 = rectF.left;
            float f4 = (float) (this.A0H >> 1);
            float f5 = f3 + f4;
            rectF2.left = f5;
            rectF2.top = rectF.top + f4;
            float f6 = rectF.right - f4;
            rectF2.right = f6;
            rectF2.bottom = rectF.bottom - f4;
            float f7 = (f6 - f5) / 2.0f;
            canvas.drawRoundRect(rectF2, f7, f7, this.A0W);
        }
        float width = (float) (getWidth() >> 1);
        Matrix matrix = this.A0T;
        Bitmap bitmap = this.A0S;
        matrix.setTranslate(width - ((float) (bitmap.getWidth() >> 1)), this.A04);
        matrix.postRotate(this.A03, (float) (bitmap.getWidth() >> 1), (float) bitmap.getHeight());
        Paint paint2 = this.A0X;
        canvas.drawBitmap(bitmap, matrix, paint2);
        Bitmap bitmap2 = this.A0R;
        canvas.drawBitmap(bitmap2, width - ((float) (bitmap2.getWidth() >> 1)), this.A02, paint2);
        Paint paint3 = this.A0U;
        paint3.setAlpha(this.A0E);
        Bitmap bitmap3 = this.A0Q;
        canvas.drawBitmap(bitmap3, width - ((float) (bitmap3.getWidth() >> 1)), this.A00, paint3);
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(this.A0I, (int) (((float) this.A0M) + this.A05));
    }

    public void setPercentageLocked(float f) {
        boolean z = false;
        if (f >= 0.0f) {
            z = true;
        }
        AnonymousClass009.A0B("Percentage must be >= 0.0", z);
        float min = Math.min(1.0f, f);
        if (this.A09 != min) {
            this.A09 = min;
            int i = this.A0M;
            int i2 = this.A0I;
            float f2 = (float) (i - i2);
            float height = ((float) this.A0S.getHeight()) * 0.39f;
            this.A0E = 255 - ((int) (Math.min(1.0f, min / 0.65f) * 255.0f));
            this.A0L = Math.min(i, Math.max((int) (((float) i) - (f2 * min)), i2));
            setTranslationY((-f2) * Math.min(min, 1.0f));
            if (min >= 0.15f) {
                if (this.A0c) {
                    A04();
                    this.A01 = this.A0B;
                    this.A00 = this.A0A;
                }
                float min2 = Math.min(1.0f, min);
                this.A03 = (2.5f * min2) - 1.75f;
                this.A02 = this.A0C - (height * min2);
                postInvalidate();
                if (Build.VERSION.SDK_INT >= 21) {
                    invalidateOutline();
                }
            } else if (this.A0d && !this.A0c) {
                A06();
            }
        }
    }
}
