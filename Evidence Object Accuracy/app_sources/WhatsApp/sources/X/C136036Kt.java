package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.6Kt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C136036Kt implements AnonymousClass01N {
    public C002601e A00;

    public C136036Kt(AnonymousClass01N r4) {
        this.A00 = new C002601e(null, new AnonymousClass01N() { // from class: X.6Ks
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return new AtomicReference(AnonymousClass01N.this.get());
            }
        });
    }

    @Override // X.AnonymousClass01N, X.AnonymousClass01H
    public Object get() {
        return ((AtomicReference) this.A00.get()).get();
    }
}
