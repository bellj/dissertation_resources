package X;

import java.util.Iterator;

/* renamed from: X.5D6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5D6 implements Iterator {
    public final Iterator A00;

    public /* synthetic */ AnonymousClass5D6(Iterator it) {
        this.A00 = it;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.A00.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        return this.A00.next();
    }

    @Override // java.util.Iterator
    public final void remove() {
        throw C12980iv.A0u("remove operation not supported on immutable iterator");
    }
}
