package X;

import android.os.Process;
import android.os.SystemClock;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1Q4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Q4 implements AnonymousClass1Q2 {
    public final C14830m7 A00;
    public final C243415d A01;

    @Override // X.AbstractC28621Oh
    public String ADy() {
        return "cpu_stats";
    }

    @Override // X.AnonymousClass1Q2
    public boolean AK1() {
        return false;
    }

    public AnonymousClass1Q4(C14830m7 r1, C243415d r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC28621Oh
    public void APh(C28631Oi r9) {
        r9.A02(Integer.valueOf(Process.getThreadPriority(Process.myTid())), "cpu_stats", "stop_pri");
        ConcurrentHashMap concurrentHashMap = r9.A0A;
        Serializable serializable = (Serializable) concurrentHashMap.get(new Pair("cpu_stats", "start_ps_cpu_ms"));
        if (serializable != null) {
            try {
                r9.A02(Long.valueOf(Process.getElapsedCpuTime() - ((Number) serializable).longValue()), "cpu_stats", "ps_cpu_ms");
            } catch (RuntimeException unused) {
                Log.e("QPL: CpuMetadataProvider/onEnd processCpuTimeMsStart is not a number");
            }
            concurrentHashMap.remove(new Pair("cpu_stats", "start_ps_cpu_ms"));
        }
        Serializable serializable2 = (Serializable) concurrentHashMap.get(new Pair("cpu_stats", "start_th_cpu_ms"));
        if (serializable2 != null) {
            try {
                r9.A02(Long.valueOf(SystemClock.currentThreadTimeMillis() - ((Number) serializable2).longValue()), "cpu_stats", "th_cpu_ms");
            } catch (RuntimeException unused2) {
                Log.e("QPL: CpuMetadataProvider/onEnd threadCpuTimeMsStart is not a number");
            }
            concurrentHashMap.remove(new Pair("cpu_stats", "start_th_cpu_ms"));
        }
    }

    @Override // X.AbstractC28621Oh
    public void AWI(C28631Oi r4) {
        r4.A02(Integer.valueOf(Process.getThreadPriority(Process.myTid())), "cpu_stats", "start_pri");
        r4.A02(Long.valueOf(Process.getElapsedCpuTime()), "cpu_stats", "start_ps_cpu_ms");
        r4.A02(Long.valueOf(SystemClock.currentThreadTimeMillis()), "cpu_stats", "start_th_cpu_ms");
    }
}
