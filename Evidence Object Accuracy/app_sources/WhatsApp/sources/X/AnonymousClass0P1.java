package X;

import androidx.work.impl.WorkDatabase;

/* renamed from: X.0P1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0P1 {
    public final WorkDatabase A00;

    public AnonymousClass0P1(WorkDatabase workDatabase) {
        this.A00 = workDatabase;
    }

    public int A00(int i, int i2) {
        synchronized (AnonymousClass0P1.class) {
            int A01 = A01("next_job_scheduler_id");
            if (A01 < i || A01 > i2) {
                this.A00.A07().AJ0(new AnonymousClass0PB("next_job_scheduler_id", (long) (i + 1)));
            } else {
                i = A01;
            }
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r4 == Integer.MAX_VALUE) goto L_0x0019;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A01(java.lang.String r7) {
        /*
            r6 = this;
            androidx.work.impl.WorkDatabase r5 = r6.A00
            r5.A03()
            X.0hx r0 = r5.A07()     // Catch: all -> 0x0032
            java.lang.Long r0 = r0.AE0(r7)     // Catch: all -> 0x0032
            r1 = 0
            if (r0 == 0) goto L_0x002a
            int r4 = r0.intValue()     // Catch: all -> 0x0032
            r0 = 2147483647(0x7fffffff, float:NaN)
            if (r4 != r0) goto L_0x002b
        L_0x0019:
            X.0hx r3 = r5.A07()     // Catch: all -> 0x0032
            long r1 = (long) r1     // Catch: all -> 0x0032
            X.0PB r0 = new X.0PB     // Catch: all -> 0x0032
            r0.<init>(r7, r1)     // Catch: all -> 0x0032
            r3.AJ0(r0)     // Catch: all -> 0x0032
            r5.A05()     // Catch: all -> 0x0032
            goto L_0x002e
        L_0x002a:
            r4 = 0
        L_0x002b:
            int r1 = r4 + 1
            goto L_0x0019
        L_0x002e:
            r5.A04()
            return r4
        L_0x0032:
            r0 = move-exception
            r5.A04()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0P1.A01(java.lang.String):int");
    }
}
