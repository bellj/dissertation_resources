package X;

import java.util.AbstractMap;
import java.util.Map;

/* renamed from: X.3tL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81023tL extends AnonymousClass1Mr<Map.Entry<K, V>> {
    public final /* synthetic */ C28311Mp this$0;

    @Override // X.AbstractC17950rf
    public boolean isPartialView() {
        return true;
    }

    public C81023tL(C28311Mp r1) {
        this.this$0 = r1;
    }

    @Override // java.util.List
    public Map.Entry get(int i) {
        C28291Mn.A01(i, this.this$0.size);
        Object[] objArr = this.this$0.alternatingKeysAndValues;
        int i2 = i << 1;
        return new AbstractMap.SimpleImmutableEntry(objArr[0 + i2], objArr[i2 + 1]);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
    public int size() {
        return this.this$0.size;
    }
}
