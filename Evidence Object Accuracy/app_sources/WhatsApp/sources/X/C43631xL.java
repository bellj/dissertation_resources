package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1xL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43631xL extends BroadcastReceiver {
    public final Object A00 = new Object();
    public volatile boolean A01 = false;
    public final /* synthetic */ C43371wu A02;

    public C43631xL(C43371wu r2) {
        this.A02 = r2;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (!this.A01) {
            synchronized (this.A00) {
                if (!this.A01) {
                    AnonymousClass22D.A00(context);
                    this.A01 = true;
                }
            }
        }
        if (!"com.whatsapp.alarm.CLIENT_PING_PERIODIC".equals(intent.getAction())) {
            StringBuilder sb = new StringBuilder("xmpp/client-ping/periodic/receiver; unexpected intent: ");
            sb.append(intent);
            Log.w(sb.toString());
            return;
        }
        Log.i("xmpp/client-ping/periodic/receiver");
        this.A02.A05();
    }
}
