package X;

/* renamed from: X.5OG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OG extends AnonymousClass5GY {
    public AnonymousClass5OG() {
    }

    public AnonymousClass5OG(AnonymousClass5OG r1) {
        super(r1);
    }

    @Override // X.AnonymousClass5WS
    public AnonymousClass5WS A7l() {
        return new AnonymousClass5OG(this);
    }

    @Override // X.AnonymousClass5XI
    public String AAf() {
        return "SHA-384";
    }

    @Override // X.AnonymousClass5XI
    public int ACZ() {
        return 48;
    }

    @Override // X.AnonymousClass5WS
    public void Aag(AnonymousClass5WS r1) {
        super.A05((AnonymousClass5GY) r1);
    }

    @Override // X.AnonymousClass5GY, X.AnonymousClass5XI
    public void reset() {
        super.reset();
        this.A02 = -3766243637369397544L;
        this.A03 = 7105036623409894663L;
        this.A04 = -7973340178411365097L;
        this.A05 = 1526699215303891257L;
        this.A06 = 7436329637833083697L;
        this.A07 = -8163818279084223215L;
        this.A08 = -2662702644619276377L;
        this.A09 = 5167115440072839076L;
    }

    @Override // X.AnonymousClass5XI
    public int A97(byte[] bArr, int i) {
        AnonymousClass5GY.A03(this, bArr, i);
        reset();
        return 48;
    }
}
