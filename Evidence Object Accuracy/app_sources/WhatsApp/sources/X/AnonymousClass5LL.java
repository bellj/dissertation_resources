package X;

/* renamed from: X.5LL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5LL extends AbstractC94514bx {
    public int A00;
    public String A01;

    public AnonymousClass5LL(int i) {
        super(i);
    }

    public static AnonymousClass4CH A00(char c, int i, int i2) {
        return new AnonymousClass4CH(Character.valueOf(c), i, i2);
    }

    public static AnonymousClass4CH A01(AbstractC94514bx r3, int i) {
        return new AnonymousClass4CH(r3.A04, r3.A01, i);
    }

    public static void A02(AnonymousClass5LL r2, boolean[] zArr) {
        int i = ((AbstractC94514bx) r2).A01;
        r2.A08(zArr);
        r2.A0B(i, ((AbstractC94514bx) r2).A01);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00c4, code lost:
        if (r1 > r0) goto L_0x00c6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0198 A[Catch: NumberFormatException -> 0x01d4, TryCatch #0 {NumberFormatException -> 0x01d4, blocks: (B:126:0x0194, B:128:0x0198, B:130:0x01a3, B:132:0x01ad, B:134:0x01b6, B:136:0x01c6, B:138:0x01cb), top: B:151:0x0194 }] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01a3 A[Catch: NumberFormatException -> 0x01d4, TryCatch #0 {NumberFormatException -> 0x01d4, blocks: (B:126:0x0194, B:128:0x0198, B:130:0x01a3, B:132:0x01ad, B:134:0x01b6, B:136:0x01c6, B:138:0x01cb), top: B:151:0x0194 }] */
    @Override // X.AbstractC94514bx
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A09(boolean[] r15) {
        /*
        // Method dump skipped, instructions count: 500
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5LL.A09(boolean[]):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bd, code lost:
        r3 = r6.A03(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c7, code lost:
        r0 = A09(X.AbstractC94514bx.A0N);
        r4.A02 = r0;
        r3 = r6.A03(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ef, code lost:
        if (r4.A0C == false) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f3, code lost:
        if (r4.A0D != false) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f5, code lost:
        A07();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00f8, code lost:
        r1 = r4.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00fc, code lost:
        if (r1 == 26) goto L_0x0106;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0105, code lost:
        throw A00(r1, r4.A01 - 1, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0106, code lost:
        r4.A04 = null;
        r4.A02 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x010b, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object A0A(java.lang.String r5, X.AnonymousClass4YL r6) {
        /*
        // Method dump skipped, instructions count: 370
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5LL.A0A(java.lang.String, X.4YL):java.lang.Object");
    }

    public void A0B(int i, int i2) {
        while (i < i2 - 1 && Character.isWhitespace(this.A01.charAt(i))) {
            i++;
        }
        while (true) {
            int i3 = i2 - 1;
            if (i3 <= i || !Character.isWhitespace(this.A01.charAt(i3))) {
                break;
            }
            i2--;
        }
        this.A04 = this.A01.substring(i, i2);
    }
}
