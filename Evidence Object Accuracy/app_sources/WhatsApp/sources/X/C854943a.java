package X;

/* renamed from: X.43a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C854943a extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public C854943a() {
        super(2474, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A00);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamUserNoticeError {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userNoticeContentVersion", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userNoticeErrorEvent", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userNoticeId", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
