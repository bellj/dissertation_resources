package X;

import com.whatsapp.settings.SettingsDataUsageActivity;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.38G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38G extends AbstractC16350or {
    public final AtomicBoolean A00 = new AtomicBoolean(false);
    public final /* synthetic */ SettingsDataUsageActivity A01;

    public /* synthetic */ AnonymousClass38G(SettingsDataUsageActivity settingsDataUsageActivity) {
        this.A01 = settingsDataUsageActivity;
    }
}
