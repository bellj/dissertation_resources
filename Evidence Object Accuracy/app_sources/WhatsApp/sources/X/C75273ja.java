package X;

import android.view.View;
import com.whatsapp.util.ViewOnClickCListenerShape3S0400000_I1;

/* renamed from: X.3ja  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75273ja extends AnonymousClass03U {
    public C15580nU A00;

    public C75273ja(View view, C15600nX r8, C15580nU r9) {
        super(view);
        view.setOnClickListener(new ViewOnClickCListenerShape3S0400000_I1(this, view, r9, r8, 2));
    }
}
