package X;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.WaTextView;
import com.whatsapp.search.views.MessageThumbView;

/* renamed from: X.34u  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34u extends AnonymousClass34g {
    public AnonymousClass018 A00;
    public boolean A01;
    public final LinearLayout A02;
    public final WaTextView A03 = C12960it.A0N(this, R.id.media_time);
    public final MessageThumbView A04;

    @Override // X.AnonymousClass34g
    public float getRatio() {
        return 1.0f;
    }

    public AnonymousClass34u(Context context) {
        super(context);
        A01();
        MessageThumbView messageThumbView = (MessageThumbView) AnonymousClass028.A0D(this, R.id.thumb_view);
        this.A04 = messageThumbView;
        this.A02 = (LinearLayout) AnonymousClass028.A0D(this, R.id.button_frame);
        C12960it.A0r(context, messageThumbView, R.string.video_preview_description);
    }

    @Override // X.AbstractC74163hQ
    public void A01() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            ((AbstractC84543zT) this).A01 = C12960it.A0S(A00);
            this.A00 = C12960it.A0R(A00);
        }
    }

    @Override // X.AnonymousClass34g
    public int getMark() {
        return R.drawable.mark_video;
    }

    public void setMessage(AnonymousClass1X2 r6) {
        super.setMessage((AbstractC16130oV) r6);
        MessageThumbView messageThumbView = this.A04;
        messageThumbView.setVisibility(0);
        messageThumbView.A00 = ((AbstractC84543zT) this).A00;
        messageThumbView.setMessage(r6);
        WaTextView waTextView = this.A03;
        waTextView.setText(C63053Ab.A00(this.A00, r6));
        waTextView.setVisibility(0);
        int textSize = ((int) waTextView.getTextSize()) + (getResources().getDimensionPixelSize(R.dimen.search_grid_video_mark_margin_bottom) << 1);
        LinearLayout linearLayout = this.A02;
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        int i = layoutParams.height;
        int i2 = layoutParams.width;
        if (i < textSize) {
            int i3 = 3;
            if (C28141Kv.A00(this.A00)) {
                i3 = 5;
            }
            linearLayout.setLayoutParams(new FrameLayout.LayoutParams(i2, textSize, i3 | 80));
        }
    }
}
