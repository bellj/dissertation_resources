package X;

import java.util.Comparator;

/* renamed from: X.0eV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C10370eV implements Comparator {
    public final /* synthetic */ C08800bs A00;
    public final /* synthetic */ C14230l4 A01;
    public final /* synthetic */ AbstractC14200l1 A02;

    public C10370eV(C08800bs r1, C14230l4 r2, AbstractC14200l1 r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        AbstractC14200l1 r3 = this.A02;
        C14210l2 r1 = new C14210l2();
        r1.A05(obj, 0);
        r1.A05(obj2, 1);
        Object A00 = C14250l6.A00(this.A01, r1.A03(), r3);
        if (A00 instanceof Integer) {
            return ((Number) A00).intValue();
        }
        C28691Op.A00("bk.action.array.SortedArray", "Got non-integer result while evaluating comparator predicate");
        return 0;
    }
}
