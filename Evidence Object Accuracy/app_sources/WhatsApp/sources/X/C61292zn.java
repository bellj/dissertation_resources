package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.2zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61292zn extends C75683kF {
    public final View A00;
    public final TextView A01;

    public C61292zn(View view) {
        super(view);
        this.A00 = AnonymousClass028.A0D(view, R.id.icebreaker_question_root);
        this.A01 = C12960it.A0I(view, R.id.icebreaker_question);
        ImageView A0K = C12970iu.A0K(view, R.id.icebreaker_questions_send_icon);
        A0K.setClickable(false);
        AnonymousClass2GE.A05(view.getContext(), A0K, R.color.primary_light);
    }

    @Override // X.C75683kF
    public /* bridge */ /* synthetic */ void A08(AnonymousClass4WS r4) {
        C61282zm r42 = (C61282zm) r4;
        this.A01.setText(r42.A00.A00);
        AnonymousClass5RV r2 = r42.A01;
        if (r2 != null) {
            C12960it.A14(this.A00, r2, r42, 4);
        }
    }
}
