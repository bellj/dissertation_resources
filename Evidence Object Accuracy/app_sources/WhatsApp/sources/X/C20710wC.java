package X;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.os.Handler;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape5S0200000_I0_5;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.SyncDevicesAndSendInvisibleMessageJob;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.0wC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20710wC {
    public static Handler A11;
    public long A00 = 0;
    public Integer A01;
    public Integer A02;
    public AtomicBoolean A03 = new AtomicBoolean();
    public final AbstractC15710nm A04;
    public final C14900mE A05;
    public final C15570nT A06;
    public final C20870wS A07;
    public final C15450nH A08;
    public final C22640zP A09;
    public final C241014f A0A;
    public final C15550nR A0B;
    public final AnonymousClass11D A0C;
    public final C15610nY A0D;
    public final AnonymousClass10T A0E;
    public final C20730wE A0F;
    public final C20720wD A0G;
    public final C18640sm A0H;
    public final C14830m7 A0I;
    public final C16590pI A0J;
    public final C14820m6 A0K;
    public final C15990oG A0L;
    public final C18240s8 A0M;
    public final C22320yt A0N;
    public final C19990v2 A0O;
    public final C241214h A0P;
    public final C21320xE A0Q;
    public final C15680nj A0R;
    public final C15650ng A0S;
    public final C25621Ac A0T;
    public final AnonymousClass13M A0U;
    public final C247616t A0V;
    public final C245215v A0W;
    public final C15600nX A0X;
    public final C20040v7 A0Y;
    public final AnonymousClass10Y A0Z;
    public final AnonymousClass15K A0a;
    public final C18770sz A0b;
    public final C14850m9 A0c;
    public final AnonymousClass11C A0d;
    public final C42821vw A0e;
    public final AnonymousClass11A A0f;
    public final C244215l A0g;
    public final AnonymousClass1E8 A0h;
    public final AnonymousClass1E5 A0i;
    public final AnonymousClass1E6 A0j;
    public final C16030oK A0k;
    public final C17220qS A0l;
    public final C14840m8 A0m;
    public final C20660w7 A0n;
    public final C22710zW A0o;
    public final C20750wG A0p;
    public final AnonymousClass10Z A0q;
    public final C20320vZ A0r;
    public final C22140ya A0s;
    public final AnonymousClass4QZ A0t;
    public final C20700wB A0u;
    public final AbstractC14440lR A0v;
    public final C14860mA A0w;
    public final Map A0x = new ConcurrentHashMap();
    public final Set A0y = Collections.newSetFromMap(new ConcurrentHashMap());
    public volatile boolean A0z;
    public volatile boolean A10;

    public C20710wC(AbstractC15710nm r12, C14900mE r13, C15570nT r14, C20870wS r15, C15450nH r16, C22640zP r17, C241014f r18, C15550nR r19, AnonymousClass11D r20, C15610nY r21, AnonymousClass10T r22, C20730wE r23, C20720wD r24, C18640sm r25, C14830m7 r26, C16590pI r27, C14820m6 r28, AnonymousClass018 r29, C15990oG r30, C18240s8 r31, C22320yt r32, C19990v2 r33, C241214h r34, C21320xE r35, C15680nj r36, C15650ng r37, C25621Ac r38, AnonymousClass13M r39, C247616t r40, C245215v r41, C15600nX r42, C20040v7 r43, AnonymousClass10Y r44, AnonymousClass15K r45, C18770sz r46, C14850m9 r47, AnonymousClass11C r48, AnonymousClass11A r49, C244215l r50, AnonymousClass1E8 r51, AnonymousClass1E5 r52, AnonymousClass1E6 r53, C16030oK r54, C17220qS r55, C14840m8 r56, C20660w7 r57, C22710zW r58, C20750wG r59, AnonymousClass10Z r60, C20320vZ r61, C22140ya r62, AnonymousClass4QZ r63, C20700wB r64, AbstractC14440lR r65, C14860mA r66) {
        C42821vw r9 = new C42821vw();
        this.A0e = r9;
        this.A0J = r27;
        this.A0I = r26;
        this.A0c = r47;
        this.A05 = r13;
        this.A04 = r12;
        this.A06 = r14;
        this.A0v = r65;
        this.A0O = r33;
        this.A0w = r66;
        this.A0n = r57;
        this.A08 = r16;
        this.A0Y = r43;
        this.A0l = r55;
        this.A0a = r45;
        this.A0B = r19;
        this.A0T = r38;
        this.A07 = r15;
        this.A0D = r21;
        this.A0r = r61;
        this.A0Z = r44;
        this.A0S = r37;
        this.A0u = r64;
        this.A0G = r24;
        this.A0j = r53;
        this.A0m = r56;
        this.A0L = r30;
        this.A0i = r52;
        this.A0b = r46;
        this.A0N = r32;
        this.A0P = r34;
        this.A0E = r22;
        this.A0F = r23;
        this.A0d = r48;
        this.A0p = r59;
        this.A0q = r60;
        this.A0K = r28;
        this.A09 = r17;
        this.A0A = r18;
        this.A0R = r36;
        this.A0h = r51;
        this.A0s = r62;
        this.A0Q = r35;
        this.A0W = r41;
        this.A0o = r58;
        this.A0k = r54;
        this.A0f = r49;
        this.A0X = r42;
        this.A0H = r25;
        this.A0U = r39;
        this.A0C = r20;
        this.A0g = r50;
        this.A0t = r63;
        this.A0V = r40;
        this.A0M = r31;
        A11 = new HandlerC42831vx(r13, r16, r19, r21, r27, r29, r37, r42, r9, r49);
    }

    public static final int A00(String str) {
        if ("invite".equals(str)) {
            return 20;
        }
        if ("linked_group_join".equals(str)) {
            return 79;
        }
        return ("auto_add".equals(str) || "default_sub_group_admin_add".equals(str)) ? 90 : 12;
    }

    public static UserJid A01(AbstractC15340mz r3) {
        AnonymousClass1IS r2 = r3.A0z;
        AbstractC14640lm r1 = r2.A00;
        if (!C15380n4.A0J(r1)) {
            return UserJid.of(r1);
        }
        if (r2.A02 && (r3 instanceof AnonymousClass1XB)) {
            AnonymousClass1XB r22 = (AnonymousClass1XB) r3;
            int i = r22.A00;
            if (i == 10) {
                return ((C30511Xs) r22).A00;
            }
            if (i == 20 || i == 52 || i == 79) {
                return (UserJid) ((C30461Xm) r22).A01.get(0);
            }
        }
        return UserJid.of(r3.A0B());
    }

    public static void A02(int i, Object obj) {
        A11.obtainMessage(i, obj).sendToTarget();
    }

    public static final boolean A03(C15370n3 r2, UserJid userJid, AnonymousClass1PD r4, String str, long j, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        if (TextUtils.equals(r2.A0K, str) && TextUtils.equals(r2.A0P, Long.toString(j)) && TextUtils.equals(r2.A0G.A02, r4.A02) && r2.A0Y == z && r2.A0i == z2 && r2.A0W == z3 && r2.A0g == z4 && C29941Vi.A00(r2.A0E, userJid) && r2.A0a == z5 && r2.A0Z == z6) {
            return false;
        }
        r2.A0K = str;
        r2.A0P = Long.toString(j);
        if (r4.A02 != null) {
            r2.A0G = r4;
        }
        r2.A0Y = z;
        r2.A0i = z2;
        r2.A0W = z3;
        r2.A0g = z4;
        r2.A0E = userJid;
        r2.A0a = z5;
        r2.A0Z = z6;
        return true;
    }

    public final int A04(AnonymousClass1YM r32, String str, Map map, int i, long j, boolean z, boolean z2, boolean z3, boolean z4) {
        C16310on A02;
        String rawString;
        int i2;
        int i3;
        UserJid userJid;
        int i4;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList(map.size());
        for (UserJid userJid2 : map.keySet()) {
            Object obj = map.get(userJid2);
            if ("admin".equals(obj)) {
                i4 = 1;
            } else {
                boolean equals = "superadmin".equals(obj);
                i4 = 0;
                if (equals) {
                    i4 = 2;
                }
            }
            AnonymousClass1YO r8 = (AnonymousClass1YO) r32.A02.get(userJid2);
            if (r8 == null) {
                StringBuilder sb = new StringBuilder("groupmgr/sync-add-participant: ");
                sb.append(userJid2);
                Log.i(sb.toString());
                arrayList.add(userJid2);
            } else if (r8.A01 != i4) {
                StringBuilder sb2 = new StringBuilder("groupmgr/sync-change-admin-participant: ");
                sb2.append(userJid2);
                sb2.append(" was ");
                sb2.append(r8.A01);
                Log.i(sb2.toString());
                arrayList2.add(r8);
            }
            arrayList3.add(new AnonymousClass1YO(userJid2, AnonymousClass1YM.A01(this.A0b.A0D(userJid2)), i4, false));
        }
        r32.A0F(arrayList3);
        ArrayList arrayList4 = new ArrayList();
        Iterator it = r32.A06().iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (!map.containsKey(next)) {
                StringBuilder sb3 = new StringBuilder("groupmgr/sync-remove-participant:");
                sb3.append(next);
                Log.i(sb3.toString());
                arrayList4.add(next);
            }
        }
        r32.A0G(arrayList4);
        if (z) {
            ArrayList A0A = r32.A0A();
            if (!arrayList.isEmpty()) {
                AnonymousClass11C r4 = this.A0d;
                C22140ya r6 = this.A0s;
                AbstractC15590nW r5 = r32.A03;
                long A00 = this.A0I.A00();
                if (!z3 || A0A.size() != 1) {
                    userJid = null;
                } else {
                    userJid = ((AnonymousClass1YO) A0A.get(0)).A03;
                }
                C30461Xm A06 = r6.A06(r32, r5, userJid, null, arrayList, 12, A00, 0);
                i3 = 2;
                r4.A00(A06, 2);
            } else {
                i3 = 2;
            }
            if (!arrayList4.isEmpty()) {
                this.A0d.A00(this.A0s.A06(null, r32.A03, null, null, arrayList4, 13, this.A0I.A00(), 0), i3);
            }
            A0L(r32.A03, Long.valueOf(j), arrayList2);
        } else {
            if (z2 && str != null && !r32.A09().equals(str)) {
                AnonymousClass11C r62 = this.A0d;
                C22140ya r7 = this.A0s;
                r62.A00(C22140ya.A00(r7.A00, r7.A03.A02(r32.A03, true), null, 86, this.A0I.A00()), 2);
            }
            C15600nX r12 = this.A0X;
            C16310on A022 = r12.A07.A02();
            try {
                AnonymousClass1Lx A002 = A022.A00();
                C18680sq r11 = r12.A09;
                if (r11.A0C()) {
                    StringBuilder sb4 = new StringBuilder("participant-user-store/saveGroupParticipants/");
                    sb4.append(r32);
                    Log.i(sb4.toString());
                    C18460sU r3 = r11.A07;
                    AbstractC15590nW r9 = r32.A03;
                    long A01 = r3.A01(r9);
                    A02 = r11.A08.A02();
                    try {
                        AnonymousClass1Lx A003 = A02.A00();
                        C16330op r72 = A02.A03;
                        r72.A01("group_participant_user", "group_jid_row_id=?", new String[]{String.valueOf(A01)});
                        Iterator it2 = r32.A07().iterator();
                        while (it2.hasNext()) {
                            AnonymousClass1YO r13 = (AnonymousClass1YO) it2.next();
                            UserJid userJid3 = r13.A03;
                            long A012 = r11.A01(userJid3);
                            ContentValues contentValues = new ContentValues(4);
                            contentValues.put("group_jid_row_id", Long.valueOf(A01));
                            contentValues.put("user_jid_row_id", Long.valueOf(A012));
                            contentValues.put("rank", Integer.valueOf(r13.A01));
                            int i5 = 0;
                            if (r13.A02) {
                                i5 = 1;
                            }
                            contentValues.put("pending", Integer.valueOf(i5));
                            r72.A02(contentValues, "group_participant_user");
                            r11.A09.A00(AnonymousClass1JO.A00(r13.A04.values()), r9, userJid3, A012);
                        }
                        A003.A00();
                        A003.close();
                    } finally {
                    }
                }
                if (!r11.A0B()) {
                    C20590w0 r10 = r12.A08;
                    StringBuilder sb5 = new StringBuilder("msgstore/saveGroupParticipants/");
                    sb5.append(r32);
                    Log.i(sb5.toString());
                    A02 = r10.A08.A02();
                    try {
                        AnonymousClass1Lx A004 = A02.A00();
                        AbstractC15590nW r73 = r32.A03;
                        String[] strArr = {r73.getRawString()};
                        C16330op r63 = A02.A03;
                        r63.A01("group_participants", "gjid = ?", strArr);
                        Iterator it3 = r32.A07().iterator();
                        while (it3.hasNext()) {
                            AnonymousClass1YO r132 = (AnonymousClass1YO) it3.next();
                            ContentValues contentValues2 = new ContentValues();
                            contentValues2.put("gjid", r73.getRawString());
                            C15570nT r33 = r10.A01;
                            UserJid userJid4 = r132.A03;
                            if (r33.A0F(userJid4)) {
                                rawString = "";
                            } else {
                                rawString = userJid4.getRawString();
                            }
                            contentValues2.put("jid", rawString);
                            contentValues2.put("admin", Integer.valueOf(r132.A01));
                            int i6 = 0;
                            if (r132.A02) {
                                i6 = 1;
                            }
                            contentValues2.put("pending", Integer.valueOf(i6));
                            AnonymousClass1YP r34 = (AnonymousClass1YP) r132.A04.get(DeviceJid.of(userJid4));
                            AnonymousClass009.A05(r34);
                            contentValues2.put("sent_sender_key", Boolean.valueOf(r34.A00));
                            r63.A02(contentValues2, "group_participants");
                        }
                        A004.A00();
                        A004.close();
                    } finally {
                        try {
                            A02.close();
                        } catch (Throwable unused) {
                        }
                    }
                }
                AbstractC15590nW r64 = r32.A03;
                if (r64 instanceof C15580nU) {
                    r12.A05.A01((C15580nU) r64, j);
                }
                A002.A00();
                A002.close();
                A022.close();
            } finally {
                try {
                    A022.close();
                } catch (Throwable unused2) {
                }
            }
        }
        if (this.A0m.A05() && !arrayList.isEmpty()) {
            C15570nT r42 = this.A06;
            if (r32.A0H(r42)) {
                r42.A08();
                if (arrayList.contains(r42.A05)) {
                    A0H(r32, new HashSet(r32.A06().A00), i, z4);
                } else {
                    A0H(r32, arrayList, i, z4);
                }
            }
        }
        this.A0v.Ab2(new RunnableBRunnable0Shape5S0200000_I0_5(this, 8, arrayList));
        if (!arrayList.isEmpty() || !arrayList4.isEmpty()) {
            r32.A0D();
            i2 = 1;
        } else {
            i2 = 0;
        }
        return !arrayList2.isEmpty() ? i2 | 2 : i2;
    }

    public int A05(C15370n3 r4) {
        int A02 = this.A0O.A02((GroupJid) r4.A0B(C15580nU.class));
        if (!r4.A0K()) {
            return 0;
        }
        if (A02 == 3 || A02 == 1) {
            return r4.A02;
        }
        return 0;
    }

    public int A06(C15580nU r3) {
        if (this.A0B.A09(r3) == null) {
            return 0;
        }
        return this.A0X.A0B.A02(1304) - 1;
    }

    public AnonymousClass1JV A07() {
        return AnonymousClass1JV.A02(this.A06, UUID.randomUUID().toString().replace("-", ""));
    }

    public AnonymousClass1XB A08(C29901Ve r10, List list) {
        A0K(r10, list, false);
        C22140ya r3 = this.A0s;
        long A00 = this.A0I.A00();
        C15570nT r0 = this.A06;
        r0.A08();
        C27631Ih r2 = r0.A05;
        AnonymousClass009.A05(r2);
        AnonymousClass1XB A002 = C22140ya.A00(r3.A00, r3.A03.A02(r10, true), null, 9, A00);
        A002.A0l(null);
        A002.A0u(list);
        A002.A0e(r2);
        return A002;
    }

    public String A09(GroupJid groupJid) {
        if (groupJid == null) {
            return null;
        }
        return this.A0D.A0C(this.A0B.A0B(groupJid), -1, false, true, true);
    }

    public final List A0A(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            GroupJid groupJid = (GroupJid) it.next();
            String A09 = this.A0O.A09(groupJid);
            if (A09 == null) {
                A09 = "";
            }
            arrayList.add(new AnonymousClass1OU(groupJid, A09, 2, 0));
        }
        return arrayList;
    }

    public List A0B(Map map) {
        ArrayList arrayList = new ArrayList(map.keySet().size());
        for (AbstractC14640lm r1 : map.keySet()) {
            C15370n3 A0A = this.A0B.A0A(r1);
            if (!(A0A == null || A0A.A0C == null)) {
                arrayList.add(A0A);
            }
        }
        Collections.sort(arrayList, new C42841vy(this.A06, this.A0D, this));
        return arrayList;
    }

    public final synchronized void A0C() {
        Integer num = this.A02;
        AnonymousClass009.A05(num);
        if (num.intValue() == 3) {
            this.A00 = this.A0I.A00();
        }
        int intValue = this.A02.intValue();
        StringBuilder sb = new StringBuilder();
        sb.append("groupmgr/sendGetGroups/ ");
        sb.append(intValue);
        Log.i(sb.toString());
        this.A01 = Integer.valueOf(intValue);
        this.A10 = true;
        C42851vz r10 = new C42851vz(this.A04, this, this.A0l, intValue);
        Log.i("GroupRequestProtocolHelper/sendGetGroups/get-groups");
        C17220qS r9 = r10.A03;
        String A01 = r9.A01();
        ArrayList arrayList = new ArrayList();
        int i = r10.A00;
        if ((i & 1) != 0) {
            arrayList.add(new AnonymousClass1V8("participants", null));
        }
        if ((i & 2) != 0) {
            arrayList.add(new AnonymousClass1V8("description", null));
        }
        r9.A0A(r10, new AnonymousClass1V8("iq", new AnonymousClass1W9[]{new AnonymousClass1W9("id", A01), new AnonymousClass1W9("xmlns", "w:g2"), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9(C29991Vn.A00, "to")}, new AnonymousClass1V8[]{new AnonymousClass1V8("participating", (AnonymousClass1W9[]) null, (AnonymousClass1V8[]) arrayList.toArray(new AnonymousClass1V8[0]))}), A01, 19, 0);
        this.A02 = null;
    }

    public synchronized void A0D(int i) {
        Log.i("groupmgr/groupSyncFailedOrTimedOut");
        this.A0z = false;
        this.A10 = false;
        this.A01 = null;
        A0E(i);
    }

    public final synchronized void A0E(int i) {
        Integer valueOf;
        int intValue;
        Integer num = this.A02;
        if (num != null) {
            valueOf = Integer.valueOf(i | num.intValue());
            this.A02 = valueOf;
        } else {
            valueOf = Integer.valueOf(i);
            this.A02 = valueOf;
        }
        Integer num2 = this.A01;
        if (num2 != null) {
            intValue = num2.intValue() | valueOf.intValue();
        } else {
            intValue = valueOf.intValue();
        }
        this.A0K.A0U(intValue);
    }

    public synchronized void A0F(int i, boolean z, boolean z2) {
        if (z2) {
            if (this.A00 != 0 && this.A0I.A00() - this.A00 < 120000) {
                StringBuilder sb = new StringBuilder();
                sb.append("groupmgr/sendGetGroups/skip backoff param=");
                sb.append(i);
                Log.i(sb.toString());
            }
        }
        if (i == 3) {
            Log.i("groupmgr/sendGetGroups/all");
            this.A0z = true;
        }
        Integer num = this.A01;
        if (num == null) {
            A0E(i);
            A0C();
        } else if (!z || (num.intValue() & i) != i) {
            A0E(i);
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("groupmgr/sendGetGroups/skip inFlight param=");
            sb2.append(i);
            Log.i(sb2.toString());
        }
    }

    public final void A0G(AnonymousClass1YM r22, C15580nU r23, UserJid userJid, AnonymousClass1OT r25, List list, int i, long j, long j2) {
        AnonymousClass1Y4 r4;
        AnonymousClass1Y4 r42;
        AnonymousClass11C r0 = this.A0d;
        if (i == 90) {
            C22140ya r3 = this.A0s;
            String A09 = A09(this.A09.A0D.A00(r23));
            if (r25 == null) {
                r42 = (AnonymousClass1Y4) C22140ya.A00(r3.A00, r3.A03.A02(r23, true), null, 90, j);
            } else {
                r42 = new AnonymousClass1Y4(r22, r25, j);
            }
            r42.A00 = A09;
            r42.A0e(userJid);
            r42.A0u(list);
            ((C30461Xm) r42).A03 = j2;
            C15570nT r2 = r3.A01;
            r2.A08();
            r4 = r42;
            if (list.contains(r2.A05)) {
                ((C30461Xm) r42).A00 = 1;
                r4 = r42;
            }
        } else {
            r4 = this.A0s.A06(r22, r23, userJid, r25, list, i, j, j2);
        }
        r0.A00(r4, 2);
    }

    public final void A0H(AnonymousClass1YM r8, Collection collection, int i, boolean z) {
        GroupJid of = GroupJid.of(r8.A03);
        if (!z || r8.A07().A00.size() < this.A0c.A02(934) || of == null) {
            this.A0G.A01((UserJid[]) collection.toArray(new UserJid[0]), i);
            return;
        }
        C20720wD r4 = this.A0G;
        C20320vZ r3 = this.A0r;
        AnonymousClass1X5 r32 = new AnonymousClass1X5(r3.A07.A02(of, true), this.A0I.A00());
        UserJid[] userJidArr = (UserJid[]) collection.toArray(new UserJid[0]);
        if (userJidArr.length == 0) {
            StringBuilder sb = new StringBuilder("SyncDevicesAndSendInvisibleMessageJob/empty recipients for ");
            sb.append(r32.A0z);
            Log.w(sb.toString());
        } else if (r4.A02.A01(r32.A0z)) {
            r4.A00.A00(new SyncDevicesAndSendInvisibleMessageJob(r32, userJidArr));
        }
    }

    public final void A0I(AnonymousClass1OU r21, GroupJid groupJid, UserJid userJid, AnonymousClass1OT r24, Integer num, int i, int i2, long j) {
        C15370n3 A09;
        SharedPreferences sharedPreferences;
        Log.i("GroupChatManager/updateLinkGroupInfoIfNeeded()");
        if (i == 2 || i == 1 || i == 3) {
            GroupJid groupJid2 = r21.A02;
            if (groupJid2 instanceof C15580nU) {
                if (groupJid != null) {
                    C22640zP r13 = this.A09;
                    Log.i("CommunityChatManageronSubgroupLinked()");
                    r13.A0K.Ab2(new RunnableBRunnable0Shape0S0400000_I0(r13, r21, r24, groupJid, 11));
                }
                if (!this.A09.A07()) {
                    Log.i("groupChatManager/updateLinkGroupInfoIfNeeded/ab prop is off");
                    C241014f r5 = this.A0A;
                    StringBuilder sb = new StringBuilder("CommunitySharedPrefs/setTempGroupType()/groupType = ");
                    sb.append(i);
                    Log.i(sb.toString());
                    synchronized (r5) {
                        sharedPreferences = r5.A00;
                        if (sharedPreferences == null) {
                            sharedPreferences = r5.A01.A01("community_shared_pref");
                            r5.A00 = sharedPreferences;
                        }
                    }
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    StringBuilder sb2 = new StringBuilder("create_");
                    sb2.append(groupJid2.getRawString());
                    edit.putInt(sb2.toString(), i).apply();
                    if (this.A0T.A03(groupJid2, i)) {
                        Log.i("groupChatManager/updateLinkGroupInfoIfNeeded/successfully add group type");
                        r5.A00(groupJid2);
                        return;
                    }
                    return;
                }
                C15580nU r7 = (C15580nU) groupJid2;
                int i3 = 3010;
                if (i2 == 79) {
                    i3 = 3012;
                }
                C22140ya r52 = this.A0s;
                AnonymousClass1Y3 A05 = r52.A05(groupJid, r7, userJid, r24, num, i, j);
                AnonymousClass11C r3 = this.A0d;
                r3.A00(A05, i3);
                if (this.A0c.A07(1727) && this.A0X.A0D(r7) && (A09 = this.A0B.A09(r7)) != null && !A09.A0h) {
                    StringBuilder sb3 = new StringBuilder("SystemMessageFactory/newSubgroupLinkedWithGroupMembershipApprovalModeDisabled/subgroupjid=");
                    sb3.append(r7);
                    sb3.append(" parentgroupjid=");
                    sb3.append(groupJid);
                    Log.i(sb3.toString());
                    AnonymousClass1Y3 r1 = new AnonymousClass1Y3(r52.A03.A02(r7, true), null, 95, j);
                    r1.A00 = 2;
                    r1.A01 = groupJid;
                    r1.A0e(userJid);
                    r3.A00(r1, 3013);
                }
                AnonymousClass1OV.A01(r24, "groupChatManager/subgroup", "scheduled handling of group action 'link' for subgroup '%s'", AnonymousClass1OV.A00(groupJid2));
            }
        }
    }

    public final void A0J(AbstractC14640lm r4, List list) {
        if (this.A0o.A03() && C15380n4.A0J(r4)) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.A0S.A0M(r4, (UserJid) it.next());
            }
        }
    }

    public final void A0K(AbstractC15590nW r13, Iterable iterable, boolean z) {
        AnonymousClass1YM A02 = this.A0X.A02(r13);
        ArrayList arrayList = new ArrayList();
        Iterator it = iterable.iterator();
        while (it.hasNext()) {
            UserJid userJid = (UserJid) it.next();
            arrayList.add(new AnonymousClass1YO(userJid, AnonymousClass1YM.A01(this.A0b.A0D(userJid)), 0, z));
        }
        A02.A0F(arrayList);
        if (C15380n4.A0G(r13)) {
            C15570nT r0 = this.A06;
            r0.A08();
            C27631Ih r7 = r0.A05;
            AnonymousClass009.A05(r7);
            A02.A03(r7, this.A0b.A0D(r7), 2, z, true);
        }
    }

    public void A0L(AbstractC15590nW r9, Long l, List list) {
        this.A0N.A01(new RunnableBRunnable0Shape0S0400000_I0(this, r9, list, l, 24), 46);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        if (r6.A0I(r19) != false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0M(X.AbstractC15590nW r18, java.util.List r19) {
        /*
            r17 = this;
            java.lang.String r0 = ""
            r10 = r19
            X.AnonymousClass009.A09(r0, r10)
            r4 = r17
            X.0nX r0 = r4.A0X
            r7 = r18
            X.1YM r6 = r0.A02(r7)
            X.0m9 r1 = r4.A0c
            r0 = 1108(0x454, float:1.553E-42)
            boolean r0 = r1.A07(r0)
            r3 = 0
            r5 = 1
            if (r0 == 0) goto L_0x0024
            boolean r0 = r6.A0I(r10)
            r1 = 0
            if (r0 == 0) goto L_0x0025
        L_0x0024:
            r1 = 1
        L_0x0025:
            r6.A0G(r10)
            X.0oK r0 = r4.A0k
            r0.A0S(r7, r10)
            boolean r0 = X.C15380n4.A0F(r7)
            if (r0 == 0) goto L_0x007e
            if (r1 == 0) goto L_0x0042
            X.0s8 r2 = r4.A0M
            r0 = 6
            com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1 r1 = new com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1
            r1.<init>(r4, r6, r7, r0)
            java.util.concurrent.ThreadPoolExecutor r0 = r2.A00
            r0.submit(r1)
        L_0x0042:
            X.0nY r0 = r4.A0D
            r0.A0J(r7)
        L_0x0047:
            int r2 = r10.size()
            r1 = 2
            X.11C r0 = r4.A0d
            if (r2 != r5) goto L_0x006b
            X.0ya r8 = r4.A0s
            r11 = 0
            X.0m7 r2 = r4.A0I
            long r13 = r2.A00()
            r12 = 7
            java.lang.Object r10 = r10.get(r3)
            com.whatsapp.jid.UserJid r10 = (com.whatsapp.jid.UserJid) r10
            r15 = 0
            r9 = r7
            X.1Xm r2 = r8.A08(r9, r10, r11, r12, r13, r15)
        L_0x0067:
            r0.A00(r2, r1)
            return
        L_0x006b:
            X.0ya r5 = r4.A0s
            r8 = 0
            X.0m7 r2 = r4.A0I
            long r12 = r2.A00()
            r11 = 14
            r14 = 0
            r9 = r8
            X.1Xm r2 = r5.A06(r6, r7, r8, r9, r10, r11, r12, r14)
            goto L_0x0067
        L_0x007e:
            r4.A0J(r7, r10)
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20710wC.A0M(X.0nW, java.util.List):void");
    }

    public final void A0N(C15580nU r7, int i) {
        C16310on A02;
        AnonymousClass1Lx A00;
        StringBuilder sb = new StringBuilder("groupmgr/updateGroupMemberCount/updating group size metadata for group: ");
        sb.append(r7);
        sb.append(" to:");
        sb.append(i);
        Log.i(sb.toString());
        AnonymousClass11D r1 = this.A0C;
        Integer A002 = r1.A00(r7);
        if (A002 != null && A002.intValue() == i) {
            return;
        }
        if (i >= 1) {
            try {
                A02 = r1.A01.A02();
                A00 = A02.A00();
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e(e);
            }
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("jid_row_id", Long.valueOf(r1.A00.A01(r7)));
                contentValues.put("member_count", Integer.valueOf(i));
                A02.A03.A06(contentValues, "group_membership_count", 5);
                A00.A00();
                A00.close();
                A02.close();
                A11.post(new RunnableBRunnable0Shape5S0200000_I0_5(this, 14, r7));
            } catch (Throwable th) {
                try {
                    A00.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Number of members can't be less than 1.");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0169, code lost:
        if (r27 != false) goto L_0x016b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01eb, code lost:
        if (r57 == 3) goto L_0x01ca;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0O(X.C15580nU r45, X.C15580nU r46, com.whatsapp.jid.UserJid r47, X.AnonymousClass1PD r48, X.AnonymousClass1V0 r49, java.lang.Integer r50, java.lang.String r51, java.lang.String r52, java.util.List r53, java.util.Map r54, int r55, int r56, int r57, long r58, long r60, long r62, long r64, boolean r66, boolean r67, boolean r68, boolean r69, boolean r70, boolean r71) {
        /*
        // Method dump skipped, instructions count: 574
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20710wC.A0O(X.0nU, X.0nU, com.whatsapp.jid.UserJid, X.1PD, X.1V0, java.lang.Integer, java.lang.String, java.lang.String, java.util.List, java.util.Map, int, int, int, long, long, long, long, boolean, boolean, boolean, boolean, boolean, boolean):void");
    }

    public final void A0P(C15580nU r9, C15580nU r10, String str, int i, long j, boolean z) {
        String str2 = str;
        if (i != 2) {
            if (i != 0) {
                if (i != 3) {
                    return;
                }
            }
            this.A0a.A02(r10);
        }
        if (r9 != null) {
            AnonymousClass15K r1 = this.A0a;
            if (str == null) {
                str2 = "";
            }
            int i2 = 2;
            if (z) {
                i2 = 3;
            }
            r1.A04(r9, Collections.singletonList(new AnonymousClass1OU(r10, str2, i2, j)));
            return;
        }
        this.A0a.A02(r10);
    }

    public final void A0Q(C15580nU r11, UserJid userJid) {
        StringBuilder sb = new StringBuilder("groupmgr/addGroupParticipantOnCurrentThread/adding participant: ");
        sb.append(userJid);
        sb.append(" to group:");
        sb.append(r11);
        Log.i(sb.toString());
        this.A0X.A02(r11).A03(userJid, this.A0b.A0D(userJid), 0, false, true);
        this.A0D.A0J(r11);
        C30461Xm A08 = this.A0s.A08(r11, userJid, null, 4, this.A0I.A00(), 0);
        this.A0S.A0p(A08);
        A11.post(new RunnableBRunnable0Shape5S0200000_I0_5(this, 7, A08));
    }

    public void A0R(C15580nU r10, AnonymousClass1V0 r11, long j) {
        if (!this.A0c.A07(1204)) {
            Log.i("groupmgr/onGrowthLockChanged/AbProp disabled - skipping");
            return;
        }
        AnonymousClass1PE A06 = this.A0O.A06(r10);
        if (A06 == null) {
            Log.e("groupmgr/onGrowthLockChanged/notification for nonexistent group");
            return;
        }
        AnonymousClass1XB A00 = this.A0j.A00(r10, A06.A0b, r11, j);
        if (A00 != null) {
            this.A0d.A00(A00, 8);
        }
        C25621Ac r3 = this.A0T;
        StringBuilder sb = new StringBuilder("msgstore/updategroupchatgrowthlockifexists/");
        sb.append(r10);
        Log.i(sb.toString());
        r3.A00.A01(new RunnableBRunnable0Shape0S0300000_I0(r3, r10, r11, 46), 58);
    }

    public void A0S(UserJid userJid, AnonymousClass1OT r11, int i, long j) {
        String str;
        int i2 = i;
        StringBuilder sb = new StringBuilder("groupmgr/onGroupEphemeralChanged/");
        sb.append(r11);
        sb.append("/");
        sb.append(i);
        Log.i(sb.toString());
        if (i < 0) {
            i2 = 0;
        }
        C15580nU A02 = C15580nU.A02(C15380n4.A00(r11.A01));
        AnonymousClass009.A05(A02);
        C15550nR r1 = this.A0B;
        C15370n3 A09 = r1.A09(A02);
        if (A09 == null) {
            str = "groupmgr/onGroupEphemeralChanged/new group";
        } else if (A09.A01 != i2) {
            Log.i("groupmgr/onGroupEphemeralChanged/changed");
            r1.A0Q(A02, i2);
            A02(3017, this.A0s.A04(A02, userJid, r11, i2, j));
            return;
        } else {
            str = "groupmgr/onGroupEphemeralChanged/did not change";
        }
        Log.i(str);
        this.A0n.A0E(r11);
    }

    public void A0T(AnonymousClass1OT r4, boolean z) {
        String str;
        StringBuilder sb = new StringBuilder("groupmgr/onGroupSuspensionChanged/");
        sb.append(r4);
        sb.append("/");
        sb.append(z);
        Log.i(sb.toString());
        C15580nU A02 = C15580nU.A02(C15380n4.A00(r4.A01));
        AnonymousClass009.A05(A02);
        C15550nR r1 = this.A0B;
        C15370n3 A09 = r1.A09(A02);
        if (A09 == null) {
            str = "groupmgr/onGroupSuspensionChanged/new group";
        } else if (A09.A0a != z) {
            Log.i("groupmgr/onGroupSuspensionChanged/changed");
            r1.A0P(A02, z);
            this.A0n.A0E(r4);
        } else {
            str = "groupmgr/onGroupSuspensionChanged/did not change";
        }
        Log.i(str);
        this.A0n.A0E(r4);
    }

    public void A0U(List list, boolean z) {
        StringBuilder sb = new StringBuilder("groupmgr/onLeaveGroup/");
        sb.append(Arrays.deepToString(list.toArray()));
        Log.i(sb.toString());
        C15570nT r2 = this.A06;
        r2.A08();
        C27631Ih r7 = r2.A05;
        AnonymousClass009.A05(r7);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C15580nU r6 = (C15580nU) it.next();
            this.A0D.A0J(r6);
            AnonymousClass1YM A02 = this.A0X.A02(r6);
            if (A02.A0H(r2)) {
                A02.A02(r7);
                this.A0k.A0T(r6);
                this.A05.A0H(new RunnableBRunnable0Shape5S0200000_I0_5(this, 13, r6));
                A0J(r6, Collections.singletonList(r7));
                C30461Xm A08 = this.A0s.A08(r6, r7, null, 5, this.A0I.A00(), 0);
                if (!z || !this.A0O.A0D(r6)) {
                    A02(3, A08);
                } else {
                    this.A0d.A00(A08, 7);
                }
            }
        }
    }

    public boolean A0V() {
        if (!this.A0c.A07(982)) {
            return false;
        }
        this.A06.A08();
        return true;
    }

    public boolean A0W(int i) {
        return A0V() && i == 1;
    }

    public boolean A0X(C15370n3 r4) {
        int A02 = this.A0O.A02((GroupJid) r4.A0B(C15580nU.class));
        if (!r4.A0K() || A02 == 3 || A02 == 1 || !r4.A0a || !this.A0c.A07(973)) {
            return false;
        }
        return true;
    }

    public boolean A0Y(C15370n3 r4) {
        return r4 != null && A0X(r4) && this.A0c.A07(1415);
    }

    public boolean A0Z(C15370n3 r4, AbstractC14640lm r5) {
        if (!r4.A0K() || !C15380n4.A0J(r5)) {
            return false;
        }
        if ((!r4.A0W || this.A0X.A0D((GroupJid) r5)) && !A0X(r4) && !this.A0i.A00(r4)) {
            return false;
        }
        return true;
    }

    public boolean A0a(C15580nU r4) {
        return A0V() && r4 != null && this.A0O.A02(r4) == 3;
    }

    public boolean A0b(C15580nU r3) {
        if (!A0V() || r3 == null || this.A0O.A02(r3) != 1) {
            return false;
        }
        return true;
    }
}
