package X;

/* renamed from: X.2RG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2RG extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;

    public AnonymousClass2RG() {
        super(2180, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamMdDeviceSyncAck {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "revoke", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
