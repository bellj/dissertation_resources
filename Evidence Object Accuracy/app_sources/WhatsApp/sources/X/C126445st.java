package X;

/* renamed from: X.5st  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126445st {
    public final AnonymousClass1V8 A00;

    public C126445st(AnonymousClass3CT r11, C126455su r12, String str, String str2, String str3, String str4, String str5, String str6) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-reject-collect");
        if (C117295Zj.A1W(str, 1, false)) {
            C41141sy.A01(A0N, "id", str);
        }
        if (C117295Zj.A1V(str2, 1, false)) {
            C41141sy.A01(A0N, "device-id", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 100, false)) {
            C41141sy.A01(A0N, "sender-vpa", str3);
        }
        if (str4 != null && AnonymousClass3JT.A0E(str4, 1, 100, true)) {
            C41141sy.A01(A0N, "sender-vpa-id", str4);
        }
        if (str5 != null && C117295Zj.A1Y(str5, true)) {
            C41141sy.A01(A0N, "upi-bank-info", str5);
        }
        if (AnonymousClass3JT.A0E(str6, 1, 100, false)) {
            C41141sy.A01(A0N, "receiver-vpa", str6);
        }
        if (r12 != null) {
            A0N.A05(r12.A00);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r11);
    }
}
