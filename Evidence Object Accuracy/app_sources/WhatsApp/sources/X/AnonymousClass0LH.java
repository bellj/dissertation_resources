package X;

/* renamed from: X.0LH  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0LH {
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b1, code lost:
        if (r3 == false) goto L_0x00b6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A00(android.view.ViewGroup r8, boolean r9) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 18
            if (r1 < r0) goto L_0x000a
            X.AnonymousClass0MC.A00(r8, r9)
        L_0x0009:
            return
        L_0x000a:
            android.animation.LayoutTransition r0 = X.AnonymousClass0K5.A00
            r5 = 1
            r3 = 0
            r4 = 0
            if (r0 != 0) goto L_0x0032
            X.09R r1 = new X.09R
            r1.<init>()
            X.AnonymousClass0K5.A00 = r1
            r0 = 2
            r1.setAnimator(r0, r4)
            android.animation.LayoutTransition r0 = X.AnonymousClass0K5.A00
            r0.setAnimator(r3, r4)
            android.animation.LayoutTransition r0 = X.AnonymousClass0K5.A00
            r0.setAnimator(r5, r4)
            android.animation.LayoutTransition r1 = X.AnonymousClass0K5.A00
            r0 = 3
            r1.setAnimator(r0, r4)
            android.animation.LayoutTransition r1 = X.AnonymousClass0K5.A00
            r0 = 4
            r1.setAnimator(r0, r4)
        L_0x0032:
            if (r9 == 0) goto L_0x007d
            android.animation.LayoutTransition r7 = r8.getLayoutTransition()
            if (r7 == 0) goto L_0x007a
            boolean r0 = r7.isRunning()
            if (r0 == 0) goto L_0x0070
            boolean r0 = X.AnonymousClass0K5.A03
            java.lang.String r6 = "Failed to access cancel method by reflection"
            java.lang.String r4 = "ViewGroupUtilsApi14"
            if (r0 != 0) goto L_0x005d
            java.lang.Class<android.animation.LayoutTransition> r2 = android.animation.LayoutTransition.class
            java.lang.String r1 = "cancel"
            java.lang.Class[] r0 = new java.lang.Class[r3]     // Catch: NoSuchMethodException -> 0x0058
            java.lang.reflect.Method r0 = r2.getDeclaredMethod(r1, r0)     // Catch: NoSuchMethodException -> 0x0058
            X.AnonymousClass0K5.A02 = r0     // Catch: NoSuchMethodException -> 0x0058
            r0.setAccessible(r5)     // Catch: NoSuchMethodException -> 0x0058
            goto L_0x005b
        L_0x0058:
            android.util.Log.i(r4, r6)
        L_0x005b:
            X.AnonymousClass0K5.A03 = r5
        L_0x005d:
            java.lang.reflect.Method r1 = X.AnonymousClass0K5.A02
            if (r1 == 0) goto L_0x0070
            java.lang.Object[] r0 = new java.lang.Object[r3]     // Catch: IllegalAccessException -> 0x006d, InvocationTargetException -> 0x0067
            r1.invoke(r7, r0)     // Catch: IllegalAccessException -> 0x006d, InvocationTargetException -> 0x0067
            goto L_0x0070
        L_0x0067:
            java.lang.String r0 = "Failed to invoke cancel method by reflection"
            android.util.Log.i(r4, r0)
            goto L_0x0070
        L_0x006d:
            android.util.Log.i(r4, r6)
        L_0x0070:
            android.animation.LayoutTransition r0 = X.AnonymousClass0K5.A00
            if (r7 == r0) goto L_0x007a
            r0 = 2131366498(0x7f0a1262, float:1.8352891E38)
            r8.setTag(r0, r7)
        L_0x007a:
            android.animation.LayoutTransition r0 = X.AnonymousClass0K5.A00
            goto L_0x00c4
        L_0x007d:
            r8.setLayoutTransition(r4)
            boolean r0 = X.AnonymousClass0K5.A04
            java.lang.String r2 = "ViewGroupUtilsApi14"
            if (r0 != 0) goto L_0x009b
            java.lang.Class<android.view.ViewGroup> r1 = android.view.ViewGroup.class
            java.lang.String r0 = "mLayoutSuppressed"
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)     // Catch: NoSuchFieldException -> 0x0094
            X.AnonymousClass0K5.A01 = r0     // Catch: NoSuchFieldException -> 0x0094
            r0.setAccessible(r5)     // Catch: NoSuchFieldException -> 0x0094
            goto L_0x0099
        L_0x0094:
            java.lang.String r0 = "Failed to access mLayoutSuppressed field by reflection"
            android.util.Log.i(r2, r0)
        L_0x0099:
            X.AnonymousClass0K5.A04 = r5
        L_0x009b:
            java.lang.reflect.Field r0 = X.AnonymousClass0K5.A01
            if (r0 == 0) goto L_0x00b6
            boolean r1 = r0.getBoolean(r8)     // Catch: IllegalAccessException -> 0x00ac
            if (r1 == 0) goto L_0x00b6
            java.lang.reflect.Field r0 = X.AnonymousClass0K5.A01     // Catch: IllegalAccessException -> 0x00ab
            r0.setBoolean(r8, r3)     // Catch: IllegalAccessException -> 0x00ab
            goto L_0x00b3
        L_0x00ab:
            r3 = r1
        L_0x00ac:
            java.lang.String r0 = "Failed to get mLayoutSuppressed field by reflection"
            android.util.Log.i(r2, r0)
            if (r3 == 0) goto L_0x00b6
        L_0x00b3:
            r8.requestLayout()
        L_0x00b6:
            r1 = 2131366498(0x7f0a1262, float:1.8352891E38)
            java.lang.Object r0 = r8.getTag(r1)
            android.animation.LayoutTransition r0 = (android.animation.LayoutTransition) r0
            if (r0 == 0) goto L_0x0009
            r8.setTag(r1, r4)
        L_0x00c4:
            r8.setLayoutTransition(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0LH.A00(android.view.ViewGroup, boolean):void");
    }
}
