package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1sS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C40861sS extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C40861sS A04;
    public static volatile AnonymousClass255 A05;
    public byte A00 = -1;
    public int A01;
    public int A02;
    public String A03 = "";

    static {
        C40861sS r0 = new C40861sS();
        A04 = r0;
        r0.A0W();
    }

    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        EnumC40871sT r0;
        boolean z = true;
        switch (r8.ordinal()) {
            case 0:
                byte b = this.A00;
                if (b != 1) {
                    if (b != 0) {
                        boolean booleanValue = ((Boolean) obj).booleanValue();
                        if ((this.A01 & 1) == 1) {
                            if (booleanValue) {
                                this.A00 = 1;
                            }
                        } else if (booleanValue) {
                            this.A00 = 0;
                        }
                    }
                    return null;
                }
                return A04;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C40861sS r10 = (C40861sS) obj2;
                int i = this.A01;
                boolean z2 = true;
                if ((i & 1) != 1) {
                    z2 = false;
                }
                String str = this.A03;
                int i2 = r10.A01;
                boolean z3 = true;
                if ((i2 & 1) != 1) {
                    z3 = false;
                }
                this.A03 = r9.Afy(str, r10.A03, z2, z3);
                if ((i & 2) != 2) {
                    z = false;
                }
                int i3 = this.A02;
                boolean z4 = false;
                if ((i2 & 2) == 2) {
                    z4 = true;
                }
                this.A02 = r9.Afp(i3, r10.A02, z, z4);
                if (r9 == C463025i.A00) {
                    this.A01 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 != 0) {
                                if (A03 == 10) {
                                    String A0A = r92.A0A();
                                    this.A01 |= 1;
                                    this.A03 = A0A;
                                } else if (A03 == 16) {
                                    int A02 = r92.A02();
                                    if (A02 == 0) {
                                        r0 = EnumC40871sT.A02;
                                    } else if (A02 == 1) {
                                        r0 = EnumC40871sT.A01;
                                    } else if (A02 != 2) {
                                        r0 = null;
                                    } else {
                                        r0 = EnumC40871sT.A03;
                                    }
                                    if (r0 == null) {
                                        super.A0X(2, A02);
                                    } else {
                                        this.A01 |= 2;
                                        this.A02 = A02;
                                    }
                                } else if (!A0a(r92, A03)) {
                                }
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
                return A04;
            case 3:
                return null;
            case 4:
                return new C40861sS();
            case 5:
                return new C82443vd();
            case 6:
                return A04;
            case 7:
                if (A05 == null) {
                    synchronized (C40861sS.class) {
                        if (A05 == null) {
                            A05 = new AnonymousClass255(A04);
                        }
                    }
                }
                return A05;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A01 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A03);
        }
        if ((this.A01 & 2) == 2) {
            i2 += CodedOutputStream.A02(2, this.A02);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A01 & 1) == 1) {
            codedOutputStream.A0I(1, this.A03);
        }
        if ((this.A01 & 2) == 2) {
            codedOutputStream.A0E(2, this.A02);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
