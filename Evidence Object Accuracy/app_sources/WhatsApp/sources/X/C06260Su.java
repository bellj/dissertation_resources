package X;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import java.util.Arrays;

/* renamed from: X.0Su  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06260Su {
    public static final Interpolator A0L = new animation.InterpolatorC07030Wk();
    public float A00;
    public float A01;
    public int A02 = -1;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public VelocityTracker A07;
    public View A08;
    public OverScroller A09;
    public boolean A0A;
    public float[] A0B;
    public float[] A0C;
    public float[] A0D;
    public float[] A0E;
    public int[] A0F;
    public int[] A0G;
    public int[] A0H;
    public final ViewGroup A0I;
    public final AnonymousClass04X A0J;
    public final Runnable A0K = new RunnableC09200cW(this);

    public C06260Su(Context context, ViewGroup viewGroup, AnonymousClass04X r6) {
        if (r6 != null) {
            this.A0I = viewGroup;
            this.A0J = r6;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.A04 = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.A06 = viewConfiguration.getScaledTouchSlop();
            this.A00 = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.A01 = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.A09 = new OverScroller(context, A0L);
            return;
        }
        throw new IllegalArgumentException("Callback may not be null");
    }

    public final int A00(int i, int i2, int i3) {
        int i4;
        if (i == 0) {
            return 0;
        }
        int width = this.A0I.getWidth();
        float abs = (float) Math.abs(i);
        float f = (float) (width >> 1);
        float sin = f + (((float) Math.sin((double) ((Math.min(1.0f, abs / ((float) width)) - 0.5f) * 0.47123894f))) * f);
        int abs2 = Math.abs(i2);
        if (abs2 > 0) {
            i4 = Math.round(Math.abs(sin / ((float) abs2)) * 1000.0f) << 2;
        } else {
            i4 = (int) (((abs / ((float) i3)) + 1.0f) * 256.0f);
        }
        return Math.min(i4, 600);
    }

    public View A01(int i, int i2) {
        ViewGroup viewGroup = this.A0I;
        int childCount = viewGroup.getChildCount();
        while (true) {
            childCount--;
            if (childCount < 0) {
                return null;
            }
            View childAt = viewGroup.getChildAt(childCount);
            if (i >= childAt.getLeft() && i < childAt.getRight() && i2 >= childAt.getTop() && i2 < childAt.getBottom()) {
                return childAt;
            }
        }
    }

    public void A02() {
        this.A02 = -1;
        float[] fArr = this.A0B;
        if (fArr != null) {
            Arrays.fill(fArr, 0.0f);
            Arrays.fill(this.A0C, 0.0f);
            Arrays.fill(this.A0D, 0.0f);
            Arrays.fill(this.A0E, 0.0f);
            Arrays.fill(this.A0H, 0);
            Arrays.fill(this.A0F, 0);
            Arrays.fill(this.A0G, 0);
            this.A05 = 0;
        }
        VelocityTracker velocityTracker = this.A07;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.A07 = null;
        }
    }

    public final void A03() {
        VelocityTracker velocityTracker = this.A07;
        float f = this.A00;
        velocityTracker.computeCurrentVelocity(1000, f);
        float xVelocity = this.A07.getXVelocity(this.A02);
        float f2 = this.A01;
        float f3 = f;
        float abs = Math.abs(xVelocity);
        if (abs < f2) {
            xVelocity = 0.0f;
        } else if (abs > f) {
            if (xVelocity <= 0.0f) {
                f3 = -f;
            }
            xVelocity = f3;
        }
        float yVelocity = this.A07.getYVelocity(this.A02);
        float f4 = this.A01;
        float abs2 = Math.abs(yVelocity);
        if (abs2 < f4) {
            yVelocity = 0.0f;
        } else if (abs2 > f) {
            if (yVelocity <= 0.0f) {
                f = -f;
            }
            yVelocity = f;
        }
        this.A0A = true;
        this.A0J.A05(this.A08, xVelocity, yVelocity);
        this.A0A = false;
        if (this.A03 == 1) {
            A04(0);
        }
    }

    public void A04(int i) {
        this.A0I.removeCallbacks(this.A0K);
        if (this.A03 != i) {
            this.A03 = i;
            this.A0J.A00(i);
            if (this.A03 == 0) {
                this.A08 = null;
            }
        }
    }

    public final void A05(int i) {
        float[] fArr = this.A0B;
        if (fArr != null) {
            int i2 = this.A05;
            int i3 = 1 << i;
            if ((i3 & i2) != 0) {
                fArr[i] = 0.0f;
                this.A0C[i] = 0.0f;
                this.A0D[i] = 0.0f;
                this.A0E[i] = 0.0f;
                this.A0H[i] = 0;
                this.A0F[i] = 0;
                this.A0G[i] = 0;
                this.A05 = (i3 ^ -1) & i2;
            }
        }
    }

    public final void A06(int i, float f, float f2) {
        float[] fArr = this.A0B;
        if (fArr == null || fArr.length <= i) {
            int i2 = i + 1;
            float[] fArr2 = new float[i2];
            float[] fArr3 = new float[i2];
            float[] fArr4 = new float[i2];
            float[] fArr5 = new float[i2];
            int[] iArr = new int[i2];
            int[] iArr2 = new int[i2];
            int[] iArr3 = new int[i2];
            if (fArr != null) {
                System.arraycopy(fArr, 0, fArr2, 0, fArr.length);
                float[] fArr6 = this.A0C;
                System.arraycopy(fArr6, 0, fArr3, 0, fArr6.length);
                float[] fArr7 = this.A0D;
                System.arraycopy(fArr7, 0, fArr4, 0, fArr7.length);
                float[] fArr8 = this.A0E;
                System.arraycopy(fArr8, 0, fArr5, 0, fArr8.length);
                int[] iArr4 = this.A0H;
                System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
                int[] iArr5 = this.A0F;
                System.arraycopy(iArr5, 0, iArr2, 0, iArr5.length);
                int[] iArr6 = this.A0G;
                System.arraycopy(iArr6, 0, iArr3, 0, iArr6.length);
            }
            this.A0B = fArr2;
            fArr = fArr2;
            this.A0C = fArr3;
            this.A0D = fArr4;
            this.A0E = fArr5;
            this.A0H = iArr;
            this.A0F = iArr2;
            this.A0G = iArr3;
        }
        this.A0D[i] = f;
        fArr[i] = f;
        float[] fArr9 = this.A0C;
        this.A0E[i] = f2;
        fArr9[i] = f2;
        int[] iArr7 = this.A0H;
        int i3 = (int) f;
        int i4 = (int) f2;
        ViewGroup viewGroup = this.A0I;
        int left = viewGroup.getLeft();
        int i5 = this.A04;
        int i6 = 0;
        if (i3 < left + i5) {
            i6 = 1;
        }
        if (i4 < viewGroup.getTop() + i5) {
            i6 |= 4;
        }
        if (i3 > viewGroup.getRight() - i5) {
            i6 |= 2;
        }
        if (i4 > viewGroup.getBottom() - i5) {
            i6 |= 8;
        }
        iArr7[i] = i6;
        this.A05 |= 1 << i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0061, code lost:
        if (r10.A02 == -1) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0063, code lost:
        A03();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(android.view.MotionEvent r11) {
        /*
        // Method dump skipped, instructions count: 410
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06260Su.A07(android.view.MotionEvent):void");
    }

    public final void A08(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i = 0; i < pointerCount; i++) {
            int pointerId = motionEvent.getPointerId(i);
            if (A0B(pointerId)) {
                float x = motionEvent.getX(i);
                float y = motionEvent.getY(i);
                this.A0D[pointerId] = x;
                this.A0E[pointerId] = y;
            }
        }
    }

    public void A09(View view, int i) {
        ViewParent parent = view.getParent();
        ViewGroup viewGroup = this.A0I;
        if (parent == viewGroup) {
            this.A08 = view;
            this.A02 = i;
            this.A0J.A06(view, i);
            A04(1);
            return;
        }
        StringBuilder sb = new StringBuilder("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (");
        sb.append(viewGroup);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }

    public boolean A0A() {
        if (this.A03 == 2) {
            OverScroller overScroller = this.A09;
            boolean computeScrollOffset = overScroller.computeScrollOffset();
            int currX = overScroller.getCurrX();
            int currY = overScroller.getCurrY();
            int left = currX - this.A08.getLeft();
            int top = currY - this.A08.getTop();
            if (left != 0) {
                AnonymousClass028.A0X(this.A08, left);
            }
            if (top != 0) {
                AnonymousClass028.A0Y(this.A08, top);
            }
            if (!(left == 0 && top == 0)) {
                this.A0J.A07(this.A08, currX, currY, left, top);
            }
            if (computeScrollOffset) {
                if (currX == overScroller.getFinalX() && currY == overScroller.getFinalY()) {
                    overScroller.abortAnimation();
                }
            }
            this.A0I.post(this.A0K);
        }
        if (this.A03 == 2) {
            return true;
        }
        return false;
    }

    public final boolean A0B(int i) {
        if (((1 << i) & this.A05) != 0) {
            return true;
        }
        StringBuilder sb = new StringBuilder("Ignoring pointerId=");
        sb.append(i);
        sb.append(" because ACTION_DOWN was not received ");
        sb.append("for this pointer before ACTION_MOVE. It likely happened because ");
        sb.append(" ViewDragHelper did not receive all the events in the event stream.");
        Log.e("ViewDragHelper", sb.toString());
        return false;
    }

    public boolean A0C(int i, int i2) {
        if (this.A0A) {
            return A0D(i, i2, (int) this.A07.getXVelocity(this.A02), (int) this.A07.getYVelocity(this.A02));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    public final boolean A0D(int i, int i2, int i3, int i4) {
        float f;
        float f2;
        int i5 = i3;
        int i6 = i4;
        int left = this.A08.getLeft();
        int top = this.A08.getTop();
        int i7 = i - left;
        int i8 = i2 - top;
        if (i7 == 0 && i8 == 0) {
            this.A09.abortAnimation();
            A04(0);
            return false;
        }
        View view = this.A08;
        int i9 = (int) this.A01;
        int i10 = (int) this.A00;
        int i11 = i10;
        int abs = Math.abs(i5);
        if (abs < i9) {
            i5 = 0;
        } else if (abs > i10) {
            if (i3 <= 0) {
                i11 = -i10;
            }
            i5 = i11;
        }
        int abs2 = Math.abs(i6);
        if (abs2 < i9) {
            i6 = 0;
        } else if (abs2 > i10) {
            if (i4 <= 0) {
                i10 = -i10;
            }
            i6 = i10;
        }
        int abs3 = Math.abs(i7);
        int abs4 = Math.abs(i8);
        int abs5 = Math.abs(i5);
        int abs6 = Math.abs(i6);
        int i12 = abs5 + abs6;
        int i13 = abs3 + abs4;
        if (i5 != 0) {
            f = (float) abs5;
            f2 = (float) i12;
        } else {
            f = (float) abs3;
            f2 = (float) i13;
        }
        float f3 = f / f2;
        float f4 = (float) abs6;
        float f5 = (float) i12;
        if (i6 == 0) {
            f4 = (float) abs4;
            f5 = (float) i13;
        }
        AnonymousClass04X r2 = this.A0J;
        int A00 = A00(i7, i5, r2.A01(view));
        OverScroller overScroller = this.A09;
        overScroller.startScroll(left, top, i7, i8, (int) ((((float) A00) * f3) + (((float) A00(i8, i6, r2.A02(view))) * (f4 / f5))));
        A04(2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bc, code lost:
        if (r5 != r11) goto L_0x00ca;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A0E(android.view.MotionEvent r14) {
        /*
        // Method dump skipped, instructions count: 258
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06260Su.A0E(android.view.MotionEvent):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0024 A[ORIG_RETURN, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A0F(android.view.View r5, float r6, float r7) {
        /*
            r4 = this;
            r3 = 0
            if (r5 == 0) goto L_0x0025
            X.04X r1 = r4.A0J
            int r0 = r1.A01(r5)
            r2 = 0
            if (r0 <= 0) goto L_0x000d
            r2 = 1
        L_0x000d:
            int r1 = r1.A02(r5)
            r0 = 0
            if (r1 <= 0) goto L_0x0015
            r0 = 1
        L_0x0015:
            if (r2 == 0) goto L_0x0026
            if (r0 == 0) goto L_0x002d
            float r6 = r6 * r6
            float r7 = r7 * r7
            float r6 = r6 + r7
            int r0 = r4.A06
            int r0 = r0 * r0
            float r0 = (float) r0
            int r0 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
        L_0x0022:
            if (r0 <= 0) goto L_0x0025
            r3 = 1
        L_0x0025:
            return r3
        L_0x0026:
            if (r0 == 0) goto L_0x0025
            float r1 = java.lang.Math.abs(r7)
            goto L_0x0031
        L_0x002d:
            float r1 = java.lang.Math.abs(r6)
        L_0x0031:
            int r0 = r4.A06
            float r0 = (float) r0
            int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06260Su.A0F(android.view.View, float, float):boolean");
    }

    public boolean A0G(View view, int i) {
        if (view == this.A08 && this.A02 == i) {
            return true;
        }
        if (view == null || !this.A0J.A08(view, i)) {
            return false;
        }
        this.A02 = i;
        A09(view, i);
        return true;
    }

    public boolean A0H(View view, int i, int i2) {
        this.A08 = view;
        this.A02 = -1;
        boolean A0D = A0D(i, i2, 0, 0);
        if (!A0D && this.A03 == 0 && this.A08 != null) {
            this.A08 = null;
        }
        return A0D;
    }
}
