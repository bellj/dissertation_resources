package X;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;
import com.whatsapp.gallery.GalleryFragmentBase;
import com.whatsapp.gallery.LinksGalleryFragment;
import com.whatsapp.webpagepreview.WebPagePreviewView;
import java.util.Set;

/* renamed from: X.2ht  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55162ht extends AnonymousClass03U {
    public C61242ze A00;
    public AbstractC15340mz A01;
    public String A02;
    public Set A03;
    public final View A04;
    public final View A05;
    public final TextView A06;
    public final TextView A07;
    public final WebPagePreviewView A08;
    public final /* synthetic */ LinksGalleryFragment A09;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C55162ht(View view, LinksGalleryFragment linksGalleryFragment) {
        super(view);
        this.A09 = linksGalleryFragment;
        this.A06 = C12960it.A0J(view, R.id.message_text);
        View findViewById = view.findViewById(R.id.message_text_holder);
        this.A05 = view.findViewById(R.id.starred_status);
        this.A04 = view.findViewById(R.id.kept_status);
        this.A07 = C12960it.A0J(view, R.id.suspicious_link_indicator);
        this.A08 = (WebPagePreviewView) AnonymousClass028.A0D(view, R.id.link_preview_frame);
        C12960it.A0x(findViewById, this, 18);
        C12960it.A0x(view, this, 19);
        view.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.3Mo
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                C55162ht r0 = C55162ht.this;
                AbstractC15340mz r4 = r0.A01;
                if (r4 == null) {
                    return false;
                }
                LinksGalleryFragment linksGalleryFragment2 = r0.A09;
                ActivityC000900k A01 = C13010iy.A01(linksGalleryFragment2);
                AbstractC13890kV r1 = (AbstractC13890kV) A01;
                boolean AIM = r1.AIM();
                AnonymousClass009.A05(A01);
                if (AIM) {
                    r1.Af1(r4);
                } else {
                    r1.AeE(r4);
                }
                ((GalleryFragmentBase) linksGalleryFragment2).A0A.A02();
                return true;
            }
        });
    }

    public void A08(AbstractC15340mz r18, int i) {
        this.A01 = r18;
        C61242ze r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        WebPagePreviewView webPagePreviewView = this.A08;
        webPagePreviewView.A02();
        TextView textView = this.A06;
        C12990iw.A1G(textView);
        webPagePreviewView.setLinkTitle(null);
        webPagePreviewView.A0L.setVisibility(8);
        View view = this.A05;
        view.setVisibility(8);
        View view2 = this.A04;
        view2.setVisibility(8);
        webPagePreviewView.setLinkHostname(null);
        webPagePreviewView.setLinkGifSize(0);
        TextView textView2 = this.A07;
        textView2.setVisibility(8);
        LinksGalleryFragment linksGalleryFragment = this.A09;
        boolean AJm = ((AbstractC13890kV) C13010iy.A01(linksGalleryFragment)).AJm(r18);
        int i2 = 0;
        View view3 = this.A0H;
        FrameLayout frameLayout = (FrameLayout) view3;
        if (AJm) {
            frameLayout.setForeground(C12980iv.A0L(linksGalleryFragment.A0p(), R.color.multi_selection));
            view3.setSelected(true);
        } else {
            frameLayout.setForeground(null);
            view3.setSelected(false);
        }
        if (this.A03 == null) {
            i2 = 8;
        }
        textView2.setVisibility(i2);
        if (r18.A0v) {
            view2.setVisibility(8);
            view.setVisibility(0);
        } else {
            view2.setVisibility(8);
            view.setVisibility(8);
        }
        C63563Cb r1 = linksGalleryFragment.A01;
        C61242ze r8 = new C61242ze(linksGalleryFragment.A01(), textView, ((GalleryFragmentBase) linksGalleryFragment).A05, linksGalleryFragment.A04, linksGalleryFragment.A05, r18, ((AbstractC13890kV) C13010iy.A01(linksGalleryFragment)).AGT(), i);
        this.A00 = r8;
        r1.A00(new AnonymousClass5U2() { // from class: X.3WT
            @Override // X.AnonymousClass5U2
            public final void AOO(Object obj) {
                C55162ht r6 = C55162ht.this;
                C91354Rk r82 = (C91354Rk) obj;
                C64643Gi r5 = r82.A00;
                AnonymousClass4QJ r4 = r5.A00;
                r6.A02 = r4.A01;
                r6.A03 = r4.A02;
                WebPagePreviewView webPagePreviewView2 = r6.A08;
                int i3 = 0;
                if (r6.A01 instanceof C30331Wz) {
                    i3 = 2;
                }
                webPagePreviewView2.setLinkTitleTypeface(i3);
                webPagePreviewView2.setLinkTitle(r82.A02);
                webPagePreviewView2.setLinkSnippet(r82.A01);
                r6.A06.setText(r82.A03);
                if (r5.A04) {
                    r6.A09.A06.A08(webPagePreviewView2.A0L, r6.A01, new C70303b4(r6));
                } else {
                    webPagePreviewView2.A0L.setVisibility(0);
                    ThumbnailButton thumbnailButton = webPagePreviewView2.A0L;
                    thumbnailButton.setImageDrawable(AnonymousClass2GE.A01(thumbnailButton.getContext(), R.drawable.ic_group_invite_link, R.color.white_alpha_80));
                    webPagePreviewView2.A0L.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    webPagePreviewView2.A0L.setScaleX(1.5f);
                    webPagePreviewView2.A0L.setScaleY(1.5f);
                    ThumbnailButton thumbnailButton2 = webPagePreviewView2.A0L;
                    C12970iu.A18(thumbnailButton2.getContext(), thumbnailButton2, R.color.media_link_thumbnail_background);
                }
                webPagePreviewView2.setLinkHostname(r4.A00);
            }
        }, r8);
    }
}
