package X;

import android.graphics.PointF;
import android.view.View;

/* renamed from: X.0FJ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FJ extends AnonymousClass0FB {
    public AbstractC06220Sq A00;
    public AbstractC06220Sq A01;

    @Override // X.AnonymousClass0FB
    public int A00(AnonymousClass02H r10, int i, int i2) {
        AbstractC06220Sq r8;
        int A02;
        boolean z;
        PointF A7U;
        int A07 = r10.A07();
        if (A07 != 0) {
            if (r10.A15()) {
                r8 = this.A01;
                if (r8 == null || r8.A02 != r10) {
                    r8 = new AnonymousClass0F0(r10);
                    this.A01 = r8;
                }
            } else if (r10.A14()) {
                r8 = this.A00;
                if (r8 == null || r8.A02 != r10) {
                    r8 = new AnonymousClass0Ez(r10);
                    this.A00 = r8;
                }
            }
            int A06 = r10.A06();
            View view = null;
            if (A06 != 0) {
                int i3 = Integer.MAX_VALUE;
                for (int i4 = 0; i4 < A06; i4++) {
                    View A0D = r10.A0D(i4);
                    int A0B = r8.A0B(A0D);
                    if (A0B < i3) {
                        view = A0D;
                        i3 = A0B;
                    }
                }
                if (!(view == null || (A02 = AnonymousClass02H.A02(view)) == -1)) {
                    if (!r10.A14() ? i2 <= 0 : i <= 0) {
                        z = false;
                    } else {
                        z = true;
                    }
                    return (!(r10 instanceof AnonymousClass02I) || (A7U = ((AnonymousClass02I) r10).A7U(A07 - 1)) == null || (A7U.x >= 0.0f && A7U.y >= 0.0f)) ? z ? A02 + 1 : A02 : z ? A02 - 1 : A02;
                }
            }
        }
        return -1;
    }

    @Override // X.AnonymousClass0FB
    public View A01(AnonymousClass02H r10) {
        AbstractC06220Sq r6;
        int A01;
        if (r10.A15()) {
            r6 = this.A01;
            if (r6 == null || r6.A02 != r10) {
                r6 = new AnonymousClass0F0(r10);
                this.A01 = r6;
            }
        } else if (!r10.A14()) {
            return null;
        } else {
            r6 = this.A00;
            if (r6 == null || r6.A02 != r10) {
                r6 = new AnonymousClass0Ez(r10);
                this.A00 = r6;
            }
        }
        int A06 = r10.A06();
        View view = null;
        if (A06 == 0) {
            return null;
        }
        if (r10.A0S()) {
            A01 = r6.A06() + (r6.A07() >> 1);
        } else {
            A01 = r6.A01() >> 1;
        }
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < A06; i2++) {
            View A0D = r10.A0D(i2);
            int abs = Math.abs((r6.A0B(A0D) + (r6.A09(A0D) >> 1)) - A01);
            if (abs < i) {
                view = A0D;
                i = abs;
            }
        }
        return view;
    }

    @Override // X.AnonymousClass0FB
    public AnonymousClass0FE A02(AnonymousClass02H r3) {
        if (!(r3 instanceof AnonymousClass02I)) {
            return null;
        }
        return new AnonymousClass0Ey(super.A01.getContext(), this);
    }

    @Override // X.AnonymousClass0FB
    public int[] A04(View view, AnonymousClass02H r9) {
        int A01;
        int A012;
        int[] iArr = new int[2];
        if (r9.A14()) {
            AbstractC06220Sq r3 = this.A00;
            if (r3 == null || r3.A02 != r9) {
                r3 = new AnonymousClass0Ez(r9);
                this.A00 = r3;
            }
            int A0B = r3.A0B(view) + (r3.A09(view) >> 1);
            if (r9.A0S()) {
                A012 = r3.A06() + (r3.A07() >> 1);
            } else {
                A012 = r3.A01() >> 1;
            }
            iArr[0] = A0B - A012;
        } else {
            iArr[0] = 0;
        }
        if (r9.A15()) {
            AbstractC06220Sq r32 = this.A01;
            if (r32 == null || r32.A02 != r9) {
                r32 = new AnonymousClass0F0(r9);
                this.A01 = r32;
            }
            int A0B2 = r32.A0B(view) + (r32.A09(view) >> 1);
            if (r9.A0S()) {
                A01 = r32.A06() + (r32.A07() >> 1);
            } else {
                A01 = r32.A01() >> 1;
            }
            iArr[1] = A0B2 - A01;
            return iArr;
        }
        iArr[1] = 0;
        return iArr;
    }
}
