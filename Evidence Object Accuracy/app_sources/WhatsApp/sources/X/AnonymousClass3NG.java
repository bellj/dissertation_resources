package X;

import android.animation.ValueAnimator;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.IDxLAdapterShape0S0100000_1_I1;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantViewLayout;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.3NG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NG implements View.OnTouchListener {
    public double A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04;
    public float A05;
    public float A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public long A0E;
    public AnonymousClass4RW A0F;
    public final /* synthetic */ VideoCallParticipantViewLayout A0G;

    public /* synthetic */ AnonymousClass3NG(VideoCallParticipantViewLayout videoCallParticipantViewLayout) {
        this.A0G = videoCallParticipantViewLayout;
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i;
        String A0f;
        boolean z;
        int i2;
        int i3;
        int i4;
        VoipActivityV2 voipActivityV2;
        float f;
        float rawX;
        int i5;
        boolean z2 = view instanceof VideoCallParticipantView;
        AnonymousClass009.A0A("PipOnTouchListener can only work with VideoCallParticipantView", z2);
        boolean z3 = false;
        if (!z2) {
            Log.i(C12960it.A0b("VideoCallParticipantViewLayout/PipOnTouchListener/ ignore, wrong view ", view));
            return false;
        }
        int i6 = ((VideoCallParticipantView) view).A03;
        if (i6 != 1) {
            A0f = C12960it.A0W(i6, "VideoCallParticipantViewLayout/PipOnTouchListener/ swallow the events when mode is ");
        } else {
            VideoCallParticipantViewLayout videoCallParticipantViewLayout = this.A0G;
            AnonymousClass018 r0 = videoCallParticipantViewLayout.A0H;
            AnonymousClass009.A05(r0);
            ViewGroup.MarginLayoutParams A0H = C12970iu.A0H(view);
            int action = motionEvent.getAction();
            if (action == 0) {
                this.A08 = videoCallParticipantViewLayout.getWidth();
                this.A07 = videoCallParticipantViewLayout.getHeight();
                this.A01 = motionEvent.getRawX();
                this.A02 = motionEvent.getRawY();
                if (C28141Kv.A01(r0)) {
                    i = A0H.leftMargin;
                } else {
                    i = A0H.rightMargin;
                }
                this.A0A = i;
                this.A0B = A0H.topMargin;
                this.A0D = view.getWidth();
                this.A0C = view.getHeight();
                AnonymousClass5RO r2 = videoCallParticipantViewLayout.A0F;
                if (r2 != null) {
                    VoipActivityV2 voipActivityV22 = ((C1101254i) r2).A00;
                    voipActivityV22.A1m = true;
                    if (voipActivityV22.A1N != null) {
                        voipActivityV22.A2z();
                    }
                }
                int i7 = videoCallParticipantViewLayout.A06;
                int i8 = videoCallParticipantViewLayout.A05;
                this.A00 = Math.sqrt((double) ((i7 * i7) + (i8 * i8)));
                this.A09 = 0;
                this.A0F = videoCallParticipantViewLayout.A02(this.A0D, this.A0C);
                this.A06 = 0.0f;
                this.A05 = 0.0f;
                this.A04 = 0.0f;
                this.A03 = 0.0f;
                this.A0E = 0;
                StringBuilder A0k = C12960it.A0k("VideoCallParticipantViewLayout/videoPipParticipantView/onTouch ACTION_DOWN downX: ");
                A0k.append(this.A01);
                A0k.append(", downY: ");
                A0k.append(this.A02);
                A0k.append(", leftMargin: ");
                A0k.append(this.A0A);
                A0k.append(", topMargin: ");
                A0f = C12960it.A0f(A0k, this.A0B);
            } else if (action != 1) {
                if (action == 2) {
                    if (this.A0F == null) {
                        A0f = "VideoCallParticipantViewLayout/videoPipParticipantView/onTouch ACTION_MOVE dispatched before ACTION_DOWN, ignore";
                    } else {
                        int i9 = this.A0A;
                        if (C28141Kv.A01(r0)) {
                            f = motionEvent.getRawX();
                            rawX = this.A01;
                        } else {
                            f = this.A01;
                            rawX = motionEvent.getRawX();
                        }
                        int rawY = this.A0B + ((int) (motionEvent.getRawY() - this.A02));
                        AnonymousClass4RW r4 = this.A0F;
                        int max = Math.max(r4.A02, Math.min(r4.A00, i9 + ((int) (f - rawX))));
                        int max2 = Math.max(r4.A03, Math.min(r4.A01, rawY));
                        motionEvent.getEventTime();
                        motionEvent.getRawX();
                        motionEvent.getRawY();
                        if (C28141Kv.A01(r0)) {
                            i5 = A0H.rightMargin;
                        } else {
                            i5 = A0H.leftMargin;
                        }
                        C42941w9.A09(view, r0, max, max2, i5, A0H.bottomMargin);
                        this.A09 = Math.max(Math.max(C12980iv.A05(max, this.A0A), C12980iv.A05(max2, this.A0B)), this.A09);
                        long eventTime = motionEvent.getEventTime() - this.A0E;
                        if (eventTime > 0) {
                            float f2 = (float) eventTime;
                            this.A05 = ((motionEvent.getRawX() - this.A03) * 1000.0f) / f2;
                            this.A06 = ((motionEvent.getRawY() - this.A04) * 1000.0f) / f2;
                        }
                        this.A03 = motionEvent.getRawX();
                        this.A04 = motionEvent.getRawY();
                        this.A0E = motionEvent.getEventTime();
                        return true;
                    }
                }
                return true;
            } else if (this.A0F == null) {
                A0f = "videoPipParticipantView/onTouch ACTION_UP dispatched before ACTION_DOWN, ignore";
            } else {
                int i10 = this.A09;
                double d = this.A00;
                if (((double) i10) < d / 60.0d) {
                    StringBuilder A0k2 = C12960it.A0k("videoPipParticipantView/onTouch ACTION_UP treat as click event  maxDistance: ");
                    A0k2.append(i10);
                    A0k2.append(", screenLength: ");
                    A0k2.append(d);
                    C12960it.A1F(A0k2);
                    view.performClick();
                    AnonymousClass5RO r02 = videoCallParticipantViewLayout.A0F;
                    if (r02 != null) {
                        voipActivityV2 = ((C1101254i) r02).A00;
                        voipActivityV2.A1m = false;
                        voipActivityV2.A37();
                        return true;
                    }
                    return true;
                }
                float f3 = this.A05;
                float f4 = this.A06;
                float sqrt = (float) Math.sqrt((double) ((f3 * f3) + (f4 * f4)));
                boolean A1U = C12960it.A1U((((double) sqrt) > (d / 1.0d) ? 1 : (((double) sqrt) == (d / 1.0d) ? 0 : -1)));
                float rawX2 = motionEvent.getRawX();
                float rawY2 = motionEvent.getRawY();
                if (A1U) {
                    double d2 = (double) ((this.A05 / sqrt) * 64.0f);
                    double d3 = (double) ((this.A06 / sqrt) * 64.0f);
                    while (rawX2 >= 0.0f && rawX2 <= ((float) this.A08) && rawY2 >= 0.0f && rawY2 <= ((float) this.A07)) {
                        rawX2 = (float) (((double) rawX2) + d2);
                        rawY2 = (float) (((double) rawY2) + d3);
                    }
                }
                boolean A01 = C28141Kv.A01(r0);
                float f5 = (float) (this.A08 / 2);
                if (!A01 ? rawX2 > f5 : rawX2 < f5) {
                    z = false;
                } else {
                    z = true;
                }
                if (rawY2 >= ((float) (this.A07 / 2))) {
                    z3 = true;
                }
                if (!(z == videoCallParticipantViewLayout.A0N && z3 == videoCallParticipantViewLayout.A0M)) {
                    videoCallParticipantViewLayout.A0N = z;
                    videoCallParticipantViewLayout.A0M = z3;
                    videoCallParticipantViewLayout.A0O = true;
                }
                AnonymousClass4RW A02 = videoCallParticipantViewLayout.A02(this.A0D, this.A0C);
                if (videoCallParticipantViewLayout.A0N) {
                    i2 = A02.A00;
                } else {
                    i2 = A02.A02;
                }
                if (videoCallParticipantViewLayout.A0M) {
                    i3 = A02.A01;
                } else {
                    i3 = A02.A03;
                }
                Point point = new Point(i2, i3);
                int i11 = point.x;
                if (C28141Kv.A01(r0)) {
                    i4 = A0H.leftMargin;
                } else {
                    i4 = A0H.rightMargin;
                }
                int i12 = i11 - i4;
                int i13 = point.y - A0H.topMargin;
                double sqrt2 = Math.sqrt((double) ((i12 * i12) + (i13 * i13)));
                long max3 = (long) Math.max(200, (int) ((500.0d * sqrt2) / this.A00));
                StringBuilder A0k3 = C12960it.A0k("VideoCallParticipantViewLayout/videoPipParticipantView/onTouch ACTION_UP xVelocity: ");
                A0k3.append(this.A05);
                A0k3.append(", yVelocity: ");
                A0k3.append(this.A06);
                A0k3.append(", velocity: ");
                A0k3.append(sqrt);
                A0k3.append(", fling: ");
                A0k3.append(A1U);
                A0k3.append(", finalRawX: ");
                A0k3.append(rawX2);
                A0k3.append(", finalRawY: ");
                A0k3.append(rawY2);
                A0k3.append(", window size: ");
                A0k3.append(videoCallParticipantViewLayout.A06);
                A0k3.append("x");
                A0k3.append(videoCallParticipantViewLayout.A05);
                A0k3.append("(");
                A0k3.append(this.A00);
                A0k3.append("), container size: ");
                A0k3.append(this.A08);
                A0k3.append("x");
                A0k3.append(this.A07);
                A0k3.append(", pipAtRight: ");
                A0k3.append(z);
                A0k3.append(", pipAtBottom: ");
                A0k3.append(z3);
                A0k3.append(", moving distance: ");
                A0k3.append(sqrt2);
                A0k3.append(", duration: ");
                A0k3.append(max3);
                C12960it.A1F(A0k3);
                StringBuilder A0k4 = C12960it.A0k("VideoCallParticipantViewLayout/animatePiPView with duration: ");
                A0k4.append(max3);
                A0k4.append(", xOffset: ");
                A0k4.append(i12);
                A0k4.append(", yOffset: ");
                A0k4.append(i13);
                A0k4.append(", final size: ");
                A0k4.append(0);
                A0k4.append("x");
                A0k4.append(0);
                C12960it.A1F(A0k4);
                if (max3 <= 0 || !videoCallParticipantViewLayout.A0K) {
                    AnonymousClass5RO r03 = videoCallParticipantViewLayout.A0F;
                    if (r03 != null) {
                        voipActivityV2 = ((C1101254i) r03).A00;
                        voipActivityV2.A1m = false;
                        voipActivityV2.A37();
                        return true;
                    }
                    return true;
                }
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                videoCallParticipantViewLayout.A07 = ofFloat;
                ofFloat.setDuration(max3);
                videoCallParticipantViewLayout.A07.addUpdateListener(new AnonymousClass3K0(videoCallParticipantViewLayout, i12, i13));
                videoCallParticipantViewLayout.A07.addListener(new IDxLAdapterShape0S0100000_1_I1(videoCallParticipantViewLayout, 2));
                videoCallParticipantViewLayout.A07.start();
                return true;
            }
        }
        Log.i(A0f);
        return true;
    }
}
