package X;

import android.view.View;
import android.widget.AdapterView;
import androidx.preference.DropDownPreference;
import androidx.preference.ListPreference;

/* renamed from: X.0Wz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07180Wz implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ DropDownPreference A00;

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView adapterView) {
    }

    public C07180Wz(DropDownPreference dropDownPreference) {
        this.A00 = dropDownPreference;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        if (i >= 0) {
            DropDownPreference dropDownPreference = this.A00;
            String charSequence = ((ListPreference) dropDownPreference).A04[i].toString();
            if (!charSequence.equals(((ListPreference) dropDownPreference).A01) && dropDownPreference.A0Q(charSequence)) {
                dropDownPreference.A0U(charSequence);
            }
        }
    }
}
