package X;

import com.whatsapp.businessdirectory.viewmodel.BusinessDirectorySearchQueryViewModel;

/* renamed from: X.2uy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59582uy extends C37171lc {
    public final int A00;
    public final BusinessDirectorySearchQueryViewModel A01;
    public final String A02;

    public C59582uy(BusinessDirectorySearchQueryViewModel businessDirectorySearchQueryViewModel, String str, int i) {
        super(AnonymousClass39o.A0a);
        this.A02 = str;
        this.A00 = i;
        this.A01 = businessDirectorySearchQueryViewModel;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
                return false;
            }
            C59582uy r5 = (C59582uy) obj;
            if (!this.A02.equals(r5.A02) || this.A00 != r5.A00 || !this.A01.equals(r5.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // X.C37171lc
    public int hashCode() {
        return this.A02.hashCode();
    }
}
