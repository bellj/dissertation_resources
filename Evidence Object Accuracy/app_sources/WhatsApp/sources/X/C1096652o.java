package X;

/* renamed from: X.52o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096652o implements AbstractC116455Vm {
    public final /* synthetic */ C15270mq A00;

    public C1096652o(C15270mq r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C15270mq r0 = this.A00;
        r0.Aak();
        AbstractC116455Vm r1 = r0.A06;
        if (r1 != null) {
            AnonymousClass5UA r02 = r0.A0A;
            if (r02 == null || r02.AJX()) {
                r1.AMr();
            }
        }
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        C15270mq r0 = this.A00;
        r0.Aak();
        AbstractC116455Vm r1 = r0.A06;
        if (r1 != null) {
            AnonymousClass5UA r02 = r0.A0A;
            if (r02 == null || r02.AJX()) {
                r1.APc(iArr);
            }
        }
    }
}
