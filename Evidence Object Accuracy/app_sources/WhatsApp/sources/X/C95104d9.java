package X;

/* renamed from: X.4d9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95104d9 {
    public static Object A00(Object obj, AnonymousClass5ZU r2, AnonymousClass5ZQ r3) {
        C16700pc.A0E(r3, 2);
        return r3.AJ5(obj, r2);
    }

    public static AnonymousClass5ZU A01(AnonymousClass5ZU r1, AbstractC115495Rt r2) {
        C16700pc.A0E(r2, 1);
        if (!C16700pc.A0O(r1.getKey(), r2)) {
            return null;
        }
        return r1;
    }

    public static AnonymousClass5X4 A02(AnonymousClass5ZU r1, AbstractC115495Rt r2) {
        C16700pc.A0E(r2, 1);
        return C16700pc.A0O(r1.getKey(), r2) ? C112775Er.A00 : r1;
    }

    public static AnonymousClass5X4 A03(AnonymousClass5ZU r1, AnonymousClass5X4 r2) {
        C16700pc.A0E(r2, 1);
        if (r2 != C112775Er.A00) {
            return (AnonymousClass5X4) r2.fold(r1, new AnonymousClass5KL());
        }
        return r1;
    }
}
