package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.25Q  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass25Q extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass25Q A06;
    public static volatile AnonymousClass255 A07;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;

    static {
        AnonymousClass25Q r0 = new AnonymousClass25Q();
        A06 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A04(1, this.A01);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A04(2, this.A04);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A04(3, this.A05);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A04(4, this.A02);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A04(5, this.A03);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A04);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A05);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0F(4, this.A02);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A03);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
