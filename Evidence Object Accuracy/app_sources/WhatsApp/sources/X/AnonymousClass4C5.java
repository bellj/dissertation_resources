package X;

/* renamed from: X.4C5  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4C5 extends Exception {
    public final int downloadStatus;

    public AnonymousClass4C5(int i) {
        this.downloadStatus = i;
    }

    @Override // java.lang.Throwable, java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("DownloadResultException{downloadStatus=");
        A0k.append(AnonymousClass1RN.A00(this.downloadStatus));
        return C12970iu.A0v(A0k);
    }
}
