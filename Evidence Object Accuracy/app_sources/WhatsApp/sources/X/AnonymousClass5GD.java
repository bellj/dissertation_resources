package X;

import java.security.SecureRandom;

/* renamed from: X.5GD  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5GD implements AnonymousClass5Wv {
    @Override // X.AnonymousClass5Wv
    public int A5m(byte[] bArr, int i) {
        int length = bArr.length;
        byte b = (byte) (length - i);
        while (i < length) {
            bArr[i] = b;
            i++;
        }
        return b;
    }

    @Override // X.AnonymousClass5Wv
    public void AIb(SecureRandom secureRandom) {
    }

    @Override // X.AnonymousClass5Wv
    public int AYq(byte[] bArr) {
        int length = bArr.length;
        int i = bArr[length - 1] & 255;
        byte b = (byte) i;
        boolean A0Y = C72463ee.A0Y(i, length) | C12960it.A1T(i);
        for (int i2 = 0; i2 < length; i2++) {
            boolean z = false;
            if (length - i2 <= i) {
                z = true;
            }
            A0Y |= z & C12980iv.A1V(bArr[i2], b);
        }
        if (!A0Y) {
            return i;
        }
        throw new C114965Nt("pad block corrupted");
    }
}
