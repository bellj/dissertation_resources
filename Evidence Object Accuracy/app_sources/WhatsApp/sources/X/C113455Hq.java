package X;

import java.security.cert.CRL;
import java.security.cert.CRLSelector;
import java.security.cert.X509CRLSelector;

/* renamed from: X.5Hq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113455Hq extends X509CRLSelector {
    public final C113105Ga A00;

    public C113455Hq(C113105Ga r3) {
        this.A00 = r3;
        CRLSelector cRLSelector = r3.A01;
        if (cRLSelector instanceof X509CRLSelector) {
            X509CRLSelector x509CRLSelector = (X509CRLSelector) cRLSelector;
            setCertificateChecking(x509CRLSelector.getCertificateChecking());
            setDateAndTime(x509CRLSelector.getDateAndTime());
            setIssuers(x509CRLSelector.getIssuers());
            setMinCRLNumber(x509CRLSelector.getMinCRL());
            setMaxCRLNumber(x509CRLSelector.getMaxCRL());
        }
    }

    @Override // java.security.cert.X509CRLSelector, java.security.cert.CRLSelector
    public boolean match(CRL crl) {
        return this.A00.ALJ(crl);
    }
}
