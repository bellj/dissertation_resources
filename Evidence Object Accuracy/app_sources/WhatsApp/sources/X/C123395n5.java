package X;

import X.AbstractC001200n;
import X.AbstractC460624h;
import X.AnonymousClass074;
import X.C123395n5;
import android.content.Context;
import com.facebook.redex.IDxCListenerShape2S0200000_3_I1;
import com.whatsapp.R;
import java.util.ArrayList;
import org.wawebrtc.MediaCodecVideoEncoder;

/* renamed from: X.5n5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123395n5 extends AbstractC118045bB {
    public AnonymousClass017 A00;
    public AnonymousClass017 A01;
    public AnonymousClass017 A02;
    public C127645up A03;
    public C122885mG A04;
    public C122935mL A05;
    public C122845mC A06;
    public C122905mI A07;
    public final C20920wX A08;
    public final C15570nT A09;
    public final C14830m7 A0A;
    public final C16590pI A0B;
    public final AnonymousClass018 A0C;
    public final AnonymousClass102 A0D;
    public final C14850m9 A0E;
    public final C25861Bc A0F;
    public final C130155yt A0G;
    public final C130125yq A0H;
    public final AnonymousClass60Y A0I;
    public final AnonymousClass61F A0J;
    public final C129235xO A0K;
    public final C129685y8 A0L;
    public final C130095yn A0M;
    public final AnonymousClass61C A0N;

    public C123395n5(C20920wX r8, C15570nT r9, C14830m7 r10, C16590pI r11, AnonymousClass018 r12, AnonymousClass102 r13, C14850m9 r14, C25861Bc r15, C130155yt r16, C130125yq r17, AnonymousClass60Y r18, AnonymousClass61F r19, C129235xO r20, C129685y8 r21, C130095yn r22, AnonymousClass61C r23) {
        super(r19);
        C122905mI r3 = new C122905mI();
        r3.A00 = 1;
        ArrayList A0l = C12960it.A0l();
        int i = 0;
        do {
            A0l.add(C31001Zq.A01(0));
            i++;
        } while (i < 3);
        r3.A03 = A0l;
        this.A07 = r3;
        this.A06 = new C122845mC();
        this.A0B = r11;
        this.A0E = r14;
        this.A0A = r10;
        this.A09 = r9;
        this.A0C = r12;
        this.A0I = r18;
        this.A08 = r8;
        this.A0G = r16;
        this.A0L = r21;
        this.A0J = r19;
        this.A0K = r20;
        this.A0H = r17;
        this.A0D = r13;
        this.A0M = r22;
        this.A0N = r23;
        this.A0F = r15;
        C122935mL A00 = C122935mL.A00(new IDxCListenerShape2S0200000_3_I1(this, 47, r18), null, r11.A00.getString(R.string.novi_manage_top_up_card), R.drawable.ic_settings, false);
        A00.A06 = false;
        this.A05 = A00;
        C122885mG r1 = new C122885mG();
        r1.A00 = 2;
        r1.A01 = null;
        this.A04 = r1;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C130095yn r1 = this.A0M;
        r1.A0C.clear();
        r1.A0B.clear();
    }

    @Override // X.AbstractC118045bB
    public void A06(AbstractC001200n r3, ActivityC13790kL r4, C125985s8 r5) {
        int i = r5.A00;
        if (i == 1) {
            AnonymousClass017 A00 = this.A0K.A00();
            this.A01 = A00;
            C117295Zj.A0s(r3, A00, this, MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT);
        } else if (i == 2) {
            AnonymousClass017 A002 = this.A0K.A00();
            this.A01 = A002;
            C117295Zj.A0s(r3, A002, this, MediaCodecVideoEncoder.MIN_ENCODER_HEIGHT);
            A09(r3);
            A08(r4, r3);
        } else {
            super.A06(r3, r4, r5);
        }
    }

    public final String A07(C1316263m r7) {
        AbstractC30791Yv r2 = r7.A02.A00;
        if (((AbstractC30781Yu) r2).A00 == 1 && ((C30801Yw) r2).A00(r7.A01.A00)) {
            return this.A0B.A00.getString(R.string.novi_conversion_summary_default);
        }
        return C12960it.A0X(this.A0B.A00, r7.A01.A06(this.A0C), new Object[1], 0, R.string.novi_conversion_summary);
    }

    public final void A08(Context context, AbstractC001200n r6) {
        this.A0J.A0B(new AbstractC136286Ly(context, this) { // from class: X.6BM
            public final /* synthetic */ Context A00;
            public final /* synthetic */ C123395n5 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC136286Ly
            public final void AT6(C1315463e r7) {
                CharSequence string;
                String str;
                C123395n5 r4 = this.A01;
                Context context2 = this.A00;
                if (r7 != null) {
                    C1316263m r5 = r7.A01;
                    if (r5 != null) {
                        AnonymousClass6F2 r2 = r5.A02;
                        string = r2.A00.AA7(context2, r2.A06(r4.A0C));
                        str = r4.A07(r5);
                    } else {
                        C130095yn r1 = r4.A0M;
                        r1.A0C.clear();
                        r1.A0B.clear();
                        string = r4.A0B.A00.getString(R.string.loading_spinner);
                        str = "";
                    }
                    C122885mG r12 = new C122885mG();
                    r12.A00 = 1;
                    r12.A02 = string;
                    r12.A03 = str;
                    r4.A04 = r12;
                }
            }
        });
        A04();
        C129685y8 r3 = this.A0L;
        C27691It A03 = C13000ix.A03();
        r3.A05.Ab2(new RunnableC135296Hx(r3, A03));
        this.A00 = A03;
        A03.A05(r6, new AnonymousClass02B(context, r6, this) { // from class: X.65h
            public final /* synthetic */ Context A00;
            public final /* synthetic */ AbstractC001200n A01;
            public final /* synthetic */ C123395n5 A02;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
            }

            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C122885mG r2;
                C123395n5 r4 = this.A02;
                Context context2 = this.A00;
                AbstractC001200n r22 = this.A01;
                C130785zy r7 = (C130785zy) obj;
                if (r7.A06()) {
                    C1316263m r5 = (C1316263m) r7.A02;
                    AnonymousClass009.A05(r5);
                    AnonymousClass6F2 r23 = r5.A02;
                    CharSequence AA7 = r23.A00.AA7(context2, r23.A06(r4.A0C));
                    String A07 = r4.A07(r5);
                    r2 = new C122885mG();
                    r2.A00 = 3;
                    r2.A02 = AA7;
                    r2.A03 = A07;
                } else {
                    C452120p r0 = r7.A00;
                    if (r0 != null && r0.A00 == 542720109) {
                        r4.A0J.A06();
                    }
                    AnonymousClass64Q r1 = 
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x004b: CONSTRUCTOR  (r1v0 'r1' X.64Q) = (r3v0 'context2' android.content.Context), (r2v0 'r22' X.00n), (r4v0 'r4' X.5n5) call: X.64Q.<init>(android.content.Context, X.00n, X.5n5):void type: CONSTRUCTOR in method: X.65h.ANq(java.lang.Object):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                        	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                        	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.64Q, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 21 more
                        */
                    /*
                        this = this;
                        X.5n5 r4 = r6.A02
                        android.content.Context r3 = r6.A00
                        X.00n r2 = r6.A01
                        X.5zy r7 = (X.C130785zy) r7
                        boolean r0 = r7.A06()
                        if (r0 == 0) goto L_0x0039
                        java.lang.Object r5 = r7.A02
                        X.63m r5 = (X.C1316263m) r5
                        X.AnonymousClass009.A05(r5)
                        X.6F2 r2 = r5.A02
                        X.1Yv r1 = r2.A00
                        X.018 r0 = r4.A0C
                        java.lang.String r0 = r2.A06(r0)
                        java.lang.CharSequence r3 = r1.AA7(r3, r0)
                        java.lang.String r1 = r4.A07(r5)
                        X.5mG r2 = new X.5mG
                        r2.<init>()
                        r0 = 3
                        r2.A00 = r0
                        r2.A02 = r3
                        r2.A03 = r1
                    L_0x0033:
                        r4.A04 = r2
                        r4.A04()
                        return
                    L_0x0039:
                        X.20p r0 = r7.A00
                        if (r0 == 0) goto L_0x0049
                        int r1 = r0.A00
                        r0 = 542720109(0x2059406d, float:1.8401932E-19)
                        if (r1 != r0) goto L_0x0049
                        X.61F r0 = r4.A0J
                        r0.A06()
                    L_0x0049:
                        X.64Q r1 = new X.64Q
                        r1.<init>(r3, r2, r4)
                        X.5mG r2 = new X.5mG
                        r2.<init>()
                        r0 = 2
                        r2.A00 = r0
                        r2.A01 = r1
                        goto L_0x0033
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C1320865h.ANq(java.lang.Object):void");
                }
            });
            AnonymousClass69L r2 = new AbstractC460624h(context, this) { // from class: X.69L
                public final /* synthetic */ Context A00;
                public final /* synthetic */ C123395n5 A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC460624h
                public final void ATX(AbstractC28901Pl r7, AnonymousClass1V8 r8) {
                    C1315463e r4;
                    C1316263m r0;
                    C123395n5 r5 = this.A01;
                    Context context2 = this.A00;
                    if (r7 != null) {
                        AnonymousClass1ZY r1 = r7.A08;
                        if ((r1 instanceof C119805f8) && (r0 = (r4 = ((C119805f8) r1).A02).A01) != null) {
                            AnonymousClass6F2 r22 = r0.A02;
                            CharSequence AA7 = r22.A00.AA7(context2, r22.A06(r5.A0C));
                            String A07 = r5.A07(r4.A01);
                            C122885mG r12 = new C122885mG();
                            r12.A00 = 3;
                            r12.A02 = AA7;
                            r12.A03 = A07;
                            r5.A04 = r12;
                            r5.A04();
                        }
                    }
                }
            };
            this.A0F.A03(r2);
            r6.ADr().A00(new AnonymousClass054(r2, this) { // from class: com.whatsapp.payments.ui.viewmodel.NoviPayHubViewModel$$ExternalSyntheticLambda14
                public final /* synthetic */ AbstractC460624h A00;
                public final /* synthetic */ C123395n5 A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AnonymousClass054
                public final void AWQ(AnonymousClass074 r4, AbstractC001200n r5) {
                    C123395n5 r22 = this.A01;
                    AbstractC460624h r1 = this.A00;
                    if (r4 == AnonymousClass074.ON_DESTROY) {
                        r22.A0F.A04(r1);
                    }
                }
            });
        }

        public void A09(AbstractC001200n r6) {
            C122905mI r4 = new C122905mI();
            r4.A00 = 1;
            ArrayList A0l = C12960it.A0l();
            int i = 0;
            do {
                A0l.add(C31001Zq.A01(0));
                i++;
            } while (i < 3);
            r4.A03 = A0l;
            this.A07 = r4;
            C130095yn r42 = this.A0M;
            AnonymousClass016 A0T = C12980iv.A0T();
            r42.A0A.Ab2(new AnonymousClass6JQ(A0T, r42, null));
            this.A02 = A0T;
            C117295Zj.A0s(r6, A0T, this, 143);
            A04();
        }
    }
