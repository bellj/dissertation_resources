package X;

import android.graphics.RectF;

/* renamed from: X.45I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass45I extends C91484Rx {
    public final int A00;
    public final String A01;

    public AnonymousClass45I(RectF rectF, String str, float f, float f2, int i, int i2) {
        super(rectF, f, f2, i);
        this.A01 = str;
        this.A00 = i2;
    }
}
