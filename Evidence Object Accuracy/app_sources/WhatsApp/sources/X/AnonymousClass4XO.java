package X;

import java.io.File;

/* renamed from: X.4XO  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XO {
    public long A00;
    public C39061pE A01;
    public boolean A02;
    public final String A03;
    public final long[] A04;
    public final /* synthetic */ C39041pA A05;

    public /* synthetic */ AnonymousClass4XO(C39041pA r2, String str) {
        this.A05 = r2;
        this.A03 = str;
        this.A04 = new long[r2.A05];
    }

    public File A00(int i) {
        File file = this.A05.A07;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A03);
        return new File(file, C12960it.A0e(".", A0h, i));
    }

    public File A01(int i) {
        File file = this.A05.A07;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A03);
        A0h.append(".");
        A0h.append(i);
        return new File(file, C12960it.A0d(".tmp", A0h));
    }
}
