package X;

import android.view.View;

/* renamed from: X.5Jv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114005Jv extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C114005Jv() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public Object AJ4(Object obj) {
        View view = (View) obj;
        C16700pc.A0E(view, 0);
        return new C850440x(view);
    }
}
