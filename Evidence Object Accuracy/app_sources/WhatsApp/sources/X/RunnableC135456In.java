package X;

/* renamed from: X.6In  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class RunnableC135456In implements Runnable {
    public final /* synthetic */ C127905vF A00;
    public final /* synthetic */ C118145bL A01;

    public /* synthetic */ RunnableC135456In(C127905vF r1, C118145bL r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        C118145bL r2 = this.A01;
        C127905vF r1 = this.A00;
        AbstractC15340mz r10 = r1.A04;
        if (r10 instanceof C30061Vy) {
            C30061Vy r102 = (C30061Vy) r10;
            C129175xI r3 = r2.A0j;
            AnonymousClass1KS A1C = r102.A1C();
            AnonymousClass1KC r9 = r1.A01;
            C120995h5 r22 = r1.A02;
            AnonymousClass6F2 r0 = r22.A01.A05.A00.A02;
            r3.A00(r0.A00, r0.A01, r1.A00, r1.A03, null, r9, r102, A1C, null, null, r22.A05, false);
            return;
        }
        C18610sj r4 = r2.A0R;
        C120995h5 r23 = r1.A02;
        AnonymousClass6F2 r02 = r23.A01.A05.A00.A02;
        r4.A02(r02.A00, r02.A01, r1.A00, r1.A03, null, r10, null, r23.A05, false);
    }
}
