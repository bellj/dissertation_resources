package X;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import com.facebook.redex.IDxEventShape6S0200000_2_I1;
import java.io.File;
import java.util.Iterator;

/* renamed from: X.36Q  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass36Q extends AbstractC28651Ol {
    public int A00 = 0;
    public C100564m7 A01;
    public AbstractC47452At A02;
    public AbstractC15710nm A03;
    public AnonymousClass4KR A04;
    public boolean A05;
    public final Uri A06;
    public final C47492Ax A07;
    public final C77353n9 A08;

    public /* synthetic */ AnonymousClass36Q(Context context, C14850m9 r9, File file, int i) {
        AnonymousClass5SH r2;
        if (r9 == null || !r9.A07(793)) {
            C106484vm r22 = new C106484vm(context);
            r22.A00 = 1;
            r2 = r22;
        } else {
            r2 = new AnonymousClass5SH() { // from class: X.4vj
                @Override // X.AnonymousClass5SH
                public final AbstractC117055Yb[] A8T(Handler handler, AbstractC72373eU r5, AnonymousClass5SO r6, AnonymousClass5SS r7, AnonymousClass5XS r8) {
                    return new AbstractC117055Yb[]{C76563lq.this};
                }
            };
        }
        this.A06 = Uri.fromFile(file);
        C77353n9 r1 = new C77353n9(context);
        this.A08 = r1;
        this.A01 = null;
        this.A05 = false;
        C47492Ax A00 = AnonymousClass3A4.A00(context, new C106404ve(), r2, r1);
        this.A07 = A00;
        AnonymousClass3HR r6 = new AnonymousClass3HR(i == 0 ? 2 : 1);
        A00.A03();
        if (!A00.A0I) {
            if (!AnonymousClass3JZ.A0H(A00.A0B, r6)) {
                A00.A0B = r6;
                A00.A09(r6, 1, 3);
                A00.A0R.A03(AnonymousClass3JZ.A02(r6.A02));
                C106424vg r5 = A00.A0U;
                AnonymousClass4XT A03 = r5.A03(r5.A06.A02);
                r5.A05(A03, new IDxEventShape6S0200000_2_I1(r6, 5, A03), 1016);
                Iterator it = A00.A0V.iterator();
                if (it.hasNext()) {
                    it.next();
                    throw C12980iv.A0n("onAudioAttributesChanged");
                }
            }
            AnonymousClass3FG r0 = A00.A0O;
            boolean AFj = A00.AFj();
            A00.A03();
            int i2 = -1;
            r0.A00();
            i2 = AFj ? 1 : i2;
            int i3 = 1;
            if (AFj && i2 != 1) {
                i3 = 2;
            }
            A00.A06(i2, i3, AFj);
        }
        A00.A5h(new C67623Sf(this));
    }
}
