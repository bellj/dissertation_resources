package X;

import android.os.Build;
import android.os.Process;
import android.os.UserManager;
import com.whatsapp.util.Log;

/* renamed from: X.18f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C251318f {
    public Boolean A00;
    public final C15450nH A01;
    public final AnonymousClass01d A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final C20660w7 A05;

    public C251318f(C15450nH r1, AnonymousClass01d r2, C16590pI r3, C14820m6 r4, C20660w7 r5) {
        this.A03 = r3;
        this.A05 = r5;
        this.A01 = r1;
        this.A02 = r2;
        this.A04 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0053, code lost:
        if (r5.A03 == null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (221770000 != r3) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(java.lang.String r7) {
        /*
            r6 = this;
            boolean r0 = r6.A01()
            if (r0 == 0) goto L_0x00c0
            X.0m6 r0 = r6.A04
            android.content.SharedPreferences r4 = r0.A00
            java.lang.String r1 = "fbns_token"
            r0 = 0
            java.lang.String r2 = r4.getString(r1, r0)
            java.lang.String r1 = "fbns_app_vers"
            r0 = 0
            int r3 = r4.getInt(r1, r0)
            r1 = 221770000(0xd37f110, float:5.6681397E-31)
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            if (r0 != 0) goto L_0x002a
            boolean r0 = r7.equals(r2)
            if (r0 == 0) goto L_0x002a
            r2 = 0
            if (r1 == r3) goto L_0x002b
        L_0x002a:
            r2 = 1
        L_0x002b:
            android.content.SharedPreferences$Editor r1 = r4.edit()
            java.lang.String r0 = "last_server_fbns_token"
            android.content.SharedPreferences$Editor r0 = r1.putString(r0, r7)
            r0.apply()
            if (r2 == 0) goto L_0x00bb
            X.07z r2 = new X.07z
            r2.<init>()
            X.0pI r0 = r6.A03
            android.content.Context r1 = r0.A00
            java.lang.String r0 = X.AnonymousClass029.A0B
            X.080 r5 = new X.080
            r5.<init>(r1, r2, r0)
            X.07z r0 = r5.A01
            boolean r3 = r0.A00
            if (r3 == 0) goto L_0x0055
            java.lang.String r0 = r5.A03
            r2 = 1
            if (r0 != 0) goto L_0x0056
        L_0x0055:
            r2 = 0
        L_0x0056:
            java.lang.String r1 = "FbnsTokenManager/requestFbnsToken fbns-enabled:"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.i(r0)
            if (r2 == 0) goto L_0x00c0
            if (r3 == 0) goto L_0x00c0
            java.lang.String r4 = r5.A03
            if (r4 == 0) goto L_0x00c0
            java.lang.String r0 = "com.facebook.rti.fbns.intent.REGISTER"
            android.content.Intent r3 = new android.content.Intent
            r3.<init>(r0)
            android.content.Context r2 = r5.A00
            java.lang.String r1 = r2.getPackageName()
            java.lang.String r0 = "pkg_name"
            r3.putExtra(r0, r1)
            java.lang.String r1 = r5.A02
            java.lang.String r0 = "appid"
            r3.putExtra(r0, r1)
            java.lang.String r0 = "com.facebook.services"
            boolean r0 = r0.equals(r4)
            if (r0 != 0) goto L_0x0098
            java.lang.String r0 = "com.facebook.services.dev"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x00c0
        L_0x0098:
            java.lang.String r1 = "com.facebook.oxygen.services.fbns.PreloadedFbnsService"
            android.content.ComponentName r0 = new android.content.ComponentName     // Catch: IllegalStateException -> 0x00b3, SecurityException -> 0x00c0, RuntimeException -> 0x00a9
            r0.<init>(r4, r1)     // Catch: IllegalStateException -> 0x00b3, SecurityException -> 0x00c0, RuntimeException -> 0x00a9
            r3.setComponent(r0)     // Catch: IllegalStateException -> 0x00b3, SecurityException -> 0x00c0, RuntimeException -> 0x00a9
            X.AnonymousClass081.A00(r2, r3)     // Catch: IllegalStateException -> 0x00b3, SecurityException -> 0x00c0, RuntimeException -> 0x00a9
            r2.startService(r3)     // Catch: IllegalStateException -> 0x00b3, SecurityException -> 0x00c0, RuntimeException -> 0x00a9
            return
        L_0x00a9:
            r1 = move-exception
            java.lang.Throwable r0 = r1.getCause()
            boolean r0 = r0 instanceof android.os.DeadObjectException
            if (r0 != 0) goto L_0x00c0
            throw r1
        L_0x00b3:
            r2 = move-exception
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 26
            if (r1 >= r0) goto L_0x00c0
            throw r2
        L_0x00bb:
            java.lang.String r0 = "FbnsTokenManager/verifyFbnsToken no-need-to-refresh"
            com.whatsapp.util.Log.i(r0)
        L_0x00c0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C251318f.A00(java.lang.String):void");
    }

    public boolean A01() {
        boolean booleanValue;
        UserManager A0J;
        C15450nH r1 = this.A01;
        boolean A05 = r1.A05(AbstractC15460nI.A0S);
        boolean A052 = r1.A05(AbstractC15460nI.A0R);
        synchronized (this) {
            Boolean bool = this.A00;
            if (bool == null) {
                boolean z = true;
                if (Build.VERSION.SDK_INT >= 17 && (A0J = this.A02.A0J()) != null) {
                    try {
                        if (A0J.getSerialNumberForUser(Process.myUserHandle()) != 0) {
                            z = false;
                        }
                    } catch (Exception e) {
                        Log.e("FbnsTokenManager/isAdminUser", e);
                    }
                }
                bool = Boolean.valueOf(z);
                this.A00 = bool;
            }
            booleanValue = bool.booleanValue();
        }
        if (A05) {
            return !A052 || booleanValue;
        }
        return false;
    }
}
