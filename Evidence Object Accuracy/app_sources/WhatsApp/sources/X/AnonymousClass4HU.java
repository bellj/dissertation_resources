package X;

/* renamed from: X.4HU  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4HU {
    public static final AnonymousClass5QS A00 = new C108124yX();
    public static final AbstractC77683ng A01;
    public static final AnonymousClass4DN A02;
    public static final AnonymousClass1UE A03;

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A02 = r3;
        C77593nX r2 = new C77593nX();
        A01 = r2;
        A03 = new AnonymousClass1UE(r2, r3, "Auth.PROXY_API");
    }
}
