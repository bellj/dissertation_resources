package X;

import com.whatsapp.util.Log;

/* renamed from: X.3eF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72243eF extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $onError;
    public final /* synthetic */ C18190s3 this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72243eF(C18190s3 r2, AnonymousClass1J7 r3) {
        super(1);
        this.this$0 = r2;
        this.$onError = r3;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Throwable th = (Throwable) obj;
        C16700pc.A0E(th, 0);
        Log.e(th);
        AnonymousClass1WI.A00(this.this$0.A00, this.$onError, th, 41);
        return AnonymousClass1WZ.A00;
    }
}
