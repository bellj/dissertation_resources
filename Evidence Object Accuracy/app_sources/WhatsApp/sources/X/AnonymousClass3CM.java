package X;

import com.whatsapp.community.deactivate.DeactivateCommunityDisclaimerActivity;

/* renamed from: X.3CM  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3CM {
    public final /* synthetic */ DeactivateCommunityDisclaimerActivity A00;

    public AnonymousClass3CM(DeactivateCommunityDisclaimerActivity deactivateCommunityDisclaimerActivity) {
        this.A00 = deactivateCommunityDisclaimerActivity;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        if (r5 != 404) goto L_0x001a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(int r5) {
        /*
            r4 = this;
            com.whatsapp.community.deactivate.DeactivateCommunityDisclaimerActivity r1 = r4.A00
            r1.AaN()
            r0 = -2
            r3 = -1
            if (r5 == r0) goto L_0x001a
            if (r5 == r3) goto L_0x001a
            r0 = 400(0x190, float:5.6E-43)
            if (r5 == r0) goto L_0x001a
            r0 = 401(0x191, float:5.62E-43)
            if (r5 == r0) goto L_0x0028
            r0 = 404(0x194, float:5.66E-43)
            r2 = 2131887597(0x7f1205ed, float:1.9409806E38)
            if (r5 == r0) goto L_0x001d
        L_0x001a:
            r2 = 2131887600(0x7f1205f0, float:1.9409812E38)
        L_0x001d:
            android.view.View r1 = r1.A00
            if (r1 != 0) goto L_0x002c
            java.lang.String r0 = "mainView"
            java.lang.RuntimeException r0 = X.C16700pc.A06(r0)
            throw r0
        L_0x0028:
            r2 = 2131887596(0x7f1205ec, float:1.9409804E38)
            goto L_0x001d
        L_0x002c:
            android.content.res.Resources r0 = r1.getResources()
            java.lang.CharSequence r0 = r0.getText(r2)
            X.1fr r0 = X.C34271fr.A00(r1, r0, r3)
            r0.A03()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3CM.A00(int):void");
    }
}
