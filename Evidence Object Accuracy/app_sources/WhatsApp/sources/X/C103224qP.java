package X;

import android.content.Context;
import com.whatsapp.migration.export.ui.ExportMigrationDataExportedActivity;

/* renamed from: X.4qP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103224qP implements AbstractC009204q {
    public final /* synthetic */ ExportMigrationDataExportedActivity A00;

    public C103224qP(ExportMigrationDataExportedActivity exportMigrationDataExportedActivity) {
        this.A00 = exportMigrationDataExportedActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
