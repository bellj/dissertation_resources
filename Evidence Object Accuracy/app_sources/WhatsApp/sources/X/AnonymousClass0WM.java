package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0WM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WM implements View.OnClickListener {
    public Context A00;
    public Method A01;
    public final View A02;
    public final String A03;

    public AnonymousClass0WM(View view, String str) {
        this.A02 = view;
        this.A03 = str;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        String obj;
        Method method = this.A01;
        if (method == null) {
            View view2 = this.A02;
            for (Context context = view2.getContext(); context != null; context = ((ContextWrapper) context).getBaseContext()) {
                try {
                    if (!context.isRestricted() && (method = context.getClass().getMethod(this.A03, View.class)) != null) {
                        this.A01 = method;
                        this.A00 = context;
                    }
                } catch (NoSuchMethodException unused) {
                }
                if (!(context instanceof ContextWrapper)) {
                    break;
                }
            }
            int id = view2.getId();
            if (id == -1) {
                obj = "";
            } else {
                StringBuilder sb = new StringBuilder(" with id '");
                sb.append(view2.getContext().getResources().getResourceEntryName(id));
                sb.append("'");
                obj = sb.toString();
            }
            StringBuilder sb2 = new StringBuilder("Could not find method ");
            sb2.append(this.A03);
            sb2.append("(View) in a parent or ancestor Context for android:onClick attribute defined on view ");
            sb2.append(view2.getClass());
            sb2.append(obj);
            throw new IllegalStateException(sb2.toString());
        }
        try {
            method.invoke(this.A00, view);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Could not execute non-public method for android:onClick", e);
        } catch (InvocationTargetException e2) {
            throw new IllegalStateException("Could not execute method for android:onClick", e2);
        }
    }
}
