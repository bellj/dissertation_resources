package X;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.redex.RunnableBRunnable0Shape0S0301000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0311000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0300000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.facebook.simplejni.NativeHolder;
import com.whatsapp.R;
import com.whatsapp.data.ProfilePhotoChange;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.report.ReportActivity;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0qV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC17250qV implements AbstractC15920o8 {
    public final AbstractC15710nm A00;
    public final C17220qS A01;
    public final C17230qT A02;
    public final ExecutorC27271Gr A03;
    public final Set A04 = new HashSet();
    public final AtomicReference A05 = new AtomicReference();
    public final boolean A06;
    public final int[] A07;

    public AbstractC17250qV(AbstractC15710nm r3, C17220qS r4, C17230qT r5, AbstractC14440lR r6, int[] iArr, boolean z) {
        ExecutorC27271Gr r0;
        this.A00 = r3;
        this.A01 = r4;
        this.A02 = r5;
        this.A07 = iArr;
        this.A06 = z;
        if (z) {
            r0 = new ExecutorC27271Gr(r6, false);
        } else {
            r0 = null;
        }
        this.A03 = r0;
    }

    public void A00(AnonymousClass1V8 r18, int i) {
        UserJid of;
        String str;
        boolean z;
        C42251uu r1;
        C20730wE r0;
        AnonymousClass1V8 A0D;
        int i2;
        ProfilePhotoChange profilePhotoChange;
        String num;
        AnonymousClass1V8 A0D2;
        AnonymousClass1V8 A0D3;
        Jid A0B;
        byte[] bArr;
        byte[] bArr2;
        StringBuilder sb;
        String str2;
        String str3;
        AnonymousClass1V8 A0E;
        AnonymousClass1V8 A0E2;
        AnonymousClass1V8 A0D4;
        AnonymousClass1V8 A0D5;
        if (this instanceof AnonymousClass10R) {
            AnonymousClass10R r02 = (AnonymousClass10R) this;
            if (i == 234) {
                AnonymousClass1V8 A0F = r18.A0F("tokens");
                HashSet hashSet = new HashSet();
                for (AnonymousClass1V8 r4 : A0F.A0J("token")) {
                    String A0H = r4.A0H("type");
                    if ("trusted_contact".equals(A0H)) {
                        UserJid of2 = UserJid.of(r18.A0B(r02.A00, AbstractC14640lm.class, "from"));
                        if (of2 == null) {
                            continue;
                        } else {
                            byte[] bArr3 = r4.A01;
                            if (bArr3 == null) {
                                throw new AnonymousClass1V9("required token element to contain data");
                            } else if (r02.A02.A00(of2, bArr3, r4.A08("t", r18.A09(r18.A0H("t"), "t"))) != 2) {
                                hashSet.add(of2);
                            }
                        }
                    } else if ("trusted_contact_outgoing".equals(A0H) && (of = UserJid.of(r4.A0B(r02.A00, AbstractC14640lm.class, "jid"))) != null) {
                        r02.A02.A08(of, Long.valueOf(r4.A09(r4.A0H("t"), "t")));
                    }
                }
                Iterator it = hashSet.iterator();
                while (it.hasNext()) {
                    r02.A01.A0H(new RunnableBRunnable0Shape7S0200000_I0_7(r02, 20, it.next()));
                }
            }
        } else if (this instanceof C230110a) {
            C230110a r03 = (C230110a) this;
            if (i == 189 && (A0D = r18.A0D(0)) != null) {
                AbstractC15710nm r8 = r03.A00;
                AbstractC14640lm r3 = (AbstractC14640lm) A0D.A0A(r8, AbstractC14640lm.class, "jid");
                if (r3 != null) {
                    long A01 = C28421Nd.A01(r18.A0I("t", null), System.currentTimeMillis());
                    AbstractC14640lm r7 = (AbstractC14640lm) A0D.A0A(r8, UserJid.class, "author");
                    String A0H2 = r18.A0H("id");
                    if (AnonymousClass1V8.A02(A0D, "set")) {
                        i2 = C28421Nd.A00(A0D.A0I("id", null), -1);
                    } else if (AnonymousClass1V8.A02(A0D, "delete")) {
                        i2 = -1;
                    } else if (AnonymousClass1V8.A02(A0D, "request")) {
                        StringBuilder sb2 = new StringBuilder("ProfilePictureNotificationHandler/onProfilePhotoLost ");
                        sb2.append(r3);
                        Log.i(sb2.toString());
                        if (!C27611If.A00(r3)) {
                            C15570nT r12 = r03.A01;
                            r12.A08();
                            C27621Ig r2 = r12.A01;
                            if (r2 != null && r12.A0F(r3)) {
                                r03.A07.A09(r2);
                                return;
                            }
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                    StringBuilder sb3 = new StringBuilder("ProfilePictureNotificationHandler/onProfilePhotoChange ");
                    sb3.append(r3);
                    sb3.append(" author:");
                    sb3.append(r7);
                    sb3.append(" photoId:");
                    sb3.append(i2);
                    Log.i(sb3.toString());
                    C15370n3 A0B2 = r03.A02.A0B(r3);
                    C15570nT r42 = r03.A01;
                    r42.A08();
                    if (r42.A05 == null) {
                        throw new AssertionError("local JID unknown");
                    } else if (A0B2.A04 != i2 || A0B2.A05 != i2) {
                        AnonymousClass10Z r122 = r03.A07;
                        boolean z2 = true;
                        C15370n3 A0B3 = r122.A05.A0B(r3);
                        if (!(A0B3.A04 == i2 && A0B3.A05 == i2)) {
                            if (i2 == -1) {
                                r122.A09.A02(r3);
                            } else {
                                r122.A00.A01(new RunnableBRunnable0Shape0S0311000_I0(A0B3, r3, r122, i2));
                            }
                        }
                        if (A0B2.A0K()) {
                            File A012 = r03.A03.A01(A0B2);
                            if (A012 == null || !A012.exists()) {
                                profilePhotoChange = null;
                            } else {
                                profilePhotoChange = new ProfilePhotoChange();
                                profilePhotoChange.newPhotoId = i2;
                                try {
                                    profilePhotoChange.oldPhoto = AnonymousClass1NM.A00(A012);
                                } catch (IOException e) {
                                    Log.w("app/xmpp/recv/handle_profile_photo_changed/", e);
                                }
                            }
                            C30501Xq r5 = new C30501Xq(new AnonymousClass1IS(r3, A0H2, true), A01 * 1000);
                            if (i2 == -1) {
                                num = null;
                            } else {
                                num = Integer.toString(i2);
                            }
                            r5.A0l(num);
                            r5.A0e(r7);
                            r5.A00 = profilePhotoChange;
                            AbstractC15340mz A013 = r03.A06.A01(r3);
                            if (A013 instanceof AnonymousClass1XB) {
                                AnonymousClass1XB r43 = (AnonymousClass1XB) A013;
                                boolean z3 = false;
                                if (r43.A00 == 11) {
                                    z3 = true;
                                }
                                if (r5.A0B() == null || !r43.A14() || !r7.equals(r43.A0B())) {
                                    z2 = false;
                                }
                                if (z3 && z2) {
                                    return;
                                }
                            }
                            r03.A05.A0S(r5);
                        }
                    }
                } else {
                    String A0I = A0D.A0I("hash", null);
                    if (A0I != null) {
                        AnonymousClass1JB r44 = AnonymousClass1JB.A0H;
                        if (!TextUtils.isEmpty(A0I)) {
                            byte[] decode = Base64.decode(A0I.getBytes(), 0);
                            Arrays.toString(decode);
                            C42271uw r13 = new C42271uw(AnonymousClass1JA.A0G);
                            z = true;
                            r13.A02 = true;
                            r13.A00 = r44;
                            r13.A02(decode);
                            r1 = r13.A01();
                            r0 = r03.A04;
                            r0.A03(r1, z);
                            return;
                        }
                        return;
                    }
                    StringBuilder sb4 = new StringBuilder("ProfilePictureNotificationHandler/handleNotification/ignoring notification for invalid jid: ");
                    sb4.append(A0D.A0I("jid", null));
                    str = sb4.toString();
                    Log.w(str);
                }
            }
        } else if (this instanceof AnonymousClass1FT) {
            AnonymousClass1FT r04 = (AnonymousClass1FT) this;
            if (i == 236 && (A0D2 = r18.A0D(0)) != null) {
                Jid A0A = r18.A0A(((AbstractC17250qV) r04).A00, Jid.class, "from");
                if (AnonymousClass1V8.A02(A0D2, "set")) {
                    String A0I2 = A0D2.A0I("hash", null);
                    if (A0I2 != null) {
                        AnonymousClass1JB r45 = AnonymousClass1JB.A0I;
                        if (!TextUtils.isEmpty(A0I2)) {
                            byte[] decode2 = Base64.decode(A0I2.getBytes(), 0);
                            Arrays.toString(decode2);
                            C42271uw r14 = new C42271uw(AnonymousClass1JA.A0G);
                            z = true;
                            r14.A02 = true;
                            r14.A00 = r45;
                            r14.A02(decode2);
                            r1 = r14.A01();
                            r0 = r04.A01;
                            r0.A03(r1, z);
                            return;
                        }
                        return;
                    }
                    String A0G = A0D2.A0G();
                    String A0I3 = r18.A0I("t", null);
                    if (A0I3 != null) {
                        r04.A00.A0S(UserJid.of(A0A), A0G, Long.parseLong(A0I3) * 1000);
                    }
                } else if (AnonymousClass1V8.A02(A0D2, "delete")) {
                    UserJid of3 = UserJid.of(A0A);
                    C15550nR r46 = r04.A00;
                    r46.A06.A0O(of3, null, 0);
                    r46.A04.A01.remove(of3);
                    r46.A00.post(new RunnableBRunnable0Shape2S0200000_I0_2(r46, 24, of3));
                }
            }
        } else if (this instanceof AnonymousClass1FU) {
            AnonymousClass1FU r05 = (AnonymousClass1FU) this;
            if (i == 235 && (A0D3 = r18.A0D(0)) != null) {
                if (AnonymousClass1V8.A02(A0D3, "log")) {
                    AnonymousClass10G r22 = r05.A00;
                    r22.A00.A06();
                    Log.i(r22.A00.A04(r05.A01.A00, null, "", null, null, "NotCalculated", null, null, -1, -1, true, false));
                    ((AbstractC17250qV) r05).A00.AaV("AppMessagingXmppHandler/onLogNotification", null, true);
                } else if (AnonymousClass1V8.A02(A0D3, "props")) {
                    r05.A03.A0M(true);
                } else if (AnonymousClass1V8.A02(A0D3, "abprops")) {
                    r05.A02.A00();
                }
            }
        } else if (this instanceof C26941Fk) {
            C26941Fk r06 = (C26941Fk) this;
            if (i == 97) {
                AnonymousClass1V8 A0E3 = r18.A0E("rmr");
                boolean z4 = false;
                if (A0E3 != null) {
                    if ("true".equals(A0E3.A0H("from_me"))) {
                        z4 = true;
                    }
                    A0B = A0E3.A0B(((AbstractC17250qV) r06).A00, Jid.class, "jid");
                } else {
                    A0B = r18.A0B(((AbstractC17250qV) r06).A00, Jid.class, "from");
                }
                AbstractC14640lm A00 = C15380n4.A00(A0B);
                String A0H3 = r18.A0H("id");
                AnonymousClass1IS r32 = new AnonymousClass1IS(A00, A0H3, z4);
                StringBuilder sb5 = new StringBuilder("media retry notification received; stanzaKeyId=");
                sb5.append(A0H3);
                sb5.append("; key=");
                sb5.append(r32);
                Log.i(sb5.toString());
                C15650ng r82 = r06.A00;
                AbstractC15340mz A03 = r82.A0K.A03(r32);
                if (A03 instanceof AbstractC16130oV) {
                    AbstractC16130oV r47 = (AbstractC16130oV) A03;
                    C16150oX r72 = r47.A02;
                    if (r72 != null) {
                        StringBuilder sb6 = new StringBuilder("media auto download re-enabled; stanzaKeyId=");
                        sb6.append(A0H3);
                        sb6.append("; key=");
                        sb6.append(r32);
                        Log.i(sb6.toString());
                        AnonymousClass1V8 A0E4 = r18.A0E("encrypt");
                        String str4 = null;
                        if (A0E4 == null || (A0E2 = A0E4.A0E("enc_p")) == null) {
                            bArr = null;
                        } else {
                            bArr = A0E2.A01;
                        }
                        if (A0E4 == null || (A0E = A0E4.A0E("enc_iv")) == null) {
                            bArr2 = null;
                        } else {
                            bArr2 = A0E.A01;
                        }
                        try {
                            String str5 = r32.A01;
                            byte[] bArr4 = r72.A0U;
                            AnonymousClass009.A05(bArr4);
                            if (!(bArr == null || bArr2 == null)) {
                                AnonymousClass104.A00(bArr4, bArr2);
                                NativeHolder nativeHolder = (NativeHolder) JniBridge.jvidispatchOOOOO(3, str5, bArr, bArr4, bArr2);
                                if (nativeHolder != null) {
                                    C43111wQ r15 = new C43111wQ(nativeHolder);
                                    JniBridge.getInstance();
                                    str4 = (String) JniBridge.jvidispatchOIO(1, (long) 30, r15.A00);
                                } else {
                                    throw new C28971Pt("encrypted message id is different from the expected one");
                                }
                            }
                            if (!TextUtils.isEmpty(str4)) {
                                r72.A0G = str4;
                            }
                            r72.A0L = true;
                            r82.A0W(r47);
                            C22310ys r33 = r06.A01;
                            C22370yy r73 = r33.A06;
                            C16150oX r6 = r47.A02;
                            if (r6 == null) {
                                sb = new StringBuilder();
                                str2 = "MediaDownloadManager/resumeReuploadingDownload/MMS unable to resume download due to missing media data; message.key = ";
                            } else if (!r6.A0a) {
                                sb = new StringBuilder();
                                str2 = "MediaDownloadManager/resumeReuploadingDownload/MMS unable to resume download; not transferring; message.key = ";
                            } else {
                                C28921Pn A002 = r73.A0V.A00(r6);
                                if (A002 != null) {
                                    if (str4 != null) {
                                        C28781Oz r23 = A002.A0Z;
                                        synchronized (r23) {
                                            r23.A0G = str4;
                                        }
                                    }
                                    A002.A0q.countDown();
                                    str3 = "media retry notification; resume reuploading download";
                                    Log.i(str3);
                                }
                                sb = new StringBuilder();
                                str2 = "MediaDownloadManager/resumeReuploadingDownload/MMS unable to resume download; downloader not found; message.key = ";
                            }
                            sb.append(str2);
                            sb.append(r47.A0z);
                            sb.append(", message.mediaHash=");
                            sb.append(r47.A05);
                            Log.e(sb.toString());
                            r33.A01(r47, r33.A02.A05(true), false);
                            str3 = "media retry notification; queue auto download";
                            Log.i(str3);
                        } catch (C28971Pt e2) {
                            Log.w("malformed encrypted data", e2);
                        }
                    } else {
                        StringBuilder sb7 = new StringBuilder("missing media data for media message; stanzaKeyId=");
                        sb7.append(A0H3);
                        sb7.append("; key=");
                        sb7.append(r32);
                        str = sb7.toString();
                        Log.w(str);
                    }
                }
            }
        } else if (this instanceof AnonymousClass1FN) {
            AnonymousClass1FN r07 = (AnonymousClass1FN) this;
            if (i == 238 && (A0D4 = r18.A0D(0)) != null) {
                byte[] bArr5 = A0D4.A01;
                C26531Dv r83 = r07.A03;
                r83.A07(bArr5, A0D4.A08("creation", 0) * 1000, A0D4.A08("expiration", (r07.A01.A00() + 2592000000L) / 1000) * 1000);
                C14900mE r48 = r07.A00;
                if (!(r48.A00 instanceof ReportActivity)) {
                    Log.i("gdpr/notify-report-available");
                    Context context = r83.A0A.A00;
                    String string = context.getString(R.string.gdpr_report_available);
                    C005602s A003 = C22630zO.A00(context);
                    A003.A0J = "other_notifications@1";
                    A003.A0B(string);
                    A003.A05(System.currentTimeMillis());
                    A003.A02(3);
                    A003.A0D(true);
                    A003.A0A(context.getString(R.string.app_name));
                    A003.A09(string);
                    Intent intent = new Intent();
                    intent.setClassName(context.getPackageName(), "com.whatsapp.report.ReportActivity");
                    A003.A09 = AnonymousClass1UY.A00(context, 0, intent, 0);
                    C18360sK.A01(A003, R.drawable.notifybar);
                    r83.A0B.A03(16, A003.A01());
                }
                C16440p1 A032 = r83.A03();
                if (A032 != null) {
                    r48.A0H(new RunnableBRunnable0Shape7S0200000_I0_7(r07, 4, A032));
                }
            }
        } else if (this instanceof AnonymousClass1FV) {
            AnonymousClass1FV r08 = (AnonymousClass1FV) this;
            if (i == 241 && (A0D5 = r18.A0D(0)) != null) {
                Object andSet = ((AbstractC17250qV) r08).A05.getAndSet(null);
                AnonymousClass009.A05(andSet);
                AnonymousClass1OT r49 = (AnonymousClass1OT) andSet;
                ((AbstractC17250qV) r08).A04.add(r49);
                AnonymousClass2QR r24 = (AnonymousClass2QR) r08.A06.A00(2, r49.A00);
                if (r24 != null) {
                    r24.A00 = A0D5.A00;
                }
                if (AnonymousClass1V8.A02(A0D5, "count")) {
                    int A06 = A0D5.A06(A0D5.A0H("value"), "value");
                    StringBuilder sb8 = new StringBuilder("prekey count running low; remainingPreKeys=");
                    sb8.append(A06);
                    Log.i(sb8.toString());
                    r08.A04.A00.execute(new RunnableBRunnable0Shape7S0200000_I0_7(r08, 3, r49));
                } else if (AnonymousClass1V8.A02(A0D5, "identity")) {
                    StringBuilder sb9 = new StringBuilder("identity changed notification received; stanzaKey=");
                    sb9.append(r49);
                    Log.i(sb9.toString());
                    r08.A04.A00.submit(new RunnableBRunnable0Shape1S0300000_I0_1(r08, r49, A0D5, 33));
                } else {
                    if (AnonymousClass1V8.A02(A0D5, "digest")) {
                        StringBuilder sb10 = new StringBuilder("server asked us to run an e2e key digest check; stanzaKey=");
                        sb10.append(r49);
                        Log.i(sb10.toString());
                        r08.A02.A1A(true);
                        r08.A00.A01();
                    }
                    r08.A02(r49);
                }
            }
        } else if (this instanceof C230210b) {
            C230210b r09 = (C230210b) this;
            UserJid userJid = (UserJid) r18.A0B(r09.A00, UserJid.class, "from");
            long A08 = r18.A08("t", 0);
            AnonymousClass1V8 A0E5 = r18.A0E("disappearing_mode");
            AnonymousClass009.A05(A0E5);
            int A05 = A0E5.A05("duration", 0);
            C15370n3 A0B4 = r09.A02.A0B(userJid);
            if (A0B4.A00 != A05) {
                r09.A01.A01(new Runnable(A0B4, userJid, r09, A05, A08) { // from class: X.2QP
                    public final /* synthetic */ int A00;
                    public final /* synthetic */ long A01;
                    public final /* synthetic */ C15370n3 A02;
                    public final /* synthetic */ UserJid A03;
                    public final /* synthetic */ C230210b A04;

                    {
                        this.A04 = r3;
                        this.A03 = r2;
                        this.A00 = r4;
                        this.A01 = r5;
                        this.A02 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        C230210b r62 = this.A04;
                        UserJid userJid2 = this.A03;
                        int i3 = this.A00;
                        long j = this.A01;
                        C15370n3 r16 = this.A02;
                        r62.A02.A0R(userJid2, i3, j);
                        r16.A00 = i3;
                        r16.A07 = j;
                        r62.A03.A02(r16);
                    }
                });
            }
        } else if (this instanceof AnonymousClass16Q) {
            AnonymousClass16Q r010 = (AnonymousClass16Q) this;
            if (i == 210) {
                HashMap hashMap = new HashMap();
                for (AnonymousClass1V8 r25 : r18.A0J("collection")) {
                    String A0I4 = r25.A0I("name", null);
                    long A004 = (long) C28421Nd.A00(r25.A0I("version", null), 0);
                    if (!TextUtils.isEmpty(A0I4)) {
                        hashMap.put(A0I4, Long.valueOf(A004));
                        if (r18.A0I("offline", null) != null) {
                            long j = 0;
                            Map map = r010.A02;
                            if (map.containsKey(A0I4)) {
                                j = ((Number) map.get(A0I4)).longValue();
                            }
                            map.put(A0I4, Long.valueOf(j + 1));
                        }
                    }
                }
                C18850tA r52 = r010.A00;
                if (r52.A0Q()) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        r52.A0X.A04((String) entry.getKey(), ((Number) entry.getValue()).longValue());
                    }
                    r52.A0I();
                }
            }
        } else if (!(this instanceof C230410d)) {
            C17240qU r011 = (C17240qU) this;
            if (i == 248) {
                r011.A00.A09(5);
                if (r011.A02.A07(1689)) {
                    r011.A01.A00();
                }
            }
        } else {
            C230410d r012 = (C230410d) this;
            if (i == 228) {
                AnonymousClass1V8 A0E6 = r18.A0E("migrate");
                if (A0E6 != null) {
                    r012.A00.A0X(A0E6.A05("urgency", 0));
                } else {
                    Log.e("EncBackupNotificationHandler/migrate child node missing");
                }
            }
        }
    }

    public final void A01(AnonymousClass1V8 r7, AnonymousClass1OT r8, int i) {
        AtomicReference atomicReference = this.A05;
        boolean z = false;
        if (atomicReference.getAndSet(r8) == null) {
            z = true;
        }
        AnonymousClass009.A0F(z);
        try {
            A00(r7, i);
        } catch (AnonymousClass1V9 e) {
            StringBuilder sb = new StringBuilder("BaseNotificationHandler/handleAndAckNotification/corrupt-stream-error/stanza ");
            sb.append(e.bufString);
            sb.append(" node:");
            sb.append(r7);
            Log.w(sb.toString());
            this.A00.AaV("CorruptStreamException", e.getMessage(), false);
        }
        AnonymousClass1OT r1 = (AnonymousClass1OT) atomicReference.getAndSet(null);
        if (r1 != null && !this.A04.contains(r1)) {
            this.A01.A0B(r1);
        }
    }

    public final void A02(AnonymousClass1OT r2) {
        this.A04.remove(r2);
        this.A01.A0B(r2);
    }

    @Override // X.AbstractC15920o8
    public final int[] ADF() {
        return this.A07;
    }

    @Override // X.AbstractC15920o8
    public final boolean AI8(Message message, int i) {
        boolean z;
        int[] iArr = this.A07;
        int length = iArr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                z = false;
                break;
            } else if (i == iArr[i2]) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (!z) {
            return false;
        }
        Object obj = message.obj;
        AnonymousClass009.A05(obj);
        AnonymousClass1V8 r6 = (AnonymousClass1V8) obj;
        Parcelable parcelable = message.getData().getParcelable("stanzaKey");
        AnonymousClass009.A05(parcelable);
        AnonymousClass1OT r7 = (AnonymousClass1OT) parcelable;
        AnonymousClass2QR r1 = (AnonymousClass2QR) this.A02.A00(2, r7.A00);
        if (r1 != null) {
            AnonymousClass1V8 A0D = r6.A0D(0);
            if (A0D != null) {
                r1.A00 = A0D.A00;
            }
            r1.A02(3);
        }
        if (this.A06) {
            ExecutorC27271Gr r0 = this.A03;
            AnonymousClass009.A05(r0);
            r0.execute(new RunnableBRunnable0Shape0S0301000_I0(this, r6, r7, i, 6));
            return true;
        }
        A01(r6, r7, i);
        return true;
    }
}
