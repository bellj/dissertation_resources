package X;

import android.os.SystemClock;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.3Ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63063Ac {
    public static void A00(ActivityC13810kN r9, C16170oZ r10, C14820m6 r11, C21320xE r12, AbstractC14440lR r13, List list, boolean z) {
        r9.A2C(R.string.register_wait_message);
        r13.Aaz(new AnonymousClass382(r9, r10, r11, r12, C12970iu.A10(r9), list, SystemClock.elapsedRealtime(), z), new Object[0]);
    }
}
