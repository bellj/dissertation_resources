package X;

import android.content.Intent;

/* renamed from: X.0kT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC13870kT {
    @Override // X.AbstractC13870kT
    void startActivityForResult(Intent intent, int i);
}
