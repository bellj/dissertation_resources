package X;

import android.content.ContentValues;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.171  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass171 {
    public final C18460sU A00;
    public final C16490p7 A01;
    public final Map A02 = new HashMap();
    public final Map A03 = new HashMap();

    public AnonymousClass171(C18460sU r2, C16490p7 r3) {
        this.A00 = r2;
        this.A01 = r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0053 A[Catch: all -> 0x006b, TRY_ENTER, TRY_LEAVE, TryCatch #5 {, blocks: (B:5:0x0005, B:15:0x0056, B:16:0x0062, B:6:0x001a, B:14:0x0053, B:21:0x006a), top: B:30:0x0005 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.C30681Yj A00(X.C15580nU r12) {
        /*
            r11 = this;
            r6 = r11
            java.util.Map r1 = r11.A02
            monitor-enter(r1)
            r0 = 1
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch: all -> 0x0070
            r4 = 0
            X.0sU r0 = r11.A00     // Catch: all -> 0x0070
            long r2 = r0.A01(r12)     // Catch: all -> 0x0070
            java.lang.String r0 = java.lang.Long.toString(r2)     // Catch: all -> 0x0070
            r5[r4] = r0     // Catch: all -> 0x0070
            X.0p7 r0 = r11.A01     // Catch: all -> 0x0070
            X.0on r2 = r0.get()     // Catch: all -> 0x0070
            X.0op r3 = r2.A03     // Catch: all -> 0x006b
            java.lang.String r0 = "SELECT subject_timestamp, announcement_version FROM group_notification_version WHERE group_jid_row_id = ?"
            android.database.Cursor r3 = r3.A09(r0, r5)     // Catch: all -> 0x006b
            if (r3 == 0) goto L_0x0045
            boolean r0 = r3.moveToNext()     // Catch: all -> 0x0064
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "subject_timestamp"
            int r0 = r3.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0064
            long r7 = r3.getLong(r0)     // Catch: all -> 0x0064
            java.lang.String r0 = "announcement_version"
            int r0 = r3.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0064
            long r9 = r3.getLong(r0)     // Catch: all -> 0x0064
            X.1Yj r5 = new X.1Yj     // Catch: all -> 0x0064
            r5.<init>(r6, r7, r9)     // Catch: all -> 0x0064
            goto L_0x004e
        L_0x0045:
            r7 = 0
            r9 = 0
            X.1Yj r5 = new X.1Yj     // Catch: all -> 0x0064
            r5.<init>(r6, r7, r9)     // Catch: all -> 0x0064
        L_0x004e:
            r1.put(r12, r5)     // Catch: all -> 0x0064
            if (r3 == 0) goto L_0x0056
            r3.close()     // Catch: all -> 0x006b
        L_0x0056:
            r2.close()     // Catch: all -> 0x0070
            java.lang.Object r0 = r1.get(r12)     // Catch: all -> 0x0070
            X.1Yj r0 = (X.C30681Yj) r0     // Catch: all -> 0x0070
            X.AnonymousClass009.A05(r0)     // Catch: all -> 0x0070
            monitor-exit(r1)     // Catch: all -> 0x0070
            return r0
        L_0x0064:
            r0 = move-exception
            if (r3 == 0) goto L_0x006a
            r3.close()     // Catch: all -> 0x006a
        L_0x006a:
            throw r0     // Catch: all -> 0x006b
        L_0x006b:
            r0 = move-exception
            r2.close()     // Catch: all -> 0x006f
        L_0x006f:
            throw r0     // Catch: all -> 0x0070
        L_0x0070:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0070
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass171.A00(X.0nU):X.1Yj");
    }

    public final void A01(C15580nU r6, long j, long j2, long j3) {
        C16310on A02 = this.A01.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("group_jid_row_id", Long.valueOf(this.A00.A01(r6)));
            contentValues.put("subject_timestamp", Long.valueOf(j));
            contentValues.put("announcement_version", Long.valueOf(j2));
            contentValues.put("participant_version", Long.valueOf(j3));
            A02.A03.A02(contentValues, "group_notification_version");
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
