package X;

import android.content.Context;
import android.content.SharedPreferences;
import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.util.Log;
import java.io.IOException;

/* renamed from: X.5fm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120195fm extends AbstractC451020e {
    public final /* synthetic */ C129965ya A00;
    public final /* synthetic */ C129115xC A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120195fm(Context context, C14900mE r2, C18650sn r3, C129965ya r4, C129115xC r5) {
        super(context, r2, r3);
        this.A00 = r4;
        this.A01 = r5;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        this.A01.A00(r2);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r2) {
        this.A01.A00(r2);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r11) {
        String str;
        C129115xC r4 = this.A01;
        try {
            AnonymousClass1V8 A0c = C117305Zk.A0c(r11);
            C452120p A00 = C452120p.A00(A0c);
            if (A00 == null) {
                if (A0c == null) {
                    A00 = new C452120p(500);
                } else {
                    AnonymousClass1V8 A0E = A0c.A0E("document");
                    if (A0E == null) {
                        A00 = new C452120p(500);
                    } else {
                        long A08 = A0E.A08("creation", 0) * 1000;
                        long A082 = 1000 * A0E.A08("expiration", 0);
                        byte[] bArr = A0E.A01;
                        Log.i("DyiViewModel/request-report/on-success");
                        C123595nP r2 = r4.A03;
                        AnonymousClass60U r3 = r2.A08;
                        String str2 = r2.A0A;
                        synchronized (r3) {
                            Log.i("dyiReportManager/on-report-available");
                            try {
                                C003501n.A08(r3.A04(str2), bArr);
                                C127315uI A002 = AnonymousClass60U.A00(bArr, A08);
                                r3.A01 = A002;
                                if (A002 == null) {
                                    Log.e("dyiReportManager/on-report-available/cannot-create-message");
                                } else {
                                    C18600si r8 = r3.A0A;
                                    r8.A0J(str2, A08);
                                    SharedPreferences.Editor A05 = C117295Zj.A05(r8);
                                    if ("personal".equals(str2)) {
                                        str = "payment_dyi_report_expiration_timestamp";
                                    } else {
                                        str = "business_payment_dyi_report_expiration_timestamp";
                                    }
                                    C12970iu.A1C(A05, str, A082);
                                    r8.A0B(2, str2);
                                }
                            } catch (IOException e) {
                                Log.e("dyiReportManager/on-report-available/cannot-save", e);
                            }
                        }
                        if (r3.A03(str2) == null) {
                            Log.i("DyiViewModel/request-report/on-error :: invalid report info");
                            AnonymousClass016 r0 = r2.A02;
                            Integer A0i = C12980iv.A0i();
                            r0.A0A(A0i);
                            C117305Zk.A1B(r2.A03, A0i, new C452120p(500));
                        } else {
                            r2.A01.A0A(r3.A03(str2));
                            AnonymousClass60U.A01(r2.A02, r3, str2);
                        }
                        int i = r4.A00;
                        if (i == 1) {
                            PinBottomSheetDialogFragment pinBottomSheetDialogFragment = r4.A02;
                            if (pinBottomSheetDialogFragment != null) {
                                pinBottomSheetDialogFragment.A1C();
                                return;
                            }
                            return;
                        } else if (i == 0) {
                            ActivityC13790kL r02 = r4.A01;
                            AnonymousClass009.A05(r02);
                            r02.AaN();
                            return;
                        } else {
                            return;
                        }
                    }
                }
            }
            r4.A00(A00);
        } catch (Exception unused) {
            Log.e("Pay: requestDyiReport -> error parsing the response");
        }
    }
}
