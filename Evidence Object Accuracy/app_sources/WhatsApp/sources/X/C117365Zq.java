package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.payments.ui.stepup.NoviReviewVideoSelfieActivity;

/* renamed from: X.5Zq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117365Zq extends BroadcastReceiver {
    public final /* synthetic */ NoviReviewVideoSelfieActivity A00;

    public C117365Zq(NoviReviewVideoSelfieActivity noviReviewVideoSelfieActivity) {
        this.A00 = noviReviewVideoSelfieActivity;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.A00.A0I.A05(intent);
    }
}
