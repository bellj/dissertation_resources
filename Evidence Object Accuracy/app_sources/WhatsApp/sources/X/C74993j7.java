package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.calling.callgrid.view.CallGrid;

/* renamed from: X.3j7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74993j7 extends AbstractC05270Ox {
    public final /* synthetic */ CallGrid A00;

    public C74993j7(CallGrid callGrid) {
        this.A00 = callGrid;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        this.A00.A04();
    }
}
