package X;

import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2Nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50082Nz {
    public final long A00;
    public final GroupJid A01;
    public final UserJid A02;
    public final Voip.CallState A03;
    public final String A04;
    public final List A05;
    public final boolean A06;
    public final boolean A07;
    public final boolean A08;
    public final boolean A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;

    public C50082Nz(GroupJid groupJid, UserJid userJid, Voip.CallState callState, String str, List list, long j, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        this.A04 = str;
        this.A03 = callState;
        this.A06 = z;
        this.A07 = z2;
        this.A0C = z3;
        this.A0A = z4;
        this.A09 = z5;
        this.A00 = j;
        this.A02 = userJid;
        this.A05 = list;
        this.A0B = z6;
        this.A01 = groupJid;
        this.A08 = z7;
    }

    public static C50082Nz A00(CallInfo callInfo, boolean z) {
        AnonymousClass1S7 r1 = callInfo.callWaitingInfo;
        if (r1.A01 == 1) {
            UserJid userJid = r1.A03.initialPeerJid;
            AnonymousClass009.A05(userJid);
            String str = r1.A04;
            Voip.CallState callState = Voip.CallState.RECEIVED_CALL;
            boolean z2 = false;
            if (r1.A00 > 1) {
                z2 = true;
            }
            boolean z3 = r1.A08;
            return new C50082Nz(r1.A02, userJid, callState, str, r1.A06, 0, false, z2, z3, false, false, true, r1.A07);
        }
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1S6 r12 : callInfo.participants.values()) {
            if (!r12.A0F) {
                arrayList.add(r12.A06);
            }
        }
        String str2 = callInfo.callId;
        Voip.CallState callState2 = callInfo.callState;
        boolean z4 = callInfo.isCaller;
        boolean isGroupCall = callInfo.isGroupCall();
        boolean z5 = callInfo.videoEnabled;
        boolean isCallOnHold = callInfo.isCallOnHold();
        long j = callInfo.callActiveTime;
        UserJid peerJid = callInfo.getPeerJid();
        AnonymousClass009.A05(peerJid);
        return new C50082Nz(callInfo.groupJid, peerJid, callState2, str2, arrayList, j, z4, isGroupCall, z5, z, isCallOnHold, false, callInfo.isJoinableGroupCall);
    }
}
