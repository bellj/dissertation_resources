package X;

import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatRadioButton;
import com.whatsapp.businessdirectory.view.custom.FilterBottomSheetDialogFragment;

/* renamed from: X.2vT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59842vT extends AbstractC37191le {
    public final AppCompatRadioButton A00;
    public final FilterBottomSheetDialogFragment A01;

    public C59842vT(AppCompatRadioButton appCompatRadioButton, FilterBottomSheetDialogFragment filterBottomSheetDialogFragment) {
        super(appCompatRadioButton);
        this.A00 = appCompatRadioButton;
        this.A01 = filterBottomSheetDialogFragment;
        appCompatRadioButton.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    }
}
