package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.collections.MarginCorrectedViewPager;

/* renamed from: X.41k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC851641k extends MarginCorrectedViewPager {
    public boolean A00;

    public AbstractC851641k(Context context) {
        super(context);
        A0O();
    }

    public AbstractC851641k(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A0O();
    }

    @Override // X.AbstractC75813kV
    public void A0O() {
        if (!this.A00) {
            this.A00 = true;
            generatedComponent();
        }
    }
}
