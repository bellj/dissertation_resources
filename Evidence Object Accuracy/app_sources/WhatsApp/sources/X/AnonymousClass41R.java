package X;

import com.whatsapp.chatinfo.ListChatInfo;

/* renamed from: X.41R  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41R extends AnonymousClass2Dn {
    public final /* synthetic */ ListChatInfo A00;

    public AnonymousClass41R(ListChatInfo listChatInfo) {
        this.A00 = listChatInfo;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r2) {
        ListChatInfo.A02(this.A00);
    }
}
