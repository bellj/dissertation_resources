package X;

/* renamed from: X.3qq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79593qq extends AnonymousClass5D8 {
    public int A00 = 0;
    public int A01;
    public final AnonymousClass4DS A02;
    public final CharSequence A03;
    public final boolean A04;
    public final /* synthetic */ AnonymousClass4IV A05;

    public C79593qq(AnonymousClass4IV r2, AnonymousClass4PP r3, CharSequence charSequence) {
        this.A05 = r2;
        this.A02 = r3.A00;
        this.A04 = r3.A02;
        this.A01 = Integer.MAX_VALUE;
        this.A03 = charSequence;
    }
}
