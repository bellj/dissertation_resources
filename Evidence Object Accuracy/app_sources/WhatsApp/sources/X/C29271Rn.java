package X;

import java.util.LinkedList;

/* renamed from: X.1Rn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29271Rn {
    public LinkedList A00 = new LinkedList();

    public C29271Rn() {
    }

    public C29271Rn(byte[] bArr) {
        for (C31461aa r2 : ((C32111bd) AbstractC27091Fz.A0E(C32111bd.A01, bArr)).A00) {
            this.A00.add(new C31451aZ(r2));
        }
    }

    public C31451aZ A00() {
        LinkedList linkedList = this.A00;
        if (!linkedList.isEmpty()) {
            return (C31451aZ) linkedList.get(0);
        }
        throw new C31541ai("No key state in record!");
    }
}
