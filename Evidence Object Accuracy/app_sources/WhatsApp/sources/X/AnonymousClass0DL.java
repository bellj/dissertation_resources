package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;

/* renamed from: X.0DL  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0DL extends AnonymousClass0DN {
    public AnonymousClass0DL(Resources resources, Bitmap bitmap) {
        super(resources, bitmap);
    }

    @Override // X.AnonymousClass0DN
    public void A02(int i, int i2, int i3, Rect rect, Rect rect2) {
        C05660Ql.A01(i, i2, i3, rect, rect2, 0);
    }
}
