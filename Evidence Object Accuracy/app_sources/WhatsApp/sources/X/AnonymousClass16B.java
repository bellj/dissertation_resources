package X;

import android.os.Message;

/* renamed from: X.16B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass16B {
    public AnonymousClass1N0 A00;
    public final C16590pI A01;
    public final C14850m9 A02;
    public final C21780xy A03;

    public AnonymousClass16B(C16590pI r1, C14850m9 r2, C21780xy r3) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
    }

    public final void A00() {
        AnonymousClass1N0 r0 = this.A00;
        if (r0 != null) {
            AnonymousClass009.A05(r0);
            r0.removeMessages(1);
            this.A00.sendEmptyMessageDelayed(1, 1000);
        }
    }

    public void A01(long j) {
        AnonymousClass1N0 r2;
        if (j >= 0 && (r2 = this.A00) != null) {
            AnonymousClass009.A0F(true);
            Message obtain = Message.obtain(r2, 4, 2, -1);
            obtain.getData().putLong("long_value", j);
            obtain.sendToTarget();
            A00();
        }
    }

    public void A02(long j, int i) {
        AnonymousClass1N0 r2;
        if (j >= 0 && (r2 = this.A00) != null) {
            AnonymousClass009.A0F(true);
            Message obtain = Message.obtain(r2, 5, i, -1);
            obtain.getData().putLong("long_value", j);
            obtain.sendToTarget();
            A00();
        }
    }
}
