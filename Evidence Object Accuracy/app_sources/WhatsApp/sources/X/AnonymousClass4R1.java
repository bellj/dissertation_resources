package X;

/* renamed from: X.4R1  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4R1 {
    public final C100564m7 A00;
    public final boolean[] A01;
    public final boolean[] A02;
    public final boolean[] A03;

    public AnonymousClass4R1(C100564m7 r3, boolean[] zArr) {
        this.A00 = r3;
        this.A02 = zArr;
        int i = r3.A01;
        this.A01 = new boolean[i];
        this.A03 = new boolean[i];
    }
}
