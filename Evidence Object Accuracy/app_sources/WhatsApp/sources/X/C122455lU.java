package X;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122455lU extends AbstractC118825cR {
    public Button A00;
    public ImageView A01;
    public LinearLayout A02;
    public TextView A03;
    public TextView A04;

    public C122455lU(View view) {
        super(view);
        this.A01 = C12970iu.A0K(view, R.id.payout_bank_icon);
        this.A04 = C12960it.A0I(view, R.id.payout_bank_name);
        this.A03 = C12960it.A0I(view, R.id.payout_bank_status);
        this.A02 = C117305Zk.A07(view, R.id.warning_container);
        this.A00 = (Button) AnonymousClass028.A0D(view, R.id.cta_button);
    }
}
