package X;

import android.os.Parcel;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Zh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30911Zh {
    public final Map A00;
    public final Map A01;

    public C30911Zh() {
        this.A01 = new HashMap();
        this.A00 = new HashMap();
    }

    public C30911Zh(Parcel parcel) {
        HashMap hashMap = new HashMap();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            hashMap.put(Integer.valueOf(parcel.readInt()), parcel.readString());
        }
        this.A01 = hashMap;
        HashMap hashMap2 = new HashMap();
        int readInt2 = parcel.readInt();
        for (int i2 = 0; i2 < readInt2; i2++) {
            hashMap2.put(Integer.valueOf(parcel.readInt()), parcel.readString());
        }
        this.A00 = hashMap2;
    }
}
