package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* renamed from: X.3gj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73743gj extends ViewGroup.MarginLayoutParams {
    public C73743gj() {
        super(-2, -2);
    }

    public C73743gj(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public C73743gj(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
