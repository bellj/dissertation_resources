package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1YX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YX {
    public int A00 = 0;
    public final C006202y A01 = new C30591Ya(this);
    public final Map A02 = new HashMap();

    public synchronized void A00(AbstractC15340mz r2, AnonymousClass1IS r3) {
        this.A02.remove(r3);
        this.A01.A08(r3, r2);
    }
}
