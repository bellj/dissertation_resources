package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.11B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11B {
    public final C18460sU A00;
    public final C16490p7 A01;
    public final C14850m9 A02;

    public AnonymousClass11B(C18460sU r1, C16490p7 r2, C14850m9 r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public static final List A00(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("group_jid_row_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("user_jid_row_id");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("is_leave");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("timestamp");
        while (cursor.moveToNext()) {
            long j = cursor.getLong(columnIndexOrThrow);
            long j2 = cursor.getLong(columnIndexOrThrow2);
            int i = (cursor.getLong(columnIndexOrThrow3) > 0 ? 1 : (cursor.getLong(columnIndexOrThrow3) == 0 ? 0 : -1));
            boolean z = false;
            if (i != 0) {
                z = true;
            }
            arrayList.add(new C30671Yi(j, j2, cursor.getLong(columnIndexOrThrow4), z));
        }
        return arrayList;
    }

    public final Map A01(List list) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C30671Yi r2 = (C30671Yi) it.next();
            hashSet.add(Long.valueOf(r2.A00));
            hashSet2.add(Long.valueOf(r2.A02));
        }
        C18460sU r1 = this.A00;
        Map A09 = r1.A09(AbstractC15590nW.class, hashSet);
        Map A092 = r1.A09(UserJid.class, hashSet2);
        HashMap hashMap = new HashMap();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            C30671Yi r5 = (C30671Yi) it2.next();
            AbstractC15590nW r8 = (AbstractC15590nW) A09.get(Long.valueOf(r5.A00));
            UserJid userJid = (UserJid) A092.get(Long.valueOf(r5.A02));
            if (!(userJid == null || r8 == null)) {
                Object obj = hashMap.get(r8);
                if (obj == null) {
                    obj = new ArrayList();
                    hashMap.put(r8, obj);
                }
                ((List) obj).add(new AnonymousClass1PR(r8, userJid, r5.A01, r5.A03));
            }
        }
        return hashMap;
    }

    public void A02(AbstractC15590nW r6) {
        String[] strArr = {String.valueOf(this.A00.A01(r6))};
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A01("group_past_participant_user", "group_jid_row_id = ?", strArr);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A03(AbstractC15590nW r8, UserJid userJid) {
        C18460sU r0 = this.A00;
        String[] strArr = {String.valueOf(r0.A01(r8)), String.valueOf(r0.A01(userJid))};
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A01("group_past_participant_user", "group_jid_row_id = ? AND user_jid_row_id = ?", strArr);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A04(AbstractC15590nW r8, UserJid userJid, long j, boolean z) {
        C18460sU r0 = this.A00;
        long A01 = r0.A01(r8);
        long A012 = r0.A01(userJid);
        ContentValues contentValues = new ContentValues(4);
        contentValues.put("group_jid_row_id", Long.valueOf(A01));
        contentValues.put("user_jid_row_id", Long.valueOf(A012));
        contentValues.put("is_leave", Boolean.valueOf(z));
        contentValues.put("timestamp", Long.valueOf(j));
        C16310on A02 = this.A01.A02();
        try {
            A02.A03.A06(contentValues, "group_past_participant_user", 5);
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
