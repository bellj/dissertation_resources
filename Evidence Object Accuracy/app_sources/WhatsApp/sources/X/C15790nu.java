package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import com.whatsapp.migration.export.api.ExportMigrationContentProvider;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0nu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15790nu {
    public static final long A06;
    public static final long A07;
    public final ComponentName A00;
    public final Context A01;
    public final PackageManager A02;
    public final AbstractC15710nm A03;
    public final C14830m7 A04;
    public final C15750nq A05;

    static {
        TimeUnit timeUnit = TimeUnit.DAYS;
        A07 = timeUnit.toMillis(2);
        A06 = timeUnit.toMillis(1);
    }

    public C15790nu(Context context, AbstractC15710nm r4, C14830m7 r5, C15750nq r6) {
        this.A04 = r5;
        this.A01 = context;
        this.A02 = context.getPackageManager();
        this.A03 = r4;
        this.A05 = r6;
        this.A00 = new ComponentName(context, ExportMigrationContentProvider.class);
    }

    public String A00() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = ((SharedPreferences) this.A05.A02.get()).getLong("/export/provider_closed/timestamp", 0);
        StringBuilder sb = new StringBuilder("providerState: ");
        sb.append(this.A02.getComponentEnabledSetting(this.A00));
        sb.append("; closedUnsuccessfully: ");
        sb.append(j);
        sb.append("; currentTime: ");
        sb.append(currentTimeMillis);
        sb.append(";");
        return sb.toString();
    }

    public String A01() {
        String str;
        int length;
        long currentTimeMillis = System.currentTimeMillis();
        long j = ((SharedPreferences) this.A05.A02.get()).getLong("/export/provider/timestamp", 0);
        Context context = this.A01;
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        String str2 = "<failed: ";
        if (applicationInfo != null) {
            try {
                str = new File(applicationInfo.dataDir).getCanonicalFile().toString();
            } catch (IOException e) {
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append(e);
                sb.append(">");
                str = sb.toString();
            }
        } else {
            str = null;
        }
        try {
            Signature[] signatureArr = this.A02.getPackageInfo(context.getPackageName(), 64).signatures;
            if (signatureArr == null || (length = signatureArr.length) == 0) {
                str2 = "<no signatures>";
            } else if (length > 1) {
                str2 = "<multiple signatures>";
            } else {
                try {
                    str2 = Base64.encodeToString(MessageDigest.getInstance("SHA-1").digest(signatureArr[0].toByteArray()), 11);
                } catch (NoSuchAlgorithmException e2) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append(e2);
                    sb2.append(">");
                    str2 = sb2.toString();
                }
            }
        } catch (PackageManager.NameNotFoundException e3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str2);
            sb3.append(e3.toString());
            sb3.append(">");
            str2 = sb3.toString();
        }
        StringBuilder sb4 = new StringBuilder("providerState: ");
        sb4.append(this.A02.getComponentEnabledSetting(this.A00));
        sb4.append("; lastAccessTime: ");
        sb4.append(j);
        sb4.append("; currentTime: ");
        sb4.append(currentTimeMillis);
        sb4.append("; dataDir: ");
        sb4.append(str);
        sb4.append("; signature: ");
        sb4.append(str2);
        sb4.append(";");
        return sb4.toString();
    }

    public void A02() {
        C15750nq r0 = this.A05;
        long currentTimeMillis = System.currentTimeMillis();
        AnonymousClass01H r4 = r0.A02;
        ((SharedPreferences) r4.get()).edit().putLong("/export/provider_closed/timestamp", currentTimeMillis).apply();
        ((SharedPreferences) r4.get()).edit().remove("/export/provider/timestamp").apply();
        this.A02.setComponentEnabledSetting(this.A00, 0, 1);
        if (A05()) {
            this.A03.AaV("xpm-provider-disable-failed", A01(), false);
        }
    }

    public void A03() {
        ((SharedPreferences) this.A05.A02.get()).edit().remove("/export/provider_closed/timestamp").apply();
        A04();
        this.A02.setComponentEnabledSetting(this.A00, 1, 1);
    }

    public void A04() {
        long currentTimeMillis = System.currentTimeMillis();
        AnonymousClass01H r10 = this.A05.A02;
        long j = ((SharedPreferences) r10.get()).getLong("/export/provider/timestamp", 0);
        long j2 = currentTimeMillis - j;
        if (j == 0 || j2 < 0 || j2 > 60000) {
            ((SharedPreferences) r10.get()).edit().putLong("/export/provider/timestamp", currentTimeMillis).apply();
        }
    }

    public boolean A05() {
        return this.A02.getComponentEnabledSetting(this.A00) == 1;
    }
}
