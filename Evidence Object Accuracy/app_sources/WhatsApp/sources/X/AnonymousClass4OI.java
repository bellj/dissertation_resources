package X;

import android.graphics.Bitmap;

/* renamed from: X.4OI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4OI {
    public final int A00;
    public final Bitmap A01;

    public AnonymousClass4OI(int i, Bitmap bitmap) {
        this.A00 = i;
        this.A01 = bitmap;
    }
}
