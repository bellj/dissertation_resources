package X;

/* renamed from: X.23F  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass23F extends AbstractC16110oT {
    public Long A00;

    public AnonymousClass23F() {
        super(3036, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamAdvStoredTimestampExpired {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "advExpireTimeInHours", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
