package X;

/* renamed from: X.46C  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass46C extends AnonymousClass4CD {
    public final int actionType;
    public final AbstractC115365Rg entity;
    public final C89634Ks event;
    public final C91654So transition;

    public AnonymousClass46C(AbstractC115365Rg r1, C89634Ks r2, C91654So r3, String str, Throwable th, int i) {
        super(str, th);
        this.event = r2;
        this.entity = r1;
        this.actionType = i;
        this.transition = r3;
    }
}
