package X;

import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.URLSpan;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3I2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3I2 {
    public static SpannableString A00(Context context, AnonymousClass4LJ r16, String str) {
        Spanned fromHtml = Html.fromHtml(str);
        String obj = fromHtml.toString();
        SpannableString spannableString = new SpannableString(obj);
        Object[] spans = fromHtml.getSpans(0, obj.length(), Object.class);
        for (Object obj2 : spans) {
            int spanStart = fromHtml.getSpanStart(obj2);
            int spanEnd = fromHtml.getSpanEnd(obj2);
            int spanFlags = fromHtml.getSpanFlags(obj2);
            if (obj2 instanceof URLSpan) {
                URLSpan uRLSpan = (URLSpan) obj2;
                if (r16 != null) {
                    String url = uRLSpan.getURL();
                    obj2 = new AnonymousClass36A(context, r16, url, A01(url), A02(url));
                }
            }
            spannableString.setSpan(obj2, spanStart, spanEnd, spanFlags);
        }
        return spannableString;
    }

    public static String A01(String str) {
        if (!str.startsWith("whatsapp:user-notice")) {
            return "open-link";
        }
        String queryParameter = Uri.parse(C12960it.A0d(str, C12960it.A0k("h://"))).getQueryParameter("action");
        if (queryParameter == null) {
            return "";
        }
        return queryParameter;
    }

    public static Map A02(String str) {
        HashMap A11 = C12970iu.A11();
        if (!str.startsWith("whatsapp:user-notice")) {
            A11.put("link", str);
        } else {
            Uri parse = Uri.parse(C12960it.A0d(str, C12960it.A0k("h://")));
            Iterator<String> it = parse.getQueryParameterNames().iterator();
            while (it.hasNext()) {
                String A0x = C12970iu.A0x(it);
                if (!"action".equals(A0x)) {
                    A11.put(A0x, parse.getQueryParameter(A0x));
                }
            }
        }
        return A11;
    }
}
