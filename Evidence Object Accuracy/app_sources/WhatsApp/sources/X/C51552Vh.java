package X;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.2Vh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51552Vh {
    public C15250mo A00;
    public final AnonymousClass02P A01;
    public final AnonymousClass02P A02;
    public final AnonymousClass016 A03 = new AnonymousClass016();
    public final C15610nY A04;
    public final C20830wO A05;
    public final C15680nj A06;
    public final C21230x5 A07;
    public final AnonymousClass1AY A08;
    public final AtomicInteger A09 = new AtomicInteger();

    public C51552Vh(AnonymousClass017 r4, AnonymousClass017 r5, AnonymousClass017 r6, AnonymousClass017 r7, C15610nY r8, AnonymousClass018 r9, C20830wO r10, C15680nj r11, C21230x5 r12, AnonymousClass1AY r13, AnonymousClass1AW r14) {
        AnonymousClass02P r1 = new AnonymousClass02P();
        this.A02 = r1;
        AnonymousClass02P r2 = new AnonymousClass02P();
        this.A01 = r2;
        this.A04 = r8;
        this.A08 = r13;
        this.A06 = r11;
        this.A05 = r10;
        this.A07 = r12;
        this.A00 = new C15250mo(r9);
        r14.A00(new AnonymousClass02O() { // from class: X.2Vz
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                C28181Ma r62;
                C51552Vh r112 = C51552Vh.this;
                AnonymousClass1KL r52 = (AnonymousClass1KL) obj;
                AnonymousClass016 r42 = r112.A03;
                r42.A0A(Boolean.TRUE);
                C15250mo r142 = (C15250mo) r52.A01;
                int A00 = C34961gz.A00(1);
                C21230x5 r82 = r112.A07;
                r82.ALF(926875649, A00);
                r82.AKy("type", 926875649, A00, r142.A02);
                boolean z = false;
                if (r142.A04 != null) {
                    z = true;
                }
                try {
                    r82.AL1("jid", 926875649, A00, z);
                    r82.AKy("token_count", 926875649, A00, r142.A02().size());
                    r82.AKy("domain", 926875649, A00, 1);
                    AnonymousClass02N r102 = r52.A00;
                    StringBuilder sb = new StringBuilder("chatSearchManager/getContactsForQuery/");
                    sb.append(r112.A09.getAndIncrement());
                    sb.append("/");
                    sb.append(r142.A01().length());
                    r62 = new C28181Ma(sb.toString());
                    ArrayList arrayList = new ArrayList();
                    try {
                        Integer num = 0;
                        if (!num.equals(Integer.valueOf(r142.A02)) || r142.A04 != null || (!(!r142.A02().isEmpty()) && r142.A06 == null)) {
                            r62.A02("empty");
                            r82.AL5(926875649, A00);
                            r42.A0A(Boolean.FALSE);
                        } else {
                            ArrayList arrayList2 = new ArrayList();
                            if (!r142.A02().isEmpty()) {
                                arrayList2.add(new AnonymousClass2W4(r112.A04, r112.A05, r142.A02()));
                            }
                            C49662Lr r15 = r142.A06;
                            if (r15 != null) {
                                arrayList2.add(r112.A08.A00(r15));
                            }
                            r62.A02("filter");
                            C34961gz.A01(r82, Integer.valueOf(A00), "filter");
                            r102.A02();
                            for (AbstractC14640lm r16 : r112.A06.A04()) {
                                r102.A02();
                                if (C20830wO.A00(r16, arrayList2)) {
                                    arrayList.add(r112.A05.A01(r16));
                                }
                            }
                            r62.A02("done");
                            r42.A0A(Boolean.FALSE);
                            r82.AL6(926875649, A00, 2);
                        }
                    } catch (AnonymousClass04U unused) {
                        r62.A02("cancelled");
                        if (r62.A00() < 300) {
                            r82.AL5(926875649, A00);
                        } else {
                            r82.AL6(926875649, A00, 4);
                        }
                    }
                    return arrayList;
                } finally {
                    r62.A01();
                }
            }
        }, r1, r2);
        r1.A0D(r4, new AnonymousClass02B() { // from class: X.2W0
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51552Vh r0 = C51552Vh.this;
                C15250mo r15 = r0.A00;
                r15.A03((String) obj);
                r0.A02.A0B(r15);
            }
        });
        r1.A0D(r5, new AnonymousClass02B() { // from class: X.2W1
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51552Vh r22 = C51552Vh.this;
                C15250mo r15 = r22.A00;
                r15.A02 = ((Number) obj).intValue();
                r22.A02.A0B(r15);
            }
        });
        r1.A0D(r6, new AnonymousClass02B() { // from class: X.2W2
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51552Vh r0 = C51552Vh.this;
                C15250mo r15 = r0.A00;
                r15.A04 = (AbstractC14640lm) obj;
                r0.A02.A0B(r15);
            }
        });
        r1.A0D(r7, new AnonymousClass02B() { // from class: X.2W3
            @Override // X.AnonymousClass02B
            public final void ANq(Object obj) {
                C51552Vh r0 = C51552Vh.this;
                C15250mo r15 = r0.A00;
                r15.A06 = (C49662Lr) obj;
                r0.A02.A0B(r15);
            }
        });
    }

    public AnonymousClass017 A00() {
        return this.A01;
    }

    public AnonymousClass017 A01() {
        return this.A03;
    }

    public void A02() {
        this.A01.A0B(new ArrayList());
    }

    public void A03() {
        this.A02.A0A(this.A00);
    }
}
