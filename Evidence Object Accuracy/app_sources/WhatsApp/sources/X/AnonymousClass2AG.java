package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0100000_I0_2;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.2AG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2AG implements ThreadFactory {
    public final String A00;
    public final AtomicInteger A01 = new AtomicInteger(0);

    public /* synthetic */ AnonymousClass2AG(String str) {
        this.A00 = str;
    }

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        RunnableBRunnable0Shape2S0100000_I0_2 runnableBRunnable0Shape2S0100000_I0_2 = new RunnableBRunnable0Shape2S0100000_I0_2(runnable, 20);
        StringBuilder sb = new StringBuilder();
        sb.append(this.A00);
        sb.append(this.A01.getAndIncrement());
        return new AnonymousClass1MS(runnableBRunnable0Shape2S0100000_I0_2, sb.toString());
    }
}
