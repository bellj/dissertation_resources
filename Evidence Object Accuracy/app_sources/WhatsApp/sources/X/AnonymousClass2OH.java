package X;

import android.content.ComponentName;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.telecom.CallAudioState;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.SelfManagedConnectionService;
import com.whatsapp.voipcalling.Voip;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.2OH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2OH extends AbstractC16220oe {
    public PhoneAccountHandle A00;
    public boolean A01;
    public final AnonymousClass01d A02;
    public final C16590pI A03;
    public final C14850m9 A04;
    public final ConcurrentMap A05 = new ConcurrentHashMap(2);
    public volatile boolean A06;

    public AnonymousClass2OH(AnonymousClass01d r3, C16590pI r4, C14850m9 r5) {
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static final Uri A01(UserJid userJid) {
        String replaceAll;
        String A04 = C248917h.A04(userJid);
        if (A04 != null && (replaceAll = A04.replaceAll("\\D", "")) != null) {
            return Uri.fromParts("tel", replaceAll, "");
        }
        Log.i("voip/SelfManagedConnectionsManager/getPhoneCallUri failed to get phone number");
        return null;
    }

    public int A05() {
        return this.A04.A02(1658);
    }

    public Connection A06(ConnectionRequest connectionRequest, boolean z) {
        Bundle bundle;
        StringBuilder sb;
        String str;
        A02();
        Bundle extras = connectionRequest.getExtras();
        C52142aJ r6 = null;
        if (extras == null) {
            sb = new StringBuilder();
            str = "voip/SelfManagedConnectionsManager/createSelfManagedConnection extras is null for request ";
        } else {
            if (!z) {
                bundle = extras.getBundle("android.telecom.extra.INCOMING_CALL_EXTRAS");
                if (bundle == null) {
                    sb = new StringBuilder();
                    str = "voip/SelfManagedConnectionsManager/createSelfManagedConnection EXTRA_INCOMING_CALL_EXTRAS is null for request ";
                }
            } else {
                bundle = extras;
            }
            String string = bundle.getString("call_id");
            UserJid nullable = UserJid.getNullable(bundle.getString("peer_jid"));
            String string2 = bundle.getString("peer_display_name");
            boolean z2 = bundle.getBoolean("is_rejoin", false);
            if (string == null || nullable == null || string2 == null) {
                sb = new StringBuilder();
                str = "voip/SelfManagedConnectionsManager/createSelfManagedConnection invalid request ";
            } else {
                r6 = new C52142aJ(this, string);
                r6.setConnectionProperties(128);
                r6.setAddress(connectionRequest.getAddress(), 1);
                r6.setCallerDisplayName(string2, 1);
                r6.setConnectionCapabilities(r6.getConnectionCapabilities() | 2);
                r6.setVideoState(connectionRequest.getVideoState());
                r6.setExtras(extras);
                StringBuilder sb2 = new StringBuilder("voip/SelfManagedConnectionsManager/createSelfManagedConnection with ");
                sb2.append(nullable);
                sb2.append(", call id: ");
                sb2.append(string);
                sb2.append(", isOutgoing ");
                sb2.append(z);
                Log.i(sb2.toString());
                A0C(r6);
                for (AnonymousClass2ON r2 : A01()) {
                    if (z) {
                        r2.A02(string, z2);
                    } else if (!(r2 instanceof AnonymousClass39T)) {
                        AnonymousClass009.A01();
                    } else {
                        AnonymousClass39T r22 = (AnonymousClass39T) r2;
                        AnonymousClass009.A01();
                        StringBuilder sb3 = new StringBuilder("voip/service/selfManagedConnectionListener/onCreateIncomingConnection ");
                        sb3.append(string);
                        Log.i(sb3.toString());
                        CallInfo callInfo = Voip.getCallInfo();
                        if (callInfo == null || (!string.equals(callInfo.callId) && !string.equals(callInfo.callWaitingInfo.A04))) {
                            r22.A00.A2N.A0A(string);
                        } else {
                            C29631Ua r23 = r22.A00;
                            r23.A0L.removeMessages(1);
                            Handler handler = r23.A0L;
                            handler.sendMessageDelayed(handler.obtainMessage(28, string), 500);
                        }
                    }
                }
                return r6;
            }
        }
        sb.append(str);
        sb.append(connectionRequest);
        Log.i(sb.toString());
        return r6;
    }

    public C52142aJ A07(String str) {
        return (C52142aJ) this.A05.get(str);
    }

    public void A08() {
        A02();
        ConcurrentMap concurrentMap = this.A05;
        if (!concurrentMap.isEmpty()) {
            Log.i("voip/SelfManagedConnectionsManager/removeAllConnections");
            Iterator it = new ArrayList(concurrentMap.values()).iterator();
            while (it.hasNext()) {
                ((C52142aJ) it.next()).A01(2);
            }
            AnonymousClass009.A0A("all connection should have been removed", concurrentMap.isEmpty());
        }
    }

    public void A09(CallAudioState callAudioState, String str) {
        A02();
        for (AnonymousClass2ON r0 : A01()) {
            r0.A00(callAudioState, str);
        }
    }

    public void A0A(ConnectionRequest connectionRequest) {
        String string;
        A02();
        Bundle extras = connectionRequest.getExtras();
        if (!(extras == null || (string = extras.getString("call_id")) == null)) {
            for (AnonymousClass2ON r2 : A01()) {
                if (!(r2 instanceof AnonymousClass39T)) {
                    AnonymousClass009.A01();
                } else {
                    AnonymousClass39T r22 = (AnonymousClass39T) r2;
                    AnonymousClass009.A01();
                    StringBuilder sb = new StringBuilder("voip/service/selfManagedConnectionListener/onCreateIncomingConnectionFailed ");
                    sb.append(string);
                    Log.i(sb.toString());
                    if (string.equals(Voip.getCurrentCallId())) {
                        C29631Ua r23 = r22.A00;
                        r23.A0L.removeMessages(1);
                        r23.A0z(string, "busy", 4);
                    }
                }
            }
        }
    }

    public void A0B(ConnectionRequest connectionRequest) {
        String string;
        A02();
        Bundle extras = connectionRequest.getExtras();
        if (!(extras == null || (string = extras.getString("call_id")) == null)) {
            for (AnonymousClass2ON r5 : A01()) {
                if (!(r5 instanceof AnonymousClass2OU)) {
                    AnonymousClass009.A01();
                } else {
                    AnonymousClass2OU r52 = (AnonymousClass2OU) r5;
                    AnonymousClass009.A01();
                    AnonymousClass19Z r3 = r52.A00;
                    AnonymousClass1MP r2 = r3.A0S;
                    StringBuilder sb = new StringBuilder("app/startOutgoingCall/onCreateOutgoingConnectionFailed ");
                    sb.append(string);
                    sb.append(", pendingCallCommand: ");
                    sb.append(r2);
                    Log.i(sb.toString());
                    if (r2 != null && string.equals(r2.A05)) {
                        Log.w("app/startOutgoingCall/failed_create_outgoing_connection");
                        r3.A0S = null;
                        r3.A01.removeMessages(1);
                    }
                    r52.A02.A03(string, 97);
                }
            }
        }
    }

    public void A0C(C52142aJ r4) {
        A02();
        ConcurrentMap concurrentMap = this.A05;
        concurrentMap.put(r4.A00(), r4);
        StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/addConnection");
        sb.append(r4);
        sb.append(", total connection count: ");
        sb.append(concurrentMap.size());
        Log.i(sb.toString());
    }

    public void A0D(C52142aJ r4) {
        A02();
        ConcurrentMap concurrentMap = this.A05;
        concurrentMap.remove(r4.A00());
        StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/removeConnection");
        sb.append(r4);
        sb.append(", total connection count: ");
        sb.append(concurrentMap.size());
        Log.i(sb.toString());
    }

    public void A0E(String str) {
        A02();
        StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/disconnectConnectionIfExists");
        sb.append(str);
        Log.i(sb.toString());
        C52142aJ A07 = A07(str);
        if (A07 != null) {
            A07.A01(2);
        }
    }

    public void A0F(String str, int i) {
        A02();
        for (AnonymousClass2ON r4 : A01()) {
            if (!(r4 instanceof AnonymousClass39T)) {
                AnonymousClass009.A01();
            } else {
                AnonymousClass39T r42 = (AnonymousClass39T) r4;
                AnonymousClass009.A01();
                StringBuilder sb = new StringBuilder("voip/service/selfManagedConnectionListener/onConnectionStateChanged ");
                sb.append(str);
                sb.append(", state ");
                sb.append(i);
                Log.i(sb.toString());
                CallInfo callInfo = Voip.getCallInfo();
                if (Voip.A09(callInfo) && str.equals(callInfo.callId)) {
                    if (i == 0) {
                        r42.A00.A0n(callInfo, true, true);
                    } else if (i == 1) {
                        r42.A00.A0n(callInfo, false, true);
                    } else if (i == 2) {
                        C29631Ua r1 = r42.A00;
                        r1.A0z = true;
                        r1.A0k(callInfo);
                        r1.A0y(callInfo.callId, 3);
                    } else if (i == 3) {
                        C29631Ua r6 = r42.A00;
                        int A01 = r6.A2N.A01();
                        Long valueOf = Long.valueOf(SystemClock.elapsedRealtime() - r6.A0D);
                        r6.A0p = valueOf;
                        if (valueOf.longValue() >= ((long) A01)) {
                            r6.A0z(str, null, 4);
                        } else if (!r6.A17) {
                            r6.A0k(callInfo);
                        }
                    } else if (i != 4) {
                        AnonymousClass009.A07("unknown SelfManagedConnection.StateChange");
                    } else {
                        r42.A00.A0m(callInfo, null, 1);
                    }
                }
            }
        }
    }

    public void A0G(String str, String str2) {
        C52142aJ A07 = A07(str);
        if (A07 != null) {
            StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/onCallAutoConnected changing CallId from: ");
            sb.append(str);
            sb.append(" -> ");
            sb.append(str2);
            Log.i(sb.toString());
            A0D(A07);
            A07.A02(str2);
            A0C(A07);
        }
    }

    public boolean A0H(Context context, UserJid userJid) {
        String str;
        A02();
        if (this.A00 != null) {
            return true;
        }
        AnonymousClass01d r4 = this.A02;
        if (r4.A0L() == null) {
            str = "voip/SelfManagedConnectionsManager/registerPhoneAccount telecomManager is null";
        } else {
            Uri A01 = A01(userJid);
            if (A01 == null) {
                str = "voip/SelfManagedConnectionsManager/registerPhoneAccount address is null";
            } else {
                StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/registerPhoneAccount ");
                sb.append(userJid);
                Log.i(sb.toString());
                PhoneAccountHandle phoneAccountHandle = new PhoneAccountHandle(new ComponentName(context, SelfManagedConnectionService.class), userJid.getRawString());
                this.A00 = phoneAccountHandle;
                Context context2 = this.A03.A00;
                PhoneAccount.Builder shortDescription = PhoneAccount.builder(phoneAccountHandle, context2.getString(R.string.whatsapp_name)).addSupportedUriScheme("tel").setAddress(A01).setCapabilities(3080).setShortDescription(context2.getString(R.string.voip_phone_account_description));
                if (Build.VERSION.SDK_INT >= 28) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("android.telecom.extra.LOG_SELF_MANAGED_CALLS", false);
                    shortDescription.setExtras(bundle);
                }
                try {
                    r4.A0L().registerPhoneAccount(shortDescription.build());
                    return true;
                } catch (Exception e) {
                    Log.e(e);
                    this.A00 = null;
                    return false;
                }
            }
        }
        Log.w(str);
        return false;
    }

    public boolean A0I(UserJid userJid, String str, String str2, boolean z) {
        String str3;
        A02();
        StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/addNewIncomingCall ");
        sb.append(userJid);
        Log.i(sb.toString());
        if (!this.A06) {
            str3 = "voip/SelfManagedConnectionsManager/addNewIncomingCall incomingEnabled is false";
        } else {
            AnonymousClass01d r4 = this.A02;
            if (r4.A0L() == null) {
                str3 = "voip/SelfManagedConnectionsManager/addNewIncomingCall telecomManager is null";
            } else if (this.A00 == null) {
                str3 = "voip/SelfManagedConnectionsManager/addNewIncomingCall phoneAccountHandle is null";
            } else {
                try {
                    if (!r4.A0L().isIncomingCallPermitted(this.A00)) {
                        Log.i("voip/SelfManagedConnectionsManager/addNewIncomingCall incoming call not permitted for the phone account handle");
                        return false;
                    }
                    Uri A01 = A01(userJid);
                    if (A01 != null) {
                        Bundle bundle = new Bundle();
                        if (z) {
                            bundle.putInt("android.telecom.extra.START_CALL_WITH_VIDEO_STATE", 3);
                        }
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("call_id", str);
                        bundle2.putString("peer_jid", userJid.getRawString());
                        bundle2.putString("peer_display_name", str2);
                        bundle2.putBoolean("is_rejoin", false);
                        bundle.putParcelable("android.telecom.extra.INCOMING_CALL_EXTRAS", bundle2);
                        bundle.putParcelable("android.telecom.extra.INCOMING_CALL_ADDRESS", A01);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("voip/SelfManagedConnectionsManager/addNewIncomingCall ");
                        sb2.append(bundle);
                        Log.i(sb2.toString());
                        r4.A0L().addNewIncomingCall(this.A00, bundle);
                        return true;
                    }
                    return false;
                } catch (Exception e) {
                    Log.e(e);
                    return false;
                }
            }
        }
        Log.w(str3);
        return false;
    }

    public boolean A0J(UserJid userJid, String str, String str2, boolean z, boolean z2) {
        String str3;
        A02();
        StringBuilder sb = new StringBuilder("voip/SelfManagedConnectionsManager/placeOutgoingCall ");
        sb.append(userJid);
        Log.i(sb.toString());
        if (!this.A01) {
            str3 = "voip/SelfManagedConnectionsManager/placeOutgoingCall outgoingEnabled is false";
        } else {
            AnonymousClass01d r4 = this.A02;
            if (r4.A0L() == null) {
                str3 = "voip/SelfManagedConnectionsManager/placeOutgoingCall telecomManager is null";
            } else if (this.A00 == null) {
                str3 = "voip/SelfManagedConnectionsManager/placeOutgoingCall phoneAccountHandle is null";
            } else {
                try {
                    if (!r4.A0L().isOutgoingCallPermitted(this.A00)) {
                        Log.w("voip/SelfManagedConnectionsManager/placeOutgoingCall outgoing call not permitted for the phone account handle");
                        return false;
                    }
                    Uri A01 = A01(userJid);
                    if (A01 != null) {
                        if (z && Build.VERSION.SDK_INT >= 28 && Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
                            z = false;
                        }
                        Bundle bundle = new Bundle();
                        if (z) {
                            bundle.putInt("android.telecom.extra.START_CALL_WITH_VIDEO_STATE", 3);
                        }
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("call_id", str);
                        bundle2.putString("peer_jid", userJid.getRawString());
                        bundle2.putString("peer_display_name", str2);
                        bundle2.putBoolean("is_rejoin", z2);
                        bundle.putParcelable("android.telecom.extra.OUTGOING_CALL_EXTRAS", bundle2);
                        bundle.putParcelable("android.telecom.extra.PHONE_ACCOUNT_HANDLE", this.A00);
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("voip/SelfManagedConnectionsManager/placeOutgoingCall ");
                        sb2.append(bundle);
                        Log.i(sb2.toString());
                        r4.A0L().placeCall(A01, bundle);
                        return true;
                    }
                    return false;
                } catch (Exception e) {
                    Log.e(e);
                    return false;
                }
            }
        }
        Log.w(str3);
        return false;
    }
}
