package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4aN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93564aN {
    public static final List A03 = Collections.unmodifiableList(C12960it.A0l());
    public String A00 = "CERTIFICATE";
    public List A01;
    public byte[] A02;

    public C93564aN(byte[] bArr) {
        List list = A03;
        this.A01 = Collections.unmodifiableList(list);
        this.A02 = bArr;
    }
}
