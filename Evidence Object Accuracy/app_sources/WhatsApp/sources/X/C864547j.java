package X;

import java.lang.ref.WeakReference;

/* renamed from: X.47j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C864547j extends AbstractC16350or {
    public final long A00;
    public final C18750sx A01;
    public final WeakReference A02;

    public C864547j(C18750sx r2, AnonymousClass5V9 r3, long j) {
        this.A01 = r2;
        this.A02 = C12970iu.A10(r3);
        this.A00 = j;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return this.A01.A02(this.A00);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass1YT r2 = (AnonymousClass1YT) obj;
        AnonymousClass5V9 r0 = (AnonymousClass5V9) this.A02.get();
        if (r0 != null) {
            r0.ANW(r2);
        }
    }
}
