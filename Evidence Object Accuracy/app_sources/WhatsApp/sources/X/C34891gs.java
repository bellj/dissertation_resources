package X;

/* renamed from: X.1gs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34891gs extends AbstractC27971Jy {
    public final String index;
    public final byte[] mutationMac;
    public final C27791Jf operation;
    public final int reason;
    public final C27831Jk syncActionValue;
    public final AnonymousClass1JR syncdKeyId;
    public final int version;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C34891gs(X.C27791Jf r3, X.AnonymousClass1JR r4, X.C27831Jk r5, java.lang.Integer r6, java.lang.String r7, byte[] r8, int r9) {
        /*
            r2 = this;
            java.lang.String r1 = "MalformedMutationException with reason "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r9)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.index = r7
            r2.reason = r9
            if (r6 == 0) goto L_0x0026
            int r0 = r6.intValue()
        L_0x001b:
            r2.version = r0
            r2.syncdKeyId = r4
            r2.mutationMac = r8
            r2.syncActionValue = r5
            r2.operation = r3
            return
        L_0x0026:
            r0 = 0
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34891gs.<init>(X.1Jf, X.1JR, X.1Jk, java.lang.Integer, java.lang.String, byte[], int):void");
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        return super.getMessage();
    }
}
