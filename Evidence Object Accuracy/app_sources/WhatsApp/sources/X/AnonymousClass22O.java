package X;

/* renamed from: X.22O  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass22O extends AbstractC28131Kt {
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final byte[] A0D;

    public AnonymousClass22O(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, byte[] bArr, int i, int i2, boolean z, boolean z2, boolean z3) {
        super(null, null);
        this.A08 = str;
        this.A09 = str2;
        this.A03 = str3;
        this.A02 = str4;
        this.A07 = str5;
        this.A01 = i;
        this.A0D = bArr;
        this.A00 = i2;
        this.A0B = z;
        this.A0C = z2;
        this.A04 = str6;
        this.A05 = str7;
        this.A06 = str8;
        this.A0A = z3;
    }
}
