package X;

import java.util.Comparator;

/* renamed from: X.3tJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81003tJ extends AbstractC95284dR {
    @Override // X.AbstractC95284dR
    public int result() {
        return 0;
    }

    public C81003tJ() {
        super(null);
    }

    public AbstractC95284dR classify(int i) {
        if (i < 0) {
            return AbstractC95284dR.LESS;
        }
        return i > 0 ? AbstractC95284dR.GREATER : AbstractC95284dR.ACTIVE;
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1 */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC95284dR
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC95284dR compare(int r2, int r3) {
        /*
            r1 = this;
            if (r2 >= r3) goto L_0x0008
            r0 = -1
        L_0x0003:
            X.4dR r0 = r1.classify(r0)
            return r0
        L_0x0008:
            boolean r0 = X.C72463ee.A0Y(r2, r3)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C81003tJ.compare(int, int):X.4dR");
    }

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compare(Object obj, Object obj2, Comparator comparator) {
        return classify(comparator.compare(obj, obj2));
    }

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compareFalseFirst(boolean z, boolean z2) {
        int i;
        if (z == z2) {
            i = 0;
        } else {
            i = -1;
            if (z) {
                i = 1;
            }
        }
        return classify(i);
    }

    @Override // X.AbstractC95284dR
    public AbstractC95284dR compareTrueFirst(boolean z, boolean z2) {
        int i;
        if (z2 == z) {
            i = 0;
        } else {
            i = -1;
            if (z2) {
                i = 1;
            }
        }
        return classify(i);
    }
}
