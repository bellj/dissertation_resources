package X;

import android.hardware.camera2.CameraDevice;
import com.facebook.redex.IDxCallableShape15S0100000_3_I1;
import java.util.concurrent.ExecutionException;

/* renamed from: X.5w7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128445w7 {
    public final /* synthetic */ AnonymousClass662 A00;

    public C128445w7(AnonymousClass662 r1) {
        this.A00 = r1;
    }

    public void A00(CameraDevice cameraDevice) {
        AnonymousClass662 r4 = this.A00;
        if (r4.A0h == cameraDevice) {
            AnonymousClass61Q r5 = r4.A0Y;
            r4.A0n = false;
            r4.A0h = null;
            r4.A0E = null;
            r4.A0C = null;
            r4.A0D = null;
            r4.A05 = null;
            C1308260c r0 = r4.A09;
            if (r0 != null) {
                r0.A03();
            }
            r4.A0X.A0G = false;
            r4.A0V.A02();
            C1308060a r1 = r4.A0Z;
            if (r1.A0D && (!r4.A0p || r1.A0C)) {
                try {
                    r4.A0d.A00(new C118975cg(this), "on_camera_closed_stop_video_recording", new IDxCallableShape15S0100000_3_I1(this, 8)).get();
                } catch (InterruptedException | ExecutionException unused) {
                    AnonymousClass616.A00();
                }
            }
            if (r5.A09 != null) {
                synchronized (AnonymousClass61Q.A0S) {
                    AnonymousClass66I r12 = r5.A08;
                    if (r12 != null) {
                        r12.A0G = false;
                        r5.A08 = null;
                    }
                }
                try {
                    r5.A09.A02();
                    r5.A09.A03();
                } catch (Exception unused2) {
                }
                r5.A09 = null;
            }
            String id = cameraDevice.getId();
            C119045cn r13 = r4.A0T;
            if (id.equals(r13.A00)) {
                r13.A01();
                r13.A00 = null;
            }
        }
    }
}
