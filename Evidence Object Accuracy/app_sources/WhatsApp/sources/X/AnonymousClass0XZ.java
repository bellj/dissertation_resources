package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import androidx.cardview.widget.CardView;

/* renamed from: X.0XZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XZ implements AbstractC12750iR {
    @Override // X.AbstractC12750iR
    public void AIq() {
    }

    @Override // X.AbstractC12750iR
    public ColorStateList AAn(AbstractC11670gf r2) {
        return ((AnonymousClass0A3) ((AnonymousClass0XY) r2).A00).A02;
    }

    @Override // X.AbstractC12750iR
    public float ACg(AbstractC11670gf r2) {
        return ((AnonymousClass0XY) r2).A01.getElevation();
    }

    @Override // X.AbstractC12750iR
    public float AE8(AbstractC11670gf r2) {
        return ((AnonymousClass0A3) ((AnonymousClass0XY) r2).A00).A00;
    }

    @Override // X.AbstractC12750iR
    public float AEU(AbstractC11670gf r3) {
        return AG3(r3) * 2.0f;
    }

    @Override // X.AbstractC12750iR
    public float AEW(AbstractC11670gf r3) {
        return AG3(r3) * 2.0f;
    }

    @Override // X.AbstractC12750iR
    public float AG3(AbstractC11670gf r2) {
        return ((AnonymousClass0A3) ((AnonymousClass0XY) r2).A00).A01;
    }

    @Override // X.AbstractC12750iR
    public void AIw(Context context, ColorStateList colorStateList, AbstractC11670gf r6, float f, float f2, float f3) {
        AnonymousClass0A3 r2 = new AnonymousClass0A3(colorStateList, f);
        AnonymousClass0XY r0 = (AnonymousClass0XY) r6;
        r0.A00 = r2;
        CardView cardView = r0.A01;
        cardView.setBackgroundDrawable(r2);
        cardView.setClipToOutline(true);
        cardView.setElevation(f2);
        AcJ(r6, f3);
    }

    @Override // X.AbstractC12750iR
    public void AOK(AbstractC11670gf r2) {
        AcJ(r2, AE8(r2));
    }

    @Override // X.AbstractC12750iR
    public void AUD(AbstractC11670gf r2) {
        AcJ(r2, AE8(r2));
    }

    @Override // X.AbstractC12750iR
    public void Abm(ColorStateList colorStateList, AbstractC11670gf r6) {
        AnonymousClass0A3 r3 = (AnonymousClass0A3) ((AnonymousClass0XY) r6).A00;
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        r3.A02 = colorStateList;
        r3.A08.setColor(colorStateList.getColorForState(r3.getState(), r3.A02.getDefaultColor()));
        r3.invalidateSelf();
    }

    @Override // X.AbstractC12750iR
    public void Ac6(AbstractC11670gf r2, float f) {
        ((AnonymousClass0XY) r2).A01.setElevation(f);
    }

    @Override // X.AbstractC12750iR
    public void AcJ(AbstractC11670gf r12, float f) {
        AnonymousClass0XY r0 = (AnonymousClass0XY) r12;
        AnonymousClass0A3 r3 = (AnonymousClass0A3) r0.A00;
        CardView cardView = r0.A01;
        boolean z = cardView.A02;
        boolean z2 = cardView.A03;
        if (!(f == r3.A00 && r3.A06 == z && r3.A07 == z2)) {
            r3.A00 = f;
            r3.A06 = z;
            r3.A07 = z2;
            r3.A00(null);
            r3.invalidateSelf();
        }
        if (!cardView.A02) {
            cardView.A05.set(0, 0, 0, 0);
            Rect rect = cardView.A04;
            AnonymousClass0XZ.super.setPadding(0 + rect.left, 0 + rect.top, 0 + rect.right, 0 + rect.bottom);
            return;
        }
        float AE8 = AE8(r12);
        float AG3 = AG3(r12);
        boolean z3 = cardView.A03;
        float f2 = AE8;
        if (z3) {
            f2 = (float) (((double) AE8) + ((1.0d - AnonymousClass0A6.A0H) * ((double) AG3)));
        }
        int ceil = (int) Math.ceil((double) f2);
        int ceil2 = (int) Math.ceil((double) AnonymousClass0A6.A00(AE8, AG3, z3));
        cardView.A05.set(ceil, ceil2, ceil, ceil2);
        Rect rect2 = cardView.A04;
        AnonymousClass0XZ.super.setPadding(ceil + rect2.left, ceil2 + rect2.top, ceil + rect2.right, ceil2 + rect2.bottom);
    }

    @Override // X.AbstractC12750iR
    public void Aci(AbstractC11670gf r3, float f) {
        AnonymousClass0A3 r1 = (AnonymousClass0A3) ((AnonymousClass0XY) r3).A00;
        if (f != r1.A01) {
            r1.A01 = f;
            r1.A00(null);
            r1.invalidateSelf();
        }
    }
}
