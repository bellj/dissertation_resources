package X;

/* renamed from: X.34L  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34L extends AbstractC69383Za {
    public AnonymousClass34L(AnonymousClass1ZL r1) {
        super(r1);
    }

    @Override // X.AbstractC69383Za, X.AbstractC33751f1
    public void A6l(C39971qq r9, AnonymousClass1XE r10) {
        AbstractC27091Fz r0;
        super.A6l(r9, r10);
        AnonymousClass1ZL r3 = r10.A00;
        if (r3 != null && r3.A00 != null && r3.A04 == 2) {
            AnonymousClass1G3 r6 = r9.A03;
            C57452n4 r02 = ((C27081Fy) r6.A00).A0L;
            if (r02 == null) {
                r02 = C57452n4.A05;
            }
            AnonymousClass1G4 A0T = r02.A0T();
            C57452n4 r1 = (C57452n4) A0T.A00;
            if (r1.A01 == 2) {
                r0 = (AbstractC27091Fz) r1.A04;
            } else {
                r0 = C57352mt.A04;
            }
            AnonymousClass1G4 A0T2 = r0.A0T();
            AnonymousClass1ZM r32 = r3.A00;
            String str = r32.A00;
            if (str != null) {
                C57352mt r12 = (C57352mt) AnonymousClass1G4.A00(A0T2);
                r12.A00 |= 1;
                r12.A02 = str;
            }
            String str2 = r32.A01;
            if (str2 != null) {
                C57352mt r13 = (C57352mt) AnonymousClass1G4.A00(A0T2);
                r13.A00 |= 2;
                r13.A03 = str2;
            }
            C57452n4 r14 = (C57452n4) AnonymousClass1G4.A00(A0T);
            r14.A04 = A0T2.A02();
            r14.A01 = 2;
            C27081Fy r15 = (C27081Fy) AnonymousClass1G4.A00(r6);
            r15.A0L = (C57452n4) A0T.A02();
            r15.A01 |= 32;
        }
    }
}
