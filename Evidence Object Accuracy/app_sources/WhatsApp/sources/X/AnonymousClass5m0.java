package X;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.5m0  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5m0 extends AbstractC118835cS {
    public final Context A00;
    public final View A01;
    public final Button A02;
    public final ImageView A03;
    public final TextView A04;
    public final TextView A05;
    public final TextEmojiLabel A06;
    public final AnonymousClass01d A07;

    public AnonymousClass5m0(View view, AnonymousClass01d r3) {
        super(view);
        this.A07 = r3;
        this.A00 = view.getContext();
        this.A02 = (Button) AnonymousClass028.A0D(view, R.id.complaint_button);
        this.A01 = AnonymousClass028.A0D(view, R.id.transaction_complaint_status);
        this.A03 = C12970iu.A0K(view, R.id.transaction_complaint_status_icon);
        this.A05 = C12960it.A0I(view, R.id.transaction_complaint_status_title);
        this.A04 = C12960it.A0I(view, R.id.transaction_complaint_status_subtitle);
        this.A06 = C12970iu.A0T(view, R.id.transaction_complaint_status_time);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r7, int i) {
        C123295mv r72 = (C123295mv) r7;
        Button button = this.A02;
        button.setOnClickListener(r72.A01);
        ImageView imageView = this.A03;
        imageView.setImageResource(r72.A00);
        Context context = this.A00;
        imageView.setColorFilter(context.getResources().getColor(R.color.wds_cool_gray_300), PorterDuff.Mode.SRC_IN);
        this.A05.setText(r72.A04);
        TextView textView = this.A04;
        textView.setText(r72.A02);
        this.A06.setText(r72.A03);
        if (r72.A06) {
            C12980iv.A14(context.getResources(), button, R.color.disabled_text);
        }
        if (r72.A07 && r72.A02 == null) {
            textView.setVisibility(8);
        }
        if (r72.A05) {
            imageView.setVisibility(0);
        } else {
            imageView.setVisibility(8);
        }
        if (r72.A07) {
            button.setVisibility(8);
            this.A01.setVisibility(0);
            return;
        }
        button.setVisibility(0);
        this.A01.setVisibility(8);
    }
}
