package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.2Ql  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50662Ql extends AnimatorListenerAdapter {
    public boolean A00;
    public final /* synthetic */ C50632Qh A01;
    public final /* synthetic */ boolean A02;

    public C50662Ql(C50632Qh r1, boolean z) {
        this.A01 = r1;
        this.A02 = z;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        this.A00 = true;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C50632Qh r1 = this.A01;
        r1.A05 = 0;
        r1.A07 = null;
        if (!this.A00) {
            AnonymousClass2QW r2 = r1.A0N;
            boolean z = this.A02;
            int i = 4;
            if (z) {
                i = 8;
            }
            r2.A00(i, z);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        C50632Qh r3 = this.A01;
        r3.A0N.A00(0, this.A02);
        r3.A05 = 1;
        r3.A07 = animator;
        this.A00 = false;
    }
}
