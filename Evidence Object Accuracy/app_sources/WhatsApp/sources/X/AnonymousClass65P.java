package X;

import com.whatsapp.payments.ui.PaymentGroupParticipantPickerActivity;
import java.util.ArrayList;

/* renamed from: X.65P  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass65P implements AnonymousClass07L {
    public final /* synthetic */ PaymentGroupParticipantPickerActivity A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public AnonymousClass65P(PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity) {
        this.A00 = paymentGroupParticipantPickerActivity;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        PaymentGroupParticipantPickerActivity paymentGroupParticipantPickerActivity = this.A00;
        ArrayList A02 = C32751cg.A02(((ActivityC13830kP) paymentGroupParticipantPickerActivity).A01, str);
        paymentGroupParticipantPickerActivity.A0K = A02;
        if (A02.isEmpty()) {
            paymentGroupParticipantPickerActivity.A0K = null;
        }
        AnonymousClass5oZ r1 = paymentGroupParticipantPickerActivity.A0E;
        if (r1 != null) {
            r1.A03(true);
            paymentGroupParticipantPickerActivity.A0E = null;
        }
        AnonymousClass5oZ r12 = new AnonymousClass5oZ(paymentGroupParticipantPickerActivity, paymentGroupParticipantPickerActivity.A0K);
        paymentGroupParticipantPickerActivity.A0E = r12;
        C12960it.A1E(r12, ((ActivityC13830kP) paymentGroupParticipantPickerActivity).A05);
        return false;
    }
}
