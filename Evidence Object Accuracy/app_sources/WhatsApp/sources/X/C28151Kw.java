package X;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.1Kw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28151Kw extends Number implements Serializable {
    public static final long serialVersionUID = 0;
    public transient AtomicLong A00;

    public C28151Kw() {
        this(0.0d);
    }

    public C28151Kw(double d) {
        this.A00 = new AtomicLong(Double.doubleToRawLongBits(d));
    }

    @Override // java.lang.Number
    public double doubleValue() {
        return Double.longBitsToDouble(this.A00.get());
    }

    @Override // java.lang.Number
    public float floatValue() {
        return (float) Double.longBitsToDouble(this.A00.get());
    }

    @Override // java.lang.Number
    public int intValue() {
        return (int) Double.longBitsToDouble(this.A00.get());
    }

    @Override // java.lang.Number
    public long longValue() {
        return (long) Double.longBitsToDouble(this.A00.get());
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.A00 = new AtomicLong();
        this.A00.set(Double.doubleToRawLongBits(objectInputStream.readDouble()));
    }

    @Override // java.lang.Object
    public String toString() {
        return Double.toString(Double.longBitsToDouble(this.A00.get()));
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeDouble(Double.longBitsToDouble(this.A00.get()));
    }
}
