package X;

import androidx.preference.PreferenceFragmentCompat;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.0cb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09250cb implements Runnable {
    public final /* synthetic */ PreferenceFragmentCompat A00;

    public RunnableC09250cb(PreferenceFragmentCompat preferenceFragmentCompat) {
        this.A00 = preferenceFragmentCompat;
    }

    @Override // java.lang.Runnable
    public void run() {
        RecyclerView recyclerView = this.A00.A03;
        recyclerView.focusableViewAvailable(recyclerView);
    }
}
