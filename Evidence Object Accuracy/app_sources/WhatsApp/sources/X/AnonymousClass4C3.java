package X;

/* renamed from: X.4C3  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4C3 extends Exception {
    public final int audioTrackState;
    public final C100614mC format;
    public final boolean isRecoverable;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4C3(X.C100614mC r3, java.lang.Exception r4, int r5, int r6, int r7, int r8, boolean r9) {
        /*
            r2 = this;
            java.lang.String r0 = "AudioTrack init failed "
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r5)
            java.lang.String r0 = " "
            r1.append(r0)
            java.lang.String r0 = "Config("
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = ", "
            r1.append(r0)
            r1.append(r7)
            r1.append(r0)
            r1.append(r8)
            java.lang.String r0 = ")"
            r1.append(r0)
            if (r9 == 0) goto L_0x003b
            java.lang.String r0 = " (recoverable)"
        L_0x002d:
            java.lang.String r0 = X.C12960it.A0d(r0, r1)
            r2.<init>(r0, r4)
            r2.audioTrackState = r5
            r2.isRecoverable = r9
            r2.format = r3
            return
        L_0x003b:
            java.lang.String r0 = ""
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4C3.<init>(X.4mC, java.lang.Exception, int, int, int, int, boolean):void");
    }
}
