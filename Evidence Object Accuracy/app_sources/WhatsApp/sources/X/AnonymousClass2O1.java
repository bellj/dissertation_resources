package X;

import android.content.Context;
import android.os.Bundle;
import java.util.List;

/* renamed from: X.2O1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2O1 extends AbstractC16350or {
    public Context A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final C20970wc A04;
    public final AnonymousClass131 A05;
    public final List A06;

    public /* synthetic */ AnonymousClass2O1(Context context, C20970wc r2, AnonymousClass131 r3, List list, int i, int i2, int i3) {
        this.A00 = context;
        this.A06 = list;
        this.A02 = i;
        this.A01 = i2;
        this.A03 = i3;
        this.A04 = r2;
        AnonymousClass009.A05(r3);
        this.A05 = r3;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        Bundle bundle = new Bundle();
        bundle.putInt("notification_type", this.A03);
        this.A04.A00(new C26391De("refresh_notification", bundle));
    }
}
