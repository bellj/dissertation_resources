package X;

import android.app.Activity;

/* renamed from: X.52T  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52T implements AbstractC116445Vl {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Activity A01;

    public AnonymousClass52T(Activity activity, int i) {
        this.A01 = activity;
        this.A00 = i;
    }

    @Override // X.AbstractC116445Vl
    public void AUr() {
        C36021jC.A00(this.A01, this.A00);
    }

    @Override // X.AbstractC116445Vl
    public void AW3(int i) {
        C36021jC.A01(this.A01, i);
    }
}
