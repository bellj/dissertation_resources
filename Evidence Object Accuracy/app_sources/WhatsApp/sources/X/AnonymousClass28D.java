package X;

import android.util.SparseArray;
import android.view.View;
import com.bloks.components.bkcomponentsslider.BKBloksComponentsSliderBinderUtil;
import com.bloks.stdlib.components.bkcomponentsstdimplprogressbar.BKBloksComponentsStdImplProgressBarBinderUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.28D  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass28D {
    public final int A00;
    public final int A01;
    public final SparseArray A02;
    public final C89054Im A03;
    public final AnonymousClass28D A04;
    public final LinkedList A05;
    public final List A06;

    public AnonymousClass28D(int i) {
        this.A03 = new C89054Im();
        this.A05 = null;
        this.A02 = new SparseArray();
        this.A01 = i;
        this.A00 = C88454Fs.A00.incrementAndGet();
        this.A06 = null;
        this.A04 = null;
        C88824Hg.A03.incrementAndGet();
    }

    public AnonymousClass28D(int i, int i2) {
        this.A03 = new C89054Im();
        this.A05 = null;
        this.A02 = new SparseArray(i2 + 1);
        this.A01 = i;
        this.A00 = C88454Fs.A00.incrementAndGet();
        this.A06 = null;
        this.A04 = null;
        C88824Hg.A03.incrementAndGet();
    }

    public AnonymousClass28D(AnonymousClass28D r5, AnonymousClass28D r6, List list, int i) {
        int i2;
        Integer num;
        int intValue;
        this.A03 = r5.A03;
        this.A05 = r5.A05;
        SparseArray clone = r5.A02.clone();
        List A0K = r5.A0K();
        C65093Ic.A00();
        int i3 = r5.A01;
        if (i3 == 13504) {
            i2 = 32;
        } else if (AnonymousClass4DC.A00(i3)) {
            i2 = A02(r5, i3);
        } else {
            num = null;
            if (!(A0K == Collections.EMPTY_LIST || num == null || (intValue = num.intValue()) == -1)) {
                clone.put(intValue, new ArrayList(A0K));
            }
            this.A02 = clone;
            this.A01 = r5.A01;
            this.A00 = i;
            this.A06 = list;
            this.A04 = r6;
            C88824Hg.A03.incrementAndGet();
        }
        num = Integer.valueOf(i2);
        if (A0K == Collections.EMPTY_LIST) {
            clone.put(intValue, new ArrayList(A0K));
        }
        this.A02 = clone;
        this.A01 = r5.A01;
        this.A00 = i;
        this.A06 = list;
        this.A04 = r6;
        C88824Hg.A03.incrementAndGet();
    }

    public AnonymousClass28D(AnonymousClass28D r2, AnonymousClass4RF r3) {
        this.A03 = r2.A03;
        LinkedList linkedList = r2.A05;
        linkedList = linkedList == null ? new LinkedList() : linkedList;
        this.A05 = linkedList;
        linkedList.addFirst(r3);
        this.A02 = r2.A02;
        this.A01 = r2.A01;
        this.A00 = r2.A00;
        this.A06 = r2.A06;
        AnonymousClass28D r0 = r2.A04;
        this.A04 = r0 == null ? null : r0;
        C88824Hg.A03.incrementAndGet();
    }

    public static float A00(AnonymousClass28D r0, int i) {
        String A0I = r0.A0I(i);
        if (A0I == null) {
            return 0.0f;
        }
        return AnonymousClass3JW.A01(A0I);
    }

    public static int A01(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        return ((mode == Integer.MIN_VALUE || mode == 1073741824) && size < i) ? size : i;
    }

    public static int A02(AnonymousClass28D r2, int i) {
        if (i == 13317 || i == 13320) {
            return 32;
        }
        if (i == 13326 || i == 13336) {
            return -1;
        }
        if (i == 16160) {
            return BKBloksComponentsSliderBinderUtil.getChildrenKeyForComponent(r2);
        }
        if (i == 15778 || i == 15728 || i == 13334 || i == 13666 || i == 13329 || i == 13335) {
            return -1;
        }
        if (i == 13797) {
            return 32;
        }
        if (i == 16229) {
            return BKBloksComponentsStdImplProgressBarBinderUtil.getChildrenKeyForComponent(r2);
        }
        if (i == 13327) {
            return 35;
        }
        throw new IllegalArgumentException(String.format("No implementation bound to key: %s", Integer.valueOf(i)));
    }

    public static int A03(String str, String str2) {
        float A01;
        if (str == null) {
            A01 = 0.0f;
        } else {
            try {
                A01 = AnonymousClass3JW.A01(str);
            } catch (AnonymousClass491 e) {
                C28691Op.A01("RichTextBinderUtils", String.format("Error parsing %s: %s", str2, str), e);
                return 0;
            }
        }
        return (int) A01;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0102 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass0ND A04(X.C14260l7 r20, X.AnonymousClass28D r21, java.util.List r22) {
        /*
        // Method dump skipped, instructions count: 509
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass28D.A04(X.0l7, X.28D, java.util.List):X.0ND");
    }

    public static AnonymousClass28D A05(AnonymousClass28D r1) {
        return r1.A0F(132);
    }

    public static String A06(AnonymousClass28D r1) {
        return r1.A0I(35);
    }

    public static void A07(C71163cU r2, Integer num) {
        EnumC630039m r0;
        int intValue = num.intValue();
        if (intValue == 1) {
            r0 = EnumC630039m.CENTER;
        } else if (intValue == 8388611 || intValue != 8388613) {
            r0 = EnumC630039m.TEXT_START;
        } else {
            r0 = EnumC630039m.TEXT_END;
        }
        r2.A0T = r0;
    }

    public static boolean A08(int i, int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i);
        int size2 = View.MeasureSpec.getSize(i);
        if (i == i2) {
            return true;
        }
        if (mode2 == 0 && mode == 0) {
            return true;
        }
        if (mode == 1073741824 && size == i3) {
            return true;
        }
        if (mode == Integer.MIN_VALUE && mode2 == 0) {
            if (size >= i3) {
                return true;
            }
            return false;
        } else if (mode2 != Integer.MIN_VALUE || mode != Integer.MIN_VALUE || size2 <= size || i3 > size) {
            return false;
        } else {
            return true;
        }
    }

    public float A09(int i, float f) {
        Number number = (Number) this.A02.get(i);
        return number != null ? number.floatValue() : f;
    }

    public int A0A() {
        return this.A01;
    }

    public int A0B(int i, int i2) {
        Object obj = this.A02.get(i);
        if (obj == null) {
            return i2;
        }
        if (!(obj instanceof String)) {
            return ((Number) obj).intValue();
        }
        try {
            return Integer.parseInt((String) obj);
        } catch (NumberFormatException unused) {
            C28691Op.A00("BloksModel", "Non-int string parsed as int");
            return i2;
        }
    }

    public long A0C(int i) {
        Object obj = this.A02.get(i);
        if (obj == null) {
            return 0;
        }
        if (!(obj instanceof String)) {
            return ((Number) obj).longValue();
        }
        try {
            return Long.parseLong((String) obj);
        } catch (NumberFormatException unused) {
            C28691Op.A00("BloksModel", "Non-long string parsed as long");
            return 0;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:371:0x06cc */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:370:0x06ca */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:460:0x0841 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:459:0x083f */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r34v0, types: [X.28D, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r0v9, types: [X.3Ia] */
    /* JADX WARN: Type inference failed for: r0v86, types: [X.2nj] */
    /* JADX WARN: Type inference failed for: r0v88, types: [X.2ng] */
    /* JADX WARN: Type inference failed for: r0v90, types: [X.2nl] */
    /* JADX WARN: Type inference failed for: r0v92, types: [X.2nh] */
    /* JADX WARN: Type inference failed for: r0v94, types: [X.3w2] */
    /* JADX WARN: Type inference failed for: r0v96 */
    /* JADX WARN: Type inference failed for: r0v99, types: [X.2k9, X.3Ia] */
    /* JADX WARN: Type inference failed for: r0v102, types: [X.3w4] */
    /* JADX WARN: Type inference failed for: r0v104, types: [X.3w3] */
    /* JADX WARN: Type inference failed for: r0v107, types: [X.0Ig] */
    /* JADX WARN: Type inference failed for: r0v109, types: [X.2nc] */
    /* JADX WARN: Type inference failed for: r0v110, types: [X.2nf] */
    /* JADX WARN: Type inference failed for: r0v111, types: [X.2ne] */
    /* JADX WARN: Type inference failed for: r0v112, types: [X.2no] */
    /* JADX WARN: Type inference failed for: r0v113, types: [X.2ni] */
    /* JADX WARN: Type inference failed for: r0v114, types: [X.2nd] */
    /* JADX WARN: Type inference failed for: r0v115, types: [X.2nb] */
    /* JADX WARN: Type inference failed for: r0v116, types: [X.2nk] */
    /* JADX WARN: Type inference failed for: r0v117, types: [X.2nn] */
    /* JADX WARN: Type inference failed for: r0v129 */
    /* JADX WARN: Type inference failed for: r0v130 */
    /* JADX WARN: Type inference failed for: r0v131, types: [X.3Ia] */
    /* JADX WARN: Type inference failed for: r0v133, types: [X.2k9] */
    /* JADX WARN: Type inference failed for: r0v134, types: [X.2k9] */
    /* JADX WARN: Type inference failed for: r0v136, types: [X.2k9] */
    /* JADX WARN: Type inference failed for: r0v179, types: [X.2nm] */
    /* JADX WARN: Type inference failed for: r0v180, types: [X.3Ia] */
    /* JADX WARN: Type inference failed for: r0v182, types: [X.2k9] */
    /* JADX WARN: Type inference failed for: r0v184, types: [X.2k8, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r0v185, types: [X.3Ia] */
    /* JADX WARN: Type inference failed for: r0v188, types: [X.0If] */
    /* JADX WARN: Type inference failed for: r0v189, types: [X.0Ie] */
    /* JADX WARN: Type inference failed for: r0v190, types: [X.0Id] */
    /* JADX WARN: Type inference failed for: r0v191, types: [X.0Ic] */
    /* JADX WARN: Type inference failed for: r0v192, types: [X.3Ia] */
    /* JADX WARN: Type inference failed for: r0v193, types: [X.2na] */
    /* JADX WARN: Type inference failed for: r0v194, types: [X.3w1] */
    /* JADX WARN: Type inference failed for: r0v199, types: [X.3Ia, java.lang.Object, X.2kA] */
    /* JADX WARN: Type inference failed for: r0v234 */
    /* JADX WARN: Type inference failed for: r0v235 */
    /* JADX WARNING: Code restructure failed: missing block: B:1056:0x1824, code lost:
        if (r5 != 1) goto L_0x1826;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x022b, code lost:
        if (r15.A04 == r5.A04) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x03c4, code lost:
        if (X.C87834De.A00(r6.A03, r13) != false) goto L_0x041a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x0418, code lost:
        if (X.C87834De.A00(r6.A03, r13) != false) goto L_0x041a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:510:0x09c0, code lost:
        if (r6 == X.EnumC869849t.A02) goto L_0x09c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:774:0x10dd, code lost:
        if (r12 > r6) goto L_0x10ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0192, code lost:
        if (A0F(87) != null) goto L_0x0194;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:781:0x10ed, code lost:
        if (r7 == Integer.MIN_VALUE) goto L_0x10ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:801:0x1141, code lost:
        if (android.view.View.MeasureSpec.getMode(r37) == 1073741824) goto L_0x1143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:860:0x126e, code lost:
        if (r12 == Integer.MIN_VALUE) goto L_0x1270;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:925:0x14a6, code lost:
        if (r3 != null) goto L_0x1489;
     */
    /* JADX WARNING: Removed duplicated region for block: B:1119:0x18fc  */
    /* JADX WARNING: Removed duplicated region for block: B:1121:0x1901  */
    /* JADX WARNING: Removed duplicated region for block: B:1129:0x1917  */
    /* JADX WARNING: Removed duplicated region for block: B:1132:0x191e  */
    /* JADX WARNING: Removed duplicated region for block: B:359:0x0690 A[Catch: IOException -> 0x085a, TryCatch #13 {IOException -> 0x085a, blocks: (B:335:0x0629, B:337:0x0633, B:338:0x0639, B:340:0x0641, B:341:0x0646, B:342:0x0649, B:343:0x065d, B:344:0x065e, B:346:0x0667, B:347:0x066c, B:350:0x0675, B:356:0x0685, B:357:0x0689, B:359:0x0690, B:360:0x069a, B:363:0x06a5, B:364:0x06ad, B:365:0x06b0, B:368:0x06ba, B:369:0x06c2, B:372:0x06ce, B:373:0x06d6, B:375:0x06e4, B:377:0x06fc, B:379:0x0702, B:380:0x0709, B:382:0x0713, B:384:0x072b, B:386:0x0731, B:390:0x073f, B:392:0x0742, B:395:0x074f, B:398:0x075b, B:405:0x076a, B:408:0x0775, B:410:0x077b, B:412:0x077e, B:414:0x0786, B:415:0x078b, B:417:0x0791, B:418:0x0794, B:419:0x079a, B:421:0x07a0, B:423:0x07ae, B:425:0x07b4, B:428:0x07be, B:429:0x07c3, B:430:0x07ce, B:433:0x07d6, B:434:0x07da, B:437:0x07e6, B:438:0x07ea, B:441:0x07f6, B:442:0x07fa, B:445:0x0806, B:446:0x080a, B:449:0x0816, B:450:0x081a, B:453:0x0826, B:454:0x082a, B:456:0x082e, B:457:0x0838, B:458:0x083b, B:461:0x0843, B:462:0x084b), top: B:1173:0x0629, inners: #9, #20 }] */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x06a3  */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x06b8  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x06cc  */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x07a0 A[Catch: IOException -> 0x085a, TryCatch #13 {IOException -> 0x085a, blocks: (B:335:0x0629, B:337:0x0633, B:338:0x0639, B:340:0x0641, B:341:0x0646, B:342:0x0649, B:343:0x065d, B:344:0x065e, B:346:0x0667, B:347:0x066c, B:350:0x0675, B:356:0x0685, B:357:0x0689, B:359:0x0690, B:360:0x069a, B:363:0x06a5, B:364:0x06ad, B:365:0x06b0, B:368:0x06ba, B:369:0x06c2, B:372:0x06ce, B:373:0x06d6, B:375:0x06e4, B:377:0x06fc, B:379:0x0702, B:380:0x0709, B:382:0x0713, B:384:0x072b, B:386:0x0731, B:390:0x073f, B:392:0x0742, B:395:0x074f, B:398:0x075b, B:405:0x076a, B:408:0x0775, B:410:0x077b, B:412:0x077e, B:414:0x0786, B:415:0x078b, B:417:0x0791, B:418:0x0794, B:419:0x079a, B:421:0x07a0, B:423:0x07ae, B:425:0x07b4, B:428:0x07be, B:429:0x07c3, B:430:0x07ce, B:433:0x07d6, B:434:0x07da, B:437:0x07e6, B:438:0x07ea, B:441:0x07f6, B:442:0x07fa, B:445:0x0806, B:446:0x080a, B:449:0x0816, B:450:0x081a, B:453:0x0826, B:454:0x082a, B:456:0x082e, B:457:0x0838, B:458:0x083b, B:461:0x0843, B:462:0x084b), top: B:1173:0x0629, inners: #9, #20 }] */
    /* JADX WARNING: Removed duplicated region for block: B:460:0x0841  */
    /* JADX WARNING: Removed duplicated region for block: B:514:0x09df  */
    /* JADX WARNING: Removed duplicated region for block: B:520:0x09fb  */
    /* JADX WARNING: Removed duplicated region for block: B:751:0x108a  */
    /* JADX WARNING: Removed duplicated region for block: B:776:0x10e2  */
    /* JADX WARNING: Removed duplicated region for block: B:780:0x10eb  */
    /* JADX WARNING: Removed duplicated region for block: B:962:0x1542  */
    /* JADX WARNING: Removed duplicated region for block: B:965:0x1557  */
    /* JADX WARNING: Removed duplicated region for block: B:968:0x156b  */
    /* JADX WARNING: Removed duplicated region for block: B:971:0x1585  */
    /* JADX WARNING: Removed duplicated region for block: B:974:0x15a6  */
    /* JADX WARNING: Removed duplicated region for block: B:977:0x15b6  */
    /* JADX WARNING: Removed duplicated region for block: B:980:0x15c6  */
    /* JADX WARNING: Removed duplicated region for block: B:982:0x15d0  */
    /* JADX WARNING: Removed duplicated region for block: B:984:0x15e6  */
    /* JADX WARNING: Removed duplicated region for block: B:988:0x15fa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AbstractC72393eW A0D(X.C92304Vj r35, int r36, int r37) {
        /*
        // Method dump skipped, instructions count: 6674
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass28D.A0D(X.4Vj, int, int):X.3eW");
    }

    public final AbstractC65073Ia A0E(AbstractC65073Ia r4, C14260l7 r5) {
        if (r4 == null) {
            r4 = new C55992k9((long) this.A00, AnonymousClass3JV.A09(r5));
        }
        r4.A04(new C93304Zx(new C106324vW(this.A03), r4));
        if (r4.A04 != EnumC869849t.DRAWABLE) {
            return r4;
        }
        throw new IllegalStateException("Trying to apply View attributes to a Drawable Node is not yet supported");
    }

    public AnonymousClass28D A0F(int i) {
        Object obj;
        SparseArray sparseArray = this.A02;
        Object obj2 = sparseArray.get(i);
        if (obj2 instanceof List) {
            obj = A0L(i).get(0);
        } else if (!(obj2 instanceof AnonymousClass28D)) {
            return null;
        } else {
            obj = sparseArray.get(i);
        }
        return (AnonymousClass28D) obj;
    }

    public AbstractC14200l1 A0G(int i) {
        Object obj = this.A02.get(i);
        if (obj == null) {
            return null;
        }
        if (obj instanceof AbstractC14200l1) {
            return (AbstractC14200l1) obj;
        }
        if (obj instanceof C94724cR) {
            return ((C94724cR) obj).A00;
        }
        return new C1093751l(new C90014Mg((String) obj), null, 0);
    }

    public String A0H() {
        Object obj = this.A02.get(33);
        if (obj == null) {
            return null;
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Number) {
            return String.valueOf(((Number) obj).longValue());
        }
        StringBuilder sb = new StringBuilder("Bloks id only supports long and String types but got: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }

    public String A0I(int i) {
        return (String) this.A02.get(i);
    }

    public String A0J(int i, String str) {
        String str2 = (String) this.A02.get(i);
        return str2 != null ? str2 : str;
    }

    public List A0K() {
        int i;
        int intValue;
        List A0L;
        C65093Ic.A00();
        int i2 = this.A01;
        if (i2 != 13504) {
            if (AnonymousClass4DC.A00(i2)) {
                i = A02(this, i2);
            }
            return Collections.EMPTY_LIST;
        }
        i = 32;
        Integer valueOf = Integer.valueOf(i);
        if (!(valueOf == null || (intValue = valueOf.intValue()) == -1 || (A0L = A0L(intValue)) == null)) {
            return A0L;
        }
        return Collections.EMPTY_LIST;
    }

    public List A0L(int i) {
        Object obj = this.A02.get(i);
        if (obj == null) {
            return Collections.emptyList();
        }
        if (!(obj instanceof AnonymousClass28D)) {
            return (List) obj;
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(obj);
        return arrayList;
    }

    public List A0M(int i) {
        List list = (List) this.A02.get(i);
        return list == null ? Collections.emptyList() : list;
    }

    public void A0N(AnonymousClass5T0 r5) {
        int i = 0;
        while (true) {
            SparseArray sparseArray = this.A02;
            if (i < sparseArray.size()) {
                Object valueAt = sparseArray.valueAt(i);
                if (valueAt == null) {
                    C28691Op.A00("BloksModel", "Null value found during traversal");
                } else {
                    r5.Afj(sparseArray.keyAt(i), valueAt);
                }
                i++;
            } else {
                return;
            }
        }
    }

    public boolean A0O(int i, boolean z) {
        Object obj = this.A02.get(i);
        if (obj == null) {
            return z;
        }
        if (obj instanceof Boolean) {
            return ((Boolean) obj).booleanValue();
        }
        if (obj instanceof Number) {
            return ((Number) obj).intValue() == 1;
        }
        C28691Op.A00("ParseUtils", "Attempting to extract boolean value from unrecognized value type");
        return z;
    }

    public boolean A0P(AnonymousClass5T3 r8) {
        if (!r8.Afk(this)) {
            for (int i : C65093Ic.A00().A06().A01(this)) {
                AnonymousClass28D A0F = A0F(i);
                if (A0F == null || !A0F.A0P(r8)) {
                }
            }
            for (int i2 : C65093Ic.A00().A06().A00(this)) {
                List A0L = A0L(i2);
                for (int i3 = 0; i3 < A0L.size(); i3++) {
                    AnonymousClass28D r0 = (AnonymousClass28D) A0L.get(i3);
                    if (r0 != null && r0.A0P(r8)) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }
}
