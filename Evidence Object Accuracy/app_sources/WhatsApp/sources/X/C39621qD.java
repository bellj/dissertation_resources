package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Process;
import com.facebook.animated.webp.WebPFrame;
import com.facebook.animated.webp.WebPImage;
import com.facebook.redex.RunnableBRunnable0Shape8S0200000_I0_8;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.PriorityQueue;

/* renamed from: X.1qD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C39621qD extends AnonymousClass1MS {
    public final C14900mE A00;
    public final C14850m9 A01;
    public final AnonymousClass1EL A02;
    public final C39681qL A03;
    public final AnonymousClass4LH A04 = new AnonymousClass4LH();
    public volatile AnonymousClass4LG A05;
    public volatile boolean A06;

    public /* synthetic */ C39621qD(C14900mE r2, C14850m9 r3, AnonymousClass1EL r4, AnonymousClass4LG r5, C39681qL r6) {
        super("StickerFramePreloader");
        this.A01 = r3;
        this.A00 = r2;
        this.A03 = r6;
        this.A02 = r4;
        this.A05 = r5;
    }

    public void A00() {
        this.A06 = true;
        C39601qB r1 = this.A05.A00;
        r1.A00 = null;
        C39681qL r12 = r1.A04;
        synchronized (r12) {
            r12.A00 = null;
        }
        interrupt();
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        C27991Ka r10;
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        Process.setThreadPriority(1);
        Bitmap bitmap4 = null;
        while (!this.A06) {
            try {
                C39681qL r3 = this.A03;
                synchronized (r3) {
                    PriorityQueue priorityQueue = r3.A01;
                    if (priorityQueue.isEmpty()) {
                        r3.wait(5000);
                    }
                    if (!priorityQueue.isEmpty()) {
                        r10 = (C27991Ka) priorityQueue.remove();
                    } else {
                        C39621qD r0 = r3.A00;
                        if (r0 != null) {
                            r0.A00();
                        }
                        r10 = null;
                    }
                }
                if (this.A01.A07(276)) {
                    if (r10 != null) {
                        AnonymousClass4LH r5 = this.A04;
                        int i = r10.A02.A06;
                        synchronized (r5) {
                            HashMap hashMap = r5.A00;
                            Integer valueOf = Integer.valueOf(i);
                            if (hashMap.containsKey(valueOf)) {
                                bitmap3 = (Bitmap) hashMap.get(valueOf);
                            } else {
                                bitmap3 = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
                                hashMap.put(valueOf, bitmap3);
                            }
                        }
                        bitmap4 = bitmap3;
                    }
                } else if (bitmap4 == null && r10 != null) {
                    C64253Ev r02 = r10.A02;
                    bitmap4 = Bitmap.createBitmap(r02.A06, r02.A05, Bitmap.Config.ARGB_8888);
                }
            } catch (InterruptedException unused) {
            } catch (Exception e) {
                Log.e("StickerFramePreloader/FrameLoaderThread failed to load frame ", e);
            }
            if (this.A06) {
                break;
            } else if (r10 != null) {
                C64253Ev r9 = r10.A02;
                AnonymousClass1EL r8 = this.A02;
                synchronized (r9) {
                    if (r9.A01 == null) {
                        Bitmap createBitmap = Bitmap.createBitmap(r9.A06, r9.A05, Bitmap.Config.ARGB_8888);
                        r9.A01 = createBitmap;
                        Canvas canvas = new Canvas(createBitmap);
                        r9.A03 = canvas;
                        canvas.drawBitmap(r9.A08, 0.0f, 0.0f, (Paint) null);
                    }
                    AnonymousClass009.A05(r9.A01);
                    AnonymousClass009.A05(r9.A03);
                    int i2 = r9.A00;
                    r9.A00 = (i2 + 1) % r9.A07;
                    WebPImage webPImage = r9.A0A;
                    C91744Sy frameInfo = webPImage.getFrameInfo(i2);
                    WebPFrame frame = webPImage.getFrame(r9.A00);
                    C91744Sy frameInfo2 = webPImage.getFrameInfo(r9.A00);
                    int i3 = r9.A00;
                    if (i3 == 0) {
                        bitmap = r9.A08;
                    } else {
                        String str = r9.A0B;
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append("_frame_");
                        sb.append(i3);
                        String obj = sb.toString();
                        AnonymousClass1O4 A03 = r8.A00.A03();
                        bitmap = (Bitmap) A03.A00(obj);
                        if (bitmap != null && bitmap.isRecycled()) {
                            A03.A00.A07(obj);
                            bitmap = null;
                        }
                    }
                    if (bitmap != null) {
                        r9.A02 = bitmap;
                        r9.A01.eraseColor(0);
                        r9.A03.drawBitmap(r9.A02, 0.0f, 0.0f, (Paint) null);
                        frame.dispose();
                        bitmap2 = r9.A02;
                    } else {
                        bitmap4.eraseColor(0);
                        float f = r9.A04;
                        frame.renderFrame((int) Math.ceil((double) (((float) frame.getWidth()) * f)), (int) Math.ceil((double) (((float) frame.getHeight()) * f)), bitmap4);
                        if (frameInfo.A05 == AnonymousClass4A8.DISPOSE_TO_BACKGROUND) {
                            r9.A00(r9.A03, frameInfo);
                        }
                        if (frameInfo2.A04 == EnumC869749s.NO_BLEND) {
                            r9.A00(r9.A03, frameInfo2);
                        }
                        int xOffset = frame.getXOffset();
                        int yOffset = frame.getYOffset();
                        frame.dispose();
                        r9.A03.drawBitmap(bitmap4, ((float) xOffset) * f, ((float) yOffset) * f, (Paint) null);
                        try {
                            Bitmap bitmap5 = r9.A01;
                            Bitmap copy = bitmap5.copy(bitmap5.getConfig(), false);
                            r9.A02 = copy;
                            String str2 = r9.A0B;
                            int i4 = r9.A00;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str2);
                            sb2.append("_frame_");
                            sb2.append(i4);
                            String obj2 = sb2.toString();
                            if (!copy.isRecycled()) {
                                r8.A00.A03().A03(obj2, copy);
                            }
                        } catch (OutOfMemoryError e2) {
                            r9.A02 = r9.A01;
                            Log.e("AnimatedWebpRenderer/renderNextFrame/OutofMemoryError: ", e2);
                        }
                        bitmap2 = r9.A02;
                    }
                }
                this.A00.A0H(new RunnableBRunnable0Shape8S0200000_I0_8(r10, 11, bitmap2));
            }
        }
        if (this.A01.A07(276)) {
            AnonymousClass4LH r4 = this.A04;
            synchronized (r4) {
                HashMap hashMap2 = r4.A00;
                Collection<Bitmap> values = hashMap2.values();
                values.size();
                for (Bitmap bitmap6 : values) {
                    if (bitmap6 != null && !bitmap6.isRecycled()) {
                        bitmap6.recycle();
                    }
                }
                hashMap2.clear();
            }
        } else if (bitmap4 != null) {
            bitmap4.recycle();
        }
    }
}
