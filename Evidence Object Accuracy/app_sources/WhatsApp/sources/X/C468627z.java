package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.27z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C468627z implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30801Yw(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30801Yw[i];
    }
}
