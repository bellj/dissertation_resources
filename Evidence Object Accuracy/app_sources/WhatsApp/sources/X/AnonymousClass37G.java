package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.37G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37G extends AbstractC16350or {
    public final C241414j A00;
    public final UserJid A01;
    public final C91674Sq A02;

    public AnonymousClass37G(C241414j r1, UserJid userJid, C91674Sq r3) {
        this.A00 = r1;
        this.A01 = userJid;
        this.A02 = r3;
    }
}
