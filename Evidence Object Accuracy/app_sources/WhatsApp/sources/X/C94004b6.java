package X;

/* renamed from: X.4b6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94004b6 {
    public static final C94004b6 A02 = new C94004b6(new C87874Dj(), 4);
    public static final C94004b6 A03 = new C94004b6(new C87874Dj(), 5);
    public final int A00;
    public final C87874Dj A01;

    static {
        new AnonymousClass4VI(false);
        new AnonymousClass4VI(false);
    }

    public C94004b6(C87874Dj r1, int i) {
        this.A00 = i;
        this.A01 = r1;
    }

    public boolean A00() {
        int i = this.A00;
        return i == 0 || i == 1 || i == 2;
    }
}
