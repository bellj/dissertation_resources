package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.camera.recording.RecordingView;
import java.io.File;

/* renamed from: X.2a4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2a4 extends Handler {
    public final /* synthetic */ AnonymousClass1sA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2a4(Looper looper, AnonymousClass1sA r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        ActivityC13810kN r0;
        int i = message.what;
        AnonymousClass1s8 r1 = this.A00.A03.A00;
        AnonymousClass1s9 r02 = r1.A0A;
        if (i != 1) {
            boolean AJy = r02.AJy();
            AnonymousClass1sA r11 = r1.A0F;
            if (AJy) {
                File file = r1.A0L;
                long currentTimeMillis = System.currentTimeMillis() - r11.A00;
                RecordingView recordingView = r11.A04;
                recordingView.A00.setText(C38131nZ.A04(r11.A06, (long) ((int) (currentTimeMillis / 1000))));
                if (file != null) {
                    long length = file.length();
                    boolean A0P = C15380n4.A0P(r11.A07);
                    C15450nH r13 = r11.A02;
                    long A02 = ((long) r13.A02(AbstractC15460nI.A28)) * 1000;
                    C16140oW r12 = AbstractC15460nI.A1p;
                    if (length > ((long) r13.A02(r12)) * 1048576 || (A0P && currentTimeMillis >= A02)) {
                        r11.A03.A00.A0O(true);
                    } else {
                        int A022 = (int) ((length * 100) / (((long) r13.A02(r12)) * 1048576));
                        if (A0P) {
                            A022 = Math.max(A022, (int) ((currentTimeMillis * 100) / A02));
                        }
                        recordingView.A01.setProgress(A022);
                    }
                }
                recordingView.setVisibility(0);
                r11.A01.sendEmptyMessageDelayed(0, 50);
                return;
            }
            RecordingView recordingView2 = r11.A04;
            recordingView2.setVisibility(8);
            recordingView2.A01.setProgress(0);
        } else if (r02.AJV() && !r1.A0A.AJy() && (r0 = r1.A08) != null && !r0.AJN()) {
            r1.A0B();
        }
    }
}
