package X;

import android.animation.Animator;
import android.widget.ImageButton;

/* renamed from: X.4eE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95714eE implements Animator.AnimatorListener {
    public final /* synthetic */ C15260mp A00;

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
    }

    public C95714eE(C15260mp r1) {
        this.A00 = r1;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        C15260mp r0 = this.A00;
        r0.A0H();
        ImageButton imageButton = r0.A0I;
        if (imageButton != null) {
            imageButton.setAlpha(1.0f);
        }
    }
}
