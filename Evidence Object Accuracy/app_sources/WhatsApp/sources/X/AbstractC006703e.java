package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import com.whatsapp.R;

/* renamed from: X.03e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC006703e {
    public C005602s A00;
    public CharSequence A01;
    public boolean A02 = false;

    public RemoteViews A03(AbstractC11240fy r2) {
        return null;
    }

    public RemoteViews A04(AbstractC11240fy r2) {
        return null;
    }

    public abstract String A05();

    public abstract void A08(AbstractC11240fy v);

    public final Bitmap A00(int i, int i2, int i3, int i4) {
        if (i4 == 0) {
            i4 = 0;
        }
        Context context = this.A00.A0B;
        Bitmap A01 = A01(IconCompat.A02(context.getResources(), context.getPackageName(), R.drawable.notification_icon_background), i4, i2);
        Canvas canvas = new Canvas(A01);
        Drawable mutate = this.A00.A0B.getResources().getDrawable(i).mutate();
        mutate.setFilterBitmap(true);
        int i5 = (i2 - i3) >> 1;
        int i6 = i3 + i5;
        mutate.setBounds(i5, i5, i6, i6);
        mutate.setColorFilter(new PorterDuffColorFilter(-1, PorterDuff.Mode.SRC_ATOP));
        mutate.draw(canvas);
        return A01;
    }

    public final Bitmap A01(IconCompat iconCompat, int i, int i2) {
        int i3;
        Drawable A06 = iconCompat.A06(this.A00.A0B);
        if (i2 == 0) {
            i3 = A06.getIntrinsicWidth();
            i2 = A06.getIntrinsicHeight();
        } else {
            i3 = i2;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i3, i2, Bitmap.Config.ARGB_8888);
        A06.setBounds(0, 0, i3, i2);
        if (i != 0) {
            A06.mutate().setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN));
        }
        A06.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a8, code lost:
        if (r13.A00.A0C == null) goto L_0x00aa;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.widget.RemoteViews A02() {
        /*
        // Method dump skipped, instructions count: 481
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC006703e.A02():android.widget.RemoteViews");
    }

    public void A06(Bundle bundle) {
        if (this.A02) {
            bundle.putCharSequence("android.summaryText", this.A01);
        }
        String A05 = A05();
        if (A05 != null) {
            bundle.putString("androidx.core.app.extra.COMPAT_TEMPLATE", A05);
        }
    }

    public void A07(RemoteViews remoteViews, RemoteViews remoteViews2) {
        remoteViews.setViewVisibility(R.id.title, 8);
        remoteViews.setViewVisibility(R.id.text2, 8);
        remoteViews.setViewVisibility(R.id.text, 8);
        remoteViews.removeAllViews(R.id.notification_main_column);
        remoteViews.addView(R.id.notification_main_column, remoteViews2.clone());
        remoteViews.setViewVisibility(R.id.notification_main_column, 0);
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = this.A00.A0B.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.notification_top_pad);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.notification_top_pad_large_text);
            float f = resources.getConfiguration().fontScale;
            if (f < 1.0f) {
                f = 1.0f;
            } else if (f > 1.3f) {
                f = 1.3f;
            }
            float f2 = (f - 1.0f) / 0.29999995f;
            remoteViews.setViewPadding(R.id.notification_main_column_container, 0, Math.round(((1.0f - f2) * ((float) dimensionPixelSize)) + (f2 * ((float) dimensionPixelSize2))), 0, 0);
        }
    }
}
