package X;

/* renamed from: X.31J  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31J extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public Long A09;
    public String A0A;
    public String A0B;

    public AnonymousClass31J() {
        super(2636, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(10, this.A00);
        r3.Abe(6, this.A01);
        r3.Abe(7, this.A02);
        r3.Abe(9, this.A0A);
        r3.Abe(2, this.A04);
        r3.Abe(1, this.A05);
        r3.Abe(5, this.A06);
        r3.Abe(4, this.A07);
        r3.Abe(8, this.A0B);
        r3.Abe(12, this.A08);
        r3.Abe(3, this.A03);
        r3.Abe(11, this.A09);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAckKickReceived {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "callStanzaType", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "mediaType", C12960it.A0Y(this.A01));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageType", C12960it.A0Y(this.A02));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaType", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numOfflineStanzasProcessing", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numStanzasProcessing", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numStanzasProcessingForType", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numStanzasWithSameId", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaType", this.A0B);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stanzaOfflineCount", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stanzaType", C12960it.A0Y(this.A03));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "timeToAckKickInMs", this.A09);
        return C12960it.A0d("}", A0k);
    }
}
