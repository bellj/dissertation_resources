package X;

/* renamed from: X.3F6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3F6 {
    public final Object A00;
    public final String A01;
    public final String A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass3F6) {
                AnonymousClass3F6 r5 = (AnonymousClass3F6) obj;
                if (!C16700pc.A0O(this.A01, r5.A01) || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A00, r5.A00)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = ((this.A01.hashCode() * 31) + this.A02.hashCode()) * 31;
        Object obj = this.A00;
        return hashCode + (obj == null ? 0 : obj.hashCode());
    }

    public AnonymousClass3F6(Object obj, String str, String str2) {
        C16700pc.A0G(str, str2);
        this.A01 = str;
        this.A02 = str2;
        this.A00 = obj;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("BreadCrumbItem(itemId=");
        A0k.append(this.A01);
        A0k.append(", itemName=");
        A0k.append(this.A02);
        A0k.append(", data=");
        return C12960it.A0a(this.A00, A0k);
    }
}
