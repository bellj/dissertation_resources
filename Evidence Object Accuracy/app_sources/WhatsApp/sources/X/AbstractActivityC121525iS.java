package X;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.MenuItem;
import com.facebook.redex.IDxCListenerShape10S0100000_3_I1;
import com.facebook.redex.IDxDListenerShape14S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.payments.CheckFirstTransaction;
import com.whatsapp.payments.ui.ConfirmPaymentFragment;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiDebitCardVerificationActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentTransactionDetailsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentsAccountSetupActivity;
import com.whatsapp.payments.ui.IndiaUpiPinPrimerFullSheetActivity;
import com.whatsapp.payments.ui.IndiaUpiPinSetUpCompletedActivity;
import com.whatsapp.payments.ui.IndiaUpiQuickBuyActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentDescriptionRow;
import com.whatsapp.payments.ui.widget.PaymentView;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.chromium.net.UrlRequest;

/* renamed from: X.5iS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121525iS extends AbstractActivityC121635j4 implements AnonymousClass6MS, AbstractC136456Mp, AbstractC136306Ma, AnonymousClass6LV, AnonymousClass6LX {
    public C238013b A00;
    public AnonymousClass130 A01;
    public AnonymousClass10S A02;
    public C15610nY A03;
    public AnonymousClass1J1 A04;
    public C21270x9 A05;
    public C15890o4 A06;
    public C20370ve A07;
    public C15370n3 A08;
    public AbstractC30791Yv A09;
    public C30821Yy A0A;
    public AbstractC28901Pl A0B;
    public UserJid A0C;
    public AnonymousClass1KC A0D;
    public CheckFirstTransaction A0E;
    public AnonymousClass68Z A0F;
    public C119835fB A0G = new C119835fB();
    public C248217a A0H;
    public C243515e A0I;
    public AnonymousClass18S A0J;
    public AnonymousClass61M A0K;
    public C18620sk A0L;
    public C120555gN A0M;
    public C120485gG A0N;
    public C22540zF A0O;
    public C22460z7 A0P;
    public C91664Sp A0Q;
    public AnonymousClass606 A0R;
    public C128895wq A0S;
    public C126885tb A0T;
    public C124235op A0U;
    public PaymentDescriptionRow A0V;
    public PaymentView A0W;
    public C129515xq A0X;
    public AnonymousClass60W A0Y;
    public C16630pM A0Z;
    public String A0a = "";
    public String A0b;
    public String A0c;
    public String A0d;
    public String A0e;
    public String A0f = null;
    public List A0g;
    public boolean A0h;
    public boolean A0i = false;
    public boolean A0j;
    public boolean A0k;
    public final AnonymousClass4UZ A0l = new C120105fd(this);
    public final C30931Zj A0m = C30931Zj.A00("IndiaUpiPaymentActivity", "payment", "IN");
    public final AtomicInteger A0n = new AtomicInteger();

    public static void A02(AbstractC28901Pl r4, AbstractActivityC121525iS r5) {
        String str;
        AbstractC28901Pl r0 = r5.A0B;
        if (r0 != r4) {
            if (AbstractActivityC119235dO.A1k(r0, r5)) {
                str = "add_credential_prompt";
            } else {
                str = "available_payment_methods_prompt";
            }
            r5.A3O(63, str);
        }
        r5.A0B = r4;
        PaymentView paymentView = r5.A0W;
        if (paymentView != null) {
            paymentView.setBankLogo(r4.A05());
            r5.A0W.setPaymentMethodText(C1311161i.A02(r5, ((AbstractActivityC121545iU) r5).A01, r5.A0B, ((AbstractActivityC121685jC) r5).A0P, true));
        }
    }

    public static void A03(AbstractActivityC121525iS r5) {
        if (!r5.A06.A08()) {
            ((AbstractActivityC121545iU) r5).A0B.AL9("request_phone_number_permission", 123);
            RequestPermissionActivity.A0B(r5);
            return;
        }
        int A01 = r5.A0Y.A01();
        if (A01 == 1) {
            r5.A2I(new AnonymousClass2GV() { // from class: X.66b
                @Override // X.AnonymousClass2GV
                public final void AO1() {
                    AbstractActivityC121525iS r1 = AbstractActivityC121525iS.this;
                    r1.startActivity(C14960mK.A00(r1));
                }
            }, R.string.payment_sending_failed, R.string.upi_unable_to_send_payment_without_phone_state_permission, R.string.change_number_title);
        } else if (A01 != 2) {
            C119755f3 r0 = (C119755f3) r5.A0B.A08;
            if (r0 == null || !"OD_UNSECURED".equals(r0.A0B) || r5.A0i) {
                ((AbstractActivityC121545iU) r5).A06.A02("pay-entry-ui");
                r5.A2C(R.string.register_wait_message);
                ((AbstractActivityC121545iU) r5).A0G = true;
                ((AbstractActivityC121545iU) r5).A09.A00();
                return;
            }
            r5.Ado(R.string.upi_unsecure_overdraft_account_non_merchant_payment_error);
        } else {
            new AlertDialog.Builder(r5).setTitle(R.string.payment_cant_process).setMessage(R.string.upi_unable_to_send_payment_with_incorrect_stored_sim_id).setPositiveButton(R.string.upi_add_payment_method, new IDxCListenerShape10S0100000_3_I1(r5, 41)).setNegativeButton(R.string.upi_cancel_payment, new IDxCListenerShape10S0100000_3_I1(r5, 24)).setCancelable(false).show();
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN
    public void A2A(int i) {
        if (i != R.string.payments_send_insufficient_funds && i != R.string.payments_amount_cannot_edit) {
            super.A2A(i);
        }
    }

    @Override // X.AbstractActivityC121685jC
    public void A2i(Bundle bundle) {
        ((AbstractActivityC121665jA) this).A08 = null;
        ((AbstractActivityC121665jA) this).A0M = null;
        super.A2i(bundle);
    }

    public final Dialog A3H(Bundle bundle) {
        AnonymousClass6BE r4 = ((AbstractActivityC121665jA) this).A0D;
        r4.A02.A07(r4.A03(0, 51, "payment_confirm_prompt", this.A0d, super.A0g, super.A0f, AbstractActivityC121685jC.A1p(this)));
        C004802e A0S = C12980iv.A0S(this);
        A0S.A07(R.string.order_details_pending_transaction_title);
        C117295Zj.A0q(A0S, this, 37, R.string.ok);
        A0S.A0B(false);
        if (bundle != null) {
            A0S.A0A(((AbstractActivityC121545iU) this).A03.A01(bundle, getString(R.string.order_details_pending_transaction_message)));
        }
        return A0S.create();
    }

    public C50952Rz A3I(C30821Yy r5, int i) {
        C50942Ry r2;
        if (i == 0 && (r2 = super.A0T.A00().A01) != null) {
            if (r5.A00.compareTo(r2.A09.A00.A02.A00) >= 0) {
                return r2.A08;
            }
        }
        return null;
    }

    public ConfirmPaymentFragment A3J(C30821Yy r16, PaymentBottomSheet paymentBottomSheet) {
        AnonymousClass1KS r10;
        C30921Zi r6;
        AbstractC15340mz r9;
        Integer num;
        C14580lf A01;
        PaymentView paymentView = this.A0W;
        if (paymentView != null) {
            r10 = paymentView.getStickerIfSelected();
        } else {
            r10 = null;
        }
        AnonymousClass2S0 r12 = null;
        if (paymentView != null) {
            r6 = paymentView.getPaymentBackground();
        } else {
            r6 = null;
        }
        if (r10 == null && r6 == null) {
            A01 = null;
        } else {
            C20350vc r5 = super.A0S;
            AbstractC14640lm r7 = ((AbstractActivityC121685jC) this).A0E;
            AnonymousClass009.A05(r7);
            UserJid userJid = ((AbstractActivityC121685jC) this).A0G;
            long j = ((AbstractActivityC121685jC) this).A02;
            if (j != 0) {
                r9 = ((AbstractActivityC121685jC) this).A09.A0K.A00(j);
            } else {
                r9 = null;
            }
            PaymentView paymentView2 = this.A0W;
            if (paymentView2 != null) {
                num = paymentView2.getStickerSendOrigin();
            } else {
                num = null;
            }
            A01 = r5.A01(r6, r7, userJid, r9, r10, num);
        }
        this.A0D = null;
        this.A0c = null;
        AbstractC30791Yv A02 = ((AbstractActivityC121545iU) this).A02.A02("INR");
        ConfirmPaymentFragment A00 = ConfirmPaymentFragment.A00(this.A0B, null, !this.A0i ? 1 : 0);
        C118025b9 r0 = super.A0X;
        if (!(r0 == null || r0.A00.A01() == null)) {
            r12 = (AnonymousClass2S0) ((AnonymousClass617) super.A0X.A00.A01()).A01;
        }
        A00.A0L = new AnonymousClass6CK(A02, r16, r12, this, paymentBottomSheet);
        A00.A0M = new AnonymousClass6CP(A01, r16, r12, A00, this);
        return A00;
    }

    public String A3K() {
        C15370n3 r1 = this.A08;
        if (r1 == null) {
            return (String) C117295Zj.A0R(((AbstractActivityC121665jA) this).A08);
        }
        return this.A03.A04(r1);
    }

    public final String A3L() {
        AnonymousClass1ZR r0;
        if (!AnonymousClass1ZS.A02(((AbstractActivityC121665jA) this).A06)) {
            r0 = ((AbstractActivityC121665jA) this).A06;
        } else if (this.A08 != null && !A3Y()) {
            return this.A03.A08(this.A08);
        } else {
            r0 = ((AbstractActivityC121665jA) this).A08;
        }
        return (String) C117295Zj.A0R(r0);
    }

    public final String A3M() {
        if (!TextUtils.isEmpty(((AbstractActivityC121665jA) this).A0F)) {
            this.A0m.A06(C12960it.A0d(((AbstractActivityC121665jA) this).A0F, C12960it.A0k("getSeqNum/incomingPayRequestId")));
            return ((AbstractActivityC121665jA) this).A0F;
        } else if (!TextUtils.isEmpty(super.A0n)) {
            this.A0m.A06(C12960it.A0d(super.A0n, C12960it.A0k("getSeqNum/transactionId")));
            return super.A0n;
        } else {
            String A0O = AbstractActivityC119235dO.A0O(this);
            this.A0m.A06(C12960it.A0d(C1309060l.A00(A0O), C12960it.A0k("getSeqNum/seqNum generated:")));
            return A0O;
        }
    }

    public void A3N() {
        UserJid of;
        C15370n3 A01;
        if (!(this instanceof IndiaUpiCheckOrderDetailsActivity)) {
            if (((AbstractActivityC121685jC) this).A0E == null) {
                ((AbstractActivityC121685jC) this).A0E = AbstractC14640lm.A01(getIntent().getStringExtra("extra_jid"));
                ((AbstractActivityC121685jC) this).A0G = UserJid.getNullable(getIntent().getStringExtra("extra_receiver_jid"));
            }
            AbstractC14640lm r1 = ((AbstractActivityC121685jC) this).A0E;
            if (C15380n4.A0J(r1)) {
                of = ((AbstractActivityC121685jC) this).A0G;
            } else {
                of = UserJid.of(r1);
            }
            this.A0C = of;
            if (A3Y()) {
                A01 = null;
            } else {
                A01 = ((AbstractActivityC121685jC) this).A08.A01(this.A0C);
            }
            this.A08 = A01;
            PaymentView paymentView = this.A0W;
            if (paymentView == null) {
                return;
            }
            if (A01 != null) {
                String A3K = A3K();
                boolean A3Z = A3Z();
                paymentView.A1F = A3K;
                paymentView.A0H.setText(A3K);
                paymentView.A07.setVisibility(C12960it.A02(A3Z ? 1 : 0));
                paymentView.A0Y.A06(paymentView.A0W, A01);
                return;
            }
            Object[] A1b = C12970iu.A1b();
            Object obj = ((AbstractActivityC121665jA) this).A08.A00;
            AnonymousClass009.A05(obj);
            String A0X = C12960it.A0X(this, obj, A1b, 0, R.string.payments_send_payment_upi_id);
            PaymentView paymentView2 = this.A0W;
            String str = (String) C117295Zj.A0R(((AbstractActivityC121665jA) this).A06);
            boolean A3Z2 = A3Z();
            if (!TextUtils.isEmpty(str)) {
                paymentView2.A1F = str;
                paymentView2.A0I.setText(A0X);
            } else {
                paymentView2.A1F = A0X;
            }
            paymentView2.A0H.setText(paymentView2.A02(paymentView2.A1F, R.string.payments_send_payment_to));
            paymentView2.A07.setVisibility(C12960it.A02(A3Z2 ? 1 : 0));
            paymentView2.A0X.A05(paymentView2.A0W, R.drawable.avatar_contact);
            return;
        }
        IndiaUpiCheckOrderDetailsActivity indiaUpiCheckOrderDetailsActivity = (IndiaUpiCheckOrderDetailsActivity) this;
        UserJid of2 = UserJid.of(indiaUpiCheckOrderDetailsActivity.A0D.A00);
        ((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A0C = of2;
        ((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A08 = (of2 == null || indiaUpiCheckOrderDetailsActivity.A3Y()) ? null : ((AbstractActivityC121685jC) indiaUpiCheckOrderDetailsActivity).A08.A01(((AbstractActivityC121525iS) indiaUpiCheckOrderDetailsActivity).A0C);
    }

    public void A3O(int i, String str) {
        AnonymousClass6BE r0 = ((AbstractActivityC121665jA) this).A0D;
        r0.A02.A07(r0.A03(C12960it.A0V(), Integer.valueOf(i), str, this.A0d, super.A0g, super.A0f, AbstractActivityC121685jC.A1p(this)));
    }

    public void A3P(Context context) {
        Intent A0D = C12990iw.A0D(context, IndiaUpiPaymentsAccountSetupActivity.class);
        A0D.putExtra("extra_setup_mode", 1);
        if (context instanceof IndiaUpiCheckOrderDetailsActivity) {
            A0D.putExtra("extra_payments_entry_type", 11);
            A0D.putExtra("extra_order_type", super.A0g);
            A0D.putExtra("extra_payment_config_id", super.A0f);
        } else {
            A0D.putExtra("extra_payments_entry_type", 6);
        }
        A0D.putExtra("extra_is_first_payment_method", !AbstractActivityC119235dO.A1l(this));
        A0D.putExtra("extra_skip_value_props_display", false);
        C35741ib.A00(A0D, "payViewAddPayment");
        startActivityForResult(A0D, 1008);
    }

    public /* synthetic */ void A3Q(AnonymousClass01E r2) {
        if ((this instanceof IndiaUpiQuickBuyActivity) && (r2 instanceof PaymentBottomSheet)) {
            ((PaymentBottomSheet) r2).A00 = null;
        }
    }

    public /* synthetic */ void A3R(AnonymousClass01E r3) {
        if ((this instanceof IndiaUpiQuickBuyActivity) && (r3 instanceof PaymentBottomSheet)) {
            ((PaymentBottomSheet) r3).A00 = new IDxDListenerShape14S0100000_3_I1(this, 21);
        }
    }

    public final void A3S(AnonymousClass1IR r5, boolean z) {
        String str;
        Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentTransactionDetailsActivity.class);
        C117305Zk.A11(A0D, r5, r5.A0C);
        A0D.putExtra("extra_transaction_ref", ((AbstractActivityC121665jA) this).A0L);
        if (this.A0j) {
            A0D.setFlags(33554432);
            A0D.putExtra("extra_return_after_completion", true);
            str = "external_app";
        } else {
            str = this.A0d;
        }
        A0D.putExtra("referral_screen", str);
        A0D.putExtra("extra_payment_flow_entry_point", ((AbstractActivityC121665jA) this).A01);
        if (z) {
            A0D.setFlags(67108864);
        }
        A0D.putExtra("extra_action_bar_display_close", true);
        A2G(A0D, true);
        AaN();
        A2q();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0128, code lost:
        if ("pay-precheck".equals("pay-precheck") == false) goto L_0x012a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A3T(X.C119705ey r18, X.C119705ey r19, X.C452120p r20, java.lang.String r21, java.lang.String r22, boolean r23) {
        /*
        // Method dump skipped, instructions count: 368
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121525iS.A3T(X.5ey, X.5ey, X.20p, java.lang.String, java.lang.String, boolean):void");
    }

    public final void A3U(C452120p r4, boolean z) {
        AaN();
        if (r4 == null) {
            A2q();
            ((ActivityC13830kP) this).A05.Ab2(new Runnable(z) { // from class: X.6ID
                public final /* synthetic */ boolean A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    boolean z2;
                    AnonymousClass1IR A02;
                    String obj;
                    AbstractActivityC121525iS r0 = AbstractActivityC121525iS.this;
                    boolean z3 = this.A01;
                    C15570nT r2 = ((ActivityC13790kL) r0).A01;
                    r2.A08();
                    C27621Ig r22 = r2.A01;
                    AnonymousClass009.A05(r22);
                    if (z3) {
                        AbstractC30791Yv r3 = r0.A09;
                        String str = ((AbstractC30781Yu) r3).A04;
                        z2 = true;
                        A02 = C31001Zq.A02(r3, r0.A0A, null, (UserJid) r22.A0D, str, null, "IN", 10, 11, AnonymousClass20N.A00("IN"), 1, 0, -1);
                    } else {
                        AbstractC30791Yv r6 = r0.A09;
                        String str2 = ((AbstractC30781Yu) r6).A04;
                        z2 = true;
                        A02 = C31001Zq.A02(r6, r0.A0A, (UserJid) r22.A0D, null, str2, null, "IN", 1, 401, AnonymousClass20N.A00("IN"), 1, 0, -1);
                    }
                    if (!TextUtils.isEmpty(r0.A0a)) {
                        r0.A0G.A0T(r0.A0a);
                    }
                    A02.A05 = C117315Zl.A02(r0);
                    A02.A0F = "UNSET";
                    C119835fB r62 = r0.A0G;
                    A02.A0A = r62;
                    A02.A0P = z2;
                    String str3 = (String) ((AbstractActivityC121665jA) r0).A08.A00;
                    if (z3) {
                        r62.A0L = str3;
                        r62.A08 = C117305Zk.A0I(C117305Zk.A0J(), String.class, ((AbstractActivityC121665jA) r0).A06.A00, "legalName");
                    } else {
                        r62.A0J = str3;
                        r62.A07 = C117305Zk.A0I(C117305Zk.A0J(), String.class, ((AbstractActivityC121665jA) r0).A06.A00, "legalName");
                    }
                    String str4 = r62.A0F;
                    AnonymousClass009.A04(str4);
                    AnonymousClass1IR A0N = r0.A07.A0N(str4, null);
                    C30931Zj r32 = r0.A0m;
                    if (A0N == null) {
                        obj = "IN- HANDLE_SEND_AGAIN Old txn is null";
                    } else {
                        StringBuilder A0k = C12960it.A0k("IN- HANDLE_SEND_AGAIN Old txn is not null, interop is ");
                        A0k.append(A0N.A0P);
                        obj = A0k.toString();
                    }
                    r32.A06(obj);
                    r0.A07.A0j(A02, A0N, str4);
                    r32.A06(C12960it.A0d(A02.A0K, C12960it.A0k("getPayNonWaVpaCallback added new transaction with trans id: ")));
                    ((ActivityC13810kN) r0).A05.A0H(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x009e: INVOKE  
                          (wrap: X.0mE : 0x0097: IGET  (r2v15 X.0mE A[REMOVE]) = (wrap: ?? : ?: CAST (X.0kN) (r0v0 'r0' X.5iS)) X.0kN.A05 X.0mE)
                          (wrap: X.6IC : 0x009b: CONSTRUCTOR  (r1v14 X.6IC A[REMOVE]) = (r5v1 'A02' X.1IR), (r0v0 'r0' X.5iS) call: X.6IC.<init>(X.1IR, X.5iS):void type: CONSTRUCTOR)
                         type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6ID.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x009b: CONSTRUCTOR  (r1v14 X.6IC A[REMOVE]) = (r5v1 'A02' X.1IR), (r0v0 'r0' X.5iS) call: X.6IC.<init>(X.1IR, X.5iS):void type: CONSTRUCTOR in method: X.6ID.run():void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 15 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IC, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 21 more
                        */
                    /*
                    // Method dump skipped, instructions count: 235
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass6ID.run():void");
                }
            });
        } else if (!AnonymousClass69E.A02(this, "upi-send-to-vpa", r4.A00, false)) {
            A39();
        }
    }

    public void A3V(AnonymousClass3FW r11, String str, int i) {
        ((AbstractActivityC121665jA) this).A0D.AKj(r11, C12960it.A0V(), Integer.valueOf(i), str, this.A0d, super.A0g, super.A0f, false, AbstractActivityC121685jC.A1p(this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003f, code lost:
        if (android.text.TextUtils.isEmpty(((X.AbstractActivityC121665jA) r29).A0F) != false) goto L_0x0041;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A3W(X.C50952Rz r30) {
        /*
            r29 = this;
            r0 = r29
            boolean r1 = r0 instanceof com.whatsapp.payments.ui.IndiaUpiSendPaymentActivity
            r9 = r30
            if (r1 != 0) goto L_0x0031
            com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity r0 = (com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity) r0
            X.5wq r3 = r0.A0S
            X.1Pl r5 = r0.A0B
            com.whatsapp.jid.UserJid r6 = r0.A0C
            X.1Yy r4 = r0.A0A
            java.lang.String r10 = r0.A0o
            X.5fB r8 = r0.A0G
            java.lang.String r12 = r0.A0H
            java.lang.String r13 = r0.A0G
            long r1 = r0.A00
            java.lang.String r14 = r0.A0g
            java.lang.String r15 = r0.A0I
            X.1ZR r7 = r0.A06
            r11 = 0
            r19 = 1
            r21 = 0
            r20 = 1
            r16 = r11
            r17 = r1
            r3.A00(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r19, r20, r21)
            return
        L_0x0031:
            boolean r1 = r0.A3Y()
            if (r1 == 0) goto L_0x0041
            java.lang.String r1 = r0.A0F
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            r28 = 1
            if (r1 == 0) goto L_0x0043
        L_0x0041:
            r28 = 0
        L_0x0043:
            java.lang.String r2 = r0.A0a
            X.4Sp r1 = r0.A0Q
            java.lang.String r8 = X.AnonymousClass4ES.A00(r1, r2)
            r0.A0b = r8
            X.5wq r10 = r0.A0S
            X.1Pl r7 = r0.A0B
            com.whatsapp.jid.UserJid r6 = r0.A0C
            X.1Yy r11 = r0.A0A
            java.lang.String r5 = r0.A0o
            java.lang.String r4 = r0.A0p
            boolean r3 = r0.A0r
            boolean r2 = r0.A0t
            X.5fB r1 = r0.A0G
            r19 = 0
            r24 = 0
            X.1ZR r0 = r0.A06
            r21 = r19
            r22 = r19
            r20 = r19
            r26 = r3
            r27 = r2
            r12 = r7
            r13 = r6
            r14 = r0
            r15 = r1
            r16 = r9
            r17 = r5
            r18 = r4
            r23 = r8
            r10.A00(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r26, r27, r28)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121525iS.A3W(X.2Rz):void");
    }

    public void A3X(AnonymousClass60V r6, Object... objArr) {
        String str;
        AaN();
        if (!(this instanceof IndiaUpiCheckOrderDetailsActivity)) {
            str = "new_payment";
        } else {
            str = "order_details";
        }
        AnonymousClass61I.A02(AnonymousClass61I.A00(((ActivityC13790kL) this).A05, null, super.A0U, null, true), ((AbstractActivityC121665jA) this).A0D, str);
        ((AbstractActivityC121665jA) this).A0D.AKg(C12980iv.A0i(), 51, "error", this.A0d);
        ((AbstractActivityC121545iU) this).A0G = false;
        int i = r6.A00;
        if (i == 0) {
            i = R.string.payments_transfer_not_init;
            r6.A00 = R.string.payments_transfer_not_init;
        } else if (i == R.string.payments_receiver_not_in_region || i == R.string.payments_receiver_disabled_in_country || i == R.string.payments_receiver_app_version_unsupported || i == R.string.payments_receiver_generic_error || i == R.string.payments_receiver_not_in_group) {
            Object[] A1b = C12970iu.A1b();
            A1b[0] = A3K();
            Adr(A1b, 0, i);
            return;
        }
        Adr(objArr, 0, i);
    }

    public boolean A3Y() {
        return ((AbstractActivityC121685jC) this).A0G == null && ((AbstractActivityC121685jC) this).A0E == null && !AnonymousClass1ZS.A02(((AbstractActivityC121665jA) this).A08);
    }

    public boolean A3Z() {
        PaymentView paymentView;
        if (!AbstractActivityC119235dO.A1l(this) || (paymentView = this.A0W) == null || paymentView.A00 == 1) {
            return false;
        }
        return true;
    }

    public boolean A3a(C119705ey r13) {
        if (!r13.A04 || r13.A05) {
            return false;
        }
        AaN();
        if (!r13.A06) {
            C36021jC.A01(this, 15);
            return true;
        } else if (!AbstractActivityC119235dO.A1l(this)) {
            Intent A0D = C12990iw.A0D(this, IndiaUpiPaymentsAccountSetupActivity.class);
            A0D.putExtra("extra_setup_mode", 1);
            Jid jid = ((AbstractActivityC121685jC) this).A0E;
            if (jid == null && (jid = ((AnonymousClass1ZO) r13).A05) == null) {
                Log.e("showNodalDisallowAlert, jid and contactData.jid is null");
            } else {
                A0D.putExtra("extra_jid", jid.getRawString());
            }
            int i = 3;
            if ("payment_composer_icon".equals(this.A0d)) {
                i = 10;
            }
            A0D.putExtra("extra_payments_entry_type", i);
            A0D.putExtra("extra_is_first_payment_method", true);
            A0D.putExtra("extra_skip_value_props_display", false);
            A0D.putExtra("extra_receiver_jid", C15380n4.A03(this.A0C));
            C35741ib.A00(A0D, "composer");
            A2G(A0D, true);
            return true;
        } else {
            AnonymousClass3FP r3 = new AnonymousClass3FP(this, this, ((ActivityC13810kN) this).A05, ((AbstractActivityC121685jC) this).A0P, C117305Zk.A0Z(this), null, new Runnable() { // from class: X.6G3
                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121525iS r1 = AbstractActivityC121525iS.this;
                    if (!C15380n4.A0J(((AbstractActivityC121685jC) r1).A0E)) {
                        r1.A2q();
                        r1.finish();
                        return;
                    }
                    ((AbstractActivityC121685jC) r1).A0G = null;
                }
            }, true);
            if (TextUtils.isEmpty(this.A0d)) {
                this.A0d = "chat";
            }
            r3.A01(this.A0C, null, this.A0d);
            return true;
        }
    }

    @Override // X.AbstractC136306Ma
    public void AOG() {
        A2M("IndiaUpiPinPrimerDialogFragment");
    }

    @Override // X.AbstractC136306Ma
    public void AOd() {
        A3Q(A0V().A0A("IndiaUpiPinPrimerDialogFragment"));
        A2M("IndiaUpiPinPrimerDialogFragment");
        Intent A0D = C12990iw.A0D(this, IndiaUpiDebitCardVerificationActivity.class);
        C117315Zl.A0M(A0D, this.A0B);
        A2v(A0D);
        startActivityForResult(A0D, 1016);
    }

    @Override // X.AbstractC136456Mp
    public void AOf() {
        A3Q(A0V().A0A("IndiaUpiForgotPinDialogFragment"));
        A2M("IndiaUpiForgotPinDialogFragment");
        C18600si r2 = ((AbstractActivityC121665jA) this).A0C;
        StringBuilder A0h = C12960it.A0h();
        A0h.append(r2.A05());
        A0h.append(";");
        r2.A0H(C12960it.A0d(this.A0B.A0A, A0h));
        this.A0h = true;
        A03(this);
    }

    @Override // X.AbstractC136456Mp
    public void AQj() {
        A3Q(A0V().A0A("IndiaUpiForgotPinDialogFragment"));
        A2M("IndiaUpiForgotPinDialogFragment");
        Intent A02 = IndiaUpiPinPrimerFullSheetActivity.A02(this, (C30861Zc) this.A0B, true);
        A2v(A02);
        startActivityForResult(A02, 1017);
    }

    @Override // X.AbstractC136456Mp
    public void AQk() {
        A2M("IndiaUpiForgotPinDialogFragment");
    }

    @Override // X.AnonymousClass6MS
    public void ARr(C452120p r14, String str) {
        ((AbstractActivityC121665jA) this).A0D.A04(this.A0B, r14, 1);
        Integer num = null;
        String str2 = null;
        if (!TextUtils.isEmpty(str)) {
            C30931Zj r3 = this.A0m;
            StringBuilder A0k = C12960it.A0k("starting sendPaymentToVpa for jid: ");
            A0k.append(((AbstractActivityC121685jC) this).A0E);
            A0k.append(" vpa: ");
            A0k.append(((AbstractActivityC121665jA) this).A08);
            C117295Zj.A1F(r3, A0k);
            AnonymousClass1ZY r2 = this.A0B.A08;
            AnonymousClass009.A06(r2, r3.A02("onListKeys: Cannot get IndiaUpiMethodData"));
            C119755f3 r22 = (C119755f3) r2;
            this.A0G.A0N = A3M();
            C119835fB r1 = this.A0G;
            r1.A0E = ((AbstractActivityC121545iU) this).A0D;
            r1.A0L = C1329668y.A00(((AbstractActivityC121665jA) this).A0B);
            this.A0G.A0M = ((AbstractActivityC121665jA) this).A0B.A0B();
            AnonymousClass1ZR r0 = ((AbstractActivityC121665jA) this).A08;
            if (r0 == null) {
                r3.A06(C12960it.A0d(((AbstractActivityC121665jA) this).A0M, C12960it.A0k("vpa is null, while fetching list-keys, vpaId: ")));
            } else {
                this.A0G.A0J = (String) AnonymousClass1ZS.A01(r0);
            }
            C119835fB r32 = this.A0G;
            r32.A0H = ((AbstractActivityC121665jA) this).A0H;
            r32.A0I = ((AbstractActivityC121665jA) this).A0I;
            r32.A0K = ((AbstractActivityC121665jA) this).A0M;
            r32.A05 = C117315Zl.A02(this);
            this.A0G.A09 = r22.A06;
            ((AbstractActivityC121545iU) this).A06.A03("upi-get-credential");
            AbstractC28901Pl r02 = this.A0B;
            String str3 = r02.A0B;
            AnonymousClass1ZR r6 = r22.A08;
            C119835fB r7 = this.A0G;
            C30821Yy r5 = this.A0A;
            String str4 = (String) C117295Zj.A0R(r02.A09);
            String A3L = A3L();
            C15370n3 r03 = this.A08;
            if (r03 != null) {
                str2 = C248917h.A01(r03);
            }
            A3D(r5, r6, r7, str, str3, str4, A3L, str2);
        } else if (r14 != null && !AnonymousClass69E.A02(this, "upi-list-keys", r14.A00, false)) {
            if (((AbstractActivityC121545iU) this).A06.A07("upi-list-keys")) {
                AbstractActivityC119235dO.A1i(this);
                return;
            }
            C30931Zj r23 = this.A0m;
            StringBuilder A0k2 = C12960it.A0k("onListKeys: ");
            if (str != null) {
                num = Integer.valueOf(str.length());
            }
            A0k2.append(num);
            r23.A06(C12960it.A0d(" failed; ; showErrorAndFinish", A0k2));
            A39();
        }
    }

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r3) {
        throw C12980iv.A0u(this.A0m.A02("onSetPin unsupported"));
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 155) {
            if (i == 1000) {
                HashMap hashMap = ((AbstractActivityC121665jA) this).A0A.A07;
                if (i2 != -1 || hashMap == null) {
                    this.A0m.A0A("REQUEST_TOS_UPDATED but found null credentialBlobs", null);
                } else {
                    AaN();
                    A2C(R.string.register_wait_message);
                    A3W(A3I(this.A0A, ((AbstractActivityC121685jC) this).A01));
                    return;
                }
            } else if (i != 1001) {
                switch (i) {
                    case 1016:
                        if (i2 == -1 && intent != null) {
                            AbstractC28901Pl r0 = (AbstractC28901Pl) intent.getParcelableExtra("extra_bank_account");
                            if (r0 != null) {
                                this.A0B = r0;
                            }
                            C18600si r2 = ((AbstractActivityC121665jA) this).A0C;
                            StringBuilder A0h = C12960it.A0h();
                            A0h.append(r2.A05());
                            A0h.append(";");
                            r2.A0H(C12960it.A0d(this.A0B.A0A, A0h));
                            Parcelable parcelable = this.A0B;
                            Intent A0D = C12990iw.A0D(this, IndiaUpiPinSetUpCompletedActivity.class);
                            A0D.putExtra("extra_bank_account", parcelable);
                            A0D.putExtra("on_settings_page", false);
                            startActivity(A0D);
                            return;
                        }
                        return;
                    case 1017:
                        if (i2 == -1) {
                            C18600si r22 = ((AbstractActivityC121665jA) this).A0C;
                            StringBuilder A0h2 = C12960it.A0h();
                            A0h2.append(r22.A05());
                            A0h2.append(";");
                            r22.A0H(C12960it.A0d(this.A0B.A0A, A0h2));
                            AbstractC28901Pl r3 = this.A0B;
                            Intent A0D2 = C12990iw.A0D(this, IndiaUpiPinSetUpCompletedActivity.class);
                            C117315Zl.A0M(A0D2, r3);
                            A0D2.putExtra("on_settings_page", false);
                            startActivityForResult(A0D2, 1018);
                            return;
                        }
                        return;
                    case 1018:
                        PaymentBottomSheet paymentBottomSheet = new PaymentBottomSheet();
                        paymentBottomSheet.A01 = A3J(this.A0A, paymentBottomSheet);
                        Adl(paymentBottomSheet, "30");
                        return;
                    default:
                        super.onActivityResult(i, i2, intent);
                        return;
                }
            } else if (i2 == -1) {
                ((AbstractActivityC121685jC) this).A0G = UserJid.getNullable(intent.getStringExtra("extra_receiver_jid"));
                return;
            } else if (!(i2 == 0 && ((AbstractActivityC121685jC) this).A0G == null)) {
                return;
            }
            A2q();
            finish();
        } else if (i2 == -1) {
            A03(this);
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        String str;
        PaymentView paymentView = this.A0W;
        if (paymentView != null && paymentView.A0I()) {
            return;
        }
        if (!C15380n4.A0J(((AbstractActivityC121685jC) this).A0E) || ((AbstractActivityC121685jC) this).A00 != 0) {
            A2q();
            finish();
            if (!(this instanceof IndiaUpiCheckOrderDetailsActivity)) {
                str = "new_payment";
            } else {
                str = "order_details";
            }
            A3V(AnonymousClass61I.A00(((ActivityC13790kL) this).A05, null, super.A0U, null, true), str, 1);
            return;
        }
        ((AbstractActivityC121685jC) this).A0G = null;
        A2i(null);
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C117295Zj.A0e(this);
        this.A0H.A03(this.A0l);
        this.A04 = this.A05.A04(this, "india-upi-payment-activity");
        this.A0j = getIntent().getBooleanExtra("return-after-pay", false);
        this.A09 = ((AbstractActivityC121545iU) this).A02.A02("INR");
        C14850m9 r13 = ((ActivityC13810kN) this).A0C;
        C14900mE r9 = ((ActivityC13810kN) this).A05;
        C17220qS r2 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r6 = ((AbstractActivityC121545iU) this).A0C;
        C1308460e r14 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r1 = ((AbstractActivityC121685jC) this).A0M;
        C18650sn r5 = ((AbstractActivityC121685jC) this).A0K;
        this.A0M = new C120555gN(this, r9, r13, r2, r14, r5, r1, r6);
        C14830m7 r11 = ((ActivityC13790kL) this).A05;
        C15570nT r10 = ((ActivityC13790kL) this).A01;
        AbstractC14440lR r0 = ((ActivityC13830kP) this).A05;
        C17070qD r4 = ((AbstractActivityC121685jC) this).A0P;
        this.A0S = new C128895wq(new C120465gE(this, r9, r10, r11, ((AbstractActivityC121545iU) this).A02, r13, r14, ((AbstractActivityC121665jA) this).A0B, r5, r1, r4, super.A0T, ((AbstractActivityC121545iU) this).A0B, r6, r0), new C125875rx(this), new Runnable() { // from class: X.6G5
            @Override // java.lang.Runnable
            public final void run() {
                AbstractActivityC121525iS r3 = AbstractActivityC121525iS.this;
                r3.A0E.A00.A00(new AnonymousClass6EW(r3, false));
            }
        });
        C15610nY r102 = this.A03;
        AnonymousClass018 r112 = ((AbstractActivityC121545iU) this).A01;
        C30931Zj r7 = this.A0m;
        C22710zW r62 = ((AbstractActivityC121685jC) this).A0O;
        C17900ra r52 = ((AbstractActivityC121685jC) this).A0N;
        C129925yW r142 = ((AbstractActivityC121545iU) this).A03;
        C20370ve r132 = this.A07;
        this.A0R = new AnonymousClass606(r102, r112, ((AbstractActivityC121685jC) this).A08, r132, r142, r1, r52, r62, r7, this, new C125885ry(this), r0, new C002601e(null, new AnonymousClass01N() { // from class: X.6Kr
            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                AbstractActivityC121525iS r12 = AbstractActivityC121525iS.this;
                C14850m9 r53 = ((ActivityC13810kN) r12).A0C;
                C14900mE r22 = ((ActivityC13810kN) r12).A05;
                C15570nT r3 = ((ActivityC13790kL) r12).A01;
                C17220qS r63 = ((AbstractActivityC121685jC) r12).A0H;
                C17070qD r122 = ((AbstractActivityC121685jC) r12).A0P;
                C21860y6 r8 = ((AbstractActivityC121685jC) r12).A0I;
                C18610sj r113 = ((AbstractActivityC121685jC) r12).A0M;
                AnonymousClass102 r42 = ((AbstractActivityC121545iU) r12).A02;
                AnonymousClass6BE r133 = ((AbstractActivityC121665jA) r12).A0D;
                return new C120495gH(r12, r22, r3, r42, r53, r63, ((AbstractActivityC121665jA) r12).A0B, r8, ((AbstractActivityC121685jC) r12).A0K, null, r113, r122, r133, ((AbstractActivityC121545iU) r12).A0B);
            }
        }));
        this.A0d = getIntent().getStringExtra("referral_screen");
        AbstractC14440lR r42 = ((ActivityC13830kP) this).A05;
        C17070qD r3 = ((AbstractActivityC121685jC) this).A0P;
        CheckFirstTransaction checkFirstTransaction = new CheckFirstTransaction(((AbstractActivityC121685jC) this).A0I, ((AbstractActivityC121665jA) this).A0C, r3, r42);
        this.A0E = checkFirstTransaction;
        ((ActivityC001000l) this).A06.A00(checkFirstTransaction);
    }

    @Override // X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        C004802e r3;
        if (i == 15) {
            r3 = C12980iv.A0S(this);
            r3.A0A(C12960it.A0X(this, this.A03.A08(this.A08), new Object[1], 0, R.string.payments_nodal_not_allowed));
            C117295Zj.A0q(r3, this, 40, R.string.ok);
            r3.A0B(false);
            r3.A08(new DialogInterface.OnCancelListener() { // from class: X.61z
                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    C36021jC.A00(AbstractActivityC121525iS.this, 15);
                }
            });
        } else if (i == 22) {
            r3 = C12980iv.A0S(this);
            r3.A0A(C12960it.A0X(this, getString(R.string.india_upi_payment_id_name), new Object[1], 0, R.string.unblock_payment_id_error_default));
            C117295Zj.A0q(r3, this, 33, R.string.ok);
            r3.A0B(false);
        } else if (i == 26) {
            BigDecimal bigDecimal = new BigDecimal(((ActivityC13810kN) this).A06.A02(AbstractC15460nI.A1x));
            r3 = C12980iv.A0S(this);
            r3.A0A(C12960it.A0X(this, C30771Yt.A05.AAB(((AbstractActivityC121545iU) this).A01, bigDecimal, 0), new Object[1], 0, R.string.upi_twenty_four_hour_send_limit_error));
            C117295Zj.A0q(r3, this, 32, R.string.ok);
            r3.A0B(false);
        } else if (i == 33) {
            return A3H(null);
        } else {
            if (i != 34) {
                switch (i) {
                    case 10:
                        r3 = C12980iv.A0S(this);
                        r3.A06(R.string.payments_check_pin_invalid_pin_retry);
                        r3.A00(R.string.forgot_upi_pin, new IDxCListenerShape10S0100000_3_I1(this, 27));
                        C117305Zk.A18(r3, this, 29, R.string.cancel);
                        C117295Zj.A0q(r3, this, 36, R.string.payments_try_again);
                        r3.A0B(true);
                        r3.A08(new DialogInterface.OnCancelListener() { // from class: X.620
                            @Override // android.content.DialogInterface.OnCancelListener
                            public final void onCancel(DialogInterface dialogInterface) {
                                C36021jC.A00(AbstractActivityC121525iS.this, 10);
                            }
                        });
                        break;
                    case 11:
                        r3 = C12980iv.A0S(this);
                        r3.A06(R.string.payments_pin_max_retries);
                        C117295Zj.A0q(r3, this, 38, R.string.forgot_upi_pin);
                        C117305Zk.A18(r3, this, 28, R.string.cancel);
                        r3.A0B(true);
                        r3.A08(new DialogInterface.OnCancelListener() { // from class: X.621
                            @Override // android.content.DialogInterface.OnCancelListener
                            public final void onCancel(DialogInterface dialogInterface) {
                                C36021jC.A00(AbstractActivityC121525iS.this, 11);
                            }
                        });
                        break;
                    case 12:
                        r3 = C12980iv.A0S(this);
                        r3.A06(R.string.payments_pin_no_pin_set);
                        C117295Zj.A0q(r3, this, 31, R.string.yes);
                        C117305Zk.A18(r3, this, 34, R.string.no);
                        r3.A0B(true);
                        r3.A08(new DialogInterface.OnCancelListener() { // from class: X.622
                            @Override // android.content.DialogInterface.OnCancelListener
                            public final void onCancel(DialogInterface dialogInterface) {
                                C36021jC.A00(AbstractActivityC121525iS.this, 12);
                            }
                        });
                        break;
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                        ((AbstractActivityC121665jA) this).A0B.A0D();
                        r3 = C12980iv.A0S(this);
                        r3.A06(R.string.payments_pin_encryption_error);
                        C117295Zj.A0q(r3, this, 25, R.string.yes);
                        C117305Zk.A18(r3, this, 26, R.string.no);
                        r3.A0B(true);
                        r3.A08(new DialogInterface.OnCancelListener() { // from class: X.623
                            @Override // android.content.DialogInterface.OnCancelListener
                            public final void onCancel(DialogInterface dialogInterface) {
                                C36021jC.A00(AbstractActivityC121525iS.this, 13);
                            }
                        });
                        break;
                    default:
                        return super.onCreateDialog(i);
                }
            } else {
                r3 = C12980iv.A0S(this);
                r3.A06(R.string.payments_change_of_receiver_not_allowed);
                C117295Zj.A0q(r3, this, 30, R.string.ok);
                r3.A0B(true);
            }
        }
        return r3.create();
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i, Bundle bundle) {
        if (i == 33) {
            return A3H(bundle);
        }
        return super.onCreateDialog(i, bundle);
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C124235op r1 = this.A0U;
        if (r1 != null) {
            r1.A03(true);
        }
        this.A04.A00();
        this.A0H.A04(this.A0l);
        C30931Zj r2 = this.A0m;
        StringBuilder A0k = C12960it.A0k("onDestroy states: ");
        A0k.append(((AbstractActivityC121545iU) this).A06);
        C117295Zj.A1F(r2, A0k);
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN, android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        String str;
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        this.A0m.A06("action bar home");
        if (!C15380n4.A0J(((AbstractActivityC121685jC) this).A0E) || ((AbstractActivityC121685jC) this).A00 != 0) {
            A2q();
            finish();
            if (!(this instanceof IndiaUpiCheckOrderDetailsActivity)) {
                str = "new_payment";
            } else {
                str = "order_details";
            }
            A3O(1, str);
            return true;
        }
        ((AbstractActivityC121685jC) this).A0G = null;
        A2i(null);
        return true;
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.A0B = (AbstractC28901Pl) bundle.getParcelable("paymentMethodSavedInst");
        ((AbstractActivityC121685jC) this).A0E = UserJid.getNullable(bundle.getString("extra_jid"));
        ((AbstractActivityC121685jC) this).A0G = UserJid.getNullable(bundle.getString("extra_receiver_jid"));
        ((AbstractActivityC121545iU) this).A0G = bundle.getBoolean("sending_payment");
        ((AbstractActivityC121665jA) this).A0F = bundle.getString("extra_incoming_pay_request_id");
        ((AbstractActivityC121685jC) this).A01 = bundle.getInt("extra_offer_eligibility_state");
        if (this.A0B != null) {
            this.A0B.A08 = (AnonymousClass1ZY) bundle.getParcelable("countryDataSavedInst");
        }
        C119835fB r0 = (C119835fB) bundle.getParcelable("countryTransDataSavedInst");
        if (r0 != null) {
            this.A0G = r0;
        }
        String string = bundle.getString("sendAmountSavedInst");
        if (string != null) {
            this.A0A = C117305Zk.A0E(this.A09, string);
        }
        ((AbstractActivityC121685jC) this).A02 = bundle.getLong("quotedMessageRowIdSavedInst");
        super.A0h = bundle.getString("paymentNoteSavedInst");
        this.A0q = C15380n4.A07(UserJid.class, bundle.getStringArrayList("paymentNoteMentionsSavedInst"));
        ((AbstractActivityC121665jA) this).A08 = (AnonymousClass1ZR) bundle.getParcelable("receiverVpaSavedInst");
        ((AbstractActivityC121665jA) this).A0M = bundle.getString("receiverVpaIdSavedInst");
        this.A0c = bundle.getString("paymentStickerMediaJobIdSavedInst");
        PaymentView paymentView = this.A0W;
        if (paymentView != null) {
            paymentView.A0A(bundle);
        } else {
            this.A0e = bundle.getString("restoredPaymentAmount");
        }
    }

    @Override // X.AbstractActivityC121665jA, X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        C30931Zj r2 = this.A0m;
        StringBuilder A0k = C12960it.A0k("onResume states: ");
        A0k.append(((AbstractActivityC121545iU) this).A06);
        C117295Zj.A1F(r2, A0k);
        isFinishing();
    }

    @Override // X.AbstractActivityC121545iU, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        Parcelable parcelable;
        super.onSaveInstanceState(bundle);
        bundle.putString("extra_jid", C15380n4.A03(((AbstractActivityC121685jC) this).A0E));
        bundle.putString("extra_receiver_jid", C15380n4.A03(((AbstractActivityC121685jC) this).A0G));
        bundle.putBoolean("sending_payment", ((AbstractActivityC121545iU) this).A0G);
        bundle.putString("extra_incoming_pay_request_id", ((AbstractActivityC121665jA) this).A0F);
        bundle.putString("extra_request_message_key", super.A0l);
        bundle.putInt("extra_offer_eligibility_state", ((AbstractActivityC121685jC) this).A01);
        Parcelable parcelable2 = this.A0B;
        if (parcelable2 != null) {
            bundle.putParcelable("paymentMethodSavedInst", parcelable2);
        }
        AbstractC28901Pl r0 = this.A0B;
        if (!(r0 == null || (parcelable = r0.A08) == null)) {
            bundle.putParcelable("countryDataSavedInst", parcelable);
        }
        Parcelable parcelable3 = this.A0G;
        if (parcelable3 != null) {
            bundle.putParcelable("countryTransDataSavedInst", parcelable3);
        }
        C30821Yy r02 = this.A0A;
        if (r02 != null) {
            bundle.putString("sendAmountSavedInst", r02.A00.toString());
        }
        long j = ((AbstractActivityC121685jC) this).A02;
        if (j != 0) {
            bundle.putLong("quotedMessageRowIdSavedInst", j);
        }
        AnonymousClass1ZR r1 = ((AbstractActivityC121665jA) this).A08;
        if (!AnonymousClass1ZS.A03(r1)) {
            bundle.putParcelable("receiverVpaSavedInst", r1);
        }
        String str = ((AbstractActivityC121665jA) this).A0M;
        if (str != null) {
            bundle.putString("receiverVpaIdSavedInst", str);
        }
        String str2 = this.A0c;
        if (str2 != null) {
            bundle.putString("paymentStickerMediaJobIdSavedInst", str2);
        }
        PaymentView paymentView = this.A0W;
        if (paymentView != null) {
            paymentView.A0B(bundle);
            bundle.putString("paymentNoteSavedInst", this.A0W.getPaymentNote());
            bundle.putStringArrayList("paymentNoteMentionsSavedInst", C15380n4.A06(this.A0W.getMentionedJids()));
            bundle.putString("restoredPaymentAmount", this.A0W.getPaymentAmountString());
        }
    }
}
