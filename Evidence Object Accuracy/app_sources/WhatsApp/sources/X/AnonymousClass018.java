package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.018  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass018 {
    public static final boolean A0C;
    public Context A00;
    public C28141Kv A01;
    public DateFormat A02;
    public DateFormat A03;
    public Locale A04;
    public Locale A05;
    public boolean A06;
    public final C16590pI A07;
    public final C14820m6 A08;
    public final C17640r9 A09;
    public final Object A0A = new Object();
    public final Set A0B = new HashSet();

    static {
        boolean z = false;
        if (Build.VERSION.SDK_INT < 26) {
            z = true;
        }
        A0C = z;
    }

    public AnonymousClass018(C16590pI r2, C14820m6 r3, C17640r9 r4) {
        this.A07 = r2;
        this.A08 = r3;
        this.A09 = r4;
        Context A01 = A01();
        this.A00 = A01;
        Locale A00 = A00(A01);
        this.A05 = A00;
        this.A04 = A00;
        AnonymousClass1MY.A08();
    }

    public static Locale A00(Context context) {
        Locale locale;
        Configuration configuration = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= 24) {
            locale = configuration.getLocales().get(0);
        } else {
            locale = configuration.locale;
        }
        if (locale != null) {
            return locale;
        }
        Locale locale2 = Locale.getDefault();
        if (locale2 == null) {
            return Locale.US;
        }
        return locale2;
    }

    public Context A01() {
        Context baseContext;
        Context A01 = this.A07.A01();
        while ((A01 instanceof ContextWrapper) && (baseContext = ((ContextWrapper) A01).getBaseContext()) != null) {
            A01 = baseContext;
        }
        return A01;
    }

    public Context A02(Context context) {
        if (A0C || context == null || context.getResources().getConfiguration().locale.equals(this.A04)) {
            return context;
        }
        Configuration configuration = new Configuration();
        configuration.setLocale(this.A04);
        return context.createConfigurationContext(configuration);
    }

    public AnonymousClass02S A03() {
        return A04().A01;
    }

    public final C28141Kv A04() {
        C28141Kv r0;
        synchronized (this.A0A) {
            if (this.A01 == null) {
                A0O();
            }
            r0 = this.A01;
        }
        return r0;
    }

    public String A05() {
        String country = A00(this.A00).getCountry();
        if (AbstractC27291Gt.A0B(country)) {
            return country;
        }
        StringBuilder sb = new StringBuilder("verifynumber/requestcode/invalid-country '");
        sb.append(country);
        sb.append("'");
        Log.i(sb.toString());
        return "ZZ";
    }

    public String A06() {
        String language = A00(this.A00).getLanguage();
        if (AbstractC27291Gt.A0A(language)) {
            return language;
        }
        StringBuilder sb = new StringBuilder("verifynumber/requestcode/invalid-language '");
        sb.append(language);
        sb.append("'");
        Log.i(sb.toString());
        return "zz";
    }

    public String A07() {
        StringBuilder sb = new StringBuilder();
        sb.append(A06());
        sb.append("_");
        sb.append(A05());
        return sb.toString();
    }

    public String A08(int i) {
        return A04().A02.A03(i);
    }

    public String A09(int i) {
        String A03;
        C28141Kv A04 = A04();
        if (A04.A07 || (A03 = A04.A03.A03(i)) == null) {
            return this.A00.getResources().getString(i);
        }
        return A03;
    }

    public String A0A(int i, Object... objArr) {
        return String.format(A00(this.A00), A08(i), objArr);
    }

    public String A0B(int i, Object... objArr) {
        return String.format(A00(this.A00), A09(i), objArr);
    }

    public String A0C(long j, int i) {
        return A04().A02.A04(Long.valueOf(j), i);
    }

    public String A0D(long j, int i) {
        C28141Kv A04 = A04();
        if (A04.A07) {
            int i2 = 2;
            if (j == 1) {
                i2 = 1;
            }
            return this.A00.getResources().getQuantityString(i, i2);
        }
        String A042 = A04.A03.A04(j, i);
        if (A042 == null) {
            return this.A00.getResources().getQuantityString(i, (int) j);
        }
        return A042;
    }

    public String A0E(TypedArray typedArray, int i) {
        int resourceId = typedArray.getResourceId(i, 0);
        if (resourceId != 0) {
            return A09(resourceId);
        }
        return null;
    }

    public String A0F(String str) {
        AnonymousClass02S A03 = A03();
        AnonymousClass02T r0 = A03.A01;
        if (str == null) {
            return null;
        }
        return A03.A02(r0, str).toString();
    }

    public String A0G(String str) {
        AnonymousClass02S A03 = A03();
        AnonymousClass02T r0 = AnonymousClass02U.A04;
        if (str == null) {
            return null;
        }
        return A03.A02(r0, str).toString();
    }

    public String A0H(Object[] objArr, int i, long j) {
        return String.format(A00(this.A00), A0C(j, i), objArr);
    }

    public String A0I(Object[] objArr, int i, long j) {
        return String.format(A00(this.A00), A0D(j, i), objArr);
    }

    public NumberFormat A0J() {
        return (NumberFormat) A04().A04.clone();
    }

    public NumberFormat A0K() {
        return (NumberFormat) A04().A05.clone();
    }

    public void A0L() {
        if (this.A06) {
            Locale.setDefault(this.A04);
            A0P();
        }
    }

    public final void A0M() {
        for (AbstractC18900tF r0 : this.A0B) {
            r0.ASC();
        }
    }

    public final void A0N() {
        synchronized (this.A0A) {
            this.A01 = null;
        }
        this.A03 = null;
        this.A02 = null;
        AnonymousClass1MY.A08();
    }

    public final void A0O() {
        C28181Ma r3 = new C28181Ma("WhatsAppLocale/setDerivedFieldsUnderLock/fieldCreationTimer");
        this.A01 = new C28141Kv(this.A00, this.A04);
        r3.A01();
    }

    public final void A0P() {
        if (!this.A00.getResources().getConfiguration().locale.equals(this.A04)) {
            if (A0C) {
                Context A01 = A01();
                this.A00 = A01;
                Resources resources = A01.getResources();
                Configuration configuration = resources.getConfiguration();
                configuration.locale = this.A04;
                resources.updateConfiguration(configuration, resources.getDisplayMetrics());
            } else {
                Configuration configuration2 = new Configuration();
                configuration2.setLocale(this.A04);
                this.A00 = A01().createConfigurationContext(configuration2);
            }
            A0N();
        }
    }

    public void A0Q(Configuration configuration) {
        Locale locale;
        this.A09.A00();
        if (Build.VERSION.SDK_INT >= 24) {
            locale = configuration.getLocales().get(0);
        } else {
            locale = configuration.locale;
        }
        if (locale == null && (locale = Locale.getDefault()) == null) {
            locale = Locale.US;
        }
        if (!this.A05.equals(locale)) {
            StringBuilder sb = new StringBuilder("whatsapplocale/savedefaultlanguage/phone language changed to: ");
            sb.append(AbstractC27291Gt.A05(locale));
            Log.i(sb.toString());
            this.A05 = locale;
            if (!this.A06) {
                this.A04 = locale;
                A0N();
                A0M();
            }
        }
    }

    public void A0R(String str) {
        String str2;
        Locale locale;
        StringBuilder sb = new StringBuilder("whatsapplocale/saveandapplylanguage/language to save: ");
        if (TextUtils.isEmpty(str)) {
            str2 = "device default";
        } else {
            str2 = str;
        }
        sb.append(str2);
        Log.i(sb.toString());
        boolean isEmpty = TextUtils.isEmpty(str);
        C14820m6 r0 = this.A08;
        if (!isEmpty) {
            r0.A0e(str);
            this.A06 = true;
            locale = AbstractC27291Gt.A09(str);
        } else {
            r0.A0I();
            this.A06 = false;
            locale = this.A05;
        }
        this.A04 = locale;
        StringBuilder sb2 = new StringBuilder("whatsapplocale/saveandapplylanguage/setting language ");
        sb2.append(locale.getDisplayLanguage(Locale.US));
        Log.i(sb2.toString());
        Locale.setDefault(this.A04);
        A0P();
        A0M();
    }

    public String[] A0S(int[] iArr) {
        int length = iArr.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            strArr[i] = A09(iArr[i]);
        }
        return strArr;
    }
}
