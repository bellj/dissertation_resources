package X;

import android.view.ViewGroup;

/* renamed from: X.0T4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0T4 {
    public static int A00(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return marginLayoutParams.getMarginEnd();
    }

    public static int A01(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return marginLayoutParams.getMarginStart();
    }

    public static void A02(ViewGroup.MarginLayoutParams marginLayoutParams, int i) {
        marginLayoutParams.setMarginEnd(i);
    }
}
