package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.whatsapp.deeplink.DeepLinkActivity;

/* renamed from: X.3g4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class HandlerC73333g4 extends Handler {
    public final /* synthetic */ DeepLinkActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HandlerC73333g4(Looper looper, DeepLinkActivity deepLinkActivity) {
        super(looper);
        this.A00 = deepLinkActivity;
        AnonymousClass009.A05(looper);
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        int i = message.arg1;
        if (i != 0) {
            this.A00.Ady(0, i);
        }
    }
}
