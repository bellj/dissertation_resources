package X;

import android.view.animation.Animation;
import android.widget.FrameLayout;
import com.whatsapp.contact.picker.PhoneContactsSelector;

/* renamed from: X.3Nx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class animation.Animation$AnimationListenerC66523Nx implements Animation.AnimationListener {
    public final int A00;
    public final /* synthetic */ PhoneContactsSelector A01;

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationRepeat(Animation animation) {
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationStart(Animation animation) {
    }

    public animation.Animation$AnimationListenerC66523Nx(PhoneContactsSelector phoneContactsSelector, int i) {
        this.A01 = phoneContactsSelector;
        this.A00 = i;
    }

    @Override // android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        PhoneContactsSelector phoneContactsSelector = this.A01;
        phoneContactsSelector.A05.clearAnimation();
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) phoneContactsSelector.A05.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, this.A00, layoutParams.rightMargin, layoutParams.bottomMargin);
        phoneContactsSelector.A05.setLayoutParams(layoutParams);
    }
}
