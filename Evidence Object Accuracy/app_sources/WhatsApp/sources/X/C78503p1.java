package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78503p1 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99434kI();
    public final int A00;
    public final String A01;
    public final String A02;
    public final boolean A03;

    public C78503p1(String str, String str2, int i, boolean z) {
        this.A01 = str;
        this.A02 = str2;
        this.A00 = i;
        this.A03 = z;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (!(obj instanceof C78503p1)) {
            return false;
        }
        return ((C78503p1) obj).A01.equals(this.A01);
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.A01.hashCode();
    }

    @Override // java.lang.Object
    public final String toString() {
        String str = this.A02;
        String str2 = this.A01;
        int i = this.A00;
        boolean z = this.A03;
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07(str) + 45 + C12970iu.A07(str2));
        A0t.append("Node{");
        A0t.append(str);
        A0t.append(", id=");
        A0t.append(str2);
        A0t.append(", hops=");
        A0t.append(i);
        A0t.append(", isNearby=");
        A0t.append(z);
        return C12960it.A0d("}", A0t);
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A0D(parcel, this.A02, 3, C95654e8.A0K(parcel, this.A01));
        C95654e8.A07(parcel, 4, this.A00);
        C95654e8.A09(parcel, 5, this.A03);
        C95654e8.A06(parcel, A00);
    }
}
