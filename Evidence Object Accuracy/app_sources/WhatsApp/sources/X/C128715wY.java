package X;

/* renamed from: X.5wY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128715wY {
    public final C15570nT A00;
    public final C22980zx A01;

    public C128715wY(C15570nT r1, C22980zx r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0064, code lost:
        if (r3.equals(X.C12960it.A0d(r2.number, r1)) == false) goto L_0x0066;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1IR A00(X.AnonymousClass1V8 r11) {
        /*
            r10 = this;
            r7 = r11
            java.lang.String r1 = r11.A00
            java.lang.String r0 = "transaction"
            boolean r0 = r0.equals(r1)
            r4 = 0
            if (r0 == 0) goto L_0x007c
            java.lang.String r0 = "transaction-type"
            java.lang.String r3 = "p2p"
            java.lang.String r8 = r11.A0I(r0, r3)
            java.lang.String r0 = "sender"
            java.lang.String r0 = r11.A0I(r0, r4)
            com.whatsapp.jid.UserJid r2 = com.whatsapp.jid.UserJid.getNullable(r0)
            java.lang.String r0 = "receiver"
            java.lang.String r0 = r11.A0I(r0, r4)
            com.whatsapp.jid.UserJid r1 = com.whatsapp.jid.UserJid.getNullable(r0)
            boolean r0 = r3.equals(r8)
            if (r0 == 0) goto L_0x0076
            if (r2 != 0) goto L_0x0076
            if (r1 != 0) goto L_0x0076
            java.lang.String r0 = "sender-info"
            X.1V8 r2 = r11.A0F(r0)     // Catch: 1V9 -> 0x0070
            java.lang.String r1 = "phone_number"
            java.lang.String r0 = ""
            java.lang.String r0 = r2.A0I(r1, r0)     // Catch: 1V9 -> 0x0070
            X.AnonymousClass009.A05(r0)     // Catch: 1V9 -> 0x0070
            java.lang.String r3 = X.C15550nR.A01(r0)     // Catch: 1V9 -> 0x0070
            X.0nT r0 = r10.A00     // Catch: 1V9 -> 0x0070
            r0.A08()     // Catch: 1V9 -> 0x0070
            com.whatsapp.Me r2 = r0.A00     // Catch: 1V9 -> 0x0070
            if (r2 == 0) goto L_0x0066
            java.lang.StringBuilder r1 = X.C12960it.A0h()     // Catch: 1V9 -> 0x0070
            java.lang.String r0 = r2.cc     // Catch: 1V9 -> 0x0070
            r1.append(r0)     // Catch: 1V9 -> 0x0070
            java.lang.String r0 = r2.number     // Catch: 1V9 -> 0x0070
            java.lang.String r0 = X.C12960it.A0d(r0, r1)     // Catch: 1V9 -> 0x0070
            boolean r0 = r3.equals(r0)     // Catch: 1V9 -> 0x0070
            r9 = 1
            if (r0 != 0) goto L_0x0067
        L_0x0066:
            r9 = 0
        L_0x0067:
            X.0zx r3 = r10.A01     // Catch: 1V9 -> 0x0070
            r6 = r4
            r5 = r4
            X.1IR r4 = r3.A02(r4, r5, r6, r7, r8, r9)     // Catch: 1V9 -> 0x0070
            return r4
        L_0x0070:
            java.lang.String r0 = "PAY: NoviActionUtils/parseResultFromAccountNode/parseTransactionNode sender-info not found"
            com.whatsapp.util.Log.e(r0)
            return r4
        L_0x0076:
            X.0zx r0 = r10.A01
            X.1IR r4 = r0.A05(r4, r11)
        L_0x007c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C128715wY.A00(X.1V8):X.1IR");
    }
}
