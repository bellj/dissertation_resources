package X;

import android.content.SharedPreferences;
import java.util.concurrent.Callable;

/* renamed from: X.5Ds  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class CallableC112525Ds implements Callable {
    public final /* synthetic */ SharedPreferences A00;
    public final /* synthetic */ Integer A01;
    public final /* synthetic */ String A02;

    public CallableC112525Ds(SharedPreferences sharedPreferences, Integer num, String str) {
        this.A00 = sharedPreferences;
        this.A02 = str;
        this.A01 = num;
    }

    @Override // java.util.concurrent.Callable
    public final /* synthetic */ Object call() {
        return Integer.valueOf(this.A00.getInt(this.A02, this.A01.intValue()));
    }
}
