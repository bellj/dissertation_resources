package X;

import android.os.Build;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;

/* renamed from: X.08A  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08A {
    public int A00;
    public int A01;
    public TextDirectionHeuristic A02;
    public final TextPaint A03;

    public AnonymousClass08A(TextPaint textPaint) {
        TextDirectionHeuristic textDirectionHeuristic;
        this.A03 = textPaint;
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            this.A00 = 1;
            this.A01 = 1;
        } else {
            this.A01 = 0;
            this.A00 = 0;
        }
        if (i >= 18) {
            textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
        } else {
            textDirectionHeuristic = null;
        }
        this.A02 = textDirectionHeuristic;
    }

    public AnonymousClass089 A00() {
        return new AnonymousClass089(this.A02, this.A03, this.A00, this.A01);
    }

    public void A01(int i) {
        this.A00 = i;
    }

    public void A02(int i) {
        this.A01 = i;
    }

    public void A03(TextDirectionHeuristic textDirectionHeuristic) {
        this.A02 = textDirectionHeuristic;
    }
}
