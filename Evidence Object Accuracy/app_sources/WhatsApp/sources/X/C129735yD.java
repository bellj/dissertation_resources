package X;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.whatsapp.R;

/* renamed from: X.5yD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129735yD {
    public C14900mE A00;
    public C18640sm A01;
    public C15650ng A02;
    public C18650sn A03;
    public C18600si A04;
    public C18610sj A05;
    public C18620sk A06;
    public C17070qD A07;
    public C130015yf A08;
    public C18590sh A09;
    public AbstractC14440lR A0A;
    public final C14850m9 A0B;
    public final AnonymousClass69D A0C;
    public final C129925yW A0D;
    public final C22710zW A0E;
    public final C120905gw A0F;
    public final C30931Zj A0G = C117305Zk.A0V("BrazilPaymentAccountActionsContainerPresenter", "payment-settings");
    public final C25871Bd A0H;

    public C129735yD(C14900mE r3, C18640sm r4, C15650ng r5, C14850m9 r6, AnonymousClass69D r7, C129925yW r8, C18650sn r9, C18600si r10, C18610sj r11, C22710zW r12, C18620sk r13, C17070qD r14, C120905gw r15, C130015yf r16, C25871Bd r17, C18590sh r18, AbstractC14440lR r19) {
        this.A00 = r3;
        this.A0A = r19;
        this.A09 = r18;
        this.A07 = r14;
        this.A02 = r5;
        this.A04 = r10;
        this.A05 = r11;
        this.A08 = r16;
        this.A06 = r13;
        this.A01 = r4;
        this.A03 = r9;
        this.A0B = r6;
        this.A0C = r7;
        this.A0E = r12;
        this.A0D = r8;
        this.A0H = r17;
        this.A0F = r15;
    }

    public final AlertDialog A00(ActivityC13790kL r5, CharSequence charSequence, CharSequence charSequence2, int i) {
        Context applicationContext = r5.getApplicationContext();
        return new AlertDialog.Builder(r5, R.style.FbPayDialogTheme).setMessage(charSequence).setTitle(charSequence2).setCancelable(true).setNegativeButton(applicationContext.getString(R.string.cancel), new DialogInterface.OnClickListener(i) { // from class: X.62d
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                C36021jC.A00(ActivityC13790kL.this, this.A00);
            }
        }).setPositiveButton(applicationContext.getString(R.string.close_payment_account_dialog_confirm_label), new DialogInterface.OnClickListener(r5, this, i) { // from class: X.62m
            public final /* synthetic */ int A00;
            public final /* synthetic */ ActivityC13790kL A01;
            public final /* synthetic */ C129735yD A02;

            {
                this.A02 = r2;
                this.A01 = r1;
                this.A00 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                C129735yD r3 = this.A02;
                ActivityC13790kL r2 = this.A01;
                C36021jC.A00(r2, this.A00);
                r2.A2C(R.string.register_wait_message);
                r3.A0F.A00(new C1328468l(r2, r3));
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener(i) { // from class: X.62D
            public final /* synthetic */ int A00;

            {
                this.A00 = r2;
            }

            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                C36021jC.A00(ActivityC13790kL.this, this.A00);
            }
        }).create();
    }

    public Dialog A01(Bundle bundle, ActivityC13790kL r6, int i) {
        Context applicationContext = r6.getApplicationContext();
        String str = null;
        switch (i) {
            case 100:
                return new AlertDialog.Builder(r6).setMessage(applicationContext.getString(R.string.payment_account_is_removed)).setPositiveButton(applicationContext.getString(R.string.ok), new DialogInterface.OnClickListener() { // from class: X.62F
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        ActivityC13790kL.this.finish();
                    }
                }).create();
            case 101:
                String string = r6.getString(R.string.delete_payment_accounts_dialog_title);
                if (bundle != null) {
                    string = bundle.getString("message");
                    str = bundle.getString("title");
                }
                return A00(r6, string, str, i);
            case 102:
                return A00(r6, r6.getString(R.string.reset_pin_delete_payment_accounts_dialog_title), null, i);
            default:
                return null;
        }
    }
}
