package X;

import android.content.Context;
import com.whatsapp.payments.ui.IndiaUpiBankAccountPickerActivity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5gm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120805gm extends C120895gv {
    public final /* synthetic */ C120455gD A00;
    public final /* synthetic */ C125825rs A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120805gm(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120455gD r11, C125825rs r12) {
        super(context, r8, r9, r10, "upi-register-vpa");
        this.A00 = r11;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        super.A02(r3);
        AbstractC136256Lv r1 = this.A00.A01;
        if (r1 != null) {
            r1.AUo(null, r3);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        super.A03(r3);
        AbstractC136256Lv r1 = this.A00.A01;
        if (r1 != null) {
            r1.AUo(null, r3);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r24) {
        super.A04(r24);
        C120455gD r6 = this.A00;
        C17070qD r13 = r6.A0A;
        AbstractC43531xB AEz = r13.A02().AEz();
        AnonymousClass009.A05(AEz);
        ArrayList AYv = AEz.AYv(r6.A05, r24);
        ArrayList A0l = C12960it.A0l();
        Iterator it = AYv.iterator();
        C119755f3 r4 = null;
        C30861Zc r12 = null;
        while (it.hasNext()) {
            C119755f3 r3 = (C119755f3) ((AnonymousClass1ZP) it.next());
            C17930rd r16 = C17930rd.A0E;
            String str = ((AbstractC30851Zb) r3).A06;
            boolean z = ((AbstractC30851Zb) r3).A08;
            int i = 2;
            int A00 = C117305Zk.A00(z ? 1 : 0);
            boolean z2 = ((AbstractC30851Zb) r3).A07;
            if (!z2) {
                i = 0;
            }
            AnonymousClass1ZR r9 = ((AbstractC30851Zb) r3).A02;
            String str2 = (String) C117295Zj.A0R(r9);
            byte[] bArr = ((AbstractC30851Zb) r3).A09;
            C30861Zc r15 = new C30861Zc(r16, A00, i, -1, -1);
            r15.A0A = str;
            r15.A0A(str2);
            r15.A0B = (String) C117295Zj.A0R(((AbstractC30851Zb) r3).A01);
            r15.A0D = bArr;
            r15.A08 = r3;
            A0l.add(r15);
            if ((r9 != null && str2.equals(C117295Zj.A0R(((AbstractC30851Zb) r6.A00).A02))) || (r4 == null && (z2 || z))) {
                r4 = r3;
                r12 = r15;
            }
        }
        r13.A00().A05(new AbstractC451720l(r12, r4, this, this.A01) { // from class: X.683
            public final /* synthetic */ C30861Zc A00;
            public final /* synthetic */ C119755f3 A01;
            public final /* synthetic */ C120805gm A02;
            public final /* synthetic */ C125825rs A03;

            {
                this.A02 = r3;
                this.A01 = r2;
                this.A00 = r1;
                this.A03 = r4;
            }

            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                C120805gm r2 = this.A02;
                C119755f3 r0 = this.A01;
                C30861Zc r42 = this.A00;
                C125825rs r32 = this.A03;
                if (r0 != null && !AnonymousClass1ZS.A02(r0.A09)) {
                    C21860y6 r1 = r2.A00.A08;
                    r1.A06(r1.A01("add_bank"));
                }
                C120455gD r22 = r2.A00;
                AbstractC136256Lv r14 = r22.A01;
                if (r14 != null && r42 != null) {
                    r14.AUo(r42, null);
                    C119755f3 r02 = (C119755f3) r42.A08;
                    if (r02 != null && C12970iu.A1Y(r02.A05.A00)) {
                        C21860y6 r17 = r22.A08;
                        r17.A06(r17.A01("2fa"));
                        IndiaUpiBankAccountPickerActivity indiaUpiBankAccountPickerActivity = r32.A00;
                        indiaUpiBankAccountPickerActivity.A0N.A00(indiaUpiBankAccountPickerActivity);
                    }
                }
            }
        }, A0l);
    }
}
