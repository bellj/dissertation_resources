package X;

import android.os.Message;

/* renamed from: X.13S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass13S implements AbstractC15920o8 {
    public C20970wc A00;
    public final C19890uq A01;

    public AnonymousClass13S(C20970wc r1, C19890uq r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{150, 192, 193, 197};
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        if (!(i == 150 || i == 197)) {
            if (i != 192) {
                if (i != 193) {
                    return false;
                }
            } else if ("terminate".equals(((AnonymousClass2MM) message.obj).A01.tag)) {
                this.A01.A06();
            }
        }
        this.A00.A00(new C26391De(Message.obtain(message), "receive_message"));
        return true;
    }
}
