package X;

import android.view.View;
import java.util.Iterator;

/* renamed from: X.5oA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123925oA extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ C121455i8 A00;

    public C123925oA(C121455i8 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        this.A00.A0E.Ab2(new Runnable() { // from class: X.6Gt
            @Override // java.lang.Runnable
            public final void run() {
                AbstractC28901Pl r2;
                C123925oA r5 = C123925oA.this;
                C121455i8 r4 = r5.A00;
                Iterator it = C117295Zj.A0Z(((C130035yh) r4).A04.A0D).iterator();
                while (true) {
                    if (!it.hasNext()) {
                        r2 = null;
                        break;
                    }
                    r2 = C117305Zk.A0H(it);
                    if (r2.A03 == 2) {
                        break;
                    }
                }
                r4.A0A.A0H(
                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0026: INVOKE  
                      (wrap: X.0mE : 0x001f: IGET  (r1v0 X.0mE A[REMOVE]) = (r4v0 'r4' X.5i8) X.5i8.A0A X.0mE)
                      (wrap: X.6IR : 0x0023: CONSTRUCTOR  (r0v4 X.6IR A[REMOVE]) = (r2v1 'r2' X.1Pl), (r5v0 'r5' X.5oA) call: X.6IR.<init>(X.1Pl, X.5oA):void type: CONSTRUCTOR)
                     type: VIRTUAL call: X.0mE.A0H(java.lang.Runnable):void in method: X.6Gt.run():void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                    	at java.util.ArrayList.forEach(ArrayList.java:1259)
                    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0023: CONSTRUCTOR  (r0v4 X.6IR A[REMOVE]) = (r2v1 'r2' X.1Pl), (r5v0 'r5' X.5oA) call: X.6IR.<init>(X.1Pl, X.5oA):void type: CONSTRUCTOR in method: X.6Gt.run():void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 15 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.6IR, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 21 more
                    */
                /*
                    this = this;
                    X.5oA r5 = X.C123925oA.this
                    X.5i8 r4 = r5.A00
                    X.5jx r0 = r4.A04
                    X.0qD r0 = r0.A0D
                    java.util.List r0 = X.C117295Zj.A0Z(r0)
                    java.util.Iterator r3 = r0.iterator()
                L_0x0010:
                    boolean r0 = r3.hasNext()
                    if (r0 == 0) goto L_0x002a
                    X.1Pl r2 = X.C117305Zk.A0H(r3)
                    int r1 = r2.A03
                    r0 = 2
                    if (r1 != r0) goto L_0x0010
                L_0x001f:
                    X.0mE r1 = r4.A0A
                    X.6IR r0 = new X.6IR
                    r0.<init>(r2, r5)
                    r1.A0H(r0)
                    return
                L_0x002a:
                    r2 = 0
                    goto L_0x001f
                */
                throw new UnsupportedOperationException("Method not decompiled: X.RunnableC134996Gt.run():void");
            }
        });
    }
}
