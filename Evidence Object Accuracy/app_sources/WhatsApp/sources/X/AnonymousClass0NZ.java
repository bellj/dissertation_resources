package X;

import android.util.SparseArray;
import androidx.constraintlayout.widget.ConstraintLayout;

/* renamed from: X.0NZ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0NZ {
    public SparseArray A00 = new SparseArray();
    public SparseArray A01 = new SparseArray();
    public AbstractC03980Jx A02 = null;
    public final ConstraintLayout A03;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x005c, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a7, code lost:
        if (r6 == -1) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0141, code lost:
        if (r0 == false) goto L_0x0143;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass0NZ(android.content.Context r21, androidx.constraintlayout.widget.ConstraintLayout r22, int r23) {
        /*
        // Method dump skipped, instructions count: 1098
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0NZ.<init>(android.content.Context, androidx.constraintlayout.widget.ConstraintLayout, int):void");
    }
}
