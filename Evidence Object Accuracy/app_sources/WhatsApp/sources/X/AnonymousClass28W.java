package X;

import java.util.ListIterator;

/* renamed from: X.28W  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass28W extends AnonymousClass1I5 implements ListIterator {
    @Override // java.util.ListIterator
    @Deprecated
    public final void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.ListIterator
    @Deprecated
    public final void set(Object obj) {
        throw new UnsupportedOperationException();
    }
}
