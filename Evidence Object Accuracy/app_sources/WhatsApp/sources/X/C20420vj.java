package X;

import android.content.SharedPreferences;

/* renamed from: X.0vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20420vj {
    public SharedPreferences A00;
    public final C14830m7 A01;
    public final C16630pM A02;

    public C20420vj(C14830m7 r1, C16630pM r2) {
        this.A01 = r1;
        this.A02 = r2;
    }

    public static final String A00(int i) {
        if (i == 1) {
            return "fg_cold_start_count_pref";
        }
        if (i == 2) {
            return "warm_start_count_pref";
        }
        if (i == 3) {
            return "lukewarm_start_count_pref";
        }
        StringBuilder sb = new StringBuilder("Unknown wam launch type enum value: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public final synchronized SharedPreferences A01() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A02.A01("core_health_event_pref_file");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }
}
