package X;

import android.content.Intent;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.service.MDSyncService;

/* renamed from: X.0tH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18920tH {
    public final C15450nH A00;
    public final C233411h A01;
    public final C233711k A02;
    public final C14830m7 A03;
    public final C16590pI A04;
    public final C15680nj A05;
    public final AnonymousClass10Y A06;
    public final AnonymousClass11B A07;
    public final C20140vH A08;
    public final C22290yq A09;
    public final C238813j A0A;
    public final C22090yV A0B;
    public final C14850m9 A0C;
    public final C20660w7 A0D;
    public final C244715q A0E;
    public final C238713i A0F;
    public final C22900zp A0G;
    public final C15860o1 A0H;
    public final AbstractC14440lR A0I;

    public C18920tH(C15450nH r2, C233411h r3, C233711k r4, C14830m7 r5, C16590pI r6, C15680nj r7, AnonymousClass10Y r8, AnonymousClass11B r9, C20140vH r10, C22290yq r11, C238813j r12, C22090yV r13, C14850m9 r14, C20660w7 r15, C244715q r16, C238713i r17, C22900zp r18, C15860o1 r19, AbstractC14440lR r20) {
        this.A03 = r5;
        this.A0C = r14;
        this.A04 = r6;
        this.A0I = r20;
        this.A08 = r10;
        this.A0D = r15;
        this.A00 = r2;
        this.A09 = r11;
        this.A0E = r16;
        this.A0G = r18;
        this.A06 = r8;
        this.A01 = r3;
        this.A0H = r19;
        this.A0B = r13;
        this.A05 = r7;
        this.A0F = r17;
        this.A02 = r4;
        this.A0A = r12;
        this.A07 = r9;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005e, code lost:
        if (r1 != null) goto L_0x0060;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A00(int r10) {
        /*
            r9 = this;
            r0 = 2
            if (r10 == r0) goto L_0x0034
            r0 = 3
            if (r10 != r0) goto L_0x008a
            X.0nH r1 = r9.A00
            X.0oW r0 = X.AbstractC15460nI.A1X
        L_0x000a:
            int r0 = r1.A02(r0)
            long r4 = (long) r0
            r2 = 0
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0089
            X.0vH r2 = r9.A08
            X.0m7 r0 = r9.A03
            long r7 = r0.A00()
            r0 = 86400000(0x5265c00, double:4.2687272E-316)
            long r4 = r4 * r0
            long r7 = r7 - r4
            r6 = 0
            X.1Ma r5 = new X.1Ma
            r5.<init>(r6)
            java.lang.String r0 = "rowidstore/getRowIdByTimestampExcludeSystemMessages"
            r5.A04(r0)
            X.0p7 r0 = r2.A06
            X.0on r4 = r0.get()
            goto L_0x0039
        L_0x0034:
            X.0nH r1 = r9.A00
            X.0oW r0 = X.AbstractC15460nI.A1a
            goto L_0x000a
        L_0x0039:
            X.0op r3 = r4.A03     // Catch: all -> 0x0064
            java.lang.String r2 = "SELECT _id FROM available_message_view WHERE (message_type != '7') AND timestamp > 0 AND timestamp <= ? ORDER BY sort_id DESC LIMIT 1"
            r0 = 1
            java.lang.String[] r1 = new java.lang.String[r0]     // Catch: all -> 0x0064
            java.lang.String r0 = java.lang.Long.toString(r7)     // Catch: all -> 0x0064
            r1[r6] = r0     // Catch: all -> 0x0064
            android.database.Cursor r1 = r3.A09(r2, r1)     // Catch: all -> 0x0064
            if (r1 == 0) goto L_0x005c
            boolean r0 = r1.moveToNext()     // Catch: all -> 0x0057
            if (r0 == 0) goto L_0x005c
            long r2 = r1.getLong(r6)     // Catch: all -> 0x0057
            goto L_0x0060
        L_0x0057:
            r0 = move-exception
            r1.close()     // Catch: all -> 0x005b
        L_0x005b:
            throw r0     // Catch: all -> 0x0064
        L_0x005c:
            r2 = 0
            if (r1 == 0) goto L_0x0069
        L_0x0060:
            r1.close()     // Catch: all -> 0x0064
            goto L_0x0069
        L_0x0064:
            r0 = move-exception
            r4.close()     // Catch: all -> 0x0068
        L_0x0068:
            throw r0
        L_0x0069:
            r4.close()
            java.lang.String r0 = "rowidstore/getRowIdByTimestampExcludeSystemMessages "
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>(r0)
            r4.append(r2)
            java.lang.String r0 = " | time spent:"
            r4.append(r0)
            long r0 = r5.A01()
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            com.whatsapp.util.Log.i(r0)
        L_0x0089:
            return r2
        L_0x008a:
            java.lang.String r1 = "Unexpected sync type "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r10)
            java.lang.String r1 = r0.toString()
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18920tH.A00(int):long");
    }

    public void A01() {
        this.A0G.A03(this.A04.A00, new Intent("com.whatsapp.service.MDSyncService.START_HISTORY_SYNC"), MDSyncService.class);
    }

    public void A02(AnonymousClass1JT r35, DeviceJid deviceJid, int i, long j, long j2, long j3) {
        C238813j r1 = this.A0A;
        C21910yB r3 = r1.A01;
        C16310on A02 = r3.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            AnonymousClass009.A00();
            C16310on A022 = r3.A02();
            int A01 = A022.A03.A01("msg_history_sync", "device_id=?  AND sync_type=?  AND status=?", new String[]{deviceJid.getRawString(), String.valueOf(i), String.valueOf(2)});
            r1.A01(A022, deviceJid);
            A022.close();
            int i2 = 0;
            if (A01 > 0) {
                i2 = 1;
            }
            r1.A02(new C40951sc(r35, deviceJid, i, 0, i2, -1, j, j2, j3, 0, 0, 0, -1));
            A00.A00();
            r1.A01(A02, deviceJid);
            A00.close();
            A02.close();
            if (i2 != 0) {
                A01();
            }
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
