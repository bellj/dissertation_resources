package X;

import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/* renamed from: X.3d3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CallableC71513d3 implements Callable, AbstractC116715Wn, AnonymousClass221 {
    public int A00 = 0;
    public long A01 = 0;
    public long A02 = 0;
    public long A03 = 0;
    public final C15450nH A04;
    public final C18790t3 A05;
    public final C14850m9 A06;
    public final C20110vE A07;
    public final C43161wW A08;
    public final C90374Nq A09;
    public final AbstractC38611oO A0A;
    public final C22600zL A0B;
    public final CountDownLatch A0C = new CountDownLatch(1);
    public final FutureTask A0D = new FutureTask(this);

    public CallableC71513d3(C15450nH r3, C18790t3 r4, C14850m9 r5, C20110vE r6, C43161wW r7, C90374Nq r8, AbstractC38611oO r9, C22600zL r10) {
        this.A06 = r5;
        this.A05 = r4;
        this.A04 = r3;
        this.A0B = r10;
        this.A07 = r6;
        this.A09 = r8;
        this.A0A = r9;
        this.A08 = r7;
    }

    public final void A00() {
        if (this.A0D.isCancelled()) {
            throw new CancellationException("plaindownload/cancelled");
        }
    }

    public final void A01(long j) {
        int i;
        long j2 = this.A01;
        if (j2 == 0) {
            i = 100;
        } else {
            i = (int) ((((double) (this.A03 + j)) * 100.0d) / ((double) j2));
        }
        if (i >= this.A00 + 5 || i == 100) {
            this.A00 = i;
            this.A0A.APO(j);
        }
    }

    @Override // X.AbstractC116715Wn
    public void A75() {
        try {
            cancel();
            this.A0C.await(3, TimeUnit.SECONDS);
        } catch (InterruptedException unused) {
            Log.e("PlainDownloadTransfer/waitCancelFinish Cannot fully cancel after 3 seconds");
        }
    }

    @Override // X.AbstractC116715Wn
    public AnonymousClass1t3 A9A() {
        try {
            FutureTask futureTask = this.A0D;
            futureTask.run();
            AnonymousClass1t3 r1 = (AnonymousClass1t3) futureTask.get();
            this.A0C.countDown();
            return r1;
        } catch (InterruptedException | CancellationException | ExecutionException e) {
            Log.e("plaindownload/exception ", e);
            this.A0C.countDown();
            return new AnonymousClass1t3(new AnonymousClass1RN(13));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:112:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01f1 A[DONT_GENERATE] */
    @Override // X.AnonymousClass221
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C95514dr Aav(X.C28481Nj r24) {
        /*
        // Method dump skipped, instructions count: 564
        */
        throw new UnsupportedOperationException("Method not decompiled: X.CallableC71513d3.Aav(X.1Nj):X.4dr");
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        int i;
        C43161wW r3 = this.A08;
        if (r3 != null) {
            r3.A0D = Long.valueOf(SystemClock.elapsedRealtime());
            r3.A02 = 0;
            int i2 = 0;
            if (this.A07.A01.A01()) {
                i2 = 4;
            }
            r3.A01 = i2;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime();
        C22600zL r7 = this.A0B;
        r7.A09();
        long elapsedRealtime2 = SystemClock.elapsedRealtime();
        C90374Nq r4 = this.A09;
        long AEj = r4.A00.AEj();
        this.A03 = AEj;
        if (r3 != null) {
            r3.A0J = Long.valueOf(elapsedRealtime2 - elapsedRealtime);
            r3.A0C = Long.valueOf(AEj);
        }
        A00();
        C37741mv A05 = r7.A05(r4.A01, 2);
        A00();
        Number number = (Number) A05.A00(this);
        if (r3 != null) {
            r3.A0I = C12980iv.A0l(A05.A01.get());
        }
        A00();
        if (number != null) {
            i = number.intValue();
        } else {
            i = 11;
        }
        AnonymousClass1RN r2 = new AnonymousClass1RN(i);
        A00();
        if (r3 != null) {
            r3.A06 = r2;
            r3.A0B = Long.valueOf(SystemClock.elapsedRealtime());
            r3.A02 = 4;
        }
        return new AnonymousClass1t3(r2);
    }

    @Override // X.AbstractC116715Wn
    public void cancel() {
        this.A0D.cancel(true);
    }
}
