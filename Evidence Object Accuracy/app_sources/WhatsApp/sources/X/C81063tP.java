package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3tP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81063tP extends AbstractC80953tE implements AbstractC117165Yt {
    public static final long serialVersionUID = 0;

    public C81063tP(AbstractC17190qP r1, int i) {
        super(r1, i);
    }

    public static C81043tN builder() {
        return new C81043tN();
    }

    public static C81063tP fromMapEntries(Collection collection, Comparator comparator) {
        if (collection.isEmpty()) {
            return of();
        }
        C28241Mh r5 = new C28241Mh(collection.size());
        int i = 0;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry A15 = C12970iu.A15(it);
            Object key = A15.getKey();
            AnonymousClass1Mr copyOf = AnonymousClass1Mr.copyOf((Collection) A15.getValue());
            if (!copyOf.isEmpty()) {
                r5.put(key, copyOf);
                i += copyOf.size();
            }
        }
        return new C81063tP(r5.build(), i);
    }

    public AnonymousClass1Mr get(Object obj) {
        AnonymousClass1Mr r0 = (AnonymousClass1Mr) this.map.get(obj);
        return r0 == null ? AnonymousClass1Mr.of() : r0;
    }

    public static C81063tP of() {
        return C81033tM.INSTANCE;
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            C28241Mh builder = AbstractC17190qP.builder();
            int i = 0;
            for (int i2 = 0; i2 < readInt; i2++) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    AnonymousClass29A builder2 = AnonymousClass1Mr.builder();
                    int i3 = 0;
                    do {
                        builder2.add(objectInputStream.readObject());
                        i3++;
                    } while (i3 < readInt2);
                    builder.put(readObject, builder2.build());
                    i += readInt2;
                } else {
                    throw new InvalidObjectException(C12960it.A0e("Invalid value count ", C12980iv.A0t(31), readInt2));
                }
            }
            try {
                AnonymousClass4H7.MAP_FIELD_SETTER.set(this, builder.build());
                AnonymousClass4H7.SIZE_FIELD_SETTER.set(this, i);
            } catch (IllegalArgumentException e) {
                throw new InvalidObjectException(e.getMessage()).initCause(e);
            }
        } else {
            throw new InvalidObjectException(C12960it.A0e("Invalid key count ", C12980iv.A0t(29), readInt));
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        C95324dW.writeMultimap(this, objectOutputStream);
    }
}
