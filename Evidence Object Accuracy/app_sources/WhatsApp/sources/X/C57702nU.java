package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57702nU extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57702nU A0G;
    public static volatile AnonymousClass255 A0H;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public long A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public AbstractC27881Jp A09;
    public AbstractC27881Jp A0A;
    public C43261wh A0B;
    public String A0C;
    public String A0D;
    public String A0E = "";
    public boolean A0F;

    static {
        C57702nU r0 = new C57702nU();
        A0G = r0;
        r0.A0W();
    }

    public C57702nU() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A07 = r0;
        this.A06 = r0;
        this.A09 = r0;
        this.A0D = "";
        this.A0C = "";
        this.A08 = r0;
        this.A0A = r0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        C81603uH r2;
        switch (r15.ordinal()) {
            case 0:
                return A0G;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                C57702nU r1 = (C57702nU) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A0E;
                int i2 = r1.A00;
                this.A0E = r7.Afy(str, r1.A0E, A1R, C12960it.A1R(i2));
                this.A07 = r7.Afm(this.A07, r1.A07, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A06 = r7.Afm(this.A06, r1.A06, C12960it.A1V(this.A00 & 4, 4), C12960it.A1V(r1.A00 & 4, 4));
                this.A09 = r7.Afm(this.A09, r1.A09, C12960it.A1V(this.A00 & 8, 8), C12960it.A1V(r1.A00 & 8, 8));
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 16, 16);
                String str2 = this.A0D;
                int i4 = r1.A00;
                this.A0D = r7.Afy(str2, r1.A0D, A1V, C12960it.A1V(i4 & 16, 16));
                this.A02 = r7.Afp(this.A02, r1.A02, C12960it.A1V(i3 & 32, 32), C12960it.A1V(i4 & 32, 32));
                this.A03 = r7.Afp(this.A03, r1.A03, C12960it.A1V(i3 & 64, 64), C12960it.A1V(i4 & 64, 64));
                this.A0C = r7.Afy(this.A0C, r1.A0C, C12960it.A1V(i3 & 128, 128), C12960it.A1V(i4 & 128, 128));
                this.A04 = r7.Afs(this.A04, r1.A04, C12960it.A1V(i3 & 256, 256), C12960it.A1V(i4 & 256, 256));
                this.A05 = r7.Afs(this.A05, r1.A05, C12960it.A1V(i3 & 512, 512), C12960it.A1V(i4 & 512, 512));
                this.A01 = r7.Afp(this.A01, r1.A01, C12960it.A1V(i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), C12960it.A1V(i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH));
                this.A08 = r7.Afm(this.A08, r1.A08, C12960it.A1V(i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH), C12960it.A1V(i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH));
                int i5 = this.A00;
                boolean A1V2 = C12960it.A1V(i5 & 4096, 4096);
                boolean z = this.A0F;
                int i6 = r1.A00;
                this.A0F = r7.Afl(A1V2, z, C12960it.A1V(i6 & 4096, 4096), r1.A0F);
                this.A0A = r7.Afm(this.A0A, r1.A0A, C12960it.A1V(i5 & DefaultCrypto.BUFFER_SIZE, DefaultCrypto.BUFFER_SIZE), C12960it.A1V(i6 & DefaultCrypto.BUFFER_SIZE, DefaultCrypto.BUFFER_SIZE));
                this.A0B = (C43261wh) r7.Aft(this.A0B, r1.A0B);
                if (r7 == C463025i.A00) {
                    this.A00 |= r1.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                AnonymousClass254 r12 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            switch (A03) {
                                case 0:
                                    break;
                                case 10:
                                    String A0A = r72.A0A();
                                    this.A00 = 1 | this.A00;
                                    this.A0E = A0A;
                                    break;
                                case 18:
                                    this.A00 |= 2;
                                    this.A07 = r72.A08();
                                    break;
                                case 26:
                                    this.A00 |= 4;
                                    this.A06 = r72.A08();
                                    break;
                                case 34:
                                    this.A00 |= 8;
                                    this.A09 = r72.A08();
                                    break;
                                case 42:
                                    String A0A2 = r72.A0A();
                                    this.A00 |= 16;
                                    this.A0D = A0A2;
                                    break;
                                case 48:
                                    this.A00 |= 32;
                                    this.A02 = r72.A02();
                                    break;
                                case 56:
                                    this.A00 |= 64;
                                    this.A03 = r72.A02();
                                    break;
                                case 66:
                                    String A0A3 = r72.A0A();
                                    this.A00 |= 128;
                                    this.A0C = A0A3;
                                    break;
                                case C43951xu.A02:
                                    this.A00 |= 256;
                                    this.A04 = r72.A06();
                                    break;
                                case 80:
                                    this.A00 |= 512;
                                    this.A05 = r72.A06();
                                    break;
                                case 88:
                                    this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                    this.A01 = r72.A02();
                                    break;
                                case 98:
                                    this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                    this.A08 = r72.A08();
                                    break;
                                case 104:
                                    this.A00 |= 4096;
                                    this.A0F = r72.A0F();
                                    break;
                                case 130:
                                    this.A00 |= DefaultCrypto.BUFFER_SIZE;
                                    this.A0A = r72.A08();
                                    break;
                                case 138:
                                    if ((this.A00 & 16384) == 16384) {
                                        r2 = (C81603uH) this.A0B.A0T();
                                    } else {
                                        r2 = null;
                                    }
                                    C43261wh r0 = (C43261wh) AbstractC27091Fz.A0H(r72, r12, C43261wh.A0O);
                                    this.A0B = r0;
                                    if (r2 != null) {
                                        this.A0B = (C43261wh) AbstractC27091Fz.A0C(r2, r0);
                                    }
                                    this.A00 |= 16384;
                                    break;
                                default:
                                    if (A0a(r72, A03)) {
                                        break;
                                    } else {
                                        break;
                                    }
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new C57702nU();
            case 5:
                return new C82343vT();
            case 6:
                break;
            case 7:
                if (A0H == null) {
                    synchronized (C57702nU.class) {
                        if (A0H == null) {
                            A0H = AbstractC27091Fz.A09(A0G);
                        }
                    }
                }
                return A0H;
            default:
                throw C12970iu.A0z();
        }
        return A0G;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A0E, 0);
        }
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A05(this.A07, 2, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A05(this.A06, 3, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A05(this.A09, 4, i2);
        }
        if ((i3 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A0D, i2);
        }
        int i4 = this.A00;
        if ((i4 & 32) == 32) {
            i2 = AbstractC27091Fz.A02(6, this.A02, i2);
        }
        if ((i4 & 64) == 64) {
            i2 = AbstractC27091Fz.A02(7, this.A03, i2);
        }
        if ((i4 & 128) == 128) {
            i2 = AbstractC27091Fz.A04(8, this.A0C, i2);
        }
        int i5 = this.A00;
        if ((i5 & 256) == 256) {
            i2 += CodedOutputStream.A06(9, this.A04);
        }
        if ((i5 & 512) == 512) {
            i2 += CodedOutputStream.A05(10, this.A05);
        }
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 = AbstractC27091Fz.A02(11, this.A01, i2);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 = AbstractC27091Fz.A05(this.A08, 12, i2);
        }
        if ((i5 & 4096) == 4096) {
            i2 += CodedOutputStream.A00(13);
        }
        if ((i5 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            i2 = AbstractC27091Fz.A05(this.A0A, 16, i2);
        }
        if ((i5 & 16384) == 16384) {
            C43261wh r0 = this.A0B;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            i2 = AbstractC27091Fz.A08(r0, 17, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A0E);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A07, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A06, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A09, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A0D);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0F(6, this.A02);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0F(7, this.A03);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A0C);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0H(9, this.A04);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0H(10, this.A05);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0F(11, this.A01);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0K(this.A08, 12);
        }
        if ((this.A00 & 4096) == 4096) {
            codedOutputStream.A0J(13, this.A0F);
        }
        if ((this.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
            codedOutputStream.A0K(this.A0A, 16);
        }
        if ((this.A00 & 16384) == 16384) {
            C43261wh r0 = this.A0B;
            if (r0 == null) {
                r0 = C43261wh.A0O;
            }
            codedOutputStream.A0L(r0, 17);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
