package X;

import android.text.Editable;
import android.widget.Button;

/* renamed from: X.47N  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47N extends C469928m {
    public final /* synthetic */ Button A00;
    public final /* synthetic */ DialogC58332oe A01;

    public AnonymousClass47N(Button button, DialogC58332oe r2) {
        this.A01 = r2;
        this.A00 = button;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        boolean A0C = AnonymousClass1US.A0C(editable.toString());
        Button button = this.A00;
        boolean z = true;
        if (A0C) {
            z = false;
        }
        button.setEnabled(z);
    }
}
