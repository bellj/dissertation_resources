package X;

import java.util.TimerTask;

/* renamed from: X.5IO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IO extends TimerTask {
    public AbstractC32541cK A00;

    public AnonymousClass5IO(AbstractC32541cK r1) {
        this.A00 = r1;
    }

    @Override // java.util.TimerTask, java.lang.Runnable
    public void run() {
        this.A00.A00();
    }
}
