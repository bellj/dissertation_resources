package X;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0110000_I0;
import com.whatsapp.util.Log;

/* renamed from: X.14A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14A extends AbstractC16230of implements ComponentCallbacks2 {
    public long A00;
    public long A01;
    public final C16620pL A02;
    public final AbstractC14440lR A03;

    @Override // android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
    }

    @Override // android.content.ComponentCallbacks
    public void onLowMemory() {
    }

    public AnonymousClass14A(C16620pL r2, C16590pI r3, AbstractC14440lR r4) {
        this.A03 = r4;
        this.A02 = r2;
        r3.A00.registerComponentCallbacks(this);
    }

    @Override // android.content.ComponentCallbacks2
    public void onTrimMemory(int i) {
        boolean z;
        if (i >= 60) {
            if (SystemClock.uptimeMillis() > this.A00 + 60000) {
                this.A00 = SystemClock.uptimeMillis();
                StringBuilder sb = new StringBuilder();
                sb.append("OnTrimMemory/level: ");
                sb.append(i);
                sb.append(", trimming memory, app in background");
                Log.i(sb.toString());
                z = false;
            } else {
                return;
            }
        } else if (i >= 15 && i < 20 && SystemClock.uptimeMillis() > this.A01 + C26061Bw.A0L) {
            this.A01 = SystemClock.uptimeMillis();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("OnTrimMemory/level: ");
            sb2.append(i);
            sb2.append(", trimming memory, app in foreground");
            Log.i(sb2.toString());
            z = true;
        } else {
            return;
        }
        this.A03.Ab2(new RunnableBRunnable0Shape0S0110000_I0(this, 12, z));
    }
}
