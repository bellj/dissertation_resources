package X;

/* renamed from: X.1FY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FY extends AbstractC17250qV {
    public final C14900mE A00;
    public final C22920zr A01;
    public final C20670w8 A02;
    public final C15990oG A03;
    public final C18240s8 A04;
    public final C16030oK A05;
    public final C20660w7 A06;

    public AnonymousClass1FY(AbstractC15710nm r9, C14900mE r10, C22920zr r11, C20670w8 r12, C15990oG r13, C18240s8 r14, C16030oK r15, C17220qS r16, C20660w7 r17, C17230qT r18, AbstractC14440lR r19) {
        super(r9, r16, r18, r19, new int[]{240}, false);
        this.A00 = r10;
        this.A06 = r17;
        this.A02 = r12;
        this.A04 = r14;
        this.A01 = r11;
        this.A03 = r13;
        this.A05 = r15;
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    @Override // X.AbstractC17250qV
    public void A00(X.AnonymousClass1V8 r32, int r33) {
        /*
        // Method dump skipped, instructions count: 1766
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1FY.A00(X.1V8, int):void");
    }
}
