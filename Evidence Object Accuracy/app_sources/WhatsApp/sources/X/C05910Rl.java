package X;

import java.util.concurrent.Executor;

/* renamed from: X.0Rl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C05910Rl {
    public static final C05910Rl A03 = new C05910Rl(null, null);
    public C05910Rl A00;
    public final Runnable A01;
    public final Executor A02;

    public C05910Rl(Runnable runnable, Executor executor) {
        this.A01 = runnable;
        this.A02 = executor;
    }
}
