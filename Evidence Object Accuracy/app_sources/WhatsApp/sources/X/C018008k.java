package X;

import android.view.Menu;
import android.view.View;
import android.view.Window;

/* renamed from: X.08k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C018008k extends Window$CallbackC013306g {
    public final /* synthetic */ AnonymousClass08Z A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C018008k(Window.Callback callback, AnonymousClass08Z r2) {
        super(callback);
        this.A00 = r2;
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public View onCreatePanelView(int i) {
        if (i == 0) {
            return new View(((C017408d) this.A00.A01).A09.getContext());
        }
        return super.onCreatePanelView(i);
    }

    @Override // X.Window$CallbackC013306g, android.view.Window.Callback
    public boolean onPreparePanel(int i, View view, Menu menu) {
        boolean onPreparePanel = super.onPreparePanel(i, view, menu);
        if (onPreparePanel) {
            AnonymousClass08Z r2 = this.A00;
            if (!r2.A05) {
                ((C017408d) r2.A01).A0D = true;
                r2.A05 = true;
            }
        }
        return onPreparePanel;
    }
}
