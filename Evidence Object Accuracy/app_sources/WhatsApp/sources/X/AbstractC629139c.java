package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.wds.components.button.WDSButton;

/* renamed from: X.39c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC629139c extends WDSButton {
    public boolean A00;

    public AbstractC629139c(Context context) {
        super(context);
        A00();
    }

    public AbstractC629139c(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }
}
