package X;

/* renamed from: X.1p8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC39021p8 {
    boolean doesRenderSupportScaling();

    AbstractC39031p9 getFrame(int i);

    int getFrameCount();

    int[] getFrameDurations();

    C91744Sy getFrameInfo(int i);

    int getHeight();

    int getLoopCount();

    int getSizeInBytes();

    int getWidth();
}
