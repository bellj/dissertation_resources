package X;

/* renamed from: X.5Yb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC117055Yb extends AnonymousClass5SG {
    boolean AIJ();

    boolean AJN();

    boolean AJx();

    void AaS(long j, long j2);

    void AcY(float f, float f2);

    @Override // X.AnonymousClass5WX
    String getName();

    void reset();
}
