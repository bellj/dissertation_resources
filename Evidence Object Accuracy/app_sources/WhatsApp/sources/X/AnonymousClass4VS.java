package X;

/* renamed from: X.4VS  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4VS {
    public int A00;
    public C94914ck[] A01 = new C94914ck[6];

    public void A00(C94914ck r5) {
        while (r5 != null) {
            int i = 0;
            while (true) {
                int i2 = this.A00;
                if (i >= i2) {
                    C94914ck[] r2 = this.A01;
                    int length = r2.length;
                    if (i2 >= length) {
                        C94914ck[] r1 = new C94914ck[length + 6];
                        System.arraycopy(r2, 0, r1, 0, i2);
                        this.A01 = r1;
                        r2 = r1;
                    }
                    int i3 = this.A00;
                    this.A00 = i3 + 1;
                    r2[i3] = r5;
                } else if (!this.A01[i].A02.equals(r5.A02)) {
                    i++;
                }
            }
            r5 = r5.A00;
        }
    }
}
