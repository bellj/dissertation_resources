package X;

import java.util.ListIterator;

/* renamed from: X.5DQ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5DQ implements ListIterator {
    public ListIterator A00;
    public final /* synthetic */ AnonymousClass5I1 A01;

    public AnonymousClass5DQ(AnonymousClass5I1 r2, int i) {
        this.A01 = r2;
        this.A00 = r2.A00.listIterator(i);
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ void add(Object obj) {
        throw C12970iu.A0z();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final boolean hasNext() {
        return this.A00.hasNext();
    }

    @Override // java.util.ListIterator
    public final boolean hasPrevious() {
        return this.A00.hasPrevious();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final /* synthetic */ Object next() {
        return this.A00.next();
    }

    @Override // java.util.ListIterator
    public final int nextIndex() {
        return this.A00.nextIndex();
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ Object previous() {
        return this.A00.previous();
    }

    @Override // java.util.ListIterator
    public final int previousIndex() {
        return this.A00.previousIndex();
    }

    @Override // java.util.ListIterator, java.util.Iterator
    public final void remove() {
        throw C12970iu.A0z();
    }

    @Override // java.util.ListIterator
    public final /* synthetic */ void set(Object obj) {
        throw C12970iu.A0z();
    }
}
