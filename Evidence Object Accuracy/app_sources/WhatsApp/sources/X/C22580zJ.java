package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.0zJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22580zJ {
    public static final long A02 = TimeUnit.DAYS.toMillis(7);
    public final C18640sm A00;
    public final C14820m6 A01;

    public C22580zJ(C18640sm r1, C14820m6 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }
}
