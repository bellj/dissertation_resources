package X;

import android.graphics.Rect;
import android.util.Log;
import android.view.WindowInsets;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/* renamed from: X.0Dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02660Dl extends AnonymousClass0PY {
    public static Constructor A02;
    public static Field A03;
    public static boolean A04;
    public static boolean A05;
    public WindowInsets A00;
    public AnonymousClass0U7 A01;

    public C02660Dl() {
        super(new C018408o());
        WindowInsets windowInsets;
        WindowInsets windowInsets2;
        if (!A05) {
            try {
                A03 = WindowInsets.class.getDeclaredField("CONSUMED");
            } catch (ReflectiveOperationException e) {
                Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets.CONSUMED field", e);
            }
            A05 = true;
        }
        Field field = A03;
        WindowInsets windowInsets3 = null;
        if (field != null) {
            try {
                windowInsets = (WindowInsets) field.get(null);
            } catch (ReflectiveOperationException e2) {
                Log.i("WindowInsetsCompat", "Could not get value from WindowInsets.CONSUMED field", e2);
            }
            if (windowInsets != null) {
                windowInsets2 = new WindowInsets(windowInsets);
                windowInsets3 = windowInsets2;
                this.A00 = windowInsets3;
            }
        }
        if (!A04) {
            try {
                A02 = WindowInsets.class.getConstructor(Rect.class);
            } catch (ReflectiveOperationException e3) {
                Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets(Rect) constructor", e3);
            }
            A04 = true;
        }
        Constructor constructor = A02;
        if (constructor != null) {
            try {
                windowInsets2 = (WindowInsets) constructor.newInstance(new Rect());
                windowInsets3 = windowInsets2;
            } catch (ReflectiveOperationException e4) {
                Log.i("WindowInsetsCompat", "Could not invoke WindowInsets(Rect) constructor", e4);
            }
        }
        this.A00 = windowInsets3;
    }

    public C02660Dl(C018408o r2) {
        super(r2);
        this.A00 = r2.A07();
    }

    @Override // X.AnonymousClass0PY
    public C018408o A00() {
        C018408o A022 = C018408o.A02(this.A00);
        A022.A00.A0C(this.A01);
        return A022;
    }

    @Override // X.AnonymousClass0PY
    public void A01(AnonymousClass0U7 r1) {
        this.A01 = r1;
    }

    @Override // X.AnonymousClass0PY
    public void A02(AnonymousClass0U7 r6) {
        WindowInsets windowInsets = this.A00;
        if (windowInsets != null) {
            this.A00 = windowInsets.replaceSystemWindowInsets(r6.A01, r6.A03, r6.A02, r6.A00);
        }
    }
}
