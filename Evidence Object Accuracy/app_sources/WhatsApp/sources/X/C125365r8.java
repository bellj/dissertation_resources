package X;

import java.util.HashSet;

/* renamed from: X.5r8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C125365r8 {
    public static final HashSet A00;
    public static final HashSet A01;
    public static final HashSet A02;

    static {
        String[] strArr = new String[4];
        strArr[0] = "01";
        strArr[1] = "02";
        strArr[2] = "19";
        A02 = C12970iu.A13("20", strArr, 3);
        String[] strArr2 = new String[4];
        strArr2[0] = "15";
        strArr2[1] = "16";
        strArr2[2] = "22";
        A00 = C12970iu.A13("23", strArr2, 3);
        String[] strArr3 = new String[2];
        strArr3[0] = "04";
        A01 = C12970iu.A13("05", strArr3, 1);
    }
}
