package X;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: X.52M  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52M implements AbstractC117035Xw {
    public final int A00 = -1;
    public final AnonymousClass4YL A01;

    public AnonymousClass52M() {
        AnonymousClass4YL r1 = AnonymousClass4ZZ.A02.A01;
        this.A01 = r1;
    }

    @Override // X.AbstractC117035Xw
    public Collection AFy(Object obj) {
        if (!(obj instanceof List)) {
            return ((Map) obj).keySet();
        }
        throw C12970iu.A0z();
    }

    @Override // X.AbstractC117035Xw
    public int AKQ(Object obj) {
        String str;
        if (obj instanceof List) {
            return C72453ed.A0C(obj);
        }
        if (obj instanceof Map) {
            return AFy(obj).size();
        }
        if (obj instanceof String) {
            return ((String) obj).length();
        }
        StringBuilder A0k = C12960it.A0k("length operation cannot be applied to ");
        if (obj != null) {
            str = C12980iv.A0s(obj);
        } else {
            str = "null";
        }
        throw new AnonymousClass5H8(C12960it.A0d(str, A0k));
    }

    @Override // X.AbstractC117035Xw
    public Object AYu(String str) {
        try {
            C94764cV r0 = new C94764cV(this.A00);
            return new AnonymousClass5LL(r0.A00).A0A(str, this.A01);
        } catch (AnonymousClass4CH e) {
            throw new C82833wG(e);
        }
    }

    @Override // X.AbstractC117035Xw
    public void Abk(Object obj, int i, Object obj2) {
        if (obj instanceof List) {
            List list = (List) obj;
            if (i == list.size()) {
                list.add(obj2);
            } else {
                list.set(i, obj2);
            }
        } else {
            throw C12970iu.A0z();
        }
    }

    @Override // X.AbstractC117035Xw
    public void Acf(Object obj, Object obj2, Object obj3) {
        String str;
        if (obj instanceof Map) {
            ((Map) obj).put(obj2.toString(), obj3);
            return;
        }
        if (C12960it.A0b("setProperty operation cannot be used with ", obj) != null) {
            str = C12980iv.A0s(obj);
        } else {
            str = "null";
        }
        throw new AnonymousClass5H8(str);
    }

    @Override // X.AbstractC117035Xw
    public Iterable Aet(Object obj) {
        String str;
        if (obj instanceof List) {
            return (Iterable) obj;
        }
        if (C12960it.A0b("Cannot iterate over ", obj) != null) {
            str = C12980iv.A0s(obj);
        } else {
            str = "null";
        }
        throw new AnonymousClass5H8(str);
    }
}
