package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3qA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79173qA extends AbstractC113535Hy<Float> implements AnonymousClass5Z5<Float>, RandomAccess {
    public static final C79173qA A02;
    public int A00;
    public float[] A01;

    public C79173qA(float[] fArr, int i) {
        this.A01 = fArr;
        this.A00 = i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C79173qA)) {
            return super.addAll(collection);
        }
        C79173qA r7 = (C79173qA) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.A01;
            if (i3 > fArr.length) {
                fArr = Arrays.copyOf(fArr, i3);
                this.A01 = fArr;
            }
            System.arraycopy(r7.A01, 0, fArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C79173qA)) {
                return super.equals(obj);
            }
            C79173qA r8 = (C79173qA) obj;
            int i = this.A00;
            if (i == r8.A00) {
                float[] fArr = r8.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == fArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.A01[i2]);
        }
        return i;
    }

    @Override // X.AbstractC113535Hy, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean remove(Object obj) {
        A02();
        for (int i = 0; i < this.A00; i++) {
            if (obj.equals(Float.valueOf(this.A01[i]))) {
                float[] fArr = this.A01;
                System.arraycopy(fArr, i + 1, fArr, i, this.A00 - i);
                this.A00--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            float[] fArr = this.A01;
            System.arraycopy(fArr, i2, fArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }

    static {
        C79173qA r0 = new C79173qA(new float[10], 0);
        A02 = r0;
        ((AbstractC113535Hy) r0).A00 = false;
    }

    public final void A03(int i, float f) {
        int i2;
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        float[] fArr = this.A01;
        if (i2 < fArr.length) {
            C72463ee.A0T(fArr, i, i2);
        } else {
            float[] fArr2 = new float[((i2 * 3) >> 1) + 1];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            System.arraycopy(this.A01, i, fArr2, i + 1, this.A00 - i);
            this.A01 = fArr2;
        }
        this.A01[i] = f;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5Z5
    public final /* synthetic */ AnonymousClass5Z5 AhN(int i) {
        if (i >= this.A00) {
            return new C79173qA(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        A03(i, C72453ed.A02(obj));
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        if (i >= 0 && i < this.A00) {
            return Float.valueOf(this.A01[i]);
        }
        throw C72453ed.A0i(i, this.A00);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        A02();
        if (i < 0 || i >= (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        float[] fArr = this.A01;
        float f = fArr[i];
        AbstractC113535Hy.A01(fArr, i2, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Float.valueOf(f);
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        float A022 = C72453ed.A02(obj);
        A02();
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
        float[] fArr = this.A01;
        float f = fArr[i];
        fArr[i] = A022;
        return Float.valueOf(f);
    }
}
