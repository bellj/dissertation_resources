package X;

import java.util.List;

/* renamed from: X.2wZ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2wZ extends AnonymousClass4KB {
    public final List A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass2wZ) && C16700pc.A0O(this.A00, ((AnonymousClass2wZ) obj).A00));
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public AnonymousClass2wZ(List list) {
        super(list);
        this.A00 = list;
    }

    public String toString() {
        return C12960it.A0a(this.A00, C12960it.A0k("LoadingContinueSearch(loadingItems="));
    }
}
