package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;

/* renamed from: X.0tl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19220tl extends AbstractC18500sY implements AbstractC19010tQ {
    public final C18460sU A00;
    public final C20370ve A01;

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19220tl(C18460sU r3, C20370ve r4, C18480sW r5) {
        super(r5, "payment_transaction", 1);
        this.A00 = r3;
        this.A01 = r4;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        Jid nullable;
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
        int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("message_row_id");
        int columnIndexOrThrow3 = cursor.getColumnIndexOrThrow("key_remote_jid");
        int columnIndexOrThrow4 = cursor.getColumnIndexOrThrow("key_id");
        int columnIndexOrThrow5 = cursor.getColumnIndexOrThrow("interop_id");
        int columnIndexOrThrow6 = cursor.getColumnIndexOrThrow("id");
        int columnIndexOrThrow7 = cursor.getColumnIndexOrThrow("timestamp");
        int columnIndexOrThrow8 = cursor.getColumnIndexOrThrow("status");
        int columnIndexOrThrow9 = cursor.getColumnIndexOrThrow("error_code");
        int columnIndexOrThrow10 = cursor.getColumnIndexOrThrow("sender");
        int columnIndexOrThrow11 = cursor.getColumnIndexOrThrow("receiver");
        int columnIndexOrThrow12 = cursor.getColumnIndexOrThrow("type");
        int columnIndexOrThrow13 = cursor.getColumnIndexOrThrow("currency");
        int columnIndexOrThrow14 = cursor.getColumnIndexOrThrow("amount_1000");
        int columnIndexOrThrow15 = cursor.getColumnIndexOrThrow("credential_id");
        int columnIndexOrThrow16 = cursor.getColumnIndexOrThrow("methods");
        int columnIndexOrThrow17 = cursor.getColumnIndexOrThrow("bank_transaction_id");
        int columnIndexOrThrow18 = cursor.getColumnIndexOrThrow("metadata");
        int columnIndexOrThrow19 = cursor.getColumnIndexOrThrow("init_timestamp");
        int columnIndexOrThrow20 = cursor.getColumnIndexOrThrow("request_key_id");
        int columnIndexOrThrow21 = cursor.getColumnIndexOrThrow("country");
        int columnIndexOrThrow22 = cursor.getColumnIndexOrThrow("version");
        int columnIndexOrThrow23 = cursor.getColumnIndexOrThrow("future_data");
        int columnIndexOrThrow24 = cursor.getColumnIndexOrThrow("service_id");
        int columnIndexOrThrow25 = cursor.getColumnIndexOrThrow("background_id");
        int columnIndexOrThrow26 = cursor.getColumnIndexOrThrow("purchase_initiator");
        C16310on A02 = this.A05.A02();
        long j = -1;
        int i = 0;
        while (cursor.moveToNext()) {
            try {
                ContentValues contentValues = new ContentValues(25);
                j = cursor.getLong(columnIndexOrThrow);
                contentValues.put("_id", Long.valueOf(j));
                if (!cursor.isNull(columnIndexOrThrow2)) {
                    contentValues.put("message_row_id", Long.valueOf(cursor.getLong(columnIndexOrThrow2)));
                }
                if (!cursor.isNull(columnIndexOrThrow3) && (nullable = Jid.getNullable(cursor.getString(columnIndexOrThrow3))) != null) {
                    long A01 = this.A00.A01(nullable);
                    if (A01 != -1) {
                        contentValues.put("remote_jid_row_id", Long.valueOf(A01));
                    }
                }
                C30021Vq.A04(contentValues, "key_id", cursor.getString(columnIndexOrThrow4));
                C30021Vq.A04(contentValues, "interop_id", cursor.getString(columnIndexOrThrow5));
                C30021Vq.A04(contentValues, "id", cursor.getString(columnIndexOrThrow6));
                if (!cursor.isNull(columnIndexOrThrow7)) {
                    contentValues.put("timestamp", Long.valueOf(cursor.getLong(columnIndexOrThrow7)));
                }
                if (!cursor.isNull(columnIndexOrThrow8)) {
                    contentValues.put("status", Long.valueOf(cursor.getLong(columnIndexOrThrow8)));
                }
                C30021Vq.A04(contentValues, "error_code", cursor.getString(columnIndexOrThrow9));
                Jid nullable2 = Jid.getNullable(cursor.getString(columnIndexOrThrow10));
                if (nullable2 != null) {
                    long A012 = this.A00.A01(nullable2);
                    if (A012 != -1) {
                        contentValues.put("sender_jid_row_id", Long.valueOf(A012));
                    }
                }
                Jid nullable3 = Jid.getNullable(cursor.getString(columnIndexOrThrow11));
                if (nullable3 != null) {
                    long A013 = this.A00.A01(nullable3);
                    if (A013 != -1) {
                        contentValues.put("receiver_jid_row_id", Long.valueOf(A013));
                    }
                }
                contentValues.put("type", Long.valueOf(cursor.getLong(columnIndexOrThrow12)));
                C30021Vq.A04(contentValues, "currency_code", cursor.getString(columnIndexOrThrow13));
                contentValues.put("amount_1000", Long.valueOf(cursor.getLong(columnIndexOrThrow14)));
                C30021Vq.A04(contentValues, "credential_id", cursor.getString(columnIndexOrThrow15));
                C30021Vq.A04(contentValues, "methods", cursor.getString(columnIndexOrThrow16));
                C30021Vq.A04(contentValues, "bank_transaction_id", cursor.getString(columnIndexOrThrow17));
                C30021Vq.A04(contentValues, "metadata", cursor.getString(columnIndexOrThrow18));
                if (!cursor.isNull(columnIndexOrThrow19)) {
                    contentValues.put("init_timestamp", Long.valueOf(cursor.getLong(columnIndexOrThrow19)));
                }
                C30021Vq.A04(contentValues, "request_key_id", cursor.getString(columnIndexOrThrow20));
                C30021Vq.A04(contentValues, "country", cursor.getString(columnIndexOrThrow21));
                contentValues.put("version", Long.valueOf(cursor.getLong(columnIndexOrThrow22)));
                C30021Vq.A06(contentValues, "future_data", cursor.getBlob(columnIndexOrThrow23));
                contentValues.put("service_id", Long.valueOf(cursor.getLong(columnIndexOrThrow24)));
                C30021Vq.A04(contentValues, "background_id", cursor.getString(columnIndexOrThrow25));
                contentValues.put("purchase_initiator", Long.valueOf(cursor.getLong(columnIndexOrThrow26)));
                A02.A03.A03(contentValues, "pay_transaction");
                i++;
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        A02.close();
        return new AnonymousClass2Ez(j, i);
    }

    @Override // X.AbstractC19010tQ
    public void onRollback() {
        C16310on A02 = this.A05.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            A02.A03.A01("pay_transaction", null, null);
            C21390xL r1 = this.A06;
            r1.A03("new_pay_transaction_ready");
            r1.A03("migration_pay_transaction_index");
            r1.A03("migration_pay_transaction_retry");
            A00.A00();
            A00.close();
            A02.close();
            Log.i(C30931Zj.A01("PaymentTransactionDatabaseMigration", "PaymentTransactionStore/resetDatabaseMigrationForDebug/done"));
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
