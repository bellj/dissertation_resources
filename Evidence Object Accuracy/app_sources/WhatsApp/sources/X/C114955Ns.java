package X;

/* renamed from: X.5Ns  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114955Ns extends AnonymousClass4X1 {
    @Override // X.AnonymousClass4X1
    public void A00(C90574Ok r4) {
        super.A00(new C90574Ok(256, r4.A01));
    }

    @Override // X.AnonymousClass4X1
    public byte[] A01() {
        byte[] A01 = super.A01();
        if (A01.length == 32) {
            A01[3] = (byte) (A01[3] & 15);
            A01[7] = (byte) (A01[7] & 15);
            A01[11] = (byte) (A01[11] & 15);
            A01[15] = (byte) (A01[15] & 15);
            A01[4] = (byte) (A01[4] & -4);
            A01[8] = (byte) (A01[8] & -4);
            A01[12] = (byte) (A01[12] & -4);
            return A01;
        }
        throw C12970iu.A0f("Poly1305 key must be 256 bits.");
    }
}
