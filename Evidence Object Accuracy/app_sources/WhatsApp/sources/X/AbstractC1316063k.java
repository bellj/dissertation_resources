package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.63k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC1316063k implements Parcelable {
    public long A00;
    public long A01;
    public String A02;
    public boolean A03;
    public final String A04;
    public final String A05;

    public abstract void A05(JSONObject jSONObject);

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AbstractC1316063k(AnonymousClass1V8 r9) {
        String A0H;
        this.A05 = r9.A0H("id");
        long A07 = r9.A07("ts") * 1000;
        this.A01 = A07;
        long j = 0;
        if (A07 <= 0) {
            this.A01 = C28421Nd.A01(C117295Zj.A0W(r9, "ts"), 0) * 1000;
        }
        this.A04 = r9.A0I("disclosure_link", "");
        if (r9.A0I("is_claimable", null) != null) {
            this.A03 = r9.A0I("is_claimable", null).equals("true");
        }
        long A08 = r9.A08("last_updated_time_usec", 0);
        this.A00 = A08 > 0 ? A08 : j;
        if (r9.A0I("initiated_on_app_type", null) == null) {
            A0H = "WA";
        } else {
            A0H = r9.A0H("initiated_on_app_type");
        }
        this.A02 = A0H;
    }

    public AbstractC1316063k(Parcel parcel) {
        this.A05 = C12990iw.A0p(parcel);
        this.A01 = parcel.readLong();
        this.A04 = C12990iw.A0p(parcel);
        this.A03 = C12960it.A1S(parcel.readInt());
        this.A00 = parcel.readLong();
        this.A02 = parcel.readString();
    }

    public AbstractC1316063k(String str) {
        JSONObject A05 = C13000ix.A05(str);
        this.A05 = A05.optString("id", "");
        this.A01 = A05.optLong("ts", 0);
        this.A04 = A05.optString("disclosure_link", "");
        this.A03 = A05.optBoolean("is_claimable", false);
        this.A00 = A05.optLong("last_updated_ts", 0);
        this.A02 = A05.optString("initiated_on_app_type", "UNSET");
    }

    public static C1316763r A01(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("refund_transaction");
        if (optJSONObject != null) {
            int A00 = C1316763r.A00(optJSONObject.optString("reason"));
            int optInt = optJSONObject.optInt("completed_timestamp_seconds");
            if (A00 != 0 && optInt > 0) {
                return new C1316763r(A00, optInt);
            }
        }
        return null;
    }

    public C30821Yy A02() {
        C1316363n r0;
        if ((this instanceof C121015h7) || (this instanceof C121005h6)) {
            r0 = ((AbstractC121025h8) this).A06;
        } else if (this instanceof C120985h4) {
            r0 = ((C120985h4) this).A00;
        } else if (!(this instanceof C120995h5)) {
            r0 = ((C121035h9) this).A00.A03;
        } else {
            r0 = ((C120995h5) this).A01.A05.A00;
        }
        return r0.A02.A01;
    }

    public String A03() {
        C1316363n r0;
        if ((this instanceof C121015h7) || (this instanceof C121005h6)) {
            r0 = ((AbstractC121025h8) this).A06;
        } else if (this instanceof C120985h4) {
            r0 = ((C120985h4) this).A00;
        } else if (!(this instanceof C120995h5)) {
            r0 = ((C121035h9) this).A00.A03;
        } else {
            r0 = ((C120995h5) this).A01.A05.A00;
        }
        return ((AbstractC30781Yu) r0.A02.A00).A04;
    }

    public JSONObject A04() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            A0a.put("id", this.A05);
            A0a.put("ts", this.A01);
            A0a.put("disclosure_link", this.A04);
            A0a.put("is_claimable", this.A03);
            A0a.put("last_updated_ts", this.A00);
            A0a.put("initiated_on_app_type", this.A02);
            A05(A0a);
            return A0a;
        } catch (JSONException unused) {
            Log.w("PAY: Transaction toJson threw exception");
            return A0a;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A05);
        parcel.writeLong(this.A01);
        parcel.writeString(this.A04);
        parcel.writeInt(this.A03 ? 1 : 0);
        parcel.writeLong(this.A00);
        parcel.writeString(this.A02);
    }
}
