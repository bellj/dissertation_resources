package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0p4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16470p4 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100234la();
    public int A00;
    public AnonymousClass1ZD A01;
    public AnonymousClass1Z6 A02;
    public AnonymousClass1Z7 A03;
    public AnonymousClass1ZC A04;
    public AnonymousClass1ZE A05;
    public String A06;
    public String A07;
    public String A08;
    public List A09;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C16470p4(AnonymousClass1Z6 r2, AnonymousClass1Z7 r3, String str, String str2, String str3) {
        this.A09 = new ArrayList();
        this.A02 = r2;
        this.A07 = str;
        this.A08 = str2;
        this.A06 = str3;
        this.A03 = r3;
        this.A00 = 5;
    }

    public C16470p4(AnonymousClass1Z6 r4, AnonymousClass1ZE r5, String str) {
        this.A09 = new ArrayList();
        this.A02 = r4;
        this.A08 = null;
        this.A07 = str;
        this.A05 = r5;
        this.A00 = 4;
    }

    public C16470p4(AnonymousClass1ZD r2, AnonymousClass1Z6 r3, AnonymousClass1Z7 r4, String str, String str2) {
        this.A09 = new ArrayList();
        this.A00 = 3;
        this.A02 = r3;
        this.A08 = str2;
        this.A07 = str;
        this.A01 = r2;
        this.A03 = r4;
    }

    @Deprecated
    public C16470p4(AnonymousClass1ZD r2, AnonymousClass1Z6 r3, AnonymousClass1ZC r4, String str, String str2, String str3, List list, int i) {
        this.A09 = new ArrayList();
        this.A02 = r3;
        this.A07 = str;
        this.A08 = str2;
        this.A06 = str3;
        this.A09 = list;
        this.A04 = r4;
        this.A01 = r2;
        this.A00 = i;
    }

    public C16470p4(Parcel parcel) {
        this.A09 = new ArrayList();
        this.A02 = (AnonymousClass1Z6) parcel.readParcelable(AnonymousClass1Z6.class.getClassLoader());
        this.A07 = parcel.readString();
        this.A08 = parcel.readString();
        this.A06 = parcel.readString();
        ArrayList arrayList = new ArrayList();
        this.A09 = arrayList;
        parcel.readList(arrayList, AnonymousClass1ZA.class.getClassLoader());
        this.A00 = parcel.readInt();
        this.A01 = (AnonymousClass1ZD) parcel.readParcelable(AnonymousClass1ZD.class.getClassLoader());
        this.A05 = (AnonymousClass1ZE) parcel.readParcelable(AnonymousClass1ZE.class.getClassLoader());
        this.A03 = (AnonymousClass1Z7) parcel.readParcelable(AnonymousClass1Z7.class.getClassLoader());
    }

    public String A00() {
        AnonymousClass1Z7 r0 = this.A03;
        if (r0 == null) {
            return null;
        }
        List list = r0.A00;
        if (list.size() == 1) {
            return ((AnonymousClass1Z9) list.get(0)).A01.A00;
        }
        return null;
    }

    public String A01() {
        AnonymousClass1Z7 r0 = this.A03;
        if (r0 == null) {
            return null;
        }
        List list = r0.A00;
        if (list.size() == 1) {
            return ((AnonymousClass1Z9) list.get(0)).A01.A01;
        }
        return null;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A07);
        parcel.writeString(this.A08);
        parcel.writeString(this.A06);
        parcel.writeList(this.A09);
        parcel.writeInt(this.A00);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A05, i);
        parcel.writeParcelable(this.A03, i);
    }
}
