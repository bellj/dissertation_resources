package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3mr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C77173mr extends AbstractC76613lv {
    public final /* synthetic */ C56062kG A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C77173mr(Timeline timeline, C56062kG r2) {
        super(timeline);
        this.A00 = r2;
    }

    @Override // X.AbstractC76613lv, com.google.android.exoplayer2.Timeline
    public C94404bl A0B(C94404bl r2, int i, long j) {
        super.A0B(r2, i, j);
        r2.A0C = true;
        return r2;
    }
}
