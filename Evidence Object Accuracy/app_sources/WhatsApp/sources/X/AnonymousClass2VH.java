package X;

import java.util.Arrays;

/* renamed from: X.2VH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2VH implements Comparable {
    public final long A00;
    public final long A01;
    public final long A02;
    public final C15570nT A03;
    public final AbstractC14640lm A04;
    public final String A05;

    public AnonymousClass2VH(C15570nT r1, AbstractC14640lm r2, String str, long j, long j2, long j3) {
        this.A03 = r1;
        this.A04 = r2;
        this.A05 = str;
        this.A01 = j;
        this.A02 = j2;
        this.A00 = j3;
    }

    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        AnonymousClass2VH r8 = (AnonymousClass2VH) obj;
        C15570nT r0 = this.A03;
        AbstractC14640lm r6 = this.A04;
        boolean A0F = r0.A0F(r6);
        AbstractC14640lm r5 = r8.A04;
        if (A0F != r0.A0F(r5)) {
            return A0F ? 1 : -1;
        }
        int i = (this.A02 > r8.A02 ? 1 : (this.A02 == r8.A02 ? 0 : -1));
        if (i != 0) {
            return i;
        }
        int compareTo = r6.getRawString().compareTo(r5.getRawString());
        if (compareTo == 0) {
            return (this.A00 > r8.A00 ? 1 : (this.A00 == r8.A00 ? 0 : -1));
        }
        return compareTo;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass2VH)) {
            return false;
        }
        AnonymousClass2VH r7 = (AnonymousClass2VH) obj;
        if (this.A01 == r7.A01 && this.A02 == r7.A02 && this.A00 == r7.A00 && this.A04.equals(r7.A04) && C29941Vi.A00(this.A05, r7.A05)) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A04, this.A05, Long.valueOf(this.A01), Long.valueOf(this.A02), Long.valueOf(this.A00)});
    }
}
