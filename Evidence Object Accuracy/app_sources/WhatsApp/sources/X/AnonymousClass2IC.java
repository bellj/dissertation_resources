package X;

import android.os.Build;
import android.view.View;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.audioRecording.AudioRecordFactory;
import com.whatsapp.audioRecording.OpusRecorderFactory;
import com.whatsapp.notification.PopupNotification;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.2IC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2IC {
    public final AbstractC15710nm A00;
    public final AnonymousClass2I9 A01;
    public final AnonymousClass2IB A02;
    public final C14330lG A03;
    public final C14900mE A04;
    public final C15450nH A05;
    public final C16170oZ A06;
    public final AudioRecordFactory A07;
    public final OpusRecorderFactory A08;
    public final C18280sC A09;
    public final AnonymousClass11P A0A;
    public final AnonymousClass2IA A0B;
    public final AnonymousClass01d A0C;
    public final C14830m7 A0D;
    public final C14820m6 A0E;
    public final AnonymousClass018 A0F;
    public final C21310xD A0G;
    public final C14850m9 A0H;
    public final C22050yP A0I;
    public final C14410lO A0J;
    public final AnonymousClass109 A0K;
    public final C16630pM A0L;
    public final C21840y4 A0M;
    public final C20320vZ A0N;
    public final AnonymousClass1AL A0O;
    public final AnonymousClass199 A0P;
    public final C255819y A0Q;
    public final AbstractC14440lR A0R;
    public final C17020q8 A0S;
    public final C254419k A0T;
    public final AnonymousClass19A A0U;
    public final C21260x8 A0V;

    public AnonymousClass2IC(AbstractC15710nm r2, AnonymousClass2I9 r3, AnonymousClass2IB r4, C14330lG r5, C14900mE r6, C15450nH r7, C16170oZ r8, AudioRecordFactory audioRecordFactory, OpusRecorderFactory opusRecorderFactory, C18280sC r11, AnonymousClass11P r12, AnonymousClass2IA r13, AnonymousClass01d r14, C14830m7 r15, C14820m6 r16, AnonymousClass018 r17, C21310xD r18, C14850m9 r19, C22050yP r20, C14410lO r21, AnonymousClass109 r22, C16630pM r23, C21840y4 r24, C20320vZ r25, AnonymousClass1AL r26, AnonymousClass199 r27, C255819y r28, AbstractC14440lR r29, C17020q8 r30, C254419k r31, AnonymousClass19A r32, C21260x8 r33) {
        this.A0D = r15;
        this.A0H = r19;
        this.A04 = r6;
        this.A0O = r26;
        this.A00 = r2;
        this.A0R = r29;
        this.A03 = r5;
        this.A0P = r27;
        this.A05 = r7;
        this.A06 = r8;
        this.A0J = r21;
        this.A0V = r33;
        this.A0T = r31;
        this.A0C = r14;
        this.A0F = r17;
        this.A0N = r25;
        this.A0I = r20;
        this.A0M = r24;
        this.A07 = audioRecordFactory;
        this.A0G = r18;
        this.A0U = r32;
        this.A0E = r16;
        this.A08 = opusRecorderFactory;
        this.A09 = r11;
        this.A0S = r30;
        this.A0K = r22;
        this.A0L = r23;
        this.A0Q = r28;
        this.A01 = r3;
        this.A0A = r12;
        this.A0B = r13;
        this.A02 = r4;
    }

    public AbstractC14670lq A00(View view, C14960mK r59, MessageReplyActivity messageReplyActivity) {
        C14680lr A00 = this.A01.A00(messageReplyActivity, view);
        C14830m7 r0 = this.A0D;
        C14850m9 r02 = this.A0H;
        C14900mE r03 = this.A04;
        AnonymousClass1AL r04 = this.A0O;
        AbstractC15710nm r05 = this.A00;
        AbstractC14440lR r06 = this.A0R;
        C14330lG r07 = this.A03;
        AnonymousClass199 r08 = this.A0P;
        C15450nH r09 = this.A05;
        C16170oZ r010 = this.A06;
        C14410lO r011 = this.A0J;
        C21260x8 r012 = this.A0V;
        C254419k r013 = this.A0T;
        AnonymousClass01d r014 = this.A0C;
        AnonymousClass018 r015 = this.A0F;
        C20320vZ r016 = this.A0N;
        C22050yP r017 = this.A0I;
        AudioRecordFactory audioRecordFactory = this.A07;
        C21310xD r15 = this.A0G;
        AnonymousClass19A r14 = this.A0U;
        C14820m6 r10 = this.A0E;
        OpusRecorderFactory opusRecorderFactory = this.A08;
        C18280sC r8 = this.A09;
        C17020q8 r7 = this.A0S;
        AnonymousClass109 r6 = this.A0K;
        C16630pM r5 = this.A0L;
        C255819y r4 = this.A0Q;
        AnonymousClass11P r3 = this.A0A;
        AnonymousClass2IA r2 = this.A0B;
        C63683Cn r1 = new C63683Cn(messageReplyActivity, (C15890o4) this.A02.A00.A03.AN1.get(), r59);
        boolean z = false;
        if (Build.VERSION.SDK_INT != 26) {
            z = true;
        }
        return new AnonymousClass39J(view, messageReplyActivity, r05, messageReplyActivity, r07, r03, r09, r010, audioRecordFactory, opusRecorderFactory, r8, r3, r2, r014, r0, r10, r015, r15, r02, r017, r011, r6, r5, r016, r04, messageReplyActivity, r08, r4, r06, r7, r013, A00, r14, r1, this, r012, z);
    }

    public AbstractC14670lq A01(KeyboardPopupLayout keyboardPopupLayout, C14960mK r59, PopupNotification popupNotification) {
        C14680lr A00 = this.A01.A00(popupNotification, keyboardPopupLayout);
        C14830m7 r0 = this.A0D;
        C14850m9 r02 = this.A0H;
        C14900mE r03 = this.A04;
        AnonymousClass1AL r04 = this.A0O;
        AbstractC15710nm r05 = this.A00;
        AbstractC14440lR r06 = this.A0R;
        C14330lG r07 = this.A03;
        AnonymousClass199 r08 = this.A0P;
        C15450nH r09 = this.A05;
        C16170oZ r010 = this.A06;
        C14410lO r011 = this.A0J;
        C21260x8 r012 = this.A0V;
        C254419k r013 = this.A0T;
        AnonymousClass01d r014 = this.A0C;
        AnonymousClass018 r015 = this.A0F;
        C20320vZ r016 = this.A0N;
        C22050yP r017 = this.A0I;
        AudioRecordFactory audioRecordFactory = this.A07;
        C21310xD r15 = this.A0G;
        AnonymousClass19A r14 = this.A0U;
        C14820m6 r10 = this.A0E;
        OpusRecorderFactory opusRecorderFactory = this.A08;
        C18280sC r8 = this.A09;
        C17020q8 r7 = this.A0S;
        AnonymousClass109 r6 = this.A0K;
        C16630pM r5 = this.A0L;
        C255819y r4 = this.A0Q;
        AnonymousClass11P r3 = this.A0A;
        AnonymousClass2IA r2 = this.A0B;
        C63683Cn r1 = new C63683Cn(popupNotification, (C15890o4) this.A02.A00.A03.AN1.get(), r59);
        boolean z = false;
        if (Build.VERSION.SDK_INT != 26) {
            z = true;
        }
        return new AnonymousClass39K(keyboardPopupLayout, popupNotification, r05, popupNotification, r07, r03, r09, r010, audioRecordFactory, opusRecorderFactory, r8, r3, r2, r014, r0, r10, r015, r15, r02, r017, r011, r6, popupNotification, r5, r016, r04, r08, r4, r06, r7, r013, A00, r14, r1, this, r012, z);
    }
}
