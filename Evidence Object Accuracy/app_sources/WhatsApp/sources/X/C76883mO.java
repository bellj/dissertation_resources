package X;

import java.util.Arrays;
import java.util.List;

/* renamed from: X.3mO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76883mO extends AbstractC93864as {
    public final long A00;
    public final List A01 = C12960it.A0l();
    public final List A02 = C12960it.A0l();

    public C76883mO(int i, long j) {
        super(i);
        this.A00 = j;
    }

    public C76883mO A00(int i) {
        List list = this.A01;
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            C76883mO r1 = (C76883mO) list.get(i2);
            if (((AbstractC93864as) r1).A00 == i) {
                return r1;
            }
        }
        return null;
    }

    public C76873mN A01(int i) {
        List list = this.A02;
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            C76873mN r1 = (C76873mN) list.get(i2);
            if (((AbstractC93864as) r1).A00 == i) {
                return r1;
            }
        }
        return null;
    }

    @Override // X.AbstractC93864as
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(AbstractC93864as.A00(super.A00));
        A0h.append(" leaves: ");
        A0h.append(Arrays.toString(this.A02.toArray()));
        A0h.append(" containers: ");
        return C12960it.A0d(Arrays.toString(this.A01.toArray()), A0h);
    }
}
