package X;

import android.content.ContentValues;
import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.15A  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15A {
    public final C240413z A00;

    public AnonymousClass15A(C240413z r1) {
        this.A00 = r1;
    }

    public synchronized int A00(String str) {
        C16310on A02 = this.A00.A02();
        AnonymousClass1Lx A00 = A02.A00();
        C16330op r10 = A02.A03;
        Cursor A08 = r10.A08("sticker_pack_order", "sticker_pack_id = ?", null, null, new String[]{"pack_order"}, new String[]{str});
        try {
            if (A08.getCount() <= 0 || !A08.moveToFirst()) {
                A08.close();
                int i = 1000;
                A08 = r10.A09("SELECT MAX(pack_order) FROM sticker_pack_order", null);
                try {
                    if (A08.getCount() <= 0 || !A08.moveToFirst()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("StickerPackOderDBTableHelper/getOrAddStickerPackOrder/max order is not available for sticker pack: ");
                        sb.append(str);
                        Log.e(sb.toString());
                    } else {
                        i = A08.getInt(0);
                    }
                    A08.close();
                    int i2 = i + 1;
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("sticker_pack_id", str);
                    contentValues.put("pack_order", Integer.valueOf(i2));
                    r10.A03(contentValues, "sticker_pack_order");
                    A00.A00();
                    A00.close();
                    A02.close();
                    return i2;
                } finally {
                }
            } else {
                int i3 = A08.getInt(A08.getColumnIndexOrThrow("pack_order"));
                A08.close();
                A00.close();
                A02.close();
                return i3;
            }
        } finally {
        }
    }
}
