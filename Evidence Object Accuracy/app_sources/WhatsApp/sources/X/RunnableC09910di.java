package X;

import androidx.sharetarget.ShortcutInfoCompatSaverImpl;

/* renamed from: X.0di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09910di implements Runnable {
    public final /* synthetic */ C02510Co A00;
    public final /* synthetic */ ShortcutInfoCompatSaverImpl A01;
    public final /* synthetic */ Runnable A02;

    public RunnableC09910di(C02510Co r1, ShortcutInfoCompatSaverImpl shortcutInfoCompatSaverImpl, Runnable runnable) {
        this.A01 = shortcutInfoCompatSaverImpl;
        this.A00 = r1;
        this.A02 = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        C02510Co r1 = this.A00;
        if (!r1.isCancelled()) {
            try {
                this.A02.run();
                r1.A07(null);
            } catch (Exception e) {
                r1.A06(e);
            }
        }
    }
}
