package X;

import java.util.concurrent.locks.LockSupport;

/* renamed from: X.5Kt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC114245Kt extends AbstractC114165Kl {
    public abstract Thread A0B();

    public final void A0C() {
        Thread A0B = A0B();
        if (Thread.currentThread() != A0B) {
            LockSupport.unpark(A0B);
        }
    }
}
