package X;

import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.5oc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124105oc extends AbstractC16350or {
    public UserJid A00;
    public final C20730wE A01;
    public final C17070qD A02;

    public C124105oc(C20730wE r1, UserJid userJid, C17070qD r3) {
        this.A02 = r3;
        this.A01 = r1;
        this.A00 = userJid;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        ArrayList A0l = C12960it.A0l();
        UserJid userJid = this.A00;
        if (userJid != null) {
            A0l.add(userJid);
            if (!this.A01.A00(AnonymousClass1JB.A0G, AnonymousClass1JA.A0B, A0l).A00()) {
                return Boolean.FALSE;
            }
            Iterator it = A0l.iterator();
            while (it.hasNext()) {
                C17070qD r0 = this.A02;
                r0.A03();
                r0.A09.A0H((UserJid) it.next());
            }
            return Boolean.TRUE;
        }
        throw C12980iv.A0n("getAllIndividualContacts");
    }
}
