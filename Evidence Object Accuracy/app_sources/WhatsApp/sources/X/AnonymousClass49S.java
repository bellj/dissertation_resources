package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49S  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49S extends Enum {
    public static final AnonymousClass49S A00 = new AnonymousClass49S("ADDITIONAL_PARAMS", "additional_params", 2);
    public static final AnonymousClass49S A01 = new AnonymousClass49S("REFFERAL", "referral", 0);
    public static final AnonymousClass49S A02 = new AnonymousClass49S("SESSION_ID", "session_id", 1);
    public final String key;

    public AnonymousClass49S(String str, String str2, int i) {
        this.key = str2;
    }
}
