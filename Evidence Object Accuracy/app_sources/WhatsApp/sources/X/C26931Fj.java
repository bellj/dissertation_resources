package X;

import android.os.Message;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/* renamed from: X.1Fj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26931Fj implements AbstractC15920o8 {
    public final AnonymousClass12O A00;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{216};
    }

    public C26931Fj(AnonymousClass12O r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        String str;
        int i2;
        int i3;
        String str2;
        if (i != 216) {
            return false;
        }
        List<C43831xf> list = (List) message.obj;
        AnonymousClass12O r3 = this.A00;
        StringBuilder sb = new StringBuilder("UserNoticeManager/onUserNoticeListReceived/serverUserNoticeList size: ");
        sb.append(list.size());
        Log.i(sb.toString());
        AnonymousClass12N r2 = r3.A08;
        TreeMap A02 = r2.A02();
        ArrayList arrayList = new ArrayList();
        for (C43831xf r10 : list) {
            C43831xf r7 = (C43831xf) A02.get(Integer.valueOf(r10.A00));
            if (r7 == null) {
                str2 = "UserNoticeManager/getUpdatedUserNoticeList/new notice";
            } else {
                int i4 = r7.A01;
                int i5 = r10.A01;
                if (i4 < i5) {
                    StringBuilder sb2 = new StringBuilder("UserNoticeManager/getUpdatedUserNoticeList/client stage is stale. client stage: ");
                    sb2.append(i4);
                    sb2.append(" sever stage: ");
                    sb2.append(i5);
                    str2 = sb2.toString();
                } else {
                    int i6 = r7.A02;
                    int i7 = r10.A02;
                    if (i6 < i7) {
                        StringBuilder sb3 = new StringBuilder("UserNoticeManager/getUpdatedUserNoticeList/new version available. client version: ");
                        sb3.append(i6);
                        sb3.append(" sever version: ");
                        sb3.append(i7);
                        Log.i(sb3.toString());
                        arrayList.add(new C43831xf(r7.A00, i4, i7, r7.A03));
                    } else {
                        StringBuilder sb4 = new StringBuilder("UserNoticeManager/getUpdatedUserNoticeList/server stage is same or stale. client stage: ");
                        sb4.append(i4);
                        sb4.append(" sever stage: ");
                        sb4.append(i5);
                        Log.i(sb4.toString());
                        arrayList.add(r7);
                    }
                }
            }
            Log.i(str2);
            arrayList.add(r10);
        }
        StringBuilder sb5 = new StringBuilder("UserNoticeManager/onUserNoticeListReceived/updatedUserNoticeList size: ");
        sb5.append(arrayList.size());
        Log.i(sb5.toString());
        r2.A04(arrayList);
        C43831xf A01 = r2.A01();
        TreeMap A022 = r2.A02();
        if (A022.isEmpty()) {
            Log.i("UserNoticeManager/getUpdatedCurrentUserNotice/notice map empty");
        } else {
            Log.i("UserNoticeManager/getUpdatedCurrentUserNotice/found metadata");
            Map.Entry firstEntry = A022.firstEntry();
            AnonymousClass009.A05(firstEntry);
            C43831xf r72 = (C43831xf) firstEntry.getValue();
            if (r72 != null) {
                r2.A03(r72);
                if (A01 != null && ((i2 = A01.A00) != (i3 = r72.A00) || A01.A02 < r72.A02)) {
                    StringBuilder sb6 = new StringBuilder("UserNoticeManager/deleteUserNoticeContentIfNecessary/notice mismatch: ");
                    boolean z = true;
                    boolean z2 = false;
                    if (i2 != i3) {
                        z2 = true;
                    }
                    sb6.append(z2);
                    sb6.append(" old version: ");
                    if (A01.A02 >= r72.A02) {
                        z = false;
                    }
                    sb6.append(z);
                    Log.i(sb6.toString());
                    r3.A07.A04(i2);
                    r3.A06();
                }
                AnonymousClass12M r8 = r3.A07;
                int i8 = r72.A02;
                int i9 = r72.A00;
                int i10 = r72.A01;
                StringBuilder sb7 = new StringBuilder("UserNoticeContentManager/fetchUserNoticeContentIfNecessary/notice id ");
                sb7.append(i9);
                sb7.append(" version: ");
                sb7.append(i8);
                sb7.append(" stage: ");
                sb7.append(i10);
                Log.i(sb7.toString());
                if (i10 == 5) {
                    str = "UserNoticeContentManager/fetchUserNoticeContentIfNecessary/end stage, skip fetch";
                } else {
                    C14850m9 r1 = r8.A05;
                    if (C43901xo.A00(r1, i9)) {
                        str = "UserNoticeContentManager/fetchUserNoticeContentIfNecessary/green alert disabled";
                    } else {
                        if (!C43901xo.A01(r1, r72) && !r8.A09(new String[]{"content.json"}, i9)) {
                            Log.i("UserNoticeContentManager/fetchUserNoticeContentIfNecessary/content does not exist, fetch");
                            r8.A05(i9);
                        }
                        r3.A08(i9, i10, i8);
                        r3.A09(r8.A03(r72), r72);
                        return true;
                    }
                }
                Log.i(str);
                r3.A08(i9, i10, i8);
                r3.A09(r8.A03(r72), r72);
                return true;
            }
        }
        r3.A03();
        r3.A05();
        return true;
    }
}
