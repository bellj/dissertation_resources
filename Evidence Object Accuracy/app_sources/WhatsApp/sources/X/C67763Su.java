package X;

import android.net.Uri;
import java.util.Map;

/* renamed from: X.3Su  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67763Su implements AnonymousClass2BW {
    public int A00;
    public final int A01;
    public final AnonymousClass5Q9 A02;
    public final AnonymousClass2BW A03;
    public final byte[] A04 = new byte[1];

    public C67763Su(AnonymousClass5Q9 r3, AnonymousClass2BW r4, int i) {
        C95314dV.A03(C12960it.A1U(i));
        this.A03 = r4;
        this.A01 = i;
        this.A02 = r3;
        this.A00 = i;
    }

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r2) {
        this.A03.A5p(r2);
    }

    @Override // X.AnonymousClass2BW
    public Map AGF() {
        return this.A03.AGF();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        return this.A03.AHS();
    }

    @Override // X.AnonymousClass2BW
    public long AYZ(AnonymousClass3H3 r2) {
        throw C12970iu.A0z();
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        throw C12970iu.A0z();
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        long max;
        int i3 = this.A00;
        if (i3 == 0) {
            AnonymousClass2BW r6 = this.A03;
            byte[] bArr2 = this.A04;
            int i4 = 0;
            if (r6.read(bArr2, 0, 1) != -1) {
                int i5 = (bArr2[0] & 255) << 4;
                if (i5 != 0) {
                    byte[] bArr3 = new byte[i5];
                    int i6 = i5;
                    while (i6 > 0) {
                        int read = r6.read(bArr3, i4, i6);
                        if (read != -1) {
                            i4 += read;
                            i6 -= read;
                        }
                    }
                    while (true) {
                        if (i5 <= 0) {
                            break;
                        }
                        int i7 = i5 - 1;
                        if (bArr3[i7] == 0) {
                            i5 = i7;
                        } else {
                            AnonymousClass5Q9 r4 = this.A02;
                            C95304dT r62 = new C95304dT(bArr3, i5);
                            C67803Sy r42 = (C67803Sy) r4;
                            if (!r42.A05) {
                                max = r42.A01;
                            } else {
                                max = Math.max(r42.A0E.A00(), r42.A01);
                            }
                            int i8 = r62.A00 - r62.A01;
                            AnonymousClass5X6 r7 = r42.A02;
                            r7.AbC(r62, i8);
                            r7.AbG(null, 1, i8, 0, max);
                            r42.A05 = true;
                        }
                    }
                }
                i3 = this.A01;
                this.A00 = i3;
            }
            return -1;
        }
        int read2 = this.A03.read(bArr, i, Math.min(i3, i2));
        if (read2 != -1) {
            this.A00 -= read2;
        }
        return read2;
    }
}
