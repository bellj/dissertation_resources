package X;

import android.util.Base64;

/* renamed from: X.1o8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38461o8 {
    public byte[] A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final String A05;
    public final String A06;
    public final String A07;
    public final String A08;
    public final String A09;

    public C38461o8(String str, String str2, String str3, String str4, String str5, String str6, byte[] bArr, int i, int i2, int i3) {
        this.A07 = str;
        this.A06 = str2;
        this.A00 = bArr;
        this.A08 = str3;
        this.A09 = str4;
        this.A04 = str5;
        this.A05 = str6;
        this.A01 = i;
        this.A03 = i2;
        this.A02 = i3;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("WebUploadMediaData{");
        stringBuffer.append("mediaId='");
        stringBuffer.append(this.A07);
        stringBuffer.append('\'');
        stringBuffer.append(", fileHash='");
        stringBuffer.append(this.A06);
        stringBuffer.append('\'');
        stringBuffer.append(", mimeType='");
        stringBuffer.append(this.A08);
        stringBuffer.append('\'');
        stringBuffer.append(", url='");
        stringBuffer.append(this.A09);
        stringBuffer.append('\'');
        stringBuffer.append(", directPath='");
        stringBuffer.append(this.A04);
        stringBuffer.append('\'');
        stringBuffer.append(", encFileHash='");
        stringBuffer.append(this.A05);
        stringBuffer.append('\'');
        stringBuffer.append(", fileSize=");
        stringBuffer.append(this.A01);
        stringBuffer.append(", width=");
        stringBuffer.append(this.A03);
        stringBuffer.append(", height=");
        stringBuffer.append(this.A02);
        stringBuffer.append(", mediaKey=");
        stringBuffer.append(Base64.encodeToString(this.A00, 0));
        stringBuffer.append('}');
        return stringBuffer.toString();
    }
}
