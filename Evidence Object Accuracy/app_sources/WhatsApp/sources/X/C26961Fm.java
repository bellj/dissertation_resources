package X;

/* renamed from: X.1Fm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26961Fm implements AbstractC15920o8 {
    public final C18350sJ A00;

    public C26961Fm(C18350sJ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{198, 199, 200, 201};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005e, code lost:
        if (r2 < 500) goto L_0x0060;
     */
    @Override // X.AbstractC15920o8
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean AI8(android.os.Message r8, int r9) {
        /*
            r7 = this;
            r3 = 1
            switch(r9) {
                case 198: goto L_0x0006;
                case 199: goto L_0x0022;
                case 200: goto L_0x0092;
                case 201: goto L_0x003c;
                default: goto L_0x0004;
            }
        L_0x0004:
            r0 = 0
            return r0
        L_0x0006:
            X.0sJ r0 = r7.A00
            java.lang.Object r2 = r8.obj
            java.lang.String r2 = (java.lang.String) r2
            java.util.List r0 = r0.A0p
            java.util.Iterator r1 = r0.iterator()
        L_0x0012:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0097
            java.lang.Object r0 = r1.next()
            X.2SS r0 = (X.AnonymousClass2SS) r0
            r0.AT8(r2)
            goto L_0x0012
        L_0x0022:
            X.0sJ r0 = r7.A00
            int r2 = r8.arg2
            java.util.List r0 = r0.A0p
            java.util.Iterator r1 = r0.iterator()
        L_0x002c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0097
            java.lang.Object r0 = r1.next()
            X.2SS r0 = (X.AnonymousClass2SS) r0
            r0.AT7(r2)
            goto L_0x002c
        L_0x003c:
            X.0sJ r4 = r7.A00
            int r2 = r8.arg2
            X.0nT r5 = r4.A05
            com.whatsapp.Me r1 = r5.A01()
            X.0pI r0 = r4.A0L
            android.content.Context r6 = r0.A00
            if (r1 == 0) goto L_0x009c
            r0 = 400(0x190, float:5.6E-43)
            if (r2 == r0) goto L_0x0080
            r0 = 401(0x191, float:5.62E-43)
            if (r2 == r0) goto L_0x0073
            r0 = 405(0x195, float:5.68E-43)
            if (r2 == r0) goto L_0x0098
            r0 = 409(0x199, float:5.73E-43)
            if (r2 == r0) goto L_0x0097
            r0 = 500(0x1f4, float:7.0E-43)
            if (r2 >= r0) goto L_0x0097
        L_0x0060:
            X.0m6 r0 = r4.A0N
            r0.A1A(r3)
            X.0zr r0 = r4.A06
            r0.A01()
        L_0x006a:
            r5.A07()
            X.0zU r0 = r4.A02
            r0.A00()
            return r3
        L_0x0073:
            android.os.Handler r2 = r4.A00
            r1 = 33
            com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7 r0 = new com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7
            r0.<init>(r4, r1, r6)
            r2.post(r0)
            goto L_0x0060
        L_0x0080:
            java.lang.String r0 = "registrationmanager/check-number/match"
            com.whatsapp.util.Log.w(r0)
            android.os.Handler r2 = r4.A00
            r1 = 32
            com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7 r0 = new com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7
            r0.<init>(r4, r1, r6)
            r2.post(r0)
            goto L_0x006a
        L_0x0092:
            X.0sJ r0 = r7.A00
            r0.A05()
        L_0x0097:
            return r3
        L_0x0098:
            r4.A05()
            return r3
        L_0x009c:
            java.lang.String r0 = "registrationmanager/response/error but already changed"
            com.whatsapp.util.Log.w(r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C26961Fm.AI8(android.os.Message, int):boolean");
    }
}
