package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.AbstractList;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.List;
import org.chromium.net.UrlRequest;
import sun.misc.Unsafe;

/* renamed from: X.4zS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108664zS implements AnonymousClass5XT {
    public static final Unsafe A0F = C95624e5.A03();
    public final int A00;
    public final int A01;
    public final int A02;
    public final AnonymousClass4UP A03;
    public final AbstractC94174bN A04;
    public final AbstractC115615Sg A05;
    public final AbstractC117125Yn A06;
    public final AbstractC115155Qk A07;
    public final AnonymousClass4DR A08;
    public final boolean A09;
    public final int[] A0A;
    public final int[] A0B;
    public final int[] A0C;
    public final int[] A0D;
    public final Object[] A0E;

    public static int A01(AnonymousClass4PN r2, AnonymousClass5XT r3, byte[] bArr, int i, int i2, int i3) {
        C108664zS r1 = (C108664zS) r3;
        Object ALd = r1.ALd();
        int A07 = r1.A07(r2, ALd, bArr, i, i2, i3);
        r1.AhF(ALd);
        r2.A02 = ALd;
        return A07;
    }

    public final int A06(int i) {
        int i2;
        int i3 = this.A00;
        if (i >= i3) {
            int i4 = this.A02;
            if (i < i4) {
                i2 = (i - i3) << 2;
                if (this.A0A[i2] == i) {
                    return i2;
                }
            } else if (i <= this.A01) {
                int i5 = i4 - i3;
                int[] iArr = this.A0A;
                int length = (iArr.length >> 2) - 1;
                while (i5 <= length) {
                    int i6 = (length + i5) >>> 1;
                    i2 = i6 << 2;
                    int i7 = iArr[i2];
                    if (i == i7) {
                        return i2;
                    }
                    if (i < i7) {
                        length = i6 - 1;
                    } else {
                        i5 = i6 + 1;
                    }
                }
            }
        }
        return -1;
    }

    public C108664zS(AnonymousClass4UP r1, AbstractC94174bN r2, AbstractC115615Sg r3, AbstractC117125Yn r4, AbstractC115155Qk r5, AnonymousClass4DR r6, int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4, Object[] objArr, int i, int i2, int i3, boolean z) {
        this.A0A = iArr;
        this.A0E = objArr;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = i3;
        this.A09 = z;
        this.A0B = iArr2;
        this.A0C = iArr3;
        this.A0D = iArr4;
        this.A07 = r5;
        this.A04 = r2;
        this.A08 = r6;
        this.A03 = r1;
        this.A06 = r4;
        this.A05 = r3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r5 >= 0) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(X.AnonymousClass4PN r6, X.AnonymousClass5XT r7, byte[] r8, int r9, int r10) {
        /*
            int r4 = r9 + 1
            r3 = r8
            byte r5 = r8[r9]
            r1 = r6
            if (r5 >= 0) goto L_0x0010
            int r4 = X.C95504dq.A04(r6, r8, r5, r4)
            int r5 = r6.A00
            if (r5 < 0) goto L_0x0022
        L_0x0010:
            int r10 = r10 - r4
            if (r5 > r10) goto L_0x0022
            r0 = r7
            java.lang.Object r2 = r7.ALd()
            int r5 = r5 + r4
            r0.Agx(r1, r2, r3, r4, r5)
            r7.AhF(r2)
            r6.A02 = r2
            return r5
        L_0x0022:
            X.48x r0 = X.C868048x.A00()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.A00(X.4PN, X.5XT, byte[], int, int):int");
    }

    public static int A02(AnonymousClass4PN r5, byte[] bArr, int i) {
        int A01 = C95504dq.A01(r5, bArr, i);
        int i2 = r5.A00;
        if (i2 == 0) {
            r5.A02 = "";
            return A01;
        }
        int i3 = A01 + i2;
        if (C12960it.A1T(C95164dF.A00.A02(bArr, A01, i3))) {
            r5.A02 = new String(bArr, A01, i2, C93094Zb.A03);
            return i3;
        }
        throw new C868048x("Protocol message had invalid UTF-8.");
    }

    public static Object A03(Object obj, int i) {
        return C95624e5.A01(obj, (long) (i & 1048575));
    }

    public static List A04(int i, Object obj) {
        return (List) C95624e5.A01(obj, (long) (i & 1048575));
    }

    public static void A05(C108684zU r2, AnonymousClass5XT r3, Object obj, int i) {
        AbstractC79223qF r22 = r2.A00;
        int i2 = i << 3;
        r22.A05(3 | i2);
        r3.Agw(r22.A00, obj);
        r22.A05(i2 | 4);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:168:0x000a */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:137:0x026e */
    public final int A07(AnonymousClass4PN r38, Object obj, byte[] bArr, int i, int i2, int i3) {
        int i4;
        Object A00;
        Object A002;
        int i5 = i;
        Unsafe unsafe = A0F;
        int i6 = -1;
        int i7 = 0;
        int i8 = 0;
        int i9 = -1;
        while (true) {
            i4 = i5;
            if (i5 < i2) {
                i4 = i5 + 1;
                byte b = bArr[i5];
                i7 = b;
                if (b < 0) {
                    i4 = C95504dq.A04(r38, bArr, b, i4);
                    i7 = r38.A00;
                }
                int i10 = (i7 == 1 ? 1 : 0) >>> 3;
                int i11 = (i7 == 1 ? 1 : 0) & 7;
                int A06 = A06(i10);
                if (A06 != i6) {
                    int[] iArr = this.A0A;
                    int i12 = iArr[A06 + 1];
                    int A01 = C72463ee.A01(i12);
                    long j = (long) (i12 & 1048575);
                    if (A01 <= 17) {
                        int i13 = iArr[A06 + 2];
                        int i14 = 1 << (i13 >>> 20);
                        int i15 = i13 & 1048575;
                        if (i15 != i9) {
                            if (i9 != -1) {
                                unsafe.putInt(obj, (long) i9, i8);
                            }
                            i8 = unsafe.getInt(obj, (long) i15);
                            i9 = i15;
                        }
                        switch (A01) {
                            case 0:
                                if (i11 == 1) {
                                    C95624e5.A02.A09(obj, j, Double.longBitsToDouble(C72453ed.A0b(bArr, i4)));
                                    i5 = i4 + 8;
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 1:
                                if (i11 == 5) {
                                    C95624e5.A02.A0A(obj, j, Float.intBitsToFloat(C72453ed.A0K(bArr, i4)));
                                    i5 = i4 + 4;
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                if (i11 == 0) {
                                    i5 = C95504dq.A02(r38, bArr, i4);
                                    unsafe.putLong(obj, j, r38.A01);
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                if (i11 == 0) {
                                    i5 = C95504dq.A01(r38, bArr, i4);
                                    unsafe.putInt(obj, j, r38.A00);
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 5:
                            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                if (i11 == 1) {
                                    unsafe.putLong(obj, j, C72453ed.A0b(bArr, i4));
                                    i5 = i4 + 8;
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 6:
                            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                if (i11 == 5) {
                                    unsafe.putInt(obj, j, C72453ed.A0K(bArr, i4));
                                    i5 = i4 + 4;
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 7:
                                if (i11 == 0) {
                                    i5 = C95504dq.A02(r38, bArr, i4);
                                    C95624e5.A02.A0D(obj, j, C12960it.A1S((r38.A01 > 0 ? 1 : (r38.A01 == 0 ? 0 : -1))));
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 8:
                                if (i11 == 2) {
                                    if ((i12 & 536870912) == 0) {
                                        i5 = C95504dq.A01(r38, bArr, i4);
                                        int i16 = r38.A00;
                                        if (i16 == 0) {
                                            r38.A02 = "";
                                        } else {
                                            r38.A02 = new String(bArr, i5, i16, C93094Zb.A03);
                                            i5 += i16;
                                        }
                                    } else {
                                        i5 = A02(r38, bArr, i4);
                                    }
                                    A002 = r38.A02;
                                    unsafe.putObject(obj, j, A002);
                                    i8 |= i14;
                                    i6 = -1;
                                    break;
                                }
                                break;
                            case 9:
                                if (i11 == 2) {
                                    i5 = A00(r38, A0A(A06), bArr, i4, i2);
                                    if ((i8 & i14) != 0) {
                                        A002 = C93094Zb.A00(unsafe.getObject(obj, j), r38.A02);
                                        unsafe.putObject(obj, j, A002);
                                        i8 |= i14;
                                        i6 = -1;
                                        break;
                                    }
                                    A002 = r38.A02;
                                    unsafe.putObject(obj, j, A002);
                                    i8 |= i14;
                                    i6 = -1;
                                }
                                break;
                            case 10:
                                i6 = -1;
                                if (i11 == 2) {
                                    i5 = C95504dq.A03(r38, bArr, i4);
                                    A00 = r38.A02;
                                    unsafe.putObject(obj, j, A00);
                                    i8 |= i14;
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                i6 = -1;
                                if (i11 == 0) {
                                    i5 = C95504dq.A01(r38, bArr, i4);
                                    int i17 = r38.A00;
                                    AbstractC115605Sf r3 = (AbstractC115605Sf) this.A0E[((A06 >> 2) << 1) + 1];
                                    if (r3 == null || r3.Ah4(i17) != null) {
                                        unsafe.putInt(obj, j, i17);
                                        i8 |= i14;
                                        break;
                                    } else {
                                        AbstractC108624zO.A06(obj).A01(i7, Long.valueOf((long) i17));
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            case 15:
                                i6 = -1;
                                if (i11 == 0) {
                                    i5 = C95504dq.A01(r38, bArr, i4);
                                    int i18 = r38.A00;
                                    unsafe.putInt(obj, j, (-(i18 & 1)) ^ (i18 >>> 1));
                                    i8 |= i14;
                                    break;
                                } else {
                                    break;
                                }
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                i6 = -1;
                                if (i11 == 0) {
                                    i5 = C95504dq.A02(r38, bArr, i4);
                                    unsafe.putLong(obj, j, C72453ed.A0U(r38.A01));
                                    i8 |= i14;
                                    break;
                                } else {
                                    break;
                                }
                            case 17:
                                if (i11 != 3) {
                                    break;
                                } else {
                                    i6 = -1;
                                    i5 = A01(r38, A0A(A06), bArr, i4, i2, (i10 << 3) | 4);
                                    if ((i8 & i14) != 0) {
                                        A00 = C93094Zb.A00(unsafe.getObject(obj, j), r38.A02);
                                        unsafe.putObject(obj, j, A00);
                                        i8 |= i14;
                                        break;
                                    }
                                    A00 = r38.A02;
                                    unsafe.putObject(obj, j, A00);
                                    i8 |= i14;
                                }
                        }
                    } else if (A01 != 27) {
                        if (A01 <= 49) {
                            i5 = A09(r38, obj, bArr, i4, i2, i7, i10, i11, A06, A01, (long) i12, j);
                        } else if (A01 != 50) {
                            i5 = A08(r38, obj, bArr, i4, i2, i7, i10, i11, i12, A01, A06, j);
                        } else if (i11 == 2) {
                            A0B(obj, j);
                            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                        }
                        if (i5 == i4) {
                            i4 = i5;
                        }
                        i6 = -1;
                    } else if (i11 == 2) {
                        AnonymousClass5Z5 r2 = (AnonymousClass5Z5) unsafe.getObject(obj, j);
                        if (!((AbstractC113535Hy) r2).A00) {
                            r2 = r2.AhN(C72463ee.A05(r2));
                            unsafe.putObject(obj, j, r2);
                        }
                        AnonymousClass5XT A0A = A0A(A06);
                        i5 = A00(r38, A0A, bArr, i4, i2);
                        while (true) {
                            r2.add(r38.A02);
                            if (i5 < i2) {
                                int A012 = C95504dq.A01(r38, bArr, i5);
                                if (i7 == r38.A00) {
                                    i5 = A00(r38, A0A, bArr, A012, i2);
                                }
                            }
                        }
                        i6 = -1;
                    }
                }
                if (i7 != i3 || i3 == 0) {
                    i5 = C95504dq.A00(r38, AbstractC108624zO.A06(obj), bArr, i7, i4, i2);
                    i6 = -1;
                }
            }
        }
        if (i9 != -1) {
            unsafe.putInt(obj, (long) i9, i8);
        }
        int[] iArr2 = this.A0C;
        if (iArr2 != null) {
            for (int i19 : iArr2) {
                if (A03(obj, this.A0A[i19 + 1]) != null && this.A0E[((i19 >> 2) << 1) + 1] != null) {
                    throw new NoSuchMethodError();
                }
            }
        }
        if (i3 == 0) {
            if (i4 != i2) {
                throw new C868048x("Failed to parse the message.");
            }
        } else if (i4 > i2 || i7 != i3) {
            throw new C868048x("Failed to parse the message.");
        }
        return i4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0179  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A08(X.AnonymousClass4PN r23, java.lang.Object r24, byte[] r25, int r26, int r27, int r28, int r29, int r30, int r31, int r32, int r33, long r34) {
        /*
        // Method dump skipped, instructions count: 420
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.A08(X.4PN, java.lang.Object, byte[], int, int, int, int, int, int, int, int, long):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:93:0x01f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int A09(X.AnonymousClass4PN r18, java.lang.Object r19, byte[] r20, int r21, int r22, int r23, int r24, int r25, int r26, int r27, long r28, long r30) {
        /*
        // Method dump skipped, instructions count: 1174
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.A09(X.4PN, java.lang.Object, byte[], int, int, int, int, int, int, int, long, long):int");
    }

    public final AnonymousClass5XT A0A(int i) {
        int i2 = (i >> 2) << 1;
        Object[] objArr = this.A0E;
        AnonymousClass5XT r0 = (AnonymousClass5XT) objArr[i2];
        if (r0 != null) {
            return r0;
        }
        AnonymousClass5XT A00 = C93984b4.A02.A00((Class) objArr[i2 + 1]);
        objArr[i2] = A00;
        return A00;
    }

    public final void A0B(Object obj, long j) {
        AnonymousClass5IM r0;
        Unsafe unsafe = A0F;
        Object object = unsafe.getObject(obj, j);
        AbstractC115615Sg r3 = this.A05;
        if (!((AnonymousClass5IM) object).zzfa) {
            AnonymousClass5IM r2 = AnonymousClass5IM.A00;
            if (r2.isEmpty()) {
                r0 = new AnonymousClass5IM();
            } else {
                r0 = new AnonymousClass5IM(r2);
            }
            r3.Ah9(r0, object);
            unsafe.putObject(obj, j, r0);
        }
        throw new NoSuchMethodError();
    }

    public final boolean A0C(int i, Object obj) {
        boolean z = this.A09;
        int[] iArr = this.A0A;
        if (z) {
            int i2 = iArr[i + 1];
            long j = (long) (i2 & 1048575);
            switch (C72463ee.A01(i2)) {
                case 0:
                    if (C95624e5.A02.A02(obj, j) != 0.0d) {
                        return true;
                    }
                    break;
                case 1:
                    if (C95624e5.A02.A03(obj, j) != 0.0f) {
                        return true;
                    }
                    break;
                case 2:
                case 3:
                case 5:
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    if (C95624e5.A02.A05(obj, j) != 0) {
                        return true;
                    }
                    break;
                case 4:
                case 6:
                case 11:
                case 12:
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                case 15:
                    if (C95624e5.A02.A04(obj, j) != 0) {
                        return true;
                    }
                    break;
                case 7:
                    return C95624e5.A02.A0F(obj, j);
                case 8:
                    Object A01 = C95624e5.A01(obj, j);
                    if (A01 instanceof String) {
                        if (!((String) A01).isEmpty()) {
                            return true;
                        }
                    } else if (!(A01 instanceof AbstractC111915Bh)) {
                        throw C72453ed.A0h();
                    } else if (!AbstractC111915Bh.A00.equals(A01)) {
                        return true;
                    }
                    break;
                case 9:
                case 17:
                    if (C95624e5.A01(obj, j) != null) {
                        return true;
                    }
                    break;
                case 10:
                    if (!AbstractC111915Bh.A00.equals(C95624e5.A01(obj, j))) {
                        return true;
                    }
                    break;
                default:
                    throw C72453ed.A0h();
            }
        } else {
            int i3 = iArr[i + 2];
            if ((C95624e5.A02.A04(obj, (long) (i3 & 1048575)) & (1 << (i3 >>> 20))) != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean A0D(Object obj, int i, int i2) {
        return C12960it.A1V(C95624e5.A02.A04(obj, C72463ee.A09(this.A0A, i2 + 2)), i);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0019 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0019 A[SYNTHETIC] */
    @Override // X.AnonymousClass5XT
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean A9Z(java.lang.Object r11, java.lang.Object r12) {
        /*
            r10 = this;
            int[] r7 = r10.A0A
            int r6 = r7.length
            r9 = 0
            r5 = 0
        L_0x0005:
            r2 = 1
            if (r5 >= r6) goto L_0x007a
            int r0 = r5 + 1
            r2 = r7[r0]
            r3 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r2 & r3
            long r0 = (long) r0
            int r2 = X.C72463ee.A01(r2)
            switch(r2) {
                case 0: goto L_0x0061;
                case 1: goto L_0x0089;
                case 2: goto L_0x0061;
                case 3: goto L_0x0061;
                case 4: goto L_0x0089;
                case 5: goto L_0x0061;
                case 6: goto L_0x0089;
                case 7: goto L_0x001c;
                case 8: goto L_0x0031;
                case 9: goto L_0x0031;
                case 10: goto L_0x0031;
                case 11: goto L_0x0089;
                case 12: goto L_0x0089;
                case 13: goto L_0x0089;
                case 14: goto L_0x0061;
                case 15: goto L_0x0089;
                case 16: goto L_0x0061;
                case 17: goto L_0x0031;
                case 18: goto L_0x004e;
                case 19: goto L_0x004e;
                case 20: goto L_0x004e;
                case 21: goto L_0x004e;
                case 22: goto L_0x004e;
                case 23: goto L_0x004e;
                case 24: goto L_0x004e;
                case 25: goto L_0x004e;
                case 26: goto L_0x004e;
                case 27: goto L_0x004e;
                case 28: goto L_0x004e;
                case 29: goto L_0x004e;
                case 30: goto L_0x004e;
                case 31: goto L_0x004e;
                case 32: goto L_0x004e;
                case 33: goto L_0x004e;
                case 34: goto L_0x004e;
                case 35: goto L_0x004e;
                case 36: goto L_0x004e;
                case 37: goto L_0x004e;
                case 38: goto L_0x004e;
                case 39: goto L_0x004e;
                case 40: goto L_0x004e;
                case 41: goto L_0x004e;
                case 42: goto L_0x004e;
                case 43: goto L_0x004e;
                case 44: goto L_0x004e;
                case 45: goto L_0x004e;
                case 46: goto L_0x004e;
                case 47: goto L_0x004e;
                case 48: goto L_0x004e;
                case 49: goto L_0x004e;
                case 50: goto L_0x004e;
                case 51: goto L_0x003c;
                case 52: goto L_0x003c;
                case 53: goto L_0x003c;
                case 54: goto L_0x003c;
                case 55: goto L_0x003c;
                case 56: goto L_0x003c;
                case 57: goto L_0x003c;
                case 58: goto L_0x003c;
                case 59: goto L_0x003c;
                case 60: goto L_0x003c;
                case 61: goto L_0x003c;
                case 62: goto L_0x003c;
                case 63: goto L_0x003c;
                case 64: goto L_0x003c;
                case 65: goto L_0x003c;
                case 66: goto L_0x003c;
                case 67: goto L_0x003c;
                case 68: goto L_0x003c;
                default: goto L_0x0019;
            }
        L_0x0019:
            int r5 = r5 + 4
            goto L_0x0005
        L_0x001c:
            boolean r3 = r10.A0C(r5, r11)
            boolean r2 = r10.A0C(r5, r12)
            if (r3 != r2) goto L_0x009f
            X.4YX r2 = X.C95624e5.A02
            boolean r3 = r2.A0F(r11, r0)
            boolean r0 = r2.A0F(r12, r0)
            goto L_0x009d
        L_0x0031:
            boolean r3 = r10.A0C(r5, r11)
            boolean r2 = r10.A0C(r5, r12)
            if (r3 != r2) goto L_0x009f
            goto L_0x004e
        L_0x003c:
            int r2 = r5 + 2
            r2 = r7[r2]
            r2 = r2 & r3
            long r2 = (long) r2
            X.4YX r8 = X.C95624e5.A02
            int r4 = r8.A04(r11, r2)
            int r2 = r8.A04(r12, r2)
            if (r4 != r2) goto L_0x009f
        L_0x004e:
            java.lang.Object r2 = X.C95624e5.A01(r11, r0)
            java.lang.Object r0 = X.C95624e5.A01(r12, r0)
            if (r2 == r0) goto L_0x0019
            if (r2 == 0) goto L_0x009f
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x009f
            goto L_0x0019
        L_0x0061:
            boolean r3 = r10.A0C(r5, r11)
            boolean r2 = r10.A0C(r5, r12)
            if (r3 != r2) goto L_0x009f
            X.4YX r2 = X.C95624e5.A02
            long r3 = r2.A05(r11, r0)
            long r1 = r2.A05(r12, r0)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0019
            return r9
        L_0x007a:
            X.3q5 r11 = (X.AbstractC79123q5) r11
            X.4cr r1 = r11.zzjp
            X.3q5 r12 = (X.AbstractC79123q5) r12
            X.4cr r0 = r12.zzjp
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x009f
            return r2
        L_0x0089:
            boolean r3 = r10.A0C(r5, r11)
            boolean r2 = r10.A0C(r5, r12)
            if (r3 != r2) goto L_0x009f
            X.4YX r2 = X.C95624e5.A02
            int r3 = r2.A04(r11, r0)
            int r0 = r2.A04(r12, r0)
        L_0x009d:
            if (r3 == r0) goto L_0x0019
        L_0x009f:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.A9Z(java.lang.Object, java.lang.Object):boolean");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0099  */
    @Override // X.AnonymousClass5XT
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int AIO(java.lang.Object r10) {
        /*
            r9 = this;
            int[] r5 = r9.A0A
            int r4 = r5.length
            r3 = 0
            r6 = 0
        L_0x0005:
            if (r3 >= r4) goto L_0x00e8
            int r0 = r3 + 1
            r7 = r5[r0]
            r8 = r5[r3]
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r7
            long r1 = (long) r0
            int r0 = X.C72463ee.A01(r7)
            r7 = 37
            switch(r0) {
                case 0: goto L_0x001e;
                case 1: goto L_0x0027;
                case 2: goto L_0x0030;
                case 3: goto L_0x0030;
                case 4: goto L_0x003a;
                case 5: goto L_0x0030;
                case 6: goto L_0x003a;
                case 7: goto L_0x0044;
                case 8: goto L_0x00a2;
                case 9: goto L_0x004d;
                case 10: goto L_0x00db;
                case 11: goto L_0x003a;
                case 12: goto L_0x003a;
                case 13: goto L_0x003a;
                case 14: goto L_0x0030;
                case 15: goto L_0x003a;
                case 16: goto L_0x0030;
                case 17: goto L_0x004d;
                case 18: goto L_0x00db;
                case 19: goto L_0x00db;
                case 20: goto L_0x00db;
                case 21: goto L_0x00db;
                case 22: goto L_0x00db;
                case 23: goto L_0x00db;
                case 24: goto L_0x00db;
                case 25: goto L_0x00db;
                case 26: goto L_0x00db;
                case 27: goto L_0x00db;
                case 28: goto L_0x00db;
                case 29: goto L_0x00db;
                case 30: goto L_0x00db;
                case 31: goto L_0x00db;
                case 32: goto L_0x00db;
                case 33: goto L_0x00db;
                case 34: goto L_0x00db;
                case 35: goto L_0x00db;
                case 36: goto L_0x00db;
                case 37: goto L_0x00db;
                case 38: goto L_0x00db;
                case 39: goto L_0x00db;
                case 40: goto L_0x00db;
                case 41: goto L_0x00db;
                case 42: goto L_0x00db;
                case 43: goto L_0x00db;
                case 44: goto L_0x00db;
                case 45: goto L_0x00db;
                case 46: goto L_0x00db;
                case 47: goto L_0x00db;
                case 48: goto L_0x00db;
                case 49: goto L_0x00db;
                case 50: goto L_0x00db;
                case 51: goto L_0x005b;
                case 52: goto L_0x0070;
                case 53: goto L_0x00c0;
                case 54: goto L_0x00c0;
                case 55: goto L_0x00af;
                case 56: goto L_0x00c0;
                case 57: goto L_0x00af;
                case 58: goto L_0x0085;
                case 59: goto L_0x009c;
                case 60: goto L_0x00d5;
                case 61: goto L_0x00d5;
                case 62: goto L_0x00af;
                case 63: goto L_0x00af;
                case 64: goto L_0x00af;
                case 65: goto L_0x00c0;
                case 66: goto L_0x00af;
                case 67: goto L_0x00c0;
                case 68: goto L_0x00d5;
                default: goto L_0x001b;
            }
        L_0x001b:
            int r3 = r3 + 4
            goto L_0x0005
        L_0x001e:
            int r6 = r6 * 53
            X.4YX r0 = X.C95624e5.A02
            double r0 = r0.A02(r10, r1)
            goto L_0x006b
        L_0x0027:
            int r6 = r6 * 53
            X.4YX r0 = X.C95624e5.A02
            float r0 = r0.A03(r10, r1)
            goto L_0x0080
        L_0x0030:
            int r6 = r6 * 53
            X.4YX r0 = X.C95624e5.A02
            long r0 = r0.A05(r10, r1)
            goto L_0x00d0
        L_0x003a:
            int r6 = r6 * 53
            X.4YX r0 = X.C95624e5.A02
            int r1 = r0.A04(r10, r1)
            goto L_0x00e5
        L_0x0044:
            int r6 = r6 * 53
            X.4YX r0 = X.C95624e5.A02
            boolean r0 = r0.A0F(r10, r1)
            goto L_0x0095
        L_0x004d:
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            if (r0 == 0) goto L_0x0057
            int r7 = r0.hashCode()
        L_0x0057:
            int r6 = r6 * 53
            int r6 = r6 + r7
            goto L_0x001b
        L_0x005b:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            double r0 = X.C72453ed.A00(r0)
        L_0x006b:
            long r0 = java.lang.Double.doubleToLongBits(r0)
            goto L_0x00d0
        L_0x0070:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            float r0 = X.C72453ed.A02(r0)
        L_0x0080:
            int r1 = java.lang.Float.floatToIntBits(r0)
            goto L_0x00e5
        L_0x0085:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            boolean r0 = X.C12970iu.A1Y(r0)
        L_0x0095:
            r1 = 1237(0x4d5, float:1.733E-42)
            if (r0 == 0) goto L_0x00e5
            r1 = 1231(0x4cf, float:1.725E-42)
            goto L_0x00e5
        L_0x009c:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
        L_0x00a2:
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            java.lang.String r0 = (java.lang.String) r0
            int r1 = r0.hashCode()
            goto L_0x00e5
        L_0x00af:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            int r1 = X.C12960it.A05(r0)
            goto L_0x00e5
        L_0x00c0:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            long r0 = X.C12980iv.A0G(r0)
        L_0x00d0:
            int r1 = X.C72453ed.A0B(r0)
            goto L_0x00e5
        L_0x00d5:
            boolean r0 = r9.A0D(r10, r8, r3)
            if (r0 == 0) goto L_0x001b
        L_0x00db:
            int r6 = r6 * 53
            java.lang.Object r0 = X.C95624e5.A01(r10, r1)
            int r1 = r0.hashCode()
        L_0x00e5:
            int r6 = r6 + r1
            goto L_0x001b
        L_0x00e8:
            int r1 = r6 * 53
            X.3q5 r10 = (X.AbstractC79123q5) r10
            X.4cr r0 = r10.zzjp
            int r0 = X.C12990iw.A08(r0, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.AIO(java.lang.Object):int");
    }

    @Override // X.AnonymousClass5XT
    public final Object ALd() {
        return ((AbstractC79123q5) this.A06).A05(4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:0x02e3  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02fd  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0376  */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x063d  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x0678  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x0690  */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x06a8  */
    /* JADX WARNING: Removed duplicated region for block: B:289:0x0708  */
    /* JADX WARNING: Removed duplicated region for block: B:362:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:366:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:368:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:370:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:420:0x0427 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x0427 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:426:0x0427 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:428:0x0427 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0270  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02c9  */
    @Override // X.AnonymousClass5XT
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Agw(X.AbstractC115185Qn r20, java.lang.Object r21) {
        /*
        // Method dump skipped, instructions count: 2196
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.Agw(X.5Qn, java.lang.Object):void");
    }

    @Override // X.AnonymousClass5XT
    public final void Agx(AnonymousClass4PN r32, Object obj, byte[] bArr, int i, int i2) {
        int i3;
        long j;
        Object A00;
        int i4 = i;
        if (this.A09) {
            Unsafe unsafe = A0F;
            while (i4 < i2) {
                int i5 = i4 + 1;
                byte b = bArr[i4];
                byte b2 = b;
                if (b < 0) {
                    i5 = C95504dq.A04(r32, bArr, b, i5);
                    b2 = r32.A00;
                }
                int i6 = (b2 == 1 ? 1 : 0) >>> 3;
                int i7 = (b2 == 1 ? 1 : 0) & 7;
                int A06 = A06(i6);
                if (A06 >= 0) {
                    int i8 = this.A0A[A06 + 1];
                    int i9 = (267386880 & i8) >>> 20;
                    long j2 = (long) (1048575 & i8);
                    if (i9 <= 17) {
                        boolean z = true;
                        switch (i9) {
                            case 0:
                                if (i7 != 1) {
                                    break;
                                } else {
                                    C95624e5.A02.A09(obj, j2, Double.longBitsToDouble(C72453ed.A0b(bArr, i5)));
                                    i4 = i5 + 8;
                                    break;
                                }
                            case 1:
                                if (i7 != 5) {
                                    break;
                                } else {
                                    C95624e5.A02.A0A(obj, j2, Float.intBitsToFloat(C72453ed.A0K(bArr, i5)));
                                    i4 = i5 + 4;
                                    break;
                                }
                            case 2:
                            case 3:
                                if (i7 != 0) {
                                    break;
                                } else {
                                    i4 = C95504dq.A02(r32, bArr, i5);
                                    j = r32.A01;
                                    unsafe.putLong(obj, j2, j);
                                    break;
                                }
                            case 4:
                            case 11:
                            case 12:
                                if (i7 != 0) {
                                    break;
                                } else {
                                    i4 = C95504dq.A01(r32, bArr, i5);
                                    i3 = r32.A00;
                                    unsafe.putInt(obj, j2, i3);
                                    break;
                                }
                            case 5:
                            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                                if (i7 != 1) {
                                    break;
                                } else {
                                    unsafe.putLong(obj, j2, C72453ed.A0b(bArr, i5));
                                    i4 = i5 + 8;
                                    break;
                                }
                            case 6:
                            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                                if (i7 != 5) {
                                    break;
                                } else {
                                    unsafe.putInt(obj, j2, C72453ed.A0K(bArr, i5));
                                    i4 = i5 + 4;
                                    break;
                                }
                            case 7:
                                if (i7 != 0) {
                                    break;
                                } else {
                                    i4 = C95504dq.A02(r32, bArr, i5);
                                    if (r32.A01 == 0) {
                                        z = false;
                                    }
                                    C95624e5.A02.A0D(obj, j2, z);
                                    break;
                                }
                            case 8:
                                if (i7 != 2) {
                                    break;
                                } else {
                                    if ((536870912 & i8) == 0) {
                                        i4 = C95504dq.A01(r32, bArr, i5);
                                        int i10 = r32.A00;
                                        if (i10 == 0) {
                                            r32.A02 = "";
                                        } else {
                                            r32.A02 = new String(bArr, i4, i10, C93094Zb.A03);
                                            i4 += i10;
                                        }
                                    } else {
                                        i4 = A02(r32, bArr, i5);
                                    }
                                    A00 = r32.A02;
                                    unsafe.putObject(obj, j2, A00);
                                    break;
                                }
                            case 9:
                                if (i7 != 2) {
                                    break;
                                } else {
                                    i4 = A00(r32, A0A(A06), bArr, i5, i2);
                                    Object object = unsafe.getObject(obj, j2);
                                    if (object != null) {
                                        A00 = C93094Zb.A00(object, r32.A02);
                                        unsafe.putObject(obj, j2, A00);
                                        break;
                                    }
                                    A00 = r32.A02;
                                    unsafe.putObject(obj, j2, A00);
                                }
                            case 10:
                                if (i7 != 2) {
                                    break;
                                } else {
                                    i4 = C95504dq.A03(r32, bArr, i5);
                                    A00 = r32.A02;
                                    unsafe.putObject(obj, j2, A00);
                                    break;
                                }
                            case 15:
                                if (i7 != 0) {
                                    break;
                                } else {
                                    i4 = C95504dq.A01(r32, bArr, i5);
                                    int i11 = r32.A00;
                                    i3 = (-(i11 & 1)) ^ (i11 >>> 1);
                                    unsafe.putInt(obj, j2, i3);
                                    break;
                                }
                            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                                if (i7 != 0) {
                                    break;
                                } else {
                                    i4 = C95504dq.A02(r32, bArr, i5);
                                    j = C72453ed.A0U(r32.A01);
                                    unsafe.putLong(obj, j2, j);
                                    break;
                                }
                        }
                    } else if (i9 != 27) {
                        if (i9 <= 49) {
                            i4 = A09(r32, obj, bArr, i5, i2, b2, i6, i7, A06, i9, (long) i8, j2);
                        } else if (i9 != 50) {
                            i4 = A08(r32, obj, bArr, i5, i2, b2, i6, i7, i8, i9, A06, j2);
                        } else if (i7 == 2) {
                            A0B(obj, j2);
                            throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
                        }
                        if (i4 == i5) {
                            i5 = i4;
                        }
                    } else if (i7 == 2) {
                        AnonymousClass5Z5 r11 = (AnonymousClass5Z5) unsafe.getObject(obj, j2);
                        if (!((AbstractC113535Hy) r11).A00) {
                            r11 = r11.AhN(C72463ee.A05(r11));
                            unsafe.putObject(obj, j2, r11);
                        }
                        AnonymousClass5XT A0A = A0A(A06);
                        i4 = A00(r32, A0A, bArr, i5, i2);
                        while (true) {
                            r11.add(r32.A02);
                            if (i4 < i2) {
                                int A01 = C95504dq.A01(r32, bArr, i4);
                                if (b2 == r32.A00) {
                                    i4 = A00(r32, A0A, bArr, A01, i2);
                                }
                            }
                        }
                    }
                }
                i4 = C95504dq.A00(r32, AbstractC108624zO.A06(obj), bArr, b2, i5, i2);
            }
            if (i4 != i2) {
                throw new C868048x("Failed to parse the message.");
            }
            return;
        }
        A07(r32, obj, bArr, i4, i2, 0);
    }

    @Override // X.AnonymousClass5XT
    public final void AhF(Object obj) {
        Object unmodifiableList;
        int[] iArr = this.A0C;
        if (iArr != null) {
            for (int i : iArr) {
                long A09 = C72463ee.A09(this.A0A, i + 1);
                Object A01 = C95624e5.A01(obj, A09);
                if (A01 != null) {
                    ((AnonymousClass5IM) A01).zzfa = false;
                    C95624e5.A07(obj, A09, A01);
                }
            }
        }
        int[] iArr2 = this.A0D;
        if (iArr2 != null) {
            for (int i2 : iArr2) {
                long j = (long) i2;
                if (!(this.A04 instanceof C79393qW)) {
                    List list = (List) C95624e5.A01(obj, j);
                    if (list instanceof AnonymousClass5Z3) {
                        unmodifiableList = ((AnonymousClass5Z3) list).AhE();
                    } else if (!C79403qX.A00.isAssignableFrom(list.getClass())) {
                        unmodifiableList = Collections.unmodifiableList(list);
                    }
                    C95624e5.A07(obj, j, unmodifiableList);
                } else {
                    ((AbstractC113535Hy) ((AnonymousClass5Z5) C95624e5.A01(obj, j))).A00 = false;
                }
            }
        }
        ((AbstractC79123q5) obj).zzjp.A02 = false;
    }

    @Override // X.AnonymousClass5XT
    public final void AhG(Object obj, Object obj2) {
        List list;
        AbstractList r1;
        int i = 0;
        while (true) {
            int[] iArr = this.A0A;
            if (i < iArr.length) {
                int i2 = iArr[i + 1];
                long j = (long) (1048575 & i2);
                int i3 = iArr[i];
                switch (C72463ee.A01(i2)) {
                    case 0:
                        if (A0C(i, obj2)) {
                            AnonymousClass4YX r6 = C95624e5.A02;
                            r6.A09(obj, j, r6.A02(obj2, j));
                            break;
                        } else {
                            continue;
                            i += 4;
                        }
                    case 1:
                        if (A0C(i, obj2)) {
                            AnonymousClass4YX r12 = C95624e5.A02;
                            r12.A0A(obj, j, r12.A03(obj2, j));
                            break;
                        } else {
                            continue;
                            i += 4;
                        }
                    case 2:
                    case 3:
                    case 5:
                    case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                        if (A0C(i, obj2)) {
                            AnonymousClass4YX r62 = C95624e5.A02;
                            r62.A0C(obj, j, r62.A05(obj2, j));
                            break;
                        } else {
                            continue;
                            i += 4;
                        }
                    case 4:
                    case 6:
                    case 11:
                    case 12:
                    case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    case 15:
                        if (A0C(i, obj2)) {
                            AnonymousClass4YX r13 = C95624e5.A02;
                            r13.A0B(obj, j, r13.A04(obj2, j));
                            break;
                        } else {
                            continue;
                            i += 4;
                        }
                    case 7:
                        if (A0C(i, obj2)) {
                            AnonymousClass4YX r14 = C95624e5.A02;
                            r14.A0D(obj, j, r14.A0F(obj2, j));
                            break;
                        } else {
                            continue;
                            i += 4;
                        }
                    case 8:
                    case 10:
                        if (A0C(i, obj2)) {
                            C95624e5.A07(obj, j, C95624e5.A01(obj2, j));
                            break;
                        } else {
                            continue;
                            i += 4;
                        }
                    case 9:
                    case 17:
                        long A09 = C72463ee.A09(iArr, i + 1);
                        if (A0C(i, obj2)) {
                            Object A01 = C95624e5.A01(obj, A09);
                            Object A012 = C95624e5.A01(obj2, A09);
                            if (A01 == null) {
                                if (A012 == null) {
                                }
                                C95624e5.A07(obj, A09, A012);
                            } else if (A012 != null) {
                                A012 = C93094Zb.A00(A01, A012);
                                C95624e5.A07(obj, A09, A012);
                                break;
                            }
                        } else {
                            continue;
                        }
                        i += 4;
                    case 18:
                    case 19:
                    case C43951xu.A01:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case C25991Bp.A0S:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                    case 47:
                    case 48:
                    case 49:
                        if (!(this.A04 instanceof C79393qW)) {
                            list = (List) C95624e5.A01(obj2, j);
                            int size = list.size();
                            List list2 = (List) C95624e5.A01(obj, j);
                            if (list2.isEmpty()) {
                                if (list2 instanceof AnonymousClass5Z3) {
                                    list2 = new C79203qD(C12980iv.A0w(size));
                                } else {
                                    list2 = C12980iv.A0w(size);
                                }
                                C95624e5.A07(obj, j, list2);
                            } else {
                                if (C79403qX.A00.isAssignableFrom(list2.getClass())) {
                                    r1 = C12980iv.A0w(list2.size() + size);
                                    r1.addAll(list2);
                                } else if (list2 instanceof C113545Hz) {
                                    r1 = new C79203qD(C12980iv.A0w(list2.size() + size));
                                    r1.addAll(list2);
                                }
                                C95624e5.A07(obj, j, r1);
                                list2 = r1;
                            }
                            int size2 = list2.size();
                            int size3 = list.size();
                            if (size2 > 0) {
                                if (size3 > 0) {
                                    list2.addAll(list);
                                }
                                list = list2;
                            }
                        } else {
                            AnonymousClass5Z5 r4 = (AnonymousClass5Z5) C95624e5.A01(obj, j);
                            list = (List) C95624e5.A01(obj2, j);
                            int size4 = r4.size();
                            int size5 = list.size();
                            if (size4 > 0) {
                                if (size5 > 0) {
                                    if (!((AbstractC113535Hy) r4).A00) {
                                        r4 = r4.AhN(size5 + size4);
                                    }
                                    r4.addAll(list);
                                }
                                list = r4;
                            }
                        }
                        C95624e5.A07(obj, j, list);
                        continue;
                        i += 4;
                    case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                        C95624e5.A07(obj, j, this.A05.Ah9(C95624e5.A01(obj, j), C95624e5.A01(obj2, j)));
                        continue;
                        i += 4;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (A0D(obj2, i3, i)) {
                            C95624e5.A07(obj, j, C95624e5.A01(obj2, j));
                            C95624e5.A02.A0B(obj, C72463ee.A09(iArr, i + 2), i3);
                        } else {
                            continue;
                        }
                        i += 4;
                    case 60:
                    case 68:
                        int i4 = iArr[i + 1];
                        int i5 = iArr[i];
                        long j2 = (long) (i4 & 1048575);
                        if (A0D(obj2, i5, i)) {
                            Object A013 = C95624e5.A01(obj, j2);
                            Object A014 = C95624e5.A01(obj2, j2);
                            if (A013 == null) {
                                if (A014 == null) {
                                }
                                C95624e5.A07(obj, j2, A014);
                                C95624e5.A02.A0B(obj, C72463ee.A09(iArr, i + 2), i5);
                            } else if (A014 != null) {
                                A014 = C93094Zb.A00(A013, A014);
                                C95624e5.A07(obj, j2, A014);
                                C95624e5.A02.A0B(obj, C72463ee.A09(iArr, i + 2), i5);
                            }
                        } else {
                            continue;
                        }
                        i += 4;
                    default:
                        i += 4;
                }
                if (!this.A09) {
                    int i6 = iArr[i + 2];
                    long j3 = (long) (i6 & 1048575);
                    AnonymousClass4YX r42 = C95624e5.A02;
                    r42.A0B(obj, j3, r42.A04(obj, j3) | (1 << (i6 >>> 20)));
                }
                i += 4;
            } else if (!this.A09) {
                C95694eC.A0S(obj, obj2);
                return;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x0244  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x02b5  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x02c3  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x02e9  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x02fd  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0345  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0354  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0399  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x05b3  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x0624  */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x0632  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x0658  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x066c  */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x06b4  */
    /* JADX WARNING: Removed duplicated region for block: B:287:0x06c1  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x0706  */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:322:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:325:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:328:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:345:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:347:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:348:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:349:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:350:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:353:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:354:0x03c8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:357:0x03c8 A[SYNTHETIC] */
    @Override // X.AnonymousClass5XT
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int AhO(java.lang.Object r19) {
        /*
        // Method dump skipped, instructions count: 2116
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C108664zS.AhO(java.lang.Object):int");
    }

    @Override // X.AnonymousClass5XT
    public final boolean AhQ(Object obj) {
        int length;
        int i;
        boolean z;
        int i2;
        int[] iArr = this.A0B;
        if (iArr == null || (length = iArr.length) == 0) {
            return true;
        }
        int i3 = -1;
        int i4 = 0;
        int i5 = 0;
        do {
            int i6 = iArr[i4];
            int A06 = A06(i6);
            int[] iArr2 = this.A0A;
            int i7 = iArr2[A06 + 1];
            boolean z2 = this.A09;
            if (!z2) {
                int i8 = iArr2[A06 + 2];
                int i9 = i8 & 1048575;
                i = 1 << (i8 >>> 20);
                if (i9 != i3) {
                    i5 = A0F.getInt(obj, (long) i9);
                    i3 = i9;
                }
            } else {
                i = 0;
            }
            if ((268435456 & i7) != 0) {
                if (z2) {
                    if (!A0C(A06, obj)) {
                        return false;
                    }
                } else if ((i5 & i) == 0) {
                    return false;
                }
            }
            int i10 = (267386880 & i7) >>> 20;
            if (i10 == 9 || i10 == 17) {
                z = false;
                i2 = z2 ? A0C(A06, obj) : i5 & i;
            } else {
                if (i10 != 27) {
                    if (i10 == 60 || i10 == 68) {
                        z = false;
                        i2 = A0D(obj, i6, A06);
                    } else if (i10 != 49) {
                        if (i10 == 50 && !((AbstractMap) A03(obj, i7)).isEmpty()) {
                            throw new NoSuchMethodError();
                        }
                        i4++;
                    }
                }
                List A04 = A04(i7, obj);
                if (!A04.isEmpty()) {
                    AnonymousClass5XT A0A = A0A(A06);
                    for (int i11 = 0; i11 < A04.size(); i11++) {
                        if (!A0A.AhQ(A04.get(i11))) {
                            return false;
                        }
                    }
                }
                i4++;
            }
            if (i2 && !A0A(A06).AhQ(A03(obj, i7))) {
                return z;
            }
            i4++;
        } while (i4 < length);
        return true;
    }
}
