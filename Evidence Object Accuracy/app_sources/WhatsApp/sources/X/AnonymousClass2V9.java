package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;

/* renamed from: X.2V9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2V9 {
    public View A00;
    public Runnable A01;

    public void A00(View.OnClickListener onClickListener, String str, int i) {
        ((TextView) AnonymousClass028.A0D(this.A00, R.id.share_link_action_item_text)).setText(str);
        ((ImageView) AnonymousClass028.A0D(this.A00, R.id.share_link_action_item_icon)).setImageDrawable(AnonymousClass00T.A04(this.A00.getContext(), i));
        this.A00.setOnClickListener(new ViewOnClickCListenerShape4S0200000_I0(onClickListener, 8, this));
    }
}
