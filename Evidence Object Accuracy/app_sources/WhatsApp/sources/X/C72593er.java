package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.search.SearchFragment;

/* renamed from: X.3er  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72593er extends AnimatorListenerAdapter {
    public final /* synthetic */ SearchFragment A00;
    public final /* synthetic */ Runnable A01;

    public C72593er(SearchFragment searchFragment, Runnable runnable) {
        this.A00 = searchFragment;
        this.A01 = runnable;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A01.run();
        super.onAnimationEnd(animator);
    }
}
