package X;

/* renamed from: X.4Az  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87304Az {
    A01(0),
    A02(1),
    A03(2);
    
    public final int mIntValue;

    EnumC87304Az(int i) {
        this.mIntValue = i;
    }
}
