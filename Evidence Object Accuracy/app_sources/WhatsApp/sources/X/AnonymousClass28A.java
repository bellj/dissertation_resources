package X;

import android.content.Context;
import android.util.AttributeSet;
import com.whatsapp.crop.CropImageView;

/* renamed from: X.28A  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass28A extends AnonymousClass03X implements AnonymousClass004 {
    public AnonymousClass2P7 A00;

    public AnonymousClass28A(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        CropImageView cropImageView = (CropImageView) this;
        if (!cropImageView.A04) {
            cropImageView.A04 = true;
            cropImageView.generatedComponent();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
