package X;

import android.database.ContentObserver;
import android.util.SparseArray;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.util.Log;

/* renamed from: X.2Am  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Am implements AbstractC35521iA {
    public static final boolean A0J = (!C38241nl.A02());
    public int A00;
    public int A01;
    public C16520pA A02;
    public C16520pA A03;
    public AnonymousClass388 A04;
    public AnonymousClass385 A05;
    public AnonymousClass385 A06;
    public MediaViewFragment A07;
    public Runnable A08;
    public boolean A09;
    public boolean A0A;
    public final ContentObserver A0B = new C47382Al(this);
    public final SparseArray A0C;
    public final C15650ng A0D;
    public final C15660nh A0E;
    public final AbstractC14640lm A0F;
    public final AbstractC16130oV A0G;
    public final AnonymousClass2JY A0H;
    public final AbstractC14440lR A0I;

    @Override // X.AbstractC35521iA
    public void ASu() {
    }

    @Override // X.AbstractC35521iA
    public void AfO(int i) {
    }

    public AnonymousClass2Am(C15650ng r3, C15660nh r4, AbstractC14640lm r5, MediaViewFragment mediaViewFragment, AbstractC16130oV r7, AnonymousClass2JY r8, AbstractC14440lR r9) {
        SparseArray sparseArray = new SparseArray();
        this.A0C = sparseArray;
        this.A07 = mediaViewFragment;
        this.A0G = r7;
        this.A0I = r9;
        this.A0F = r5;
        this.A0H = r8;
        this.A0D = r3;
        this.A0E = r4;
        sparseArray.put(0, r7);
    }

    @Override // X.AbstractC35521iA
    public AbstractC16130oV AEG(int i) {
        C16520pA r1;
        C16520pA r0;
        int i2 = i - this.A00;
        SparseArray sparseArray = this.A0C;
        AbstractC16130oV r6 = (AbstractC16130oV) sparseArray.get(i2);
        if (r6 == null) {
            r6 = null;
            if (i2 < 0) {
                C16520pA r7 = this.A02;
                if (r7 != null) {
                    int i3 = (-i2) - 1;
                    if (i3 >= r7.getCount()) {
                        r0 = this.A02;
                        r0.getCount();
                    } else if (this.A02.moveToPosition(i3)) {
                        if (!this.A09 && this.A05 == null && this.A02.getPosition() > (this.A02.getCount() >> 1)) {
                            StringBuilder sb = new StringBuilder("MediaMessagesNavigator/navigator/ start upgrade head cursor count:");
                            sb.append(this.A02.getCount());
                            sb.append(" pos:");
                            sb.append(this.A02.getPosition());
                            Log.i(sb.toString());
                            AnonymousClass385 r4 = new AnonymousClass385(this.A0D, this.A0E, this.A0F, this, this.A02.getPosition(), this.A0G.A11, true);
                            this.A05 = r4;
                            this.A0I.Aaz(r4, new Void[0]);
                        }
                        r1 = this.A02;
                        r6 = r1.A00();
                    }
                }
            } else if (i2 == 0) {
                r6 = this.A0G;
            } else {
                C16520pA r3 = this.A03;
                if (r3 != null) {
                    int i4 = i2 - 1;
                    if (i4 >= r3.getCount()) {
                        r0 = this.A03;
                        r0.getCount();
                    } else if (this.A03.moveToPosition(i4)) {
                        if (!this.A0A && this.A06 == null && this.A03.getPosition() > (this.A03.getCount() >> 1)) {
                            StringBuilder sb2 = new StringBuilder("MediaMessagesNavigator/navigator/ start upgrade tail cursor count:");
                            sb2.append(this.A03.getCount());
                            sb2.append(" pos:");
                            sb2.append(this.A03.getPosition());
                            Log.i(sb2.toString());
                            AnonymousClass385 r42 = new AnonymousClass385(this.A0D, this.A0E, this.A0F, this, this.A03.getPosition(), this.A0G.A11, false);
                            this.A06 = r42;
                            this.A0I.Aaz(r42, new Void[0]);
                        }
                        r1 = this.A03;
                        r6 = r1.A00();
                    }
                }
            }
            if (r6 != null) {
                sparseArray.put(i2, r6);
                return r6;
            }
        }
        return r6;
    }

    @Override // X.AbstractC35521iA
    public int AFq(AnonymousClass1IS r5) {
        int i = 0;
        while (true) {
            SparseArray sparseArray = this.A0C;
            if (i >= sparseArray.size()) {
                return -2;
            }
            int keyAt = sparseArray.keyAt(i);
            if (r5.equals(((AbstractC15340mz) sparseArray.get(keyAt)).A0z)) {
                return this.A00 + keyAt;
            }
            i++;
        }
    }

    @Override // X.AbstractC35521iA
    public void Ac4(Runnable runnable) {
        this.A08 = runnable;
    }

    @Override // X.AbstractC35521iA
    public void AeB() {
        AnonymousClass388 r2 = new AnonymousClass388(this.A0D, this.A0E, this.A0F, this, this.A0G);
        this.A04 = r2;
        this.A0I.Aaz(r2, new Void[0]);
    }

    @Override // X.AbstractC35521iA
    public void AeQ() {
        AnonymousClass388 r0 = this.A04;
        if (r0 != null && !((AbstractC16350or) r0).A02.isCancelled()) {
            this.A04.A03(true);
        }
    }

    @Override // X.AbstractC35521iA
    public void close() {
        AeQ();
        C16520pA r0 = this.A02;
        if (r0 != null) {
            r0.close();
        }
        this.A02 = null;
        C16520pA r02 = this.A03;
        if (r02 != null) {
            r02.close();
        }
        this.A03 = null;
        AnonymousClass385 r03 = this.A05;
        if (r03 != null) {
            r03.A03(true);
        }
        this.A05 = null;
        AnonymousClass385 r04 = this.A06;
        if (r04 != null) {
            r04.A03(true);
        }
        this.A06 = null;
        this.A09 = false;
        this.A0A = false;
        this.A00 = 0;
        this.A01 = 0;
        this.A0C.clear();
    }

    @Override // X.AbstractC35521iA
    public int getCount() {
        return this.A00 + 1 + this.A01;
    }
}
