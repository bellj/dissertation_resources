package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.util.Log;

/* renamed from: X.15g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C243715g {
    public final C22320yt A00;
    public final C16370ot A01;
    public final C16510p9 A02;
    public final C19990v2 A03;
    public final C18460sU A04;
    public final C20090vC A05;
    public final AnonymousClass12I A06;
    public final C20850wQ A07;
    public final C16490p7 A08;
    public final C18470sV A09;
    public final C238213d A0A;

    public C243715g(C22320yt r1, C16370ot r2, C16510p9 r3, C19990v2 r4, C18460sU r5, C20090vC r6, AnonymousClass12I r7, C20850wQ r8, C16490p7 r9, C18470sV r10, C238213d r11) {
        this.A04 = r5;
        this.A02 = r3;
        this.A03 = r4;
        this.A09 = r10;
        this.A05 = r6;
        this.A0A = r11;
        this.A00 = r1;
        this.A01 = r2;
        this.A06 = r7;
        this.A08 = r9;
        this.A07 = r8;
    }

    public void A00(AbstractC14640lm r5, long j, long j2) {
        StringBuilder sb = new StringBuilder("msgstore/setchatreadreceiptssent/");
        sb.append(r5);
        sb.append(" ");
        sb.append(j2);
        Log.i(sb.toString());
        AnonymousClass1PE A06 = this.A03.A06(r5);
        if (A06 == null) {
            StringBuilder sb2 = new StringBuilder("msgstore/setchatreadreceiptssent/no chat for ");
            sb2.append(r5);
            Log.w(sb2.toString());
        } else if (j2 > A06.A0R) {
            A06.A0Q = j;
            A06.A0R = j2;
            this.A00.A01(new RunnableBRunnable0Shape4S0200000_I0_4(this, 29, A06), 43);
        }
    }
}
