package X;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* renamed from: X.0TW  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0TW {
    public static Field A00;
    public static Method A01;
    public static boolean A02;
    public static boolean A03;

    public static void A00(View view, PopupWindow popupWindow, int i, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 19) {
            AnonymousClass0L6.A00(view, popupWindow, i, i2, i3);
            return;
        }
        if ((C05660Ql.A00(i3, AnonymousClass028.A05(view)) & 7) == 5) {
            i -= popupWindow.getWidth() - view.getWidth();
        }
        popupWindow.showAsDropDown(view, i, i2);
    }

    public static void A01(PopupWindow popupWindow, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            AnonymousClass0R0.A00(popupWindow, i);
            return;
        }
        if (!A03) {
            try {
                Method declaredMethod = PopupWindow.class.getDeclaredMethod("setWindowLayoutType", Integer.TYPE);
                A01 = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (Exception unused) {
            }
            A03 = true;
        }
        Method method = A01;
        if (method != null) {
            try {
                method.invoke(popupWindow, Integer.valueOf(i));
            } catch (Exception unused2) {
            }
        }
    }

    public static void A02(PopupWindow popupWindow, boolean z) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            AnonymousClass0R0.A01(popupWindow, z);
        } else if (i >= 21) {
            if (!A02) {
                try {
                    Field declaredField = PopupWindow.class.getDeclaredField("mOverlapAnchor");
                    A00 = declaredField;
                    declaredField.setAccessible(true);
                } catch (NoSuchFieldException e) {
                    Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e);
                }
                A02 = true;
            }
            Field field = A00;
            if (field != null) {
                try {
                    field.set(popupWindow, Boolean.valueOf(z));
                } catch (IllegalAccessException e2) {
                    Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e2);
                }
            }
        }
    }
}
