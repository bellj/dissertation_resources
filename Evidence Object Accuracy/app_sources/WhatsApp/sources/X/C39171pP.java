package X;

import android.os.PowerManager;
import com.whatsapp.Mp4Ops;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.1pP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39171pP extends AbstractRunnableC39141pM {
    public static final C90364Np A0J = new C90364Np(30, 72);
    public static final C90364Np A0K = new C90364Np(48, 96);
    public final PowerManager.WakeLock A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final Mp4Ops A03;
    public final C15450nH A04;
    public final C17050qB A05;
    public final C16590pI A06;
    public final C14820m6 A07;
    public final C19350ty A08;
    public final C15660nh A09;
    public final C14850m9 A0A;
    public final C16120oU A0B;
    public final C39161pO A0C;
    public final C239713s A0D;
    public final C26831Ez A0E;
    public final AnonymousClass160 A0F;
    public final C15690nk A0G;
    public final C22190yg A0H;
    public final AnonymousClass152 A0I;

    public C39171pP(PowerManager.WakeLock wakeLock, AbstractC15710nm r3, C14330lG r4, Mp4Ops mp4Ops, C15450nH r6, C17050qB r7, C16590pI r8, C14820m6 r9, C19350ty r10, C15660nh r11, C14850m9 r12, C16120oU r13, C39161pO r14, C239713s r15, C26831Ez r16, AnonymousClass160 r17, C15690nk r18, C22190yg r19, AnonymousClass152 r20) {
        super(r14);
        this.A06 = r8;
        this.A03 = mp4Ops;
        this.A0A = r12;
        this.A01 = r3;
        this.A02 = r4;
        this.A0B = r13;
        this.A04 = r6;
        this.A0H = r19;
        this.A0D = r15;
        this.A08 = r10;
        this.A09 = r11;
        this.A05 = r7;
        this.A0I = r20;
        this.A07 = r9;
        this.A0G = r18;
        this.A0E = r16;
        this.A0F = r17;
        this.A00 = wakeLock;
        this.A0C = r14;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:134:0x0391 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:135:0x0393 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:136:0x0395 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:137:0x0397 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:138:0x039a */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:139:0x039d */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:201:0x0511 */
    /* JADX DEBUG: Multi-variable search result rejected for r15v2, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r15v1, types: [X.0pI] */
    /* JADX WARN: Type inference failed for: r15v6 */
    /* JADX WARN: Type inference failed for: r15v7 */
    /* JADX WARN: Type inference failed for: r15v8 */
    /* JADX WARN: Type inference failed for: r15v9 */
    /* JADX WARN: Type inference failed for: r15v11 */
    /* JADX WARN: Type inference failed for: r15v12 */
    /* JADX WARN: Type inference failed for: r15v13 */
    /* JADX WARN: Type inference failed for: r15v14 */
    /* JADX WARN: Type inference failed for: r15v15 */
    /* JADX WARN: Type inference failed for: r15v16 */
    /* JADX WARN: Type inference failed for: r15v17 */
    /* JADX WARN: Type inference failed for: r15v18 */
    /* JADX WARN: Type inference failed for: r15v19 */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0359 A[Catch: IllegalStateException -> 0x039d, IllegalArgumentException -> 0x039a, 47W -> 0x0397, FileNotFoundException -> 0x0395, IOException -> 0x0393, 1pl -> 0x0391, all -> 0x0511, TryCatch #11 {all -> 0x0511, blocks: (B:48:0x01a9, B:64:0x01e1, B:65:0x01e3, B:104:0x02e9, B:106:0x02fc, B:107:0x0306, B:108:0x0318, B:109:0x031e, B:114:0x0328, B:116:0x0332, B:118:0x0348, B:119:0x0353, B:121:0x0359, B:123:0x035d, B:124:0x0362, B:127:0x036d, B:128:0x0375, B:129:0x0376, B:130:0x037c, B:131:0x0381, B:132:0x0382, B:133:0x0390, B:144:0x03a6, B:147:0x03c9, B:149:0x03ee, B:154:0x0400, B:157:0x0425, B:158:0x042f, B:161:0x0437, B:164:0x0459, B:166:0x047b), top: B:213:0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0376 A[Catch: IllegalStateException -> 0x039d, IllegalArgumentException -> 0x039a, 47W -> 0x0397, FileNotFoundException -> 0x0395, IOException -> 0x0393, 1pl -> 0x0391, all -> 0x0511, TryCatch #11 {all -> 0x0511, blocks: (B:48:0x01a9, B:64:0x01e1, B:65:0x01e3, B:104:0x02e9, B:106:0x02fc, B:107:0x0306, B:108:0x0318, B:109:0x031e, B:114:0x0328, B:116:0x0332, B:118:0x0348, B:119:0x0353, B:121:0x0359, B:123:0x035d, B:124:0x0362, B:127:0x036d, B:128:0x0375, B:129:0x0376, B:130:0x037c, B:131:0x0381, B:132:0x0382, B:133:0x0390, B:144:0x03a6, B:147:0x03c9, B:149:0x03ee, B:154:0x0400, B:157:0x0425, B:158:0x042f, B:161:0x0437, B:164:0x0459, B:166:0x047b), top: B:213:0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0485  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x049e  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x04ae  */
    @Override // X.AbstractRunnableC39141pM
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC39731qS A00() {
        /*
        // Method dump skipped, instructions count: 1320
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39171pP.A00():X.1qS");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0050, code lost:
        if (r2 == null) goto L_0x0052;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass45F A01(X.AnonymousClass45C r9, X.AbstractC16130oV r10, java.io.File r11) {
        /*
            r8 = this;
            r7 = 0
            if (r10 == 0) goto L_0x00d6
            java.lang.String r0 = r10.A05
            if (r0 == 0) goto L_0x00d6
            X.0oX r0 = r10.A02
            if (r0 == 0) goto L_0x00d6
            java.io.File r0 = r0.A0F
            if (r0 == 0) goto L_0x00d6
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x00d6
            X.0nk r1 = r8.A0G     // Catch: IOException -> 0x00cd
            X.0oX r0 = r10.A02     // Catch: IOException -> 0x00cd
            java.io.File r0 = r0.A0F     // Catch: IOException -> 0x00cd
            X.C14350lI.A0A(r1, r0, r11)     // Catch: IOException -> 0x00cd
            java.lang.String r1 = r10.A05     // Catch: IOException -> 0x00cd
            java.lang.String r0 = X.C38531oF.A00(r11)     // Catch: IOException -> 0x00cd
            boolean r0 = r1.equals(r0)     // Catch: IOException -> 0x00cd
            if (r0 != 0) goto L_0x002e
            X.C14350lI.A0M(r11)     // Catch: IOException -> 0x00cd
            return r7
        L_0x002e:
            X.1pO r6 = r8.A0C     // Catch: IOException -> 0x00cd
            boolean r5 = r6.A04     // Catch: IOException -> 0x00cd
            X.1IS r0 = r10.A0z     // Catch: IOException -> 0x00cd
            X.0lm r0 = r0.A00     // Catch: IOException -> 0x00cd
            boolean r0 = X.C15380n4.A0N(r0)     // Catch: IOException -> 0x00cd
            if (r5 != r0) goto L_0x0052
            X.160 r1 = r8.A0F     // Catch: IOException -> 0x00cd
            X.0p3 r0 = r10.A0G()     // Catch: IOException -> 0x00cd
            X.AnonymousClass009.A05(r0)     // Catch: IOException -> 0x00cd
            r1.A01(r0)     // Catch: IOException -> 0x00cd
            X.0p3 r0 = r10.A0G()     // Catch: IOException -> 0x00cd
            byte[] r2 = r0.A07()     // Catch: IOException -> 0x00cd
            if (r2 != 0) goto L_0x007d
        L_0x0052:
            if (r5 == 0) goto L_0x0057
            X.4Np r0 = X.C39171pP.A0K     // Catch: IOException -> 0x00cd
            goto L_0x0059
        L_0x0057:
            X.4Np r0 = X.C39171pP.A0J     // Catch: IOException -> 0x00cd
        L_0x0059:
            int r4 = r0.A00     // Catch: IOException -> 0x00cd
            if (r5 == 0) goto L_0x00ca
            X.4Np r0 = X.C39171pP.A0K     // Catch: IOException -> 0x00cd
        L_0x005f:
            int r3 = r0.A01     // Catch: IOException -> 0x00cd
            r2 = r5 ^ 1
            r1 = 1
            X.1ot r0 = new X.1ot     // Catch: IOException -> 0x00cd
            r0.<init>(r3, r1)     // Catch: IOException -> 0x00cd
            android.graphics.Bitmap r0 = X.C26521Du.A00(r0, r11)     // Catch: IOException -> 0x00cd
            if (r0 == 0) goto L_0x0075
            byte[] r2 = X.AnonymousClass3GQ.A00(r0, r4, r2)     // Catch: IOException -> 0x00cd
            if (r2 != 0) goto L_0x007d
        L_0x0075:
            android.graphics.Bitmap r0 = X.C26521Du.A01(r11)     // Catch: IOException -> 0x00cd
            byte[] r2 = X.C26521Du.A03(r0)     // Catch: IOException -> 0x00cd
        L_0x007d:
            X.1qT r4 = r6.A01     // Catch: IOException -> 0x00cd
            java.io.File r0 = r6.A03     // Catch: IOException -> 0x00cd
            long r0 = r0.length()     // Catch: IOException -> 0x00cd
            X.1qU r3 = r4.A03     // Catch: IOException -> 0x00cd
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: IOException -> 0x00cd
            r3.A0J = r0     // Catch: IOException -> 0x00cd
            r0 = 3
            long r0 = (long) r0     // Catch: IOException -> 0x00cd
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: IOException -> 0x00cd
            r3.A0N = r0     // Catch: IOException -> 0x00cd
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r5)     // Catch: IOException -> 0x00cd
            r3.A01 = r0     // Catch: IOException -> 0x00cd
            r0 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch: IOException -> 0x00cd
            r3.A00 = r0     // Catch: IOException -> 0x00cd
            int r0 = r10.A00     // Catch: IOException -> 0x00cd
            long r0 = (long) r0     // Catch: IOException -> 0x00cd
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: IOException -> 0x00cd
            r3.A05 = r0     // Catch: IOException -> 0x00cd
            long r0 = r11.length()     // Catch: IOException -> 0x00cd
            java.lang.Long r0 = java.lang.Long.valueOf(r0)     // Catch: IOException -> 0x00cd
            r3.A07 = r0     // Catch: IOException -> 0x00cd
            r4.A01()     // Catch: IOException -> 0x00cd
            int r0 = r10.A00     // Catch: IOException -> 0x00cd
            r9.A00 = r0     // Catch: IOException -> 0x00cd
            r0 = 1
            r9.A01 = r0     // Catch: IOException -> 0x00cd
            r9.A00 = r11     // Catch: IOException -> 0x00cd
            r9.A03 = r2     // Catch: IOException -> 0x00cd
            r9.A02 = r0     // Catch: IOException -> 0x00cd
            X.45F r0 = r9.A00()     // Catch: IOException -> 0x00cd
            return r0
        L_0x00ca:
            X.4Np r0 = X.C39171pP.A0J     // Catch: IOException -> 0x00cd
            goto L_0x005f
        L_0x00cd:
            r1 = move-exception
            java.lang.String r0 = "mediatranscodequeue/attemptReuseExistingVideo"
            com.whatsapp.util.Log.e(r0, r1)
            X.C14350lI.A0M(r11)
        L_0x00d6:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C39171pP.A01(X.45C, X.0oV, java.io.File):X.45F");
    }

    public final void A02(C39741qT r8, C38981p3 r9, C39011p7 r10, int i, int i2, long j) {
        C39751qU r2 = r8.A03;
        r2.A0P = "transcode";
        r2.A0C = Long.valueOf((long) r9.A02);
        int i3 = r9.A00;
        int i4 = 9;
        if (this.A0C.A04) {
            i4 = 6;
        }
        r10.A00 = C239713s.A00(i3, i, i2, i4, j);
        r10.A0D();
    }

    public final void A03(Exception exc) {
        C47722Cd r1 = new C47722Cd();
        r1.A00 = 5;
        r1.A05 = exc.toString();
        this.A0B.A06(r1);
    }

    public final boolean A04(C39741qT r5, File file, File file2) {
        r5.A03.A0P = "checkAndRepair";
        C14350lI.A0A(this.A02.A04, file, file2);
        try {
            return this.A03.A05(file2);
        } catch (C39361pl e) {
            Mp4Ops.A00(this.A06.A00, this.A01, file2, e, "only repair on upload");
            throw e;
        } catch (IOException e2) {
            Log.e("mediatranscodequeue/repair/io-exception/", e2);
            throw e2;
        }
    }
}
