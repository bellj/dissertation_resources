package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2nQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57662nQ extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57662nQ A0D;
    public static volatile AnonymousClass255 A0E;
    public int A00;
    public int A01;
    public AbstractC27881Jp A02 = AbstractC27881Jp.A01;
    public String A03 = "";
    public String A04 = "";
    public String A05 = "";
    public String A06 = "";
    public String A07 = "";
    public String A08 = "";
    public String A09 = "";
    public boolean A0A;
    public boolean A0B;
    public boolean A0C;

    static {
        C57662nQ r0 = new C57662nQ();
        A0D = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r7, Object obj, Object obj2) {
        switch (r7.ordinal()) {
            case 0:
                return A0D;
            case 1:
                AbstractC462925h r8 = (AbstractC462925h) obj;
                C57662nQ r9 = (C57662nQ) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                String str = this.A09;
                int i2 = r9.A00;
                this.A09 = r8.Afy(str, r9.A09, A1R, C12960it.A1R(i2));
                this.A03 = r8.Afy(this.A03, r9.A03, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A01 = r8.Afp(this.A01, r9.A01, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                this.A08 = r8.Afy(this.A08, r9.A08, C12960it.A1V(i & 8, 8), C12960it.A1V(i2 & 8, 8));
                this.A04 = r8.Afy(this.A04, r9.A04, C12960it.A1V(i & 16, 16), C12960it.A1V(i2 & 16, 16));
                this.A02 = r8.Afm(this.A02, r9.A02, C12960it.A1V(i & 32, 32), C12960it.A1V(i2 & 32, 32));
                int i3 = this.A00;
                boolean A1V = C12960it.A1V(i3 & 64, 64);
                String str2 = this.A06;
                int i4 = r9.A00;
                this.A06 = r8.Afy(str2, r9.A06, A1V, C12960it.A1V(i4 & 64, 64));
                this.A05 = r8.Afy(this.A05, r9.A05, C12960it.A1V(i3 & 128, 128), C12960it.A1V(i4 & 128, 128));
                this.A07 = r8.Afy(this.A07, r9.A07, C12960it.A1V(i3 & 256, 256), C12960it.A1V(i4 & 256, 256));
                this.A0A = r8.Afl(C12960it.A1V(i3 & 512, 512), this.A0A, C12960it.A1V(i4 & 512, 512), r9.A0A);
                this.A0B = r8.Afl(C12960it.A1V(i3 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), this.A0B, C12960it.A1V(i4 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH), r9.A0B);
                this.A0C = r8.Afl(C12960it.A1V(i3 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH), this.A0C, C12960it.A1V(i4 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH, EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH), r9.A0C);
                if (r8 == C463025i.A00) {
                    this.A00 = i3 | i4;
                }
                return this;
            case 2:
                AnonymousClass253 r82 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        int A03 = r82.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 10:
                                String A0A = r82.A0A();
                                this.A00 = 1 | this.A00;
                                this.A09 = A0A;
                                break;
                            case 18:
                                String A0A2 = r82.A0A();
                                this.A00 |= 2;
                                this.A03 = A0A2;
                                break;
                            case 24:
                                int A02 = r82.A02();
                                if (A02 != 0 && A02 != 1 && A02 != 2) {
                                    super.A0X(3, A02);
                                    break;
                                } else {
                                    this.A00 |= 4;
                                    this.A01 = A02;
                                    break;
                                }
                            case 34:
                                String A0A3 = r82.A0A();
                                this.A00 |= 8;
                                this.A08 = A0A3;
                                break;
                            case 42:
                                String A0A4 = r82.A0A();
                                this.A00 |= 16;
                                this.A04 = A0A4;
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                this.A00 |= 32;
                                this.A02 = r82.A08();
                                break;
                            case 58:
                                String A0A5 = r82.A0A();
                                this.A00 |= 64;
                                this.A06 = A0A5;
                                break;
                            case 66:
                                String A0A6 = r82.A0A();
                                this.A00 |= 128;
                                this.A05 = A0A6;
                                break;
                            case 74:
                                String A0A7 = r82.A0A();
                                this.A00 |= 256;
                                this.A07 = A0A7;
                                break;
                            case 80:
                                this.A00 |= 512;
                                this.A0A = r82.A0F();
                                break;
                            case 88:
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A0B = r82.A0F();
                                break;
                            case 96:
                                this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                this.A0C = r82.A0F();
                                break;
                            default:
                                if (A0a(r82, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        throw AbstractC27091Fz.A0J(e, this);
                    } catch (IOException e2) {
                        throw AbstractC27091Fz.A0K(this, e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new C57662nQ();
            case 5:
                return new C81613uI();
            case 6:
                break;
            case 7:
                if (A0E == null) {
                    synchronized (C57662nQ.class) {
                        if (A0E == null) {
                            A0E = AbstractC27091Fz.A09(A0D);
                        }
                    }
                }
                return A0E;
            default:
                throw C12970iu.A0z();
        }
        return A0D;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = AbstractC27091Fz.A04(1, this.A09, 0);
        }
        if ((this.A00 & 2) == 2) {
            i2 = AbstractC27091Fz.A04(2, this.A03, i2);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A03(3, this.A01, i2);
        }
        if ((i3 & 8) == 8) {
            i2 = AbstractC27091Fz.A04(4, this.A08, i2);
        }
        if ((this.A00 & 16) == 16) {
            i2 = AbstractC27091Fz.A04(5, this.A04, i2);
        }
        int i4 = this.A00;
        if ((i4 & 32) == 32) {
            i2 = AbstractC27091Fz.A05(this.A02, 6, i2);
        }
        if ((i4 & 64) == 64) {
            i2 = AbstractC27091Fz.A04(7, this.A06, i2);
        }
        if ((this.A00 & 128) == 128) {
            i2 = AbstractC27091Fz.A04(8, this.A05, i2);
        }
        if ((this.A00 & 256) == 256) {
            i2 = AbstractC27091Fz.A04(9, this.A07, i2);
        }
        int i5 = this.A00;
        if ((i5 & 512) == 512) {
            i2 += CodedOutputStream.A00(10);
        }
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i2 += CodedOutputStream.A00(11);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i2 += CodedOutputStream.A00(12);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A09);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A03);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0E(3, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0I(4, this.A08);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0I(5, this.A04);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A02, 6);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0I(7, this.A06);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A05);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0I(9, this.A07);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0J(10, this.A0A);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0J(11, this.A0B);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0J(12, this.A0C);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
