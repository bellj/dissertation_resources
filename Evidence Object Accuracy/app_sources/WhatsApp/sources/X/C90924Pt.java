package X;

import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.4Pt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90924Pt {
    public final float A00;
    public final LatLng A01;
    public final String A02;

    public C90924Pt(String str, double d, double d2, float f) {
        this.A01 = new LatLng(d, d2);
        this.A00 = f;
        this.A02 = str;
    }
}
