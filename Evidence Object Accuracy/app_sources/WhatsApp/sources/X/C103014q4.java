package X;

import android.content.Context;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;

/* renamed from: X.4q4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103014q4 implements AbstractC009204q {
    public final /* synthetic */ MediaAlbumActivity A00;

    public C103014q4(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
