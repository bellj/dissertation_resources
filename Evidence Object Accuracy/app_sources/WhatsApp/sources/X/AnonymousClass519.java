package X;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.redex.IDxProviderShape16S0100000_2_I1;
import com.google.android.material.appbar.AppBarLayout;
import com.whatsapp.R;

/* renamed from: X.519  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass519 implements AbstractC117155Ys {
    public float A00 = 0.0f;
    public int A01 = 0;
    public final float A02;
    public final float A03;
    public final Resources A04;
    public final View A05;
    public final View A06;
    public final ViewGroup.LayoutParams A07;
    public final ViewGroup.MarginLayoutParams A08;
    public final TextView A09;
    public final TextView A0A;
    public final AnonymousClass018 A0B;
    public final AnonymousClass5EX A0C;
    public final AnonymousClass5EX A0D;
    public final AnonymousClass5EX A0E;
    public final AnonymousClass5EX A0F;
    public final AnonymousClass5EX A0G;
    public final AnonymousClass5EX A0H;
    public final AnonymousClass5EX A0I;
    public final AnonymousClass5EX A0J;
    public final AnonymousClass5EX A0K;
    public final AnonymousClass5EX A0L;
    public final AnonymousClass5EX A0M;
    public final AnonymousClass5EX A0N;
    public final AnonymousClass5EX A0O;
    public final AnonymousClass5EX A0P;
    public final AnonymousClass5EX A0Q;

    public AnonymousClass519(View view, View view2, TextView textView, TextView textView2, AnonymousClass018 r7) {
        this.A0B = r7;
        this.A05 = (View) view2.getParent();
        this.A04 = view2.getResources();
        this.A06 = view2;
        this.A0A = textView;
        this.A09 = textView2;
        this.A08 = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
        this.A03 = textView.getTextSize();
        this.A07 = textView2.getLayoutParams();
        this.A02 = textView2.getTextSize();
        this.A0C = new AnonymousClass5EX(new AnonymousClass01N(view, this) { // from class: X.5ES
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass519 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                AnonymousClass519 r0 = this.A01;
                return Float.valueOf(((float) this.A00.getHeight()) - (((float) r0.A04.getDimensionPixelSize(R.dimen.space_base)) * 2.0f));
            }
        });
        this.A0I = new AnonymousClass5EX(new AnonymousClass01N(view2, this) { // from class: X.5EQ
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass519 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return Float.valueOf(1.0f - (AnonymousClass5EX.A00(this.A01.A0C) / ((float) this.A00.getHeight())));
            }
        });
        this.A0D = new AnonymousClass5EX(new AnonymousClass01N(view2, this) { // from class: X.5EP
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass519 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return Float.valueOf(((float) this.A00.getWidth()) * (1.0f - AnonymousClass5EX.A00(this.A01.A0I)));
            }
        });
        this.A0J = new AnonymousClass5EX(new AnonymousClass01N(view2, view, this, r7) { // from class: X.5EV
            public final /* synthetic */ View A00;
            public final /* synthetic */ View A01;
            public final /* synthetic */ AnonymousClass519 A02;
            public final /* synthetic */ AnonymousClass018 A03;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A03 = r4;
                this.A01 = r2;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                float left;
                AnonymousClass519 r4 = this.A02;
                View view3 = this.A00;
                AnonymousClass018 r3 = this.A03;
                View view4 = this.A01;
                float left2 = (float) view3.getLeft();
                if (C28141Kv.A00(r3)) {
                    left = ((float) view4.getRight()) - AnonymousClass5EX.A00(r4.A0D);
                } else {
                    left = (float) view4.getLeft();
                }
                return Float.valueOf(left - left2);
            }
        });
        this.A0K = new AnonymousClass5EX(new AnonymousClass01N(view2, this) { // from class: X.5ER
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass519 A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                return Float.valueOf(((float) this.A01.A04.getDimensionPixelSize(R.dimen.space_base)) - ((float) this.A00.getTop()));
            }
        });
        this.A0M = A00(textView, 2);
        this.A0H = A00(this, 8);
        this.A0G = new AnonymousClass5EX(new AnonymousClass01N(view, this, r7) { // from class: X.5EU
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass519 A01;
            public final /* synthetic */ AnonymousClass018 A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                int right;
                AnonymousClass519 r4 = this.A01;
                AnonymousClass018 r1 = this.A02;
                View view3 = this.A00;
                Resources resources = r4.A04;
                int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.space_base);
                boolean A00 = C28141Kv.A00(r1);
                int left = view3.getLeft();
                if (A00) {
                    right = (int) (((((float) view3.getRight()) - AnonymousClass5EX.A00(r4.A0D)) - ((float) resources.getDimensionPixelSize(R.dimen.space_loose))) + 0.5f);
                } else {
                    left = (int) (((float) left) + AnonymousClass5EX.A00(r4.A0D) + ((float) resources.getDimensionPixelSize(R.dimen.space_loose)) + 0.5f);
                    right = view3.getRight();
                }
                return new Rect(left, dimensionPixelSize, right, (int) (((float) dimensionPixelSize) + AnonymousClass5EX.A00(r4.A0H) + 0.5f));
            }
        });
        this.A0P = A00(this, 3);
        this.A0Q = A00(this, 7);
        this.A0L = A00(textView2, 1);
        this.A0F = A00(this, 4);
        this.A0E = new AnonymousClass5EX(new AnonymousClass01N(view, this, r7) { // from class: X.5ET
            public final /* synthetic */ View A00;
            public final /* synthetic */ AnonymousClass519 A01;
            public final /* synthetic */ AnonymousClass018 A02;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A00 = r1;
            }

            @Override // X.AnonymousClass01N, X.AnonymousClass01H
            public final Object get() {
                int i;
                int right;
                AnonymousClass519 r4 = this.A01;
                AnonymousClass018 r1 = this.A02;
                View view3 = this.A00;
                Resources resources = r4.A04;
                float dimensionPixelSize = ((float) resources.getDimensionPixelSize(R.dimen.space_base)) + AnonymousClass5EX.A00(r4.A0H);
                boolean A00 = C28141Kv.A00(r1);
                int left = view3.getLeft();
                if (A00) {
                    i = (int) (dimensionPixelSize + 0.5f);
                    right = (int) (((((float) view3.getRight()) - AnonymousClass5EX.A00(r4.A0D)) - ((float) resources.getDimensionPixelSize(R.dimen.space_loose))) + 0.5f);
                } else {
                    left = (int) (((float) left) + AnonymousClass5EX.A00(r4.A0D) + ((float) resources.getDimensionPixelSize(R.dimen.space_loose)) + 0.5f);
                    i = (int) (dimensionPixelSize + 0.5f);
                    right = view3.getRight();
                }
                return new Rect(left, i, right, (int) (dimensionPixelSize + AnonymousClass5EX.A00(r4.A0F) + 0.5f));
            }
        });
        this.A0N = A00(this, 5);
        this.A0O = A00(this, 6);
    }

    public static AnonymousClass5EX A00(Object obj, int i) {
        return new AnonymousClass5EX(new IDxProviderShape16S0100000_2_I1(obj, i));
    }

    public static final void A01(View view, float f, float f2, float f3, float f4, float f5) {
        view.setScaleX(f5);
        view.setScaleY(f5);
        float f6 = 1.0f - f5;
        view.setTranslationX(f3 - (((((float) view.getWidth()) / 2.0f) - f) * f6));
        view.setTranslationY(f4 - (((((float) view.getHeight()) / 2.0f) - f2) * f6));
    }

    public static boolean A02(TextView textView) {
        textView.setScaleX(1.0f);
        textView.setScaleY(1.0f);
        textView.setTranslationY(0.0f);
        textView.setAlpha(1.0f);
        textView.setMaxLines(3);
        if (Build.VERSION.SDK_INT >= 27) {
            AnonymousClass0R1.A00(textView, 0);
        } else if (textView instanceof AnonymousClass02c) {
            ((AnonymousClass02c) textView).setAutoSizeTextTypeWithDefaults(0);
            return false;
        }
        return false;
    }

    public final ViewGroup.MarginLayoutParams A03(int i, int i2) {
        ViewGroup.MarginLayoutParams marginLayoutParams = this.A08;
        if (marginLayoutParams instanceof RelativeLayout.LayoutParams) {
            return new RelativeLayout.LayoutParams(i, i2);
        }
        if (marginLayoutParams instanceof C52902bs) {
            return new C52902bs(i, i2);
        }
        if (marginLayoutParams instanceof LinearLayout.LayoutParams) {
            return new LinearLayout.LayoutParams(i, i2);
        }
        if (marginLayoutParams instanceof FrameLayout.LayoutParams) {
            return new FrameLayout.LayoutParams(i, i2);
        }
        throw C12960it.A0U("Title view parent is of an unsupported type. Provide a custom LayoutParams factory.");
    }

    public final void A04(Rect rect, ViewGroup.MarginLayoutParams marginLayoutParams, AppBarLayout appBarLayout) {
        if (!C28391Mz.A00() || (!C28141Kv.A00(this.A0B))) {
            marginLayoutParams.leftMargin = rect.left;
        } else {
            marginLayoutParams.rightMargin = appBarLayout.getWidth() - rect.right;
        }
        marginLayoutParams.topMargin = rect.top;
    }

    public final void A05(AppBarLayout appBarLayout) {
        Rect rect = (Rect) this.A0E.get();
        ViewGroup.MarginLayoutParams A03 = A03(rect.width(), rect.height());
        A04(rect, A03, appBarLayout);
        TextView textView = this.A09;
        if (textView.getWidth() != rect.width()) {
            textView.setLayoutParams(A03);
        }
    }

    public final void A06(AppBarLayout appBarLayout) {
        Rect rect = (Rect) this.A0G.get();
        ViewGroup.MarginLayoutParams A03 = A03(rect.width(), rect.height());
        A04(rect, A03, appBarLayout);
        TextView textView = this.A0A;
        if (textView.getWidth() != rect.width()) {
            textView.setLayoutParams(A03);
        }
    }

    @Override // X.AbstractC115785Sx
    public void ATD(AppBarLayout appBarLayout, int i) {
        float height = ((float) i) / (((float) appBarLayout.getHeight()) - ((float) appBarLayout.getMinimumHeight()));
        float f = this.A00;
        if (height != f) {
            if (f == 0.0f || f == -1.0f) {
                View view = this.A06;
                view.setPivotX(((float) view.getWidth()) / 2.0f);
                view.setPivotY(((float) view.getHeight()) / 2.0f);
                TextView textView = this.A0A;
                textView.setPivotX(((float) textView.getWidth()) / 2.0f);
                textView.setPivotY(((float) textView.getHeight()) / 2.0f);
                TextView textView2 = this.A09;
                textView2.setPivotX(((float) textView2.getWidth()) / 2.0f);
                textView2.setPivotY(((float) textView2.getHeight()) / 2.0f);
            }
            if (height == 0.0f) {
                View view2 = this.A06;
                view2.setScaleX(1.0f);
                view2.setScaleY(1.0f);
                view2.setTranslationX(0.0f);
                view2.setTranslationY(0.0f);
                TextView textView3 = this.A0A;
                boolean A02 = A02(textView3);
                textView3.setLayoutParams(this.A08);
                textView3.setTextSize(A02 ? 1 : 0, this.A03);
                textView3.setGravity(1);
                TextView textView4 = this.A09;
                boolean A022 = A02(textView4);
                textView4.setLayoutParams(this.A07);
                textView4.setTextSize(A022 ? 1 : 0, this.A02);
                textView4.setGravity(1);
            } else {
                float f2 = -height;
                float f3 = (float) (-i);
                A01(this.A06, 0.0f, 0.0f, AnonymousClass5EX.A00(this.A0J) * f2, f3 + (AnonymousClass5EX.A00(this.A0K) * f2), 1.0f - (AnonymousClass5EX.A00(this.A0I) * f2));
                double d = (double) f2;
                int i2 = 1;
                if (d < 0.5d) {
                    TextView textView5 = this.A0A;
                    if (textView5.getMaxLines() == 1) {
                        boolean A023 = A02(textView5);
                        textView5.setLayoutParams(this.A08);
                        textView5.setTextSize(A023 ? 1 : 0, this.A03);
                        textView5.setGravity(1);
                    }
                    A01(textView5, ((float) textView5.getWidth()) / 2.0f, ((float) textView5.getHeight()) / 2.0f, 0.0f, f3 + (AnonymousClass5EX.A00(this.A0Q) * f2), 1.0f - (AnonymousClass5EX.A00(this.A0P) * f2));
                    textView5.setAlpha(1.0f - (f2 * 2.0f));
                } else {
                    TextView textView6 = this.A0A;
                    if (textView6.getMaxLines() != 1) {
                        textView6.setScaleX(1.0f);
                        textView6.setScaleY(1.0f);
                        textView6.setMaxLines(1);
                        AnonymousClass04D.A09(textView6, this.A04.getDimensionPixelSize(R.dimen.text_general_xsmall), (int) (this.A03 + 0.5f), 1, 0);
                        A06(appBarLayout);
                        int i3 = 51;
                        if (C28141Kv.A00(this.A0B)) {
                            i3 = 53;
                        }
                        textView6.setGravity(i3);
                    }
                    if (!C28141Kv.A00(this.A0B)) {
                        i2 = -1;
                    }
                    float f4 = (f2 - 0.5f) * 2.0f;
                    textView6.setTranslationX((((float) (i2 * textView6.getLeft())) / 4.0f) * (1.0f - f4));
                    textView6.setTranslationY(f3);
                    textView6.setAlpha(f4);
                }
                int i4 = 1;
                int i5 = (d > 0.5d ? 1 : (d == 0.5d ? 0 : -1));
                TextView textView7 = this.A09;
                int maxLines = textView7.getMaxLines();
                if (i5 < 0) {
                    if (maxLines == 1) {
                        boolean A024 = A02(textView7);
                        textView7.setLayoutParams(this.A07);
                        textView7.setTextSize(A024 ? 1 : 0, this.A02);
                        textView7.setGravity(1);
                    }
                    A01(textView7, ((float) textView7.getWidth()) / 2.0f, ((float) textView7.getHeight()) / 2.0f, 0.0f, f3 + (AnonymousClass5EX.A00(this.A0O) * f2), 1.0f - (AnonymousClass5EX.A00(this.A0N) * f2));
                    textView7.setAlpha(1.0f - (f2 * 2.0f));
                } else {
                    if (maxLines != 1) {
                        textView7.setScaleX(1.0f);
                        textView7.setScaleY(1.0f);
                        textView7.setMaxLines(1);
                        AnonymousClass04D.A09(textView7, this.A04.getDimensionPixelSize(R.dimen.text_general_xsmall), (int) (this.A02 + 0.5f), 1, 0);
                        A05(appBarLayout);
                        int i6 = 51;
                        if (C28141Kv.A00(this.A0B)) {
                            i6 = 53;
                        }
                        textView7.setGravity(i6);
                    }
                    if (!C28141Kv.A00(this.A0B)) {
                        i4 = -1;
                    }
                    float f5 = (f2 - 0.5f) * 2.0f;
                    textView7.setTranslationX((((float) (i4 * textView7.getLeft())) / 4.0f) * (1.0f - f5));
                    textView7.setTranslationY(f3);
                    textView7.setAlpha(f5);
                }
            }
        }
        if (height == 0.0f) {
            View view3 = this.A05;
            int height2 = view3.getHeight();
            this.A0G.A00 = null;
            this.A0E.A00 = null;
            if (view3.getLayoutParams().height != height2) {
                view3.getLayoutParams().height = height2;
            }
        } else if (height == -1.0f) {
            View view4 = this.A05;
            int height3 = view4.getHeight();
            this.A0G.A00 = null;
            this.A0E.A00 = null;
            if (view4.getLayoutParams().height != height3) {
                view4.getLayoutParams().height = height3;
            }
            this.A0M.get();
            this.A0L.get();
            A06(appBarLayout);
            A05(appBarLayout);
        }
        this.A00 = height;
        this.A01 = i;
    }
}
