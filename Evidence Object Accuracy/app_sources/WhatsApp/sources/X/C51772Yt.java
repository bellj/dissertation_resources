package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import com.whatsapp.Conversation;
import com.whatsapp.util.Log;

/* renamed from: X.2Yt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51772Yt extends BroadcastReceiver {
    public final /* synthetic */ Conversation A00;

    public C51772Yt(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        try {
            Conversation conversation = this.A00;
            conversation.unregisterReceiver(this);
            Log.i("conversation/reset-ime");
            InputMethodManager A0Q = ((ActivityC13810kN) conversation).A08.A0Q();
            AnonymousClass009.A05(A0Q);
            A0Q.restartInput(conversation.A2w);
            conversation.A06 = null;
        } catch (Exception e) {
            Log.e("conversation/unregister user present receiver ", e);
        }
    }
}
