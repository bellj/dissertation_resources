package X;

import java.util.Random;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: X.2Sv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51102Sv {
    public final C49402Kp A00;
    public final Random A01;
    public final SSLSocketFactory A02;

    public C51102Sv(C49402Kp r4, C18800t4 r5, Random random) {
        AnonymousClass2UX r2;
        synchronized (r5) {
            r2 = r5.A01;
            if (r2 == null) {
                r2 = new AnonymousClass2UX(r5.A06.A00, r5.A08);
                r5.A01 = r2;
            }
        }
        this.A02 = r2;
        this.A00 = r4;
        this.A01 = random;
    }
}
