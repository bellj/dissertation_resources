package X;

/* renamed from: X.1XK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XK extends AbstractC15340mz implements AbstractC16420oz {
    public int A00;

    public AnonymousClass1XK(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 36, j);
    }

    @Override // X.AbstractC15340mz
    public void A0T(int i) {
        if (i != 512) {
            super.A0T(i);
        }
    }

    @Override // X.AbstractC16420oz
    public void A6k(C39971qq r6) {
        AnonymousClass1G3 r4 = r6.A03;
        C57692nT r0 = ((C27081Fy) r4.A00).A0W;
        if (r0 == null) {
            r0 = C57692nT.A0D;
        }
        C56882m6 r3 = (C56882m6) r0.A0T();
        AnonymousClass1G8 r02 = ((C57692nT) r3.A00).A0C;
        if (r02 == null) {
            r02 = AnonymousClass1G8.A05;
        }
        AnonymousClass1G9 r2 = (AnonymousClass1G9) r02.A0T();
        AnonymousClass1IS r1 = this.A0z;
        r2.A07(C15380n4.A03(r1.A00));
        r2.A08(r1.A02);
        r3.A03();
        C57692nT r12 = (C57692nT) r3.A00;
        r12.A0C = (AnonymousClass1G8) r2.A02();
        r12.A00 |= 1;
        int i = this.A00;
        r3.A03();
        C57692nT r13 = (C57692nT) r3.A00;
        r13.A00 |= 4;
        r13.A01 = i;
        r3.A05(EnumC87324Bb.A04);
        r4.A08(r3);
    }
}
