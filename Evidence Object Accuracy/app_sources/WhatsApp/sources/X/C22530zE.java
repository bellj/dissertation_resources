package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.conversation.ConversationListView;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.0zE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22530zE extends AbstractC16220oe {
    public void A05() {
        for (AnonymousClass22S r0 : A01()) {
            ConversationListView conversationListView = r0.A00.A1g;
            conversationListView.post(new RunnableBRunnable0Shape4S0100000_I0_4(conversationListView, 46));
        }
    }

    public void A06() {
        Iterator it = A01().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public void A07(Set set) {
        if (!set.isEmpty()) {
            for (AnonymousClass22S r0 : A01()) {
                ConversationListView conversationListView = r0.A00.A1g;
                conversationListView.post(new RunnableBRunnable0Shape4S0100000_I0_4(conversationListView, 46));
            }
        }
    }
}
