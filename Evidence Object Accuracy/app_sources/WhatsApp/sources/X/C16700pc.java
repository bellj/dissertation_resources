package X;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Arrays;

/* renamed from: X.0pc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16700pc {
    public static View A00(Activity activity, int i) {
        View A05 = AnonymousClass00T.A05(activity, i);
        A0B(A05);
        return A05;
    }

    public static View A01(LayoutInflater layoutInflater, ViewGroup viewGroup, int i) {
        View inflate = layoutInflater.inflate(i, viewGroup, false);
        A0B(inflate);
        return inflate;
    }

    public static View A02(View view, int i) {
        View A0D = AnonymousClass028.A0D(view, i);
        A0B(A0D);
        return A0D;
    }

    public static View A03(View view, int i) {
        View findViewById = view.findViewById(i);
        A0B(findViewById);
        return findViewById;
    }

    public static C27541Hx A04(AnonymousClass017 r0) {
        Object A01 = r0.A01();
        A0C(A01);
        A0B(A01);
        return (C27541Hx) A01;
    }

    public static Object A05(AbstractC16710pd r0) {
        Object value = r0.getValue();
        A0B(value);
        return value;
    }

    public static RuntimeException A06(String str) {
        A0K(str);
        return new RuntimeException("Redex: Unreachable code after no-return invoke");
    }

    public static String A07(String str) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[4];
        String className = stackTraceElement.getClassName();
        String methodName = stackTraceElement.getMethodName();
        StringBuilder sb = new StringBuilder("Parameter specified as non-null is null: method ");
        sb.append(className);
        sb.append(".");
        sb.append(methodName);
        sb.append(", parameter ");
        sb.append(str);
        return sb.toString();
    }

    public static String A08(String str, Object obj) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(obj);
        return sb.toString();
    }

    public static void A09() {
        C27551Hy r0 = new C27551Hy();
        A0M(r0);
        throw r0;
    }

    public static void A0A(Object obj) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("UNKNOWN");
            sb.append(" must not be null");
            IllegalStateException illegalStateException = new IllegalStateException(sb.toString());
            A0M(illegalStateException);
            throw illegalStateException;
        }
    }

    public static void A0B(Object obj) {
        if (obj == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("UNKNOWN");
            sb.append(" must not be null");
            NullPointerException nullPointerException = new NullPointerException(sb.toString());
            A0M(nullPointerException);
            throw nullPointerException;
        }
    }

    public static void A0C(Object obj) {
        if (obj == null) {
            NullPointerException nullPointerException = new NullPointerException();
            A0M(nullPointerException);
            throw nullPointerException;
        }
    }

    public static void A0D(Object obj, int i) {
        if (obj == null) {
            String num = Integer.toString(i);
            StringBuilder sb = new StringBuilder("param at index = ");
            sb.append(num);
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException(A07(sb.toString()));
            A0M(illegalArgumentException);
            throw illegalArgumentException;
        }
    }

    public static void A0E(Object obj, int i) {
        if (obj == null) {
            String num = Integer.toString(i);
            StringBuilder sb = new StringBuilder("param at index = ");
            sb.append(num);
            NullPointerException nullPointerException = new NullPointerException(A07(sb.toString()));
            A0M(nullPointerException);
            throw nullPointerException;
        }
    }

    public static void A0F(Object obj, Object obj2) {
        A0E(obj, 0);
        A0E(obj2, 1);
    }

    public static void A0G(Object obj, Object obj2) {
        A0E(obj, 1);
        A0E(obj2, 2);
    }

    public static void A0H(Object obj, Object obj2) {
        A0E(obj, 2);
        A0E(obj2, 3);
    }

    public static void A0I(Object obj, Object obj2) {
        A0E(obj, 0);
        A0E(obj2, 1);
    }

    public static void A0J(Object obj, Object obj2, Object obj3, Object obj4, Object obj5) {
        A0E(obj, 2);
        A0E(obj2, 3);
        A0E(obj3, 4);
        A0E(obj4, 5);
        A0E(obj5, 6);
    }

    public static void A0K(String str) {
        StringBuilder sb = new StringBuilder("lateinit property ");
        sb.append(str);
        sb.append(" has not been initialized");
        C27561Hz r0 = new C27561Hz(sb.toString());
        A0M(r0);
        throw r0;
    }

    public static void A0L(String str, Throwable th) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            if (str.equals(stackTrace[i2].getClassName())) {
                i = i2;
            }
        }
        th.setStackTrace((StackTraceElement[]) Arrays.copyOfRange(stackTrace, i + 1, length));
    }

    public static void A0M(Throwable th) {
        A0L(C16700pc.class.getName(), th);
    }

    public static boolean A0N(Object obj, Object obj2) {
        A0E(obj, 0);
        A0E(obj2, 1);
        return true;
    }

    public static boolean A0O(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }
}
