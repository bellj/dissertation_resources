package X;

import java.util.ArrayList;

/* renamed from: X.0IT  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0IT extends AnonymousClass03S {
    public static final ArrayList A0C = new ArrayList(5);
    public static final String[] A0D;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public AnonymousClass0PQ A08 = new AnonymousClass0PQ();
    public final C05050Ob A09 = new C05050Ob();
    public final AnonymousClass0Q8 A0A;
    public final int[] A0B = new int[2];

    public abstract C06440Tp A0A(int i, int i2, int i3);

    static {
        String[] strArr = new String[22];
        A0D = strArr;
        int i = 0;
        do {
            strArr[i] = String.valueOf(i);
            i++;
        } while (i <= 21);
    }

    public AnonymousClass0IT(AnonymousClass04Q r2, AnonymousClass0Q8 r3) {
        super(r2);
        this.A0A = r3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:107:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0216  */
    @Override // X.AnonymousClass03S
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A08(android.graphics.Canvas r33) {
        /*
        // Method dump skipped, instructions count: 724
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0IT.A08(android.graphics.Canvas):void");
    }

    @Override // X.AnonymousClass03S
    public void A09(boolean z) {
        super.A09(z);
    }

    public void A0B(int i, int i2, int i3, int i4) {
        String str;
        int i5 = super.A07;
        C06440Tp r4 = new C06440Tp(i5, i5);
        r4.A02 = i;
        r4.A03 = i2;
        r4.A04 = i3;
        r4.A0C = 1;
        this.A0A.A02(r4);
        AnonymousClass0IQ r2 = new AnonymousClass0IQ(this, r4, i, i2, i3, i4);
        if (i3 >= 0) {
            String[] strArr = A0D;
            if (i3 < strArr.length) {
                str = strArr[i3];
                AnonymousClass0UE.A02(r2, str);
            }
        }
        str = "INVALID_ZOOM_LEVEL";
        AnonymousClass0UE.A02(r2, str);
    }
}
