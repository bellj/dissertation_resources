package X;

import android.graphics.Matrix;

/* renamed from: X.0Hc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03260Hc extends AbstractC03490Hz implements AbstractC12130hQ {
    public Matrix A00;

    @Override // X.AnonymousClass0OO
    public String A00() {
        return "group";
    }

    @Override // X.AbstractC12130hQ
    public void Ad4(Matrix matrix) {
        this.A00 = matrix;
    }
}
