package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1uY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C42031uY extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C42031uY A07;
    public static volatile AnonymousClass255 A08;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public AbstractC27881Jp A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;

    static {
        C42031uY r0 = new C42031uY();
        A07 = r0;
        r0.A0W();
    }

    public C42031uY() {
        AbstractC27881Jp r0 = AbstractC27881Jp.A01;
        this.A04 = r0;
        this.A05 = r0;
        this.A06 = r0;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 2) == 2) {
            i2 = 0 + CodedOutputStream.A04(1, this.A01);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A09(this.A04, 2);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A09(this.A05, 3);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A09(this.A06, 4);
        }
        if ((i3 & 1) == 1) {
            i2 += CodedOutputStream.A04(5, this.A02);
        }
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A04(6, this.A03);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(1, this.A01);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A04, 2);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A05, 3);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A06, 4);
        }
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(5, this.A02);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(6, this.A03);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
