package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/* renamed from: X.09x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C020809x extends Drawable.ConstantState {
    public final Drawable.ConstantState A00;

    public C020809x(Drawable.ConstantState constantState) {
        this.A00 = constantState;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public boolean canApplyTheme() {
        return this.A00.canApplyTheme();
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public int getChangingConfigurations() {
        return this.A00.getChangingConfigurations();
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable() {
        AnonymousClass07M r2 = new AnonymousClass07M(null);
        Drawable newDrawable = this.A00.newDrawable();
        ((AbstractC013706k) r2).A00 = newDrawable;
        newDrawable.setCallback(r2.A06);
        return r2;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources) {
        AnonymousClass07M r2 = new AnonymousClass07M(null);
        Drawable newDrawable = this.A00.newDrawable(resources);
        ((AbstractC013706k) r2).A00 = newDrawable;
        newDrawable.setCallback(r2.A06);
        return r2;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources, Resources.Theme theme) {
        AnonymousClass07M r2 = new AnonymousClass07M(null);
        Drawable newDrawable = this.A00.newDrawable(resources, theme);
        ((AbstractC013706k) r2).A00 = newDrawable;
        newDrawable.setCallback(r2.A06);
        return r2;
    }
}
