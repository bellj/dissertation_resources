package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.ArrayList;

/* renamed from: X.5we  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class C128775we {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ BrazilPayBloksActivity A01;

    public /* synthetic */ C128775we(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity) {
        this.A01 = brazilPayBloksActivity;
        this.A00 = r1;
    }

    public final void A00(C30881Ze r9, C452120p r10, ArrayList arrayList, boolean z) {
        BrazilPayBloksActivity brazilPayBloksActivity = this.A01;
        AnonymousClass3FE r3 = this.A00;
        if (r10 == null) {
            brazilPayBloksActivity.A2o(r3, r9, null, arrayList, z);
        } else {
            AbstractActivityC121705jc.A0l(r3, null, r10.A00);
        }
        brazilPayBloksActivity.A0G.A02 = false;
    }
}
