package X;

/* renamed from: X.3B7  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3B7 {
    public static final String A00;
    public static final String A01;
    public static final String A02;
    public static final String A03;
    public static final String A04;

    static {
        StringBuilder A0k = C12960it.A0k("SELECT ");
        String str = C16500p8.A00;
        A0k.append(AnonymousClass1Ux.A01("message", str));
        A0k.append(", ");
        A0k.append("file_size");
        A0k.append(" FROM ");
        A0k.append("message_media AS message_media");
        A0k.append(" JOIN ");
        A0k.append("available_message_view AS message");
        A0k.append(" ON message_media.message_row_id = message._id");
        A0k.append(" JOIN ");
        A0k.append("message_forwarded AS message_forwarded");
        A0k.append(" ON message_forwarded.message_row_id = message_media.message_row_id");
        A0k.append(" WHERE ");
        A0k.append("message_forwarded.forward_score > ?");
        A0k.append(" AND ");
        A0k.append("message_type IN ('2', '1', '25', '3', '28', '13', '29', '9', '26')");
        A01 = C12960it.A0d(" ORDER BY file_size DESC", A0k);
        StringBuilder A0j = C12960it.A0j("SELECT ");
        A0j.append(str);
        A0j.append(" , ");
        C12960it.A1L("message_forwarded.forward_score", " FROM ", "available_message_view AS message", A0j);
        A0j.append(" JOIN ");
        A0j.append("message_forwarded AS message_forwarded");
        A0j.append(" ON message_forwarded.message_row_id = message._id");
        A0j.append(" WHERE ");
        A0j.append("message_forwarded.forward_score >= ?");
        A0j.append(" AND ");
        String A0d = C12960it.A0d("message_type IN ('2', '1', '25', '3', '28', '13', '29', '9', '26')", A0j);
        A04 = C12960it.A0d(" ORDER BY sort_id DESC", C12960it.A0j(A0d));
        A03 = C12960it.A0d(" ORDER BY sort_id ASC", C12960it.A0j(A0d));
        A02 = C12960it.A0d(" ORDER BY media_size DESC", C12960it.A0j(A0d));
        A00 = C12960it.A0d(" ORDER BY message_forwarded.forward_score DESC", C12960it.A0j(A0d));
    }
}
