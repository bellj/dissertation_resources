package X;

import android.content.Intent;

/* renamed from: X.0Xl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C07300Xl implements AbstractC12330hk {
    public final int A00;
    public final Intent A01;
    public final /* synthetic */ AbstractServiceC003801r A02;

    public C07300Xl(Intent intent, AbstractServiceC003801r r2, int i) {
        this.A02 = r2;
        this.A01 = intent;
        this.A00 = i;
    }

    @Override // X.AbstractC12330hk
    public void A7T() {
        this.A02.stopSelf(this.A00);
    }

    @Override // X.AbstractC12330hk
    public Intent getIntent() {
        return this.A01;
    }
}
