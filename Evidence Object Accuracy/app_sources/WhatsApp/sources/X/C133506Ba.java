package X;

import android.content.Intent;
import com.facebook.redex.IDxAListenerShape19S0100000_3_I1;
import com.whatsapp.R;
import com.whatsapp.payments.service.NoviVideoSelfieFgService;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.ArrayList;

/* renamed from: X.6Ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133506Ba implements AnonymousClass6MX {
    public final /* synthetic */ C133536Bd A00;
    public final /* synthetic */ String A01;

    public C133506Ba(C133536Bd r1, String str) {
        this.A00 = r1;
        this.A01 = str;
    }

    @Override // X.AnonymousClass6MX
    public void AY5(int i) {
        NoviVideoSelfieFgService noviVideoSelfieFgService = this.A00.A00;
        noviVideoSelfieFgService.A06 = (noviVideoSelfieFgService.A05 * ((long) i)) / 100;
        NoviVideoSelfieFgService.A00(noviVideoSelfieFgService);
    }

    @Override // X.AnonymousClass6MX
    public void AY6(C452120p r11, String str, String str2, boolean z) {
        StringBuilder A0k = C12960it.A0k("VideoSelfie video upload success? ");
        A0k.append(z);
        A0k.append(" videoHandle: ");
        A0k.append(str);
        A0k.append(" videoMimetype: ");
        Log.i(C12960it.A0d(str2, A0k));
        if (!z || str == null) {
            Intent intent = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
            intent.putExtra("extra_event_type", "extra_event_upload_failed");
            NoviVideoSelfieFgService noviVideoSelfieFgService = this.A00.A00;
            C117305Zk.A0y(noviVideoSelfieFgService, intent);
            noviVideoSelfieFgService.A04(noviVideoSelfieFgService.getString(R.string.novi_selfie_upload_media_failed));
            return;
        }
        C128365vz r2 = new AnonymousClass610("SELFI_VIDEO_UPLOAD_STATUS", "SELFIE_UPLOAD", "MODAL").A00;
        r2.A0Q = "SUCCESSFUL";
        r2.A0g = "STEP_UP_SELFIE";
        NoviVideoSelfieFgService noviVideoSelfieFgService2 = this.A00.A00;
        r2.A0L = noviVideoSelfieFgService2.getResources().getString(R.string.novi_selfie_review_submit);
        r2.A0V = str;
        C1316663q r1 = noviVideoSelfieFgService2.A0C;
        if (r1 != null) {
            r2.A0E = r1.A02;
            r2.A0K = noviVideoSelfieFgService2.A0E;
            r2.A0f = r1.A03;
        }
        if (noviVideoSelfieFgService2.A01 != 10) {
            noviVideoSelfieFgService2.A0A.A05(r2);
        }
        String str3 = this.A01;
        String str4 = noviVideoSelfieFgService2.A0E;
        String str5 = noviVideoSelfieFgService2.A0F;
        Intent intent2 = new Intent("NoviReviewVideoSelfieActivity.selfie_service_events");
        intent2.putExtra("extra_event_type", "extra_event_answer_selfie_begin");
        C117305Zk.A0y(noviVideoSelfieFgService2, intent2);
        AnonymousClass61S[] r4 = new AnonymousClass61S[2];
        r4[0] = AnonymousClass61S.A00("action", "novi-answer-video-selfie-step-up-challenge");
        C1310460z A0F = C117295Zj.A0F(AnonymousClass61S.A00("disable_face_rec", str5), r4, 1);
        AnonymousClass61S[] r7 = new AnonymousClass61S[2];
        C1316663q r5 = noviVideoSelfieFgService2.A0C;
        AnonymousClass61S.A05("entry_flow", r5.A03, r7, 0);
        C1310460z A0B = C117315Zl.A0B("step_up", C12960it.A0m(AnonymousClass61S.A00("metadata", r5.A04), r7, 1));
        ArrayList arrayList = A0F.A02;
        arrayList.add(A0B);
        C117305Zk.A1R("step_up_challenge", arrayList, AnonymousClass61S.A02("challenge_id", str4));
        C117305Zk.A1R("video", arrayList, AnonymousClass61S.A02("handle", str));
        C117305Zk.A1R("image", arrayList, AnonymousClass61S.A02("handle", str3));
        C130155yt.A02(new IDxAListenerShape19S0100000_3_I1(noviVideoSelfieFgService2, 8), noviVideoSelfieFgService2.A09, A0F, noviVideoSelfieFgService2.A01);
        C128615wO r22 = noviVideoSelfieFgService2.A0D;
        File A00 = r22.A00("selfie.mp4");
        File A002 = r22.A00("selfie.jpeg");
        if (A00 != null) {
            A00.delete();
        }
        if (A002 != null) {
            A002.delete();
        }
    }
}
