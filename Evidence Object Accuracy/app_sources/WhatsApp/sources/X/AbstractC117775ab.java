package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.whatsapp.payments.ui.widget.PaymentInteropShimmerRow;

/* renamed from: X.5ab  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC117775ab extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC117775ab(Context context) {
        super(context);
        A00();
    }

    public AbstractC117775ab(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00();
    }

    public AbstractC117775ab(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00();
    }

    public void A00() {
        if (!this.A01) {
            this.A01 = true;
            PaymentInteropShimmerRow paymentInteropShimmerRow = (PaymentInteropShimmerRow) this;
            AnonymousClass01J r1 = ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06;
            paymentInteropShimmerRow.A03 = C117305Zk.A0P(r1);
            paymentInteropShimmerRow.A04 = (AnonymousClass1A8) r1.AER.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
