package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;

/* renamed from: X.1Pd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28821Pd extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ View A00;
    public final /* synthetic */ RunnableBRunnable0Shape0S0400000_I0 A01;

    public C28821Pd(View view, RunnableBRunnable0Shape0S0400000_I0 runnableBRunnable0Shape0S0400000_I0) {
        this.A01 = runnableBRunnable0Shape0S0400000_I0;
        this.A00 = view;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        View view = this.A00;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = -2;
        view.setLayoutParams(layoutParams);
        view.setVisibility(8);
    }
}
