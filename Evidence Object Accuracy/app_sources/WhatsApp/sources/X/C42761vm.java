package X;

import com.whatsapp.util.Log;

/* renamed from: X.1vm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C42761vm extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ AnonymousClass1J7 $callback;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C42761vm(AnonymousClass1J7 r2) {
        super(1);
        this.$callback = r2;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        Throwable th = (Throwable) obj;
        C16700pc.A0E(th, 0);
        Log.e(th);
        this.$callback.AJ4(Boolean.FALSE);
        return AnonymousClass1WZ.A00;
    }
}
