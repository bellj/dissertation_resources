package X;

import android.app.Person;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import androidx.core.graphics.drawable.IconCompat;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.1xu */
/* loaded from: classes2.dex */
public class C43951xu {
    public static final int A00;
    public static final int A01;
    public static final int A02;
    public static final int A03;
    public static final String A04;
    public static final Set A05 = new AnonymousClass5II();

    public static int A00(Context context) {
        if (Build.VERSION.SDK_INT < 25) {
            return 8;
        }
        return C007703y.A00(context);
    }

    public static ShortcutInfo A01(Context context, AnonymousClass130 r6, C15550nR r7, C15610nY r8, AnonymousClass131 r9, C15370n3 r10, int i) {
        Log.i("WaShortcutsApiHelper/createShortcutForContact");
        Jid jid = r10.A0D;
        AnonymousClass009.A05(jid);
        ShortcutInfo.Builder rank = new ShortcutInfo.Builder(context, jid.getRawString()).setShortLabel(r8.A04(r10)).setCategories(A05).setLongLived(true).setRank(i);
        Intent A0i = new C14960mK().A0i(context, (AbstractC14640lm) r10.A0B(AbstractC14640lm.class));
        C35741ib.A01(A0i, "WaShortcutsHelper");
        ShortcutInfo.Builder intent = rank.setIntent(A0i.setAction("android.intent.action.VIEW"));
        Bitmap A012 = r9.A01(context, r10, 0.0f, 72);
        if (A012 == null) {
            A012 = AnonymousClass130.A00(r6.A01.A00, 0.0f, r6.A01(r10), 72);
        }
        intent.setIcon(Icon.createWithAdaptiveBitmap(A02(A012)));
        if (C15380n4.A0L(r10.A0D)) {
            intent.setPerson(new Person.Builder().setName(r8.A04(r10)).setUri(A06(context, r7, r10)).build());
        }
        return intent.build();
    }

    public static Bitmap A02(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(A03, A03, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        RectF rectF = new RectF(0.0f, 0.0f, 108.0f, 108.0f);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        paint.setColor(-1);
        canvas.drawRect(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, ((float) (canvas.getWidth() - bitmap.getWidth())) / 2.0f, ((float) (canvas.getHeight() - bitmap.getHeight())) / 2.0f, paint);
        return createBitmap;
    }

    public static C007303s A03(Context context, C15550nR r3, C15610nY r4, C15370n3 r5) {
        C007403t r1 = new C007403t();
        r1.A01 = r4.A04(r5);
        r1.A03 = A06(context, r3, r5);
        return new C007303s(r1);
    }

    public static C007603x A04(Context context, AnonymousClass130 r8, C15550nR r9, C15610nY r10, AnonymousClass131 r11, C15370n3 r12, int i) {
        Log.i("WaShortcutsApiHelper/createShortcutCompatForContact");
        Jid jid = r12.A0D;
        AnonymousClass009.A05(jid);
        AnonymousClass03w r4 = new AnonymousClass03w(context, jid.getRawString());
        String A042 = r10.A04(r12);
        C007603x r3 = r4.A00;
        r3.A0B = A042;
        r3.A0F = A05;
        r3.A0N = true;
        r3.A02 = i;
        Intent A0i = new C14960mK().A0i(context, (AbstractC14640lm) r12.A0B(AbstractC14640lm.class));
        C35741ib.A01(A0i, "WaShortcutsHelper");
        r3.A0P = new Intent[]{A0i.setAction("android.intent.action.VIEW")};
        Bitmap A012 = r11.A01(context, r12, 0.0f, 72);
        if (A012 == null) {
            A012 = AnonymousClass130.A00(r8.A01.A00, 0.0f, r8.A01(r12), 72);
        }
        Bitmap A022 = A02(A012);
        IconCompat iconCompat = new IconCompat(5);
        iconCompat.A06 = A022;
        r3.A09 = iconCompat;
        if (C15380n4.A0L(r12.A0D)) {
            r3.A0Q = new C007303s[]{A03(context, r9, r10, r12)};
        }
        return r4.A00();
    }

    public static C007603x A05(String str, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C007603x r1 = (C007603x) it.next();
            if (r1.A0D.equals(str)) {
                return r1;
            }
        }
        return null;
    }

    public static String A06(Context context, C15550nR r1, C15370n3 r2) {
        Uri A052 = r1.A05(context.getContentResolver(), r2);
        if (A052 != null) {
            return A052.toString();
        }
        return null;
    }

    public static List A07(C238013b r5, C15550nR r6, C19990v2 r7, C20830wO r8, C19780uf r9, C15600nX r10) {
        Log.i("WaShortcutsApiHelper/getFrequentContacts");
        ArrayList arrayList = new ArrayList();
        for (AbstractC14640lm r3 : r9.A01(null)) {
            C15370n3 A0A = r6.A0A(r3);
            if (A0A != null && !r5.A0I(UserJid.of(r3)) && !r7.A0F(r3) && !C15380n4.A0M(r3) && !C15380n4.A0N(r3) && (!A0A.A0K() || r10.A0C((GroupJid) r3))) {
                arrayList.add(A0A);
            }
        }
        boolean isEmpty = arrayList.isEmpty();
        ArrayList arrayList2 = arrayList;
        if (isEmpty) {
            Log.i("WaShortcutsApiHelper/getFrequentContacts/get N contacts");
            List A032 = r8.A03(20);
            boolean isEmpty2 = A032.isEmpty();
            arrayList2 = A032;
            if (isEmpty2) {
                Log.i("WaShortcutsApiHelper/getFrequentContacts/get contact picker list");
                r6.A06.A0S(A032, 0, false, false);
                arrayList2 = A032;
            }
        }
        return arrayList2;
    }

    public static void A08(Context context) {
        C007703y.A05(context);
    }

    public static void A09(Context context) {
        ShortcutManager shortcutManager = (ShortcutManager) context.getSystemService(ShortcutManager.class);
        List<ShortcutInfo> shortcuts = shortcutManager.getShortcuts(8);
        ArrayList arrayList = new ArrayList();
        for (ShortcutInfo shortcutInfo : shortcuts) {
            arrayList.add(shortcutInfo.getId());
        }
        shortcutManager.removeLongLivedShortcuts(arrayList);
    }

    public static synchronized void A0D(Context context, AbstractC15710nm r15, C238013b r16, AnonymousClass130 r17, C15550nR r18, C15610nY r19, AnonymousClass131 r20, C15890o4 r21, C19990v2 r22, C20830wO r23, C19780uf r24, C15600nX r25) {
        synchronized (C43951xu.class) {
            List A07 = A07(r16, r18, r22, r23, r24, r25);
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            if (r21.A00.A00.checkCallingOrSelfPermission("android.permission.CAMERA") == 0) {
                z = true;
            }
            if (z) {
                AnonymousClass03w r6 = new AnonymousClass03w(context, "open_camera");
                String string = context.getString(R.string.shortcut_camera);
                C007603x r5 = r6.A00;
                r5.A0B = string;
                r5.A09 = IconCompat.A02(context.getResources(), context.getPackageName(), R.drawable.ic_shortcut_camera_alt);
                Intent intent = new Intent();
                intent.setClassName(context.getPackageName(), "com.whatsapp.camera.LauncherCameraActivity");
                r5.A0P = new Intent[]{intent.setAction("android.intent.action.VIEW")};
                arrayList.add(r6.A00());
            }
            int A002 = A00(context);
            for (int i = 0; i < A07.size(); i++) {
                arrayList.add(A04(context, r17, r18, r19, r20, (C15370n3) A07.get(i), i));
                if (A002 != arrayList.size()) {
                }
            }
            try {
                A0J(context, arrayList);
            } catch (IllegalArgumentException | IllegalStateException | SecurityException e) {
                r15.AaV("WaShortcutsHelper/rebuildDynamicShortcuts", null, true);
                Log.w("WaShortcutsHelper/exception happened. ", e);
            }
        }
    }

    public static synchronized void A0F(Context context, AnonymousClass130 r12, C15550nR r13, C15610nY r14, AnonymousClass131 r15, C15370n3 r16) {
        synchronized (C43951xu.class) {
            List A032 = C007703y.A03(context);
            Jid jid = r16.A0D;
            AnonymousClass009.A05(jid);
            if (A0K(A05(jid.getRawString(), A032), r14, r16)) {
                Log.i("WaShortcutsApiHelper/publishShortcut/shortcut already published");
            } else {
                ((ShortcutManager) context.getSystemService(ShortcutManager.class)).pushDynamicShortcut(A01(context, r12, r13, r14, r15, r16, Math.min(A032.size(), A00(context))));
            }
        }
    }

    public static void A0G(Context context, C15370n3 r3) {
        ShortcutManager shortcutManager = (ShortcutManager) context.getSystemService(ShortcutManager.class);
        ArrayList arrayList = new ArrayList();
        Jid jid = r3.A0D;
        AnonymousClass009.A05(jid);
        arrayList.add(jid.getRawString());
        if (shortcutManager != null) {
            shortcutManager.disableShortcuts(arrayList);
        }
    }

    public static void A0H(Context context, AbstractC14640lm r4) {
        String rawString = r4.getRawString();
        ShortcutManager shortcutManager = (ShortcutManager) context.getSystemService(ShortcutManager.class);
        for (ShortcutInfo shortcutInfo : shortcutManager.getShortcuts(8)) {
            if (shortcutInfo.getId().equals(rawString)) {
                shortcutManager.removeLongLivedShortcuts(Collections.singletonList(shortcutInfo.getId()));
                return;
            }
        }
    }

    public static void A0J(Context context, List list) {
        Log.i("WaShortcutsApiHelper/rebuild shortcut lists");
        C007703y.A05(context);
        int A002 = A00(context);
        if (list.size() > A002) {
            list = list.subList(0, A002);
        }
        C007703y.A07(context, list);
    }

    public static boolean A0K(C007603x r1, C15610nY r2, C15370n3 r3) {
        return r1 != null && r1.A0B.toString().equals(r2.A04(r3));
    }
}
