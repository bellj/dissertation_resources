package X;

import java.util.UUID;

/* renamed from: X.4Lx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C89924Lx {
    public final UUID A00;
    public final byte[] A01;

    public C89924Lx(UUID uuid, byte[] bArr) {
        this.A00 = uuid;
        this.A01 = bArr;
    }
}
