package X;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* renamed from: X.3gW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73613gW extends GestureDetector.SimpleOnGestureListener {
    public final /* synthetic */ C54692h8 A00;
    public final /* synthetic */ AnonymousClass4JZ A01;

    public C73613gW(C54692h8 r1, AnonymousClass4JZ r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        AnonymousClass4JZ r0 = this.A01;
        if (r0 == null) {
            return false;
        }
        r0.A00.A2i(true);
        return true;
    }
}
