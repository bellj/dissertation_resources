package X;

/* renamed from: X.0uZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC19720uZ extends AbstractC17770rM implements AbstractC19700uX, AbstractC17920rc {
    public AnonymousClass3FJ A00;
    public final C19680uV A01;

    public AbstractC19720uZ(C19680uV r1, C19630uQ r2) {
        super(r2);
        this.A01 = r1;
    }

    @Override // X.AbstractC17920rc
    public void AIu(String str) {
        AnonymousClass01J r1 = this.A01.A00.A01;
        this.A00 = new AnonymousClass3FJ((C16590pI) r1.AMg.get(), this, (C17120qI) r1.ALs.get(), str);
    }
}
