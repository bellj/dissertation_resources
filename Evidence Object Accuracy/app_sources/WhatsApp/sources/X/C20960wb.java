package X;

import android.content.Context;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FilenameFilter;

/* renamed from: X.0wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20960wb implements AbstractC16990q5 {
    public final C16590pI A00;

    @Override // X.AbstractC16990q5
    public /* synthetic */ void AOp() {
    }

    public C20960wb(C16590pI r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC16990q5
    public void AOo() {
        Context context = this.A00.A00;
        File filesDir = context.getFilesDir();
        String[] list = filesDir.list(new FilenameFilter() { // from class: X.5BM
            @Override // java.io.FilenameFilter
            public final boolean accept(File file, String str) {
                return str.endsWith(".pack");
            }
        });
        if (list != null) {
            for (String str : list) {
                String[] split = str.substring(0, str.lastIndexOf(".pack")).split("_");
                int length = split.length;
                if (length > 1) {
                    try {
                        if (Long.parseLong(split[length - 1]) != new File(context.getPackageCodePath()).lastModified() / 1000) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("translations/cleanupOldPackFiles Clearing old pack file: ");
                            sb.append(str);
                            Log.i(sb.toString());
                            if (!new File(filesDir, str).delete()) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("translations/cleanupOldPackFiles Could not delete old pack file: ");
                                sb2.append(str);
                                Log.e(sb2.toString());
                            }
                        }
                    } catch (NumberFormatException unused) {
                        StringBuilder sb3 = new StringBuilder("translations/cleanupOldPackFiles Pack file name did not contain version info: ");
                        sb3.append(str);
                        Log.w(sb3.toString());
                    }
                }
            }
        }
    }
}
