package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.community.CommunityTabViewModel;

/* renamed from: X.428  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass428 extends AbstractC40571ro {
    public final /* synthetic */ CommunityTabViewModel A00;

    public AnonymousClass428(CommunityTabViewModel communityTabViewModel) {
        this.A00 = communityTabViewModel;
    }

    @Override // X.AbstractC40571ro
    public void A00(AbstractC14640lm r4, AnonymousClass1IS r5) {
        this.A00.A0O.execute(new RunnableBRunnable0Shape11S0200000_I1_1(this, 7, r4));
    }
}
