package X;

/* renamed from: X.3zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84723zn extends AbstractC92594Wn {
    public final String A00;

    public C84723zn(AbstractC116555Vx r1, String str, boolean z) {
        super(r1, z);
        this.A00 = str;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C84723zn r5 = (C84723zn) obj;
            if (!this.A00.equals(r5.A00) || r5.A01 != this.A01) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return this.A00.hashCode();
    }
}
