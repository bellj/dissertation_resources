package X;

import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.text.Layout;
import android.text.TextUtils;
import android.text.style.LineBackgroundSpan;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.0Vt  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Vt implements LineBackgroundSpan {
    public final Paint A00;
    public final List A01;

    public AnonymousClass0Vt(Layout layout, int i, int i2, int i3, int i4, int i5, int i6) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i7 = 0; i7 < layout.getLineCount(); i7++) {
            RectF rectF = new RectF(layout.getLineLeft(i7), (float) layout.getLineTop(i7), layout.getLineRight(i7), (float) layout.getLineBottom(i7));
            String charSequence = layout.getText().subSequence(layout.getLineStart(i7), layout.getLineEnd(i7)).toString();
            if (rectF.width() > 0.0f && !TextUtils.isEmpty(charSequence.replace("\n", ""))) {
                arrayList2.add(rectF);
            } else if (!arrayList2.isEmpty()) {
                arrayList.add(arrayList2);
                arrayList2 = new ArrayList();
            }
        }
        if (!arrayList2.isEmpty()) {
            arrayList.add(arrayList2);
        }
        ArrayList arrayList3 = new ArrayList();
        for (int i8 = 0; i8 < arrayList.size(); i8++) {
            List list = (List) arrayList.get(i8);
            Path path = new Path();
            arrayList3.add(path);
            int size = list.size();
            int i9 = size << 1;
            PointF[] pointFArr = new PointF[i9];
            PointF[] pointFArr2 = new PointF[i9];
            for (int i10 = 0; i10 <= size - 1; i10++) {
                RectF rectF2 = (RectF) list.get(i10);
                int i11 = i10 << 1;
                int i12 = i11 + 1;
                float f = (float) i3;
                float f2 = (float) i4;
                pointFArr2[i11] = new PointF(rectF2.right + f, rectF2.top - f2);
                float f3 = rectF2.right + f;
                float f4 = (float) i5;
                pointFArr2[i12] = new PointF(f3, rectF2.bottom + f4);
                float f5 = (float) i2;
                pointFArr[i11] = new PointF(rectF2.left - f5, rectF2.top - f2);
                pointFArr[i12] = new PointF(rectF2.left - f5, rectF2.bottom + f4);
            }
            for (int i13 = 1; i13 < pointFArr2.length; i13++) {
                PointF pointF = pointFArr2[i13];
                PointF pointF2 = pointFArr2[i13 - 1];
                float f6 = pointF.x;
                float f7 = pointF2.x;
                if (f6 > f7) {
                    pointF2.y = pointF.y;
                } else if (f6 < f7) {
                    pointF.y = pointF2.y;
                }
            }
            for (int i14 = 1; i14 < pointFArr.length; i14++) {
                PointF pointF3 = pointFArr[i14];
                PointF pointF4 = pointFArr[i14 - 1];
                float f8 = pointF3.x;
                float f9 = pointF4.x;
                if (f8 > f9) {
                    pointF3.y = pointF4.y;
                } else if (f8 < f9) {
                    pointF4.y = pointF3.y;
                }
            }
            float f10 = (float) i6;
            List A00 = A00(pointFArr2, f10, true);
            List A002 = A00(pointFArr, f10, false);
            path.moveTo(((PointF) A00.get(0)).x, ((PointF) A00.get(0)).y);
            for (int i15 = 1; i15 < A00.size(); i15++) {
                path.lineTo(((PointF) A00.get(i15)).x, ((PointF) A00.get(i15)).y);
            }
            for (int size2 = A002.size() - 1; size2 >= 0; size2--) {
                path.lineTo(((PointF) A002.get(size2)).x, ((PointF) A002.get(size2)).y);
            }
            path.close();
        }
        this.A01 = arrayList3;
        Paint paint = new Paint(1);
        this.A00 = paint;
        paint.setColor(i);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setPathEffect(new CornerPathEffect((float) i6));
    }

    public static List A00(PointF[] pointFArr, float f, boolean z) {
        float min;
        ArrayList arrayList = new ArrayList();
        Collections.addAll(arrayList, pointFArr);
        int i = 0;
        while (i < (arrayList.size() >> 1) - 1) {
            int i2 = i << 1;
            PointF pointF = (PointF) arrayList.get(i2);
            PointF pointF2 = (PointF) arrayList.get(i2 + 1);
            PointF pointF3 = (PointF) arrayList.get(i2 + 2);
            PointF pointF4 = (PointF) arrayList.get(i2 + 3);
            if (Math.abs(pointF2.x - pointF3.x) < f) {
                arrayList.remove(pointF2);
                arrayList.remove(pointF3);
                float f2 = pointF.x;
                float f3 = pointF4.x;
                if (z) {
                    min = Math.max(f2, f3);
                } else {
                    min = Math.min(f2, f3);
                }
                pointF4.x = min;
                pointF.x = min;
                i--;
            }
            i++;
        }
        return arrayList;
    }

    @Override // android.text.style.LineBackgroundSpan
    public void drawBackground(Canvas canvas, Paint paint, int i, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6, int i7, int i8) {
        for (Path path : this.A01) {
            canvas.drawPath(path, this.A00);
        }
    }
}
