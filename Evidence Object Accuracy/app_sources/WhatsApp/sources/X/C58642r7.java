package X;

import android.transition.Transition;
import android.view.animation.AlphaAnimation;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;

/* renamed from: X.2r7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58642r7 extends AbstractC100714mM {
    public final /* synthetic */ MediaComposerActivity A00;

    public C58642r7(MediaComposerActivity mediaComposerActivity) {
        this.A00 = mediaComposerActivity;
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionEnd(Transition transition) {
        MediaComposerActivity mediaComposerActivity = this.A00;
        if (mediaComposerActivity.A02.getVisibility() != 8) {
            mediaComposerActivity.A02.setVisibility(8);
            AlphaAnimation A0R = C12980iv.A0R();
            A0R.setDuration(200);
            mediaComposerActivity.A02.startAnimation(A0R);
        }
        MediaComposerFragment A2g = mediaComposerActivity.A2g();
        if (A2g != null) {
            A2g.A1A();
        }
    }

    @Override // X.AbstractC100714mM, android.transition.Transition.TransitionListener
    public void onTransitionStart(Transition transition) {
        MediaComposerFragment A2g = this.A00.A2g();
        if (A2g != null) {
            A2g.A1B();
        }
    }
}
