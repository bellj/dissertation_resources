package X;

/* renamed from: X.30I  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30I extends AbstractC16110oT {
    public Boolean A00;
    public Long A01;
    public Long A02;

    public AnonymousClass30I() {
        super(3046, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAdvIdentityTimestampInvalid {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "advIdentityIncomingTimestampInHours", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "advIdentityLocalTimestampInHours", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "advIdentityPrimaryIdentityMatch", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
