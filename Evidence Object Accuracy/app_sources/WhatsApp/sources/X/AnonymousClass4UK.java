package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.4UK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4UK {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public long A05;
    public long A06;
    public long A07;
    public long A08;
    public long A09;
    public UserJid A0A;
    public Boolean A0B;
    public Boolean A0C;
    public Boolean A0D;
    public Boolean A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Long A0I;
    public Long A0J;
    public String A0K;
    public String A0L;
    public boolean A0M;
}
