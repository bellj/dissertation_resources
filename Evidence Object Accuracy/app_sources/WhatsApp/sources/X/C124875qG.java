package X;

import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.ECGenParameterSpec;

/* renamed from: X.5qG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124875qG {
    public static PublicKey A00() {
        try {
            KeyPairGenerator instance = KeyPairGenerator.getInstance("EC", "AndroidKeyStore");
            KeyGenParameterSpec.Builder userAuthenticationRequired = new KeyGenParameterSpec.Builder("payment_bio_key_alias", 4).setDigests("SHA-256").setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1")).setUserAuthenticationRequired(true);
            int i = Build.VERSION.SDK_INT;
            if (i >= 30) {
                userAuthenticationRequired.setUserAuthenticationParameters(0, 2);
            } else {
                userAuthenticationRequired.setUserAuthenticationValidityDurationSeconds(-1);
            }
            if (i >= 24) {
                userAuthenticationRequired.setInvalidatedByBiometricEnrollment(true);
            }
            instance.initialize(userAuthenticationRequired.build());
            return instance.generateKeyPair().getPublic();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(C12960it.A0f(C12960it.A0k("FingerprintHelper/generateKey generateKey: api="), Build.VERSION.SDK_INT), e);
        }
    }
}
