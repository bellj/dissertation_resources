package X;

import android.util.SparseArray;
import com.whatsapp.R;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3G5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3G5 {
    public static AnonymousClass4TT A00(AnonymousClass4Yz r13, C14260l7 r14, AnonymousClass4TT r15, C94244bU r16, AnonymousClass28D r17, AbstractC116425Vj r18, C90764Pd r19) {
        AnonymousClass28D r0;
        SparseArray sparseArray = r14.A01;
        AbstractC17450qp r11 = (AbstractC17450qp) sparseArray.get(R.id.bk_context_key_interpreter_extensions);
        if (r11 == null) {
            r11 = C65093Ic.A00().A0A;
        }
        AnonymousClass3H7 r4 = new AnonymousClass3H7(r13, r15, r16, (AnonymousClass3CV) r14.A02(R.id.bk_context_key_scoped_client_id_mapper), r18, r19, r11, (String) sparseArray.get(R.id.bk_context_key_logging_id));
        HashSet A12 = C12970iu.A12();
        HashMap A11 = C12970iu.A11();
        if (r15 == null) {
            r0 = null;
        } else {
            r0 = r15.A01;
        }
        AnonymousClass28D A01 = A01(r4, r17, r0, A11, A12);
        HashMap hashMap = new HashMap(A12.size());
        Iterator it = A12.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            hashMap.put(next, r4.A01.A03.get(next));
        }
        Map map = r4.A01.A03;
        Map map2 = r4.A0A;
        return new AnonymousClass4TT(r4.A04, r17, A01, r4.A09, map, map2, hashMap);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:189:0x0212 */
    /* JADX DEBUG: Multi-variable search result rejected for r13v6, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r13v4 */
    /* JADX WARN: Type inference failed for: r13v5, types: [X.28D, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r13v7, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x02e5, code lost:
        if (r34.A04 != r7) goto L_0x02e7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AnonymousClass28D A01(X.AnonymousClass3H7 r32, X.AnonymousClass28D r33, X.AnonymousClass28D r34, java.util.Map r35, java.util.Set r36) {
        /*
        // Method dump skipped, instructions count: 877
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3G5.A01(X.3H7, X.28D, X.28D, java.util.Map, java.util.Set):X.28D");
    }
}
