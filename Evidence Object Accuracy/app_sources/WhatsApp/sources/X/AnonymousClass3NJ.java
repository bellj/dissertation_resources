package X;

import android.graphics.Matrix;
import android.view.View;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.3NJ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass3NJ implements View.OnTouchListener {
    public final Matrix A00 = C13000ix.A01();
    public final PhotoView A01;
    public final AbstractC16130oV A02;

    public AnonymousClass3NJ(PhotoView photoView, AbstractC16130oV r3) {
        this.A02 = r3;
        this.A01 = photoView;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9  */
    @Override // android.view.View.OnTouchListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r10, android.view.MotionEvent r11) {
        /*
            r9 = this;
            int r0 = r11.getActionMasked()
            r2 = 1
            if (r0 != r2) goto L_0x0061
            com.whatsapp.mediaview.PhotoView r8 = r9.A01
            android.graphics.Bitmap r7 = r8.getPhoto()
            if (r7 == 0) goto L_0x0070
            android.graphics.Matrix r0 = r8.getImageMatrix()
            android.graphics.Matrix r5 = r9.A00
            r0.invert(r5)
            r6 = 2
            float[] r4 = new float[r6]
            float r1 = r11.getRawX()
            int r0 = r8.getLeft()
            float r0 = (float) r0
            float r1 = r1 - r0
            r3 = 0
            r4[r3] = r1
            float r1 = r11.getRawY()
            int r0 = r8.getTop()
            float r0 = (float) r0
            float r1 = r1 - r0
            r4[r2] = r1
            float[] r1 = new float[r6]
            int r0 = r7.getWidth()
            float r0 = (float) r0
            r1[r3] = r0
            int r0 = r7.getHeight()
            float r0 = (float) r0
            r1[r2] = r0
            r5.mapPoints(r4)
            X.0oV r0 = r9.A02
            com.whatsapp.InteractiveAnnotation r4 = X.AnonymousClass3GA.A01(r0, r4, r1)
            if (r4 == 0) goto L_0x0070
            r3 = r9
            boolean r0 = r9 instanceof X.AnonymousClass33Y
            if (r0 != 0) goto L_0x0069
            boolean r0 = r9 instanceof X.AnonymousClass33X
            if (r0 != 0) goto L_0x0062
            X.33W r3 = (X.AnonymousClass33W) r3
            com.whatsapp.mediaview.MediaViewFragment r1 = r3.A00
            com.whatsapp.mediaview.PhotoView r0 = r3.A01
        L_0x005e:
            com.whatsapp.mediaview.MediaViewFragment.A03(r4, r1, r0)
        L_0x0061:
            return r2
        L_0x0062:
            X.33X r3 = (X.AnonymousClass33X) r3
            com.whatsapp.mediaview.MediaViewFragment r1 = r3.A00
            com.whatsapp.mediaview.PhotoView r0 = r3.A01
            goto L_0x005e
        L_0x0069:
            X.33Y r3 = (X.AnonymousClass33Y) r3
            com.whatsapp.mediaview.MediaViewFragment r1 = r3.A00
            com.whatsapp.mediaview.PhotoView r0 = r3.A01
            goto L_0x005e
        L_0x0070:
            r1 = r9
            boolean r0 = r9 instanceof X.AnonymousClass33Y
            if (r0 != 0) goto L_0x008a
            boolean r0 = r9 instanceof X.AnonymousClass33X
            if (r0 != 0) goto L_0x0095
            X.33W r1 = (X.AnonymousClass33W) r1
            int r0 = r11.getActionMasked()
            if (r0 != r2) goto L_0x0061
            com.whatsapp.mediaview.MediaViewFragment r1 = r1.A00
            boolean r0 = r1.A0G
            r0 = r0 ^ r2
            r1.A1M(r0, r2)
            return r2
        L_0x008a:
            X.33Y r1 = (X.AnonymousClass33Y) r1
            int r0 = r11.getActionMasked()
            if (r0 != r2) goto L_0x0061
            com.whatsapp.videoplayback.ExoPlaybackControlView r1 = r1.A02
            goto L_0x009f
        L_0x0095:
            X.33X r1 = (X.AnonymousClass33X) r1
            int r0 = r11.getActionMasked()
            if (r0 != r2) goto L_0x0061
            com.whatsapp.videoplayback.ExoPlaybackControlView r1 = r1.A02
        L_0x009f:
            boolean r0 = r1.A07()
            if (r0 == 0) goto L_0x00a9
            r1.A00()
            return r2
        L_0x00a9:
            r1.A01()
            r0 = 3000(0xbb8, float:4.204E-42)
            r1.A06(r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3NJ.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
