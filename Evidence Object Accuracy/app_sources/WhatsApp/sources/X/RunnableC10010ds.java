package X;

import android.view.MenuItem;

/* renamed from: X.0ds  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10010ds implements Runnable {
    public final /* synthetic */ MenuItem A00;
    public final /* synthetic */ AnonymousClass0XT A01;
    public final /* synthetic */ AnonymousClass0NH A02;
    public final /* synthetic */ AnonymousClass07H A03;

    public RunnableC10010ds(MenuItem menuItem, AnonymousClass0XT r2, AnonymousClass0NH r3, AnonymousClass07H r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = menuItem;
        this.A03 = r4;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0NH r1 = this.A02;
        if (r1 != null) {
            AnonymousClass0CM r2 = this.A01.A00;
            r2.A0D = true;
            r1.A01.A0F(false);
            r2.A0D = false;
        }
        MenuItem menuItem = this.A00;
        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
            this.A03.A0K(menuItem, null, 4);
        }
    }
}
