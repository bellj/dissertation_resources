package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.5wp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128885wp {
    public final String A00;
    public final String A01;
    public final List A02;

    public C128885wp(AnonymousClass1V8 r11) {
        String A0H = r11.A0H("default_validation_regex");
        String A0H2 = r11.A0H("error_message");
        List A0J = r11.A0J("validation_rule");
        ArrayList A0l = C12960it.A0l();
        Iterator it = A0J.iterator();
        while (it.hasNext()) {
            AnonymousClass1V8 A0d = C117305Zk.A0d(it);
            A0l.add(new C127365uN(A0d.A0I("card_network", null), A0d.A0H("regex"), A0d.A0I("error_message", null)));
        }
        this.A00 = A0H;
        this.A01 = A0H2;
        this.A02 = Collections.unmodifiableList(A0l);
    }

    public Map A00() {
        HashMap A11 = C12970iu.A11();
        A11.put("default_validation_regex", this.A00);
        A11.put("error_message", this.A01);
        ArrayList A0l = C12960it.A0l();
        for (C127365uN r3 : this.A02) {
            HashMap A112 = C12970iu.A11();
            String str = r3.A00;
            if (str != null) {
                A112.put("card_network", str.toLowerCase(Locale.US));
            }
            A112.put("regex", r3.A02);
            String str2 = r3.A01;
            if (str2 != null) {
                A112.put("error_message", str2);
            }
            A0l.add(A112);
        }
        A11.put("validation_rules", A0l);
        return A11;
    }
}
