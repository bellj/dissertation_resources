package X;

import com.facebook.simplejni.NativeHolder;
import com.whatsapp.wamsys.JniBridge;

/* renamed from: X.1Ru  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29341Ru {
    public final NativeHolder A00;

    public C29341Ru(NativeHolder nativeHolder) {
        this.A00 = nativeHolder;
    }

    public C29341Ru(byte[] bArr, byte[] bArr2) {
        JniBridge.getInstance();
        this.A00 = new C29341Ru((NativeHolder) JniBridge.jvidispatchOOO(0, bArr, bArr2)).A00;
    }
}
