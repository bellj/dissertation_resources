package X;

import android.database.Cursor;
import com.whatsapp.util.Log;

/* renamed from: X.3AY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AY {
    public static Integer A00(AnonymousClass01d r1, C15890o4 r2) {
        int A02 = r2.A02("android.permission.READ_CONTACTS");
        Integer num = null;
        if (A02 != 0) {
            Log.i("phonebook/getCount/permission_denied");
        } else {
            Cursor A00 = C42371v6.A00(r1, "phonebook/get_count/");
            if (A00 != null) {
                try {
                    num = Integer.valueOf(A00.getCount());
                } catch (Throwable th) {
                    try {
                        A00.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            if (A00 != null) {
                A00.close();
                return num;
            }
        }
        return num;
    }
}
