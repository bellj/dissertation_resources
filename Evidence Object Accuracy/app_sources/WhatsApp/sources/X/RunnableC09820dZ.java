package X;

/* renamed from: X.0dZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09820dZ implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass0MR A01;
    public final /* synthetic */ AnonymousClass0OI A02;

    public RunnableC09820dZ(AnonymousClass0MR r1, AnonymousClass0OI r2, int i) {
        this.A02 = r2;
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0MR r0 = this.A01;
        int i = this.A00;
        AnonymousClass08K r02 = r0.A00;
        if (r02 != null) {
            r02.A01(i);
        }
    }
}
