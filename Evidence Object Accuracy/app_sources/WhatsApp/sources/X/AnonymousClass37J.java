package X;

import com.whatsapp.invites.InviteGroupParticipantsActivity;
import java.lang.ref.WeakReference;

/* renamed from: X.37J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass37J extends AbstractC16350or {
    public final AnonymousClass131 A00;
    public final C15370n3 A01;
    public final WeakReference A02;

    public AnonymousClass37J(AnonymousClass131 r2, C15370n3 r3, InviteGroupParticipantsActivity inviteGroupParticipantsActivity) {
        this.A00 = r2;
        this.A02 = C12970iu.A10(inviteGroupParticipantsActivity);
        this.A01 = r3;
    }
}
