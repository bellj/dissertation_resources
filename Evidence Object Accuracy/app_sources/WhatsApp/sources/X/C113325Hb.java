package X;

import java.security.cert.CRLException;

/* renamed from: X.5Hb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113325Hb extends CRLException {
    public Throwable cause;

    public C113325Hb(Throwable th) {
        super("Exception reading IssuingDistributionPoint");
        this.cause = th;
    }

    @Override // java.lang.Throwable
    public Throwable getCause() {
        return this.cause;
    }
}
