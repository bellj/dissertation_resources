package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.5gX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120655gX extends C120895gv {
    public final /* synthetic */ C126675tG A00;
    public final /* synthetic */ C120405g8 A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120655gX(Context context, C14900mE r8, C126675tG r9, C18650sn r10, C64513Fv r11, C120405g8 r12) {
        super(context, r8, r10, r11, "upi-get-blocked-vpas");
        this.A01 = r12;
        this.A00 = r9;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r3) {
        C126675tG r1 = this.A00;
        Log.e(C12960it.A0b("PAY: IndiaUpiBlockListManager fetch error: ", r3));
        AnonymousClass5US r0 = r1.A01;
        if (r0 != null) {
            r0.AVD(r3);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r3) {
        C126675tG r1 = this.A00;
        Log.e(C12960it.A0b("PAY: IndiaUpiBlockListManager fetch error: ", r3));
        AnonymousClass5US r0 = r1.A01;
        if (r0 != null) {
            r0.AVD(r3);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r10) {
        ArrayList arrayList;
        AnonymousClass1V8 A0c = C117305Zk.A0c(r10);
        if (A0c != null) {
            arrayList = C12960it.A0l();
            AnonymousClass1V8[] r4 = A0c.A03;
            if (r4 != null) {
                for (AnonymousClass1V8 r1 : r4) {
                    String A0W = C117295Zj.A0W(r1, "vpa");
                    if (!TextUtils.isEmpty(A0W)) {
                        arrayList.add(A0W);
                    }
                }
            }
        } else {
            arrayList = null;
        }
        C126675tG r5 = this.A00;
        AnonymousClass68Z r42 = r5.A00;
        synchronized (r42) {
            long A00 = r42.A04.A00();
            r42.A00 = A00;
            if (arrayList != null) {
                StringBuilder A0h = C12960it.A0h();
                A0h.append("PAY: IndiaUpiBlockListManager fetch success size: ");
                A0h.append(arrayList.size());
                A0h.append(" time: ");
                A0h.append(r42.A00);
                C12960it.A1F(A0h);
                Set set = r42.A0B;
                set.clear();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    set.add(C117305Zk.A0I(C117305Zk.A0J(), String.class, C12970iu.A0x(it), "upiHandle"));
                }
                r42.A0A.A0G(TextUtils.join(";", arrayList));
            } else {
                StringBuilder A0h2 = C12960it.A0h();
                A0h2.append("PAY: IndiaUpiBlockListManager fetch success hash matched time: ");
                A0h2.append(A00);
                C12960it.A1F(A0h2);
            }
            C12970iu.A1C(C117295Zj.A05(r42.A0A), "payments_block_list_last_sync_time", r42.A00);
        }
        AnonymousClass5US r12 = r5.A01;
        if (r12 != null) {
            r12.AVD(null);
        }
    }
}
