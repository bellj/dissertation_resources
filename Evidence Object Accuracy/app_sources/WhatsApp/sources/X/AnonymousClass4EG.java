package X;

import com.whatsapp.R;

/* renamed from: X.4EG  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4EG {
    public static int A00(AbstractC15340mz r1, boolean z) {
        byte b = r1.A0y;
        if (b == 0) {
            if (z) {
                return R.string.message_read;
            }
            return R.string.message_read_by;
        } else if (C30041Vv.A0I(b)) {
            if (z) {
                return R.string.message_seen_view_once;
            }
            return R.string.message_seen_by_view_once;
        } else if (r1 instanceof C27671Iq) {
            return R.string.poll_seen_by;
        } else {
            if (z) {
                return R.string.message_seen;
            }
            return R.string.message_seen_by;
        }
    }
}
