package X;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import com.whatsapp.R;

/* renamed from: X.19t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C255319t {
    public final C254919p A00;
    public final C15610nY A01;
    public final C14950mJ A02;
    public final C255219s A03;
    public final C15660nh A04;
    public final AbstractC14440lR A05;

    public C255319t(C254919p r1, C15610nY r2, C14950mJ r3, C255219s r4, C15660nh r5, AbstractC14440lR r6) {
        this.A05 = r6;
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
    }

    public AnonymousClass04S A00(Activity activity, AbstractC13860kS r5, C15370n3 r6) {
        C004802e r2 = new C004802e(activity);
        r2.A06(R.string.export_conversation_ask_about_media);
        r2.setPositiveButton(R.string.include_media, new DialogInterface.OnClickListener(activity, r5, this, r6) { // from class: X.4h5
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ AbstractC13860kS A01;
            public final /* synthetic */ C255319t A02;
            public final /* synthetic */ C15370n3 A03;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
                this.A03 = r4;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C255319t r4 = this.A02;
                Activity activity2 = this.A00;
                AbstractC13860kS r22 = this.A01;
                C15370n3 r1 = this.A03;
                C36021jC.A00(activity2, 10);
                r4.A01(activity2, r22, r1, true);
            }
        });
        r2.A00(R.string.without_media, new DialogInterface.OnClickListener(activity, r5, this, r6) { // from class: X.4h6
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ AbstractC13860kS A01;
            public final /* synthetic */ C255319t A02;
            public final /* synthetic */ C15370n3 A03;

            {
                this.A02 = r3;
                this.A00 = r1;
                this.A01 = r2;
                this.A03 = r4;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                C255319t r4 = this.A02;
                Activity activity2 = this.A00;
                AbstractC13860kS r22 = this.A01;
                C15370n3 r1 = this.A03;
                C36021jC.A00(activity2, 10);
                r4.A01(activity2, r22, r1, false);
            }
        });
        return r2.create();
    }

    public final void A01(Context context, AbstractC13860kS r13, C15370n3 r14, boolean z) {
        r13.Ady(R.string.register_xmpp_title, R.string.register_wait_message);
        AbstractC14440lR r1 = this.A05;
        C14950mJ r7 = this.A02;
        C255219s r8 = this.A03;
        r1.Aaz(new AnonymousClass387(context, r13, this.A00, this.A01, r7, r8, r14, z), new Void[0]);
    }
}
