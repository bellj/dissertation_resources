package X;

import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.0zU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22690zU {
    public final C15570nT A00;
    public final C238013b A01;
    public final C16590pI A02;
    public final AnonymousClass15L A03;
    public final C15650ng A04;
    public final C17220qS A05;
    public final C22230yk A06;
    public final C22140ya A07;

    public C22690zU(C15570nT r1, C238013b r2, C16590pI r3, AnonymousClass15L r4, C15650ng r5, C17220qS r6, C22230yk r7, C22140ya r8) {
        this.A02 = r3;
        this.A00 = r1;
        this.A05 = r6;
        this.A01 = r2;
        this.A06 = r7;
        this.A04 = r5;
        this.A07 = r8;
        this.A03 = r4;
    }

    public void A00() {
        Log.i("ChangeNumberManager/deleteChangeNumberContacts");
        new File(this.A02.A00.getFilesDir(), "change_number_contacts.json").delete();
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00cb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01() {
        /*
            r9 = this;
            java.lang.String r0 = "changenumbermanager/sendchangenumber"
            com.whatsapp.util.Log.i(r0)
            X.0nT r0 = r9.A00
            com.whatsapp.Me r5 = r0.A01()
            X.AnonymousClass009.A05(r5)
            java.lang.String r3 = r5.jabber_id
            java.lang.String r0 = "ChangeNumberManager/getChangeNumberContacts"
            com.whatsapp.util.Log.i(r0)
            X.0pI r0 = r9.A02
            android.content.Context r0 = r0.A00
            java.io.File r1 = r0.getFilesDir()
            java.lang.String r0 = "change_number_contacts.json"
            java.io.File r2 = new java.io.File
            r2.<init>(r1, r0)
            boolean r0 = r2.exists()
            r4 = 0
            if (r0 == 0) goto L_0x00a3
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
            r1.<init>(r2)     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
            r0.<init>(r1)     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
            android.util.JsonReader r7 = new android.util.JsonReader     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
            r7.<init>(r0)     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
            r7.beginObject()     // Catch: all -> 0x0087
            r8 = r4
            r6 = r4
        L_0x003f:
            boolean r0 = r7.hasNext()     // Catch: all -> 0x0087
            if (r0 == 0) goto L_0x0074
            java.lang.String r1 = r7.nextName()     // Catch: all -> 0x0087
            java.lang.String r0 = "notify_jids"
            boolean r0 = r1.equals(r0)     // Catch: all -> 0x0087
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = "old_jid"
            boolean r0 = r1.equals(r0)     // Catch: all -> 0x0087
            if (r0 == 0) goto L_0x003f
            java.lang.String r8 = r7.nextString()     // Catch: all -> 0x0087
            goto L_0x003f
        L_0x005e:
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch: all -> 0x0087
            r6.<init>()     // Catch: all -> 0x0087
            r7.beginArray()     // Catch: all -> 0x0087
        L_0x0066:
            boolean r0 = r7.hasNext()     // Catch: all -> 0x0087
            if (r0 == 0) goto L_0x003f
            java.lang.String r0 = r7.nextString()     // Catch: all -> 0x0087
            r6.add(r0)     // Catch: all -> 0x0087
            goto L_0x0066
        L_0x0074:
            if (r8 == 0) goto L_0x007e
            if (r6 == 0) goto L_0x007e
            X.1zw r2 = new X.1zw     // Catch: all -> 0x0087
            r2.<init>(r8, r6)     // Catch: all -> 0x0087
            goto L_0x007f
        L_0x007e:
            r2 = r4
        L_0x007f:
            r7.close()     // Catch: FileNotFoundException -> 0x0085, IOException -> 0x0083
            goto L_0x0098
        L_0x0083:
            r1 = move-exception
            goto L_0x0093
        L_0x0085:
            r1 = move-exception
            goto L_0x008e
        L_0x0087:
            r0 = move-exception
            r7.close()     // Catch: all -> 0x008b
        L_0x008b:
            throw r0     // Catch: FileNotFoundException -> 0x008c, IOException -> 0x0091
        L_0x008c:
            r1 = move-exception
            r2 = r4
        L_0x008e:
            java.lang.String r0 = "ChangeNumberManager/getChangeNumberContacts/notFoundJson "
            goto L_0x0095
        L_0x0091:
            r1 = move-exception
            r2 = r4
        L_0x0093:
            java.lang.String r0 = "ChangeNumberManager/getChangeNumberContacts/ioErrorJson "
        L_0x0095:
            com.whatsapp.util.Log.w(r0, r1)
        L_0x0098:
            if (r2 == 0) goto L_0x00a3
            java.lang.String r0 = r2.A00
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x00a3
            r4 = r2
        L_0x00a3:
            X.13b r0 = r9.A01
            java.util.Set r3 = r0.A03()
            if (r4 == 0) goto L_0x00cb
            java.lang.Class<com.whatsapp.jid.UserJid> r1 = com.whatsapp.jid.UserJid.class
            java.util.ArrayList r0 = r4.A01
            java.util.List r2 = X.C15380n4.A07(r1, r0)
            java.util.Iterator r1 = r2.iterator()
        L_0x00b7:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00cc
            java.lang.Object r0 = r1.next()
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x00b7
            r1.remove()
            goto L_0x00b7
        L_0x00cb:
            r2 = 0
        L_0x00cc:
            X.0qS r4 = r9.A05
            java.lang.String r0 = r5.jabber_id
            X.1zx r3 = new X.1zx
            r3.<init>(r0, r2)
            r2 = 0
            r1 = 0
            r0 = 61
            android.os.Message r0 = android.os.Message.obtain(r2, r1, r0, r1, r3)
            r4.A08(r0, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22690zU.A01():void");
    }

    public boolean A02() {
        return this.A00.A01() != null;
    }
}
