package X;

import com.facebook.redex.RunnableBRunnable0Shape1S0101000_I1;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;
import com.whatsapp.jid.UserJid;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.3ZT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZT implements AnonymousClass5WD {
    public final /* synthetic */ AcceptInviteLinkActivity A00;
    public final /* synthetic */ String A01;

    public AnonymousClass3ZT(AcceptInviteLinkActivity acceptInviteLinkActivity, String str) {
        this.A00 = acceptInviteLinkActivity;
        this.A01 = str;
    }

    @Override // X.AnonymousClass5WD
    public void APl(int i) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape1S0101000_I1(this, i, 1));
    }

    @Override // X.AnonymousClass5WD
    public void AWt(C15580nU r21, UserJid userJid, AnonymousClass1PD r23, String str, Map map, int i, int i2, long j, long j2) {
        int i3;
        AcceptInviteLinkActivity acceptInviteLinkActivity = this.A00;
        List A0B = acceptInviteLinkActivity.A0E.A0B(map);
        AnonymousClass1YM r5 = new AnonymousClass1YM(r21);
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Object value = A15.getValue();
            boolean equals = "admin".equals(value);
            boolean equals2 = "superadmin".equals(value);
            if (equals) {
                i3 = 1;
            } else if (equals2) {
                i3 = 2;
            }
            r5.A03((UserJid) A15.getKey(), acceptInviteLinkActivity.A0B.A0D((UserJid) A15.getKey()), i3, false, true);
        }
        C25691Aj r0 = acceptInviteLinkActivity.A0A;
        r0.A01.put(acceptInviteLinkActivity.A0G, r5);
        ((ActivityC13810kN) acceptInviteLinkActivity).A05.A0H(new RunnableBRunnable0Shape1S1200000_I1(new C63373Bi(r21, userJid, r23, str, null, A0B, i, 0, j), this, this.A01, 7));
    }
}
