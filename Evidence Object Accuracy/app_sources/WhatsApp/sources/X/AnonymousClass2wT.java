package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2wT  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2wT extends AnonymousClass4K7 {
    public final AnonymousClass4SX A00;
    public final UserJid A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass2wT) {
                AnonymousClass2wT r5 = (AnonymousClass2wT) obj;
                if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2wT(AnonymousClass4SX r2, UserJid userJid) {
        super(6);
        C16700pc.A0G(r2, userJid);
        this.A00 = r2;
        this.A01 = userJid;
    }

    public int hashCode() {
        return C12990iw.A08(this.A01, this.A00.hashCode() * 31);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("SearchContextCategoryListDisplayItem(categoryData=");
        A0k.append(this.A00);
        A0k.append(", bizJid=");
        return C12960it.A0a(this.A01, A0k);
    }
}
