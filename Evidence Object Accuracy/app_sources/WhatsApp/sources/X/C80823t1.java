package X;

import java.util.Comparator;
import java.util.SortedMap;
import java.util.SortedSet;

/* renamed from: X.3t1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C80823t1 extends AbstractC80933tC<K, V>.KeySet implements SortedSet<K> {
    public final /* synthetic */ AbstractC80933tC this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C80823t1(AbstractC80933tC r1, SortedMap sortedMap) {
        super(r1, sortedMap);
        this.this$0 = r1;
    }

    @Override // java.util.SortedSet
    public Comparator comparator() {
        return sortedMap().comparator();
    }

    @Override // java.util.SortedSet
    public Object first() {
        return sortedMap().firstKey();
    }

    @Override // java.util.SortedSet
    public SortedSet headSet(Object obj) {
        return new C80823t1(this.this$0, sortedMap().headMap(obj));
    }

    @Override // java.util.SortedSet
    public Object last() {
        return sortedMap().lastKey();
    }

    public SortedMap sortedMap() {
        return (SortedMap) super.map();
    }

    @Override // java.util.SortedSet
    public SortedSet subSet(Object obj, Object obj2) {
        return new C80823t1(this.this$0, sortedMap().subMap(obj, obj2));
    }

    @Override // java.util.SortedSet
    public SortedSet tailSet(Object obj) {
        return new C80823t1(this.this$0, sortedMap().tailMap(obj));
    }
}
