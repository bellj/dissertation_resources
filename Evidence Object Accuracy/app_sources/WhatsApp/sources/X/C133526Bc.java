package X;

import com.whatsapp.R;
import com.whatsapp.payments.ui.stepup.NoviPayStepUpBloksActivity;
import java.util.HashMap;

/* renamed from: X.6Bc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133526Bc implements AnonymousClass6MX {
    public final /* synthetic */ AnonymousClass3FE A00;
    public final /* synthetic */ NoviPayStepUpBloksActivity A01;

    @Override // X.AnonymousClass6MX
    public void AY5(int i) {
    }

    public C133526Bc(AnonymousClass3FE r1, NoviPayStepUpBloksActivity noviPayStepUpBloksActivity) {
        this.A01 = noviPayStepUpBloksActivity;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6MX
    public void AY6(C452120p r5, String str, String str2, boolean z) {
        if (!z || str == null) {
            NoviPayStepUpBloksActivity noviPayStepUpBloksActivity = this.A01;
            C004802e A0S = C12980iv.A0S(noviPayStepUpBloksActivity);
            C117305Zk.A17(A0S, C117295Zj.A0S(noviPayStepUpBloksActivity, A0S, R.string.novi_bloks_doc_upload_failed_message), this.A00, 75);
            return;
        }
        HashMap A11 = C12970iu.A11();
        A11.put("image_handle", str);
        A11.put("mime_type", str2);
        this.A00.A01("on_success", A11);
    }
}
