package X;

/* renamed from: X.43R  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43R extends AbstractC16110oT {
    public String A00;
    public String A01;

    public AnonymousClass43R() {
        super(3448, new AnonymousClass00E(1, 1000, 2000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamUnknownStanza {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "unknownStanzaTag", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "unknownStanzaType", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
