package X;

import android.os.Build;
import com.facebook.soloader.SysUtil$LollipopSysdeps;
import com.facebook.soloader.SysUtil$MarshmallowSysdeps;
import java.io.File;
import java.io.RandomAccessFile;

/* renamed from: X.3G2  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3G2 {
    public static void A00(File file) {
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                for (File file2 : listFiles) {
                    A00(file2);
                }
                return;
            }
            throw C12990iw.A0i(C12960it.A0b("cannot list directory ", file));
        } else if (!file.getPath().endsWith("_lock")) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
            try {
                randomAccessFile.getFD().sync();
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    try {
                        randomAccessFile.close();
                    } catch (Throwable unused) {
                    }
                }
            }
        }
    }

    public static String[] A01() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            return SysUtil$MarshmallowSysdeps.getSupportedAbis();
        }
        return i >= 21 ? SysUtil$LollipopSysdeps.getSupportedAbis() : new String[]{Build.CPU_ABI, Build.CPU_ABI2};
    }
}
