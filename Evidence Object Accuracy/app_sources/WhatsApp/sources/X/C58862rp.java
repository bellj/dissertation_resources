package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.2rp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58862rp extends AbstractC84023yH {
    public final /* synthetic */ C65113Ie A00;
    public final /* synthetic */ AnonymousClass3DX A01;
    public final /* synthetic */ C64413Fl A02;

    public C58862rp(C65113Ie r1, AnonymousClass3DX r2, C64413Fl r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        AnonymousClass3DX r1 = this.A01;
        C44791zY r2 = r1.A06;
        C65113Ie r5 = this.A00;
        C64413Fl r6 = this.A02;
        AnonymousClass3HV A05 = r2.A05(new AnonymousClass5TJ(r1.A0Q) { // from class: X.53F
            public final /* synthetic */ AtomicLong A00;

            {
                this.A00 = r1;
            }

            @Override // X.AnonymousClass5TJ
            public final void AOt(long j) {
                this.A00.addAndGet(j);
            }
        }, r1.A05, r5, r6, i);
        r6.A02();
        return A05;
    }
}
