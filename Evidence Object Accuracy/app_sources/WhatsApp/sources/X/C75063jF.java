package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.search.SearchFragment;

/* renamed from: X.3jF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75063jF extends AbstractC05270Ox {
    public final /* synthetic */ SearchFragment A00;

    public C75063jF(SearchFragment searchFragment) {
        this.A00 = searchFragment;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        SearchFragment searchFragment = this.A00;
        C34271fr r1 = searchFragment.A04;
        if (r1 != null) {
            r1.A04(3);
        }
        searchFragment.A1A();
        if (i2 != 0 && recyclerView.A0B == 1) {
            searchFragment.A1G.A0X(false);
        }
    }
}
