package X;

/* renamed from: X.31G  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass31G extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Long A02;
    public Long A03;
    public Long A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public String A09;
    public String A0A;

    public AnonymousClass31G() {
        super(2100, new AnonymousClass00E(1, 10, 1000), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A02);
        r3.Abe(1, this.A03);
        r3.Abe(4, this.A04);
        r3.Abe(3, this.A05);
        r3.Abe(12, this.A06);
        r3.Abe(10, this.A09);
        r3.Abe(8, this.A07);
        r3.Abe(7, this.A08);
        r3.Abe(6, this.A00);
        r3.Abe(11, this.A0A);
        r3.Abe(5, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidBatteryUsage {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "networkMobileBytesRx", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "networkMobileBytesTx", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "networkWifiBytesRx", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "networkWifiBytesTx", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numberPushNotifications", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sessionName", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sessionUpT", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "sessionWallclockT", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "systemCpuTime", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "tags", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "userCpuTime", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
