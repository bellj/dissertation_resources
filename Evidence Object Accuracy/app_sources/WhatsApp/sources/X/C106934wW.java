package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4wW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106934wW implements AnonymousClass5WY {
    public final int A00;
    public final long A01;
    public final long A02;
    public final long A03;
    public final AnonymousClass4T4 A04;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public C106934wW(AnonymousClass4T4 r9, int i, long j, long j2) {
        this.A04 = r9;
        this.A00 = i;
        this.A03 = j;
        long j3 = (j2 - j) / ((long) r9.A01);
        this.A01 = j3;
        this.A02 = AnonymousClass3JZ.A07(j3 * ((long) i), SearchActionVerificationClientService.MS_TO_NS, (long) r9.A03);
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A02;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        AnonymousClass4T4 r6 = this.A04;
        long j2 = (long) r6.A03;
        long j3 = (long) this.A00;
        long j4 = (j2 * j) / (j3 * SearchActionVerificationClientService.MS_TO_NS);
        long j5 = this.A01 - 1;
        long A0X = C72453ed.A0X(j4, j5);
        long j6 = this.A03;
        long j7 = (long) r6.A01;
        long A07 = AnonymousClass3JZ.A07(A0X * j3, SearchActionVerificationClientService.MS_TO_NS, j2);
        C94324bc r10 = new C94324bc(A07, j6 + (j7 * A0X));
        if (A07 >= j || A0X == j5) {
            return new C92684Xa(r10, r10);
        }
        long j8 = A0X + 1;
        return C92684Xa.A00(r10, AnonymousClass3JZ.A07(j8 * j3, SearchActionVerificationClientService.MS_TO_NS, j2), j6 + (j7 * j8));
    }
}
