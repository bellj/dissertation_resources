package X;

/* renamed from: X.28u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C470628u extends Exception {
    public final String message;

    public C470628u(String str) {
        StringBuilder sb = new StringBuilder("Package ");
        sb.append(str);
        sb.append(" not authorized");
        this.message = sb.toString();
    }

    @Override // java.lang.Throwable
    public String getMessage() {
        return this.message;
    }
}
