package X;

import android.text.Editable;
import com.whatsapp.Conversation;

/* renamed from: X.35x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C622835x extends C469928m {
    public final /* synthetic */ Conversation A00;

    public C622835x(Conversation conversation) {
        this.A00 = conversation;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        Conversation conversation = this.A00;
        conversation.A20.A08 = editable.toString();
        C15360n1 r2 = conversation.A20;
        r2.A0A = C32751cg.A02(conversation.A26, r2.A08);
        C15360n1 r1 = conversation.A20;
        r1.A09 = r1.A08;
        conversation.A1g.getConversationCursorAdapter().A01++;
        conversation.A1g.A02();
    }
}
