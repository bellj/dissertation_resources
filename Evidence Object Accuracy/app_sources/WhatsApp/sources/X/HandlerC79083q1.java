package X;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.concurrent.locks.Lock;

/* renamed from: X.3q1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class HandlerC79083q1 extends HandlerC472529t {
    public final /* synthetic */ C108364yw A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC79083q1(Looper looper, C108364yw r2) {
        super(looper);
        this.A00 = r2;
    }

    @Override // android.os.Handler
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            AbstractC92104Uo r2 = (AbstractC92104Uo) message.obj;
            C108364yw r0 = this.A00;
            Lock lock = r0.A0D;
            lock.lock();
            try {
                if (r0.A0E == r2.A00) {
                    if (r2 instanceof C77853ny) {
                        C77853ny r22 = (C77853ny) r2;
                        C108324ys r4 = r22.A00;
                        C78323oj r1 = r22.A01;
                        if (r4.A07(0)) {
                            C56492ky r5 = r1.A01;
                            if (r5.A01 == 0) {
                                C78463ox r12 = r1.A02;
                                C13020j0.A01(r12);
                                r5 = r12.A02;
                                if (r5.A01 == 0) {
                                    r4.A08 = true;
                                    IAccountAccessor A00 = r12.A00();
                                    C13020j0.A01(A00);
                                    r4.A04 = A00;
                                    r4.A09 = r12.A03;
                                    r4.A0A = r12.A04;
                                    r4.A02();
                                } else {
                                    String valueOf = String.valueOf(r5);
                                    Log.wtf("GACConnecting", "Sign-in succeeded with resolve account failure: ".concat(valueOf), new Exception());
                                    r4.A03(r5);
                                }
                            } else {
                                if (r4.A06 && !r5.A00()) {
                                    r4.A00();
                                    r4.A02();
                                }
                                r4.A03(r5);
                            }
                        }
                    } else if (r2 instanceof C77833nw) {
                        ((C77833nw) r2).A00.AV1(new C56492ky(16, null));
                    } else if (!(r2 instanceof C77843nx)) {
                        ((C77823nv) r2).A00.Agb(1);
                    } else {
                        C77843nx r23 = (C77843nx) r2;
                        ((C108324ys) r23.A01.A00).A03(r23.A00);
                    }
                }
            } finally {
                lock.unlock();
            }
        } else if (i != 2) {
            Log.w("GACStateManager", C12960it.A0e("Unknown message id: ", C12980iv.A0t(31), i));
        } else {
            throw ((Throwable) message.obj);
        }
    }
}
