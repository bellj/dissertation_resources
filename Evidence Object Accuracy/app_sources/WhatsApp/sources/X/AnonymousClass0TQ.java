package X;

import android.os.Build;
import android.view.View;
import java.util.ArrayList;

/* renamed from: X.0TQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0TQ {
    public static final AbstractC06480Tu A00;
    public static final AbstractC06480Tu A01;

    static {
        AnonymousClass0EI r0;
        AbstractC06480Tu r02;
        if (Build.VERSION.SDK_INT >= 21) {
            r0 = new AnonymousClass0EI();
        } else {
            r0 = null;
        }
        A00 = r0;
        try {
            r02 = (AbstractC06480Tu) AnonymousClass0EH.class.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            r02 = null;
        }
        A01 = r02;
    }

    public static void A00(AnonymousClass00N r8, AnonymousClass01E r9, AnonymousClass01E r10, boolean z, boolean z2) {
        AnonymousClass0O9 r0;
        AbstractC000600h r7;
        if (z) {
            r0 = r10.A0C;
        } else {
            r0 = r9.A0C;
        }
        if (!(r0 == null || (r7 = r0.A07) == null)) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (r8 != null) {
                int size = r8.size();
                for (int i = 0; i < size; i++) {
                    Object[] objArr = r8.A02;
                    int i2 = i << 1;
                    arrayList2.add(objArr[i2]);
                    arrayList.add(objArr[i2 + 1]);
                }
            }
            if (!z2) {
                r7.A02(arrayList2, arrayList, null);
            }
        }
    }

    public static void A01(ArrayList arrayList, int i) {
        if (arrayList != null) {
            int size = arrayList.size();
            while (true) {
                size--;
                if (size >= 0) {
                    ((View) arrayList.get(size)).setVisibility(i);
                } else {
                    return;
                }
            }
        }
    }
}
