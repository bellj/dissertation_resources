package X;

import android.os.Process;
import android.text.SpannableStringBuilder;
import android.text.util.Linkify;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape1S0600000_I1;
import java.util.ArrayList;

/* renamed from: X.398  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass398 extends AnonymousClass1MS {
    public final AnonymousClass4KM A00;
    public final C22710zW A01;
    public final C17070qD A02;
    public final String A03;
    public volatile boolean A04;
    public final /* synthetic */ AnonymousClass3DI A05;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass398(AnonymousClass4KM r2, AnonymousClass3DI r3, C22710zW r4, C17070qD r5, String str) {
        super("LinkifierThread");
        this.A05 = r3;
        this.A00 = r2;
        this.A03 = str;
        this.A02 = r5;
        this.A01 = r4;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Process.setThreadPriority(10);
        while (!this.A04) {
            try {
                C91264Rb r0 = (C91264Rb) this.A00.A00.takeLast();
                if (r0 != null) {
                    CharSequence charSequence = r0.A02;
                    TextView textView = r0.A00;
                    Object obj = r0.A03;
                    AbstractC116035Tw r7 = r0.A01;
                    SpannableStringBuilder A0J = C12990iw.A0J(charSequence);
                    if (obj.equals(textView.getTag())) {
                        String str = this.A03;
                        C17070qD r2 = this.A02;
                        C22710zW r1 = this.A01;
                        try {
                            Linkify.addLinks(A0J, 2);
                            C33771f3.A06(A0J);
                            C63263Ax.A00(A0J, str);
                            C63243Av.A00(A0J, r1, r2);
                            C63253Aw.A00(A0J, r1, r2);
                        } catch (Exception unused) {
                        }
                        ArrayList A05 = C42971wC.A05(A0J);
                        if (A05 != null && !A05.isEmpty() && obj.equals(textView.getTag())) {
                            AnonymousClass3DI r9 = this.A05;
                            r9.A01.A08(charSequence.toString(), C12990iw.A0J(A0J));
                            r9.A02.A0H(new RunnableBRunnable0Shape1S0600000_I1(A0J, textView, r7, this, r9, obj));
                        }
                    }
                }
            } catch (InterruptedException unused2) {
                return;
            }
        }
    }
}
