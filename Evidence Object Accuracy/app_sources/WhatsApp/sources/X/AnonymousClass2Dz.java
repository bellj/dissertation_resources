package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.2Dz  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Dz {
    public C15370n3 A00;
    public UserJid A01;
    public String A02;
    public final String A03;

    public AnonymousClass2Dz(C16590pI r2, AnonymousClass018 r3, C15370n3 r4) {
        this.A02 = C248917h.A01(r4);
        this.A03 = (String) C15610nY.A00(r2.A00, r3, r4);
        if (r4.A0f) {
            this.A01 = (UserJid) r4.A0B(UserJid.class);
        }
        this.A00 = r4;
    }

    public AnonymousClass2Dz(String str, String str2) {
        this.A02 = str;
        this.A03 = str2;
    }
}
