package X;

import android.graphics.Paint;
import android.text.Spanned;

/* renamed from: X.3Gc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64583Gc {
    public final float A00;
    public final Paint A01;

    public C64583Gc(Paint paint, float f) {
        this.A01 = paint;
        this.A00 = f;
    }

    public static void A00(Paint paint, CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            Paint.FontMetricsInt A00 = AnonymousClass3I9.A00(paint);
            int textSize = (int) ((paint.getTextSize() * 1.1f) + 0.5f);
            AnonymousClass36k[] r3 = (AnonymousClass36k[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), AnonymousClass36k.class);
            if (r3 != null) {
                for (AnonymousClass36k r0 : r3) {
                    r0.A00 = A00;
                    r0.A03().setBounds(0, 0, textSize, textSize);
                }
            }
        }
    }
}
