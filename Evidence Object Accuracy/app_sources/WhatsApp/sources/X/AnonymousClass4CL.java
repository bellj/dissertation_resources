package X;

import android.media.MediaCodec;

/* renamed from: X.4CL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4CL extends Exception {
    public final C95494dp codecInfo;
    public final String diagnosticInfo;
    public final AnonymousClass4CL fallbackDecoderInitializationException;
    public final String mimeType;
    public final boolean secureDecoderRequired;

    public AnonymousClass4CL(C95494dp r1, AnonymousClass4CL r2, String str, String str2, String str3, Throwable th, boolean z) {
        super(str, th);
        this.mimeType = str2;
        this.secureDecoderRequired = z;
        this.codecInfo = r1;
        this.diagnosticInfo = str3;
        this.fallbackDecoderInitializationException = r2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4CL(X.C100614mC r9, java.lang.Throwable r10, int r11) {
        /*
            r8 = this;
            java.lang.String r0 = "Decoder init failed: ["
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r11)
            java.lang.String r0 = "], "
            java.lang.String r3 = X.C12960it.A0Z(r9, r0, r1)
            java.lang.String r4 = r9.A0T
            java.lang.String r2 = "neg_"
            java.lang.String r0 = "com.google.android.exoplayer2.mediacodec.MediaCodecRenderer_"
            java.lang.StringBuilder r1 = X.C12960it.A0k(r0)
            r1.append(r2)
            int r0 = java.lang.Math.abs(r11)
            java.lang.String r5 = X.C12960it.A0f(r1, r0)
            r1 = 0
            r7 = 0
            r0 = r8
            r6 = r10
            r2 = r1
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4CL.<init>(X.4mC, java.lang.Throwable, int):void");
    }

    public static String A00(Throwable th) {
        if (th instanceof MediaCodec.CodecException) {
            return ((MediaCodec.CodecException) th).getDiagnosticInfo();
        }
        return null;
    }
}
