package X;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/* renamed from: X.3o8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C77953o8 extends AbstractC77963o9 implements AnonymousClass5Yh {
    public final Bundle A00;
    public final AnonymousClass3BW A01;
    public final Integer A02;

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final int AET() {
        return 12451000;
    }

    @Override // X.AbstractC95064d1, X.AbstractC72443eb
    public final boolean Aae() {
        return true;
    }

    public C77953o8(Context context, Bundle bundle, Looper looper, AbstractC14980mM r12, AbstractC15000mO r13, AnonymousClass3BW r14) {
        super(context, looper, r12, r13, r14, 44);
        this.A01 = r14;
        this.A00 = bundle;
        this.A02 = r14.A00;
    }

    public static Bundle A00(AnonymousClass3BW r4) {
        Integer num = r4.A00;
        Bundle A0D = C12970iu.A0D();
        A0D.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", null);
        if (num != null) {
            A0D.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", num.intValue());
        }
        A0D.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", false);
        A0D.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", false);
        A0D.putString("com.google.android.gms.signin.internal.serverClientId", null);
        A0D.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
        A0D.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", false);
        A0D.putString("com.google.android.gms.signin.internal.hostedDomain", null);
        A0D.putString("com.google.android.gms.signin.internal.logSessionId", null);
        A0D.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", false);
        return A0D;
    }

    @Override // X.AbstractC95064d1
    public final Bundle A02() {
        String str = this.A01.A02;
        if (!this.A0F.getPackageName().equals(str)) {
            this.A00.putString("com.google.android.gms.signin.internal.realClientPackageName", str);
        }
        return this.A00;
    }

    @Override // X.AnonymousClass5Yh
    public final void AgU(AnonymousClass5YR r8) {
        GoogleSignInAccount googleSignInAccount;
        try {
            Account account = new Account("<<default account>>", "com.google");
            if ("<<default account>>".equals(account.name)) {
                googleSignInAccount = C65263Iv.A00(this.A0F).A02();
            } else {
                googleSignInAccount = null;
            }
            Integer num = this.A02;
            C13020j0.A01(num);
            C78383op r0 = new C78383op(account, googleSignInAccount, 2, num.intValue());
            C98384ib r3 = (C98384ib) A03();
            C78073oK r2 = new C78073oK(r0, 1);
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken(r3.A01);
            obtain.writeInt(1);
            r2.writeToParcel(obtain, 0);
            obtain.writeStrongBinder(r8.asBinder());
            r3.A00(12, obtain);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                r8.AgR(new C78323oj(new C56492ky(8, null), null, 1));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }
}
