package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.4ac  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C93714ac {
    public final float A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final String A04;
    public final List A05;

    public C93714ac(String str, List list, float f, int i, int i2, int i3) {
        this.A05 = list;
        this.A02 = i;
        this.A03 = i2;
        this.A01 = i3;
        this.A00 = f;
        this.A04 = str;
    }

    public static C93714ac A00(C95304dT r15) {
        int i;
        float f;
        try {
            int A01 = (C95304dT.A01(r15, 4) & 3) + 1;
            if (A01 != 3) {
                ArrayList A0l = C12960it.A0l();
                int A0C = r15.A0C() & 31;
                for (int i2 = 0; i2 < A0C; i2++) {
                    int A0F = r15.A0F();
                    int i3 = r15.A01;
                    r15.A0T(A0F);
                    byte[] bArr = r15.A02;
                    byte[] bArr2 = AnonymousClass4ZS.A00;
                    int length = bArr2.length;
                    byte[] bArr3 = new byte[length + A0F];
                    System.arraycopy(bArr2, 0, bArr3, 0, length);
                    System.arraycopy(bArr, i3, bArr3, length, A0F);
                    A0l.add(bArr3);
                }
                int A0C2 = r15.A0C();
                for (int i4 = 0; i4 < A0C2; i4++) {
                    int A0F2 = r15.A0F();
                    int i5 = r15.A01;
                    r15.A0T(A0F2);
                    byte[] bArr4 = r15.A02;
                    byte[] bArr5 = AnonymousClass4ZS.A00;
                    int length2 = bArr5.length;
                    byte[] bArr6 = new byte[length2 + A0F2];
                    System.arraycopy(bArr5, 0, bArr6, 0, length2);
                    System.arraycopy(bArr4, i5, bArr6, length2, A0F2);
                    A0l.add(bArr6);
                }
                String str = null;
                int i6 = -1;
                if (A0C > 0) {
                    AnonymousClass4TQ A02 = C95464dl.A02((byte[]) A0l.get(0), A01, ((byte[]) A0l.get(0)).length);
                    i6 = A02.A06;
                    i = A02.A02;
                    f = A02.A00;
                    int i7 = A02.A04;
                    int i8 = A02.A01;
                    int i9 = A02.A03;
                    Object[] objArr = new Object[3];
                    C12960it.A1P(objArr, i7, 0);
                    C12980iv.A1T(objArr, i8);
                    C12990iw.A1V(objArr, i9);
                    str = String.format("avc1.%02X%02X%02X", objArr);
                } else {
                    i = -1;
                    f = 1.0f;
                }
                return new C93714ac(str, A0l, f, A01, i6, i);
            }
            throw C72463ee.A0D();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new AnonymousClass496("Error parsing AVC config", e);
        }
    }
}
