package X;

import java.util.List;

/* renamed from: X.4QP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4QP {
    public final CharSequence A00;
    public final String A01;
    public final List A02;

    public AnonymousClass4QP(CharSequence charSequence, String str, List list) {
        this.A01 = str;
        this.A02 = list;
        this.A00 = charSequence;
    }
}
