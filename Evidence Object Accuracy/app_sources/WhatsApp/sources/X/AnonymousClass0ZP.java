package X;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;

/* renamed from: X.0ZP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0ZP implements AbstractC12420ht {
    public final ViewGroupOverlay A00;

    public AnonymousClass0ZP(ViewGroup viewGroup) {
        this.A00 = viewGroup.getOverlay();
    }

    @Override // X.AbstractC12420ht
    public void A5b(View view) {
        this.A00.add(view);
    }

    @Override // X.AbstractC12420ht
    public void AaF(View view) {
        this.A00.remove(view);
    }
}
