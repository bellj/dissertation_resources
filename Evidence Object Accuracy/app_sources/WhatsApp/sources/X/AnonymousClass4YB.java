package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4YB  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4YB {
    public long A00;
    public long A01 = -9223372036854775807L;
    public long A02;

    public AnonymousClass4YB(long j) {
        this.A00 = j;
    }

    public synchronized long A00() {
        return this.A00;
    }

    public synchronized long A01() {
        long j;
        j = -9223372036854775807L;
        if (this.A00 == Long.MAX_VALUE) {
            j = 0;
        } else if (this.A01 != -9223372036854775807L) {
            j = this.A02;
        }
        return j;
    }

    public synchronized long A02(long j) {
        if (j == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        if (this.A01 != -9223372036854775807L) {
            this.A01 = j;
        } else {
            long j2 = this.A00;
            if (j2 != Long.MAX_VALUE) {
                this.A02 = j2 - j;
            }
            this.A01 = j;
            notifyAll();
        }
        return j + this.A02;
    }

    public synchronized long A03(long j) {
        if (j == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        long j2 = this.A01;
        if (j2 != -9223372036854775807L) {
            long A0W = C72453ed.A0W(j2, 90000);
            long j3 = (4294967296L + A0W) / 8589934592L;
            long j4 = ((j3 - 1) * 8589934592L) + j;
            j += j3 * 8589934592L;
            if (Math.abs(j4 - A0W) < Math.abs(j - A0W)) {
                j = j4;
            }
        }
        return A02((j * SearchActionVerificationClientService.MS_TO_NS) / 90000);
    }
}
