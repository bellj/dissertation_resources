package X;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0;
import com.whatsapp.settings.chat.wallpaper.SolidColorWallpaper;

/* renamed from: X.2Uq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51422Uq extends BaseAdapter {
    public final Context A00;
    public final /* synthetic */ SolidColorWallpaper A01;

    @Override // android.widget.Adapter
    public Object getItem(int i) {
        return null;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return 0;
    }

    public C51422Uq(Context context, SolidColorWallpaper solidColorWallpaper) {
        this.A01 = solidColorWallpaper;
        this.A00 = context;
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A01.A02.length;
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = new C74213hb(this.A00);
            view.setLayoutParams(new AbsListView.LayoutParams(-1, -1));
        }
        SolidColorWallpaper solidColorWallpaper = this.A01;
        view.setBackgroundColor(solidColorWallpaper.A02[i]);
        view.setContentDescription(solidColorWallpaper.getString(SolidColorWallpaper.A04[i]));
        view.setOnClickListener(new ViewOnClickCListenerShape0S0101000_I0(this, i, 6));
        return view;
    }
}
