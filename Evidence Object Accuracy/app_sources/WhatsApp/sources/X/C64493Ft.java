package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.legacy.widget.Space;
import com.google.android.material.textfield.TextInputLayout;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3Ft  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64493Ft {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public Animator A06;
    public Typeface A07;
    public FrameLayout A08;
    public LinearLayout A09;
    public TextView A0A;
    public TextView A0B;
    public CharSequence A0C;
    public CharSequence A0D;
    public boolean A0E;
    public boolean A0F;
    public final float A0G;
    public final Context A0H;
    public final TextInputLayout A0I;

    public C64493Ft(TextInputLayout textInputLayout) {
        Context context = textInputLayout.getContext();
        this.A0H = context;
        this.A0I = textInputLayout;
        this.A0G = (float) context.getResources().getDimensionPixelSize(R.dimen.design_textinput_caption_translate_y);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        if (android.text.TextUtils.isEmpty(r4.A0D) != false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
            r4 = this;
            r3 = 0
            r4.A0C = r3
            android.animation.Animator r0 = r4.A06
            if (r0 == 0) goto L_0x000a
            r0.cancel()
        L_0x000a:
            int r1 = r4.A00
            r0 = 1
            if (r1 != r0) goto L_0x001f
            boolean r0 = r4.A0F
            if (r0 == 0) goto L_0x001c
            java.lang.CharSequence r0 = r4.A0D
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            r0 = 2
            if (r1 == 0) goto L_0x001d
        L_0x001c:
            r0 = 0
        L_0x001d:
            r4.A01 = r0
        L_0x001f:
            int r2 = r4.A00
            int r1 = r4.A01
            android.widget.TextView r0 = r4.A0A
            boolean r0 = r4.A06(r0, r3)
            r4.A01(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C64493Ft.A00():void");
    }

    public final void A01(int i, int i2, boolean z) {
        TextView textView;
        TextView textView2;
        TextView textView3;
        TextView textView4;
        if (z) {
            AnimatorSet animatorSet = new AnimatorSet();
            this.A06 = animatorSet;
            ArrayList A0l = C12960it.A0l();
            A04(this.A0B, A0l, 2, i, i2, this.A0F);
            A04(this.A0A, A0l, 1, i, i2, this.A0E);
            AnonymousClass2R4.A00(animatorSet, A0l);
            if (i == 1) {
                textView3 = this.A0A;
            } else if (i != 2) {
                textView3 = null;
            } else {
                textView3 = this.A0B;
            }
            if (i2 == 1) {
                textView4 = this.A0A;
            } else if (i2 != 2) {
                textView4 = null;
            } else {
                textView4 = this.A0B;
            }
            animatorSet.addListener(new AnonymousClass2YY(textView3, textView4, this, i2, i));
            animatorSet.start();
        } else if (i != i2) {
            if (i2 != 0) {
                if (i2 == 1) {
                    textView2 = this.A0A;
                } else if (i2 == 2) {
                    textView2 = this.A0B;
                }
                if (textView2 != null) {
                    textView2.setVisibility(0);
                    textView2.setAlpha(1.0f);
                }
            }
            if (i != 0) {
                if (i == 1) {
                    textView = this.A0A;
                } else if (i == 2) {
                    textView = this.A0B;
                }
                if (textView != null) {
                    textView.setVisibility(4);
                    if (i == 1) {
                        textView.setText((CharSequence) null);
                    }
                }
            }
            this.A00 = i2;
        }
        TextInputLayout textInputLayout = this.A0I;
        textInputLayout.A03();
        textInputLayout.A0G(z, false);
        textInputLayout.A04();
    }

    public void A02(TextView textView, int i) {
        LinearLayout linearLayout;
        if (this.A09 == null && this.A08 == null) {
            Context context = this.A0H;
            LinearLayout linearLayout2 = new LinearLayout(context);
            this.A09 = linearLayout2;
            linearLayout2.setOrientation(0);
            TextInputLayout textInputLayout = this.A0I;
            textInputLayout.addView(this.A09, -1, -2);
            FrameLayout frameLayout = new FrameLayout(context);
            this.A08 = frameLayout;
            this.A09.addView(frameLayout, -1, new FrameLayout.LayoutParams(-2, -2));
            this.A09.addView(new Space(context), new LinearLayout.LayoutParams(0, 0, 1.0f));
            EditText editText = textInputLayout.A0L;
            if (!(editText == null || (linearLayout = this.A09) == null)) {
                AnonymousClass028.A0e(linearLayout, AnonymousClass028.A07(editText), 0, AnonymousClass028.A06(textInputLayout.A0L), 0);
            }
        }
        if (i == 0 || i == 1) {
            this.A08.setVisibility(0);
            this.A08.addView(textView);
            this.A02++;
        } else {
            this.A09.addView(textView, i);
        }
        this.A09.setVisibility(0);
        this.A05++;
    }

    public void A03(TextView textView, int i) {
        FrameLayout frameLayout;
        LinearLayout linearLayout = this.A09;
        if (linearLayout != null) {
            if ((i == 0 || i == 1) && (frameLayout = this.A08) != null) {
                int i2 = this.A02 - 1;
                this.A02 = i2;
                if (i2 == 0) {
                    frameLayout.setVisibility(8);
                }
                this.A08.removeView(textView);
            } else {
                linearLayout.removeView(textView);
            }
            int i3 = this.A05 - 1;
            this.A05 = i3;
            LinearLayout linearLayout2 = this.A09;
            if (i3 == 0) {
                linearLayout2.setVisibility(8);
            }
        }
    }

    public final void A04(TextView textView, List list, int i, int i2, int i3, boolean z) {
        if (textView != null && z) {
            if (i == i3 || i == i2) {
                float f = 0.0f;
                if (i3 == i) {
                    f = 1.0f;
                }
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.ALPHA, f);
                ofFloat.setDuration(167L);
                ofFloat.setInterpolator(C50732Qs.A03);
                list.add(ofFloat);
                if (i3 == i) {
                    ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(textView, View.TRANSLATION_Y, -this.A0G, 0.0f);
                    ofFloat2.setDuration(217L);
                    ofFloat2.setInterpolator(C50732Qs.A04);
                    list.add(ofFloat2);
                }
            }
        }
    }

    public boolean A05() {
        if (this.A01 != 1 || this.A0A == null || TextUtils.isEmpty(this.A0C)) {
            return false;
        }
        return true;
    }

    public final boolean A06(TextView textView, CharSequence charSequence) {
        TextInputLayout textInputLayout = this.A0I;
        if (!AnonymousClass028.A0r(textInputLayout) || !textInputLayout.isEnabled()) {
            return false;
        }
        return this.A01 != this.A00 || textView == null || !TextUtils.equals(textView.getText(), charSequence);
    }
}
