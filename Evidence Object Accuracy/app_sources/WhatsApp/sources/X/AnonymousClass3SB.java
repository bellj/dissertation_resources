package X;

import android.content.SharedPreferences;
import android.os.RemoteException;
import android.text.TextUtils;

/* renamed from: X.3SB  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3SB implements AbstractC12060hJ {
    public final /* synthetic */ C06110Sf A00;
    public final /* synthetic */ C14820m6 A01;

    public AnonymousClass3SB(C06110Sf r1, C14820m6 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AbstractC12060hJ
    public void ARQ(int i) {
        String str;
        try {
            AnonymousClass0P2 A01 = this.A00.A01();
            String A012 = A01.A01();
            if (A012 == null || A012.indexOf("invite_code=") != 0) {
                str = null;
            } else {
                str = A012.substring(12);
            }
            if (!TextUtils.isEmpty(str)) {
                SharedPreferences sharedPreferences = this.A01.A00;
                if (C12980iv.A0p(sharedPreferences, "invite_code_from_referrer") == null) {
                    long A00 = A01.A00();
                    C12970iu.A1D(sharedPreferences.edit(), "invite_code_from_referrer", str);
                    C12970iu.A1C(sharedPreferences.edit(), "referrer_clicked_time", A00);
                } else if (C12980iv.A0F(sharedPreferences, "referrer_clicked_time") <= A01.A00()) {
                    long A002 = A01.A00();
                    C12970iu.A1D(sharedPreferences.edit(), "invite_code_from_referrer", str);
                    C12970iu.A1C(sharedPreferences.edit(), "referrer_clicked_time", A002);
                }
            }
        } catch (RemoteException unused) {
        } catch (Throwable th) {
            this.A00.A02();
            throw th;
        }
        this.A00.A02();
    }
}
