package X;

/* renamed from: X.1fE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33881fE extends AbstractC16110oT {
    public Boolean A00;
    public Integer A01;
    public Integer A02;
    public Integer A03;
    public Long A04;

    public C33881fE() {
        super(3578, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A04);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A00);
        r3.Abe(5, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        StringBuilder sb = new StringBuilder("WamE2eRetryReject {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "msgRetryCount", this.A04);
        Integer num2 = this.A02;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryRejectReason", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryRevoke", this.A00);
        Integer num3 = this.A03;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "senderDeviceType", obj3);
        sb.append("}");
        return sb.toString();
    }
}
