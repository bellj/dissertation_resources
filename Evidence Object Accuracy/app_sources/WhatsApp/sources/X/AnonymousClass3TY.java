package X;

import java.io.Serializable;
import java.util.List;

/* renamed from: X.3TY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3TY implements AnonymousClass28S, Serializable {
    public static final long serialVersionUID = 0;
    public final List components;

    public /* synthetic */ AnonymousClass3TY(List list) {
        this.components = list;
    }

    @Override // X.AnonymousClass28S
    public boolean A66(Object obj) {
        for (int i = 0; i < this.components.size(); i++) {
            if (!((AnonymousClass28S) this.components.get(i)).A66(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj instanceof AnonymousClass3TY) {
            return this.components.equals(((AnonymousClass3TY) obj).components);
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.components.hashCode() + 306654252;
    }

    @Override // java.lang.Object
    public String toString() {
        List list = this.components;
        StringBuilder A0k = C12960it.A0k("Predicates.");
        A0k.append("and");
        A0k.append('(');
        boolean z = true;
        for (Object obj : list) {
            if (!z) {
                A0k.append(',');
            }
            A0k.append(obj);
            z = false;
        }
        return C12970iu.A0u(A0k);
    }
}
