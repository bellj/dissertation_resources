package X;

import com.whatsapp.MessageDialogFragment;
import com.whatsapp.payments.ui.IndiaUpiBankAccountDetailsActivity;

/* renamed from: X.68q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1328968q implements AnonymousClass1FK {
    public final /* synthetic */ int A00 = 13;
    public final /* synthetic */ AnonymousClass1FK A01;
    public final /* synthetic */ AbstractC16870pt A02;
    public final /* synthetic */ IndiaUpiBankAccountDetailsActivity A03;

    public C1328968q(AnonymousClass1FK r2, AbstractC16870pt r3, IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity) {
        this.A03 = indiaUpiBankAccountDetailsActivity;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r2) {
        this.A01.AV3(r2);
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r5) {
        IndiaUpiBankAccountDetailsActivity indiaUpiBankAccountDetailsActivity = this.A03;
        indiaUpiBankAccountDetailsActivity.A0H.A06(C12960it.A0b("removePayment/onResponseError. paymentNetworkError: ", r5));
        AbstractC16870pt r1 = this.A02;
        if (r1 != null) {
            r1.AKa(r5, this.A00);
        }
        AnonymousClass60V A04 = indiaUpiBankAccountDetailsActivity.A0A.A04(null, r5.A00);
        if (A04.A00 != 0) {
            indiaUpiBankAccountDetailsActivity.AaN();
            MessageDialogFragment.A00(A04.A01(indiaUpiBankAccountDetailsActivity)).A01().A1F(indiaUpiBankAccountDetailsActivity.A0V(), null);
            return;
        }
        this.A01.AVA(r5);
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r2) {
        this.A01.AVB(r2);
    }
}
