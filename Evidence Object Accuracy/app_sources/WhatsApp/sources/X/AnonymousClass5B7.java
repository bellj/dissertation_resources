package X;

import com.whatsapp.service.UnsentMessagesNetworkAvailableJob;
import com.whatsapp.voipcalling.SelfManagedConnectionService;

/* renamed from: X.5B7  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5B7 implements AnonymousClass01L {
    public abstract void A00(UnsentMessagesNetworkAvailableJob unsentMessagesNetworkAvailableJob);

    public abstract void A01(SelfManagedConnectionService selfManagedConnectionService);
}
