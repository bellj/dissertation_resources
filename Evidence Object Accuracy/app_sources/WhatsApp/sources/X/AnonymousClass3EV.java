package X;

import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.Toolbar;
import com.facebook.redex.ViewOnClickCListenerShape12S0100000_I1_6;
import com.whatsapp.R;

/* renamed from: X.3EV  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3EV {
    public String A00;
    public String A01 = "BACK";
    public final ActivityC13810kN A02;
    public final AnonymousClass018 A03;

    public AnonymousClass3EV(ActivityC13810kN r2, AnonymousClass018 r3) {
        C16700pc.A0G(r3, r2);
        this.A03 = r3;
        this.A02 = r2;
    }

    public final Drawable A00() {
        if (C16700pc.A0O(this.A01, "NONE")) {
            return null;
        }
        AnonymousClass018 r3 = this.A03;
        ActivityC13810kN r2 = this.A02;
        boolean A0O = C16700pc.A0O(this.A01, "CLOSE");
        int i = R.drawable.ic_back;
        if (A0O) {
            i = R.drawable.ic_close;
        }
        AnonymousClass2GF A00 = AnonymousClass2GF.A00(r2, r3, i);
        A00.setColorFilter(r2.getResources().getColor(R.color.wabloksui_screen_back_arrow), PorterDuff.Mode.SRC_ATOP);
        return A00;
    }

    public final void A01(Toolbar toolbar, AnonymousClass5VC r4, String str, String str2) {
        Resources resources;
        this.A00 = str;
        if (str2 == null) {
            this.A01 = "BACK";
        } else {
            this.A01 = str2;
        }
        Drawable A00 = A00();
        if (toolbar != null) {
            toolbar.setNavigationIcon(A00);
        }
        ActivityC13810kN r0 = this.A02;
        if (r0 == null || (resources = r0.getResources()) == null) {
            if (toolbar == null) {
                return;
            }
        } else if (toolbar != null) {
            toolbar.setBackgroundColor(resources.getColor(R.color.wabloksui_screen_toolbar));
        } else {
            return;
        }
        toolbar.setNavigationOnClickListener(new ViewOnClickCListenerShape12S0100000_I1_6(r4, 4));
    }
}
