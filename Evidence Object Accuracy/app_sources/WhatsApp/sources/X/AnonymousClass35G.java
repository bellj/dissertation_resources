package X;

/* renamed from: X.35G  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35G extends AbstractC75753kM {
    public final AnonymousClass2d1 A00;

    public AnonymousClass35G(AnonymousClass4L9 r1, AnonymousClass2d1 r2) {
        super(r2, r1);
        this.A00 = r2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0046  */
    @Override // X.AbstractC75753kM
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(java.lang.Integer r10) {
        /*
            r9 = this;
            android.view.View r0 = r9.A0H
            android.content.Context r2 = r0.getContext()
            int r5 = r10.intValue()
            r4 = 2
            r3 = 0
            if (r5 == 0) goto L_0x0081
            r8 = 1
            if (r5 == r8) goto L_0x008c
            if (r5 == r4) goto L_0x0049
            r0 = 3
            if (r5 != r0) goto L_0x009b
            r0 = 2131893080(0x7f121b58, float:1.9420926E38)
            java.lang.String r7 = r2.getString(r0)
            android.graphics.drawable.GradientDrawable r6 = new android.graphics.drawable.GradientDrawable
            r6.<init>()
            r6.setSize(r8, r8)
            r0 = 2131099833(0x7f0600b9, float:1.781203E38)
            int r0 = X.AnonymousClass00T.A00(r2, r0)
            r6.setColor(r0)
            r1 = 2131231684(0x7f0803c4, float:1.8079456E38)
            r0 = 2131101353(0x7f0606a9, float:1.7815113E38)
            android.graphics.drawable.Drawable r3 = X.AnonymousClass2GE.A01(r2, r1, r0)
        L_0x0039:
            X.2d1 r1 = r9.A00
            r1.A00(r6, r3, r7)
            if (r5 != r4) goto L_0x0046
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER
        L_0x0042:
            r1.setPreviewScaleType(r0)
            return
        L_0x0046:
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.CENTER_CROP
            goto L_0x0042
        L_0x0049:
            r0 = 2131891910(0x7f1216c6, float:1.9418553E38)
            java.lang.String r7 = r2.getString(r0)
            android.graphics.drawable.GradientDrawable r6 = new android.graphics.drawable.GradientDrawable
            r6.<init>()
            r6.setSize(r8, r8)
            r0 = 2131101125(0x7f0605c5, float:1.781465E38)
            int r0 = X.AnonymousClass00T.A00(r2, r0)
            r6.setColor(r0)
            r0 = 2131101154(0x7f0605e2, float:1.781471E38)
            int r1 = X.AnonymousClass00T.A00(r2, r0)
            r0 = 2131232854(0x7f080856, float:1.808183E38)
            android.graphics.drawable.Drawable r0 = X.C12970iu.A0C(r2, r0)
            android.graphics.drawable.Drawable r2 = X.AnonymousClass2GE.A04(r0, r1)
            android.graphics.drawable.Drawable[] r1 = new android.graphics.drawable.Drawable[r4]
            r0 = 0
            r1[r0] = r6
            r1[r8] = r2
            android.graphics.drawable.LayerDrawable r6 = new android.graphics.drawable.LayerDrawable
            r6.<init>(r1)
            goto L_0x0039
        L_0x0081:
            r0 = 2131893078(0x7f121b56, float:1.9420922E38)
            java.lang.String r7 = r2.getString(r0)
            r0 = 2131232250(0x7f0805fa, float:1.8080604E38)
            goto L_0x0096
        L_0x008c:
            r0 = 2131893079(0x7f121b57, float:1.9420924E38)
            java.lang.String r7 = r2.getString(r0)
            r0 = 2131232251(0x7f0805fb, float:1.8080606E38)
        L_0x0096:
            android.graphics.drawable.Drawable r6 = X.C12970iu.A0C(r2, r0)
            goto L_0x0039
        L_0x009b:
            java.lang.String r0 = "Unknown categoryType: "
            java.lang.String r0 = X.C12960it.A0b(r0, r10)
            java.lang.RuntimeException r0 = X.C12990iw.A0m(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass35G.A09(java.lang.Integer):void");
    }
}
