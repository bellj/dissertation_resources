package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.0Kk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04110Kk {
    public static Handler A00(Looper looper) {
        return Handler.createAsync(looper);
    }
}
