package X;

import android.content.Context;
import com.whatsapp.stickers.StickerStorePackPreviewActivity;

/* renamed from: X.4qz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103584qz implements AbstractC009204q {
    public final /* synthetic */ StickerStorePackPreviewActivity A00;

    public C103584qz(StickerStorePackPreviewActivity stickerStorePackPreviewActivity) {
        this.A00 = stickerStorePackPreviewActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
