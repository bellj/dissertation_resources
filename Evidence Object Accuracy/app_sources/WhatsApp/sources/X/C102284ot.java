package X;

import android.view.View;
import android.widget.AbsListView;
import com.whatsapp.location.LiveLocationPrivacyActivity;

/* renamed from: X.4ot  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C102284ot implements AbsListView.OnScrollListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ LiveLocationPrivacyActivity A01;

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public C102284ot(LiveLocationPrivacyActivity liveLocationPrivacyActivity, int i) {
        this.A01 = liveLocationPrivacyActivity;
        this.A00 = i;
    }

    @Override // android.widget.AbsListView.OnScrollListener
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        View view;
        float f;
        if (i + i2 == i3) {
            int bottom = absListView.getChildAt(i2 - 1).getBottom();
            LiveLocationPrivacyActivity liveLocationPrivacyActivity = this.A01;
            int bottom2 = liveLocationPrivacyActivity.A04.getBottom() - liveLocationPrivacyActivity.A04.getPaddingBottom();
            view = liveLocationPrivacyActivity.A00;
            if (bottom == bottom2) {
                f = 0.0f;
                view.setElevation(f);
            }
        } else {
            view = this.A01.A00;
        }
        f = (float) this.A00;
        view.setElevation(f);
    }
}
