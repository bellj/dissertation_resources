package X;

import java.util.Locale;

/* renamed from: X.1xH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C43591xH {
    public final String A00;
    public final Locale[] A01;

    public C43591xH(String str, Locale[] localeArr) {
        this.A01 = localeArr;
        this.A00 = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HsmMessagePackEvent{locales=");
        sb.append(AbstractC27291Gt.A08(this.A01));
        sb.append(", namespace='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
