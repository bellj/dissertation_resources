package X;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import com.whatsapp.HomeActivity;

/* renamed from: X.2Mb  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Mb extends InsetDrawable {
    public final /* synthetic */ HomeActivity A00;

    @Override // android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    public void draw(Canvas canvas) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2Mb(Drawable drawable, HomeActivity homeActivity) {
        super(drawable, 0);
        this.A00 = homeActivity;
    }
}
