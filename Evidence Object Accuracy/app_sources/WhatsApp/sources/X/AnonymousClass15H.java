package X;

/* renamed from: X.15H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15H {
    public final C16120oU A00;
    public final AnonymousClass165 A01;

    public AnonymousClass15H(C16120oU r1, AnonymousClass165 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00() {
        AnonymousClass165 r5 = this.A01;
        synchronized (r5.A03) {
            r5.A00().edit().putInt("sticker_pack_delete_count", r5.A00().getInt("sticker_pack_delete_count", 0) + 1).apply();
        }
    }
}
