package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyPhoneNumber;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.2Yz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51832Yz extends BroadcastReceiver {
    public final AnonymousClass01d A00;
    public final C15890o4 A01;
    public final C14820m6 A02;
    public final C20800wL A03;
    public final C862946p A04;
    public final AbstractC14440lR A05;
    public final Object A06 = C12970iu.A0l();
    public final String A07;
    public final String A08;
    public final String A09;
    public final WeakReference A0A;
    public volatile boolean A0B = false;

    public C51832Yz(AnonymousClass01d r2, C15890o4 r3, C14820m6 r4, C20800wL r5, AbstractC44441yy r6, C862946p r7, AbstractC14440lR r8, String str, String str2, String str3) {
        this.A07 = str;
        this.A05 = r8;
        this.A08 = str2;
        this.A09 = str3;
        this.A04 = r7;
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r4;
        this.A03 = r5;
        this.A0A = C12970iu.A10(r6);
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        boolean z;
        if (!this.A0B) {
            synchronized (this.A06) {
                if (!this.A0B) {
                    AnonymousClass22D.A00(context);
                    this.A0B = true;
                }
            }
        }
        Log.i("flashcall/receiving-incoming-call");
        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra("state")) && intent.hasExtra("incoming_number")) {
            String stringExtra = intent.getStringExtra("incoming_number");
            AbstractC44441yy r10 = (AbstractC44441yy) this.A0A.get();
            if (r10 != null) {
                if (TextUtils.isEmpty(stringExtra)) {
                    Log.i("flashcall/Could not retrieve incoming call phone number");
                    this.A04.A02 = true;
                } else {
                    String replaceAll = stringExtra.replaceAll("\\D", "");
                    String str = this.A07;
                    Matcher matcher = Pattern.compile(str).matcher(replaceAll);
                    String replaceAll2 = str.replaceAll("\\D", "");
                    int length = replaceAll.length();
                    int i = length - 7;
                    int lastIndexOf = replaceAll.lastIndexOf(replaceAll2, i - 1);
                    if (!matcher.matches() || length < replaceAll2.length() + 7 || lastIndexOf == -1 || !replaceAll.substring(lastIndexOf, i).equals(replaceAll2)) {
                        Log.i("flashcall/incoming phone number does not match CLI");
                        this.A04.A01 = true;
                    } else {
                        Log.i("flashcall/incoming phone number matches CLI");
                        VerifyPhoneNumber verifyPhoneNumber = (VerifyPhoneNumber) r10;
                        verifyPhoneNumber.A15 = true;
                        verifyPhoneNumber.A2y();
                        if (((ActivityC13790kL) verifyPhoneNumber).A0B.A00() == 8) {
                            verifyPhoneNumber.A2o();
                            verifyPhoneNumber.A0g.A01();
                            Log.i("verifyphonenumber/receive-primary-flash-call/valid-phone-number");
                            verifyPhoneNumber.A38(R.string.verify_flash_call_automatically);
                        } else {
                            Log.i("verifyphonenumber/receive-secondary-flash-call");
                        }
                        Log.i("flashcall/sending code for verification");
                        C862946p r11 = this.A04;
                        if (!this.A01.A06()) {
                            Log.i("flashcall/Cannot end call");
                        } else {
                            int i2 = Build.VERSION.SDK_INT;
                            AnonymousClass01d r0 = this.A00;
                            if (i2 >= 28) {
                                TelecomManager A0L = r0.A0L();
                                if (A0L != null) {
                                    A0L.endCall();
                                    Log.i("flashcall/End call successful");
                                }
                            } else {
                                TelephonyManager A0N = r0.A0N();
                                if (A0N != null) {
                                    try {
                                        Method declaredMethod = A0N.getClass().getDeclaredMethod("getITelephony", new Class[0]);
                                        declaredMethod.setAccessible(true);
                                        Object invoke = declaredMethod.invoke(A0N, new Object[0]);
                                        invoke.getClass().getDeclaredMethod("endCall", new Class[0]).invoke(invoke, new Object[0]);
                                        Log.i("flashcall/End call successful");
                                    } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                                        Log.i("flashcall/Cannot end call", e);
                                    }
                                }
                            }
                            z = true;
                            r11.A00 = Boolean.valueOf(z);
                            AbstractC14440lR r3 = this.A05;
                            String str2 = this.A08;
                            String str3 = this.A09;
                            AnonymousClass009.A05(str3);
                            C626938f r6 = new C626938f(this.A02, null, this.A03, r10, r11, AnonymousClass4AY.AUTO_DETECTED, str2, str3, "flash", null, null, false);
                            String[] A08 = C13000ix.A08();
                            A08[0] = replaceAll;
                            r3.Aaz(r6, A08);
                            return;
                        }
                        z = false;
                        r11.A00 = Boolean.valueOf(z);
                        AbstractC14440lR r3 = this.A05;
                        String str2 = this.A08;
                        String str3 = this.A09;
                        AnonymousClass009.A05(str3);
                        C626938f r6 = new C626938f(this.A02, null, this.A03, r10, r11, AnonymousClass4AY.AUTO_DETECTED, str2, str3, "flash", null, null, false);
                        String[] A08 = C13000ix.A08();
                        A08[0] = replaceAll;
                        r3.Aaz(r6, A08);
                        return;
                    }
                }
                VerifyPhoneNumber verifyPhoneNumber2 = (VerifyPhoneNumber) r10;
                verifyPhoneNumber2.A15 = false;
                verifyPhoneNumber2.A2y();
                if (((ActivityC13790kL) verifyPhoneNumber2).A0B.A00() == 8) {
                    verifyPhoneNumber2.A2o();
                    verifyPhoneNumber2.A0g.A01();
                    Log.i("verifyphonenumber/receive-primary-flash-call/invalid-phone-number");
                    verifyPhoneNumber2.A3G(verifyPhoneNumber2.A2f(), verifyPhoneNumber2.A2g());
                    return;
                }
                Log.i("verifyphonenumber/receive-secondary-flash-call");
            }
        }
    }
}
