package X;

/* renamed from: X.4dV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95314dV {
    public static void A00(int i, int i2) {
        if (i < 0 || i >= i2) {
            throw new IndexOutOfBoundsException();
        }
    }

    public static void A01(Object obj) {
        if (obj == null) {
            throw C72463ee.A0D();
        }
    }

    public static void A02(Object obj, boolean z) {
        if (!z) {
            throw C12960it.A0U(String.valueOf(obj));
        }
    }

    public static void A03(boolean z) {
        if (!z) {
            throw C72453ed.A0h();
        }
    }

    public static void A04(boolean z) {
        if (!z) {
            throw C72463ee.A0D();
        }
    }
}
