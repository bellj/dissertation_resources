package X;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.3IH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3IH {
    public static AnonymousClass3K4 A00;
    public static final Object A01 = C12970iu.A0l();
    public static final Map A02 = new HashMap(4);
    public static final Map A03 = new HashMap(4);
    public static final WeakHashMap A04 = new WeakHashMap();

    public static C89834Lo A00(Context context, AnonymousClass5SC r6) {
        C89834Lo r1 = null;
        synchronized (A01) {
            Map map = A03;
            Map map2 = (Map) map.get(context);
            if (map2 == null) {
                Context context2 = context;
                while ((context2 instanceof ContextWrapper) && !(context2 instanceof Activity) && !(context2 instanceof Application) && !(context2 instanceof Service)) {
                    context2 = ((ContextWrapper) context2).getBaseContext();
                }
                if (!A04.containsKey(context2)) {
                    if (A00 == null) {
                        A00 = new AnonymousClass3K4();
                        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(A00);
                    }
                    map2 = C12970iu.A11();
                    map.put(context, map2);
                }
            }
            Class<?> cls = r6.getClass();
            r1 = (C89834Lo) map2.get(cls);
            if (r1 == null) {
                r1 = new C89834Lo(cls);
                map2.put(cls, r1);
            }
        }
        return r1;
    }

    public static void A01(Context context, Map map) {
        map.remove(context);
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Context context2 = (Context) C12970iu.A15(A0n).getKey();
            Context context3 = context;
            while (context3 instanceof ContextWrapper) {
                context3 = ((ContextWrapper) context3).getBaseContext();
            }
            while (context2 instanceof ContextWrapper) {
                context2 = ((ContextWrapper) context2).getBaseContext();
            }
            if (context2 == context3) {
                A0n.remove();
            }
        }
    }
}
