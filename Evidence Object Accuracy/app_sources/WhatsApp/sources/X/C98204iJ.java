package X;

import android.content.ComponentName;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.HashMap;

/* renamed from: X.4iJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C98204iJ implements Handler.Callback {
    public final /* synthetic */ C94924cl A00;

    public /* synthetic */ C98204iJ(C94924cl r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler.Callback
    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            HashMap hashMap = this.A00.A03;
            synchronized (hashMap) {
                C65083Ib r4 = (C65083Ib) message.obj;
                ServiceConnectionC97804hf r3 = (ServiceConnectionC97804hf) hashMap.get(r4);
                if (r3 != null && r3.A05.isEmpty()) {
                    if (r3.A03) {
                        C94924cl r2 = r3.A06;
                        r2.A04.removeMessages(1, r3.A04);
                        r2.A02.A01(r2.A00, r3);
                        r3.A03 = false;
                        r3.A00 = 2;
                    }
                    hashMap.remove(r4);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            HashMap hashMap2 = this.A00.A03;
            synchronized (hashMap2) {
                C65083Ib r7 = (C65083Ib) message.obj;
                ServiceConnectionC97804hf r5 = (ServiceConnectionC97804hf) hashMap2.get(r7);
                if (r5 != null && r5.A00 == 3) {
                    String valueOf = String.valueOf(r7);
                    StringBuilder A0t = C12980iv.A0t(valueOf.length() + 47);
                    A0t.append("Timeout waiting for ServiceConnection callback ");
                    A0t.append(valueOf);
                    Log.e("GmsClientSupervisor", A0t.toString(), new Exception());
                    ComponentName componentName = r5.A01;
                    if (componentName == null && (componentName = r7.A01) == null) {
                        String str = r7.A03;
                        C13020j0.A01(str);
                        componentName = new ComponentName(str, "unknown");
                    }
                    r5.onServiceDisconnected(componentName);
                }
            }
            return true;
        }
    }
}
