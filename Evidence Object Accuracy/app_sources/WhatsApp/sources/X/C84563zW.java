package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3zW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C84563zW extends AbstractC88024Dy {
    public final UserJid A00;
    public final String A01;

    public C84563zW(UserJid userJid, String str) {
        this.A00 = userJid;
        this.A01 = str;
    }
}
