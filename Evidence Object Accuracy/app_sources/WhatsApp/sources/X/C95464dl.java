package X;

import android.util.Log;
import java.util.Arrays;

/* renamed from: X.4dl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95464dl {
    public static int[] A00 = new int[10];
    public static final Object A01 = C12970iu.A0l();
    public static final byte[] A02 = {0, 0, 0, 1};
    public static final float[] A03 = {1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 2.1818182f, 1.8181819f, 2.909091f, 2.4242425f, 1.6363636f, 1.3636364f, 1.939394f, 1.6161616f, 1.3333334f, 1.5f, 2.0f};

    public static int A00(byte[] bArr, int i) {
        int i2;
        synchronized (A01) {
            int i3 = 0;
            int i4 = 0;
            while (i3 < i) {
                while (true) {
                    if (i3 >= i - 2) {
                        i3 = i;
                        break;
                    }
                    if (bArr[i3] == 0 && bArr[i3 + 1] == 0 && bArr[i3 + 2] == 3) {
                        break;
                    }
                    i3++;
                }
                if (i3 < i) {
                    int[] iArr = A00;
                    int length = iArr.length;
                    if (length <= i4) {
                        iArr = Arrays.copyOf(iArr, length << 1);
                        A00 = iArr;
                    }
                    i4++;
                    iArr[i4] = i3;
                    i3 += 3;
                }
            }
            i2 = i - i4;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < i4; i7++) {
                int i8 = A00[i7] - i6;
                System.arraycopy(bArr, i6, bArr, i5, i8);
                int i9 = i5 + i8;
                int i10 = i9 + 1;
                bArr[i9] = 0;
                i5 = i10 + 1;
                bArr[i10] = 0;
                i6 += i8 + 3;
            }
            System.arraycopy(bArr, i6, bArr, i5, i2 - i5);
        }
        return i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0073, code lost:
        if (r7[r10 - 3] == 0) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0079, code lost:
        if (r7[r10 - 2] != 0) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x007b, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x007e, code lost:
        if (r7[r2] == 1) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x008e, code lost:
        if (r7[r2] != 0) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00a4, code lost:
        if (r8[2] != false) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00a9, code lost:
        if (r8[1] != false) goto L_0x007b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A01(byte[] r7, boolean[] r8, int r9, int r10) {
        /*
            int r5 = r10 - r9
            r6 = 0
            r4 = 1
            boolean r0 = X.C12990iw.A1W(r5)
            X.C95314dV.A04(r0)
            if (r5 == 0) goto L_0x00ac
            boolean r0 = r8[r6]
            if (r0 == 0) goto L_0x001b
            r8[r6] = r6
            r8[r4] = r6
            r0 = 2
            r8[r0] = r6
            int r9 = r9 + -3
            return r9
        L_0x001b:
            r3 = 2
            if (r5 <= r4) goto L_0x002e
            boolean r0 = r8[r4]
            if (r0 == 0) goto L_0x002e
            byte r0 = r7[r9]
            if (r0 != r4) goto L_0x002e
            r8[r6] = r6
            r8[r4] = r6
            r8[r3] = r6
            int r9 = r9 - r3
            return r9
        L_0x002e:
            if (r5 <= r3) goto L_0x0046
            boolean r0 = r8[r3]
            if (r0 == 0) goto L_0x0046
            byte r0 = r7[r9]
            if (r0 != 0) goto L_0x0046
            int r0 = r9 + 1
            byte r0 = r7[r0]
            if (r0 != r4) goto L_0x0046
            r8[r6] = r6
            r8[r4] = r6
            r8[r3] = r6
            int r9 = r9 - r4
            return r9
        L_0x0046:
            int r2 = r10 + -1
            int r9 = r9 + r3
        L_0x0049:
            if (r9 >= r2) goto L_0x006d
            byte r0 = r7[r9]
            r0 = r0 & 254(0xfe, float:3.56E-43)
            if (r0 != 0) goto L_0x006a
            int r1 = r9 + -2
            byte r0 = r7[r1]
            if (r0 != 0) goto L_0x0068
            int r0 = r9 + -1
            byte r0 = r7[r0]
            if (r0 != 0) goto L_0x0068
            byte r0 = r7[r9]
            if (r0 != r4) goto L_0x0068
            r8[r6] = r6
            r8[r4] = r6
            r8[r3] = r6
            return r1
        L_0x0068:
            int r9 = r9 + -2
        L_0x006a:
            int r9 = r9 + 3
            goto L_0x0049
        L_0x006d:
            if (r5 <= r3) goto L_0x00a0
            int r0 = r10 + -3
            byte r0 = r7[r0]
            if (r0 != 0) goto L_0x0080
        L_0x0075:
            int r0 = r10 + -2
            byte r0 = r7[r0]
            if (r0 != 0) goto L_0x0080
        L_0x007b:
            byte r1 = r7[r2]
            r0 = 1
            if (r1 == r4) goto L_0x0081
        L_0x0080:
            r0 = 0
        L_0x0081:
            r8[r6] = r0
            if (r5 <= r4) goto L_0x009b
            int r0 = r10 + -2
            byte r0 = r7[r0]
            if (r0 != 0) goto L_0x0090
        L_0x008b:
            byte r1 = r7[r2]
            r0 = 1
            if (r1 == 0) goto L_0x0091
        L_0x0090:
            r0 = 0
        L_0x0091:
            r8[r4] = r0
            byte r0 = r7[r2]
            if (r0 != 0) goto L_0x0098
            r6 = 1
        L_0x0098:
            r8[r3] = r6
            return r10
        L_0x009b:
            boolean r0 = r8[r3]
            if (r0 == 0) goto L_0x0090
            goto L_0x008b
        L_0x00a0:
            if (r5 != r3) goto L_0x00a7
            boolean r0 = r8[r3]
            if (r0 == 0) goto L_0x0080
            goto L_0x0075
        L_0x00a7:
            boolean r0 = r8[r4]
            if (r0 == 0) goto L_0x0080
            goto L_0x007b
        L_0x00ac:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95464dl.A01(byte[], boolean[], int, int):int");
    }

    public static AnonymousClass4TQ A02(byte[] bArr, int i, int i2) {
        int A012;
        AnonymousClass4YP r5 = new AnonymousClass4YP(bArr, i, i2);
        r5.A05(8);
        int A022 = r5.A02(8);
        int A023 = r5.A02(8);
        int A024 = r5.A02(8);
        int A013 = r5.A01();
        int i3 = 1;
        if (A022 == 100 || A022 == 110 || A022 == 122 || A022 == 244 || A022 == 44 || A022 == 83 || A022 == 86 || A022 == 118 || A022 == 128 || A022 == 138) {
            A012 = r5.A01();
            if (A012 == 3) {
                r5.A03();
            }
            r5.A01();
            r5.A01();
            r5.A03();
            if (r5.A06()) {
                int i4 = 12;
                if (A012 != 3) {
                    i4 = 8;
                }
                int i5 = 0;
                do {
                    if (r5.A06()) {
                        int i6 = 64;
                        if (i5 < 6) {
                            i6 = 16;
                        }
                        int i7 = 8;
                        int i8 = 0;
                        while (true) {
                            int A002 = ((r5.A00() + i7) + 256) % 256;
                            if (A002 != 0) {
                                i7 = A002;
                            }
                            do {
                                i8++;
                                if (i8 >= i6) {
                                    break;
                                }
                            } while (A002 == 0);
                        }
                    }
                    i5++;
                } while (i5 < i4);
            }
        } else {
            A012 = 1;
        }
        r5.A01();
        int A014 = r5.A01();
        if (A014 == 0) {
            r5.A01();
        } else if (A014 == 1) {
            r5.A03();
            r5.A00();
            r5.A00();
            long A015 = (long) r5.A01();
            for (int i9 = 0; ((long) i9) < A015; i9++) {
                r5.A01();
            }
        }
        r5.A01();
        r5.A03();
        int A016 = r5.A01() + 1;
        boolean A06 = r5.A06();
        int i10 = 2 - (A06 ? 1 : 0);
        int A017 = (r5.A01() + 1) * i10;
        if (!A06) {
            r5.A03();
        }
        r5.A03();
        int i11 = A016 << 4;
        int i12 = A017 << 4;
        if (r5.A06()) {
            int A018 = r5.A01();
            int A019 = r5.A01();
            int A0110 = r5.A01();
            int A0111 = r5.A01();
            if (A012 != 0) {
                int i13 = 1;
                if (A012 != 3) {
                    i13 = 2;
                    if (A012 == 1) {
                        i3 = 2;
                    }
                }
                i10 *= i3;
                i3 = i13;
            }
            i11 -= (A018 + A019) * i3;
            i12 -= (A0110 + A0111) * i10;
        }
        float f = 1.0f;
        if (r5.A06() && r5.A06()) {
            int A025 = r5.A02(8);
            if (A025 == 255) {
                int A026 = r5.A02(16);
                int A027 = r5.A02(16);
                if (!(A026 == 0 || A027 == 0)) {
                    f = ((float) A026) / ((float) A027);
                }
            } else {
                float[] fArr = A03;
                if (A025 < fArr.length) {
                    f = fArr[A025];
                } else {
                    Log.w("NalUnitUtil", C12960it.A0W(A025, "Unexpected aspect_ratio_idc value: "));
                }
            }
        }
        return new AnonymousClass4TQ(f, A022, A023, A024, A013, i11, i12);
    }
}
