package X;

import com.whatsapp.mediaview.MediaViewFragment;
import java.lang.ref.WeakReference;

/* renamed from: X.37o  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625237o extends AbstractC16350or {
    public final C15660nh A00;
    public final AnonymousClass1X7 A01;
    public final C22190yg A02;
    public final AnonymousClass19O A03;
    public final WeakReference A04;

    public C625237o(C15660nh r2, MediaViewFragment mediaViewFragment, AnonymousClass1X7 r4, C22190yg r5, AnonymousClass19O r6) {
        this.A02 = r5;
        this.A00 = r2;
        this.A03 = r6;
        this.A04 = C12970iu.A10(mediaViewFragment);
        this.A01 = r4;
    }
}
