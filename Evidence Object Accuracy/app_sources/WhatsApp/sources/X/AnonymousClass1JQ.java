package X;

import com.whatsapp.jid.Jid;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;

/* renamed from: X.1JQ  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1JQ {
    public static final Set A08 = Collections.unmodifiableSet(new HashSet(Arrays.asList("regular", "regular_low", "regular_high", "critical_block", "critical_unblock_low")));
    public static final Set A09 = Collections.unmodifiableSet(new HashSet(Arrays.asList("critical_block", "critical_unblock_low")));
    public AnonymousClass1JR A00;
    public boolean A01;
    public byte[] A02;
    public final int A03;
    public final long A04;
    public final C27791Jf A05;
    public final String A06;
    public final String A07;

    public AnonymousClass1JQ(C27791Jf r1, AnonymousClass1JR r2, String str, String str2, int i, long j, boolean z) {
        this.A04 = j;
        this.A07 = str;
        this.A03 = i;
        this.A00 = r2;
        this.A05 = r1;
        this.A06 = str2;
        this.A01 = z;
    }

    public static String A00(String[] strArr) {
        JSONArray jSONArray = new JSONArray();
        for (String str : strArr) {
            jSONArray.put(str);
        }
        String obj = jSONArray.toString();
        AnonymousClass009.A05(obj);
        return obj;
    }

    public AnonymousClass271 A01() {
        AnonymousClass271 r4 = (AnonymousClass271) C27831Jk.A0R.A0T();
        long j = this.A04;
        r4.A03();
        C27831Jk r1 = (C27831Jk) r4.A00;
        r1.A00 |= 1;
        r1.A01 = j;
        return r4;
    }

    public C27831Jk A02() {
        AnonymousClass271 A01;
        if ((this instanceof C27801Jg) || (A01 = A01()) == null) {
            return null;
        }
        return (C27831Jk) A01.A02();
    }

    public String A03() {
        if (this instanceof C34481gD) {
            return "setting_unarchiveChats";
        }
        if (this instanceof C34551gK) {
            return "time_format";
        }
        if (this instanceof C34591gO) {
            return "star";
        }
        if (this instanceof C34461gB) {
            return "sentinel";
        }
        if (this instanceof C27801Jg) {
            return ((C27801Jg) this).A00[0];
        }
        if (this instanceof C34571gM) {
            return "setting_pushName";
        }
        if (this instanceof C34691gY) {
            return "primary_feature";
        }
        if (this instanceof C34651gU) {
            return "pin_v1";
        }
        if (this instanceof C34611gQ) {
            return "mute";
        }
        if (this instanceof C34521gH) {
            return "markChatAsRead";
        }
        if (this instanceof AnonymousClass1JP) {
            return "setting_locale";
        }
        if (this instanceof AnonymousClass251) {
            return "favoriteSticker";
        }
        if (this instanceof C34411g6) {
            return "deleteMessageForMe";
        }
        if (this instanceof C34371g2) {
            return "deleteChat";
        }
        if (this instanceof C34501gF) {
            return "contact";
        }
        if (!(this instanceof C34671gW)) {
            return !(this instanceof C34631gS) ? "android_unsupported_actions" : "archive";
        }
        return "clearChat";
    }

    public synchronized void A04(boolean z) {
        this.A01 = z;
    }

    public synchronized boolean A05() {
        return this.A01;
    }

    public String[] A06() {
        String[] strArr;
        char c;
        String str;
        String[] strArr2;
        AnonymousClass1IS r1;
        AbstractC14640lm r0;
        String[] strArr3;
        String str2;
        char c2;
        AbstractC14640lm r2;
        char c3;
        String str3;
        Jid jid;
        String str4;
        if (this instanceof C34481gD) {
            strArr = new String[1];
            c = 0;
            str = "setting_unarchiveChats";
        } else if (!(this instanceof C34551gK)) {
            if (!(this instanceof C34591gO)) {
                if (this instanceof C34461gB) {
                    strArr3 = new String[2];
                    strArr3[0] = "sentinel";
                    str2 = this.A06;
                } else if (this instanceof C27801Jg) {
                    return ((C27801Jg) this).A00;
                } else {
                    if (this instanceof C34571gM) {
                        strArr = new String[1];
                        c = 0;
                        str = "setting_pushName";
                    } else if (!(this instanceof C34691gY)) {
                        if (!(this instanceof C34651gU)) {
                            if (this instanceof C34611gQ) {
                                strArr3 = new String[2];
                                strArr3[0] = "mute";
                                jid = ((C34611gQ) this).A01;
                            } else if (this instanceof C34521gH) {
                                r2 = ((C34521gH) this).A01;
                                strArr3 = new String[2];
                                c3 = 0;
                                str3 = "markChatAsRead";
                            } else if (this instanceof AnonymousClass1JP) {
                                strArr = new String[1];
                                c = 0;
                                str = "setting_locale";
                            } else if (this instanceof AnonymousClass251) {
                                strArr3 = new String[2];
                                strArr3[0] = "favoriteSticker";
                                str2 = ((AnonymousClass251) this).A00.A05;
                            } else if (!(this instanceof C34411g6)) {
                                if (this instanceof C34371g2) {
                                    C34371g2 r02 = (C34371g2) this;
                                    AbstractC14640lm r4 = r02.A01;
                                    boolean z = r02.A02;
                                    strArr3 = new String[3];
                                    strArr3[0] = "deleteChat";
                                    strArr3[1] = r4.getRawString();
                                    str2 = z ? "1" : "0";
                                    c2 = 2;
                                } else if (this instanceof C34501gF) {
                                    strArr3 = new String[2];
                                    strArr3[0] = "contact";
                                    jid = ((C34501gF) this).A00;
                                } else if (this instanceof C34671gW) {
                                    C34671gW r03 = (C34671gW) this;
                                    AbstractC14640lm r5 = r03.A01;
                                    boolean z2 = r03.A03;
                                    boolean z3 = r03.A02;
                                    strArr3 = new String[4];
                                    strArr3[0] = "clearChat";
                                    strArr3[1] = r5.getRawString();
                                    if (z2) {
                                        str4 = "1";
                                    } else {
                                        str4 = "0";
                                    }
                                    strArr3[2] = str4;
                                    str2 = z3 ? "1" : "0";
                                    c2 = 3;
                                } else if (!(this instanceof C34631gS)) {
                                    strArr = new String[1];
                                    c = 0;
                                    str = "android_unsupported_actions";
                                } else {
                                    r2 = ((C34631gS) this).A01;
                                    strArr3 = new String[2];
                                    c3 = 0;
                                    str3 = "archive";
                                }
                                strArr3[c2] = str2;
                                return strArr3;
                            } else {
                                C34411g6 r22 = (C34411g6) this;
                                strArr2 = new String[]{"deleteMessageForMe"};
                                r1 = r22.A02;
                                r0 = r22.A01;
                            }
                            str2 = jid.getRawString();
                        } else {
                            r2 = ((C34651gU) this).A00;
                            strArr3 = new String[2];
                            c3 = 0;
                            str3 = "pin_v1";
                        }
                        strArr3[c3] = str3;
                        str2 = r2.getRawString();
                    } else {
                        strArr = new String[1];
                        c = 0;
                        str = "primary_feature";
                    }
                }
                c2 = 1;
                strArr3[c2] = str2;
                return strArr3;
            }
            C34591gO r23 = (C34591gO) this;
            strArr2 = new String[]{"star"};
            r1 = r23.A01;
            r0 = r23.A00;
            return AnonymousClass1K2.A01(r0, r1, strArr2);
        } else {
            strArr = new String[1];
            c = 0;
            str = "time_format";
        }
        strArr[c] = str;
        return strArr;
    }

    public boolean equals(Object obj) {
        byte[] A02;
        byte[] A022;
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1JQ r5 = (AnonymousClass1JQ) obj;
            if (!Arrays.equals(A06(), r5.A06()) || !this.A05.equals(r5.A05)) {
                return false;
            }
            C27831Jk A023 = A02();
            if (A023 == null) {
                A02 = null;
            } else {
                A02 = A023.A02();
            }
            C27831Jk A024 = r5.A02();
            if (A024 == null) {
                A022 = null;
            } else {
                A022 = A024.A02();
            }
            if (!Arrays.equals(A02, A022)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(Arrays.hashCode(A06())), this.A05, A02()});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncMutation{rowId='");
        sb.append(this.A07);
        sb.append('\'');
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName='");
        sb.append(this.A06);
        sb.append('\'');
        sb.append(", version=");
        sb.append(this.A03);
        sb.append(", keyId=");
        sb.append(this.A00);
        sb.append(", areDependenciesMissing=");
        sb.append(this.A01);
        sb.append('}');
        return sb.toString();
    }
}
