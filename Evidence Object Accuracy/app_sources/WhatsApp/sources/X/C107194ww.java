package X;

/* renamed from: X.4ww  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107194ww implements AnonymousClass5X7 {
    public static final double[] A0G = {23.976023976023978d, 24.0d, 25.0d, 29.97002997002997d, 30.0d, 50.0d, 59.94005994005994d, 60.0d};
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public AnonymousClass5X6 A05;
    public String A06;
    public boolean A07;
    public boolean A08;
    public boolean A09;
    public boolean A0A;
    public final C94094bF A0B = new C94094bF();
    public final C92814Xn A0C;
    public final C92494Wd A0D;
    public final C95304dT A0E;
    public final boolean[] A0F = new boolean[4];

    @Override // X.AnonymousClass5X7
    public void AYo() {
    }

    public C107194ww(C92494Wd r3) {
        C95304dT r0;
        this.A0D = r3;
        if (r3 != null) {
            this.A0C = new C92814Xn(178);
            r0 = new C95304dT();
        } else {
            r0 = null;
            this.A0C = null;
        }
        this.A0E = r0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01c7, code lost:
        if (r8 >= 0) goto L_0x0050;
     */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0170  */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A7a(X.C95304dT r28) {
        /*
        // Method dump skipped, instructions count: 459
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107194ww.A7a(X.4dT):void");
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r3, C92824Xo r4) {
        r4.A03();
        this.A06 = r4.A02();
        this.A05 = r3.Af4(r4.A01(), 2);
        C92494Wd r0 = this.A0D;
        if (r0 != null) {
            r0.A00(r3, r4);
        }
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        this.A01 = j;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [boolean, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AnonymousClass5X7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AbP() {
        /*
            r3 = this;
            boolean[] r0 = r3.A0F
            boolean r2 = X.C72453ed.A1W(r0)
            X.4bF r0 = r3.A0B
            r0.A02 = r2
            r0.A00 = r2
            r0.A01 = r2
            X.4Xn r0 = r3.A0C
            if (r0 == 0) goto L_0x0016
            r0.A02 = r2
            r0.A01 = r2
        L_0x0016:
            r0 = 0
            r3.A04 = r0
            r3.A0A = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107194ww.AbP():void");
    }
}
