package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3md  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77033md extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(19);
    public final String A00;
    public final String A01;

    public C77033md(Parcel parcel) {
        super(parcel.readString());
        this.A00 = parcel.readString();
        this.A01 = parcel.readString();
    }

    public C77033md(String str, String str2, String str3) {
        super(str);
        this.A00 = str2;
        this.A01 = str3;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77033md.class != obj.getClass()) {
                return false;
            }
            C77033md r5 = (C77033md) obj;
            if (!super.A00.equals(((AbstractC107404xH) r5).A00) || !AnonymousClass3JZ.A0H(this.A00, r5.A00) || !AnonymousClass3JZ.A0H(this.A01, r5.A01)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = 0;
        int A05 = (C72453ed.A05(super.A00.hashCode()) + C72453ed.A0E(this.A00)) * 31;
        String str = this.A01;
        if (str != null) {
            i = str.hashCode();
        }
        return A05 + i;
    }

    @Override // X.AbstractC107404xH, java.lang.Object
    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(super.A00);
        A0h.append(": description=");
        A0h.append(this.A00);
        A0h.append(": value=");
        return C12960it.A0d(this.A01, A0h);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(super.A00);
        parcel.writeString(this.A00);
        parcel.writeString(this.A01);
    }
}
