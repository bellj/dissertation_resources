package X;

/* renamed from: X.0FP  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0FP extends AbstractC02880Ff {
    public final /* synthetic */ C07720Zy A00;

    @Override // X.AbstractC05330Pd
    public String A01() {
        return "INSERT OR IGNORE INTO `WorkName` (`name`,`work_spec_id`) VALUES (?,?)";
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass0FP(AnonymousClass0QN r1, C07720Zy r2) {
        super(r1);
        this.A00 = r2;
    }

    @Override // X.AbstractC02880Ff
    public void A03(AbstractC12830ic r3, Object obj) {
        AnonymousClass0N5 r4 = (AnonymousClass0N5) obj;
        String str = r4.A00;
        if (str == null) {
            r3.A6T(1);
        } else {
            r3.A6U(1, str);
        }
        String str2 = r4.A01;
        if (str2 == null) {
            r3.A6T(2);
        } else {
            r3.A6U(2, str2);
        }
    }
}
