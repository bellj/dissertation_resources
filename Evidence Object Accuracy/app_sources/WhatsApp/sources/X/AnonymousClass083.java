package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* renamed from: X.083  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass083 extends ContextWrapper {
    public static ArrayList A01;
    public static final Object A02 = new Object();
    public final Resources A00;

    public AnonymousClass083(Context context) {
        super(context);
        this.A00 = new AnonymousClass08G(this, context.getResources());
    }

    public static Context A00(Context context) {
        AnonymousClass083 r1;
        if (!(context instanceof AnonymousClass083) && !(context.getResources() instanceof AnonymousClass08G)) {
            context.getResources();
            if (Build.VERSION.SDK_INT < 21) {
                synchronized (A02) {
                    ArrayList arrayList = A01;
                    if (arrayList != null) {
                        int size = arrayList.size();
                        while (true) {
                            size--;
                            if (size < 0) {
                                break;
                            }
                            WeakReference weakReference = (WeakReference) arrayList.get(size);
                            if (weakReference == null || weakReference.get() == null) {
                                arrayList.remove(size);
                            }
                        }
                        int size2 = arrayList.size();
                        while (true) {
                            size2--;
                            if (size2 < 0) {
                                break;
                            }
                            WeakReference weakReference2 = (WeakReference) A01.get(size2);
                            if (weakReference2 != null && (r1 = (AnonymousClass083) weakReference2.get()) != null && r1.getBaseContext() == context) {
                                return r1;
                            }
                        }
                    } else {
                        A01 = new ArrayList();
                    }
                    AnonymousClass083 r2 = new AnonymousClass083(context);
                    A01.add(new WeakReference(r2));
                    return r2;
                }
            }
        }
        return context;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public AssetManager getAssets() {
        return this.A00.getAssets();
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Resources getResources() {
        return this.A00;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Resources.Theme getTheme() {
        return super.getTheme();
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public void setTheme(int i) {
        super.setTheme(i);
    }
}
