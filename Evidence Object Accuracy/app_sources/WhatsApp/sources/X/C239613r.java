package X;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import com.facebook.redex.RunnableBRunnable0Shape0S0220000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.whatsapp.GifHelper;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.13r  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C239613r {
    public final C14330lG A00;
    public final C14900mE A01;
    public final C15450nH A02;
    public final C16170oZ A03;
    public final AnonymousClass01d A04;
    public final C16590pI A05;
    public final AnonymousClass018 A06;
    public final C15650ng A07;
    public final AnonymousClass12H A08;
    public final AnonymousClass19M A09;
    public final C14850m9 A0A;
    public final C16120oU A0B;
    public final C14410lO A0C;
    public final AnonymousClass109 A0D;
    public final C20740wF A0E;
    public final C16630pM A0F;
    public final C239913u A0G;
    public final AnonymousClass1AB A0H;
    public final C26511Dt A0I;
    public final C22190yg A0J;
    public final AbstractC14440lR A0K;
    public final C26501Ds A0L;

    public C239613r(C14330lG r2, C14900mE r3, C15450nH r4, C16170oZ r5, AnonymousClass01d r6, C16590pI r7, AnonymousClass018 r8, C15650ng r9, AnonymousClass12H r10, AnonymousClass19M r11, C14850m9 r12, C16120oU r13, C14410lO r14, AnonymousClass109 r15, C20740wF r16, C16630pM r17, C239913u r18, AnonymousClass1AB r19, C26511Dt r20, C22190yg r21, AbstractC14440lR r22, C26501Ds r23) {
        this.A0A = r12;
        this.A01 = r3;
        this.A05 = r7;
        this.A0K = r22;
        this.A00 = r2;
        this.A0B = r13;
        this.A09 = r11;
        this.A02 = r4;
        this.A03 = r5;
        this.A0L = r23;
        this.A0C = r14;
        this.A0I = r20;
        this.A0J = r21;
        this.A04 = r6;
        this.A06 = r8;
        this.A07 = r9;
        this.A08 = r10;
        this.A0G = r18;
        this.A0E = r16;
        this.A0H = r19;
        this.A0D = r15;
        this.A0F = r17;
    }

    public void A00(Activity activity, AbstractC13860kS r27, AnonymousClass24S r28, C32731ce r29, String str, List list, List list2, int i, boolean z, boolean z2) {
        C14900mE r4;
        int i2;
        StringBuilder sb = new StringBuilder("sendmedia/sendmedia/size=");
        sb.append(list2.size());
        Log.i(sb.toString());
        ArrayList arrayList = new ArrayList();
        Iterator it = list2.iterator();
        while (it.hasNext()) {
            Uri uri = (Uri) it.next();
            String A0N = C22200yh.A0N(uri, this.A04);
            C22190yg r6 = this.A0J;
            byte A04 = r6.A04(uri);
            if (A04 != 1) {
                if (A04 == 2) {
                    r6.A0D(uri, r27, new AbstractC39321pf(activity, uri, r27, r28, this, r29, str, list, z) { // from class: X.24V
                        public final /* synthetic */ Activity A00;
                        public final /* synthetic */ Uri A01;
                        public final /* synthetic */ AbstractC13860kS A02;
                        public final /* synthetic */ AnonymousClass24S A03;
                        public final /* synthetic */ C239613r A04;
                        public final /* synthetic */ C32731ce A05;
                        public final /* synthetic */ String A06;
                        public final /* synthetic */ List A07;
                        public final /* synthetic */ boolean A08;

                        {
                            this.A04 = r5;
                            this.A08 = r9;
                            this.A07 = r8;
                            this.A06 = r7;
                            this.A05 = r6;
                            this.A03 = r4;
                            this.A01 = r2;
                            this.A02 = r3;
                            this.A00 = r1;
                        }

                        @Override // X.AbstractC39321pf
                        public final void AQW(File file) {
                            C239613r r5 = this.A04;
                            boolean z3 = this.A08;
                            List list3 = this.A07;
                            String str2 = this.A06;
                            C32731ce r62 = this.A05;
                            AnonymousClass24S r42 = this.A03;
                            Uri uri2 = this.A01;
                            AbstractC13860kS r2 = this.A02;
                            Activity activity2 = this.A00;
                            try {
                                if (!r5.A06(r62, null, C14370lK.A05, file, str2, list3, z3, true, false)) {
                                    r42.AQ8();
                                }
                                r42.AY8(uri2);
                            } catch (IOException e) {
                                if (e.getMessage() == null || !e.getMessage().contains("No space")) {
                                    r5.A01.A07(R.string.share_failed, 0);
                                } else {
                                    r5.A01.A0D(r2, activity2.getString(R.string.error_no_disc_space));
                                }
                                Log.e("sendmedia/sendaudio/ioerror ", e);
                            }
                        }
                    });
                } else if (A04 != 3) {
                    if (A04 == 4) {
                        this.A0K.Ab2(new RunnableBRunnable0Shape0S0300000_I0(this, uri, list, 6));
                    } else if (A04 != 9) {
                        if (A04 != 13) {
                            if (!(A04 == 23 || A04 == 37)) {
                            }
                        } else if (z2) {
                            r6.A0D(uri, r27, new AbstractC39321pf(activity, uri, r27, r28, this, r29, str, list, z) { // from class: X.24T
                                public final /* synthetic */ Activity A00;
                                public final /* synthetic */ Uri A01;
                                public final /* synthetic */ AbstractC13860kS A02;
                                public final /* synthetic */ AnonymousClass24S A03;
                                public final /* synthetic */ C239613r A04;
                                public final /* synthetic */ C32731ce A05;
                                public final /* synthetic */ String A06;
                                public final /* synthetic */ List A07;
                                public final /* synthetic */ boolean A08;

                                {
                                    this.A04 = r5;
                                    this.A08 = r9;
                                    this.A07 = r8;
                                    this.A06 = r7;
                                    this.A05 = r6;
                                    this.A03 = r4;
                                    this.A01 = r2;
                                    this.A02 = r3;
                                    this.A00 = r1;
                                }

                                @Override // X.AbstractC39321pf
                                public final void AQW(File file) {
                                    C239613r r5 = this.A04;
                                    boolean z3 = this.A08;
                                    List list3 = this.A07;
                                    String str2 = this.A06;
                                    C32731ce r62 = this.A05;
                                    AnonymousClass24S r1 = this.A03;
                                    Uri uri2 = this.A01;
                                    AbstractC13860kS r2 = this.A02;
                                    Activity activity2 = this.A00;
                                    try {
                                        r5.A06(r62, null, C14370lK.A04, file, str2, list3, z3, true, false);
                                        r1.AY8(uri2);
                                    } catch (IOException e) {
                                        if (e.getMessage() == null || !e.getMessage().contains("No space")) {
                                            r5.A01.A07(R.string.share_failed, 0);
                                        } else {
                                            r5.A01.A0D(r2, activity2.getString(R.string.error_no_disc_space));
                                        }
                                        Log.e("sendmedia/createconv/ioerror ", e);
                                    }
                                }
                            });
                        } else {
                            arrayList.add(uri);
                        }
                    } else if (C15380n4.A0P(list)) {
                        Log.e("sendmedia/senddocument/error: Trying to share a document to status");
                        this.A01.A07(R.string.share_failed, 0);
                    } else {
                        A01(uri, r27, null, A0N, null, list, null, false);
                        r28.AY8(uri);
                    }
                } else if (z2) {
                    r6.A0D(uri, r27, new AbstractC39321pf(activity, uri, r27, r28, this, r29, str, list) { // from class: X.24U
                        public final /* synthetic */ Activity A00;
                        public final /* synthetic */ Uri A01;
                        public final /* synthetic */ AbstractC13860kS A02;
                        public final /* synthetic */ AnonymousClass24S A03;
                        public final /* synthetic */ C239613r A04;
                        public final /* synthetic */ C32731ce A05;
                        public final /* synthetic */ String A06;
                        public final /* synthetic */ List A07;

                        {
                            this.A04 = r5;
                            this.A01 = r2;
                            this.A00 = r1;
                            this.A07 = r8;
                            this.A06 = r7;
                            this.A05 = r6;
                            this.A03 = r4;
                            this.A02 = r3;
                        }

                        @Override // X.AbstractC39321pf
                        public final void AQW(File file) {
                            int i3;
                            Throwable e;
                            byte[] bArr;
                            C239613r r9 = this.A04;
                            Uri uri2 = this.A01;
                            Activity activity2 = this.A00;
                            List list3 = this.A07;
                            String str2 = this.A06;
                            C32731ce r62 = this.A05;
                            AnonymousClass24S r5 = this.A03;
                            AbstractC13860kS r42 = this.A02;
                            try {
                                String queryParameter = uri2.getQueryParameter("doodle");
                                AnonymousClass3JD r13 = null;
                                if (queryParameter != null) {
                                    r13 = AnonymousClass3JD.A01(activity2, r9.A06, r9.A09, r9.A0H, C22200yh.A0I(r9.A00, queryParameter));
                                    if (r13 != null) {
                                        r13.A05 = queryParameter;
                                    } else {
                                        throw new NullPointerException("Doodle object is null");
                                    }
                                }
                                boolean A01 = GifHelper.A01(file);
                                Log.i("sendmedia/sendmediafile - send video");
                                C16150oX r2 = new C16150oX();
                                r2.A0F = file;
                                if (r13 == null) {
                                    bArr = C26521Du.A03(C26521Du.A01(file));
                                } else {
                                    Bitmap A012 = C26521Du.A01(file);
                                    if (A012 == null || (!A012.isMutable() && (A012 = A012.copy(Bitmap.Config.ARGB_8888, true)) == null)) {
                                        bArr = null;
                                    } else {
                                        r13.A07(A012, 0, false, false);
                                        bArr = C26521Du.A03(A012);
                                    }
                                    String str3 = r13.A05;
                                    if (str3 == null) {
                                        String A0L = C22200yh.A0L();
                                        r2.A0H = A0L;
                                        if (!r13.A0A(C22200yh.A0I(r9.A00, A0L))) {
                                            i3 = 0;
                                            try {
                                                r9.A01.A07(R.string.share_failed, 0);
                                                r5.AY8(uri2);
                                            } catch (IOException | NullPointerException e2) {
                                                e = e2;
                                                if (e.getMessage() == null || !e.getMessage().contains("No space")) {
                                                    r9.A01.A07(R.string.share_failed, i3);
                                                } else {
                                                    r9.A01.A0D(r42, activity2.getString(R.string.error_no_disc_space));
                                                }
                                                Log.e("sendmedia/sendvideo/error ", e);
                                                return;
                                            }
                                        }
                                    } else {
                                        r2.A0H = str3;
                                    }
                                }
                                C16170oZ r0 = r9.A03;
                                C14410lO r15 = r9.A0C;
                                byte b = 3;
                                if (A01) {
                                    b = 13;
                                }
                                r0.A05(r15.A00(null, r2, r62, null, str2, list3, null, null, b, 0, 0, false), bArr, false, false);
                                i3 = 0;
                                r5.AY8(uri2);
                            } catch (IOException | NullPointerException e3) {
                                e = e3;
                                i3 = 0;
                            }
                        }
                    });
                } else {
                    arrayList.add(uri);
                }
            }
            if (z2) {
                try {
                    A02(uri, r29, null, null, str, list, AnonymousClass1Y6.A01(uri.getQueryParameter("mentions")), null, i, 0, false, false, false, false, false);
                } catch (C39351pj e) {
                    Log.e("sendmedia/sendimages/share-failed/ ", e);
                    r4 = this.A01;
                    i2 = R.string.error_file_is_not_a_image;
                    r4.A05(i2, 0);
                } catch (IOException e2) {
                    Log.e("sendmedia/sendimages/share-failed/ ", e2);
                    if (e2.getMessage() == null || !e2.getMessage().contains("No space")) {
                        this.A01.A07(R.string.share_failed, 0);
                    } else {
                        r4 = this.A01;
                        i2 = R.string.error_no_disc_space;
                        r4.A05(i2, 0);
                    }
                } catch (OutOfMemoryError e3) {
                    Log.e("sendmedia/sendimages/share-failed/ ", e3);
                    r4 = this.A01;
                    i2 = R.string.error_out_of_memory;
                    r4.A05(i2, 0);
                } catch (SecurityException e4) {
                    Log.e("sendmedia/sendimages/share-failed/ ", e4);
                    this.A01.A05(R.string.no_access_permission, 0);
                }
                r28.AY8(uri);
            } else {
                arrayList.add(uri);
            }
        }
        if (!arrayList.isEmpty()) {
            AnonymousClass24W r5 = new AnonymousClass24W(activity);
            r5.A0C = arrayList;
            r5.A0A = str;
            r5.A0B = C15380n4.A06(list);
            r5.A01 = 5;
            r5.A0G = true;
            if (list.size() > 1) {
                r5.A0H = true;
            }
            if (C15380n4.A0P(list)) {
                r5.A07 = r29;
            }
            activity.startActivity(r5.A00());
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                r28.AY7((Uri) it2.next());
            }
        } else if (list.size() > 1) {
            activity.startActivity(C14960mK.A02(activity));
        }
    }

    public void A01(Uri uri, AbstractC13860kS r30, AbstractC15340mz r31, String str, String str2, List list, List list2, boolean z) {
        StringBuilder sb = new StringBuilder("sendmedia/send-document mime:");
        sb.append(str);
        sb.append(" jids:");
        sb.append(Arrays.deepToString(list.toArray()));
        Log.i(sb.toString());
        AbstractC14440lR r6 = this.A0K;
        C14850m9 r5 = this.A0A;
        C14900mE r12 = this.A01;
        C16590pI r15 = this.A05;
        C14330lG r11 = this.A00;
        C16120oU r4 = this.A0B;
        r6.Aaz(new AnonymousClass24R(uri, r30, r11, r12, this.A03, this.A04, r15, this.A06, r5, r4, this.A0C, r31, this.A0I, this.A0J, str, str2, list, list2, z), new Void[0]);
    }

    public void A02(Uri uri, C32731ce r23, AbstractC14470lU r24, AbstractC15340mz r25, String str, List list, List list2, List list3, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        Bitmap A07 = this.A0J.A07(uri, 100, 100);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        A07.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        A07.recycle();
        C16170oZ r0 = this.A03;
        C14410lO r4 = this.A0C;
        C16150oX r6 = new C16150oX();
        byte b = 1;
        if (z3) {
            b = 42;
        }
        r0.A04(r4.A00(uri, r6, r23, r25, str, list, list2, list3, b, i, i2, z), r24, null, byteArray, z2, z4, z5);
    }

    public void A03(C38421o4 r12, boolean z, boolean z2) {
        boolean z3;
        ArrayList arrayList = new ArrayList();
        CopyOnWriteArrayList copyOnWriteArrayList = r12.A01;
        for (AbstractC15340mz r2 : Collections.unmodifiableList(copyOnWriteArrayList)) {
            AnonymousClass109 r1 = this.A0D;
            if ((r2 instanceof AbstractC16130oV) && r1.A01((AbstractC16130oV) r2) != null) {
                arrayList.add(r2);
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            StringBuilder sb = new StringBuilder("sendmedia/retrymediaupload/already-uploading ");
            AnonymousClass1IS r13 = ((AbstractC15340mz) it.next()).A0z;
            sb.append(r13);
            Log.i(sb.toString());
            r12.A02(r13);
        }
        synchronized (r12) {
            z3 = false;
            if (copyOnWriteArrayList.size() == 0) {
                z3 = true;
            }
        }
        if (z3) {
            Log.i("sendmedia/retrymediaupload/nothing-to-upload");
        } else {
            this.A0K.Ab2(new RunnableBRunnable0Shape0S0220000_I0(this, r12, 0, z, z2));
        }
    }

    public void A04(AbstractC16130oV r4) {
        C16150oX r1 = r4.A02;
        if (r4.A08 == null || !(r1 == null || r1.A0F == null)) {
            A05(r4, true, true);
        } else {
            this.A0E.A01(r4);
        }
        this.A08.A08(r4, -1);
    }

    public void A05(AbstractC16130oV r3, boolean z, boolean z2) {
        C16150oX r1 = r3.A02;
        AnonymousClass009.A05(r1);
        if (z && !r1.A0L) {
            r1.A0L = true;
            this.A07.A0a(r3, -1);
        }
        A03(new C38421o4(Collections.singletonList(r3)), z, z2);
    }

    public boolean A06(C32731ce r20, AbstractC15340mz r21, C14370lK r22, File file, String str, List list, boolean z, boolean z2, boolean z3) {
        int A02;
        File A0F;
        C14850m9 r6 = this.A0A;
        AnonymousClass018 r8 = this.A06;
        C14900mE r7 = this.A01;
        C15450nH r2 = this.A02;
        StringBuilder sb = new StringBuilder("mediafileutils/checkmediafilesize src:");
        sb.append(file.getAbsolutePath());
        Log.i(sb.toString());
        if (r22 == C14370lK.A08) {
            A02 = r6.A02(542);
        } else {
            A02 = r2.A02(AbstractC15460nI.A1p);
        }
        long j = (long) A02;
        if (file.length() > 1048576 * j) {
            StringBuilder sb2 = new StringBuilder("mediafileutils/checkmediafilesize/too large:");
            sb2.append(file.length());
            Log.w(sb2.toString());
            String A0B = r8.A0B(R.string.file_too_large_with_placeholder, r8.A0H(new Object[]{String.format(AnonymousClass018.A00(r8.A00), "%d", Integer.valueOf(A02))}, 280, j));
            if (z) {
                r7.A0E(A0B, 1);
                return false;
            }
            r7.A0L(A0B, 1);
            return false;
        }
        Log.i("sendmedia/sendmediafile");
        if (!z2) {
            A0F = file;
        } else {
            C14330lG r1 = this.A00;
            A0F = C22200yh.A0F(r1, this.A0F, r22, file, 0);
            Log.i("sendmedia/sendmediafile - sending hidden file");
            C14350lI.A0A(r1.A04, file, A0F);
        }
        C16150oX r82 = new C16150oX();
        r82.A0F = A0F;
        byte[] bArr = null;
        if ((r22 == C14370lK.A0X || r22 == C14370lK.A04 || r22 == C14370lK.A0a) && (bArr = C26521Du.A03(C26521Du.A01(A0F))) == null) {
            Log.w("sendmedia/sendmediafile no video thumbnail generated");
        }
        this.A03.A05(this.A0C.A00(null, r82, r20, r21, str, list, null, null, r22.A00, 0, 0, z3), bArr, false, false);
        return true;
    }
}
