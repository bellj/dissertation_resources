package X;

/* renamed from: X.66E  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass66E implements AnonymousClass6Lj {
    public final /* synthetic */ AnonymousClass66I A00;

    public AnonymousClass66E(AnonymousClass66I r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass6Lj
    public void AXX() {
        AnonymousClass66I r3 = this.A00;
        if (!r3.A0G) {
            return;
        }
        if (r3.A0E == 1 || r3.A0E == 7) {
            r3.A0E = 0;
            r3.A09 = Boolean.FALSE;
            r3.A02 = new AnonymousClass6L0("Failed to start operation. Operation timed out.");
        } else if (r3.A0E == 2 || r3.A0E == 3 || r3.A0E == 4) {
            r3.A0E = 0;
        }
    }
}
