package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.redex.RunnableBRunnable0Shape8S0100000_I0_8;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;

/* renamed from: X.3XD  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XD implements AnonymousClass23E {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ ImageComposerFragment A01;
    public final /* synthetic */ AnonymousClass21Y A02;

    @Override // X.AnonymousClass23E
    public /* synthetic */ void A6L() {
    }

    public AnonymousClass3XD(Bundle bundle, ImageComposerFragment imageComposerFragment, AnonymousClass21Y r3) {
        this.A01 = imageComposerFragment;
        this.A00 = bundle;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass23E
    public void AQ8() {
        ActivityC000900k A0B = this.A01.A0B();
        if (A0B != null) {
            A0B.A0d();
        }
    }

    @Override // X.AnonymousClass23E
    public void AWw(Bitmap bitmap, boolean z) {
        Uri uri;
        ImageComposerFragment imageComposerFragment = this.A01;
        Context A0p = imageComposerFragment.A0p();
        if (A0p != null && imageComposerFragment.A08.getTag() == (uri = ((MediaComposerFragment) imageComposerFragment).A00)) {
            if (this.A00 == null) {
                AnonymousClass21Y r1 = this.A02;
                String A08 = ((MediaComposerActivity) r1).A1B.A00(uri).A08();
                String ACe = r1.ACe(((MediaComposerFragment) imageComposerFragment).A00);
                if (A08 != null) {
                    AnonymousClass3JD A03 = AnonymousClass3JD.A03(A0p, ((MediaComposerFragment) imageComposerFragment).A07, ((MediaComposerFragment) imageComposerFragment).A08, ((MediaComposerFragment) imageComposerFragment).A0H, A08);
                    if (A03 != null) {
                        AnonymousClass2Ab r12 = ((MediaComposerFragment) imageComposerFragment).A0D;
                        r12.A0H.setDoodle(A03);
                        r12.A0O.A05(ACe);
                    }
                } else if (!(!((MediaComposerFragment) imageComposerFragment).A0D.A0O.A04.isEmpty())) {
                    RectF rectF = new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight());
                    AnonymousClass2Ab r13 = ((MediaComposerFragment) imageComposerFragment).A0D;
                    r13.A0I.A06 = rectF;
                    r13.A0H.A00 = 0.0f;
                    r13.A05(rectF);
                }
            }
            if (!z) {
                imageComposerFragment.A08.A05(imageComposerFragment.A07.A03);
                ActivityC000900k A0B = imageComposerFragment.A0B();
                if (A0B != null) {
                    A0B.A0d();
                }
            } else {
                AnonymousClass21U r4 = imageComposerFragment.A07;
                if (bitmap != null) {
                    r4.A04 = bitmap;
                    r4.A0B = false;
                }
                r4.A05(null, new RunnableBRunnable0Shape8S0100000_I0_8(r4, 10), r4.A01);
            }
            AnonymousClass21U r0 = imageComposerFragment.A07;
            r0.A04();
            AnonymousClass21W r02 = r0.A0A;
            if (r02 != null) {
                r02.A02();
            }
        }
    }
}
