package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.google.android.material.snackbar.SnackbarContentLayout;

/* renamed from: X.2YL  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YL extends AnimatorListenerAdapter {
    public final /* synthetic */ AbstractC15160mf A00;

    public AnonymousClass2YL(AbstractC15160mf r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A00.A02();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        SnackbarContentLayout snackbarContentLayout = (SnackbarContentLayout) this.A00.A06;
        snackbarContentLayout.A03.setAlpha(0.0f);
        long j = (long) 180;
        long j2 = (long) 70;
        snackbarContentLayout.A03.animate().alpha(1.0f).setDuration(j).setStartDelay(j2).start();
        if (snackbarContentLayout.A02.getVisibility() == 0) {
            snackbarContentLayout.A02.setAlpha(0.0f);
            snackbarContentLayout.A02.animate().alpha(1.0f).setDuration(j).setStartDelay(j2).start();
        }
    }
}
