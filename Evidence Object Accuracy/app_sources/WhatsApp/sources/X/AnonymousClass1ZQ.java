package X;

import android.os.Parcel;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1ZQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZQ {
    public long A00;
    public final Map A01;

    public AnonymousClass1ZQ() {
        this.A00 = 0;
        this.A01 = new HashMap();
    }

    public AnonymousClass1ZQ(Parcel parcel) {
        this.A00 = parcel.readLong();
        HashMap hashMap = new HashMap();
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            hashMap.put(Integer.valueOf(parcel.readInt()), parcel.readString());
        }
        this.A01 = hashMap;
    }
}
