package X;

import android.view.View;
import androidx.constraintlayout.widget.Group;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2hW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54932hW extends AnonymousClass03U {
    public final Group A00;
    public final TextEmojiLabel A01;
    public final AnonymousClass3CN A02;

    public C54932hW(View view, AnonymousClass3CN r4) {
        super(view);
        C12960it.A0y(AnonymousClass028.A0D(view, R.id.link_device_button), r4, 17);
        this.A00 = (Group) AnonymousClass028.A0D(view, R.id.privacy_narrative_e2ee_group);
        this.A02 = r4;
        this.A01 = C12970iu.A0T(view, R.id.e2ee_description_text);
    }
}
