package X;

import android.content.Context;
import android.content.res.Resources;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.2d4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53192d4 extends LinearLayout implements AnonymousClass004 {
    public int A00;
    public LinearLayout A01;
    public ThumbnailButton A02;
    public AnonymousClass2P7 A03;
    public boolean A04;

    public C53192d4(Context context) {
        super(context, null, 0);
        if (!this.A04) {
            this.A04 = true;
            generatedComponent();
        }
        LinearLayout.inflate(getContext(), R.layout.call_avatar_view, this);
        setOrientation(1);
        setGravity(1);
        this.A01 = (LinearLayout) findViewById(R.id.contact_photo_layout);
        this.A02 = (ThumbnailButton) findViewById(R.id.contact_photo);
        this.A00 = getResources().getDimensionPixelSize(R.dimen.call_avatar_view_elevation);
    }

    public int A00(int i) {
        Resources resources = getResources();
        if (i == 1) {
            return resources.getDimensionPixelSize(R.dimen.call_avatar_view_photo);
        }
        int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.call_avatar_view_photo_with_two_participants);
        if (i >= 2) {
            return dimensionPixelSize - ((i - 2) * getResources().getDimensionPixelSize(R.dimen.call_avatar_view_photo_participants_difference));
        }
        return 0;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A03;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A03 = r0;
        }
        return r0.generatedComponent();
    }

    public ThumbnailButton getContactPhoto() {
        return this.A02;
    }

    public LinearLayout getContactPhotoLayout() {
        return this.A01;
    }
}
