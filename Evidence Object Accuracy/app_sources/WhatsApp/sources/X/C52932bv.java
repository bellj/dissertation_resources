package X;

import android.app.Activity;
import android.content.Context;
import android.widget.FrameLayout;

/* renamed from: X.2bv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52932bv extends FrameLayout {
    public int A00 = -1;
    public final /* synthetic */ Activity A01;
    public final /* synthetic */ C42561vP A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52932bv(Activity activity, Context context, C42561vP r4) {
        super(context);
        this.A02 = r4;
        this.A01 = activity;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int rotation = this.A01.getWindowManager().getDefaultDisplay().getRotation();
        int i5 = this.A00;
        if (!(i5 == -1 || i5 == rotation)) {
            C42561vP r2 = this.A02;
            if (!r2.A03 || r2.A04) {
                C42561vP.A01(r2);
            } else {
                r2.A04 = true;
                r2.A08.getViewTreeObserver().addOnGlobalLayoutListener(r2.A02);
            }
        }
        this.A00 = rotation;
        super.onLayout(z, i, i2, i3, i4);
    }
}
