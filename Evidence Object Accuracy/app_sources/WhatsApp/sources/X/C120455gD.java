package X;

import android.content.Context;

/* renamed from: X.5gD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120455gD extends C126705tJ {
    public C119755f3 A00;
    public AbstractC136256Lv A01;
    public final Context A02;
    public final C14900mE A03;
    public final C18640sm A04;
    public final AnonymousClass102 A05;
    public final C17220qS A06;
    public final C1329668y A07;
    public final C21860y6 A08;
    public final C18650sn A09;
    public final C17070qD A0A;
    public final C18590sh A0B;

    public C120455gD(Context context, C14900mE r3, C18640sm r4, AnonymousClass102 r5, C17220qS r6, C1308460e r7, C1329668y r8, C21860y6 r9, C18650sn r10, C18610sj r11, C17070qD r12, AbstractC136256Lv r13, C18590sh r14) {
        super(r7.A04, r11);
        this.A02 = context;
        this.A03 = r3;
        this.A06 = r6;
        this.A0B = r14;
        this.A0A = r12;
        this.A08 = r9;
        this.A05 = r5;
        this.A04 = r4;
        this.A09 = r10;
        this.A07 = r8;
        this.A01 = r13;
    }
}
