package X;

/* renamed from: X.1j3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35951j3 extends AbstractC16110oT {
    public Boolean A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public Long A04;

    public C35951j3() {
        super(2810, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A02);
        r3.Abe(4, this.A03);
        r3.Abe(3, this.A04);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamArchiveStateDaily {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "settingsKeepChatsArchived", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalGroupArchivedChats", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalIndividualArchivedChats", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalUnreadGroupArchivedChats", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "totalUnreadIndividualArchivedChats", this.A04);
        sb.append("}");
        return sb.toString();
    }
}
