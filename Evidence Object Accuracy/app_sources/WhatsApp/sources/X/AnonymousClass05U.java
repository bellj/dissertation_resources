package X;

import java.util.Map;

/* renamed from: X.05U  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05U implements Map.Entry {
    public AnonymousClass05U A00;
    public AnonymousClass05U A01;
    public final Object A02;
    public final Object A03;

    public AnonymousClass05U(Object obj, Object obj2) {
        this.A02 = obj;
        this.A03 = obj2;
    }

    @Override // java.util.Map.Entry, java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AnonymousClass05U)) {
            return false;
        }
        AnonymousClass05U r4 = (AnonymousClass05U) obj;
        if (!this.A02.equals(r4.A02) || !this.A03.equals(r4.A03)) {
            return false;
        }
        return true;
    }

    @Override // java.util.Map.Entry
    public Object getKey() {
        return this.A02;
    }

    @Override // java.util.Map.Entry
    public Object getValue() {
        return this.A03;
    }

    @Override // java.util.Map.Entry, java.lang.Object
    public int hashCode() {
        return this.A02.hashCode() ^ this.A03.hashCode();
    }

    @Override // java.util.Map.Entry
    public Object setValue(Object obj) {
        throw new UnsupportedOperationException("An entry modification is not supported");
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A02);
        sb.append("=");
        sb.append(this.A03);
        return sb.toString();
    }
}
