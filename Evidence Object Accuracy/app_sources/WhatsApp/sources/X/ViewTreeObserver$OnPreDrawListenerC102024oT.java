package X;

import android.view.ViewTreeObserver;

/* renamed from: X.4oT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC102024oT implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ C15350n0 A00;
    public final /* synthetic */ AnonymousClass1OY A01;
    public final /* synthetic */ AnonymousClass1IS A02;

    public ViewTreeObserver$OnPreDrawListenerC102024oT(C15350n0 r1, AnonymousClass1OY r2, AnonymousClass1IS r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass1OY r1 = this.A01;
        C12980iv.A1G(r1, this);
        r1.A0e(this.A02);
        return true;
    }
}
