package X;

/* renamed from: X.1AX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1AX {
    public long A00 = 0;
    public long A01 = 0;
    public long A02 = 0;
    public long A03 = 0;
    public long A04 = 0;
    public long A05 = 0;
    public long A06 = 0;
    public final C16120oU A07;
    public final AbstractC21180x0 A08;
    public final boolean A09;
    public final boolean A0A;

    public AnonymousClass1AX(C14850m9 r3, C16120oU r4, AbstractC21180x0 r5) {
        this.A07 = r4;
        this.A08 = r5;
        this.A09 = r3.A07(125);
        this.A0A = r3.A07(980);
    }

    public final void A00(int i, int i2) {
        String str;
        Integer valueOf = Integer.valueOf(i2);
        if (valueOf == null || (str = valueOf.toString()) == null) {
            str = "";
        }
        this.A08.AKw(i, "camera_facing", str);
    }

    public final void A01(int i, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_end");
        String obj = sb.toString();
        if (this.A0A) {
            this.A08.ALB(i, obj);
        }
    }

    public final void A02(int i, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("_start");
        String obj = sb.toString();
        if (this.A0A) {
            this.A08.ALB(i, obj);
        }
    }

    public final void A03(Integer num, int i, int i2) {
        String str;
        String str2;
        AbstractC21180x0 r2 = this.A08;
        if (num.intValue() == 0) {
            str = "api_1";
        } else {
            str = "api_2";
        }
        r2.AKw(i, "camera_api", str);
        if (i2 == 1) {
            str2 = "camera_core";
        } else {
            str2 = "wa";
        }
        r2.AKw(i, "camera_type", str2);
    }

    public final void A04(String str, String str2) {
        if (this.A0A) {
            AbstractC21180x0 r2 = this.A08;
            if (!r2.AJj(554251647)) {
                r2.ALG(554251647, "startup_type", str);
                r2.AKw(554251647, "origin", str2);
            }
        }
    }

    public void A05(short s) {
        if (this.A0A) {
            this.A08.AL7(554251647, s);
        }
    }
}
