package X;

import android.graphics.drawable.Drawable;
import java.io.File;

/* renamed from: X.1og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C38771og {
    public int A00 = Integer.MAX_VALUE;
    public long A01 = 1048576;
    public Drawable A02;
    public Drawable A03;
    public AbstractC38761of A04;
    public boolean A05 = false;
    public final C14900mE A06;
    public final C18790t3 A07;
    public final C18810t5 A08;
    public final File A09;
    public final String A0A;

    public C38771og(C14900mE r3, C18790t3 r4, C18810t5 r5, File file, String str) {
        this.A06 = r3;
        this.A07 = r4;
        this.A08 = r5;
        this.A09 = file;
        this.A0A = str;
    }

    public C38721ob A00() {
        return new C38721ob(this.A06, this.A08, this, this.A00);
    }
}
