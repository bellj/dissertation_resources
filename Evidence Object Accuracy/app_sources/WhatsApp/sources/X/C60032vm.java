package X;

import com.google.android.material.chip.Chip;
import com.whatsapp.R;

/* renamed from: X.2vm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60032vm extends C60052vo {
    public C60032vm(Chip chip, AnonymousClass2Jw r2) {
        super(chip, r2);
    }

    @Override // X.C60052vo, X.AbstractC75703kH
    public void A08(AnonymousClass4UW r4) {
        Chip chip = ((C60052vo) this).A00;
        chip.setChipIconResource(R.drawable.ic_business_hours);
        chip.setChipIconVisible(true);
        super.A08(r4);
        C12970iu.A19(chip.getContext(), chip, R.string.biz_dir_filter_open_now);
        C12960it.A0r(chip.getContext(), chip, R.string.biz_dir_filter_open_now);
        C12960it.A13(chip, this, r4, 15);
    }
}
