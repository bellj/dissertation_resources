package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78393oq extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99074ji();
    public final int A00;
    public final long A01;
    public final boolean A02;
    public final C78313oi[] A03;

    public C78393oq(C78313oi[] r2, int i, long j, boolean z) {
        this.A01 = j;
        this.A03 = r2;
        this.A02 = z;
        if (z) {
            this.A00 = i;
        } else {
            this.A00 = -1;
        }
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A08(parcel, 2, this.A01);
        C95654e8.A0I(parcel, this.A03, 3, i);
        C95654e8.A07(parcel, 4, this.A00);
        C95654e8.A09(parcel, 5, this.A02);
        C95654e8.A06(parcel, A00);
    }
}
