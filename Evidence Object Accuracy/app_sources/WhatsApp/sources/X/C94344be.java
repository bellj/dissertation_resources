package X;

/* renamed from: X.4be  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94344be {
    public static final C94344be A03 = new C94344be(1.0f, 1.0f);
    public final float A00;
    public final float A01;
    public final int A02;

    public C94344be(float f, float f2) {
        boolean z = true;
        C95314dV.A03(C12960it.A1U((f > 0.0f ? 1 : (f == 0.0f ? 0 : -1))));
        C95314dV.A03(f2 <= 0.0f ? false : z);
        this.A01 = f;
        this.A00 = f2;
        this.A02 = Math.round(f * 1000.0f);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C94344be.class != obj.getClass()) {
                return false;
            }
            C94344be r5 = (C94344be) obj;
            if (!(this.A01 == r5.A01 && this.A00 == r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return C72453ed.A05(Float.floatToRawIntBits(this.A01)) + Float.floatToRawIntBits(this.A00);
    }

    public String toString() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = Float.valueOf(this.A01);
        A1a[1] = Float.valueOf(this.A00);
        return C72463ee.A0G("PlaybackParameters(speed=%.2f, pitch=%.2f)", A1a);
    }
}
