package X;

/* renamed from: X.0cB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC08990cB implements Runnable {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    public RunnableC08990cB(LayoutInflater$Factory2C011505o r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0020, code lost:
        if (r1 == false) goto L_0x0022;
     */
    @Override // java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            X.05o r4 = r5.A00
            android.widget.PopupWindow r2 = r4.A09
            androidx.appcompat.widget.ActionBarContextView r1 = r4.A0L
            r0 = 55
            r3 = 0
            r2.showAtLocation(r1, r0, r3, r3)
            X.0QQ r0 = r4.A0N
            if (r0 == 0) goto L_0x0013
            r0.A00()
        L_0x0013:
            boolean r0 = r4.A0h
            if (r0 == 0) goto L_0x0022
            android.view.ViewGroup r0 = r4.A07
            if (r0 == 0) goto L_0x0022
            boolean r1 = X.AnonymousClass028.A0r(r0)
            r0 = 1
            if (r1 != 0) goto L_0x0023
        L_0x0022:
            r0 = 0
        L_0x0023:
            r2 = 1065353216(0x3f800000, float:1.0)
            androidx.appcompat.widget.ActionBarContextView r1 = r4.A0L
            if (r0 == 0) goto L_0x0041
            r0 = 0
            r1.setAlpha(r0)
            androidx.appcompat.widget.ActionBarContextView r0 = r4.A0L
            X.0QQ r1 = X.AnonymousClass028.A0F(r0)
            r1.A02(r2)
            r4.A0N = r1
            X.0Df r0 = new X.0Df
            r0.<init>(r5)
            r1.A09(r0)
            return
        L_0x0041:
            r1.setAlpha(r2)
            androidx.appcompat.widget.ActionBarContextView r0 = r4.A0L
            r0.setVisibility(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.RunnableC08990cB.run():void");
    }
}
