package X;

/* renamed from: X.26t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C466726t extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public /* synthetic */ C466726t() {
        super(AnonymousClass25E.A03);
    }

    public void A05(int i) {
        A03();
        AnonymousClass25E r1 = (AnonymousClass25E) this.A00;
        r1.A00 |= 1;
        r1.A01 = i;
    }

    public void A06(AbstractC27881Jp r3) {
        A03();
        AnonymousClass25E r1 = (AnonymousClass25E) this.A00;
        r1.A00 |= 2;
        r1.A02 = r3;
    }
}
