package X;

import android.app.Activity;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S1300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape3S0100000_I0_3;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.13b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C238013b {
    public C43741xW A00;
    public boolean A01;
    public boolean A02;
    public final AnonymousClass18R A03;
    public final AbstractC15710nm A04;
    public final C22260yn A05;
    public final C14900mE A06;
    public final C15450nH A07;
    public final C16240og A08;
    public final C26631Ef A09;
    public final C43671xP A0A;
    public final AnonymousClass10S A0B;
    public final C26311Cv A0C;
    public final C22700zV A0D;
    public final C15610nY A0E;
    public final C20020v5 A0F;
    public final C14830m7 A0G;
    public final C14820m6 A0H;
    public final AnonymousClass018 A0I;
    public final C15650ng A0J;
    public final AnonymousClass10Y A0K;
    public final C20140vH A0L;
    public final C14850m9 A0M;
    public final C16120oU A0N;
    public final C17220qS A0O;
    public final C22140ya A0P;
    public final AbstractC14440lR A0Q;
    public final C14860mA A0R;
    public final Set A0S = new HashSet();

    public C238013b(AnonymousClass18R r3, AbstractC15710nm r4, C22260yn r5, C14900mE r6, C15450nH r7, C16240og r8, C26631Ef r9, AnonymousClass10S r10, C26311Cv r11, C22700zV r12, C15610nY r13, C20020v5 r14, C14830m7 r15, C14820m6 r16, AnonymousClass018 r17, C15650ng r18, AnonymousClass10Y r19, C20140vH r20, C14850m9 r21, C16120oU r22, C17220qS r23, C22140ya r24, AbstractC14440lR r25, C232010t r26, C14860mA r27) {
        C43671xP r1 = new C43671xP(r26);
        this.A0G = r15;
        this.A03 = r3;
        this.A06 = r6;
        this.A04 = r4;
        this.A0Q = r25;
        this.A0L = r20;
        this.A0R = r27;
        this.A07 = r7;
        this.A0O = r23;
        this.A0E = r13;
        this.A0I = r17;
        this.A05 = r5;
        this.A0B = r10;
        this.A0J = r18;
        this.A0K = r19;
        this.A08 = r8;
        this.A0F = r14;
        this.A0H = r16;
        this.A0P = r24;
        this.A0C = r11;
        this.A09 = r9;
        this.A0A = r1;
        this.A0N = r22;
        this.A0M = r21;
        this.A0D = r12;
    }

    public static int A00(String str) {
        switch (str.hashCode()) {
            case -449631153:
                if (str.equals("otp_did_not_request")) {
                    return 5;
                }
                break;
            case -119378578:
                if (str.equals("offensive_messages")) {
                    return 4;
                }
                break;
            case 3536713:
                if (str.equals("spam")) {
                    return 3;
                }
                break;
            case 291932813:
                if (str.equals("no_longer_needed")) {
                    return 1;
                }
                break;
            case 1245889503:
                if (str.equals("no_sign_up")) {
                    return 2;
                }
                break;
        }
        return 0;
    }

    public List A01(AbstractC14640lm r15) {
        ArrayList arrayList = new ArrayList();
        AnonymousClass10Y r5 = this.A0K;
        for (Pair pair : r5.A06(r15, 5)) {
            AbstractC15340mz r10 = (AbstractC15340mz) pair.first;
            C43721xU r4 = new C43721xU();
            if (r10 instanceof C28851Pg) {
                r4.A01 = ((C28851Pg) r10).A00.A03;
            }
            long j = r10.A0I;
            boolean z = false;
            if (r15 != null) {
                String[] strArr = {String.valueOf(r5.A03.A02(r15)), String.valueOf(j)};
                C16310on A01 = r5.A09.get();
                try {
                    Cursor A09 = A01.A03.A09("SELECT  1  FROM available_message_view WHERE chat_row_id = ? AND timestamp >= ? AND message_type NOT IN ('7') AND from_me = 1 LIMIT 1", strArr);
                    if (A09 == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("msgstore/get/no outgoing message for: ");
                        sb.append(r15);
                        sb.append(" after ");
                        sb.append(j);
                        Log.i(sb.toString());
                    } else {
                        if (A09.getCount() > 0) {
                            z = true;
                        }
                        A09.close();
                    }
                    A01.close();
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            r4.A02 = z;
            r4.A00 = Long.valueOf(TimeUnit.MILLISECONDS.toHours(r10.A0I) * TimeUnit.HOURS.toSeconds(1));
            arrayList.add(r4);
        }
        return arrayList;
    }

    public Map A02() {
        String string = this.A09.A00().getString("biz_block_reasons", null);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    linkedHashMap.put(next, jSONObject.get(next));
                }
            } catch (JSONException unused) {
            }
        }
        return linkedHashMap;
    }

    public synchronized Set A03() {
        return new HashSet(this.A0S);
    }

    public void A04() {
        HashSet hashSet;
        synchronized (this) {
            Set set = this.A0S;
            hashSet = new HashSet(set);
            set.clear();
            A05();
            this.A05.A01(new RunnableBRunnable0Shape3S0100000_I0_3(this, 15));
        }
        if (!hashSet.isEmpty()) {
            this.A0B.A08(hashSet);
        }
    }

    public void A05() {
        C14820m6 r1 = this.A0H;
        r1.A0d(null);
        r1.A00.edit().remove("block_list_receive_time").apply();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0043, code lost:
        if (r4.equals(r1) != false) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001e, code lost:
        if (r1.equals(r3) == false) goto L_0x0020;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
        // Method dump skipped, instructions count: 290
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C238013b.A06():void");
    }

    public final void A07(Activity activity, AnonymousClass1P3 r23, AnonymousClass1P2 r24, AnonymousClass1P4 r25, boolean z) {
        Integer num;
        String A01;
        String str;
        AnonymousClass1W9[] r3;
        boolean z2;
        String str2;
        AnonymousClass1P3 r11 = r23;
        boolean z3 = r24.A07;
        String str3 = r24.A05;
        if (str3 != null) {
            num = Integer.valueOf(A00(str3));
        } else {
            num = null;
        }
        C20020v5 r6 = this.A0F;
        UserJid userJid = r24.A02;
        synchronized (r6) {
            if (r6.A0M.A07(1034) && !C15380n4.A0J(userJid)) {
                SharedPreferences A04 = r6.A04();
                String rawString = userJid.getRawString();
                StringBuilder sb = new StringBuilder();
                sb.append(rawString);
                sb.append("_integrity");
                String obj = sb.toString();
                C40631ru A00 = C40631ru.A00(A04.getString(obj, "0,null,null"));
                A00.A02 = num;
                A00.A01 = Boolean.valueOf(z3);
                A04.edit().putString(obj, A00.toString()).apply();
            }
        }
        if (this.A08.A06) {
            if (r25 == null && r23 == null) {
                r11 = new AnonymousClass1P3(activity, this, z3) { // from class: X.1xX
                    public final /* synthetic */ Activity A00;
                    public final /* synthetic */ C238013b A01;
                    public final /* synthetic */ boolean A02;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                        this.A02 = r3;
                    }

                    @Override // X.AnonymousClass1P3
                    public final void AVM(boolean z4) {
                        C238013b r2 = this.A01;
                        Activity activity2 = this.A00;
                        boolean z5 = this.A02;
                        if (!z4) {
                            r2.A0H.A00.edit().remove("block_list_receive_time").apply();
                            if (activity2 != null && !activity2.isFinishing()) {
                                C14900mE r22 = r2.A06;
                                int i = R.string.unblock_timeout;
                                if (z5) {
                                    i = R.string.block_timeout;
                                }
                                r22.A0L(activity2.getString(i), 0);
                            }
                        }
                    }
                };
            }
            C43661xO A002 = this.A0C.A00(userJid);
            C14900mE r10 = this.A06;
            C43761xY r7 = new C43761xY(activity, this.A04, r10, r11, this, r24, A002, this.A0B, this.A0H, this.A0O, r25, this.A0R, z);
            AnonymousClass1P4 r4 = r7.A0B;
            if (r4 != null) {
                A01 = r4.A01;
            } else {
                A01 = r7.A0A.A01();
            }
            C43781xa r5 = new C43781xa(r7.A02, new C43771xZ(r7), r7.A09, r7.A0A);
            AnonymousClass1P2 r112 = r7.A06;
            C43661xO r102 = r7.A07;
            if (r4 != null) {
                str = r4.A00;
            } else {
                str = null;
            }
            Log.i("blocklistv2setprotocolhelper/sendSetBlocklistRequest");
            C17220qS r0 = r5.A03;
            AnonymousClass1V8 r13 = null;
            String string = r5.A02.A00.getString("block_list_v2_dhash", null);
            String str4 = "block";
            if (TextUtils.isEmpty(string)) {
                r3 = new AnonymousClass1W9[2];
                z2 = r112.A07;
                if (!z2) {
                    str4 = "unblock";
                }
                r3[0] = new AnonymousClass1W9("action", str4);
                r3[1] = new AnonymousClass1W9(r112.A02, "jid");
            } else {
                r3 = new AnonymousClass1W9[3];
                z2 = r112.A07;
                if (!z2) {
                    str4 = "unblock";
                }
                r3[0] = new AnonymousClass1W9("action", str4);
                r3[1] = new AnonymousClass1W9(r112.A02, "jid");
                r3[2] = new AnonymousClass1W9("dhash", string);
            }
            if (z2) {
                Boolean bool = r112.A03;
                if (bool == null) {
                    str2 = "none";
                } else {
                    str2 = bool.booleanValue() ? "true" : "false";
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(new AnonymousClass1W9("first_message", str2));
                String str5 = r112.A05;
                if (str5 != null) {
                    arrayList.add(new AnonymousClass1W9("reason", str5));
                    String str6 = r112.A06;
                    if (str6 != null) {
                        arrayList.add(new AnonymousClass1W9("reason_description", str6));
                    }
                }
                String str7 = r112.A04;
                if (str7 != null) {
                    arrayList.add(new AnonymousClass1W9("entry_point", str7));
                }
                if (r102 != null) {
                    arrayList.add(new AnonymousClass1W9("business_discovery_entry_point", r102.A03));
                    arrayList.add(new AnonymousClass1W9("business_discovery_timestamp", r102.A00));
                    String str8 = r102.A02;
                    if (str8 != null) {
                        arrayList.add(new AnonymousClass1W9("business_discovery_id", str8));
                    }
                }
                r13 = new AnonymousClass1V8("biz_opt_out", (AnonymousClass1W9[]) arrayList.toArray(new AnonymousClass1W9[0]));
            }
            AnonymousClass1V8 r103 = new AnonymousClass1V8(r13, "item", r3);
            int i = 5;
            if (str == null) {
                i = 4;
            }
            AnonymousClass1W9[] r32 = new AnonymousClass1W9[i];
            r32[0] = new AnonymousClass1W9(AnonymousClass1VY.A00, "to");
            r32[1] = new AnonymousClass1W9("id", A01);
            r32[2] = new AnonymousClass1W9("type", "set");
            r32[3] = new AnonymousClass1W9("xmlns", "blocklist");
            if (str != null) {
                r32[4] = new AnonymousClass1W9("web", str);
            }
            r0.A0D(r5, new AnonymousClass1V8(r103, "iq", r32), A01, 2, 20000);
        }
    }

    public final void A08(Activity activity, AnonymousClass1P3 r17, C15370n3 r18, String str, String str2, String str3, boolean z, boolean z2) {
        AnonymousClass1P3 r12;
        Boolean bool;
        UserJid userJid = (UserJid) r18.A0B(UserJid.class);
        AnonymousClass009.A05(userJid);
        if (z2) {
            r12 = new AnonymousClass1P3(activity, r17, this, r18, z) { // from class: X.1xQ
                public final /* synthetic */ Activity A00;
                public final /* synthetic */ AnonymousClass1P3 A01;
                public final /* synthetic */ C238013b A02;
                public final /* synthetic */ C15370n3 A03;
                public final /* synthetic */ boolean A04;

                {
                    this.A02 = r3;
                    this.A04 = r5;
                    this.A00 = r1;
                    this.A03 = r4;
                    this.A01 = r2;
                }

                @Override // X.AnonymousClass1P3
                public final void AVM(boolean z3) {
                    C238013b r9 = this.A02;
                    boolean z4 = this.A04;
                    Activity activity2 = this.A00;
                    C15370n3 r7 = this.A03;
                    AnonymousClass1P3 r6 = this.A01;
                    if (z3) {
                        int i = R.string.unblock_confirmation;
                        if (z4) {
                            i = R.string.block_confirmation;
                        }
                        r9.A06.A0E(activity2.getString(i, r9.A0E.A04(r7)), 1);
                    }
                    if (r6 != null) {
                        r6.AVM(z3);
                    }
                }
            };
        } else {
            r12 = new AnonymousClass1P3() { // from class: X.1xT
                @Override // X.AnonymousClass1P3
                public final void AVM(boolean z3) {
                    AnonymousClass1P3 r0 = AnonymousClass1P3.this;
                    if (r0 != null) {
                        r0.AVM(z3);
                    }
                }
            };
        }
        if (!z || str == null || this.A0K.A01(userJid) == null) {
            bool = null;
        } else {
            bool = Boolean.valueOf(!this.A0L.A05(userJid));
        }
        AnonymousClass1P2 r3 = new AnonymousClass1P2(r18, userJid, bool, str, str2, str3, null, z);
        this.A03.A00(activity, new C43691xR(activity, r12, this, r3), r3.A07);
    }

    public void A09(Activity activity, AnonymousClass1P3 r13, UserJid userJid) {
        this.A03.A00(activity, new C43691xR(activity, r13, this, new AnonymousClass1P2(null, userJid, null, null, null, null, null, false)), false);
    }

    public void A0A(Activity activity, C15370n3 r11, UserJid userJid, String str, String str2, String str3) {
        Boolean bool;
        List list = null;
        if (str == null) {
            bool = null;
        } else if (this.A0K.A01(userJid) == null) {
            bool = null;
        } else {
            bool = Boolean.valueOf(!this.A0L.A05(userJid));
        }
        if (A0H(r11, true)) {
            list = A01(userJid);
        }
        A07(activity, null, new AnonymousClass1P2(r11, userJid, bool, str, str2, str3, list, true), null, false);
    }

    public void A0B(Activity activity, C15370n3 r11, boolean z) {
        A08(activity, null, r11, null, null, null, false, z);
    }

    public void A0C(Activity activity, UserJid userJid) {
        A09(activity, null, userJid);
    }

    public final void A0D(UserJid userJid, String str, boolean z) {
        A0E(userJid, z);
        try {
            C16310on A02 = this.A0A.A00.A02();
            if (z) {
                ContentValues contentValues = new ContentValues(1);
                contentValues.put("jid", userJid.getRawString());
                AbstractC21570xd.A04(contentValues, A02, "wa_block_list");
            } else {
                AbstractC21570xd.A02(A02, "wa_block_list", "jid = ?", new String[]{userJid.getRawString()});
            }
            A02.close();
            this.A0H.A0d(str);
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder("contact-mgr-db/unable to update blocked state  ");
            sb.append(userJid);
            sb.append(", ");
            sb.append(z);
            AnonymousClass009.A08(sb.toString(), e);
        }
        this.A06.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(this, 20, userJid));
    }

    public void A0E(UserJid userJid, boolean z) {
        C22140ya r2 = this.A0P;
        long A00 = this.A0G.A00();
        C30581Xz r22 = (C30581Xz) C22140ya.A00(r2.A00, r2.A03.A02(userJid, true), null, 58, A00);
        r22.A00 = z;
        this.A0J.A0r(r22, -1);
    }

    public void A0F(C37241lo r17) {
        AnonymousClass1V8 r5;
        C14830m7 r12 = this.A0G;
        C43731xV r8 = new C43731xV(this.A04, this.A06, this, r12, this.A0H, this.A0O, r17);
        Log.i("getblocklistprotocolhelper/sendGetBlocklistRequest");
        C17220qS r9 = r8.A05;
        String A01 = r9.A01();
        String string = r8.A04.A00.getString("block_list_v2_dhash", null);
        if (TextUtils.isEmpty(string)) {
            r5 = null;
        } else {
            r5 = new AnonymousClass1V8("item", new AnonymousClass1W9[]{new AnonymousClass1W9("dhash", string)});
        }
        r9.A09(r8, new AnonymousClass1V8(r5, "iq", new AnonymousClass1W9[]{new AnonymousClass1W9(AnonymousClass1VY.A00, "to"), new AnonymousClass1W9("id", A01), new AnonymousClass1W9("type", "get"), new AnonymousClass1W9("xmlns", "blocklist")}), A01, 198, 0);
    }

    public synchronized void A0G(String str, Set set) {
        String str2;
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet(set);
        Set set2 = this.A0S;
        hashSet2.removeAll(set2);
        HashSet hashSet3 = new HashSet(set2);
        hashSet3.removeAll(set);
        hashSet.addAll(hashSet2);
        hashSet.addAll(hashSet3);
        boolean z = this.A01;
        SharedPreferences sharedPreferences = this.A0H.A00;
        boolean z2 = false;
        if (sharedPreferences.getLong("block_list_receive_time", 0) != 0) {
            z2 = true;
        }
        this.A01 = true;
        if (!hashSet.isEmpty()) {
            if (z2 && z) {
                StringBuilder sb = new StringBuilder();
                sb.append("old block list: ");
                sb.append(Arrays.toString(set2.toArray()));
                Log.e(sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("new block list: ");
                sb2.append(Arrays.toString(set.toArray()));
                Log.e(sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("added: ");
                sb3.append(Arrays.toString(hashSet2.toArray()));
                Log.e(sb3.toString());
                StringBuilder sb4 = new StringBuilder();
                sb4.append("removed: ");
                sb4.append(Arrays.toString(hashSet3.toArray()));
                Log.e(sb4.toString());
                if (hashSet2.isEmpty() || hashSet3.isEmpty()) {
                    str2 = !hashSet2.isEmpty() ? "Added" : "Removed";
                } else {
                    str2 = "Added/Removed";
                }
                this.A04.AaV("block list de-synchronization", str2, true);
            }
            this.A05.A01(new RunnableBRunnable0Shape0S1300000_I0(this, set, hashSet, str, 2));
        } else {
            sharedPreferences.edit().putLong("block_list_receive_time", this.A0G.A00()).apply();
            if (!z2) {
                this.A0B.A08(Collections.emptyList());
            }
        }
    }

    public boolean A0H(C15370n3 r4, boolean z) {
        UserJid of;
        if (r4 == null) {
            of = null;
        } else {
            of = UserJid.of(r4.A0D);
        }
        return !this.A0M.A07(978) && z && r4 != null && of != null && r4.A0J() && this.A0D.A02(of);
    }

    public synchronized boolean A0I(UserJid userJid) {
        return userJid == null ? false : this.A0S.contains(userJid);
    }
}
