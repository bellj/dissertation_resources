package X;

/* renamed from: X.04i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008504i extends AnonymousClass030 {
    public final AnonymousClass00O A00;

    public C008504i(C007904b r3) {
        AnonymousClass00O r1 = new AnonymousClass00O();
        this.A00 = r1;
        r1.A08(r3.A00);
    }

    @Override // X.AnonymousClass030
    public /* bridge */ /* synthetic */ AnonymousClass032 A01() {
        AnonymousClass0IV r6 = new AnonymousClass0IV();
        AnonymousClass00O r5 = this.A00;
        int size = r5.size();
        for (int i = 0; i < size; i++) {
            Object[] objArr = r5.A02;
            int i2 = i << 1;
            Object obj = objArr[i2];
            r6.mMetricsMap.put(obj, ((AnonymousClass030) objArr[i2 + 1]).A01());
            r6.mMetricsValid.put(obj, Boolean.FALSE);
        }
        return r6;
    }

    @Override // X.AnonymousClass030
    public /* bridge */ /* synthetic */ boolean A02(AnonymousClass032 r9) {
        boolean z;
        Boolean bool;
        AnonymousClass0IV r92 = (AnonymousClass0IV) r9;
        if (r92 != null) {
            AnonymousClass00O r7 = r92.mMetricsMap;
            int size = r7.size();
            boolean z2 = false;
            for (int i = 0; i < size; i++) {
                Class cls = (Class) r7.A02[i << 1];
                AnonymousClass030 r1 = (AnonymousClass030) this.A00.get(cls);
                if (r1 != null) {
                    z = r1.A02(r92.A03(cls));
                } else {
                    z = false;
                }
                AnonymousClass00O r12 = r92.mMetricsValid;
                if (z) {
                    bool = Boolean.TRUE;
                } else {
                    bool = Boolean.FALSE;
                }
                r12.put(cls, bool);
                z2 |= z;
            }
            return z2;
        }
        throw new IllegalArgumentException("Null value passed to getSnapshot!");
    }
}
