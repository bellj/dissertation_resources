package X;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3DJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3DJ {
    public int A00 = 0;
    public final C18790t3 A01;
    public final C22050yP A02;
    public final C18810t5 A03;
    public final String A04 = C12990iw.A0n();
    public final ArrayList A05 = C12960it.A0l();
    public final AtomicInteger A06 = new AtomicInteger();

    public AnonymousClass3DJ(C18790t3 r2, C22050yP r3, C18810t5 r4) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0061, code lost:
        if (r0 == 3) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00(X.C36011jB r14) {
        /*
            r13 = this;
            X.0yP r5 = r13.A02
            int r0 = r14.A0E
            r1 = 3
            r12 = 2
            if (r0 == r1) goto L_0x0009
            r12 = 4
        L_0x0009:
            int r0 = r13.A00
            if (r0 != 0) goto L_0x0060
            r13.A00 = r1
        L_0x000f:
            r11 = 2
        L_0x0010:
            java.lang.Integer r0 = r14.A02
            int r10 = r0.intValue()
            java.lang.String r9 = r14.A06
            int r8 = r14.A00
            boolean r7 = r14.A0A
            java.lang.String r6 = r14.A0F
            java.util.ArrayList r0 = r14.A08
            int r1 = r0.size()
            long r2 = r14.A01
            X.319 r4 = new X.319
            r4.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r12)
            r4.A03 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            r4.A04 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r10)
            r4.A02 = r0
            r4.A07 = r9
            java.lang.Long r0 = X.C12980iv.A0l(r8)
            r4.A05 = r0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r7)
            r4.A00 = r0
            r4.A08 = r6
            double r0 = (double) r1
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            r4.A01 = r0
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r4.A06 = r0
            X.0oU r0 = r5.A0G
            r0.A06(r4)
            return
        L_0x0060:
            r11 = 4
            if (r0 != r1) goto L_0x0010
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3DJ.A00(X.1jB):void");
    }
}
