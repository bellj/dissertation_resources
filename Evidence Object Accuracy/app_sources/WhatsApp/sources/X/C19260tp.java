package X;

import android.database.Cursor;

@Deprecated
/* renamed from: X.0tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19260tp extends AbstractC18500sY {
    public C19260tp(C18480sW r3) {
        super(r3, "legacy_quoted_order_message", 1);
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        return new AnonymousClass2Ez(-1, 0);
    }
}
