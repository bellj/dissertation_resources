package X;

import android.graphics.PointF;

/* renamed from: X.4Vz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92464Vz {
    public long A00;
    public final C92424Vv A01;
    public final C73023fY A02;
    public final C73023fY A03 = new C73023fY();
    public final C73023fY A04;
    public final C73023fY A05;

    public C92464Vz(PointF pointF, long j) {
        C73023fY r2 = new C73023fY();
        this.A04 = r2;
        C73023fY r1 = new C73023fY();
        this.A05 = r1;
        this.A02 = new C73023fY();
        this.A01 = new C92424Vv(pointF, j);
        this.A00 = j;
        r2.set(pointF);
        r1.set(0.0f, 0.0f);
    }

    public final void A00() {
        C73023fY r3 = this.A02;
        C73023fY r2 = this.A03;
        C73023fY r5 = this.A04;
        float f = ((PointF) r2).x - ((PointF) r5).x;
        ((PointF) r3).x = f;
        float f2 = ((PointF) r2).y - ((PointF) r5).y;
        ((PointF) r3).y = f2;
        float f3 = f * 0.05f;
        ((PointF) r3).x = f3;
        float f4 = f2 * 0.05f;
        ((PointF) r3).y = f4;
        C73023fY r32 = this.A05;
        float f5 = ((PointF) r32).x + f3;
        ((PointF) r32).x = f5;
        float f6 = ((PointF) r32).y + f4;
        ((PointF) r32).y = f6;
        float f7 = f5 * 0.7f;
        ((PointF) r32).x = f7;
        float f8 = f6 * 0.7f;
        ((PointF) r32).y = f8;
        ((PointF) r5).x += f7;
        ((PointF) r5).y += f8;
        long j = (long) (((double) this.A00) + 3.0d);
        this.A00 = j;
        this.A01.A00(r5, j);
    }
}
