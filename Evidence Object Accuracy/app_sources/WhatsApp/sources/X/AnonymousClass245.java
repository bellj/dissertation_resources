package X;

/* renamed from: X.245  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass245 extends AbstractC16110oT {
    public String A00;

    public AnonymousClass245() {
        super(1378, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamBusinessUnmute {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "muteeId", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
