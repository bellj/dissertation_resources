package X;

/* renamed from: X.6L0  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6L0 extends RuntimeException {
    public AnonymousClass6L0(String str) {
        super(str);
    }

    public AnonymousClass6L0(String str, Throwable th) {
        super(str, th);
    }
}
