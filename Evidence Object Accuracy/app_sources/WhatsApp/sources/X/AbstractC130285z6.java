package X;

import com.whatsapp.util.Log;

/* renamed from: X.5z6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC130285z6 {
    public static AbstractC130285z6 A01(C128715wY r2, AnonymousClass1V8 r3, String str) {
        char c = 65535;
        try {
            switch (str.hashCode()) {
                case -1852691096:
                    if (str.equals("SELFIE")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1504126555:
                    if (str.equals("DOCUMENT_UPLOAD")) {
                        c = 2;
                        break;
                    }
                    break;
                case -1362600187:
                    if (str.equals("SMS_OTP")) {
                        c = 0;
                        break;
                    }
                    break;
                case -708597224:
                    if (str.equals("TEXT_INPUT")) {
                        c = 7;
                        break;
                    }
                    break;
                case 74901:
                    if (str.equals("KYC")) {
                        c = '\b';
                        break;
                    }
                    break;
                case 82915:
                    if (str.equals("TDS")) {
                        c = 5;
                        break;
                    }
                    break;
                case 77859202:
                    if (str.equals("REKYC")) {
                        c = 4;
                        break;
                    }
                    break;
                case 504547704:
                    if (str.equals("MANUAL_REVIEW__AUTO_TRIGGERED")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1793934804:
                    if (str.equals("PASSWORD_CHANGE")) {
                        c = 6;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return new C121355hi(r3.A0H("challenge_id"), r3.A0F("sms_otp").A0H("obfuscated_phone_number"));
                case 1:
                    return new C121365hj(r3.A0F("video_selfie"), r3.A0H("challenge_id"));
                case 2:
                    return new C121325hf(r3.A0F("document_upload"), C117295Zj.A0W(r3, "challenge_id"));
                case 3:
                    return new C121335hg(r3.A0F("manual_review").A0F("review_message"));
                case 4:
                    return new C121345hh(r3.A0F("rekyc"), r3.A0H("challenge_id"));
                case 5:
                    return new C121375hk(r3.A0F("tds"), r3.A0H("challenge_id"));
                case 6:
                    return new C121315he(r3.A0H("challenge_id"));
                case 7:
                    if (r2 != null) {
                        return new C121395hm(r2, r3.A0F("text_input"));
                    }
                    return new C121305hd();
                case '\b':
                    return C121385hl.A00(r3.A0F("kyc"));
                default:
                    return new C121305hd();
            }
        } catch (AnonymousClass1V9 unused) {
            Log.e("Can't build StepUpChallenge");
            return null;
        }
    }
}
