package X;

import java.io.Serializable;

/* renamed from: X.1aw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC31681aw implements Serializable {
    public static final long serialVersionUID = 0;

    @Override // java.lang.Object
    public abstract boolean equals(Object obj);

    @Override // java.lang.Object
    public abstract int hashCode();

    @Override // java.lang.Object
    public abstract String toString();

    public Object A00() {
        if (this instanceof C31691ax) {
            return ((C31691ax) this).reference;
        }
        throw new IllegalStateException("value is absent");
    }
}
