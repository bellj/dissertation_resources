package X;

/* renamed from: X.69n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1331169n implements AnonymousClass6MN {
    public final /* synthetic */ C129365xb A00;
    public final /* synthetic */ C128545wH A01;
    public final /* synthetic */ C128925wt A02;

    public C1331169n(C129365xb r1, C128545wH r2, C128925wt r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass6MN
    public void APo(C452120p r2) {
        this.A02.A00(r2);
    }

    @Override // X.AnonymousClass6MN
    public void AX8(String[] strArr) {
        C129365xb r14 = this.A00;
        C18610sj r0 = r14.A04;
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[4];
        r4[1] = new AnonymousClass1W9("token", strArr[C12990iw.A1a("action", "reset-payment-pin", r4) ? 1 : 0]);
        C117295Zj.A1P("credential-id", r14.A0B, r4);
        C117305Zk.A1Q("device-id", r14.A0A.A01(), r4);
        C130775zx r1 = r14.A09;
        C128545wH r15 = this.A01;
        r0.A0F(new C120225fp(r14.A00, r14.A01, r14.A03, r14, r15, this.A02), new AnonymousClass1V8(r15.A02(C130775zx.A00(null, null, "RESET", strArr[1], null, new Object[0], C117295Zj.A03(r1.A01))), "account", r4), "set", C26061Bw.A0L);
    }
}
