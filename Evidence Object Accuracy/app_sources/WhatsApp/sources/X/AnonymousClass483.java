package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.media.AudioManager;

/* renamed from: X.483  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass483 extends AnonymousClass3FX {
    public final BroadcastReceiver A00 = new C72933fP(this);
    public final Context A01;
    public final AnonymousClass01d A02;

    public AnonymousClass483(Context context, AnonymousClass01d r3) {
        this.A01 = context;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass3FX
    public void A01() {
        this.A01.registerReceiver(this.A00, new IntentFilter("android.intent.action.HEADSET_PLUG"));
    }

    @Override // X.AnonymousClass3FX
    public void A02() {
        this.A01.unregisterReceiver(this.A00);
    }

    @Override // X.AnonymousClass3FX
    public boolean A03() {
        AudioManager A0G = this.A02.A0G();
        if (A0G == null) {
            return false;
        }
        return A0G.isWiredHeadsetOn();
    }
}
