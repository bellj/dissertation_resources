package X;

/* renamed from: X.33w  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C620533w extends AnonymousClass4EU {
    public final int A00;
    public final String A01;
    public final String A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C620533w) {
                C620533w r5 = (C620533w) obj;
                if (this.A00 != r5.A00 || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A01, r5.A01)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = this.A00 * 31;
        String str = this.A02;
        int i2 = 0;
        int hashCode = (i + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.A01;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    public C620533w(int i, String str, String str2) {
        this.A00 = i;
        this.A02 = str;
        this.A01 = str2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("EventShowError(errorCode=");
        A0k.append(this.A00);
        A0k.append(", errorTitle=");
        A0k.append((Object) this.A02);
        A0k.append(", errorMessage=");
        return C12960it.A0a(this.A01, A0k);
    }
}
