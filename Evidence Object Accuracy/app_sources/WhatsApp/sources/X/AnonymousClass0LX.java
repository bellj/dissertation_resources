package X;

/* renamed from: X.0LX  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0LX {
    public static void A00(Throwable th) {
        if (Error.class.isInstance(th)) {
            throw ((Throwable) Error.class.cast(th));
        } else if (RuntimeException.class.isInstance(th)) {
            throw ((Throwable) RuntimeException.class.cast(th));
        }
    }
}
