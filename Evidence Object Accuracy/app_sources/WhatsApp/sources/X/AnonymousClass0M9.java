package X;

import com.whatsapp.R;

/* renamed from: X.0M9  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0M9 {
    public static final int[] A00 = {R.attr.keylines, R.attr.statusBarBackground};
    public static final int[] A01 = {16842931, R.attr.layout_anchor, R.attr.layout_anchorGravity, R.attr.layout_behavior, R.attr.layout_dodgeInsetEdges, R.attr.layout_insetEdge, R.attr.layout_keyline};
}
