package X;

import android.database.ContentObserver;
import java.util.Iterator;

/* renamed from: X.3fS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C72963fS extends ContentObserver {
    public final /* synthetic */ C94134bJ A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72963fS(C94134bJ r2) {
        super(null);
        this.A00 = r2;
    }

    @Override // android.database.ContentObserver
    public final void onChange(boolean z) {
        C94134bJ r3 = this.A00;
        synchronized (r3.A03) {
            r3.A06 = null;
        }
        synchronized (r3.A04) {
            Iterator it = r3.A05.iterator();
            if (it.hasNext()) {
                it.next();
                throw C12980iv.A0n("zzk");
            }
        }
    }
}
