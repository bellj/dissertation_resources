package X;

import com.whatsapp.util.Log;
import java.util.HashMap;

/* renamed from: X.6AW  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AW implements AnonymousClass6MQ {
    public final /* synthetic */ AnonymousClass1ZR A00;
    public final /* synthetic */ C120525gK A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ String A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ String A06;
    public final /* synthetic */ HashMap A07;

    public AnonymousClass6AW(AnonymousClass1ZR r1, C120525gK r2, String str, String str2, String str3, String str4, String str5, HashMap hashMap) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = str;
        this.A07 = hashMap;
        this.A05 = str2;
        this.A03 = str3;
        this.A04 = str4;
        this.A06 = str5;
    }

    @Override // X.AnonymousClass6MQ
    public void AOb(C119705ey r11) {
        C120525gK r0 = this.A01;
        AnonymousClass1ZR r1 = r11.A02;
        AnonymousClass009.A05(r1);
        String str = r11.A03;
        r0.A01(r1, this.A00, str, this.A02, this.A05, this.A03, this.A04, this.A06, this.A07);
    }

    @Override // X.AnonymousClass6MQ
    public void APo(C452120p r2) {
        Log.w("PAY: IndiaUpiPinActions: could not fetch VPA information to set pin");
        AnonymousClass6MS r0 = this.A01.A00;
        if (r0 != null) {
            r0.AVu(r2);
        }
    }
}
