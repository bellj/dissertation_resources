package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.17v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C250317v {
    public final C22490zA A00;
    public final C16170oZ A01;
    public final C250217u A02;
    public final C14830m7 A03;
    public final C15650ng A04;
    public final C242114q A05;

    public C250317v(C22490zA r1, C16170oZ r2, C250217u r3, C14830m7 r4, C15650ng r5, C242114q r6) {
        this.A03 = r4;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A05 = r6;
        this.A00 = r1;
    }

    public final List A00(AbstractC14640lm r7, Set set, boolean z) {
        ArrayList arrayList = new ArrayList();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            C461824w r0 = (C461824w) it.next();
            C15650ng r3 = this.A04;
            AbstractC15340mz A03 = r3.A0K.A03(new AnonymousClass1IS(r7, r0.A03, r0.A04));
            if (A03 != null && (!A03.A0v || z)) {
                arrayList.add(A03);
            }
        }
        return arrayList;
    }

    public void A01(C34361g1 r24, AbstractC14640lm r25, boolean z, boolean z2) {
        AnonymousClass1XB r6;
        int i;
        long j = r24.A00;
        long max = Math.max(j, r24.A01);
        C15650ng r8 = this.A04;
        long A07 = r8.A07(r25, max);
        C16510p9 r11 = r8.A0N;
        String valueOf = String.valueOf(A07);
        C14830m7 r15 = r8.A0F;
        String[] strArr = {String.valueOf(r11.A02(r25)), valueOf, String.valueOf(r15.A00()), String.valueOf(1000)};
        C16490p7 r14 = r8.A0t;
        C16310on A01 = r14.get();
        try {
            Cursor A09 = A01.A03.A09("   SELECT _id, sort_id, key_id, from_me, timestamp, receipt_server_timestamp, starred, media_size, status FROM available_message_view WHERE chat_row_id = ? AND sort_id <= ? AND (message_type != '7') AND (expire_timestamp IS NULL OR expire_timestamp >= ? OR keep_in_chat = 1)  ORDER BY sort_id DESC  LIMIT ? ", strArr);
            try {
                ArrayList arrayList = new ArrayList();
                if (A09 != null) {
                    while (A09.moveToNext()) {
                        arrayList.add(new C461924x(A09, r25));
                    }
                    A09.close();
                }
                Iterator it = arrayList.iterator();
                long j2 = Long.MAX_VALUE;
                while (it.hasNext()) {
                    C461924x r1 = (C461924x) it.next();
                    long j3 = r1.A00;
                    if (j3 < j2 && r1.A01 >= j) {
                        j2 = j3;
                    }
                }
                long j4 = -1;
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    C461924x r12 = (C461924x) it2.next();
                    long j5 = r12.A00;
                    if (j5 < j2 && j5 > j4 && r12.A01 < j) {
                        j4 = j5;
                    }
                }
                String[] strArr2 = {String.valueOf(r11.A02(r25)), String.valueOf(j4), valueOf, String.valueOf(max), String.valueOf(r15.A00())};
                C16310on A012 = r14.get();
                try {
                    Cursor A092 = A012.A03.A09("   SELECT _id, sort_id, key_id, from_me, timestamp, receipt_server_timestamp, starred, media_size, status FROM available_message_view WHERE chat_row_id = ? AND sort_id > ? AND sort_id <= ? AND timestamp <= ?  AND message_type = 7 AND (expire_timestamp IS NULL OR expire_timestamp >= ? OR keep_in_chat = 1)  ORDER BY sort_id DESC ", strArr2);
                    try {
                        ArrayList arrayList2 = new ArrayList();
                        if (A092 != null) {
                            while (A092.moveToNext()) {
                                arrayList2.add(new C461924x(A092, r25));
                            }
                            A092.close();
                        }
                        ArrayList arrayList3 = new ArrayList();
                        ArrayList arrayList4 = new ArrayList();
                        Iterator it3 = arrayList.iterator();
                        while (it3.hasNext()) {
                            C461924x r2 = (C461924x) it3.next();
                            if (r2.A00 > j4 && (!r2.A03 || z2)) {
                                if (r2.A01 < j) {
                                    arrayList4.add(r8.A0K.A03(r2.A02));
                                }
                            }
                        }
                        arrayList3.addAll(arrayList4);
                        ArrayList arrayList5 = new ArrayList();
                        Iterator it4 = arrayList2.iterator();
                        while (it4.hasNext()) {
                            C461924x r13 = (C461924x) it4.next();
                            long j6 = r13.A01;
                            if (j6 <= max && (r6 = (AnonymousClass1XB) r8.A0K.A03(r13.A02)) != null && (i = r6.A00) != 67 && (!((Set) C32201bm.A00.get()).contains(Integer.valueOf(i)) || j6 < this.A03.A00() - TimeUnit.DAYS.toMillis(1))) {
                                arrayList5.add(r6);
                            }
                        }
                        arrayList3.addAll(arrayList5);
                        arrayList3.addAll(A00(r25, r24.A02, z2));
                        arrayList3.addAll(A00(r25, r24.A03, z2));
                        if (j4 != -1) {
                            Long valueOf2 = Long.valueOf(j4);
                            if (z2) {
                                this.A05.A02(r25, valueOf2);
                            }
                            r8.A0Q(r25, true);
                            r8.A0O(r25, valueOf2, true, z);
                        }
                        if (!arrayList3.isEmpty()) {
                            r8.A0h(arrayList3, z ? 1 : 0);
                        }
                    } finally {
                    }
                } finally {
                    try {
                        A012.close();
                    } catch (Throwable unused) {
                    }
                }
            } finally {
            }
        } finally {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
        }
    }
}
