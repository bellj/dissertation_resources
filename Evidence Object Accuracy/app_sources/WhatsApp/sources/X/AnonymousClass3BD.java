package X;

import java.util.Arrays;

/* renamed from: X.3BD  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BD {
    public final AnonymousClass1V8 A00;

    public AnonymousClass3BD(AnonymousClass3CT r5, byte[] bArr) {
        C41141sy A00 = C41141sy.A00();
        C41141sy.A01(A00, "xmlns", "w:auth:key");
        C41141sy r2 = new C41141sy("key");
        AnonymousClass3JT.A0C(bArr, 32, 32);
        r2.A01 = bArr;
        A00.A05(r2.A03());
        A00.A07(r5.A00, C12960it.A0l());
        r5.A00(A00, Arrays.asList(new String[0]));
        this.A00 = A00.A03();
    }
}
