package X;

import android.view.animation.Animation;
import com.whatsapp.registration.RegisterPhone;

/* renamed from: X.3x2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83313x2 extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ RegisterPhone A00;

    public C83313x2(RegisterPhone registerPhone) {
        this.A00 = registerPhone;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        RegisterPhone registerPhone = this.A00;
        if (!registerPhone.A0U) {
            registerPhone.A06.setVisibility(8);
        }
    }
}
