package X;

import android.view.View;

/* renamed from: X.0Dg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02620Dg extends AnonymousClass0Y9 {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    public C02620Dg(LayoutInflater$Factory2C011505o r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMC(View view) {
        LayoutInflater$Factory2C011505o r2 = this.A00;
        r2.A0L.setAlpha(1.0f);
        r2.A0N.A09(null);
        r2.A0N = null;
    }

    @Override // X.AnonymousClass0Y9, X.AbstractC12530i4
    public void AMD(View view) {
        LayoutInflater$Factory2C011505o r2 = this.A00;
        r2.A0L.setVisibility(0);
        r2.A0L.sendAccessibilityEvent(32);
        if (r2.A0L.getParent() instanceof View) {
            AnonymousClass028.A0R((View) r2.A0L.getParent());
        }
    }
}
