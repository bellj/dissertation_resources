package X;

import java.util.LinkedHashMap;

/* renamed from: X.4YA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4YA {
    public int A00 = 0;
    public final AnonymousClass5S7 A01;
    public final LinkedHashMap A02 = new LinkedHashMap();

    public AnonymousClass4YA(AnonymousClass5S7 r2) {
        this.A01 = r2;
    }

    public synchronized int A00() {
        return this.A02.size();
    }

    public synchronized int A01() {
        return this.A00;
    }

    public synchronized Object A02(Object obj) {
        Object remove;
        int i;
        remove = this.A02.remove(obj);
        int i2 = this.A00;
        if (remove == null) {
            i = 0;
        } else {
            i = this.A01.AGn(remove);
        }
        this.A00 = i2 - i;
        return remove;
    }

    public synchronized void A03(Object obj, Object obj2) {
        int i;
        LinkedHashMap linkedHashMap = this.A02;
        Object remove = linkedHashMap.remove(obj);
        int i2 = this.A00;
        if (remove == null) {
            i = 0;
        } else {
            i = this.A01.AGn(remove);
        }
        this.A00 = i2 - i;
        linkedHashMap.put(obj, obj2);
        this.A00 += this.A01.AGn(obj2);
    }
}
