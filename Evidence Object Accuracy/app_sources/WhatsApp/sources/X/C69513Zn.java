package X;

import com.whatsapp.phonematching.MatchPhoneNumberFragment;

/* renamed from: X.3Zn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69513Zn implements AnonymousClass2SS {
    public final /* synthetic */ MatchPhoneNumberFragment A00;

    public C69513Zn(MatchPhoneNumberFragment matchPhoneNumberFragment) {
        this.A00 = matchPhoneNumberFragment;
    }

    @Override // X.AnonymousClass2SS
    public void AT7(int i) {
        this.A00.A03.sendEmptyMessage(3);
    }

    @Override // X.AnonymousClass2SS
    public void AT8(String str) {
        MatchPhoneNumberFragment matchPhoneNumberFragment = this.A00;
        C15570nT r0 = matchPhoneNumberFragment.A00;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        String str2 = r02.user;
        AnonymousClass009.A05(str2);
        matchPhoneNumberFragment.A03.sendEmptyMessage(C12990iw.A04(str2.equals(str) ? 1 : 0));
    }
}
