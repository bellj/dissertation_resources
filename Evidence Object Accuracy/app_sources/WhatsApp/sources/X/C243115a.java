package X;

import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.15a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C243115a {
    public final C003101j A00;
    public final AnonymousClass01d A01;
    public final C14830m7 A02;
    public final C002801g A03;
    public final C243415d A04;
    public final C243315c A05;
    public final ConcurrentHashMap A06 = new ConcurrentHashMap();

    public C243115a(C003101j r2, AnonymousClass01d r3, C14830m7 r4, C002801g r5, C243415d r6, C243315c r7) {
        this.A02 = r4;
        this.A05 = r7;
        this.A04 = r6;
        this.A01 = r3;
        this.A03 = r5;
        this.A00 = r2;
    }
}
