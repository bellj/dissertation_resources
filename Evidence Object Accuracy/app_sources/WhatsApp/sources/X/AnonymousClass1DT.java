package X;

/* renamed from: X.1DT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1DT {
    public final C15450nH A00;
    public final AnonymousClass127 A01;
    public final C18640sm A02;
    public final C16590pI A03;
    public final C14820m6 A04;
    public final AnonymousClass180 A05;
    public final AnonymousClass1DR A06;
    public final C16120oU A07;

    public AnonymousClass1DT(C15450nH r1, AnonymousClass127 r2, C18640sm r3, C16590pI r4, C14820m6 r5, AnonymousClass180 r6, AnonymousClass1DR r7, C16120oU r8) {
        this.A03 = r4;
        this.A01 = r2;
        this.A07 = r8;
        this.A00 = r1;
        this.A04 = r5;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
    }
}
