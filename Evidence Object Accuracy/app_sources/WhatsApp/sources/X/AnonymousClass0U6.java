package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.voipcalling.DefaultCryptoCallback;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.chromium.net.UrlRequest;

/* renamed from: X.0U6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0U6 {
    public static SparseIntArray A02;
    public static final int[] A03 = {0, 4, 8};
    public HashMap A00 = new HashMap();
    public HashMap A01 = new HashMap();

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A02 = sparseIntArray;
        sparseIntArray.append(76, 25);
        A02.append(77, 26);
        A02.append(79, 29);
        A02.append(80, 30);
        A02.append(86, 36);
        A02.append(85, 35);
        A02.append(58, 4);
        A02.append(57, 3);
        A02.append(55, 1);
        A02.append(94, 6);
        A02.append(95, 7);
        A02.append(65, 17);
        A02.append(66, 18);
        A02.append(67, 19);
        A02.append(0, 27);
        A02.append(81, 32);
        A02.append(82, 33);
        A02.append(64, 10);
        A02.append(63, 9);
        A02.append(98, 13);
        A02.append(101, 16);
        A02.append(99, 14);
        A02.append(96, 11);
        A02.append(100, 15);
        A02.append(97, 12);
        A02.append(89, 40);
        A02.append(74, 39);
        A02.append(73, 41);
        A02.append(88, 42);
        A02.append(72, 20);
        A02.append(87, 37);
        A02.append(62, 5);
        A02.append(75, 82);
        A02.append(84, 82);
        A02.append(78, 82);
        A02.append(56, 82);
        A02.append(54, 82);
        A02.append(5, 24);
        A02.append(7, 28);
        A02.append(23, 31);
        A02.append(24, 8);
        A02.append(6, 34);
        A02.append(8, 2);
        A02.append(3, 23);
        A02.append(4, 21);
        A02.append(2, 22);
        A02.append(13, 43);
        A02.append(26, 44);
        A02.append(21, 45);
        A02.append(22, 46);
        A02.append(20, 60);
        A02.append(18, 47);
        A02.append(19, 48);
        A02.append(14, 49);
        A02.append(15, 50);
        A02.append(16, 51);
        A02.append(17, 52);
        A02.append(25, 53);
        A02.append(90, 54);
        A02.append(68, 55);
        A02.append(91, 56);
        A02.append(69, 57);
        A02.append(92, 58);
        A02.append(70, 59);
        A02.append(59, 61);
        A02.append(61, 62);
        A02.append(60, 63);
        A02.append(27, 64);
        A02.append(106, 65);
        A02.append(33, 66);
        A02.append(107, 67);
        A02.append(103, 79);
        A02.append(1, 38);
        A02.append(102, 68);
        A02.append(93, 69);
        A02.append(71, 70);
        A02.append(31, 71);
        A02.append(29, 72);
        A02.append(30, 73);
        A02.append(32, 74);
        A02.append(28, 75);
        A02.append(104, 76);
        A02.append(83, 77);
        A02.append(C43951xu.A03, 78);
        A02.append(53, 80);
        A02.append(52, 81);
    }

    public static int A00(TypedArray typedArray, int i, int i2) {
        int resourceId = typedArray.getResourceId(i, i2);
        return resourceId == -1 ? typedArray.getInt(i, -1) : resourceId;
    }

    public static final int[] A01(View view, String str) {
        int i;
        Object obj;
        HashMap hashMap;
        String[] split = str.split(",");
        Context context = view.getContext();
        int length = split.length;
        int[] iArr = new int[length];
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            String trim = split[i2].trim();
            try {
                i = AnonymousClass0KO.class.getField(trim).getInt(null);
            } catch (Exception unused) {
                i = 0;
            }
            if (i == 0) {
                i = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            if (i == 0 && view.isInEditMode() && (view.getParent() instanceof ConstraintLayout)) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view.getParent();
                if (trim == null || (hashMap = constraintLayout.A0E) == null || !hashMap.containsKey(trim)) {
                    obj = null;
                } else {
                    obj = constraintLayout.A0E.get(trim);
                }
                if (obj != null && (obj instanceof Integer)) {
                    i = ((Number) obj).intValue();
                }
            }
            iArr[i3] = i;
            i2++;
            i3++;
        }
        return i3 != length ? Arrays.copyOf(iArr, i3) : iArr;
    }

    public final AnonymousClass0SY A02(int i) {
        HashMap hashMap = this.A00;
        Integer valueOf = Integer.valueOf(i);
        if (!hashMap.containsKey(valueOf)) {
            hashMap.put(valueOf, new AnonymousClass0SY());
        }
        return (AnonymousClass0SY) hashMap.get(valueOf);
    }

    public final AnonymousClass0SY A03(Context context, AttributeSet attributeSet) {
        StringBuilder sb;
        String str;
        String str2;
        AnonymousClass0SY r5 = new AnonymousClass0SY();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, AnonymousClass0MN.A00);
        int indexCount = obtainStyledAttributes.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = obtainStyledAttributes.getIndex(i);
            if (!(index == 1 || 23 == index || 24 == index)) {
                r5.A03.A06 = true;
                r5.A02.A0w = true;
                r5.A04.A04 = true;
                r5.A05.A0C = true;
            }
            SparseIntArray sparseIntArray = A02;
            switch (sparseIntArray.get(index)) {
                case 1:
                    AnonymousClass0SO r1 = r5.A02;
                    r1.A08 = A00(obtainStyledAttributes, index, r1.A08);
                    continue;
                case 2:
                    AnonymousClass0SO r12 = r5.A02;
                    r12.A09 = obtainStyledAttributes.getDimensionPixelSize(index, r12.A09);
                    continue;
                case 3:
                    AnonymousClass0SO r13 = r5.A02;
                    r13.A0A = A00(obtainStyledAttributes, index, r13.A0A);
                    continue;
                case 4:
                    AnonymousClass0SO r14 = r5.A02;
                    r14.A0B = A00(obtainStyledAttributes, index, r14.A0B);
                    continue;
                case 5:
                    r5.A02.A0r = obtainStyledAttributes.getString(index);
                    continue;
                case 6:
                    AnonymousClass0SO r15 = r5.A02;
                    r15.A0E = obtainStyledAttributes.getDimensionPixelOffset(index, r15.A0E);
                    continue;
                case 7:
                    AnonymousClass0SO r16 = r5.A02;
                    r16.A0F = obtainStyledAttributes.getDimensionPixelOffset(index, r16.A0F);
                    continue;
                case 8:
                    if (Build.VERSION.SDK_INT >= 17) {
                        AnonymousClass0SO r17 = r5.A02;
                        r17.A0G = obtainStyledAttributes.getDimensionPixelSize(index, r17.A0G);
                    } else {
                        continue;
                    }
                case 9:
                    AnonymousClass0SO r18 = r5.A02;
                    r18.A0H = A00(obtainStyledAttributes, index, r18.A0H);
                    continue;
                case 10:
                    AnonymousClass0SO r19 = r5.A02;
                    r19.A0I = A00(obtainStyledAttributes, index, r19.A0I);
                    continue;
                case 11:
                    AnonymousClass0SO r110 = r5.A02;
                    r110.A0J = obtainStyledAttributes.getDimensionPixelSize(index, r110.A0J);
                    continue;
                case 12:
                    AnonymousClass0SO r111 = r5.A02;
                    r111.A0K = obtainStyledAttributes.getDimensionPixelSize(index, r111.A0K);
                    continue;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    AnonymousClass0SO r112 = r5.A02;
                    r112.A0L = obtainStyledAttributes.getDimensionPixelSize(index, r112.A0L);
                    continue;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    AnonymousClass0SO r113 = r5.A02;
                    r113.A0M = obtainStyledAttributes.getDimensionPixelSize(index, r113.A0M);
                    continue;
                case 15:
                    AnonymousClass0SO r114 = r5.A02;
                    r114.A0N = obtainStyledAttributes.getDimensionPixelSize(index, r114.A0N);
                    continue;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    AnonymousClass0SO r115 = r5.A02;
                    r115.A0O = obtainStyledAttributes.getDimensionPixelSize(index, r115.A0O);
                    continue;
                case 17:
                    AnonymousClass0SO r116 = r5.A02;
                    r116.A0P = obtainStyledAttributes.getDimensionPixelOffset(index, r116.A0P);
                    continue;
                case 18:
                    AnonymousClass0SO r117 = r5.A02;
                    r117.A0Q = obtainStyledAttributes.getDimensionPixelOffset(index, r117.A0Q);
                    continue;
                case 19:
                    AnonymousClass0SO r118 = r5.A02;
                    r118.A01 = obtainStyledAttributes.getFloat(index, r118.A01);
                    continue;
                case C43951xu.A01 /* 20 */:
                    AnonymousClass0SO r119 = r5.A02;
                    r119.A03 = obtainStyledAttributes.getFloat(index, r119.A03);
                    continue;
                case 21:
                    AnonymousClass0SO r120 = r5.A02;
                    r120.A0a = obtainStyledAttributes.getLayoutDimension(index, r120.A0a);
                    continue;
                case 22:
                    C04900Nm r8 = r5.A04;
                    int i2 = obtainStyledAttributes.getInt(index, r8.A03);
                    r8.A03 = i2;
                    r8.A03 = A03[i2];
                    continue;
                case 23:
                    AnonymousClass0SO r121 = r5.A02;
                    r121.A0c = obtainStyledAttributes.getLayoutDimension(index, r121.A0c);
                    continue;
                case 24:
                    AnonymousClass0SO r122 = r5.A02;
                    r122.A0V = obtainStyledAttributes.getDimensionPixelSize(index, r122.A0V);
                    continue;
                case 25:
                    AnonymousClass0SO r123 = r5.A02;
                    r123.A0W = A00(obtainStyledAttributes, index, r123.A0W);
                    continue;
                case 26:
                    AnonymousClass0SO r124 = r5.A02;
                    r124.A0X = A00(obtainStyledAttributes, index, r124.A0X);
                    continue;
                case 27:
                    AnonymousClass0SO r125 = r5.A02;
                    r125.A0d = obtainStyledAttributes.getInt(index, r125.A0d);
                    continue;
                case 28:
                    AnonymousClass0SO r126 = r5.A02;
                    r126.A0e = obtainStyledAttributes.getDimensionPixelSize(index, r126.A0e);
                    continue;
                case 29:
                    AnonymousClass0SO r127 = r5.A02;
                    r127.A0f = A00(obtainStyledAttributes, index, r127.A0f);
                    continue;
                case C25991Bp.A0S /* 30 */:
                    AnonymousClass0SO r128 = r5.A02;
                    r128.A0g = A00(obtainStyledAttributes, index, r128.A0g);
                    continue;
                case 31:
                    if (Build.VERSION.SDK_INT >= 17) {
                        AnonymousClass0SO r129 = r5.A02;
                        r129.A0h = obtainStyledAttributes.getDimensionPixelSize(index, r129.A0h);
                    } else {
                        continue;
                    }
                case 32:
                    AnonymousClass0SO r130 = r5.A02;
                    r130.A0i = A00(obtainStyledAttributes, index, r130.A0i);
                    continue;
                case 33:
                    AnonymousClass0SO r131 = r5.A02;
                    r131.A0j = A00(obtainStyledAttributes, index, r131.A0j);
                    continue;
                case 34:
                    AnonymousClass0SO r132 = r5.A02;
                    r132.A0k = obtainStyledAttributes.getDimensionPixelSize(index, r132.A0k);
                    continue;
                case 35:
                    AnonymousClass0SO r133 = r5.A02;
                    r133.A0l = A00(obtainStyledAttributes, index, r133.A0l);
                    continue;
                case 36:
                    AnonymousClass0SO r134 = r5.A02;
                    r134.A0m = A00(obtainStyledAttributes, index, r134.A0m);
                    continue;
                case 37:
                    AnonymousClass0SO r135 = r5.A02;
                    r135.A05 = obtainStyledAttributes.getFloat(index, r135.A05);
                    continue;
                case 38:
                    r5.A00 = obtainStyledAttributes.getResourceId(index, r5.A00);
                    continue;
                case 39:
                    AnonymousClass0SO r136 = r5.A02;
                    r136.A04 = obtainStyledAttributes.getFloat(index, r136.A04);
                    continue;
                case 40:
                    AnonymousClass0SO r137 = r5.A02;
                    r137.A06 = obtainStyledAttributes.getFloat(index, r137.A06);
                    continue;
                case 41:
                    AnonymousClass0SO r138 = r5.A02;
                    r138.A0U = obtainStyledAttributes.getInt(index, r138.A0U);
                    continue;
                case 42:
                    AnonymousClass0SO r139 = r5.A02;
                    r139.A0n = obtainStyledAttributes.getInt(index, r139.A0n);
                    continue;
                case 43:
                    C04900Nm r140 = r5.A04;
                    r140.A00 = obtainStyledAttributes.getFloat(index, r140.A00);
                    continue;
                case 44:
                    if (Build.VERSION.SDK_INT >= 21) {
                        AnonymousClass0SN r141 = r5.A05;
                        r141.A0B = true;
                        r141.A00 = obtainStyledAttributes.getDimension(index, r141.A00);
                    } else {
                        continue;
                    }
                case 45:
                    AnonymousClass0SN r142 = r5.A05;
                    r142.A02 = obtainStyledAttributes.getFloat(index, r142.A02);
                    continue;
                case DefaultCryptoCallback.E2E_EXTENDED_V2_KEY_LENGTH /* 46 */:
                    AnonymousClass0SN r143 = r5.A05;
                    r143.A03 = obtainStyledAttributes.getFloat(index, r143.A03);
                    continue;
                case 47:
                    AnonymousClass0SN r144 = r5.A05;
                    r144.A04 = obtainStyledAttributes.getFloat(index, r144.A04);
                    continue;
                case 48:
                    AnonymousClass0SN r145 = r5.A05;
                    r145.A05 = obtainStyledAttributes.getFloat(index, r145.A05);
                    continue;
                case 49:
                    AnonymousClass0SN r146 = r5.A05;
                    r146.A06 = obtainStyledAttributes.getDimension(index, r146.A06);
                    continue;
                case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                    AnonymousClass0SN r147 = r5.A05;
                    r147.A07 = obtainStyledAttributes.getDimension(index, r147.A07);
                    continue;
                case 51:
                    AnonymousClass0SN r148 = r5.A05;
                    r148.A08 = obtainStyledAttributes.getDimension(index, r148.A08);
                    continue;
                case 52:
                    AnonymousClass0SN r149 = r5.A05;
                    r149.A09 = obtainStyledAttributes.getDimension(index, r149.A09);
                    continue;
                case 53:
                    if (Build.VERSION.SDK_INT >= 21) {
                        AnonymousClass0SN r150 = r5.A05;
                        r150.A0A = obtainStyledAttributes.getDimension(index, r150.A0A);
                    } else {
                        continue;
                    }
                case 54:
                    AnonymousClass0SO r151 = r5.A02;
                    r151.A0o = obtainStyledAttributes.getInt(index, r151.A0o);
                    continue;
                case 55:
                    AnonymousClass0SO r152 = r5.A02;
                    r152.A0R = obtainStyledAttributes.getInt(index, r152.A0R);
                    continue;
                case 56:
                    AnonymousClass0SO r153 = r5.A02;
                    r153.A0p = obtainStyledAttributes.getDimensionPixelSize(index, r153.A0p);
                    continue;
                case 57:
                    AnonymousClass0SO r154 = r5.A02;
                    r154.A0S = obtainStyledAttributes.getDimensionPixelSize(index, r154.A0S);
                    continue;
                case 58:
                    AnonymousClass0SO r155 = r5.A02;
                    r155.A0q = obtainStyledAttributes.getDimensionPixelSize(index, r155.A0q);
                    continue;
                case 59:
                    AnonymousClass0SO r156 = r5.A02;
                    r156.A0T = obtainStyledAttributes.getDimensionPixelSize(index, r156.A0T);
                    continue;
                case 60:
                    AnonymousClass0SN r157 = r5.A05;
                    r157.A01 = obtainStyledAttributes.getFloat(index, r157.A01);
                    continue;
                case 61:
                    AnonymousClass0SO r158 = r5.A02;
                    r158.A0C = A00(obtainStyledAttributes, index, r158.A0C);
                    continue;
                case 62:
                    AnonymousClass0SO r159 = r5.A02;
                    r159.A0D = obtainStyledAttributes.getDimensionPixelSize(index, r159.A0D);
                    continue;
                case 63:
                    AnonymousClass0SO r160 = r5.A02;
                    r160.A00 = obtainStyledAttributes.getFloat(index, r160.A00);
                    continue;
                case 64:
                    AnonymousClass0S0 r161 = r5.A03;
                    r161.A02 = A00(obtainStyledAttributes, index, r161.A02);
                    continue;
                case 65:
                    int i3 = obtainStyledAttributes.peekValue(index).type;
                    AnonymousClass0S0 r82 = r5.A03;
                    if (i3 == 3) {
                        str2 = obtainStyledAttributes.getString(index);
                    } else {
                        str2 = C04270Lb.A00[obtainStyledAttributes.getInteger(index, 0)];
                    }
                    r82.A05 = str2;
                    continue;
                case 66:
                    r5.A03.A03 = obtainStyledAttributes.getInt(index, 0);
                    continue;
                case 67:
                    AnonymousClass0S0 r162 = r5.A03;
                    r162.A01 = obtainStyledAttributes.getFloat(index, r162.A01);
                    continue;
                case 68:
                    C04900Nm r163 = r5.A04;
                    r163.A01 = obtainStyledAttributes.getFloat(index, r163.A01);
                    continue;
                case 69:
                    r5.A02.A07 = obtainStyledAttributes.getFloat(index, 1.0f);
                    continue;
                case 70:
                    r5.A02.A02 = obtainStyledAttributes.getFloat(index, 1.0f);
                    continue;
                case 71:
                    Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                    continue;
                case C43951xu.A02 /* 72 */:
                    AnonymousClass0SO r164 = r5.A02;
                    r164.A0Y = obtainStyledAttributes.getInt(index, r164.A0Y);
                    continue;
                case 73:
                    AnonymousClass0SO r165 = r5.A02;
                    r165.A0Z = obtainStyledAttributes.getDimensionPixelSize(index, r165.A0Z);
                    continue;
                case 74:
                    r5.A02.A0t = obtainStyledAttributes.getString(index);
                    continue;
                case 75:
                    AnonymousClass0SO r166 = r5.A02;
                    r166.A0x = obtainStyledAttributes.getBoolean(index, r166.A0x);
                    continue;
                case 76:
                    AnonymousClass0S0 r167 = r5.A03;
                    r167.A04 = obtainStyledAttributes.getInt(index, r167.A04);
                    continue;
                case 77:
                    r5.A02.A0s = obtainStyledAttributes.getString(index);
                    continue;
                case 78:
                    C04900Nm r168 = r5.A04;
                    r168.A02 = obtainStyledAttributes.getInt(index, r168.A02);
                    continue;
                case 79:
                    AnonymousClass0S0 r169 = r5.A03;
                    r169.A00 = obtainStyledAttributes.getFloat(index, r169.A00);
                    continue;
                case 80:
                    AnonymousClass0SO r170 = r5.A02;
                    r170.A0v = obtainStyledAttributes.getBoolean(index, r170.A0v);
                    continue;
                case 81:
                    AnonymousClass0SO r171 = r5.A02;
                    r171.A0u = obtainStyledAttributes.getBoolean(index, r171.A0u);
                    continue;
                case 82:
                    sb = new StringBuilder();
                    str = "unused attribute 0x";
                    break;
                default:
                    sb = new StringBuilder();
                    str = "Unknown attribute 0x";
                    break;
            }
            sb.append(str);
            sb.append(Integer.toHexString(index));
            sb.append("   ");
            sb.append(sparseIntArray.get(index));
            Log.w("ConstraintSet", sb.toString());
        }
        obtainStyledAttributes.recycle();
        return r5;
    }

    public void A04(ConstraintLayout constraintLayout) {
        Method method;
        Object[] objArr;
        String str;
        int childCount = constraintLayout.getChildCount();
        HashMap hashMap = this.A00;
        HashSet hashSet = new HashSet(hashMap.keySet());
        for (int i = 0; i < childCount; i++) {
            View childAt = constraintLayout.getChildAt(i);
            int id = childAt.getId();
            if (!hashMap.containsKey(Integer.valueOf(id))) {
                StringBuilder sb = new StringBuilder("id unknown ");
                try {
                    str = childAt.getContext().getResources().getResourceEntryName(childAt.getId());
                } catch (Exception unused) {
                    str = "UNKNOWN";
                }
                sb.append(str);
                Log.w("ConstraintSet", sb.toString());
            } else if (id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            } else if (id != -1) {
                if (hashMap.containsKey(Integer.valueOf(id))) {
                    Integer valueOf = Integer.valueOf(id);
                    hashSet.remove(valueOf);
                    AnonymousClass0SY r8 = (AnonymousClass0SY) hashMap.get(valueOf);
                    if (childAt instanceof Barrier) {
                        r8.A02.A0b = 1;
                    }
                    int i2 = r8.A02.A0b;
                    if (i2 != -1 && i2 == 1) {
                        Barrier barrier = (Barrier) childAt;
                        barrier.setId(id);
                        AnonymousClass0SO r1 = r8.A02;
                        barrier.A00 = r1.A0Y;
                        barrier.setMargin(r1.A0Z);
                        barrier.setAllowsGoneWidget(r1.A0x);
                        int[] iArr = r1.A0z;
                        if (iArr == null) {
                            String str2 = r1.A0t;
                            if (str2 != null) {
                                r1.A0z = A01(barrier, str2);
                                iArr = r8.A02.A0z;
                            }
                        }
                        barrier.setReferencedIds(iArr);
                    }
                    AnonymousClass064 r11 = (AnonymousClass064) childAt.getLayoutParams();
                    r11.A00();
                    r8.A01(r11);
                    HashMap hashMap2 = r8.A01;
                    Class<?> cls = childAt.getClass();
                    for (String str3 : hashMap2.keySet()) {
                        AnonymousClass0SL r15 = (AnonymousClass0SL) hashMap2.get(str3);
                        StringBuilder sb2 = new StringBuilder("set");
                        sb2.append(str3);
                        String obj = sb2.toString();
                        try {
                            switch (r15.A03.ordinal()) {
                                case 0:
                                    method = cls.getMethod(obj, Integer.TYPE);
                                    objArr = new Object[]{Integer.valueOf(r15.A02)};
                                    method.invoke(childAt, objArr);
                                    break;
                                case 1:
                                    method = cls.getMethod(obj, Float.TYPE);
                                    objArr = new Object[]{Float.valueOf(r15.A00)};
                                    method.invoke(childAt, objArr);
                                    break;
                                case 2:
                                    method = cls.getMethod(obj, Integer.TYPE);
                                    objArr = new Object[]{Integer.valueOf(r15.A01)};
                                    method.invoke(childAt, objArr);
                                    break;
                                case 3:
                                    method = cls.getMethod(obj, Drawable.class);
                                    ColorDrawable colorDrawable = new ColorDrawable();
                                    colorDrawable.setColor(r15.A01);
                                    objArr = new Object[]{colorDrawable};
                                    method.invoke(childAt, objArr);
                                    break;
                                case 4:
                                    method = cls.getMethod(obj, CharSequence.class);
                                    objArr = new Object[]{r15.A05};
                                    method.invoke(childAt, objArr);
                                    break;
                                case 5:
                                    method = cls.getMethod(obj, Boolean.TYPE);
                                    objArr = new Object[]{Boolean.valueOf(r15.A06)};
                                    method.invoke(childAt, objArr);
                                    break;
                                case 6:
                                    method = cls.getMethod(obj, Float.TYPE);
                                    objArr = new Object[]{Float.valueOf(r15.A00)};
                                    method.invoke(childAt, objArr);
                                    break;
                            }
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(" Custom Attribute \"");
                            sb3.append(str3);
                            sb3.append("\" not found on ");
                            sb3.append(cls.getName());
                            Log.e("TransitionLayout", sb3.toString());
                            e.printStackTrace();
                        } catch (NoSuchMethodException e2) {
                            Log.e("TransitionLayout", e2.getMessage());
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append(" Custom Attribute \"");
                            sb4.append(str3);
                            sb4.append("\" not found on ");
                            String name = cls.getName();
                            sb4.append(name);
                            Log.e("TransitionLayout", sb4.toString());
                            StringBuilder sb5 = new StringBuilder();
                            sb5.append(name);
                            sb5.append(" must have a method ");
                            sb5.append(obj);
                            Log.e("TransitionLayout", sb5.toString());
                        }
                    }
                    childAt.setLayoutParams(r11);
                    C04900Nm r12 = r8.A04;
                    if (r12.A02 == 0) {
                        childAt.setVisibility(r12.A03);
                    }
                    int i3 = Build.VERSION.SDK_INT;
                    if (i3 >= 17) {
                        childAt.setAlpha(r8.A04.A00);
                        AnonymousClass0SN r13 = r8.A05;
                        childAt.setRotation(r13.A01);
                        childAt.setRotationX(r13.A02);
                        childAt.setRotationY(r13.A03);
                        childAt.setScaleX(r13.A04);
                        childAt.setScaleY(r13.A05);
                        if (!Float.isNaN(r13.A06)) {
                            childAt.setPivotX(r8.A05.A06);
                        }
                        if (!Float.isNaN(r8.A05.A07)) {
                            childAt.setPivotY(r8.A05.A07);
                        }
                        AnonymousClass0SN r14 = r8.A05;
                        childAt.setTranslationX(r14.A08);
                        childAt.setTranslationY(r14.A09);
                        if (i3 >= 21) {
                            AnonymousClass0SN r16 = r8.A05;
                            childAt.setTranslationZ(r16.A0A);
                            if (r16.A0B) {
                                childAt.setElevation(r16.A00);
                            }
                        }
                    }
                } else {
                    StringBuilder sb6 = new StringBuilder("WARNING NO CONSTRAINTS for view ");
                    sb6.append(id);
                    Log.v("ConstraintSet", sb6.toString());
                }
            }
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            Number number = (Number) it.next();
            AnonymousClass0SY r5 = (AnonymousClass0SY) hashMap.get(number);
            AnonymousClass0SO r7 = r5.A02;
            int i4 = r7.A0b;
            if (i4 != -1 && i4 == 1) {
                Barrier barrier2 = new Barrier(constraintLayout.getContext());
                barrier2.setId(number.intValue());
                int[] iArr2 = r7.A0z;
                if (iArr2 == null) {
                    String str4 = r7.A0t;
                    if (str4 != null) {
                        iArr2 = A01(barrier2, str4);
                        r7.A0z = iArr2;
                    }
                    barrier2.A00 = r7.A0Y;
                    barrier2.setMargin(r7.A0Z);
                    AnonymousClass064 r0 = new AnonymousClass064();
                    barrier2.A02();
                    r5.A01(r0);
                    constraintLayout.addView(barrier2, r0);
                }
                barrier2.setReferencedIds(iArr2);
                barrier2.A00 = r7.A0Y;
                barrier2.setMargin(r7.A0Z);
                AnonymousClass064 r0 = new AnonymousClass064();
                barrier2.A02();
                r5.A01(r0);
                constraintLayout.addView(barrier2, r0);
            }
            if (r7.A0y) {
                View guideline = new Guideline(constraintLayout.getContext());
                guideline.setId(number.intValue());
                AnonymousClass064 r02 = new AnonymousClass064();
                r5.A01(r02);
                constraintLayout.addView(guideline, r02);
            }
        }
    }

    public void A05(ConstraintLayout constraintLayout) {
        AnonymousClass0SL r1;
        int childCount = constraintLayout.getChildCount();
        HashMap hashMap = this.A00;
        hashMap.clear();
        for (int i = 0; i < childCount; i++) {
            View childAt = constraintLayout.getChildAt(i);
            AnonymousClass064 r13 = (AnonymousClass064) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!hashMap.containsKey(Integer.valueOf(id))) {
                    hashMap.put(Integer.valueOf(id), new AnonymousClass0SY());
                }
                AnonymousClass0SY r7 = (AnonymousClass0SY) hashMap.get(Integer.valueOf(id));
                HashMap hashMap2 = this.A01;
                HashMap hashMap3 = new HashMap();
                Class<?> cls = childAt.getClass();
                for (String str : hashMap2.keySet()) {
                    AnonymousClass0SL r8 = (AnonymousClass0SL) hashMap2.get(str);
                    try {
                        if (str.equals("BackgroundColor")) {
                            r1 = new AnonymousClass0SL(r8, Integer.valueOf(((ColorDrawable) childAt.getBackground()).getColor()));
                        } else {
                            StringBuilder sb = new StringBuilder();
                            sb.append("getMap");
                            sb.append(str);
                            r1 = new AnonymousClass0SL(r8, cls.getMethod(sb.toString(), new Class[0]).invoke(childAt, new Object[0]));
                        }
                        hashMap3.put(str, r1);
                    } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
                r7.A01 = hashMap3;
                AnonymousClass0SY.A00(r13, r7, id);
                r7.A04.A03 = childAt.getVisibility();
                int i2 = Build.VERSION.SDK_INT;
                if (i2 >= 17) {
                    r7.A04.A00 = childAt.getAlpha();
                    AnonymousClass0SN r12 = r7.A05;
                    r12.A01 = childAt.getRotation();
                    r12.A02 = childAt.getRotationX();
                    r12.A03 = childAt.getRotationY();
                    r12.A04 = childAt.getScaleX();
                    r12.A05 = childAt.getScaleY();
                    float pivotX = childAt.getPivotX();
                    float pivotY = childAt.getPivotY();
                    if (!(((double) pivotX) == 0.0d && ((double) pivotY) == 0.0d)) {
                        AnonymousClass0SN r0 = r7.A05;
                        r0.A06 = pivotX;
                        r0.A07 = pivotY;
                    }
                    AnonymousClass0SN r14 = r7.A05;
                    r14.A08 = childAt.getTranslationX();
                    r14.A09 = childAt.getTranslationY();
                    if (i2 >= 21) {
                        AnonymousClass0SN r15 = r7.A05;
                        r15.A0A = childAt.getTranslationZ();
                        if (r15.A0B) {
                            r15.A00 = childAt.getElevation();
                        }
                    }
                }
                if (childAt instanceof Barrier) {
                    Barrier barrier = (Barrier) childAt;
                    AnonymousClass0SO r16 = r7.A02;
                    r16.A0x = barrier.A01.A02;
                    r16.A0z = barrier.getReferencedIds();
                    r16.A0Y = barrier.A00;
                    r16.A0Z = barrier.A01.A01;
                }
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }
}
