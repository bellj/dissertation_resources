package X;

import android.graphics.drawable.Animatable2;
import android.graphics.drawable.Drawable;

/* renamed from: X.03J  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass03J {
    public Animatable2.AnimationCallback A00;

    public void A01(Drawable drawable) {
    }

    public Animatable2.AnimationCallback A00() {
        Animatable2.AnimationCallback animationCallback = this.A00;
        if (animationCallback != null) {
            return animationCallback;
        }
        C020709v r0 = new C020709v(this);
        this.A00 = r0;
        return r0;
    }
}
