package X;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.whatsapp.jid.UserJid;
import com.whatsapp.wabloks.commerce.ui.view.WaExtensionsBottomsheetModalActivity;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0rN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17780rN {
    public final C21010wg A00;
    public final C14900mE A01;
    public final C16170oZ A02;
    public final C15650ng A03;
    public final AbstractC14440lR A04;
    public final C19820uj A05;

    public C17780rN(C21010wg r2, C14900mE r3, C16170oZ r4, C15650ng r5, AbstractC14440lR r6, C19820uj r7) {
        C16700pc.A0E(r4, 1);
        C16700pc.A0E(r6, 2);
        C16700pc.A0E(r5, 3);
        C16700pc.A0E(r3, 4);
        C16700pc.A0E(r2, 5);
        this.A02 = r4;
        this.A04 = r6;
        this.A03 = r5;
        this.A01 = r3;
        this.A00 = r2;
        this.A05 = r7;
    }

    public void A00(Activity activity, C90904Pr r25, Map map) {
        Intent intent;
        Bundle extras;
        if (activity != null && (intent = activity.getIntent()) != null && (extras = intent.getExtras()) != null) {
            String string = extras.getString("chat_id");
            UserJid nullable = UserJid.getNullable(string);
            String string2 = extras.getString("flow_token");
            String string3 = extras.getString("flow_data_endpoint");
            String string4 = extras.getString("aes_key");
            String string5 = extras.getString("initial_vector");
            String string6 = extras.getString("user_locale");
            if (string != null && nullable != null && string2 != null && string3 != null && string4 != null && string5 != null && string6 != null && map != null) {
                map.put("flow_token", string2);
                map.put("user_locale", string6);
                ((WaExtensionsBottomsheetModalActivity) activity).A03.A03.A0B(true);
                JSONObject jSONObject = new JSONObject(map);
                String obj = jSONObject.toString();
                C16700pc.A0B(obj);
                C19820uj r11 = this.A05;
                AnonymousClass1WA r5 = new AnonymousClass1WA(activity, this.A00, this.A01, r25, this.A04, r11, string3, obj, string4, string5, true);
                String obj2 = jSONObject.toString();
                C16700pc.A0B(obj2);
                r11.A00(nullable, r5, obj2, string4, string5, false);
            }
        }
    }
}
