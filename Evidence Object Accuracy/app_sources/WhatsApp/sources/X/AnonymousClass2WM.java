package X;

import android.content.Context;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/* renamed from: X.2WM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2WM {
    public final Context A00;
    public final AnonymousClass018 A01;
    public final AnonymousClass2WL A02;
    public final AnonymousClass2WL A03;
    public final AnonymousClass2WL A04;
    public final Calendar A05;

    public AnonymousClass2WM(Context context, AnonymousClass018 r6) {
        this.A00 = context;
        this.A01 = r6;
        AnonymousClass2WL r1 = new AnonymousClass2WL(context, r6, Calendar.getInstance(), 1);
        this.A03 = r1;
        r1.add(6, -2);
        AnonymousClass2WL r12 = new AnonymousClass2WL(context, r6, Calendar.getInstance(), 2);
        this.A04 = r12;
        r12.add(6, -7);
        AnonymousClass2WL r13 = new AnonymousClass2WL(context, r6, Calendar.getInstance(), 3);
        this.A02 = r13;
        r13.add(6, -28);
        Calendar instance = Calendar.getInstance();
        this.A05 = instance;
        instance.add(6, -366);
    }

    public AnonymousClass2WL A00(long j) {
        int i;
        GregorianCalendar gregorianCalendar;
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date(j));
        AnonymousClass2WL r1 = this.A03;
        if (!instance.after(r1)) {
            r1 = this.A04;
            if (!instance.after(r1)) {
                r1 = this.A02;
                if (!instance.after(r1)) {
                    boolean after = instance.after(this.A05);
                    Context context = this.A00;
                    AnonymousClass018 r4 = this.A01;
                    if (after) {
                        i = 4;
                        gregorianCalendar = new GregorianCalendar(instance.get(1), instance.get(2), 1);
                    } else {
                        i = 5;
                        gregorianCalendar = new GregorianCalendar(instance.get(1), 1, 1);
                    }
                    return new AnonymousClass2WL(context, r4, gregorianCalendar, i);
                }
            }
        }
        return r1;
    }
}
