package X;

import android.view.View;
import androidx.appcompat.widget.SearchView;

/* renamed from: X.0WN  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WN implements View.OnFocusChangeListener {
    public final /* synthetic */ SearchView A00;

    public AnonymousClass0WN(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // android.view.View.OnFocusChangeListener
    public void onFocusChange(View view, boolean z) {
        SearchView searchView = this.A00;
        View.OnFocusChangeListener onFocusChangeListener = searchView.A08;
        if (onFocusChangeListener != null) {
            onFocusChangeListener.onFocusChange(searchView, z);
        }
    }
}
