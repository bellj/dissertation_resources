package X;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;

/* renamed from: X.0mR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C15030mR extends AbstractC15040mS {
    public long A00;
    public boolean A01;
    public boolean A02;
    public final C15100mZ A03;
    public final C15120mb A04;
    public final AbstractC14150kw A05;
    public final C56622lJ A06;
    public final AbstractC14150kw A07;
    public final AnonymousClass2Z2 A08;
    public final C93924ay A09 = new C93924ay(((C15050mT) this).A00.A04);

    public C15030mR(C14160kx r3) {
        super(r3);
        this.A08 = new AnonymousClass2Z2(r3);
        this.A04 = new C15120mb(r3);
        this.A06 = new C56622lJ(r3);
        this.A03 = new C15100mZ(r3);
        this.A07 = new C79703r2(r3, this);
        this.A05 = new C56632lK(r3, this);
    }

    private final void A00() {
        C56602lH r1 = ((C15050mT) this).A00.A0B;
        C14160kx.A01(r1);
        if (r1.A01) {
            r1.A0H();
        }
    }

    private final void A01() {
        AbstractC14150kw r5 = this.A07;
        if (r5.A02 != 0) {
            A09("All hits dispatched or no network/service. Going to power save mode");
        }
        r5.A02 = 0;
        AbstractC14150kw.A00(r5).removeCallbacks(r5.A01);
    }

    private final void A02() {
        C56602lH r5 = ((C15050mT) this).A00.A0B;
        C14160kx.A01(r5);
        if (r5.A00 && !r5.A01) {
            C14170ky.A00();
            A0G();
            try {
                C15120mb r2 = this.A04;
                C14170ky.A00();
                r2.A0G();
                long A00 = C15120mb.A00(r2, C15120mb.A04, null);
                if (A00 != 0 && Math.abs(System.currentTimeMillis() - A00) <= ((Number) C88904Hw.A0V.A00()).longValue()) {
                    A0D("Dispatch alarm scheduled (ms)", Long.valueOf(((Number) C88904Hw.A0U.A00()).longValue()));
                    r5.A0I();
                }
            } catch (SQLiteException e) {
                A0C("Failed to get min/max hit times from local store", e);
            }
        }
    }

    public static final void A03(C56242kZ r12, AnonymousClass3BQ r13, C15030mR r14) {
        C13020j0.A01(r13);
        C13020j0.A01(r12);
        C14160kx r4 = ((C15050mT) r14).A00;
        C77423nG r6 = new C77423nG(r4);
        String str = r13.A02;
        C13020j0.A05(str);
        C13020j0.A05(str);
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("uri");
        builder.authority("google-analytics.com");
        builder.path(str);
        Uri build = builder.build();
        AnonymousClass3IT r5 = ((AnonymousClass4PG) r6).A00;
        List list = r5.A09;
        ListIterator listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (build.equals(((AbstractC116345Vb) listIterator.next()).Ah3())) {
                listIterator.remove();
            }
        }
        C14160kx r1 = r6.A01;
        list.add(new C56542lB(r1, str));
        r6.A00 = r13.A04;
        AnonymousClass3IT r2 = new AnonymousClass3IT(r5);
        C56552lC r0 = r1.A07;
        C14160kx.A01(r0);
        r0.A0G();
        r2.A02(r0.A00);
        C79683r0 r02 = r1.A0A;
        r02.A0G();
        C14170ky r03 = ((C15050mT) r02).A00.A03;
        C13020j0.A01(r03);
        DisplayMetrics displayMetrics = r03.A01.getResources().getDisplayMetrics();
        C56212kW r15 = new C56212kW();
        r15.A02 = C64933Hm.A01(Locale.getDefault());
        r15.A00 = displayMetrics.widthPixels;
        r15.A01 = displayMetrics.heightPixels;
        r2.A02(r15);
        Iterator it = r6.A02.iterator();
        if (it.hasNext()) {
            it.next();
            throw new NullPointerException("zza");
        }
        C56232kY r7 = (C56232kY) r2.A01(C56232kY.class);
        r7.A00 = "data";
        r7.A05 = true;
        r2.A02(r12);
        C77413nF r62 = (C77413nF) r2.A01(C77413nF.class);
        C56252ka r52 = (C56252ka) r2.A01(C56252ka.class);
        for (Map.Entry entry : r13.A03.entrySet()) {
            String str2 = (String) entry.getKey();
            String str3 = (String) entry.getValue();
            if ("an".equals(str2)) {
                r52.A00 = str3;
            } else if ("av".equals(str2)) {
                r52.A01 = str3;
            } else if ("aid".equals(str2)) {
                r52.A02 = str3;
            } else if ("aiid".equals(str2)) {
                r52.A03 = str3;
            } else if ("uid".equals(str2)) {
                r7.A02 = str3;
            } else {
                C13020j0.A05(str2);
                if (str2 != null && str2.startsWith("&")) {
                    str2 = str2.substring(1);
                }
                C13020j0.A07(str2, "Name can not be empty or \"&\"");
                r62.A00.put(str2, str3);
            }
        }
        C15050mT.A06(r14, str, r12, null, "Sending installation campaign to", 3);
        C15070mW r04 = r4.A0D;
        C14160kx.A01(r04);
        r2.A01 = r04.A0H();
        C14170ky r63 = r2.A07.A01;
        if (r2.A06) {
            throw new IllegalStateException("Measurement prototype can't be submitted");
        } else if (!r2.A05) {
            AnonymousClass3IT r53 = new AnonymousClass3IT(r2);
            r53.A02 = SystemClock.elapsedRealtime();
            long j = r53.A01;
            if (j == 0) {
                j = System.currentTimeMillis();
            }
            r53.A00 = j;
            r53.A05 = true;
            r63.A03.execute(new RunnableBRunnable0Shape0S0200000_I0(r53, 1, r63));
        } else {
            throw new IllegalStateException("Measurement can only be submitted once");
        }
    }

    public final long A0H() {
        long longValue = ((Number) C88904Hw.A0Q.A00()).longValue();
        C56562lD r1 = ((C15050mT) this).A00.A0E;
        C14160kx.A01(r1);
        r1.A0G();
        if (!r1.A03) {
            return longValue;
        }
        C14160kx.A01(r1);
        r1.A0G();
        return ((long) r1.A00) * 1000;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        if (r1 == false) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00c9, code lost:
        if (r4 <= 0) goto L_0x00cb;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0I() {
        /*
        // Method dump skipped, instructions count: 395
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15030mR.A0I():void");
    }

    public final void A0J() {
        if (!this.A02 && ((Boolean) C88904Hw.A0J.A00()).booleanValue()) {
            C15100mZ r5 = this.A03;
            if (!r5.A0I()) {
                long longValue = ((Number) C88904Hw.A0E.A00()).longValue();
                C93924ay r4 = this.A09;
                if (r4.A00(longValue)) {
                    r4.A00 = SystemClock.elapsedRealtime();
                    A09("Connecting to service");
                    C14170ky.A00();
                    r5.A0G();
                    if (r5.A00 == null) {
                        AnonymousClass3LI r3 = r5.A01;
                        C14170ky.A00();
                        Intent intent = new Intent("com.google.android.gms.analytics.service.START");
                        intent.setComponent(new ComponentName("com.google.android.gms", "com.google.android.gms.analytics.service.AnalyticsService"));
                        C15100mZ r2 = r3.A02;
                        Context context = ((C15050mT) r2).A00.A00;
                        intent.putExtra("app_package_name", context.getPackageName());
                        AnonymousClass3IW A00 = AnonymousClass3IW.A00();
                        synchronized (r3) {
                            r3.A00 = null;
                            r3.A01 = true;
                            boolean A02 = A00.A02(context, intent, r2.A01, context.getClass().getName(), 129);
                            r2.A0D("Bind to service requested", Boolean.valueOf(A02));
                            if (!A02) {
                                r3.A01 = false;
                                return;
                            }
                            try {
                                r3.wait(((Long) C88904Hw.A0B.A00()).longValue());
                            } catch (InterruptedException unused) {
                                r2.A0A("Wait for service connect was interrupted");
                            }
                            r3.A01 = false;
                            C79663qy r1 = r3.A00;
                            r3.A00 = null;
                            if (r1 == null) {
                                r2.A08("Successfully bound to service but never got onServiceConnected callback");
                            }
                            if (r1 != null) {
                                r5.A00 = r1;
                                C15100mZ.A00(r5);
                            } else {
                                return;
                            }
                        }
                    }
                    A09("Connected to service");
                    r4.A00 = 0;
                    A0K();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056 A[LOOP:0: B:15:0x0056->B:22:0x0070, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0052 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0K() {
        /*
            r8 = this;
            X.C14170ky.A00()
            X.C14170ky.A00()
            r8.A0G()
            X.4Vl r0 = X.C88904Hw.A0J
            java.lang.Object r0 = r0.A00()
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x001c
            java.lang.String r0 = "Service client disabled. Can't dispatch local hits to device AnalyticsService"
            r8.A0A(r0)
        L_0x001c:
            X.0mZ r7 = r8.A03
            boolean r0 = r7.A0I()
            if (r0 != 0) goto L_0x002a
            java.lang.String r0 = "Service not connected"
            r8.A09(r0)
        L_0x0029:
            return
        L_0x002a:
            X.0mb r6 = r8.A04
            long r3 = r6.A0H()
            r1 = 0
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0029
            java.lang.String r0 = "Dispatching local hits to device AnalyticsService"
            r8.A09(r0)
        L_0x003b:
            X.4Vl r0 = X.C88904Hw.A0W     // Catch: SQLiteException -> 0x0093
            java.lang.Object r0 = r0.A00()     // Catch: SQLiteException -> 0x0093
            java.lang.Number r0 = (java.lang.Number) r0     // Catch: SQLiteException -> 0x0093
            int r0 = r0.intValue()     // Catch: SQLiteException -> 0x0093
            long r0 = (long) r0     // Catch: SQLiteException -> 0x0093
            java.util.List r5 = r6.A0J(r0)     // Catch: SQLiteException -> 0x0093
            boolean r0 = r5.isEmpty()     // Catch: SQLiteException -> 0x0093
            if (r0 == 0) goto L_0x0056
            r8.A0I()     // Catch: SQLiteException -> 0x0093
            goto L_0x008e
        L_0x0056:
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x003b
            r0 = 0
            java.lang.Object r1 = r5.get(r0)
            X.3H2 r1 = (X.AnonymousClass3H2) r1
            boolean r0 = r7.A0J(r1)
            if (r0 != 0) goto L_0x006d
            r8.A0I()
            return
        L_0x006d:
            r5.remove(r1)
            long r3 = r1.A01     // Catch: SQLiteException -> 0x008f
            X.C14170ky.A00()     // Catch: SQLiteException -> 0x008f
            r6.A0G()     // Catch: SQLiteException -> 0x008f
            r0 = 1
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch: SQLiteException -> 0x008f
            r2.<init>(r0)     // Catch: SQLiteException -> 0x008f
            java.lang.Long r1 = java.lang.Long.valueOf(r3)     // Catch: SQLiteException -> 0x008f
            r2.add(r1)     // Catch: SQLiteException -> 0x008f
            java.lang.String r0 = "Deleting hit, id"
            r6.A0D(r0, r1)     // Catch: SQLiteException -> 0x008f
            r6.A0K(r2)     // Catch: SQLiteException -> 0x008f
            goto L_0x0056
        L_0x008e:
            return
        L_0x008f:
            r1 = move-exception
            java.lang.String r0 = "Failed to remove hit that was send for delivery"
            goto L_0x0096
        L_0x0093:
            r1 = move-exception
            java.lang.String r0 = "Failed to read hits from store"
        L_0x0096:
            r8.A0C(r0, r1)
            r8.A01()
            r8.A00()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15030mR.A0K():void");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:278:0x055e */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r35v0, types: [X.0mR, X.0mT, X.0mS] */
    /* JADX WARN: Type inference failed for: r3v16, types: [X.0mb, X.0mT, X.0mS] */
    /* JADX WARN: Type inference failed for: r7v2, types: [java.util.List, java.util.Collection] */
    /* JADX WARN: Type inference failed for: r7v3, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r7v4, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r7v5, types: [java.util.List] */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02d3, code lost:
        r6.A00.getPackageName();
        r4 = new java.io.ByteArrayOutputStream();
        r3 = new java.util.zip.GZIPOutputStream(r4);
        r3.write(r10);
        r3.close();
        r4.close();
        r13 = r4.toByteArray();
        r9 = r13.length;
        r17 = java.lang.Integer.valueOf(r9);
        r12 = r10.length;
        X.C15050mT.A06(r5, r17, java.lang.Long.valueOf((((long) r9) * 100) / ((long) r12)), r11, "POST compressed size, ratio %, url", 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x030c, code lost:
        if (r9 <= r12) goto L_0x0319;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x030e, code lost:
        r5.A07(r17, java.lang.Integer.valueOf(r12), "Compressed payload is larger then uncompressed. compressed, uncompressed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0326, code lost:
        if (android.util.Log.isLoggable((java.lang.String) X.C88904Hw.A0K.A00(), 2) == false) goto L_0x033e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0328, code lost:
        r3 = new java.lang.String(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0335, code lost:
        if (r3.length() == 0) goto L_0x0344;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0337, code lost:
        r3 = "\n".concat(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x033b, code lost:
        r5.A0D("Post payload", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x033e, code lost:
        r10 = r5.A0I(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0344, code lost:
        r3 = new java.lang.String("\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x034a, code lost:
        r10.setDoOutput(true);
        r10.addRequestProperty("Content-Encoding", "gzip");
        r10.setFixedLengthStreamingMode(r9);
        r10.connect();
        r9 = r10.getOutputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x035e, code lost:
        r9.write(r13);
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0364, code lost:
        X.C56622lJ.A01(r5, r10);
        r9 = r10.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x036d, code lost:
        if (r9 != 200) goto L_0x0384;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x036f, code lost:
        r3 = r6.A06;
        X.C14160kx.A01(r3);
        X.C14170ky.A00();
        r6 = r3.A00;
        X.C14170ky.A00();
        r6.A00 = java.lang.System.currentTimeMillis();
        r9 = 200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0384, code lost:
        r5.A0B("POST status", java.lang.Integer.valueOf(r9));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x038d, code lost:
        r10.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0391, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0393, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0394, code lost:
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0396, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0397, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0398, code lost:
        r5.A0E("Network compressed POST connection error", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x039d, code lost:
        if (r9 != null) goto L_0x039f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x039f, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x03a3, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x03a4, code lost:
        r5.A0C("Error closing http compressed post connection output stream", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x03a9, code lost:
        if (r10 != null) goto L_0x03ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x03ab, code lost:
        r10.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x03ae, code lost:
        r9 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x03b0, code lost:
        r9 = X.C56622lJ.A00(r5, r11, r8.A01.toByteArray());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x03bc, code lost:
        if (r9 != 200) goto L_0x03cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x03be, code lost:
        r5.A0D("Batched upload completed. Hits batched", java.lang.Integer.valueOf(r8.A00));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x03cb, code lost:
        r4 = java.lang.Integer.valueOf(r9);
        r5.A0D("Network error uploading hits. status code", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x03dc, code lost:
        if (r3.A00().contains(r4) != false) goto L_0x03de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x03de, code lost:
        r5.A0A("Server instructed the client to stop batching");
        r5.A00.A00 = android.os.SystemClock.elapsedRealtime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x03ed, code lost:
        r7 = new java.util.ArrayList(r10.size());
        r15 = r10.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x03fe, code lost:
        if (r15.hasNext() == false) goto L_0x055e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0400, code lost:
        r8 = (X.AnonymousClass3H2) r15.next();
        X.C13020j0.A01(r8);
        r10 = r8.A05;
        r12 = r5.A0H(r8, true ^ r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0411, code lost:
        if (r12 != null) goto L_0x0442;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0413, code lost:
        r10 = r6.A0C;
        X.C14160kx.A01(r10);
        r9 = "Error formatting hit for upload";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x041a, code lost:
        r10.A0E(X.C12960it.A0c(r9, "Discarding hit. "), r8.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0427, code lost:
        r7.add(java.lang.Long.valueOf(r8.A01));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x043e, code lost:
        if (r7.size() < ((java.lang.Integer) r21.A00()).intValue()) goto L_0x03fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0442, code lost:
        r11 = r12.length();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0452, code lost:
        if (r11 > ((java.lang.Integer) X.C88904Hw.A0c.A00()).intValue()) goto L_0x04cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0456, code lost:
        if (r10 == false) goto L_0x048a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0458, code lost:
        r3 = X.C88904Hw.A0Z;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x045a, code lost:
        r10 = (java.lang.String) r3.A00();
        r4 = (java.lang.String) X.C88904Hw.A0a.A00();
        r11 = new java.lang.StringBuilder(((r10.length() + 1) + r4.length()) + r11);
        r11.append(r10);
        r11.append(r4);
        r11.append("?");
        r11.append(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x048a, code lost:
        r3 = X.C88904Hw.A0Y;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x048d, code lost:
        r4 = new java.net.URL(r11.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0492, code lost:
        r5.A0B("GET request", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0497, code lost:
        r9 = r5.A0I(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x049b, code lost:
        r9.connect();
        X.C56622lJ.A01(r5, r9);
        r10 = r9.getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x04a7, code lost:
        if (r10 != 200) goto L_0x04be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x04a9, code lost:
        r3 = r6.A06;
        X.C14160kx.A01(r3);
        X.C14170ky.A00();
        r10 = r3.A00;
        X.C14170ky.A00();
        r10.A00 = java.lang.System.currentTimeMillis();
        r10 = 200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x04be, code lost:
        r5.A0B("GET status", java.lang.Integer.valueOf(r10));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x04c7, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x04cb, code lost:
        r3 = r5.A0H(r8, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x04d0, code lost:
        if (r3 != null) goto L_0x04db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x04d2, code lost:
        r10 = r6.A0C;
        X.C14160kx.A01(r10);
        r9 = "Error formatting hit for POST upload";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x04db, code lost:
        r9 = r3.getBytes();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x04ec, code lost:
        if (r9.length <= ((java.lang.Integer) X.C88904Hw.A0h.A00()).intValue()) goto L_0x04f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x04ee, code lost:
        r10 = r6.A0C;
        X.C14160kx.A01(r10);
        r9 = "Hit payload exceeds size limit";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x04f7, code lost:
        if (r10 == false) goto L_0x04fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x04f9, code lost:
        r3 = X.C88904Hw.A0Z;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x04fc, code lost:
        r3 = X.C88904Hw.A0Y;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x04fe, code lost:
        r10 = (java.lang.String) r3.A00();
        r4 = (java.lang.String) X.C88904Hw.A0a.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x0510, code lost:
        if (r4.length() == 0) goto L_0x0517;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0512, code lost:
        r4 = r10.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0517, code lost:
        r4 = new java.lang.String(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x0521, code lost:
        r10 = X.C56622lJ.A00(r5, new java.net.URL(r4), r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0527, code lost:
        if (r10 != 200) goto L_0x055e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x052b, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x052d, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x052e, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x052f, code lost:
        r5.A0E("Network GET connection error", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x0534, code lost:
        if (r9 != null) goto L_0x0536;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0536, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x053a, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x053b, code lost:
        r5.A0C("Error trying to parse the hardcoded host url", r4);
        r3 = "Failed to build collect POST endpoint url";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0543, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0544, code lost:
        r5.A0C("Error trying to parse the hardcoded host url", r4);
        r3 = "Failed to build collect GET endpoint url";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x054b, code lost:
        r5.A08(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x054f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0550, code lost:
        r5.A0C("Error trying to parse the hardcoded host url", r4);
        r5.A08("Failed to build batching endpoint url");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x055a, code lost:
        r7 = java.util.Collections.emptyList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x055e, code lost:
        r6 = r7.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x0566, code lost:
        if (r6.hasNext() != false) goto L_0x0568;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0568, code lost:
        r1 = java.lang.Math.max(r1, ((java.lang.Long) r6.next()).longValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0577, code lost:
        r3.A0K(r7);
        r23.addAll(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0585, code lost:
        if (r23.isEmpty() == false) goto L_0x0587;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x059d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x059e, code lost:
        A0C("Failed to remove hit that was send for delivery", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x05a4, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x05b4, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x05b5, code lost:
        if (r10 != null) goto L_0x05b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x05b7, code lost:
        r10.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x05ba, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x05c0, code lost:
        throw new java.lang.IllegalArgumentException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x05c1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x05c9, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x05ca, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x05cb, code lost:
        A0C("Failed to remove successfully uploaded hits", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x05d7, code lost:
        A01();
        A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x05dd, code lost:
        r3.A0G();
        r3.A0I().setTransactionSuccessful();
        r3.A0G();
        r3.A0I().endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00b9, code lost:
        A09("Store is empty, nothing to dispatch");
        A01();
        A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x060a, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x060b, code lost:
        A0C("Failed to commit local dispatch transaction", r2);
        A01();
        A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:?, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:?, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0103, code lost:
        if (r3.A0I() == false) goto L_0x0150;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0105, code lost:
        A09("Service connected, sending hits to the service");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x010e, code lost:
        if (r10.isEmpty() != false) goto L_0x0150;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0110, code lost:
        r7 = (X.AnonymousClass3H2) r10.get(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011d, code lost:
        if (r3.A0J(r7) == false) goto L_0x0150;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x011f, code lost:
        r3 = r7.A01;
        r1 = java.lang.Math.max(r1, r3);
        r10.remove(r7);
        A0B("Hit sent do device AnalyticsService for delivery", r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x012d, code lost:
        X.C14170ky.A00();
        r3.A0G();
        r6 = new java.util.ArrayList(1);
        r7 = java.lang.Long.valueOf(r3);
        r6.add(r7);
        r3.A0D("Deleting hit, id", r7);
        r3.A0K(r6);
        r23.add(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0154, code lost:
        if (r5.A0J() == false) goto L_0x0581;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0156, code lost:
        X.C14170ky.A00();
        r5.A0G();
        r6 = ((X.C15050mT) r5).A00;
        r3 = r6.A09;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x016d, code lost:
        if (r3.A00().isEmpty() != false) goto L_0x01cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0185, code lost:
        if (r5.A00.A00(((long) ((java.lang.Integer) X.C88904Hw.A02.A00()).intValue()) * 1000) == false) goto L_0x01cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0187, code lost:
        r4 = (java.lang.String) X.C88904Hw.A0d.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0195, code lost:
        if ("BATCH_BY_SESSION".equalsIgnoreCase(r4) != false) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x019d, code lost:
        if ("BATCH_BY_TIME".equalsIgnoreCase(r4) != false) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01a5, code lost:
        if ("BATCH_BY_BRUTE_FORCE".equalsIgnoreCase(r4) != false) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01ad, code lost:
        if ("BATCH_BY_COUNT".equalsIgnoreCase(r4) != false) goto L_0x01b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01af, code lost:
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01b6, code lost:
        if ("BATCH_BY_SIZE".equalsIgnoreCase(r4) == false) goto L_0x01b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01b8, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01b9, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01c9, code lost:
        if (true == "GZIP".equalsIgnoreCase((java.lang.String) X.C88904Hw.A0e.A00())) goto L_0x01cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01cc, code lost:
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01cd, code lost:
        r16 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01cf, code lost:
        if (r7 == false) goto L_0x03ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01d6, code lost:
        if ((!r10.isEmpty()) == false) goto L_0x05bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01d8, code lost:
        X.C15050mT.A06(r5, java.lang.Boolean.valueOf(r16), java.lang.Integer.valueOf(r10.size()), null, "Uploading batched hits. compression, count", 2);
        r8 = new X.AnonymousClass4PQ(r5);
        r7 = new java.util.ArrayList();
        r15 = r10.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0201, code lost:
        if (r15.hasNext() == false) goto L_0x029f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0203, code lost:
        r13 = (X.AnonymousClass3H2) r15.next();
        X.C13020j0.A01(r13);
        r4 = r8.A00;
        r12 = r8.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x021b, code lost:
        if ((r4 + 1) > ((java.lang.Integer) r20.A00()).intValue()) goto L_0x029f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x021d, code lost:
        r3 = r12.A0H(r13, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0222, code lost:
        if (r3 != null) goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0224, code lost:
        r10 = ((X.C15050mT) r12).A00.A0C;
        X.C14160kx.A01(r10);
        r11 = "Error formatting hit";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x022e, code lost:
        r11 = r3.getBytes();
        r3 = r11.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x023f, code lost:
        if (r3 <= ((java.lang.Integer) X.C88904Hw.A0g.A00()).intValue()) goto L_0x0251;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0241, code lost:
        r10 = ((X.C15050mT) r12).A00.A0C;
        X.C14160kx.A01(r10);
        r11 = "Hit size exceeds the maximum size limit";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x024a, code lost:
        if (r13 == null) goto L_0x0289;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x024c, code lost:
        r4 = r13.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0251, code lost:
        r10 = r8.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0257, code lost:
        if (r10.size() <= 0) goto L_0x025b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0259, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x026c, code lost:
        if ((r10.size() + r3) > ((java.lang.Integer) X.C88904Hw.A00.A00()).intValue()) goto L_0x029f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0272, code lost:
        if (r10.size() <= 0) goto L_0x0279;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0274, code lost:
        r10.write(X.C56622lJ.A02);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0279, code lost:
        r10.write(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x027c, code lost:
        r8.A00++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0282, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0283, code lost:
        r12.A0C("Failed to write payload when batching hits", r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0289, code lost:
        r4 = "no hit data";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x028b, code lost:
        r10.A0E(X.C12960it.A0c(r11, "Discarding hit. "), r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x02a1, code lost:
        if (r8.A00 == 0) goto L_0x055e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x02a3, code lost:
        r9 = (java.lang.String) X.C88904Hw.A0Z.A00();
        r4 = (java.lang.String) X.C88904Hw.A0b.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x02b7, code lost:
        if (r4.length() == 0) goto L_0x02be;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02b9, code lost:
        r3 = r9.concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x02be, code lost:
        r3 = new java.lang.String(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x02c3, code lost:
        r11 = new java.net.URL(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x02c8, code lost:
        if (r16 == false) goto L_0x03b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x02ca, code lost:
        r10 = r8.A01.toByteArray();
        X.C13020j0.A01(r10);
     */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03ab A[Catch: all -> 0x05f2, TryCatch #20 {Exception -> 0x0663, blocks: (B:10:0x0048, B:13:0x0064, B:16:0x006f, B:17:0x0076, B:202:0x0587, B:219:0x05dd, B:221:0x060b, B:222:0x0616, B:224:0x0638, B:225:0x063e, B:227:0x0644, B:18:0x009a, B:19:0x00ab, B:21:0x00b9, B:22:0x00c6, B:23:0x00d3, B:24:0x00d7, B:26:0x00dd, B:28:0x00e9, B:29:0x00ff, B:31:0x0105, B:32:0x010a, B:34:0x0110, B:36:0x011f, B:37:0x012d, B:38:0x0150, B:40:0x0156, B:42:0x016f, B:44:0x0187, B:46:0x0197, B:48:0x019f, B:50:0x01a7, B:52:0x01af, B:55:0x01b9, B:61:0x01d1, B:63:0x01d8, B:64:0x01fd, B:66:0x0203, B:68:0x021d, B:70:0x0224, B:71:0x022e, B:73:0x0241, B:75:0x024c, B:76:0x0251, B:78:0x0259, B:79:0x025b, B:81:0x026e, B:83:0x0274, B:84:0x0279, B:85:0x027c, B:87:0x0283, B:89:0x028b, B:90:0x0294, B:91:0x029f, B:93:0x02a3, B:95:0x02b9, B:96:0x02be, B:97:0x02c3, B:99:0x02ca, B:117:0x038d, B:125:0x039f, B:127:0x03a4, B:129:0x03ab, B:131:0x03b0, B:134:0x03be, B:135:0x03cb, B:137:0x03de, B:138:0x03ed, B:139:0x03fa, B:141:0x0400, B:143:0x0413, B:144:0x041a, B:145:0x0427, B:148:0x0442, B:152:0x0458, B:153:0x045a, B:154:0x048a, B:155:0x048d, B:156:0x0492, B:162:0x04c7, B:163:0x04cb, B:165:0x04d2, B:166:0x04db, B:168:0x04ee, B:170:0x04f9, B:171:0x04fc, B:172:0x04fe, B:174:0x0512, B:175:0x0517, B:176:0x051c, B:177:0x0521, B:186:0x0536, B:188:0x053b, B:190:0x0544, B:191:0x054b, B:193:0x0550, B:194:0x055a, B:195:0x055e, B:196:0x0562, B:198:0x0568, B:199:0x0577, B:200:0x0581, B:204:0x059e, B:208:0x05b7, B:209:0x05ba, B:210:0x05bb, B:211:0x05c0, B:213:0x05c9, B:215:0x05cb, B:217:0x05d2, B:218:0x05d7, B:158:0x049b, B:160:0x04a9, B:161:0x04be, B:184:0x052f, B:112:0x035e, B:123:0x0398), top: B:254:0x0048, inners: #12 }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x03de A[Catch: all -> 0x05f2, TryCatch #20 {Exception -> 0x0663, blocks: (B:10:0x0048, B:13:0x0064, B:16:0x006f, B:17:0x0076, B:202:0x0587, B:219:0x05dd, B:221:0x060b, B:222:0x0616, B:224:0x0638, B:225:0x063e, B:227:0x0644, B:18:0x009a, B:19:0x00ab, B:21:0x00b9, B:22:0x00c6, B:23:0x00d3, B:24:0x00d7, B:26:0x00dd, B:28:0x00e9, B:29:0x00ff, B:31:0x0105, B:32:0x010a, B:34:0x0110, B:36:0x011f, B:37:0x012d, B:38:0x0150, B:40:0x0156, B:42:0x016f, B:44:0x0187, B:46:0x0197, B:48:0x019f, B:50:0x01a7, B:52:0x01af, B:55:0x01b9, B:61:0x01d1, B:63:0x01d8, B:64:0x01fd, B:66:0x0203, B:68:0x021d, B:70:0x0224, B:71:0x022e, B:73:0x0241, B:75:0x024c, B:76:0x0251, B:78:0x0259, B:79:0x025b, B:81:0x026e, B:83:0x0274, B:84:0x0279, B:85:0x027c, B:87:0x0283, B:89:0x028b, B:90:0x0294, B:91:0x029f, B:93:0x02a3, B:95:0x02b9, B:96:0x02be, B:97:0x02c3, B:99:0x02ca, B:117:0x038d, B:125:0x039f, B:127:0x03a4, B:129:0x03ab, B:131:0x03b0, B:134:0x03be, B:135:0x03cb, B:137:0x03de, B:138:0x03ed, B:139:0x03fa, B:141:0x0400, B:143:0x0413, B:144:0x041a, B:145:0x0427, B:148:0x0442, B:152:0x0458, B:153:0x045a, B:154:0x048a, B:155:0x048d, B:156:0x0492, B:162:0x04c7, B:163:0x04cb, B:165:0x04d2, B:166:0x04db, B:168:0x04ee, B:170:0x04f9, B:171:0x04fc, B:172:0x04fe, B:174:0x0512, B:175:0x0517, B:176:0x051c, B:177:0x0521, B:186:0x0536, B:188:0x053b, B:190:0x0544, B:191:0x054b, B:193:0x0550, B:194:0x055a, B:195:0x055e, B:196:0x0562, B:198:0x0568, B:199:0x0577, B:200:0x0581, B:204:0x059e, B:208:0x05b7, B:209:0x05ba, B:210:0x05bb, B:211:0x05c0, B:213:0x05c9, B:215:0x05cb, B:217:0x05d2, B:218:0x05d7, B:158:0x049b, B:160:0x04a9, B:161:0x04be, B:184:0x052f, B:112:0x035e, B:123:0x0398), top: B:254:0x0048, inners: #12 }] */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0568 A[Catch: all -> 0x05f2, LOOP:5: B:196:0x0562->B:198:0x0568, LOOP_END, TRY_LEAVE, TryCatch #20 {Exception -> 0x0663, blocks: (B:10:0x0048, B:13:0x0064, B:16:0x006f, B:17:0x0076, B:202:0x0587, B:219:0x05dd, B:221:0x060b, B:222:0x0616, B:224:0x0638, B:225:0x063e, B:227:0x0644, B:18:0x009a, B:19:0x00ab, B:21:0x00b9, B:22:0x00c6, B:23:0x00d3, B:24:0x00d7, B:26:0x00dd, B:28:0x00e9, B:29:0x00ff, B:31:0x0105, B:32:0x010a, B:34:0x0110, B:36:0x011f, B:37:0x012d, B:38:0x0150, B:40:0x0156, B:42:0x016f, B:44:0x0187, B:46:0x0197, B:48:0x019f, B:50:0x01a7, B:52:0x01af, B:55:0x01b9, B:61:0x01d1, B:63:0x01d8, B:64:0x01fd, B:66:0x0203, B:68:0x021d, B:70:0x0224, B:71:0x022e, B:73:0x0241, B:75:0x024c, B:76:0x0251, B:78:0x0259, B:79:0x025b, B:81:0x026e, B:83:0x0274, B:84:0x0279, B:85:0x027c, B:87:0x0283, B:89:0x028b, B:90:0x0294, B:91:0x029f, B:93:0x02a3, B:95:0x02b9, B:96:0x02be, B:97:0x02c3, B:99:0x02ca, B:117:0x038d, B:125:0x039f, B:127:0x03a4, B:129:0x03ab, B:131:0x03b0, B:134:0x03be, B:135:0x03cb, B:137:0x03de, B:138:0x03ed, B:139:0x03fa, B:141:0x0400, B:143:0x0413, B:144:0x041a, B:145:0x0427, B:148:0x0442, B:152:0x0458, B:153:0x045a, B:154:0x048a, B:155:0x048d, B:156:0x0492, B:162:0x04c7, B:163:0x04cb, B:165:0x04d2, B:166:0x04db, B:168:0x04ee, B:170:0x04f9, B:171:0x04fc, B:172:0x04fe, B:174:0x0512, B:175:0x0517, B:176:0x051c, B:177:0x0521, B:186:0x0536, B:188:0x053b, B:190:0x0544, B:191:0x054b, B:193:0x0550, B:194:0x055a, B:195:0x055e, B:196:0x0562, B:198:0x0568, B:199:0x0577, B:200:0x0581, B:204:0x059e, B:208:0x05b7, B:209:0x05ba, B:210:0x05bb, B:211:0x05c0, B:213:0x05c9, B:215:0x05cb, B:217:0x05d2, B:218:0x05d7, B:158:0x049b, B:160:0x04a9, B:161:0x04be, B:184:0x052f, B:112:0x035e, B:123:0x0398), top: B:254:0x0048, inners: #12 }] */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0638 A[Catch: Exception -> 0x0663, TryCatch #20 {Exception -> 0x0663, blocks: (B:10:0x0048, B:13:0x0064, B:16:0x006f, B:17:0x0076, B:202:0x0587, B:219:0x05dd, B:221:0x060b, B:222:0x0616, B:224:0x0638, B:225:0x063e, B:227:0x0644, B:18:0x009a, B:19:0x00ab, B:21:0x00b9, B:22:0x00c6, B:23:0x00d3, B:24:0x00d7, B:26:0x00dd, B:28:0x00e9, B:29:0x00ff, B:31:0x0105, B:32:0x010a, B:34:0x0110, B:36:0x011f, B:37:0x012d, B:38:0x0150, B:40:0x0156, B:42:0x016f, B:44:0x0187, B:46:0x0197, B:48:0x019f, B:50:0x01a7, B:52:0x01af, B:55:0x01b9, B:61:0x01d1, B:63:0x01d8, B:64:0x01fd, B:66:0x0203, B:68:0x021d, B:70:0x0224, B:71:0x022e, B:73:0x0241, B:75:0x024c, B:76:0x0251, B:78:0x0259, B:79:0x025b, B:81:0x026e, B:83:0x0274, B:84:0x0279, B:85:0x027c, B:87:0x0283, B:89:0x028b, B:90:0x0294, B:91:0x029f, B:93:0x02a3, B:95:0x02b9, B:96:0x02be, B:97:0x02c3, B:99:0x02ca, B:117:0x038d, B:125:0x039f, B:127:0x03a4, B:129:0x03ab, B:131:0x03b0, B:134:0x03be, B:135:0x03cb, B:137:0x03de, B:138:0x03ed, B:139:0x03fa, B:141:0x0400, B:143:0x0413, B:144:0x041a, B:145:0x0427, B:148:0x0442, B:152:0x0458, B:153:0x045a, B:154:0x048a, B:155:0x048d, B:156:0x0492, B:162:0x04c7, B:163:0x04cb, B:165:0x04d2, B:166:0x04db, B:168:0x04ee, B:170:0x04f9, B:171:0x04fc, B:172:0x04fe, B:174:0x0512, B:175:0x0517, B:176:0x051c, B:177:0x0521, B:186:0x0536, B:188:0x053b, B:190:0x0544, B:191:0x054b, B:193:0x0550, B:194:0x055a, B:195:0x055e, B:196:0x0562, B:198:0x0568, B:199:0x0577, B:200:0x0581, B:204:0x059e, B:208:0x05b7, B:209:0x05ba, B:210:0x05bb, B:211:0x05c0, B:213:0x05c9, B:215:0x05cb, B:217:0x05d2, B:218:0x05d7, B:158:0x049b, B:160:0x04a9, B:161:0x04be, B:184:0x052f, B:112:0x035e, B:123:0x0398), top: B:254:0x0048, inners: #12 }] */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0644 A[Catch: Exception -> 0x0663, TryCatch #20 {Exception -> 0x0663, blocks: (B:10:0x0048, B:13:0x0064, B:16:0x006f, B:17:0x0076, B:202:0x0587, B:219:0x05dd, B:221:0x060b, B:222:0x0616, B:224:0x0638, B:225:0x063e, B:227:0x0644, B:18:0x009a, B:19:0x00ab, B:21:0x00b9, B:22:0x00c6, B:23:0x00d3, B:24:0x00d7, B:26:0x00dd, B:28:0x00e9, B:29:0x00ff, B:31:0x0105, B:32:0x010a, B:34:0x0110, B:36:0x011f, B:37:0x012d, B:38:0x0150, B:40:0x0156, B:42:0x016f, B:44:0x0187, B:46:0x0197, B:48:0x019f, B:50:0x01a7, B:52:0x01af, B:55:0x01b9, B:61:0x01d1, B:63:0x01d8, B:64:0x01fd, B:66:0x0203, B:68:0x021d, B:70:0x0224, B:71:0x022e, B:73:0x0241, B:75:0x024c, B:76:0x0251, B:78:0x0259, B:79:0x025b, B:81:0x026e, B:83:0x0274, B:84:0x0279, B:85:0x027c, B:87:0x0283, B:89:0x028b, B:90:0x0294, B:91:0x029f, B:93:0x02a3, B:95:0x02b9, B:96:0x02be, B:97:0x02c3, B:99:0x02ca, B:117:0x038d, B:125:0x039f, B:127:0x03a4, B:129:0x03ab, B:131:0x03b0, B:134:0x03be, B:135:0x03cb, B:137:0x03de, B:138:0x03ed, B:139:0x03fa, B:141:0x0400, B:143:0x0413, B:144:0x041a, B:145:0x0427, B:148:0x0442, B:152:0x0458, B:153:0x045a, B:154:0x048a, B:155:0x048d, B:156:0x0492, B:162:0x04c7, B:163:0x04cb, B:165:0x04d2, B:166:0x04db, B:168:0x04ee, B:170:0x04f9, B:171:0x04fc, B:172:0x04fe, B:174:0x0512, B:175:0x0517, B:176:0x051c, B:177:0x0521, B:186:0x0536, B:188:0x053b, B:190:0x0544, B:191:0x054b, B:193:0x0550, B:194:0x055a, B:195:0x055e, B:196:0x0562, B:198:0x0568, B:199:0x0577, B:200:0x0581, B:204:0x059e, B:208:0x05b7, B:209:0x05ba, B:210:0x05bb, B:211:0x05c0, B:213:0x05c9, B:215:0x05cb, B:217:0x05d2, B:218:0x05d7, B:158:0x049b, B:160:0x04a9, B:161:0x04be, B:184:0x052f, B:112:0x035e, B:123:0x0398), top: B:254:0x0048, inners: #12 }] */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x0587 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x039f A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x05dd A[EDGE_INSN: B:265:0x05dd->B:219:0x05dd ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:281:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0L(X.AbstractC115635Si r36, long r37) {
        /*
        // Method dump skipped, instructions count: 1681
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15030mR.A0L(X.5Si, long):void");
    }
}
