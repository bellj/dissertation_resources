package X;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1I9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1I9 extends AnonymousClass1IA {
    public AnonymousClass1I9(BlockingQueue blockingQueue, ThreadFactory threadFactory, TimeUnit timeUnit) {
        super("WhatsApp Worker", blockingQueue, threadFactory, timeUnit, 5, 128, 1);
    }

    @Override // java.util.concurrent.ThreadPoolExecutor
    public void afterExecute(Runnable runnable, Throwable th) {
        AnonymousClass168.A05.A01(this, runnable);
    }

    @Override // java.util.concurrent.ThreadPoolExecutor
    public void beforeExecute(Thread thread, Runnable runnable) {
        AnonymousClass168.A05.A03(this, runnable, thread);
    }

    @Override // java.util.concurrent.ThreadPoolExecutor, java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        AnonymousClass168.A05.A02(this, runnable);
        super.execute(runnable);
    }
}
