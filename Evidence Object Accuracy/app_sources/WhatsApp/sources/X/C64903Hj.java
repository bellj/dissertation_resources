package X;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import java.util.Arrays;
import java.util.Iterator;
import java.util.MissingFormatArgumentException;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: X.3Hj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C64903Hj {
    public final Bundle A00;

    public C64903Hj(Bundle bundle) {
        this.A00 = new Bundle(bundle);
    }

    public static String A00(String str) {
        return str.startsWith("gcm.n.") ? str.substring(6) : str;
    }

    public static boolean A01(Bundle bundle) {
        String str = "gcm.n.e";
        if (!"1".equals(bundle.getString(str))) {
            if (str.startsWith("gcm.n.")) {
                str = str.replace("gcm.n.", "gcm.notification.");
            }
            if (!"1".equals(bundle.getString(str))) {
                String str2 = "gcm.n.icon";
                if (bundle.getString(str2) == null) {
                    if (str2.startsWith("gcm.n.")) {
                        str2 = str2.replace("gcm.n.", "gcm.notification.");
                    }
                    if (bundle.getString(str2) == null) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public final Bundle A02() {
        Bundle bundle = this.A00;
        Bundle bundle2 = new Bundle(bundle);
        Iterator<String> it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (A0x.startsWith("google.c.") || A0x.startsWith("gcm.n.") || A0x.startsWith("gcm.notification.")) {
                bundle2.remove(A0x);
            }
        }
        return bundle2;
    }

    public final Bundle A03() {
        Bundle bundle = this.A00;
        Bundle bundle2 = new Bundle(bundle);
        Iterator<String> it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (!A0x.startsWith("google.c.a.") && !A0x.equals("from")) {
                bundle2.remove(A0x);
            }
        }
        return bundle2;
    }

    public final Integer A04(String str) {
        String A06 = A06(str);
        if (TextUtils.isEmpty(A06)) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(A06));
        } catch (NumberFormatException unused) {
            String A00 = A00(str);
            StringBuilder A0i = C12960it.A0i(A06, C12970iu.A07(A00) + 38);
            A0i.append("Couldn't parse value of ");
            A0i.append(A00);
            A0i.append("(");
            A0i.append(A06);
            Log.w("NotificationParams", C12960it.A0d(") into an int", A0i));
            return null;
        }
    }

    public final String A05(Resources resources, String str, String str2) {
        String[] strArr;
        String A06 = A06(str2);
        if (!TextUtils.isEmpty(A06)) {
            return A06;
        }
        String A062 = A06(String.valueOf(str2).concat("_loc_key"));
        String str3 = null;
        if (!TextUtils.isEmpty(A062)) {
            int identifier = resources.getIdentifier(A062, "string", str);
            if (identifier == 0) {
                String A00 = A00(String.valueOf(str2).concat("_loc_key"));
                StringBuilder A0i = C12960it.A0i(str2, C12970iu.A07(A00) + 49);
                A0i.append(A00);
                A0i.append(" resource not found: ");
                A0i.append(str2);
                Log.w("NotificationParams", C12960it.A0d(" Default value will be used.", A0i));
            } else {
                JSONArray A07 = A07(String.valueOf(str2).concat("_loc_args"));
                if (A07 == null) {
                    strArr = null;
                } else {
                    int length = A07.length();
                    strArr = new String[length];
                    for (int i = 0; i < length; i++) {
                        strArr[i] = A07.optString(i);
                    }
                }
                if (strArr == null) {
                    return resources.getString(identifier);
                }
                try {
                    str3 = resources.getString(identifier, strArr);
                    return str3;
                } catch (MissingFormatArgumentException e) {
                    String A002 = A00(str2);
                    String arrays = Arrays.toString(strArr);
                    StringBuilder A0i2 = C12960it.A0i(arrays, C12970iu.A07(A002) + 58);
                    A0i2.append("Missing format argument for ");
                    C12990iw.A1T(A0i2, A002);
                    A0i2.append(arrays);
                    Log.w("NotificationParams", C12960it.A0d(" Default value will be used.", A0i2), e);
                    return str3;
                }
            }
        }
        return null;
    }

    public final String A06(String str) {
        Bundle bundle = this.A00;
        if (!bundle.containsKey(str) && str.startsWith("gcm.n.")) {
            String replace = str.replace("gcm.n.", "gcm.notification.");
            if (bundle.containsKey(replace)) {
                str = replace;
            }
        }
        return bundle.getString(str);
    }

    public final JSONArray A07(String str) {
        String A06 = A06(str);
        if (TextUtils.isEmpty(A06)) {
            return null;
        }
        try {
            return new JSONArray(A06);
        } catch (JSONException unused) {
            String A00 = A00(str);
            StringBuilder A0i = C12960it.A0i(A06, C12970iu.A07(A00) + 50);
            A0i.append("Malformed JSON for key ");
            C12990iw.A1T(A0i, A00);
            A0i.append(A06);
            Log.w("NotificationParams", C12960it.A0d(", falling back to default", A0i));
            return null;
        }
    }

    public final boolean A08(String str) {
        String A06 = A06(str);
        return "1".equals(A06) || Boolean.parseBoolean(A06);
    }
}
