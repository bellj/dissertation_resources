package X;

import java.util.concurrent.CancellationException;

/* renamed from: X.5LK  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5LK extends AbstractRunnableC75993ks {
    public int A00;

    public AnonymousClass5LK(int i) {
        super(C88864Hl.A06, 0);
        this.A00 = i;
    }

    public void A00(Throwable th) {
        if (!(this instanceof C114205Kp)) {
            C114215Kq r1 = (C114215Kq) this;
            while (true) {
                Object obj = r1._state;
                if (obj instanceof AbstractC115535Rx) {
                    throw C12960it.A0U("Not completed");
                } else if (obj instanceof C94894ci) {
                    return;
                } else {
                    if (obj instanceof C92844Xq) {
                        C92844Xq r2 = (C92844Xq) obj;
                        if (r2.A02 != null) {
                            throw C12960it.A0U("Must be called at most once");
                        }
                        Object obj2 = r2.A01;
                        AbstractC114125Kh r7 = r2.A04;
                        AnonymousClass1J7 r6 = r2.A03;
                        if (AnonymousClass0KN.A00(r1, obj, new C92844Xq(obj2, r2.A00, th, r6, r7), C114215Kq.A04)) {
                            if (r7 != null) {
                                r1.A0D(r7);
                            }
                            if (r6 != null) {
                                try {
                                    r6.AJ4(th);
                                    return;
                                } catch (Throwable th2) {
                                    C88234Eu.A00(r1.A02, new C113295Gy(C16700pc.A08("Exception in resume onCancellation handler for ", r1), th2));
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        if (AnonymousClass0KN.A00(r1, obj, new C92844Xq(obj, null, th, null, null), C114215Kq.A04)) {
                            return;
                        }
                    }
                }
            }
        }
    }

    public final void A01(Throwable th, Throwable th2) {
        AnonymousClass5WO r1;
        if (th == null) {
            if (th2 != null) {
                th = th2;
            } else {
                return;
            }
        } else if (th2 != null) {
            C88164En.A00(th, th2);
        }
        StringBuilder A0k = C12960it.A0k("Fatal exception in coroutines machinery for ");
        A0k.append(this);
        C87384Bh r2 = new C87384Bh(C12960it.A0d(". Please read KDoc to 'handleFatalException' method and report this incident to maintainers", A0k), th);
        if (!(this instanceof C114205Kp)) {
            r1 = ((C114215Kq) this).A01;
        } else {
            r1 = (C114205Kp) this;
        }
        C88234Eu.A00(r1.ABi(), r2);
    }

    public Throwable A02(Object obj) {
        C94894ci r3;
        if (!(obj instanceof C94894ci) || (r3 = (C94894ci) obj) == null) {
            return null;
        }
        return r3.A00;
    }

    @Override // java.lang.Runnable
    public final void run() {
        Throwable th;
        Object obj;
        Object obj2;
        AnonymousClass5WO r5;
        AnonymousClass5X4 ABi;
        Object A00;
        AnonymousClass5X4 ABi2;
        Object obj3;
        Throwable A02;
        AnonymousClass5BR r0;
        AbstractC02760Dw r1;
        try {
            boolean z = this instanceof C114205Kp;
            if (!z) {
                obj2 = ((C114215Kq) this).A01;
            } else {
                obj2 = (C114205Kp) this;
            }
            C114205Kp r02 = (C114205Kp) obj2;
            r5 = r02.A02;
            Object obj4 = r02.A01;
            ABi = r5.ABi();
            A00 = C94704cP.A00(obj4, ABi);
            th = null;
            if (A00 != C94704cP.A03) {
                C88224Et.A00(r5, ABi);
            }
            ABi2 = r5.ABi();
            if (!z) {
                obj3 = ((C114215Kq) this)._state;
            } else {
                C114205Kp r8 = (C114205Kp) this;
                obj3 = r8.A00;
                r8.A00 = AnonymousClass4HG.A01;
            }
            A02 = A02(obj3);
        } catch (Throwable th2) {
            th = th2;
            try {
                obj = AnonymousClass1WZ.A00;
            } catch (Throwable th3) {
                obj = new AnonymousClass5BR(th3);
            }
        }
        if (A02 == null) {
            int i = this.A00;
            if ((i == 1 || i == 2) && (r1 = (AbstractC02760Dw) ABi2.get(AbstractC02760Dw.A00)) != null && !r1.AJD()) {
                CancellationException ABF = r1.ABF();
                A00(ABF);
                r0 = new AnonymousClass5BR(ABF);
            } else {
                if ((this instanceof C114215Kq) && (obj3 instanceof C92844Xq)) {
                    obj3 = ((C92844Xq) obj3).A01;
                }
                r5.Aas(obj3);
                obj = AnonymousClass1WZ.A00;
                C94704cP.A01(A00, ABi);
                A01(th, AnonymousClass5BU.A00(obj));
            }
        } else {
            r0 = new AnonymousClass5BR(A02);
        }
        r5.Aas(r0);
        obj = AnonymousClass1WZ.A00;
        C94704cP.A01(A00, ABi);
        A01(th, AnonymousClass5BU.A00(obj));
    }
}
