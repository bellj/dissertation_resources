package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.3fC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72803fC extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass2CF A00;
    public final /* synthetic */ Runnable A01;

    public C72803fC(AnonymousClass2CF r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass2CF r2 = this.A00;
        if (r2.A09 < 0.15f) {
            r2.A06();
        }
        Runnable runnable = this.A01;
        if (runnable != null) {
            r2.post(runnable);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        AnonymousClass2CF r1 = this.A00;
        r1.setVisibility(0);
        r1.A0d = true;
    }
}
