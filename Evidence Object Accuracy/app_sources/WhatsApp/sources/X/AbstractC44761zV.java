package X;

/* renamed from: X.1zV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC44761zV {
    public abstract String toString();

    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00dd, code lost:
        if (r4.A03 == false) goto L_0x00df;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A00() {
        /*
        // Method dump skipped, instructions count: 274
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC44761zV.A00():boolean");
    }

    public boolean A01() {
        boolean z;
        boolean z2;
        if (this instanceof C44801zZ) {
            C44801zZ r0 = (C44801zZ) this;
            synchronized (r0.A07) {
                z = r0.A03;
            }
            return z;
        } else if (!(this instanceof C44811za)) {
            return true;
        } else {
            C44811za r02 = (C44811za) this;
            synchronized (r02.A07) {
                z2 = r02.A00;
            }
            return z2;
        }
    }

    public boolean A02() {
        boolean z;
        boolean z2;
        if (this instanceof C44801zZ) {
            C44801zZ r0 = (C44801zZ) this;
            synchronized (r0.A07) {
                z = r0.A04;
            }
            return z;
        } else if (!(this instanceof C44811za)) {
            return true;
        } else {
            C44811za r02 = (C44811za) this;
            synchronized (r02.A07) {
                z2 = r02.A01;
            }
            return z2;
        }
    }

    public boolean A03() {
        boolean z;
        boolean z2;
        if (this instanceof C44801zZ) {
            C44801zZ r0 = (C44801zZ) this;
            synchronized (r0.A07) {
                z = r0.A05;
            }
            return z;
        } else if (!(this instanceof C44811za)) {
            return true;
        } else {
            C44811za r02 = (C44811za) this;
            synchronized (r02.A07) {
                z2 = r02.A02;
            }
            return z2;
        }
    }

    public boolean A04() {
        boolean z;
        boolean z2;
        if (this instanceof C44801zZ) {
            C44801zZ r0 = (C44801zZ) this;
            synchronized (r0.A07) {
                z = r0.A06;
            }
            return z;
        } else if (!(this instanceof C44811za)) {
            return true;
        } else {
            C44811za r02 = (C44811za) this;
            synchronized (r02.A07) {
                z2 = r02.A03;
            }
            return z2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001f, code lost:
        if (r0 == false) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x003c, code lost:
        if (r0 == false) goto L_0x003e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A05() {
        /*
            r3 = this;
            boolean r0 = r3 instanceof X.C44801zZ
            if (r0 != 0) goto L_0x0027
            boolean r0 = r3 instanceof X.C44811za
            if (r0 != 0) goto L_0x000a
            r2 = 1
            return r2
        L_0x000a:
            r2 = r3
            X.1za r2 = (X.C44811za) r2
            java.lang.Object r1 = r2.A07
            monitor-enter(r1)
            boolean r0 = r2.A02     // Catch: all -> 0x0024
            if (r0 == 0) goto L_0x0021
            boolean r0 = r2.A01     // Catch: all -> 0x0024
            if (r0 == 0) goto L_0x0021
            boolean r0 = r2.A03     // Catch: all -> 0x0024
            if (r0 == 0) goto L_0x0021
            boolean r0 = r2.A00     // Catch: all -> 0x0024
            r2 = 1
            if (r0 != 0) goto L_0x0022
        L_0x0021:
            r2 = 0
        L_0x0022:
            monitor-exit(r1)     // Catch: all -> 0x0024
            return r2
        L_0x0024:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0024
            throw r0
        L_0x0027:
            r2 = r3
            X.1zZ r2 = (X.C44801zZ) r2
            java.lang.Object r1 = r2.A07
            monitor-enter(r1)
            boolean r0 = r2.A05     // Catch: all -> 0x0041
            if (r0 == 0) goto L_0x003e
            boolean r0 = r2.A04     // Catch: all -> 0x0041
            if (r0 == 0) goto L_0x003e
            boolean r0 = r2.A06     // Catch: all -> 0x0041
            if (r0 == 0) goto L_0x003e
            boolean r0 = r2.A03     // Catch: all -> 0x0041
            r2 = 1
            if (r0 != 0) goto L_0x003f
        L_0x003e:
            r2 = 0
        L_0x003f:
            monitor-exit(r1)     // Catch: all -> 0x0041
            return r2
        L_0x0041:
            r0 = move-exception
            monitor-exit(r1)     // Catch: all -> 0x0041
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC44761zV.A05():boolean");
    }
}
