package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.3HN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3HN {
    public final int A00;

    public AnonymousClass3HN(int i) {
        this.A00 = i;
    }

    public static C44711zQ A00(AnonymousClass1V8 r3) {
        boolean z;
        String str = null;
        if (r3 != null) {
            AnonymousClass1V8 A0E = r3.A0E("after");
            if (A0E != null) {
                str = A0E.A0G();
            }
            z = !TextUtils.isEmpty(str);
        } else {
            z = false;
        }
        return new C44711zQ(str, z);
    }

    public C44721zR A01(AnonymousClass1V8 r5) {
        String str;
        if (this.A00 != 1) {
            str = "product_catalog";
        } else {
            str = "product_list";
        }
        AnonymousClass1V8 A0E = r5.A0E(str);
        if (A0E == null) {
            return null;
        }
        List<AnonymousClass1V8> A0J = A0E.A0J("product");
        ArrayList A0l = C12960it.A0l();
        for (AnonymousClass1V8 r0 : A0J) {
            C44691zO A02 = A02(r0);
            if (A02 != null) {
                A0l.add(A02);
            }
        }
        return new C44721zR(A00(A0E.A0E("paging")), A0l);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01ff, code lost:
        if (java.lang.Boolean.parseBoolean(r8.A0G()) == false) goto L_0x0201;
     */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x014a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x013a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C44691zO A02(X.AnonymousClass1V8 r44) {
        /*
        // Method dump skipped, instructions count: 760
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3HN.A02(X.1V8):X.1zO");
    }

    public void A03(C14650lo r4, UserJid userJid, AnonymousClass1V8 r6) {
        String str;
        String A0I;
        if (this.A00 != 1) {
            str = "product_catalog";
        } else {
            str = "product_list";
        }
        AnonymousClass1V8 A0E = r6.A0E(str);
        if (A0E != null && (A0I = A0E.A0I("cart_enabled", null)) != null) {
            r4.A07(userJid, Boolean.parseBoolean(A0I));
        }
    }
}
