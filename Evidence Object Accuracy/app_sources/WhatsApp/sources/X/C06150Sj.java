package X;

import android.content.Context;
import android.hardware.biometrics.BiometricManager;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.util.Log;
import com.whatsapp.R;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* renamed from: X.0Sj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06150Sj {
    public final BiometricManager A00;
    public final AbstractC11660ge A01;
    public final AnonymousClass049 A02;

    public C06150Sj(AbstractC11660ge r5) {
        BiometricManager biometricManager;
        this.A01 = r5;
        int i = Build.VERSION.SDK_INT;
        AnonymousClass049 r2 = null;
        if (i >= 29) {
            biometricManager = r5.AAq();
        } else {
            biometricManager = null;
        }
        this.A00 = biometricManager;
        this.A02 = i <= 29 ? new AnonymousClass049(((AnonymousClass0XW) r5).A00) : r2;
    }

    public final int A00() {
        int i;
        boolean A01 = C05580Qc.A01(((AnonymousClass0XW) this.A01).A00);
        AnonymousClass049 r1 = this.A02;
        if (r1 == null) {
            Log.e("BiometricManager", "Failure in canAuthenticate(). FingerprintManager was null.");
            i = 1;
        } else if (!r1.A06()) {
            i = 12;
        } else {
            boolean A05 = r1.A05();
            i = 0;
            if (!A05) {
                i = 11;
            }
        }
        if (A01) {
            return i == 0 ? 0 : -1;
        }
        return i;
    }

    public final int A01() {
        BiometricPrompt.CryptoObject A00;
        Method A02 = C06280Sw.A02();
        if (!(A02 == null || (A00 = C05560Qa.A00(C05560Qa.A01())) == null)) {
            try {
                Object invoke = A02.invoke(this.A00, A00);
                if (invoke instanceof Integer) {
                    return ((Number) invoke).intValue();
                }
                Log.w("BiometricManager", "Invalid return type for canAuthenticate(CryptoObject).");
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                Log.w("BiometricManager", "Failed to invoke canAuthenticate(CryptoObject).", e);
            }
        }
        int A022 = A02();
        Context context = ((AnonymousClass0XW) this.A01).A00;
        String str = Build.MODEL;
        if (Build.VERSION.SDK_INT < 30 && str != null) {
            for (String str2 : context.getResources().getStringArray(R.array.assume_strong_biometrics_models)) {
                if (str.equals(str2)) {
                    return A022;
                }
            }
        }
        return A022 == 0 ? A00() : A022;
    }

    public final int A02() {
        BiometricManager biometricManager = this.A00;
        if (biometricManager != null) {
            return C06280Sw.A00(biometricManager);
        }
        Log.e("BiometricManager", "Failure in canAuthenticate(). BiometricManager was null.");
        return 1;
    }

    public int A03(int i) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 30) {
            BiometricManager biometricManager = this.A00;
            if (biometricManager != null) {
                return AnonymousClass0KI.A00(biometricManager, i);
            }
            Log.e("BiometricManager", "Failure in canAuthenticate(). BiometricManager was null.");
            return 1;
        } else if (!AnonymousClass0QX.A01(i)) {
            return -2;
        } else {
            Context context = ((AnonymousClass0XW) this.A01).A00;
            if (C05580Qc.A00(context) == null) {
                return 12;
            }
            if ((32768 & i) != 0) {
                if (C05580Qc.A01(context)) {
                    return 0;
                }
                return 11;
            } else if (i2 == 29) {
                if ((i & 255) == 255) {
                    return A02();
                }
                return A01();
            } else if (i2 != 28) {
                AnonymousClass049 r1 = this.A02;
                if (r1 == null) {
                    Log.e("BiometricManager", "Failure in canAuthenticate(). FingerprintManager was null.");
                    return 1;
                } else if (!r1.A06()) {
                    return 12;
                } else {
                    if (!r1.A05()) {
                        return 11;
                    }
                    return 0;
                }
            } else if (AnonymousClass0KM.A00(context)) {
                return A00();
            } else {
                return 12;
            }
        }
    }
}
