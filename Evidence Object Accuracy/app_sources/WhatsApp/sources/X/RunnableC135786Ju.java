package X;

/* renamed from: X.6Ju  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final /* synthetic */ class RunnableC135786Ju implements Runnable {
    public final /* synthetic */ AnonymousClass22U A00;
    public final /* synthetic */ AbstractC1311061h A01;
    public final /* synthetic */ AnonymousClass61P A02;
    public final /* synthetic */ C30061Vy A03;

    public /* synthetic */ RunnableC135786Ju(AnonymousClass22U r1, AbstractC1311061h r2, AnonymousClass61P r3, C30061Vy r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass61P r1 = this.A02;
        C30061Vy r3 = this.A03;
        AnonymousClass22U r0 = this.A00;
        AbstractC1311061h r2 = this.A01;
        r1.A01.A0L(r0.A02, r3);
        r2.A9z();
    }
}
