package X;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4k0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99254k0 implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        Uri uri = null;
        Bundle bundle = null;
        byte[] bArr = null;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            char c = (char) readInt;
            if (c == 2) {
                uri = (Uri) C95664e9.A07(parcel, Uri.CREATOR, readInt);
            } else if (c == 4) {
                bundle = C95664e9.A05(parcel, readInt);
            } else if (c != 5) {
                C95664e9.A0D(parcel, readInt);
            } else {
                bArr = C95664e9.A0I(parcel, readInt);
            }
        }
        C95664e9.A0C(parcel, A01);
        return new C78453ow(uri, bundle, bArr);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new C78453ow[i];
    }
}
