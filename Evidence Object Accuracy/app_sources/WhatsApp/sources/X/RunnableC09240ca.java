package X;

import android.widget.ListView;
import androidx.fragment.app.ListFragment;

/* renamed from: X.0ca  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09240ca implements Runnable {
    public final /* synthetic */ ListFragment A00;

    public RunnableC09240ca(ListFragment listFragment) {
        this.A00 = listFragment;
    }

    @Override // java.lang.Runnable
    public void run() {
        ListView listView = this.A00.A04;
        listView.focusableViewAvailable(listView);
    }
}
