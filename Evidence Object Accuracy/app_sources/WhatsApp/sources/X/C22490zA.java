package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0zA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22490zA {
    public volatile AtomicReference A00;

    public C22490zA(C21820y2 r4) {
        this.A00 = new AtomicReference(new AnonymousClass1m8(null, r4, false));
    }

    public AnonymousClass1m8 A00() {
        Object obj = this.A00.get();
        AnonymousClass009.A05(obj);
        return (AnonymousClass1m8) obj;
    }
}
