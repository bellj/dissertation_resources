package X;

import java.util.ArrayList;

/* renamed from: X.3Or  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66723Or implements AnonymousClass07L {
    public final /* synthetic */ AbstractActivityC36551k4 A00;

    @Override // X.AnonymousClass07L
    public boolean AUY(String str) {
        return false;
    }

    public C66723Or(AbstractActivityC36551k4 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass07L
    public boolean AUX(String str) {
        AbstractActivityC36551k4 r1 = this.A00;
        r1.A0H = str;
        ArrayList A02 = C32751cg.A02(((ActivityC13830kP) r1).A01, str);
        r1.A0I = A02;
        if (A02.isEmpty()) {
            r1.A0I = null;
        }
        AbstractActivityC36551k4.A02(r1);
        return false;
    }
}
