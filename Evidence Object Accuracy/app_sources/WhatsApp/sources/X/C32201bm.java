package X;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1bm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32201bm {
    public static final AnonymousClass01H A00 = new AnonymousClass01H() { // from class: X.5B6
        @Override // X.AnonymousClass01H
        public final Object get() {
            Integer[] numArr = new Integer[58];
            C12960it.A1O(numArr, 28);
            C12980iv.A1T(numArr, 4);
            C12990iw.A1V(numArr, 7);
            numArr[3] = 10;
            C12960it.A1P(numArr, 12, 4);
            numArr[5] = 14;
            numArr[6] = 18;
            C12960it.A1P(numArr, 67, 7);
            numArr[8] = 20;
            numArr[9] = 79;
            C12960it.A1P(numArr, 5, 10);
            numArr[11] = 51;
            C12960it.A1P(numArr, 52, 12);
            numArr[13] = 13;
            C12960it.A1P(numArr, 22, 14);
            numArr[15] = 23;
            numArr[16] = 24;
            numArr[17] = 25;
            C12960it.A1P(numArr, 26, 18);
            numArr[19] = 34;
            C12960it.A1P(numArr, 35, 20);
            numArr[21] = 36;
            C12960it.A1P(numArr, 46, 22);
            C12960it.A1P(numArr, 47, 23);
            C12960it.A1P(numArr, 48, 24);
            C12960it.A1P(numArr, 49, 25);
            C12960it.A1P(numArr, 50, 26);
            numArr[27] = 55;
            C12960it.A1P(numArr, 37, 28);
            numArr[29] = 39;
            numArr[30] = 40;
            numArr[31] = 41;
            numArr[32] = 42;
            numArr[33] = 43;
            C12960it.A1P(numArr, 44, 34);
            C12960it.A1P(numArr, 45, 35);
            C12960it.A1P(numArr, 56, 36);
            numArr[37] = 57;
            numArr[38] = 59;
            numArr[39] = 60;
            numArr[40] = 61;
            numArr[41] = 69;
            numArr[42] = 62;
            numArr[43] = 63;
            numArr[44] = 64;
            numArr[45] = 65;
            C12960it.A1P(numArr, 66, 46);
            C12960it.A1P(numArr, 68, 47);
            numArr[48] = 71;
            numArr[49] = 75;
            numArr[50] = 76;
            C12960it.A1P(numArr, 77, 51);
            C12960it.A1P(numArr, 78, 52);
            numArr[53] = 86;
            numArr[54] = 87;
            numArr[55] = 90;
            numArr[56] = 93;
            return C12970iu.A13(94, numArr, 57);
        }
    };
    public static final Set A01 = Collections.unmodifiableSet(new HashSet(Arrays.asList(37, 39, 40, 44, 41, 42, 64, 65, 66)));

    public static boolean A00(int i) {
        return i == -2 || i == -1 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 7 || i == 8 || i == 9 || i == 11 || i == 12 || i == 13 || i == 14 || i == 15 || i == 16 || i == 17 || i == 20 || i == 21 || i == 27 || i == 29 || i == 30 || i == 31 || i == 32 || i == 33 || i == 51 || i == 52 || i == 53 || i == 54 || i == 56 || i == 73 || i == 74 || i == 79 || i == 81 || i == 82 || i == 83 || i == 84 || i == 85 || i == 86 || i == 90 || i == 91 || i == 92 || i == 93 || i == 94;
    }

    public static boolean A01(int i) {
        return i == 22 || i == 23 || i == 24 || i == 25 || i == 26 || i == 34 || i == 35 || i == 36 || i == 46 || i == 47 || i == 48 || i == 49 || i == 50 || i == 55;
    }
}
