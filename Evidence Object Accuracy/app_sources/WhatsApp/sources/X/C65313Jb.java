package X;

import android.animation.ValueAnimator;
import com.google.android.material.textfield.TextInputLayout;

/* renamed from: X.3Jb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65313Jb implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ TextInputLayout A00;

    public C65313Jb(TextInputLayout textInputLayout) {
        this.A00 = textInputLayout;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.A00.A0r.A04(C12960it.A00(valueAnimator));
    }
}
