package X;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/* renamed from: X.5pb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124475pb extends InputStream {
    public ByteArrayInputStream A00;
    public boolean A01;
    public final InputStream A02;
    public final AnonymousClass5GB A03;

    public C124475pb(InputStream inputStream, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5) {
        AnonymousClass5GB r3 = new AnonymousClass5GB(new C71653dH());
        this.A03 = r3;
        this.A02 = inputStream;
        byte[] A05 = C16050oM.A05(new byte[]{4}, bArr, new byte[]{bArr5 != null ? (byte) 2 : 0}, bArr2);
        this.A00 = new ByteArrayInputStream(bArr5 != null ? C16050oM.A05(A05, bArr5) : A05);
        r3.AIf(new C113035Ft(new AnonymousClass20K(bArr3), bArr4, bArr5, 128), true);
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A02.close();
    }

    @Override // java.io.InputStream
    public int read() {
        byte[] bArr = new byte[1];
        while (true) {
            int read = read(bArr, 0, 1);
            if (read == -1) {
                return -1;
            }
            if (read != 0) {
                return bArr[0];
            }
            Thread.yield();
        }
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        ByteArrayInputStream byteArrayInputStream = this.A00;
        if (byteArrayInputStream != null) {
            int read = byteArrayInputStream.read(bArr, i, i2);
            if (read > 0) {
                return read;
            }
            this.A00 = null;
            if (this.A01) {
                return -1;
            }
        }
        byte[] bArr2 = new byte[i2];
        int read2 = this.A02.read(bArr2, 0, i2);
        if (read2 == -1) {
            AnonymousClass5GB r1 = this.A03;
            byte[] bArr3 = new byte[r1.AEp(0)];
            try {
                ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(bArr3, 0, r1.A97(bArr3, 0));
                this.A00 = byteArrayInputStream2;
                this.A01 = true;
                return byteArrayInputStream2.read(bArr, i, i2);
            } catch (C114965Nt unused) {
                throw new AssertionError("Encryption failed.");
            }
        } else if (read2 <= 0) {
            return read2;
        } else {
            byte[] bArr4 = new byte[read2];
            int AZZ = this.A03.AZZ(bArr2, 0, read2, bArr4, 0);
            if (AZZ <= i2) {
                System.arraycopy(bArr4, 0, bArr, i, AZZ);
                return AZZ;
            }
            System.arraycopy(bArr4, 0, bArr, i, i2);
            this.A00 = new ByteArrayInputStream(bArr4, i2, AZZ - i2);
            return i2;
        }
    }
}
