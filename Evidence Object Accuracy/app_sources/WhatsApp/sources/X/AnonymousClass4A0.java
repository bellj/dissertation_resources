package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4A0  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4A0 extends Enum {
    public static final /* synthetic */ AnonymousClass4A0[] A00;
    public static final AnonymousClass4A0 A01;
    public static final AnonymousClass4A0 A02;

    public static AnonymousClass4A0 valueOf(String str) {
        return (AnonymousClass4A0) Enum.valueOf(AnonymousClass4A0.class, str);
    }

    public static AnonymousClass4A0[] values() {
        return (AnonymousClass4A0[]) A00.clone();
    }

    static {
        AnonymousClass4A0 r3 = new AnonymousClass4A0("CATALOG_CATEGORY_FLOW", 0);
        A01 = r3;
        AnonymousClass4A0 r1 = new AnonymousClass4A0("CATALOG_SEARCH_FLOW", 1);
        A02 = r1;
        AnonymousClass4A0[] r0 = new AnonymousClass4A0[2];
        C72453ed.A1J(r3, r1, r0);
        A00 = r0;
    }

    public AnonymousClass4A0(String str, int i) {
    }
}
