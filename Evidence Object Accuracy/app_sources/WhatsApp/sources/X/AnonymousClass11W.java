package X;

import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import com.facebook.redex.RunnableBRunnable0Shape0S0200100_I0;
import com.whatsapp.notification.MessageNotificationDismissedReceiver;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.11W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass11W implements AnonymousClass11U {
    public final Context A00;
    public final AnonymousClass11V A01;
    public final AbstractC14440lR A02;
    public final Map A03 = new HashMap();
    public final AtomicBoolean A04 = new AtomicBoolean();

    public AnonymousClass11W(C16590pI r2, AnonymousClass11V r3, AbstractC14440lR r4) {
        this.A01 = r3;
        this.A02 = r4;
        this.A00 = r2.A00;
    }

    @Override // X.AnonymousClass11U
    public PendingIntent A8F(C15370n3 r6, AbstractC15340mz r7) {
        Context context = this.A00;
        Intent intent = new Intent(context, MessageNotificationDismissedReceiver.class);
        intent.setData(ContentUris.withAppendedId(C42961wB.A00, r6.A08()));
        intent.putExtra("last_message_time", r7.A0I);
        intent.putExtra("chat_jid", C15380n4.A03(r7.A0z.A00));
        return AnonymousClass1UY.A01(context, 1, intent, 134217728);
    }

    @Override // X.AnonymousClass11U
    public void AHy(Intent intent) {
        String stringExtra = intent.getStringExtra("chat_jid");
        long longExtra = intent.getLongExtra("last_message_time", -1);
        try {
            AbstractC14640lm A00 = AbstractC14640lm.A00(stringExtra);
            this.A03.put(A00, Long.valueOf(longExtra));
            this.A02.Ab2(new RunnableBRunnable0Shape0S0200100_I0(this, A00, 6, longExtra));
        } catch (AnonymousClass1MW unused) {
            Log.e("messagenotificationdismisshelper/handleDismissIntent: Invalid Jid stored in intent");
        }
    }

    @Override // X.AnonymousClass11U
    public boolean AdL(AbstractC15340mz r10) {
        if (this.A04.compareAndSet(false, true)) {
            AnonymousClass11V r6 = this.A01;
            C14830m7 r7 = r6.A00;
            String[] strArr = {Long.toString(r7.A00() - 604800000)};
            C232010t r62 = r6.A01;
            C16310on A02 = r62.A02();
            try {
                A02.A03.A01("dismissed_chat", "timestamp < ?", strArr);
                A02.close();
                ArrayList arrayList = new ArrayList();
                String[] strArr2 = {Long.toString(r7.A00() - 604800000)};
                A02 = r62.get();
                try {
                    Cursor A09 = A02.A03.A09("SELECT chat_jid, timestamp FROM dismissed_chat WHERE timestamp > ?", strArr2);
                    int columnIndexOrThrow = A09.getColumnIndexOrThrow("chat_jid");
                    int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("timestamp");
                    while (A09.moveToNext()) {
                        AbstractC14640lm A01 = AbstractC14640lm.A01(A09.getString(columnIndexOrThrow));
                        long j = A09.getLong(columnIndexOrThrow2);
                        if (A01 != null) {
                            arrayList.add(new AnonymousClass01T(A01, Long.valueOf(j)));
                        }
                    }
                    A09.close();
                    A02.close();
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        AnonymousClass01T r0 = (AnonymousClass01T) it.next();
                        this.A03.put(r0.A00, r0.A01);
                    }
                } finally {
                }
            } finally {
            }
        }
        AbstractC14640lm r2 = r10.A0z.A00;
        Map map = this.A03;
        if (!map.containsKey(r2) || ((Number) map.get(r2)).longValue() < r10.A0I) {
            return false;
        }
        return true;
    }
}
