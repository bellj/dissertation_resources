package X;

import android.util.Base64;
import com.whatsapp.util.Log;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5wk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128835wk {
    public final String A00;
    public final C130495zV A01;

    public C128835wk(String str, C130495zV r2) {
        this.A01 = r2;
        this.A00 = str;
    }

    public AnonymousClass6F0 A00(String str, String str2, String str3, String str4, JSONObject jSONObject) {
        C130495zV r8;
        String str5;
        try {
            String string = jSONObject.getString("txnId");
            String string2 = jSONObject.getString("credential");
            Matcher matcher = Pattern.compile("\\{([^}]*)\\}").matcher(str);
            StringBuffer stringBuffer = new StringBuffer();
            AnonymousClass6F0 r3 = null;
            if (matcher.find()) {
                String group = matcher.group();
                String substring = group.substring(1, group.length() - 1);
                try {
                    r8 = this.A01;
                    str5 = this.A00;
                } catch (C124795q8 e) {
                    Log.e(C12960it.A0d(e.toString(), C12960it.A0k("PAY: ")));
                }
                if (str5 == null || !str5.isEmpty()) {
                    ArrayList A0l = C12960it.A0l();
                    for (C127545uf r1 : C130495zV.A01) {
                        if (r1.A00.equals(str5)) {
                            A0l.add(r1);
                        }
                    }
                    if (A0l.size() != 0) {
                        C127545uf r7 = (C127545uf) A0l.get(new SecureRandom().nextInt(A0l.size()));
                        r8.A00 = r7.A02;
                        StringBuilder sb = new StringBuilder(500);
                        try {
                            String A0n = C117305Zk.A0n(AnonymousClass61J.A04(AnonymousClass61J.A02(substring), AnonymousClass61J.A01(str4)));
                            C117325Zm.A08(string2, "|", string, sb);
                            sb.append("|");
                            sb.append(A0n);
                            try {
                                byte[] bytes = sb.toString().getBytes();
                                PublicKey A0p = C117305Zk.A0p(Base64.decode(r8.A00.getBytes("utf-8"), 2));
                                Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                instance.init(1, A0p);
                                AnonymousClass6F0 r0 = new AnonymousClass6F0(new AnonymousClass6F1(r7.A01, r7.A00, C117305Zk.A0n(instance.doFinal(bytes))));
                                r3 = r0;
                                r0.type = str2;
                                r0.subType = str3;
                                AnonymousClass6F1 r2 = r0.data;
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append("2.0|");
                                r2.encryptedBase64String = C12960it.A0d(r2.encryptedBase64String, A0h);
                                matcher.appendReplacement(stringBuffer, Matcher.quoteReplacement(r3.data.encryptedBase64String.replaceAll("\n", "")));
                            } catch (Exception e2) {
                                throw C117315Zl.A0J(e2);
                            }
                        } catch (Exception unused) {
                            throw new C124795q8(EnumC124485pc.A04);
                        }
                    } else {
                        throw new C124795q8(EnumC124485pc.A01);
                    }
                } else {
                    throw new C124795q8(EnumC124485pc.A00);
                }
            }
            if (stringBuffer.length() > 0) {
                matcher.appendTail(stringBuffer);
            }
            if (r3 != null) {
                r3.data.encryptedBase64String = stringBuffer.toString();
            }
            return r3;
        } catch (JSONException e3) {
            throw C117315Zl.A0J(e3);
        }
    }
}
