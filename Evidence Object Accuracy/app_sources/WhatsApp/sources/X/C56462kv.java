package X;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.msys.mci.DefaultCrypto;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.2kv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56462kv extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99004jb();
    public String A00;

    public C56462kv(String str) {
        C13020j0.A02(str, "json must not be null");
        this.A00 = str;
    }

    public static C56462kv A00(Context context, int i) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
            while (true) {
                int read = openRawResource.read(bArr, 0, EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    try {
                        break;
                    } catch (IOException unused) {
                    }
                }
            }
            openRawResource.close();
            try {
                byteArrayOutputStream.close();
            } catch (IOException unused2) {
            }
            return new C56462kv(new String(byteArrayOutputStream.toByteArray(), DefaultCrypto.UTF_8));
        } catch (IOException e) {
            String valueOf = String.valueOf(e);
            StringBuilder A0t = C12980iv.A0t(valueOf.length() + 37);
            A0t.append("Failed to read resource ");
            A0t.append(i);
            A0t.append(": ");
            throw new Resources.NotFoundException(C12960it.A0d(valueOf, A0t));
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int A01 = C95654e8.A01(parcel);
        C95654e8.A0D(parcel, this.A00, 2, false);
        C95654e8.A06(parcel, A01);
    }
}
