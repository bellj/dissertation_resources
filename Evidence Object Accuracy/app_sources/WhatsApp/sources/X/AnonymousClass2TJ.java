package X;

import java.util.List;

/* renamed from: X.2TJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2TJ implements AnonymousClass2TK {
    public final AnonymousClass2FO A00;
    public final List A01;

    public AnonymousClass2TJ(AnonymousClass2FO r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // X.AnonymousClass2TK
    public AbstractC35581iK A8O(boolean z) {
        AnonymousClass2JH r3;
        if (!z) {
            r3 = new AnonymousClass2JH();
            r3.A04 = true;
        } else {
            r3 = new AnonymousClass2JH();
            r3.A01 = 2;
            r3.A00 = 7;
            r3.A02 = 2;
            r3.A03 = null;
        }
        return new C68843Wy(this.A00.A00(r3), this.A01);
    }
}
