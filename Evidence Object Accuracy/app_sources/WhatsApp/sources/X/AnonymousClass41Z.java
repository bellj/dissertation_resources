package X;

import com.whatsapp.group.GroupAdminPickerActivity;

/* renamed from: X.41Z  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass41Z extends AnonymousClass2Dn {
    public final /* synthetic */ GroupAdminPickerActivity A00;

    public AnonymousClass41Z(GroupAdminPickerActivity groupAdminPickerActivity) {
        this.A00 = groupAdminPickerActivity;
    }

    @Override // X.AnonymousClass2Dn
    public void A00(AbstractC14640lm r3) {
        GroupAdminPickerActivity groupAdminPickerActivity = this.A00;
        groupAdminPickerActivity.A2h(groupAdminPickerActivity.A0N);
    }
}
