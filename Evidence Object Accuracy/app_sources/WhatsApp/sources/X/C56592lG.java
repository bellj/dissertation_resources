package X;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/* renamed from: X.2lG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C56592lG extends AbstractC15040mS {
    public Future A00;
    public volatile String A01;

    public C56592lG(C14160kx r1) {
        super(r1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x004c A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String A00(X.C56592lG r7) {
        /*
            java.lang.String r6 = "0"
            java.lang.String r3 = "Failed to close clientId writing stream"
            java.lang.String r1 = X.C12990iw.A0n()
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r5 = r1.toLowerCase(r0)
            X.0kx r0 = r7.A00     // Catch: Exception -> 0x0063
            X.0ky r0 = r0.A03     // Catch: Exception -> 0x0063
            X.C13020j0.A01(r0)     // Catch: Exception -> 0x0063
            android.content.Context r4 = r0.A01     // Catch: Exception -> 0x0063
            X.C13020j0.A05(r5)     // Catch: Exception -> 0x0063
            java.lang.String r0 = "ClientId should be saved from worker thread"
            X.C13020j0.A06(r0)     // Catch: Exception -> 0x0063
            r2 = 0
            java.lang.String r0 = "Storing clientId"
            r7.A0D(r0, r5)     // Catch: FileNotFoundException -> 0x0044, IOException -> 0x003d, all -> 0x0057
            java.lang.String r1 = "gaClientId"
            r0 = 0
            java.io.FileOutputStream r2 = r4.openFileOutput(r1, r0)     // Catch: FileNotFoundException -> 0x0044, IOException -> 0x003d, all -> 0x0057
            byte[] r0 = r5.getBytes()     // Catch: FileNotFoundException -> 0x0044, IOException -> 0x003d, all -> 0x0057
            r2.write(r0)     // Catch: FileNotFoundException -> 0x0044, IOException -> 0x003d, all -> 0x0057
            r2.close()     // Catch: IOException -> 0x0037, Exception -> 0x0063
            goto L_0x003c
        L_0x0037:
            r0 = move-exception
            r7.A0C(r3, r0)     // Catch: Exception -> 0x0063
            return r5
        L_0x003c:
            return r5
        L_0x003d:
            r1 = move-exception
            java.lang.String r0 = "Error writing to clientId file"
            r7.A0C(r0, r1)     // Catch: all -> 0x0057
            goto L_0x004a
        L_0x0044:
            r1 = move-exception
            java.lang.String r0 = "Error creating clientId file"
            r7.A0C(r0, r1)     // Catch: all -> 0x0057
        L_0x004a:
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch: IOException -> 0x0050, Exception -> 0x0063
            goto L_0x0055
        L_0x0050:
            r0 = move-exception
            r7.A0C(r3, r0)     // Catch: Exception -> 0x0063
            goto L_0x0056
        L_0x0055:
            return r6
        L_0x0056:
            return r6
        L_0x0057:
            r1 = move-exception
            if (r2 == 0) goto L_0x0062
            r2.close()     // Catch: IOException -> 0x005e, Exception -> 0x0063
            goto L_0x0062
        L_0x005e:
            r0 = move-exception
            r7.A0C(r3, r0)     // Catch: Exception -> 0x0063
        L_0x0062:
            throw r1     // Catch: Exception -> 0x0063
        L_0x0063:
            r1 = move-exception
            java.lang.String r0 = "Error saving clientId file"
            r7.A0C(r0, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C56592lG.A00(X.2lG):java.lang.String");
    }

    public final String A0H() {
        String str;
        FutureTask futureTask;
        A0G();
        synchronized (this) {
            if (this.A01 == null) {
                C14170ky r2 = ((C15050mT) this).A00.A03;
                C13020j0.A01(r2);
                CallableC71383cq r1 = new CallableC71383cq(this);
                if (Thread.currentThread() instanceof AnonymousClass5HA) {
                    FutureTask futureTask2 = new FutureTask(r1);
                    futureTask2.run();
                    futureTask = futureTask2;
                } else {
                    futureTask = r2.A03.submit(r1);
                }
                this.A00 = futureTask;
            }
            Future future = this.A00;
            if (future != null) {
                try {
                    this.A01 = (String) future.get();
                } catch (InterruptedException e) {
                    A0E("ClientId loading or generation was interrupted", e);
                    this.A01 = "0";
                } catch (ExecutionException e2) {
                    A0C("Failed to load or generate client id", e2);
                    this.A01 = "0";
                }
                if (this.A01 == null) {
                    this.A01 = "0";
                }
                A0D("Loaded clientId", this.A01);
                this.A00 = null;
            }
            str = this.A01;
        }
        return str;
    }
}
