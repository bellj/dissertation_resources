package X;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;

/* renamed from: X.38P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass38P extends AbstractC16350or {
    public int A00;
    public ContentResolver A01;
    public Bitmap A02;
    public Bitmap A03;
    public Uri A04;

    public AnonymousClass38P(ActivityC000900k r1) {
        super(r1);
    }
}
