package X;

/* renamed from: X.3UY  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3UY implements AnonymousClass5TH {
    public final /* synthetic */ ActivityC13810kN A00;
    public final /* synthetic */ C18120rw A01;
    public final /* synthetic */ C235812f A02;

    public AnonymousClass3UY(ActivityC13810kN r1, C18120rw r2, C235812f r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0048, code lost:
        if (r6 != 9) goto L_0x0025;
     */
    @Override // X.AnonymousClass5TH
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AOM(int r6) {
        /*
            r5 = this;
            X.0kN r3 = r5.A00
            r3.AaN()
            r0 = 5
            if (r6 != r0) goto L_0x0013
            X.12f r2 = r5.A02
            X.C16700pc.A0B(r2)
            r1 = 0
            r0 = 4
            r2.A03(r1, r1, r1, r0)
            return
        L_0x0013:
            X.12f r4 = r5.A02
            X.C16700pc.A0B(r4)
            r0 = 7
            if (r6 != r0) goto L_0x0033
            r2 = 2131886376(0x7f120128, float:1.940733E38)
            r1 = 0
            java.lang.Object[] r0 = new java.lang.Object[r1]
            r3.Adr(r0, r1, r2)
        L_0x0024:
            r1 = 0
        L_0x0025:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r1)
            java.lang.String r2 = X.AnonymousClass4EW.A00(r6)
            r1 = 0
            r0 = 6
            r4.A03(r3, r1, r2, r0)
            return
        L_0x0033:
            r0 = 2131886375(0x7f120127, float:1.9407327E38)
            r3.Ado(r0)
            r1 = 2
            if (r6 == r1) goto L_0x004b
            r0 = 3
            if (r6 == r0) goto L_0x004b
            r0 = 7
            if (r6 == r0) goto L_0x0024
            r0 = 8
            if (r6 == r0) goto L_0x004b
            r0 = 9
            if (r6 == r0) goto L_0x004b
            goto L_0x0025
        L_0x004b:
            r1 = 1
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3UY.AOM(int):void");
    }
}
