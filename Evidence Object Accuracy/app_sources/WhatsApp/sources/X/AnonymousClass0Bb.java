package X;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

/* renamed from: X.0Bb  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Bb extends FrameLayout {
    public static final Property A06;
    public static final Property A07;
    public static final Interpolator A08 = AnonymousClass0L1.A00(0.17f, 0.17f, 0.0f, 1.0f);
    public EnumC03850Jj A00 = EnumC03850Jj.DEFAULT;
    public final Animator.AnimatorListener A01;
    public final ObjectAnimator A02;
    public final ObjectAnimator A03;
    public final ObjectAnimator A04;
    public final ObjectAnimator A05;

    static {
        Class cls = Float.TYPE;
        A07 = new C02240Ao(cls);
        A06 = new C02250Ap(cls);
    }

    public AnonymousClass0Bb(Context context) {
        super(context);
        AnonymousClass095 r2 = new AnonymousClass095(this);
        this.A01 = r2;
        ObjectAnimator objectAnimator = new ObjectAnimator();
        Interpolator interpolator = A08;
        objectAnimator.setInterpolator(interpolator);
        objectAnimator.addListener(r2);
        this.A04 = objectAnimator;
        ObjectAnimator objectAnimator2 = new ObjectAnimator();
        objectAnimator2.setInterpolator(interpolator);
        this.A05 = objectAnimator2;
        ObjectAnimator objectAnimator3 = new ObjectAnimator();
        objectAnimator3.setInterpolator(interpolator);
        objectAnimator3.addListener(r2);
        this.A02 = objectAnimator3;
        ObjectAnimator objectAnimator4 = new ObjectAnimator();
        objectAnimator4.setInterpolator(interpolator);
        this.A03 = objectAnimator4;
        A01();
    }

    public static boolean A00(Context context) {
        return 1 == context.getResources().getConfiguration().getLayoutDirection();
    }

    public final void A01() {
        boolean z;
        A02(280, 200);
        Context context = getContext();
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (Build.VERSION.SDK_INT < 17 || (applicationInfo.flags & 4194304) == 0) {
            z = false;
        } else {
            z = A00(context);
        }
        ObjectAnimator objectAnimator = this.A04;
        float f = -1.0f;
        if (objectAnimator != null) {
            objectAnimator.setProperty(A07);
            float[] fArr = new float[2];
            float f2 = 1.0f;
            if (z) {
                f2 = -1.0f;
            }
            fArr[0] = f2;
            fArr[1] = 0.0f;
            objectAnimator.setFloatValues(fArr);
        }
        ObjectAnimator objectAnimator2 = this.A05;
        if (objectAnimator2 != null) {
            objectAnimator2.setProperty(A07);
            float[] fArr2 = new float[2];
            fArr2[0] = 0.0f;
            float f3 = -1.0f;
            if (z) {
                f3 = 1.0f;
            }
            fArr2[1] = f3;
            objectAnimator2.setFloatValues(fArr2);
        }
        ObjectAnimator objectAnimator3 = this.A02;
        if (objectAnimator3 != null) {
            objectAnimator3.setProperty(A07);
            float[] fArr3 = new float[2];
            float f4 = -1.0f;
            if (z) {
                f4 = 1.0f;
            }
            fArr3[0] = f4;
            fArr3[1] = 0.0f;
            objectAnimator3.setFloatValues(fArr3);
        }
        ObjectAnimator objectAnimator4 = this.A03;
        if (objectAnimator4 != null) {
            objectAnimator4.setProperty(A07);
            float[] fArr4 = new float[2];
            fArr4[0] = 0.0f;
            if (!z) {
                f = 1.0f;
            }
            fArr4[1] = f;
            objectAnimator4.setFloatValues(fArr4);
        }
    }

    public final void A02(long j, long j2) {
        ObjectAnimator objectAnimator = this.A04;
        if (objectAnimator != null) {
            objectAnimator.setDuration(j);
        }
        ObjectAnimator objectAnimator2 = this.A05;
        if (objectAnimator2 != null) {
            objectAnimator2.setDuration(j2);
        }
        ObjectAnimator objectAnimator3 = this.A02;
        if (objectAnimator3 != null) {
            objectAnimator3.setDuration(j);
        }
        ObjectAnimator objectAnimator4 = this.A03;
        if (objectAnimator4 != null) {
            objectAnimator4.setDuration(j2);
        }
    }

    public final void A03(View view, EnumC03850Jj r10, boolean z) {
        ObjectAnimator objectAnimator;
        ObjectAnimator objectAnimator2;
        if (!(r10 == null || this.A00 == r10)) {
            switch (r10.ordinal()) {
                case 1:
                    A02(250, 250);
                    ObjectAnimator objectAnimator3 = this.A04;
                    if (objectAnimator3 != null) {
                        objectAnimator3.setProperty(A06);
                        objectAnimator3.setFloatValues(0.0f, 1.0f);
                    }
                    ObjectAnimator objectAnimator4 = this.A05;
                    if (objectAnimator4 != null) {
                        objectAnimator4.setProperty(A06);
                        objectAnimator4.setFloatValues(1.0f, 0.0f);
                    }
                    ObjectAnimator objectAnimator5 = this.A02;
                    if (objectAnimator5 != null) {
                        objectAnimator5.setProperty(A06);
                        objectAnimator5.setFloatValues(0.0f, 1.0f);
                    }
                    ObjectAnimator objectAnimator6 = this.A03;
                    if (objectAnimator6 != null) {
                        objectAnimator6.setProperty(A06);
                        objectAnimator6.setFloatValues(1.0f, 0.0f);
                        break;
                    }
                    break;
                case 2:
                    A02(0, 0);
                    break;
                default:
                    A01();
                    break;
            }
            this.A00 = r10;
        }
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        }
        addView(view, new ViewGroup.LayoutParams(-1, -1));
        int childCount = getChildCount();
        if (z) {
            objectAnimator = this.A04;
            objectAnimator2 = this.A05;
        } else {
            objectAnimator = this.A02;
            objectAnimator2 = this.A03;
        }
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt == view) {
                childAt.setVisibility(0);
                if (childCount > 1 && objectAnimator != null) {
                    if (objectAnimator.isStarted()) {
                        objectAnimator.cancel();
                    }
                    objectAnimator.setTarget(childAt);
                    objectAnimator.start();
                }
            } else if (childAt.getVisibility() == 0) {
                if (objectAnimator2 != null) {
                    if (objectAnimator2.isStarted()) {
                        objectAnimator2.cancel();
                    }
                    objectAnimator2.setTarget(childAt);
                    objectAnimator2.start();
                } else {
                    removeView(childAt);
                }
            }
        }
    }

    public View getPrimaryChild() {
        int childCount = getChildCount();
        if (childCount == 0) {
            return null;
        }
        return getChildAt(childCount - 1);
    }
}
