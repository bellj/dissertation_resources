package X;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;

/* renamed from: X.5Na  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC114775Na extends AnonymousClass1TL implements AbstractC117215Yz {
    public AnonymousClass1TN[] A00;

    public AbstractC114775Na() {
        this.A00 = C94954co.A03;
    }

    public AbstractC114775Na(C94954co r2) {
        this.A00 = r2.A07();
    }

    public AbstractC114775Na(AnonymousClass1TN[] r2, boolean z) {
        this.A00 = r2;
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A06() {
        return new AnonymousClass5NZ(this.A00, false);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return true;
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return new AnonymousClass5DB(this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        int A0B = A0B();
        if (A0B == 0) {
            return "[]";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('[');
        int i = 0;
        while (true) {
            stringBuffer.append(this.A00[i]);
            i++;
            if (i >= A0B) {
                stringBuffer.append(']');
                return stringBuffer.toString();
            }
            stringBuffer.append(", ");
        }
    }

    public AbstractC114775Na(AnonymousClass1TN r3) {
        if (r3 != null) {
            this.A00 = new AnonymousClass1TN[]{r3};
            return;
        }
        throw C12980iv.A0n("'element' cannot be null");
    }

    public AbstractC114775Na(AnonymousClass1TN[] r4) {
        if (r4 != null) {
            for (AnonymousClass1TN r0 : r4) {
                if (r0 != null) {
                }
            }
            this.A00 = C94954co.A04(r4);
            return;
        }
        throw C12980iv.A0n("'elements' cannot be null, or contain null");
    }

    public static AnonymousClass1TN A00(AbstractC114775Na r1) {
        return r1.A0D(0);
    }

    public static AnonymousClass1TN A01(AbstractC114775Na r1) {
        return r1.A0D(1);
    }

    public static AbstractC114775Na A04(Object obj) {
        if (obj == null || (obj instanceof AbstractC114775Na)) {
            return (AbstractC114775Na) obj;
        }
        if (obj instanceof AbstractC117235Zb) {
            return A04((Object) ((AnonymousClass1TN) obj).Aer());
        }
        if (obj instanceof byte[]) {
            try {
                return A04((Object) AnonymousClass1TL.A03((byte[]) obj));
            } catch (IOException e) {
                throw C12970iu.A0f(C12960it.A0d(e.getMessage(), C12960it.A0k("failed to construct sequence from byte[]: ")));
            }
        } else {
            if (obj instanceof AnonymousClass1TN) {
                AnonymousClass1TL Aer = ((AnonymousClass1TN) obj).Aer();
                if (Aer instanceof AbstractC114775Na) {
                    return (AbstractC114775Na) Aer;
                }
            }
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(obj), C12960it.A0k("unknown object in getInstance: ")));
        }
    }

    public static AbstractC114775Na A05(AnonymousClass5NU r2, boolean z) {
        if (!z) {
            AnonymousClass1TL A00 = AnonymousClass5NU.A00(r2);
            if (r2.A02) {
                return r2 instanceof C114815Ne ? new AnonymousClass5NX(A00) : new AnonymousClass5NY(A00);
            }
            if (A00 instanceof AbstractC114775Na) {
                AbstractC114775Na r1 = (AbstractC114775Na) A00;
                return r2 instanceof C114815Ne ? r1 : (AbstractC114775Na) r1.A07();
            }
            throw C12970iu.A0f(C12960it.A0d(C12980iv.A0s(r2), C12960it.A0k("unknown object in getInstance: ")));
        } else if (r2.A02) {
            return A04((Object) AnonymousClass5NU.A00(r2));
        } else {
            throw C12970iu.A0f("object implicit - explicit expected.");
        }
    }

    @Override // X.AnonymousClass1TL
    public AnonymousClass1TL A07() {
        AnonymousClass5NY r2;
        if (!(this instanceof AnonymousClass5NW)) {
            return ((this instanceof AnonymousClass5NY) || (this instanceof AnonymousClass5NZ)) ? this : new AnonymousClass5NY(this.A00);
        }
        AnonymousClass5NW r1 = (AnonymousClass5NW) this;
        synchronized (r1) {
            r1.A0E();
            r2 = new AnonymousClass5NY(((AbstractC114775Na) r1).A00);
        }
        return r2;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r10, boolean z) {
        if (this instanceof AnonymousClass5NW) {
            AnonymousClass5NW r2 = (AnonymousClass5NW) this;
            synchronized (r2) {
                byte[] bArr = r2.A00;
                if (bArr != null) {
                    r10.A06(bArr, 48, z);
                } else {
                    new AnonymousClass5NY(((AbstractC114775Na) r2).A00).A08(r10, z);
                }
            }
        } else if (this instanceof AnonymousClass5NY) {
            AnonymousClass5NY r7 = (AnonymousClass5NY) this;
            if (z) {
                r10.A00.write(48);
            }
            AnonymousClass1TP A00 = r10.A00();
            int length = ((AbstractC114775Na) r7).A00.length;
            int i = r7.A00;
            int i2 = 0;
            if (i < 0) {
                if (length > 16) {
                    i = 0;
                    for (int i3 = 0; i3 < length; i3++) {
                        i = C72453ed.A0S(((AbstractC114775Na) r7).A00, i3, i);
                    }
                    r7.A00 = i;
                } else {
                    AnonymousClass1TL[] r8 = new AnonymousClass1TL[length];
                    int i4 = 0;
                    for (int i5 = 0; i5 < length; i5++) {
                        AnonymousClass1TL A07 = C72463ee.A0N(((AbstractC114775Na) r7).A00, i5).A07();
                        r8[i5] = A07;
                        i4 += A07.A05();
                    }
                    r7.A00 = i4;
                    r10.A02(i4);
                    while (i2 < length) {
                        A00.A04(r8[i2], true);
                        i2++;
                    }
                    return;
                }
            }
            r10.A02(i);
            while (i2 < length) {
                A00.A04(C72463ee.A0N(((AbstractC114775Na) r7).A00, i2), true);
                i2++;
            }
        } else if (!(this instanceof AnonymousClass5NZ)) {
            r10.A07(this.A00, 48, z);
        } else {
            AnonymousClass5NZ r6 = (AnonymousClass5NZ) this;
            if (z) {
                r10.A00.write(48);
            }
            AnonymousClass5N8 A01 = r10.A01();
            int length2 = ((AbstractC114775Na) r6).A00.length;
            int i6 = r6.A00;
            int i7 = 0;
            if (i6 < 0) {
                if (length2 > 16) {
                    i6 = 0;
                    for (int i8 = 0; i8 < length2; i8++) {
                        i6 = C72453ed.A0R(((AbstractC114775Na) r6).A00, i8, i6);
                    }
                    r6.A00 = i6;
                } else {
                    AnonymousClass1TL[] r82 = new AnonymousClass1TL[length2];
                    int i9 = 0;
                    for (int i10 = 0; i10 < length2; i10++) {
                        AnonymousClass1TL A06 = C72463ee.A0N(((AbstractC114775Na) r6).A00, i10).A06();
                        r82[i10] = A06;
                        i9 += A06.A05();
                    }
                    r6.A00 = i9;
                    r10.A02(i9);
                    while (i7 < length2) {
                        r82[i7].A08(A01, true);
                        i7++;
                    }
                    return;
                }
            }
            r10.A02(i6);
            while (i7 < length2) {
                C72463ee.A0N(((AbstractC114775Na) r6).A00, i7).A06().A08(A01, true);
                i7++;
            }
        }
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r6) {
        if (r6 instanceof AbstractC114775Na) {
            AbstractC114775Na r62 = (AbstractC114775Na) r6;
            int A0B = A0B();
            if (r62.A0B() == A0B) {
                for (int i = 0; i < A0B; i++) {
                    AnonymousClass1TL A0N = C72463ee.A0N(this.A00, i);
                    AnonymousClass1TL A0N2 = C72463ee.A0N(r62.A00, i);
                    if (A0N == A0N2 || A0N.A0A(A0N2)) {
                    }
                }
                return true;
            }
        }
        return false;
    }

    public int A0B() {
        int length;
        if (!(this instanceof AnonymousClass5NW)) {
            return this.A00.length;
        }
        AnonymousClass5NW r1 = (AnonymousClass5NW) this;
        synchronized (r1) {
            r1.A0E();
            length = ((AbstractC114775Na) r1).A00.length;
        }
        return length;
    }

    public Enumeration A0C() {
        if (!(this instanceof AnonymousClass5NW)) {
            return new C112335Cz(this);
        }
        AnonymousClass5NW r1 = (AnonymousClass5NW) this;
        synchronized (r1) {
            byte[] bArr = r1.A00;
            if (bArr != null) {
                return new AnonymousClass5D4(bArr);
            }
            return new C112335Cz(r1);
        }
    }

    public AnonymousClass1TN A0D(int i) {
        AnonymousClass1TN r0;
        if (!(this instanceof AnonymousClass5NW)) {
            return this.A00[i];
        }
        AnonymousClass5NW r1 = (AnonymousClass5NW) this;
        synchronized (r1) {
            r1.A0E();
            r0 = ((AbstractC114775Na) r1).A00[i];
        }
        return r0;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        int length = this.A00.length;
        int i = length + 1;
        while (true) {
            length--;
            if (length < 0) {
                return i;
            }
            i = (i * 257) ^ C72463ee.A0N(this.A00, length).hashCode();
        }
    }
}
