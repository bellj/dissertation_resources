package X;

import android.os.IBinder;

/* renamed from: X.3rV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C79973rV extends C98404id implements AnonymousClass5YM {
    public C79973rV(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.vision.face.internal.client.INativeFaceDetectorCreator");
    }
}
