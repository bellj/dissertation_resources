package X;

/* renamed from: X.1bx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32311bx {
    public static final String A00;
    public static final String A01;

    static {
        StringBuilder sb = new StringBuilder("SELECT ");
        String str = C16500p8.A00;
        sb.append(str);
        sb.append(", ");
        sb.append("link.link_index AS link_index");
        sb.append(" FROM ");
        sb.append("(SELECT message_row_id, link_index FROM message_link)");
        sb.append(" AS link JOIN ");
        sb.append("available_message_view AS message");
        sb.append(" ON message._id = link.message_row_id");
        sb.append(" WHERE ");
        sb.append("chat_row_id = ?");
        A00 = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT ");
        sb2.append(str);
        sb2.append(", ");
        sb2.append("links.link_index AS link_index");
        sb2.append(" FROM ");
        sb2.append("message_view AS message, ");
        sb2.append("(SELECT message_row_id, link_index FROM message_link AS link WHERE link.chat_row_id = ?) links");
        sb2.append(" WHERE ");
        sb2.append("message._id = links.message_row_id");
        sb2.append(" ORDER BY sort_id DESC");
        A01 = sb2.toString();
    }
}
