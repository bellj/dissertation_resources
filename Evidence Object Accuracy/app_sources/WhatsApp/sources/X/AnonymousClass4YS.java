package X;

import sun.misc.Unsafe;

/* renamed from: X.4YS  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4YS {
    public Unsafe A00;

    public AnonymousClass4YS(Unsafe unsafe) {
        this.A00 = unsafe;
    }

    public static byte A00(Object obj, long j) {
        int i;
        boolean z = C95634e6.A04;
        long j2 = -4 & j;
        AnonymousClass4YS r0 = C95634e6.A01;
        if (z) {
            i = r0.A04(obj, j2);
            j ^= -1;
        } else {
            i = r0.A00.getInt(obj, j2);
        }
        return (byte) (i >>> ((int) ((j & 3) << 3)));
    }

    public byte A01(Object obj, long j) {
        return A00(obj, j);
    }

    public double A02(Object obj, long j) {
        return Double.longBitsToDouble(this.A00.getLong(obj, j));
    }

    public float A03(Object obj, long j) {
        return Float.intBitsToFloat(this.A00.getInt(obj, j));
    }

    public final int A04(Object obj, long j) {
        return this.A00.getInt(obj, j);
    }

    public final long A05(Object obj, long j) {
        return this.A00.getLong(obj, j);
    }

    public void A06(Object obj, long j, byte b) {
        if (C95634e6.A04) {
            C95634e6.A07(obj, j, b);
        } else {
            C95634e6.A08(obj, j, b);
        }
    }

    public void A07(Object obj, long j, double d) {
        A0A(obj, j, Double.doubleToLongBits(d));
    }

    public void A08(Object obj, long j, float f) {
        A09(obj, j, Float.floatToIntBits(f));
    }

    public final void A09(Object obj, long j, int i) {
        this.A00.putInt(obj, j, i);
    }

    public final void A0A(Object obj, long j, long j2) {
        this.A00.putLong(obj, j, j2);
    }

    public void A0B(Object obj, long j, boolean z) {
        boolean z2 = C95634e6.A04;
        byte b = z ? (byte) 1 : 0;
        if (z2) {
            C95634e6.A07(obj, j, b);
        } else {
            C95634e6.A08(obj, j, b);
        }
    }

    public boolean A0C(Object obj, long j) {
        return C12960it.A1S(A00(obj, j));
    }
}
