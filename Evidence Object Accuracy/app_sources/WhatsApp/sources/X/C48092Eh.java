package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;

/* renamed from: X.2Eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48092Eh extends AnimatorListenerAdapter {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ View A02;
    public final /* synthetic */ ViewPropertyAnimator A03;
    public final /* synthetic */ AnonymousClass03U A04;
    public final /* synthetic */ C48042Ec A05;

    public C48092Eh(View view, ViewPropertyAnimator viewPropertyAnimator, AnonymousClass03U r3, C48042Ec r4, int i, int i2) {
        this.A05 = r4;
        this.A04 = r3;
        this.A00 = i;
        this.A02 = view;
        this.A01 = i2;
        this.A03 = viewPropertyAnimator;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        if (this.A00 != 0) {
            this.A02.setTranslationX(0.0f);
        }
        if (this.A01 != 0) {
            this.A02.setTranslationY(0.0f);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.A03.setListener(null);
        C48042Ec r2 = this.A05;
        AnonymousClass03U r1 = this.A04;
        r2.A03(r1);
        r2.A05.remove(r1);
        r2.A0H();
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        this.A05.A0C = true;
    }
}
