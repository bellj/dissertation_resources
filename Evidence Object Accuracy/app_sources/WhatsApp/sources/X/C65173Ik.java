package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.ViewOnClickCListenerShape2S0500000_I1;
import com.whatsapp.util.ViewOnClickCListenerShape3S0400000_I1;

/* renamed from: X.3Ik  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65173Ik {
    public static View.OnClickListener A00(Context context, C15570nT r9, C244415n r10, C30341Xa r11, boolean z) {
        UserJid A0C = r11.A0C();
        UserJid userJid = A0C;
        if (r11.A0z.A02) {
            userJid = null;
        }
        r9.A08();
        if (z) {
            return new ViewOnClickCListenerShape3S0400000_I1(context, userJid, r11, r10, 1);
        }
        return new ViewOnClickCListenerShape2S0500000_I1(context, r11, r9, r10, A0C, 0);
    }

    public static String A01(Context context, C15570nT r13, C14830m7 r14, AnonymousClass018 r15, C16030oK r16, C30341Xa r17, boolean z) {
        long A04;
        r13.A08();
        if (!z) {
            return context.getString(R.string.live_location_sharing_ended);
        }
        long j = r17.A0I + ((long) (r17.A00 * 1000));
        if (r17.A0z.A02) {
            A04 = r16.A05(r17);
        } else {
            A04 = r16.A04(r17);
        }
        long A02 = r14.A02(A04);
        Object[] objArr = new Object[1];
        if (A04 > r14.A00()) {
            return AnonymousClass3JK.A01(r15, C12960it.A0X(context, AnonymousClass3JK.A00(r15, A02), objArr, 0, R.string.live_location_live_until), A02);
        }
        return AnonymousClass3JK.A01(r15, C12960it.A0X(context, AnonymousClass3JK.A00(r15, j), objArr, 0, R.string.live_location_live_until), j);
    }

    public static boolean A02(C14830m7 r8, C30341Xa r9, long j) {
        long A00 = r8.A00();
        return !r9.A0z.A02 ? j > A00 : (j == -1 && r9.A0I + (((long) r9.A00) * 1000) > A00) || j > A00;
    }
}
