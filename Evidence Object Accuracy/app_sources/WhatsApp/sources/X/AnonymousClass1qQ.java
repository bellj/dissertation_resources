package X;

import android.net.Uri;
import java.io.File;

/* renamed from: X.1qQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1qQ {
    public final int A00;
    public final int A01;
    public final long A02;
    public final long A03;
    public final AnonymousClass1KA A04;
    public final C14370lK A05;
    public final AnonymousClass1KB A06;
    public final File A07;
    public final String A08;
    public final String A09;
    public final String A0A;
    public final String A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;

    public AnonymousClass1qQ(AnonymousClass1KA r4, C14370lK r5, AnonymousClass1KB r6, File file, String str, String str2, String str3, String str4, int i, int i2, long j, long j2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        if (z3 && (r5 == C14370lK.A0B || r5 == C14370lK.A0Z || r5 == C14370lK.A0G || r5 == C14370lK.A0R || r5 == C14370lK.A06 || r5 == C14370lK.A0L || r5 == C14370lK.A07)) {
            AnonymousClass009.A0C("Image transcoding should have quality settings", r4 != null);
        }
        this.A08 = str;
        this.A07 = file;
        this.A0B = str2;
        this.A09 = str3;
        this.A0A = str4;
        this.A05 = r5;
        this.A00 = i;
        this.A01 = i2;
        this.A02 = j;
        this.A03 = j2;
        this.A0D = z;
        this.A0G = z2;
        this.A0C = z3;
        this.A06 = r6;
        this.A04 = r4;
        this.A0E = z4;
        this.A0F = z5;
    }

    public File A00() {
        Uri parse;
        String str = this.A0B;
        if (str == null || (parse = Uri.parse(str)) == null || !parse.isAbsolute()) {
            return null;
        }
        return C14350lI.A03(parse);
    }
}
