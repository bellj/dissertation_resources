package X;

import androidx.window.sidecar.SidecarDisplayFeature;

/* renamed from: X.0fg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C11060fg extends AnonymousClass1WI implements AnonymousClass1J7 {
    public C11060fg() {
        super(1);
    }

    @Override // X.AnonymousClass1J7
    public Object AJ4(Object obj) {
        SidecarDisplayFeature sidecarDisplayFeature = (SidecarDisplayFeature) obj;
        C16700pc.A0E(sidecarDisplayFeature, 0);
        boolean z = true;
        if (!(sidecarDisplayFeature.getType() == 1 || sidecarDisplayFeature.getType() == 2)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
