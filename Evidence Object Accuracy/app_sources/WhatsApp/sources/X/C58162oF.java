package X;

import android.view.animation.Animation;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.2oF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58162oF extends Abstractanimation.Animation$AnimationListenerC28831Pe {
    public final /* synthetic */ VideoCallParticipantView A00;
    public final /* synthetic */ VoipActivityV2 A01;

    public C58162oF(VideoCallParticipantView videoCallParticipantView, VoipActivityV2 voipActivityV2) {
        this.A01 = voipActivityV2;
        this.A00 = videoCallParticipantView;
    }

    @Override // X.Abstractanimation.Animation$AnimationListenerC28831Pe, android.view.animation.Animation.AnimationListener
    public void onAnimationEnd(Animation animation) {
        Log.i("voip/VoipActivityV2/shrinkPreviewToPip/onAnimationEnd");
        VoipActivityV2 voipActivityV2 = this.A01;
        voipActivityV2.A1m = false;
        voipActivityV2.A0V.clearAnimation();
        voipActivityV2.A0V.setVisibility(8);
    }
}
