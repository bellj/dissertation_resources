package X;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.whatsapp.polls.PollVoterViewModel;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0kV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC13890kV extends AbstractC13900kW {
    void A5t(AnonymousClass1IS v);

    void A5u(Drawable drawable, View view);

    void A8w(AnonymousClass1IS v);

    void AAC(AbstractC15340mz v);

    AnonymousClass3DI AAl();

    int ABe();

    C64533Fx ABj();

    int ACM(AnonymousClass1X3 v);

    PollVoterViewModel AFm();

    ArrayList AGT();

    int AH8(AbstractC15340mz v);

    boolean AIM();

    boolean AJl();

    boolean AJm(AbstractC15340mz v);

    boolean AJv();

    boolean AKC(AbstractC15340mz v);

    void AUy(AbstractC15340mz v, boolean z);

    void AbN(AbstractC15340mz v);

    void Ach(AbstractC15340mz v);

    void Acq(List list, boolean z);

    void Ad0(AbstractC15340mz v, int i);

    boolean AdM(AnonymousClass1IS v);

    boolean AdX();

    boolean Adn();

    void AeE(AbstractC15340mz v);

    boolean Af1(AbstractC15340mz v);

    void AfW(AnonymousClass1X3 v, long j);

    void AfZ(AbstractC15340mz v);
}
