package X;

import android.util.Pair;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.2Rt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C50892Rt {
    public static void A00(AbstractC15710nm r11, AnonymousClass1V8 r12, C47572Bl r13, String str) {
        int i;
        Pair pair;
        Pair pair2;
        AnonymousClass1V8 A0E = r12.A0E(str);
        AnonymousClass009.A05(A0E);
        List<AnonymousClass1V8> A0J = A0E.A0J("participant");
        ArrayList arrayList = new ArrayList();
        A01(r11, A0E, UserJid.class, "participant", "jid", arrayList);
        ArrayList arrayList2 = new ArrayList();
        A03(A0E, "type", arrayList2);
        ArrayList arrayList3 = new ArrayList();
        A03(A0E, "error", arrayList3);
        ArrayList arrayList4 = new ArrayList();
        for (AnonymousClass1V8 r1 : A0J) {
            AnonymousClass1V8 A0E2 = r1.A0E("add_request");
            if (A0E2 == null) {
                pair2 = null;
            } else {
                pair2 = new Pair(A0E2.A0I("code", null), A0E2.A0I("expiration", null));
            }
            arrayList4.add(pair2);
        }
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            Object obj = arrayList.get(i2);
            String str2 = (String) arrayList3.get(i2);
            if (str2 != null) {
                try {
                    i = Integer.valueOf(str2).intValue();
                } catch (Exception unused) {
                    i = 499;
                }
                r13.A01.put(obj, Integer.valueOf(i));
                if (i == 403 && (pair = (Pair) arrayList4.get(i2)) != null) {
                    try {
                        r13.A02.put(obj, new C47582Bm((String) pair.first, Long.valueOf((String) pair.second).longValue()));
                    } catch (Exception unused2) {
                    }
                }
            } else {
                Object obj2 = arrayList2.get(i2);
                if (obj2 == null) {
                    obj2 = "";
                }
                r13.A03.put(obj, obj2);
            }
        }
    }

    public static void A01(AbstractC15710nm r2, AnonymousClass1V8 r3, Class cls, String str, String str2, List list) {
        for (AnonymousClass1V8 r0 : r3.A0J(str)) {
            list.add(r0.A0A(r2, cls, str2));
        }
    }

    public static void A02(AbstractC15710nm r6, AnonymousClass1V8 r7, String str, Map map, Map map2) {
        int i;
        AnonymousClass1V8 A0E = r7.A0E(str);
        AnonymousClass009.A05(A0E);
        ArrayList arrayList = new ArrayList();
        A01(r6, A0E, UserJid.class, "participant", "jid", arrayList);
        ArrayList arrayList2 = new ArrayList();
        A03(A0E, "type", arrayList2);
        ArrayList arrayList3 = new ArrayList();
        A03(A0E, "error", arrayList3);
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            Object obj = arrayList.get(i2);
            String str2 = (String) arrayList3.get(i2);
            if (str2 != null) {
                try {
                    i = Integer.valueOf(str2);
                } catch (Exception unused) {
                    i = 499;
                }
                map2.put(obj, i);
            } else {
                Object obj2 = arrayList2.get(i2);
                if (obj2 == null) {
                    obj2 = "";
                }
                map.put(obj, obj2);
            }
        }
    }

    public static void A03(AnonymousClass1V8 r2, String str, List list) {
        for (AnonymousClass1V8 r1 : r2.A0J("participant")) {
            list.add(r1.A0I(str, null));
        }
    }
}
