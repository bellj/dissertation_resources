package X;

import java.util.Map;

/* renamed from: X.12X  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass12X {
    public Map A00;

    public Object A00(Map map, Object obj) {
        if (this instanceof C25751Ap) {
            return null;
        }
        if (this instanceof AnonymousClass18B) {
            int i = 4;
            if (!map.containsKey(2498024)) {
                i = 3;
                if (!map.containsKey(2498023)) {
                    if (!map.containsKey(2498022)) {
                        return null;
                    }
                    i = 2;
                }
            }
            return Integer.valueOf(i);
        } else if (this instanceof C25731An) {
            return null;
        } else {
            if (this instanceof AnonymousClass1ES) {
                return "Unable to fetch list.";
            }
            if (this instanceof C235912g) {
                return "Unable to fetch configuration.";
            }
            if (!(this instanceof AnonymousClass12W)) {
                return null;
            }
            return "Unable to delete configuration.";
        }
    }
}
