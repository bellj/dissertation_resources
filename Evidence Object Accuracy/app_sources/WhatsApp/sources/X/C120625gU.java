package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.util.Log;

/* renamed from: X.5gU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120625gU extends C120895gv {
    public final /* synthetic */ C120525gK A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120625gU(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120525gK r11) {
        super(context, r8, r9, r10, "upi-list-keys");
        this.A00 = r11;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r4) {
        super.A02(r4);
        C120525gK r2 = this.A00;
        r2.A0E.A05(r4, "in_upi_list_keys_tag");
        AnonymousClass6MS r1 = r2.A00;
        if (r1 != null) {
            r1.ARr(r4, null);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r4) {
        super.A03(r4);
        C120525gK r2 = this.A00;
        r2.A0E.A05(r4, "in_upi_list_keys_tag");
        AnonymousClass6MS r1 = r2.A00;
        if (r1 != null) {
            r1.ARr(r4, null);
        }
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r7) {
        String str;
        super.A04(r7);
        AnonymousClass1V8 A0c = C117305Zk.A0c(r7);
        if (A0c == null) {
            str = "PAY: IndiaUpiPinActions sendGetListKeys: empty account node";
        } else {
            String A0I = A0c.A0I("keys", null);
            if (TextUtils.isEmpty(A0I)) {
                str = "PAY: IndiaUpiPinActions sendGetListKeys: missing keys";
            } else {
                C120525gK r3 = this.A00;
                r3.A08.A0H(A0I);
                r3.A0E.A06("in_upi_list_keys_tag", 2);
                AnonymousClass6MS r0 = r3.A00;
                if (r0 != null) {
                    r0.ARr(null, A0I);
                    return;
                }
                return;
            }
        }
        Log.w(str);
        C120525gK r2 = this.A00;
        r2.A0E.A05(null, "in_upi_list_keys_tag");
        AnonymousClass6MS r1 = r2.A00;
        if (r1 != null) {
            r1.ARr(C117305Zk.A0L(), null);
        }
    }
}
