package X;

import android.view.View;

/* renamed from: X.0WT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0WT implements View.OnTouchListener {
    public C04620Mk A00;
    public final C14260l7 A01;
    public final AnonymousClass28D A02;
    public final AnonymousClass28D A03;

    public AnonymousClass0WT(C14260l7 r2, AnonymousClass28D r3, AnonymousClass28D r4) {
        this.A03 = r3;
        this.A01 = r2;
        this.A02 = r4;
        this.A00 = (C04620Mk) AnonymousClass3JV.A04(r2, r3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0030  */
    @Override // android.view.View.OnTouchListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r9, android.view.MotionEvent r10) {
        /*
            r8 = this;
            int r1 = r10.getAction()
            r4 = 0
            if (r1 == 0) goto L_0x0049
            r7 = 1
            if (r1 == r7) goto L_0x0015
            r0 = 3
            if (r1 == r0) goto L_0x0020
            r0 = 4
            if (r1 == r0) goto L_0x0020
        L_0x0010:
            X.0Mk r0 = r8.A00
            boolean r0 = r0.A00
            return r0
        L_0x0015:
            X.0Mk r6 = r8.A00
            boolean r0 = r6.A00
            if (r0 == 0) goto L_0x0010
            X.28D r5 = r8.A03
            r0 = 38
            goto L_0x002a
        L_0x0020:
            X.0Mk r6 = r8.A00
            boolean r0 = r6.A00
            if (r0 == 0) goto L_0x0010
            X.28D r5 = r8.A03
            r0 = 35
        L_0x002a:
            X.0l1 r3 = r5.A0G(r0)
            if (r3 == 0) goto L_0x0046
            X.0l2 r2 = new X.0l2
            r2.<init>()
            X.28D r0 = r8.A02
            r2.A05(r0, r4)
            X.0l7 r1 = r8.A01
            r2.A05(r1, r7)
            X.0l3 r0 = r2.A03()
            X.C28701Oq.A01(r1, r5, r0, r3)
        L_0x0046:
            r6.A00 = r4
            goto L_0x0010
        L_0x0049:
            X.28D r3 = r8.A03
            r0 = 36
            X.0l1 r2 = r3.A0G(r0)
            if (r2 != 0) goto L_0x0054
            return r4
        L_0x0054:
            X.0l2 r1 = new X.0l2
            r1.<init>()
            X.28D r0 = r8.A02
            r1.A05(r0, r4)
            X.0l3 r1 = r1.A03()
            X.0l7 r0 = r8.A01
            java.lang.Object r2 = X.C28701Oq.A01(r0, r3, r1, r2)
            boolean r0 = r2 instanceof java.lang.Number
            if (r0 != 0) goto L_0x007c
            boolean r0 = r2 instanceof java.lang.Boolean
            if (r0 != 0) goto L_0x007c
            java.lang.String r1 = "bk.components.FoaTouchExtension"
            java.lang.String r0 = "Got non-boolean result while evaluating touch down expression"
            X.C28691Op.A00(r1, r0)
            X.0Mk r0 = r8.A00
            r0.A00 = r4
            goto L_0x0010
        L_0x007c:
            X.0Mk r1 = r8.A00
            boolean r0 = X.C64983Hr.A02(r2)
            r1.A00 = r0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass0WT.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
