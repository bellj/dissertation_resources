package X;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import java.io.IOException;
import java.util.Map;

/* renamed from: X.0HF  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HF extends AbstractC08070aX {
    public AnonymousClass0QR A00;
    public final Paint A01 = new C020609t(3);
    public final Rect A02 = new Rect();
    public final Rect A03 = new Rect();

    public AnonymousClass0HF(AnonymousClass0AA r3, AnonymousClass0PU r4) {
        super(r3, r4);
    }

    @Override // X.AbstractC08070aX
    public void A06(Canvas canvas, Matrix matrix, int i) {
        Bitmap A07 = A07();
        if (A07 != null && !A07.isRecycled()) {
            float A00 = AnonymousClass0UV.A00();
            Paint paint = this.A01;
            paint.setAlpha(i);
            AnonymousClass0QR r0 = this.A00;
            if (r0 != null) {
                paint.setColorFilter((ColorFilter) r0.A03());
            }
            canvas.save();
            canvas.concat(matrix);
            Rect rect = this.A03;
            rect.set(0, 0, A07.getWidth(), A07.getHeight());
            Rect rect2 = this.A02;
            rect2.set(0, 0, (int) (((float) A07.getWidth()) * A00), (int) (((float) A07.getHeight()) * A00));
            canvas.drawBitmap(A07, rect, rect2, paint);
            canvas.restore();
        }
    }

    public final Bitmap A07() {
        Bitmap decodeStream;
        Context context;
        String str = this.A0M.A0H;
        AnonymousClass0AA r5 = this.A0K;
        C06000Ru r4 = null;
        if (r5.getCallback() != null) {
            C06000Ru r2 = r5.A07;
            if (r2 != null) {
                Drawable.Callback callback = r5.getCallback();
                if (callback == null || !(callback instanceof View)) {
                    context = null;
                } else {
                    context = ((View) callback).getContext();
                }
                if (!(context == null && r2.A01 == null) && !r2.A01.equals(context)) {
                    r5.A07 = null;
                }
            }
            r4 = r5.A07;
            if (r4 == null) {
                r4 = new C06000Ru(r5.getCallback(), r5.A09, r5.A04.A0A);
                r5.A07 = r4;
            }
        }
        if (r4 == null) {
            return null;
        }
        Map map = r4.A03;
        C04970Nt r8 = (C04970Nt) map.get(str);
        if (r8 == null) {
            return null;
        }
        Bitmap bitmap = r8.A00;
        if (bitmap != null) {
            return bitmap;
        }
        String str2 = r8.A03;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = true;
        options.inDensity = 160;
        if (!str2.startsWith("data:") || str2.indexOf("base64,") <= 0) {
            try {
                String str3 = r4.A02;
                if (!TextUtils.isEmpty(str3)) {
                    AssetManager assets = r4.A01.getAssets();
                    StringBuilder sb = new StringBuilder();
                    sb.append(str3);
                    sb.append(str2);
                    try {
                        decodeStream = BitmapFactory.decodeStream(assets.open(sb.toString()), null, options);
                        int i = r8.A02;
                        int i2 = r8.A01;
                        if (!(decodeStream.getWidth() == i && decodeStream.getHeight() == i2)) {
                            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeStream, i, i2, true);
                            decodeStream.recycle();
                            decodeStream = createScaledBitmap;
                        }
                    } catch (IllegalArgumentException e) {
                        AnonymousClass0R5.A01("Unable to decode image.", e);
                        return null;
                    }
                } else {
                    throw new IllegalStateException("You must set an images folder before loading an image. Set it with LottieComposition#setImagesFolder or LottieDrawable#setImagesFolder");
                }
            } catch (IOException e2) {
                AnonymousClass0R5.A01("Unable to open asset.", e2);
                return null;
            }
        } else {
            try {
                byte[] decode = Base64.decode(str2.substring(str2.indexOf(44) + 1), 0);
                decodeStream = BitmapFactory.decodeByteArray(decode, 0, decode.length, options);
            } catch (IllegalArgumentException e3) {
                AnonymousClass0R5.A01("data URL did not have correct base64 format.", e3);
                return null;
            }
        }
        synchronized (C06000Ru.A04) {
            ((C04970Nt) map.get(str)).A00 = decodeStream;
        }
        return decodeStream;
    }

    @Override // X.AbstractC08070aX, X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        AnonymousClass0Gu r1;
        super.A5q(r3, obj);
        if (obj == AbstractC12810iX.A00) {
            if (r3 == null) {
                r1 = null;
            } else {
                r1 = new AnonymousClass0Gu(r3, null);
            }
            this.A00 = r1;
        }
    }

    @Override // X.AbstractC08070aX, X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        super.AAy(matrix, rectF, z);
        Bitmap A07 = A07();
        if (A07 != null) {
            rectF.set(0.0f, 0.0f, ((float) A07.getWidth()) * AnonymousClass0UV.A00(), ((float) A07.getHeight()) * AnonymousClass0UV.A00());
            this.A08.mapRect(rectF);
        }
    }
}
