package X;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5BX  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5BX implements Cloneable {
    public Object A00;
    public List A01 = C12960it.A0l();

    public final int A00() {
        if (this.A00 == null) {
            Iterator it = this.A01.iterator();
            if (!it.hasNext()) {
                return 0;
            }
            it.next();
            throw new NoSuchMethodError();
        }
        throw new NoSuchMethodError();
    }

    public final void A01() {
        if (this.A00 == null) {
            Iterator it = this.A01.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NoSuchMethodError();
            }
            return;
        }
        throw new NoSuchMethodError();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: X.4cq[] */
    /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: byte[][] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.lang.Object
    public final /* synthetic */ Object clone() {
        Object clone;
        AnonymousClass5BX r3 = new AnonymousClass5BX();
        try {
            List list = this.A01;
            if (list == null) {
                r3.A01 = null;
            } else {
                r3.A01.addAll(list);
            }
            Object obj = this.A00;
            if (obj != null) {
                if (obj instanceof AbstractC94974cq) {
                    clone = ((AbstractC94974cq) obj).clone();
                } else if (obj instanceof byte[]) {
                    clone = ((byte[]) obj).clone();
                } else {
                    int i = 0;
                    if (obj instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) obj;
                        byte[][] bArr2 = new byte[bArr.length];
                        r3.A00 = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = bArr[i].clone();
                            i++;
                        }
                    } else if (obj instanceof boolean[]) {
                        clone = ((boolean[]) obj).clone();
                    } else if (obj instanceof int[]) {
                        clone = ((int[]) obj).clone();
                    } else if (obj instanceof long[]) {
                        clone = ((long[]) obj).clone();
                    } else if (obj instanceof float[]) {
                        clone = ((float[]) obj).clone();
                    } else if (obj instanceof double[]) {
                        clone = ((double[]) obj).clone();
                    } else if (obj instanceof AbstractC94974cq[]) {
                        AbstractC94974cq[] r4 = (AbstractC94974cq[]) obj;
                        AbstractC94974cq[] r1 = new AbstractC94974cq[r4.length];
                        r3.A00 = r1;
                        while (i < r4.length) {
                            r1[i] = r4[i].clone();
                            i++;
                        }
                    }
                }
                r3.A00 = clone;
                return r3;
            }
            return r3;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        List list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AnonymousClass5BX)) {
            return false;
        }
        AnonymousClass5BX r5 = (AnonymousClass5BX) obj;
        if (this.A00 == null || r5.A00 == null) {
            List list2 = this.A01;
            if (list2 != null && (list = r5.A01) != null) {
                return list2.equals(list);
            }
            try {
                int A00 = A00();
                byte[] bArr = new byte[A00];
                new C95484do(bArr, A00);
                A01();
                int A002 = r5.A00();
                byte[] bArr2 = new byte[A002];
                new C95484do(bArr2, A002);
                r5.A01();
                return Arrays.equals(bArr, bArr2);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            throw C12980iv.A0n("zzrk");
        }
    }

    @Override // java.lang.Object
    public final int hashCode() {
        try {
            int A00 = A00();
            byte[] bArr = new byte[A00];
            new C95484do(bArr, A00);
            A01();
            return Arrays.hashCode(bArr) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
