package X;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

/* renamed from: X.53K  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass53K implements AnonymousClass5XB {
    public final int A00;
    public final int A01;
    public final Drawable A02;
    public final Drawable A03;
    public final AnonymousClass5WK A04;
    public final String A05;
    public final String A06;
    public final WeakReference A07;

    @Override // X.AnonymousClass5XB
    public boolean A9o() {
        return false;
    }

    public AnonymousClass53K(Drawable drawable, Drawable drawable2, ImageView imageView, AnonymousClass5WK r5, String str, String str2, int i, int i2) {
        WeakReference weakReference;
        if (imageView != null) {
            weakReference = C12970iu.A10(imageView);
        } else {
            weakReference = null;
        }
        this.A07 = weakReference;
        this.A06 = str;
        this.A05 = str2;
        this.A03 = drawable;
        this.A02 = drawable2;
        this.A01 = i;
        this.A00 = i2;
        this.A04 = r5;
    }

    @Override // X.AnonymousClass5XB
    public int AE9() {
        return this.A00;
    }

    @Override // X.AnonymousClass5XB
    public int AEB() {
        return this.A01;
    }

    @Override // X.AnonymousClass5XB
    public String AHT() {
        return this.A06;
    }

    @Override // X.AnonymousClass5XB
    public String getId() {
        return this.A05;
    }
}
