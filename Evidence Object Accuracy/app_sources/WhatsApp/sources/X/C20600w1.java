package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.0w1 */
/* loaded from: classes2.dex */
public class C20600w1 extends AbstractC20610w2 {
    public final C15570nT A00;
    public final C16370ot A01;
    public final C21390xL A02;

    public C20600w1(AbstractC15710nm r7, C15570nT r8, C16370ot r9, C18460sU r10, C20850wQ r11, C16490p7 r12, C21390xL r13, C22840zj r14) {
        super(r7, r10, r11, r12, r14);
        this.A00 = r8;
        this.A02 = r13;
        this.A01 = r9;
    }

    public static /* synthetic */ boolean A00(C20600w1 r6, UserJid userJid, long j) {
        long A01 = ((AbstractC20610w2) r6).A02.A01(userJid.getPrimaryDevice());
        try {
            C16310on A02 = r6.A04.A02();
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("message_row_id", Long.valueOf(j));
            contentValues.put("receipt_device_jid_row_id", Long.valueOf(A01));
            if (A02.A03.A02(contentValues, "receipt_device") != -1) {
                A02.close();
                return true;
            }
            A02.close();
            return false;
        } catch (SQLiteDatabaseCorruptException e) {
            Log.e(e);
            r6.A03.A02();
            return false;
        }
    }

    public final C32611cR A05(AnonymousClass1IS r10, long j) {
        C32611cR r3 = new C32611cR();
        if (j != -1) {
            String[] strArr = {String.valueOf(j)};
            try {
                C16310on A01 = this.A04.get();
                Cursor A09 = A01.A03.A09("SELECT receipt_device_jid_row_id, receipt_device_timestamp FROM receipt_device WHERE message_row_id = ?", strArr);
                while (A09.moveToNext()) {
                    long j2 = A09.getLong(A09.getColumnIndexOrThrow("receipt_device_jid_row_id"));
                    C18460sU r8 = super.A02;
                    DeviceJid deviceJid = (DeviceJid) r8.A07(DeviceJid.class, j2);
                    if (deviceJid != null) {
                        r3.A00.put(deviceJid, new C32621cS(A09.getLong(1)));
                    } else {
                        StringBuilder sb = new StringBuilder();
                        sb.append("receiptsmessagestore/getmessagedevicereceipts: got a null deviceJid for key=");
                        sb.append(r10);
                        sb.append(", deviceJidRowId=");
                        sb.append(j2);
                        sb.append(", jid=");
                        sb.append(r8.A03(j2));
                        Log.e(sb.toString());
                    }
                }
                A09.close();
                A01.close();
                return r3;
            } catch (SQLiteDatabaseCorruptException e) {
                Log.e(e);
                this.A03.A02();
            }
        }
        return r3;
    }
}
