package X;

import android.graphics.Color;
import android.view.animation.Interpolator;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.util.ArrayList;
import org.chromium.net.UrlRequest;

/* renamed from: X.0RQ  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0RQ {
    public static final C05850Rf A00 = C05850Rf.A00("nm");
    public static final C05850Rf A01 = C05850Rf.A00("nm", "ind", "refId", "ty", "parent", "sw", "sh", "sc", "ks", "tt", "masksProperties", "shapes", "t", "ef", "sr", "st", "w", "h", "ip", "op", "tm", "cl", "hd");
    public static final C05850Rf A02 = C05850Rf.A00("d", "a");

    public static AnonymousClass0PU A00(C05540Py r59, AbstractC08850bx r60) {
        StringBuilder sb;
        String str;
        AnonymousClass0J6 r36 = AnonymousClass0J6.NONE;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        r60.A0A();
        Float valueOf = Float.valueOf(1.0f);
        AnonymousClass0JI r35 = null;
        Float valueOf2 = Float.valueOf(0.0f);
        String str2 = "UNSET";
        String str3 = null;
        C08170ah r34 = null;
        AnonymousClass0H8 r26 = null;
        C04850Nh r11 = null;
        AnonymousClass0H9 r31 = null;
        long j = -1;
        float f = 0.0f;
        float f2 = 0.0f;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        float f3 = 1.0f;
        float f4 = 0.0f;
        int i4 = 0;
        int i5 = 0;
        boolean z = false;
        long j2 = 0;
        String str4 = null;
        while (r60.A0H()) {
            switch (r60.A04(A01)) {
                case 0:
                    str2 = r60.A08();
                    break;
                case 1:
                    j2 = (long) r60.A03();
                    break;
                case 2:
                    str3 = r60.A08();
                    break;
                case 3:
                    int A03 = r60.A03();
                    r35 = AnonymousClass0JI.UNKNOWN;
                    if (A03 >= 6) {
                        break;
                    } else {
                        r35 = AnonymousClass0JI.values()[A03];
                        break;
                    }
                case 4:
                    j = (long) r60.A03();
                    break;
                case 5:
                    i = (int) (((float) r60.A03()) * AnonymousClass0UV.A00());
                    break;
                case 6:
                    i2 = (int) (((float) r60.A03()) * AnonymousClass0UV.A00());
                    break;
                case 7:
                    i3 = Color.parseColor(r60.A08());
                    break;
                case 8:
                    r34 = AnonymousClass0RN.A00(r59, r60);
                    break;
                case 9:
                    int A032 = r60.A03();
                    if (A032 < AnonymousClass0J6.values().length) {
                        r36 = AnonymousClass0J6.values()[A032];
                        switch (r36.ordinal()) {
                            case 3:
                                str = "Unsupported matte type: Luma";
                                AnonymousClass0R5.A00(str);
                                r59.A0E.add(str);
                                r59.A03++;
                                break;
                            case 4:
                                str = "Unsupported matte type: Luma Inverted";
                                AnonymousClass0R5.A00(str);
                                r59.A0E.add(str);
                                r59.A03++;
                                break;
                            default:
                                r59.A03++;
                                break;
                        }
                    } else {
                        sb = new StringBuilder("Unsupported matte type: ");
                        sb.append(A032);
                        String obj = sb.toString();
                        AnonymousClass0R5.A00(obj);
                        r59.A0E.add(obj);
                        break;
                    }
                case 10:
                    r60.A09();
                    while (r60.A0H()) {
                        r60.A0A();
                        AnonymousClass0JS r5 = null;
                        AnonymousClass0H7 r6 = null;
                        AnonymousClass0HA r8 = null;
                        boolean z2 = false;
                        while (r60.A0H()) {
                            String A07 = r60.A07();
                            switch (A07.hashCode()) {
                                case 111:
                                    if (!A07.equals("o")) {
                                        r60.A0E();
                                        break;
                                    } else {
                                        r8 = AnonymousClass0TE.A02(r59, r60);
                                        break;
                                    }
                                case 3588:
                                    if (!A07.equals("pt")) {
                                        r60.A0E();
                                        break;
                                    } else {
                                        r6 = new AnonymousClass0H7(AnonymousClass0TN.A00(r59, C08350az.A00, r60, AnonymousClass0UV.A00(), false));
                                        break;
                                    }
                                case 104433:
                                    if (!A07.equals("inv")) {
                                        r60.A0E();
                                        break;
                                    } else {
                                        z2 = r60.A0I();
                                        break;
                                    }
                                case 3357091:
                                    if (!A07.equals("mode")) {
                                        r60.A0E();
                                        break;
                                    } else {
                                        String A08 = r60.A08();
                                        switch (A08.hashCode()) {
                                            case 97:
                                                if (!A08.equals("a")) {
                                                    StringBuilder sb2 = new StringBuilder("Unknown mask mode ");
                                                    sb2.append(A07);
                                                    sb2.append(". Defaulting to Add.");
                                                    AnonymousClass0R5.A00(sb2.toString());
                                                    r5 = AnonymousClass0JS.MASK_MODE_ADD;
                                                    break;
                                                } else {
                                                    r5 = AnonymousClass0JS.MASK_MODE_ADD;
                                                    break;
                                                }
                                            case 105:
                                                if (!A08.equals("i")) {
                                                    StringBuilder sb2 = new StringBuilder("Unknown mask mode ");
                                                    sb2.append(A07);
                                                    sb2.append(". Defaulting to Add.");
                                                    AnonymousClass0R5.A00(sb2.toString());
                                                    r5 = AnonymousClass0JS.MASK_MODE_ADD;
                                                    break;
                                                } else {
                                                    AnonymousClass0R5.A00("Animation contains intersect masks. They are not supported but will be treated like add masks.");
                                                    r59.A0E.add("Animation contains intersect masks. They are not supported but will be treated like add masks.");
                                                    r5 = AnonymousClass0JS.MASK_MODE_INTERSECT;
                                                    break;
                                                }
                                            case 110:
                                                if (!A08.equals("n")) {
                                                    StringBuilder sb2 = new StringBuilder("Unknown mask mode ");
                                                    sb2.append(A07);
                                                    sb2.append(". Defaulting to Add.");
                                                    AnonymousClass0R5.A00(sb2.toString());
                                                    r5 = AnonymousClass0JS.MASK_MODE_ADD;
                                                    break;
                                                } else {
                                                    r5 = AnonymousClass0JS.MASK_MODE_NONE;
                                                    break;
                                                }
                                            case 115:
                                                if (!A08.equals("s")) {
                                                    StringBuilder sb2 = new StringBuilder("Unknown mask mode ");
                                                    sb2.append(A07);
                                                    sb2.append(". Defaulting to Add.");
                                                    AnonymousClass0R5.A00(sb2.toString());
                                                    r5 = AnonymousClass0JS.MASK_MODE_ADD;
                                                    break;
                                                } else {
                                                    r5 = AnonymousClass0JS.MASK_MODE_SUBTRACT;
                                                    break;
                                                }
                                            default:
                                                StringBuilder sb2 = new StringBuilder("Unknown mask mode ");
                                                sb2.append(A07);
                                                sb2.append(". Defaulting to Add.");
                                                AnonymousClass0R5.A00(sb2.toString());
                                                r5 = AnonymousClass0JS.MASK_MODE_ADD;
                                                break;
                                        }
                                    }
                                default:
                                    r60.A0E();
                                    break;
                            }
                        }
                        r60.A0C();
                        arrayList.add(new C04860Ni(r8, r6, r5, z2));
                    }
                    r59.A03 += arrayList.size();
                    r60.A0B();
                    break;
                case 11:
                    r60.A09();
                    while (r60.A0H()) {
                        AbstractC12040hH A012 = AnonymousClass0TM.A01(r59, r60);
                        if (A012 != null) {
                            arrayList2.add(A012);
                        }
                    }
                    r60.A0B();
                    break;
                case 12:
                    r60.A0A();
                    while (r60.A0H()) {
                        int A04 = r60.A04(A02);
                        if (A04 == 0) {
                            r26 = new AnonymousClass0H8(AnonymousClass0TN.A00(r59, C08340ay.A00, r60, 1.0f, false));
                        } else if (A04 != 1) {
                            r60.A0D();
                            r60.A0E();
                        } else {
                            r60.A09();
                            if (r60.A0H()) {
                                r60.A0A();
                                r11 = null;
                                while (r60.A0H()) {
                                    if (r60.A04(AnonymousClass0MD.A01) != 0) {
                                        r60.A0D();
                                        r60.A0E();
                                    } else {
                                        r60.A0A();
                                        AnonymousClass0H4 r62 = null;
                                        AnonymousClass0H4 r10 = null;
                                        AnonymousClass0H9 r9 = null;
                                        AnonymousClass0H9 r82 = null;
                                        while (r60.A0H()) {
                                            int A042 = r60.A04(AnonymousClass0MD.A00);
                                            if (A042 == 0) {
                                                r62 = AnonymousClass0TE.A00(r59, r60);
                                            } else if (A042 == 1) {
                                                r10 = AnonymousClass0TE.A00(r59, r60);
                                            } else if (A042 == 2) {
                                                r9 = AnonymousClass0TE.A01(r59, r60, true);
                                            } else if (A042 != 3) {
                                                r60.A0D();
                                                r60.A0E();
                                            } else {
                                                r82 = AnonymousClass0TE.A01(r59, r60, true);
                                            }
                                        }
                                        r60.A0C();
                                        r11 = new C04850Nh(r62, r10, r9, r82);
                                    }
                                }
                                r60.A0C();
                                if (r11 == null) {
                                    r11 = new C04850Nh(null, null, null, null);
                                }
                            }
                            while (r60.A0H()) {
                                r60.A0E();
                            }
                            r60.A0B();
                        }
                    }
                    r60.A0C();
                    break;
                case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                    r60.A09();
                    ArrayList arrayList3 = new ArrayList();
                    while (r60.A0H()) {
                        r60.A0A();
                        while (r60.A0H()) {
                            if (r60.A04(A00) != 0) {
                                r60.A0D();
                                r60.A0E();
                            } else {
                                arrayList3.add(r60.A08());
                            }
                        }
                        r60.A0C();
                    }
                    r60.A0B();
                    sb = new StringBuilder("Lottie doesn't support layer effects. If you are using them for  fills, strokes, trim paths etc. then try adding them directly as contents  in your shape. Found: ");
                    sb.append(arrayList3);
                    String obj = sb.toString();
                    AnonymousClass0R5.A00(obj);
                    r59.A0E.add(obj);
                    break;
                case UrlRequest.Status.READING_RESPONSE /* 14 */:
                    f3 = (float) r60.A02();
                    break;
                case 15:
                    f4 = (float) r60.A02();
                    break;
                case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                    i4 = (int) (((float) r60.A03()) * AnonymousClass0UV.A00());
                    break;
                case 17:
                    i5 = (int) (((float) r60.A03()) * AnonymousClass0UV.A00());
                    break;
                case 18:
                    f = (float) r60.A02();
                    break;
                case 19:
                    f2 = (float) r60.A02();
                    break;
                case C43951xu.A01 /* 20 */:
                    r31 = AnonymousClass0TE.A01(r59, r60, false);
                    break;
                case 21:
                    str4 = r60.A08();
                    break;
                case 22:
                    z = r60.A0I();
                    break;
                default:
                    r60.A0D();
                    r60.A0E();
                    break;
            }
        }
        r60.A0C();
        float f5 = f / f3;
        float f6 = f2 / f3;
        ArrayList arrayList4 = new ArrayList();
        if (f5 > 0.0f) {
            arrayList4.add(new AnonymousClass0U8((Interpolator) null, r59, Float.valueOf(f5), valueOf2, valueOf2, 0.0f));
        }
        if (f6 <= 0.0f) {
            f6 = r59.A00;
        }
        arrayList4.add(new AnonymousClass0U8((Interpolator) null, r59, Float.valueOf(f6), valueOf, valueOf, f5));
        arrayList4.add(new AnonymousClass0U8((Interpolator) null, r59, Float.valueOf(Float.MAX_VALUE), valueOf2, valueOf2, f6));
        if (str2.endsWith(".ai") || "ai".equals(str4)) {
            AnonymousClass0R5.A00("Convert your Illustrator layers to shape layers.");
            r59.A0E.add("Convert your Illustrator layers to shape layers.");
        }
        return new AnonymousClass0PU(r59, r31, r26, r11, r34, r35, r36, str2, str3, arrayList2, arrayList, arrayList4, f3, f4, i, i2, i3, i4, i5, j2, j, z);
    }
}
