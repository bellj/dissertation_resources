package X;

/* renamed from: X.4as  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC93864as {
    public final int A00;

    public AbstractC93864as(int i) {
        this.A00 = i;
    }

    public static String A00(int i) {
        StringBuilder A0k = C12960it.A0k("");
        A0k.append((char) ((i >> 24) & 255));
        A0k.append((char) ((i >> 16) & 255));
        A0k.append((char) ((i >> 8) & 255));
        return C72463ee.A0H(A0k, (char) (i & 255));
    }

    public String toString() {
        return A00(this.A00);
    }
}
