package X;

import android.text.TextUtils;
import com.whatsapp.jid.Jid;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/* renamed from: X.1v8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42391v8 {
    public static String A00(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('_');
        sb.append(UUID.randomUUID().toString());
        return sb.toString();
    }

    public static List A01(MessageDigest messageDigest, List list, List list2) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list2.iterator();
        while (it.hasNext()) {
            C15370n3 r1 = (C15370n3) it.next();
            if (!A04(r1.A0D) && A02(r1, messageDigest, list)) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public static boolean A02(C15370n3 r6, MessageDigest messageDigest, List list) {
        Jid jid = r6.A0D;
        AnonymousClass009.A05(jid);
        if (!jid.equals(AnonymousClass1VY.A00)) {
            StringBuilder sb = new StringBuilder();
            sb.append(jid.user);
            sb.append("WA_ADD_NOTIF");
            String obj = sb.toString();
            messageDigest.reset();
            messageDigest.update(obj.getBytes());
            byte[] digest = messageDigest.digest();
            for (int i = 0; i < list.size(); i++) {
                byte[] bArr = (byte[]) list.get(i);
                if (digest.length >= bArr.length) {
                    for (int i2 = 0; i2 < bArr.length; i2++) {
                        if (digest[i2] == bArr[i2]) {
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean A03(C15370n3 r1, boolean z) {
        C28811Pc r0 = r1.A0C;
        if (r0 == null || TextUtils.isEmpty(r0.A01)) {
            return true;
        }
        Jid jid = r1.A0D;
        return jid == null ? !z : A04(jid);
    }

    public static boolean A04(Jid jid) {
        return jid == null || !jid.isProtocolCompliant() || C15380n4.A0M(jid);
    }
}
