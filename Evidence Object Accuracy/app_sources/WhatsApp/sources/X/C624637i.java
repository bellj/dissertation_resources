package X;

import java.io.File;

/* renamed from: X.37i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624637i extends AbstractC16350or {
    public final int A00;
    public final C89434Jy A01;
    public final File A02;
    public final boolean A03;
    public final byte[] A04;

    public C624637i(AbstractC001200n r1, C89434Jy r2, File file, byte[] bArr, int i, boolean z) {
        super(r1);
        this.A01 = r2;
        this.A04 = bArr;
        this.A00 = i;
        this.A03 = z;
        this.A02 = file;
    }
}
