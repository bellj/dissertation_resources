package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.4m6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C100554m6 implements Parcelable {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(31);
    public int A00;
    public final int A01;
    public final C100614mC[] A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: X.4mC[] */
    /* JADX WARN: Multi-variable type inference failed */
    public C100554m6(Parcel parcel) {
        int readInt = parcel.readInt();
        this.A01 = readInt;
        this.A02 = new C100614mC[readInt];
        for (int i = 0; i < this.A01; i++) {
            this.A02[i] = C12990iw.A0I(parcel, C100614mC.class);
        }
    }

    public C100554m6(C100614mC... r3) {
        int length = r3.length;
        C95314dV.A04(C12960it.A1U(length));
        this.A02 = r3;
        this.A01 = length;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C100554m6.class != obj.getClass()) {
                return false;
            }
            C100554m6 r5 = (C100554m6) obj;
            if (this.A01 != r5.A01 || !Arrays.equals(this.A02, r5.A02)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.A00;
        if (i != 0) {
            return i;
        }
        int hashCode = 527 + Arrays.hashCode(this.A02);
        this.A00 = hashCode;
        return hashCode;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2 = this.A01;
        parcel.writeInt(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            parcel.writeParcelable(this.A02[i3], 0);
        }
    }
}
