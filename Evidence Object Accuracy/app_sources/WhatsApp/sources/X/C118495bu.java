package X;

import com.whatsapp.payments.ui.IndiaUpiMandatePaymentActivity;

/* renamed from: X.5bu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118495bu extends AnonymousClass0Yo {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass1IR A01;
    public final /* synthetic */ C120555gN A02;
    public final /* synthetic */ C120475gF A03;
    public final /* synthetic */ C120565gO A04;
    public final /* synthetic */ C120525gK A05;
    public final /* synthetic */ IndiaUpiMandatePaymentActivity A06;
    public final /* synthetic */ C128355vy A07;
    public final /* synthetic */ String A08;

    public C118495bu(AnonymousClass1IR r1, C120555gN r2, C120475gF r3, C120565gO r4, C120525gK r5, IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity, C128355vy r7, String str, int i) {
        this.A07 = r7;
        this.A06 = indiaUpiMandatePaymentActivity;
        this.A01 = r1;
        this.A05 = r5;
        this.A04 = r4;
        this.A02 = r2;
        this.A03 = r3;
        this.A00 = i;
        this.A08 = str;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118005b7.class)) {
            IndiaUpiMandatePaymentActivity indiaUpiMandatePaymentActivity = this.A06;
            C128355vy r0 = this.A07;
            C14830m7 r4 = r0.A09;
            C16590pI r5 = r0.A0A;
            C14900mE r3 = r0.A00;
            AbstractC14440lR r14 = r0.A0k;
            C241414j r6 = r0.A0H;
            C17070qD r9 = r0.A0W;
            C243515e r8 = r0.A0S;
            AnonymousClass1IR r7 = this.A01;
            C120525gK r13 = this.A05;
            C120565gO r12 = this.A04;
            return new C118005b7(indiaUpiMandatePaymentActivity, r3, r4, r5, r6, r7, r8, r9, this.A02, this.A03, r12, r13, r14, this.A08, this.A00);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
