package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1F7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1F7 {
    public final AnonymousClass10V A00;
    public final C22050yP A01;
    public final C17080qE A02;
    public final C20700wB A03;

    public AnonymousClass1F7(AnonymousClass10V r1, C22050yP r2, C17080qE r3, C20700wB r4) {
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    public final void A00(C42051ua r4) {
        C32431c9 r0;
        UserJid userJid = r4.A0C;
        if (userJid != null) {
            boolean equals = "image".equals(r4.A0E);
            C20700wB r02 = this.A03;
            AnonymousClass009.A05(userJid);
            if (equals) {
                r0 = r02.A01;
            } else {
                r0 = r02.A02;
            }
            r0.A01(userJid);
        }
    }
}
