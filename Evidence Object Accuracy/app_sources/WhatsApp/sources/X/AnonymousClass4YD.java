package X;

import android.util.Pair;

/* renamed from: X.4YD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4YD {
    public long A00;
    public AnonymousClass4YD A01;
    public AnonymousClass4Y1 A02;
    public C100564m7 A03 = C100564m7.A03;
    public AnonymousClass4R7 A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;
    public final C64463Fq A08;
    public final AbstractC14080kp A09;
    public final AnonymousClass4M7 A0A;
    public final Object A0B;
    public final AnonymousClass5WX[] A0C;
    public final AbstractC116795Wx[] A0D;
    public final boolean[] A0E;

    public AnonymousClass4YD(AnonymousClass4Y1 r9, C64463Fq r10, AnonymousClass4M7 r11, AnonymousClass4R7 r12, AnonymousClass5VZ r13, AnonymousClass5WX[] r14, long j) {
        this.A0C = r14;
        this.A00 = j;
        this.A0A = r11;
        this.A08 = r10;
        C28741Ov r6 = r9.A04;
        Object obj = r6.A04;
        this.A0B = obj;
        this.A02 = r9;
        this.A04 = r12;
        int length = r14.length;
        this.A0D = new AbstractC116795Wx[length];
        this.A0E = new boolean[length];
        long j2 = r9.A03;
        long j3 = r9.A01;
        Pair pair = (Pair) obj;
        Object obj2 = pair.first;
        C28741Ov A01 = r6.A01(pair.second);
        C67613Se r62 = (C67613Se) r10.A09.get(obj2);
        r10.A0A.add(r62);
        C90694Ow r4 = (C90694Ow) r10.A06.get(r62);
        if (r4 != null) {
            r4.A01.A9J(r4.A00);
        }
        r62.A04.add(A01);
        AbstractC14080kp A05 = r62.A02.A8S(A01, r13, j2);
        r10.A07.put(A05, r62);
        r10.A02();
        if (!(j3 == -9223372036854775807L || j3 == Long.MIN_VALUE)) {
            A05 = new C67673Sk(A05, j3);
        }
        this.A09 = A05;
    }

    public long A00() {
        if (!this.A07) {
            return this.A02.A03;
        }
        if (this.A06) {
            long AB2 = this.A09.AB2();
            if (AB2 != Long.MIN_VALUE) {
                return AB2;
            }
        }
        return this.A02.A00;
    }

    public long A01(AnonymousClass4R7 r18, boolean[] zArr, long j, boolean z) {
        AnonymousClass5WX[] r6;
        int length;
        AnonymousClass4R7 r2;
        int i = 0;
        while (true) {
            boolean z2 = true;
            if (i >= r18.A00) {
                break;
            }
            boolean[] zArr2 = this.A0E;
            if (z || (r2 = this.A04) == null || !AnonymousClass3JZ.A0H(r18.A02[i], r2.A02[i]) || !AnonymousClass3JZ.A0H(r18.A03[i], r2.A03[i])) {
                z2 = false;
            }
            zArr2[i] = z2;
            i++;
        }
        AbstractC116795Wx[] r11 = this.A0D;
        int i2 = 0;
        while (true) {
            r6 = this.A0C;
            length = r6.length;
            if (i2 >= length) {
                break;
            }
            if (((AbstractC106444vi) r6[i2]).A09 == 7) {
                r11[i2] = null;
            }
            i2++;
        }
        this.A04 = r18;
        AbstractC14080kp r10 = this.A09;
        AbstractC117085Ye[] r12 = r18.A03;
        long AbW = r10.AbW(r11, r12, this.A0E, zArr, j);
        for (int i3 = 0; i3 < length; i3++) {
            if (((AbstractC106444vi) r6[i3]).A09 == 7 && this.A04.A02[i3] != null) {
                r11[i3] = new C107594xd();
            }
        }
        this.A06 = false;
        for (int i4 = 0; i4 < r11.length; i4++) {
            if (r11[i4] != null) {
                C95314dV.A04(C12960it.A1W(r18.A02[i4]));
                if (((AbstractC106444vi) r6[i4]).A09 != 7) {
                    this.A06 = true;
                }
            } else {
                C95314dV.A04(C12980iv.A1X(r12[i4]));
            }
        }
        return AbW;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v14, resolved type: X.4R6[] */
    /* JADX DEBUG: Multi-variable search result rejected for r2v17, resolved type: X.4R6 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v17, resolved type: X.4R6 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v32, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r2v33, resolved type: X.4R6 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v225, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r2v43, resolved type: java.lang.Object[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v309, resolved type: int[][] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x030b, code lost:
        if (r27 == 0) goto L_0x030d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x03e2, code lost:
        if (r7.length > 1) goto L_0x03ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0177, code lost:
        if ((r1 & r31) == 0) goto L_0x0179;
     */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x02ec A[LOOP:8: B:66:0x017c->B:133:0x02ec, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:441:0x02cc A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass4R7 A02() {
        /*
        // Method dump skipped, instructions count: 2007
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4YD.A02():X.4R7");
    }

    public void A03() {
        long j = this.A02.A01;
        C64463Fq r6 = this.A08;
        AbstractC14080kp r5 = this.A09;
        try {
            if (j == -9223372036854775807L || j == Long.MIN_VALUE) {
                r6.A06(r5);
            } else {
                r6.A06(((C67673Sk) r5).A04);
            }
        } catch (RuntimeException e) {
            C64923Hl.A01("MediaPeriodHolder", "Period release failed.", e);
        }
    }
}
