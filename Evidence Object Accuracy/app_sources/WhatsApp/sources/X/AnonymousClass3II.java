package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.3II  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3II {
    public static final AnonymousClass3C9 A03 = new AnonymousClass3C9();
    public final String A00;
    public final Map A01;
    public final AnonymousClass3II[] A02;

    public /* synthetic */ AnonymousClass3II(String str) {
        this.A00 = str;
        this.A02 = null;
        this.A01 = null;
    }

    public /* synthetic */ AnonymousClass3II(String str, AnonymousClass3II[] r7) {
        LinkedHashMap linkedHashMap;
        this.A00 = str;
        this.A02 = r7;
        if (r7 == null) {
            linkedHashMap = null;
        } else {
            int length = r7.length;
            int A032 = C17540qy.A03(length);
            linkedHashMap = new LinkedHashMap(A032 < 16 ? 16 : A032);
            int i = 0;
            while (i < length) {
                AnonymousClass3II r1 = r7[i];
                i++;
                linkedHashMap.put(r1.A00, r1);
            }
        }
        this.A01 = linkedHashMap;
    }
}
