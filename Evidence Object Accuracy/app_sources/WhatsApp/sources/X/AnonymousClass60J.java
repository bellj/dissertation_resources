package X;

import android.graphics.Bitmap;
import android.graphics.Rect;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.60J  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass60J {
    public static final C125515rN A0L = new C125515rN(6);
    public static final C125515rN A0M = new C125515rN(5);
    public static final C125515rN A0N = new C125515rN(4);
    public static final C125515rN A0O = new C125515rN(3);
    public static final C125525rO A0P = new C125525rO(9);
    public static final C125525rO A0Q = new C125525rO(10);
    public static final C125525rO A0R = new C125525rO(14);
    public static final C125525rO A0S = new C125525rO(13);
    public static final C125525rO A0T = new C125525rO(17);
    public static final C125525rO A0U = new C125525rO(7);
    public static final C125525rO A0V = new C125525rO(20);
    public static final C125525rO A0W = new C125525rO(11);
    public static final C125525rO A0X = new C125525rO(12);
    public static final C125525rO A0Y = new C125525rO(21);
    public static final C125525rO A0Z = new C125525rO(0);
    public static final C125525rO A0a = new C125525rO(1);
    public static final C125525rO A0b = new C125525rO(8);
    public static final C125525rO A0c = new C125525rO(15);
    public static final C125525rO A0d = new C125525rO(19);
    public static final C125525rO A0e = new C125525rO(18);
    public static final C125525rO A0f = new C125525rO(2);
    public static final C125525rO A0g = new C125525rO(16);
    public final int A00;
    public final int A01;
    public final Bitmap A02;
    public final Rect A03;
    public final Rect A04;
    public final Rect A05;
    public final C129975yb A06;
    public final AnonymousClass60J A07;
    public final Boolean A08;
    public final Float A09;
    public final Float A0A;
    public final Integer A0B;
    public final Integer A0C;
    public final Integer A0D;
    public final Integer A0E;
    public final Integer A0F;
    public final Integer A0G;
    public final Long A0H;
    public final Long A0I;
    public final byte[] A0J;
    public final byte[] A0K;

    public /* synthetic */ AnonymousClass60J(C129755yF r2) {
        this.A03 = r2.A01;
        this.A04 = r2.A0K;
        this.A01 = r2.A0J;
        this.A00 = r2.A0I;
        this.A0J = r2.A0G;
        this.A0K = r2.A0H;
        this.A06 = r2.A03;
        this.A05 = r2.A02;
        this.A0H = r2.A0E;
        this.A0D = r2.A0A;
        this.A09 = r2.A06;
        this.A0B = r2.A08;
        this.A0A = r2.A07;
        this.A0I = r2.A0F;
        this.A07 = r2.A04;
        this.A0E = r2.A0B;
        this.A0G = r2.A0D;
        this.A08 = r2.A05;
        this.A0F = r2.A0C;
        this.A0C = r2.A09;
        this.A02 = r2.A00;
    }

    public Object A00(C125515rN r3) {
        int i;
        int i2 = r3.A00;
        if (i2 == 3) {
            return this.A04;
        }
        if (i2 == 4) {
            return this.A03;
        }
        if (i2 == 5) {
            i = this.A01;
        } else if (i2 == 6) {
            i = this.A00;
        } else {
            throw C12990iw.A0m(C12960it.A0W(i2, "Invalid required photo capture result key: "));
        }
        return Integer.valueOf(i);
    }

    public Object A01(C125525rO r3) {
        int i = r3.A00;
        if (i == 0) {
            return this.A0J;
        }
        if (i == 1) {
            return this.A06;
        }
        if (i == 2) {
            return this.A05;
        }
        if (i == 3) {
            return this.A04;
        }
        switch (i) {
            case 7:
                return this.A0H;
            case 8:
                return this.A0D;
            case 9:
                return this.A09;
            case 10:
                return this.A0B;
            case 11:
                return this.A0A;
            case 12:
                return this.A0I;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                return null;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                return this.A07;
            case 15:
                return this.A0E;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                return this.A0G;
            case 17:
                return this.A08;
            case 18:
                return this.A0F;
            case 19:
                return this.A0K;
            case C43951xu.A01:
                return this.A0C;
            case 21:
                return this.A02;
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Invalid photo capture result key: "));
        }
    }
}
