package X;

import android.os.Bundle;
import java.util.HashMap;

/* renamed from: X.5yW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129925yW {
    public HashMap A00;
    public boolean A01 = false;
    public final C16590pI A02;
    public final C124925qO A03;
    public final C18600si A04;
    public final C22710zW A05;

    public C129925yW(C16590pI r2, C124925qO r3, C18600si r4, C22710zW r5) {
        this.A02 = r2;
        this.A04 = r4;
        this.A05 = r5;
        this.A03 = r3;
    }

    public String A00(int i) {
        if (this.A05.A03.A07(698)) {
            return A02(String.valueOf(i));
        }
        return null;
    }

    public String A01(Bundle bundle, String str) {
        String A02;
        return (bundle == null || !this.A05.A03.A07(698) || (A02 = A02(String.valueOf(bundle.getInt("error_code")))) == null) ? str : A02;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:65|(4:104|66|(2:67|(1:69)(1:113))|70)|88|76) */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0165, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0166, code lost:
        com.whatsapp.util.Log.e(X.C12960it.A0b("PAY: ErrorMapMetadata/parseToJson/JSONException: ", r1));
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0074 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A02(java.lang.String r16) {
        /*
        // Method dump skipped, instructions count: 369
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C129925yW.A02(java.lang.String):java.lang.String");
    }
}
