package X;

/* renamed from: X.5Fx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113075Fx implements AnonymousClass20L {
    public AnonymousClass20L A00;
    public byte[] A01;

    public C113075Fx(AnonymousClass20L r3, byte[] bArr) {
        this(r3, bArr, 0, bArr.length);
    }

    public C113075Fx(AnonymousClass20L r3, byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        this.A01 = bArr2;
        this.A00 = r3;
        System.arraycopy(bArr, i, bArr2, 0, i2);
    }
}
