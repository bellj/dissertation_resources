package X;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.maps.internal.IProjectionDelegate;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.3F0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3F0 {
    public final IProjectionDelegate A00;

    public AnonymousClass3F0(IProjectionDelegate iProjectionDelegate) {
        this.A00 = iProjectionDelegate;
    }

    public Point A00(LatLng latLng) {
        C13020j0.A01(latLng);
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, latLng);
            return (Point) BinderC56502l7.A00(C65873Li.A00(A01, r2, 2));
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public LatLng A01(Point point) {
        Parcelable parcelable;
        try {
            IProjectionDelegate iProjectionDelegate = this.A00;
            BinderC56502l7 r0 = new BinderC56502l7(point);
            C65873Li r2 = (C65873Li) iProjectionDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            Parcel A02 = r2.A02(1, A01);
            Parcelable.Creator creator = LatLng.CREATOR;
            if (A02.readInt() == 0) {
                parcelable = null;
            } else {
                parcelable = (Parcelable) creator.createFromParcel(A02);
            }
            LatLng latLng = (LatLng) parcelable;
            A02.recycle();
            return latLng;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public C56442kt A02() {
        Parcelable parcelable;
        try {
            C65873Li r2 = (C65873Li) this.A00;
            Parcel A02 = r2.A02(3, r2.A01());
            Parcelable.Creator creator = C56442kt.CREATOR;
            if (A02.readInt() == 0) {
                parcelable = null;
            } else {
                parcelable = (Parcelable) creator.createFromParcel(A02);
            }
            C56442kt r0 = (C56442kt) parcelable;
            A02.recycle();
            return r0;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
