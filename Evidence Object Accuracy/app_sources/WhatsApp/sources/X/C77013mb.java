package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.3mb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77013mb extends AbstractC107404xH {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(13);
    public final String A00;
    public final boolean A01;
    public final boolean A02;
    public final AbstractC107404xH[] A03;
    public final String[] A04;

    /* JADX DEBUG: Multi-variable search result rejected for r1v2, resolved type: X.4xH[] */
    /* JADX WARN: Multi-variable type inference failed */
    public C77013mb(Parcel parcel) {
        super("CTOC");
        this.A00 = parcel.readString();
        boolean z = true;
        this.A02 = C12960it.A1S(parcel.readByte());
        this.A01 = parcel.readByte() == 0 ? false : z;
        this.A04 = parcel.createStringArray();
        int readInt = parcel.readInt();
        this.A03 = new AbstractC107404xH[readInt];
        for (int i = 0; i < readInt; i++) {
            this.A03[i] = C12990iw.A0I(parcel, AbstractC107404xH.class);
        }
    }

    public C77013mb(String str, AbstractC107404xH[] r3, String[] strArr, boolean z, boolean z2) {
        super("CTOC");
        this.A00 = str;
        this.A02 = z;
        this.A01 = z2;
        this.A04 = strArr;
        this.A03 = r3;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C77013mb.class != obj.getClass()) {
                return false;
            }
            C77013mb r5 = (C77013mb) obj;
            if (this.A02 != r5.A02 || this.A01 != r5.A01 || !AnonymousClass3JZ.A0H(this.A00, r5.A00) || !Arrays.equals(this.A04, r5.A04) || !Arrays.equals(this.A03, r5.A03)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return ((C72453ed.A05(this.A02 ? 1 : 0) + (this.A01 ? 1 : 0)) * 31) + C72453ed.A0E(this.A00);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A00);
        parcel.writeByte(this.A02 ? (byte) 1 : 0);
        parcel.writeByte(this.A01 ? (byte) 1 : 0);
        parcel.writeStringArray(this.A04);
        AbstractC107404xH[] r4 = this.A03;
        int length = r4.length;
        parcel.writeInt(length);
        for (AbstractC107404xH r0 : r4) {
            parcel.writeParcelable(r0, 0);
        }
    }
}
