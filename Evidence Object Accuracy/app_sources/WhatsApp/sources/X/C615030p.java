package X;

/* renamed from: X.30p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C615030p extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Integer A02;
    public Long A03;
    public String A04;
    public String A05;

    public C615030p() {
        super(2638, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(7, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(6, this.A04);
        r3.Abe(2, this.A03);
        r3.Abe(5, this.A05);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamOfflineCountTooHigh {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "callStanzaType", C12960it.A0Y(this.A00));
        String str = null;
        Integer num = this.A01;
        if (num != null) {
            str = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "messageType", str);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "notificationStanzaType", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "offlineCount", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "receiptStanzaType", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "stanzaType", C12960it.A0Y(this.A02));
        return C12960it.A0d("}", A0k);
    }
}
