package X;

import com.whatsapp.jid.UserJid;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1E4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1E4 {
    public final C14650lo A00;
    public final AnonymousClass1F8 A01;
    public final AnonymousClass1F5 A02;
    public final AnonymousClass12E A03;
    public final AnonymousClass1F3 A04;
    public final AnonymousClass1F6 A05;
    public final AnonymousClass1F7 A06;
    public final AnonymousClass12C A07;
    public final AnonymousClass1F4 A08;
    public final AnonymousClass12D A09;
    public final C22410z2 A0A;

    public AnonymousClass1E4(C14650lo r1, AnonymousClass1F8 r2, AnonymousClass1F5 r3, AnonymousClass12E r4, AnonymousClass1F3 r5, AnonymousClass1F6 r6, AnonymousClass1F7 r7, AnonymousClass12C r8, AnonymousClass1F4 r9, AnonymousClass12D r10, C22410z2 r11) {
        this.A04 = r5;
        this.A0A = r11;
        this.A00 = r1;
        this.A08 = r9;
        this.A02 = r3;
        this.A05 = r6;
        this.A06 = r7;
        this.A01 = r2;
        this.A07 = r8;
        this.A09 = r10;
        this.A03 = r4;
    }

    public static void A00(C28231Mf r6, C28501No r7, List list, long j) {
        C42061ub r0 = r6.A01;
        C42071uc.A00(r0, r7);
        r6.A08.A02(r0.A00, r7, list, r6.A0N, r6.A0O, j);
    }

    public void A01(C42051ua r9, C42081ud r10, C15370n3 r11, long j) {
        UserJid userJid = r9.A0C;
        AnonymousClass009.A05(userJid);
        if (userJid.equals(r11.A0D)) {
            A02(r10, null, Collections.singletonList(new AnonymousClass1Mg(r11).A01()), Collections.singletonMap(r9.A0C, r9), null, j);
            return;
        }
        StringBuilder sb = new StringBuilder("jid doesn't match, jid1=");
        sb.append(r9.A0C);
        sb.append(", jid2=");
        sb.append(r11.A0D);
        throw new IllegalArgumentException(sb.toString());
    }

    /*  JADX ERROR: NullPointerException in pass: RegionMakerVisitor
        java.lang.NullPointerException
        */
    public void A02(X.C42081ud r34, X.C28501No r35, java.util.List r36, java.util.Map r37, java.util.Map r38, long r39) {
        /*
        // Method dump skipped, instructions count: 2314
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1E4.A02(X.1ud, X.1No, java.util.List, java.util.Map, java.util.Map, long):void");
    }
}
