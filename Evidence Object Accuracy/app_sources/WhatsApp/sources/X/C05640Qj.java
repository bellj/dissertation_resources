package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.0Qj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05640Qj {
    public static int A00(Drawable drawable) {
        return drawable.getLayoutDirection();
    }

    public static boolean A01(int i, Drawable drawable) {
        return drawable.setLayoutDirection(i);
    }
}
