package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;

/* renamed from: X.3Ll  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Ll implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        AnonymousClass1IR A01;
        int readInt = parcel.readInt();
        int readInt2 = parcel.readInt();
        long readLong = parcel.readLong();
        String readString = parcel.readString();
        int readInt3 = parcel.readInt();
        String readString2 = parcel.readString();
        String readString3 = parcel.readString();
        UserJid nullable = UserJid.getNullable(parcel.readString());
        UserJid nullable2 = UserJid.getNullable(parcel.readString());
        String readString4 = parcel.readString();
        String readString5 = parcel.readString();
        String readString6 = parcel.readString();
        String readString7 = parcel.readString();
        ArrayList readArrayList = parcel.readArrayList(AnonymousClass20Z.class.getClassLoader());
        UserJid nullable3 = UserJid.getNullable(parcel.readString());
        boolean A1S = C12960it.A1S(parcel.readInt());
        long readLong2 = parcel.readLong();
        String readString8 = parcel.readString();
        String readString9 = parcel.readString();
        int readInt4 = parcel.readInt();
        boolean A1V = C12960it.A1V(parcel.readInt(), 1);
        C30921Zi r2 = (C30921Zi) C12990iw.A0I(parcel, C30921Zi.class);
        int readInt5 = parcel.readInt();
        int readInt6 = parcel.readInt();
        byte[] bArr = new byte[readInt6];
        if (readInt6 > 0) {
            parcel.readByteArray(bArr);
        }
        int readInt7 = parcel.readInt();
        AbstractC30891Zf r1 = (AbstractC30891Zf) C12990iw.A0I(parcel, AbstractC30891Zf.class);
        AbstractC30791Yv A00 = AnonymousClass102.A00(parcel);
        if (readInt == 5) {
            AnonymousClass1IR r0 = new AnonymousClass1IR(readString9, 5, readInt4, readLong2);
            r0.A0R = bArr;
            r0.A0L = readString4;
            r0.A0C = nullable3;
            r0.A0Q = A1S;
            r0.A0P = A1V;
            r0.A07 = A00;
            return r0;
        }
        C30821Yy A002 = C30821Yy.A00(readString2, readInt3);
        if (readInt != 4) {
            A01 = new AnonymousClass1IR(A00, A002, nullable, nullable2, readString, readString3, readString5, readString6, readString7, null, readString9, readInt, readInt2, readInt4, readInt7, readInt5, readLong2, readLong);
            A01.A0L = readString4;
            A01.A0N = readArrayList;
            A01.A0C = nullable3;
            A01.A0Q = A1S;
            A01.A0A = r1;
            A01.A07 = A00;
        } else {
            A01 = C31001Zq.A01(readLong2);
        }
        A01.A0M = readString8;
        A01.A0P = A1V;
        A01.A05(r2);
        return A01;
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new AnonymousClass1IR[i];
    }
}
