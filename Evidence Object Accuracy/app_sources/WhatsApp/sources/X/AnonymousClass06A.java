package X;

import java.util.Locale;

/* renamed from: X.06A  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass06A {
    public int A00;
    public AnonymousClass02T A01;
    public boolean A02;

    public AnonymousClass06A() {
        this.A02 = AnonymousClass069.A00(Locale.getDefault()) != 1 ? false : true;
        this.A01 = AnonymousClass02S.A05;
        this.A00 = 2;
    }

    public AnonymousClass06A(boolean z) {
        this.A02 = z;
        this.A01 = AnonymousClass02S.A05;
        this.A00 = 2;
    }

    public AnonymousClass02S A00() {
        int i = this.A00;
        if (i != 2 || this.A01 != AnonymousClass02S.A05) {
            return new AnonymousClass02S(this.A01, i, this.A02);
        } else if (this.A02) {
            return AnonymousClass02S.A04;
        } else {
            return AnonymousClass02S.A03;
        }
    }
}
