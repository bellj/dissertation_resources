package X;

import android.location.Location;
import android.util.Pair;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: X.15q  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C244715q {
    public final AbstractC15710nm A00;
    public final C244615p A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final C14830m7 A04;
    public final AnonymousClass15L A05;
    public final C19990v2 A06;
    public final C20830wO A07;
    public final C15600nX A08;
    public final C20040v7 A09;
    public final C21250x7 A0A;
    public final C22290yq A0B;
    public final C16030oK A0C;
    public final C244415n A0D;
    public final C244515o A0E;
    public final C15860o1 A0F;

    public C244715q(AbstractC15710nm r2, C244615p r3, C15570nT r4, C15450nH r5, C14830m7 r6, AnonymousClass15L r7, C19990v2 r8, C20830wO r9, C15600nX r10, C20040v7 r11, C21250x7 r12, C22290yq r13, C16030oK r14, C244415n r15, C244515o r16, C15860o1 r17) {
        this.A04 = r6;
        this.A00 = r2;
        this.A02 = r4;
        this.A06 = r8;
        this.A03 = r5;
        this.A0A = r12;
        this.A09 = r11;
        this.A0D = r15;
        this.A0B = r13;
        this.A0F = r17;
        this.A0E = r16;
        this.A01 = r3;
        this.A0C = r14;
        this.A05 = r7;
        this.A08 = r10;
        this.A07 = r9;
    }

    public Pair A00(AbstractC14640lm r12) {
        AnonymousClass1IS r1;
        List A03;
        AbstractC15340mz A032;
        C22290yq r5 = this.A0B;
        int A00 = r5.A02.A00(r12);
        List list = null;
        if (A00 > 0 && (A032 = r5.A03.A03(r12, A00)) != null && (list = r5.A02(r12, 20, null, A032.A12 - 1)) == null) {
            list = new ArrayList();
        }
        if (list == null || list.isEmpty()) {
            r1 = null;
        } else {
            r1 = ((AbstractC15340mz) list.get(0)).A0z;
            if (!(r1 == null || (A03 = r5.A03(r1, null, 20)) == null)) {
                list.addAll(0, A03);
            }
        }
        return Pair.create(r1, list);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008f, code lost:
        if (r2 == -2) goto L_0x0091;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AnonymousClass1JW A01(X.AbstractC14640lm r9) {
        /*
            r8 = this;
            X.0nm r1 = r8.A00
            X.0nH r0 = r8.A03
            X.1JW r4 = new X.1JW
            r4.<init>(r1, r0)
            X.0v2 r5 = r8.A06
            long r0 = r5.A05(r9)
            r4.A0A = r0
            java.util.concurrent.ConcurrentHashMap r0 = r5.A0B()
            java.lang.Object r0 = r0.get(r9)
            X.1PE r0 = (X.AnonymousClass1PE) r0
            if (r0 != 0) goto L_0x00cc
            r0 = 0
        L_0x001e:
            r4.A04 = r0
            r4.A0C = r9
            X.0o1 r7 = r8.A0F
            java.lang.String r0 = r9.getRawString()
            X.1da r0 = r7.A08(r0)
            long r0 = r0.A00()
            r4.A08 = r0
            int r0 = r5.A00(r9)
            r4.A01 = r0
            boolean r0 = r5.A0E(r9)
            r4.A0O = r0
            X.0wO r0 = r8.A07
            X.0n3 r6 = r0.A01(r9)
            java.lang.String r0 = r6.A0K
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            r0 = 0
            if (r1 != 0) goto L_0x004f
            java.lang.String r0 = r6.A0K
        L_0x004f:
            r4.A0I = r0
            boolean r0 = r6.A0K()
            if (r0 == 0) goto L_0x0064
            X.0nX r1 = r8.A08
            r0 = r9
            com.whatsapp.jid.GroupJid r0 = (com.whatsapp.jid.GroupJid) r0
            boolean r0 = r1.A0C(r0)
            r0 = r0 ^ 1
            r4.A0S = r0
        L_0x0064:
            com.whatsapp.jid.UserJid r3 = com.whatsapp.jid.UserJid.of(r9)
            if (r3 == 0) goto L_0x007e
            X.15L r0 = r8.A05
            X.1Xs r2 = r0.A00(r3)
            if (r2 == 0) goto L_0x007e
            com.whatsapp.jid.UserJid r0 = r2.A00
            boolean r1 = r9.equals(r0)
            com.whatsapp.jid.UserJid r0 = r2.A01
            if (r1 == 0) goto L_0x00c1
            r4.A0E = r0
        L_0x007e:
            long r0 = r7.A02(r9)
            r4.A09 = r0
            X.0x7 r0 = r8.A0A
            int r2 = r0.A00(r9)
            r0 = -1
            if (r2 == r0) goto L_0x0091
            r1 = -2
            r0 = 0
            if (r2 != r1) goto L_0x0092
        L_0x0091:
            r0 = 1
        L_0x0092:
            r0 = r0 ^ 1
            r4.A0Q = r0
            if (r3 == 0) goto L_0x00bc
            X.1PG r1 = r5.A08(r3)
            if (r1 == 0) goto L_0x00a9
            int r0 = r1.expiration
            r4.A03 = r0
            long r2 = r1.ephemeralSettingTimestamp
            r0 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r0
            r4.A07 = r2
        L_0x00a9:
            java.util.concurrent.ConcurrentHashMap r0 = r5.A0B()
            java.lang.Object r0 = r0.get(r9)
            X.1PE r0 = (X.AnonymousClass1PE) r0
            if (r0 != 0) goto L_0x00b9
            r0 = 0
        L_0x00b6:
            r4.A00 = r0
            return r4
        L_0x00b9:
            int r0 = r0.A04
            goto L_0x00b6
        L_0x00bc:
            int r0 = r6.A01
            r4.A03 = r0
            goto L_0x00a9
        L_0x00c1:
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x007e
            com.whatsapp.jid.UserJid r0 = r2.A00
            r4.A0D = r0
            goto L_0x007e
        L_0x00cc:
            int r0 = r0.A0A
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C244715q.A01(X.0lm):X.1JW");
    }

    public boolean A02(AbstractC14640lm r22, String str, long j, boolean z) {
        HashMap hashMap;
        long j2;
        C35221hT r1;
        C30751Yr r2;
        long j3;
        if (j > 0) {
            hashMap = new HashMap();
            hashMap.put("duration", Long.toString(j));
        } else {
            hashMap = null;
        }
        ArrayList arrayList = new ArrayList();
        C16030oK r5 = this.A0C;
        if (r5.A0f(r22)) {
            byte[] bArr = null;
            if (!z) {
                return false;
            }
            Location A01 = this.A01.A01("web-live-location-participants");
            if (A01 != null) {
                C244415n r12 = this.A0D;
                r2 = r12.A05(A01);
                bArr = r12.A04(r2, null).A02();
            } else {
                r2 = null;
            }
            AbstractC15710nm r13 = this.A00;
            C15450nH r14 = this.A03;
            C15570nT r15 = this.A02;
            r15.A08();
            C27621Ig r16 = r15.A01;
            AnonymousClass009.A05(r16);
            AbstractC14640lm r152 = (AbstractC14640lm) r16.A0D;
            AnonymousClass009.A05(r152);
            long A03 = r5.A03(r22) / 1000;
            if (r2 != null) {
                j3 = (this.A04.A00() - r2.A05) / 1000;
            } else {
                j3 = -1;
            }
            arrayList.add(new C37391mJ(r13, r14, r152, bArr, A03, j3));
        }
        Iterator it = r5.A07(r22).iterator();
        while (it.hasNext()) {
            C30751Yr r4 = (C30751Yr) it.next();
            AbstractC15710nm r132 = this.A00;
            C15450nH r142 = this.A03;
            UserJid userJid = r4.A06;
            synchronized (r5.A0W) {
                Map map = (Map) r5.A0A().get(r22);
                j2 = (map == null || (r1 = (C35221hT) map.get(userJid)) == null) ? -1 : r1.A00;
            }
            arrayList.add(new C37391mJ(r132, r142, userJid, null, j2 / 1000, (this.A04.A00() - r4.A05) / 1000));
        }
        this.A0E.A02(str, arrayList, Collections.emptyMap(), hashMap, 29, false);
        return true;
    }
}
