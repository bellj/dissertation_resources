package X;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.message.KeptMessagesActivity;
import com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import com.whatsapp.util.ViewOnClickCListenerShape4S0200000_I0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.1hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractActivityC35431hr extends AbstractActivityC35441hs implements AbstractC13890kV, AnonymousClass03M {
    public MenuItem A00;
    public C239613r A01;
    public AnonymousClass12T A02;
    public C22330yu A03;
    public AnonymousClass10S A04;
    public C22700zV A05;
    public AnonymousClass1J1 A06;
    public C53632el A07;
    public AnonymousClass1A6 A08;
    public C255419u A09;
    public AnonymousClass15Q A0A;
    public C25641Ae A0B;
    public C15240mn A0C;
    public C16490p7 A0D;
    public C22100yW A0E;
    public C22180yf A0F;
    public C231510o A0G;
    public C244215l A0H;
    public AbstractC14640lm A0I;
    public C16630pM A0J;
    public C88054Ec A0K;
    public AnonymousClass12U A0L;
    public C23000zz A0M;
    public AnonymousClass01H A0N;
    public String A0O;
    public ArrayList A0P;
    public final AbsListView.OnScrollListener A0Q = new AnonymousClass3O4(this);
    public final AnonymousClass2Dn A0R = new AnonymousClass41X(this);
    public final C27131Gd A0S = new AnonymousClass420(this);
    public final AbstractC33331dp A0T = new C857944e(this);

    public void A2o() {
        Bundle bundle;
        if (!TextUtils.isEmpty(this.A0O)) {
            bundle = new Bundle();
            bundle.putString("query", this.A0O);
        } else {
            bundle = null;
        }
        A0W().A00(bundle, this);
    }

    public void A2p() {
        int i;
        View view;
        int i2;
        View findViewById;
        if (!(this instanceof StarredMessagesActivity)) {
            KeptMessagesActivity keptMessagesActivity = (KeptMessagesActivity) this;
            ScrollView scrollView = keptMessagesActivity.A01;
            if (scrollView != null && keptMessagesActivity.A02 != null && keptMessagesActivity.A00 != null) {
                i2 = 0;
                i = 8;
                if (((AnonymousClass0BR) ((AbstractActivityC35431hr) keptMessagesActivity).A07).A02 == null) {
                    scrollView.setVisibility(8);
                    keptMessagesActivity.A02.setVisibility(8);
                    findViewById = keptMessagesActivity.A00;
                    findViewById.setVisibility(i2);
                    return;
                }
                ArrayList arrayList = ((AbstractActivityC35431hr) keptMessagesActivity).A0P;
                if (arrayList == null || arrayList.isEmpty()) {
                    scrollView.setVisibility(0);
                    keptMessagesActivity.A02.setVisibility(8);
                } else {
                    scrollView.setVisibility(8);
                    keptMessagesActivity.A02.setVisibility(0);
                    keptMessagesActivity.A02.setText(keptMessagesActivity.getString(R.string.search_no_results, ((AbstractActivityC35431hr) keptMessagesActivity).A0O));
                }
                view = keptMessagesActivity.A00;
                view.setVisibility(i);
            }
            return;
        }
        i2 = 0;
        i = 8;
        if (((AnonymousClass0BR) this.A07).A02 == null) {
            findViewById(R.id.empty_view).setVisibility(8);
            findViewById(R.id.search_no_matches).setVisibility(8);
            findViewById = findViewById(R.id.progress);
            findViewById.setVisibility(i2);
            return;
        }
        ArrayList arrayList2 = this.A0P;
        if (arrayList2 == null || arrayList2.isEmpty()) {
            findViewById(R.id.empty_view).setVisibility(0);
            findViewById(R.id.search_no_matches).setVisibility(8);
        } else {
            findViewById(R.id.empty_view).setVisibility(8);
            TextView textView = (TextView) findViewById(R.id.search_no_matches);
            textView.setVisibility(0);
            textView.setText(getString(R.string.search_no_results, this.A0O));
        }
        view = findViewById(R.id.progress);
        view.setVisibility(i);
    }

    @Override // X.AnonymousClass03M
    public AnonymousClass0QL AOi(Bundle bundle, int i) {
        AbstractC20010v4 r3;
        if (!(this instanceof StarredMessagesActivity)) {
            r3 = super.A0P;
        } else {
            r3 = super.A0S;
        }
        return new C54002fw(this, r3, this.A0I, bundle == null ? null : bundle.getString("query"));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
        if (r0 != null) goto L_0x002c;
     */
    @Override // X.AnonymousClass03M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ void AS2(X.AnonymousClass0QL r4, java.lang.Object r5) {
        /*
            r3 = this;
            android.database.Cursor r5 = (android.database.Cursor) r5
            X.2el r0 = r3.A07
            r0.A00(r5)
            r3.A2p()
            java.lang.String r0 = r3.A0O
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x003d
            X.2el r0 = r3.A07
            boolean r1 = r0.isEmpty()
            android.view.MenuItem r0 = r3.A00
            if (r1 == 0) goto L_0x003e
            r2 = 0
            if (r0 == 0) goto L_0x002f
            boolean r0 = r0.isActionViewExpanded()
            if (r0 == 0) goto L_0x002a
            android.view.MenuItem r0 = r3.A00
            r0.collapseActionView()
        L_0x002a:
            android.view.MenuItem r0 = r3.A00
        L_0x002c:
            r0.setVisible(r2)
        L_0x002f:
            r1 = r3
            boolean r0 = r3 instanceof com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity
            if (r0 == 0) goto L_0x003d
            com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity r1 = (com.whatsapp.conversation.conversationrow.message.StarredMessagesActivity) r1
            android.view.MenuItem r0 = r1.A00
            if (r0 == 0) goto L_0x003d
            r0.setVisible(r2)
        L_0x003d:
            return
        L_0x003e:
            r2 = 1
            if (r0 == 0) goto L_0x002f
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC35431hr.AS2(X.0QL, java.lang.Object):void");
    }

    @Override // X.AnonymousClass03M
    public void AS8(AnonymousClass0QL r3) {
        this.A07.A00(null);
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC000900k, X.ActivityC001000l, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        String str;
        if (i != 2) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            Collection A2h = A2h();
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("jids");
            if (A2h.isEmpty() || stringArrayListExtra == null) {
                StringBuilder sb = new StringBuilder();
                if (!(this instanceof StarredMessagesActivity)) {
                    str = "kept";
                } else {
                    str = "starred";
                }
                sb.append(str);
                sb.append("/forward/failed");
                Log.w(sb.toString());
                ((ActivityC13810kN) this).A05.A07(R.string.message_forward_failed, 0);
            } else {
                List A07 = C15380n4.A07(AbstractC14640lm.class, stringArrayListExtra);
                C32731ce r6 = null;
                if (AnonymousClass4ZH.A00(((ActivityC13810kN) this).A0C, A07)) {
                    AnonymousClass009.A05(intent);
                    r6 = (C32731ce) intent.getParcelableExtra("status_distribution");
                }
                ArrayList arrayList = new ArrayList(A2h);
                Collections.sort(arrayList, new C45041zy());
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((AbstractActivityC13750kH) this).A03.A06(this.A01, r6, (AbstractC15340mz) it.next(), A07);
                }
                if (A07.size() != 1 || C15380n4.A0N((Jid) A07.get(0))) {
                    A2a(A07);
                } else {
                    ((ActivityC13790kL) this).A00.A07(this, new C14960mK().A0g(this, ((AbstractActivityC13750kH) this).A07.A0B((AbstractC14640lm) A07.get(0))));
                }
            }
            A2j();
        }
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        String str;
        C53632el r5;
        super.onCreate(bundle);
        A29();
        AbstractC005102i A1U = A1U();
        AnonymousClass009.A05(A1U);
        A1U.A0M(true);
        this.A04.A03(this.A0S);
        this.A03.A03(this.A0R);
        this.A0H.A03(this.A0T);
        C21270x9 r4 = ((AbstractActivityC13750kH) this).A0B;
        StringBuilder sb = new StringBuilder();
        if (!(this instanceof StarredMessagesActivity)) {
            str = "kept";
        } else {
            str = "starred";
        }
        sb.append(str);
        sb.append("-messages-activity");
        this.A06 = r4.A04(this, sb.toString());
        C15570nT r0 = ((ActivityC13790kL) this).A01;
        r0.A08();
        if (r0.A00 != null) {
            C16490p7 r02 = this.A0D;
            r02.A04();
            if (r02.A01 && ((ActivityC13790kL) this).A0B.A01()) {
                this.A0I = AbstractC14640lm.A01(getIntent().getStringExtra("jid"));
                this.A0A.A00(bundle);
                this.A0B.A02(this.A0I, getClass().getName());
                boolean z = this instanceof KeptMessagesActivity;
                AnonymousClass12P r1 = ((ActivityC13790kL) this).A00;
                if (!z) {
                    ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I0 = new ViewOnClickCListenerShape4S0200000_I0(this, 19, r1);
                    r5 = new C53632el(this, ((ActivityC13790kL) this).A01, ((AbstractActivityC13750kH) this).A07, this.A06, ((AbstractActivityC13750kH) this).A0F, this, ((AbstractActivityC13750kH) this).A0M, viewOnClickCListenerShape4S0200000_I0);
                } else {
                    ViewOnClickCListenerShape4S0200000_I0 viewOnClickCListenerShape4S0200000_I02 = new ViewOnClickCListenerShape4S0200000_I0(this, 19, r1);
                    r5 = new AnonymousClass2z8(this, ((ActivityC13790kL) this).A01, ((AbstractActivityC13750kH) this).A07, ((AbstractActivityC13750kH) this).A09, this.A06, ((AbstractActivityC13750kH) this).A0F, this, ((AbstractActivityC13750kH) this).A0M, viewOnClickCListenerShape4S0200000_I02);
                }
                this.A07 = r5;
                A0W().A02(this);
                return;
            }
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("/create/no-me-or-msgstore-db");
        Log.i(sb2.toString());
        startActivity(C14960mK.A04(this));
        finish();
    }

    @Override // X.ActivityC13790kL, android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.A0C.A0O()) {
            AbstractC005102i A1U = A1U();
            AnonymousClass009.A05(A1U);
            SearchView searchView = new SearchView(A1U.A02());
            searchView.setMaxWidth(Integer.MAX_VALUE);
            ((TextView) searchView.findViewById(R.id.search_src_text)).setTextColor(getResources().getColor(R.color.search_text_color_dark));
            searchView.setQueryHint(getString(R.string.search_hint));
            searchView.A0B = new C66773Ow(this);
            MenuItem icon = menu.add(0, R.id.menuitem_search, 0, R.string.search).setIcon(R.drawable.ic_action_search);
            this.A00 = icon;
            icon.setVisible(!((ActivityC13770kJ) this).A00.isEmpty());
            this.A00.setActionView(searchView);
            this.A00.setShowAsAction(10);
            this.A00.setOnActionExpandListener(new MenuItem$OnActionExpandListenerC100784mT(this));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13770kJ, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.A06.A00();
        this.A04.A04(this.A0S);
        this.A03.A04(this.A0R);
        this.A0H.A04(this.A0T);
        ((AbstractActivityC13750kH) this).A0I.A06();
        if (isFinishing()) {
            this.A0B.A03(this.A0I, getClass().getName());
        }
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000900k, android.app.Activity
    public void onPause() {
        super.onPause();
        if (((AbstractActivityC13750kH) this).A0I.A0B()) {
            ((AbstractActivityC13750kH) this).A0I.A03();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.AbstractActivityC13840kQ, X.ActivityC000900k, android.app.Activity
    public void onResume() {
        super.onResume();
        if (((AbstractActivityC13750kH) this).A0I.A0B()) {
            ((AbstractActivityC13750kH) this).A0I.A05();
        }
        this.A07.notifyDataSetChanged();
    }

    @Override // X.AbstractActivityC13750kH, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        this.A0A.A01(bundle);
        super.onSaveInstanceState(bundle);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean onSearchRequested() {
        MenuItem menuItem = this.A00;
        if (menuItem == null) {
            return false;
        }
        menuItem.expandActionView();
        return false;
    }
}
