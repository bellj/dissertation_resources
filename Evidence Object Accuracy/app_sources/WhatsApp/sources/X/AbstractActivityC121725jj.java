package X;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.HomeActivity;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPaymentSettingsFragment;
import com.whatsapp.payments.ui.IndiaUpiPaymentSettingsActivity;
import com.whatsapp.payments.ui.IndiaUpiPaymentSettingsFragment;
import com.whatsapp.payments.ui.NoviSharedPaymentSettingsActivity;
import com.whatsapp.payments.ui.NoviSharedPaymentSettingsFragment;
import com.whatsapp.payments.ui.PaymentSettingsFragment;

/* renamed from: X.5jj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121725jj extends AbstractActivityC119285dT {
    public C22180yf A00;
    public C22710zW A01;
    public PaymentSettingsFragment A02;
    public final C30931Zj A03 = C117305Zk.A0V("PaymentSettingsActivity", "payment-settings");

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if ("api.whatsapp.com".equals(r1) != false) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A2e(java.lang.String r5) {
        /*
            r4 = this;
            android.net.Uri r1 = android.net.Uri.parse(r5)
            java.util.List r0 = r1.getPathSegments()
            int r2 = r0.size()
            java.lang.String r0 = r1.getScheme()
            java.lang.String r1 = r1.getHost()
            boolean r0 = X.C22180yf.A04(r0, r1)
            if (r0 != 0) goto L_0x0023
            java.lang.String r0 = "api.whatsapp.com"
            boolean r1 = r0.equals(r1)
            r0 = 0
            if (r1 == 0) goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            if (r2 <= r0) goto L_0x0048
            r1 = 2131889217(0x7f120c41, float:1.9413091E38)
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]
            X.2AC r3 = com.whatsapp.MessageDialogFragment.A01(r0, r1)
            r2 = 2131890036(0x7f120f74, float:1.9414752E38)
            r1 = 6
            com.facebook.redex.IDxCListenerShape5S0000000_3_I1 r0 = new com.facebook.redex.IDxCListenerShape5S0000000_3_I1
            r0.<init>(r1)
            r3.A02(r0, r2)
            androidx.fragment.app.DialogFragment r2 = r3.A01()
            X.01F r1 = r4.A0V()
            r0 = 0
            r2.A1F(r1, r0)
        L_0x0048:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractActivityC121725jj.A2e(java.lang.String):void");
    }

    public boolean A2f() {
        if (!isTaskRoot()) {
            return false;
        }
        Intent A0D = C12990iw.A0D(this, HomeActivity.class);
        if (Build.VERSION.SDK_INT >= 21) {
            finishAndRemoveTask();
            startActivity(A0D);
            return true;
        }
        startActivity(A0D);
        return false;
    }

    @Override // X.ActivityC13810kN, X.ActivityC001000l, android.app.Activity
    public void onBackPressed() {
        AbstractC118085bF r7;
        PaymentSettingsFragment paymentSettingsFragment = this.A02;
        if (!(paymentSettingsFragment == null || (r7 = paymentSettingsFragment.A0t) == null)) {
            AnonymousClass2S0 r6 = paymentSettingsFragment.A0m;
            if (!(r7 instanceof C123505nG)) {
                AnonymousClass61I.A01(AnonymousClass61I.A00(r7.A04, null, r6, null, false), r7.A08, 1, "payment_home", null, 1);
            } else {
                C123505nG r72 = (C123505nG) r7;
                AbstractC16870pt r5 = ((AbstractC118085bF) r72).A08;
                if (r5 instanceof AnonymousClass6BE) {
                    AnonymousClass6BE r52 = (AnonymousClass6BE) r5;
                    Integer A0V = C12960it.A0V();
                    AnonymousClass6BE.A00(r52.A02(A0V, A0V, "payment_home", null), AnonymousClass61I.A00(((AbstractC118085bF) r72).A04, null, r6, null, false), r52, r72.A0D());
                }
            }
        }
        if (!A2f()) {
            super.onBackPressed();
        }
    }

    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        PaymentSettingsFragment noviSharedPaymentSettingsFragment;
        super.onCreate(bundle);
        setContentView(R.layout.payment_settings);
        if (!this.A01.A07()) {
            this.A03.A06("onCreate payment is not enabled; finish");
            finish();
            return;
        }
        AbstractC005102i A1U = A1U();
        if (A1U != null) {
            C117305Zk.A16(A1U, R.string.payments_activity_title);
        }
        Intent intent = getIntent();
        if (this instanceof NoviSharedPaymentSettingsActivity) {
            noviSharedPaymentSettingsFragment = new NoviSharedPaymentSettingsFragment();
        } else if (!(this instanceof IndiaUpiPaymentSettingsActivity)) {
            noviSharedPaymentSettingsFragment = new BrazilPaymentSettingsFragment();
        } else {
            noviSharedPaymentSettingsFragment = new IndiaUpiPaymentSettingsFragment();
        }
        this.A02 = noviSharedPaymentSettingsFragment;
        if (bundle == null) {
            if (!(intent == null || intent.getExtras() == null)) {
                Bundle bundle2 = new Bundle(intent.getExtras());
                Bundle bundle3 = ((AnonymousClass01E) this.A02).A05;
                if (bundle3 != null) {
                    bundle2.putAll(bundle3);
                }
                this.A02.A0U(bundle2);
            }
            C004902f r3 = new C004902f(A0V());
            r3.A0A(this.A02, null, R.id.payment_settings_fragment_container);
            r3.A01();
        }
        if (intent != null) {
            String stringExtra = intent.getStringExtra("actual_deep_link");
            if (!TextUtils.isEmpty(stringExtra)) {
                A2e(stringExtra);
            }
        }
    }

    @Override // X.ActivityC000900k, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        PaymentSettingsFragment paymentSettingsFragment = this.A02;
        if (paymentSettingsFragment != null) {
            paymentSettingsFragment.A1L(intent);
        }
    }
}
