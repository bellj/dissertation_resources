package X;

import android.os.Handler;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.whatsapp.mediacomposer.doodle.ColorPickerComponent;
import com.whatsapp.mediacomposer.doodle.ColorPickerView;
import com.whatsapp.mediacomposer.doodle.titlebar.TitleBarView;

/* renamed from: X.3YI  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YI implements AnonymousClass5Yv {
    public final Runnable A00;
    public final /* synthetic */ AnonymousClass2Ab A01;

    public AnonymousClass3YI(AnonymousClass2Ab r1, Runnable runnable) {
        this.A01 = r1;
        this.A00 = runnable;
    }

    @Override // X.AnonymousClass5W5
    public void APL() {
        AnonymousClass2Ab r2 = this.A01;
        r2.A0Q.A08 = true;
        if (r2.A0J.A02 || r2.A0K.A01 != null) {
            Handler handler = r2.A0A;
            Runnable runnable = this.A00;
            handler.removeCallbacks(runnable);
            handler.postDelayed(runnable, 400);
        }
    }

    @Override // X.AnonymousClass5W5
    public void APM() {
        AnonymousClass2Ab r4 = this.A01;
        C47342Ag r2 = r4.A0Q;
        int i = 0;
        r2.A08 = false;
        C47322Ae r7 = r4.A0J;
        if (r7.A02 || r4.A0K.A01 != null) {
            C454721t r5 = r4.A0O;
            AbstractC454821u r6 = r5.A01;
            r4.A0A.removeCallbacks(this.A00);
            if (!r4.A08()) {
                if (r6 == null) {
                    r4.A0E.A05(true);
                    r4.A03.A00();
                } else if (r7.A02) {
                    ColorPickerComponent colorPickerComponent = r4.A0E;
                    if (colorPickerComponent.A05.getVisibility() != 0) {
                        colorPickerComponent.A00();
                    }
                }
                if (r2.A00() == 2) {
                    TitleBarView titleBarView = r2.A0H;
                    Animation animation = r2.A0E;
                    if (titleBarView.A00.getVisibility() != 0) {
                        titleBarView.A00.setVisibility(0);
                        titleBarView.A00.startAnimation(animation);
                    }
                    r4.A0E.A00();
                } else {
                    r2.A04();
                }
                r4.A0H.setSystemUiVisibility(1280);
                if (!(!r5.A03.A00.isEmpty())) {
                    i = 4;
                }
                TitleBarView titleBarView2 = r2.A0H;
                titleBarView2.setUndoButtonVisibility(i);
                boolean A00 = C28141Kv.A00(r4.A0B);
                RelativeLayout relativeLayout = titleBarView2.A07;
                ImageView imageView = titleBarView2.A02;
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
                layoutParams.addRule(!A00 ? 1 : 0, imageView.getId());
                relativeLayout.setLayoutParams(layoutParams);
                r4.A03.A02();
            }
        }
    }

    @Override // X.AnonymousClass5Yv
    public void AVy(AbstractC454821u r6) {
        AnonymousClass2Ab r3 = this.A01;
        C47342Ag r4 = r3.A0Q;
        if (r4.A00() != 2) {
            boolean A0K = r6.A0K();
            if (A0K || r6.A0J()) {
                if (r4.A00() == 5) {
                    r3.A0E.A00();
                }
                if (r6.A0J()) {
                    int color = r6.A01.getColor();
                    if (color != 0) {
                        r3.A0E.setColorAndInvalidate(color);
                    }
                    ColorPickerView colorPickerView = r3.A0E.A05;
                    colorPickerView.A01();
                    colorPickerView.invalidate();
                }
                if (A0K) {
                    r3.A0E.setSizeAndInvalidate(r6.A02() * r3.A0H.A00);
                }
            } else {
                r3.A0E.A05(true);
                r3.A03.A00();
            }
            r3.A04();
        }
    }
}
