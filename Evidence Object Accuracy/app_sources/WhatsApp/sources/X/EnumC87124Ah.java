package X;

/* renamed from: X.4Ah  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public enum EnumC87124Ah {
    /* Fake field, exist only in values array */
    EF5(0),
    /* Fake field, exist only in values array */
    EF13(1);
    
    public final int mIntValue;

    EnumC87124Ah(int i) {
        this.mIntValue = i;
    }
}
