package X;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.LinkedList;

/* renamed from: X.0o6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass0o6 {
    public final C240413z A00;

    public AnonymousClass0o6(C240413z r1) {
        this.A00 = r1;
    }

    public static final AnonymousClass1KZ A00(Cursor cursor, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        String string = cursor.getString(i3);
        String str = "";
        if (string == null) {
            string = str;
        }
        String string2 = cursor.getString(i4);
        if (string2 != null) {
            str = string2;
        }
        C37451mQ r3 = new C37451mQ();
        r3.A0B = AnonymousClass144.A01(cursor.getString(i), cursor.getString(i2));
        r3.A0D = string;
        r3.A0F = str;
        r3.A0J = new LinkedList();
        r3.A0I = new LinkedList();
        boolean z = true;
        r3.A0P = true;
        if (i5 > 0) {
            r3.A05 = cursor.getString(i5);
        }
        if (i6 > 0) {
            boolean z2 = false;
            if (cursor.getInt(i6) == 1) {
                z2 = true;
            }
            r3.A0L = z2;
        }
        if (i7 > 0) {
            if (cursor.getInt(i7) != 1) {
                z = false;
            }
            r3.A0M = z;
            r3.A0K = z;
        }
        return new AnonymousClass1KZ(r3);
    }

    public void A01(AnonymousClass1KZ r8, String str, String str2) {
        C16310on A02 = this.A00.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("authority", str);
            contentValues.put("sticker_pack_id", str2);
            contentValues.put("sticker_pack_name", r8.A0F);
            contentValues.put("sticker_pack_publisher", r8.A0H);
            String str3 = r8.A0E;
            contentValues.put("sticker_pack_image_data_hash", str3);
            int i = 1;
            int i2 = 0;
            if (r8.A0L) {
                i2 = 1;
            }
            contentValues.put("avoid_cache", Integer.valueOf(i2));
            if (!r8.A0M) {
                i = 0;
            }
            contentValues.put("is_animated_pack", Integer.valueOf(i));
            A02.A03.A06(contentValues, "third_party_whitelist_packs", 5);
            r8.A01 = r8.A08;
            r8.A02 = str3;
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public boolean A02(String str, String str2) {
        boolean z = false;
        String[] strArr = {str, str2};
        C16310on A01 = this.A00.get();
        try {
            Cursor A08 = A01.A03.A08("third_party_whitelist_packs", "authority = ? AND sticker_pack_id = ?", null, null, null, strArr);
            if (A08.getCount() > 0) {
                z = true;
            }
            A08.close();
            A01.close();
            return z;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
