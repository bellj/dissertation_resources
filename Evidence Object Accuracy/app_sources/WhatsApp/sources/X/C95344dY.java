package X;

import java.io.IOException;
import java.util.Hashtable;

/* renamed from: X.4dY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95344dY {
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0070, code lost:
        if (r1 >= r6) goto L_0x0078;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String A00(X.AnonymousClass1TN r7) {
        /*
            java.lang.String r3 = A01(r7)
            int r2 = r3.length()
            r7 = 0
            if (r2 <= 0) goto L_0x0034
            char r1 = r3.charAt(r7)
            r0 = 35
            if (r1 != r0) goto L_0x0034
            r0 = 1
            int r2 = r2 - r0
            byte[] r0 = X.C95374db.A01(r3, r2)     // Catch: IOException -> 0x0022
            X.1TL r1 = X.AnonymousClass1TL.A03(r0)     // Catch: IOException -> 0x0022
            boolean r0 = r1 instanceof X.AnonymousClass5VP
            if (r0 == 0) goto L_0x0034
            goto L_0x002e
        L_0x0022:
            r1 = move-exception
            java.lang.String r0 = "unknown encoding in name: "
            java.lang.String r0 = X.C12960it.A0b(r0, r1)
            java.lang.IllegalStateException r0 = X.C12960it.A0U(r0)
            throw r0
        L_0x002e:
            X.5VP r1 = (X.AnonymousClass5VP) r1
            java.lang.String r3 = r1.AGy()
        L_0x0034:
            java.lang.String r5 = X.AnonymousClass1T7.A00(r3)
            int r1 = r5.length()
            r0 = 2
            if (r1 < r0) goto L_0x00a7
            int r6 = r1 + -1
        L_0x0041:
            r4 = 32
            r3 = 92
            if (r7 >= r6) goto L_0x0058
            char r0 = r5.charAt(r7)
            if (r0 != r3) goto L_0x0058
            int r0 = r7 + 1
            char r0 = r5.charAt(r0)
            if (r0 != r4) goto L_0x0058
            int r7 = r7 + 2
            goto L_0x0041
        L_0x0058:
            int r2 = r7 + 1
            r1 = r6
        L_0x005b:
            if (r1 <= r2) goto L_0x006e
            int r0 = r1 + -1
            char r0 = r5.charAt(r0)
            if (r0 != r3) goto L_0x006e
            char r0 = r5.charAt(r1)
            if (r0 != r4) goto L_0x006e
            int r1 = r1 + -2
            goto L_0x005b
        L_0x006e:
            if (r7 > 0) goto L_0x0072
            if (r1 >= r6) goto L_0x0078
        L_0x0072:
            int r0 = r1 + 1
            java.lang.String r5 = r5.substring(r7, r0)
        L_0x0078:
            java.lang.String r0 = "  "
            int r0 = r5.indexOf(r0)
            if (r0 < 0) goto L_0x00a7
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            r0 = 0
            char r2 = r5.charAt(r0)
            r3.append(r2)
            r1 = 1
        L_0x008e:
            int r0 = r5.length()
            if (r1 >= r0) goto L_0x00a3
            char r0 = r5.charAt(r1)
            if (r2 != r4) goto L_0x009c
            if (r0 == r4) goto L_0x00a0
        L_0x009c:
            r3.append(r0)
            r2 = r0
        L_0x00a0:
            int r1 = r1 + 1
            goto L_0x008e
        L_0x00a3:
            java.lang.String r5 = r3.toString()
        L_0x00a7:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95344dY.A00(X.1TN):java.lang.String");
    }

    public static String A01(AnonymousClass1TN r8) {
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        if (!(r8 instanceof AnonymousClass5VP) || (r8 instanceof AnonymousClass5NR)) {
            try {
                stringBuffer.append('#');
                byte[] A0c = C72463ee.A0c(r8);
                stringBuffer.append(AnonymousClass1T7.A02(C95374db.A02(A0c, 0, A0c.length)));
            } catch (IOException unused) {
                throw C12970iu.A0f("Other value has no encoded form");
            }
        } else {
            String AGy = ((AnonymousClass5VP) r8).AGy();
            if (AGy.length() > 0 && AGy.charAt(0) == '#') {
                stringBuffer.append('\\');
            }
            stringBuffer.append(AGy);
        }
        int length = stringBuffer.length();
        int i2 = 2;
        if (!(stringBuffer.length() >= 2 && stringBuffer.charAt(0) == '\\' && stringBuffer.charAt(1) == '#')) {
            i2 = 0;
        }
        while (i2 != length) {
            char charAt = stringBuffer.charAt(i2);
            if (!(charAt == '\"' || charAt == '\\' || charAt == '+' || charAt == ',')) {
                switch (charAt) {
                    case ';':
                    case '<':
                    case '=':
                    case '>':
                        break;
                    default:
                        i2++;
                        continue;
                }
            }
            stringBuffer.insert(i2, "\\");
            i2 += 2;
            length++;
        }
        if (stringBuffer.length() > 0) {
            while (stringBuffer.length() > i && stringBuffer.charAt(i) == ' ') {
                stringBuffer.insert(i, "\\");
                i += 2;
            }
        }
        int length2 = stringBuffer.length() - 1;
        while (length2 >= 0 && stringBuffer.charAt(length2) == ' ') {
            stringBuffer.insert(length2, '\\');
            length2--;
        }
        return stringBuffer.toString();
    }

    public static void A02(StringBuffer stringBuffer, Hashtable hashtable, AnonymousClass5MW r4) {
        AnonymousClass1TK r1 = r4.A01;
        String str = (String) hashtable.get(r1);
        if (str == null) {
            str = r1.A01;
        }
        stringBuffer.append(str);
        stringBuffer.append('=');
        stringBuffer.append(A01(r4.A00));
    }

    public static void A03(StringBuffer stringBuffer, Hashtable hashtable, C114605Mj r6) {
        if (r6.A00.A01.length > 1) {
            AnonymousClass5MW[] A04 = r6.A04();
            boolean z = true;
            for (int i = 0; i != A04.length; i++) {
                if (z) {
                    z = false;
                } else {
                    stringBuffer.append('+');
                }
                A02(stringBuffer, hashtable, A04[i]);
            }
        } else if (r6.A03() != null) {
            A02(stringBuffer, hashtable, r6.A03());
        }
    }

    public static boolean A04(C114605Mj r9, C114605Mj r10) {
        if (r9.A00.A01.length == r10.A00.A01.length) {
            AnonymousClass5MW[] A04 = r9.A04();
            AnonymousClass5MW[] A042 = r10.A04();
            int length = A04.length;
            if (length == A042.length) {
                for (int i = 0; i != length; i++) {
                    AnonymousClass5MW r3 = A04[i];
                    AnonymousClass5MW r2 = A042[i];
                    if (r3 == r2 || (r3 != null && r2 != null && r3.A01.A04(r2.A01) && A00(r3.A00).equals(A00(r2.A00)))) {
                    }
                }
                return true;
            }
        }
        return false;
    }
}
