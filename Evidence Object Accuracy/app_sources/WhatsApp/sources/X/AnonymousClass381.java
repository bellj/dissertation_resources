package X;

import android.widget.ImageView;
import com.whatsapp.R;
import java.lang.ref.WeakReference;

/* renamed from: X.381  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass381 extends AbstractC16350or {
    public final float A00;
    public final int A01;
    public final AnonymousClass130 A02;
    public final AnonymousClass131 A03;
    public final C15370n3 A04;
    public final C20710wC A05;
    public final WeakReference A06;

    public AnonymousClass381(ImageView imageView, AnonymousClass130 r4, AnonymousClass131 r5, C15370n3 r6, C20710wC r7) {
        float dimension;
        this.A02 = r4;
        this.A05 = r7;
        this.A03 = r5;
        this.A04 = r6;
        this.A01 = C12960it.A09(imageView).getDimensionPixelSize(R.dimen.conversation_profile_photo_size);
        if (this.A05.A0b(C15580nU.A02(r6.A0D))) {
            dimension = -2.14748365E9f;
        } else {
            dimension = C12960it.A09(imageView).getDimension(R.dimen.small_avatar_radius);
        }
        this.A00 = dimension;
        this.A06 = C12970iu.A10(imageView);
    }
}
