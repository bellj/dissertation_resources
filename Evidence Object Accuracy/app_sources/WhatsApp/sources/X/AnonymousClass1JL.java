package X;

import com.whatsapp.instrumentation.service.InstrumentationFGService;
import com.whatsapp.media.download.service.MediaDownloadService;
import com.whatsapp.migration.android.integration.service.GoogleMigrateService;
import com.whatsapp.service.GcmFGService;
import com.whatsapp.service.MDSyncService;
import com.whatsapp.service.WebClientService;
import com.whatsapp.voipcalling.VoiceFGService;

/* renamed from: X.1JL  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1JL extends AbstractServiceC27491Hs implements AnonymousClass004 {
    public final Object A00 = new Object();
    public volatile C71083cM A01;

    public void A00() {
        if (this instanceof VoiceFGService) {
            VoiceFGService voiceFGService = (VoiceFGService) this;
            if (!voiceFGService.A01) {
                voiceFGService.A01 = true;
                AnonymousClass01J r1 = ((C58182oH) ((AnonymousClass5B7) voiceFGService.generatedComponent())).A01;
                ((AnonymousClass1JK) voiceFGService).A01 = (C22900zp) r1.A7t.get();
                voiceFGService.A00 = r1.A2X();
            }
        } else if (this instanceof WebClientService) {
            WebClientService webClientService = (WebClientService) this;
            if (!webClientService.A01) {
                webClientService.A01 = true;
                AnonymousClass01J r12 = ((C58182oH) ((AnonymousClass5B7) webClientService.generatedComponent())).A01;
                ((AnonymousClass1JK) webClientService).A01 = (C22900zp) r12.A7t.get();
                webClientService.A00 = (C26461Dl) r12.ANG.get();
            }
        } else if (this instanceof MDSyncService) {
            MDSyncService mDSyncService = (MDSyncService) this;
            if (!mDSyncService.A0E) {
                mDSyncService.A0E = true;
                AnonymousClass01J r2 = ((C58182oH) ((AnonymousClass5B7) mDSyncService.generatedComponent())).A01;
                ((AnonymousClass1JK) mDSyncService).A01 = (C22900zp) r2.A7t.get();
                mDSyncService.A03 = (C16590pI) r2.AMg.get();
                mDSyncService.A0A = (AbstractC14440lR) r2.ANe.get();
                mDSyncService.A02 = (C15450nH) r2.AII.get();
                mDSyncService.A09 = (AnonymousClass17R) r2.AIz.get();
                mDSyncService.A07 = (C14840m8) r2.ACi.get();
                mDSyncService.A05 = (C245716a) r2.AJR.get();
                mDSyncService.A04 = (C22100yW) r2.A3g.get();
                mDSyncService.A08 = (C238713i) r2.A98.get();
                mDSyncService.A06 = (C238813j) r2.ABy.get();
            }
        } else if (this instanceof GcmFGService) {
            GcmFGService gcmFGService = (GcmFGService) this;
            if (!gcmFGService.A00) {
                gcmFGService.A00 = true;
                ((AnonymousClass1JK) gcmFGService).A01 = (C22900zp) ((C58182oH) ((AnonymousClass5B7) gcmFGService.generatedComponent())).A01.A7t.get();
            }
        } else if (this instanceof GoogleMigrateService) {
            GoogleMigrateService googleMigrateService = (GoogleMigrateService) this;
            if (!googleMigrateService.A07) {
                googleMigrateService.A07 = true;
                AnonymousClass01J r22 = ((C58182oH) ((AnonymousClass5B7) googleMigrateService.generatedComponent())).A01;
                ((AnonymousClass1JK) googleMigrateService).A01 = (C22900zp) r22.A7t.get();
                googleMigrateService.A06 = (AbstractC14440lR) r22.ANe.get();
                googleMigrateService.A00 = (AbstractC15710nm) r22.A4o.get();
                googleMigrateService.A05 = (C25661Ag) r22.A8G.get();
                googleMigrateService.A01 = (AnonymousClass01d) r22.ALI.get();
                googleMigrateService.A03 = (C26031Bt) r22.A8g.get();
                googleMigrateService.A02 = (C26061Bw) r22.A8e.get();
                googleMigrateService.A04 = (C26451Dk) r22.A8f.get();
            }
        } else if (!(this instanceof MediaDownloadService)) {
            InstrumentationFGService instrumentationFGService = (InstrumentationFGService) this;
            if (!instrumentationFGService.A02) {
                instrumentationFGService.A02 = true;
                ((AnonymousClass1JK) instrumentationFGService).A01 = (C22900zp) ((C58182oH) ((AnonymousClass5B7) instrumentationFGService.generatedComponent())).A01.A7t.get();
            }
        } else {
            MediaDownloadService mediaDownloadService = (MediaDownloadService) this;
            if (!mediaDownloadService.A07) {
                mediaDownloadService.A07 = true;
                AnonymousClass01J r13 = ((C58182oH) ((AnonymousClass5B7) mediaDownloadService.generatedComponent())).A01;
                ((AnonymousClass1JK) mediaDownloadService).A01 = (C22900zp) r13.A7t.get();
                mediaDownloadService.A02 = (C16590pI) r13.AMg.get();
                mediaDownloadService.A05 = (AbstractC14440lR) r13.ANe.get();
                mediaDownloadService.A00 = (C15550nR) r13.A45.get();
                mediaDownloadService.A01 = (C15610nY) r13.AMe.get();
                mediaDownloadService.A03 = (C22370yy) r13.AB0.get();
            }
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        if (this.A01 == null) {
            synchronized (this.A00) {
                if (this.A01 == null) {
                    this.A01 = new C71083cM(this);
                }
            }
        }
        return this.A01.generatedComponent();
    }

    @Override // android.app.Service
    public void onCreate() {
        A00();
        super.onCreate();
    }
}
