package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.12N  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass12N {
    public SharedPreferences A00;
    public final C16630pM A01;
    public final TreeMap A02 = new TreeMap();

    public AnonymousClass12N(C16630pM r2) {
        this.A01 = r2;
    }

    public final synchronized SharedPreferences A00() {
        SharedPreferences sharedPreferences;
        sharedPreferences = this.A00;
        if (sharedPreferences == null) {
            sharedPreferences = this.A01.A01("user_notice_prefs");
            this.A00 = sharedPreferences;
        }
        return sharedPreferences;
    }

    public C43831xf A01() {
        SharedPreferences A00 = A00();
        int i = A00.getInt("current_user_notice_id", -1);
        if (i == -1) {
            return null;
        }
        return new C43831xf(i, A00.getInt("current_user_notice_stage", 0), A00.getInt("current_user_notice_version", 0), A00.getLong("current_user_notice_stage_timestamp", 0));
    }

    public TreeMap A02() {
        String string;
        C43831xf r6;
        TreeMap treeMap = this.A02;
        if (treeMap.isEmpty() && (string = A00().getString("user_notices", null)) != null) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    JSONObject jSONObject2 = new JSONObject(jSONObject.get(next).toString());
                    try {
                        r6 = new C43831xf(jSONObject2.getInt("id"), jSONObject2.getInt("stage"), jSONObject2.getInt("version"), jSONObject2.getLong("t"));
                    } catch (JSONException e) {
                        Log.e("UserNoticeMetadata/fromJSON exception: ", e);
                        r6 = null;
                    }
                    treeMap.put(Integer.valueOf(next), r6);
                }
            } catch (JSONException e2) {
                Log.e("UserNoticeSharedPreferences/getUserNoticeMap/parsing failed", e2);
            }
        }
        return treeMap;
    }

    public void A03(C43831xf r6) {
        SharedPreferences.Editor edit = A00().edit();
        int i = r6.A00;
        edit.putInt("current_user_notice_id", i).putInt("current_user_notice_stage", r6.A01).putLong("current_user_notice_stage_timestamp", r6.A03).putInt("current_user_notice_version", r6.A02).apply();
        TreeMap A02 = A02();
        A02.put(Integer.valueOf(i), r6);
        A04(new ArrayList(A02.values()));
    }

    public void A04(List list) {
        HashMap hashMap = new HashMap();
        TreeMap treeMap = this.A02;
        treeMap.clear();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C43831xf r6 = (C43831xf) it.next();
            JSONObject jSONObject = new JSONObject();
            try {
                int i = r6.A00;
                jSONObject.put("id", i);
                jSONObject.put("stage", r6.A01);
                jSONObject.put("t", r6.A03);
                jSONObject.put("version", r6.A02);
                hashMap.put(String.valueOf(i), jSONObject.toString());
                treeMap.put(Integer.valueOf(i), r6);
            } catch (JSONException e) {
                Log.e("UserNoticeMetadata/toJSON exception: ", e);
            }
        }
        A00().edit().putString("user_notices", new JSONObject(hashMap).toString()).apply();
    }
}
