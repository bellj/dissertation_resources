package X;

import android.os.CountDownTimer;
import com.whatsapp.registration.RegisterPhone;

/* renamed from: X.2Zr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51922Zr extends CountDownTimer {
    public final /* synthetic */ RegisterPhone A00;

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51922Zr(RegisterPhone registerPhone) {
        super(200, 200);
        this.A00 = registerPhone;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x009b, code lost:
        if (r14 != 0) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00c2, code lost:
        if (r10.charAt(r8) == r13.charAt(r12)) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c4, code lost:
        r8 = r8 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00cf, code lost:
        if (r8 == -2) goto L_0x00d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01a6, code lost:
        if (r4 == 30) goto L_0x017b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0179  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01a9  */
    @Override // android.os.CountDownTimer
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onFinish() {
        /*
        // Method dump skipped, instructions count: 429
        */
        throw new UnsupportedOperationException("Method not decompiled: X.CountDownTimerC51922Zr.onFinish():void");
    }
}
