package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;

/* renamed from: X.2T1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2T1 extends AnonymousClass2T2 {
    public Drawable A00;

    public AnonymousClass2T1(Context context) {
        super(context);
    }

    @Override // X.AnonymousClass2T3
    public void setMediaItem(AbstractC35611iN r4) {
        Context context;
        int i;
        super.setMediaItem(r4);
        if (r4 != null) {
            int type = r4.getType();
            if (type == 1) {
                context = getContext();
                i = R.drawable.mark_video;
            } else if (type == 2) {
                context = getContext();
                i = R.drawable.mark_gif;
            }
            this.A00 = AnonymousClass00T.A04(context, i);
            return;
        }
        this.A00 = null;
    }
}
