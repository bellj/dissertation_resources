package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0B1  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0B1 extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(13);
    public int A00;

    public AnonymousClass0B1(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readInt();
    }

    public AnonymousClass0B1(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("HorizontalScrollView.SavedState{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" scrollPosition=");
        sb.append(this.A00);
        sb.append("}");
        return sb.toString();
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.A00);
    }
}
