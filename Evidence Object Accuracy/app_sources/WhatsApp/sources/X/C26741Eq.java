package X;

/* renamed from: X.1Eq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26741Eq {
    public final C14830m7 A00;
    public final C14420lP A01;

    public C26741Eq(C14830m7 r1, C14420lP r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public boolean A00(C14430lQ r7) {
        if (System.currentTimeMillis() - r7.A04 >= 86400000) {
            r7.A04 = System.currentTimeMillis();
            r7.A01 = 1;
        } else {
            int i = r7.A01;
            if (i >= 15) {
                return false;
            }
            r7.A01 = i + 1;
        }
        this.A01.A03(r7);
        return true;
    }
}
