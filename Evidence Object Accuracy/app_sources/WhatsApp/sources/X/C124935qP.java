package X;

import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;

/* renamed from: X.5qP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124935qP {
    public static void A00(ActivityC13810kN r6) {
        Toolbar A08 = C117305Zk.A08(r6);
        r6.A1e(A08);
        AbstractC005102i A1U = r6.A1U();
        if (A1U != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                r6.getWindow().addFlags(Integer.MIN_VALUE);
                r6.getWindow().setStatusBarColor(AnonymousClass00T.A00(r6, R.color.ob_status_bar));
            }
            A08.setBackgroundColor(AnonymousClass00T.A00(r6, R.color.primary_surface));
            A1U.A0D(AnonymousClass00T.A04(r6, R.drawable.onboarding_actionbar_home_close));
            A1U.A0P(false);
            A1U.A0M(true);
            A08.setOverflowIcon(AnonymousClass00T.A04(r6, R.drawable.onboarding_actionbar_overflow_button));
            View findViewById = r6.findViewById(R.id.scroll_view);
            if (findViewById != null) {
                findViewById.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener(findViewById, A1U, r6) { // from class: X.655
                    public final /* synthetic */ View A00;
                    public final /* synthetic */ AbstractC005102i A01;
                    public final /* synthetic */ ActivityC13810kN A02;

                    {
                        this.A00 = r1;
                        this.A01 = r2;
                        this.A02 = r3;
                    }

                    @Override // android.view.ViewTreeObserver.OnScrollChangedListener
                    public final void onScrollChanged() {
                        this.A01.A07(C117295Zj.A00(this.A02, this.A00));
                    }
                });
            }
        }
    }
}
