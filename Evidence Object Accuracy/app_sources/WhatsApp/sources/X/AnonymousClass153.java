package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.SecureRandom;

/* renamed from: X.153  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass153 {
    public static final C37831n5 A06 = new C37831n5(4, 5, false);
    public SecureRandom A00;
    public final AbstractC15710nm A01;
    public final C14830m7 A02;
    public final C16370ot A03;
    public final C15690nk A04;
    public final AnonymousClass152 A05;

    public AnonymousClass153(AbstractC15710nm r1, C14830m7 r2, C16370ot r3, C15690nk r4, AnonymousClass152 r5) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A04 = r4;
        this.A05 = r5;
    }

    public static final C37921nE A00(C37891nB r3, InputStream inputStream, int i) {
        try {
            C37901nC r2 = new C37901nC(r3, inputStream, i);
            do {
            } while (r2.read(new byte[DefaultCrypto.BUFFER_SIZE]) >= 0);
            C37921nE r0 = r2.A04;
            r2.close();
            return r0;
        } catch (IOException e) {
            Log.e("mediaupload/calculate-sidecar/ioexception", e);
            throw e;
        }
    }

    public C37951nH A01(AbstractC37941nG r4, C14510lY r5, File file) {
        C14380lL r1 = r5.A02;
        if (!r1.A0A) {
            C37961nI r2 = new C37961nI(A05(r5, file), MessageDigest.getInstance("SHA-256"));
            return new C37951nH(r2, r2, null, this);
        }
        int[] iArr = r1.A0E;
        if (iArr == null || iArr.length <= 0) {
            return A02(r4, r5, file);
        }
        return A03(r4, file, iArr);
    }

    public final C37951nH A02(AbstractC37941nG r5, C14510lY r6, File file) {
        C37961nI r3 = new C37961nI(A05(r6, file), MessageDigest.getInstance("SHA-256"));
        return new C37951nH(r3, new C37961nI(r5.A9M(r3), MessageDigest.getInstance("SHA-256")), null, this);
    }

    public C37951nH A03(AbstractC37941nG r8, File file, int[] iArr) {
        AnonymousClass009.A05(file);
        FileInputStream A0K = C22200yh.A0K(file);
        this.A04.A05(A0K);
        if (iArr.length == 4) {
            long j = (long) (iArr[0] + iArr[1] + iArr[2]);
            if (j != -1) {
                C37961nI r4 = new C37961nI(new BufferedInputStream(A0K), MessageDigest.getInstance("SHA-256"));
                C37961nI r3 = new C37961nI(new C37601mh(r4, j), MessageDigest.getInstance("SHA-256"));
                return new C37951nH(r4, new C37961nI(r8.A9M(new C38011nN(r3, r4)), MessageDigest.getInstance("SHA-256")), r3, this);
            }
        }
        C37961nI r32 = new C37961nI(new BufferedInputStream(A0K), MessageDigest.getInstance("SHA-256"));
        return new C37951nH(r32, new C37961nI(r8.A9M(r32), MessageDigest.getInstance("SHA-256")), null, this);
    }

    public C37981nK A04(AbstractC14500lX r6, C14510lY r7, C37931nF r8) {
        C37961nI r1;
        AbstractC37941nG ACi = r6.ACi(r8.A00.A01);
        File file = r7.A02.A06;
        AnonymousClass009.A05(file);
        C37951nH A01 = A01(ACi, r7, file);
        try {
            byte[] bArr = new byte[16384];
            do {
                r1 = A01.A00;
            } while (r1.read(bArr) >= 0);
            C37981nK r0 = new C37981nK(new C37971nJ(A01.A02.A00(), false), new C37971nJ(r1.A00(), false), r8);
            A01.close();
            return r0;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final InputStream A05(C14510lY r4, File file) {
        if (r4 instanceof C37991nL) {
            File file2 = r4.A02.A06;
            AnonymousClass009.A05(file2);
            return new C38001nM((C37991nL) r4, this, file2);
        }
        AnonymousClass009.A05(file);
        FileInputStream A0K = C22200yh.A0K(file);
        C14370lK r1 = r4.A02.A05;
        if (!(r1 == C14370lK.A0S || r1 == C14370lK.A09 || r1 == C14370lK.A0C || r1 == C14370lK.A0Y || r1 == C14370lK.A0A || r1 == C14370lK.A0H)) {
            this.A04.A05(A0K);
        }
        return new BufferedInputStream(A0K);
    }

    public String A06(C14370lK r3, File file, boolean z) {
        AnonymousClass009.A0F(A07(r3, file, z));
        if (C14370lK.A05 == r3 || C14370lK.A0I == r3) {
            if (z) {
                return AnonymousClass152.A06(A06);
            }
            return AnonymousClass152.A06(AnonymousClass152.A03(file));
        } else if (C14370lK.A0X != r3 && C14370lK.A04 != r3 && C14370lK.A0a != r3) {
            return null;
        } else {
            int i = AnonymousClass152.A04(file, false).A01;
            if (i == 7) {
                return "video/quicktime";
            }
            return i == 3 ? "video/3gpp" : "video/mp4";
        }
    }

    public boolean A07(C14370lK r3, File file, boolean z) {
        if (C14370lK.A05 == r3 || C14370lK.A0I == r3) {
            if (z || this.A05.A0D(file)) {
                return true;
            }
            return false;
        } else if (C14370lK.A0X == r3 || C14370lK.A04 == r3 || C14370lK.A0a == r3) {
            return this.A05.A0E(file);
        } else {
            return true;
        }
    }
}
