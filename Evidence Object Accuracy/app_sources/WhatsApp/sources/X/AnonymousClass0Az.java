package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Az  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Az extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(12);
    public boolean A00;

    public AnonymousClass0Az(Parcel parcel) {
        super(parcel);
        this.A00 = parcel.readByte() != 0;
    }

    public AnonymousClass0Az(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeByte(this.A00 ? (byte) 1 : 0);
    }
}
