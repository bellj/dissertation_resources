package X;

import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;
import com.whatsapp.profile.ViewProfilePhoto;

/* renamed from: X.55p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1104555p implements AbstractC32851cq {
    public final /* synthetic */ ViewProfilePhoto A00;

    public C1104555p(ViewProfilePhoto viewProfilePhoto) {
        this.A00 = viewProfilePhoto;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        ViewProfilePhoto viewProfilePhoto = this.A00;
        viewProfilePhoto.A0G.A01(viewProfilePhoto);
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        ViewProfilePhoto viewProfilePhoto = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_profile_photo_view_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_profile_photo_view;
        }
        RequestPermissionActivity.A0K(viewProfilePhoto, R.string.permission_storage_need_write_access_on_profile_photo_view_request, i2);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        ViewProfilePhoto viewProfilePhoto = this.A00;
        viewProfilePhoto.A0G.A01(viewProfilePhoto);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        ViewProfilePhoto viewProfilePhoto = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access;
        }
        RequestPermissionActivity.A0K(viewProfilePhoto, R.string.permission_storage_need_write_access_request, i2);
    }
}
