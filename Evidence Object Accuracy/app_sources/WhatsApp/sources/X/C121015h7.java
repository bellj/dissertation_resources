package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5h7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121015h7 extends AbstractC121025h8 {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(33);
    public final C127345uL A00;
    public final C1316163l A01;
    public final C1309260n A02;

    public C121015h7(AnonymousClass102 r7, AnonymousClass1V8 r8) {
        super(r7, r8);
        AnonymousClass1V8 A0E = r8.A0E("store");
        AnonymousClass1V8 A0E2 = r8.A0E("brand");
        this.A01 = C1316163l.A00(A0E);
        C127345uL r5 = null;
        if (A0E2 != null) {
            try {
                String A0H = A0E2.A0H("name");
                String A0I = A0E2.A0I("thumbnail_image_url", null);
                AnonymousClass1V8 A0E3 = A0E2.A0E("description");
                r5 = new C127345uL(A0H, A0I, A0E3 != null ? A0E3.A0I("text", null) : "");
            } catch (AnonymousClass1V9 unused) {
                Log.e("PAY: NoviWithdrawCashBrand/fromProtocolTreeNode can't construct NoviWithdrawCashBrand");
            }
        }
        this.A00 = r5;
        this.A02 = C1309260n.A01(r8.A0E("instructions"));
    }

    public C121015h7(Parcel parcel) {
        super(parcel);
        C127345uL r0;
        this.A01 = (C1316163l) C12990iw.A0I(parcel, C1316163l.class);
        if (parcel.readInt() == 1) {
            r0 = new C127345uL(parcel.readString(), parcel.readString(), parcel.readString());
        } else {
            r0 = null;
        }
        this.A00 = r0;
        int readInt = parcel.readInt();
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < readInt; i++) {
            A0l.add(parcel.readString());
        }
        this.A02 = new C1309260n(A0l);
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00dc A[LOOP:0: B:32:0x00d6->B:34:0x00dc, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0079 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C121015h7(java.lang.String r12) {
        /*
            r11 = this;
            r11.<init>(r12)
            org.json.JSONObject r4 = X.C13000ix.A05(r12)
            java.lang.String r0 = "store"
            java.lang.String r1 = r4.optString(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            if (r0 != 0) goto L_0x0065
            org.json.JSONObject r1 = X.C13000ix.A05(r1)     // Catch: JSONException -> 0x0060
            java.lang.String r0 = "id"
            java.lang.String r7 = r1.getString(r0)     // Catch: JSONException -> 0x0060
            java.lang.String r0 = "full_address"
            java.lang.String r8 = r1.getString(r0)     // Catch: JSONException -> 0x0060
            java.lang.String r0 = "icon"
            java.lang.String r9 = r1.getString(r0)     // Catch: JSONException -> 0x0060
            java.lang.String r0 = "name"
            java.lang.String r10 = r1.getString(r0)     // Catch: JSONException -> 0x0060
            java.lang.String r0 = "coordinates"
            java.lang.String r1 = r1.getString(r0)     // Catch: JSONException -> 0x0060
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch: JSONException -> 0x0060
            if (r0 != 0) goto L_0x0056
            org.json.JSONObject r1 = X.C13000ix.A05(r1)     // Catch: JSONException -> 0x0051, JSONException -> 0x0060
            java.lang.String r0 = "latitude"
            double r2 = r1.getDouble(r0)     // Catch: JSONException -> 0x0051, JSONException -> 0x0060
            java.lang.String r0 = "longitude"
            double r0 = r1.getDouble(r0)     // Catch: JSONException -> 0x0051, JSONException -> 0x0060
            X.5zd r6 = new X.5zd     // Catch: JSONException -> 0x0051, JSONException -> 0x0060
            r6.<init>(r2, r0)     // Catch: JSONException -> 0x0051, JSONException -> 0x0060
            goto L_0x0057
        L_0x0051:
            java.lang.String r0 = "PAY: NoviCoordinates fromJsonString threw exception"
            com.whatsapp.util.Log.e(r0)     // Catch: JSONException -> 0x0060
        L_0x0056:
            r6 = 0
        L_0x0057:
            X.AnonymousClass009.A05(r6)     // Catch: JSONException -> 0x0060
            X.63l r5 = new X.63l     // Catch: JSONException -> 0x0060
            r5.<init>(r6, r7, r8, r9, r10)     // Catch: JSONException -> 0x0060
            goto L_0x0066
        L_0x0060:
            java.lang.String r0 = "PAY: NoviWithdrawCashStore fromJsonString threw exception"
            com.whatsapp.util.Log.e(r0)
        L_0x0065:
            r5 = 0
        L_0x0066:
            r11.A01 = r5
            java.lang.String r0 = "brand"
            java.lang.String r1 = r4.optString(r0)
            java.lang.String r6 = "description"
            java.lang.String r2 = "image_url"
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            r5 = 0
            if (r0 != 0) goto L_0x00a8
            org.json.JSONObject r1 = X.C13000ix.A05(r1)     // Catch: JSONException -> 0x00a3
            java.lang.String r0 = "name"
            java.lang.String r3 = r1.getString(r0)     // Catch: JSONException -> 0x00a3
            boolean r0 = r1.has(r2)     // Catch: JSONException -> 0x00a3
            if (r0 == 0) goto L_0x009f
            java.lang.String r2 = r1.getString(r2)     // Catch: JSONException -> 0x00a3
        L_0x008d:
            boolean r0 = r1.has(r6)     // Catch: JSONException -> 0x00a3
            if (r0 == 0) goto L_0x009d
            java.lang.String r1 = r1.getString(r6)     // Catch: JSONException -> 0x00a3
        L_0x0097:
            X.5uL r0 = new X.5uL     // Catch: JSONException -> 0x00a3
            r0.<init>(r3, r2, r1)     // Catch: JSONException -> 0x00a3
            goto L_0x00a1
        L_0x009d:
            r1 = r5
            goto L_0x0097
        L_0x009f:
            r2 = r5
            goto L_0x008d
        L_0x00a1:
            r5 = r0
            goto L_0x00a8
        L_0x00a3:
            java.lang.String r0 = "PAY: NoviWithdrawCashBrand fromJsonString threw exception"
            com.whatsapp.util.Log.e(r0)
        L_0x00a8:
            r11.A00 = r5
            java.lang.String r0 = "instructions"
            java.lang.String r1 = r4.optString(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r1)
            r2 = 0
            if (r0 != 0) goto L_0x00f8
            org.json.JSONObject r1 = X.C13000ix.A05(r1)
            java.lang.String r0 = "instruction"
            org.json.JSONArray r5 = r1.optJSONArray(r0)
            if (r5 == 0) goto L_0x00f8
            int r0 = r5.length()
            if (r0 == 0) goto L_0x00f8
            int r0 = r5.length()
            java.util.List r0 = java.util.Collections.nCopies(r0, r2)
            java.util.ArrayList r4 = X.C12980iv.A0x(r0)
            r3 = 0
        L_0x00d6:
            int r0 = r5.length()
            if (r3 >= r0) goto L_0x00f3
            java.lang.Object r2 = r5.get(r3)
            org.json.JSONObject r2 = (org.json.JSONObject) r2
            java.lang.String r0 = "text"
            java.lang.String r1 = r2.getString(r0)
            r0 = 0
            int r0 = X.C1309260n.A00(r0, r4, r2, r3)
            r4.set(r0, r1)
            int r3 = r3 + 1
            goto L_0x00d6
        L_0x00f3:
            X.60n r2 = new X.60n
            r2.<init>(r4)
        L_0x00f8:
            r11.A02 = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C121015h7.<init>(java.lang.String):void");
    }

    @Override // X.AbstractC121025h8, X.AbstractC1316063k
    public void A05(JSONObject jSONObject) {
        super.A05(jSONObject);
        try {
            C1316163l r2 = this.A01;
            if (r2 != null) {
                JSONObject A0a = C117295Zj.A0a();
                try {
                    A0a.put("id", r2.A04);
                    A0a.put("full_address", r2.A02);
                    A0a.put("icon", r2.A03);
                    A0a.put("name", r2.A01);
                    C130575zd r4 = r2.A00;
                    JSONObject A0a2 = C117295Zj.A0a();
                    try {
                        A0a2.put("latitude", r4.A00);
                        A0a2.put("longitude", r4.A01);
                    } catch (JSONException unused) {
                        Log.w("PAY:NoviCoordinates/toJson: JSONException thrown");
                    }
                    A0a.put("coordinates", A0a2);
                } catch (JSONException unused2) {
                    Log.w("PAY:NoviWithdrawCashStore/toJson: JSONException thrown");
                }
                jSONObject.put("store", A0a);
            }
            C127345uL r42 = this.A00;
            if (r42 != null) {
                JSONObject A0a3 = C117295Zj.A0a();
                try {
                    A0a3.put("name", r42.A02);
                    A0a3.put("image_url", r42.A01);
                    A0a3.put("description", r42.A00);
                } catch (JSONException unused3) {
                    Log.w("PAY:NoviWithdrawCashBrand/toJson: JSONException thrown");
                }
                jSONObject.put("brand", A0a3);
            }
            C1309260n r0 = this.A02;
            if (r0 != null) {
                JSONObject A0a4 = C117295Zj.A0a();
                List list = r0.A00;
                if (list != null && !list.isEmpty()) {
                    JSONArray A0L = C117315Zl.A0L();
                    for (int i = 0; i < list.size(); i++) {
                        Object obj = list.get(i);
                        JSONObject A0a5 = C117295Zj.A0a();
                        A0a5.put("text", obj);
                        A0a5.put("sequence", i);
                        A0L.put(A0a5);
                    }
                    A0a4.put("instruction", A0L);
                }
                jSONObject.put("instructions", A0a4);
            }
        } catch (JSONException unused4) {
            Log.w("PAY:NoviTransactionWithdrawalCash failed to create the JSON");
        }
    }

    @Override // X.AbstractC121025h8, X.AbstractC1316063k, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A01, i);
        C127345uL r2 = this.A00;
        if (r2 != null) {
            parcel.writeInt(1);
            parcel.writeString(r2.A02);
            parcel.writeString(r2.A01);
            parcel.writeString(r2.A00);
        } else {
            parcel.writeInt(0);
        }
        C1309260n r0 = this.A02;
        if (r0 != null) {
            List list = r0.A00;
            parcel.writeInt(list.size());
            Iterator it = list.iterator();
            while (it.hasNext()) {
                parcel.writeString(C12970iu.A0x(it));
            }
            return;
        }
        parcel.writeInt(0);
    }
}
