package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.EditText;
import com.facebook.redex.RunnableBRunnable0Shape10S0100000_I0_10;
import com.whatsapp.R;
import com.whatsapp.registration.ChangeNumber;
import com.whatsapp.util.Log;

/* renamed from: X.27n  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC467827n extends Handler {
    public final /* synthetic */ ChangeNumber A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public HandlerC467827n(Looper looper, ChangeNumber changeNumber) {
        super(looper);
        this.A00 = changeNumber;
    }

    @Override // android.os.Handler
    public void handleMessage(Message message) {
        EditText editText;
        int i = message.what;
        if (i == 1) {
            Log.w("changenumber/check-number/match");
            ChangeNumber changeNumber = this.A00;
            changeNumber.A0J.removeMessages(4);
            ((ActivityC13830kP) changeNumber).A05.Ab2(new RunnableBRunnable0Shape10S0100000_I0_10(this, 29));
            if (!ChangeNumber.A0N.equals(AbstractActivityC452520u.A0R)) {
                C36021jC.A00(changeNumber, 1);
                C36021jC.A01(changeNumber, 2);
                C91794Td r0 = changeNumber.A0F;
                if (r0 != null) {
                    editText = r0.A02;
                } else {
                    return;
                }
            } else {
                changeNumber.A2k();
                return;
            }
        } else if (i == 2) {
            Log.w("changenumber/check-number/mismatch");
            ChangeNumber changeNumber2 = this.A00;
            changeNumber2.A0J.removeMessages(4);
            C36021jC.A00(changeNumber2, 1);
            changeNumber2.Ado(R.string.delete_account_mismatch);
            C91794Td r02 = changeNumber2.A0F;
            if (r02 != null) {
                editText = r02.A03;
            } else {
                return;
            }
        } else if (i == 3) {
            Log.e("changenumber/error");
            ChangeNumber changeNumber3 = this.A00;
            C36021jC.A00(changeNumber3, 1);
            C36021jC.A01(changeNumber3, 109);
            return;
        } else if (i == 4) {
            Log.e("changenumber/timeout");
            ChangeNumber changeNumber4 = this.A00;
            changeNumber4.A0J.removeMessages(4);
            C36021jC.A00(changeNumber4, 1);
            C36021jC.A01(changeNumber4, 109);
            return;
        } else {
            return;
        }
        if (editText != null) {
            editText.requestFocus();
        }
    }
}
