package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.WindowInsets;
import com.whatsapp.R;

/* renamed from: X.0Uc  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Uc {
    public static float A00(View view) {
        return view.getElevation();
    }

    public static float A01(View view) {
        return view.getZ();
    }

    public static ColorStateList A02(View view) {
        return view.getBackgroundTintList();
    }

    public static PorterDuff.Mode A03(View view) {
        return view.getBackgroundTintMode();
    }

    public static C018408o A04(View view) {
        return AnonymousClass0RU.A00(view);
    }

    public static String A05(View view) {
        return view.getTransitionName();
    }

    public static void A06(ColorStateList colorStateList, View view) {
        view.setBackgroundTintList(colorStateList);
    }

    public static void A07(PorterDuff.Mode mode, View view) {
        view.setBackgroundTintMode(mode);
    }

    public static void A08(Rect rect, View view, C018408o r3) {
        WindowInsets A07 = r3.A07();
        if (A07 != null) {
            C018408o.A01(view, view.computeSystemWindowInsets(A07, rect));
        } else {
            rect.setEmpty();
        }
    }

    public static void A09(View view) {
        view.stopNestedScroll();
    }

    public static void A0A(View view, float f) {
        view.setElevation(f);
    }

    public static void A0B(View view, WindowInsets windowInsets) {
        View.OnApplyWindowInsetsListener onApplyWindowInsetsListener = (View.OnApplyWindowInsetsListener) view.getTag(R.id.tag_window_insets_animation_callback);
        if (onApplyWindowInsetsListener != null) {
            onApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
        }
    }

    public static void A0C(View view, AnonymousClass07F r3) {
        View.OnApplyWindowInsetsListener r0;
        if (Build.VERSION.SDK_INT < 30) {
            view.setTag(R.id.tag_on_apply_window_listener, r3);
        }
        if (r3 == null) {
            r0 = (View.OnApplyWindowInsetsListener) view.getTag(R.id.tag_window_insets_animation_callback);
        } else {
            r0 = new AnonymousClass0W4(view, r3);
        }
        view.setOnApplyWindowInsetsListener(r0);
    }

    public static void A0D(View view, String str) {
        view.setTransitionName(str);
    }

    public static void A0E(View view, boolean z) {
        view.setNestedScrollingEnabled(z);
    }

    public static boolean A0F(View view) {
        return view.isNestedScrollingEnabled();
    }
}
