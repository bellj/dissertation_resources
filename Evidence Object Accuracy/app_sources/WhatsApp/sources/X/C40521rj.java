package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1rj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40521rj {
    public int A00 = -1;
    public long A01 = -1;
    public boolean A02 = false;
    public final long A03;
    public final long A04;
    public final UserJid A05;
    public final String A06;
    public final String A07;

    public C40521rj(UserJid userJid, String str, String str2, long j, long j2) {
        this.A05 = userJid;
        this.A07 = str;
        this.A06 = str2;
        this.A03 = j;
        this.A04 = j2;
    }
}
