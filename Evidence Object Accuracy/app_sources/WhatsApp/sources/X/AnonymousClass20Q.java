package X;

/* renamed from: X.20Q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20Q {
    public final byte[] A00;

    public AnonymousClass20Q(byte[] bArr) {
        this.A00 = bArr;
    }

    public byte[] A00(byte[] bArr, byte[] bArr2, int i, long j) {
        try {
            AnonymousClass5GB r5 = new AnonymousClass5GB(new C71653dH());
            byte[] bArr3 = new byte[12];
            C16050oM.A02(bArr3, 4, j);
            byte[] bArr4 = this.A00;
            r5.AIf(new C113075Fx(new AnonymousClass20K(bArr4, bArr4.length), bArr3), true);
            r5.AZX(bArr, 0, bArr.length);
            int AEp = r5.AEp(i);
            byte[] bArr5 = new byte[AEp];
            int AZZ = r5.AZZ(bArr2, 0, i, bArr5, 0);
            int A97 = AZZ + r5.A97(bArr5, AZZ);
            if (A97 >= AEp) {
                return bArr5;
            }
            byte[] bArr6 = new byte[A97];
            System.arraycopy(bArr5, 0, bArr6, 0, A97);
            return bArr6;
        } catch (C114965Nt e) {
            throw new AssertionError(e);
        }
    }

    public byte[] A01(byte[] bArr, byte[] bArr2, long j) {
        try {
            AnonymousClass5GB r4 = new AnonymousClass5GB(new C71653dH());
            byte[] bArr3 = new byte[12];
            C16050oM.A02(bArr3, 4, j);
            byte[] bArr4 = this.A00;
            r4.AIf(new C113075Fx(new AnonymousClass20K(bArr4, bArr4.length), bArr3), false);
            r4.AZX(bArr, 0, bArr.length);
            int length = bArr2.length;
            int AEp = r4.AEp(length);
            byte[] bArr5 = new byte[AEp];
            int AZZ = r4.AZZ(bArr2, 0, length, bArr5, 0);
            int A97 = AZZ + r4.A97(bArr5, AZZ);
            if (A97 >= AEp) {
                return bArr5;
            }
            byte[] bArr6 = new byte[A97];
            System.arraycopy(bArr5, 0, bArr6, 0, A97);
            return bArr6;
        } catch (C114965Nt e) {
            throw new AnonymousClass20R(e, this);
        }
    }
}
