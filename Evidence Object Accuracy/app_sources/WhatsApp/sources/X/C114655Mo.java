package X;

/* renamed from: X.5Mo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114655Mo extends AnonymousClass1TM {
    public static final AnonymousClass5NG A04 = new AnonymousClass5NG(20);
    public static final AnonymousClass5NG A05 = new AnonymousClass5NG(1);
    public static final C114725Mv A06;
    public static final C114725Mv A07;
    public AnonymousClass5NG A00 = A04;
    public AnonymousClass5NG A01 = A05;
    public C114725Mv A02 = A06;
    public C114725Mv A03 = A07;

    static {
        C114725Mv r2 = new C114725Mv(AnonymousClass5ME.A00, AnonymousClass1TW.A07);
        A06 = r2;
        A07 = new C114725Mv(r2, AnonymousClass1TJ.A1J);
    }

    public C114655Mo() {
    }

    public C114655Mo(AbstractC114775Na r5) {
        for (int i = 0; i != r5.A0B(); i++) {
            AnonymousClass5NU r2 = (AnonymousClass5NU) r5.A0D(i);
            int i2 = r2.A00;
            if (i2 == 0) {
                this.A02 = C114725Mv.A00(AbstractC114775Na.A05(r2, true));
            } else if (i2 == 1) {
                this.A03 = C114725Mv.A00(AbstractC114775Na.A05(r2, true));
            } else if (i2 == 2) {
                this.A00 = AnonymousClass5NG.A00(AnonymousClass5NU.A00(r2));
            } else if (i2 == 3) {
                this.A01 = AnonymousClass5NG.A00(AnonymousClass5NU.A00(r2));
            } else {
                throw C12970iu.A0f("unknown tag");
            }
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(4);
        C114725Mv r1 = this.A02;
        if (!r1.equals(A06)) {
            C94954co.A02(r1, r3, 0, true);
        }
        C114725Mv r12 = this.A03;
        if (!r12.equals(A07)) {
            C94954co.A03(r12, r3, true);
        }
        AnonymousClass5NG r13 = this.A00;
        if (!r13.A04(A04)) {
            C94954co.A02(r13, r3, 2, true);
        }
        AnonymousClass5NG r14 = this.A01;
        if (!r14.A04(A05)) {
            C94954co.A02(r14, r3, 3, true);
        }
        return new AnonymousClass5NZ(r3);
    }
}
