package X;

import android.os.Message;
import android.text.TextUtils;
import java.util.ArrayList;

/* renamed from: X.1Fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26921Fi implements AbstractC15920o8 {
    public final C23000zz A00;

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{249};
    }

    public C26921Fi(C23000zz r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        if (i != 249) {
            return false;
        }
        Object obj = message.obj;
        AnonymousClass009.A05(obj);
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1V8 r2 : ((AnonymousClass1V8) obj).A0J("notice")) {
            String A0I = r2.A0I("id", null);
            if (!TextUtils.isEmpty(A0I)) {
                arrayList.add(A0I);
            }
        }
        this.A00.A02(arrayList, 0);
        return true;
    }
}
