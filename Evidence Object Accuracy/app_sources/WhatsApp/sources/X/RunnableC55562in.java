package X;

import android.graphics.Matrix;
import com.facebook.redex.EmptyBaseRunnable0;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.2in  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55562in extends EmptyBaseRunnable0 implements Runnable {
    public float A00;
    public long A01;
    public boolean A02;
    public final PhotoView A03;

    public RunnableC55562in(PhotoView photoView) {
        this.A03 = photoView;
    }

    @Override // java.lang.Runnable
    public void run() {
        long j;
        if (!this.A02) {
            float f = this.A00;
            if (f != 0.0f) {
                long currentTimeMillis = System.currentTimeMillis();
                long j2 = this.A01;
                if (j2 != -1) {
                    j = currentTimeMillis - j2;
                } else {
                    j = 0;
                }
                float f2 = 0.0f * ((float) j);
                if ((f < 0.0f && f + f2 > 0.0f) || (f > 0.0f && f + f2 < 0.0f)) {
                    f2 = 0.0f - f;
                }
                PhotoView photoView = this.A03;
                photoView.A07 += f2;
                Matrix matrix = photoView.A0B;
                matrix.postRotate(f2, (float) (photoView.getWidth() >> 1), (float) C13000ix.A00(photoView));
                photoView.setImageMatrix(matrix);
                float f3 = this.A00 + f2;
                this.A00 = f3;
                if (f3 == 0.0f) {
                    this.A02 = true;
                    photoView.A07 = (float) Math.round(photoView.A07);
                    photoView.A08(true);
                    photoView.requestLayout();
                    photoView.invalidate();
                }
                this.A01 = currentTimeMillis;
            }
            if (!this.A02) {
                this.A03.post(this);
            }
        }
    }
}
