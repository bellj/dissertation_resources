package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import com.whatsapp.gallery.MediaGalleryFragmentBase;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: X.3X7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3X7 implements AnonymousClass23D {
    public Context A00;
    public C616531g A01;
    public final int A02;
    public final Uri A03;
    public final AnonymousClass018 A04;
    public final AnonymousClass19M A05;
    public final C39341ph A06;
    public final AnonymousClass1AB A07;
    public final C22190yg A08;
    public final AtomicBoolean A09 = new AtomicBoolean(false);

    public AnonymousClass3X7(Uri uri, AnonymousClass018 r4, AnonymousClass19M r5, C39341ph r6, C616531g r7, AnonymousClass1AB r8, C22190yg r9, int i) {
        this.A00 = r7.getContext();
        this.A04 = r4;
        this.A05 = r5;
        this.A08 = r9;
        this.A07 = r8;
        this.A03 = uri;
        this.A06 = r6;
        this.A01 = r7;
        this.A02 = i;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        StringBuilder A0h = C12960it.A0h();
        C12970iu.A1V(this.A03, A0h);
        return C12960it.A0d("-thumb", A0h);
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        Bitmap bitmap;
        Rect rect;
        AtomicBoolean atomicBoolean = this.A09;
        if (!atomicBoolean.get() && this.A01.getTag() == this) {
            C39341ph r4 = this.A06;
            Uri fromFile = Uri.fromFile(r4.A04());
            C22190yg r3 = this.A08;
            byte A04 = r3.A04(this.A03);
            if (A04 == 1) {
                try {
                    int i = this.A02;
                    bitmap = r3.A07(fromFile, i, i);
                } catch (C39351pj | IOException unused) {
                    bitmap = MediaGalleryFragmentBase.A0U;
                }
            } else if (A04 == 3 || A04 == 13) {
                File A05 = r4.A05();
                AnonymousClass009.A05(A05);
                Bitmap A01 = C26521Du.A01(A05);
                if (A01 != null) {
                    Bitmap.Config config = A01.getConfig();
                    int i2 = this.A02;
                    if (config == null) {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    bitmap = Bitmap.createBitmap(i2, i2, config);
                    Canvas canvas = new Canvas(bitmap);
                    Paint A0F = C12990iw.A0F();
                    A0F.setAntiAlias(true);
                    A0F.setFilterBitmap(true);
                    A0F.setDither(true);
                    int width = A01.getWidth();
                    int height = A01.getHeight();
                    if (width > height) {
                        rect = new Rect((width - height) >> 1, 0, (width + height) >> 1, height);
                    } else {
                        rect = new Rect(0, (height - width) >> 1, width, (height + width) >> 1);
                    }
                    canvas.drawBitmap(A01, rect, new Rect(0, 0, i2, i2), A0F);
                    A01.recycle();
                } else {
                    bitmap = MediaGalleryFragmentBase.A0U;
                }
            } else {
                bitmap = null;
            }
            if (!atomicBoolean.get()) {
                if (!(bitmap == null || r4.A08() == null)) {
                    if (!bitmap.isMutable()) {
                        bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                    }
                    AnonymousClass3JD A03 = AnonymousClass3JD.A03(this.A00, this.A04, this.A05, this.A07, r4.A08());
                    if (A03 != null) {
                        A03.A07(bitmap, 0, false, false);
                    }
                }
                return bitmap == null ? MediaGalleryFragmentBase.A0U : bitmap;
            }
        }
        return null;
    }
}
