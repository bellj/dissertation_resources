package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.59t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1115359t implements AbstractC41521tf {
    public final /* synthetic */ View A00;
    public final /* synthetic */ AnonymousClass016 A01;
    public final /* synthetic */ AnonymousClass2VG A02;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    @Override // X.AbstractC41521tf
    public /* synthetic */ void Adu(View view) {
    }

    public C1115359t(View view, AnonymousClass016 r2, AnonymousClass2VG r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = view;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00.getContext().getResources().getDimensionPixelSize(R.dimen.reactions_bottom_sheet_row_media_size);
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r4) {
        this.A01.A0A(bitmap);
    }
}
