package X;

import java.util.Locale;

/* renamed from: X.5zw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130765zw {
    public final C452120p A00;
    public final Integer A01;

    public C130765zw(int i) {
        this.A00 = null;
        this.A01 = Integer.valueOf(i);
    }

    public C130765zw(C452120p r2) {
        this.A00 = r2;
        this.A01 = null;
    }

    public String toString() {
        String str;
        int intValue;
        C452120p r0 = this.A00;
        if (r0 != null) {
            str = "NETWORK";
            intValue = r0.A00;
        } else {
            str = "CLIENT";
            Integer num = this.A01;
            AnonymousClass009.A05(num);
            intValue = num.intValue();
        }
        Locale locale = Locale.US;
        Object[] A1a = C12980iv.A1a();
        A1a[0] = str;
        C12960it.A1P(A1a, intValue, 1);
        return String.format(locale, "[type=%s, code=%d]", A1a);
    }
}
