package X;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.TypedValue;
import android.view.View;
import com.facebook.redex.RunnableBRunnable0Shape3S0300000_I1;
import com.whatsapp.TextEmojiLabel;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3I6  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3I6 {
    public static final Pattern A00 = Pattern.compile("\\[(\\d+)]");

    public static Spannable A00(View view, C14260l7 r19, AnonymousClass28D r20, AbstractC21000wf r21) {
        Object[] objArr;
        boolean z;
        SpannableStringBuilder A0J;
        Integer valueOf;
        boolean z2;
        SpannableStringBuilder A0J2;
        int i;
        Object obj = r20.A02.get(44);
        int i2 = 0;
        if (obj != null) {
            objArr = ((List) obj).toArray(new String[0]);
        } else {
            objArr = null;
        }
        String A0I = r20.A0I(36);
        if (objArr != null) {
            A0I = MessageFormat.format(A0I, objArr);
        }
        if (A0I == null) {
            return new SpannableStringBuilder();
        }
        Map map = AnonymousClass1Z4.A01;
        Iterator it = map.keySet().iterator();
        while (true) {
            if (it.hasNext()) {
                if (A0I.contains(C12970iu.A0x(it))) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (z) {
            A0J = C12990iw.A0J(AnonymousClass1Z4.A00(view.getContext(), A0I));
        } else {
            A0J = C12990iw.A0J(A0I);
        }
        AnonymousClass28D A0F = r20.A0F(43);
        int i3 = 40;
        String A0I2 = r20.A0I(40);
        if (A0F != null) {
            valueOf = Integer.valueOf(AnonymousClass4Di.A00(r19, A0F));
        } else {
            if (A0I2 != null) {
                try {
                    valueOf = Integer.valueOf(AnonymousClass3JW.A05(A0I2));
                } catch (AnonymousClass491 e) {
                    C28691Op.A01("WaRcRichTextComponentBinderUtils", String.format("Failed to parse text color %s", A0I2), e);
                }
            }
            valueOf = null;
        }
        if (valueOf != null) {
            A0J.setSpan(new ForegroundColorSpan(valueOf.intValue()), 0, A0I.length(), 0);
        }
        String A0I3 = r20.A0I(41);
        if (A0I3 != null) {
            try {
                A0J.setSpan(new AbsoluteSizeSpan(Math.round(AnonymousClass3JW.A01(A0I3))), 0, A0I.length(), 0);
            } catch (AnonymousClass491 e2) {
                C28691Op.A01("WaRcRichTextComponentBinderUtils", String.format("Failed to parse text color %s", A0I2), e2);
            }
        }
        String A0I4 = r20.A0I(42);
        if (A0I4 != null) {
            try {
                A0J.setSpan(new StyleSpan(AnonymousClass3JW.A09(A0I4)), 0, A0I.length(), 0);
            } catch (AnonymousClass491 e3) {
                C28691Op.A01("WaRcRichTextComponentBinderUtils", String.format("Failed to textStyle  %s", A0I3), e3);
            }
        }
        Matcher matcher = A00.matcher(A0I);
        int i4 = 0;
        while (matcher.find()) {
            String group = matcher.group();
            int A002 = C28421Nd.A00(group.replaceAll("[\\[\\]]", ""), -1);
            if (A002 >= 0 && A002 < r20.A0K().size()) {
                AnonymousClass28D A0U = C12980iv.A0U(r20.A0K(), A002);
                String A0I5 = A0U.A0I(38);
                if (objArr != null) {
                    A0I5 = MessageFormat.format(A0I5, objArr);
                }
                int start = matcher.start() + i4;
                if (A0I5 != null) {
                    int length = A0I5.length();
                    int length2 = group.length();
                    i4 += length - length2;
                    Iterator it2 = map.keySet().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (A0I5.contains(C12970iu.A0x(it2))) {
                                z2 = true;
                                break;
                            }
                        } else {
                            z2 = false;
                            break;
                        }
                    }
                    if (z2) {
                        A0J2 = C12990iw.A0J(AnonymousClass1Z4.A00(view.getContext(), A0I5));
                    } else {
                        A0J2 = C12990iw.A0J(A0I5);
                    }
                    String A0I6 = A0U.A0I(i3);
                    if (A0I6 != null) {
                        try {
                            A0J2.setSpan(new ForegroundColorSpan(AnonymousClass3JW.A05(A0I6)), i2, length, i2);
                        } catch (AnonymousClass491 unused) {
                            C28691Op.A00("WaRcRichTextComponentBinderUtils", "Error parsing TextSpan color");
                        }
                    }
                    AnonymousClass28D A0F2 = A0U.A0F(44);
                    if (A0F2 != null) {
                        A0J2.setSpan(new ForegroundColorSpan(AnonymousClass4Di.A00(r19, A0F2)), 0, length, 0);
                    }
                    String A0I7 = A0U.A0I(42);
                    if (A0I7 != null) {
                        try {
                            A0J2.setSpan(new AbsoluteSizeSpan(Math.round(TypedValue.applyDimension(2, AnonymousClass3JW.A03(A0I7), r19.A00.getResources().getDisplayMetrics()))), 0, length, 0);
                        } catch (AnonymousClass491 unused2) {
                            C28691Op.A00("WaRcRichTextComponentBinderUtils", "Error parsing TextSpan size");
                        }
                    }
                    String A0I8 = A0U.A0I(43);
                    if (A0I8 != null) {
                        try {
                            A0J2.setSpan(new StyleSpan(AnonymousClass3JW.A09(A0I8)), 0, length, 0);
                        } catch (AnonymousClass491 unused3) {
                            C28691Op.A00("WaRcRichTextComponentBinderUtils", "Error parsing TextSpan textStyle");
                        }
                    }
                    String A06 = AnonymousClass28D.A06(A0U);
                    if (A06 != null) {
                        A0J2.setSpan(new TypefaceSpan(A06), 0, length, 0);
                    }
                    AbstractC14200l1 A0G = A0U.A0G(36);
                    if (A0G != null) {
                        if (valueOf != null) {
                            i = valueOf.intValue();
                        } else {
                            i = 0;
                        }
                        if (A0F2 != null) {
                            i = AnonymousClass4Di.A00(r19, A0F2);
                        }
                        A0J2.setSpan(r21.AHg(new RunnableBRunnable0Shape3S0300000_I1(r20, A0G, r19, 11), i, -65536, 1711315404), 0, length, 0);
                    }
                    A0J.replace(start, length2 + start, (CharSequence) A0J2);
                }
            }
            i2 = 0;
            i3 = 40;
        }
        return A0J;
    }

    public static void A01(Spannable spannable, C004602b r7, AnonymousClass28D r8) {
        AbstractC28491Nn.A03((TextEmojiLabel) r7);
        r7.setText(spannable);
        String A0I = r8.A0I(38);
        if (A0I != null) {
            try {
                r7.setGravity(AnonymousClass3JW.A07(A0I));
            } catch (AnonymousClass491 e) {
                C28691Op.A01("WaRcRichTextComponentBinderUtils", String.format("Failed to parse text align %s", A0I), e);
            }
        }
        String A06 = AnonymousClass28D.A06(r8);
        if (A06 != null) {
            try {
                r7.setLineHeight((int) AnonymousClass3JW.A01(A06));
            } catch (AnonymousClass491 e2) {
                C28691Op.A01("WaRcRichTextComponentBinderUtils", String.format("Failed to parse line height pixel  %s", A06), e2);
            }
        }
    }
}
