package X;

import com.whatsapp.jid.Jid;
import com.whatsapp.protocol.VoipStanzaChildNode;

/* renamed from: X.2MM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2MM {
    public final Jid A00;
    public final VoipStanzaChildNode A01;
    public final String A02;
    public final String A03;
    public final boolean A04;

    public AnonymousClass2MM(Jid jid, VoipStanzaChildNode voipStanzaChildNode, String str, String str2, boolean z) {
        if (C15380n4.A0I(jid)) {
            this.A03 = str;
            this.A00 = jid;
            this.A02 = str2;
            this.A01 = voipStanzaChildNode;
            this.A04 = z;
            return;
        }
        StringBuilder sb = new StringBuilder("CallStanza:Wrong jid type: ");
        sb.append(jid);
        throw new IllegalArgumentException(sb.toString());
    }
}
