package X;

import android.media.AudioAttributes;

/* renamed from: X.3HR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3HR {
    public static final AnonymousClass3HR A03 = new AnonymousClass3HR(1);
    public AudioAttributes A00;
    public final int A01 = 1;
    public final int A02;

    public /* synthetic */ AnonymousClass3HR(int i) {
        this.A02 = i;
    }

    public AudioAttributes A00() {
        AudioAttributes audioAttributes = this.A00;
        if (audioAttributes != null) {
            return audioAttributes;
        }
        AudioAttributes.Builder usage = new AudioAttributes.Builder().setContentType(0).setFlags(0).setUsage(this.A02);
        if (AnonymousClass3JZ.A01 >= 29) {
            usage.setAllowedCapturePolicy(this.A01);
        }
        AudioAttributes build = usage.build();
        this.A00 = build;
        return build;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass3HR.class != obj.getClass()) {
                return false;
            }
            AnonymousClass3HR r5 = (AnonymousClass3HR) obj;
            if (!(this.A02 == r5.A02 && this.A01 == r5.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ((506447 + this.A02) * 31) + this.A01;
    }
}
