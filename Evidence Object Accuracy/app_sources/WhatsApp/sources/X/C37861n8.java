package X;

import java.io.InputStream;

/* renamed from: X.1n8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C37861n8 extends InputStream {
    public long A00 = 0;
    public InputStream A01;
    public byte[] A02 = new byte[1];

    @Override // java.io.InputStream
    public boolean markSupported() {
        return false;
    }

    public C37861n8(InputStream inputStream, long j) {
        this.A01 = inputStream;
        this.A00 = j;
    }

    @Override // java.io.InputStream
    public int available() {
        return this.A01.available();
    }

    @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.A01.close();
    }

    @Override // java.io.InputStream
    public void mark(int i) {
        throw new IllegalStateException("Can't mark");
    }

    @Override // java.io.InputStream
    public int read() {
        byte[] bArr = this.A02;
        int read = read(bArr, 0, 1);
        if (read == -1) {
            return -1;
        }
        if (read == 1) {
            return bArr[0] & 255;
        }
        StringBuilder sb = new StringBuilder("OIS unexpected return value: ");
        sb.append(read);
        throw new IllegalStateException(sb.toString());
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    @Override // java.io.InputStream
    public int read(byte[] bArr, int i, int i2) {
        int read = this.A01.read(bArr, i, i2);
        this.A00 += (long) read;
        return read;
    }

    @Override // java.io.InputStream
    public void reset() {
        throw new IllegalStateException("Can't reset");
    }

    @Override // java.io.InputStream
    public long skip(long j) {
        long skip = this.A01.skip(j);
        this.A00 += skip;
        return skip;
    }
}
