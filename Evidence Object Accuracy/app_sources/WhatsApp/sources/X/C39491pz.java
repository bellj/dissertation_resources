package X;

import java.util.HashMap;

/* renamed from: X.1pz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39491pz extends HashMap<Integer, String> {
    public C39491pz() {
        put(0, "NO_CATEGORY");
        put(1, "MANIFEST_WAITING");
        put(2, "MANIFEST_ERROR");
        put(3, "LOADING");
        put(4, "LOAD_FAILED");
        put(5, "UP_TO_DATE");
    }
}
