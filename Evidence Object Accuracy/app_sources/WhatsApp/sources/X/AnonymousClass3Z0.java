package X;

import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.community.deactivate.DeactivateCommunityDisclaimerActivity;

/* renamed from: X.3Z0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3Z0 implements AbstractC21730xt {
    public final AnonymousClass3CM A00;
    public final C17220qS A01;

    public AnonymousClass3Z0(AnonymousClass3CM r1, C17220qS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A00.A00(-1);
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        C16700pc.A0E(r4, 1);
        AnonymousClass1V8 A0E = r4.A0E("error");
        int i = -2;
        if (A0E != null) {
            i = A0E.A05("code", -2);
        }
        this.A00.A00(i);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        DeactivateCommunityDisclaimerActivity deactivateCommunityDisclaimerActivity = this.A00.A00;
        deactivateCommunityDisclaimerActivity.AaN();
        Object[] A1b = C12970iu.A1b();
        C15610nY r1 = deactivateCommunityDisclaimerActivity.A02;
        if (r1 != null) {
            C15370n3 r0 = deactivateCommunityDisclaimerActivity.A04;
            if (r0 == null) {
                throw C16700pc.A06("parentGroupContact");
            }
            String A0X = C12960it.A0X(deactivateCommunityDisclaimerActivity, r1.A04(r0), A1b, 0, R.string.deactivate_community_success);
            C16700pc.A0B(A0X);
            AnonymousClass12P r2 = ((ActivityC13790kL) deactivateCommunityDisclaimerActivity).A00;
            Intent A02 = C14960mK.A02(deactivateCommunityDisclaimerActivity);
            A02.putExtra("snackbar_message", A0X);
            A02.setFlags(67108864);
            r2.A06(deactivateCommunityDisclaimerActivity, A02);
            return;
        }
        throw C16700pc.A06("waContactNames");
    }
}
