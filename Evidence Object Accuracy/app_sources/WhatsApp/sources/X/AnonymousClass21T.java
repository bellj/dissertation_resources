package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import com.google.android.exoplayer2.Timeline;
import com.whatsapp.videoplayback.ExoPlaybackControlView;
import java.io.File;

/* renamed from: X.21T  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass21T {
    public AnonymousClass5V1 A00;
    public AnonymousClass5V2 A01;
    public AnonymousClass5V3 A02;
    public AnonymousClass5V4 A03;
    public AbstractC47472Av A04;

    public static AnonymousClass21T A00(Context context, C14900mE r14, AnonymousClass01d r15, AnonymousClass018 r16, AbstractC14440lR r17, File file, boolean z, boolean z2, boolean z3) {
        if (!z2 || !(!C38241nl.A02())) {
            String absolutePath = file.getAbsolutePath();
            if (!z3) {
                return new AnonymousClass39E(context, absolutePath, z);
            }
            return new C865347u(context, absolutePath, z);
        }
        AnonymousClass21S r3 = new AnonymousClass21S(AnonymousClass12P.A00(context), r14, r15, r16, r17, null, null, true, z3);
        r3.A07 = Uri.fromFile(file);
        r3.A0I = z;
        r3.A0F();
        r3.A0F = true;
        return r3;
    }

    public int A01() {
        long AC9;
        if (this instanceof AnonymousClass39E) {
            return ((AnonymousClass39E) this).A00.getCurrentPosition();
        }
        if (!(this instanceof AnonymousClass39G)) {
            if (!(this instanceof AnonymousClass21S)) {
                AC9 = ((AnonymousClass39F) this).A02.A00();
            } else {
                C47492Ax r0 = ((AnonymousClass21S) this).A08;
                if (r0 == null) {
                    return 0;
                }
                AC9 = r0.AC9();
            }
            return (int) AC9;
        }
        throw new UnsupportedOperationException("not implemented yet");
    }

    public int A02() {
        long ACb;
        if (this instanceof AnonymousClass39E) {
            return ((AnonymousClass39E) this).A00.getDuration();
        }
        if (this instanceof AnonymousClass39G) {
            return ((AnonymousClass39G) this).A03.A01.getDuration();
        }
        if (!(this instanceof AnonymousClass21S)) {
            ACb = ((AnonymousClass39F) this).A02.A03;
        } else {
            C47492Ax r0 = ((AnonymousClass21S) this).A08;
            if (r0 == null) {
                return 0;
            }
            ACb = r0.ACb();
        }
        return (int) ACb;
    }

    public Bitmap A03() {
        if (this instanceof AnonymousClass39E) {
            return ((AnonymousClass39E) this).A00.getBitmap();
        }
        if (this instanceof AnonymousClass39G) {
            AnonymousClass39G r4 = (AnonymousClass39G) this;
            Drawable current = r4.A01.getCurrent();
            if (current instanceof BitmapDrawable) {
                return ((BitmapDrawable) current).getBitmap();
            }
            Bitmap bitmap = r4.A00;
            if (bitmap == null || bitmap.isRecycled()) {
                r4.A00 = Bitmap.createBitmap(Math.max(current.getIntrinsicWidth(), 1), Math.max(current.getIntrinsicHeight(), 1), Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(r4.A00);
            current.draw(canvas);
            current.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            return r4.A00;
        } else if (!(this instanceof AnonymousClass21S)) {
            return null;
        } else {
            AnonymousClass21S r1 = (AnonymousClass21S) this;
            if (r1.A0M || r1.A08 == null || !r1.A0L) {
                return null;
            }
            return r1.A0Y.getCurrentFrame();
        }
    }

    public View A04() {
        if (this instanceof AnonymousClass39E) {
            return ((AnonymousClass39E) this).A00;
        }
        if (this instanceof AnonymousClass39G) {
            return ((AnonymousClass39G) this).A02;
        }
        if (!(this instanceof AnonymousClass21S)) {
            return ((AnonymousClass39F) this).A01;
        }
        return ((AnonymousClass21S) this).A0Y;
    }

    public void A05() {
        if (this instanceof AnonymousClass39E) {
            ((AnonymousClass39E) this).A00.pause();
        } else if (this instanceof AnonymousClass39G) {
            ((AnonymousClass39G) this).A01.stop();
        } else if (!(this instanceof AnonymousClass21S)) {
            AnonymousClass39F r1 = (AnonymousClass39F) this;
            r1.A02.A02();
            r1.A00.removeMessages(0);
        } else {
            C47492Ax r12 = ((AnonymousClass21S) this).A08;
            if (r12 != null) {
                r12.AcW(false);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06() {
        /*
        // Method dump skipped, instructions count: 328
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass21T.A06():void");
    }

    public void A07() {
        if (this instanceof AnonymousClass39E) {
            ((AnonymousClass39E) this).A00.start();
        } else if (this instanceof AnonymousClass39G) {
            ((AnonymousClass39G) this).A01.start();
        } else if (!(this instanceof AnonymousClass21S)) {
            AnonymousClass39F r0 = (AnonymousClass39F) this;
            C92784Xk r2 = r0.A02;
            r2.A01();
            Handler handler = r0.A00;
            handler.removeMessages(0);
            handler.sendEmptyMessageDelayed(0, (long) (((int) r2.A03) - ((int) r2.A00())));
        } else {
            AnonymousClass21S r22 = (AnonymousClass21S) this;
            r22.hashCode();
            if (r22.A08 != null) {
                r22.A0I();
                r22.A08.AcW(true);
                return;
            }
            r22.A0O = true;
            r22.A0F();
        }
    }

    public void A08() {
        AudioManager A0G;
        long j;
        if (this instanceof AnonymousClass39E) {
            AnonymousClass2CP r1 = ((AnonymousClass39E) this).A00;
            MediaPlayer mediaPlayer = r1.A09;
            if (mediaPlayer != null) {
                mediaPlayer.reset();
                r1.A09.release();
                r1.A09 = null;
                r1.A0H = false;
                r1.A00 = 0;
                r1.A03 = 0;
            }
        } else if (this instanceof AnonymousClass39G) {
            AnonymousClass39G r12 = (AnonymousClass39G) this;
            r12.A03.close();
            r12.A01.stop();
        } else if (!(this instanceof AnonymousClass21S)) {
            AnonymousClass39F r13 = (AnonymousClass39F) this;
            r13.A02.A02();
            r13.A00.removeMessages(0);
        } else {
            AnonymousClass21S r2 = (AnonymousClass21S) this;
            r2.hashCode();
            r2.A0N = false;
            r2.A0G = false;
            C47492Ax r0 = r2.A08;
            if (r0 != null) {
                r2.A0O = r0.AFj();
                r2.A08.AcW(false);
                r2.A0P = false;
                Timeline ACE = r2.A08.ACE();
                if (ACE != null && !ACE.A0D()) {
                    int ACF = r2.A08.ACF();
                    r2.A01 = ACF;
                    C94404bl A0B = ACE.A0B(new C94404bl(), ACF, 0);
                    if (!A0B.A0A) {
                        r2.A0P = true;
                        if (A0B.A0D) {
                            j = r2.A08.AC9();
                        } else {
                            j = -9223372036854775807L;
                        }
                        r2.A05 = j;
                    }
                }
                r2.A08.A0A(false);
                C47492Ax r02 = r2.A08;
                r02.A03();
                r02.A02();
                r02.A07(null, false);
                r02.A05(0, 0);
                r2.A08.AaK(r2.A0S);
                r2.A08.A01();
                r2.A08 = null;
                AbstractC47472Av r03 = ((AnonymousClass21T) r2).A04;
                if (r03 != null) {
                    r03.ATt(false, 1);
                }
                C47482Aw r04 = r2.A0Y;
                r04.A01 = null;
                AnonymousClass3FK r05 = r04.A03;
                if (r05 != null) {
                    r05.A00();
                }
                r2.A09 = null;
                ExoPlaybackControlView exoPlaybackControlView = r2.A0C;
                if (exoPlaybackControlView != null) {
                    exoPlaybackControlView.setPlayer(null);
                    ExoPlaybackControlView exoPlaybackControlView2 = r2.A0C;
                    exoPlaybackControlView2.removeCallbacks(exoPlaybackControlView2.A0O);
                    exoPlaybackControlView2.removeCallbacks(exoPlaybackControlView2.A0P);
                }
                if (!r2.A0F && (A0G = r2.A0U.A0G()) != null) {
                    AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener = r2.A06;
                    if (onAudioFocusChangeListener == null) {
                        onAudioFocusChangeListener = new C97974hw();
                        r2.A06 = onAudioFocusChangeListener;
                    }
                    A0G.abandonAudioFocus(onAudioFocusChangeListener);
                }
            }
        }
    }

    public void A09(int i) {
        if (this instanceof AnonymousClass39E) {
            ((AnonymousClass39E) this).A00.seekTo(i);
        } else if (this instanceof AnonymousClass39G) {
            throw new UnsupportedOperationException("not implemented yet");
        } else if (!(this instanceof AnonymousClass21S)) {
            AnonymousClass39F r3 = (AnonymousClass39F) this;
            C92784Xk r2 = r3.A02;
            r2.A00 = (long) i;
            r2.A01 = SystemClock.elapsedRealtime();
            Handler handler = r3.A00;
            handler.removeMessages(0);
            handler.sendEmptyMessageDelayed(0, (long) (((int) r2.A03) - ((int) r2.A00())));
        } else {
            AnonymousClass21S r0 = (AnonymousClass21S) this;
            C47492Ax r32 = r0.A08;
            if (r32 != null) {
                r32.AbS(r32.ACF(), (long) i);
                return;
            }
            r0.A03 = i;
        }
    }

    public void A0A(boolean z) {
        if (this instanceof AnonymousClass39E) {
            ((AnonymousClass39E) this).A00.setMute(z);
        } else if (!(this instanceof AnonymousClass39G) && (this instanceof AnonymousClass21S)) {
            AnonymousClass21S r0 = (AnonymousClass21S) this;
            r0.A0J = z;
            C47492Ax r1 = r0.A08;
            if (r1 != null) {
                float f = 1.0f;
                if (z) {
                    f = 0.0f;
                }
                r1.A04(f);
            }
        }
    }

    public boolean A0B() {
        if (this instanceof AnonymousClass39E) {
            return ((AnonymousClass39E) this).A00.isPlaying();
        }
        if (this instanceof AnonymousClass39G) {
            return ((AnonymousClass39G) this).A01.isRunning();
        }
        if (!(this instanceof AnonymousClass21S)) {
            return ((AnonymousClass39F) this).A02.A02;
        }
        AnonymousClass21S r3 = (AnonymousClass21S) this;
        C47492Ax r1 = r3.A08;
        if (r1 == null || r3.A0M) {
            return false;
        }
        int AFl = r1.AFl();
        if ((AFl == 3 || AFl == 2) && r3.A08.AFj()) {
            return true;
        }
        return false;
    }

    public boolean A0C() {
        if (this instanceof AnonymousClass39E) {
            return ((AnonymousClass39E) this).A00.A0H;
        }
        if (this instanceof AnonymousClass39G) {
            throw new UnsupportedOperationException("not implemented yet");
        } else if (!(this instanceof AnonymousClass21S)) {
            return true;
        } else {
            return ((AnonymousClass21S) this).A0N;
        }
    }

    public boolean A0D() {
        if ((this instanceof AnonymousClass39E) || (this instanceof AnonymousClass39G) || !(this instanceof AnonymousClass21S)) {
            return false;
        }
        return ((AnonymousClass21S) this).A0H;
    }
}
