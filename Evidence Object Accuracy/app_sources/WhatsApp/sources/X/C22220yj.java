package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0yj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22220yj {
    public AnonymousClass1BW A00;
    public final int A01;
    public final Comparator A02 = new Comparator() { // from class: X.1os
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return Float.compare(((AbstractC38871oq) obj2).AHi(), ((AbstractC38871oq) obj).AHi());
        }
    };
    public volatile List A03;

    public C22220yj(AnonymousClass1BW r2, int i) {
        this.A00 = r2;
        this.A01 = i;
    }

    public int A00() {
        int min;
        A05();
        synchronized (this) {
            min = Math.min(this.A01, this.A03.size());
        }
        return min;
    }

    public Object A01(int i) {
        Object ADD;
        A05();
        synchronized (this) {
            ADD = ((AbstractC38871oq) this.A03.get(i)).ADD();
        }
        return ADD;
    }

    public List A02() {
        List A03;
        A05();
        synchronized (this) {
            A03 = A03(Math.min(this.A01, this.A03.size()));
        }
        return A03;
    }

    public List A03(int i) {
        A05();
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            for (int i2 = 0; i2 < Math.min(i, this.A03.size()); i2++) {
                arrayList.add(((AbstractC38871oq) this.A03.get(i2)).ADD());
            }
        }
        return arrayList;
    }

    public Map A04() {
        HashMap hashMap;
        A05();
        synchronized (this) {
            hashMap = new HashMap();
            for (AbstractC38871oq r0 : this.A03) {
                hashMap.put(r0.ADD(), Float.valueOf(r0.AHi()));
            }
        }
        return hashMap;
    }

    public void A05() {
        if (this.A03 == null) {
            synchronized (this) {
                if (this.A03 == null) {
                    this.A03 = Collections.synchronizedList(this.A00.AIX());
                }
            }
        }
    }

    public void A06(Object obj) {
        A05();
        synchronized (this) {
            int size = this.A03.size();
            while (true) {
                size--;
                if (size < 0) {
                    this.A00.AZH(this.A03);
                } else if (((AbstractC38871oq) this.A03.get(size)).A7R(obj)) {
                    A08(size);
                }
            }
        }
    }

    public boolean A07(Object obj) {
        boolean z;
        A05();
        synchronized (this) {
            z = false;
            for (AbstractC38871oq r3 : this.A03) {
                float AHi = r3.AHi();
                if (!r3.A7R(obj)) {
                    r3.AdB(((float) Math.round((AHi * 0.9f) * 100.0f)) / 100.0f);
                } else {
                    r3.AdB(((float) Math.round((AHi + 1.0f) * 100.0f)) / 100.0f);
                    z = true;
                }
            }
            if (!z) {
                A09(this.A00.A8G(obj, 1.0f));
            }
            Collections.sort(this.A03, this.A02);
            AnonymousClass009.A05(this.A03);
            int size = this.A03.size();
            while (true) {
                size--;
                if (size >= this.A01) {
                    A08(size);
                } else {
                    this.A00.AZH(this.A03);
                }
            }
        }
        return z;
    }

    public void A08(int i) {
        A05();
        synchronized (this) {
            this.A03.remove(i);
        }
    }

    public void A09(AbstractC38871oq r2) {
        A05();
        synchronized (this) {
            this.A03.add(r2);
        }
    }
}
