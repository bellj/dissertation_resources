package X;

import android.os.Bundle;

/* renamed from: X.5br  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118465br extends AnonymousClass0Yo {
    public final /* synthetic */ Bundle A00;
    public final /* synthetic */ AbstractC16870pt A01;
    public final /* synthetic */ C129395xe A02;

    public C118465br(Bundle bundle, AbstractC16870pt r2, C129395xe r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = bundle;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C118185bP.class)) {
            C129395xe r1 = this.A02;
            C14830m7 r2 = r1.A07;
            C14900mE r22 = r1.A00;
            C15570nT r23 = r1.A01;
            C16590pI r24 = r1.A08;
            AbstractC14440lR r25 = r1.A0Q;
            C241414j r26 = r1.A0E;
            AnonymousClass14X r27 = r1.A0P;
            C15550nR r28 = r1.A05;
            AnonymousClass01d r29 = r1.A06;
            AnonymousClass018 r210 = r1.A09;
            C17070qD r211 = r1.A0K;
            C238013b r212 = r1.A04;
            C15650ng r15 = r1.A0A;
            AnonymousClass1AA r14 = r1.A03;
            AnonymousClass604 r13 = r1.A0N;
            C21860y6 r12 = r1.A0H;
            C20300vX r11 = r1.A0B;
            C22710zW r10 = r1.A0J;
            AnonymousClass102 r9 = r1.A0D;
            C14650lo r8 = r1.A02;
            C129925yW r7 = r1.A0F;
            AbstractC16870pt r6 = this.A01;
            AnonymousClass17Z r5 = r1.A0M;
            C20370ve r4 = r1.A0C;
            AnonymousClass1A7 r3 = r1.A0L;
            C243515e r213 = r1.A0I;
            return new C118185bP(this.A00, r22, r23, r8, r14, r212, r28, r29, r2, r24, r210, r15, r11, r4, r9, r26, r7, r1.A0G, r12, r213, r10, r211, r3, r6, r5, r13, r27, r25);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
