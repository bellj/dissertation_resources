package X;

import android.text.TextUtils;

/* renamed from: X.1XQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1XQ extends AnonymousClass1XR implements AbstractC28871Pi, AbstractC16400ox, AbstractC16420oz {
    public C28891Pk A00;

    public AnonymousClass1XQ(C16150oX r9, AnonymousClass1IS r10, AnonymousClass1XQ r11, long j) {
        super(r9, r10, (AnonymousClass1XR) r11, j, true);
        this.A00 = r11.A00.A00();
    }

    public AnonymousClass1XQ(AnonymousClass1IS r2, long j) {
        super(r2, (byte) 29, j);
    }

    @Override // X.AbstractC28871Pi
    public String ADA() {
        if (TextUtils.isEmpty(this.A00.A02)) {
            return this.A00.A01;
        }
        StringBuilder sb = new StringBuilder();
        C28891Pk r1 = this.A00;
        sb.append(r1.A01);
        sb.append(" ");
        sb.append(r1.A02);
        return sb.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AEh(AnonymousClass018 r3) {
        StringBuilder sb = new StringBuilder("👾 ");
        sb.append(this.A00.A01);
        return sb.toString();
    }

    @Override // X.AbstractC28871Pi
    public String AFs() {
        return this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public String AG2() {
        return this.A00.A01;
    }

    @Override // X.AbstractC28871Pi
    public C28891Pk AH7() {
        return this.A00;
    }

    @Override // X.AbstractC28871Pi
    public void Acz(C28891Pk r1) {
        this.A00 = r1;
    }
}
