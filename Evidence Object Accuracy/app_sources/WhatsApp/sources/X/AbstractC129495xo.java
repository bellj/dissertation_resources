package X;

import android.text.TextUtils;
import com.whatsapp.R;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/* renamed from: X.5xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC129495xo {
    public Map A00 = C12970iu.A11();
    public final int A01;

    public AbstractC129495xo(int i) {
        this.A01 = i;
    }

    public C127705uv A00() {
        int i;
        int i2;
        int i3;
        int i4;
        if (this instanceof C121075hD) {
            i = this.A01;
            i2 = R.drawable.novi_logo;
            i3 = R.string.novi_title;
            i4 = R.string.novi_advantage;
        } else if (!(this instanceof C121065hC)) {
            return null;
        } else {
            i = this.A01;
            i2 = 0;
            i3 = R.string.facebook_pay;
            i4 = R.string.fbpay_advantage;
        }
        return new C127705uv(i, i2, i3, i4);
    }

    public void A01(AbstractC16830pp r5, Set set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (!TextUtils.isEmpty(A0x)) {
                this.A00.put(A0x.toUpperCase(Locale.US), r5);
            }
        }
    }
}
