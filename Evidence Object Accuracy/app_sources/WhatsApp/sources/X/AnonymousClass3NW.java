package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import com.whatsapp.invites.InviteGroupParticipantsActivity;

/* renamed from: X.3NW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NW implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ InviteGroupParticipantsActivity A01;

    public AnonymousClass3NW(View view, InviteGroupParticipantsActivity inviteGroupParticipantsActivity) {
        this.A01 = inviteGroupParticipantsActivity;
        this.A00 = view;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        View view = this.A00;
        C12980iv.A1F(view, this);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(200);
        view.startAnimation(translateAnimation);
    }
}
