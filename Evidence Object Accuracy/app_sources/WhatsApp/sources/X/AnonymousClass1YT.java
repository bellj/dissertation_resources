package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1YT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YT {
    public int A00;
    public int A01;
    public long A02;
    public long A03;
    public GroupJid A04;
    public AbstractC30391Xf A05;
    public AnonymousClass1YS A06;
    public String A07;
    public boolean A08;
    public final long A09;
    public final DeviceJid A0A;
    public final AnonymousClass1YU A0B;
    public final Map A0C = new LinkedHashMap();
    public final boolean A0D;
    public final boolean A0E;
    public volatile AnonymousClass1YW A0F;
    public volatile boolean A0G;
    public volatile boolean A0H;
    public transient boolean A0I;

    public AnonymousClass1YT(AnonymousClass1YW r5, DeviceJid deviceJid, GroupJid groupJid, AbstractC30391Xf r8, AnonymousClass1YU r9, AnonymousClass1YS r10, String str, Collection collection, int i, int i2, long j, long j2, long j3, boolean z, boolean z2, boolean z3, boolean z4) {
        this.A0B = r9;
        this.A05 = r8;
        this.A03 = j;
        this.A09 = j2;
        this.A0H = z;
        this.A01 = i;
        this.A00 = i2;
        this.A02 = j3;
        this.A0E = z2;
        this.A0D = z3;
        this.A04 = groupJid;
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AnonymousClass1YV r2 = (AnonymousClass1YV) it.next();
            this.A0C.put(r2.A02, r2);
        }
        this.A0G = z4;
        this.A0A = deviceJid;
        this.A07 = str;
        this.A06 = r10;
        this.A0F = r5;
    }

    public static AnonymousClass1YT A00(GroupJid groupJid, AbstractC30391Xf r22, List list, int i, int i2, int i3, long j, long j2, long j3, boolean z, boolean z2, boolean z3) {
        AnonymousClass1IS r4 = r22.A0z;
        AbstractC14640lm r3 = r4.A00;
        UserJid of = UserJid.of(r3);
        if (!C15380n4.A0L(of)) {
            StringBuilder sb = new StringBuilder("CallLog/fromFMessage V1 bad UserJid: ");
            sb.append(r3);
            Log.e(sb.toString());
            return null;
        }
        return new AnonymousClass1YT(null, null, groupJid, r22, new AnonymousClass1YU(i, of, r4.A01, r4.A02), null, null, list, i2, i3, j, j2, j3, z, false, z2, z3);
    }

    public static AnonymousClass1YT A01(AbstractC30391Xf r20, int i, int i2, long j, boolean z, boolean z2) {
        AnonymousClass1IS r1 = r20.A0z;
        AbstractC14640lm r2 = r1.A00;
        UserJid of = UserJid.of(r2);
        if (!C15380n4.A0L(of)) {
            StringBuilder sb = new StringBuilder("CallLog/fromFMessage Legacy bad UserJid: ");
            sb.append(r2);
            Log.e(sb.toString());
            return null;
        }
        AnonymousClass1YU r5 = new AnonymousClass1YU(-1, of, r1.A01, r1.A02);
        long j2 = r20.A0I;
        return new AnonymousClass1YT(null, DeviceJid.of(of), null, r20, r5, null, null, Collections.emptyList(), i, i2, -1, j2, j, z, true, z2, false);
    }

    public synchronized long A02() {
        return this.A03;
    }

    public AnonymousClass1YU A03() {
        AnonymousClass1YU r0 = this.A0B;
        UserJid userJid = r0.A01;
        boolean z = r0.A03;
        return new AnonymousClass1YU(r0.A00, userJid, r0.A02, z);
    }

    public List A04() {
        return new ArrayList(this.A0C.values());
    }

    public synchronized void A05() {
        this.A0I = false;
    }

    public synchronized void A06(int i) {
        if (this.A00 != i) {
            this.A0I = true;
        }
        this.A00 = i;
    }

    public synchronized void A07(long j) {
        this.A03 = j;
    }

    public synchronized void A08(UserJid userJid, int i) {
        Map map = this.A0C;
        AnonymousClass1YV r1 = (AnonymousClass1YV) map.get(userJid);
        if (r1 != null) {
            synchronized (r1) {
                r1.A00 = i;
                r1.A03 = true;
            }
        } else {
            AnonymousClass1YV r12 = new AnonymousClass1YV(userJid, i, -1);
            map.put(r12.A02, r12);
            this.A0I = true;
        }
    }

    public synchronized void A09(AnonymousClass1YS r2) {
        this.A08 = true;
        this.A0I = true;
        this.A06 = r2;
    }

    public synchronized void A0A(boolean z) {
        if (this.A0H != z) {
            this.A0I = true;
        }
        this.A0H = z;
    }

    public boolean A0B() {
        return this.A0C.size() >= 2 || this.A0F != null;
    }

    public synchronized boolean A0C() {
        if (!this.A0I && this.A03 != -1) {
            for (AnonymousClass1YV r0 : this.A0C.values()) {
                if (r0.A01()) {
                }
            }
            return false;
        }
        return true;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass1YT r7 = (AnonymousClass1YT) obj;
            if (!(this.A03 == r7.A03 && this.A0B.equals(r7.A0B) && this.A09 == r7.A09 && this.A0H == r7.A0H && this.A01 == r7.A01 && this.A02 == r7.A02 && this.A00 == r7.A00 && this.A0E == r7.A0E && this.A0D == r7.A0D && C29941Vi.A00(this.A04, r7.A04) && this.A0G == r7.A0G && this.A0C.equals(r7.A0C) && C29941Vi.A00(this.A0A, r7.A0A) && C29941Vi.A00(this.A07, r7.A07) && C29941Vi.A00(this.A0F, r7.A0F))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.A03), this.A0B, Long.valueOf(this.A09), Boolean.valueOf(this.A0H), Integer.valueOf(this.A01), Long.valueOf(this.A02), Integer.valueOf(this.A00), Boolean.valueOf(this.A0E), Boolean.valueOf(this.A0D), this.A0C, this.A04, Boolean.valueOf(this.A0G), this.A0A, this.A07, this.A06, this.A0F});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("CallLog[rowId=");
        sb.append(this.A03);
        sb.append(", key=");
        sb.append(this.A0B);
        sb.append(", timestamp=");
        sb.append(this.A09);
        sb.append(", videoCall=");
        sb.append(this.A0H);
        sb.append(", duration=");
        sb.append(this.A01);
        sb.append(", bytesTransferred=");
        sb.append(this.A02);
        sb.append(", callResult=");
        sb.append(this.A00);
        sb.append(", isLegacy=");
        sb.append(this.A0E);
        sb.append(", fromMissedCall=");
        sb.append(this.A0D);
        sb.append(", groupJid=");
        sb.append(this.A04);
        sb.append(", isJoinableGroupCall=");
        sb.append(this.A0G);
        sb.append(", participants.size=");
        sb.append(this.A0C.size());
        sb.append(", callCreatorDeviceJid=");
        sb.append(this.A0A);
        sb.append(", callRandomId=");
        sb.append(this.A07);
        sb.append(", joinableData=");
        sb.append(this.A06);
        sb.append(", callLinkData=");
        sb.append(this.A0F);
        sb.append("]");
        return sb.toString();
    }
}
