package X;

import android.content.Context;

/* renamed from: X.4Xe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC92724Xe {
    public final C18660so A00;
    public final AnonymousClass17T A01;
    public final AbstractC16870pt A02;

    public abstract void A00(Context context, String str);

    public abstract void A01(String str);

    public AbstractC92724Xe(C18660so r1, AnonymousClass17T r2, AbstractC16870pt r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public boolean A02() {
        return !this.A00.A0A();
    }
}
