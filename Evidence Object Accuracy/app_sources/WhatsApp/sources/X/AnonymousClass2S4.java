package X;

import com.whatsapp.util.Log;

/* renamed from: X.2S4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2S4 implements AnonymousClass2S5 {
    public final /* synthetic */ AnonymousClass2S2 A00;
    public final /* synthetic */ AnonymousClass17Z A01;
    public final /* synthetic */ C50952Rz A02;
    public final /* synthetic */ String A03;

    public AnonymousClass2S4(AnonymousClass2S2 r1, AnonymousClass17Z r2, C50952Rz r3, String str) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = str;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2S5
    public void APo(C452120p r3) {
        StringBuilder sb = new StringBuilder("PAY: PaymentIncentiveManager/getIncentiveOfferInfo/getOfferDetails/onError : ");
        sb.append(r3);
        Log.e(sb.toString());
        AnonymousClass2S2 r1 = this.A00;
        if (r1 != null) {
            Log.e("PAY: PaymentIncentiveManager/syncIncentiveData/refreshGetIncentiveOfferInfo failed");
            r1.A00.APk();
        }
    }

    @Override // X.AnonymousClass2S5
    public void AX2(AnonymousClass1V8 r9) {
        AnonymousClass17Z r6 = this.A01;
        C50952Rz r2 = this.A02;
        String str = this.A03;
        AnonymousClass2S2 r5 = this.A00;
        try {
            C50942Ry r7 = new C50942Ry(r6.A08, r2, r9, str);
            C50922Rw r4 = r6.A0F;
            long j = r7.A08.A01;
            C50942Ry r3 = (C50942Ry) r4.A02.get(Long.valueOf(j));
            if (r3 != null) {
                r7.A02 = r3.A02;
                r7.A00 = r3.A00;
                r7.A01 = r3.A01;
            }
            r4.A02(r7, j);
            if (r5 != null) {
                r5.A00(r7);
            }
        } catch (Exception e) {
            Log.e("PAY: PaymentIncentiveManager/processSuccessfulGetOfferDetails : Error while parsing ", e);
            r6.A04();
            if (r5 != null) {
                Log.e("PAY: PaymentIncentiveManager/syncIncentiveData/refreshGetIncentiveOfferInfo failed");
                r5.A00.APk();
            }
        }
    }
}
