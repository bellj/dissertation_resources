package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4xP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107484xP implements AnonymousClass5YX {
    public static final Parcelable.Creator CREATOR = C72463ee.A0A(8);
    public final int A00;
    public final int A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final boolean A05;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C107484xP(Parcel parcel) {
        this.A00 = parcel.readInt();
        this.A02 = parcel.readString();
        this.A03 = parcel.readString();
        this.A04 = parcel.readString();
        this.A05 = C12960it.A1S(parcel.readInt());
        this.A01 = parcel.readInt();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0007, code lost:
        if (r6 > 0) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C107484xP(java.lang.String r2, java.lang.String r3, java.lang.String r4, int r5, int r6, boolean r7) {
        /*
            r1 = this;
            r1.<init>()
            r0 = -1
            if (r6 == r0) goto L_0x0009
            r0 = 0
            if (r6 <= 0) goto L_0x000a
        L_0x0009:
            r0 = 1
        L_0x000a:
            X.C95314dV.A03(r0)
            r1.A00 = r5
            r1.A02 = r2
            r1.A03 = r3
            r1.A04 = r4
            r1.A05 = r7
            r1.A01 = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107484xP.<init>(java.lang.String, java.lang.String, java.lang.String, int, int, boolean):void");
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ byte[] AHp() {
        return null;
    }

    @Override // X.AnonymousClass5YX
    public /* synthetic */ C100614mC AHq() {
        return null;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C107484xP.class != obj.getClass()) {
                return false;
            }
            C107484xP r5 = (C107484xP) obj;
            if (this.A00 != r5.A00 || !AnonymousClass3JZ.A0H(this.A02, r5.A02) || !AnonymousClass3JZ.A0H(this.A03, r5.A03) || !AnonymousClass3JZ.A0H(this.A04, r5.A04) || this.A05 != r5.A05 || this.A01 != r5.A01) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = 0;
        int A05 = (((C72453ed.A05(this.A00) + C72453ed.A0E(this.A02)) * 31) + C72453ed.A0E(this.A03)) * 31;
        String str = this.A04;
        if (str != null) {
            i = str.hashCode();
        }
        return ((((A05 + i) * 31) + (this.A05 ? 1 : 0)) * 31) + this.A01;
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("IcyHeaders: name=\"");
        A0k.append(this.A03);
        A0k.append("\", genre=\"");
        A0k.append(this.A02);
        A0k.append("\", bitrate=");
        A0k.append(this.A00);
        A0k.append(", metadataInterval=");
        return C12960it.A0f(A0k, this.A01);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A00);
        parcel.writeString(this.A02);
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
        parcel.writeInt(this.A05 ? 1 : 0);
        parcel.writeInt(this.A01);
    }
}
