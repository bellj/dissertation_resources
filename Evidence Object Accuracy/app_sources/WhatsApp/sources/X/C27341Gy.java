package X;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: X.1Gy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27341Gy extends AbstractC250017s {
    public final C250217u A00;
    public final C233411h A01;
    public final C14830m7 A02;
    public final C16510p9 A03;
    public final C15650ng A04;
    public final C16490p7 A05;
    public final C242114q A06;

    public C27341Gy(C250217u r1, C233411h r2, C14830m7 r3, C16510p9 r4, C15650ng r5, C16490p7 r6, C242114q r7, C233511i r8) {
        super(r8);
        this.A02 = r3;
        this.A03 = r4;
        this.A04 = r5;
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A06 = r7;
    }

    public final List A08(Cursor cursor, C16310on r15, boolean z) {
        C461824w A02;
        ArrayList arrayList = new ArrayList();
        long A00 = this.A02.A00();
        while (cursor.moveToNext()) {
            AbstractC14640lm A05 = this.A03.A05(cursor.getLong(cursor.getColumnIndexOrThrow("chat_row_id")));
            if (!(A05 == null || (A02 = this.A00.A02(cursor, r15, A05)) == null)) {
                arrayList.add(new C34591gO(null, A02.A02, new AnonymousClass1IS(A02.A01, A02.A03, A02.A04), null, A00, z, false));
            }
        }
        return arrayList;
    }

    public final void A09(C34591gO r5, AbstractC15340mz r6) {
        boolean z = r5.A02;
        boolean z2 = r6.A0v;
        if (z) {
            if (!z2) {
                this.A06.A01(Collections.singleton(r6), true, false);
            }
        } else if (z2) {
            this.A06.A03(Collections.singleton(r6), false);
        }
    }
}
