package X;

import android.content.DialogInterface;
import java.lang.ref.WeakReference;

/* renamed from: X.0V4  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V4 implements DialogInterface.OnClickListener {
    public final WeakReference A00;

    public AnonymousClass0V4(AnonymousClass0EP r2) {
        this.A00 = new WeakReference(r2);
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        WeakReference weakReference = this.A00;
        if (weakReference.get() != null) {
            ((AnonymousClass0EP) weakReference.get()).A06(true);
        }
    }
}
