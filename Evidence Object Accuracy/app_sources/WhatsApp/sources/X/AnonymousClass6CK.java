package X;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiCheckOrderDetailsActivity;
import com.whatsapp.payments.ui.PaymentBottomSheet;
import com.whatsapp.payments.ui.widget.PaymentDescriptionRow;
import com.whatsapp.payments.ui.widget.PaymentMethodRow;

/* renamed from: X.6CK  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6CK implements AbstractC124835qC {
    public final /* synthetic */ AbstractC30791Yv A00;
    public final /* synthetic */ C30821Yy A01;
    public final /* synthetic */ AnonymousClass2S0 A02;
    public final /* synthetic */ AbstractActivityC121525iS A03;
    public final /* synthetic */ PaymentBottomSheet A04;

    @Override // X.AbstractC124835qC
    public boolean AdV() {
        return false;
    }

    @Override // X.AbstractC124835qC
    public void Adj(AbstractC28901Pl r1, PaymentMethodRow paymentMethodRow) {
    }

    @Override // X.AbstractC124835qC
    public boolean Adt() {
        return true;
    }

    public AnonymousClass6CK(AbstractC30791Yv r1, C30821Yy r2, AnonymousClass2S0 r3, AbstractActivityC121525iS r4, PaymentBottomSheet paymentBottomSheet) {
        this.A03 = r4;
        this.A04 = paymentBottomSheet;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC124835qC
    public void A6F(ViewGroup viewGroup) {
        C50942Ry r8;
        AbstractActivityC121525iS r5 = this.A03;
        if (!r5.A0X.A01(r5.A0B, r5.A0o)) {
            TextView A0I = C12960it.A0I(r5.getLayoutInflater().inflate(R.layout.confirm_payment_total_amount_row, viewGroup, true), R.id.amount);
            AbstractC30791Yv r2 = this.A00;
            AnonymousClass018 r0 = ((AbstractActivityC121545iU) r5).A01;
            C30821Yy r7 = this.A01;
            A0I.setText(r2.AAA(r0, r7, 0));
            if (!TextUtils.isEmpty(((AbstractActivityC121665jA) r5).A0F) && r5.A3Y()) {
                TextView textView = (TextView) r5.getLayoutInflater().inflate(R.layout.confirm_payment_debit_warning, viewGroup, false);
                AnonymousClass2GE.A08(textView, r5.getResources().getColor(R.color.secondary_text));
                viewGroup.addView(textView);
            }
            AnonymousClass2S0 r02 = this.A02;
            if (r02 != null && (r8 = r02.A01) != null) {
                View r4 = new C117745aX(r5, ((AbstractActivityC121545iU) r5).A01, r7, r8, ((AbstractActivityC121685jC) r5).A01, false);
                int i = ((AbstractActivityC121685jC) r5).A01;
                if (i != 0) {
                    if (i != 1) {
                        if (!(i == 2 || i == 3)) {
                            if (i != 4) {
                                if (!(i == 5 || i == 7)) {
                                    return;
                                }
                            } else if (r8.A01 == 0) {
                                viewGroup.addView(r4);
                                ((AbstractActivityC121685jC) r5).A0T.A05(-1, 1);
                                return;
                            } else {
                                return;
                            }
                        }
                    } else if (r8.A00 == 0) {
                        viewGroup.addView(r4);
                        ((AbstractActivityC121685jC) r5).A0T.A05(1, -1);
                        return;
                    } else {
                        return;
                    }
                }
                viewGroup.addView(r4);
            }
        }
    }

    @Override // X.AbstractC124835qC
    public String ABX(AbstractC28901Pl r4, int i) {
        AbstractActivityC121525iS r2 = this.A03;
        boolean A1k = AbstractActivityC119235dO.A1k(r4, r2);
        int i2 = R.string.payments_send_payment_text;
        if (A1k) {
            i2 = R.string.payments_use_another_payment_method_button_text;
        }
        return r2.getString(i2);
    }

    @Override // X.AbstractC124835qC
    public String ACJ(AbstractC28901Pl r3) {
        return this.A03.getString(R.string.payments_send_payment_using);
    }

    @Override // X.AbstractC124835qC
    public String ACK(AbstractC28901Pl r5) {
        AbstractActivityC121525iS r3 = this.A03;
        return C1311161i.A02(r3, ((AbstractActivityC121545iU) r3).A01, r5, ((AbstractActivityC121685jC) r3).A0P, false);
    }

    @Override // X.AbstractC124835qC
    public String ACf(AbstractC28901Pl r3, int i) {
        AbstractActivityC121525iS r1 = this.A03;
        if (AbstractActivityC119235dO.A1k(r3, r1)) {
            return r1.getString(R.string.payment_method_overdraft_p2p_education_text);
        }
        return null;
    }

    @Override // X.AbstractC124835qC
    public String AEO(AbstractC28901Pl r6) {
        AbstractActivityC121525iS r4 = this.A03;
        String A00 = C1329668y.A00(((AbstractActivityC121665jA) r4).A0B);
        if (!TextUtils.isEmpty(A00)) {
            return C12960it.A0X(r4, A00, C12970iu.A1b(), 0, R.string.india_upi_payment_id_with_upi_label);
        }
        return null;
    }

    @Override // X.AbstractC124835qC
    public void AMM(ViewGroup viewGroup) {
        AbstractActivityC121525iS r4 = this.A03;
        C129515xq r2 = r4.A0X;
        boolean A3Y = r4.A3Y();
        int i = 0;
        boolean A1W = C12960it.A1W(((AbstractActivityC121665jA) r4).A0F);
        if (!A3Y || A1W || !r2.A01.A07(1718) || r4.A0X.A01(r4.A0B, r4.A0o)) {
            i = 8;
        }
        viewGroup.setVisibility(i);
        PaymentDescriptionRow paymentDescriptionRow = (PaymentDescriptionRow) viewGroup.findViewById(R.id.payment_description_row);
        r4.A0V = paymentDescriptionRow;
        paymentDescriptionRow.A01(r4.A0a);
    }

    @Override // X.AbstractC124835qC
    public void AMN(ViewGroup viewGroup) {
        AnonymousClass6BE r7;
        Integer num;
        String str;
        boolean z;
        boolean A1p;
        String str2;
        String str3;
        AnonymousClass3FW A00;
        String str4;
        AbstractActivityC121525iS r6 = this.A03;
        boolean A01 = r6.A0X.A01(r6.A0B, r6.A0o);
        Integer A0i = C12980iv.A0i();
        LayoutInflater layoutInflater = r6.getLayoutInflater();
        if (A01) {
            View inflate = layoutInflater.inflate(R.layout.use_another_payment_account_bottom_sheet_header, viewGroup, true);
            C117295Zj.A0o(C117295Zj.A07(r6, inflate, C12960it.A0I(inflate, R.id.text), R.string.overdraft_payment_non_p2m_bottom_sheet_title), this, this.A04, 14);
            r7 = ((AbstractActivityC121665jA) r6).A0D;
            num = null;
            str = r6.A0d;
            z = false;
            A1p = AbstractActivityC121685jC.A1p(r6);
            str2 = ((AbstractActivityC121685jC) r6).A0g;
            str3 = ((AbstractActivityC121685jC) r6).A0f;
            str4 = "add_credential_prompt";
            A00 = null;
        } else {
            View inflate2 = layoutInflater.inflate(R.layout.confirm_dialog_title, viewGroup, true);
            ImageView A07 = C117295Zj.A07(r6, inflate2, C12960it.A0I(inflate2, R.id.text), R.string.confirm_payment_bottom_sheet_default_title);
            C30821Yy r3 = this.A01;
            AnonymousClass2S0 r2 = this.A02;
            A07.setOnClickListener(new View.OnClickListener(r3, r2, this, this.A04) { // from class: X.64Z
                public final /* synthetic */ C30821Yy A00;
                public final /* synthetic */ AnonymousClass2S0 A01;
                public final /* synthetic */ AnonymousClass6CK A02;
                public final /* synthetic */ PaymentBottomSheet A03;

                {
                    this.A02 = r3;
                    this.A00 = r1;
                    this.A01 = r2;
                    this.A03 = r4;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AnonymousClass6CK r0 = this.A02;
                    C30821Yy r62 = this.A00;
                    AnonymousClass2S0 r5 = this.A01;
                    PaymentBottomSheet paymentBottomSheet = this.A03;
                    AbstractActivityC121525iS r32 = r0.A03;
                    r32.A3V(AnonymousClass61I.A00(((ActivityC13790kL) r32).A05, r62, r5, null, true), "payment_confirm_prompt", 1);
                    paymentBottomSheet.A1K();
                }
            });
            r7 = ((AbstractActivityC121665jA) r6).A0D;
            num = null;
            str = r6.A0d;
            z = false;
            A1p = AbstractActivityC121685jC.A1p(r6);
            str2 = ((AbstractActivityC121685jC) r6).A0g;
            str3 = ((AbstractActivityC121685jC) r6).A0f;
            A00 = AnonymousClass61I.A00(((ActivityC13790kL) r6).A05, r3, r2, null, true);
            str4 = "payment_confirm_prompt";
        }
        r7.AKj(A00, A0i, num, str4, str, str2, str3, z, A1p);
    }

    @Override // X.AbstractC124835qC
    public void AMP(ViewGroup viewGroup) {
        boolean z;
        AbstractActivityC121525iS r4 = this.A03;
        if (!r4.A0X.A01(r4.A0B, r4.A0o)) {
            View inflate = r4.getLayoutInflater().inflate(R.layout.india_upi_confirm_payment_recipient_row, viewGroup, true);
            ImageView A0K = C12970iu.A0K(inflate, R.id.payment_recipient_profile_pic);
            TextView A0I = C12960it.A0I(inflate, R.id.payment_recipient_name);
            TextView A0I2 = C12960it.A0I(inflate, R.id.payment_recipient_vpa);
            View A0D = AnonymousClass028.A0D(inflate, R.id.expand_receiver_details_button);
            if (r4.A3Z()) {
                A0D.setVisibility(0);
                if (!(r4 instanceof IndiaUpiCheckOrderDetailsActivity)) {
                    z = !r4.A3Y();
                } else {
                    z = false;
                }
                int i = 45;
                if (z) {
                    i = 46;
                }
                C117295Zj.A0n(inflate, this, i);
            } else {
                A0D.setVisibility(8);
                inflate.setOnClickListener(null);
            }
            C15370n3 r1 = r4.A08;
            if (r1 != null) {
                r4.A04.A06(A0K, r1);
                A0I.setText(r4.A3K());
                if (AnonymousClass1ZS.A02(((AbstractActivityC121665jA) r4).A08)) {
                    A0I2.setVisibility(8);
                    return;
                }
            } else {
                r4.A01.A05(A0K, R.drawable.avatar_contact);
                C117315Zl.A0N(A0I, C117295Zj.A0R(((AbstractActivityC121665jA) r4).A06));
            }
            Object obj = ((AbstractActivityC121665jA) r4).A08.A00;
            AnonymousClass009.A05(obj);
            A0I2.setText(C12960it.A0X(r4, obj, new Object[1], 0, R.string.india_upi_payment_id_with_upi_label));
        }
    }

    @Override // X.AbstractC124835qC
    public void AQh(ViewGroup viewGroup, AbstractC28901Pl r4) {
        AbstractActivityC121525iS r1 = this.A03;
        AbstractActivityC119235dO.A0p(r1.getLayoutInflater(), viewGroup, r1);
    }

    @Override // X.AbstractC124835qC
    public boolean AdO(AbstractC28901Pl r2, int i) {
        return AbstractActivityC119235dO.A1k(r2, this.A03);
    }

    @Override // X.AbstractC124835qC
    public boolean AdU(AbstractC28901Pl r2) {
        return !AbstractActivityC119235dO.A1k(r2, this.A03);
    }
}
