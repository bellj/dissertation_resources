package X;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;

/* renamed from: X.3pJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78683pJ extends AnonymousClass1U5 implements AnonymousClass5SX {
    public static final Parcelable.Creator CREATOR = new C99114jm();
    public int A00;
    public Intent A01;
    public final int A02;

    public C78683pJ() {
        this(2, 0, null);
    }

    public C78683pJ(int i, int i2, Intent intent) {
        this.A02 = i;
        this.A00 = i2;
        this.A01 = intent;
    }

    @Override // X.AnonymousClass5SX
    public final Status AGw() {
        if (this.A00 == 0) {
            return Status.A09;
        }
        return Status.A05;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A02);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A0A(parcel, this.A01, i, A00);
    }
}
