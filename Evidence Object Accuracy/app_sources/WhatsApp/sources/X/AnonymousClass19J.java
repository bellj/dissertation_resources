package X;

/* renamed from: X.19J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19J {
    public int A00 = 0;
    public long A01;
    public String A02;
    public boolean A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final C16120oU A06;
    public final AnonymousClass00E A07;

    public AnonymousClass19J(C14830m7 r3, C14850m9 r4, C16120oU r5) {
        AnonymousClass00E r1 = AbstractC16110oT.DEFAULT_SAMPLING_RATE;
        this.A04 = r3;
        this.A05 = r4;
        this.A06 = r5;
        this.A07 = r1;
    }

    public final void A00(int i) {
        AnonymousClass30U r6 = new AnonymousClass30U();
        boolean z = false;
        if (this.A02 != null) {
            z = true;
        }
        AnonymousClass009.A0A("RevokeUiActionWamEventLogger/logNextEvent: A session must be started before logging.", z);
        r6.A02 = this.A02;
        r6.A00 = Integer.valueOf(i);
        long A00 = this.A04.A00();
        r6.A01 = Long.valueOf(A00 - this.A01);
        this.A01 = A00;
        this.A06.A05(r6);
    }

    public final boolean A01(int i) {
        C14850m9 r1 = this.A05;
        return (r1.A07(1335) || r1.A07(1333) || r1.A07(1292)) && this.A03 && this.A00 == i;
    }
}
