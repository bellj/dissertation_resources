package X;

import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S2100000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape7S0200000_I0_7;
import com.whatsapp.jid.UserJid;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1Fl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26951Fl extends AbstractC17250qV {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C237913a A03;
    public final C20770wI A04;
    public final C238013b A05;
    public final C233711k A06;
    public final AnonymousClass10V A07;
    public final C14830m7 A08;
    public final C14820m6 A09;
    public final C22100yW A0A;
    public final C22130yZ A0B;
    public final AnonymousClass150 A0C;
    public final C14850m9 A0D;
    public final C22950zu A0E;
    public final C23000zz A0F;
    public final AnonymousClass1F2 A0G;

    public C26951Fl(AbstractC15710nm r9, C14900mE r10, C15570nT r11, C237913a r12, C20770wI r13, C238013b r14, C233711k r15, AnonymousClass10V r16, C14830m7 r17, C14820m6 r18, C22100yW r19, C22130yZ r20, AnonymousClass150 r21, C14850m9 r22, C17220qS r23, C17230qT r24, C22950zu r25, C23000zz r26, AnonymousClass1F2 r27, AbstractC14440lR r28) {
        super(r9, r23, r24, r28, new int[]{203}, false);
        this.A08 = r17;
        this.A0D = r22;
        this.A01 = r10;
        this.A03 = r12;
        this.A00 = r9;
        this.A02 = r11;
        this.A05 = r14;
        this.A0E = r25;
        this.A07 = r16;
        this.A0F = r26;
        this.A09 = r18;
        this.A0B = r20;
        this.A04 = r13;
        this.A0A = r19;
        this.A06 = r15;
        this.A0C = r21;
        this.A0G = r27;
    }

    @Override // X.AbstractC17250qV
    public void A00(AnonymousClass1V8 r10, int i) {
        C22950zu r3;
        C453921k r0;
        boolean z;
        long A09;
        AnonymousClass1V8 A0C = r10.A0C();
        String str = A0C.A00;
        if ("status".equals(str)) {
            if ("modify".equals(A0C.A0I("action", null))) {
                r3 = this.A0E;
                r0 = new C453921k();
                z = true;
                r0.A05 = true;
            } else {
                String A0I = A0C.A0I("dhash", null);
                this.A01.A0H(new RunnableBRunnable0Shape0S2100000_I0(this, A0C.A0G(), A0I, 7));
                return;
            }
        } else if ("picture".equals(str)) {
            if ("modify".equals(A0C.A0I("action", null))) {
                r3 = this.A0E;
                r0 = new C453921k();
                z = true;
                r0.A03 = true;
            } else if (A0C.A0E("delete") != null) {
                AnonymousClass10V r1 = this.A07;
                C15570nT r02 = this.A02;
                r02.A08();
                r1.A02(r02.A05);
                return;
            } else {
                return;
            }
        } else if ("devices".equals(str)) {
            boolean z2 = false;
            if (r10.A0I("offline", null) != null) {
                z2 = true;
            }
            boolean equals = "modify".equals(A0C.A0I("action", null));
            if ("critical_sync_timeout".equals(A0C.A0I("reason", null))) {
                C233711k r12 = this.A06;
                r12.A01().edit().putLong("syncd_bootstrap_fail_time", this.A08.A00()).apply();
            }
            if (equals || z2) {
                r3 = this.A0E;
                r0 = new C453921k();
                z = true;
                r0.A02 = true;
            } else if (!A0C.A0J("device").isEmpty()) {
                AnonymousClass1V8 A0E = A0C.A0E("key-index-list");
                if (A0E == null) {
                    A09 = 0;
                } else {
                    A09 = A0E.A09(A0E.A0H("ts"), "ts");
                }
                if (this.A09.A00.getLong("adv_timestamp_sec", -1) <= A09) {
                    this.A01.A0H(new RunnableBRunnable0Shape7S0200000_I0_7(this, 12, C40981sh.A01(this.A00, A0C)));
                    return;
                }
                return;
            } else {
                return;
            }
        } else if ("privacy".equals(str)) {
            if ("modify".equals(A0C.A0I("action", null))) {
                r3 = this.A0E;
                r0 = new C453921k();
                z = true;
                r0.A04 = true;
            } else if (!A0C.A0J("category").isEmpty()) {
                HashMap hashMap = new HashMap();
                for (AnonymousClass1V8 r2 : A0C.A0J("category")) {
                    hashMap.put(r2.A0H("name"), r2.A0H("value"));
                }
                this.A04.A02(hashMap);
                return;
            } else {
                return;
            }
        } else if ("blocklist".equals(str)) {
            String string = this.A09.A00.getString("block_list_v2_dhash", null);
            String A0I2 = A0C.A0I("prev_dhash", null);
            if ("modify".equals(A0C.A0I("action", null)) || !TextUtils.equals(string, A0I2)) {
                r3 = this.A0E;
                r0 = new C453921k();
                z = true;
                r0.A01 = true;
            } else if (!A0C.A0J("item").isEmpty()) {
                AbstractC15710nm r5 = this.A00;
                HashMap hashMap2 = new HashMap();
                for (AnonymousClass1V8 r32 : A0C.A0J("item")) {
                    hashMap2.put(r32.A0B(r5, UserJid.class, "jid"), Boolean.valueOf("block".equals(r32.A0H("action"))));
                }
                HashSet hashSet = new HashSet();
                HashSet hashSet2 = new HashSet();
                for (Map.Entry entry : hashMap2.entrySet()) {
                    boolean booleanValue = ((Boolean) entry.getValue()).booleanValue();
                    Object key = entry.getKey();
                    if (booleanValue) {
                        hashSet.add(key);
                    } else {
                        hashSet2.add(key);
                    }
                }
                C238013b r4 = this.A05;
                String A0I3 = A0C.A0I("dhash", null);
                Set set = r4.A0S;
                hashSet.removeAll(set);
                hashSet2.retainAll(set);
                if (!hashSet.isEmpty() || !hashSet2.isEmpty()) {
                    Iterator it = hashSet.iterator();
                    while (it.hasNext()) {
                        r4.A0E((UserJid) it.next(), true);
                    }
                    Iterator it2 = hashSet2.iterator();
                    while (it2.hasNext()) {
                        r4.A0E((UserJid) it2.next(), false);
                    }
                    synchronized (r4) {
                        set.addAll(hashSet);
                        set.removeAll(hashSet2);
                        r4.A0A.A00(r4.A03());
                        r4.A0H.A0d(A0I3);
                        HashSet hashSet3 = new HashSet(hashSet);
                        hashSet3.addAll(hashSet2);
                        r4.A06.A0H(new RunnableBRunnable0Shape1S0200000_I0_1(r4, 21, hashSet3));
                    }
                    return;
                }
                return;
            } else {
                return;
            }
        } else if ("tos".equals(str)) {
            if (this.A0D.A07(877)) {
                this.A0F.A01(AnonymousClass2SR.A00(r10));
                return;
            }
            return;
        } else if ("disappearing_mode".equals(str)) {
            int A05 = A0C.A05("duration", 0);
            long millis = TimeUnit.SECONDS.toMillis(A0C.A08("t", 0));
            AnonymousClass150 r6 = this.A0C;
            if (r6.A05.A00().getLong("disappearing_mode_timestamp", 0) < millis) {
                r6.A05(A05, millis);
                return;
            }
            return;
        } else {
            return;
        }
        r3.A00(new C454021l(r0), false, z, false);
    }
}
