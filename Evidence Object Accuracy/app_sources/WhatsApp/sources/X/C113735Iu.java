package X;

/* renamed from: X.5Iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113735Iu extends AnonymousClass5BW implements AnonymousClass1J8, AnonymousClass1WJ, AnonymousClass5ZW {
    public final int arity;
    public final int flags = 0;

    public C113735Iu(Class cls, Object obj, String str, String str2, int i) {
        super(cls, obj, str, str2, false);
        this.arity = i;
    }

    @Override // X.AnonymousClass1WJ
    public int AAk() {
        return this.arity;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof C113735Iu)) {
                return false;
            }
            C113735Iu r4 = (C113735Iu) obj;
            if (!C16700pc.A0O(A00(), r4.A00()) || !this.name.equals(r4.name) || !this.signature.equals(r4.signature) || this.flags != r4.flags || this.arity != r4.arity || !C16700pc.A0O(this.receiver, r4.receiver)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode;
        if (A00() == null) {
            hashCode = 0;
        } else {
            hashCode = A00().hashCode() * 31;
        }
        return ((hashCode + this.name.hashCode()) * 31) + this.signature.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        AnonymousClass5ZW r0 = this.A00;
        if (r0 == null) {
            r0 = this;
            this.A00 = this;
        }
        if (r0 != this) {
            return r0.toString();
        }
        String str = this.name;
        if ("<init>".equals(str)) {
            return "constructor (Kotlin reflection is not available)";
        }
        StringBuilder A0k = C12960it.A0k("function ");
        A0k.append(str);
        return C12960it.A0d(" (Kotlin reflection is not available)", A0k);
    }
}
