package X;

import android.content.Context;
import com.whatsapp.stickers.AddThirdPartyStickerPackActivity;

/* renamed from: X.3Ob  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66563Ob implements AbstractC009204q {
    public final /* synthetic */ AddThirdPartyStickerPackActivity A00;

    public C66563Ob(AddThirdPartyStickerPackActivity addThirdPartyStickerPackActivity) {
        this.A00 = addThirdPartyStickerPackActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        AddThirdPartyStickerPackActivity addThirdPartyStickerPackActivity = this.A00;
        if (!addThirdPartyStickerPackActivity.A04) {
            addThirdPartyStickerPackActivity.A04 = true;
            AnonymousClass2FL r2 = (AnonymousClass2FL) ((AnonymousClass2FJ) addThirdPartyStickerPackActivity.generatedComponent());
            AnonymousClass01J r1 = r2.A1E;
            addThirdPartyStickerPackActivity.A03 = C12960it.A0T(r1);
            addThirdPartyStickerPackActivity.A00 = C12970iu.A0b(r1);
            addThirdPartyStickerPackActivity.A02 = r2.A0K();
        }
    }
}
