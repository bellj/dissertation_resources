package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.io.Closeable;
import java.io.InputStream;
import java.nio.charset.Charset;

/* renamed from: X.0bv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08830bv implements Closeable {
    public int A00;
    public int A01;
    public byte[] A02;
    public final InputStream A03;
    public final Charset A04;
    public final /* synthetic */ C08860by A05;

    public C08830bv(C08860by r3, InputStream inputStream, Charset charset) {
        this.A05 = r3;
        if (charset == null) {
            throw null;
        } else if (charset.equals(C08860by.A0F)) {
            this.A03 = inputStream;
            this.A04 = charset;
            this.A02 = new byte[DefaultCrypto.BUFFER_SIZE];
        } else {
            throw new IllegalArgumentException("Unsupported encoding");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003b, code lost:
        if (r3[r2] == 13) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String A00() {
        /*
            r8 = this;
            java.io.InputStream r5 = r8.A03
            monitor-enter(r5)
            byte[] r2 = r8.A02     // Catch: all -> 0x009e
            if (r2 == 0) goto L_0x0096
            int r6 = r8.A01     // Catch: all -> 0x009e
            int r1 = r8.A00     // Catch: all -> 0x009e
            if (r6 < r1) goto L_0x0021
            int r0 = r2.length     // Catch: all -> 0x009e
            r6 = 0
            int r1 = r5.read(r2, r6, r0)     // Catch: all -> 0x009e
            r0 = -1
            if (r1 == r0) goto L_0x001b
            r8.A01 = r6     // Catch: all -> 0x009e
            r8.A00 = r1     // Catch: all -> 0x009e
            goto L_0x0021
        L_0x001b:
            java.io.EOFException r0 = new java.io.EOFException     // Catch: all -> 0x009e
            r0.<init>()     // Catch: all -> 0x009e
            throw r0     // Catch: all -> 0x009e
        L_0x0021:
            r4 = r6
        L_0x0022:
            r7 = 10
            if (r4 == r1) goto L_0x004f
            byte[] r3 = r8.A02     // Catch: all -> 0x009e
            byte r0 = r3[r4]     // Catch: all -> 0x009e
            if (r0 != r7) goto L_0x002d
            goto L_0x0030
        L_0x002d:
            int r4 = r4 + 1
            goto L_0x0022
        L_0x0030:
            if (r4 == r6) goto L_0x0033
            goto L_0x0035
        L_0x0033:
            r2 = r4
            goto L_0x003d
        L_0x0035:
            int r2 = r4 + -1
            byte r1 = r3[r2]     // Catch: all -> 0x009e
            r0 = 13
            if (r1 != r0) goto L_0x0033
        L_0x003d:
            int r2 = r2 - r6
            java.nio.charset.Charset r0 = r8.A04     // Catch: all -> 0x009e
            java.lang.String r0 = r0.name()     // Catch: all -> 0x009e
            java.lang.String r1 = new java.lang.String     // Catch: all -> 0x009e
            r1.<init>(r3, r6, r2, r0)     // Catch: all -> 0x009e
            int r0 = r4 + 1
            r8.A01 = r0     // Catch: all -> 0x009e
            monitor-exit(r5)     // Catch: all -> 0x009e
            return r1
        L_0x004f:
            int r1 = r1 - r6
            int r0 = r1 + 80
            X.0Ik r6 = new X.0Ik     // Catch: all -> 0x009e
            r6.<init>(r8, r0)     // Catch: all -> 0x009e
        L_0x0057:
            byte[] r2 = r8.A02     // Catch: all -> 0x009e
            int r1 = r8.A01     // Catch: all -> 0x009e
            int r0 = r8.A00     // Catch: all -> 0x009e
            int r0 = r0 - r1
            r6.write(r2, r1, r0)     // Catch: all -> 0x009e
            r2 = -1
            r8.A00 = r2     // Catch: all -> 0x009e
            byte[] r1 = r8.A02     // Catch: all -> 0x009e
            int r0 = r1.length     // Catch: all -> 0x009e
            r4 = 0
            int r3 = r5.read(r1, r4, r0)     // Catch: all -> 0x009e
            if (r3 == r2) goto L_0x0090
            r8.A01 = r4     // Catch: all -> 0x009e
            r8.A00 = r3     // Catch: all -> 0x009e
            r2 = 0
        L_0x0073:
            if (r2 == r3) goto L_0x0057
            byte[] r1 = r8.A02     // Catch: all -> 0x009e
            byte r0 = r1[r2]     // Catch: all -> 0x009e
            if (r0 != r7) goto L_0x007c
            goto L_0x007f
        L_0x007c:
            int r2 = r2 + 1
            goto L_0x0073
        L_0x007f:
            if (r2 == r4) goto L_0x0086
            int r0 = r2 - r4
            r6.write(r1, r4, r0)     // Catch: all -> 0x009e
        L_0x0086:
            int r0 = r2 + 1
            r8.A01 = r0     // Catch: all -> 0x009e
            java.lang.String r0 = r6.toString()     // Catch: all -> 0x009e
            monitor-exit(r5)     // Catch: all -> 0x009e
            return r0
        L_0x0090:
            java.io.EOFException r0 = new java.io.EOFException     // Catch: all -> 0x009e
            r0.<init>()     // Catch: all -> 0x009e
            throw r0     // Catch: all -> 0x009e
        L_0x0096:
            java.lang.String r1 = "LineReader is closed"
            java.io.IOException r0 = new java.io.IOException     // Catch: all -> 0x009e
            r0.<init>(r1)     // Catch: all -> 0x009e
            throw r0     // Catch: all -> 0x009e
        L_0x009e:
            r0 = move-exception
            monitor-exit(r5)     // Catch: all -> 0x009e
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C08830bv.A00():java.lang.String");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        InputStream inputStream = this.A03;
        synchronized (inputStream) {
            if (this.A02 != null) {
                this.A02 = null;
                inputStream.close();
            }
        }
    }
}
