package X;

import android.os.Build;
import android.os.Bundle;

/* renamed from: X.0Sb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06070Sb {
    public final Object A00;

    public AnonymousClass04Z A00(int i) {
        return null;
    }

    public AnonymousClass04Z A01(int i) {
        return null;
    }

    public boolean A02(int i, int i2, Bundle bundle) {
        return false;
    }

    public C06070Sb() {
        Object r0;
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            r0 = new C02720Dr(this);
        } else if (i >= 19) {
            r0 = new C02730Ds(this);
        } else {
            r0 = new AnonymousClass0BE(this);
        }
        this.A00 = r0;
    }

    public C06070Sb(Object obj) {
        this.A00 = obj;
    }
}
