package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.5fE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119865fE extends AbstractC18830t7 {
    public final C16590pI A00;
    public final AnonymousClass018 A01;
    public final C18810t5 A02;
    public final AbstractC17860rW A03;
    public final C18600si A04;
    public final C18800t4 A05;

    @Override // X.AbstractC18830t7
    public boolean A05() {
        return true;
    }

    @Override // X.AbstractC18830t7
    public boolean A09(String str, byte[] bArr) {
        return true;
    }

    public C119865fE(C18790t3 r8, C16590pI r9, AnonymousClass018 r10, C18810t5 r11, AbstractC17860rW r12, C18600si r13, C18800t4 r14, AbstractC14440lR r15) {
        super(r8, r9, r11, r14, r15, 14);
        this.A00 = r9;
        this.A01 = r10;
        this.A05 = r14;
        this.A02 = r11;
        this.A04 = r13;
        this.A03 = r12;
    }

    @Override // X.AbstractC18830t7
    public synchronized File A00(String str) {
        File file = new File(this.A00.A00.getFilesDir(), str);
        if (!file.exists()) {
            return null;
        }
        return file;
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ String A01(Object obj) {
        return C12980iv.A0p(this.A04.A01(), "payments_error_map_tag");
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ void A04(Object obj, String str) {
        C12970iu.A1D(C117295Zj.A05(this.A04), "payments_error_map_tag", str);
    }

    @Override // X.AbstractC18830t7
    public boolean A06(File file) {
        return file == null || file.length() == 0;
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A07(InputStream inputStream, Object obj) {
        File A00 = A00("payments_error_map.json");
        if (A00 != null) {
            C14350lI.A0M(A00);
        }
        File A002 = super.A00("");
        if (A002 == null) {
            Log.e("PAY:ErrorMapAssetManager/storeAssets/ Could not prepare resource directory");
            return false;
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(A002.getAbsolutePath(), "payments_error_map.json"));
            C14350lI.A0G(inputStream, fileOutputStream);
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            Log.e("PAY:ErrorMapAssetManager/store Failed!", e);
            return false;
        }
    }

    @Override // X.AbstractC18830t7
    public /* bridge */ /* synthetic */ boolean A08(Object obj) {
        return !A06(A00("payments_error_map.json"));
    }

    public void A0A() {
        AnonymousClass68F r3 = new AnonymousClass68F(this);
        C18600si r6 = this.A04;
        if (r6.A01.A00() - r6.A01().getLong("payments_error_map_last_sync_time_millis", (System.currentTimeMillis() - 604800000) - 1) <= 604800000) {
            String A0p = C12980iv.A0p(r6.A01(), "error_map_key");
            String ABp = this.A03.ABp();
            if (A0p != null) {
                String[] split = A0p.split("_");
                if (split[0].equals(ABp) && split[1].equals(this.A01.A06()) && split[2].equals("1")) {
                    return;
                }
            }
        }
        String ABp2 = this.A03.ABp();
        StringBuilder A0k = C12960it.A0k("https://static.whatsapp.net/payments/error_map?product_type=payments_p2p_fbpay&country=");
        A0k.append(ABp2);
        A0k.append("&lg=");
        A0k.append(this.A01.A06());
        A0k.append("&platform=android&app_type=");
        A0k.append("CONSUMER");
        A0k.append("&api_version=");
        super.A03(r3, null, null, C12960it.A0d("1", A0k));
    }
}
