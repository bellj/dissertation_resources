package X;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.IProjectionDelegate;
import com.google.android.gms.maps.internal.IUiSettingsDelegate;
import com.google.android.gms.maps.model.CameraPosition;

/* renamed from: X.1j4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35961j4 {
    public C92114Up A00;
    public final IGoogleMapDelegate A01;

    public C35961j4(IGoogleMapDelegate iGoogleMapDelegate) {
        C13020j0.A01(iGoogleMapDelegate);
        this.A01 = iGoogleMapDelegate;
    }

    public final AnonymousClass3F0 A00() {
        IProjectionDelegate iProjectionDelegate;
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A02 = r2.A02(26, r2.A01());
            IBinder readStrongBinder = A02.readStrongBinder();
            if (readStrongBinder == null) {
                iProjectionDelegate = null;
            } else {
                IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IProjectionDelegate");
                if (queryLocalInterface instanceof IProjectionDelegate) {
                    iProjectionDelegate = (IProjectionDelegate) queryLocalInterface;
                } else {
                    iProjectionDelegate = new C79823rG(readStrongBinder);
                }
            }
            A02.recycle();
            return new AnonymousClass3F0(iProjectionDelegate);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final C92114Up A01() {
        IUiSettingsDelegate iUiSettingsDelegate;
        try {
            C92114Up r0 = this.A00;
            if (r0 != null) {
                return r0;
            }
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A02 = r2.A02(25, r2.A01());
            IBinder readStrongBinder = A02.readStrongBinder();
            if (readStrongBinder == null) {
                iUiSettingsDelegate = null;
            } else {
                IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
                if (queryLocalInterface instanceof IUiSettingsDelegate) {
                    iUiSettingsDelegate = (IUiSettingsDelegate) queryLocalInterface;
                } else {
                    iUiSettingsDelegate = new C79833rH(readStrongBinder);
                }
            }
            A02.recycle();
            C92114Up r02 = new C92114Up(iUiSettingsDelegate);
            this.A00 = r02;
            return r02;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final CameraPosition A02() {
        Parcelable parcelable;
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A02 = r2.A02(1, r2.A01());
            Parcelable.Creator creator = CameraPosition.CREATOR;
            if (A02.readInt() == 0) {
                parcelable = null;
            } else {
                parcelable = (Parcelable) creator.createFromParcel(A02);
            }
            CameraPosition cameraPosition = (CameraPosition) parcelable;
            A02.recycle();
            return cameraPosition;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final C36311jg A03(C56482kx r4) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, r4);
            Parcel A02 = r2.A02(11, A01);
            AnonymousClass5YC A00 = AbstractBinderC79903rO.A00(A02.readStrongBinder());
            A02.recycle();
            if (A00 != null) {
                return new C36311jg(A00);
            }
            return null;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A04() {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            r2.A03(94, r2.A01());
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A05() {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            A01.writeFloat(16.0f);
            r2.A03(93, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A06() {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            r2.A03(14, r2.A01());
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A07(int i) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            A01.writeInt(i);
            r2.A03(16, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A08(int i, int i2, int i3, int i4) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            A01.writeInt(i);
            A01.writeInt(i2);
            A01.writeInt(i3);
            A01.writeInt(i4);
            r2.A03(39, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A09(AnonymousClass4IX r4) {
        try {
            C13020j0.A02(r4, "CameraUpdate must not be null.");
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            IObjectWrapper iObjectWrapper = r4.A00;
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(iObjectWrapper, A01);
            r2.A03(5, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0A(AnonymousClass4IX r4) {
        try {
            C13020j0.A02(r4, "CameraUpdate must not be null.");
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            IObjectWrapper iObjectWrapper = r4.A00;
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(iObjectWrapper, A01);
            r2.A03(4, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0B(AnonymousClass4IX r5, AbstractC116405Vh r6) {
        BinderC79853rJ r0;
        try {
            C13020j0.A02(r5, "CameraUpdate must not be null.");
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            IObjectWrapper iObjectWrapper = r5.A00;
            if (r6 == null) {
                r0 = null;
            } else {
                r0 = new BinderC79853rJ(r6);
            }
            C65873Li r3 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r3.A01();
            C65183In.A00(iObjectWrapper, A01);
            C65183In.A00(r0, A01);
            r3.A03(6, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0C(AnonymousClass4IX r6, AbstractC116405Vh r7) {
        BinderC79853rJ r2;
        try {
            C13020j0.A02(r6, "CameraUpdate must not be null.");
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            IObjectWrapper iObjectWrapper = r6.A00;
            if (r7 == null) {
                r2 = null;
            } else {
                r2 = new BinderC79853rJ(r7);
            }
            C65873Li r4 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r4.A01();
            C65183In.A00(iObjectWrapper, A01);
            A01.writeInt(400);
            C65183In.A00(r2, A01);
            r4.A03(7, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0D(AbstractC115685Sn r4) {
        try {
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            BinderC56682lP r0 = new BinderC56682lP(r4);
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(33, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0E(AbstractC115695So r4) {
        try {
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            BinderC79883rM r0 = new BinderC79883rM(r4);
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(99, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0F(AbstractC115705Sp r4) {
        try {
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            BinderC79873rL r0 = new BinderC79873rL(r4);
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(96, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0G(AbstractC115715Sq r4) {
        try {
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            BinderC56672lO r0 = new BinderC56672lO(r4);
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(32, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0H(AbstractC115725Sr r4) {
        try {
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            BinderC79893rN r0 = new BinderC79893rN(r4);
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(28, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0I(AbstractC115745St r4) {
        try {
            IGoogleMapDelegate iGoogleMapDelegate = this.A01;
            BinderC56652lM r0 = new BinderC56652lM(r4);
            C65873Li r2 = (C65873Li) iGoogleMapDelegate;
            Parcel A01 = r2.A01();
            C65183In.A00(r0, A01);
            r2.A03(30, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public void A0J(C56462kv r4) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            C65183In.A01(A01, r4);
            Parcel A02 = r2.A02(91, A01);
            A02.readInt();
            A02.recycle();
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0K(boolean z) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            A01.writeInt(z ? 1 : 0);
            Parcel A02 = r2.A02(20, A01);
            A02.readInt();
            A02.recycle();
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0L(boolean z) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            A01.writeInt(z ? 1 : 0);
            r2.A03(22, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final void A0M(boolean z) {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A01 = r2.A01();
            A01.writeInt(z ? 1 : 0);
            r2.A03(18, A01);
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }

    public final boolean A0N() {
        try {
            C65873Li r2 = (C65873Li) this.A01;
            Parcel A02 = r2.A02(17, r2.A01());
            boolean z = false;
            if (A02.readInt() != 0) {
                z = true;
            }
            A02.recycle();
            return z;
        } catch (RemoteException e) {
            throw new C113245Gt(e);
        }
    }
}
