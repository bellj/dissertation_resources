package X;

import android.graphics.drawable.Drawable;
import android.view.ViewTreeObserver;

/* renamed from: X.4oC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101854oC implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass1OY A00;

    public ViewTreeObserver$OnPreDrawListenerC101854oC(AnonymousClass1OY r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        AnonymousClass1OY r3 = this.A00;
        C12980iv.A1G(r3.A0A, this);
        Drawable popupDrawable = r3.getPopupDrawable();
        AbstractC13890kV r1 = ((AbstractC28551Oa) r3).A0a;
        if (r1 == null || popupDrawable == null) {
            return true;
        }
        r1.A5u(popupDrawable, r3.A0A);
        return true;
    }
}
