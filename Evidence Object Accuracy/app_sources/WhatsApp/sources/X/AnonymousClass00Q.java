package X;

import android.content.Context;
import android.os.UserManager;

/* renamed from: X.00Q  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass00Q {
    public static boolean A00(Context context) {
        return ((UserManager) context.getSystemService(UserManager.class)).isUserUnlocked();
    }
}
