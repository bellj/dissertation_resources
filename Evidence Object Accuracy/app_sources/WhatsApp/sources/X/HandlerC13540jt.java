package X;

import android.os.Handler;
import android.os.Looper;

/* renamed from: X.0jt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class HandlerC13540jt extends Handler {
    public HandlerC13540jt() {
    }

    public HandlerC13540jt(Looper looper) {
        super(looper);
    }

    public HandlerC13540jt(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
