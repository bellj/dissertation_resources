package X;

import com.whatsapp.data.ProfilePhotoChange;

/* renamed from: X.1Xq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30501Xq extends AnonymousClass1XB {
    public ProfilePhotoChange A00;
    public String A01;

    public C30501Xq(AnonymousClass1IS r2, long j) {
        super(r2, 6, j);
    }

    @Override // X.AbstractC15340mz
    public void A0l(String str) {
        A16(str);
    }

    public String A15() {
        String str;
        synchronized (this.A10) {
            str = this.A01;
        }
        return str;
    }

    public void A16(String str) {
        synchronized (this.A10) {
            this.A01 = str;
        }
    }
}
