package X;

import android.os.Handler;
import android.os.Message;
import java.util.List;

/* renamed from: X.63S  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass63S implements Handler.Callback {
    public final /* synthetic */ AnonymousClass63B A00;

    public AnonymousClass63S(AnonymousClass63B r1) {
        this.A00 = r1;
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        if (message.what == 1) {
            AnonymousClass63B r5 = this.A00;
            if (r5.A0B) {
                List list = r5.A0A;
                int i = message.arg1;
                if (list != null && i < list.size()) {
                    list.get(i);
                    list.get(0);
                    list.get(list.size() - 1);
                    List list2 = r5.A05.A00;
                    if (0 < list2.size()) {
                        list2.get(0);
                        throw C12980iv.A0n("onZoomChange");
                    }
                }
                return true;
            }
        }
        if (message.what != 2) {
            return false;
        }
        List list3 = this.A00.A05.A00;
        if (0 < list3.size()) {
            list3.get(0);
            throw C12980iv.A0n("onZoomError");
        }
        return true;
    }
}
