package X;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import com.whatsapp.wabloks.ui.WaBloksActivity;

/* renamed from: X.5pV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C124435pV extends AbstractC119405dv {
    public AbstractC28681Oo A00;

    public C124435pV(AnonymousClass018 r2, WaBloksActivity waBloksActivity) {
        super(r2, waBloksActivity);
        C41691tw.A02(waBloksActivity, R.color.primary);
    }

    private void A00() {
        WaBloksActivity waBloksActivity = this.A04;
        Toolbar toolbar = (Toolbar) AnonymousClass00T.A05(waBloksActivity, R.id.wabloks_screen_toolbar);
        AnonymousClass2GF A00 = AnonymousClass2GF.A00(waBloksActivity, this.A03, R.drawable.ic_back);
        C117305Zk.A13(waBloksActivity.getResources(), A00, R.color.white);
        toolbar.setNavigationIcon(A00);
        toolbar.setBackgroundColor(waBloksActivity.getResources().getColor(R.color.primary));
        toolbar.setTitleTextColor(waBloksActivity.getResources().getColor(R.color.screen_title_text));
    }

    @Override // X.AbstractC119405dv
    public void A03(Intent intent, Bundle bundle) {
        if (intent != null) {
            this.A01 = intent.getStringExtra("bk_phoenix_navbar_title");
            intent.getStringExtra("bk_phoenix_navbar_leading_button_icon");
        }
        A00();
        A00();
    }

    @Override // X.AbstractC119405dv
    public void A04(AbstractC115815Ta r3) {
        try {
            this.A01 = r3.AAL().A0I(36);
            AnonymousClass3BN r1 = new AnonymousClass3BN(r3.AAL().A0F(40));
            if (AnonymousClass1US.A0C(this.A01)) {
                this.A01 = r1.A02;
            }
            if (r1.A00 != null) {
                this.A00 = new AbstractC28681Oo() { // from class: X.67I
                    @Override // X.AbstractC28681Oo
                    public final AbstractC14200l1 AAN() {
                        return AnonymousClass3BN.this.A00;
                    }
                };
            }
            A00();
        } catch (ClassCastException e) {
            Log.e(C12960it.A0b("Bloks: Invalid navigation bar type", e));
        }
        A00();
    }
}
