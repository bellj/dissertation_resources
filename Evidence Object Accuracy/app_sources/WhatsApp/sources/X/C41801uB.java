package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.1uB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41801uB extends AbstractC16110oT {
    public Boolean A00;

    public C41801uB() {
        super(2098, new AnonymousClass00E(1, 10, SearchActionVerificationClientService.NOTIFICATION_ID), 1, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("WamUiActionRealTime {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "chatdInternetConnectivity", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
