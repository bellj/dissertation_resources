package X;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.contact.picker.ContactPickerFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3Ol  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66663Ol implements AnonymousClass02Q {
    public final /* synthetic */ ContactPickerFragment A00;

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r3) {
        return false;
    }

    public C66663Ol(ContactPickerFragment contactPickerFragment) {
        this.A00 = contactPickerFragment;
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r12) {
        ContactPickerFragment contactPickerFragment;
        int i;
        AnonymousClass3UA r9;
        String A0I;
        if (menuItem.getItemId() == R.id.menuitem_new_broadcast) {
            contactPickerFragment = this.A00;
            int A02 = contactPickerFragment.A0O.A02(AbstractC15460nI.A1I);
            if (A02 <= 0 || contactPickerFragment.A2a.size() <= A02) {
                Context A0p = contactPickerFragment.A0p();
                Set keySet = contactPickerFragment.A2a.keySet();
                Intent A0A = C12970iu.A0A();
                A0A.setClassName(A0p.getPackageName(), "com.whatsapp.contact.picker.ListMembersSelector");
                if (!keySet.isEmpty()) {
                    A0A.putExtra("selected", C15380n4.A06(keySet));
                }
                contactPickerFragment.A0v(A0A);
                contactPickerFragment.A0m.A00();
            } else {
                r9 = contactPickerFragment.A0m;
                Object[] objArr = new Object[1];
                C12960it.A1P(objArr, A02, 0);
                A0I = contactPickerFragment.A16.A0I(objArr, R.plurals.broadcast_reach_limit, (long) A02);
                r9.Adp(A0I);
                contactPickerFragment.A0m.A00();
            }
        } else if (menuItem.getItemId() == R.id.menuitem_new_group) {
            contactPickerFragment = this.A00;
            int A022 = contactPickerFragment.A1F.A0B.A02(1304) - 1;
            if (A022 <= 0 || contactPickerFragment.A2a.size() <= (i = A022 - 1)) {
                contactPickerFragment.A1P.A03(4);
                ActivityC000900k A0C = contactPickerFragment.A0C();
                A0C.startActivity(C14960mK.A0b(A0C, C15380n4.A06(contactPickerFragment.A2a.keySet()), 4));
                contactPickerFragment.A0m.A00();
            } else {
                r9 = contactPickerFragment.A0m;
                Object[] objArr2 = new Object[1];
                C12960it.A1P(objArr2, i, 0);
                A0I = contactPickerFragment.A16.A0I(objArr2, R.plurals.groupchat_reach_limit, (long) i);
                r9.Adp(A0I);
                contactPickerFragment.A0m.A00();
            }
        } else if (menuItem.getItemId() == R.id.menuitem_share) {
            ContactPickerFragment contactPickerFragment2 = this.A00;
            boolean z = contactPickerFragment2.A1B().getBoolean("skip_preview", false);
            ArrayList arrayList = contactPickerFragment2.A20;
            if (arrayList != null) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    if (contactPickerFragment2.A1i.A04((Uri) it.next()) != 1) {
                        break;
                    }
                }
            }
            if (!z) {
                contactPickerFragment2.A1a(null);
                return false;
            }
            C36021jC.A01(contactPickerFragment2.A0B(), 1);
            return false;
        }
        return false;
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r6) {
        ContactPickerFragment contactPickerFragment = this.A00;
        if (contactPickerFragment.A2N || contactPickerFragment.A2V || contactPickerFragment.A2T) {
            menu.add(0, R.id.menuitem_share, 0, R.string.send).setIcon(R.drawable.input_send).setShowAsAction(2);
            return true;
        }
        contactPickerFragment.A0L.A08();
        menu.add(0, R.id.menuitem_new_broadcast, 0, R.string.new_broadcast).setShowAsAction(1);
        menu.add(0, R.id.menuitem_new_group, 0, R.string.menuitem_groupchat).setShowAsAction(1);
        return true;
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r7) {
        ContactPickerFragment contactPickerFragment = this.A00;
        Set set = contactPickerFragment.A2c;
        set.clear();
        Map map = contactPickerFragment.A2a;
        set.addAll(map.keySet());
        Handler handler = contactPickerFragment.A2W;
        Runnable runnable = contactPickerFragment.A2X;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 200);
        map.clear();
        contactPickerFragment.A0l.notifyDataSetChanged();
        contactPickerFragment.A0G = null;
    }
}
