package X;

import java.util.Comparator;
import java.util.List;

/* renamed from: X.0Th  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C06360Th {
    public static final Comparator A05 = new C10310eP();
    public final List A00;
    public final float[] A01 = new float[3];
    public final int[] A02;
    public final int[] A03;
    public final AbstractC11320g6[] A04;

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0156, code lost:
        if (r2 < r1) goto L_0x0158;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C06360Th(int[] r14, X.AbstractC11320g6[] r15) {
        /*
        // Method dump skipped, instructions count: 478
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C06360Th.<init>(int[], X.0g6[]):void");
    }

    public static void A00(int[] iArr, int i, int i2, int i3) {
        if (i == -2) {
            while (i2 <= i3) {
                int i4 = iArr[i2];
                iArr[i2] = (i4 & 31) | (((i4 >> 5) & 31) << 10) | (((i4 >> 10) & 31) << 5);
                i2++;
            }
        } else if (i == -1) {
            while (i2 <= i3) {
                int i5 = iArr[i2];
                iArr[i2] = ((i5 >> 10) & 31) | ((i5 & 31) << 10) | (((i5 >> 5) & 31) << 5);
                i2++;
            }
        }
    }

    public final boolean A01(float[] fArr) {
        int length;
        AbstractC11320g6[] r0 = this.A04;
        if (r0 == null || (length = r0.length) <= 0) {
            return false;
        }
        int i = 0;
        do {
            float f = fArr[2];
            if (f >= 0.95f || f <= 0.05f) {
                return true;
            }
            float f2 = fArr[0];
            if (f2 >= 10.0f && f2 <= 37.0f && fArr[1] <= 0.82f) {
                return true;
            }
            i++;
        } while (i < length);
        return false;
    }
}
