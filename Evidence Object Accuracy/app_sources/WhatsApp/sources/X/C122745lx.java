package X;

import android.view.View;
import android.widget.Button;
import com.facebook.redex.ViewOnClickCListenerShape0S0300000_I0;
import com.whatsapp.R;

/* renamed from: X.5lx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122745lx extends AbstractC118835cS {
    public final View A00;
    public final Button A01;
    public final Button A02;
    public final Button A03;
    public final AnonymousClass18P A04;

    public C122745lx(View view, AnonymousClass18P r3) {
        super(view);
        this.A04 = r3;
        this.A02 = (Button) AnonymousClass028.A0D(view, R.id.request_cancel_button);
        this.A03 = (Button) AnonymousClass028.A0D(view, R.id.retry_withdrawal_button);
        this.A01 = (Button) AnonymousClass028.A0D(view, R.id.accept_payment_button);
        this.A00 = AnonymousClass028.A0D(view, R.id.button_group_view);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r15, int i) {
        AbstractC16830pp AFX;
        C123205mm r3 = (C123205mm) r15;
        C127915vG r1 = r3.A02;
        if (r1 != null) {
            AnonymousClass18P r5 = this.A04;
            View view = this.A00;
            AnonymousClass1In r10 = r3.A01;
            AnonymousClass1IR r8 = r1.A01;
            AbstractC15340mz r11 = r1.A02;
            Button button = this.A02;
            Button button2 = this.A03;
            Button button3 = this.A01;
            AbstractC38231nk r9 = r3.A00;
            view.setVisibility(8);
            if (r8.A02 == 110) {
                View A0D = AnonymousClass028.A0D(view, R.id.request_decline_button);
                View A0D2 = AnonymousClass028.A0D(view, R.id.request_pay_button);
                A0D.setVisibility(8);
                A0D2.setVisibility(8);
                view.setVisibility(0);
                button2.setVisibility(0);
                AbstractC38041nQ A01 = r5.A0B.A01(r8.A0G);
                if (A01 != null && (AFX = A01.AFX(r8.A0I)) != null) {
                    button2.setOnClickListener(new ViewOnClickCListenerShape0S0300000_I0(AFX, view.getContext(), r11, 12));
                }
            } else if (r8.A0C()) {
                r5.A04(view, button, r8, r10, true);
            } else if (r8.A02 == 102) {
                r5.A02(view, button3, r8);
            } else {
                r5.A03(view, button, r8, r9, r10, r11, "payment_transaction_details", true);
            }
        }
    }
}
