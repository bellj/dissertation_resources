package X;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.2vV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C59862vV extends AbstractC37191le {
    public final View A00;
    public final View A01;
    public final TextEmojiLabel A02;

    public C59862vV(View view) {
        super(view);
        this.A00 = view;
        TextEmojiLabel textEmojiLabel = (TextEmojiLabel) C16700pc.A02(view, R.id.allow_location_btn);
        this.A02 = textEmojiLabel;
        this.A01 = C16700pc.A02(view, R.id.more_options_btn);
        Context context = view.getContext();
        SpannableStringBuilder A0J = C12990iw.A0J(C16700pc.A08("# ", context.getString(R.string.biz_dir_allow_location_access)));
        C52252aV.A02(textEmojiLabel.getPaint(), AnonymousClass2GE.A03(context, C12970iu.A0C(context, R.drawable.ic_location_nearby), R.color.white), A0J, -1, 0, 1);
        textEmojiLabel.setText(A0J);
    }
}
