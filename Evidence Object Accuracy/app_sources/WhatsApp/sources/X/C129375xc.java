package X;

import android.media.CamcorderProfile;
import android.os.SystemClock;
import com.whatsapp.voipcalling.GlVideoRenderer;
import java.io.FileDescriptor;
import org.chromium.net.UrlRequest;

/* renamed from: X.5xc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129375xc {
    public int A00 = 0;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05 = 0;
    public long A06 = -1;
    public long A07 = -1;
    public long A08 = -1;
    public long A09 = -1;
    public AnonymousClass60Q A0A;
    public FileDescriptor A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public String A0F;
    public boolean A0G = true;

    public C129375xc(String str, int i, int i2, int i3, int i4) {
        if (str == null) {
            throw C12970iu.A0f("Both file path or file descriptor must be not be null, one must be set.");
        } else if (i == 0) {
            throw C12970iu.A0f("Frame width must be greater 0");
        } else if (i2 != 0) {
            this.A0F = str;
            this.A0B = null;
            this.A03 = i;
            this.A02 = i2;
            this.A04 = i3;
            this.A01 = i4;
        } else {
            throw C12970iu.A0f("Frame height must be greater 0");
        }
    }

    public static AnonymousClass60Q A00(CamcorderProfile camcorderProfile, String str, int i, int i2) {
        C129375xc r3 = new C129375xc(str, camcorderProfile.videoFrameWidth, camcorderProfile.videoFrameHeight, i, i2);
        r3.A01(AnonymousClass60Q.A0L, Integer.valueOf(camcorderProfile.audioCodec));
        r3.A01(AnonymousClass60Q.A0R, Integer.valueOf(camcorderProfile.videoCodec));
        r3.A01(AnonymousClass60Q.A0P, Long.valueOf(SystemClock.elapsedRealtime()));
        return new AnonymousClass60Q(r3);
    }

    public void A01(C125535rP r3, Object obj) {
        int i = r3.A00;
        switch (i) {
            case 1:
                this.A0B = (FileDescriptor) obj;
                return;
            case 2:
                int A05 = C12960it.A05(obj);
                if (A05 != 0) {
                    this.A03 = A05;
                    return;
                }
                throw C12970iu.A0f("Frame width must be greater 0");
            case 3:
                int A052 = C12960it.A05(obj);
                if (A052 != 0) {
                    this.A02 = A052;
                    return;
                }
                throw C12970iu.A0f("Frame height must be greater 0");
            case 4:
                this.A04 = C12960it.A05(obj);
                return;
            case 5:
                if (!obj.equals(C12960it.A0V()) || !obj.equals(C12980iv.A0i())) {
                    throw C12970iu.A0f("Camera facing must be either 0 (BACK) or 1 (FRONT)");
                }
                this.A01 = C12960it.A05(obj);
                return;
            case 6:
                this.A0G = C12970iu.A1Y(obj);
                return;
            case 7:
                this.A00 = C12960it.A05(obj);
                return;
            case 8:
                this.A05 = C12960it.A05(obj);
                return;
            case 9:
                this.A0C = (Integer) obj;
                return;
            case 10:
                this.A0D = (Integer) obj;
                return;
            case 11:
                this.A0E = (Integer) obj;
                return;
            case 12:
                this.A06 = C12980iv.A0G(obj);
                return;
            case UrlRequest.Status.WAITING_FOR_RESPONSE /* 13 */:
                this.A07 = C12980iv.A0G(obj);
                return;
            case UrlRequest.Status.READING_RESPONSE /* 14 */:
                this.A08 = C12980iv.A0G(obj);
                return;
            case 15:
                this.A09 = C12980iv.A0G(obj);
                return;
            case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                this.A0A = (AnonymousClass60Q) obj;
                return;
            default:
                throw C12990iw.A0m(C12960it.A0W(i, "Failed to set video capture value: "));
        }
    }
}
