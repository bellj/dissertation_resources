package X;

import android.os.Bundle;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;

/* renamed from: X.0Dt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02740Dt extends C06070Sb {
    public final /* synthetic */ AnonymousClass0DW A00;

    public C02740Dt(AnonymousClass0DW r1) {
        this.A00 = r1;
    }

    @Override // X.C06070Sb
    public AnonymousClass04Z A00(int i) {
        return new AnonymousClass04Z(AccessibilityNodeInfo.obtain(this.A00.A08(i).A02));
    }

    @Override // X.C06070Sb
    public AnonymousClass04Z A01(int i) {
        AnonymousClass0DW r0 = this.A00;
        int i2 = i == 2 ? r0.A00 : r0.A01;
        if (i2 == Integer.MIN_VALUE) {
            return null;
        }
        return A00(i2);
    }

    @Override // X.C06070Sb
    public boolean A02(int i, int i2, Bundle bundle) {
        int i3;
        int i4;
        AnonymousClass0DW r2 = this.A00;
        if (i == -1) {
            return r2.A06.performAccessibilityAction(i2, bundle);
        }
        if (i2 == 1) {
            return r2.A0F(i);
        }
        if (i2 != 2) {
            if (i2 == 64) {
                AccessibilityManager accessibilityManager = r2.A07;
                if (!accessibilityManager.isEnabled() || !accessibilityManager.isTouchExplorationEnabled() || (i4 = r2.A00) == i) {
                    return false;
                }
                if (i4 != Integer.MIN_VALUE && i4 == i4) {
                    r2.A00 = Integer.MIN_VALUE;
                    r2.A06.invalidate();
                    r2.A0A(i4, 65536);
                }
                r2.A00 = i;
                r2.A06.invalidate();
                i3 = 32768;
            } else if (i2 != 128) {
                return r2.A0G(i, i2, bundle);
            } else {
                if (r2.A00 != i) {
                    return false;
                }
                r2.A00 = Integer.MIN_VALUE;
                r2.A06.invalidate();
                i3 = 65536;
            }
        } else if (r2.A01 != i) {
            return false;
        } else {
            r2.A01 = Integer.MIN_VALUE;
            i3 = 8;
        }
        r2.A0A(i, i3);
        return true;
    }
}
