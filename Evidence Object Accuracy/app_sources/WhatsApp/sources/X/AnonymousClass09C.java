package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: X.09C  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09C extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass0BZ A00;

    public AnonymousClass09C(AnonymousClass0BZ r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        AbstractC12100hN r0 = this.A00.A05;
        if (r0 != null) {
            r0.API();
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AbstractC12100hN r0 = this.A00.A05;
        if (r0 != null) {
            r0.API();
        }
    }
}
