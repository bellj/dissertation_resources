package X;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape6S0200000_I0_6;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3Ye  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69163Ye implements AnonymousClass5W7 {
    public final /* synthetic */ Context A00;
    public final /* synthetic */ AnonymousClass1IR A01;
    public final /* synthetic */ AnonymousClass1In A02;
    public final /* synthetic */ AnonymousClass18P A03;
    public final /* synthetic */ AbstractC15340mz A04;
    public final /* synthetic */ String A05;
    public final /* synthetic */ boolean A06;

    public C69163Ye(Context context, AnonymousClass1IR r2, AnonymousClass1In r3, AnonymousClass18P r4, AbstractC15340mz r5, String str, boolean z) {
        this.A03 = r4;
        this.A00 = context;
        this.A05 = str;
        this.A01 = r2;
        this.A04 = r5;
        this.A02 = r3;
        this.A06 = z;
    }

    @Override // X.AnonymousClass5W7
    public void AQ8() {
        this.A03.A00(this.A00, this.A01);
    }

    @Override // X.AnonymousClass5W7
    public void AWu() {
        Intent A0D;
        AbstractC30791Yv A00;
        String A03;
        UserJid of;
        AbstractC16830pp AFX;
        AnonymousClass18P r4 = this.A03;
        Context context = this.A00;
        String str = this.A05;
        AnonymousClass1IR r8 = this.A01;
        AbstractC15340mz r5 = this.A04;
        AnonymousClass1In r1 = this.A02;
        if (this.A06) {
            if (r5 == null) {
                of = null;
            } else {
                of = UserJid.of(r5.A0B());
            }
            RunnableBRunnable0Shape6S0200000_I0_6 runnableBRunnable0Shape6S0200000_I0_6 = new RunnableBRunnable0Shape6S0200000_I0_6(r4, 34, r1);
            if (r8.A0C != null) {
                C14900mE r12 = r4.A00;
                if (r12.A0M()) {
                    r12.A06(0, R.string.register_wait_message);
                    C12990iw.A1N(new AnonymousClass38C(r4.A01, r4.A03, r4.A05, r8, of, r4.A0A, r4.A0B, r4.A0C, runnableBRunnable0Shape6S0200000_I0_6, 15), r4.A0D);
                    return;
                }
                return;
            }
            AbstractC38041nQ A01 = r4.A0B.A01(r8.A0G);
            if (A01 != null && (AFX = A01.AFX(r8.A0I)) != null) {
                r4.A00.A06(0, R.string.register_wait_message);
                AbstractC450620a AF8 = AFX.AF8();
                AnonymousClass009.A05(AF8);
                AF8.Aa3(AnonymousClass12P.A00(context), r8, new AnonymousClass5UT(r8, r4, runnableBRunnable0Shape6S0200000_I0_6) { // from class: X.3Yd
                    public final /* synthetic */ AnonymousClass1IR A00;
                    public final /* synthetic */ AnonymousClass18P A01;
                    public final /* synthetic */ Runnable A02;

                    {
                        this.A01 = r2;
                        this.A00 = r1;
                        this.A02 = r3;
                    }

                    @Override // X.AnonymousClass5UT
                    public final void AUq(C452120p r13) {
                        AnonymousClass18P r42 = this.A01;
                        AnonymousClass1IR r0 = this.A00;
                        Runnable runnable = this.A02;
                        if (r13 == null) {
                            AbstractC14440lR r14 = r42.A0D;
                            String str2 = r0.A0K;
                            int i = r0.A03;
                            C14830m7 r02 = r42.A01;
                            C12990iw.A1N(new C864647k(r42.A0B, runnable, str2, i, r02.A00(), r02.A00()), r14);
                            return;
                        }
                        int i2 = 0;
                        if (r13.A00 == 443) {
                            i2 = R.string.payments_upgrade_error;
                        }
                        C14900mE r03 = r42.A00;
                        if (i2 == 0) {
                            i2 = R.string.request_cannot_be_rejected;
                        }
                        r03.A07(i2, 0);
                        r03.A03();
                    }
                });
                return;
            }
            return;
        }
        AnonymousClass18T r52 = r4.A08;
        if (!(!r4.A07.A0B()) || r52.A04.A09()) {
            A0D = C12990iw.A0D(context, r52.A06.A02().AGb());
        } else {
            A0D = C12990iw.A0D(context, r52.A06.A02().AAW());
            A0D.putExtra("extra_setup_mode", 1);
        }
        A0D.putExtra("extra_referral_screen", str);
        if (r8.A0C != null) {
            A0D.putExtra("extra_request_message_key", r8.A0L);
            A0D.putExtra("extra_conversation_message_type", 3);
            String str2 = r8.A0K;
            if (str2 != null) {
                A0D.putExtra("extra_request_id", str2);
            }
            AbstractC14640lm r2 = r8.A0C;
            String str3 = "extra_jid";
            if (C15380n4.A0J(r2)) {
                A0D.putExtra(str3, r2.getRawString());
                A03 = C15380n4.A03(r8.A0D);
                str3 = "extra_receiver_jid";
            } else {
                A03 = C15380n4.A03(r8.A0D);
            }
            A0D.putExtra(str3, A03);
        }
        if (!TextUtils.isEmpty(r8.A0K)) {
            A0D.putExtra("extra_transaction_id", r8.A0K);
        }
        AbstractC30891Zf r0 = r8.A0A;
        if (r0 != null) {
            A0D.putExtra("extra_payment_handle", new AnonymousClass1ZR(new AnonymousClass2SM(), String.class, r0.A0F(), "paymentHandle"));
            A0D.putExtra("extra_incoming_pay_request_id", r8.A0A.A0E());
        }
        C30821Yy r02 = r8.A08;
        if (!(r02 == null || TextUtils.isEmpty(r02.toString()) || (A00 = r52.A05.A00()) == null)) {
            A0D.putExtra("extra_payment_preset_amount", A00.AA8(r52.A03, r8.A08));
        }
        ((ActivityC13810kN) AnonymousClass12P.A00(context)).A2G(A0D, false);
    }
}
