package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3ru  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80223ru extends AnonymousClass5I0<Long> implements AnonymousClass5Z6<Long>, AbstractC115265Qv, RandomAccess {
    public static final C80223ru A02;
    public int A00;
    public long[] A01;

    static {
        C80223ru r0 = new C80223ru(new long[0], 0);
        A02 = r0;
        ((AnonymousClass5I0) r0).A00 = false;
    }

    public C80223ru() {
        this(new long[10], 0);
    }

    public C80223ru(long[] jArr, int i) {
        this.A01 = jArr;
        this.A00 = i;
    }

    public final void A03(int i) {
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
    }

    public final void A04(long j) {
        A02();
        int i = this.A00;
        long[] jArr = this.A01;
        if (i == jArr.length) {
            long[] jArr2 = new long[((i * 3) >> 1) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.A01 = jArr2;
            jArr = jArr2;
        }
        int i2 = this.A00;
        this.A00 = i2 + 1;
        jArr[i2] = j;
    }

    @Override // X.AnonymousClass5Z6
    public final /* synthetic */ AnonymousClass5Z6 Agl(int i) {
        if (i >= this.A00) {
            return new C80223ru(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        long A0G = C12980iv.A0G(obj);
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        long[] jArr = this.A01;
        if (i2 < jArr.length) {
            C72463ee.A0T(jArr, i, i2);
        } else {
            long[] jArr2 = new long[((i2 * 3) >> 1) + 1];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.A01, i, jArr2, i + 1, this.A00 - i);
            this.A01 = jArr2;
        }
        this.A01[i] = A0G;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* synthetic */ boolean add(Object obj) {
        A04(C12980iv.A0G(obj));
        return true;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C80223ru)) {
            return super.addAll(collection);
        }
        C80223ru r7 = (C80223ru) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.A01;
            if (i3 > jArr.length) {
                jArr = Arrays.copyOf(jArr, i3);
                this.A01 = jArr;
            }
            System.arraycopy(r7.A01, 0, jArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean contains(Object obj) {
        return C12980iv.A1V(indexOf(obj), -1);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C80223ru)) {
                return super.equals(obj);
            }
            C80223ru r11 = (C80223ru) obj;
            int i = this.A00;
            if (i == r11.A00) {
                long[] jArr = r11.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == jArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        A03(i);
        return Long.valueOf(this.A01[i]);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + C72453ed.A0B(this.A01[i2]);
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Long) {
            long A0G = C12980iv.A0G(obj);
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.A01[i] == A0G) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        A02();
        A03(i);
        long[] jArr = this.A01;
        long j = jArr[i];
        AnonymousClass5I0.A01(jArr, this.A00, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            long[] jArr = this.A01;
            System.arraycopy(jArr, i2, jArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        long A0G = C12980iv.A0G(obj);
        A02();
        A03(i);
        long[] jArr = this.A01;
        long j = jArr[i];
        jArr[i] = A0G;
        return Long.valueOf(j);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }
}
