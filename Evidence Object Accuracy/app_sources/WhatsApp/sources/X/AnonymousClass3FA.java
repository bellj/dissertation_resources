package X;

import java.util.Map;

/* renamed from: X.3FA  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FA {
    public final long A00;
    public final String A01;
    public final Map A02;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AnonymousClass3FA) {
                AnonymousClass3FA r8 = (AnonymousClass3FA) obj;
                if (this.A00 != r8.A00 || !C16700pc.A0O(this.A01, r8.A01) || !C16700pc.A0O(this.A02, r8.A02)) {
                }
            }
            return false;
        }
        return true;
    }

    public AnonymousClass3FA(String str, Map map, long j) {
        this.A00 = j;
        this.A01 = str;
        this.A02 = map;
    }

    public int hashCode() {
        long j = this.A00;
        int hashCode = ((((int) (j ^ (j >>> 32))) * 31) + this.A01.hashCode()) * 31;
        Map map = this.A02;
        return hashCode + (map == null ? 0 : map.hashCode());
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("FdsError(errorCode=");
        A0k.append(this.A00);
        A0k.append(", errorMessage=");
        A0k.append(this.A01);
        A0k.append(", params=");
        return C12960it.A0a(this.A02, A0k);
    }
}
