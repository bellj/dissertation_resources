package X;

import android.content.Context;

/* renamed from: X.5ft  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120265ft extends AbstractC451020e {
    public final /* synthetic */ C120425gA A00;
    public final /* synthetic */ AbstractC16870pt A01;
    public final /* synthetic */ C128805wh A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120265ft(Context context, C14900mE r2, C18650sn r3, C120425gA r4, AbstractC16870pt r5, C128805wh r6) {
        super(context, r2, r3);
        this.A00 = r4;
        this.A01 = r5;
        this.A02 = r6;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r1) {
        A05(r1);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r1) {
        A05(r1);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r5) {
        AnonymousClass1V8 A0c = C117305Zk.A0c(r5);
        if (A0c == null) {
            A05(C117305Zk.A0L());
        }
        AnonymousClass1V8 A0E = A0c.A0E("transaction");
        if (A0E == null) {
            A05(C117305Zk.A0L());
        }
        this.A00.A06.Ab2(new Runnable(this.A02, A0E) { // from class: X.6J9
            public final /* synthetic */ C128805wh A01;
            public final /* synthetic */ AnonymousClass1V8 A02;

            {
                this.A02 = r3;
                this.A01 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                C120265ft r0 = C120265ft.this;
                this.A01.A00(r0.A00.A05.A05(null, this.A02), null);
            }
        });
    }

    public final void A05(C452120p r3) {
        AbstractC16870pt r1 = this.A01;
        if (r1 != null) {
            r1.AKa(r3, 18);
        }
        this.A02.A00(null, r3);
    }
}
