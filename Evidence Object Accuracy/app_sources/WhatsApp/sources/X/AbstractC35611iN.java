package X;

import android.graphics.Bitmap;
import android.net.Uri;

/* renamed from: X.1iN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC35611iN {
    Uri AAE();

    long ACQ();

    long ACb();

    String AES();

    Bitmap Aem(int i);

    long getContentLength();

    int getType();
}
