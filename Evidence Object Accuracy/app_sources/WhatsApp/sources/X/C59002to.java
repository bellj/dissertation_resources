package X;

import android.text.TextUtils;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.2to  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59002to extends AnonymousClass2EI {
    public final AnonymousClass19T A00;
    public final AnonymousClass281 A01;
    public final C19870uo A02;
    public final C17220qS A03;
    public final C19840ul A04;

    public C59002to(C14650lo r1, AnonymousClass19T r2, AnonymousClass281 r3, C19870uo r4, C17220qS r5, C19840ul r6) {
        super(r1);
        this.A04 = r6;
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r4;
    }

    public boolean A02() {
        boolean z;
        String A01 = this.A03.A01();
        C14650lo r0 = super.A01;
        AnonymousClass281 r7 = this.A01;
        UserJid userJid = r7.A00;
        String A012 = r0.A07.A01(userJid);
        this.A04.A03("report_product_tag");
        C19870uo r2 = this.A02;
        ArrayList A0l = C12960it.A0l();
        String str = r7.A01;
        AnonymousClass2EI.A00("id", str, A0l);
        String str2 = r7.A02;
        if (!TextUtils.isEmpty(str2)) {
            AnonymousClass2EI.A00("reason", str2, A0l);
        }
        AnonymousClass2EI.A00("catalog_session_id", r7.A03, A0l);
        if (A012 != null) {
            AnonymousClass2EI.A00("direct_connection_encrypted_info", A012, A0l);
        }
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[2];
        int A1a = C12990iw.A1a("type", "report_product", r4);
        r4[1] = new AnonymousClass1W9(userJid, "biz_jid");
        AnonymousClass1V8 r5 = new AnonymousClass1V8("request", r4, (AnonymousClass1V8[]) A0l.toArray(new AnonymousClass1V8[A1a]));
        AnonymousClass1W9[] r42 = new AnonymousClass1W9[4];
        C12960it.A1M("id", A01, r42, A1a);
        C12960it.A1M("xmlns", "fb:thrift_iq", r42, 1);
        C12960it.A1M("type", "set", r42, 2);
        AnonymousClass1V8 A00 = AnonymousClass1V8.A00(AnonymousClass1VY.A00, r5, r42, 3);
        try {
            if (r2.A00.A07(1319)) {
                z = C19870uo.A00(this, A01);
            } else {
                z = r2.A01.A0D(this, A00, A01, 193, 32000);
            }
        } catch (AnonymousClass1V9 e) {
            Log.e(e.getMessage());
            z = false;
        }
        StringBuilder A0k = C12960it.A0k("app/sendReportBizProduct productId=");
        A0k.append(str);
        A0k.append(" success:");
        A0k.append(z);
        C12960it.A1F(A0k);
        return z;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("sendReportBizProduct/delivery-error");
        C19840ul r4 = this.A04;
        r4.A02("report_product_tag");
        this.A00.A02(this.A01, false);
        r4.A06("report_product_tag", false);
    }

    @Override // X.AnonymousClass1W3
    public void APB(UserJid userJid) {
        Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("sendReportBizProduct/direct-connection-error/jid=")));
        this.A00.A02(this.A01, false);
    }

    @Override // X.AnonymousClass1W3
    public void APC(UserJid userJid) {
        Log.e(C12960it.A0d(userJid.getRawString(), C12960it.A0k("sendReportBizProduct/direct-connection-success/jid=")));
        A02();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        Log.e("sendReportBizProduct/response-error");
        C19840ul r4 = this.A04;
        r4.A02("report_product_tag");
        AnonymousClass281 r2 = this.A01;
        if (!A01(r2.A00, C41151sz.A00(r6))) {
            this.A00.A02(r2, false);
        }
        r4.A06("report_product_tag", false);
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r7, String str) {
        AnonymousClass19T r1;
        AnonymousClass281 r0;
        C19840ul r5 = this.A04;
        r5.A02("report_product_tag");
        AnonymousClass1V8 A0E = r7.A0E("response");
        boolean z = false;
        if (A0E != null) {
            AnonymousClass1V8 A0E2 = A0E.A0E("success");
            if (A0E2 != null) {
                boolean equals = "true".equals(A0E2.A0G());
                r1 = this.A00;
                r0 = this.A01;
                if (equals) {
                    z = true;
                }
            } else {
                return;
            }
        } else {
            Log.e(C12960it.A0d(r7.toString(), C12960it.A0k("sendReportBizProduct/corrupted-response:")));
            r1 = this.A00;
            r0 = this.A01;
        }
        r1.A02(r0, z);
        r5.A06("report_product_tag", z);
    }
}
