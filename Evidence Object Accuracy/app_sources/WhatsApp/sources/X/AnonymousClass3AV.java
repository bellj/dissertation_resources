package X;

import android.app.Activity;
import android.content.Intent;

/* renamed from: X.3AV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AV {
    public static void A00(Activity activity, C14820m6 r4) {
        String str;
        boolean A1W = C12980iv.A1W(r4.A00, "community_nux");
        Intent A0A = C12970iu.A0A();
        String packageName = activity.getPackageName();
        if (A1W) {
            str = "com.whatsapp.community.NewCommunityActivity";
        } else {
            str = "com.whatsapp.community.CommunityNUXActivity";
        }
        A0A.setClassName(packageName, str);
        activity.startActivity(A0A);
    }
}
