package X;

import java.lang.ref.WeakReference;

/* renamed from: X.37p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C625337p extends AbstractC16350or {
    public final C14330lG A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final String A03;
    public final WeakReference A04;

    public C625337p(ActivityC13810kN r2, C14330lG r3, C14900mE r4, C15570nT r5, String str) {
        this.A01 = r4;
        this.A02 = r5;
        this.A00 = r3;
        this.A04 = C12970iu.A10(r2);
        this.A03 = str;
    }
}
