package X;

import android.os.PowerManager;
import com.whatsapp.Mp4Ops;

/* renamed from: X.1Ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26821Ey {
    public PowerManager.WakeLock A00;
    public final AbstractC15710nm A01;
    public final C14330lG A02;
    public final Mp4Ops A03;
    public final C002701f A04;
    public final C15450nH A05;
    public final C17050qB A06;
    public final AnonymousClass01d A07;
    public final C16590pI A08;
    public final C14820m6 A09;
    public final C14950mJ A0A;
    public final C19350ty A0B;
    public final C15660nh A0C;
    public final C14850m9 A0D;
    public final C16120oU A0E;
    public final C239713s A0F;
    public final C26831Ez A0G;
    public final AnonymousClass1EK A0H;
    public final AnonymousClass160 A0I;
    public final C22590zK A0J;
    public final C26511Dt A0K;
    public final C15690nk A0L;
    public final C22190yg A0M;
    public final AnonymousClass152 A0N;
    public final C26521Du A0O;

    public C26821Ey(AbstractC15710nm r2, C14330lG r3, Mp4Ops mp4Ops, C002701f r5, C15450nH r6, C17050qB r7, AnonymousClass01d r8, C16590pI r9, C14820m6 r10, C14950mJ r11, C19350ty r12, C15660nh r13, C14850m9 r14, C16120oU r15, C239713s r16, C26831Ez r17, AnonymousClass1EK r18, AnonymousClass160 r19, C22590zK r20, C26511Dt r21, C15690nk r22, C22190yg r23, AnonymousClass152 r24, C26521Du r25) {
        this.A08 = r9;
        this.A03 = mp4Ops;
        this.A0D = r14;
        this.A01 = r2;
        this.A02 = r3;
        this.A0E = r15;
        this.A05 = r6;
        this.A0A = r11;
        this.A0K = r21;
        this.A0M = r23;
        this.A07 = r8;
        this.A0F = r16;
        this.A0O = r25;
        this.A0J = r20;
        this.A0B = r12;
        this.A0C = r13;
        this.A06 = r7;
        this.A0N = r24;
        this.A09 = r10;
        this.A0L = r22;
        this.A0G = r17;
        this.A0I = r19;
        this.A04 = r5;
        this.A0H = r18;
    }
}
