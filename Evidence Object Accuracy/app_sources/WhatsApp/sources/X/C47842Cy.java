package X;

import java.lang.ref.ReferenceQueue;

/* renamed from: X.2Cy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47842Cy {
    public static final AnonymousClass2D0 A00 = new AnonymousClass2D0();
    public static final C47852Cz A01 = new C47852Cz();
    public static final Thread A02;
    public static final ReferenceQueue A03 = new ReferenceQueue();

    static {
        AnonymousClass2D5 r0 = new AnonymousClass2D5();
        A02 = r0;
        r0.start();
    }
}
