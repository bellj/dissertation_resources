package X;

import java.util.ArrayList;

/* renamed from: X.5zW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C130505zW {
    public static final ArrayList A01;
    public static final ArrayList A02;
    public final AnonymousClass1V8 A00;

    static {
        String[] strArr = new String[2];
        strArr[0] = "1";
        A02 = C12960it.A0m("2", strArr, 1);
        String[] strArr2 = new String[2];
        strArr2[0] = "0";
        A01 = C12960it.A0m("1", strArr2, 1);
    }

    public C130505zW(AnonymousClass3CT r12, Long l, Long l2, String str, String str2, String str3, String str4, String str5) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy A0N = C117295Zj.A0N(A0M);
        C41141sy.A01(A0N, "action", "upi-bind-device");
        if (C117295Zj.A1W(str, 1, false)) {
            C41141sy.A01(A0N, "device-id", str);
        }
        if (AnonymousClass3JT.A0E(str2, 1, 35, false)) {
            C41141sy.A01(A0N, "verification-data", str2);
        }
        if (AnonymousClass3JT.A0E(str3, 1, 10, false)) {
            C41141sy.A01(A0N, "provider-type", str3);
        }
        if (str4 != null && AnonymousClass3JT.A0E(str4, 1, 100, true)) {
            C41141sy.A01(A0N, "sms-phone-number", str4);
        }
        if (l != null && AnonymousClass3JT.A0D(l, 0, true)) {
            C117315Zl.A0X(A0N, "delay", l.longValue());
        }
        if (l2 != null && AnonymousClass3JT.A0D(l2, 0, true)) {
            C117315Zl.A0X(A0N, "counter", l2.longValue());
        }
        A0N.A0A("2", "version", A02);
        ArrayList arrayList = A01;
        if (str5 != null) {
            A0N.A0A(str5, "existing-account", arrayList);
        }
        this.A00 = C117295Zj.A0J(A0N, A0M, r12);
    }
}
