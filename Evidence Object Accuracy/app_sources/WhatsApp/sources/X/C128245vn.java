package X;

import android.content.Context;

/* renamed from: X.5vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128245vn {
    public final Context A00;
    public final C14900mE A01;
    public final C18640sm A02;
    public final AnonymousClass102 A03;
    public final C18650sn A04;
    public final C18610sj A05;
    public final C17070qD A06;
    public final C128565wJ A07;
    public final C18590sh A08;
    public final String A09;

    public C128245vn(Context context, C14900mE r2, C18640sm r3, AnonymousClass102 r4, C18650sn r5, C18610sj r6, C17070qD r7, C128565wJ r8, C18590sh r9, String str) {
        this.A00 = context;
        this.A01 = r2;
        this.A08 = r9;
        this.A06 = r7;
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
        this.A09 = str;
        this.A07 = r8;
    }
}
