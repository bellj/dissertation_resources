package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.wearable.ConnectionConfiguration;

/* renamed from: X.4kS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C99534kS implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        int A01 = C95664e9.A01(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        int i2 = 0;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < A01) {
            int readInt = parcel.readInt();
            switch ((char) readInt) {
                case 2:
                    str = C95664e9.A08(parcel, readInt);
                    break;
                case 3:
                    str2 = C95664e9.A08(parcel, readInt);
                    break;
                case 4:
                    i = C95664e9.A02(parcel, readInt);
                    break;
                case 5:
                    i2 = C95664e9.A02(parcel, readInt);
                    break;
                case 6:
                    z = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case 7:
                    z2 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\b':
                    str3 = C95664e9.A08(parcel, readInt);
                    break;
                case '\t':
                    z3 = C12960it.A1S(C95664e9.A02(parcel, readInt));
                    break;
                case '\n':
                    str4 = C95664e9.A08(parcel, readInt);
                    break;
                case 11:
                    str5 = C95664e9.A08(parcel, readInt);
                    break;
                default:
                    C95664e9.A0D(parcel, readInt);
                    break;
            }
        }
        C95664e9.A0C(parcel, A01);
        return new ConnectionConfiguration(str, str2, str3, str4, str5, i, i2, z, z2, z3);
    }

    @Override // android.os.Parcelable.Creator
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new ConnectionConfiguration[i];
    }
}
