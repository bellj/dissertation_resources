package X;

import com.facebook.redex.RunnableBRunnable0Shape1S1101000_I1;
import com.whatsapp.util.Log;

/* renamed from: X.3ZF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3ZF implements AbstractC21730xt {
    public final C14900mE A00;
    public final C17220qS A01;
    public final AnonymousClass4OD A02;

    public AnonymousClass3ZF(C14900mE r1, C17220qS r2, AnonymousClass4OD r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        Log.e("sendGetContactQrCode/delivery-error");
        this.A00.A0I(new RunnableBRunnable0Shape1S1101000_I1(this, null, 0, 1));
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r6, String str) {
        Log.e("sendGetContactQrCode/response-error");
        this.A00.A0I(new RunnableBRunnable0Shape1S1101000_I1(this, null, C41151sz.A00(r6), 1));
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r6, String str) {
        String str2;
        String str3;
        AnonymousClass1V8 A0E = r6.A0E("qr");
        if (A0E == null || !"contact".equals(A0E.A0I("type", null))) {
            str2 = null;
        } else {
            str2 = A0E.A0I("code", null);
            if (str2 != null) {
                str3 = "sendGetContactQrCode/success";
                Log.e(str3);
                this.A00.A0I(new RunnableBRunnable0Shape1S1101000_I1(this, str2, 0, 1));
            }
        }
        str3 = "sendGetContactQrCode/error: invalid response";
        Log.e(str3);
        this.A00.A0I(new RunnableBRunnable0Shape1S1101000_I1(this, str2, 0, 1));
    }
}
