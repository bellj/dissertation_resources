package X;

import android.net.Uri;

/* renamed from: X.29b  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C471329b extends AbstractC16350or {
    public final Uri A00;
    public final C471229a A01;
    public final C25721Am A02;
    public final C19380u1 A03;

    public C471329b(C471229a r2, C25721Am r3, C19380u1 r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = null;
    }

    public C471329b(Uri uri, C471229a r2, C25721Am r3, C19380u1 r4) {
        this.A03 = r4;
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = uri;
    }
}
