package X;

import android.content.ContentResolver;
import android.net.Uri;
import java.io.File;
import java.util.Date;

/* renamed from: X.2UR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2UR implements AbstractC35611iN, AnonymousClass2U0 {
    public final int A00;
    public final long A01;
    public final ContentResolver A02;
    public final Uri A03;
    public final File A04;
    public final boolean A05;

    @Override // X.AbstractC35611iN
    public String AES() {
        return "image/*";
    }

    @Override // X.AnonymousClass2U0
    public byte AHf() {
        return 1;
    }

    @Override // X.AbstractC35611iN
    public int getType() {
        return 0;
    }

    public AnonymousClass2UR(ContentResolver contentResolver, File file, int i, boolean z) {
        Uri.Builder buildUpon = Uri.fromFile(file).buildUpon();
        if (z) {
            buildUpon.appendQueryParameter("flip-h", "1");
        }
        if (i != 0) {
            buildUpon.appendQueryParameter("rotation", Integer.toString(i));
        }
        Uri build = buildUpon.build();
        long length = file.length();
        this.A02 = contentResolver;
        this.A03 = build;
        this.A01 = length;
        this.A04 = file;
        this.A05 = z;
        this.A00 = i;
    }

    @Override // X.AbstractC35611iN
    public Uri AAE() {
        return this.A03;
    }

    @Override // X.AbstractC35611iN
    public long ACQ() {
        return new Date(this.A04.lastModified()).getTime();
    }

    @Override // X.AbstractC35611iN
    public /* synthetic */ long ACb() {
        return 0;
    }

    @Override // X.AnonymousClass2U0
    public File ACy() {
        return this.A04;
    }

    @Override // X.AnonymousClass2U0
    public int AGI() {
        return this.A00;
    }

    @Override // X.AnonymousClass2U0
    public boolean AJR() {
        return this.A05;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:18:0x0010 */
    /* JADX WARN: Type inference failed for: r8v0, types: [int] */
    /* JADX WARN: Type inference failed for: r8v2, types: [android.graphics.Bitmap] */
    /* JADX WARN: Type inference failed for: r8v3 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // X.AbstractC35611iN
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap Aem(int r16) {
        /*
            r15 = this;
            r7 = 0
            android.content.ContentResolver r6 = r15.A02     // Catch: Exception -> 0x0044
            android.net.Uri r5 = r15.A03     // Catch: Exception -> 0x0044
            android.graphics.Matrix r13 = X.C22200yh.A0D(r6, r5)     // Catch: Exception -> 0x0044
            r8 = r16
            long r1 = (long) r8     // Catch: Exception -> 0x0044
            long r1 = r1 * r1
            r3 = 2
            long r1 = r1 * r3
            java.io.File r3 = X.C14350lI.A03(r5)     // Catch: FileNotFoundException -> 0x0024, Exception -> 0x0044
            if (r3 == 0) goto L_0x001d
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            android.os.ParcelFileDescriptor r0 = android.os.ParcelFileDescriptor.open(r3, r0)     // Catch: FileNotFoundException -> 0x0024, Exception -> 0x0044
            goto L_0x0025
        L_0x001d:
            java.lang.String r0 = "r"
            android.os.ParcelFileDescriptor r0 = r6.openFileDescriptor(r5, r0)     // Catch: FileNotFoundException -> 0x0024, Exception -> 0x0044
            goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            android.graphics.Bitmap r8 = X.AnonymousClass3Il.A02(r0, r8, r1)     // Catch: Exception -> 0x0044
            if (r13 == 0) goto L_0x0043
            boolean r0 = r13.isIdentity()     // Catch: Exception -> 0x0044
            if (r0 != 0) goto L_0x0043
            if (r8 == 0) goto L_0x0042
            r9 = 0
            int r11 = r8.getWidth()     // Catch: Exception -> 0x0044
            int r12 = r8.getHeight()     // Catch: Exception -> 0x0044
            r14 = 1
            r10 = 0
            android.graphics.Bitmap r7 = android.graphics.Bitmap.createBitmap(r8, r9, r10, r11, r12, r13, r14)     // Catch: Exception -> 0x0044
        L_0x0042:
            return r7
        L_0x0043:
            return r8
        L_0x0044:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2UR.Aem(int):android.graphics.Bitmap");
    }

    @Override // X.AbstractC35611iN
    public long getContentLength() {
        return this.A01;
    }
}
