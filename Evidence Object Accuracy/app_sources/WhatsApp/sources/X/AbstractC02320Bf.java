package X;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;

/* renamed from: X.0Bf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC02320Bf extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    public static final Interpolator A00 = new DecelerateInterpolator();
}
