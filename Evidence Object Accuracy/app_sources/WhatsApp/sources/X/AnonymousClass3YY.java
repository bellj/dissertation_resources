package X;

import com.whatsapp.R;
import com.whatsapp.migration.export.service.MessagesExporterService;
import com.whatsapp.util.Log;

/* renamed from: X.3YY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YY implements AnonymousClass2F5 {
    public final /* synthetic */ MessagesExporterService A00;

    public /* synthetic */ AnonymousClass3YY(MessagesExporterService messagesExporterService) {
        this.A00 = messagesExporterService;
    }

    @Override // X.AnonymousClass2F5
    public void ANg() {
        MessagesExporterService messagesExporterService = this.A00;
        AnonymousClass3F9 r4 = messagesExporterService.A01;
        Log.i("MessagesExporterNotificationManager/onCancellationComplete()");
        r4.A02(C16590pI.A00(r4.A00).getString(R.string.export_notification_export_cancelled), null, -1, true);
        Log.i("xpm-export-service-onCancellationCompleted/sent export cancellation complete logging");
        messagesExporterService.stopSelf();
    }

    @Override // X.AnonymousClass2F5
    public void ANh() {
        AnonymousClass3F9 r4 = this.A00.A01;
        Log.i("MessagesExporterNotificationManager/onCancelling()");
        r4.A02(C16590pI.A00(r4.A00).getString(R.string.export_notification_cancelling_export), null, -1, false);
    }

    @Override // X.AnonymousClass2F5
    public void AOL() {
        Log.i("xpm-export-service-onComplete/success");
        AnonymousClass3F9 r4 = this.A00.A01;
        Log.i("MessagesExporterNotificationManager/onComplete()");
        r4.A02(C16590pI.A00(r4.A00).getString(R.string.export_notification_export_completed), null, -1, true);
        Log.i("xpm-export-service-onComplete/sent export complete logging");
    }

    @Override // X.AnonymousClass2F5
    public void APl(int i) {
        Log.i(C12960it.A0W(i, "xpm-export-service-onError/errorCode = "));
        AnonymousClass3F9 r4 = this.A00.A01;
        C16590pI r2 = r4.A00;
        r4.A02(C16590pI.A00(r2).getString(R.string.export_notification_export_failed), C16590pI.A00(r2).getString(R.string.export_notification_export_failed_detail), -1, true);
    }

    @Override // X.AnonymousClass2F5
    public void AQ5() {
        this.A00.A01.A01(0);
    }

    @Override // X.AnonymousClass2F5
    public void AUL(int i) {
        Log.i(C12960it.A0W(i, "xpm-export-service-onProgress; progress="));
        this.A00.A01.A01(i);
    }
}
