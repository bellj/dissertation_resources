package X;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.whatsapp.R;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.3Fd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64333Fd {
    public AnonymousClass4L3 A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05;
    public final int A06;
    public final Context A07;
    public final C22040yO A08;
    public final ArrayList A09;

    public C64333Fd(Context context, AnonymousClass018 r4, C22040yO r5) {
        this.A07 = context;
        this.A08 = r5;
        this.A06 = context.getResources().getDimensionPixelSize(R.dimen.sms_code_input_box_width_small);
        this.A02 = context.getResources().getDimensionPixelSize(R.dimen.sms_code_input_box_height_small);
        this.A05 = context.getResources().getDimensionPixelSize(R.dimen.sms_code_input_box_width_large);
        this.A01 = context.getResources().getDimensionPixelSize(R.dimen.sms_code_input_box_height_large);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.sms_code_input_box_margin_horizontal);
        if (C28141Kv.A01(r4)) {
            this.A04 = dimensionPixelSize;
        } else {
            this.A03 = dimensionPixelSize;
        }
        this.A09 = C12960it.A0l();
    }

    public String A00() {
        StringBuilder A0h = C12960it.A0h();
        Iterator it = this.A09.iterator();
        while (it.hasNext()) {
            EditText editText = (EditText) it.next();
            if (!TextUtils.isEmpty(editText.getText())) {
                C12970iu.A1V(editText.getText(), A0h);
            }
        }
        return A0h.toString();
    }

    public void A01() {
        ArrayList arrayList = this.A09;
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C12990iw.A1G((TextView) it.next());
        }
        ((View) arrayList.get(0)).requestFocus();
    }

    public void A02(boolean z) {
        Iterator it = this.A09.iterator();
        while (it.hasNext()) {
            ((View) it.next()).setEnabled(z);
        }
    }

    public boolean A03() {
        return C12960it.A1S(this.A08.A01(1780) ? 1 : 0);
    }
}
