package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/* renamed from: X.63e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1315463e implements Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(9);
    public final AnonymousClass63Y A00;
    public final C1316263m A01;
    public final String A02;
    public final String A03;
    public final String A04;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C1315463e(AnonymousClass63Y r1, C1316263m r2, String str, String str2, String str3) {
        this.A02 = str;
        this.A01 = r2;
        this.A00 = r1;
        this.A03 = str2;
        this.A04 = str3;
    }

    public static C1315463e A00(AnonymousClass102 r9, AnonymousClass1V8 r10) {
        AnonymousClass63Y r4;
        String A0H = r10.A0H("id");
        C1316263m r5 = null;
        String A0I = r10.A0I("kyc-status", null);
        AnonymousClass1V8 A0E = r10.A0E("balance");
        String A0I2 = r10.A0I("local_iso_code", null);
        String A0I3 = r10.A0I("primary_iso_code", null);
        String A0I4 = r10.A0I("tkyc_tier", null);
        if (TextUtils.isEmpty(A0I2) || TextUtils.isEmpty(A0I3)) {
            r4 = null;
        } else {
            r4 = new AnonymousClass63Y(r9.A02(A0I2), r9.A02(A0I3), A0I3);
        }
        if (A0E != null) {
            r5 = C1316263m.A00(r9, A0E);
        }
        return new C1315463e(r4, r5, A0H, A0I, A0I4);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.A02);
        parcel.writeParcelable(this.A01, i);
        parcel.writeParcelable(this.A00, i);
        parcel.writeString(this.A03);
        parcel.writeString(this.A04);
    }
}
