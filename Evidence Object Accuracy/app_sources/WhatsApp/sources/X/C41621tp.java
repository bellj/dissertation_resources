package X;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;

/* renamed from: X.1tp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C41621tp extends C006202y {
    public final /* synthetic */ AnonymousClass1O4 A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C41621tp(AnonymousClass1O4 r1, int i) {
        super(i);
        this.A00 = r1;
    }

    @Override // X.C006202y
    public /* bridge */ /* synthetic */ int A02(Object obj, Object obj2) {
        AnonymousClass1O4 r2 = this.A00;
        Object obj3 = ((C41631tq) obj2).A01;
        if ((r2 instanceof AnonymousClass1O7) || (r2 instanceof C41641tr) || (r2 instanceof AnonymousClass1O5) || (r2 instanceof AnonymousClass1O8)) {
            return ((Bitmap) obj3).getByteCount() >> 10;
        }
        return 1;
    }

    @Override // X.C006202y
    public /* bridge */ /* synthetic */ void A09(Object obj, Object obj2, Object obj3, boolean z) {
        AnonymousClass1O4 r1 = this.A00;
        Object obj4 = ((C41631tq) obj2).A01;
        if (r1 instanceof AnonymousClass1O5) {
            AnonymousClass1O5 r12 = (AnonymousClass1O5) r1;
            if (z) {
                for (C41501td r0 : r12.A00.A0L) {
                    r0.A00.A05.put(obj, new SoftReference(obj4));
                }
            }
        }
    }
}
