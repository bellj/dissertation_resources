package X;

import java.util.LinkedList;
import java.util.List;

/* renamed from: X.4xA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C107334xA implements AbstractC117015Xu {
    public final /* synthetic */ C106474vl A00;

    public C107334xA(C106474vl r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC117015Xu
    public List ACR(String str, boolean z, boolean z2) {
        List<C95494dp> A03 = C95604e3.A03(str, z, z2);
        if (!A03.isEmpty()) {
            if (!this.A00.A00) {
                LinkedList linkedList = new LinkedList();
                boolean z3 = false;
                for (C95494dp r2 : A03) {
                    if (z3 || !r2.A03.startsWith("OMX.google")) {
                        linkedList.add(r2);
                    } else {
                        z3 = true;
                        linkedList.add(0, r2);
                    }
                }
                return linkedList;
            }
            A03.get(0);
        }
        return A03;
    }
}
