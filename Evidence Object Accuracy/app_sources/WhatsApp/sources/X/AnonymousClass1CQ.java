package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1CQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CQ {
    public final C14650lo A00;
    public final C14850m9 A01;

    public AnonymousClass1CQ(C14650lo r1, C14850m9 r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(AnonymousClass5TP r3, UserJid userJid) {
        C14850m9 r1 = this.A01;
        if (r1.A07(1678) && r1.A07(1096)) {
            C14650lo r12 = this.A00;
            if (r12.A08()) {
                r12.A03(new AbstractC30111Wd(r3, userJid) { // from class: X.53Q
                    public final /* synthetic */ AnonymousClass5TP A01;
                    public final /* synthetic */ UserJid A02;

                    {
                        this.A01 = r2;
                        this.A02 = r3;
                    }

                    @Override // X.AbstractC30111Wd
                    public final void ANP(C30141Wg r4) {
                        AnonymousClass5TP r2 = this.A01;
                        UserJid userJid2 = this.A02;
                        if (r4 != null && r4.A0H) {
                            r2.AQG(userJid2);
                        }
                    }
                }, userJid);
            }
        }
    }
}
