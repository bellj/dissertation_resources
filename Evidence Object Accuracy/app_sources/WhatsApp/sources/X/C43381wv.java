package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.1wv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43381wv extends BroadcastReceiver {
    public final /* synthetic */ C19890uq A00;

    public C43381wv(C19890uq r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if ("com.whatsapp.MessageHandler.RECONNECT_ACTION".equals(intent.getAction())) {
            Log.i("xmpp/handler/reconnect");
            this.A00.A0B(intent.getIntExtra("connect_reason", 0));
            return;
        }
        StringBuilder sb = new StringBuilder("unknown intent received in reconnect receiver ");
        sb.append(intent);
        Log.w(sb.toString());
    }
}
