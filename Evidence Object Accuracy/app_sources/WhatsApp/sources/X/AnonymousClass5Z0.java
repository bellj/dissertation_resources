package X;

import java.util.Comparator;

/* renamed from: X.5Z0  reason: invalid class name */
/* loaded from: classes3.dex */
public interface AnonymousClass5Z0 extends Iterable {
    @Override // X.AnonymousClass5Z0
    Comparator comparator();
}
