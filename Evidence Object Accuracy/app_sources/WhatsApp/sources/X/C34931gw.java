package X;

import java.util.HashSet;
import java.util.Set;

/* renamed from: X.1gw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34931gw implements AbstractC34921gv {
    @Override // X.AbstractC34921gv
    public String AEZ() {
        return "p";
    }

    @Override // X.AbstractC34921gv
    public Set AEH(AbstractC15340mz r5) {
        String str;
        String str2;
        String str3;
        if (r5.A0m == null) {
            return null;
        }
        HashSet hashSet = new HashSet();
        hashSet.add("");
        AnonymousClass1IR r2 = r5.A0L;
        if (r2 != null) {
            if (AnonymousClass14X.A07(r2)) {
                str = "c";
            } else {
                str = "i";
            }
            hashSet.add(str);
            if (r2.A0F()) {
                int i = r2.A02;
                if (i == 12) {
                    str3 = "n";
                } else if (i == 17) {
                    str3 = "q";
                }
                hashSet.add(str3);
            }
            if (r2.A0Q) {
                str2 = "s";
            } else {
                str2 = "r";
            }
            hashSet.add(str2);
        }
        return hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0009, code lost:
        if (r1 == false) goto L_0x000b;
     */
    @Override // X.AbstractC34921gv
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C34941gx AEa(X.C15250mo r6) {
        /*
            r5 = this;
            java.lang.Boolean r0 = r6.A07
            if (r0 == 0) goto L_0x000b
            boolean r1 = r0.booleanValue()
            r0 = 1
            if (r1 != 0) goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x0018
            r0 = 0
            return r0
        L_0x0018:
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
            X.1Zk r4 = r6.A05
            if (r4 == 0) goto L_0x0060
            boolean r0 = r4.A04
            java.lang.String r1 = "n"
            if (r0 == 0) goto L_0x0075
            java.lang.String r0 = "q"
            r2.add(r0)
            r2.add(r1)
        L_0x0034:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            boolean r0 = r4.A06
            if (r0 == 0) goto L_0x0042
            java.lang.String r0 = "s"
            r1.add(r0)
        L_0x0042:
            X.1Zl r0 = r4.A00
            if (r0 == 0) goto L_0x004b
            java.lang.String r0 = "r"
            r1.add(r0)
        L_0x004b:
            boolean r0 = r4.A02
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = "c"
            r1.add(r0)
        L_0x0054:
            boolean r0 = r4.A03
            if (r0 == 0) goto L_0x005d
            java.lang.String r0 = "i"
            r1.add(r0)
        L_0x005d:
            r3.addAll(r1)
        L_0x0060:
            boolean r0 = r3.isEmpty()
            if (r0 == 0) goto L_0x006b
            java.lang.String r0 = ""
            r3.add(r0)
        L_0x006b:
            X.1gx r0 = new X.1gx
            r0.<init>()
            r0.A00 = r3
            r0.A01 = r2
            return r0
        L_0x0075:
            boolean r0 = r4.A05
            if (r0 == 0) goto L_0x0034
            r3.add(r1)
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C34931gw.AEa(X.0mo):X.1gx");
    }
}
