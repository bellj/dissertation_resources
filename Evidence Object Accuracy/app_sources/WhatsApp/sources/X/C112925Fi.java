package X;

import java.io.IOException;

/* renamed from: X.5Fi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C112925Fi implements AbstractC117245Zc {
    public C92754Xh A00;

    public C112925Fi(C92754Xh r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VQ
    public AnonymousClass1TL ADw() {
        return new C114795Nc(this.A00.A01());
    }

    @Override // X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        try {
            return ADw();
        } catch (IOException e) {
            throw new AnonymousClass4CU(e.getMessage(), e);
        }
    }
}
