package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5lE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122295lE extends AbstractC118825cR {
    public final TextView A00;

    public C122295lE(View view) {
        super(view);
        this.A00 = C12960it.A0I(view, R.id.instance_transaction_info);
    }
}
