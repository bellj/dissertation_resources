package X;

import android.content.Context;
import android.net.Uri;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: X.3Sx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C67793Sx implements AnonymousClass2BW {
    public AnonymousClass2BW A00;
    public AnonymousClass2BW A01;
    public AnonymousClass2BW A02;
    public AnonymousClass2BW A03;
    public AnonymousClass2BW A04;
    public AnonymousClass2BW A05;
    public AnonymousClass2BW A06;
    public AnonymousClass2BW A07;
    public final Context A08;
    public final AnonymousClass2BW A09;
    public final List A0A = C12960it.A0l();

    public C67793Sx(Context context, AnonymousClass2BW r3) {
        this.A08 = context.getApplicationContext();
        this.A09 = r3;
    }

    public final void A00(AnonymousClass2BW r4) {
        int i = 0;
        while (true) {
            List list = this.A0A;
            if (i < list.size()) {
                r4.A5p((AnonymousClass5QP) list.get(i));
                i++;
            } else {
                return;
            }
        }
    }

    @Override // X.AnonymousClass2BW
    public void A5p(AnonymousClass5QP r2) {
        this.A09.A5p(r2);
        this.A0A.add(r2);
        AnonymousClass2BW r0 = this.A04;
        if (r0 != null) {
            r0.A5p(r2);
        }
        AnonymousClass2BW r02 = this.A00;
        if (r02 != null) {
            r02.A5p(r2);
        }
        AnonymousClass2BW r03 = this.A01;
        if (r03 != null) {
            r03.A5p(r2);
        }
        AnonymousClass2BW r04 = this.A06;
        if (r04 != null) {
            r04.A5p(r2);
        }
        AnonymousClass2BW r05 = this.A07;
        if (r05 != null) {
            r05.A5p(r2);
        }
        AnonymousClass2BW r06 = this.A02;
        if (r06 != null) {
            r06.A5p(r2);
        }
        AnonymousClass2BW r07 = this.A05;
        if (r07 != null) {
            r07.A5p(r2);
        }
    }

    @Override // X.AnonymousClass2BW
    public Map AGF() {
        AnonymousClass2BW r0 = this.A03;
        return r0 == null ? Collections.emptyMap() : r0.AGF();
    }

    @Override // X.AnonymousClass2BW
    public Uri AHS() {
        AnonymousClass2BW r0 = this.A03;
        if (r0 == null) {
            return null;
        }
        return r0.AHS();
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ee  */
    @Override // X.AnonymousClass2BW
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long AYZ(X.AnonymousClass3H3 r5) {
        /*
        // Method dump skipped, instructions count: 281
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C67793Sx.AYZ(X.3H3):long");
    }

    @Override // X.AnonymousClass2BW
    public void close() {
        AnonymousClass2BW r0 = this.A03;
        if (r0 != null) {
            try {
                r0.close();
            } finally {
                this.A03 = null;
            }
        }
    }

    @Override // X.AnonymousClass2BY
    public int read(byte[] bArr, int i, int i2) {
        return this.A03.read(bArr, i, i2);
    }
}
