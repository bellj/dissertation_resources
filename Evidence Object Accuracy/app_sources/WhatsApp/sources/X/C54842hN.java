package X;

import android.animation.ValueAnimator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape10S0200000_I1;

/* renamed from: X.2hN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54842hN extends AbstractC05270Ox {
    public ValueAnimator A00;
    public Runnable A01;
    public boolean A02;
    public final C54652h4 A03;
    public final boolean A04;

    public C54842hN(C54652h4 r3, boolean z) {
        this.A03 = r3;
        r3.A01 = this;
        this.A02 = C12960it.A1S((r3.A00 > 0.0f ? 1 : (r3.A00 == 0.0f ? 0 : -1)));
        this.A04 = z;
        if (z) {
            r3.A00 = 1.0f;
        }
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        if (this.A04) {
            return;
        }
        if (i == 0) {
            RunnableBRunnable0Shape10S0200000_I1 runnableBRunnable0Shape10S0200000_I1 = new RunnableBRunnable0Shape10S0200000_I1(this, 29, recyclerView);
            this.A01 = runnableBRunnable0Shape10S0200000_I1;
            recyclerView.postDelayed(runnableBRunnable0Shape10S0200000_I1, 1500);
            return;
        }
        recyclerView.removeCallbacks(this.A01);
        if (!this.A02) {
            ValueAnimator valueAnimator = this.A00;
            if (valueAnimator != null && valueAnimator.isRunning()) {
                this.A00.cancel();
            }
            C54652h4 r1 = this.A03;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(r1.A00, 1.0f);
            ofFloat.addUpdateListener(new C65433Jo(recyclerView, r1));
            ofFloat.setDuration(200L);
            ofFloat.start();
            this.A02 = true;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            C54842hN r4 = (C54842hN) obj;
            if (this.A04 == r4.A04) {
                return this.A03.equals(r4.A03);
            }
        }
        return false;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A03;
        return C12960it.A06(Boolean.valueOf(this.A04), A1a);
    }
}
