package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.2gZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54342gZ extends AnonymousClass02M {
    public final List A00 = C12960it.A0l();
    public final /* synthetic */ C69223Yk A01;

    public C54342gZ(C69223Yk r2) {
        this.A01 = r2;
        A07(true);
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r5, int i) {
        AnonymousClass4OU r3 = (AnonymousClass4OU) this.A00.get(i);
        ((AbstractC75693kG) r5).A08(new ViewOnClickCListenerShape3S0200000_I1_1(this, 45, r3), r3, C12960it.A1V(this.A01.A00, r3.A00));
    }

    @Override // X.AnonymousClass02M
    public AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        LayoutInflater A0E = C12960it.A0E(viewGroup);
        if (i == 0) {
            return new AnonymousClass35l(A0E.inflate(R.layout.avatar_sticker_header_text_item, viewGroup, false));
        }
        return new C622135k(A0E.inflate(R.layout.sticker_pack_preview, viewGroup, false));
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        return this.A00.get(i) instanceof AnonymousClass47I ? 0 : 1;
    }
}
