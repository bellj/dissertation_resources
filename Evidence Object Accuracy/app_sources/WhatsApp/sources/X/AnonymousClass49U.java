package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.49U  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass49U extends Enum {
    public static final AnonymousClass49U A00 = new AnonymousClass49U("BOTTOM_LEFT", 0.0f, 1.0f, 1.0f, 2.0f, 2);
    public static final AnonymousClass49U A01 = new AnonymousClass49U("BOTTOM_RIGHT", 1.0f, 1.0f, 2.0f, 2.0f, 3);
    public static final AnonymousClass49U A02 = new AnonymousClass49U("TOP_LEFT", 0.0f, 0.0f, 1.0f, 1.0f, 0);
    public static final AnonymousClass49U A03 = new AnonymousClass49U("TOP_RIGHT", 1.0f, 0.0f, 2.0f, 1.0f, 1);
    public final float bottom;
    public final float left;
    public final float right;
    public final float top;

    public AnonymousClass49U(String str, float f, float f2, float f3, float f4, int i) {
        this.left = f;
        this.top = f2;
        this.right = f3;
        this.bottom = f4;
    }
}
