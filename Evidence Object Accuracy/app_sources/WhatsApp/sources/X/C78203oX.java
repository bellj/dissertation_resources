package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3oX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78203oX extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C99394kE();
    public final int A00;
    public final C78453ow A01;

    public C78203oX(C78453ow r1, int i) {
        this.A00 = i;
        this.A01 = r1;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 2, this.A00);
        C95654e8.A0A(parcel, this.A01, i, A00);
    }
}
