package X;

/* renamed from: X.2zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C61282zm extends AnonymousClass4WS {
    public final C92664Wv A00;
    public final AnonymousClass5RV A01;

    public C61282zm(C92664Wv r2, AnonymousClass5RV r3) {
        super(2);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass4WS
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        return this.A00.equals(((C61282zm) obj).A00);
    }

    @Override // X.AnonymousClass4WS
    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.hashCode());
        return C12960it.A06(this.A00, A1a);
    }
}
