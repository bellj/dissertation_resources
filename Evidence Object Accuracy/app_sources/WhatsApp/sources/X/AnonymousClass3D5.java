package X;

import java.util.Map;

/* renamed from: X.3D5  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3D5 {
    public final /* synthetic */ AbstractC17770rM A00;
    public final /* synthetic */ C27661Io A01;
    public final /* synthetic */ AnonymousClass46T A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ Map A04;

    public AnonymousClass3D5(AbstractC17770rM r1, C27661Io r2, AnonymousClass46T r3, String str, Map map) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = str;
        this.A04 = map;
        this.A00 = r1;
    }

    public void A00() {
        C27661Io r4 = this.A01;
        C63963Dq r0 = r4.A02;
        if (r0 == null) {
            throw C16700pc.A06("fcsLoadingEventManager");
        }
        r0.A00.A02(r0.A01).A01(new AnonymousClass6ED("onLoadingCompletion", r4.A05));
        AnonymousClass46T r2 = this.A02;
        Map map = this.A04;
        r4.A03(r2, map);
        r4.A02(this.A00, r2, map);
    }
}
