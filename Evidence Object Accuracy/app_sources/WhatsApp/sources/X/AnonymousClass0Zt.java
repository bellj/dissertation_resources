package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0Zt  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Zt implements AbstractC11430gH {
    public AbstractC11440gI A00;
    public AbstractC06170Sl A01;
    public Object A02;
    public final List A03 = new ArrayList();

    public abstract boolean A01(C004401z v);

    public abstract boolean A02(Object obj);

    public AnonymousClass0Zt(AbstractC06170Sl r2) {
        this.A01 = r2;
    }

    public final void A00(AbstractC11440gI r10, Object obj) {
        List<String> list = this.A03;
        if (!(list.isEmpty() || r10 == null)) {
            if (obj == null || A02(obj)) {
                AnonymousClass0Zu r102 = (AnonymousClass0Zu) r10;
                synchronized (r102.A01) {
                    AbstractC12450hw r0 = r102.A00;
                    if (r0 != null) {
                        r0.AM9(list);
                    }
                }
                return;
            }
            AnonymousClass0Zu r103 = (AnonymousClass0Zu) r10;
            synchronized (r103.A01) {
                ArrayList arrayList = new ArrayList();
                for (String str : list) {
                    if (r103.A02(str)) {
                        C06390Tk.A00().A02(AnonymousClass0Zu.A03, String.format("Constraints met for %s", str), new Throwable[0]);
                        arrayList.add(str);
                    }
                }
                AbstractC12450hw r02 = r103.A00;
                if (r02 != null) {
                    r02.AM8(arrayList);
                }
            }
        }
    }
}
