package X;

import android.content.Context;
import com.whatsapp.util.Log;

/* renamed from: X.5gu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120885gu extends C120895gv {
    public final /* synthetic */ C120465gE A00;
    public final /* synthetic */ C50952Rz A01;
    public final /* synthetic */ C125875rx A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120885gu(Context context, C14900mE r8, C18650sn r9, C64513Fv r10, C120465gE r11, C50952Rz r12, C125875rx r13) {
        super(context, r8, r9, r10, "pay-precheck");
        this.A00 = r11;
        this.A02 = r13;
        this.A01 = r12;
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A02(C452120p r8) {
        super.A02(r8);
        A05(r8);
        this.A02.A00.A3T(null, null, r8, null, null, false);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A03(C452120p r8) {
        super.A03(r8);
        A05(r8);
        this.A02.A00.A3T(null, null, r8, null, null, false);
    }

    @Override // X.C120895gv, X.AbstractC451020e
    public void A04(AnonymousClass1V8 r20) {
        AbstractActivityC121525iS r2;
        C50952Rz r22;
        C119705ey r11;
        C50952Rz r23;
        super.A04(r20);
        try {
            AnonymousClass1V8 A0E = r20.A0E("account");
            if (A0E == null) {
                Log.e("PAY: IndiaUpiPayPrecheckAction sendPrecheck: empty account node");
                A05(null);
                this.A02.A00.A3T(null, null, C117305Zk.A0L(), null, null, false);
                return;
            }
            C119715ez r9 = new C119715ez();
            C120465gE r6 = this.A00;
            AnonymousClass102 r112 = r6.A04;
            r9.A01(r112, A0E, 8);
            AnonymousClass1V8 A0E2 = A0E.A0E("transaction");
            AnonymousClass1V8 A0E3 = A0E.A0E("upi");
            AnonymousClass1V8 A0E4 = A0E.A0E("account");
            C452120p A00 = C452120p.A00(A0E);
            AnonymousClass1V8 A0E5 = A0E.A0E("offer_eligibility");
            if (A0E2 != null && A0E3 != null) {
                String A0H = A0E2.A0H("id");
                String A0H2 = A0E3.A0H("token");
                if (!(A0E5 == null || (r23 = this.A01) == null)) {
                    r6.A0A.A08(null, A0E5, r23.A01);
                }
                r6.A0B.A06("in_pay_precheck_tag", 2);
                this.A02.A00.A3T(null, null, null, A0H, A0H2, C12960it.A1W(A0E5));
            } else if (r9.A0E() != null) {
                C119705ey r12 = new C119705ey();
                if (A0E4 != null) {
                    r12.A01(r112, A0E4, 0);
                }
                if ("sender".equals(r9.A0E())) {
                    r11 = new C119705ey();
                    C15570nT r0 = r6.A02;
                    r0.A08();
                    ((AnonymousClass1ZO) r11).A05 = r0.A05;
                    AnonymousClass1ZR A05 = r9.A05();
                    r11.A02 = A05;
                    r11.A03 = r9.A0D();
                    r6.A08.A08(null, 2);
                    if (A05.A00 != null) {
                        r6.A06.A0F(A05, r12.A03);
                        r12 = null;
                        r6.A0B.A06("in_pay_precheck_tag", 2);
                        this.A02.A00.A3T(r11, r12, null, null, null, false);
                    }
                } else if (((AnonymousClass1ZO) r12).A05 != null) {
                    if (r12.A04) {
                        r12.A02 = null;
                        r12.A03 = null;
                    }
                    C38051nR A002 = r6.A09.A00();
                    C12990iw.A1N(new C61322zq(A002, r12), A002.A03);
                    r11 = null;
                    r6.A0B.A06("in_pay_precheck_tag", 2);
                    this.A02.A00.A3T(r11, r12, null, null, null, false);
                }
                r11 = null;
                r12 = null;
                r6.A0B.A06("in_pay_precheck_tag", 2);
                this.A02.A00.A3T(r11, r12, null, null, null, false);
            } else {
                if (A00 != null) {
                    if (A00.A00 == 2896004) {
                        r6.A0D.Ab2(new Runnable() { // from class: X.6FS
                            @Override // java.lang.Runnable
                            public final void run() {
                                C120885gu.this.A00.A0A.A06(null, true);
                            }
                        });
                    }
                    if (!(A0E5 == null || (r22 = this.A01) == null)) {
                        r6.A0A.A08(null, A0E5, r22.A01);
                    }
                    A05(A00);
                    r2 = this.A02.A00;
                } else {
                    A05(null);
                    C125875rx r02 = this.A02;
                    A00 = C117305Zk.A0L();
                    r2 = r02.A00;
                }
                r2.A3T(null, null, A00, null, null, false);
            }
        } catch (AnonymousClass1V9 unused) {
            A05(null);
            this.A02.A00.A3T(null, null, C117305Zk.A0L(), null, null, false);
        }
    }

    public final void A05(C452120p r3) {
        this.A00.A0B.A05(r3, "in_pay_precheck_tag");
    }
}
