package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.1c5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C32391c5 {
    public final long A00;
    public final Jid A01;
    public final Jid A02;
    public final String A03;
    public final String A04;
    public final String A05;

    public C32391c5(Jid jid, Jid jid2, String str, String str2, String str3, long j) {
        this.A04 = str;
        this.A02 = jid;
        this.A01 = jid2;
        this.A05 = str2;
        this.A03 = str3;
        this.A00 = j;
    }
}
