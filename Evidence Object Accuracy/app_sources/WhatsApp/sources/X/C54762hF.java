package X;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.stickers.StickerStoreFeaturedTabFragment;
import com.whatsapp.stickers.StickerStoreTabFragment;

/* renamed from: X.2hF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54762hF extends AbstractC05270Ox {
    public final /* synthetic */ StickerStoreFeaturedTabFragment A00;

    public C54762hF(StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment) {
        this.A00 = stickerStoreFeaturedTabFragment;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        View view;
        C73893gy r1;
        StickerStoreFeaturedTabFragment stickerStoreFeaturedTabFragment = this.A00;
        int A19 = ((StickerStoreTabFragment) stickerStoreFeaturedTabFragment).A03.A19();
        int top = stickerStoreFeaturedTabFragment.A02.getTop();
        if (A19 == 0) {
            if (top != stickerStoreFeaturedTabFragment.A02.getHeight()) {
                view = stickerStoreFeaturedTabFragment.A02;
                r1 = new C73893gy(stickerStoreFeaturedTabFragment.A02, view.getHeight());
            } else {
                return;
            }
        } else if (top != 0 && stickerStoreFeaturedTabFragment.A02.getAnimation() == null) {
            view = stickerStoreFeaturedTabFragment.A02;
            r1 = new C73893gy(view, 0);
        } else {
            return;
        }
        view.startAnimation(r1);
    }
}
