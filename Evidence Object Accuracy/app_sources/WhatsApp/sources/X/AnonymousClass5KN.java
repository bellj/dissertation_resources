package X;

/* renamed from: X.5KN  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5KN extends AnonymousClass1WI implements AnonymousClass5ZQ {
    public AnonymousClass5KN() {
        super(2);
    }

    @Override // X.AnonymousClass5ZQ
    public /* bridge */ /* synthetic */ Object AJ5(Object obj, Object obj2) {
        boolean z = true;
        if (!C12970iu.A1Y(obj)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }
}
