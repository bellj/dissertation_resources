package X;

/* renamed from: X.0XL  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0XL implements AbstractC12280hf {
    public boolean A00;
    public final /* synthetic */ AnonymousClass08Z A01;

    public AnonymousClass0XL(AnonymousClass08Z r1) {
        this.A01 = r1;
    }

    @Override // X.AbstractC12280hf
    public void AOF(AnonymousClass07H r3, boolean z) {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass08Z r1 = this.A01;
            r1.A01.A93();
            r1.A00.onPanelClosed(C43951xu.A03, r3);
            this.A00 = false;
        }
    }

    @Override // X.AbstractC12280hf
    public boolean ATF(AnonymousClass07H r3) {
        this.A01.A00.onMenuOpened(C43951xu.A03, r3);
        return true;
    }
}
