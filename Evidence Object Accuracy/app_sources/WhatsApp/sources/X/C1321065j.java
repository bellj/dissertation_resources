package X;

/* renamed from: X.65j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1321065j implements AnonymousClass02B {
    public boolean A00 = false;
    public final /* synthetic */ AnonymousClass02O A01;
    public final /* synthetic */ AnonymousClass02P A02;

    public C1321065j(AnonymousClass02O r2, AnonymousClass02P r3) {
        this.A01 = r2;
        this.A02 = r3;
    }

    @Override // X.AnonymousClass02B
    public void ANq(Object obj) {
        if (this.A00 || C12970iu.A1Y(this.A01.apply(obj))) {
            this.A00 = true;
            this.A02.A0B(obj);
        }
    }
}
