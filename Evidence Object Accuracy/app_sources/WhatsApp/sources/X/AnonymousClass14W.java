package X;

/* renamed from: X.14W  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass14W {
    public final C15450nH A00;
    public final C15550nR A01;
    public final C22700zV A02;
    public final C15610nY A03;
    public final C14830m7 A04;
    public final C14850m9 A05;
    public final AnonymousClass11G A06;
    public final C22140ya A07;

    public AnonymousClass14W(C15450nH r1, C15550nR r2, C22700zV r3, C15610nY r4, C14830m7 r5, C14850m9 r6, AnonymousClass11G r7, C22140ya r8) {
        this.A04 = r5;
        this.A05 = r6;
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A06 = r7;
        this.A02 = r3;
        this.A07 = r8;
    }
}
