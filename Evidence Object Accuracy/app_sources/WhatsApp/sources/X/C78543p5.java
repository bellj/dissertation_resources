package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3p5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78543p5 extends AnonymousClass1U5 {
    public static final Parcelable.Creator CREATOR = new C98814jI();
    public final int A00;
    public final C78633pE A01;
    public final String A02;

    public C78543p5(C78633pE r2, String str) {
        this.A00 = 1;
        this.A02 = str;
        this.A01 = r2;
    }

    public C78543p5(C78633pE r1, String str, int i) {
        this.A00 = i;
        this.A02 = str;
        this.A01 = r1;
    }

    @Override // android.os.Parcelable
    public final void writeToParcel(Parcel parcel, int i) {
        int A00 = C95654e8.A00(parcel);
        C95654e8.A07(parcel, 1, this.A00);
        C95654e8.A0B(parcel, this.A01, 3, i, C95654e8.A0K(parcel, this.A02));
        C95654e8.A06(parcel, A00);
    }
}
