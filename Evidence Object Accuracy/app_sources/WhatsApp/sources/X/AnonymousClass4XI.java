package X;

import java.io.File;

/* renamed from: X.4XI  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4XI {
    public final long A00;
    public final long A01;
    public final File A02;
    public final String A03;

    public AnonymousClass4XI(File file) {
        this.A02 = file;
        this.A00 = file.lastModified();
        this.A01 = file.length();
        this.A03 = file.getName();
    }

    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass4XI) && this.A02.equals(((AnonymousClass4XI) obj).A02);
    }

    public int hashCode() {
        return C72453ed.A0D(this.A02);
    }
}
