package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.Date;

/* renamed from: X.0oL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16040oL {
    public int A00;
    public C29331Rt A01;
    public final C14830m7 A02;
    public final AnonymousClass1RO A03;

    public C16040oL(C14830m7 r1, AnonymousClass1RO r2) {
        this.A02 = r1;
        this.A03 = r2;
    }

    public int A00() {
        C16310on A01 = this.A03.get();
        try {
            String valueOf = String.valueOf(0);
            Cursor A08 = A01.A03.A08("identities", "recipient_id=? AND recipient_type = ? AND device_id=?", null, null, new String[]{"next_prekey_id"}, new String[]{String.valueOf(-1), valueOf, valueOf});
            if (A08.moveToNext()) {
                int i = A08.getInt(0);
                A08.close();
                A01.close();
                return i;
            }
            throw new SQLiteException("Missing entry for self in identities table");
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public int A01() {
        if (this.A00 == 0) {
            C16310on A01 = this.A03.get();
            try {
                String valueOf = String.valueOf(0);
                Cursor A08 = A01.A03.A08("identities", "recipient_id=? AND recipient_type = ? AND device_id=?", null, null, new String[]{"registration_id"}, new String[]{String.valueOf(-1), valueOf, valueOf});
                if (A08.moveToNext()) {
                    this.A00 = A08.getInt(0);
                    A08.close();
                    A01.close();
                } else {
                    throw new SQLiteException("Missing entry for self in identities table");
                }
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return this.A00;
    }

    public C29331Rt A02() {
        if (this.A01 == null) {
            C16310on A01 = this.A03.get();
            try {
                String valueOf = String.valueOf(0);
                Cursor A08 = A01.A03.A08("identities", "recipient_id=? AND recipient_type = ? AND device_id=?", null, null, new String[]{"public_key", "private_key"}, new String[]{String.valueOf(-1), valueOf, valueOf});
                if (A08.moveToNext()) {
                    this.A01 = new C29331Rt(A08.getBlob(0), A08.getBlob(1));
                    A08.close();
                    A01.close();
                } else {
                    throw new SQLiteException("Missing entry for self in identities table");
                }
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
        return this.A01;
    }

    public boolean A03(C15950oC r8) {
        C16310on A02 = this.A03.A02();
        try {
            long A01 = (long) A02.A03.A01("identities", "recipient_id=? AND recipient_type = ? AND device_id=?", r8.A00());
            StringBuilder sb = new StringBuilder();
            sb.append("axolotl deleted ");
            sb.append(A01);
            sb.append(" identities for ");
            sb.append(r8);
            Log.i(sb.toString());
            boolean z = false;
            if (A01 > 0) {
                z = true;
            }
            A02.close();
            return z;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public byte[] A04(C15950oC r13) {
        C16310on A01 = this.A03.get();
        try {
            Cursor A08 = A01.A03.A08("identities", "recipient_id=? AND recipient_type = ? AND device_id=?", null, null, new String[]{"public_key", "timestamp"}, r13.A00());
            if (!A08.moveToNext()) {
                StringBuilder sb = new StringBuilder();
                sb.append("axolotl found no identity entry for ");
                sb.append(r13);
                Log.i(sb.toString());
                A08.close();
                A01.close();
                return null;
            }
            byte[] blob = A08.getBlob(0);
            Date date = new Date(A08.getLong(1) * 1000);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("axolotl found an identity entry for ");
            sb2.append(r13);
            sb2.append(" dated ");
            sb2.append(date);
            Log.i(sb2.toString());
            A08.close();
            A01.close();
            return blob;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
