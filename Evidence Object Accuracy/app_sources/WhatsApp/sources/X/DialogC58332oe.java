package X;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.facebook.redex.RunnableBRunnable0Shape11S0200000_I1_1;
import com.whatsapp.KeyboardPopupLayout;
import com.whatsapp.R;
import com.whatsapp.WaEditText;
import com.whatsapp.emoji.search.EmojiSearchContainer;
import java.util.ArrayList;

/* renamed from: X.2oe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class DialogC58332oe extends AnonymousClass27U {
    public int A00;
    public WaEditText A01;
    public C15270mq A02;
    public CharSequence A03;
    public boolean A04 = true;
    public boolean A05;
    public boolean A06 = true;
    public final int A07;
    public final int A08;
    public final int A09;
    public final int A0A;
    public final int A0B;
    public final int A0C;
    public final AbstractC15710nm A0D;
    public final AbstractC116455Vm A0E = new AnonymousClass3UE(this);
    public final C14900mE A0F;
    public final AnonymousClass01d A0G;
    public final C14820m6 A0H;
    public final AnonymousClass5U9 A0I;
    public final AnonymousClass19M A0J;
    public final C231510o A0K;
    public final AnonymousClass193 A0L;
    public final C14850m9 A0M;
    public final C16630pM A0N;
    public final C252718t A0O;
    public final String A0P;

    public DialogC58332oe(Activity activity, AbstractC15710nm r9, C14900mE r10, AnonymousClass01d r11, C14830m7 r12, C14820m6 r13, AnonymousClass018 r14, AnonymousClass5U9 r15, AnonymousClass19M r16, C231510o r17, AnonymousClass193 r18, C14850m9 r19, C16630pM r20, C252718t r21, String str, int i, int i2, int i3, int i4, int i5, int i6) {
        super(activity, r11, r12, r14, R.layout.emoji_edittext_dialog);
        this.A0M = r19;
        this.A0F = r10;
        this.A0O = r21;
        this.A0D = r9;
        this.A0J = r16;
        this.A0K = r17;
        this.A0G = r11;
        this.A0L = r18;
        this.A0H = r13;
        this.A0N = r20;
        this.A07 = i;
        this.A0A = i6;
        this.A0I = r15;
        this.A0C = i2;
        this.A0B = i3;
        this.A09 = i4;
        this.A08 = i5;
        this.A0P = str;
    }

    @Override // android.app.Dialog
    public void onBackPressed() {
        if (this.A02.isShowing()) {
            this.A02.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    @Override // X.AnonymousClass27U, android.app.Dialog
    public void onCreate(Bundle bundle) {
        Window window;
        super.onCreate(bundle);
        int i = this.A0C;
        ((TextView) findViewById(R.id.dialog_title)).setText(i);
        setTitle(i);
        Button button = (Button) findViewById(R.id.ok_btn);
        C12960it.A0x(button, this, 9);
        C12960it.A0x(findViewById(R.id.cancel_btn), this, 10);
        ArrayList A0l = C12960it.A0l();
        TextView textView = (TextView) findViewById(R.id.counter_tv);
        WaEditText waEditText = (WaEditText) findViewById(R.id.edit_text);
        this.A01 = waEditText;
        AnonymousClass018 r13 = super.A04;
        C42941w9.A0C(waEditText, r13);
        int i2 = this.A0B;
        if (i2 > 0) {
            if (this.A00 == 0) {
                textView.setVisibility(0);
            }
            A0l.add(new C100654mG(i2));
        }
        if (!this.A06) {
            A0l.add(new C100634mE());
        }
        if (!A0l.isEmpty()) {
            this.A01.setFilters((InputFilter[]) A0l.toArray(new InputFilter[0]));
        }
        WaEditText waEditText2 = this.A01;
        AnonymousClass19M r14 = this.A0J;
        AnonymousClass01d r11 = this.A0G;
        C16630pM r1 = this.A0N;
        waEditText2.addTextChangedListener(new AnonymousClass367(waEditText2, textView, r11, r13, r14, r1, i2, this.A00, this.A05));
        if (!this.A04) {
            this.A01.addTextChangedListener(new AnonymousClass47N(button, this));
        }
        this.A01.setInputType(this.A0A);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.width = -1;
        attributes.gravity = 48;
        getWindow().setAttributes(attributes);
        if (Build.VERSION.SDK_INT >= 21 && (window = getWindow()) != null) {
            window.addFlags(Integer.MIN_VALUE);
            window.clearFlags(67108864);
            window.setStatusBarColor(AnonymousClass00T.A00(getContext(), R.color.primary));
        }
        ImageButton imageButton = (ImageButton) findViewById(R.id.emoji_btn);
        Activity activity = super.A01;
        C252718t r4 = this.A0O;
        AbstractC15710nm r8 = this.A0D;
        C231510o r15 = this.A0K;
        this.A02 = new C15270mq(activity, imageButton, r8, (KeyboardPopupLayout) findViewById(R.id.emoji_edit_text_layout), this.A01, r11, this.A0H, r13, r14, r15, this.A0L, r1, r4);
        C15330mx r5 = new C15330mx(activity, r13, r14, this.A02, r15, (EmojiSearchContainer) findViewById(R.id.emoji_search_container), r1);
        r5.A00 = new AbstractC14020ki() { // from class: X.56h
            @Override // X.AbstractC14020ki
            public final void APd(C37471mS r3) {
                DialogC58332oe.this.A0E.APc(r3.A00);
            }
        };
        C15270mq r42 = this.A02;
        r42.A0C(this.A0E);
        r42.A0E = new RunnableBRunnable0Shape11S0200000_I1_1(this, 25, r5);
        setOnCancelListener(new DialogInterface.OnCancelListener() { // from class: X.4f5
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                DialogC58332oe r0 = DialogC58332oe.this;
                C36021jC.A00(((AnonymousClass27U) r0).A01, r0.A07);
            }
        });
        TextView textView2 = (TextView) findViewById(R.id.dialog_subtitle);
        if (TextUtils.isEmpty(null)) {
            textView2.setVisibility(8);
        } else {
            textView2.setVisibility(0);
            textView2.setText((CharSequence) null);
        }
        TextView textView3 = (TextView) findViewById(R.id.dialog_footer);
        if (TextUtils.isEmpty(this.A03)) {
            textView3.setVisibility(8);
        } else {
            textView3.setVisibility(0);
            textView3.setText(this.A03);
        }
        int i3 = this.A09;
        if (i3 != 0) {
            this.A01.setHint(getContext().getString(i3));
        }
        WaEditText waEditText3 = this.A01;
        String str = this.A0P;
        waEditText3.setText(AbstractC36671kL.A05(activity, r14, str));
        if (!TextUtils.isEmpty(str)) {
            this.A01.selectAll();
        }
        this.A01.A04(false);
        getWindow().setSoftInputMode(5);
    }
}
