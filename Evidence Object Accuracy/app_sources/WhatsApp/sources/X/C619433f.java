package X;

import android.os.ParcelFileDescriptor;
import android.util.JsonReader;
import com.whatsapp.util.Log;

/* renamed from: X.33f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C619433f extends AnonymousClass5BB {
    public final ParcelFileDescriptor A00;
    public final C15740np A01;

    public C619433f(ParcelFileDescriptor parcelFileDescriptor, JsonReader jsonReader, C15740np r3) {
        super(jsonReader);
        this.A00 = parcelFileDescriptor;
        this.A01 = r3;
    }

    @Override // X.AnonymousClass5BB
    public /* bridge */ /* synthetic */ Object A01(JsonReader jsonReader) {
        jsonReader.beginObject();
        long j = -1;
        String str = null;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("path")) {
                str = jsonReader.nextString();
            } else if (!nextName.equals("size")) {
                Log.e("GoogleMigrateFileData/parseFileDataObject/field not recognized");
                jsonReader.skipValue();
            } else {
                j = jsonReader.nextLong();
            }
        }
        jsonReader.endObject();
        if (str != null) {
            return new AnonymousClass3F4(this.A01.A02(str), j);
        }
        Log.e("GoogleMigrateFileData/parseFileDataObject/file path is null or empty");
        return null;
    }

    @Override // X.AnonymousClass5BB
    public boolean A03(JsonReader jsonReader) {
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if ("files".equals(jsonReader.nextName())) {
                jsonReader.beginArray();
                return true;
            }
            jsonReader.skipValue();
        }
        return false;
    }

    @Override // X.AnonymousClass5BB, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        super.close();
        this.A00.close();
    }
}
