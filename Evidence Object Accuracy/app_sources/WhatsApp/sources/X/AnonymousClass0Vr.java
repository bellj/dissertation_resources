package X;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/* renamed from: X.0Vr  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Vr implements TextWatcher {
    public EditText A00;
    public C14260l7 A01;
    public AnonymousClass28D A02;
    public AbstractC14200l1 A03;
    public boolean A04 = false;

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        if (!this.A04) {
            this.A04 = true;
            if (!(this.A02 == null || this.A00 == null || this.A03 == null || this.A01 == null)) {
                String obj = editable.toString();
                AnonymousClass28D r3 = this.A02;
                AbstractC14200l1 r2 = this.A03;
                C14210l2 r0 = new C14210l2();
                r0.A04(obj, 0);
                String str = (String) C28701Oq.A01(this.A01, r3, r0.A03(), r2);
                if (!obj.equals(str)) {
                    editable.replace(0, editable.length(), str);
                    this.A00.setSelection(editable.length());
                }
            }
            this.A04 = false;
        }
    }
}
