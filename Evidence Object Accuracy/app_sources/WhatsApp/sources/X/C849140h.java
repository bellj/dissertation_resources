package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.40h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C849140h extends C92194Ux {
    public final int A00;
    public final C15370n3 A01;
    public final UserJid A02;
    public final boolean A03;
    public final boolean A04;

    public C849140h(C15370n3 r2, UserJid userJid, int i, boolean z, boolean z2) {
        super(1);
        this.A03 = z;
        this.A02 = userJid;
        this.A04 = z2;
        this.A01 = r2;
        this.A00 = i;
    }

    @Override // X.C92194Ux
    public boolean A00(C92194Ux r6) {
        if (!super.A00(r6) || !(r6 instanceof C849140h)) {
            return false;
        }
        C849140h r62 = (C849140h) r6;
        if (!this.A02.equals(r62.A02) || this.A04 != r62.A04) {
            return false;
        }
        int i = this.A00;
        int i2 = r62.A00;
        if (i == 1) {
            if (i2 != 1) {
                return false;
            }
        } else if (i == 11) {
            if (i2 != 11) {
                return false;
            }
        } else if (i2 == 11 || i2 == 1) {
            return false;
        }
        return true;
    }
}
