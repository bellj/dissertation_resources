package X;

/* renamed from: X.5FP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FP implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FP(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r7) {
        int[] iArr = (int[]) obj;
        appendable.append('[');
        boolean z = false;
        for (int i : iArr) {
            z = C72453ed.A1U(appendable, z);
            appendable.append(Integer.toString(i));
        }
        appendable.append(']');
    }
}
