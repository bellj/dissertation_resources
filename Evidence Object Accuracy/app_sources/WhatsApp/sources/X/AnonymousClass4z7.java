package X;

import android.content.Context;

/* renamed from: X.4z7  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4z7 implements AbstractC116365Vd {
    @Override // X.AbstractC116365Vd
    public final int Agj(Context context, String str) {
        return C95564dy.A00(context, str);
    }

    @Override // X.AbstractC116365Vd
    public final int Ah1(Context context, String str, boolean z) {
        return C95564dy.A01(context, str, true);
    }
}
