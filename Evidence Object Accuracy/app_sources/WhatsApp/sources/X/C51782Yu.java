package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.whatsapp.util.Log;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;
import com.whatsapp.voipcalling.VoipActivityV2;

/* renamed from: X.2Yu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51782Yu extends BroadcastReceiver {
    public final /* synthetic */ C29631Ua A00;

    public C51782Yu(C29631Ua r1) {
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        CallInfo callInfo;
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction()) && (callInfo = Voip.getCallInfo()) != null && callInfo.callState == Voip.CallState.RECEIVED_CALL && Build.VERSION.SDK_INT >= 21) {
            C29631Ua r2 = this.A00;
            AnonymousClass1L4 r0 = r2.A0c;
            if (r0 == null || !((VoipActivityV2) r0).A1k) {
                Log.i("voip/unlockReceiver generate headsup notification when user unlock the screen in RECEIVED_CALL state");
                r2.A0l(callInfo, 1, false);
            }
        }
    }
}
