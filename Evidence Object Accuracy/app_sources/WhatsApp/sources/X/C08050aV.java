package X;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0aV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08050aV implements AbstractC12030hG, AbstractC12860ig, AbstractC12870ih {
    public AnonymousClass0QR A00;
    public final Paint A01 = new C020609t(1);
    public final Path A02;
    public final AnonymousClass0AA A03;
    public final AnonymousClass0QR A04;
    public final AnonymousClass0QR A05;
    public final AbstractC08070aX A06;
    public final String A07;
    public final List A08 = new ArrayList();
    public final boolean A09;

    public C08050aV(AnonymousClass0AA r5, C08250ap r6, AbstractC08070aX r7) {
        AnonymousClass0HA r2;
        Path path = new Path();
        this.A02 = path;
        this.A06 = r7;
        this.A07 = r6.A03;
        this.A09 = r6.A05;
        this.A03 = r5;
        AnonymousClass0H4 r1 = r6.A01;
        if (r1 == null || (r2 = r6.A02) == null) {
            this.A04 = null;
            this.A05 = null;
            return;
        }
        path.setFillType(r6.A00);
        C03230Gz r12 = new C03230Gz(r1.A00);
        this.A04 = r12;
        r12.A07.add(this);
        r7.A03(r12);
        AnonymousClass0H0 r13 = new AnonymousClass0H0(r2.A00);
        this.A05 = r13;
        r13.A07.add(this);
        r7.A03(r13);
    }

    @Override // X.AbstractC12480hz
    public void A5q(AnonymousClass0SF r3, Object obj) {
        AnonymousClass0QR r0;
        if (obj == AbstractC12810iX.A0R) {
            r0 = this.A04;
        } else if (obj == AbstractC12810iX.A0S) {
            r0 = this.A05;
        } else if (obj == AbstractC12810iX.A00) {
            AnonymousClass0QR r1 = this.A00;
            if (r1 != null) {
                this.A06.A0O.remove(r1);
            }
            if (r3 == null) {
                this.A00 = null;
                return;
            }
            AnonymousClass0Gu r02 = new AnonymousClass0Gu(r3, null);
            this.A00 = r02;
            r02.A07.add(this);
            this.A06.A03(this.A00);
            return;
        } else {
            return;
        }
        r0.A08(r3);
    }

    @Override // X.AbstractC12860ig
    public void A9C(Canvas canvas, Matrix matrix, int i) {
        if (!this.A09) {
            Paint paint = this.A01;
            C03230Gz r2 = (C03230Gz) this.A04;
            AnonymousClass0U8 AC6 = r2.A06.AC6();
            AnonymousClass0MI.A00();
            paint.setColor(r2.A09(AC6, r2.A01()));
            int i2 = 0;
            paint.setAlpha(Math.max(0, Math.min(255, (int) ((((((float) i) / 255.0f) * ((float) ((Number) this.A05.A03()).intValue())) / 100.0f) * 255.0f))));
            AnonymousClass0QR r0 = this.A00;
            if (r0 != null) {
                paint.setColorFilter((ColorFilter) r0.A03());
            }
            Path path = this.A02;
            path.reset();
            while (true) {
                List list = this.A08;
                if (i2 < list.size()) {
                    path.addPath(((AbstractC12850if) list.get(i2)).AF0(), matrix);
                    i2++;
                } else {
                    canvas.drawPath(path, paint);
                    AnonymousClass0MI.A00();
                    return;
                }
            }
        }
    }

    @Override // X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        Path path = this.A02;
        path.reset();
        int i = 0;
        while (true) {
            List list = this.A08;
            if (i < list.size()) {
                path.addPath(((AbstractC12850if) list.get(i)).AF0(), matrix);
                i++;
            } else {
                path.computeBounds(rectF, false);
                rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
                return;
            }
        }
    }

    @Override // X.AbstractC12030hG
    public void AYB() {
        this.A03.invalidateSelf();
    }

    @Override // X.AbstractC12480hz
    public void Aan(C06430To r1, C06430To r2, List list, int i) {
        AnonymousClass0U0.A01(this, r1, r2, list, i);
    }

    @Override // X.AbstractC12470hy
    public void Aby(List list, List list2) {
        for (int i = 0; i < list2.size(); i++) {
            Object obj = list2.get(i);
            if (obj instanceof AbstractC12850if) {
                this.A08.add(obj);
            }
        }
    }

    @Override // X.AbstractC12470hy
    public String getName() {
        return this.A07;
    }
}
