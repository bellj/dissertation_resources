package X;

import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListFragment;
import com.whatsapp.blockbusiness.blockreasonlist.BlockReasonListViewModel;

/* renamed from: X.3de  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71873de extends AnonymousClass1WI implements AnonymousClass1WK {
    public final /* synthetic */ BlockReasonListFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C71873de(BlockReasonListFragment blockReasonListFragment) {
        super(0);
        this.this$0 = blockReasonListFragment;
    }

    @Override // X.AnonymousClass1WK
    public /* bridge */ /* synthetic */ Object AJ3() {
        return C13000ix.A02(this.this$0).A00(BlockReasonListViewModel.class);
    }
}
