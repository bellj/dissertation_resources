package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;

/* renamed from: X.5la  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C122515la extends AbstractC118835cS {
    public final TextView A00;

    public C122515la(View view) {
        super(view);
        TextView A0I = C12960it.A0I(view, R.id.title);
        this.A00 = A0I;
        C27531Hw.A06(A0I);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r3, int i) {
        this.A00.setText(((C123065mY) r3).A00);
    }
}
