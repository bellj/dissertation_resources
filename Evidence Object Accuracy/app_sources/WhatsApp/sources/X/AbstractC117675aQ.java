package X;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.TransactionsExpandableView;
import java.util.List;

/* renamed from: X.5aQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC117675aQ extends LinearLayout {
    public int A00 = 2;
    public View.OnClickListener A01;
    public ViewGroup A02;
    public FrameLayout A03;
    public ImageView A04;
    public LinearLayout A05;
    public TextView A06;
    public TextView A07;
    public CharSequence A08;
    public CharSequence A09;
    public final List A0A = C12960it.A0l();

    public AbstractC117675aQ(Context context) {
        super(context);
        A00(context, null);
    }

    public AbstractC117675aQ(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A00(context, attributeSet);
    }

    public AbstractC117675aQ(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A00(context, attributeSet);
    }

    private void A00(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate(R.layout.payment_expandable_listview, (ViewGroup) this, true);
        setOrientation(1);
        this.A07 = C12960it.A0I(this, R.id.header);
        this.A02 = C117315Zl.A04(this, R.id.see_more_container);
        this.A04 = C12970iu.A0K(this, R.id.see_more_icon);
        this.A06 = C12960it.A0I(this, R.id.see_more_text);
        this.A03 = (FrameLayout) AnonymousClass028.A0D(this, R.id.custom_empty_view_container);
        this.A05 = C117305Zk.A07(this, R.id.list_item_container);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C125375r9.A03);
            try {
                this.A00 = obtainStyledAttributes.getInt(2, 2);
                int resourceId = obtainStyledAttributes.getResourceId(0, 0);
                if (resourceId > 0) {
                    this.A04.setImageDrawable(context.getResources().getDrawable(resourceId));
                }
                int resourceId2 = obtainStyledAttributes.getResourceId(1, 0);
                if (resourceId2 > 0) {
                    this.A06.setText(resourceId2);
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }

    public void A01(List list) {
        ViewGroup viewGroup;
        View.OnClickListener onClickListener;
        List list2 = this.A0A;
        list2.clear();
        this.A05.removeAllViews();
        boolean isEmpty = list.isEmpty();
        FrameLayout frameLayout = this.A03;
        if (!isEmpty) {
            frameLayout.setVisibility(8);
            int size = list.size();
            int i = this.A00;
            ViewGroup viewGroup2 = this.A02;
            if (size > i) {
                viewGroup2.setVisibility(0);
                this.A06.setText(this.A09);
                viewGroup = this.A02;
                onClickListener = this.A01;
                viewGroup.setOnClickListener(onClickListener);
            } else {
                viewGroup2.setVisibility(8);
            }
        } else if (frameLayout.getChildCount() > 0) {
            this.A03.setVisibility(0);
            this.A02.setVisibility(8);
        } else {
            this.A02.setVisibility(0);
            this.A06.setText(this.A08);
            viewGroup = this.A02;
            onClickListener = null;
            viewGroup.setOnClickListener(onClickListener);
        }
        list2.addAll(list.subList(0, Math.min(list.size(), this.A00)));
        for (int i2 = 0; i2 < list2.size(); i2++) {
            TransactionsExpandableView transactionsExpandableView = (TransactionsExpandableView) this;
            View A00 = transactionsExpandableView.A00.A00(transactionsExpandableView, (AnonymousClass1IR) list.get(i2));
            Object obj = list.get(i2);
            int size2 = list2.size();
            ((AbstractC136376Mh) A00).A6W(obj);
            int i3 = size2 - 1;
            View findViewById = A00.findViewById(R.id.divider);
            int i4 = 8;
            if (i2 < i3) {
                i4 = 0;
            }
            findViewById.setVisibility(i4);
            this.A05.addView(A00);
        }
    }

    public ImageView getSeeMoreImageView() {
        return this.A04;
    }

    public int getSizeLimit() {
        return this.A00;
    }

    public void setCustomEmptyView(View view) {
        this.A03.addView(view);
    }

    public void setSeeMoreView(CharSequence charSequence, CharSequence charSequence2, View.OnClickListener onClickListener) {
        this.A09 = charSequence;
        this.A08 = charSequence2;
        this.A01 = onClickListener;
    }

    public void setTitle(CharSequence charSequence) {
        this.A07.setText(charSequence);
        this.A07.setVisibility(0);
    }
}
