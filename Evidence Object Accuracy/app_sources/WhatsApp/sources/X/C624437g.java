package X;

import android.content.Context;
import android.graphics.Bitmap;
import java.lang.ref.WeakReference;

/* renamed from: X.37g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C624437g extends AbstractC16350or {
    public final Bitmap A00;
    public final C14330lG A01;
    public final AbstractC116305Ux A02;
    public final WeakReference A03;

    public C624437g(Context context, Bitmap bitmap, C14330lG r4, AbstractC116305Ux r5) {
        this.A03 = C12970iu.A10(context);
        this.A00 = bitmap;
        this.A01 = r4;
        this.A02 = r5;
    }
}
