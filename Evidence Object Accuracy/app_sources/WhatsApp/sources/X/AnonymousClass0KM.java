package X;

import android.content.Context;
import android.os.Build;

/* renamed from: X.0KM  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0KM {
    public static boolean A00(Context context) {
        return Build.VERSION.SDK_INT >= 23 && context != null && context.getPackageManager() != null && AnonymousClass0KL.A00(context.getPackageManager());
    }
}
