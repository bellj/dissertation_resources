package X;

/* renamed from: X.1xw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43971xw {
    public final C006202y A00 = new C006202y(1000);
    public final C006202y A01 = new C006202y(1000);

    public void A00(AnonymousClass1YT r4) {
        C006202y r2 = this.A01;
        synchronized (r2) {
            r2.A08(Long.valueOf(r4.A02()), r4);
        }
        C006202y r1 = this.A00;
        synchronized (r1) {
            r1.A08(r4.A0B, r4);
        }
    }

    public void A01(AnonymousClass1YT r4) {
        C006202y r2 = this.A01;
        synchronized (r2) {
            r2.A07(Long.valueOf(r4.A02()));
        }
        C006202y r1 = this.A00;
        synchronized (r1) {
            r1.A07(r4.A0B);
        }
    }
}
