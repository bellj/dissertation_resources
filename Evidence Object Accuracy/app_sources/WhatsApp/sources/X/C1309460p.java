package X;

import android.content.Context;
import java.math.BigDecimal;
import java.math.RoundingMode;

/* renamed from: X.60p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309460p {
    public final AnonymousClass6F2 A00;
    public final AnonymousClass6F2 A01;
    public final AnonymousClass603 A02;

    public C1309460p(AnonymousClass6F2 r1, AnonymousClass6F2 r2, AnonymousClass603 r3) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
    }

    public static C1309460p A00(AnonymousClass102 r4, AnonymousClass1V8 r5) {
        AnonymousClass6F2 r2;
        AnonymousClass1V8 A0F = r5.A0F("input");
        AnonymousClass1V8 A0E = r5.A0E("trading");
        AnonymousClass1V8 A0F2 = r5.A0F("quote");
        AnonymousClass6F2 A00 = AnonymousClass6F2.A00(r4, A0F);
        if (A0E != null) {
            r2 = AnonymousClass6F2.A00(r4, A0E);
        } else {
            r2 = null;
        }
        return new C1309460p(A00, r2, AnonymousClass603.A00(r4, A0F2));
    }

    public static final CharSequence A01(Context context, AnonymousClass018 r6, AnonymousClass6F2 r7, AnonymousClass6F2 r8) {
        AbstractC30791Yv r3;
        BigDecimal divide;
        boolean A00;
        C30821Yy r1 = r8.A01;
        if (!r1.A02()) {
            return "";
        }
        AbstractC30791Yv r4 = r8.A00;
        if (r4 instanceof C30801Yw) {
            r3 = r7.A00;
            A00 = ((C30801Yw) r4).A00(r3);
        } else {
            r3 = r7.A00;
            if (r3 instanceof C30801Yw) {
                A00 = ((C30801Yw) r3).A00(r4);
            }
            divide = r7.A01.A00.divide(r1.A00, RoundingMode.HALF_EVEN);
            return C1316563p.A01(context, r6, r3, r4, divide);
        }
        if (A00) {
            divide = BigDecimal.ONE;
            return C1316563p.A01(context, r6, r3, r4, divide);
        }
        divide = r7.A01.A00.divide(r1.A00, RoundingMode.HALF_EVEN);
        return C1316563p.A01(context, r6, r3, r4, divide);
    }
}
