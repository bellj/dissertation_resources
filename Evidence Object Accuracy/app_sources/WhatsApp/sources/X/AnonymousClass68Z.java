package X;

import android.app.Activity;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/* renamed from: X.68Z  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68Z implements AnonymousClass18N {
    public long A00 = -1;
    public final AnonymousClass18R A01;
    public final C14900mE A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final C16590pI A05;
    public final C17220qS A06;
    public final C1308460e A07;
    public final C1329668y A08;
    public final C18650sn A09;
    public final C18600si A0A;
    public final Set A0B = C12970iu.A12();

    public AnonymousClass68Z(AnonymousClass18R r6, C14900mE r7, C18640sm r8, C14830m7 r9, C16590pI r10, C17220qS r11, C1308460e r12, C1329668y r13, C18650sn r14, C18600si r15) {
        this.A05 = r10;
        this.A04 = r9;
        this.A01 = r6;
        this.A02 = r7;
        this.A06 = r11;
        this.A0A = r15;
        this.A07 = r12;
        this.A03 = r8;
        this.A09 = r14;
        this.A08 = r13;
        this.A00 = r15.A01().getLong("payments_block_list_last_sync_time", -1);
        String string = r15.A01().getString("payments_block_list", "");
        if (!TextUtils.isEmpty(string)) {
            for (String str : string.split(";")) {
                this.A0B.add(C117295Zj.A0E(str));
            }
        }
    }

    public synchronized void A00(AnonymousClass1ZR r5, boolean z) {
        StringBuilder A0h = C12960it.A0h();
        A0h.append("PAY: IndiaUpiBlockListManager before block vpa: ");
        A0h.append(r5);
        A0h.append(" blocked: ");
        A0h.append(z);
        C12960it.A1F(A0h);
        if (z) {
            Set<AnonymousClass1ZR> set = this.A0B;
            if (!set.contains(r5)) {
                set.add(r5);
                Log.i(C12960it.A0b("PAY: IndiaUpiBlockListManager add vpa: ", r5));
                C18600si r3 = this.A0A;
                HashSet A12 = C12970iu.A12();
                for (AnonymousClass1ZR r0 : set) {
                    A12.add(r0.A00);
                }
                r3.A0G(TextUtils.join(";", A12));
            }
        } else {
            Set<AnonymousClass1ZR> set2 = this.A0B;
            if (set2.contains(r5)) {
                set2.remove(r5);
                Log.i(C12960it.A0b("PAY: IndiaUpiBlockListManager remove vpa: ", r5));
                C18600si r32 = this.A0A;
                HashSet A122 = C12970iu.A12();
                for (AnonymousClass1ZR r02 : set2) {
                    A122.add(r02.A00);
                }
                r32.A0G(TextUtils.join(";", A122));
            }
        }
    }

    @Override // X.AnonymousClass18N
    public void A9p(AnonymousClass5US r15, C18610sj r16) {
        C120405g8 r5 = new C120405g8(this.A05.A00, this.A02, this.A03, this.A06, this, this.A07, this.A09, r16);
        C126675tG r9 = new C126675tG(this, r15);
        Log.i("PAY: getBlockedVpas called");
        ArrayList A0x = C12980iv.A0x(r5.A04.AAs());
        for (int i = 0; i < A0x.size(); i++) {
            A0x.set(i, C003501n.A02(((String) A0x.get(i)).toLowerCase(Locale.US)));
        }
        Collections.sort(A0x);
        StringBuilder A0h = C12960it.A0h();
        Iterator it = A0x.iterator();
        while (it.hasNext()) {
            A0h.append(C12970iu.A0x(it));
        }
        String A02 = C003501n.A02(A0h.toString());
        C64513Fv r11 = ((C126705tJ) r5).A00;
        if (r11 != null) {
            r11.A04("upi-get-blocked-vpas");
        }
        C17220qS r3 = r5.A03;
        String A01 = r3.A01();
        C117325Zm.A05(r3, new C120655gX(r5.A00, r5.A01, r9, r5.A05, r11, r5), new C130475zT(new AnonymousClass3CS(A01), A02).A00, A01);
    }

    @Override // X.AnonymousClass18N
    public synchronized Set AAs() {
        HashSet A12;
        A12 = C12970iu.A12();
        for (AnonymousClass1ZR r0 : this.A0B) {
            A12.add((String) r0.A00);
        }
        return A12;
    }

    @Override // X.AnonymousClass18N
    public synchronized boolean AJE(AnonymousClass1ZR r2) {
        return this.A0B.contains(r2);
    }

    @Override // X.AnonymousClass18N
    public synchronized boolean AJQ() {
        return C12960it.A1S((this.A00 > -1 ? 1 : (this.A00 == -1 ? 0 : -1)));
    }

    @Override // X.AnonymousClass18N
    public synchronized void Acr() {
        Log.i("PAY: IndiaUpiBlockListManager setShouldFetch called");
        this.A00 = -1;
        C12970iu.A1C(C117295Zj.A05(this.A0A), "payments_block_list_last_sync_time", -1);
    }

    @Override // X.AnonymousClass18N
    public synchronized boolean AdQ() {
        boolean z;
        StringBuilder A0h = C12960it.A0h();
        A0h.append("PAY: IndiaUpiBlockListManager shouldFetch lastFetched: ");
        A0h.append(this.A00);
        C12960it.A1F(A0h);
        if (!this.A08.A04().A00()) {
            if (this.A00 != -1) {
                if (this.A04.A00() - this.A00 >= 86400000) {
                }
            }
            z = true;
        }
        z = false;
        return z;
    }

    @Override // X.AnonymousClass18N
    public void Afe(Activity activity, AnonymousClass5US r10, C18610sj r11, String str, boolean z) {
        this.A01.A00(activity, new AbstractC43701xS(activity, this, r10, r11, str, z) { // from class: X.66X
            public final /* synthetic */ Activity A00;
            public final /* synthetic */ AnonymousClass68Z A01;
            public final /* synthetic */ AnonymousClass5US A02;
            public final /* synthetic */ C18610sj A03;
            public final /* synthetic */ String A04;
            public final /* synthetic */ boolean A05;

            {
                this.A01 = r2;
                this.A03 = r4;
                this.A04 = r5;
                this.A05 = r6;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AbstractC43701xS
            public final void A6f() {
                String str2;
                AnonymousClass1V8 r0;
                AnonymousClass68Z r7 = this.A01;
                C18610sj r6 = this.A03;
                String str3 = this.A04;
                boolean z2 = this.A05;
                Activity activity2 = this.A00;
                AnonymousClass5US r4 = this.A02;
                C120405g8 r9 = new C120405g8(r7.A05.A00, r7.A02, r7.A03, r7.A06, r7, r7.A07, r7.A09, r6);
                C128855wm r62 = new C128855wm(activity2, r7, r4);
                StringBuilder A0k = C12960it.A0k("PAY: blockNonWaVpa called vpa: ");
                A0k.append(C1309060l.A02(str3));
                A0k.append(" block: ");
                A0k.append(z2);
                C12960it.A1F(A0k);
                if (z2) {
                    str2 = "upi-block-vpa";
                } else {
                    str2 = "upi-unblock-vpa";
                }
                C64513Fv r8 = ((C126705tJ) r9).A00;
                if (r8 != null) {
                    r8.A04(str2);
                }
                C17220qS r2 = r9.A03;
                String A01 = r2.A01();
                AnonymousClass3CT r3 = new AnonymousClass3CT(A01);
                if (z2) {
                    r0 = new C126415sq(r3, str3).A00;
                } else {
                    r0 = new C126575t6(r3, str3).A00;
                }
                C117325Zm.A06(r2, new C120845gq(r9.A00, r9.A01, r62, r9.A05, r8, r9, str2, str3, z2), r0, A01);
            }
        }, z);
    }

    @Override // X.AnonymousClass18N
    public synchronized void clear() {
        Log.i("PAY: IndiaUpiBlockListManager clear");
        this.A0B.clear();
        this.A0A.A0G("");
    }

    @Override // X.AnonymousClass18N
    public synchronized int size() {
        return this.A0B.size();
    }
}
