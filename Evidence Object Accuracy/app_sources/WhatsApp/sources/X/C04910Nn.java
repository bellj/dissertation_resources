package X;

import android.content.ComponentName;
import android.support.v4.app.INotificationSideChannel;
import java.util.ArrayDeque;

/* renamed from: X.0Nn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04910Nn {
    public int A00 = 0;
    public INotificationSideChannel A01;
    public ArrayDeque A02 = new ArrayDeque();
    public boolean A03 = false;
    public final ComponentName A04;

    public C04910Nn(ComponentName componentName) {
        this.A04 = componentName;
    }
}
