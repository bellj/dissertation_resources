package X;

import android.view.View;
import android.view.ViewGroup;

@Deprecated
/* renamed from: X.019  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass019 extends AnonymousClass01A {
    public C004902f A00 = null;
    public AnonymousClass01E A01 = null;
    public boolean A02;
    public final int A03;
    public final AnonymousClass01F A04;

    public long A0F(int i) {
        return (long) i;
    }

    public abstract AnonymousClass01E A0G(int i);

    public AnonymousClass019(AnonymousClass01F r2, int i) {
        this.A04 = r2;
        this.A03 = i;
    }

    @Override // X.AnonymousClass01A
    public Object A05(ViewGroup viewGroup, int i) {
        if (this.A00 == null) {
            this.A00 = new C004902f(this.A04);
        }
        long A0F = A0F(i);
        int id = viewGroup.getId();
        StringBuilder sb = new StringBuilder("android:switcher:");
        sb.append(id);
        sb.append(":");
        sb.append(A0F);
        AnonymousClass01E A0A = this.A04.A0A(sb.toString());
        if (A0A != null) {
            this.A00.A0D(new AnonymousClass0Ta(A0A, 7));
        } else {
            A0A = A0G(i);
            C004902f r5 = this.A00;
            int id2 = viewGroup.getId();
            int id3 = viewGroup.getId();
            StringBuilder sb2 = new StringBuilder("android:switcher:");
            sb2.append(id3);
            sb2.append(":");
            sb2.append(A0F);
            r5.A0A(A0A, sb2.toString(), id2);
        }
        if (A0A != this.A01) {
            A0A.A0b(false);
            if (this.A03 == 1) {
                this.A00.A08(A0A, AnonymousClass05I.STARTED);
            } else {
                A0A.A0n(false);
                return A0A;
            }
        }
        return A0A;
    }

    @Override // X.AnonymousClass01A
    public void A0A(ViewGroup viewGroup) {
        C004902f r2 = this.A00;
        if (r2 != null) {
            if (!this.A02) {
                try {
                    this.A02 = true;
                    r2.A03();
                } finally {
                    this.A02 = false;
                }
            }
            this.A00 = null;
        }
    }

    @Override // X.AnonymousClass01A
    public void A0B(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            StringBuilder sb = new StringBuilder("ViewPager with adapter ");
            sb.append(this);
            sb.append(" requires a view id");
            throw new IllegalStateException(sb.toString());
        }
    }

    @Override // X.AnonymousClass01A
    public void A0C(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01E r6 = (AnonymousClass01E) obj;
        AnonymousClass01E r0 = this.A01;
        if (r6 != r0) {
            if (r0 != null) {
                r0.A0b(false);
                if (this.A03 == 1) {
                    C004902f r2 = this.A00;
                    if (r2 == null) {
                        r2 = new C004902f(this.A04);
                        this.A00 = r2;
                    }
                    r2.A08(this.A01, AnonymousClass05I.STARTED);
                } else {
                    this.A01.A0n(false);
                }
            }
            r6.A0b(true);
            if (this.A03 == 1) {
                C004902f r1 = this.A00;
                if (r1 == null) {
                    r1 = new C004902f(this.A04);
                    this.A00 = r1;
                }
                r1.A08(r6, AnonymousClass05I.RESUMED);
            } else {
                r6.A0n(true);
            }
            this.A01 = r6;
        }
    }

    @Override // X.AnonymousClass01A
    public void A0D(ViewGroup viewGroup, Object obj, int i) {
        AnonymousClass01E r5 = (AnonymousClass01E) obj;
        C004902f r2 = this.A00;
        if (r2 == null) {
            r2 = new C004902f(this.A04);
            this.A00 = r2;
        }
        AnonymousClass01F r1 = r5.A0H;
        if (r1 == null || r1 == r2.A0J) {
            r2.A0D(new AnonymousClass0Ta(r5, 6));
            if (r5.equals(this.A01)) {
                this.A01 = null;
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder("Cannot detach Fragment attached to a different FragmentManager. Fragment ");
        sb.append(r5.toString());
        sb.append(" is already attached to a FragmentManager.");
        throw new IllegalStateException(sb.toString());
    }

    @Override // X.AnonymousClass01A
    public boolean A0E(View view, Object obj) {
        return ((AnonymousClass01E) obj).A0A == view;
    }
}
