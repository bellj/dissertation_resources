package X;

import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.widget.EditText;

/* renamed from: X.3C4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C4 {
    public float A00;
    public float A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05 = 0;
    public int A06;
    public int A07 = -1;
    public ColorStateList A08;
    public ColorStateList A09;
    public Rect A0A = C12980iv.A0J();
    public Drawable A0B;
    public Drawable A0C;
    public Parcelable A0D;
    public Editable A0E;
    public TextUtils.TruncateAt A0F;
    public TextUtils.TruncateAt A0G;
    public TextWatcher A0H;
    public KeyListener A0I;
    public KeyListener A0J;
    public EditText A0K;
    public AnonymousClass5RC A0L;
    public AnonymousClass3MF A0M;
    public Object A0N;
    public boolean A0O = true;

    public AnonymousClass3C4(String str, float f) {
        this.A0E = C12990iw.A0J(str);
        this.A01 = f;
    }
}
