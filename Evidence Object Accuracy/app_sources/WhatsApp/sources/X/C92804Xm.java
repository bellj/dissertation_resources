package X;

import java.util.Arrays;

/* renamed from: X.4Xm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92804Xm {
    public final String A00;
    public final String A01;
    public final C92854Xr A02;
    public final Object[] A03;

    public C92804Xm(String str, String str2, C92854Xr r3, Object... objArr) {
        this.A01 = str;
        this.A00 = str2;
        this.A02 = r3;
        this.A03 = objArr;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C92804Xm)) {
            return false;
        }
        C92804Xm r4 = (C92804Xm) obj;
        if (!this.A01.equals(r4.A01) || !this.A00.equals(r4.A00) || !this.A02.equals(r4.A02) || !Arrays.equals(this.A03, r4.A03)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.A01.hashCode() ^ Integer.rotateLeft(this.A00.hashCode(), 8)) ^ Integer.rotateLeft(this.A02.hashCode(), 16)) ^ Integer.rotateLeft(Arrays.hashCode(this.A03), 24);
    }

    public String toString() {
        StringBuilder A0h = C12960it.A0h();
        A0h.append(this.A01);
        A0h.append(" : ");
        A0h.append(this.A00);
        A0h.append(' ');
        A0h.append(this.A02);
        A0h.append(' ');
        return C12960it.A0d(Arrays.toString(this.A03), A0h);
    }
}
