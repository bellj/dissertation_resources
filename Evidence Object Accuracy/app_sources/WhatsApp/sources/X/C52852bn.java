package X;

import android.text.TextUtils;
import android.widget.Filter;
import com.whatsapp.R;
import com.whatsapp.group.GroupChatInfo;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.2bn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52852bn extends Filter {
    public final /* synthetic */ C36661kH A00;

    public /* synthetic */ C52852bn(C36661kH r1) {
        this.A00 = r1;
    }

    @Override // android.widget.Filter
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        ArrayList arrayList;
        C15370n3 r8;
        AnonymousClass1YO A01;
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (!TextUtils.isEmpty(charSequence)) {
            ArrayList A0l = C12960it.A0l();
            String charSequence2 = charSequence.toString();
            C36661kH r2 = this.A00;
            GroupChatInfo groupChatInfo = r2.A07;
            ArrayList A02 = C32751cg.A02(((AbstractActivityC33001d7) groupChatInfo).A08, charSequence2);
            boolean contains = AnonymousClass1US.A08(charSequence).contains(AnonymousClass1US.A08(groupChatInfo.getString(R.string.group_admin)));
            for (AbstractC36121jM r5 : r2.A02) {
                if ((r5 instanceof AbstractC36111jL) && (r8 = ((AbstractC36111jL) r5).A00) != null) {
                    if (!groupChatInfo.A0Y.A0M(r8, A02, true)) {
                        if (!C32751cg.A03(((AbstractActivityC33001d7) groupChatInfo).A08, r8.A0U, A02, true)) {
                            if (!(!contains || (A01 = ((AbstractActivityC33001d7) groupChatInfo).A0C.A01(groupChatInfo.A1C, C15370n3.A04(r8))) == null || A01.A01 == 0)) {
                            }
                        }
                    }
                    A0l.add(r5);
                }
            }
            boolean isEmpty = A0l.isEmpty();
            arrayList = A0l;
            if (isEmpty) {
                A0l.add(0, new C36131jN(charSequence.toString()));
                arrayList = A0l;
            }
        } else {
            arrayList = this.A00.A02;
        }
        filterResults.values = arrayList;
        filterResults.count = arrayList.size();
        return filterResults;
    }

    @Override // android.widget.Filter
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        List list;
        Object obj = filterResults.values;
        if (obj == null) {
            list = this.A00.A02;
        } else {
            list = (ArrayList) obj;
        }
        this.A00.A01(list);
    }
}
