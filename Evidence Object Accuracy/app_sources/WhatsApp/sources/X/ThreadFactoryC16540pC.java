package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* renamed from: X.0pC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ThreadFactoryC16540pC implements ThreadFactory {
    public final String A00;
    public final ThreadFactory A01 = Executors.defaultThreadFactory();

    public ThreadFactoryC16540pC(String str) {
        C13020j0.A02(str, "Name must not be null");
        this.A00 = str;
    }

    @Override // java.util.concurrent.ThreadFactory
    public final Thread newThread(Runnable runnable) {
        Thread newThread = this.A01.newThread(new RunnableBRunnable0Shape0S0100000_I0(runnable));
        newThread.setName(this.A00);
        return newThread;
    }
}
