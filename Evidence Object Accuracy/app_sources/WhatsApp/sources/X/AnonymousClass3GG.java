package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3GG  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GG {
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002a, code lost:
        if (r1 != 29) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.AbstractC35601iM A00(X.AbstractC16130oV r11, X.AnonymousClass19O r12) {
        /*
            r3 = r11
            X.0oX r0 = r11.A02
            if (r0 == 0) goto L_0x002c
            java.io.File r5 = r0.A0F
            if (r5 == 0) goto L_0x002c
            byte r1 = r11.A0y
            r0 = 1
            if (r1 == r0) goto L_0x0071
            r0 = 2
            if (r1 == r0) goto L_0x0062
            r0 = 3
            if (r1 == r0) goto L_0x0053
            r0 = 9
            if (r1 == r0) goto L_0x0043
            r0 = 13
            if (r1 == r0) goto L_0x0034
            r0 = 25
            if (r1 == r0) goto L_0x0071
            r0 = 26
            if (r1 == r0) goto L_0x0043
            r0 = 28
            if (r1 == r0) goto L_0x0053
            r0 = 29
            if (r1 == r0) goto L_0x0034
        L_0x002c:
            long r0 = r11.A0I
            X.43w r2 = new X.43w
            r2.<init>(r11, r0)
            return r2
        L_0x0034:
            long r9 = r11.A0I
            int r0 = r11.A00
            long r0 = (long) r0
            X.43y r2 = new X.43y
            r6 = r2
            r7 = r11
            r8 = r5
            r11 = r0
            r6.<init>(r7, r8, r9, r11)
            return r2
        L_0x0043:
            X.0p1 r3 = (X.C16440p1) r3
            long r7 = r3.A0I
            int r0 = r3.A00
            long r9 = (long) r0
            java.lang.String r6 = r3.A06
            r4 = r12
            X.31f r2 = new X.31f
            r2.<init>(r3, r4, r5, r6, r7, r9)
            return r2
        L_0x0053:
            long r9 = r11.A0I
            int r0 = r11.A00
            long r0 = (long) r0
            X.43z r2 = new X.43z
            r6 = r2
            r7 = r11
            r8 = r5
            r11 = r0
            r6.<init>(r7, r8, r9, r11)
            return r2
        L_0x0062:
            long r9 = r11.A0I
            int r0 = r11.A00
            long r0 = (long) r0
            X.43x r2 = new X.43x
            r6 = r2
            r7 = r11
            r8 = r5
            r11 = r0
            r6.<init>(r7, r8, r9, r11)
            return r2
        L_0x0071:
            long r0 = r11.A0I
            X.31e r2 = new X.31e
            r2.<init>(r11, r5, r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3GG.A00(X.0oV, X.19O):X.1iM");
    }

    public static void A01(Bitmap bitmap, Drawable drawable, AbstractC35611iN r8, AnonymousClass2T3 r9, int i, boolean z) {
        int i2;
        AbstractC16130oV r0;
        Context context = r9.getContext();
        if (bitmap == null) {
            r9.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            int type = r8.getType();
            if (type == 0) {
                r9.setBackgroundColor(i);
                i2 = R.drawable.ic_missing_thumbnail_picture;
            } else if (type == 1 || type == 2) {
                r9.setBackgroundColor(i);
                i2 = R.drawable.ic_missing_thumbnail_video;
            } else if (type == 3) {
                AbstractC35611iN r1 = r9.A05;
                if (!(r1 instanceof AbstractC35601iM) || (r0 = ((AbstractC35601iM) r1).A03) == null || ((AbstractC15340mz) r0).A08 != 1) {
                    C12970iu.A18(context, r9, R.color.music_scrubber);
                    i2 = R.drawable.icon_audio_large;
                } else {
                    C12970iu.A18(context, r9, R.color.action_mode);
                    i2 = R.drawable.icon_ppt_large;
                }
            } else if (type != 4) {
                r9.setBackgroundColor(i);
                r9.setImageResource(0);
                return;
            } else {
                r9.setBackgroundColor(i);
                r9.setImageDrawable(C26511Dt.A03(context, r8.AES(), null, true));
                return;
            }
            r9.setImageResource(i2);
            return;
        }
        C12990iw.A1E(r9);
        r9.setBackgroundColor(0);
        r9.A00 = bitmap;
        if (z) {
            Drawable[] drawableArr = new Drawable[2];
            drawableArr[0] = drawable;
            C12960it.A15(r9, new BitmapDrawable(context.getResources(), bitmap), drawableArr);
            return;
        }
        r9.setImageBitmap(bitmap);
    }
}
