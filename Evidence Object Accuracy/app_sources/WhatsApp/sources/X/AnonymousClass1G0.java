package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/* renamed from: X.1G0  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1G0 implements AnonymousClass1G1 {
    public int A00 = 0;

    public AbstractC27881Jp A00() {
        try {
            AnonymousClass4MP r2 = new AnonymousClass4MP(AGd());
            CodedOutputStream codedOutputStream = r2.A00;
            AgI(codedOutputStream);
            C56842m2 r0 = (C56842m2) codedOutputStream;
            if (r0.A01 - r0.A00 == 0) {
                return new C27861Jn(r2.A01);
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("Serializing ");
            sb.append(getClass().getName());
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    public void A01(OutputStream outputStream) {
        int AGd = AGd();
        int A01 = CodedOutputStream.A01(AGd) + AGd;
        if (A01 > 4096) {
            A01 = 4096;
        }
        C40921sY r1 = new C40921sY(outputStream, A01);
        r1.A0C(AGd);
        AgI(r1);
        if (r1.A00 > 0) {
            r1.A0O();
        }
    }

    public byte[] A02() {
        try {
            int AGd = AGd();
            byte[] bArr = new byte[AGd];
            C56842m2 r0 = new C56842m2(bArr, AGd);
            AgI(r0);
            if (r0.A01 - r0.A00 == 0) {
                return bArr;
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("Serializing ");
            sb.append(getClass().getName());
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }
}
