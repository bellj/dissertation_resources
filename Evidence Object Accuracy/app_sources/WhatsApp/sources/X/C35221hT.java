package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1hT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35221hT {
    public final long A00;
    public final UserJid A01;
    public final AnonymousClass1IS A02;

    public C35221hT(UserJid userJid, AnonymousClass1IS r2, long j) {
        this.A01 = userJid;
        this.A00 = j;
        this.A02 = r2;
    }
}
