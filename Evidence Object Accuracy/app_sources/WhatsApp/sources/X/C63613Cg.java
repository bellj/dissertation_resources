package X;

import android.content.pm.PackageManager;
import java.util.Set;

/* renamed from: X.3Cg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63613Cg {
    public final PackageManager A00;
    public final AbstractC17940re A01;
    public final C81073tQ A02;

    public C63613Cg(PackageManager packageManager, C81073tQ r3, Set set) {
        this.A00 = packageManager;
        this.A01 = AbstractC17940re.copyOf(set);
        this.A02 = C81073tQ.copyOf(r3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00(android.net.Uri r8) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63613Cg.A00(android.net.Uri):void");
    }
}
