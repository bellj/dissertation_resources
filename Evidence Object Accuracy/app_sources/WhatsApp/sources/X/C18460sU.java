package X;

import android.database.Cursor;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0200100_I0;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0sU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18460sU {
    public final AbstractC15710nm A00;
    public final C16490p7 A01;
    public final C21390xL A02;
    public final Map A03 = new ConcurrentHashMap();
    public final Map A04 = new ConcurrentHashMap();

    public C18460sU(AbstractC15710nm r2, C16490p7 r3, C21390xL r4) {
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3;
    }

    public static final void A00(String str, String str2, String str3, String str4, int i, int i2, int i3) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" user=");
        sb.append(str2);
        sb.append(" server=");
        sb.append(str3);
        sb.append(" agent=");
        sb.append(i);
        sb.append(" device=");
        sb.append(i2);
        sb.append(" type=");
        sb.append(i3);
        sb.append(" rawString=");
        sb.append(str4);
        Log.e(sb.toString());
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0231  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long A01(com.whatsapp.jid.Jid r25) {
        /*
        // Method dump skipped, instructions count: 677
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18460sU.A01(com.whatsapp.jid.Jid):long");
    }

    public final long A02(Jid jid) {
        Cursor cursor;
        C16310on A02 = this.A01.A02();
        try {
            long j = -1;
            if (jid instanceof DeviceJid) {
                cursor = A02.A03.A09("SELECT _id FROM jid WHERE user = ? AND server = ? AND agent = ? AND device = ? AND type = ?", new String[]{jid.user, jid.getServer(), Integer.toString(jid.getAgent()), Integer.toString(jid.getDevice()), Integer.toString(jid.getType())});
                if (cursor.moveToLast()) {
                    j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                }
            } else {
                cursor = A02.A03.A09("SELECT _id FROM jid WHERE user = ? AND server = ? AND agent = ? AND type = ?", new String[]{jid.user, jid.getServer(), Integer.toString(jid.getAgent()), Integer.toString(jid.getType())});
                if (cursor.moveToLast()) {
                    j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                }
            }
            cursor.close();
            A02.close();
            return j;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Jid A03(long j) {
        Jid jid = null;
        if (j <= 0) {
            return null;
        }
        Map map = this.A04;
        Long valueOf = Long.valueOf(j);
        if (map.containsKey(valueOf)) {
            return (Jid) map.get(valueOf);
        }
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT user, server, agent, device, type, raw_string FROM jid WHERE _id = ?", new String[]{Long.toString(j)});
            if (A09.moveToLast()) {
                jid = A05(A09, A01, A09.getColumnIndexOrThrow("user"), A09.getColumnIndexOrThrow("server"), A09.getColumnIndexOrThrow("agent"), A09.getColumnIndexOrThrow("device"), A09.getColumnIndexOrThrow("type"), A09.getColumnIndexOrThrow("raw_string"), j);
            }
            A09.close();
            A01.close();
            return jid;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Jid A04(Cursor cursor, int i, int i2, int i3, int i4, int i5, int i6) {
        int i7;
        String A02 = AnonymousClass1Tx.A02(cursor, i);
        String A022 = AnonymousClass1Tx.A02(cursor, i2);
        int i8 = cursor.getInt(i3);
        if (cursor.isNull(i4)) {
            i7 = 0;
        } else {
            i7 = cursor.getInt(i4);
        }
        int i9 = cursor.getInt(i5);
        String A023 = AnonymousClass1Tx.A02(cursor, i6);
        try {
            Jid jid = Jid.get(A023);
            if (i9 == 0) {
                if (jid instanceof DeviceJid) {
                    jid = ((DeviceJid) jid).getUserJid();
                }
            } else if (i9 == 17 && (jid instanceof UserJid)) {
                jid = DeviceJid.of(jid);
                AnonymousClass009.A05(jid);
            }
            if (AnonymousClass1US.A0D(A02, jid.user) && AnonymousClass1US.A0D(A022, jid.getServer()) && i8 == jid.getAgent() && i7 == jid.getDevice() && i9 == jid.getType()) {
                return jid;
            }
            A00("jidstore/readjidfromcursor/cursormismatch", A02, A022, A023, i8, i7, i9);
            return null;
        } catch (AnonymousClass1MW unused) {
            if (i9 == 11 && TextUtils.isEmpty(A02) && TextUtils.isEmpty(A022) && i8 == 0 && i7 == 0 && TextUtils.isEmpty(A023)) {
                return C29831Uv.A00;
            }
            A00("jidstore/readjidfromcursor/invalidjid", A02, A022, A023, i8, i7, i9);
            return null;
        }
    }

    public Jid A05(Cursor cursor, C16310on r15, int i, int i2, int i3, int i4, int i5, int i6, long j) {
        if (j <= 0) {
            return null;
        }
        Map map = this.A04;
        Long valueOf = Long.valueOf(j);
        if (map.containsKey(valueOf)) {
            return (Jid) map.get(valueOf);
        }
        Jid A04 = A04(cursor, i, i2, i3, i4, i5, i6);
        if (A04 == null) {
            return A04;
        }
        if (r15.A03.A00.inTransaction()) {
            r15.A03(new RunnableBRunnable0Shape0S0200100_I0(this, A04, 2, j));
            return A04;
        }
        A0B(A04, j);
        return A04;
    }

    public Jid A06(Cursor cursor, C16310on r16, Class cls, int i, int i2, int i3, int i4, int i5, int i6, long j) {
        try {
            return (Jid) cls.cast(A05(cursor, r16, i, i2, i3, i4, i5, i6, j));
        } catch (ClassCastException e) {
            StringBuilder sb = new StringBuilder("JidStore/readJidByRowId/jid wrong class; rowId=");
            sb.append(j);
            sb.append("; db_data=");
            sb.append(A08(j));
            Log.e(sb.toString(), e);
            this.A00.AaV("invalid-jid-in-store", null, false);
            return null;
        }
    }

    public Jid A07(Class cls, long j) {
        try {
            return (Jid) cls.cast(A03(j));
        } catch (ClassCastException e) {
            StringBuilder sb = new StringBuilder("JidStore/readJidByRowId/jid wrong class; rowId=");
            sb.append(j);
            sb.append("; db_data=");
            sb.append(A08(j));
            Log.e(sb.toString(), e);
            this.A00.AaV("JidStore/readJidByRowId", "invalid-jid-in-store", true);
            return null;
        }
    }

    public final String A08(long j) {
        C16310on A01 = this.A01.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT user, server, agent, device, type, raw_string FROM jid WHERE _id = ?", new String[]{Long.toString(j)});
            if (A09.moveToLast()) {
                String string = A09.getString(A09.getColumnIndexOrThrow("user"));
                String string2 = A09.getString(A09.getColumnIndexOrThrow("server"));
                int i = A09.getInt(A09.getColumnIndexOrThrow("agent"));
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("device");
                int i2 = A09.isNull(columnIndexOrThrow) ? 0 : A09.getInt(columnIndexOrThrow);
                int i3 = A09.getInt(A09.getColumnIndexOrThrow("type"));
                String string3 = A09.getString(A09.getColumnIndexOrThrow("raw_string"));
                boolean isNull = A09.isNull(columnIndexOrThrow);
                if (string != null) {
                    String str = string;
                    AnonymousClass009.A0E(true);
                    int length = string.length();
                    if (length > 4) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(TextUtils.join("", Collections.nCopies(length - 4, '*')));
                        sb.append(AnonymousClass1US.A02(4, string));
                        str = sb.toString();
                    }
                    if (string3 != null) {
                        string3 = string3.replace(string, str);
                    }
                    string = str;
                }
                StringBuilder sb2 = new StringBuilder("user=");
                sb2.append(string);
                sb2.append("; server=");
                sb2.append(string2);
                sb2.append("; agent=");
                sb2.append(i);
                sb2.append("; device=");
                sb2.append(i2);
                sb2.append("; type=");
                sb2.append(i3);
                sb2.append("; rawString=");
                sb2.append(string3);
                sb2.append("; has_device=");
                sb2.append(isNull ? "no" : "yes");
                String obj = sb2.toString();
                A09.close();
                A01.close();
                return obj;
            }
            A09.close();
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Map A09(Class cls, Collection collection) {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            long longValue = ((Number) it.next()).longValue();
            Map map = this.A04;
            Long valueOf = Long.valueOf(longValue);
            if (map.containsKey(valueOf)) {
                hashMap.put(valueOf, A07(cls, longValue));
            } else {
                arrayList.add(Long.toString(longValue));
            }
        }
        C29841Uw r1 = new C29841Uw(arrayList.toArray(AnonymousClass01V.A0H), 975);
        C16310on A01 = this.A01.get();
        try {
            Iterator it2 = r1.iterator();
            while (it2.hasNext()) {
                String[] strArr = (String[]) it2.next();
                C16330op r4 = A01.A03;
                int length = strArr.length;
                StringBuilder sb = new StringBuilder("SELECT _id, user, server, agent, device, type, raw_string FROM jid WHERE _id IN ");
                sb.append(AnonymousClass1Ux.A00(length));
                Cursor A09 = r4.A09(sb.toString(), strArr);
                int columnIndexOrThrow = A09.getColumnIndexOrThrow("_id");
                int columnIndexOrThrow2 = A09.getColumnIndexOrThrow("user");
                int columnIndexOrThrow3 = A09.getColumnIndexOrThrow("server");
                int columnIndexOrThrow4 = A09.getColumnIndexOrThrow("agent");
                int columnIndexOrThrow5 = A09.getColumnIndexOrThrow("device");
                int columnIndexOrThrow6 = A09.getColumnIndexOrThrow("type");
                int columnIndexOrThrow7 = A09.getColumnIndexOrThrow("raw_string");
                while (A09.moveToNext()) {
                    long j = A09.getLong(columnIndexOrThrow);
                    hashMap.put(Long.valueOf(j), A06(A09, A01, cls, columnIndexOrThrow2, columnIndexOrThrow3, columnIndexOrThrow4, columnIndexOrThrow5, columnIndexOrThrow6, columnIndexOrThrow7, j));
                }
                A09.close();
            }
            A01.close();
            Iterator it3 = collection.iterator();
            while (it3.hasNext()) {
                Long valueOf2 = Long.valueOf(((Number) it3.next()).longValue());
                if (!hashMap.containsKey(valueOf2)) {
                    hashMap.put(valueOf2, null);
                }
            }
            return hashMap;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A0A(Cursor cursor) {
        while (cursor.moveToNext()) {
            String A02 = AnonymousClass1Tx.A02(cursor, 0);
            if (!TextUtils.isEmpty(A02)) {
                for (String str : A02.split(",")) {
                    Jid nullable = Jid.getNullable(str);
                    if (nullable != null) {
                        A01(nullable);
                    }
                }
            }
        }
    }

    public final void A0B(Jid jid, long j) {
        Map map = this.A04;
        Long valueOf = Long.valueOf(j);
        map.put(valueOf, jid);
        this.A03.put(jid, valueOf);
    }

    public boolean A0C() {
        return this.A02.A01("jid_ready", 0) != 0;
    }
}
