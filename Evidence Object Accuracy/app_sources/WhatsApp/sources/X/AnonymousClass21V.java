package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.net.Uri;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.whatsapp.mediacomposer.ImageComposerFragment;
import com.whatsapp.mediacomposer.MediaComposerActivity;
import com.whatsapp.mediacomposer.MediaComposerFragment;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.21V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21V implements GestureDetector.OnDoubleTapListener {
    public final /* synthetic */ ImageComposerFragment A00;
    public final /* synthetic */ MediaComposerFragment A01;

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    public AnonymousClass21V(ImageComposerFragment imageComposerFragment) {
        this.A00 = imageComposerFragment;
        this.A01 = imageComposerFragment;
    }

    public void A00() {
        ImageComposerFragment imageComposerFragment = this.A00;
        AnonymousClass21Y r3 = (AnonymousClass21Y) imageComposerFragment.A0B();
        if (r3 != null) {
            Uri uri = ((MediaComposerFragment) imageComposerFragment).A00;
            ((MediaComposerActivity) r3).A1B.A00(uri).A0A(imageComposerFragment.A07.A01);
        }
        if (imageComposerFragment.A0c()) {
            if (imageComposerFragment.A0B() != null && imageComposerFragment.A08.getDrawable() == null) {
                imageComposerFragment.A0B().A0d();
            }
            imageComposerFragment.A08.A05(imageComposerFragment.A07.A03);
            AnonymousClass2Ab r7 = ((MediaComposerFragment) imageComposerFragment).A0D;
            C93694aa r2 = r7.A0P;
            r2.A02 = null;
            r2.A03 = null;
            if (r7.A08) {
                for (AnonymousClass33J r0 : r7.A0O.A01()) {
                    r0.A0R(r2);
                }
                r7.A08 = false;
            }
            C454921v r6 = r7.A0G;
            Bitmap bitmap = r6.A05;
            if (bitmap != null) {
                bitmap.eraseColor(0);
            }
            C454721t r02 = r6.A0G;
            ArrayList arrayList = new ArrayList();
            for (AbstractC454821u r1 : r02.A04) {
                if (r1 instanceof AnonymousClass33J) {
                    AnonymousClass33J r12 = (AnonymousClass33J) r1;
                    if (r12.A03 instanceof AnonymousClass33P) {
                        arrayList.add(r12);
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                r6.A03(true);
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    AnonymousClass33J r4 = (AnonymousClass33J) it.next();
                    AbstractC64833Hb r32 = r4.A03;
                    if (r32 instanceof AnonymousClass33P) {
                        Bitmap bitmap2 = r6.A05;
                        PointF pointF = r6.A0D;
                        int i = r6.A00;
                        r4.A01 = bitmap2;
                        r4.A02 = pointF;
                        r4.A00 = i;
                    }
                    r4.A05 = false;
                    Bitmap bitmap3 = r4.A01;
                    if (bitmap3 != null) {
                        r32.A01(bitmap3, r4.A02, r4.A00);
                        AbstractC64833Hb r13 = r4.A03;
                        Canvas canvas = r13.A00;
                        if (canvas != null) {
                            r13.A02(canvas);
                        }
                    }
                }
            }
            r7.A0H.invalidate();
        }
    }

    @Override // android.view.GestureDetector.OnDoubleTapListener
    public boolean onDoubleTap(MotionEvent motionEvent) {
        ImageComposerFragment imageComposerFragment = this.A00;
        boolean onDoubleTap = imageComposerFragment.A06.A05.onDoubleTap(motionEvent);
        if (onDoubleTap) {
            imageComposerFragment.A1K(false, true);
        }
        return onDoubleTap;
    }
}
