package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.CodeInputField;
import com.whatsapp.backup.encryptedbackup.EncryptionKeyFragment;

/* renamed from: X.3MC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3MC implements TextWatcher {
    public final /* synthetic */ CodeInputField A00;
    public final /* synthetic */ EncryptionKeyFragment A01;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public AnonymousClass3MC(CodeInputField codeInputField, EncryptionKeyFragment encryptionKeyFragment) {
        this.A01 = encryptionKeyFragment;
        this.A00 = codeInputField;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int i4;
        String charSequence2 = charSequence.toString();
        EncryptionKeyFragment encryptionKeyFragment = this.A01;
        String lowerCase = charSequence2.toLowerCase(C12970iu.A14(encryptionKeyFragment.A03));
        if (!charSequence.toString().equals(lowerCase)) {
            CodeInputField codeInputField = this.A00;
            codeInputField.setText(lowerCase);
            codeInputField.setSelection(charSequence.length());
        }
        if (charSequence.length() == 4 && (i4 = encryptionKeyFragment.A00 + 1) < 16) {
            encryptionKeyFragment.A04[i4].requestFocus();
        }
        StringBuilder A0h = C12960it.A0h();
        int i5 = 0;
        do {
            Editable text = encryptionKeyFragment.A04[i5].getText();
            if (text == null) {
                break;
            }
            A0h.append((CharSequence) text);
            i5++;
        } while (i5 < 16);
        encryptionKeyFragment.A01.A02.A0B(A0h.toString());
    }
}
