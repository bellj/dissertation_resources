package X;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.whatsapp.InteractiveAnnotation;
import com.whatsapp.SerializablePoint;

/* renamed from: X.3GA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GA {
    public static InteractiveAnnotation A00(ImageView imageView, AbstractC16130oV r8, float f, float f2) {
        Drawable drawable = imageView.getDrawable();
        if (drawable == null) {
            return null;
        }
        Matrix A01 = C13000ix.A01();
        imageView.getImageMatrix().invert(A01);
        float[] fArr = {f - ((float) imageView.getLeft()), f2 - ((float) imageView.getTop())};
        float[] fArr2 = {(float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight()};
        A01.mapPoints(fArr);
        return A01(r8, fArr, fArr2);
    }

    public static InteractiveAnnotation A01(AbstractC16130oV r19, float[] fArr, float[] fArr2) {
        InteractiveAnnotation[] interactiveAnnotationArr;
        C16150oX r0 = r19.A02;
        if (r0 == null || (interactiveAnnotationArr = r0.A0V) == null || interactiveAnnotationArr.length <= 0) {
            return null;
        }
        int i = 0;
        PointF pointF = new PointF(fArr2[0], fArr2[1]);
        SerializablePoint serializablePoint = new SerializablePoint((double) ((int) fArr[0]), (double) ((int) fArr[1]));
        SerializablePoint serializablePoint2 = new SerializablePoint(serializablePoint.x / ((double) pointF.x), serializablePoint.y / ((double) pointF.y));
        while (true) {
            InteractiveAnnotation[] interactiveAnnotationArr2 = r0.A0V;
            if (i >= interactiveAnnotationArr2.length) {
                return null;
            }
            InteractiveAnnotation interactiveAnnotation = interactiveAnnotationArr2[i];
            SerializablePoint[] serializablePointArr = interactiveAnnotation.polygonVertices;
            if (!(serializablePointArr == null || interactiveAnnotation.serializableLocation == null)) {
                int i2 = 0;
                boolean z = false;
                while (true) {
                    int length = serializablePointArr.length;
                    if (i2 >= length) {
                        break;
                    }
                    SerializablePoint serializablePoint3 = serializablePointArr[i2];
                    i2++;
                    SerializablePoint serializablePoint4 = serializablePointArr[i2 % length];
                    double d = serializablePoint3.x;
                    double d2 = serializablePoint2.x;
                    if ((d <= d2 && d2 < serializablePoint4.x) || (serializablePoint4.x <= d2 && d2 < d)) {
                        double d3 = serializablePoint2.y;
                        double d4 = serializablePoint4.y;
                        double d5 = serializablePoint3.y;
                        if (d3 < (((d4 - d5) * (d2 - d)) / (serializablePoint4.x - d)) + d5) {
                            z = !z;
                        }
                    }
                }
                if (z) {
                    return interactiveAnnotation;
                }
            }
            i++;
        }
    }
}
