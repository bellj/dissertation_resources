package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;

/* renamed from: X.5g3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120355g3 extends AbstractC129955yZ {
    public final Context A00;
    public final AnonymousClass102 A01;
    public final C17220qS A02;
    public final AnonymousClass60Z A03;
    public final C128765wd A04;
    public final String A05;
    public final String A06;
    public final String A07;

    public C120355g3(Context context, C14900mE r14, C15570nT r15, C14830m7 r16, AnonymousClass102 r17, C241414j r18, C17220qS r19, AnonymousClass60Z r20, C18650sn r21, C18610sj r22, C17070qD r23, AnonymousClass60T r24, C129385xd r25, C128765wd r26, String str, String str2, String str3) {
        super(context, r14, r15, r16, r18, r21, r22, r23, r24, r25);
        this.A00 = context;
        this.A02 = r19;
        this.A01 = r17;
        this.A03 = r20;
        this.A05 = str;
        this.A07 = str2;
        this.A06 = str3;
        this.A04 = r26;
    }

    public final void A03(String str) {
        Log.i("PAY: BrazilVerifyCardSendAuthCodeAction sendAuthCode");
        C17220qS r5 = this.A02;
        String A01 = r5.A01();
        C117295Zj.A1B(r5, new IDxRCallbackShape2S0100000_3_I1(this.A00, super.A01, super.A05, this, 8), new C126325sh(new AnonymousClass3CT(A01), this.A06, this.A07, str).A00, A01);
    }
}
