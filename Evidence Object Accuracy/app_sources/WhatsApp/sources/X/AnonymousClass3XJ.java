package X;

import android.view.View;
import com.whatsapp.R;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.MediaViewFragment;
import com.whatsapp.videoplayback.ExoPlaybackControlView;

/* renamed from: X.3XJ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XJ implements AnonymousClass5X1 {
    public final float A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ MediaViewBaseFragment A02;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r4.A1N() == false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass3XJ(android.view.View r3, com.whatsapp.mediaview.MediaViewBaseFragment r4) {
        /*
            r2 = this;
            r2.A02 = r4
            r2.A01 = r3
            r2.<init>()
            X.21p r0 = r4.A0B
            boolean r0 = r0 instanceof X.AnonymousClass33Z
            if (r0 != 0) goto L_0x0016
            boolean r1 = r4.A1N()
            r0 = 1061997773(0x3f4ccccd, float:0.8)
            if (r1 != 0) goto L_0x0018
        L_0x0016:
            r0 = 1056964608(0x3f000000, float:0.5)
        L_0x0018:
            r2.A00 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3XJ.<init>(android.view.View, com.whatsapp.mediaview.MediaViewBaseFragment):void");
    }

    @Override // X.AnonymousClass5X1
    public void APG(View view) {
        MediaViewBaseFragment mediaViewBaseFragment = this.A02;
        AbstractC454421p r1 = mediaViewBaseFragment.A0B;
        if (((r1 instanceof AnonymousClass33Z) || !mediaViewBaseFragment.A1N()) && (r1 instanceof C454321o)) {
            mediaViewBaseFragment.A09.setAlpha(0.0f);
        }
        mediaViewBaseFragment.A1G();
    }

    @Override // X.AnonymousClass5X1
    public void APV(int i) {
        C47492Ax r0;
        MediaViewBaseFragment mediaViewBaseFragment = this.A02;
        if (mediaViewBaseFragment instanceof MediaViewFragment) {
            MediaViewFragment mediaViewFragment = (MediaViewFragment) mediaViewBaseFragment;
            AnonymousClass21S r1 = mediaViewFragment.A1U;
            if (i == 1) {
                if (r1 != null) {
                    r1.A05();
                    ExoPlaybackControlView exoPlaybackControlView = mediaViewFragment.A1U.A0C;
                    if (exoPlaybackControlView != null && !exoPlaybackControlView.A07()) {
                        exoPlaybackControlView.A01();
                        exoPlaybackControlView.A06(3000);
                    }
                }
                mediaViewFragment.A1R();
            } else if (r1 != null && r1.A0C == null && (r0 = r1.A08) != null) {
                r0.AcW(true);
            }
        }
    }

    @Override // X.AnonymousClass5X1
    public void AVv(View view) {
        AbstractC14720lw r0 = (AbstractC14720lw) this.A02.A0B();
        if (r0 != null) {
            r0.AXE();
        }
    }

    @Override // X.AnonymousClass5X1
    public void AWA(View view, float f) {
        float f2;
        MediaViewBaseFragment mediaViewBaseFragment = this.A02;
        AbstractC14720lw r0 = (AbstractC14720lw) mediaViewBaseFragment.A0B();
        if (r0 != null) {
            r0.APJ();
        }
        float f3 = 1.0f - f;
        float f4 = this.A00;
        if (f3 < f4) {
            f2 = 0.0f;
        } else {
            f2 = (f3 - f4) / (1.0f - f4);
        }
        this.A01.setAlpha(f2);
        mediaViewBaseFragment.A06.setAlpha(f2);
        if ((mediaViewBaseFragment.A0B instanceof AnonymousClass33Z) || !mediaViewBaseFragment.A1N()) {
            mediaViewBaseFragment.A09.setAlpha(f2);
        }
        int childCount = mediaViewBaseFragment.A09.getChildCount();
        for (int i = 0; i < childCount; i++) {
            mediaViewBaseFragment.A09.getChildAt(i).findViewById(R.id.footer).setAlpha(f2 * f2);
        }
        mediaViewBaseFragment.A1M(true, true);
    }
}
