package X;

/* renamed from: X.0GL  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0GL extends AnonymousClass043 {
    public final C006503b A00;

    public AnonymousClass0GL(C006503b r1) {
        this.A00 = r1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass0GL.class != obj.getClass()) {
            return false;
        }
        return this.A00.equals(((AnonymousClass0GL) obj).A00);
    }

    public int hashCode() {
        return (AnonymousClass0GL.class.getName().hashCode() * 31) + this.A00.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Success {mOutputData=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
