package X;

/* renamed from: X.4Xd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92714Xd {
    public final float A00;
    public final float A01;

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof C92714Xd) {
                C92714Xd r5 = (C92714Xd) obj;
                if (!C16700pc.A0O(Float.valueOf(this.A01), Float.valueOf(r5.A01)) || !C16700pc.A0O(Float.valueOf(this.A00), Float.valueOf(r5.A00))) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Float.floatToIntBits(this.A01) * 31) + Float.floatToIntBits(this.A00);
    }

    public C92714Xd(float f, float f2) {
        this.A01 = f;
        this.A00 = f2;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Size(width=");
        A0k.append(this.A01);
        A0k.append(", height=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
