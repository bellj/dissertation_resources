package X;

import android.os.Build;

/* renamed from: X.01u  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C004101u {
    public static final C004101u A08 = new C004101u(new C003901s());
    public long A00;
    public long A01;
    public AnonymousClass0P0 A02;
    public EnumC004001t A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;
    public boolean A07;

    public C004101u() {
        this.A03 = EnumC004001t.NOT_REQUIRED;
        this.A00 = -1;
        this.A01 = -1;
        this.A02 = new AnonymousClass0P0();
    }

    public C004101u(C003901s r3) {
        this.A03 = EnumC004001t.NOT_REQUIRED;
        this.A00 = -1;
        this.A01 = -1;
        this.A02 = new AnonymousClass0P0();
        this.A05 = false;
        int i = Build.VERSION.SDK_INT;
        this.A06 = false;
        this.A03 = r3.A01;
        this.A04 = r3.A02;
        this.A07 = false;
        if (i >= 24) {
            this.A02 = r3.A00;
            this.A00 = -1;
            this.A01 = -1;
        }
    }

    public C004101u(C004101u r3) {
        this.A03 = EnumC004001t.NOT_REQUIRED;
        this.A00 = -1;
        this.A01 = -1;
        this.A02 = new AnonymousClass0P0();
        this.A05 = r3.A05;
        this.A06 = r3.A06;
        this.A03 = r3.A03;
        this.A04 = r3.A04;
        this.A07 = r3.A07;
        this.A02 = r3.A02;
    }

    public AnonymousClass0P0 A00() {
        return this.A02;
    }

    public void A01(AnonymousClass0P0 r1) {
        this.A02 = r1;
    }

    public void A02(boolean z) {
        this.A06 = z;
    }

    public boolean A03() {
        return this.A02.A00.size() > 0;
    }

    public boolean A04() {
        return this.A06;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && C004101u.class == obj.getClass()) {
            C004101u r7 = (C004101u) obj;
            if (this.A05 == r7.A05 && this.A06 == r7.A06 && this.A04 == r7.A04 && this.A07 == r7.A07 && this.A00 == r7.A00 && this.A01 == r7.A01 && this.A03 == r7.A03) {
                return this.A02.equals(r7.A02);
            }
        }
        return false;
    }

    public int hashCode() {
        long j = this.A00;
        long j2 = this.A01;
        return (((((((((((((this.A03.hashCode() * 31) + (this.A05 ? 1 : 0)) * 31) + (this.A06 ? 1 : 0)) * 31) + (this.A04 ? 1 : 0)) * 31) + (this.A07 ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.A02.hashCode();
    }
}
