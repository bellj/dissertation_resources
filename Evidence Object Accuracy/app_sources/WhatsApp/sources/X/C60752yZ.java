package X;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape8S0100000_I1_2;
import com.whatsapp.R;
import com.whatsapp.conversation.conversationrow.ConversationRowAudioPreview;
import com.whatsapp.search.views.itemviews.AudioPlayerView;
import java.io.File;
import java.util.concurrent.TimeUnit;

/* renamed from: X.2yZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60752yZ extends AbstractC60572yF {
    public AnonymousClass01H A00;
    public AnonymousClass01H A01;
    public AnonymousClass01H A02;
    public final View.OnClickListener A03 = new ViewOnClickCListenerShape8S0100000_I1_2(this, 33);
    public final View A04;
    public final TextView A05;
    public final ConversationRowAudioPreview A06;
    public final AnonymousClass19D A07;
    public final AnonymousClass11P A08;
    public final AudioPlayerView A09;

    public C60752yZ(Context context, AbstractC13890kV r9, AnonymousClass19D r10, AnonymousClass11P r11, C30421Xi r12) {
        super(context, r9, r12);
        this.A07 = r10;
        this.A08 = r11;
        this.A04 = findViewById(R.id.conversation_row_root);
        AudioPlayerView audioPlayerView = (AudioPlayerView) AnonymousClass028.A0D(this, R.id.conversation_row_audio_player_view);
        this.A09 = audioPlayerView;
        this.A06 = (ConversationRowAudioPreview) findViewById(R.id.conversation_row_audio_preview);
        this.A05 = C12960it.A0I(this, R.id.description);
        audioPlayerView.setPlaybackListener(new AnonymousClass3OU(r11, audioPlayerView, new AbstractC116165Uj() { // from class: X.59D
            @Override // X.AbstractC116165Uj
            public final C30421Xi ACs() {
                return C60752yZ.this.getFMessage();
            }
        }, new AnonymousClass34V(this), this.A01));
        View.OnLongClickListener onLongClickListener = this.A1a;
        audioPlayerView.setSeekbarLongClickListener(onLongClickListener);
        audioPlayerView.setOnControlButtonLongClickListener(onLongClickListener);
        A1P();
        int AFz = ((AnonymousClass19F) this.A01.get()).AFz(r12.A11);
        if (AFz >= 0) {
            audioPlayerView.setSeekbarProgress(AFz);
        }
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1P();
    }

    @Override // X.AbstractC42671vd, X.AnonymousClass1OY
    public void A0y() {
        if (((AbstractC28551Oa) this).A0L.A07(931)) {
            C40691s6.A04(this.A04.getRootView(), this.A08, this.A00);
        }
        if (((AbstractC42671vd) this).A01 == null || AnonymousClass1OY.A0U(this)) {
            C30421Xi r3 = (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
            StringBuilder A0k = C12960it.A0k("conversationrowvoicenote/viewmessage ");
            A0k.append(r3.A0z);
            C12960it.A1F(A0k);
            Context context = getContext();
            AnonymousClass59B r4 = new AnonymousClass59B(this);
            AnonymousClass1CY r5 = ((AbstractC28551Oa) this).A0P;
            AnonymousClass009.A05(r5);
            if (AnonymousClass3JG.A03(context, ((AnonymousClass1OY) this).A0J, r3, r4, r5, this.A1O)) {
                C35191hP A00 = this.A07.A00(AnonymousClass12P.A02(this), r3, false);
                A00.A0C(r3);
                A00.A0L = new AnonymousClass55F(this);
                A00.A0F(false);
                A0s();
            }
        }
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A1P();
        } else if (C30041Vv.A13(getFMessage())) {
            A1Q();
        }
    }

    public final void A1P() {
        AbstractView$OnClickListenerC34281fs r0;
        AnonymousClass018 r2;
        String A04;
        File file;
        C30421Xi r8 = (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        C16150oX A00 = AbstractC15340mz.A00(r8);
        this.A04.setContentDescription(C65023Hv.A01(getContext(), ((AnonymousClass1OY) this).A0X, ((AnonymousClass1OY) this).A0Z, this.A0k, ((AbstractC28551Oa) this).A0K, r8));
        TextView textView = this.A05;
        textView.setVisibility(0);
        if (((AbstractC16130oV) r8).A00 == 0) {
            ((AbstractC16130oV) r8).A00 = C22200yh.A07(A00.A0F);
        }
        AbstractC16130oV fMessage = getFMessage();
        if (C30041Vv.A12(fMessage)) {
            ConversationRowAudioPreview conversationRowAudioPreview = this.A06;
            if (conversationRowAudioPreview != null) {
                conversationRowAudioPreview.A00();
            }
            textView.setText(C44891zj.A03(((AbstractC28551Oa) this).A0K, ((AbstractC16130oV) r8).A01));
            AudioPlayerView audioPlayerView = this.A09;
            audioPlayerView.setPlayButtonState(4);
            audioPlayerView.setOnControlButtonClickListener(((AbstractC42671vd) this).A07);
            audioPlayerView.setSeekbarProgress(0);
            r2 = ((AbstractC28551Oa) this).A0K;
        } else if (C30041Vv.A13(fMessage)) {
            if (AnonymousClass1US.A0C(r8.A16()) && (file = A00.A0F) != null) {
                ((AbstractC16130oV) r8).A07 = file.getName();
            }
            if (AnonymousClass1US.A0C(r8.A16())) {
                textView.setVisibility(8);
            } else {
                textView.setText(r8.A16());
            }
            AudioPlayerView audioPlayerView2 = this.A09;
            audioPlayerView2.setSeekbarColor(AnonymousClass00T.A00(getContext(), R.color.music_scrubber));
            A1Q();
            audioPlayerView2.setOnControlButtonClickListener(this.A03);
            A0w();
            A1N(r8);
        } else {
            ConversationRowAudioPreview conversationRowAudioPreview2 = this.A06;
            if (conversationRowAudioPreview2 != null) {
                conversationRowAudioPreview2.A00();
            }
            textView.setText(C44891zj.A03(((AbstractC28551Oa) this).A0K, ((AbstractC16130oV) r8).A01));
            boolean A11 = C30041Vv.A11(r8);
            AudioPlayerView audioPlayerView3 = this.A09;
            if (!A11) {
                audioPlayerView3.setPlayButtonState(2);
                r0 = ((AbstractC42671vd) this).A09;
            } else {
                audioPlayerView3.setPlayButtonState(3);
                r0 = ((AbstractC42671vd) this).A08;
            }
            audioPlayerView3.setOnControlButtonClickListener(r0);
            int i = ((AbstractC16130oV) r8).A00;
            r2 = ((AbstractC28551Oa) this).A0K;
            if (i != 0) {
                A04 = C38131nZ.A04(r2, (long) i);
                setDuration(A04);
                A0w();
                A1N(r8);
            }
        }
        A04 = C44891zj.A03(r2, ((AbstractC16130oV) r8).A01);
        setDuration(A04);
        A0w();
        A1N(r8);
    }

    public final void A1Q() {
        C30421Xi r4 = (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
        AnonymousClass11P r1 = this.A08;
        if (!r1.A0D(r4)) {
            A1R(r4);
            return;
        }
        C35191hP A00 = r1.A00();
        if (A00 != null) {
            if (!A00.A0I()) {
                A1R(r4);
            } else {
                AudioPlayerView audioPlayerView = this.A09;
                audioPlayerView.setPlayButtonState(1);
                audioPlayerView.setSeekbarMax((int) TimeUnit.SECONDS.toMillis((long) ((AbstractC16130oV) r4).A00));
                audioPlayerView.setSeekbarProgress(A00.A02());
                setDuration(C38131nZ.A04(((AbstractC28551Oa) this).A0K, (long) (A00.A02() / 1000)));
                C12980iv.A1L(this.A06);
            }
            AudioPlayerView audioPlayerView2 = this.A09;
            audioPlayerView2.setSeekbarContentDescription((long) A00.A02());
            A00.A0L = new AnonymousClass55F(this);
            A00.A0K = new AnonymousClass2z0(this, this.A06, A00, new AnonymousClass5U0() { // from class: X.55H
                @Override // X.AnonymousClass5U0
                public final void APZ(int i) {
                    C60752yZ r3 = C60752yZ.this;
                    r3.setDuration(C38131nZ.A04(((AbstractC28551Oa) r3).A0K, (long) i));
                }
            }, new AnonymousClass5U1() { // from class: X.55K
                @Override // X.AnonymousClass5U1
                public final void AW0(boolean z) {
                    View findViewById = AnonymousClass12P.A00(C60752yZ.this.getContext()).findViewById(R.id.proximity_overlay);
                    if (findViewById != null) {
                        int i = 4;
                        if (z) {
                            i = 0;
                        }
                        findViewById.setVisibility(i);
                    }
                }
            }, audioPlayerView2);
        }
    }

    public final void A1R(C30421Xi r4) {
        int intValue;
        Number number = (Number) C35191hP.A0y.get(r4.A0z);
        if (number == null) {
            intValue = 0;
        } else {
            intValue = number.intValue();
        }
        AudioPlayerView audioPlayerView = this.A09;
        audioPlayerView.setPlayButtonState(0);
        audioPlayerView.setSeekbarMax(((AbstractC16130oV) r4).A00 * 1000);
        audioPlayerView.setSeekbarProgress(intValue);
        audioPlayerView.setSeekbarContentDescription((long) intValue);
        setDuration(C38131nZ.A04(((AbstractC28551Oa) this).A0K, (long) ((AbstractC16130oV) r4).A00));
        ConversationRowAudioPreview conversationRowAudioPreview = this.A06;
        if (conversationRowAudioPreview != null) {
            conversationRowAudioPreview.A00();
        }
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_audio_left;
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public C30421Xi getFMessage() {
        return (C30421Xi) ((AbstractC16130oV) ((AbstractC28551Oa) this).A0O);
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_audio_left;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_audio_right;
    }

    public void setDuration(String str) {
        ConversationRowAudioPreview conversationRowAudioPreview = this.A06;
        if (conversationRowAudioPreview != null) {
            conversationRowAudioPreview.setDuration(str);
        }
    }

    @Override // X.AbstractC42671vd, X.AbstractC28551Oa
    public void setFMessage(AbstractC15340mz r2) {
        AnonymousClass009.A0F(r2 instanceof C30421Xi);
        super.setFMessage(r2);
    }
}
