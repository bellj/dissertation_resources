package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Handler;

/* renamed from: X.0dy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10070dy implements Runnable {
    public final Throwable A00;
    public final /* synthetic */ Handler A01;
    public final /* synthetic */ AnonymousClass01E A02;
    public final /* synthetic */ Runnable A03;

    public RunnableC10070dy(Handler handler, AnonymousClass01E r6, Runnable runnable) {
        this.A02 = r6;
        this.A01 = handler;
        this.A03 = runnable;
        Thread currentThread = Thread.currentThread();
        StringBuilder sb = new StringBuilder("Runnable instantiated on thread id: ");
        sb.append(currentThread.getId());
        sb.append(", name: ");
        sb.append(currentThread.getName());
        this.A00 = new Throwable(sb.toString());
    }

    @Override // java.lang.Runnable
    public final void run() {
        try {
            Context A0p = this.A02.A0p();
            boolean z = true;
            boolean z2 = false;
            if (A0p == null) {
                z2 = true;
            }
            if (!(A0p instanceof ContextWrapper) || ((ContextWrapper) A0p).getBaseContext() != null) {
                z = false;
            }
            if (z2 || z) {
                this.A01.post(this);
            } else {
                this.A03.run();
            }
        } catch (Throwable th) {
            Throwable th2 = this.A00;
            if (th2 != null) {
                if (AnonymousClass0TU.A01.AJi(5)) {
                    AnonymousClass0TU.A01.Ag0("CDSThreadTracing", "--- start debug trace");
                }
                if (AnonymousClass0TU.A01.AJi(5)) {
                    AnonymousClass0TU.A01.Ag1("CDSThreadTracing", "Thread tracing stacktrace", th2);
                }
                if (AnonymousClass0TU.A01.AJi(5)) {
                    AnonymousClass0TU.A01.Ag0("CDSThreadTracing", "--- end debug trace");
                }
            }
            throw th;
        }
    }
}
