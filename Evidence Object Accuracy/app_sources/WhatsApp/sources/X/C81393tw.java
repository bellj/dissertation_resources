package X;

import android.os.Bundle;

/* renamed from: X.3tw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81393tw extends AbstractC13590jy {
    @Override // X.AbstractC13590jy
    public final boolean A03() {
        return true;
    }

    public C81393tw(int i, Bundle bundle) {
        super(bundle, i, 2);
    }

    @Override // X.AbstractC13590jy
    public final void A00(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            A02(null);
        } else {
            A01(new C13680k9(4, "Invalid response to one way request"));
        }
    }
}
