package X;

import android.util.SparseArray;
import java.util.Iterator;

/* renamed from: X.4v0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106024v0 implements AnonymousClass5XK {
    public C08870bz A00;
    public final SparseArray A01 = new SparseArray();
    public final AnonymousClass4Vh A02;
    public final boolean A03;

    public C106024v0(AnonymousClass4Vh r2, boolean z) {
        this.A02 = r2;
        this.A03 = z;
    }

    public static C08870bz A00(C08870bz r2) {
        C08870bz r0;
        C75853kZ r1;
        try {
            if (!C08870bz.A01(r2) || !(r2.A04() instanceof C75853kZ) || (r1 = (C75853kZ) r2.A04()) == null) {
                r0 = null;
            } else {
                synchronized (r1) {
                    C08870bz r02 = r1.A00;
                    r0 = r02 != null ? r02.A03() : null;
                }
            }
            return r0;
        } finally {
            if (r2 != null) {
                r2.close();
            }
        }
    }

    @Override // X.AnonymousClass5XK
    public synchronized boolean A7c(int i) {
        boolean containsKey;
        AnonymousClass4Vh r0 = this.A02;
        C105914up r3 = r0.A02;
        C105864uk r2 = new C105864uk(r0.A00, i);
        synchronized (r3) {
            AnonymousClass4YA r1 = r3.A04;
            synchronized (r1) {
                containsKey = r1.A02.containsKey(r2);
            }
        }
        return containsKey;
    }

    @Override // X.AnonymousClass5XK
    public synchronized C08870bz AAr(int i, int i2, int i3) {
        C08870bz r0;
        AbstractC12520i3 r1;
        C08870bz r02;
        AnonymousClass4SJ r4;
        boolean z;
        if (!this.A03) {
            r0 = null;
        } else {
            AnonymousClass4Vh r6 = this.A02;
            while (true) {
                synchronized (r6) {
                    r1 = null;
                    Iterator it = r6.A03.iterator();
                    if (it.hasNext()) {
                        r1 = (AbstractC12520i3) it.next();
                        it.remove();
                    }
                }
                if (r1 != null) {
                    C105914up r5 = r6.A02;
                    synchronized (r5) {
                        r4 = (AnonymousClass4SJ) r5.A05.A02(r1);
                        z = true;
                        boolean z2 = false;
                        if (r4 != null) {
                            AnonymousClass4SJ r12 = (AnonymousClass4SJ) r5.A04.A02(r1);
                            if (r12.A00 == 0) {
                                z2 = true;
                            }
                            AnonymousClass0RA.A01(z2);
                            r02 = r12.A02;
                        } else {
                            r02 = null;
                            z = false;
                        }
                    }
                    if (z) {
                        C105914up.A00(r4);
                        continue;
                    }
                    if (r02 != null) {
                        break;
                    }
                } else {
                    r02 = null;
                    break;
                }
            }
            r0 = A00(r02);
        }
        return r0;
    }

    @Override // X.AnonymousClass5XK
    public synchronized C08870bz AB6(int i) {
        AnonymousClass4SJ r2;
        Object obj;
        C08870bz r0;
        AnonymousClass4Vh r02 = this.A02;
        C105914up r4 = r02.A02;
        C105864uk r3 = new C105864uk(r02.A00, i);
        synchronized (r4) {
            r2 = (AnonymousClass4SJ) r4.A05.A02(r3);
            AnonymousClass4YA r1 = r4.A04;
            synchronized (r1) {
                obj = r1.A02.get(r3);
            }
            AnonymousClass4SJ r03 = (AnonymousClass4SJ) obj;
            if (r03 != null) {
                r0 = r4.A01(r03);
            } else {
                r0 = null;
            }
        }
        C105914up.A00(r2);
        r4.A04();
        r4.A03();
        return A00(r0);
    }

    @Override // X.AnonymousClass5XK
    public synchronized C08870bz ACt(int i) {
        C08870bz r0;
        C08870bz r02 = this.A00;
        if (r02 != null) {
            r0 = r02.A03();
        } else {
            r0 = null;
        }
        return A00(r0);
    }

    @Override // X.AnonymousClass5XK
    public synchronized void AQo(C08870bz r6, int i, int i2) {
        C08870bz A00 = C08870bz.A00(C08870bz.A05, new C75853kZ(r6, C94234bT.A03));
        if (A00 != null) {
            C08870bz A002 = this.A02.A00(A00, i);
            if (C08870bz.A01(A002)) {
                SparseArray sparseArray = this.A01;
                C08870bz r0 = (C08870bz) sparseArray.get(i);
                if (r0 != null) {
                    r0.close();
                }
                sparseArray.put(i, A002);
                AnonymousClass0UN.A01(C106024v0.class, Integer.valueOf(i), sparseArray, "cachePreparedFrame(%d) cached. Pending frames: %s");
            }
            A00.close();
        }
    }

    @Override // X.AnonymousClass5XK
    public synchronized void AQq(C08870bz r6, int i, int i2) {
        SparseArray sparseArray = this.A01;
        C08870bz r0 = (C08870bz) sparseArray.get(i);
        if (r0 != null) {
            sparseArray.delete(i);
            r0.close();
            AnonymousClass0UN.A01(C106024v0.class, Integer.valueOf(i), sparseArray, "removePreparedReference(%d) removed. Pending frames: %s");
        }
        C08870bz A00 = C08870bz.A00(C08870bz.A05, new C75853kZ(r6, C94234bT.A03));
        if (A00 != null) {
            C08870bz r02 = this.A00;
            if (r02 != null) {
                r02.close();
            }
            this.A00 = this.A02.A00(A00, i);
            A00.close();
        }
    }

    @Override // X.AnonymousClass5XK
    public synchronized void clear() {
        C08870bz r0 = this.A00;
        if (r0 != null) {
            r0.close();
        }
        this.A00 = null;
        int i = 0;
        while (true) {
            SparseArray sparseArray = this.A01;
            if (i < sparseArray.size()) {
                C08870bz r02 = (C08870bz) sparseArray.valueAt(i);
                if (r02 != null) {
                    r02.close();
                }
                i++;
            } else {
                sparseArray.clear();
            }
        }
    }
}
