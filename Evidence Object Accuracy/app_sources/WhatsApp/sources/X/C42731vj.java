package X;

import android.content.SharedPreferences;

/* renamed from: X.1vj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C42731vj implements AnonymousClass19W {
    public final C18170s1 A00;
    public final AbstractC42721vi A01;

    public C42731vj(C18170s1 r1, AbstractC42721vi r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass19W
    public void AZO(AbstractC44401yr r5) {
        String str;
        C16700pc.A0E(r5, 0);
        C18160s0 r0 = this.A00.A00;
        String A01 = r0.A01();
        Object value = r0.A01.getValue();
        C16700pc.A0B(value);
        String string = ((SharedPreferences) value).getString("pref_avatar_fb_id", null);
        if (A01 == null) {
            str = "The access token is missing";
        } else if (string == null) {
            str = "fbId is missing";
        } else {
            this.A01.A7y(A01, string).AZO(r5);
            return;
        }
        r5.APp(new Exception(str));
    }
}
