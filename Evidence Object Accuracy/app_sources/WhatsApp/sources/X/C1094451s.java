package X;

/* renamed from: X.51s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094451s implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r4, AbstractC94534c0 r5, AnonymousClass4RG r6) {
        if (!(r4 instanceof C83003wX) || !(r5 instanceof C83003wX)) {
            return r4.equals(r5);
        }
        C83003wX A03 = r4.A03();
        C83003wX A032 = r5.A03();
        if (A03 == A032) {
            return true;
        }
        Object obj = A03.A00;
        if (obj != null) {
            if (obj.equals(A032.A08())) {
                return true;
            }
        } else if (A032.A00 == null) {
            return true;
        }
        return false;
    }
}
