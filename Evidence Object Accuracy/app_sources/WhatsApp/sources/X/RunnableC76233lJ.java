package X;

import com.facebook.redex.EmptyBaseRunnable0;

/* renamed from: X.3lJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final /* synthetic */ class RunnableC76233lJ extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ float A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ AnonymousClass4ME A04;

    public /* synthetic */ RunnableC76233lJ(AnonymousClass4ME r1, float f, int i, int i2, int i3) {
        this.A04 = r1;
        this.A01 = i;
        this.A02 = i2;
        this.A03 = i3;
        this.A00 = f;
    }

    @Override // java.lang.Runnable
    public final void run() {
        AnonymousClass4ME r0 = this.A04;
        int i = this.A01;
        int i2 = this.A02;
        int i3 = this.A03;
        r0.A01.AYK(this.A00, i, i2, i3);
    }
}
