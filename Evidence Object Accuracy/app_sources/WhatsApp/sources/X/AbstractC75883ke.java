package X;

import android.util.SparseIntArray;

/* renamed from: X.3ke  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC75883ke extends AbstractC105924uq {
    public final int[] A00;

    public AbstractC75883ke(AbstractC11580gW r5, C93604aR r6, AbstractC115035Pl r7) {
        super(r5, r6, r7);
        SparseIntArray sparseIntArray = r6.A03;
        this.A00 = new int[sparseIntArray.size()];
        int i = 0;
        while (true) {
            int[] iArr = this.A00;
            if (i < iArr.length) {
                iArr[i] = sparseIntArray.keyAt(i);
                i++;
            } else {
                return;
            }
        }
    }
}
