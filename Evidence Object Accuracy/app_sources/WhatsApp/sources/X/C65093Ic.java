package X;

import android.content.Context;

/* renamed from: X.3Ic  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65093Ic {
    public static C65093Ic A0D;
    public final Context A00;
    public final AnonymousClass01c A01;
    public final AnonymousClass4ZA A02;
    public final AnonymousClass4ZB A03;
    public final C87844Df A04;
    public final AnonymousClass4MZ A05;
    public final C18520sa A06;
    public final C64723Gq A07;
    public final C18550sd A08;
    public final C87884Dk A09;
    public final AbstractC17450qp A0A;
    public final C18530sb A0B;
    public final C18570sf A0C;

    public C65093Ic(Context context, AnonymousClass01c r3, AnonymousClass4ZA r4, AnonymousClass4ZB r5, C87844Df r6, C18520sa r7, C64723Gq r8, C18550sd r9, C87884Dk r10, AbstractC17450qp r11, C18530sb r12, C18570sf r13, AnonymousClass4N1 r14) {
        this.A00 = context;
        this.A0A = r11;
        this.A04 = r6;
        this.A09 = r10;
        this.A0B = r12;
        this.A06 = r7;
        this.A08 = r9;
        this.A07 = r8;
        this.A02 = r4;
        this.A0C = r13;
        this.A03 = r5;
        this.A01 = r3;
        this.A05 = new AnonymousClass4MZ(r14);
    }

    public static C65093Ic A00() {
        C65093Ic r0 = A0D;
        if (r0 != null) {
            return r0;
        }
        throw C12960it.A0U("Can't find bloks instance. Is it initialized?");
    }

    public static C90764Pd A01(AnonymousClass4MX r4) {
        return new C90764Pd(r4.A00, new C87824Dd(), r4.A01);
    }

    public static int[] A02(AnonymousClass28D r1) {
        return A00().A06().A00(r1);
    }

    public static int[] A03(AnonymousClass28D r1) {
        return A00().A06().A01(r1);
    }

    public AnonymousClass01c A04() {
        return this.A01;
    }

    public C64723Gq A05() {
        return this.A07;
    }

    public C18570sf A06() {
        C18570sf r0 = this.A0C;
        if (r0 != null) {
            return r0;
        }
        throw C12960it.A0U("No child attribute mapper configured. Unexpectedly attempting to traverse children nodes.");
    }
}
