package X;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.crypto.interfaces.PBEKey;
import javax.crypto.spec.PBEKeySpec;
import javax.security.auth.Destroyable;

/* renamed from: X.5ED  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5ED implements PBEKey, Destroyable {
    public String algorithm;
    public int digest;
    public final AtomicBoolean hasBeenDestroyed = new AtomicBoolean(false);
    public final int iterationCount;
    public int ivSize;
    public int keySize;
    public AnonymousClass1TK oid;
    public final AnonymousClass20L param;
    public final char[] password;
    public final byte[] salt;
    public boolean tryWrong = false;
    public int type;

    public AnonymousClass5ED(String str, PBEKeySpec pBEKeySpec, AnonymousClass1TK r5, AnonymousClass20L r6, int i, int i2, int i3, int i4) {
        this.algorithm = str;
        this.oid = r5;
        this.type = i;
        this.digest = i2;
        this.keySize = i3;
        this.ivSize = i4;
        this.password = pBEKeySpec.getPassword();
        this.iterationCount = pBEKeySpec.getIterationCount();
        this.salt = pBEKeySpec.getSalt();
        this.param = r6;
    }

    @Override // java.security.Key
    public String getAlgorithm() {
        A00(this);
        return this.algorithm;
    }

    @Override // java.security.Key
    public String getFormat() {
        return "RAW";
    }

    @Override // javax.crypto.interfaces.PBEKey
    public int getIterationCount() {
        A00(this);
        return this.iterationCount;
    }

    @Override // javax.crypto.interfaces.PBEKey
    public byte[] getSalt() {
        A00(this);
        return AnonymousClass1TT.A02(this.salt);
    }

    @Override // javax.security.auth.Destroyable
    public boolean isDestroyed() {
        return this.hasBeenDestroyed.get();
    }

    public static void A00(Destroyable destroyable) {
        if (destroyable.isDestroyed()) {
            throw C12960it.A0U("key has been destroyed");
        }
    }

    @Override // javax.security.auth.Destroyable
    public void destroy() {
        if (!this.hasBeenDestroyed.getAndSet(true)) {
            char[] cArr = this.password;
            if (cArr != null) {
                Arrays.fill(cArr, (char) 0);
            }
            byte[] bArr = this.salt;
            if (bArr != null) {
                Arrays.fill(bArr, (byte) 0);
            }
        }
    }

    @Override // java.security.Key
    public byte[] getEncoded() {
        A00(this);
        AnonymousClass20L r1 = this.param;
        if (r1 != null) {
            if (r1 instanceof C113075Fx) {
                r1 = ((C113075Fx) r1).A00;
            }
            return ((AnonymousClass20K) r1).A00;
        }
        int i = this.type;
        if (i == 2) {
            return AbstractC94944cn.A00(this.password);
        }
        char[] cArr = this.password;
        if (i == 5) {
            return AbstractC94944cn.A01(cArr);
        }
        if (cArr == null) {
            return new byte[0];
        }
        int length = cArr.length;
        byte[] bArr = new byte[length];
        for (int i2 = 0; i2 != length; i2++) {
            bArr[i2] = (byte) cArr[i2];
        }
        return bArr;
    }

    @Override // javax.crypto.interfaces.PBEKey
    public char[] getPassword() {
        A00(this);
        char[] cArr = this.password;
        if (cArr != null) {
            return AnonymousClass1TT.A03(cArr);
        }
        throw C12960it.A0U("no password available");
    }
}
