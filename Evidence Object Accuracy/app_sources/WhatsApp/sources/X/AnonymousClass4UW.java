package X;

/* renamed from: X.4UW  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UW {
    public boolean A00() {
        if (this instanceof AnonymousClass40D) {
            return ((AnonymousClass40D) this).A00;
        }
        if (this instanceof AnonymousClass40A) {
            return false;
        }
        if (this instanceof AnonymousClass40C) {
            return ((AnonymousClass40C) this).A00;
        }
        if ((this instanceof AnonymousClass409) || !(this instanceof AnonymousClass40B)) {
            return false;
        }
        return ((AnonymousClass40B) this).A00;
    }
}
