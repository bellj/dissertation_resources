package X;

import android.content.Context;
import java.lang.ref.WeakReference;

/* renamed from: X.21J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21J implements AnonymousClass21I {
    public final AnonymousClass049 A00;

    public AnonymousClass21J(Context context) {
        this.A00 = new AnonymousClass049(context);
    }

    @Override // X.AnonymousClass21I
    public void A6I(AnonymousClass02N r5, AnonymousClass21K r6) {
        WeakReference weakReference = new WeakReference(r6);
        try {
            this.A00.A04(new AnonymousClass21L(this, weakReference), null, r5);
        } catch (NullPointerException e) {
            e.getMessage();
            AnonymousClass21K r0 = (AnonymousClass21K) weakReference.get();
            if (r0 != null) {
                r0.AMc();
            }
        }
    }

    @Override // X.AnonymousClass21I
    public boolean A6u() {
        AnonymousClass049 r1 = this.A00;
        return r1.A06() && r1.A05();
    }

    @Override // X.AnonymousClass21I
    public boolean AIC() {
        return this.A00.A05();
    }

    @Override // X.AnonymousClass21I
    public boolean AJT() {
        return this.A00.A06();
    }
}
