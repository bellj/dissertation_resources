package X;

import android.graphics.Bitmap;
import android.net.Uri;
import com.whatsapp.gallery.MediaGalleryFragmentBase;

/* renamed from: X.3X3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3X3 implements AnonymousClass23D {
    public final /* synthetic */ AbstractC35611iN A00;
    public final /* synthetic */ C54972ha A01;

    public AnonymousClass3X3(AbstractC35611iN r1, C54972ha r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass23D
    public String AH5() {
        Uri AAE = this.A00.AAE();
        StringBuilder A0h = C12960it.A0h();
        A0h.append(AAE);
        String A0d = C12960it.A0d("-gallery_thumb", A0h);
        C16700pc.A0B(A0d);
        return A0d;
    }

    @Override // X.AnonymousClass23D
    public Bitmap AKU() {
        C54972ha r2 = this.A01;
        if (!C16700pc.A0O(r2.A03.getTag(), this)) {
            return null;
        }
        Bitmap Aem = this.A00.Aem(r2.A00);
        return Aem == null ? MediaGalleryFragmentBase.A0U : Aem;
    }
}
