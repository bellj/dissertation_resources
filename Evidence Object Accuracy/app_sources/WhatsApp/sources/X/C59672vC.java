package X;

import com.whatsapp.businessdirectory.view.fragment.BusinessDirectorySearchQueryFragment;

/* renamed from: X.2vC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59672vC extends AbstractC75133jM {
    public final /* synthetic */ BusinessDirectorySearchQueryFragment A00;

    public C59672vC(BusinessDirectorySearchQueryFragment businessDirectorySearchQueryFragment) {
        this.A00 = businessDirectorySearchQueryFragment;
    }

    @Override // X.AbstractC75133jM
    public void A02() {
        this.A00.A09.A09();
    }

    @Override // X.AbstractC75133jM
    public boolean A03() {
        C63363Bh r0 = (C63363Bh) this.A00.A09.A0P.A04.A01();
        return r0 == null || r0.A07;
    }
}
