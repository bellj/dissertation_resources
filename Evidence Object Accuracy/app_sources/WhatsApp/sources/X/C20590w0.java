package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.0w0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C20590w0 {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C15450nH A02;
    public final AnonymousClass13T A03;
    public final C14830m7 A04;
    public final C14820m6 A05;
    public final AnonymousClass1YL A06 = new AnonymousClass1YL() { // from class: X.1Yh
        @Override // X.AnonymousClass1YL
        public final AnonymousClass1YM A7t(AbstractC15590nW r13) {
            UserJid userJid;
            C20590w0 r8 = C20590w0.this;
            AnonymousClass1YM r3 = new AnonymousClass1YM(r13);
            AbstractC15590nW r5 = r3.A03;
            ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
            C16310on A01 = r8.A08.get();
            try {
                Cursor A09 = A01.A03.A09("SELECT jid, admin, pending, sent_sender_key FROM group_participants WHERE gjid = ?", new String[]{r5.getRawString()});
                while (A09.moveToNext()) {
                    try {
                        String string = A09.getString(0);
                        if (TextUtils.isEmpty(string)) {
                            C15570nT r0 = r8.A01;
                            r0.A08();
                            userJid = r0.A05;
                            AnonymousClass009.A05(userJid);
                        } else {
                            userJid = UserJid.get(string);
                        }
                        int i = A09.isNull(3) ? 0 : A09.getInt(3);
                        int i2 = A09.getInt(1);
                        boolean z = false;
                        if (A09.getInt(2) == 1) {
                            z = true;
                        }
                        boolean z2 = false;
                        if (i == 1) {
                            z2 = true;
                        }
                        AnonymousClass1YO r1 = new AnonymousClass1YO(userJid, i2, z, z2);
                        concurrentHashMap.put(r1.A03, r1);
                    } catch (AnonymousClass1MW e) {
                        Log.e("ParticipantMessageStore/getGroupParticipants invalid jid from db", e);
                    }
                }
                A09.close();
                A01.close();
                r3.A02 = concurrentHashMap;
                r3.A0D();
                Iterator it = r3.A07().iterator();
                int i3 = 0;
                while (it.hasNext()) {
                    ((AnonymousClass1YO) it.next()).A00 = i3;
                    i3++;
                }
                return r3;
            } catch (Throwable th) {
                try {
                    A01.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    };
    public final C245215v A07;
    public final C16490p7 A08;
    public final C21390xL A09;

    public C20590w0(AbstractC15710nm r2, C15570nT r3, C15450nH r4, AnonymousClass13T r5, C14830m7 r6, C14820m6 r7, C245215v r8, C16490p7 r9, C21390xL r10) {
        this.A04 = r6;
        this.A00 = r2;
        this.A01 = r3;
        this.A02 = r4;
        this.A09 = r10;
        this.A03 = r5;
        this.A08 = r9;
        this.A05 = r7;
        this.A07 = r8;
    }

    public Set A00(AbstractC15590nW r13) {
        HashSet hashSet = new HashSet();
        C16310on A01 = this.A08.get();
        try {
            Cursor A08 = A01.A03.A08("group_participants", "gjid = ?", null, null, new String[]{"jid"}, new String[]{r13.getRawString()});
            while (A08.moveToNext()) {
                String string = A08.getString(0);
                if (TextUtils.isEmpty(string)) {
                    C15570nT r0 = this.A01;
                    r0.A08();
                    C27631Ih r02 = r0.A05;
                    AnonymousClass009.A05(r02);
                    hashSet.add(r02);
                } else {
                    try {
                        hashSet.add(UserJid.get(string));
                    } catch (AnonymousClass1MW unused) {
                    }
                }
            }
            A08.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused2) {
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:60:0x00bc */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.util.List, java.util.Collection] */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00e6 A[Catch: all -> 0x0106, TryCatch #4 {all -> 0x010d, blocks: (B:5:0x0022, B:38:0x00f8, B:41:0x00ff, B:6:0x004f, B:8:0x0055, B:10:0x007b, B:12:0x0084, B:13:0x0090, B:15:0x009f, B:17:0x00a7, B:18:0x00b2, B:26:0x00c5, B:28:0x00cb, B:30:0x00dc, B:31:0x00e0, B:34:0x00e6, B:36:0x00ee), top: B:54:0x0022 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00ff A[EDGE_INSN: B:56:0x00ff->B:41:0x00ff ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Set A01(X.AbstractC15590nW r27, java.lang.String r28) {
        /*
        // Method dump skipped, instructions count: 275
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C20590w0.A01(X.0nW, java.lang.String):java.util.Set");
    }

    public Set A02(UserJid userJid) {
        String rawString;
        HashSet hashSet = new HashSet();
        if (this.A01.A0F(userJid)) {
            rawString = "";
        } else {
            rawString = userJid.getRawString();
        }
        C16310on A01 = this.A08.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT gjid FROM group_participants WHERE jid = ? AND sent_sender_key = 1", new String[]{rawString});
            while (A09.moveToNext()) {
                AbstractC15590nW A05 = AbstractC15590nW.A05(A09.getString(0));
                if (A05 != null) {
                    hashSet.add(A05);
                }
            }
            A09.close();
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A03(AnonymousClass1YO r7, AnonymousClass1YM r8, boolean z) {
        DeviceJid of = DeviceJid.of(r7.A03);
        ConcurrentHashMap concurrentHashMap = r7.A04;
        if (concurrentHashMap.get(of) == null) {
            AbstractC15710nm r3 = this.A00;
            StringBuilder sb = new StringBuilder("grp=");
            sb.append(r8.A03);
            sb.append("participants=");
            sb.append(r7.toString());
            sb.append("props=");
            sb.append(this.A09.A00("participant_user_ready", 0));
            sb.append("/");
            sb.append(2);
            r3.AaV("participant-message-store/group-participant-without-device", sb.toString(), true);
        }
        Iterator it = AnonymousClass1JO.A00(concurrentHashMap.values()).iterator();
        while (it.hasNext()) {
            ((AnonymousClass1YP) it.next()).A00 = z;
        }
    }

    public void A04(AnonymousClass1YM r4) {
        Iterator it = r4.A07().iterator();
        while (it.hasNext()) {
            A03((AnonymousClass1YO) it.next(), r4, false);
        }
        AbstractC15590nW r2 = r4.A03;
        A05(r2);
        AnonymousClass13T r0 = this.A03;
        r0.A01.A01(new C30641Yf(r2));
    }

    public final void A05(AbstractC15590nW r9) {
        StringBuilder sb = new StringBuilder("msgstore/setParticipantsHaveSenderKey/");
        sb.append(r9);
        sb.append(" value:");
        sb.append(false);
        Log.i(sb.toString());
        C16310on A02 = this.A08.A02();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("sent_sender_key", (Boolean) false);
            A02.A03.A00("group_participants", contentValues, "gjid = ?", new String[]{r9.getRawString()});
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final void A06(AbstractC15590nW r7, String str, String str2, Collection collection, int i) {
        String join = TextUtils.join(",", collection);
        if (!TextUtils.equals(str, str2)) {
            ContentValues contentValues = new ContentValues(6);
            contentValues.put("timestamp", Long.valueOf(this.A04.A00() / 1000));
            contentValues.put("gjid", r7.getRawString());
            contentValues.put("jid", join);
            contentValues.put("action", Integer.valueOf(i));
            contentValues.put("old_phash", str);
            contentValues.put("new_phash", str2);
            C16310on A02 = this.A08.A02();
            try {
                A02.A03.A02(contentValues, "group_participants_history");
                A02.close();
            } catch (Throwable th) {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        }
    }
}
