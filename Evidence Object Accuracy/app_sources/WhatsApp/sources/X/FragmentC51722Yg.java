package X;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import com.facebook.redex.RunnableBRunnable0Shape1S1200000_I1;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: X.2Yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class FragmentC51722Yg extends Fragment implements AbstractC116815Wz {
    public static final WeakHashMap A03 = new WeakHashMap();
    public int A00 = 0;
    public Bundle A01;
    public final Map A02 = Collections.synchronizedMap(new AnonymousClass00N());

    @Override // X.AbstractC116815Wz
    public final void A5e(LifecycleCallback lifecycleCallback, String str) {
        Map map = this.A02;
        if (!map.containsKey("ConnectionlessLifecycleHelper")) {
            map.put("ConnectionlessLifecycleHelper", lifecycleCallback);
            if (this.A00 > 0) {
                new HandlerC73383g9(Looper.getMainLooper()).post(new RunnableBRunnable0Shape1S1200000_I1(lifecycleCallback, this));
                return;
            }
            return;
        }
        StringBuilder A0t = C12980iv.A0t(C12970iu.A07("ConnectionlessLifecycleHelper") + 59);
        A0t.append("LifecycleCallback with tag ");
        A0t.append("ConnectionlessLifecycleHelper");
        throw C12970iu.A0f(C12960it.A0d(" already added to this fragment.", A0t));
    }

    @Override // X.AbstractC116815Wz
    public final LifecycleCallback AB9(Class cls, String str) {
        return (LifecycleCallback) DialogInterface$OnCancelListenerC56312kg.class.cast(this.A02.get("ConnectionlessLifecycleHelper"));
    }

    @Override // X.AbstractC116815Wz
    public final Activity ADs() {
        return getActivity();
    }

    @Override // android.app.Fragment
    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        Iterator A0o = C12960it.A0o(this.A02);
        while (A0o.hasNext()) {
            A0o.next();
        }
    }

    @Override // android.app.Fragment
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Iterator A0o = C12960it.A0o(this.A02);
        while (A0o.hasNext()) {
            ((LifecycleCallback) A0o.next()).A03(i, i2, intent);
        }
    }

    @Override // android.app.Fragment
    public final void onCreate(Bundle bundle) {
        Bundle bundle2;
        super.onCreate(bundle);
        this.A00 = 1;
        this.A01 = bundle;
        Iterator A0n = C12960it.A0n(this.A02);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            LifecycleCallback lifecycleCallback = (LifecycleCallback) A15.getValue();
            if (bundle != null) {
                bundle2 = bundle.getBundle(C12990iw.A0r(A15));
            } else {
                bundle2 = null;
            }
            lifecycleCallback.A04(bundle2);
        }
    }

    @Override // android.app.Fragment
    public final void onDestroy() {
        super.onDestroy();
        this.A00 = 5;
        Iterator A0o = C12960it.A0o(this.A02);
        while (A0o.hasNext()) {
            A0o.next();
        }
    }

    @Override // android.app.Fragment
    public final void onResume() {
        super.onResume();
        this.A00 = 3;
        Iterator A0o = C12960it.A0o(this.A02);
        while (A0o.hasNext()) {
            ((LifecycleCallback) A0o.next()).A00();
        }
    }

    @Override // android.app.Fragment
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            Iterator A0n = C12960it.A0n(this.A02);
            while (A0n.hasNext()) {
                Map.Entry A15 = C12970iu.A15(A0n);
                Bundle A0D = C12970iu.A0D();
                ((LifecycleCallback) A15.getValue()).A05(A0D);
                bundle.putBundle(C12990iw.A0r(A15), A0D);
            }
        }
    }

    @Override // android.app.Fragment
    public final void onStart() {
        super.onStart();
        this.A00 = 2;
        Iterator A0o = C12960it.A0o(this.A02);
        while (A0o.hasNext()) {
            ((LifecycleCallback) A0o.next()).A01();
        }
    }

    @Override // android.app.Fragment
    public final void onStop() {
        super.onStop();
        this.A00 = 4;
        Iterator A0o = C12960it.A0o(this.A02);
        while (A0o.hasNext()) {
            ((LifecycleCallback) A0o.next()).A02();
        }
    }
}
