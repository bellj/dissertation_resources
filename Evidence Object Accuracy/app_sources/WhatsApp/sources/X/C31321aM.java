package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.1aM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31321aM extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C31321aM A0E;
    public static volatile AnonymousClass255 A0F;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public AbstractC27881Jp A05;
    public AbstractC27881Jp A06;
    public AbstractC27881Jp A07;
    public AbstractC27881Jp A08;
    public AnonymousClass1K6 A09 = AnonymousClass277.A01;
    public C31881bG A0A;
    public C57582nI A0B;
    public C31841bC A0C;
    public boolean A0D;

    static {
        C31321aM r0 = new C31321aM();
        A0E = r0;
        r0.A0W();
    }

    public C31321aM() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A06 = r1;
        this.A07 = r1;
        this.A08 = r1;
        this.A05 = r1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        C32031bV r1;
        C82493vi r12;
        C31891bH r13;
        switch (r8.ordinal()) {
            case 0:
                return A0E;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                C31321aM r10 = (C31321aM) obj2;
                int i = this.A00;
                boolean z = true;
                if ((i & 1) != 1) {
                    z = false;
                }
                int i2 = this.A04;
                int i3 = r10.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A04 = r9.Afp(i2, r10.A04, z, z2);
                boolean z3 = false;
                if ((i & 2) == 2) {
                    z3 = true;
                }
                AbstractC27881Jp r2 = this.A06;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A06 = r9.Afm(r2, r10.A06, z3, z4);
                boolean z5 = false;
                if ((this.A00 & 4) == 4) {
                    z5 = true;
                }
                AbstractC27881Jp r3 = this.A07;
                boolean z6 = false;
                if ((r10.A00 & 4) == 4) {
                    z6 = true;
                }
                this.A07 = r9.Afm(r3, r10.A07, z5, z6);
                boolean z7 = false;
                if ((this.A00 & 8) == 8) {
                    z7 = true;
                }
                AbstractC27881Jp r32 = this.A08;
                boolean z8 = false;
                if ((r10.A00 & 8) == 8) {
                    z8 = true;
                }
                this.A08 = r9.Afm(r32, r10.A08, z7, z8);
                boolean z9 = false;
                if ((this.A00 & 16) == 16) {
                    z9 = true;
                }
                int i4 = this.A02;
                boolean z10 = false;
                if ((r10.A00 & 16) == 16) {
                    z10 = true;
                }
                this.A02 = r9.Afp(i4, r10.A02, z9, z10);
                this.A0A = (C31881bG) r9.Aft(this.A0A, r10.A0A);
                this.A09 = r9.Afr(this.A09, r10.A09);
                this.A0B = (C57582nI) r9.Aft(this.A0B, r10.A0B);
                this.A0C = (C31841bC) r9.Aft(this.A0C, r10.A0C);
                int i5 = this.A00;
                boolean z11 = false;
                if ((i5 & 256) == 256) {
                    z11 = true;
                }
                int i6 = this.A03;
                int i7 = r10.A00;
                boolean z12 = false;
                if ((i7 & 256) == 256) {
                    z12 = true;
                }
                this.A03 = r9.Afp(i6, r10.A03, z11, z12);
                boolean z13 = false;
                if ((i5 & 512) == 512) {
                    z13 = true;
                }
                int i8 = this.A01;
                boolean z14 = false;
                if ((i7 & 512) == 512) {
                    z14 = true;
                }
                this.A01 = r9.Afp(i8, r10.A01, z13, z14);
                boolean z15 = false;
                if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z15 = true;
                }
                boolean z16 = this.A0D;
                boolean z17 = false;
                if ((i7 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
                    z17 = true;
                }
                this.A0D = r9.Afl(z15, z16, z17, r10.A0D);
                boolean z18 = false;
                if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z18 = true;
                }
                AbstractC27881Jp r22 = this.A05;
                boolean z19 = false;
                if ((i7 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
                    z19 = true;
                }
                this.A05 = r9.Afm(r22, r10.A05, z18, z19);
                if (r9 == C463025i.A00) {
                    this.A00 |= r10.A00;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                AnonymousClass254 r102 = (AnonymousClass254) obj2;
                while (true) {
                    try {
                        int A03 = r92.A03();
                        switch (A03) {
                            case 0:
                                break;
                            case 8:
                                this.A00 |= 1;
                                this.A04 = r92.A02();
                                break;
                            case 18:
                                this.A00 |= 2;
                                this.A06 = r92.A08();
                                break;
                            case 26:
                                this.A00 |= 4;
                                this.A07 = r92.A08();
                                break;
                            case 34:
                                this.A00 |= 8;
                                this.A08 = r92.A08();
                                break;
                            case 40:
                                this.A00 |= 16;
                                this.A02 = r92.A02();
                                break;
                            case SearchActionVerificationClientService.TIME_TO_SLEEP_IN_MS /* 50 */:
                                if ((this.A00 & 32) == 32) {
                                    r13 = (C31891bH) this.A0A.A0T();
                                } else {
                                    r13 = null;
                                }
                                C31881bG r0 = (C31881bG) r92.A09(r102, C31881bG.A05.A0U());
                                this.A0A = r0;
                                if (r13 != null) {
                                    r13.A04(r0);
                                    this.A0A = (C31881bG) r13.A01();
                                }
                                this.A00 |= 32;
                                break;
                            case 58:
                                AnonymousClass1K6 r14 = this.A09;
                                if (!((AnonymousClass1K7) r14).A00) {
                                    r14 = AbstractC27091Fz.A0G(r14);
                                    this.A09 = r14;
                                }
                                r14.add((C31881bG) r92.A09(r102, C31881bG.A05.A0U()));
                                break;
                            case 66:
                                if ((this.A00 & 64) == 64) {
                                    r12 = (C82493vi) this.A0B.A0T();
                                } else {
                                    r12 = null;
                                }
                                C57582nI r02 = (C57582nI) r92.A09(r102, C57582nI.A08.A0U());
                                this.A0B = r02;
                                if (r12 != null) {
                                    r12.A04(r02);
                                    this.A0B = (C57582nI) r12.A01();
                                }
                                this.A00 |= 64;
                                break;
                            case 74:
                                if ((this.A00 & 128) == 128) {
                                    r1 = (C32031bV) this.A0C.A0T();
                                } else {
                                    r1 = null;
                                }
                                C31841bC r03 = (C31841bC) r92.A09(r102, C31841bC.A04.A0U());
                                this.A0C = r03;
                                if (r1 != null) {
                                    r1.A04(r03);
                                    this.A0C = (C31841bC) r1.A01();
                                }
                                this.A00 |= 128;
                                break;
                            case 80:
                                this.A00 |= 256;
                                this.A03 = r92.A02();
                                break;
                            case 88:
                                this.A00 |= 512;
                                this.A01 = r92.A02();
                                break;
                            case 96:
                                this.A00 |= EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH;
                                this.A0D = r92.A0F();
                                break;
                            case 106:
                                this.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
                                this.A05 = r92.A08();
                                break;
                            default:
                                if (A0a(r92, A03)) {
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } catch (C28971Pt e) {
                        e.unfinishedMessage = this;
                        throw new RuntimeException(e);
                    } catch (IOException e2) {
                        C28971Pt r15 = new C28971Pt(e2.getMessage());
                        r15.unfinishedMessage = this;
                        throw new RuntimeException(r15);
                    }
                }
            case 3:
                ((AnonymousClass1K7) this.A09).A00 = false;
                return null;
            case 4:
                return new C31321aM();
            case 5:
                return new C31901bI();
            case 6:
                break;
            case 7:
                if (A0F == null) {
                    synchronized (C31321aM.class) {
                        if (A0F == null) {
                            A0F = new AnonymousClass255(A0E);
                        }
                    }
                }
                return A0F;
            default:
                throw new UnsupportedOperationException();
        }
        return A0E;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i;
        int i2 = ((AbstractC27091Fz) this).A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i = CodedOutputStream.A04(1, this.A04) + 0;
        } else {
            i = 0;
        }
        if ((i3 & 2) == 2) {
            i += CodedOutputStream.A09(this.A06, 2);
        }
        if ((i3 & 4) == 4) {
            i += CodedOutputStream.A09(this.A07, 3);
        }
        if ((i3 & 8) == 8) {
            i += CodedOutputStream.A09(this.A08, 4);
        }
        if ((i3 & 16) == 16) {
            i += CodedOutputStream.A04(5, this.A02);
        }
        if ((i3 & 32) == 32) {
            C31881bG r0 = this.A0A;
            if (r0 == null) {
                r0 = C31881bG.A05;
            }
            i += CodedOutputStream.A0A(r0, 6);
        }
        for (int i4 = 0; i4 < this.A09.size(); i4++) {
            i += CodedOutputStream.A0A((AnonymousClass1G1) this.A09.get(i4), 7);
        }
        if ((this.A00 & 64) == 64) {
            C57582nI r02 = this.A0B;
            if (r02 == null) {
                r02 = C57582nI.A08;
            }
            i += CodedOutputStream.A0A(r02, 8);
        }
        if ((this.A00 & 128) == 128) {
            C31841bC r03 = this.A0C;
            if (r03 == null) {
                r03 = C31841bC.A04;
            }
            i += CodedOutputStream.A0A(r03, 9);
        }
        int i5 = this.A00;
        if ((i5 & 256) == 256) {
            i += CodedOutputStream.A04(10, this.A03);
        }
        if ((i5 & 512) == 512) {
            i += CodedOutputStream.A04(11, this.A01);
        }
        if ((i5 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            i += CodedOutputStream.A00(12);
        }
        if ((i5 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            i += CodedOutputStream.A09(this.A05, 13);
        }
        int A00 = i + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0F(1, this.A04);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0K(this.A06, 2);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0K(this.A07, 3);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0K(this.A08, 4);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0F(5, this.A02);
        }
        if ((this.A00 & 32) == 32) {
            C31881bG r0 = this.A0A;
            if (r0 == null) {
                r0 = C31881bG.A05;
            }
            codedOutputStream.A0L(r0, 6);
        }
        for (int i = 0; i < this.A09.size(); i++) {
            codedOutputStream.A0L((AnonymousClass1G1) this.A09.get(i), 7);
        }
        if ((this.A00 & 64) == 64) {
            C57582nI r02 = this.A0B;
            if (r02 == null) {
                r02 = C57582nI.A08;
            }
            codedOutputStream.A0L(r02, 8);
        }
        if ((this.A00 & 128) == 128) {
            C31841bC r03 = this.A0C;
            if (r03 == null) {
                r03 = C31841bC.A04;
            }
            codedOutputStream.A0L(r03, 9);
        }
        if ((this.A00 & 256) == 256) {
            codedOutputStream.A0F(10, this.A03);
        }
        if ((this.A00 & 512) == 512) {
            codedOutputStream.A0F(11, this.A01);
        }
        if ((this.A00 & EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == 1024) {
            codedOutputStream.A0J(12, this.A0D);
        }
        if ((this.A00 & EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH) == 2048) {
            codedOutputStream.A0K(this.A05, 13);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
