package X;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.3co  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C71363co implements Iterator, AbstractC16910px {
    public int A00;
    public int A01;
    public int A02;
    public int A03 = -1;
    public C114075Kc A04;
    public final /* synthetic */ C112845Ey A05;

    public C71363co(C112845Ey r5) {
        this.A05 = r5;
        int length = r5.A01.length();
        if (0 <= length) {
            this.A01 = 0;
            this.A02 = 0;
            return;
        }
        StringBuilder A0k = C12960it.A0k("Cannot coerce value to an empty range: maximum ");
        A0k.append(length);
        A0k.append(" is less than minimum ");
        A0k.append(0);
        A0k.append('.');
        throw C12970iu.A0f(A0k.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        if (r0 < r1) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A00() {
        /*
            r8 = this;
            int r2 = r8.A02
            r6 = 0
            if (r2 >= 0) goto L_0x000b
            r8.A03 = r6
            r0 = 0
            r8.A04 = r0
            return
        L_0x000b:
            X.5Ey r7 = r8.A05
            int r1 = r7.A00
            r4 = -1
            r5 = 1
            if (r1 <= 0) goto L_0x001a
            int r0 = r8.A00
            int r0 = r0 + r5
            r8.A00 = r0
            if (r0 >= r1) goto L_0x006c
        L_0x001a:
            java.lang.CharSequence r3 = r7.A01
            int r0 = r3.length()
            if (r2 > r0) goto L_0x006c
            X.5ZQ r1 = r7.A02
            int r0 = r8.A02
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r1 = r1.AJ5(r3, r0)
            X.0qw r1 = (X.C17520qw) r1
            if (r1 != 0) goto L_0x0046
            int r2 = r8.A01
            int r0 = r3.length()
            int r1 = r0 + -1
        L_0x003a:
            X.5Kc r0 = new X.5Kc
            r0.<init>(r2, r1)
            r8.A04 = r0
        L_0x0041:
            r8.A02 = r4
            r8.A03 = r5
            return
        L_0x0046:
            java.lang.Object r0 = r1.first
            int r4 = X.C12960it.A05(r0)
            java.lang.Object r0 = r1.second
            int r3 = X.C12960it.A05(r0)
            int r2 = r8.A01
            r0 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r4 > r0) goto L_0x0064
            X.5Kc r1 = X.C114075Kc.A00
        L_0x005a:
            r8.A04 = r1
            int r4 = r4 + r3
            r8.A01 = r4
            if (r3 != 0) goto L_0x0062
            r6 = 1
        L_0x0062:
            int r4 = r4 + r6
            goto L_0x0041
        L_0x0064:
            int r0 = r4 + -1
            X.5Kc r1 = new X.5Kc
            r1.<init>(r2, r0)
            goto L_0x005a
        L_0x006c:
            int r2 = r8.A01
            java.lang.CharSequence r0 = r7.A01
            int r1 = X.AnonymousClass03B.A00(r0)
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C71363co.A00():void");
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        if (this.A03 == -1) {
            A00();
        }
        return C12970iu.A1W(this.A03);
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        if (this.A03 == -1) {
            A00();
        }
        if (this.A03 != 0) {
            C114075Kc r1 = this.A04;
            if (r1 != null) {
                this.A04 = null;
                this.A03 = -1;
                return r1;
            }
            throw C12980iv.A0n("null cannot be cast to non-null type kotlin.ranges.IntRange");
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator
    public void remove() {
        throw C12980iv.A0u("Operation is not supported for read-only collection");
    }
}
