package X;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: X.1Du  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C26521Du {
    public final AnonymousClass14P A00;

    public C26521Du(AnonymousClass14P r1) {
        this.A00 = r1;
    }

    public static Bitmap A00(C38901ot r13, File file) {
        boolean z;
        Throwable e;
        String str;
        Bitmap A0B;
        byte[] embeddedPicture;
        if (file == null) {
            Log.e("mediafileutils/createVideoThumbnail/file=null");
            return null;
        }
        try {
            C38911ou.A03(file);
            z = true;
        } catch (IOException unused) {
            z = false;
        }
        if (z) {
            try {
                try {
                    C38911ou A00 = C38911ou.A00(ParcelFileDescriptor.open(file, 268435456), true);
                    try {
                        Bitmap A04 = A00.A04(0);
                        A00.close();
                        return A04;
                    } catch (Throwable th) {
                        try {
                            A00.close();
                        } catch (Throwable unused2) {
                        }
                        throw th;
                    }
                } catch (IOException | IllegalArgumentException e2) {
                    e = e2;
                    str = "mediafileutils/createGifThumbnail/gif file not read ";
                    Log.e(str, e);
                    return null;
                }
            } catch (Exception e3) {
                e = e3;
                str = "mediafileutils/createGifThumbnail/unexpected gif exception ";
                Log.e(str, e);
                return null;
            }
        } else {
            int i = r13.A00;
            boolean z2 = r13.A01;
            C38931ow r0 = new C38931ow(file);
            try {
                C38941ox r8 = new C38941ox();
                try {
                    r8.setDataSource(r0.A00.getAbsolutePath());
                    int i2 = Build.VERSION.SDK_INT;
                    Bitmap scaledFrameAtTime = (i2 < 27 || i <= 0) ? null : r8.getScaledFrameAtTime(0, 0, i, i);
                    if (scaledFrameAtTime == null) {
                        scaledFrameAtTime = r8.getFrameAtTime(0);
                    }
                    if (scaledFrameAtTime == null) {
                        scaledFrameAtTime = r8.getFrameAtTime();
                    }
                    if (scaledFrameAtTime == null && (embeddedPicture = r8.getEmbeddedPicture()) != null) {
                        scaledFrameAtTime = BitmapFactory.decodeByteArray(embeddedPicture, 0, embeddedPicture.length, C22200yh.A02);
                    }
                    if (!(scaledFrameAtTime == null || i <= 0 || scaledFrameAtTime == (A0B = C22200yh.A0B(scaledFrameAtTime, null, i, i)))) {
                        scaledFrameAtTime.recycle();
                        scaledFrameAtTime = A0B;
                    }
                    if (scaledFrameAtTime != null && (i2 <= 19 || (z2 && scaledFrameAtTime.getConfig() != Bitmap.Config.ARGB_8888))) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        scaledFrameAtTime.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
                        scaledFrameAtTime.recycle();
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inDither = true;
                        options.inInputShareable = true;
                        options.inPurgeable = true;
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        scaledFrameAtTime = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, C22200yh.A02);
                    }
                    if (scaledFrameAtTime == null) {
                        Log.w("mediafileutils/createVideoThumbnail/no bitmap created");
                    }
                    r8.close();
                    return scaledFrameAtTime;
                } catch (Throwable th2) {
                    try {
                        r8.close();
                    } catch (Throwable unused3) {
                    }
                    throw th2;
                }
            } catch (IOException e4) {
                Log.e("mediafileutils/createVideoThumbnail/unable to load video", e4);
                return null;
            } catch (RuntimeException e5) {
                Log.e("mediafileutils/createVideoThumbnail/corrupt video file", e5);
                return null;
            }
        }
    }

    public static Bitmap A01(File file) {
        return A00(new C38901ot(-1, false), file);
    }

    public static Bitmap A02(File file, long j) {
        if (j == 0) {
            return A01(file);
        }
        try {
            C38941ox r1 = new C38941ox();
            try {
                r1.setDataSource(file.getAbsolutePath());
                Bitmap frameAtTime = r1.getFrameAtTime(j);
                r1.close();
                return frameAtTime;
            } catch (Throwable th) {
                try {
                    r1.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (Exception | NoSuchMethodError unused2) {
            return null;
        }
    }

    public static byte[] A03(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("orig_thumbnail/width:");
        sb.append(bitmap.getWidth());
        sb.append("/height:");
        sb.append(bitmap.getHeight());
        Log.i(sb.toString());
        if (bitmap.getWidth() > 100 || bitmap.getHeight() > 100) {
            float max = Math.max(((float) bitmap.getWidth()) / 100.0f, ((float) bitmap.getHeight()) / 100.0f);
            Rect rect = new Rect(0, 0, (int) (((float) bitmap.getWidth()) / max), (int) (((float) bitmap.getHeight()) / max));
            rect.right = Math.max(rect.right, 1);
            rect.bottom = Math.max(rect.bottom, 1);
            Rect rect2 = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            Bitmap.Config config = bitmap.getConfig();
            try {
                int width = rect.width();
                int height = rect.height();
                if (config == null) {
                    config = Bitmap.Config.ARGB_8888;
                }
                Bitmap createBitmap = Bitmap.createBitmap(width, height, config);
                Canvas canvas = new Canvas(createBitmap);
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setFilterBitmap(true);
                paint.setDither(true);
                canvas.drawBitmap(bitmap, rect2, rect, paint);
                bitmap.recycle();
                StringBuilder sb2 = new StringBuilder("rescaled_thumbnail/width:");
                sb2.append(createBitmap.getWidth());
                sb2.append("/height:");
                sb2.append(createBitmap.getHeight());
                Log.i(sb2.toString());
                bitmap = createBitmap;
            } catch (OutOfMemoryError e) {
                Log.e("video-thumbnail/scale/out-of-memory", e);
                bitmap.recycle();
                throw e;
            }
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        bitmap.recycle();
        return byteArray;
    }

    public byte[] A04(File file) {
        int i;
        Bitmap bitmap;
        Uri fromFile = Uri.fromFile(file);
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                AnonymousClass14P r9 = this.A00;
                if (!TextUtils.isEmpty(fromFile.toString())) {
                    Matrix A0D = C22200yh.A0D(r9.A00.A0C(), fromFile);
                    if (!TextUtils.isEmpty(fromFile.toString())) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        InputStream A04 = r9.A04(fromFile);
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeStream(A04, null, options);
                        A04.close();
                        int i2 = options.outWidth;
                        if (i2 <= 0 || (i = options.outHeight) <= 0) {
                            throw new C37511mW();
                        }
                        options.inSampleSize = 1;
                        int i3 = 1;
                        int max = Math.max(i2, i);
                        while (true) {
                            max >>= 1;
                            if (max <= 80) {
                                break;
                            }
                            i3 <<= 1;
                            options.inSampleSize = i3;
                        }
                        options.inDither = true;
                        options.inJustDecodeBounds = false;
                        options.inScaled = false;
                        options.inPurgeable = true;
                        options.inInputShareable = true;
                        StringBuilder sb = new StringBuilder("sample_rotate_image/width=");
                        sb.append(i2);
                        sb.append(" | height=");
                        sb.append(i);
                        sb.append(" | sample_size=");
                        sb.append(i3);
                        Log.i(sb.toString());
                        options.inPreferQualityOverSpeed = true;
                        options.inMutable = true;
                        try {
                            bitmap = r9.A03(options, A0D, fromFile);
                        } catch (OutOfMemoryError e) {
                            int i4 = options.inSampleSize << 1;
                            options.inSampleSize = i4;
                            StringBuilder sb2 = new StringBuilder("sample_rotate_image/oom ");
                            sb2.append(i4);
                            Log.i(sb2.toString(), e);
                            bitmap = r9.A03(options, A0D, fromFile);
                        }
                        bitmap.isMutable();
                        StringBuilder sb3 = new StringBuilder("sample_rotate_image/final_size:");
                        sb3.append(bitmap.getWidth());
                        sb3.append(" | ");
                        sb3.append(bitmap.getHeight());
                        Log.i(sb3.toString());
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        bitmap.recycle();
                        byteArrayOutputStream.close();
                        return byteArray;
                    }
                    StringBuilder sb4 = new StringBuilder("No file ");
                    sb4.append(fromFile);
                    throw new FileNotFoundException(sb4.toString());
                }
                StringBuilder sb5 = new StringBuilder("No file ");
                sb5.append(fromFile);
                throw new FileNotFoundException(sb5.toString());
            } catch (Throwable th) {
                try {
                    byteArrayOutputStream.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } catch (C37511mW e2) {
            Log.e("thumbnailutils/getImageThumb/file is not an image", e2);
            return null;
        } catch (IOException e3) {
            Log.e("thumbnailutils/getImageThumb/unable to load image", e3);
            return null;
        } catch (OutOfMemoryError e4) {
            Log.e("thumbnailutils/getImageThumb/out of memory when generating the thumbnail", e4);
            return null;
        }
    }
}
