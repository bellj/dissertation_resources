package X;

/* renamed from: X.3Rk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67413Rk implements AbstractC009404s {
    public boolean A00;
    public final C16170oZ A01;
    public final C16370ot A02;
    public final AbstractC14640lm A03;
    public final C40481rf A04;
    public final AnonymousClass19O A05;

    public C67413Rk(C16170oZ r1, C16370ot r2, AbstractC14640lm r3, C40481rf r4, AnonymousClass19O r5, boolean z) {
        this.A01 = r1;
        this.A05 = r5;
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
        this.A00 = z;
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.equals(AnonymousClass2VG.class)) {
            return new AnonymousClass2VG(this.A01, this.A02, this.A03, this.A04, this.A05, this.A00);
        }
        throw C12970iu.A0f(C12960it.A0b("Unknown class ", cls));
    }
}
