package X;

import java.lang.ref.WeakReference;

/* renamed from: X.2Fo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48322Fo extends AbstractC16350or {
    public final C14900mE A00;
    public final AbstractC37361mF A01;
    public final WeakReference A02;

    public C48322Fo(ActivityC13810kN r2, C14900mE r3, AbstractC37361mF r4) {
        super(r2);
        this.A02 = new WeakReference(r2);
        this.A00 = r3;
        this.A01 = r4;
    }
}
