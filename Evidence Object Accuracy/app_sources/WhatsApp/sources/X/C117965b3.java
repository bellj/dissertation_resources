package X;

/* renamed from: X.5b3  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117965b3 extends AnonymousClass015 {
    public final AnonymousClass016 A00 = C12980iv.A0T();
    public final C21860y6 A01;
    public final C17070qD A02;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        if (r0.A07.A07(979) == false) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C117965b3(X.C21860y6 r6, X.C17070qD r7) {
        /*
            r5 = this;
            r5.<init>()
            X.016 r0 = X.C12980iv.A0T()
            r5.A00 = r0
            r5.A02 = r7
            r5.A01 = r6
            boolean r4 = r6.A0A()
            X.0qD r3 = r5.A02
            X.1ng r0 = X.C117305Zk.A0K(r3)
            if (r0 == 0) goto L_0x0024
            X.0m9 r1 = r0.A07
            r0 = 979(0x3d3, float:1.372E-42)
            boolean r0 = r1.A07(r0)
            r2 = 1
            if (r0 != 0) goto L_0x0025
        L_0x0024:
            r2 = 0
        L_0x0025:
            boolean r0 = r6.A0A()
            if (r0 == 0) goto L_0x0039
            r0 = 2131890596(0x7f1211a4, float:1.9415888E38)
        L_0x002e:
            X.5uz r1 = new X.5uz
            r1.<init>(r5, r0, r4, r2)
            X.016 r0 = r5.A00
            r0.A0B(r1)
            return
        L_0x0039:
            X.1ng r0 = X.C117305Zk.A0K(r3)
            if (r0 != 0) goto L_0x0041
            r0 = -1
            goto L_0x002e
        L_0x0041:
            int r0 = r0.A04()
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C117965b3.<init>(X.0y6, X.0qD):void");
    }
}
