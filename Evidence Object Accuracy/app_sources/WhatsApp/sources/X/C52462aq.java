package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.2aq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52462aq extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(59);
    public final float A00;
    public final String A01;
    public final String A02;
    public final boolean A03;

    public /* synthetic */ C52462aq(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A02 = parcel.readString();
        this.A03 = C12970iu.A1W(parcel.readInt());
        this.A00 = parcel.readFloat();
    }

    public C52462aq(Parcelable parcelable, String str, String str2, float f, boolean z) {
        super(parcelable);
        this.A01 = str;
        this.A02 = str2;
        this.A03 = z;
        this.A00 = f;
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeString(this.A02);
        parcel.writeInt(this.A03 ? 1 : 0);
        parcel.writeFloat(this.A00);
    }
}
