package X;

import android.os.Build;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/* renamed from: X.0RL  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0RL {
    public static Method A00;
    public static Method A01;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i < 21) {
            try {
                Class<?> cls = Class.forName("libcore.icu.ICU");
                A01 = cls.getMethod("getScript", String.class);
                A00 = cls.getMethod("addLikelySubtags", String.class);
            } catch (Exception e) {
                A01 = null;
                A00 = null;
                Log.w("ICUCompat", e);
            }
        } else if (i < 24) {
            try {
                A00 = Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", Locale.class);
            } catch (Exception e2) {
                throw new IllegalStateException(e2);
            }
        }
    }

    public static String A00(Locale locale) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 24) {
            return AnonymousClass0T3.A02(AnonymousClass0T3.A00(AnonymousClass0T3.A01(locale)));
        }
        if (i >= 21) {
            try {
                return C04140Kn.A00((Locale) A00.invoke(null, locale));
            } catch (IllegalAccessException | InvocationTargetException e) {
                Log.w("ICUCompat", e);
                return C04140Kn.A00(locale);
            }
        } else {
            Object obj = locale.toString();
            try {
                Method method = A00;
                if (method != null) {
                    obj = method.invoke(null, obj);
                }
            } catch (IllegalAccessException | InvocationTargetException e2) {
                Log.w("ICUCompat", e2);
            }
            if (obj == null) {
                return null;
            }
            try {
                Method method2 = A01;
                if (method2 != null) {
                    return (String) method2.invoke(null, obj);
                }
            } catch (IllegalAccessException | InvocationTargetException e3) {
                Log.w("ICUCompat", e3);
            }
            return null;
        }
    }
}
