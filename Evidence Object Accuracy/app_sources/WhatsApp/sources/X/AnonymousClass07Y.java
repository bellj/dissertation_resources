package X;

import android.graphics.Rect;
import android.view.View;
import androidx.viewpager.widget.ViewPager;

/* renamed from: X.07Y  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07Y implements AnonymousClass07F {
    public final Rect A00 = new Rect();
    public final /* synthetic */ ViewPager A01;

    public AnonymousClass07Y(ViewPager viewPager) {
        this.A01 = viewPager;
    }

    @Override // X.AnonymousClass07F
    public C018408o AMH(View view, C018408o r10) {
        C018408o A0I = AnonymousClass028.A0I(view, r10);
        if (A0I.A00.A0E()) {
            return A0I;
        }
        Rect rect = this.A00;
        rect.left = A0I.A04();
        rect.top = A0I.A06();
        rect.right = A0I.A05();
        rect.bottom = A0I.A03();
        ViewPager viewPager = this.A01;
        int childCount = viewPager.getChildCount();
        for (int i = 0; i < childCount; i++) {
            C018408o A0H = AnonymousClass028.A0H(viewPager.getChildAt(i), A0I);
            rect.left = Math.min(A0H.A04(), rect.left);
            rect.top = Math.min(A0H.A06(), rect.top);
            rect.right = Math.min(A0H.A05(), rect.right);
            rect.bottom = Math.min(A0H.A03(), rect.bottom);
        }
        return A0I.A08(rect.left, rect.top, rect.right, rect.bottom);
    }
}
