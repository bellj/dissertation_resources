package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.util.concurrent.ExecutionException;

/* renamed from: X.1oQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38631oQ extends AbstractC38641oR implements AbstractC28771Oy, AbstractC38621oP {
    public final C002701f A00;
    public final C22370yy A01;
    public final AnonymousClass1KS A02;
    public final AnonymousClass1VC A03 = new AnonymousClass1VC();
    public final File A04;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public C38631oQ(C14900mE r10, C002701f r11, C15450nH r12, C18790t3 r13, C14950mJ r14, C14850m9 r15, C20110vE r16, C22370yy r17, C22600zL r18, AnonymousClass1KS r19, File file) {
        super(r12, r13, r14, r15, r16, r18, r10.A06);
        this.A02 = r19;
        this.A04 = file;
        this.A01 = r17;
        this.A00 = r11;
    }

    @Override // X.AbstractC38621oP
    public AnonymousClass1RN A9B() {
        if (this.A01.A0D(new C1108857g(this), this, null, null, this.A02.A0C, false)) {
            try {
                return (AnonymousClass1RN) this.A03.get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e("DuplicateStickerDownloadListener/waitForResult ", e);
                return new AnonymousClass1RN(1);
            }
        } else {
            A5g(this);
            return A01().A00;
        }
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r1, C28781Oz r2) {
    }
}
