package X;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/* renamed from: X.4Og  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C90534Og {
    public final Bitmap A00;
    public final Canvas A01;

    public /* synthetic */ C90534Og(int i, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        C16700pc.A0B(createBitmap);
        this.A00 = createBitmap;
        this.A01 = new Canvas(createBitmap);
    }
}
