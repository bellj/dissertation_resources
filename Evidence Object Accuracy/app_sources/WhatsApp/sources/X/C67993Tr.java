package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.3Tr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67993Tr implements AbstractC116635Wf {
    public View A00;
    public final C53042cM A01;
    public final C15450nH A02;
    public final AnonymousClass2SL A03;

    public C67993Tr(C53042cM r1, C15450nH r2, AnonymousClass2SL r3) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
    }

    @Override // X.AbstractC116635Wf
    public void AIR() {
        C12970iu.A1G(this.A00);
    }

    @Override // X.AbstractC116635Wf
    public boolean AdK() {
        int i;
        AnonymousClass2SL r7 = this.A03;
        C15450nH r2 = this.A02;
        Context context = this.A01.getContext();
        if (r2.A05(AbstractC15460nI.A17) && context.getPackageManager().getLaunchIntentForPackage("com.whatsapp.w4b") == null) {
            C14820m6 r5 = r7.A00;
            SharedPreferences sharedPreferences = r5.A00;
            if (C38121nY.A00(System.currentTimeMillis(), C12980iv.A0E(sharedPreferences, "biz_app_cross_sell_banner_notif_time") * 1000) < sharedPreferences.getInt("biz_app_cross_sell_banner_expiry_days", 0) && sharedPreferences.getInt("biz_app_cross_sell_banner_dismiss_count", 0) < 1 && sharedPreferences.getInt("biz_app_cross_sell_banner_click_count", 0) < 1) {
                if (r5.A1J("biz_app_upsell_banner_timestamp", 86400000)) {
                    if (sharedPreferences.getInt("biz_app_cross_sell_banner_consecutive_days", 0) >= 2) {
                        r7.A01(0);
                    } else {
                        if (sharedPreferences.getInt("biz_app_cross_sell_banner_cool_off_days", 0) >= 5) {
                            r7.A02(0);
                        } else {
                            int i2 = sharedPreferences.getInt("biz_app_cross_sell_banner_cool_off_days", 0);
                            if (i2 <= 0 || i2 > 5) {
                                C12960it.A0u(sharedPreferences, "biz_app_cross_sell_banner_total_days", sharedPreferences.getInt("biz_app_cross_sell_banner_total_days", 0) + 1);
                                r7.A01(sharedPreferences.getInt("biz_app_cross_sell_banner_consecutive_days", 0) + 1);
                            }
                        }
                        r5.A0k("biz_app_upsell_banner_timestamp");
                    }
                    r7.A02(sharedPreferences.getInt("biz_app_cross_sell_banner_cool_off_days", 0) + 1);
                    r5.A0k("biz_app_upsell_banner_timestamp");
                }
                if (sharedPreferences.getInt("biz_app_cross_sell_banner_total_days", 0) < 4 && ((i = sharedPreferences.getInt("biz_app_cross_sell_banner_cool_off_days", 0)) <= 0 || i > 5)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override // X.AbstractC116635Wf
    public void AfF() {
        if (AdK() && this.A00 == null) {
            C53042cM r2 = this.A01;
            View A0F = C12960it.A0F(C12960it.A0E(r2), r2, R.layout.coversations_biz_app_upsell_chat_banner);
            this.A00 = A0F;
            r2.addView(A0F);
        }
        View view = this.A00;
        if (view == null) {
            C53042cM r22 = this.A01;
            view = C12960it.A0F(C12960it.A0E(r22), r22, R.layout.coversations_biz_app_upsell_chat_banner);
            this.A00 = view;
        }
        TextEmojiLabel A0T = C12970iu.A0T(view, R.id.smb_upsell_chat_banner_description);
        C53042cM r23 = this.A01;
        AbstractC28491Nn.A06(A0T, r23.getContext().getString(R.string.smb_upsell_chat_banner_description));
        r23.setBackgroundResource(R.color.chat_banner_background);
        C12960it.A10(r23, this, 31);
        C12960it.A10(AnonymousClass028.A0D(view, R.id.close), this, 32);
        view.setVisibility(0);
        this.A03.A00(1);
    }
}
