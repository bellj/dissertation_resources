package X;

import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;

/* renamed from: X.5Hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113375Hg extends CertPathValidatorException {
    public C113375Hg(String str, CertPath certPath, int i) {
        super(str, null, certPath, i);
    }
}
