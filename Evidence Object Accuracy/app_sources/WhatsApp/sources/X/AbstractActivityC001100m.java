package X;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

/* renamed from: X.00m  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractActivityC001100m extends Activity implements AbstractC001200n, AbstractC001300o {
    public AnonymousClass00O A00 = new AnonymousClass00O();
    public C009804x A01 = new C009804x(this);

    @Override // X.AbstractC001200n
    public abstract AbstractC009904y ADr();

    public static final void A0J() {
    }

    public static void A0K(String[] strArr) {
    }

    @Deprecated
    public void A0L(AnonymousClass0KQ r3) {
        this.A00.put(r3.getClass(), r3);
    }

    @Deprecated
    public void A0M(Class cls) {
        this.A00.get(cls);
    }

    @Override // X.AbstractC001300o
    public boolean AeZ(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        View decorView = getWindow().getDecorView();
        if (decorView == null || !AnonymousClass028.A0o(keyEvent, decorView)) {
            return AnonymousClass0MK.A00(keyEvent, decorView, this, this);
        }
        return true;
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        View decorView = getWindow().getDecorView();
        if (decorView == null || !AnonymousClass028.A0o(keyEvent, decorView)) {
            return super.dispatchKeyShortcutEvent(keyEvent);
        }
        return true;
    }

    @Override // android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AnonymousClass05H.A00(this);
    }

    @Override // android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        C009804x r2 = this.A01;
        AnonymousClass05I r1 = AnonymousClass05I.CREATED;
        r2.A06("markState");
        r2.A06("setCurrentState");
        r2.A05(r1);
        super.onSaveInstanceState(bundle);
    }
}
