package X;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* renamed from: X.0lK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14370lK implements Parcelable {
    public static final C14370lK A04;
    public static final C14370lK A05;
    public static final C14370lK A06;
    public static final C14370lK A07;
    public static final C14370lK A08;
    public static final C14370lK A09;
    public static final C14370lK A0A;
    public static final C14370lK A0B;
    public static final C14370lK A0C;
    public static final C14370lK A0D;
    public static final C14370lK A0E;
    public static final C14370lK A0F;
    public static final C14370lK A0G;
    public static final C14370lK A0H;
    public static final C14370lK A0I;
    public static final C14370lK A0J;
    public static final C14370lK A0K;
    public static final C14370lK A0L;
    public static final C14370lK A0M;
    public static final C14370lK A0N;
    public static final C14370lK A0O;
    public static final C14370lK A0P;
    public static final C14370lK A0Q;
    public static final C14370lK A0R;
    public static final C14370lK A0S;
    public static final C14370lK A0T;
    public static final C14370lK A0U;
    public static final C14370lK A0V;
    public static final C14370lK A0W;
    public static final C14370lK A0X;
    public static final C14370lK A0Y;
    public static final C14370lK A0Z;
    public static final C14370lK A0a;
    public static final byte[] A0b;
    public static final byte[] A0c;
    public static final byte[] A0d;
    public static final byte[] A0e;
    public static final byte[] A0f;
    public static final byte[] A0g;
    public static final byte[] A0h;
    public static final byte[] A0i;
    public static final byte[] A0j;
    public static final byte[] A0k;
    public static final byte[] A0l;
    public static final byte[] A0m;
    public static final byte[] A0n;
    public static final byte[] A0o;
    public static final Parcelable.Creator CREATOR = new C100384lp();
    public final byte A00;
    public final String A01;
    public final String A02;
    public final byte[] A03;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        return this == obj;
    }

    static {
        byte[] bytes = "WhatsApp Audio Keys".getBytes();
        A0c = bytes;
        byte[] bytes2 = "WhatsApp Image Keys".getBytes();
        A0f = bytes2;
        byte[] bytes3 = "WhatsApp Image Thumbnail Keys".getBytes();
        A0g = bytes3;
        byte[] bytes4 = "WhatsApp Video Keys".getBytes();
        A0n = bytes4;
        byte[] bytes5 = "WhatsApp Video Thumbnail Keys".getBytes();
        A0o = bytes5;
        byte[] bytes6 = "WhatsApp Document Keys".getBytes();
        A0d = bytes6;
        byte[] bytes7 = "WhatsApp Document Thumbnail Keys".getBytes();
        A0e = bytes7;
        byte[] bytes8 = "WhatsApp App State Keys".getBytes();
        A0i = bytes8;
        byte[] bytes9 = "WhatsApp History Keys".getBytes();
        A0j = bytes9;
        byte[] bytes10 = "walibra_hkdf_info".getBytes();
        A0k = bytes10;
        byte[] bytes11 = "WhatsApp Link Thumbnail Keys".getBytes();
        A0h = bytes11;
        byte[] bytes12 = "WhatsApp Payment Background Keys".getBytes();
        A0l = bytes12;
        byte[] bytes13 = "PAYMENT_DOC_UPLOAD".getBytes();
        A0m = bytes13;
        byte[] bytes14 = "ads-image".getBytes();
        A0b = bytes14;
        A05 = new C14370lK("audio", "AUD", bytes, (byte) 2);
        A0I = new C14370lK("ptt", "PTT", bytes, (byte) 2);
        A0B = new C14370lK("image", "IMG", bytes2, (byte) 1);
        A0C = new C14370lK("thumbnail-image", null, bytes3, (byte) 1);
        A0Z = new C14370lK("image", "IMG", bytes2, (byte) 42);
        A0R = new C14370lK("image", "IMG", bytes2, (byte) 23);
        A0E = new C14370lK("image", "IMG", bytes2, (byte) 57);
        A06 = new C14370lK("image", "IMG", bytes2, (byte) 37);
        A0O = new C14370lK("image", "IMG", bytes2, (byte) 44);
        A0S = new C14370lK("sticker", "STK", bytes2, (byte) 20);
        A0V = new C14370lK("image", null, bytes2, (byte) 25);
        A0G = new C14370lK("kyc-id", null, bytes2, (byte) 1);
        A0X = new C14370lK("video", "VID", bytes4, (byte) 3);
        A0F = new C14370lK("video", "VID", bytes4, (byte) 3);
        A0Y = new C14370lK("thumbnail-video", null, bytes5, (byte) 3);
        A0a = new C14370lK("video", "VID", bytes4, (byte) 43);
        A04 = new C14370lK("gif", "VID", bytes4, (byte) 13);
        A0A = new C14370lK("thumbnail-gif", null, bytes5, (byte) 13);
        A0W = new C14370lK("video", null, bytes4, (byte) 28);
        A0U = new C14370lK("gif", null, bytes4, (byte) 29);
        A08 = new C14370lK("document", "DOC", bytes6, (byte) 9);
        A0D = new C14370lK("document", "DOC", bytes6, (byte) 9);
        A0T = new C14370lK("document", null, bytes6, (byte) 26);
        A09 = new C14370lK("thumbnail-document", null, bytes7, (byte) 9);
        A0H = new C14370lK("thumbnail-link", null, bytes11, (byte) 0);
        A0J = new C14370lK("md-app-state", null, bytes8, (byte) 71);
        A0K = new C14370lK("md-msg-hist", "HIST_SYNC", bytes9, (byte) 35);
        A0M = new C14370lK("novi-image", null, bytes10, (byte) 1);
        A0N = new C14370lK("novi-video", null, bytes10, (byte) 3);
        A0P = new C14370lK("payment-bg-image", null, bytes12, (byte) 65);
        A0Q = new C14370lK("payment-br-document", null, bytes13, (byte) 1);
        A0L = new C14370lK("ads-image", null, bytes14, (byte) 1);
        A07 = new C14370lK("biz-cover-photo", "IMG", bytes2, (byte) 1);
    }

    public C14370lK(String str, String str2, byte[] bArr, byte b) {
        this.A00 = b;
        this.A03 = bArr;
        this.A02 = str;
        this.A01 = str2;
    }

    public static C14370lK A00(byte b) {
        if (b == 0) {
            return A0H;
        }
        if (b == 1) {
            return A0C;
        }
        if (b == 3) {
            return A0Y;
        }
        if (b == 9) {
            return A09;
        }
        if (b == 13) {
            return A0A;
        }
        StringBuilder sb = new StringBuilder("media-file-type: The media type is not supported: type=");
        sb.append((int) b);
        throw new IllegalArgumentException(sb.toString());
    }

    public static C14370lK A01(byte b, int i) {
        if (b == 1) {
            return i == 6 ? A0G : A0B;
        }
        if (b == 2) {
            return i == 1 ? A0I : A05;
        }
        if (b == 3) {
            return A0X;
        }
        if (b == 9) {
            return A08;
        }
        if (b == 13) {
            return A04;
        }
        if (b == 20) {
            return A0S;
        }
        if (b == 23) {
            return A0R;
        }
        if (b == 35) {
            return A0K;
        }
        if (b == 37) {
            return A06;
        }
        if (b == 57) {
            return A0E;
        }
        if (b == 65) {
            return A0P;
        }
        if (b == 71) {
            return A0J;
        }
        if (b == 25) {
            return A0V;
        }
        if (b == 26) {
            return A0T;
        }
        if (b == 28) {
            return A0W;
        }
        if (b == 29) {
            return A0U;
        }
        if (b == 62) {
            return A0F;
        }
        if (b == 63) {
            return A0D;
        }
        switch (b) {
            case 42:
                return A0Z;
            case 43:
                return A0a;
            case 44:
                return A0O;
            default:
                StringBuilder sb = new StringBuilder("media-file-type: The media type is not supported: type=");
                sb.append((int) b);
                sb.append(", mediaOrigin=");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
        }
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Byte.valueOf(this.A00)});
    }

    @Override // java.lang.Object
    public String toString() {
        int i;
        StringBuilder sb = new StringBuilder("MmsType{type=");
        sb.append((int) this.A00);
        sb.append(", origin=");
        if (this == A0I) {
            i = 1;
        } else {
            i = 0;
            if (this == A0G) {
                i = 6;
            }
        }
        sb.append(i);
        sb.append(", fileType=");
        sb.append(this.A02);
        sb.append('}');
        return sb.toString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        int i2;
        parcel.writeByte(this.A00);
        if (this == A0I) {
            i2 = 1;
        } else {
            i2 = 0;
            if (this == A0G) {
                i2 = 6;
            }
        }
        parcel.writeInt(i2);
    }
}
