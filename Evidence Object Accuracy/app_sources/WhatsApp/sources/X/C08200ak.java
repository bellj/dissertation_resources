package X;

import java.util.List;

/* renamed from: X.0ak  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08200ak implements AbstractC12040hH {
    public final float A00;
    public final AnonymousClass0H9 A01;
    public final AnonymousClass0H9 A02;
    public final AnonymousClass0H5 A03;
    public final AnonymousClass0HA A04;
    public final AnonymousClass0H6 A05;
    public final AnonymousClass0H6 A06;
    public final AnonymousClass0J4 A07;
    public final EnumC03720Iw A08;
    public final EnumC03820Jg A09;
    public final String A0A;
    public final List A0B;
    public final boolean A0C;

    public C08200ak(AnonymousClass0H9 r1, AnonymousClass0H9 r2, AnonymousClass0H5 r3, AnonymousClass0HA r4, AnonymousClass0H6 r5, AnonymousClass0H6 r6, AnonymousClass0J4 r7, EnumC03720Iw r8, EnumC03820Jg r9, String str, List list, float f, boolean z) {
        this.A0A = str;
        this.A07 = r7;
        this.A03 = r3;
        this.A04 = r4;
        this.A06 = r5;
        this.A05 = r6;
        this.A02 = r1;
        this.A08 = r8;
        this.A09 = r9;
        this.A00 = f;
        this.A0B = list;
        this.A01 = r2;
        this.A0C = z;
    }

    @Override // X.AbstractC12040hH
    public AbstractC12470hy Aes(AnonymousClass0AA r2, AbstractC08070aX r3) {
        return new C03220Gr(r2, this, r3);
    }
}
