package X;

import android.graphics.drawable.GradientDrawable;

/* renamed from: X.3fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73083fe extends GradientDrawable {
    @Override // android.graphics.drawable.GradientDrawable, android.graphics.drawable.Drawable
    public boolean isStateful() {
        return true;
    }
}
