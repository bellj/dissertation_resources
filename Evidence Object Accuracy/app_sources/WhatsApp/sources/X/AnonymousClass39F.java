package X;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;

/* renamed from: X.39F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39F extends AnonymousClass21T {
    public final Handler A00 = new Handler(Looper.getMainLooper(), new Handler.Callback() { // from class: X.4iQ
        @Override // android.os.Handler.Callback
        public final boolean handleMessage(Message message) {
            AnonymousClass39F r1 = AnonymousClass39F.this;
            r1.A02.A02();
            AnonymousClass5V2 r0 = ((AnonymousClass21T) r1).A01;
            if (r0 == null) {
                return true;
            }
            r0.AOT(r1);
            return true;
        }
    });
    public final View A01;
    public final C92784Xk A02 = new C92784Xk(4500);

    public AnonymousClass39F(View view) {
        this.A01 = view;
    }
}
