package X;

/* renamed from: X.4XN  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XN {
    public final int A00;
    public final int A01;
    public final C100614mC A02;
    public final C100614mC A03;
    public final String A04;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0006, code lost:
        if (r6 == 0) goto L_0x0008;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass4XN(X.C100614mC r2, X.C100614mC r3, java.lang.String r4, int r5, int r6) {
        /*
            r1 = this;
            r1.<init>()
            if (r5 == 0) goto L_0x0008
            r0 = 0
            if (r6 != 0) goto L_0x0009
        L_0x0008:
            r0 = 1
        L_0x0009:
            X.C95314dV.A03(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 != 0) goto L_0x001d
            r1.A04 = r4
            r1.A03 = r2
            r1.A02 = r3
            r1.A01 = r5
            r1.A00 = r6
            return
        L_0x001d:
            java.lang.IllegalArgumentException r0 = X.C72453ed.A0h()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass4XN.<init>(X.4mC, X.4mC, java.lang.String, int, int):void");
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || AnonymousClass4XN.class != obj.getClass()) {
                return false;
            }
            AnonymousClass4XN r5 = (AnonymousClass4XN) obj;
            if (this.A01 != r5.A01 || this.A00 != r5.A00 || !this.A04.equals(r5.A04) || !this.A03.equals(r5.A03) || !this.A02.equals(r5.A02)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return C12990iw.A08(this.A02, C12990iw.A08(this.A03, (((C72453ed.A05(this.A01) + this.A00) * 31) + this.A04.hashCode()) * 31) * 31);
    }
}
