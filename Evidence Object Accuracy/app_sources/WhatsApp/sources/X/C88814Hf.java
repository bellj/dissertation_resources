package X;

import com.facebook.msys.mci.DefaultCrypto;
import java.nio.charset.Charset;

/* renamed from: X.4Hf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88814Hf {
    public static final Charset A00 = Charset.forName("ISO-8859-1");
    public static final Charset A01 = Charset.forName("US-ASCII");
    public static final Charset A02 = Charset.forName("UTF-16");
    public static final Charset A03 = Charset.forName("UTF-16BE");
    public static final Charset A04 = Charset.forName("UTF-16LE");
    public static final Charset A05 = Charset.forName(DefaultCrypto.UTF_8);
}
