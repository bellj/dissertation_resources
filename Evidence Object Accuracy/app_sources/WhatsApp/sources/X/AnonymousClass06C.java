package X;

import android.os.Build;
import android.os.LocaleList;
import java.util.Locale;

/* renamed from: X.06C  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass06C {
    public static final AnonymousClass06C A01 = A02(new Locale[0]);
    public final AnonymousClass06D A00;

    public AnonymousClass06C(AnonymousClass06D r1) {
        this.A00 = r1;
    }

    public static AnonymousClass06C A00(LocaleList localeList) {
        return new AnonymousClass06C(new C07380Xt(localeList));
    }

    public static AnonymousClass06C A01(String str) {
        Locale A03;
        if (str == null || str.isEmpty()) {
            return A01;
        }
        String[] split = str.split(",", -1);
        int length = split.length;
        Locale[] localeArr = new Locale[length];
        for (int i = 0; i < length; i++) {
            int i2 = Build.VERSION.SDK_INT;
            String str2 = split[i];
            if (i2 >= 21) {
                A03 = C04120Kl.A00(str2);
            } else {
                A03 = A03(str2);
            }
            localeArr[i] = A03;
        }
        return A02(localeArr);
    }

    public static AnonymousClass06C A02(Locale... localeArr) {
        if (Build.VERSION.SDK_INT >= 24) {
            return A00(C04130Km.A00(localeArr));
        }
        return new AnonymousClass06C(new AnonymousClass06K(localeArr));
    }

    public static Locale A03(String str) {
        String str2 = "-";
        if (!str.contains(str2)) {
            str2 = "_";
            if (!str.contains(str2)) {
                return new Locale(str);
            }
        }
        String[] split = str.split(str2, -1);
        int length = split.length;
        if (length > 2) {
            return new Locale(split[0], split[1], split[2]);
        }
        if (length > 1) {
            return new Locale(split[0], split[1]);
        }
        if (length == 1) {
            return new Locale(split[0]);
        }
        StringBuilder sb = new StringBuilder("Can not parse language tag: [");
        sb.append(str);
        sb.append("]");
        throw new IllegalArgumentException(sb.toString());
    }

    public boolean equals(Object obj) {
        return (obj instanceof AnonymousClass06C) && this.A00.equals(((AnonymousClass06C) obj).A00);
    }

    public int hashCode() {
        return this.A00.hashCode();
    }

    public String toString() {
        return this.A00.toString();
    }
}
