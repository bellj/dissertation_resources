package X;

import android.os.Bundle;
import android.os.Message;
import android.util.SparseArray;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/* renamed from: X.2N1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2N1 extends AbstractC35941j2 {
    public final /* synthetic */ AnonymousClass2L8 A00;
    public final /* synthetic */ DeviceJid[] A01;

    public AnonymousClass2N1(AnonymousClass2L8 r1, DeviceJid[] deviceJidArr) {
        this.A00 = r1;
        this.A01 = deviceJidArr;
    }

    @Override // X.AbstractC35941j2
    public void A00(int i) {
        C450720b r2 = this.A00.A0H;
        DeviceJid[] deviceJidArr = this.A01;
        Log.i("xmpp/reader/on-get-pre-key-error");
        AbstractC450820c r4 = r2.A00;
        Bundle bundle = new Bundle();
        bundle.putParcelableArray("jids", deviceJidArr);
        bundle.putInt("errorCode", i);
        r4.AYY(Message.obtain(null, 0, 76, 0, bundle));
    }

    @Override // X.AbstractC35941j2
    public void A02(AnonymousClass1V8 r30) {
        byte b;
        byte[] bArr;
        C29211Rh r0;
        AnonymousClass1V8 A0F = r30.A0F("list");
        DeviceJid[] deviceJidArr = this.A01;
        int length = deviceJidArr.length;
        HashSet hashSet = new HashSet(length);
        HashMap hashMap = new HashMap(length);
        AnonymousClass1V8[] r10 = A0F.A03;
        int i = 0;
        if (r10 != null) {
            AnonymousClass2L8 r9 = this.A00;
            Arrays.sort(r10, new C112195Ck(r9));
            ArrayList arrayList = new ArrayList();
            int length2 = r10.length;
            int i2 = 0;
            while (i2 < length2) {
                AnonymousClass1V8 r7 = r10[i2];
                DeviceJid deviceJid = (DeviceJid) r7.A0B(r9.A04, DeviceJid.class, "jid");
                AnonymousClass1V8 A0E = r7.A0E("error");
                if (A0E != null) {
                    int A06 = A0E.A06(A0E.A0H("code"), "code");
                    hashSet.add(deviceJid);
                    hashMap.put(deviceJid, Integer.valueOf(A06));
                } else {
                    AnonymousClass1V8 A0F2 = r7.A0F("identity");
                    AnonymousClass1V8 A0E2 = r7.A0E("device-identity");
                    AnonymousClass1V8 A0F3 = r7.A0F("registration");
                    AnonymousClass1V8 A0E3 = r7.A0E("type");
                    if (A0E3 == null) {
                        b = 5;
                    } else {
                        byte[] bArr2 = A0E3.A01;
                        if (bArr2 == null || bArr2.length != 1) {
                            throw new AnonymousClass1V9("type node should contain exactly 1 byte");
                        }
                        b = bArr2[i];
                    }
                    AnonymousClass1V8 A0E4 = r7.A0E("key");
                    if (A0E4 != null) {
                        bArr = null;
                        r0 = new C29211Rh(A0E4.A0F("id").A01, A0E4.A0F("value").A01, null);
                    } else {
                        bArr = null;
                        r0 = null;
                    }
                    AnonymousClass1V8 A0F4 = r7.A0F("skey");
                    AnonymousClass1V8 A0F5 = A0F4.A0F("id");
                    AnonymousClass1V8 A0F6 = A0F4.A0F("value");
                    AnonymousClass1V8 A0F7 = A0F4.A0F("signature");
                    byte[] bArr3 = A0F2.A01;
                    byte[] bArr4 = A0F3.A01;
                    if (A0E2 != null) {
                        bArr = A0E2.A01;
                    }
                    AnonymousClass2Cv r4 = new AnonymousClass2Cv(deviceJid, r0, new C29211Rh(A0F5.A01, A0F6.A01, A0F7.A01), bArr3, bArr4, bArr, b);
                    if (r9.A0C.A07(632)) {
                        arrayList.add(r4);
                    } else {
                        List singletonList = Collections.singletonList(r4);
                        C450720b r1 = r9.A0H;
                        Log.i("xmpp/reader/on-get-pre-key-success");
                        r1.A00.AYY(Message.obtain(null, 0, 74, 0, singletonList));
                    }
                    hashSet.add(deviceJid);
                }
                i2++;
                i = 0;
            }
            if (!arrayList.isEmpty()) {
                C450720b r2 = r9.A0H;
                Log.i("xmpp/reader/on-get-pre-key-success");
                r2.A00.AYY(Message.obtain(null, i, 74, i, arrayList));
            }
        }
        SparseArray sparseArray = new SparseArray();
        while (i < length) {
            DeviceJid deviceJid2 = deviceJidArr[i];
            if (!hashSet.contains(deviceJid2)) {
                C450720b r22 = this.A00.A0H;
                Log.i("xmpp/reader/on-get-pre-key-none");
                AbstractC450820c r72 = r22.A00;
                Bundle bundle = new Bundle();
                bundle.putParcelable("jid", deviceJid2);
                r72.AYY(Message.obtain(null, 0, 75, 0, bundle));
            }
            if (hashMap.containsKey(deviceJid2)) {
                int intValue = ((Number) hashMap.get(deviceJid2)).intValue();
                List list = (List) sparseArray.get(intValue);
                if (list != null) {
                    list.add(deviceJid2);
                } else {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(deviceJid2);
                    sparseArray.put(intValue, arrayList2);
                }
            }
            i++;
        }
        int size = sparseArray.size();
        for (int i3 = 0; i3 < size; i3++) {
            C450720b r3 = this.A00.A0H;
            int keyAt = sparseArray.keyAt(i3);
            Log.i("xmpp/reader/on-get-pre-key-error");
            AbstractC450820c r42 = r3.A00;
            Bundle bundle2 = new Bundle();
            bundle2.putParcelableArray("jids", (Jid[]) ((List) sparseArray.valueAt(i3)).toArray(new DeviceJid[0]));
            bundle2.putInt("errorCode", keyAt);
            r42.AYY(Message.obtain(null, 0, 76, 0, bundle2));
        }
    }
}
