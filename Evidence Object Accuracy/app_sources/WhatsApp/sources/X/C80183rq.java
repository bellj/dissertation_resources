package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3rq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80183rq extends AnonymousClass5I0<Boolean> implements AnonymousClass5Z6<Boolean>, AbstractC115265Qv, RandomAccess {
    public static final C80183rq A02;
    public int A00;
    public boolean[] A01;

    static {
        C80183rq r0 = new C80183rq(new boolean[0], 0);
        A02 = r0;
        ((AnonymousClass5I0) r0).A00 = false;
    }

    public C80183rq() {
        this(new boolean[10], 0);
    }

    public C80183rq(boolean[] zArr, int i) {
        this.A01 = zArr;
        this.A00 = i;
    }

    public final void A03(boolean z) {
        A02();
        int i = this.A00;
        boolean[] zArr = this.A01;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[((i * 3) >> 1) + 1];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.A01 = zArr2;
            zArr = zArr2;
        }
        int i2 = this.A00;
        this.A00 = i2 + 1;
        zArr[i2] = z;
    }

    @Override // X.AnonymousClass5Z6
    public final /* synthetic */ AnonymousClass5Z6 Agl(int i) {
        if (i >= this.A00) {
            return new C80183rq(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        boolean A1Y = C12970iu.A1Y(obj);
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        boolean[] zArr = this.A01;
        if (i2 < zArr.length) {
            C72463ee.A0T(zArr, i, i2);
        } else {
            boolean[] zArr2 = new boolean[((i2 * 3) >> 1) + 1];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.A01, i, zArr2, i + 1, this.A00 - i);
            this.A01 = zArr2;
        }
        this.A01[i] = A1Y;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* synthetic */ boolean add(Object obj) {
        A03(C12970iu.A1Y(obj));
        return true;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C80183rq)) {
            return super.addAll(collection);
        }
        C80183rq r7 = (C80183rq) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.A01;
            if (i3 > zArr.length) {
                zArr = Arrays.copyOf(zArr, i3);
                this.A01 = zArr;
            }
            System.arraycopy(r7.A01, 0, zArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean contains(Object obj) {
        return C12980iv.A1V(indexOf(obj), -1);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C80183rq)) {
                return super.equals(obj);
            }
            C80183rq r8 = (C80183rq) obj;
            int i = this.A00;
            if (i == r8.A00) {
                boolean[] zArr = r8.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == zArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        if (i >= 0 && i < this.A00) {
            return Boolean.valueOf(this.A01[i]);
        }
        throw C72453ed.A0i(i, this.A00);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            int i3 = i * 31;
            int i4 = 1237;
            if (this.A01[i2]) {
                i4 = 1231;
            }
            i = i3 + i4;
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Boolean) {
            boolean A1Y = C12970iu.A1Y(obj);
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.A01[i] == A1Y) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        int i2;
        A02();
        if (i < 0 || i >= (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        boolean[] zArr = this.A01;
        boolean z = zArr[i];
        AnonymousClass5I0.A01(zArr, i2, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            boolean[] zArr = this.A01;
            System.arraycopy(zArr, i2, zArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        boolean A1Y = C12970iu.A1Y(obj);
        A02();
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
        boolean[] zArr = this.A01;
        boolean z = zArr[i];
        zArr[i] = A1Y;
        return Boolean.valueOf(z);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }
}
