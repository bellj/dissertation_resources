package X;

import android.content.Context;
import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.4qD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103104qD implements AbstractC009204q {
    public final /* synthetic */ GroupChatInfo A00;

    public C103104qD(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
