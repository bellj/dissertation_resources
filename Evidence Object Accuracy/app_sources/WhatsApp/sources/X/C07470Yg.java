package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0Yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07470Yg implements AnonymousClass02B {
    public final /* synthetic */ BiometricFragment A00;

    public C07470Yg(BiometricFragment biometricFragment) {
        this.A00 = biometricFragment;
    }

    @Override // X.AnonymousClass02B
    public /* bridge */ /* synthetic */ void ANq(Object obj) {
        if (((Boolean) obj).booleanValue()) {
            BiometricFragment biometricFragment = this.A00;
            biometricFragment.A1E(1);
            biometricFragment.A18();
            AnonymousClass0EP r2 = biometricFragment.A01;
            AnonymousClass016 r1 = r2.A0E;
            if (r1 == null) {
                r1 = new AnonymousClass016();
                r2.A0E = r1;
            }
            AnonymousClass0EP.A00(r1, false);
        }
    }
}
