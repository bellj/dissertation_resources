package X;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.WaEditText;
import com.whatsapp.util.Log;
import java.util.ArrayList;

/* renamed from: X.3MJ  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3MJ implements TextWatcher {
    public int A00 = -1;
    public int A01 = -1;
    public String A02 = "";
    public boolean A03;
    public final int A04;
    public final int A05;
    public final WaEditText A06;
    public final WaEditText A07;
    public final WaEditText A08;
    public final C64333Fd A09;
    public final AnonymousClass4L3 A0A;
    public final ArrayList A0B;

    public /* synthetic */ AnonymousClass3MJ(WaEditText waEditText, WaEditText waEditText2, WaEditText waEditText3, C64333Fd r5, AnonymousClass4L3 r6, ArrayList arrayList, int i, int i2) {
        this.A09 = r5;
        this.A0A = r6;
        this.A0B = arrayList;
        this.A08 = waEditText;
        this.A06 = waEditText2;
        this.A07 = waEditText3;
        this.A04 = i;
        this.A05 = i2;
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        int i;
        String str;
        if (!TextUtils.isEmpty(this.A02)) {
            WaEditText waEditText = this.A06;
            waEditText.removeTextChangedListener(this);
            waEditText.setText("");
            waEditText.addTextChangedListener(this);
        }
        if (!TextUtils.isEmpty(editable) || this.A00 != 0) {
            int i2 = this.A00;
            WaEditText waEditText2 = this.A06;
            if (i2 == 1) {
                waEditText2.removeTextChangedListener(this);
                if (Character.isDigit(editable.charAt(this.A01))) {
                    int i3 = this.A01;
                    waEditText2.setText(editable.subSequence(i3, i3 + 1));
                    WaEditText waEditText3 = this.A07;
                    if (waEditText3 != null && !TextUtils.isEmpty(waEditText2.getText())) {
                        waEditText3.requestFocus();
                    }
                } else {
                    waEditText2.setText("");
                }
                waEditText2.addTextChangedListener(this);
            } else {
                waEditText2.removeTextChangedListener(this);
                int i4 = 0;
                int i5 = 0;
                while (true) {
                    i = this.A05;
                    if (i4 >= i || i5 >= editable.length()) {
                        break;
                    }
                    if (!Character.isDigit(editable.charAt(i5))) {
                        i4--;
                    } else {
                        if (i5 < this.A00) {
                            str = Character.toString(editable.charAt(i5));
                        } else {
                            str = "";
                        }
                        ((TextView) this.A0B.get(i4)).setText(str);
                    }
                    i4++;
                    i5++;
                }
                waEditText2.addTextChangedListener(this);
                int i6 = this.A00;
                if (i6 < i) {
                    ((View) this.A0B.get(i6)).requestFocus();
                }
                if (this.A04 != this.A0B.size() - 1) {
                    return;
                }
            }
            String A00 = this.A09.A00();
            if (A00.length() == this.A0B.size()) {
                AnonymousClass4L3 r1 = this.A0A;
                if (r1 == null) {
                    Log.e("CodeInputBoxManager/CodeInputBoxTextWatcher/afterTextChanged/invalid callback");
                    return;
                }
                Log.i("CodeInputBoxManager/CodeInputBoxTextWatcher/afterTextChanged trigger callback");
                r1.A00.A3Y(A00);
            }
        } else if (this.A06.hasFocus() && this.A03) {
            WaEditText waEditText4 = this.A08;
            AnonymousClass009.A03(waEditText4);
            waEditText4.setText("");
            waEditText4.requestFocus();
        }
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        boolean z = false;
        if (!TextUtils.isEmpty(this.A06.getText())) {
            this.A02 = charSequence.toString();
        } else if (this.A08 != null) {
            z = true;
        }
        this.A03 = z;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.A00 = i3;
        this.A01 = i;
    }
}
