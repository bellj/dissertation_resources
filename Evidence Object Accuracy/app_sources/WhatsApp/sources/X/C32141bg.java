package X;

import java.io.Serializable;
import java.util.Arrays;

/* renamed from: X.1bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32141bg implements Serializable {
    public static final long serialVersionUID = 1;
    public final int actualActors;
    public final int hostStorage;
    public final long privacyModeTs;

    public C32141bg() {
        this.hostStorage = 0;
        this.actualActors = 0;
        this.privacyModeTs = 0;
    }

    public C32141bg(int i, long j, int i2) {
        this.hostStorage = i;
        this.actualActors = i2;
        this.privacyModeTs = j;
    }

    public C32141bg(C32141bg r3) {
        long j;
        if (r3 == null) {
            this.hostStorage = 0;
            this.actualActors = 0;
            j = 0;
        } else {
            this.hostStorage = r3.hostStorage;
            this.actualActors = r3.actualActors;
            j = r3.privacyModeTs;
        }
        this.privacyModeTs = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        if (r4.equals("2") == false) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0014, code lost:
        if (r3.equals("2") == false) goto L_0x0016;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C32141bg(java.lang.String r3, java.lang.String r4, java.lang.String r5) {
        /*
            r2 = this;
            r2.<init>()
            if (r3 == 0) goto L_0x0016
            java.lang.String r0 = "1"
            boolean r0 = r3.equals(r0)
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = "2"
            boolean r0 = r3.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x0017
        L_0x0016:
            r1 = 0
        L_0x0017:
            r2.hostStorage = r1
            if (r4 == 0) goto L_0x002c
            java.lang.String r0 = "1"
            boolean r0 = r4.equals(r0)
            if (r0 != 0) goto L_0x0038
            java.lang.String r0 = "2"
            boolean r0 = r4.equals(r0)
            r1 = 2
            if (r0 != 0) goto L_0x002d
        L_0x002c:
            r1 = 0
        L_0x002d:
            r2.actualActors = r1
            r0 = 0
            long r0 = X.C28421Nd.A01(r5, r0)
            r2.privacyModeTs = r0
            return
        L_0x0038:
            r1 = 1
            goto L_0x002d
        L_0x003a:
            r1 = 1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C32141bg.<init>(java.lang.String, java.lang.String, java.lang.String):void");
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C32141bg r7 = (C32141bg) obj;
            if (!(this.hostStorage == r7.hostStorage && this.actualActors == r7.actualActors && this.privacyModeTs == r7.privacyModeTs)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.hostStorage), Integer.valueOf(this.actualActors), Long.valueOf(this.privacyModeTs)});
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder("PrivacyMode{hostStorage=");
        sb.append(this.hostStorage);
        sb.append(", actualActors=");
        sb.append(this.actualActors);
        sb.append(", privacyModeTs=");
        sb.append(this.privacyModeTs);
        sb.append('}');
        return sb.toString();
    }
}
