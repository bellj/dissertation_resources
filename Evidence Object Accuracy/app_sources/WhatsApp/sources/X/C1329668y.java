package X;

import android.text.TextUtils;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.68y  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1329668y implements AnonymousClass17Y {
    public final C14830m7 A00;
    public final C14850m9 A01;
    public final C21860y6 A02;
    public final C18600si A03;

    public C1329668y(C14830m7 r1, C14850m9 r2, C21860y6 r3, C18600si r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static String A00(C1329668y r0) {
        return (String) r0.A04().A00;
    }

    public static final JSONObject A01(String str, String str2, JSONObject jSONObject) {
        try {
            JSONObject optJSONObject = jSONObject.optJSONObject(str2);
            if (optJSONObject == null) {
                optJSONObject = C117295Zj.A0a();
                jSONObject.put(str2, optJSONObject);
            }
            JSONObject optJSONObject2 = optJSONObject.optJSONObject(str);
            if (optJSONObject2 != null) {
                return optJSONObject2;
            }
            JSONObject A0a = C117295Zj.A0a();
            optJSONObject.put(str, A0a);
            return A0a;
        } catch (JSONException e) {
            StringBuilder A0k = C12960it.A0k("PAY: IndiaUpiPaymentSharedPrefs getOrInsertFieldByPsP for field: ");
            A0k.append(str2);
            Log.w(C12960it.A0d(" threw: ", A0k), e);
            return null;
        }
    }

    public static void A02(String str, JSONObject jSONObject) {
        String optString = jSONObject.optString(str);
        if (!TextUtils.isEmpty(optString)) {
            jSONObject.put(str, C1309060l.A00(optString));
        }
    }

    public synchronized int A03() {
        int i;
        String A04;
        try {
            A04 = this.A03.A04();
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs getSimIndex threw: ", e);
        }
        if (!TextUtils.isEmpty(A04)) {
            i = C13000ix.A05(A04).optInt("device_binding_sim_subscripiton_id", -1);
        }
        i = 0;
        return i;
    }

    public synchronized AnonymousClass1ZR A04() {
        String str;
        str = null;
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                str = C13000ix.A05(A04).optString("vpa", null);
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs readAccountHandle threw: ", e);
        }
        return C117305Zk.A0I(C117305Zk.A0J(), String.class, str, "upiHandle");
    }

    public synchronized AnonymousClass1ZR A05() {
        byte[] bArr;
        bArr = null;
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                JSONObject A05 = C13000ix.A05(A04);
                String optString = A05.optString("token", null);
                long optLong = A05.optLong("tokenTs", 0);
                long millis = TimeUnit.DAYS.toMillis(20);
                if (!TextUtils.isEmpty(optString) && this.A00.A00() - optLong < millis) {
                    bArr = Base64.decode(optString, 0);
                }
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs readToken threw: ", e);
        }
        return C117305Zk.A0I(C117305Zk.A0J(), byte[].class, bArr, "sessionToken");
    }

    public String A06() {
        String str = "ICIWC";
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                str = C13000ix.A05(A04).optString("sequenceNumberPrefix", str);
                return str;
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs getPaymentSequenceNumberPrefix threw: ", e);
        }
        return str;
    }

    public String A07() {
        String str = null;
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                str = C13000ix.A05(A04).optString("psp", null);
                return str;
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs getPsp threw: ", e);
        }
        return str;
    }

    public synchronized String A08() {
        return this.A03.A01().getString("payments_upi_aliases", null);
    }

    public synchronized String A09() {
        String[] A0P;
        A0P = A0P("device_binding_sim_iccid");
        if (A0P[0] == null) {
            A0P = A0P("device_binding_sim_id");
        }
        return A0P[0];
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String A0A() {
        /*
            r10 = this;
            monitor-enter(r10)
            r6 = 0
            X.0si r0 = r10.A03     // Catch: JSONException -> 0x003b, all -> 0x0048
            java.lang.String r1 = r0.A04()     // Catch: JSONException -> 0x003b, all -> 0x0048
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch: JSONException -> 0x003b, all -> 0x0048
            if (r0 != 0) goto L_0x0046
            org.json.JSONObject r3 = X.C13000ix.A05(r1)     // Catch: JSONException -> 0x003b, all -> 0x0048
            java.lang.String r0 = "listKeys"
            java.lang.String r9 = r3.optString(r0, r6)     // Catch: JSONException -> 0x003b, all -> 0x0048
            java.lang.String r2 = "listKeysTs"
            r0 = 0
            long r7 = r3.optLong(r2, r0)     // Catch: JSONException -> 0x003b, all -> 0x0048
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.DAYS     // Catch: JSONException -> 0x003b, all -> 0x0048
            r0 = 1
            long r4 = r2.toMillis(r0)     // Catch: JSONException -> 0x003b, all -> 0x0048
            boolean r0 = android.text.TextUtils.isEmpty(r9)     // Catch: JSONException -> 0x003b, all -> 0x0048
            if (r0 != 0) goto L_0x0042
            X.0m7 r0 = r10.A00     // Catch: JSONException -> 0x003b, all -> 0x0048
            long r2 = r0.A00()     // Catch: JSONException -> 0x003b, all -> 0x0048
            long r2 = r2 - r7
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            r0 = 1
            if (r1 < 0) goto L_0x0043
            goto L_0x0042
        L_0x003b:
            r1 = move-exception
            java.lang.String r0 = "PAY: IndiaUpiPaymentSharedPrefs readListKeys threw: "
            com.whatsapp.util.Log.w(r0, r1)     // Catch: all -> 0x0048
            goto L_0x0046
        L_0x0042:
            r0 = 0
        L_0x0043:
            if (r0 == 0) goto L_0x0046
            r6 = r9
        L_0x0046:
            monitor-exit(r10)
            return r6
        L_0x0048:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1329668y.A0A():java.lang.String");
    }

    public synchronized String A0B() {
        String str;
        str = null;
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                str = C13000ix.A05(A04).optString("vpaId", null);
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs readVpaId threw: ", e);
        }
        return str;
    }

    public synchronized void A0C() {
        try {
            C18600si r2 = this.A03;
            JSONObject A0c = C117295Zj.A0c(r2);
            A0c.remove("listKeys");
            A0c.remove("listKeysTs");
            C117295Zj.A1E(r2, A0c);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteTokenAndKeys threw: ", e);
        }
    }

    public synchronized void A0D() {
        try {
            C18600si r2 = this.A03;
            JSONObject A0c = C117295Zj.A0c(r2);
            A0c.remove("token");
            A0c.remove("tokenTs");
            A0c.remove("listKeys");
            A0c.remove("listKeysTs");
            C117295Zj.A1E(r2, A0c);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteTokenAndKeys threw: ", e);
        }
    }

    public synchronized void A0E(int i) {
        try {
            C18600si r2 = this.A03;
            JSONObject A0c = C117295Zj.A0c(r2);
            A0c.put("device_binding_sim_subscripiton_id", i);
            C117295Zj.A1E(r2, A0c);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs setSimIndex threw: ", e);
        }
    }

    public synchronized void A0F(AnonymousClass1ZR r3, String str) {
        try {
            C18600si r1 = this.A03;
            JSONObject A0c = C117295Zj.A0c(r1);
            A0G(r3, str, A0c);
            C117295Zj.A1E(r1, A0c);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs storeVpaHandle threw: ", e);
        }
    }

    public final synchronized void A0G(AnonymousClass1ZR r4, String str, JSONObject jSONObject) {
        jSONObject.put("v", "2");
        Object obj = r4.A00;
        AnonymousClass009.A05(obj);
        jSONObject.put("vpa", obj);
        if (!TextUtils.isEmpty(str)) {
            jSONObject.put("vpaId", str);
        }
        jSONObject.put("vpaTs", this.A00.A00());
    }

    public synchronized void A0H(String str) {
        JSONObject A05;
        try {
            C18600si r5 = this.A03;
            String A04 = r5.A04();
            long A00 = this.A00.A00();
            if (TextUtils.isEmpty(A04)) {
                A05 = C117295Zj.A0a();
            } else {
                A05 = C13000ix.A05(A04);
            }
            A05.put("v", "2");
            A05.put("listKeys", str);
            A05.put("listKeysTs", A00);
            C117295Zj.A1E(r5, A05);
            StringBuilder A0h = C12960it.A0h();
            A0h.append("PAY: IndiaUpiPaymentSharedPrefs storeListKeys at timestamp: ");
            A0h.append(A00);
            C12960it.A1F(A0h);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs storeListKeys threw: ", e);
        }
    }

    public synchronized void A0I(String str, String str2, String str3) {
        try {
            C18600si r2 = this.A03;
            JSONObject A0b = C117295Zj.A0b(r2);
            A0J(str, str2, A0b);
            if (!TextUtils.isEmpty(str3)) {
                A0b.put("device_binding_sim_id", str3);
            }
            A0b.remove("device_binding_sim_subscripiton_id");
            C117295Zj.A1E(r2, A0b);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs storeDeviceBinding threw: ", e);
        }
    }

    public final synchronized void A0J(String str, String str2, JSONObject jSONObject) {
        jSONObject.put("v", "2");
        jSONObject.put("psp", str);
        jSONObject.put("sequenceNumberPrefix", str2);
        JSONObject A01 = A01(str, "devBindingByPsp", jSONObject);
        if (A01 != null) {
            A01.put("devBinding", true);
        }
    }

    public synchronized void A0K(JSONArray jSONArray) {
        C18600si r0 = this.A03;
        C12970iu.A1D(C117295Zj.A05(r0), "payments_upi_aliases", jSONArray.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r2.A01.A07(1644) == false) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean A0L() {
        /*
            r2 = this;
            monitor-enter(r2)
            X.0si r0 = r2.A03     // Catch: all -> 0x001d
            android.content.SharedPreferences r1 = r0.A01()     // Catch: all -> 0x001d
            java.lang.String r0 = "payment_account_recovered"
            boolean r0 = X.C12980iv.A1W(r1, r0)     // Catch: all -> 0x001d
            if (r0 == 0) goto L_0x001a
            X.0m9 r1 = r2.A01     // Catch: all -> 0x001d
            r0 = 1644(0x66c, float:2.304E-42)
            boolean r1 = r1.A07(r0)     // Catch: all -> 0x001d
            r0 = 1
            if (r1 != 0) goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            monitor-exit(r2)
            return r0
        L_0x001d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C1329668y.A0L():boolean");
    }

    public boolean A0M(C119755f3 r7, AnonymousClass6BE r8, String str) {
        ArrayList arrayList;
        if (TextUtils.isEmpty(str) || !A0O(str)) {
            return false;
        }
        if (!this.A01.A07(1661)) {
            return true;
        }
        if (r7 == null || (arrayList = r7.A0G) == null) {
            return false;
        }
        boolean contains = arrayList.contains(str);
        if (!contains) {
            AnonymousClass2SP r2 = new AnonymousClass2SP();
            r2.A0Z = "redo_device_binding";
            r2.A09 = 0;
            r2.A02 = Boolean.valueOf(this.A02.A0E("add_bank"));
            r8.AKf(r2);
            A8l(str, true);
        }
        return contains;
    }

    public boolean A0N(String str) {
        if (TextUtils.isEmpty(str) || !A0O(str)) {
            return false;
        }
        return true;
    }

    public final synchronized boolean A0O(String str) {
        boolean z;
        JSONObject optJSONObject;
        boolean optBoolean;
        z = false;
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                JSONObject A05 = C13000ix.A05(A04);
                JSONObject optJSONObject2 = A05.optJSONObject("devBindingByPsp");
                if (optJSONObject2 == null || (optJSONObject = optJSONObject2.optJSONObject(str)) == null || !(optBoolean = optJSONObject.optBoolean("devBinding", false))) {
                    z = A05.optBoolean("devBinding", false);
                } else {
                    z = optBoolean;
                }
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs readDeviceBinding threw: ", e);
        }
        return z;
    }

    public final String[] A0P(String... strArr) {
        try {
            String A04 = this.A03.A04();
            if (!TextUtils.isEmpty(A04)) {
                JSONObject A05 = C13000ix.A05(A04);
                int length = strArr.length;
                String[] strArr2 = new String[length];
                for (int i = 0; i < length; i++) {
                    strArr2[i] = A05.optString(strArr[i], null);
                }
                return strArr2;
            }
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs readFromPaymentInfo for keys threw: ", e);
        }
        return new String[strArr.length];
    }

    @Override // X.AnonymousClass17Y
    public void A8k() {
        try {
            C18600si r2 = this.A03;
            JSONObject A0c = C117295Zj.A0c(r2);
            A0c.remove("token");
            A0c.remove("tokenTs");
            A0c.remove("vpa");
            A0c.remove("vpaId");
            A0c.remove("vpaTs");
            A0c.remove("listKeys");
            A0c.remove("listKeysTs");
            A0c.remove("skipDevBinding");
            A0c.remove("devBindingByPsp");
            A0c.remove("psp");
            A0c.remove("sequenceNumberPrefix");
            A0c.remove("devBinding");
            A0c.remove("signedQrCode");
            A0c.remove("signedQrCodeTs");
            C117295Zj.A1E(r2, A0c);
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteTokenAndKeys threw: ", e);
        }
    }

    @Override // X.AnonymousClass17Y
    public synchronized boolean A8l(String str, boolean z) {
        boolean z2;
        try {
            C18600si r2 = this.A03;
            String A04 = r2.A04();
            if (!TextUtils.isEmpty(A04)) {
                JSONObject A05 = C13000ix.A05(A04);
                if (TextUtils.isEmpty(str)) {
                    A05.remove("smsVerifDataSentToPsp");
                    A05.remove("devBindingByPsp");
                } else {
                    JSONObject optJSONObject = A05.optJSONObject("smsVerifDataSentToPsp");
                    if (optJSONObject != null) {
                        optJSONObject.remove(str);
                    }
                    JSONObject optJSONObject2 = A05.optJSONObject("devBindingByPsp");
                    if (optJSONObject2 != null) {
                        optJSONObject2.remove(str);
                    }
                }
                if (z) {
                    A05.remove("psp");
                }
                A05.remove("sequenceNumberPrefix");
                A05.remove("skipDevBinding");
                A05.remove("smsVerifData");
                A05.remove("smsVerifDataGateway");
                A05.remove("devBinding");
                A05.remove("smsVerifDataGen");
                A05.remove("device_binding_sim_iccid");
                A05.remove("device_binding_sim_id");
                A05.remove("device_binding_sim_subscripiton_id");
                C117295Zj.A1E(r2, A05);
            }
            z2 = true;
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs deleteDeviceBinding threw: ", e);
            z2 = false;
        }
        return z2;
    }

    @Override // X.AnonymousClass17Y
    public boolean AdJ(AbstractC30891Zf r3) {
        return !this.A02.A0C() && A04().A00();
    }

    @Override // X.AnonymousClass17Y
    public synchronized boolean AfM() {
        this.A02.A07("tos_no_wallet");
        C12960it.A0t(C117295Zj.A05(this.A03), "payment_account_recovered", true);
        return true;
    }

    @Override // X.AnonymousClass17Y
    public synchronized boolean AfV(AnonymousClass1ZY r6) {
        boolean z;
        if (r6 != null) {
            if (r6 instanceof C119755f3) {
                C119755f3 r62 = (C119755f3) r6;
                A0F(r62.A09, r62.A0F);
                String str = r62.A0A;
                try {
                    C18600si r2 = this.A03;
                    JSONObject A0b = C117295Zj.A0b(r2);
                    if (!TextUtils.isEmpty(str)) {
                        A0b.put("psp", str);
                    }
                    C117295Zj.A1E(r2, A0b);
                } catch (JSONException unused) {
                    Log.w("PAY: IndiaUpiPaymentSharedPrefs storePsp threw");
                }
                z = true;
            }
        }
        z = false;
        return z;
    }

    public String toString() {
        String str;
        try {
            JSONObject A0b = C117295Zj.A0b(this.A03);
            if (!TextUtils.isEmpty(A0b.optString("listKeys"))) {
                str = "[keys exist]";
            } else {
                str = "[no keys]";
            }
            A0b.put("listKeys", str);
            String optString = A0b.optString("vpa");
            if (!TextUtils.isEmpty(optString)) {
                A0b.put("vpa", C1309060l.A02(optString));
            }
            String optString2 = A0b.optString("smsVerifDataGateway");
            if (!TextUtils.isEmpty(optString2)) {
                A0b.put("smsVerifDataGateway", optString2);
            }
            A02("smsVerifDataGen", A0b);
            A02("smsVerifData", A0b);
            A02("token", A0b);
            JSONObject optJSONObject = A0b.optJSONObject("smsVerifDataSentToPsp");
            if (optJSONObject != null) {
                Iterator<String> keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    JSONObject optJSONObject2 = optJSONObject.optJSONObject(C12970iu.A0x(keys));
                    if (optJSONObject2 != null) {
                        A02("smsVerifData", optJSONObject2);
                    }
                }
            }
            return A0b.toString();
        } catch (JSONException e) {
            Log.w("PAY: IndiaUpiPaymentSharedPrefs toString threw: ", e);
            return "";
        }
    }
}
