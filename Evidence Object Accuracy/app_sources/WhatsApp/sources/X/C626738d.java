package X;

import android.database.Cursor;
import android.os.SystemClock;
import com.whatsapp.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: X.38d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C626738d extends AbstractC16350or {
    public int A00;
    public long A01 = 1;
    public C30031Vu A02;
    public AbstractC15340mz A03;
    public final C15650ng A04;
    public final C15240mn A05;
    public final C15250mo A06;
    public final AnonymousClass134 A07;
    public final AbstractC14640lm A08;
    public final AbstractC15340mz A09;
    public final WeakReference A0A;
    public final boolean A0B;

    public C626738d(AbstractC14050kl r3, C15650ng r4, C15240mn r5, C15250mo r6, AnonymousClass134 r7, AbstractC15340mz r8, boolean z) {
        this.A07 = r7;
        this.A05 = r5;
        this.A04 = r4;
        AbstractC14640lm r0 = r6.A04;
        AnonymousClass009.A05(r0);
        this.A08 = r0;
        this.A06 = r6;
        this.A0B = z;
        this.A09 = r8;
        this.A0A = C12970iu.A10(r3);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        C34981h1 A00;
        String str;
        String[] strArr;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        C15240mn r6 = this.A05;
        AbstractC15340mz r2 = this.A09;
        boolean z = this.A0B;
        C15250mo r7 = this.A06;
        if (r6.A0O()) {
            C28181Ma r12 = new C28181Ma("FtsMessageStore/searchforjid");
            AbstractC14640lm r3 = r7.A04;
            AnonymousClass009.A05(r3);
            long A02 = C30041Vv.A02(r2);
            if (A02 == Long.MIN_VALUE) {
                Log.e("FtsMessageStore/searchforjid/startSortId < 0");
            } else {
                StringBuilder A0k = C12960it.A0k("start:");
                A0k.append(A02);
                A0k.append(" up:");
                A0k.append(z);
                r12.A02(A0k.toString());
                if (!r7.A02().isEmpty()) {
                    long A04 = r6.A04();
                    String str2 = "";
                    if (A04 == 1) {
                        StringBuilder A0k2 = C12960it.A0k(C34971h0.A06);
                        C242214r.A02(A0k2, z);
                        str2 = C12960it.A0d(" LIMIT 1", A0k2);
                        strArr = new String[]{r6.A0G(r7.A01()), String.valueOf(r6.A06.A02(r3)), String.valueOf(A02)};
                        str = "FIND_FTS_MESSAGE_FOR_JID_DEPRECATED";
                    } else if (A04 == 5) {
                        r7.A0F = true;
                        StringBuilder A0k3 = C12960it.A0k(C34971h0.A08);
                        C242214r.A02(A0k3, z);
                        str2 = C12960it.A0d(" LIMIT 1", A0k3);
                        strArr = new String[]{r6.A0B(null, r7, null), String.valueOf(A02)};
                        str = "FIND_FTS_MESSAGE_FOR_JID";
                    } else {
                        str = str2;
                        strArr = null;
                    }
                    AnonymousClass009.A05(strArr);
                    r12.A02("compiled");
                    A00 = r6.A08(str2, str, strArr);
                    if (A00.A00 == -2) {
                        r7.A0F = false;
                        A00 = r6.A08(str2, "FIND_FTS_MESSAGE_FOR_JID_CONTENT_ONLY", new String[]{r6.A0B(null, r7, null), String.valueOf(A02)});
                    }
                    r12.A02(C12970iu.A0w(C12960it.A0k("found: "), A00.A01));
                    r12.A01();
                }
            }
            A00 = C34981h1.A00(-4);
        } else {
            C28181Ma r4 = new C28181Ma("FtsMessageStore/like/searchforjid");
            AbstractC14640lm r8 = r7.A04;
            AnonymousClass009.A05(r8);
            long A022 = C30041Vv.A02(r2);
            if (A022 == Long.MIN_VALUE) {
                Log.e("FtsMessageStore/like/searchforjid/startid < 0");
                A00 = C34981h1.A00(-4);
            } else {
                String replace = r7.A01().replace("'", "''").replace("%", "\\%");
                StringBuilder A0k4 = C12960it.A0k("(");
                A0k4.append("(");
                A0k4.append("data");
                A0k4.append(" LIKE '%");
                A0k4.append(replace);
                A0k4.append("%' ESCAPE '\\' AND ");
                A0k4.append("message_type");
                A0k4.append(" = '");
                A0k4.append(0);
                A0k4.append("') ");
                A0k4.append(" OR ");
                A0k4.append("(");
                C12960it.A1L("media_name", " LIKE '%", replace, A0k4);
                C12960it.A1L("%' ESCAPE '\\' AND ", "message_type", " = '", A0k4);
                A0k4.append(5);
                C12960it.A1L("') ", " OR ", "(", A0k4);
                C12960it.A1L("media_caption", " LIKE '%", replace, A0k4);
                C12960it.A1L("%' ESCAPE '\\' AND NOT ", "message_type", " = '", A0k4);
                A0k4.append(0);
                A0k4.append("') ");
                A0k4.append(")");
                StringBuilder A0k5 = C12960it.A0k("   SELECT _id, sort_id FROM available_message_view WHERE chat_row_id = ?");
                A0k5.append(" AND ");
                A0k5.append((CharSequence) A0k4);
                C242214r.A02(A0k5, z);
                String A0d = C12960it.A0d(" LIMIT 1", A0k5);
                A00 = C34981h1.A00(-4);
                String[] strArr2 = {String.valueOf(r6.A06.A02(r8)), String.valueOf(A022)};
                C16310on A01 = r6.A0C.get();
                try {
                    Cursor A09 = A01.A03.A09(A0d, strArr2);
                    if (A09.moveToNext()) {
                        A00 = new C34981h1(1, A09.getLong(A09.getColumnIndexOrThrow("sort_id")), A09.getLong(A09.getColumnIndexOrThrow("_id")));
                    }
                    A09.close();
                    A01.close();
                    r4.A02(C12970iu.A0w(C12960it.A0k("found: "), A00.A01));
                    r4.A01();
                } catch (Throwable th) {
                    try {
                        A01.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
        }
        long j = A00.A01;
        if (j > 0) {
            C15650ng r22 = this.A04;
            AbstractC14640lm r32 = this.A08;
            this.A02 = r22.A09(r32, 100, j, -1);
            this.A03 = r22.A0K.A00(j);
            this.A00 = this.A07.A02(r32, this.A02.A02, A00.A02);
            Cursor cursor = this.A02.A00;
            if (cursor != null) {
                this.A02.A00.moveToPosition(Math.max(0, (cursor.getCount() - this.A00) - 50));
            }
        }
        if (this.A01 != 1) {
            return null;
        }
        ActivityC13810kN.A0q(elapsedRealtime, 300);
        return null;
    }
}
