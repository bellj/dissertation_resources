package X;

import java.util.HashMap;

/* renamed from: X.5zm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130665zm {
    public C125695rf A00;
    public C126815tU A01;
    public C127355uM A02;
    public C125705rg A03;
    public C124975qT A04;
    public Boolean A05;
    public String A06;

    public C130665zm(C125695rf r1, C126815tU r2, C127355uM r3, C125705rg r4, C124975qT r5, Boolean bool, String str) {
        this.A06 = str;
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = r2;
        this.A00 = r1;
        this.A04 = r5;
        this.A05 = bool;
    }

    public static C130665zm A00(AnonymousClass1V8 r19) {
        String str;
        C127355uM r15;
        C125705rg r0;
        C126815tU r14;
        C125695rf r13;
        C124975qT r17;
        AnonymousClass1V8 A0E = r19.A0E("name");
        AnonymousClass1V8 A0E2 = r19.A0E("id_doc");
        AnonymousClass1V8 A0E3 = r19.A0E("saved_dob");
        AnonymousClass1V8 A0E4 = r19.A0E("country");
        AnonymousClass1V8 A0E5 = r19.A0E("address");
        AnonymousClass1V8 A0E6 = r19.A0E("citizenship");
        AnonymousClass1V8 A0E7 = r19.A0E("place_of_birth");
        AnonymousClass1V8 A0E8 = r19.A0E("was_document_uploaded");
        Boolean bool = null;
        if (A0E2 != null) {
            str = A0E2.A0H("name");
        } else {
            str = null;
        }
        if (A0E3 != null) {
            r15 = new C127355uM(A0E3.A0F("day").A04("value"), A0E3.A0F("month").A04("value") - 1, A0E3.A0F("year").A04("value"));
        } else {
            r15 = null;
        }
        if (A0E != null) {
            HashMap A11 = C12970iu.A11();
            AnonymousClass1V8[] r4 = A0E.A03;
            if (r4 != null) {
                for (AnonymousClass1V8 r2 : r4) {
                    A11.put(r2.A0H("type"), r2.A0H("value"));
                }
            }
            r0 = new C125705rg(A11);
        } else {
            r0 = null;
        }
        if (A0E4 != null) {
            r14 = new C126815tU(null, new C126835tW(A0E4.A0H("name"), A0E4.A0H("code")));
        } else {
            r14 = null;
        }
        if (A0E5 != null) {
            HashMap A112 = C12970iu.A11();
            AnonymousClass1V8[] r10 = A0E5.A03;
            if (r10 != null) {
                for (AnonymousClass1V8 r12 : r10) {
                    A112.put(r12.A0H("type"), r12.A0H("value"));
                }
            }
            r13 = new C125695rf(A112);
        } else {
            r13 = null;
        }
        if (A0E6 != null) {
            A0E6.A0H("value");
        }
        if (A0E7 != null) {
            AnonymousClass1V8 A0E9 = A0E7.A0E("city");
            AnonymousClass009.A05(A0E9);
            AnonymousClass1V8 A0E10 = A0E7.A0E("country");
            AnonymousClass009.A05(A0E10);
            A0E9.A0H("value");
            A0E10.A0H("value");
            r17 = new C124975qT();
        } else {
            r17 = null;
        }
        if (A0E8 != null) {
            bool = Boolean.valueOf("true".equals(A0E8.A0H("value")));
        }
        return new C130665zm(r13, r14, r15, r0, r17, bool, str);
    }
}
