package X;

import android.content.Context;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.34q  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34q extends AnonymousClass34r {
    public boolean A00;

    public AnonymousClass34q(Context context, C15570nT r3, C15550nR r4, C15610nY r5, C63563Cb r6, C63543Bz r7, AnonymousClass01d r8, C14830m7 r9, AnonymousClass018 r10, AnonymousClass19M r11, C16630pM r12, AnonymousClass12F r13) {
        super(context, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13);
        A00();
        setId(R.id.gif_row);
    }

    @Override // X.AnonymousClass2V0
    public /* bridge */ /* synthetic */ void A05(AbstractC15340mz r2, List list) {
        AbstractC16130oV r22 = (AbstractC16130oV) r2;
        super.A05(r22, list);
        ((AnonymousClass34r) this).A00.setMessage(r22);
    }

    @Override // X.AnonymousClass34r
    public String getDefaultMessageText() {
        return this.A0C.A03;
    }

    @Override // X.AnonymousClass34r
    public int getDrawableRes() {
        return R.drawable.msg_status_gif;
    }

    @Override // X.AnonymousClass34r
    public int getIconSizeIncrease() {
        return C12960it.A09(this).getDimensionPixelSize(R.dimen.search_gif_icon_label_size_increase);
    }
}
