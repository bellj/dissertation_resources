package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2mV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C57112mV extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C57112mV A03;
    public static volatile AnonymousClass255 A04;
    public int A00;
    public long A01;
    public AnonymousClass1K6 A02 = AnonymousClass277.A01;

    static {
        C57112mV r0 = new C57112mV();
        A03 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r12, Object obj, Object obj2) {
        switch (r12.ordinal()) {
            case 0:
                return A03;
            case 1:
                AbstractC462925h r4 = (AbstractC462925h) obj;
                C57112mV r14 = (C57112mV) obj2;
                this.A02 = r4.Afr(this.A02, r14.A02);
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                long j = this.A01;
                int i2 = r14.A00;
                this.A01 = r4.Afs(j, r14.A01, A1R, C12960it.A1R(i2));
                if (r4 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r42 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A032 = r42.A03();
                            if (A032 == 0) {
                                break;
                            } else if (A032 == 10) {
                                String A0A = r42.A0A();
                                AnonymousClass1K6 r1 = this.A02;
                                if (!((AnonymousClass1K7) r1).A00) {
                                    r1 = AbstractC27091Fz.A0G(r1);
                                    this.A02 = r1;
                                }
                                r1.add(A0A);
                            } else if (A032 == 16) {
                                this.A00 |= 1;
                                this.A01 = r42.A06();
                            } else if (!A0a(r42, A032)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                AbstractC27091Fz.A0R(this.A02);
                return null;
            case 4:
                return new C57112mV();
            case 5:
                return new C81693uQ();
            case 6:
                break;
            case 7:
                if (A04 == null) {
                    synchronized (C57112mV.class) {
                        if (A04 == null) {
                            A04 = AbstractC27091Fz.A09(A03);
                        }
                    }
                }
                return A04;
            default:
                throw C12970iu.A0z();
        }
        return A03;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A02.size(); i3++) {
            i2 += CodedOutputStream.A0B(C12960it.A0g(this.A02, i3));
        }
        int size = 0 + i2 + this.A02.size();
        if ((this.A00 & 1) == 1) {
            size += CodedOutputStream.A05(2, this.A01);
        }
        return AbstractC27091Fz.A07(this, size);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.A02.size(); i++) {
            codedOutputStream.A0I(1, C12960it.A0g(this.A02, i));
        }
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0H(2, this.A01);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
