package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.ArrayList;

/* renamed from: X.09A  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass09A extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass07M A00;

    public AnonymousClass09A(AnonymousClass07M r1) {
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass07M r4 = this.A00;
        ArrayList arrayList = new ArrayList(r4.A05);
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            ((AnonymousClass03J) arrayList.get(i)).A01(r4);
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        ArrayList arrayList = new ArrayList(this.A00.A05);
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            arrayList.get(i);
        }
    }
}
