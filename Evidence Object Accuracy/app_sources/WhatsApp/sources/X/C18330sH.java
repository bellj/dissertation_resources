package X;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import androidx.core.graphics.drawable.IconCompat;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.whatsapp.Conversation;
import com.whatsapp.R;
import com.whatsapp.jid.Jid;
import com.whatsapp.util.Log;

/* renamed from: X.0sH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18330sH {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C238013b A02;
    public final AnonymousClass130 A03;
    public final C15550nR A04;
    public final C15610nY A05;
    public final AnonymousClass131 A06;
    public final C16590pI A07;
    public final C15890o4 A08;
    public final C19990v2 A09;
    public final C20830wO A0A;
    public final C19780uf A0B;
    public final C15600nX A0C;
    public final AbstractC14440lR A0D;

    public C18330sH(AbstractC15710nm r1, C14900mE r2, C238013b r3, AnonymousClass130 r4, C15550nR r5, C15610nY r6, AnonymousClass131 r7, C16590pI r8, C15890o4 r9, C19990v2 r10, C20830wO r11, C19780uf r12, C15600nX r13, AbstractC14440lR r14) {
        this.A07 = r8;
        this.A01 = r2;
        this.A00 = r1;
        this.A0D = r14;
        this.A09 = r10;
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
        this.A08 = r9;
        this.A0B = r12;
        this.A0C = r13;
        this.A06 = r7;
        this.A0A = r11;
    }

    public final C007603x A00(C15370n3 r8, boolean z, boolean z2) {
        Intent intent;
        String str;
        Context context = this.A07.A00;
        String A02 = AbstractC32741cf.A02(this.A05.A04(r8));
        if (z2) {
            intent = new Intent(context, Conversation.class);
            str = "android.intent.action.MAIN";
        } else {
            intent = new Intent();
            str = "com.whatsapp.Conversation";
        }
        intent.setAction(str);
        intent.addFlags(335544320);
        Jid jid = r8.A0D;
        AnonymousClass009.A05(jid);
        intent.putExtra("jid", jid.getRawString());
        intent.putExtra("displayname", A02);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.shortcut_image_size);
        Bitmap bitmap = null;
        if (z && (bitmap = this.A06.A00(context, r8, context.getResources().getDimension(R.dimen.small_avatar_radius), dimensionPixelSize)) == null) {
            AnonymousClass130 r4 = this.A03;
            bitmap = r4.A03(r4.A01.A00, r4.A01(r8));
            if (!(bitmap.getWidth() == dimensionPixelSize && bitmap.getHeight() == dimensionPixelSize)) {
                bitmap = Bitmap.createScaledBitmap(bitmap, dimensionPixelSize, dimensionPixelSize, true);
            }
        }
        C35741ib.A01(intent, "ShortcutIntentHelper");
        Jid jid2 = r8.A0D;
        AnonymousClass009.A05(jid2);
        AnonymousClass03w r3 = new AnonymousClass03w(context, jid2.getRawString());
        Intent[] intentArr = {intent};
        C007603x r2 = r3.A00;
        r2.A0P = intentArr;
        r2.A0B = A02;
        if (bitmap != null) {
            IconCompat iconCompat = new IconCompat(1);
            iconCompat.A06 = bitmap;
            r2.A09 = iconCompat;
        }
        return r3.A00();
    }

    public void A01() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            Log.i("WaShortcutsHelper/deletealldynamicshortcuts");
            C43951xu.A0A(this.A07.A00);
        }
        if (i >= 30) {
            Log.i("WaShortcutsHelper/deleteallcachedshortcuts");
            C43951xu.A0B(this.A07.A00);
        }
    }

    public void A02() {
        if (Build.VERSION.SDK_INT >= 23) {
            this.A0D.Ab4(new RunnableBRunnable0Shape1S0100000_I0_1(this, 35), "WaShortcutsHelper/updateAppShortcuts");
        }
    }

    public void A03(Context context, C15370n3 r8) {
        if (Build.VERSION.SDK_INT >= 30) {
            Log.i("WaShortcutsHelper/publishShortcut");
            C43951xu.A0E(context, this.A03, this.A04, this.A05, this.A06, r8);
        }
    }

    public void A04(C14820m6 r16, C16490p7 r17) {
        if (Build.VERSION.SDK_INT >= 23) {
            r17.A04();
            if (r17.A01) {
                SharedPreferences sharedPreferences = r16.A00;
                if (sharedPreferences.getInt("sharing_shortcuts_version", 0) != 1) {
                    Context context = this.A07.A00;
                    AbstractC15710nm r4 = this.A00;
                    C19990v2 r11 = this.A09;
                    AnonymousClass130 r6 = this.A03;
                    C15550nR r7 = this.A04;
                    C15610nY r8 = this.A05;
                    C43951xu.A0C(context, r4, this.A02, r6, r7, r8, this.A06, this.A08, r11, this.A0A, this.A0B, this.A0C);
                    sharedPreferences.edit().putInt("sharing_shortcuts_version", 1).apply();
                }
            }
        }
    }

    public void A05(C15370n3 r5) {
        Context context = this.A07.A00;
        C007603x A00 = A00(r5, true, false);
        if (C007703y.A08(context)) {
            C007703y.A06(context, A00);
            if (Build.VERSION.SDK_INT >= 26) {
                return;
            }
        } else {
            Intent A01 = C007703y.A01(context, A00);
            A01.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            context.sendBroadcast(A01);
        }
        this.A01.A07(R.string.conversation_shortcut_added, 1);
    }

    public void A06(C15370n3 r4) {
        Context context = this.A07.A00;
        if (Build.VERSION.SDK_INT >= 26) {
            C43951xu.A0G(context, r4);
            return;
        }
        Intent A01 = C007703y.A01(context, A00(r4, false, false));
        A01.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        context.sendBroadcast(A01);
    }

    public void A07(AbstractC14640lm r3) {
        if (Build.VERSION.SDK_INT >= 30) {
            Log.i("WaShortcutsHelper/removeShortcutFromCache");
            C43951xu.A0I(this.A07.A00, r3);
        }
    }
}
