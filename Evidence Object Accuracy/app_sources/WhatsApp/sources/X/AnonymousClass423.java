package X;

import com.whatsapp.jid.UserJid;
import com.whatsapp.status.playback.fragment.StatusPlaybackContactFragment;
import java.util.Collection;

/* renamed from: X.423  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass423 extends C27131Gd {
    public final /* synthetic */ StatusPlaybackContactFragment A00;

    public AnonymousClass423(StatusPlaybackContactFragment statusPlaybackContactFragment) {
        this.A00 = statusPlaybackContactFragment;
    }

    @Override // X.C27131Gd
    public void A00(AbstractC14640lm r3) {
        if (r3 != null) {
            StatusPlaybackContactFragment statusPlaybackContactFragment = this.A00;
            if (r3.equals(statusPlaybackContactFragment.A0P)) {
                statusPlaybackContactFragment.A1K();
            }
        }
    }

    @Override // X.C27131Gd
    public void A03(UserJid userJid) {
        if (userJid != null) {
            StatusPlaybackContactFragment statusPlaybackContactFragment = this.A00;
            if (userJid.equals(statusPlaybackContactFragment.A0P)) {
                statusPlaybackContactFragment.A1K();
            }
        }
    }

    @Override // X.C27131Gd
    public void A06(Collection collection) {
        this.A00.A1K();
    }
}
