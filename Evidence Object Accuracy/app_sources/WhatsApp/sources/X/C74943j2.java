package X;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* renamed from: X.3j2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74943j2 extends AbstractC018308n {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C38671oW A01;

    public C74943j2(C38671oW r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC018308n
    public void A01(Rect rect, View view, C05480Ps r5, RecyclerView recyclerView) {
        rect.set(0, 0, this.A00, 0);
    }
}
