package X;

import java.lang.ref.WeakReference;

/* renamed from: X.4PT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4PT {
    public int A00;
    public boolean A01;
    public final WeakReference A02;

    public AnonymousClass4PT(AnonymousClass5R8 r2, int i) {
        this.A02 = C12970iu.A10(r2);
        this.A00 = i;
    }
}
