package X;

/* renamed from: X.230  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass230 extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;
    public Long A03;

    public AnonymousClass230() {
        super(3056, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(4, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(1, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamDisappearingModeSettingChange {");
        Integer num = this.A00;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "disappearingModeEntryPoint", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "lastToggleTimestamp", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "newEphemeralityDuration", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "previousEphemeralityDuration", this.A03);
        sb.append("}");
        return sb.toString();
    }
}
