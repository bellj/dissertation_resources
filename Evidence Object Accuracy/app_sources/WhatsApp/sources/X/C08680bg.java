package X;

import java.util.List;

/* renamed from: X.0bg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08680bg implements AbstractC72393eW {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final AbstractC65073Ia A04;
    public final List A05;

    @Override // X.AbstractC72393eW
    public Object ADo() {
        return null;
    }

    @Override // X.AbstractC72393eW
    public int AEr() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEs() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEt() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AEu() {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AHs(int i) {
        return 0;
    }

    @Override // X.AbstractC72393eW
    public int AHt(int i) {
        return 0;
    }

    public C08680bg(AbstractC65073Ia r1, List list, int i, int i2, int i3, int i4) {
        this.A05 = list;
        this.A04 = r1;
        this.A02 = i;
        this.A00 = i2;
        this.A03 = i3;
        this.A01 = i4;
    }

    @Override // X.AbstractC72393eW
    public AbstractC72393eW ABK(int i) {
        return (AbstractC72393eW) this.A05.get(i);
    }

    @Override // X.AbstractC72393eW
    public int ABP() {
        return this.A05.size();
    }

    @Override // X.AbstractC72393eW
    public int ADK() {
        return this.A01;
    }

    @Override // X.AbstractC72393eW
    public AbstractC65073Ia AG8() {
        return this.A04;
    }

    @Override // X.AbstractC72393eW
    public int AHm() {
        return this.A03;
    }

    @Override // X.AbstractC72393eW
    public int getHeight() {
        return this.A00;
    }

    @Override // X.AbstractC72393eW
    public int getWidth() {
        return this.A02;
    }
}
