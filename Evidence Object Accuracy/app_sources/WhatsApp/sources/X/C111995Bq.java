package X;

import java.security.PrivilegedAction;

/* renamed from: X.5Bq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C111995Bq implements PrivilegedAction {
    public final /* synthetic */ String A00;

    public C111995Bq(String str) {
        this.A00 = str;
    }

    @Override // java.security.PrivilegedAction
    public Object run() {
        return System.getProperty(this.A00);
    }
}
