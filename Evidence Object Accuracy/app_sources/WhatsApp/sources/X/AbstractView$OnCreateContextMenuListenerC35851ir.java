package X;

import X.AbstractView$OnCreateContextMenuListenerC35851ir;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.redex.RunnableBRunnable0Shape7S0100000_I0_7;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.location.ContactLiveLocationThumbnail;
import com.whatsapp.location.DragBottomSheetIndicator;
import com.whatsapp.location.GroupChatLiveLocationsActivity;
import com.whatsapp.location.GroupChatLiveLocationsActivity2;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.util.ViewOnClickCListenerShape15S0100000_I0_2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1ir  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractView$OnCreateContextMenuListenerC35851ir implements AbstractC18870tC, LocationListener, View.OnCreateContextMenuListener {
    public float A00;
    public float A01;
    public float A02;
    public float A03;
    public float A04 = 0.0f;
    public float A05;
    public float A06 = 0.0f;
    public float A07;
    public float A08;
    public int A09;
    public int A0A;
    public int A0B;
    public long A0C = C26061Bw.A0L;
    public long A0D;
    public Activity A0E;
    public BroadcastReceiver A0F = new C72913fN(this);
    public Bitmap A0G;
    public Bitmap A0H;
    public Drawable A0I;
    public Location A0J;
    public View A0K;
    public View A0L;
    public View A0M;
    public View A0N;
    public View A0O;
    public View A0P;
    public View A0Q;
    public View A0R;
    public View A0S;
    public View A0T;
    public View A0U;
    public TextView A0V;
    public TextView A0W;
    public RecyclerView A0X;
    public RecyclerView A0Y;
    public BottomSheetBehavior A0Z;
    public BottomSheetBehavior A0a;
    public AnonymousClass1J1 A0b;
    public AbstractC14640lm A0c;
    public UserJid A0d;
    public ContactLiveLocationThumbnail A0e;
    public ContactLiveLocationThumbnail A0f;
    public DragBottomSheetIndicator A0g;
    public C36241jY A0h;
    public C36241jY A0i;
    public C90294Ni A0j;
    public C35981j7 A0k;
    public C35991j8 A0l;
    public C30751Yr A0m;
    public C30751Yr A0n;
    public C30751Yr A0o;
    public boolean A0p = false;
    public boolean A0q;
    public boolean A0r;
    public boolean A0s = false;
    public boolean A0t = false;
    public boolean A0u = false;
    public final Handler A0v = new Handler(Looper.getMainLooper());
    public final AnonymousClass12P A0w;
    public final C244615p A0x;
    public final C14900mE A0y;
    public final C15570nT A0z;
    public final C16240og A10;
    public final AnonymousClass2Dn A11 = new C850741b(this);
    public final C22330yu A12;
    public final AnonymousClass130 A13;
    public final C15550nR A14;
    public final C27131Gd A15 = new AnonymousClass421(this);
    public final AnonymousClass10S A16;
    public final C15610nY A17;
    public final C21270x9 A18;
    public final AnonymousClass131 A19;
    public final C14830m7 A1A;
    public final C15890o4 A1B;
    public final AnonymousClass018 A1C;
    public final AbstractC18860tB A1D = new AnonymousClass1m1(this);
    public final AnonymousClass12H A1E;
    public final AbstractC33331dp A1F = new C858544k(this);
    public final C244215l A1G;
    public final AnonymousClass13Q A1H = new C69013Xp(this);
    public final AnonymousClass13P A1I = new C69043Xs(this);
    public final C16030oK A1J;
    public final AnonymousClass13O A1K;
    public final C244415n A1L;
    public final AnonymousClass19Z A1M;
    public final Runnable A1N = new RunnableBRunnable0Shape7S0100000_I0_7(this, 31);
    public final Runnable A1O = new RunnableBRunnable0Shape7S0100000_I0_7(this, 28);
    public final Runnable A1P = new RunnableBRunnable0Shape7S0100000_I0_7(this, 30);
    public final List A1Q = new ArrayList();
    public final List A1R = new ArrayList();
    public final List A1S = new ArrayList();
    public final Map A1T = new HashMap();
    public final Set A1U = new LinkedHashSet();
    public volatile boolean A1V;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARC() {
    }

    @Override // android.location.LocationListener
    public void onProviderDisabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onProviderEnabled(String str) {
    }

    @Override // android.location.LocationListener
    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public AbstractView$OnCreateContextMenuListenerC35851ir(AnonymousClass12P r4, C244615p r5, C14900mE r6, C15570nT r7, C16240og r8, C22330yu r9, AnonymousClass130 r10, C15550nR r11, AnonymousClass10S r12, C15610nY r13, C21270x9 r14, AnonymousClass131 r15, C14830m7 r16, C15890o4 r17, AnonymousClass018 r18, AnonymousClass12H r19, C244215l r20, C16030oK r21, AnonymousClass13O r22, C244415n r23, AnonymousClass19Z r24) {
        this.A1A = r16;
        this.A0y = r6;
        this.A0z = r7;
        this.A1M = r24;
        this.A0w = r4;
        this.A18 = r14;
        this.A1L = r23;
        this.A13 = r10;
        this.A14 = r11;
        this.A17 = r13;
        this.A1C = r18;
        this.A16 = r12;
        this.A1E = r19;
        this.A10 = r8;
        this.A12 = r9;
        this.A0x = r5;
        this.A1B = r17;
        this.A1J = r21;
        this.A0r = r17.A03();
        this.A19 = r15;
        this.A1G = r20;
        this.A1K = r22;
    }

    public static double A00(double d) {
        double sin = Math.sin((d * 3.141592653589793d) / 180.0d);
        return Math.max(Math.min(Math.log((sin + 1.0d) / (1.0d - sin)) / 2.0d, 3.141592653589793d), -3.141592653589793d) / 2.0d;
    }

    public static final String A01(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((C30751Yr) it.next()).A06.getRawString());
        }
        Collections.sort(arrayList);
        return TextUtils.join("|", arrayList);
    }

    public static /* synthetic */ void A02(AbstractView$OnCreateContextMenuListenerC35851ir r1, float f, boolean z) {
        r1.A06 = f;
        r1.A0M(Math.max(r1.A04, f), z);
    }

    public static boolean A03(LatLngBounds latLngBounds) {
        LatLng latLng = latLngBounds.A01;
        double d = latLng.A00;
        LatLng latLng2 = latLngBounds.A00;
        if (d - latLng2.A00 > 80.0d) {
            return false;
        }
        double d2 = latLng2.A01 - latLng.A01;
        if (d2 < 0.0d) {
            d2 += 360.0d;
        }
        if (d2 <= 90.0d) {
            return true;
        }
        return false;
    }

    public Dialog A04(int i) {
        if (i == 0) {
            C004802e r2 = new C004802e(this.A0E);
            r2.A06(R.string.live_location_stop_sharing_dialog);
            r2.A0B(true);
            r2.setNegativeButton(R.string.cancel, null);
            r2.setPositiveButton(R.string.live_location_stop, new DialogInterface.OnClickListener() { // from class: X.4fr
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    AbstractView$OnCreateContextMenuListenerC35851ir r22 = AbstractView$OnCreateContextMenuListenerC35851ir.this;
                    C36021jC.A00(r22.A0E, 0);
                    AbstractC14640lm r1 = r22.A0c;
                    if (r1 != null) {
                        r22.A1J.A0P(r1);
                    }
                }
            });
            AnonymousClass04S create = r2.create();
            create.requestWindowFeature(1);
            return create;
        } else if (i != 2) {
            return null;
        } else {
            DialogInterface$OnClickListenerC96684fq r22 = new DialogInterface.OnClickListener() { // from class: X.4fq
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    AbstractView$OnCreateContextMenuListenerC35851ir r3 = AbstractView$OnCreateContextMenuListenerC35851ir.this;
                    r3.A0E.startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 0);
                    C36021jC.A00(r3.A0E, 2);
                }
            };
            C004802e r1 = new C004802e(this.A0E);
            r1.A07(R.string.gps_required_title);
            r1.A06(R.string.gps_required_body);
            r1.A0B(true);
            r1.setPositiveButton(R.string.ok, r22);
            return r1.create();
        }
    }

    public Bitmap A05(C35991j8 r20) {
        ContactLiveLocationThumbnail contactLiveLocationThumbnail;
        View view;
        List list;
        boolean z;
        Bitmap A01;
        Activity activity;
        int i;
        ArrayList arrayList = new ArrayList();
        int i2 = r20.A00;
        float f = 1.0f;
        if (i2 == 1) {
            contactLiveLocationThumbnail = this.A0f;
            view = this.A0Q;
            list = r20.A04;
            if (list.size() == 1) {
                C15370n3 A0B = this.A14.A0B(((C30751Yr) list.get(0)).A06);
                AnonymousClass131 r14 = this.A19;
                Activity activity2 = this.A0E;
                Bitmap A012 = r14.A01(activity2, A0B, this.A0E.getResources().getDimension(R.dimen.live_location_selected_avatar_radius), activity2.getResources().getDimensionPixelSize(R.dimen.live_location_selected_avatar_size));
                if (A012 == null) {
                    AnonymousClass130 r2 = this.A13;
                    A012 = r2.A03(this.A0E, r2.A01(A0B));
                    z = true;
                } else {
                    z = false;
                }
                arrayList.add(A012);
            } else {
                for (int i3 = 0; i3 < Math.min(list.size(), 4); i3++) {
                    C15370n3 A0B2 = this.A14.A0B(((C30751Yr) list.get(i3)).A06);
                    AnonymousClass131 r0 = this.A19;
                    Activity activity3 = this.A0E;
                    Bitmap A013 = r0.A01(activity3, A0B2, 0.0f, activity3.getResources().getDimensionPixelSize(R.dimen.live_location_selected_avatar_size));
                    if (A013 == null) {
                        A013 = this.A0H;
                    }
                    arrayList.add(A013);
                }
                z = false;
            }
        } else {
            contactLiveLocationThumbnail = this.A0e;
            view = this.A0K;
            list = r20.A04;
            if (list.size() == 1) {
                C15370n3 A0B3 = this.A14.A0B(((C30751Yr) list.get(0)).A06);
                AnonymousClass131 r142 = this.A19;
                Activity activity4 = this.A0E;
                Bitmap A014 = r142.A01(activity4, A0B3, this.A0E.getResources().getDimension(R.dimen.small_avatar_radius), activity4.getResources().getDimensionPixelSize(R.dimen.small_avatar_size));
                if (A014 == null) {
                    AnonymousClass130 r22 = this.A13;
                    A014 = r22.A03(this.A0E, r22.A01(A0B3));
                    z = true;
                } else {
                    z = false;
                }
                arrayList.add(A014);
            } else {
                for (int i4 = 0; i4 < Math.min(list.size(), 4); i4++) {
                    C15370n3 A0B4 = this.A14.A0B(((C30751Yr) list.get(i4)).A06);
                    AnonymousClass131 r02 = this.A19;
                    Activity activity5 = this.A0E;
                    Bitmap A015 = r02.A01(activity5, A0B4, 0.0f, activity5.getResources().getDimensionPixelSize(R.dimen.small_avatar_size));
                    if (A015 == null) {
                        A015 = this.A0G;
                    }
                    arrayList.add(A015);
                }
                z = false;
            }
            if (i2 == 2) {
                f = 0.3f;
            }
        }
        contactLiveLocationThumbnail.setAlpha(f);
        if (arrayList.size() == 1) {
            A01 = (Bitmap) arrayList.get(0);
        } else {
            A01 = C21270x9.A01(arrayList, 0.0f);
        }
        contactLiveLocationThumbnail.setImageBitmap(A01);
        int i5 = r20.A01;
        if (i5 == 0) {
            activity = this.A0E;
            i = R.color.live_location_live_location_marker_glow;
        } else if (i5 != 1) {
            if (i5 == 2) {
                contactLiveLocationThumbnail.A01 = 0;
            }
            contactLiveLocationThumbnail.A03 = z;
            contactLiveLocationThumbnail.A02 = list.size();
            Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
            view.draw(new Canvas(createBitmap));
            return createBitmap;
        } else {
            activity = this.A0E;
            i = R.color.live_location_stale_location_marker;
        }
        contactLiveLocationThumbnail.A01 = AnonymousClass00T.A00(activity, i);
        contactLiveLocationThumbnail.A03 = z;
        contactLiveLocationThumbnail.A02 = list.size();
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    public LatLng A06() {
        double latitude;
        double longitude;
        if (this.A0d != null) {
            for (C30751Yr r5 : this.A1Q) {
                if (r5 != null && r5.A05 > 0 && r5.A06.equals(this.A0d)) {
                    latitude = r5.A00;
                    longitude = r5.A01;
                    break;
                }
            }
        }
        Location A01 = this.A0x.A01("group-chat-live-location-ui");
        if (A01 == null) {
            return null;
        }
        latitude = A01.getLatitude();
        longitude = A01.getLongitude();
        return new LatLng(latitude, longitude);
    }

    public C35991j8 A07(LatLng latLng) {
        LatLng latLng2;
        LatLng latLng3;
        C35971j6 A09 = A09();
        if (A09 != null) {
            Point A00 = A09.A00(latLng);
            Point point = new Point(A00.x - (this.A0B >> 1), A00.y - (this.A0A >> 1));
            Point point2 = new Point(A00.x + (this.A0B >> 1), A00.y + (this.A0A >> 1));
            AnonymousClass3EP r4 = new AnonymousClass3EP();
            AnonymousClass3F0 r8 = A09.A01;
            if (r8 != null) {
                latLng2 = r8.A01(point);
            } else {
                AnonymousClass03T A05 = A09.A00.A05((float) point.x, (float) point.y);
                latLng2 = new LatLng(A05.A00, A05.A01);
            }
            r4.A01(latLng2);
            if (r8 != null) {
                latLng3 = r8.A01(point2);
            } else {
                AnonymousClass03T A052 = A09.A00.A05((float) point2.x, (float) point2.y);
                latLng3 = new LatLng(A052.A00, A052.A01);
            }
            r4.A01(latLng3);
            LatLngBounds A002 = r4.A00();
            for (C35991j8 r1 : this.A1R) {
                if (A002.A00(r1.A00())) {
                    return r1;
                }
            }
        }
        return null;
    }

    public C35991j8 A08(C30751Yr r6) {
        if (r6 == null) {
            return null;
        }
        for (C35991j8 r3 : this.A1R) {
            List list = r3.A04;
            if (list.size() > 1 && list.contains(r6)) {
                return r3;
            }
        }
        return null;
    }

    public C35971j6 A09() {
        if (!(this instanceof C36291je)) {
            AnonymousClass04Q r0 = ((C36301jf) this).A00.A05;
            if (r0 != null) {
                return new C35971j6(r0.A0R);
            }
            return null;
        }
        C35961j4 r02 = ((C36291je) this).A00.A06;
        if (r02 != null) {
            return new C35971j6(r02.A00());
        }
        return null;
    }

    public String A0A(C35991j8 r14) {
        List<C30751Yr> list = r14.A04;
        if (list.size() == 1 && this.A0z.A0F(((C30751Yr) list.get(0)).A06)) {
            return this.A0E.getString(R.string.your_live_location_marker_content_description);
        }
        ArrayList arrayList = new ArrayList();
        for (C30751Yr r0 : list) {
            arrayList.add(r0.A06);
        }
        AnonymousClass018 r5 = this.A1C;
        C15610nY r7 = this.A17;
        HashSet hashSet = new HashSet();
        return r5.A0I(new Object[]{r5.A0F(r7.A0G(hashSet, 3, -1, r7.A0N(arrayList, hashSet), true))}, R.plurals.live_location_marker_content_description, (long) arrayList.size());
    }

    public void A0B() {
        LatLng latLng;
        C35961j4 r3;
        AnonymousClass4IX A01;
        AnonymousClass04Q r32;
        C05200Oq r2;
        if (!(this instanceof C36291je)) {
            GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = ((C36301jf) this).A00;
            if (!(!((ActivityC13810kN) groupChatLiveLocationsActivity).A0E) && groupChatLiveLocationsActivity.A05 != null) {
                if (groupChatLiveLocationsActivity.A0L.A0o != null && !groupChatLiveLocationsActivity.A0W) {
                    groupChatLiveLocationsActivity.A0W = true;
                    C30751Yr r6 = groupChatLiveLocationsActivity.A0L.A0o;
                    AnonymousClass03T r4 = new AnonymousClass03T(r6.A00, r6.A01);
                    float A02 = GroupChatLiveLocationsActivity.A02(groupChatLiveLocationsActivity, r6.A02, -1.0f);
                    if (A02 > groupChatLiveLocationsActivity.A05.A02().A02 || A02 == -1.0f) {
                        r32 = groupChatLiveLocationsActivity.A05;
                        r2 = new C05200Oq();
                        r2.A06 = r4;
                    } else {
                        r32 = groupChatLiveLocationsActivity.A05;
                        r2 = AnonymousClass0R9.A01(r4, A02);
                    }
                    r32.A0B(r2, groupChatLiveLocationsActivity.A04, 1500);
                } else if (!groupChatLiveLocationsActivity.A0L.A0u) {
                    groupChatLiveLocationsActivity.A2i(true);
                }
            }
        } else {
            C36291je r33 = (C36291je) this;
            GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = r33.A00;
            if (!(!((ActivityC13810kN) groupChatLiveLocationsActivity2).A0E) && groupChatLiveLocationsActivity2.A06 != null && !groupChatLiveLocationsActivity2.A0X) {
                AbstractView$OnCreateContextMenuListenerC35851ir r1 = groupChatLiveLocationsActivity2.A0M;
                if (r1.A0o == null || r33.A0t) {
                    C35991j8 r0 = r1.A0l;
                    if (r0 != null) {
                        latLng = r0.A00();
                        if (!groupChatLiveLocationsActivity2.A06.A00().A02().A04.A00(latLng)) {
                            AbstractView$OnCreateContextMenuListenerC35851ir r12 = groupChatLiveLocationsActivity2.A0M;
                            if (!r12.A0t) {
                                groupChatLiveLocationsActivity2.A0X = true;
                            } else if (groupChatLiveLocationsActivity2.A2j(r12.A0l.A00())) {
                                groupChatLiveLocationsActivity2.A0M.A0C();
                                return;
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (!r1.A0u) {
                        groupChatLiveLocationsActivity2.A2i(true);
                        return;
                    } else {
                        return;
                    }
                } else {
                    groupChatLiveLocationsActivity2.A0X = true;
                    C30751Yr r34 = groupChatLiveLocationsActivity2.A0M.A0o;
                    latLng = new LatLng(r34.A00, r34.A01);
                    float A022 = GroupChatLiveLocationsActivity2.A02(groupChatLiveLocationsActivity2, r34.A02, -1.0f);
                    if (A022 <= groupChatLiveLocationsActivity2.A06.A02().A02 && A022 != -1.0f) {
                        r3 = groupChatLiveLocationsActivity2.A06;
                        A01 = C65193Io.A02(latLng, A022);
                        r3.A0B(A01, groupChatLiveLocationsActivity2.A05);
                    }
                }
                r3 = groupChatLiveLocationsActivity2.A06;
                A01 = C65193Io.A01(latLng);
                r3.A0B(A01, groupChatLiveLocationsActivity2.A05);
            }
        }
    }

    public void A0C() {
        this.A0o = null;
        this.A0j = null;
        A0Q(null);
        A0I();
        this.A0h.A02();
    }

    public void A0D() {
        this.A0b.A00();
        this.A16.A04(this.A15);
        this.A1E.A04(this.A1D);
        this.A12.A04(this.A11);
        this.A1G.A04(this.A1F);
    }

    public void A0E() {
        this.A10.A04(this);
        this.A0D = 0;
        Handler handler = this.A0v;
        handler.removeCallbacks(this.A1N);
        AnonymousClass13O r4 = this.A1K;
        AbstractC14640lm r3 = this.A0c;
        synchronized (r4.A07) {
            r4.A08.remove(r3);
            if (!r4.A09.contains(r3)) {
                r4.A02(new AnonymousClass2NK(r3, null));
            }
        }
        handler.removeCallbacks(this.A1P);
        handler.removeCallbacks(this.A1O);
        this.A0x.A04(this);
        this.A0J = null;
        this.A0E.unregisterReceiver(this.A0F);
        C16030oK r2 = this.A1J;
        r2.A0c.remove(this.A1I);
        r2.A0b.remove(this.A1H);
    }

    public void A0F() {
        C244615p r5 = this.A0x;
        this.A0q = r5.A07();
        this.A0r = this.A1B.A03();
        Context applicationContext = this.A0E.getApplicationContext();
        C16030oK r4 = this.A1J;
        LocationSharingService.A01(applicationContext, r4);
        if (this.A0m == null) {
            r4.A0W(this.A1H);
            r4.A0X(this.A1I);
            A0J();
        }
        A0K();
        this.A0E.invalidateOptionsMenu();
        this.A0E.registerReceiver(this.A0F, new IntentFilter("android.location.PROVIDERS_CHANGED"));
        if (r4.A0f(this.A0c)) {
            r5.A05(this, "group-chat-live-location-ui-onresume", 0.0f, 3, 5000, 1000);
        }
        this.A10.A03(this);
    }

    public void A0G() {
        if (!(this instanceof C36291je)) {
            C36301jf r2 = (C36301jf) this;
            GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = r2.A00;
            if (!(!((ActivityC13810kN) groupChatLiveLocationsActivity).A0E)) {
                groupChatLiveLocationsActivity.A2f();
                r2.A0B();
                if (!groupChatLiveLocationsActivity.A0L.A0u) {
                    groupChatLiveLocationsActivity.A0K.setLocationMode(2);
                    return;
                }
                return;
            }
            return;
        }
        C36291je r22 = (C36291je) this;
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = r22.A00;
        if (!(!((ActivityC13810kN) groupChatLiveLocationsActivity2).A0E)) {
            groupChatLiveLocationsActivity2.A2f();
            r22.A0B();
            if (!groupChatLiveLocationsActivity2.A0M.A0u) {
                groupChatLiveLocationsActivity2.A0L.setLocationMode(2);
            }
        }
    }

    public final void A0H() {
        int i;
        AnonymousClass018 r8 = this.A1C;
        List list = this.A1S;
        this.A0V.setText(r8.A0I(new Object[]{Integer.valueOf(list.size())}, R.plurals.live_location_marker_title, (long) list.size()));
        this.A0i.A02();
        if (this.A0R != null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.A0E.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int min = Math.min((int) (Math.min(4.5d, (double) list.size()) * ((double) this.A07)), displayMetrics.heightPixels >> 1);
            BottomSheetBehavior bottomSheetBehavior = this.A0Z;
            bottomSheetBehavior.A0J = true;
            bottomSheetBehavior.A0M(5);
            int i2 = this.A0a.A0B;
            RecyclerView recyclerView = this.A0Y;
            if (i2 != 3) {
                recyclerView.setLayoutParams(new LinearLayout.LayoutParams(-1, min));
                this.A0a.A0M(3);
            } else if (min != recyclerView.getHeight()) {
                this.A0Y.clearAnimation();
                C73963h5 r2 = new C73963h5(this.A0Y, this, min);
                r2.setDuration((long) ((int) (((float) min) / this.A0E.getResources().getDisplayMetrics().density)));
                BottomSheetBehavior bottomSheetBehavior2 = this.A0Z;
                if (bottomSheetBehavior2.A0M) {
                    i = -1;
                } else {
                    i = bottomSheetBehavior2.A09;
                }
                A0N((float) i, false);
                this.A0Y.startAnimation(r2);
            }
        } else if (this.A0M.getTranslationY() != 0.0f) {
            this.A0M.clearAnimation();
            View view = this.A0M;
            view.setTranslationY((float) view.getHeight());
            AnonymousClass028.A0F(this.A0M).A06(0.0f);
        }
    }

    public final void A0I() {
        this.A1S.clear();
        this.A0i.A02();
        A0Q(null);
        if (this.A0R != null) {
            this.A0Y.clearAnimation();
            BottomSheetBehavior bottomSheetBehavior = this.A0Z;
            if (bottomSheetBehavior.A0B != 4) {
                bottomSheetBehavior.A0M(4);
                A0X(true);
            }
            BottomSheetBehavior bottomSheetBehavior2 = this.A0a;
            if (bottomSheetBehavior2.A0B != 5) {
                bottomSheetBehavior2.A0M(5);
            } else {
                this.A06 = 0.0f;
                A0M(Math.max(this.A04, 0.0f), true);
            }
        } else {
            this.A0M.clearAnimation();
            AnonymousClass028.A0F(this.A0M).A06((float) this.A0M.getHeight());
        }
        A0G();
    }

    public final void A0J() {
        AnonymousClass457 r4 = new AnonymousClass457(this.A0c, this);
        Handler handler = this.A0v;
        Runnable runnable = this.A1N;
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, this.A0C);
        AnonymousClass13O r3 = this.A1K;
        AbstractC14640lm r2 = this.A0c;
        synchronized (r3.A07) {
            r3.A08.add(r2);
            r3.A01(r4);
        }
    }

    public final void A0K() {
        this.A0y.A0H(new RunnableBRunnable0Shape7S0100000_I0_7(this, 29));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0L() {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractView$OnCreateContextMenuListenerC35851ir.A0L():void");
    }

    public void A0M(float f, boolean z) {
        if (!(this instanceof C36291je)) {
            C36301jf r3 = (C36301jf) this;
            GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = r3.A00;
            int i = (int) f;
            groupChatLiveLocationsActivity.A01 = i;
            AnonymousClass04Q r0 = groupChatLiveLocationsActivity.A05;
            if (r0 != null) {
                r0.A08(0, 0, i);
            }
            if (z) {
                r3.A0B();
                return;
            }
            return;
        }
        C36291je r32 = (C36291je) this;
        GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = r32.A00;
        int i2 = (int) f;
        groupChatLiveLocationsActivity2.A01 = i2;
        C35961j4 r02 = groupChatLiveLocationsActivity2.A06;
        if (r02 != null) {
            r02.A08(0, 0, 0, i2);
        }
        if (z) {
            r32.A0B();
        }
    }

    public final void A0N(float f, boolean z) {
        this.A04 = f;
        if (this.A0g.getVisibility() != 8) {
            this.A04 -= this.A0E.getResources().getDisplayMetrics().density * 20.0f;
        }
        float f2 = this.A04;
        float max = Math.max(f2, this.A06);
        this.A0P.setTranslationY(-f2);
        A0M(max, z);
    }

    public void A0O(Activity activity, Bundle bundle) {
        this.A0E = activity;
        this.A05 = activity.getResources().getDimension(R.dimen.group_participant_row_height);
        this.A07 = activity.getResources().getDimension(R.dimen.live_location_selected_row_height);
        this.A00 = activity.getResources().getDimension(R.dimen.live_location_list_drag_indicator_height);
        this.A08 = activity.getResources().getDimension(R.dimen.tab_height);
        this.A03 = activity.getResources().getDimension(R.dimen.live_location_row_spacing);
        this.A01 = activity.getResources().getDimension(R.dimen.live_location_row_divider_inset_left);
        this.A02 = activity.getResources().getDimension(R.dimen.live_location_row_divider_inset_right);
        this.A0b = this.A18.A04(activity, "group-chat-live-locations-ui");
        AbstractC14640lm A01 = AbstractC14640lm.A01(activity.getIntent().getStringExtra("jid"));
        AnonymousClass009.A05(A01);
        this.A0c = A01;
        this.A0d = UserJid.getNullable(activity.getIntent().getStringExtra("target"));
        UserJid nullable = UserJid.getNullable(activity.getIntent().getStringExtra("final_location_jid"));
        long longExtra = activity.getIntent().getLongExtra("final_location_timestamp", 0);
        if (nullable != null && longExtra > 0) {
            C30751Yr r5 = new C30751Yr(nullable);
            this.A0m = r5;
            r5.A05 = longExtra;
            r5.A00 = activity.getIntent().getDoubleExtra("final_location_latitude", 0.0d);
            this.A0m.A01 = activity.getIntent().getDoubleExtra("final_location_longitude", 0.0d);
        }
        this.A0q = this.A0x.A07();
        A0W("group-chat-live-location-ui-oncreate");
        this.A0X = (RecyclerView) activity.findViewById(R.id.user_list);
        this.A0N = activity.findViewById(R.id.list_holder);
        this.A0g = (DragBottomSheetIndicator) activity.findViewById(R.id.drag_indicator);
        this.A0P = activity.findViewById(R.id.map_bottom);
        this.A0O = activity.findViewById(R.id.list_holder_shadow);
        View view = this.A0N;
        int i = 8;
        if (view != null) {
            view.setVisibility(8);
            this.A0O.setVisibility(8);
            this.A0g.setVisibility(8);
            this.A0Z = new BottomSheetBehavior() { // from class: com.whatsapp.location.GroupChatLiveLocationsUi$13
                @Override // com.google.android.material.bottomsheet.BottomSheetBehavior, X.AbstractC009304r
                public boolean A0C(MotionEvent motionEvent, View view2, CoordinatorLayout coordinatorLayout) {
                    return AbstractView$OnCreateContextMenuListenerC35851ir.this.A0a.A0B == 5 && super.A0C(motionEvent, view2, coordinatorLayout);
                }
            };
            BottomSheetBehavior bottomSheetBehavior = this.A0Z;
            ((AnonymousClass0B5) this.A0N.getLayoutParams()).A00(bottomSheetBehavior);
            bottomSheetBehavior.A0J = false;
            bottomSheetBehavior.A0E = new AnonymousClass2lZ(this);
            this.A0L = activity.findViewById(R.id.drag_indicator_click);
            ViewOnClickCListenerShape15S0100000_I0_2 viewOnClickCListenerShape15S0100000_I0_2 = new ViewOnClickCListenerShape15S0100000_I0_2(this, 4);
            this.A0g.setOnClickListener(viewOnClickCListenerShape15S0100000_I0_2);
            this.A0L.setOnClickListener(viewOnClickCListenerShape15S0100000_I0_2);
        }
        this.A0S = activity.findViewById(R.id.selected_list_title_holder);
        this.A0V = (TextView) activity.findViewById(R.id.selected_list_title);
        this.A0Y = (RecyclerView) activity.findViewById(R.id.selected_list);
        activity.findViewById(R.id.selected_cancel).setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 5));
        this.A0R = activity.findViewById(R.id.selected_list_holder);
        View findViewById = activity.findViewById(R.id.landscape_selected_list_holder);
        this.A0M = findViewById;
        View view2 = this.A0R;
        if (view2 != null) {
            BottomSheetBehavior A00 = BottomSheetBehavior.A00(view2);
            this.A0a = A00;
            A00.A0E = new C56732la(this);
            A00.A0N = true;
            A00.A0M(5);
        } else {
            findViewById.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver$OnPreDrawListenerC101874oE(this));
        }
        this.A0I = AnonymousClass00T.A04(activity, R.drawable.live_location_list_divider);
        C54582gx r7 = new C54582gx(this);
        List<C30751Yr> list = this.A1Q;
        this.A0h = new C36241jY(this, list, false);
        this.A0X.setLayoutManager(new LinearLayoutManager());
        this.A0X.setAdapter(this.A0h);
        RecyclerView recyclerView = this.A0X;
        recyclerView.A0h = true;
        recyclerView.setOnCreateContextMenuListener(this);
        this.A0X.A0l(r7);
        List list2 = this.A1S;
        this.A0i = new C36241jY(this, list2, true);
        this.A0Y.setLayoutManager(new LinearLayoutManager());
        this.A0Y.setAdapter(this.A0i);
        RecyclerView recyclerView2 = this.A0Y;
        recyclerView2.A0h = true;
        recyclerView2.A0l(r7);
        this.A0W = (TextView) activity.findViewById(R.id.status);
        this.A0T = activity.findViewById(R.id.status_panel);
        A0X(true);
        this.A1E.A03(this.A1D);
        this.A16.A03(this.A15);
        this.A12.A03(this.A11);
        this.A1G.A03(this.A1F);
        View inflate = View.inflate(this.A0E, R.layout.contact_live_location_marker, null);
        this.A0K = inflate;
        this.A0e = (ContactLiveLocationThumbnail) inflate.findViewById(R.id.contact_photo);
        this.A0G = AnonymousClass130.A00(activity, 0.0f, R.drawable.avatar_contact, activity.getResources().getDimensionPixelSize(R.dimen.small_avatar_size));
        this.A0H = AnonymousClass130.A00(activity, 0.0f, R.drawable.avatar_contact, activity.getResources().getDimensionPixelSize(R.dimen.live_location_selected_avatar_size));
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.A0K.measure(makeMeasureSpec, makeMeasureSpec);
        this.A0K.layout(0, 0, this.A0K.getMeasuredWidth(), this.A0K.getMeasuredHeight());
        View inflate2 = View.inflate(this.A0E, R.layout.contact_live_location_selected_marker, null);
        this.A0Q = inflate2;
        this.A0f = (ContactLiveLocationThumbnail) inflate2.findViewById(R.id.contact_photo);
        this.A0Q.measure(makeMeasureSpec, makeMeasureSpec);
        this.A0B = this.A0Q.getMeasuredWidth();
        int measuredHeight = this.A0Q.getMeasuredHeight();
        this.A0A = measuredHeight;
        this.A0Q.layout(0, 0, this.A0B, measuredHeight);
        if (bundle != null) {
            this.A0s = bundle.getBoolean("map_follow_user", false);
            this.A0u = bundle.getBoolean("map_touched", false);
            AbstractC14640lm A012 = AbstractC14640lm.A01(bundle.getString("selected_user_jid"));
            if (A012 != null) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    C30751Yr r1 = (C30751Yr) it.next();
                    if (r1.A06.equals(A012)) {
                        this.A0o = r1;
                        break;
                    }
                }
            }
            HashSet hashSet = new HashSet();
            C15380n4.A0C(UserJid.class, bundle.getStringArrayList("selected_user_jids"), hashSet);
            if (!hashSet.isEmpty()) {
                for (C30751Yr r12 : list) {
                    if (hashSet.contains(r12.A06)) {
                        list2.add(r12);
                    }
                }
                Collections.sort(list2, new C36231jX(this.A0z, this.A14, this.A17));
                this.A0i.A02();
                A0H();
            }
        }
        View findViewById2 = activity.findViewById(R.id.zoom_out);
        this.A0U = findViewById2;
        findViewById2.setOnClickListener(new ViewOnClickCListenerShape15S0100000_I0_2(this, 6));
        View view3 = this.A0U;
        if (this.A0u && this.A0m == null) {
            i = 0;
        }
        view3.setVisibility(i);
        this.A0k = new C35981j7(this.A1L, new C36231jX(this.A0z, this.A14, this.A17), (float) this.A0B, (float) this.A0A);
        LocationSharingService.A01(activity.getApplicationContext(), this.A1J);
    }

    public void A0P(Bundle bundle) {
        bundle.putBoolean("map_follow_user", this.A0s);
        bundle.putBoolean("map_touched", this.A0u);
        C30751Yr r0 = this.A0o;
        if (r0 != null) {
            bundle.putString("selected_user_jid", r0.A06.getRawString());
        }
        List<C30751Yr> list = this.A1S;
        if (!list.isEmpty()) {
            ArrayList<String> arrayList = new ArrayList<>(list.size());
            for (C30751Yr r02 : list) {
                arrayList.add(r02.A06.getRawString());
            }
            bundle.putStringArrayList("selected_user_jids", arrayList);
        }
    }

    public final void A0Q(C35991j8 r10) {
        ArrayList arrayList = new ArrayList();
        List<C35991j8> list = this.A1R;
        synchronized (list) {
            if (r10 == null) {
                this.A0l = null;
                for (C35991j8 r0 : list) {
                    arrayList.add(new C35991j8(this.A1L, r0.A04, 0));
                }
            } else {
                for (C35991j8 r4 : list) {
                    if (r4 == r10) {
                        arrayList.add(new C35991j8(this.A1L, r4.A04, 1));
                        this.A0l = r4;
                    } else {
                        arrayList.add(new C35991j8(this.A1L, r4.A04, 2));
                    }
                }
            }
            list.clear();
            list.addAll(arrayList);
            this.A0t = false;
        }
    }

    public void A0R(C35991j8 r6, boolean z) {
        A0Q(r6);
        List list = r6.A04;
        if (list.size() == 1) {
            A0T((C30751Yr) list.get(0));
            return;
        }
        this.A0o = null;
        List list2 = this.A1S;
        list2.clear();
        list2.addAll(list);
        Collections.sort(list2, new C36231jX(this.A0z, this.A14, this.A17));
        this.A0i.A02();
        A0H();
        if (z) {
            A0K();
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:84:0x000a */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v0, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARN: Type inference failed for: r6v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r6v11, types: [java.util.List] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0S(X.C35971j6 r12) {
        /*
        // Method dump skipped, instructions count: 461
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractView$OnCreateContextMenuListenerC35851ir.A0S(X.1j6):void");
    }

    public final void A0T(C30751Yr r9) {
        C35961j4 r2;
        AnonymousClass4IX A01;
        Point A00;
        int i;
        AnonymousClass04Q r3;
        C05200Oq r22;
        Point A04;
        int i2;
        this.A0o = null;
        A0I();
        if (r9 != null) {
            for (C35991j8 r1 : this.A1R) {
                if (r1.A02 == r9) {
                    break;
                }
            }
        }
        r1 = null;
        A0Q(r1);
        this.A0o = r9;
        if (!(this instanceof C36291je)) {
            C36301jf r5 = (C36301jf) this;
            GroupChatLiveLocationsActivity groupChatLiveLocationsActivity = r5.A00;
            AnonymousClass04Q r0 = groupChatLiveLocationsActivity.A05;
            AnonymousClass009.A05(r0);
            r0.A07();
            r5.A0u = true;
            View view = r5.A0U;
            int i3 = 8;
            if (groupChatLiveLocationsActivity.A0L.A0m == null) {
                i3 = 0;
            }
            view.setVisibility(i3);
            groupChatLiveLocationsActivity.A0K.setLocationMode(2);
            AnonymousClass03T r52 = new AnonymousClass03T(r9.A00, r9.A01);
            float A02 = GroupChatLiveLocationsActivity.A02(groupChatLiveLocationsActivity, r9.A02, 16.0f);
            groupChatLiveLocationsActivity.A0W = true;
            if (groupChatLiveLocationsActivity.A05.A02().A02 >= A02 || (i2 = (A04 = groupChatLiveLocationsActivity.A05.A0R.A04(r52)).x) <= 0 || A04.y <= 0 || i2 >= groupChatLiveLocationsActivity.A0K.getWidth() || A04.y >= groupChatLiveLocationsActivity.A0K.getHeight()) {
                r3 = groupChatLiveLocationsActivity.A05;
                r22 = new C05200Oq();
                r22.A06 = r52;
            } else {
                r3 = groupChatLiveLocationsActivity.A05;
                r22 = AnonymousClass0R9.A01(r52, A02);
            }
            r3.A0B(r22, groupChatLiveLocationsActivity.A04, 1500);
            groupChatLiveLocationsActivity.A2f();
        } else {
            C36291je r53 = (C36291je) this;
            GroupChatLiveLocationsActivity2 groupChatLiveLocationsActivity2 = r53.A00;
            C35961j4 r02 = groupChatLiveLocationsActivity2.A06;
            if (r02 != null) {
                try {
                    C65873Li r23 = (C65873Li) r02.A01;
                    r23.A03(8, r23.A01());
                    r53.A0u = true;
                    View view2 = r53.A0U;
                    int i4 = 8;
                    if (groupChatLiveLocationsActivity2.A0M.A0m == null) {
                        i4 = 0;
                    }
                    view2.setVisibility(i4);
                    groupChatLiveLocationsActivity2.A0L.setLocationMode(2);
                    if (!groupChatLiveLocationsActivity2.A0X) {
                        groupChatLiveLocationsActivity2.A0X = true;
                        LatLng latLng = new LatLng(r9.A00, r9.A01);
                        float A022 = GroupChatLiveLocationsActivity2.A02(groupChatLiveLocationsActivity2, r9.A02, 16.0f);
                        groupChatLiveLocationsActivity2.A06.A05();
                        if (groupChatLiveLocationsActivity2.A06.A02().A02 >= A022 || (i = (A00 = groupChatLiveLocationsActivity2.A06.A00().A00(latLng)).x) <= 0 || A00.y <= 0 || i >= groupChatLiveLocationsActivity2.A0L.getWidth() || A00.y >= groupChatLiveLocationsActivity2.A0L.getHeight()) {
                            r2 = groupChatLiveLocationsActivity2.A06;
                            A01 = C65193Io.A01(latLng);
                        } else {
                            groupChatLiveLocationsActivity2.A06.A05();
                            r2 = groupChatLiveLocationsActivity2.A06;
                            A01 = C65193Io.A02(latLng, A022);
                        }
                        r2.A0B(A01, groupChatLiveLocationsActivity2.A05);
                    }
                    groupChatLiveLocationsActivity2.A2f();
                } catch (RemoteException e) {
                    throw new C113245Gt(e);
                }
            }
        }
        BottomSheetBehavior bottomSheetBehavior = this.A0Z;
        if (bottomSheetBehavior != null) {
            bottomSheetBehavior.A0M(4);
        }
    }

    public final void A0U(C30751Yr r5) {
        Map map = this.A1T;
        synchronized (map) {
            map.put(r5.A06, r5);
        }
        this.A0v.postDelayed(this.A1O, 3000);
    }

    public void A0V(Float f) {
        C90294Ni r5 = this.A0j;
        if (r5 == null) {
            return;
        }
        if (f == null || ((double) Math.abs(r5.A00 - f.floatValue())) <= 0.05d) {
            String A01 = A01(r5.A01);
            this.A0j = null;
            for (C35991j8 r1 : this.A1R) {
                if (A01.equals(A01(r1.A04))) {
                    A0R(r1, false);
                    return;
                }
            }
        }
    }

    public final void A0W(String str) {
        if (this.A0m != null) {
            List list = this.A1Q;
            list.clear();
            list.add(this.A0m);
        } else {
            List list2 = this.A1Q;
            list2.clear();
            C16030oK r1 = this.A1J;
            list2.addAll(r1.A07(this.A0c));
            if (r1.A0f(this.A0c)) {
                if (this.A0n == null) {
                    C15570nT r0 = this.A0z;
                    r0.A08();
                    C27621Ig r02 = r0.A01;
                    AnonymousClass009.A05(r02);
                    UserJid userJid = (UserJid) r02.A0D;
                    AnonymousClass009.A05(userJid);
                    this.A0n = new C30751Yr(userJid);
                    C244615p r5 = this.A0x;
                    Location A01 = r5.A01(str);
                    if (A01 != null) {
                        this.A0n.A00 = A01.getLatitude();
                        this.A0n.A01 = A01.getLongitude();
                        this.A0n.A05 = A01.getTime();
                        this.A0n.A02 = A01.getSpeed();
                        this.A0n.A03 = (int) A01.getAccuracy();
                        this.A0n.A04 = (int) A01.getBearing();
                    }
                    r5.A05(this, str, 0.0f, 3, 5000, 1000);
                }
                list2.add(0, this.A0n);
                return;
            }
        }
        this.A0n = null;
        this.A0J = null;
        this.A0x.A04(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00d4, code lost:
        if (r0 == 4) goto L_0x00b2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0X(boolean r16) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractView$OnCreateContextMenuListenerC35851ir.A0X(boolean):void");
    }

    public boolean A0Y(int i, int i2) {
        Activity activity = this.A0E;
        if (activity != null) {
            LocationSharingService.A01(activity.getApplicationContext(), this.A1J);
        }
        if (i == 34) {
            if (i2 != -1) {
                return false;
            }
            this.A1L.A07(this.A0E, this.A0c);
        } else if (i != 100) {
            return false;
        } else {
            if (i2 == 1000) {
                AnonymousClass12P r3 = this.A0w;
                Activity activity2 = this.A0E;
                r3.A07(activity2, new C14960mK().A0i(activity2, this.A0c));
                this.A0E.finish();
                return true;
            }
        }
        return true;
    }

    @Override // X.AbstractC18870tC
    public void AR9() {
        A0J();
    }

    @Override // android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
    }

    @Override // android.location.LocationListener
    public void onLocationChanged(Location location) {
        if (AnonymousClass13N.A03(location, this.A0J)) {
            this.A0J = location;
            C30751Yr r2 = this.A0n;
            if (r2 != null) {
                r2.A00 = location.getLatitude();
                this.A0n.A01 = location.getLongitude();
                this.A0n.A05 = location.getTime();
                this.A0n.A02 = location.getSpeed();
                this.A0n.A03 = (int) location.getAccuracy();
                this.A0n.A04 = (int) location.getBearing();
                this.A0h.A02();
                if (this.A0p) {
                    this.A0p = false;
                } else {
                    A0U(this.A0n);
                    return;
                }
            } else if (!this.A1J.A0f(this.A0c)) {
                return;
            }
            A0K();
        }
    }
}
