package X;

import android.app.ActivityManager;
import android.content.SharedPreferences;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Collections;

/* renamed from: X.2BK  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2BK {
    public static int A00(AnonymousClass01d r6) {
        int i;
        int i2;
        ArrayList arrayList = new ArrayList();
        int A01 = AnonymousClass1Q7.A01();
        if (A01 >= 1) {
            int i3 = 2008;
            if (A01 != 1) {
                i3 = 2012;
                if (A01 <= 3) {
                    i3 = 2011;
                }
            }
            arrayList.add(Integer.valueOf(i3));
        }
        long A00 = (long) AnonymousClass1Q7.A00();
        if (A00 != -1) {
            if (A00 <= 528000) {
                i2 = 2008;
            } else if (A00 <= 620000) {
                i2 = 2009;
            } else if (A00 <= 1020000) {
                i2 = 2010;
            } else if (A00 <= 1220000) {
                i2 = 2011;
            } else if (A00 <= 1520000) {
                i2 = 2012;
            } else {
                i2 = 2014;
                if (A00 <= 2020000) {
                    i2 = 2013;
                }
            }
            arrayList.add(Integer.valueOf(i2));
        }
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ActivityManager A03 = r6.A03();
        if (A03 == null) {
            Log.w("deviceinfo/get-total-memory am=null");
        } else {
            A03.getMemoryInfo(memoryInfo);
            long j = memoryInfo.totalMem;
            if (j > 0) {
                if (j <= 201326592) {
                    i = 2008;
                } else if (j <= 304087040) {
                    i = 2009;
                } else if (j <= 536870912) {
                    i = 2010;
                } else if (j <= 1073741824) {
                    i = 2011;
                } else if (j <= 1610612736) {
                    i = 2012;
                } else {
                    i = 2014;
                    if (j <= 2147483648L) {
                        i = 2013;
                    }
                }
                arrayList.add(Integer.valueOf(i));
            }
        }
        if (arrayList.isEmpty()) {
            return -1;
        }
        Collections.sort(arrayList);
        int size = arrayList.size() & 1;
        int size2 = arrayList.size() >> 1;
        if (size == 1) {
            return ((Number) arrayList.get(size2)).intValue();
        }
        int i4 = size2 - 1;
        return ((Number) arrayList.get(i4)).intValue() + ((((Number) arrayList.get(i4 + 1)).intValue() - ((Number) arrayList.get(i4)).intValue()) >> 1);
    }

    public static int A01(AnonymousClass01d r4, C16630pM r5) {
        SharedPreferences A01 = r5.A01(AnonymousClass01V.A07);
        int i = A01.getInt("year_class_cached_value_pref", -1);
        if (i != -1) {
            return i;
        }
        int A00 = A00(r4);
        A01.edit().putInt("year_class_cached_value_pref", A00).apply();
        return A00;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0071, code lost:
        if (X.AnonymousClass1Q7.A00() < 1800000) goto L_0x0024;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A02(X.AnonymousClass01d r7, X.C16630pM r8) {
        /*
            java.lang.String r0 = X.AnonymousClass01V.A07
            android.content.SharedPreferences r6 = r8.A01(r0)
            java.lang.String r5 = "year_class_cached_value_2016_pref"
            r1 = -1
            int r0 = r6.getInt(r5, r1)
            if (r0 != r1) goto L_0x009a
            android.app.ActivityManager$MemoryInfo r1 = new android.app.ActivityManager$MemoryInfo
            r1.<init>()
            android.app.ActivityManager r0 = r7.A03()
            if (r0 != 0) goto L_0x0030
            java.lang.String r0 = "deviceinfo/get-total-memory am=null"
            com.whatsapp.util.Log.w(r0)
        L_0x0020:
            int r7 = A00(r7)
        L_0x0024:
            android.content.SharedPreferences$Editor r0 = r6.edit()
            android.content.SharedPreferences$Editor r0 = r0.putInt(r5, r7)
            r0.apply()
            return r7
        L_0x0030:
            r0.getMemoryInfo(r1)
            long r3 = r1.totalMem
            r1 = -1
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 == 0) goto L_0x0020
            r1 = 805306368(0x30000000, double:3.97874211E-315)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x004e
            int r1 = X.AnonymousClass1Q7.A01()
            r0 = 1
            r7 = 2010(0x7da, float:2.817E-42)
            if (r1 > r0) goto L_0x0024
            r7 = 2009(0x7d9, float:2.815E-42)
            goto L_0x0024
        L_0x004e:
            r1 = 1073741824(0x40000000, double:5.304989477E-315)
            r7 = 2012(0x7dc, float:2.82E-42)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0063
            int r1 = X.AnonymousClass1Q7.A00()
            r0 = 1300000(0x13d620, float:1.821688E-39)
            if (r1 >= r0) goto L_0x0024
            r7 = 2011(0x7db, float:2.818E-42)
            goto L_0x0024
        L_0x0063:
            r1 = 1610612736(0x60000000, double:7.957484216E-315)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0074
            int r1 = X.AnonymousClass1Q7.A00()
            r0 = 1800000(0x1b7740, float:2.522337E-39)
            if (r1 >= r0) goto L_0x0097
            goto L_0x0024
        L_0x0074:
            r1 = 2147483648(0x80000000, double:1.0609978955E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0097
            r1 = 3221225472(0xc0000000, double:1.591496843E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0089
            r7 = 2014(0x7de, float:2.822E-42)
            goto L_0x0024
        L_0x0089:
            r1 = 5368709120(0x140000000, double:2.6524947387E-314)
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            r7 = 2016(0x7e0, float:2.825E-42)
            if (r0 > 0) goto L_0x0024
            r7 = 2015(0x7df, float:2.824E-42)
            goto L_0x0024
        L_0x0097:
            r7 = 2013(0x7dd, float:2.821E-42)
            goto L_0x0024
        L_0x009a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2BK.A02(X.01d, X.0pM):int");
    }
}
