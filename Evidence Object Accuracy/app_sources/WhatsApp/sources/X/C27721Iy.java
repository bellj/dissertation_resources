package X;

/* renamed from: X.1Iy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27721Iy implements AbstractC27731Iz {
    public String A00 = "";
    public final C27711Iw A01;

    @Override // X.AbstractC27731Iz
    public int AHd() {
        return 1;
    }

    public C27721Iy(C27711Iw r2, String str) {
        this.A01 = r2;
        this.A00 = str;
    }

    @Override // X.AbstractC27731Iz
    public boolean A9Y(AbstractC27731Iz r3) {
        return (r3 instanceof C27721Iy) && AnonymousClass1US.A0D(this.A00, ((C27721Iy) r3).A00);
    }

    @Override // X.AbstractC27731Iz
    public long AGJ() {
        C27711Iw r0 = this.A01;
        if (r0 == null) {
            return -1;
        }
        return r0.A11;
    }
}
