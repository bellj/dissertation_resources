package X;

import java.util.Arrays;

/* renamed from: X.25i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C463025i implements AbstractC462925h {
    public static final C463025i A00 = new C463025i();

    @Override // X.AbstractC462925h
    public boolean Afl(boolean z, boolean z2, boolean z3, boolean z4) {
        return z3 ? z4 : z2;
    }

    @Override // X.AbstractC462925h
    public AbstractC27881Jp Afm(AbstractC27881Jp r1, AbstractC27881Jp r2, boolean z, boolean z2) {
        return z2 ? r2 : r1;
    }

    @Override // X.AbstractC462925h
    public double Afn(double d, double d2, boolean z, boolean z2) {
        return z2 ? d2 : d;
    }

    @Override // X.AbstractC462925h
    public float Afo(float f, float f2, boolean z, boolean z2) {
        return z2 ? f2 : f;
    }

    @Override // X.AbstractC462925h
    public int Afp(int i, int i2, boolean z, boolean z2) {
        return z2 ? i2 : i;
    }

    @Override // X.AbstractC462925h
    public long Afs(long j, long j2, boolean z, boolean z2) {
        return z2 ? j2 : j;
    }

    @Override // X.AbstractC462925h
    public Object Afu(Object obj, Object obj2, boolean z) {
        return obj2;
    }

    @Override // X.AbstractC462925h
    public void Afw(boolean z) {
    }

    @Override // X.AbstractC462925h
    public Object Afx(Object obj, Object obj2, boolean z) {
        return obj2;
    }

    @Override // X.AbstractC462925h
    public String Afy(String str, String str2, boolean z, boolean z2) {
        return z2 ? str2 : str;
    }

    @Override // X.AbstractC462925h
    public AbstractC41941uP Afq(AbstractC41941uP r4, AbstractC41941uP r5) {
        int size = r4.size();
        int size2 = r5.size();
        if (size <= 0) {
            return r5;
        }
        if (size2 > 0) {
            if (!((AnonymousClass1K7) r4).A00) {
                r4 = r4.ALY(size2 + size);
            }
            r4.addAll(r5);
        }
        return r4;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass1K6 Afr(AnonymousClass1K6 r4, AnonymousClass1K6 r5) {
        int size = r4.size();
        int size2 = r5.size();
        if (size <= 0) {
            return r5;
        }
        if (size2 > 0) {
            if (!((AnonymousClass1K7) r4).A00) {
                r4 = r4.ALZ(size2 + size);
            }
            r4.addAll(r5);
        }
        return r4;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass1G1 Aft(AnonymousClass1G1 r3, AnonymousClass1G1 r4) {
        if (r3 == null) {
            return r4;
        }
        if (r4 == null) {
            return r3;
        }
        AnonymousClass1G4 A0T = ((AbstractC27091Fz) r3).A0T();
        if (A0T.A02.getClass().isInstance(r4)) {
            A0T.A04((AbstractC27091Fz) ((AnonymousClass1G0) r4));
            return A0T.A02();
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    @Override // X.AbstractC462925h
    public Object Afv(Object obj, Object obj2, boolean z) {
        return z ? Aft((AnonymousClass1G1) obj, (AnonymousClass1G1) obj2) : obj2;
    }

    @Override // X.AbstractC462925h
    public AnonymousClass256 Afz(AnonymousClass256 r8, AnonymousClass256 r9) {
        if (r9 == AnonymousClass256.A04) {
            return r8;
        }
        int i = r8.count + r9.count;
        int[] copyOf = Arrays.copyOf(r8.A02, i);
        System.arraycopy(r9.A02, 0, copyOf, r8.count, r9.count);
        Object[] copyOf2 = Arrays.copyOf(r8.A03, i);
        System.arraycopy(r9.A03, 0, copyOf2, r8.count, r9.count);
        return new AnonymousClass256(copyOf, copyOf2, i, true);
    }
}
