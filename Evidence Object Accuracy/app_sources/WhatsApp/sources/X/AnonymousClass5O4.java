package X;

/* renamed from: X.5O4  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5O4 extends AbstractC112995Fp {
    public int A00;
    public byte[] A01;
    public byte[] A02;
    public byte[] A03;
    public final int A04;
    public final AnonymousClass5XE A05;

    @Override // X.AnonymousClass5XE
    public int AAt() {
        return this.A04;
    }

    @Override // X.AnonymousClass5XE
    public int AZY(byte[] bArr, byte[] bArr2, int i, int i2) {
        int i3 = this.A04;
        A01(bArr, bArr2, i, i3, i2);
        return i3;
    }

    @Override // X.AnonymousClass5XE
    public void reset() {
        byte[] bArr = this.A01;
        System.arraycopy(bArr, 0, this.A03, 0, bArr.length);
        this.A00 = 0;
        this.A05.reset();
    }

    public AnonymousClass5O4(AnonymousClass5XE r3, int i) {
        super(r3);
        if (i > (r3.AAt() << 3) || i < 8 || i % 8 != 0) {
            StringBuilder A0k = C12960it.A0k("0FB");
            A0k.append(i);
            throw C12970iu.A0f(C12960it.A0d(" not supported", A0k));
        }
        this.A05 = r3;
        this.A04 = i / 8;
        int AAt = r3.AAt();
        this.A01 = new byte[AAt];
        this.A03 = new byte[AAt];
        this.A02 = new byte[AAt];
    }

    @Override // X.AnonymousClass5XE
    public String AAf() {
        StringBuilder A0h = C12960it.A0h();
        C72463ee.A0U(A0h, this.A05);
        A0h.append("/OFB");
        return C12960it.A0f(A0h, this.A04 << 3);
    }

    @Override // X.AnonymousClass5XE
    public void AIf(AnonymousClass20L r7, boolean z) {
        if (r7 instanceof C113075Fx) {
            C113075Fx r72 = (C113075Fx) r7;
            byte[] bArr = r72.A01;
            int length = bArr.length;
            byte[] bArr2 = this.A01;
            int length2 = bArr2.length;
            if (length < length2) {
                int i = length2 - length;
                System.arraycopy(bArr, 0, bArr2, i, length);
                for (int i2 = 0; i2 < i; i2++) {
                    bArr2[i2] = 0;
                }
            } else {
                System.arraycopy(bArr, 0, bArr2, 0, length2);
            }
            reset();
            r7 = r72.A00;
        } else {
            reset();
        }
        if (r7 != null) {
            this.A05.AIf(r7, true);
        }
    }
}
