package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.5Kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C114205Kp extends AnonymousClass5LK implements AnonymousClass5WO, AnonymousClass5VE {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A04 = AtomicReferenceFieldUpdater.newUpdater(C114205Kp.class, Object.class, "_reusableCancellableContinuation");
    public Object A00 = AnonymousClass4HG.A01;
    public final Object A01;
    public final AnonymousClass5WO A02;
    public final AbstractC10990fX A03;
    public volatile /* synthetic */ Object _reusableCancellableContinuation;

    @Override // X.AnonymousClass5WO
    public AnonymousClass5X4 ABi() {
        return this.A02.ABi();
    }

    public C114205Kp(AnonymousClass5WO r4, AbstractC10990fX r5) {
        super(-1);
        this.A03 = r5;
        this.A02 = r4;
        Object fold = r4.ABi().fold(C12980iv.A0i(), C94704cP.A00);
        C16700pc.A0C(fold);
        this.A01 = fold;
        this._reusableCancellableContinuation = null;
    }

    @Override // X.AnonymousClass5VE
    public AnonymousClass5VE ABA() {
        AnonymousClass5WO r1 = this.A02;
        if (r1 instanceof AnonymousClass5VE) {
            return (AnonymousClass5VE) r1;
        }
        return null;
    }

    @Override // X.AnonymousClass5WO
    public void Aas(Object obj) {
        AnonymousClass5WO r8 = this.A02;
        AnonymousClass5X4 ABi = r8.ABi();
        Object obj2 = obj;
        Throwable A00 = AnonymousClass5BU.A00(obj);
        if (A00 != null) {
            obj2 = new C94894ci(A00, false);
        }
        AbstractC10990fX r1 = this.A03;
        if (r1.A03(ABi)) {
            this.A00 = obj2;
            ((AnonymousClass5LK) this).A00 = 0;
            r1.A04(this, ABi);
            return;
        }
        AbstractC114165Kl A11 = C72453ed.A11();
        long j = A11.A00;
        if (j >= 4294967296L) {
            this.A00 = obj2;
            ((AnonymousClass5LK) this).A00 = 0;
            A11.A08(this);
            return;
        }
        A11.A00 = j + 4294967296L;
        try {
            AnonymousClass5X4 ABi2 = r8.ABi();
            Object A002 = C94704cP.A00(this.A01, ABi2);
            r8.Aas(obj);
            C94704cP.A01(A002, ABi2);
            do {
            } while (A11.A0A());
        } finally {
            try {
            } finally {
            }
        }
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("DispatchedContinuation[");
        A0k.append(this.A03);
        A0k.append(", ");
        A0k.append(C88244Ev.A00(this.A02));
        return C72453ed.A0t(A0k);
    }
}
