package X;

import java.lang.Thread;

/* renamed from: X.4Eu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88234Eu {
    public static final void A00(AnonymousClass5X4 r6, Throwable th) {
        Throwable runtimeException;
        try {
            AnonymousClass5ZT r0 = (AnonymousClass5ZT) r6.get(AnonymousClass5ZT.A00);
            if (r0 != null) {
                r0.handleException(r6, th);
                return;
            }
        } catch (Throwable th2) {
            if (th != th2) {
                RuntimeException runtimeException2 = new RuntimeException("Exception while trying to handle coroutine exception", th2);
                C88164En.A00(runtimeException2, th);
                th = runtimeException2;
            }
        }
        for (AnonymousClass5ZT r02 : AnonymousClass4GZ.A00) {
            try {
                r02.handleException(r6, th);
            } catch (Throwable th3) {
                Thread currentThread = Thread.currentThread();
                Thread.UncaughtExceptionHandler uncaughtExceptionHandler = currentThread.getUncaughtExceptionHandler();
                if (th == th3) {
                    runtimeException = th;
                } else {
                    runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th3);
                    C88164En.A00(runtimeException, th);
                }
                uncaughtExceptionHandler.uncaughtException(currentThread, runtimeException);
            }
        }
        Thread currentThread2 = Thread.currentThread();
        currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, th);
    }
}
