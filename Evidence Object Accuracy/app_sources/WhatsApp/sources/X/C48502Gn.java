package X;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.LayoutInflater;

/* renamed from: X.2Gn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48502Gn extends ContextWrapper {
    public LayoutInflater A00;
    public final AbstractC18390sN A01;
    public final AnonymousClass046 A02;
    public final AnonymousClass018 A03;

    public C48502Gn(Context context, AbstractC18390sN r4, AnonymousClass018 r5) {
        super(r5.A02(context));
        AnonymousClass046 A00;
        this.A03 = r5;
        this.A01 = r4;
        boolean z = getBaseContext().getResources() instanceof AnonymousClass046;
        Resources resources = getBaseContext().getResources();
        if (z) {
            A00 = (AnonymousClass046) resources;
        } else {
            A00 = AnonymousClass046.A00(resources, r5);
        }
        this.A02 = A00;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Context createConfigurationContext(Configuration configuration) {
        return new C48502Gn(super.createConfigurationContext(configuration), this.A01, this.A03);
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Resources getResources() {
        return this.A02;
    }

    @Override // android.content.ContextWrapper, android.content.Context
    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return super.getSystemService(str);
        }
        LayoutInflater layoutInflater = this.A00;
        if (layoutInflater != null) {
            return layoutInflater;
        }
        C48582Gv r2 = new C48582Gv(this, LayoutInflater.from(getBaseContext()), ((C18380sM) this.A01).A00);
        this.A00 = r2;
        return r2;
    }
}
