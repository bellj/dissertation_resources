package X;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.util.Log;

/* renamed from: X.2Yr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C51762Yr extends BroadcastReceiver {
    public final C19370u0 A00;

    public C51762Yr(C19370u0 r1) {
        AnonymousClass009.A05(r1);
        this.A00 = r1;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (getResultCode() == -1) {
            C27371Hb r8 = new C27371Hb(getResultData(), getResultExtras(true).getLong("timestamp", Long.MAX_VALUE));
            intent.getPackage();
            String str = intent.getPackage();
            C19370u0 r6 = this.A00;
            C27371Hb A00 = r6.A00();
            if (r8.A01 != null && r8.A00 < A00.A00) {
                r6.A01(r8);
                StringBuilder A0k = C12960it.A0k("updated phone id from ");
                A0k.append(A00);
                A0k.append(" to ");
                A0k.append(r8);
                A0k.append(" based on package ");
                Log.i(C12960it.A0d(str, A0k));
                return;
            }
            return;
        }
        intent.getPackage();
    }
}
