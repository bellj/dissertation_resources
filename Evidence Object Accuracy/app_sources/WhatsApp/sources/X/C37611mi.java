package X;

import android.net.Uri;
import com.whatsapp.util.Log;
import java.net.MalformedURLException;
import java.net.URL;

/* renamed from: X.1mi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37611mi {
    public static String A00(String str) {
        if (str == null) {
            return null;
        }
        try {
            return A01(new URL(str));
        } catch (MalformedURLException e) {
            Log.w("redactedversion/not-url", e);
            int length = str.length();
            if (length <= 25) {
                return "***";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str.substring(0, length - 25));
            sb.append("***");
            return sb.toString();
        }
    }

    public static String A01(URL url) {
        String str;
        int length;
        if (url == null) {
            return null;
        }
        String path = url.getPath();
        if (path == null || (length = path.length()) <= 25) {
            str = "";
        } else {
            str = path.substring(0, length - 25);
        }
        Uri.Builder builder = new Uri.Builder();
        Uri.Builder authority = builder.scheme(url.getProtocol()).authority(url.getHost());
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("***");
        AnonymousClass009.A05(path);
        sb.append(path.substring(path.length() - 4));
        authority.path(sb.toString()).encodedQuery(url.getQuery());
        return builder.build().toString();
    }
}
