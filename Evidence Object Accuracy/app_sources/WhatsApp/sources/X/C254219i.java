package X;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.text.TextUtils;
import java.util.ArrayList;

/* renamed from: X.19i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C254219i {
    public final C15570nT A00;
    public final C19990v2 A01;
    public final C14850m9 A02;

    public C254219i(C15570nT r1, C19990v2 r2, C14850m9 r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public Intent A00(C15370n3 r4, AbstractC14640lm r5, boolean z) {
        String A09;
        boolean z2;
        String A04 = C248917h.A04(r5);
        if (r4 == null || !r4.A0J()) {
            if (!this.A02.A07(945) || r4 == null) {
                A09 = this.A01.A09(r5);
            } else {
                A09 = r4.A0U;
            }
            z2 = false;
        } else {
            A09 = r4.A0D();
            z2 = true;
        }
        return A01(A04, A09, z, z2);
    }

    public final Intent A01(String str, String str2, boolean z, boolean z2) {
        Intent intent;
        this.A00.A08();
        AnonymousClass009.A0F(true);
        if (z) {
            intent = new Intent("android.intent.action.INSERT", ContactsContract.Contacts.CONTENT_URI);
        } else {
            intent = new Intent("android.intent.action.INSERT_OR_EDIT");
            intent.setType("vnd.android.cursor.item/contact");
        }
        if (!TextUtils.isEmpty(str2)) {
            if (z2) {
                ArrayList<? extends Parcelable> arrayList = new ArrayList<>();
                ContentValues contentValues = new ContentValues();
                contentValues.put("mimetype", "vnd.android.cursor.item/name");
                contentValues.put("data2", str2);
                arrayList.add(contentValues);
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("mimetype", "vnd.android.cursor.item/organization");
                contentValues2.put("data1", str2);
                arrayList.add(contentValues2);
                intent.putParcelableArrayListExtra("data", arrayList);
            } else {
                intent.putExtra("name", str2);
            }
        }
        intent.putExtra("phone", str);
        intent.putExtra("phone_type", 2);
        intent.setFlags(524288);
        return intent;
    }
}
