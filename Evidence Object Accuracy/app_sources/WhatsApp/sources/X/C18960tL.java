package X;

import android.content.SharedPreferences;
import com.whatsapp.util.Log;

/* renamed from: X.0tL  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18960tL {
    public final AbstractC15710nm A00;
    public final C21220x4 A01;
    public final C18640sm A02;
    public final C14820m6 A03;
    public final C21210x3 A04;
    public final C14840m8 A05;
    public final ExecutorC27271Gr A06;
    public final C14860mA A07;

    public C18960tL(AbstractC15710nm r3, C21220x4 r4, C18640sm r5, C14820m6 r6, C21210x3 r7, C14840m8 r8, AbstractC14440lR r9, C14860mA r10) {
        this.A00 = r3;
        this.A07 = r10;
        this.A04 = r7;
        this.A05 = r8;
        this.A03 = r6;
        this.A01 = r4;
        this.A02 = r5;
        this.A06 = new ExecutorC27271Gr(r9, false);
    }

    public synchronized void A00() {
        C14840m8 r1 = this.A05;
        if (!r1.A03() && r1.A02()) {
            Log.i("MDOptInInitializer/forceOptIn updated");
            A01();
        }
    }

    public final synchronized void A01() {
        C14860mA r9 = this.A07;
        if (r9.A0M()) {
            Log.i("MDOptInInitializer/Portal Found");
            this.A05.A00();
        } else {
            C14840m8 r5 = this.A05;
            SharedPreferences sharedPreferences = r5.A04.A00;
            if (sharedPreferences.getLong("md_opt_in_awareness_period_deadline", 0) == 0) {
                sharedPreferences.edit().putLong("md_opt_in_awareness_period_deadline", r5.A03.A00() + (((long) r5.A07.A02(857)) * 86400000)).apply();
            }
            if (!r9.A04().isEmpty()) {
                if (r5.A03.A00() < sharedPreferences.getLong("md_opt_in_awareness_period_deadline", 0)) {
                }
            }
            Log.i("MDOptInInitializer/Opting In");
            this.A03.A00.edit().putBoolean("md_opt_in_show_forced_dialog", true).apply();
            r5.A00();
            this.A01.A00();
        }
    }
}
