package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.4RR  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4RR {
    public final Drawable A00;
    public final AnonymousClass5TR A01;
    public final C90154Mu A02;
    public final String A03;

    public AnonymousClass4RR(Drawable drawable, AnonymousClass5TR r2, C90154Mu r3, String str) {
        this.A00 = drawable;
        this.A03 = str;
        this.A01 = r2;
        this.A02 = r3;
    }
}
