package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.2n0  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2n0 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass2n0 A04;
    public static volatile AnonymousClass255 A05;
    public int A00;
    public int A01;
    public long A02;
    public String A03 = "";

    static {
        AnonymousClass2n0 r0 = new AnonymousClass2n0();
        A04 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r15, Object obj, Object obj2) {
        switch (r15.ordinal()) {
            case 0:
                return A04;
            case 1:
                AbstractC462925h r7 = (AbstractC462925h) obj;
                AnonymousClass2n0 r6 = (AnonymousClass2n0) obj2;
                int i = this.A00;
                boolean A1R = C12960it.A1R(i);
                long j = this.A02;
                int i2 = r6.A00;
                this.A02 = r7.Afs(j, r6.A02, A1R, C12960it.A1R(i2));
                this.A01 = r7.Afp(this.A01, r6.A01, C12960it.A1V(i & 2, 2), C12960it.A1V(i2 & 2, 2));
                this.A03 = r7.Afy(this.A03, r6.A03, C12960it.A1V(i & 4, 4), C12960it.A1V(i2 & 4, 4));
                if (r7 == C463025i.A00) {
                    this.A00 = i | i2;
                }
                return this;
            case 2:
                AnonymousClass253 r72 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r72.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 8) {
                                this.A00 |= 1;
                                this.A02 = r72.A06();
                            } else if (A03 == 16) {
                                this.A00 |= 2;
                                this.A01 = r72.A02();
                            } else if (A03 == 26) {
                                String A0A = r72.A0A();
                                this.A00 |= 4;
                                this.A03 = A0A;
                            } else if (!A0a(r72, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            throw AbstractC27091Fz.A0K(this, e);
                        }
                    } catch (C28971Pt e2) {
                        throw AbstractC27091Fz.A0J(e2, this);
                    }
                }
            case 3:
                return null;
            case 4:
                return new AnonymousClass2n0();
            case 5:
                return new C82393vY();
            case 6:
                break;
            case 7:
                if (A05 == null) {
                    synchronized (AnonymousClass2n0.class) {
                        if (A05 == null) {
                            A05 = AbstractC27091Fz.A09(A04);
                        }
                    }
                }
                return A05;
            default:
                throw C12970iu.A0z();
        }
        return A04;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A05(1, this.A02);
        }
        if ((i3 & 2) == 2) {
            i2 = AbstractC27091Fz.A02(2, this.A01, i2);
        }
        if ((i3 & 4) == 4) {
            i2 = AbstractC27091Fz.A04(3, this.A03, i2);
        }
        return AbstractC27091Fz.A07(this, i2);
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0H(1, this.A02);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0F(2, this.A01);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A03);
        }
        AbstractC27091Fz.A0N(codedOutputStream, this);
    }
}
