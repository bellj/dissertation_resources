package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: X.1IC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1IC extends LinkedBlockingQueue<Runnable> {
    public AnonymousClass1IC() {
        super((int) EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH);
    }

    @Override // java.util.concurrent.LinkedBlockingQueue, java.util.concurrent.BlockingQueue, java.util.Queue
    public /* bridge */ /* synthetic */ boolean offer(Object obj) {
        if (size() == 0) {
            return super.offer(obj);
        }
        return false;
    }
}
