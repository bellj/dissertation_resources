package X;

import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;

/* renamed from: X.0kw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC14150kw {
    public static volatile Handler A03;
    public final C14160kx A00;
    public final Runnable A01 = new RunnableBRunnable0Shape0S0100000_I0(this, 7);
    public volatile long A02;

    public AbstractC14150kw(C14160kx r3) {
        this.A00 = r3;
    }

    public static final Handler A00(AbstractC14150kw r3) {
        Handler handler;
        if (A03 != null) {
            return A03;
        }
        synchronized (AbstractC14150kw.class) {
            if (A03 == null) {
                A03 = new HandlerC73363g7(r3.A00.A00.getMainLooper());
            }
            handler = A03;
        }
        return handler;
    }

    public void A01() {
        C15030mR r5 = ((C56632lK) this).A00;
        try {
            C15120mb r7 = r5.A04;
            C14170ky.A00();
            r7.A0G();
            C93924ay r2 = r7.A00;
            if (r2.A00(86400000)) {
                r2.A00 = SystemClock.elapsedRealtime();
                r7.A09("Deleting stale hits (if any)");
                r7.A0D("Deleted stale hits, count", Integer.valueOf(r7.A0I().delete("hits2", "hit_time < ?", new String[]{Long.toString(System.currentTimeMillis() - 2592000000L)})));
            }
            r5.A0I();
        } catch (SQLiteException e) {
            r5.A0E("Failed to delete stale hits", e);
        }
        r5.A05.A02(86400000);
    }

    public final void A02(long j) {
        this.A02 = 0;
        Handler A00 = A00(this);
        Runnable runnable = this.A01;
        A00.removeCallbacks(runnable);
        if (j >= 0) {
            C14160kx r2 = this.A00;
            this.A02 = System.currentTimeMillis();
            if (!A00(this).postDelayed(runnable, j)) {
                C56582lF r22 = r2.A0C;
                C14160kx.A01(r22);
                r22.A0C("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }
}
