package X;

/* renamed from: X.42x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C854642x extends AbstractC16110oT {
    public Long A00;

    public C854642x() {
        super(3460, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamContactDiscoveryTimeToEngagementNonDocs {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "elapsedTimeInSeconds", this.A00);
        return C12960it.A0d("}", A0k);
    }
}
