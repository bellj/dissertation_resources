package X;

/* renamed from: X.1gO  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C34591gO extends AnonymousClass1JQ implements AbstractC34421g7, AbstractC34531gI {
    public final AbstractC14640lm A00;
    public final AnonymousClass1IS A01;
    public final boolean A02;

    public C34591gO(AnonymousClass1JR r11, AbstractC14640lm r12, AnonymousClass1IS r13, String str, long j, boolean z, boolean z2) {
        super(C27791Jf.A03, r11, str, "regular_high", 2, j, z2);
        this.A01 = r13;
        this.A00 = r12;
        this.A02 = z;
    }

    @Override // X.AnonymousClass1JQ
    public AnonymousClass271 A01() {
        AnonymousClass271 A01 = super.A01();
        AnonymousClass009.A05(A01);
        AnonymousClass1G4 A0T = C34581gN.A02.A0T();
        boolean z = this.A02;
        A0T.A03();
        C34581gN r1 = (C34581gN) A0T.A00;
        r1.A00 |= 1;
        r1.A01 = z;
        A01.A03();
        C27831Jk r12 = (C27831Jk) A01.A00;
        r12.A0L = (C34581gN) A0T.A02();
        r12.A00 |= 2;
        return A01;
    }

    @Override // X.AbstractC34421g7, X.AbstractC34381g3
    public /* synthetic */ AbstractC14640lm ABH() {
        AbstractC14640lm r0 = this.A01.A00;
        AnonymousClass009.A05(r0);
        return r0;
    }

    @Override // X.AbstractC34421g7
    public AnonymousClass1IS AEI() {
        return this.A01;
    }

    @Override // X.AbstractC34531gI
    public boolean AKD() {
        return !this.A02;
    }

    @Override // X.AnonymousClass1JQ
    public String toString() {
        StringBuilder sb = new StringBuilder("StarMessageMutation{rowId=");
        sb.append(this.A07);
        sb.append(", key=");
        sb.append(this.A01);
        sb.append(", participant=");
        sb.append(this.A00);
        sb.append(", starred=");
        sb.append(this.A02);
        sb.append(", timestamp=");
        sb.append(this.A04);
        sb.append(", areDependenciesMissing=");
        sb.append(A05());
        sb.append(", operation=");
        sb.append(this.A05);
        sb.append(", collectionName=");
        sb.append(this.A06);
        sb.append(", keyId=");
        sb.append(super.A00);
        sb.append('}');
        return sb.toString();
    }
}
