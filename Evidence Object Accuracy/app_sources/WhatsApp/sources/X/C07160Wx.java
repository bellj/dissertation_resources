package X;

import android.view.View;
import android.widget.AdapterView;

/* renamed from: X.0Wx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07160Wx implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ AnonymousClass0XR A00;

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView adapterView) {
    }

    public C07160Wx(AnonymousClass0XR r1) {
        this.A00 = r1;
    }

    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        C02360Bs r1;
        if (i != -1 && (r1 = this.A00.A0E) != null) {
            r1.A0B = false;
        }
    }
}
