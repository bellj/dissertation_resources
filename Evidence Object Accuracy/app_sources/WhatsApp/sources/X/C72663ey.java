package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.whatsapp.profile.ProfileInfoActivity;

/* renamed from: X.3ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C72663ey extends AnimatorListenerAdapter {
    public boolean A00 = true;
    public final /* synthetic */ ProfileInfoActivity A01;
    public final /* synthetic */ Runnable A02;

    public C72663ey(ProfileInfoActivity profileInfoActivity, Runnable runnable) {
        this.A01 = profileInfoActivity;
        this.A02 = runnable;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        if (this.A00) {
            this.A00 = false;
            if (!(!((ActivityC13810kN) this.A01).A0E)) {
                this.A02.run();
            }
        }
    }
}
