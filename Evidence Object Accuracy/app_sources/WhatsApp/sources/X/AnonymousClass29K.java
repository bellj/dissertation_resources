package X;

import android.util.JsonWriter;
import android.util.Pair;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.29K  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass29K {
    public static int A00(AnonymousClass3HV r9, C15810nw r10, C15890o4 r11, String str) {
        long j;
        String str2;
        File file = new File(str);
        if (!file.exists()) {
            return 1;
        }
        if (r9 == null) {
            return 2;
        }
        long length = file.length();
        AnonymousClass3FI r3 = r9.A02;
        if (r3 != null) {
            j = r3.A00;
        } else {
            j = r9.A00;
        }
        if (length != j) {
            return 4;
        }
        String A09 = C44771zW.A09(r10, r11, file, file.length());
        if (A09 == null) {
            StringBuilder sb = new StringBuilder("gdrive/v2/utils/is-local-same-as-remote/md5-is-null/ ");
            sb.append(file);
            Log.i(sb.toString());
            return 1;
        }
        if (r3 != null) {
            str2 = r3.A03;
        } else {
            str2 = r9.A03;
        }
        if (A09.equals(str2)) {
            return 3;
        }
        return 4;
    }

    public static C65113Ie A01(AbstractC44761zV r2, C44791zY r3, String str, String str2) {
        try {
            return (C65113Ie) C44921zm.A00(new C83993yE(r3, str), r2, str2, 14);
        } catch (C84053yL unused) {
            return null;
        }
    }

    public static Map A02(AbstractC44761zV r6, C65113Ie r7) {
        String str;
        HashMap hashMap = new HashMap(1000);
        String str2 = null;
        do {
            Pair pair = (Pair) C44921zm.A00(new C83973yC(r7, str2), r6, "gdrive/v2/load-files", 14);
            if (pair == null) {
                return null;
            }
            str2 = (String) pair.second;
            for (AnonymousClass3HV r1 : (List) pair.first) {
                AnonymousClass3FI r0 = r1.A02;
                if (r0 != null) {
                    str = r0.A04;
                } else {
                    str = r1.A06;
                }
                hashMap.put(str, r1);
            }
        } while (str2 != null);
        StringBuilder sb = new StringBuilder("gdrive/v2/load-files result ");
        sb.append(hashMap.size());
        Log.i(sb.toString());
        return Collections.unmodifiableMap(hashMap);
    }

    public static Map A03(AbstractC44761zV r8, C65113Ie r9, List list) {
        String str;
        String str2;
        HashMap hashMap = new HashMap(1000);
        String str3 = null;
        do {
            Pair pair = (Pair) C44921zm.A00(new C83963yB(r9, str3), r8, "gdrive/v2/load-files", 14);
            if (pair == null) {
                return null;
            }
            str3 = (String) pair.second;
            for (AnonymousClass3HV r3 : (List) pair.first) {
                AnonymousClass3FI r0 = r3.A02;
                if (r0 != null) {
                    str = r0.A04;
                } else {
                    str = r3.A06;
                }
                AnonymousClass3HV r1 = (AnonymousClass3HV) hashMap.put(str, r3);
                if (r1 != null) {
                    if (str.equals(r3.A06)) {
                        list.add(r3);
                        AnonymousClass3FI r02 = r1.A02;
                        if (r02 != null) {
                            str2 = r02.A04;
                        } else {
                            str2 = r1.A06;
                        }
                        hashMap.put(str2, r1);
                    } else {
                        list.add(r1);
                    }
                }
            }
        } while (str3 != null);
        StringBuilder sb = new StringBuilder("gdrive/v2/load-files result ");
        sb.append(hashMap.size());
        Log.i(sb.toString());
        return Collections.unmodifiableMap(hashMap);
    }

    public static void A04(JsonWriter jsonWriter, Map map) {
        jsonWriter.beginObject();
        for (Map.Entry entry : map.entrySet()) {
            jsonWriter.name((String) entry.getKey());
            Object value = entry.getValue();
            if (value instanceof Number) {
                jsonWriter.value((Number) value);
            } else if (value instanceof String) {
                jsonWriter.value((String) value);
            } else if (value instanceof Boolean) {
                jsonWriter.value(((Boolean) value).booleanValue());
            } else if (value instanceof Map) {
                A04(jsonWriter, (Map) value);
            } else if (value instanceof Set) {
                jsonWriter.beginArray();
                for (Object obj : ((Set) value).toArray()) {
                    jsonWriter.value((String) obj);
                }
                jsonWriter.endArray();
            } else {
                StringBuilder sb = new StringBuilder("Unexpected value type ");
                sb.append(value);
                sb.append(" for ");
                sb.append((String) entry.getKey());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        jsonWriter.endObject();
    }

    public static void A05(C15820nx r5, Collection collection) {
        C14820m6 r3;
        String A09;
        int i;
        if (r5.A04()) {
            boolean z = true;
            Iterator it = collection.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!A09((AnonymousClass3HV) it.next())) {
                        z = false;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (r5.A04() && (A09 = (r3 = r5.A03).A09()) != null) {
                if (!z) {
                    i = 1;
                    if (r3.A06(A09) == 1) {
                        return;
                    }
                } else {
                    i = 2;
                }
                r3.A0l(A09, i);
            }
        }
    }

    public static boolean A06(AnonymousClass5TI r3, AbstractC44761zV r4, C44791zY r5, AnonymousClass3HV r6, File file) {
        C58882rr r2 = new C58882rr(r3, r4, r5, r6, file);
        StringBuilder sb = new StringBuilder("gdrive/restore/file ");
        sb.append(file.getAbsolutePath());
        Boolean bool = (Boolean) C44921zm.A00(r2, r4, sb.toString(), 14);
        return bool != null && bool.booleanValue();
    }

    public static boolean A07(AbstractC44761zV r6, C65113Ie r7, C44791zY r8, List list) {
        int size = list.size();
        int i = 0;
        while (i < size) {
            int i2 = i + 2500;
            if (C44921zm.A00(new C58802rj(r7, r8, list.subList(i, Math.min(i2, size))), r6, "gdrive/backup/files", 14) != Boolean.TRUE) {
                return false;
            }
            i = i2;
        }
        return true;
    }

    public static boolean A08(AbstractC44761zV r3, C44791zY r4) {
        Boolean bool = (Boolean) C44921zm.A00(new C83943y9(r4), r3, "gdrive-backup-util/fetch-token", 14);
        return bool != null && bool.booleanValue();
    }

    public static boolean A09(AnonymousClass3HV r7) {
        EnumC16570pG[] values = EnumC16570pG.values();
        for (EnumC16570pG r3 : values) {
            String str = r7.A06;
            StringBuilder sb = new StringBuilder(".crypt");
            sb.append(r3.version);
            if (str.endsWith(sb.toString())) {
                return true;
            }
        }
        return r7.A06.endsWith(".mcrypt1");
    }

    public static boolean A0A(C14820m6 r2) {
        int A03 = r2.A03();
        return A03 == 11 || A03 == 31 || A03 == 30 || A03 == 28 || A03 == 29;
    }
}
