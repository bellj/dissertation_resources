package X;

/* renamed from: X.56Y  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass56Y implements AbstractC32661cW {
    public int A00 = 0;
    public int A01 = 0;
    public final int A02 = 128;

    @Override // X.AbstractC32661cW
    public int ADt() {
        return this.A00;
    }

    @Override // X.AbstractC32661cW
    public boolean AXF(int i, boolean z) {
        if (z) {
            this.A01++;
        }
        if (this.A01 < this.A02) {
            return true;
        }
        this.A00 = i;
        return false;
    }
}
