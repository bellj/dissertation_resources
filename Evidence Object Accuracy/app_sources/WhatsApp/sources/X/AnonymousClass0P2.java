package X;

import android.os.Bundle;

/* renamed from: X.0P2  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0P2 {
    public final Bundle A00;

    public AnonymousClass0P2(Bundle bundle) {
        this.A00 = bundle;
    }

    public long A00() {
        return this.A00.getLong("referrer_click_timestamp_seconds");
    }

    public String A01() {
        return this.A00.getString("install_referrer");
    }
}
