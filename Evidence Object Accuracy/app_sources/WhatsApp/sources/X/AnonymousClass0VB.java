package X;

import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;

/* renamed from: X.0VB  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0VB implements SQLiteDatabase.CursorFactory {
    public final /* synthetic */ AbstractC12390hq A00;
    public final /* synthetic */ AnonymousClass0ZE A01;

    public AnonymousClass0VB(AbstractC12390hq r1, AnonymousClass0ZE r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.database.sqlite.SQLiteDatabase.CursorFactory
    public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
        this.A00.A6V(new AnonymousClass0ZI(sQLiteQuery));
        return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
    }
}
