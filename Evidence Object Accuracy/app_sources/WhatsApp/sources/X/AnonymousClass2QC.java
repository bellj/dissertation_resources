package X;

import com.whatsapp.jid.Jid;

/* renamed from: X.2QC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2QC extends AnonymousClass2PA {
    public final int A00;
    public final AbstractC14640lm A01;
    public final AnonymousClass1IS A02;

    public AnonymousClass2QC(AbstractC14640lm r1, Jid jid, AnonymousClass1IS r3, String str, int i, long j) {
        super(jid, str, j);
        this.A01 = r1;
        this.A02 = r3;
        this.A00 = i;
    }
}
