package X;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.3Bj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63383Bj {
    public AnonymousClass4TF A00 = null;
    public boolean A01;
    public final C19950uw A02;
    public final C19940uv A03;
    public final C14560ld A04;
    public final C14520lZ A05;
    public final String A06;
    public final AtomicReference A07;
    public final int[] A08;

    public C63383Bj(C19950uw r2, C19940uv r3, C14560ld r4, C14520lZ r5, String str, AtomicReference atomicReference, int[] iArr) {
        this.A03 = r3;
        this.A02 = r2;
        this.A04 = r4;
        this.A05 = r5;
        this.A06 = str;
        this.A07 = atomicReference;
        this.A08 = iArr;
    }
}
