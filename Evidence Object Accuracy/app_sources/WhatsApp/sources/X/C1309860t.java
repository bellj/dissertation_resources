package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: X.60t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1309860t {
    public String A00;
    public final String A01;
    public final String A02;
    public final List A03;
    public final boolean A04;
    public final boolean A05;

    public C1309860t(AnonymousClass1V8 r8) {
        List list;
        String A0H = r8.A0H("format");
        this.A01 = A0H;
        this.A02 = r8.A0H("title");
        this.A04 = "true".equals(r8.A0I("is_editable", null));
        this.A05 = C117295Zj.A1T(r8, "is_optional", null, "true");
        if ("DROPDOWN".equals(A0H)) {
            ArrayList A0l = C12960it.A0l();
            AnonymousClass1V8[] r5 = r8.A03;
            if (r5 != null) {
                for (AnonymousClass1V8 r1 : r5) {
                    String A0H2 = r1.A0H("value");
                    String A0W = C117295Zj.A0W(r1, "text");
                    if (A0W == null) {
                        A0W = A0H2;
                    }
                    A0l.add(new C126825tV(A0H2, A0W));
                }
            }
            list = Collections.unmodifiableList(A0l);
        } else {
            list = null;
        }
        this.A03 = list;
    }

    public C1309860t(String str, String str2, List list, boolean z, boolean z2) {
        this.A01 = str;
        this.A02 = str2;
        this.A04 = z;
        this.A05 = z2;
        this.A03 = list;
    }

    public C1309860t(JSONObject jSONObject) {
        List unmodifiableList;
        this.A01 = jSONObject.getString("format");
        this.A02 = jSONObject.getString("title");
        this.A04 = jSONObject.getBoolean("is_editable");
        this.A05 = jSONObject.getBoolean("is_optional");
        JSONArray optJSONArray = jSONObject.optJSONArray("options");
        if (optJSONArray == null) {
            unmodifiableList = null;
        } else {
            ArrayList A0w = C12980iv.A0w(optJSONArray.length());
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                A0w.add(new C126825tV(jSONObject2.getString("value"), jSONObject2.getString("text")));
            }
            unmodifiableList = Collections.unmodifiableList(A0w);
        }
        this.A03 = unmodifiableList;
    }

    public JSONObject A00() {
        JSONObject put = C117295Zj.A0a().put("format", this.A01).put("title", this.A02).put("is_editable", this.A04).put("is_optional", this.A05);
        List<C126825tV> list = this.A03;
        if (list != null) {
            JSONArray A0L = C117315Zl.A0L();
            for (C126825tV r3 : list) {
                C117315Zl.A0Y(r3.A00, "text", A0L, C117295Zj.A0a().put("value", r3.A01));
            }
            put.put("options", A0L);
        }
        return put;
    }
}
