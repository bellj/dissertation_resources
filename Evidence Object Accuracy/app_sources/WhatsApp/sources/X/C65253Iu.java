package X;

import android.net.Uri;
import androidx.core.view.inputmethod.EditorInfoCompat;
import java.io.EOFException;
import java.io.File;
import java.io.InputStream;

/* renamed from: X.3Iu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65253Iu {
    public static final C90364Np A0D = new C90364Np(30, 72);
    public static final C90364Np A0E = new C90364Np(48, 96);
    public static final byte[] A0F = {69, 120, 105, 102, 0};
    public static final byte[] A0G = {74, 70, 73, 70, 0};
    public static final byte[] A0H = {74, 70, 88, 88, 0};
    public static final int[] A0I = {0, 208, 209, 210, 211, 212, 213, 214, 215};
    public boolean A00;
    public final long A01;
    public final Uri A02;
    public final C16150oX A03;
    public final C15450nH A04;
    public final C39741qT A05;
    public final AnonymousClass01d A06;
    public final C14950mJ A07;
    public final AnonymousClass1KA A08;
    public final C22190yg A09;
    public final File A0A;
    public final boolean A0B;
    public final boolean A0C;

    public C65253Iu(Uri uri, C16150oX r4, C15450nH r5, C39741qT r6, AnonymousClass01d r7, C14950mJ r8, AnonymousClass1KA r9, C22190yg r10, File file, long j, boolean z, boolean z2) {
        this.A04 = r5;
        this.A07 = r8;
        this.A09 = r10;
        this.A06 = r7;
        this.A02 = uri;
        this.A0A = file;
        this.A03 = r4;
        this.A08 = r9;
        this.A0C = z;
        this.A01 = j;
        this.A05 = r6;
        this.A0B = z2;
        int i = r9.A00;
        r6.A03.A0C = C12980iv.A0l(i);
    }

    public static void A00(InputStream inputStream, int i) {
        byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
        while (i > 0) {
            int min = Math.min(Math.min((int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH, i), (int) EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
            int i2 = 0;
            while (i2 < min) {
                int read = inputStream.read(bArr, 0 + i2, min - i2);
                if (read == -1) {
                    break;
                }
                i2 += read;
            }
            if (i2 != -1) {
                i -= 1024;
            } else {
                throw new EOFException(C12960it.A0W(i, "Unexpected EOF skipping "));
            }
        }
    }

    public static boolean A01(byte[] bArr, byte[] bArr2) {
        int length = bArr.length - 0;
        int length2 = bArr2.length;
        if (length >= length2) {
            for (int i = 0; i < length2; i++) {
                if (bArr[0 + i] == bArr2[i]) {
                }
            }
            return true;
        }
        return false;
    }
}
