package X;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;

/* renamed from: X.4cN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C94694cN {
    public static final Class A00;
    public static final Method A01;
    public static final Method A02;

    static {
        Method method;
        Method method2;
        Class A002 = AnonymousClass1TA.A00(C94694cN.class, "javax.crypto.spec.GCMParameterSpec");
        A00 = A002;
        if (A002 != null) {
            try {
                method2 = (Method) AccessController.doPrivileged(new C112045Bv("getTLen"));
            } catch (PrivilegedActionException unused) {
                method2 = null;
            }
            A02 = method2;
            try {
                method = (Method) AccessController.doPrivileged(new C112045Bv("getIV"));
            } catch (PrivilegedActionException unused2) {
                method = null;
            }
        } else {
            method = null;
        }
        A01 = method;
    }

    public static AlgorithmParameterSpec A00(AnonymousClass1TL r6) {
        C114635Mm r62;
        if (r6 != null) {
            try {
                r62 = new C114635Mm(AbstractC114775Na.A04((Object) r6));
            } catch (NoSuchMethodException unused) {
                throw new InvalidParameterSpecException("No constructor found!");
            } catch (Exception e) {
                throw new InvalidParameterSpecException(C12960it.A0d(e.getMessage(), C12960it.A0k("Construction failed: ")));
            }
        } else {
            r62 = null;
        }
        Constructor constructor = A00.getConstructor(Integer.TYPE, byte[].class);
        Object[] objArr = new Object[2];
        C12960it.A1P(objArr, r62.A00 << 3, 0);
        objArr[1] = AnonymousClass1TT.A02(r62.A01);
        return (AlgorithmParameterSpec) constructor.newInstance(objArr);
    }
}
