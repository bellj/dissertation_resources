package X;

import com.whatsapp.util.Log;

/* renamed from: X.3Wb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68613Wb implements AbstractC32851cq {
    public final /* synthetic */ C627338j A00;

    public C68613Wb(C627338j r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        throw C12960it.A0U("must not be called");
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        C627338j r2 = this.A00;
        r2.A00 = -2;
        Log.i(C12960it.A0d(r2.A01, C12960it.A0k("contactsupporttask/externalstorage/avail external storage not calculated, state=")));
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        this.A00.A00 = -2;
        Log.i("contactsupporttask/externalstorage/avail external storage not calculated, permission denied");
    }
}
