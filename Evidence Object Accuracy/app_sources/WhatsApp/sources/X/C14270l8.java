package X;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape0S0100000_I0;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.0l8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14270l8 {
    public static final Handler A0L = new Handler(Looper.getMainLooper());
    public int A00;
    public C63733Cs A01;
    public AnonymousClass4TT A02;
    public AnonymousClass5T1 A03;
    public C94244bU A04 = new C94244bU();
    public AnonymousClass28D A05;
    public Object A06 = new Object();
    public WeakReference A07 = new WeakReference(null);
    public Map A08 = Collections.emptyMap();
    public boolean A09;
    public boolean A0A = false;
    public boolean A0B = false;
    public final C93634aU A0C;
    public final AnonymousClass4ZD A0D;
    public final AbstractC116425Vj A0E;
    public final Runnable A0F = new RunnableBRunnable0Shape0S0100000_I0(this, 16);
    public final List A0G = new ArrayList();
    public final List A0H = new ArrayList();
    public final List A0I = new ArrayList();
    public final List A0J = new ArrayList();
    public final Map A0K = new HashMap();

    public C14270l8(C93634aU r3, AnonymousClass28D r4, AnonymousClass4ZD r5, AbstractC116425Vj r6) {
        this.A05 = r4;
        this.A0C = r3;
        this.A0D = r5;
        this.A0E = r6;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:90:0x00c8 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:56:0x0139 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:66:0x0165 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:91:0x00c8 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r8v2 */
    /* JADX WARN: Type inference failed for: r8v3 */
    /* JADX WARN: Type inference failed for: r8v4, types: [java.util.Map] */
    /* JADX WARN: Type inference failed for: r8v5, types: [java.util.Map] */
    /* JADX WARN: Type inference failed for: r8v6 */
    /* JADX WARN: Type inference failed for: r8v7, types: [java.util.AbstractMap] */
    /* JADX WARN: Type inference failed for: r8v8, types: [java.util.HashMap] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair A00(java.util.List r27) {
        /*
        // Method dump skipped, instructions count: 413
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14270l8.A00(java.util.List):android.util.Pair");
    }

    public C92554Wj A01(C14260l7 r5, AnonymousClass5T1 r6, Map map) {
        C94244bU r3 = this.A04;
        if (!map.isEmpty() || !r3.A00.isEmpty()) {
            r3 = new C94244bU(r3.A02, r3.A01, r3.A03, map);
        }
        this.A04 = r3;
        this.A01 = new C63733Cs(r5.A00);
        this.A07 = new WeakReference(r5);
        this.A03 = r6;
        Pair A00 = A00(Collections.singletonList(this.A0C));
        if (!((List) A00.second).isEmpty()) {
            this.A05 = A03((List) A00.second);
        }
        synchronized (this.A06) {
            this.A0B = true;
            if (this.A0A) {
                Handler handler = A0L;
                Runnable runnable = this.A0F;
                handler.removeCallbacks(runnable);
                handler.post(runnable);
            }
        }
        return new C92554Wj(this.A04, this.A05);
    }

    public AnonymousClass28D A02() {
        if (this.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to access a tree on a destroyed BloksTreeManager");
        }
        AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
        return A03(this.A0I);
    }

    public final AnonymousClass28D A03(List list) {
        AbstractC116425Vj r2 = this.A0E;
        r2.A6N("Bloks SnapshotComponent");
        AnonymousClass28D r1 = this.A05;
        try {
            if (!list.isEmpty()) {
                r1 = AnonymousClass3AD.A00(new C1092751b(list), r1);
            }
            return r1;
        } finally {
            r2.A9T();
        }
    }

    public void A04() {
        if (!this.A09) {
            AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
            List list = this.A0J;
            if (!list.isEmpty()) {
                Pair A00 = A00(list);
                list.clear();
                this.A0I.addAll((Collection) A00.second);
            }
            AbstractC116425Vj r4 = this.A0E;
            r4.A6N("Bloks ModelMutation");
            List list2 = this.A0I;
            AnonymousClass28D A03 = A03(list2);
            boolean z = true;
            boolean z2 = false;
            if (this.A05 != A03) {
                z2 = true;
            }
            this.A05 = A03;
            list2.clear();
            r4.A9T();
            Map map = this.A0K;
            if (!map.isEmpty()) {
                C94244bU r1 = this.A04;
                HashMap hashMap = new HashMap(r1.A03);
                hashMap.putAll(map);
                this.A04 = new C94244bU(r1.A02, r1.A01, hashMap, r1.A00);
                map.clear();
            } else {
                z = z2;
            }
            AnonymousClass5T1 r3 = this.A03;
            if (r3 != null && z) {
                r3.AT3(new C92554Wj(this.A04, this.A05));
            }
        }
    }

    public void A05(AnonymousClass5T5 r3, AnonymousClass4WH r4) {
        if (this.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to enqueue mutations on a destroyed BloksTreeManager");
            return;
        }
        AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
        this.A0I.add(new Pair(r3, r4));
    }

    public void A06(AnonymousClass4WH r4, long j) {
        if (this.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to enqueue mutations on a destroyed BloksTreeManager");
            return;
        }
        AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
        this.A0I.add(new Pair(new C1093251g(j), r4));
    }

    public void A07(AnonymousClass4WH r3, long j) {
        if (this.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to enqueue mutations on a destroyed BloksTreeManager");
            return;
        }
        A06(r3, j);
        A04();
    }

    public void A08(AnonymousClass4WH r4, String str) {
        if (this.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to enqueue mutations on a destroyed BloksTreeManager");
            return;
        }
        AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
        this.A0I.add(new Pair(new C1093351h(str), r4));
    }

    public void A09(String str, Object obj) {
        if (this.A09) {
            C28691Op.A00("BloksTreeManager", "Trying to enqueue mutations on a destroyed BloksTreeManager");
            return;
        }
        AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
        C88824Hg.A05.incrementAndGet();
        this.A0K.put(str, obj);
        synchronized (this.A06) {
            if (!this.A0B) {
                this.A0A = true;
                return;
            }
            Handler handler = A0L;
            Runnable runnable = this.A0F;
            handler.removeCallbacks(runnable);
            handler.post(runnable);
        }
    }
}
