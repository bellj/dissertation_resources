package X;

import java.util.concurrent.Executor;

/* renamed from: X.5E6  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass5E6 implements Executor {
    public static final Executor A00 = new AnonymousClass5E6();

    @Override // java.util.concurrent.Executor
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
