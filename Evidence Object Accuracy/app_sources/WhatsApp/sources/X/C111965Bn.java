package X;

import java.lang.Thread;

/* renamed from: X.5Bn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C111965Bn implements Thread.UncaughtExceptionHandler {
    public final /* synthetic */ C14160kx A00;

    public C111965Bn(C14160kx r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public final void uncaughtException(Thread thread, Throwable th) {
        this.A00.A0C.A0C("Job execution failed", th);
    }
}
