package X;

import android.text.SpannableStringBuilder;
import com.whatsapp.voipcalling.GlVideoRenderer;
import org.chromium.net.UrlRequest;

/* renamed from: X.02S  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass02S {
    public static final AnonymousClass02S A03;
    public static final AnonymousClass02S A04;
    public static final AnonymousClass02T A05;
    public static final String A06 = Character.toString(8206);
    public static final String A07 = Character.toString(8207);
    public final int A00;
    public final AnonymousClass02T A01;
    public final boolean A02;

    static {
        AnonymousClass02T r3 = AnonymousClass02U.A01;
        A05 = r3;
        A03 = new AnonymousClass02S(r3, 2, false);
        A04 = new AnonymousClass02S(r3, 2, true);
    }

    public AnonymousClass02S(AnonymousClass02T r1, int i, boolean z) {
        this.A02 = z;
        this.A00 = i;
        this.A01 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006f, code lost:
        if (r7 == 0) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0071, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0074, code lost:
        if (r4.A01 <= 0) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007a, code lost:
        switch(r4.A00()) {
            case 14: goto L_0x0085;
            case 15: goto L_0x0085;
            case 16: goto L_0x0081;
            case 17: goto L_0x0081;
            case 18: goto L_0x007e;
            default: goto L_0x007d;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007e, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0081, code lost:
        if (r5 != r3) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0085, code lost:
        if (r5 != r3) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0089, code lost:
        r3 = r3 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        return -1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A00(java.lang.CharSequence r7) {
        /*
            r0 = 0
            X.0SH r4 = new X.0SH
            r4.<init>(r7)
            r4.A01 = r0
            r6 = 1
            r5 = 0
            r7 = 0
            r3 = 0
        L_0x000c:
            int r1 = r4.A01
            int r0 = r4.A02
            if (r1 >= r0) goto L_0x006b
            if (r5 != 0) goto L_0x006f
            java.lang.CharSequence r2 = r4.A03
            char r0 = r2.charAt(r1)
            r4.A00 = r0
            boolean r1 = java.lang.Character.isHighSurrogate(r0)
            int r0 = r4.A01
            if (r1 == 0) goto L_0x0057
            int r2 = java.lang.Character.codePointAt(r2, r0)
            int r1 = r4.A01
            int r0 = java.lang.Character.charCount(r2)
            int r1 = r1 + r0
            r4.A01 = r1
            byte r1 = java.lang.Character.getDirectionality(r2)
        L_0x0035:
            if (r1 == 0) goto L_0x0054
            if (r1 == r6) goto L_0x0051
            r0 = 2
            if (r1 == r0) goto L_0x0051
            r0 = 9
            if (r1 == r0) goto L_0x000c
            switch(r1) {
                case 14: goto L_0x004d;
                case 15: goto L_0x004d;
                case 16: goto L_0x0049;
                case 17: goto L_0x0049;
                case 18: goto L_0x0045;
                default: goto L_0x0043;
            }
        L_0x0043:
            r5 = r3
            goto L_0x000c
        L_0x0045:
            int r3 = r3 + -1
            r7 = 0
            goto L_0x000c
        L_0x0049:
            int r3 = r3 + 1
            r7 = 1
            goto L_0x000c
        L_0x004d:
            int r3 = r3 + 1
            r7 = -1
            goto L_0x000c
        L_0x0051:
            if (r3 != 0) goto L_0x0043
            goto L_0x0083
        L_0x0054:
            if (r3 != 0) goto L_0x0043
            goto L_0x0087
        L_0x0057:
            int r0 = r0 + 1
            r4.A01 = r0
            char r1 = r4.A00
            r0 = 1792(0x700, float:2.511E-42)
            if (r1 >= r0) goto L_0x0066
            byte[] r0 = X.AnonymousClass0SH.A04
            byte r1 = r0[r1]
            goto L_0x0035
        L_0x0066:
            byte r1 = java.lang.Character.getDirectionality(r1)
            goto L_0x0035
        L_0x006b:
            if (r5 != 0) goto L_0x006f
        L_0x006d:
            r7 = 0
            return r7
        L_0x006f:
            if (r7 == 0) goto L_0x0072
            return r7
        L_0x0072:
            int r0 = r4.A01
            if (r0 <= 0) goto L_0x006d
            byte r0 = r4.A00()
            switch(r0) {
                case 14: goto L_0x0085;
                case 15: goto L_0x0085;
                case 16: goto L_0x0081;
                case 17: goto L_0x0081;
                case 18: goto L_0x007e;
                default: goto L_0x007d;
            }
        L_0x007d:
            goto L_0x0072
        L_0x007e:
            int r3 = r3 + 1
            goto L_0x0072
        L_0x0081:
            if (r5 != r3) goto L_0x0089
        L_0x0083:
            r7 = 1
            return r7
        L_0x0085:
            if (r5 != r3) goto L_0x0089
        L_0x0087:
            r7 = -1
            return r7
        L_0x0089:
            int r3 = r3 + -1
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass02S.A00(java.lang.CharSequence):int");
    }

    public static int A01(CharSequence charSequence) {
        AnonymousClass0SH r4 = new AnonymousClass0SH(charSequence);
        r4.A01 = r4.A02;
        int i = 0;
        int i2 = 0;
        while (r4.A01 > 0) {
            byte A00 = r4.A00();
            if (A00 != 0) {
                if (A00 == 1 || A00 == 2) {
                    if (i == 0) {
                        return 1;
                    }
                } else if (A00 != 9) {
                    switch (A00) {
                        case UrlRequest.Status.READING_RESPONSE /* 14 */:
                        case 15:
                            if (i2 == i) {
                                return -1;
                            }
                            i--;
                            break;
                        case GlVideoRenderer.CAP_RENDER_I420 /* 16 */:
                        case 17:
                            if (i2 == i) {
                                return 1;
                            }
                            i--;
                            break;
                        case 18:
                            i++;
                            break;
                    }
                } else {
                    continue;
                }
            } else if (i == 0) {
                return -1;
            }
            if (i2 == 0) {
                i2 = i;
            }
        }
        return 0;
    }

    public CharSequence A02(AnonymousClass02T r6, CharSequence charSequence) {
        AnonymousClass02T r2;
        String str;
        AnonymousClass02T r4;
        String str2;
        if (charSequence == null) {
            return null;
        }
        boolean AK0 = r6.AK0(charSequence, 0, charSequence.length());
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if ((this.A00 & 2) != 0) {
            if (AK0) {
                r4 = AnonymousClass02U.A05;
            } else {
                r4 = AnonymousClass02U.A04;
            }
            boolean AK02 = r4.AK0(charSequence, 0, charSequence.length());
            if (!this.A02) {
                if (AK02 || A00(charSequence) == 1) {
                    str2 = A06;
                    spannableStringBuilder.append((CharSequence) str2);
                }
                str2 = "";
                spannableStringBuilder.append((CharSequence) str2);
            } else {
                if (!AK02 || A00(charSequence) == -1) {
                    str2 = A07;
                    spannableStringBuilder.append((CharSequence) str2);
                }
                str2 = "";
                spannableStringBuilder.append((CharSequence) str2);
            }
        }
        boolean z = this.A02;
        if (AK0 != z) {
            char c = 8234;
            if (AK0) {
                c = 8235;
            }
            spannableStringBuilder.append(c);
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append((char) 8236);
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (AK0) {
            r2 = AnonymousClass02U.A05;
        } else {
            r2 = AnonymousClass02U.A04;
        }
        boolean AK03 = r2.AK0(charSequence, 0, charSequence.length());
        if (!z) {
            if (AK03 || A01(charSequence) == 1) {
                str = A06;
            }
            str = "";
        } else {
            if (!AK03 || A01(charSequence) == -1) {
                str = A07;
            }
            str = "";
        }
        spannableStringBuilder.append((CharSequence) str);
        return spannableStringBuilder;
    }
}
