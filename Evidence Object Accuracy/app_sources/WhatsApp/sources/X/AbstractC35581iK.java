package X;

import android.database.ContentObserver;
import java.util.HashMap;

/* renamed from: X.1iK  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public interface AbstractC35581iK {
    HashMap AAz();

    AbstractC35611iN AEC(int i);

    void AaX();

    void close();

    int getCount();

    boolean isEmpty();

    void registerContentObserver(ContentObserver contentObserver);

    void unregisterContentObserver(ContentObserver contentObserver);
}
