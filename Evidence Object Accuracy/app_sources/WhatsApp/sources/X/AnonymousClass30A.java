package X;

/* renamed from: X.30A  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30A extends AbstractC16110oT {
    public Integer A00;

    public AnonymousClass30A() {
        super(3528, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamSurfaceGroupsLinkClick {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupTypeClient", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
