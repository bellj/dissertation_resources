package X;

import android.content.Context;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.69F  reason: invalid class name */
/* loaded from: classes4.dex */
public abstract class AnonymousClass69F implements AnonymousClass5X2 {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public ArrayList A04;

    @Override // X.AnonymousClass5X2
    public abstract View buildPaymentHelpSupportSection(Context context, AbstractC28901Pl v, String str);

    public static void A00(Object obj, Object obj2, AbstractCollection abstractCollection) {
        abstractCollection.add(new Pair(obj, obj2));
    }

    public void A01(List list) {
        ArrayList arrayList = this.A04;
        if (!(arrayList == null || arrayList.isEmpty())) {
            StringBuilder A0h = C12960it.A0h();
            for (int i = 0; i < arrayList.size(); i++) {
                A0h.append((String) arrayList.get(i));
                if (i < arrayList.size() - 1) {
                    A0h.append(", ");
                }
            }
            list.add(new Pair("Topic IDs", A0h.toString()));
        }
    }

    @Override // X.AnonymousClass5X2
    public List AF3() {
        String str;
        String str2;
        boolean z = this instanceof C121165hM;
        ArrayList A0l = C12960it.A0l();
        boolean isEmpty = TextUtils.isEmpty(this.A02);
        if (!z) {
            if (!isEmpty) {
                A00("Payments fb txn id", this.A02, A0l);
            }
            if (!TextUtils.isEmpty(this.A00)) {
                str = this.A00;
                str2 = "Payments bank txn id";
                A00(str2, str, A0l);
            }
        } else if (!isEmpty) {
            str = this.A02;
            str2 = "Payments fb txn id";
            A00(str2, str, A0l);
        }
        if (!TextUtils.isEmpty(this.A01)) {
            A00("Payments return value", this.A01, A0l);
        }
        if (!TextUtils.isEmpty(this.A03)) {
            A00("Payments status", this.A03, A0l);
        }
        A01(A0l);
        return A0l;
    }

    @Override // X.AnonymousClass5X2
    public boolean AIL() {
        return !(this instanceof C121165hM);
    }

    @Override // X.AnonymousClass5X2
    public void AcT(String str, String str2, String str3, String str4, ArrayList arrayList) {
        this.A02 = str;
        this.A00 = str2;
        this.A01 = str3;
        this.A03 = str4;
        this.A04 = arrayList;
    }
}
