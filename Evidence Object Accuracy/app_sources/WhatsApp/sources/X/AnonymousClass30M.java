package X;

/* renamed from: X.30M  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30M extends AbstractC16110oT {
    public Integer A00;
    public Integer A01;
    public Long A02;

    public AnonymousClass30M() {
        super(2068, new AnonymousClass00E(1, 20, 20), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A00);
        r3.Abe(1, this.A02);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCameraTtSwitch {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraApi", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraTtSwitchDuration", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "cameraType", C12960it.A0Y(this.A01));
        return C12960it.A0d("}", A0k);
    }
}
