package X;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import com.whatsapp.R;

/* renamed from: X.35H  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass35H extends AbstractC75753kM {
    public AbstractC16350or A00;
    public boolean A01 = false;
    public final C16590pI A02;
    public final AnonymousClass2FO A03;
    public final C457522x A04;
    public final AnonymousClass2d1 A05;
    public final AbstractC14440lR A06;

    public AnonymousClass35H(C16590pI r2, AnonymousClass2FO r3, C457522x r4, AnonymousClass4L9 r5, AnonymousClass2d1 r6, AbstractC14440lR r7) {
        super(r6, r5);
        this.A05 = r6;
        this.A04 = r4;
        this.A02 = r2;
        this.A06 = r7;
        this.A03 = r3;
    }

    @Override // X.AbstractC75753kM
    public AbstractC16350or A08() {
        return this.A00;
    }

    @Override // X.AbstractC75753kM
    public void A09(Integer num) {
        Context context = this.A0H.getContext();
        String string = context.getString(R.string.wallpaper_categories_my_photos);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setSize(1, 1);
        gradientDrawable.setColor(AnonymousClass00T.A00(context, R.color.wallpaper_category_my_photos_background));
        this.A05.A00(gradientDrawable, AnonymousClass2GE.A01(context, R.drawable.ic_attachment_gallery, R.color.wallpaper_category_my_photos_placeholder_tint), string);
        if (!this.A01 && this.A00 == null) {
            AnonymousClass36z r2 = new AnonymousClass36z(this.A03, new AnonymousClass4OH(this, string));
            this.A00 = r2;
            this.A06.Aaz(r2, new Object[0]);
        }
    }
}
