package X;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.StateSet;

/* renamed from: X.0CD  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0CD extends AnonymousClass09z {
    public int[][] A00;

    public AnonymousClass0CD(Resources resources, AnonymousClass0CD r3, AnonymousClass0CA r4) {
        super(resources, r3, r4);
        int[][] iArr;
        if (r3 != null) {
            iArr = r3.A00;
        } else {
            iArr = new int[this.A0X.length];
        }
        this.A00 = iArr;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int[][] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // X.AnonymousClass09z
    public void A04() {
        int length = this.A00.length;
        int[][] iArr = new int[length];
        for (int i = length - 1; i >= 0; i--) {
            int[][] iArr2 = this.A00;
            iArr[i] = iArr2[i] != null ? iArr2[i].clone() : null;
        }
        this.A00 = iArr;
    }

    @Override // X.AnonymousClass09z
    public void A05(int i, int i2) {
        super.A05(i, i2);
        int[][] iArr = new int[i2];
        System.arraycopy(this.A00, 0, iArr, 0, i);
        this.A00 = iArr;
    }

    public int A08(int[] iArr) {
        int[][] iArr2 = this.A00;
        int i = this.A0A;
        for (int i2 = 0; i2 < i; i2++) {
            if (StateSet.stateSetMatches(iArr2[i2], iArr)) {
                return i2;
            }
        }
        return -1;
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable() {
        return new AnonymousClass0CA(null, this);
    }

    @Override // android.graphics.drawable.Drawable.ConstantState
    public Drawable newDrawable(Resources resources) {
        return new AnonymousClass0CA(resources, this);
    }
}
