package X;

import java.util.Iterator;

/* renamed from: X.1I5  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1I5 implements Iterator {
    @Override // java.util.Iterator
    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
