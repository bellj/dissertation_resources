package X;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* renamed from: X.02d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public interface AbstractC004702d {
    void setSupportCompoundDrawablesTintList(ColorStateList colorStateList);

    void setSupportCompoundDrawablesTintMode(PorterDuff.Mode mode);
}
