package X;

import android.app.Application;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.1ku  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36941ku extends AnonymousClass014 {
    public String A00 = null;
    public boolean A01;
    public final AnonymousClass02P A02 = new AnonymousClass02P();
    public final AnonymousClass02P A03 = new AnonymousClass02P();
    public final AnonymousClass016 A04 = new AnonymousClass016();
    public final AnonymousClass016 A05 = new AnonymousClass016();
    public final AnonymousClass016 A06 = new AnonymousClass016();
    public final AnonymousClass016 A07 = new AnonymousClass016();
    public final AnonymousClass016 A08 = new AnonymousClass016();
    public final C15550nR A09;
    public final C15610nY A0A;
    public final AnonymousClass018 A0B;
    public final AnonymousClass1AW A0C;
    public final C27691It A0D = new C27691It();
    public final List A0E = new ArrayList();

    public C36941ku(Application application, C15550nR r3, C15610nY r4, AnonymousClass018 r5, AnonymousClass1AW r6) {
        super(application);
        this.A09 = r3;
        this.A0B = r5;
        this.A0A = r4;
        this.A0C = r6;
    }

    public static final void A00(C15370n3 r2, Map map) {
        String str = r2.A0K;
        if (TextUtils.isEmpty(str)) {
            Log.i("InviteNonWhatsAppContactPickerViewModel/fillNameToContactMap/display name missing");
            return;
        }
        List list = (List) map.get(str);
        if (list == null) {
            list = new ArrayList();
        }
        list.add(r2);
        map.put(str, list);
    }

    public void A04(String str) {
        this.A00 = str;
        ArrayList A02 = C32751cg.A02(this.A0B, str);
        this.A08.A0B(0);
        this.A06.A0B(A02);
    }
}
