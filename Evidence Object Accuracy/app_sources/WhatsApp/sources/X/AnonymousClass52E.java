package X;

/* renamed from: X.52E  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52E implements AnonymousClass5T9 {
    public final C95264dP A00;
    public final AbstractC117035Xw A01;

    public AnonymousClass52E(C95264dP r1, AbstractC117035Xw r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5T9
    public Object get() {
        return this.A01.AYu(this.A00.A04);
    }
}
