package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import com.whatsapp.acceptinvitelink.AcceptInviteLinkActivity;

/* renamed from: X.4o2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnGlobalLayoutListenerC101754o2 implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ View A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AcceptInviteLinkActivity A02;

    public ViewTreeObserver$OnGlobalLayoutListenerC101754o2(View view, View view2, AcceptInviteLinkActivity acceptInviteLinkActivity) {
        this.A02 = acceptInviteLinkActivity;
        this.A01 = view;
        this.A00 = view2;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        View view = this.A01;
        C12980iv.A1F(view, this);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(200);
        view.startAnimation(translateAnimation);
        this.A00.startAnimation(translateAnimation);
    }
}
