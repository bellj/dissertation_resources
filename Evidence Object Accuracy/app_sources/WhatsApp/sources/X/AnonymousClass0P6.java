package X;

import java.util.HashSet;

/* renamed from: X.0P6  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0P6 {
    public final AnonymousClass02N A00;
    public final AnonymousClass0Q9 A01;

    public AnonymousClass0P6(AnonymousClass02N r1, AnonymousClass0Q9 r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public void A00() {
        AnonymousClass0Q9 r2 = this.A01;
        AnonymousClass02N r0 = this.A00;
        HashSet hashSet = r2.A06;
        if (hashSet.remove(r0) && hashSet.isEmpty()) {
            r2.A00();
        }
    }

    public boolean A01() {
        AnonymousClass0Q9 r1 = this.A01;
        EnumC03930Jr A01 = EnumC03930Jr.A01(r1.A04.A0A);
        EnumC03930Jr r12 = r1.A01;
        if (A01 == r12) {
            return true;
        }
        EnumC03930Jr r0 = EnumC03930Jr.VISIBLE;
        return (A01 == r0 || r12 == r0) ? false : true;
    }
}
