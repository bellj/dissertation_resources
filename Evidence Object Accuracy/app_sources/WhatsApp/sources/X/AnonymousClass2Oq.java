package X;

import com.whatsapp.util.Log;

/* renamed from: X.2Oq  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2Oq implements AbstractC42321v1 {
    public final /* synthetic */ C253318z A00;

    public /* synthetic */ AnonymousClass2Oq(C253318z r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC42321v1
    public void AI5(AnonymousClass1JA r6, String str, int i, int i2, long j) {
        if (j > 0) {
            C253318z r3 = this.A00;
            long A00 = r3.A07.A00() + j;
            C20840wP r4 = r3.A05;
            r4.A03(A00);
            if (i2 == 503 && r3.A09.A07(1297)) {
                Log.e("ContactQuerySync/handleSyncContactError need global backoff");
                r4.A01().edit().putLong("global_backoff_time", A00).apply();
            }
        }
    }

    @Override // X.AbstractC42321v1
    public void AI6(C42061ub r4, String str, int i) {
        StringBuilder sb = new StringBuilder("ContactQuerySync/result sid=");
        sb.append(str);
        sb.append(" index=");
        sb.append(0);
        Log.i(sb.toString());
        this.A00.A0D.put(str, r4);
    }

    @Override // X.AbstractC42321v1
    public void AI7(String str, int i, int i2, long j) {
        throw new UnsupportedOperationException();
    }
}
