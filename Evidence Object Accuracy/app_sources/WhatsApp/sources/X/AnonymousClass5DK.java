package X;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

/* renamed from: X.5DK  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DK implements Iterator {
    public final Iterator delegateIterator;
    public final Collection originalDelegate;
    public final /* synthetic */ C113515Hw this$1;

    public AnonymousClass5DK(C113515Hw r2) {
        this.this$1 = r2;
        Collection collection = r2.delegate;
        this.originalDelegate = collection;
        this.delegateIterator = AbstractC80933tC.iteratorOrListIterator(collection);
    }

    public AnonymousClass5DK(C113515Hw r2, Iterator it) {
        this.this$1 = r2;
        this.originalDelegate = r2.delegate;
        this.delegateIterator = it;
    }

    public Iterator getDelegateIterator() {
        validateIterator();
        return this.delegateIterator;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        validateIterator();
        return this.delegateIterator.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        validateIterator();
        return this.delegateIterator.next();
    }

    @Override // java.util.Iterator
    public void remove() {
        this.delegateIterator.remove();
        C113515Hw r1 = this.this$1;
        AbstractC80933tC.access$210(r1.this$0);
        r1.removeIfEmpty();
    }

    public void validateIterator() {
        this.this$1.refreshIfEmpty();
        if (this.this$1.delegate != this.originalDelegate) {
            throw new ConcurrentModificationException();
        }
    }
}
