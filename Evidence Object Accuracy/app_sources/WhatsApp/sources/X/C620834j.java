package X;

import android.content.Context;
import android.util.Pair;
import android.util.SparseIntArray;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape1S0101000_I1;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.whatsapp.R;
import com.whatsapp.search.SearchViewModel;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: X.34j  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C620834j extends AbstractC621034l {
    public boolean A00;

    public C620834j(Context context) {
        super(context);
        A01();
        setLayoutParams(new ViewGroup.MarginLayoutParams(-1, -2));
    }

    @Override // X.AbstractC74153hP
    public void A01() {
        if (!this.A00) {
            this.A00 = true;
            ((AbstractC621034l) this).A00 = C12960it.A0R(AnonymousClass2P6.A00(generatedComponent()));
        }
    }

    public void A02(SparseIntArray sparseIntArray, SearchViewModel searchViewModel) {
        ChipGroup chipGroup = this.A01;
        chipGroup.removeAllViews();
        ArrayList A0l = C12960it.A0l();
        boolean z = false;
        for (int i = 0; i < sparseIntArray.size(); i++) {
            if (sparseIntArray.keyAt(i) != 0) {
                A0l.add(C12960it.A0D(Integer.valueOf(sparseIntArray.keyAt(i)), sparseIntArray.valueAt(i)));
            }
        }
        C12980iv.A1S(A0l, 22);
        Iterator it = A0l.iterator();
        while (it.hasNext()) {
            int A05 = C12960it.A05(((Pair) it.next()).first);
            AnonymousClass4TG r5 = (AnonymousClass4TG) AnonymousClass3GL.A00().get(A05);
            if (r5 != null) {
                Chip chip = new Chip(getContext(), null);
                chip.setText(r5.A05);
                chip.setClickable(true);
                chip.setOnClickListener(new ViewOnClickCListenerShape1S0101000_I1(searchViewModel, A05, 5));
                AnonymousClass3GL.A01(getContext(), chip, A05, R.color.search_token_text);
                chip.setChipIconTintResource(R.color.search_token_icon);
                C12960it.A0s(getContext(), chip, R.color.search_token_text);
                chip.setChipBackgroundColorResource(R.color.searchChipBackground);
                chip.setId(r5.A04);
                chipGroup.addView(chip);
            }
        }
        if (sparseIntArray.get(0) == 1) {
            z = true;
        }
        setBackground(z);
    }

    private void setBackground(boolean z) {
        Context context = getContext();
        int i = R.color.primary_surface;
        if (z) {
            i = R.color.neutral_primary;
        }
        C12970iu.A18(context, this, i);
    }
}
