package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.3YK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YK implements AbstractC470728v {
    public final int A00;

    @Override // X.AbstractC470728v
    public boolean A6v() {
        return false;
    }

    public AnonymousClass3YK(int i) {
        this.A00 = i;
    }

    @Override // X.AbstractC470728v
    public AbstractC454821u A8X(Context context, AnonymousClass018 r4, boolean z) {
        switch (this.A00) {
            case 0:
                return new AnonymousClass33E();
            case 1:
                return new AnonymousClass33F();
            case 2:
                return new AnonymousClass33G();
            case 3:
                return new AnonymousClass33K();
            case 4:
                return new AnonymousClass33H();
            case 5:
                return new AnonymousClass33I();
            case 6:
                return new AnonymousClass33C(context, r4, z);
            case 7:
                return new AnonymousClass33M(context, r4, z);
            case 8:
                return new AnonymousClass33N(context, r4, context.getString(R.string.attach_location), z);
            default:
                return null;
        }
    }

    @Override // X.AbstractC470728v
    public C37471mS[] ACh() {
        C37471mS[] r2;
        int[] iArr;
        int i;
        switch (this.A00) {
            case 0:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 8599;
                break;
            case 1:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 11093;
                break;
            case 2:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 128306;
                break;
            case 3:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 128173;
                break;
            case 4:
            case 5:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 128172;
                break;
            case 6:
            case 7:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 128346;
                break;
            case 8:
                r2 = new C37471mS[1];
                iArr = new int[1];
                i = 128205;
                break;
            default:
                return AbstractC470728v.A00;
        }
        iArr[0] = i;
        r2[0] = new C37471mS(iArr);
        return r2;
    }

    @Override // X.AbstractC470728v
    public String AH5() {
        return C12960it.A0f(C12960it.A0k("custom:"), this.A00);
    }

    @Override // X.AbstractC470728v
    public boolean Aac() {
        int i = this.A00;
        return i == 7 || i == 8;
    }
}
