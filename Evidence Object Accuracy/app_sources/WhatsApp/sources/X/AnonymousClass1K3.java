package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1K3  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1K3 extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass1K3 A07;
    public static volatile AnonymousClass255 A08;
    public int A00;
    public long A01;
    public AbstractC27881Jp A02;
    public AbstractC27881Jp A03;
    public AbstractC27881Jp A04;
    public String A05 = "";
    public String A06 = "";

    static {
        AnonymousClass1K3 r0 = new AnonymousClass1K3();
        A07 = r0;
        r0.A0W();
    }

    public AnonymousClass1K3() {
        AbstractC27881Jp r1 = AbstractC27881Jp.A01;
        this.A04 = r1;
        this.A03 = r1;
        this.A02 = r1;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        int i3 = this.A00;
        if ((i3 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A09(this.A04, 1);
        }
        if ((i3 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A05);
        }
        if ((this.A00 & 4) == 4) {
            i2 += CodedOutputStream.A07(3, this.A06);
        }
        int i4 = this.A00;
        if ((i4 & 8) == 8) {
            i2 += CodedOutputStream.A06(4, this.A01);
        }
        if ((i4 & 16) == 16) {
            i2 += CodedOutputStream.A09(this.A03, 5);
        }
        if ((i4 & 32) == 32) {
            i2 += CodedOutputStream.A09(this.A02, 6);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0K(this.A04, 1);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A05);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0I(3, this.A06);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0H(4, this.A01);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0K(this.A03, 5);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0K(this.A02, 6);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
