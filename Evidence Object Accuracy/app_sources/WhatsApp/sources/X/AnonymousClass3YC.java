package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

/* renamed from: X.3YC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3YC implements AbstractC116725Wo {
    public long A00 = 0;
    public final C14950mJ A01;
    public final C14850m9 A02;
    public final File A03;

    public AnonymousClass3YC(C14950mJ r3, C14850m9 r4, File file) {
        this.A02 = r4;
        this.A01 = r3;
        this.A03 = file;
    }

    @Override // X.AbstractC116725Wo
    public long AEj() {
        return this.A00;
    }

    @Override // X.AbstractC116725Wo
    public OutputStream AYn(AbstractC37631mk r6) {
        if (r6.getContentLength() <= this.A01.A01()) {
            try {
                if (this.A02.A07(1539)) {
                    AfU();
                    return new FileOutputStream(this.A03, true);
                }
                File file = this.A03;
                C14350lI.A0M(file);
                return new FileOutputStream(file, false);
            } catch (FileNotFoundException e) {
                Log.e("plainfiledownload/FileNotFoundException", e);
                throw new AnonymousClass4C5(9);
            }
        } else {
            Log.w(C12970iu.A0s(this.A03, C12960it.A0k("plainfiledownload/not enough space to store the file: ")));
            throw new AnonymousClass4C5(4);
        }
    }

    @Override // X.AbstractC116725Wo
    public void AfU() {
        long j;
        if (this.A02.A07(1539)) {
            j = this.A03.length();
        } else {
            j = 0;
        }
        this.A00 = j;
    }
}
