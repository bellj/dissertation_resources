package X;

import com.whatsapp.payments.pin.ui.PinBottomSheetDialogFragment;
import com.whatsapp.util.Log;

/* renamed from: X.5xC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129115xC {
    public final /* synthetic */ int A00;
    public final /* synthetic */ ActivityC13790kL A01;
    public final /* synthetic */ PinBottomSheetDialogFragment A02;
    public final /* synthetic */ C123595nP A03;
    public final /* synthetic */ String A04;

    public C129115xC(ActivityC13790kL r1, PinBottomSheetDialogFragment pinBottomSheetDialogFragment, C123595nP r3, String str, int i) {
        this.A03 = r3;
        this.A00 = i;
        this.A02 = pinBottomSheetDialogFragment;
        this.A01 = r1;
        this.A04 = str;
    }

    public void A00(C452120p r4) {
        PinBottomSheetDialogFragment pinBottomSheetDialogFragment;
        Log.i("DyiViewModel/request-report/on-error");
        int i = r4.A00;
        if (i == 1440 || i == 444 || i == 478 || i == 1441 || i == 445 || i == 1448 || i == 10718) {
            C123595nP r2 = this.A03;
            String str = this.A04;
            AnonymousClass009.A05(str);
            r2.A06(r4, this.A02, str);
            return;
        }
        if (this.A00 == 1 && (pinBottomSheetDialogFragment = this.A02) != null) {
            pinBottomSheetDialogFragment.A1M();
            pinBottomSheetDialogFragment.A1B();
        }
        C123595nP r22 = this.A03;
        AnonymousClass016 r0 = r22.A02;
        Integer A0i = C12980iv.A0i();
        r0.A0A(A0i);
        C117305Zk.A1B(r22.A03, A0i, r4);
    }
}
