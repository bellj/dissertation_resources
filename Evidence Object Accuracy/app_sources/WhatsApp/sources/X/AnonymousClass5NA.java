package X;

import java.util.Arrays;

/* renamed from: X.5NA  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NA extends AnonymousClass1TL {
    public byte[] A00;

    @Override // X.AnonymousClass1TL
    public int A05() {
        int length = this.A00.length;
        return AnonymousClass1TQ.A00(length) + 1 + length;
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A00, 23, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    public String toString() {
        return AnonymousClass1T7.A02(this.A00);
    }

    public AnonymousClass5NA(byte[] bArr) {
        byte b;
        byte b2;
        int length = bArr.length;
        if (length >= 2) {
            this.A00 = bArr;
            if (length <= 0 || (b = bArr[0]) < 48 || b > 57 || length <= 1 || (b2 = bArr[1]) < 48 || b2 > 57) {
                throw C12970iu.A0f("illegal characters in UTCTime string");
            }
            return;
        }
        throw C12970iu.A0f("UTCTime string too short");
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NA)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NA) r3).A00);
    }

    public String A0B() {
        StringBuilder A0h;
        String substring;
        String A02 = AnonymousClass1T7.A02(this.A00);
        int indexOf = A02.indexOf(45);
        if (indexOf >= 0 || (indexOf = A02.indexOf(43)) >= 0) {
            if (indexOf == A02.length() - 3) {
                A02 = C12960it.A0d("00", C12960it.A0j(A02));
            }
            A0h = C12960it.A0h();
            if (indexOf == 10) {
                C72453ed.A1O(A02, A0h, 0, 10);
                A0h.append("00GMT");
                C72453ed.A1O(A02, A0h, 10, 13);
                A0h.append(":");
                substring = A02.substring(13, 15);
            } else {
                C72453ed.A1O(A02, A0h, 0, 12);
                A0h.append("GMT");
                C72453ed.A1O(A02, A0h, 12, 15);
                A0h.append(":");
                substring = A02.substring(15, 17);
            }
        } else {
            int length = A02.length();
            A0h = C12960it.A0h();
            if (length == 11) {
                C72453ed.A1O(A02, A0h, 0, 10);
                substring = "00GMT+00:00";
            } else {
                C72453ed.A1O(A02, A0h, 0, 12);
                substring = "GMT+00:00";
            }
        }
        return C12960it.A0d(substring, A0h);
    }
}
