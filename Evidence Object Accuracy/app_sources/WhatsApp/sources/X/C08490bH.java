package X;

/* renamed from: X.0bH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08490bH implements AbstractC12120hP {
    public int A00;
    public int A01;
    public String A02;
    public boolean A03;
    public boolean A04;

    public C08490bH(boolean z, boolean z2, int i, int i2, String str) {
        this.A00 = i;
        this.A01 = i2;
        this.A03 = z;
        this.A04 = z2;
        this.A02 = str;
    }

    @Override // X.AbstractC12120hP
    public boolean ALK(AnonymousClass0K2 r8, AnonymousClass0I1 r9) {
        String str;
        int i;
        int i2;
        int i3;
        if (!this.A04 || this.A02 != null) {
            str = this.A02;
        } else {
            str = r9.A00();
        }
        AbstractC12490i0 r0 = ((AnonymousClass0OO) r9).A00;
        if (r0 != null) {
            i = 0;
            i2 = 0;
            for (AnonymousClass0OO r02 : r0.ABO()) {
                if (r02 == r9) {
                    i = i2;
                }
                if (str == null || r02.A00().equals(str)) {
                    i2++;
                }
            }
        } else {
            i = 0;
            i2 = 1;
        }
        if (this.A03) {
            i3 = i + 1;
        } else {
            i3 = i2 - i;
        }
        int i4 = this.A00;
        int i5 = this.A01;
        if (i4 != 0) {
            int i6 = i3 - i5;
            if (i6 % i4 != 0) {
                return false;
            }
            int signum = Integer.signum(i6);
            if (!(signum == 0 || signum == Integer.signum(i4))) {
                return false;
            }
        } else if (i3 != i5) {
            return false;
        }
        return true;
    }

    public String toString() {
        String str;
        Object[] objArr;
        String str2;
        if (this.A03) {
            str = "";
        } else {
            str = "last-";
        }
        if (this.A04) {
            objArr = new Object[]{str, Integer.valueOf(this.A00), Integer.valueOf(this.A01), this.A02};
            str2 = "nth-%schild(%dn%+d of type <%s>)";
        } else {
            objArr = new Object[]{str, Integer.valueOf(this.A00), Integer.valueOf(this.A01)};
            str2 = "nth-%schild(%dn%+d)";
        }
        return String.format(str2, objArr);
    }
}
