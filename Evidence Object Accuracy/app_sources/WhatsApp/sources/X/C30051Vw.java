package X;

/* renamed from: X.1Vw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C30051Vw {
    public static boolean A17(AbstractC15340mz r3) {
        int i;
        if (r3 == null) {
            return true;
        }
        if (!A18(r3) || ((i = r3.A07) != 0 && (i & 1) == 1 && r3.A0V == null)) {
            return false;
        }
        AbstractC15340mz A0E = r3.A0E();
        if (A0E == null || A18(A0E)) {
            return true;
        }
        return false;
    }

    public static boolean A18(AbstractC15340mz r2) {
        C38101nW A14;
        if ((r2.A0G() == null || r2.A0G().A05()) && (!(r2 instanceof AbstractC16130oV) || (A14 = ((AbstractC16130oV) r2).A14()) == null || A14.A04())) {
            return true;
        }
        return false;
    }

    public static boolean A19(AbstractC15340mz r6) {
        if (1531267200000L <= r6.A0I && r6.A12(1)) {
            if (!C35011h5.A04(r6)) {
                byte b = r6.A0y;
                if (b != 0) {
                    if (!(b == 1 || b == 2 || b == 3 || b == 4 || b == 5 || b == 9 || b == 23 || b == 32 || b == 37 || b == 52 || b == 55 || b == 57 || b == 13 || b == 14 || b == 62 || b == 63)) {
                        switch (b) {
                        }
                    }
                } else if (!(r6 instanceof AnonymousClass1XB) && r6.A0L == null) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }
}
