package X;

import android.util.Pair;

/* renamed from: X.1qj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39901qj {
    public final Pair A00;
    public final Pair A01;
    public final byte[] A02;
    public final byte[] A03;

    public C39901qj(Pair pair, Pair pair2, byte[] bArr, byte[] bArr2) {
        this.A02 = bArr;
        this.A01 = pair;
        this.A00 = pair2;
        this.A03 = bArr2;
    }
}
