package X;

import android.content.Context;
import android.view.View;

/* renamed from: X.2LH  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2LH extends View {
    public final /* synthetic */ int A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2LH(Context context, int i) {
        super(context);
        this.A00 = i;
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, View.MeasureSpec.makeMeasureSpec(this.A00, 1073741824));
    }
}
