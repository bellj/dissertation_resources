package X;

import android.net.Uri;
import android.view.View;
import com.whatsapp.shops.ShopsProductPreviewFragment;
import com.whatsapp.shops.ShopsProductPreviewFragmentViewModel;

/* renamed from: X.5oD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123955oD extends AbstractView$OnClickListenerC34281fs {
    public final /* synthetic */ ShopsProductPreviewFragment A00;

    public C123955oD(ShopsProductPreviewFragment shopsProductPreviewFragment) {
        this.A00 = shopsProductPreviewFragment;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        ShopsProductPreviewFragment shopsProductPreviewFragment = this.A00;
        shopsProductPreviewFragment.A03.Ab9(shopsProductPreviewFragment.A0C(), Uri.parse(shopsProductPreviewFragment.A09));
        ShopsProductPreviewFragmentViewModel shopsProductPreviewFragmentViewModel = shopsProductPreviewFragment.A06;
        if (shopsProductPreviewFragmentViewModel.A01.AKA()) {
            AnonymousClass30V r1 = new AnonymousClass30V();
            r1.A01 = C12970iu.A0h();
            r1.A00 = C12960it.A0V();
            shopsProductPreviewFragmentViewModel.A00.A07(r1);
        }
    }
}
