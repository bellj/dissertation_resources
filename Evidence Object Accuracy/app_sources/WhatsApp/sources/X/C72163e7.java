package X;

import com.whatsapp.catalogcategory.view.viewmodel.CatalogCategoryGroupsViewModel;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/* renamed from: X.3e7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C72163e7 extends AnonymousClass1WI implements AnonymousClass1J7 {
    public final /* synthetic */ UserJid $bizJid;
    public final /* synthetic */ CatalogCategoryGroupsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C72163e7(CatalogCategoryGroupsViewModel catalogCategoryGroupsViewModel, UserJid userJid) {
        super(1);
        this.this$0 = catalogCategoryGroupsViewModel;
        this.$bizJid = userJid;
    }

    @Override // X.AnonymousClass1J7
    public /* bridge */ /* synthetic */ Object AJ4(Object obj) {
        AnonymousClass4K7 r0;
        AnonymousClass4K6 r8 = (AnonymousClass4K6) obj;
        C16700pc.A0E(r8, 0);
        if (r8 instanceof C60252wM) {
            List<AnonymousClass4SX> list = ((C60252wM) r8).A01;
            UserJid userJid = this.$bizJid;
            ArrayList A0E = C16760pi.A0E(list);
            for (AnonymousClass4SX r1 : list) {
                if (r1.A04) {
                    r0 = new AnonymousClass2wR(r1, userJid);
                } else {
                    r0 = new AnonymousClass2wS(r1, userJid);
                }
                A0E.add(r0);
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (AnonymousClass4SX r12 : list) {
                if (!r12.A04) {
                    String str = r12.A01;
                    C16700pc.A0B(str);
                    ArrayList A0l = C12960it.A0l();
                    int i = 0;
                    do {
                        i++;
                        A0l.add(new C849640p());
                    } while (i < 3);
                    linkedHashMap.put(str, A0l);
                }
            }
            C12990iw.A0P(this.this$0.A08).A0A(new AnonymousClass412(A0E, linkedHashMap));
        }
        return AnonymousClass1WZ.A00;
    }
}
