package X;

import com.whatsapp.util.Log;

/* renamed from: X.20s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C452420s {
    public final C14830m7 A00;
    public final C16590pI A01;
    public final C14820m6 A02;
    public final C17220qS A03;
    public final AbstractC14440lR A04;

    public C452420s(C14830m7 r1, C16590pI r2, C14820m6 r3, C17220qS r4, AbstractC14440lR r5) {
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public void A00() {
        Log.i("BackupTokenProtocolHelper/sendBackupTokenRequest");
        String A0C = this.A02.A0C();
        if (!AnonymousClass1US.A0C(A0C)) {
            byte[] A0C2 = C003501n.A0C();
            C17220qS r7 = this.A03;
            String A01 = r7.A01();
            r7.A0A(new AnonymousClass3ZG(this, A0C, A0C2), new AnonymousClass1V8(new AnonymousClass1V8("token", A0C2, (AnonymousClass1W9[]) null), "iq", new AnonymousClass1W9[]{new AnonymousClass1W9("to", "s.whatsapp.net"), new AnonymousClass1W9("xmlns", "w:auth:backup:token"), new AnonymousClass1W9("type", "set"), new AnonymousClass1W9("id", A01)}), A01, 226, 32000);
        }
    }
}
