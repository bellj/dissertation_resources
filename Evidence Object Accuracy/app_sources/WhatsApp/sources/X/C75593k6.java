package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageButton;

/* renamed from: X.3k6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75593k6 extends AnonymousClass03U {
    public final View A00;
    public final ImageView A01;
    public final TextView A02;
    public final WaImageButton A03;
    public final /* synthetic */ C37251lt A04;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C75593k6(View view, C37251lt r3) {
        super(view);
        this.A04 = r3;
        this.A01 = C12970iu.A0L(view, R.id.audio_call_participant_photo);
        this.A00 = view.findViewById(R.id.audio_call_status_layout);
        this.A02 = C12960it.A0J(view, R.id.audio_call_status);
        this.A03 = (WaImageButton) view.findViewById(R.id.audio_call_status_button);
    }
}
