package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.0jr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13520jr {
    public static int A06;
    public static PendingIntent A07;
    public Messenger A00;
    public Messenger A01;
    public C13560jv A02;
    public final Context A03;
    public final AnonymousClass00O A04 = new AnonymousClass00O();
    public final C13430ji A05;

    public C13520jr(Context context, C13430ji r4) {
        this.A03 = context;
        this.A05 = r4;
        this.A00 = new Messenger(new HandlerC13530js(Looper.getMainLooper(), this));
    }

    public static /* synthetic */ void A00(Message message, C13520jr r10) {
        String str;
        Bundle bundle;
        String str2;
        String str3;
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new C13550ju());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof C13560jv) {
                        r10.A02 = (C13560jv) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        r10.A01 = (Messenger) parcelableExtra;
                    }
                }
                Intent intent2 = (Intent) message.obj;
                String action = intent2.getAction();
                if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
                    String stringExtra = intent2.getStringExtra("registration_id");
                    if (stringExtra == null) {
                        stringExtra = intent2.getStringExtra("unregistered");
                    }
                    if (stringExtra == null) {
                        String stringExtra2 = intent2.getStringExtra("error");
                        if (stringExtra2 == null) {
                            String valueOf = String.valueOf(intent2.getExtras());
                            StringBuilder sb = new StringBuilder(valueOf.length() + 49);
                            sb.append("Unexpected response, no error or registration id ");
                            sb.append(valueOf);
                            Log.w("FirebaseInstanceId", sb.toString());
                            return;
                        }
                        if (Log.isLoggable("FirebaseInstanceId", 3)) {
                            if (stringExtra2.length() != 0) {
                                str2 = "Received InstanceID error ".concat(stringExtra2);
                            } else {
                                str2 = new String("Received InstanceID error ");
                            }
                            Log.d("FirebaseInstanceId", str2);
                        }
                        if (stringExtra2.startsWith("|")) {
                            String[] split = stringExtra2.split("\\|");
                            if (split.length <= 2 || !"ID".equals(split[1])) {
                                Log.w("FirebaseInstanceId", stringExtra2.length() != 0 ? "Unexpected structured response ".concat(stringExtra2) : new String("Unexpected structured response "));
                                return;
                            }
                            str = split[2];
                            String str4 = split[3];
                            if (str4.startsWith(":")) {
                                str4 = str4.substring(1);
                            }
                            bundle = intent2.putExtra("error", str4).getExtras();
                        } else {
                            AnonymousClass00O r3 = r10.A04;
                            synchronized (r3) {
                                for (int i = 0; i < r3.size(); i++) {
                                    r10.A03((String) r3.A02[i << 1], intent2.getExtras());
                                }
                            }
                            return;
                        }
                    } else {
                        Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
                        if (matcher.matches()) {
                            str = matcher.group(1);
                            String group = matcher.group(2);
                            bundle = intent2.getExtras();
                            bundle.putString("registration_id", group);
                        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                            Log.d("FirebaseInstanceId", stringExtra.length() != 0 ? "Unexpected response string: ".concat(stringExtra) : new String("Unexpected response string: "));
                            return;
                        } else {
                            return;
                        }
                    }
                    r10.A03(str, bundle);
                    return;
                } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf2 = String.valueOf(action);
                    if (valueOf2.length() != 0) {
                        str3 = "Unexpected response action: ".concat(valueOf2);
                    } else {
                        str3 = new String("Unexpected response action: ");
                    }
                    Log.d("FirebaseInstanceId", str3);
                    return;
                } else {
                    return;
                }
            }
        }
        Log.w("FirebaseInstanceId", "Dropping invalid message");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b1, code lost:
        if (((X.C13680k9) r4.getCause()).zza == 4) goto L_0x00b3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle A01(android.os.Bundle r6) {
        /*
            r5 = this;
            X.0ji r0 = r5.A05
            int r1 = r0.A03()
            r0 = 12000000(0xb71b00, float:1.6815582E-38)
            if (r1 < r0) goto L_0x00b3
            android.content.Context r0 = r5.A03
            X.0jw r2 = X.C13570jw.A00(r0)
            monitor-enter(r2)
            int r1 = r2.A00     // Catch: all -> 0x0075
            int r0 = r1 + 1
            r2.A00 = r0     // Catch: all -> 0x0075
            monitor-exit(r2)
            X.0jx r0 = new X.0jx
            r0.<init>(r1, r6)
            X.0jz r4 = r2.A01(r0)
            java.lang.String r0 = "Must not be called on the main application thread"
            X.C13020j0.A06(r0)     // Catch: InterruptedException | ExecutionException -> 0x0078
            java.lang.String r0 = "Task must not be null"
            X.C13020j0.A02(r4, r0)     // Catch: InterruptedException | ExecutionException -> 0x0078
            boolean r0 = r4.A09()     // Catch: InterruptedException | ExecutionException -> 0x0078
            if (r0 != 0) goto L_0x0052
            r0 = 0
            X.0k0 r3 = new X.0k0     // Catch: InterruptedException | ExecutionException -> 0x0078
            r3.<init>(r0)     // Catch: InterruptedException | ExecutionException -> 0x0078
            java.util.concurrent.Executor r2 = X.C13650k5.A01     // Catch: InterruptedException | ExecutionException -> 0x0078
            r4.A06(r3, r2)     // Catch: InterruptedException | ExecutionException -> 0x0078
            r4.A05(r3, r2)     // Catch: InterruptedException | ExecutionException -> 0x0078
            X.0k6 r1 = r4.A03     // Catch: InterruptedException | ExecutionException -> 0x0078
            X.0k7 r0 = new X.0k7     // Catch: InterruptedException | ExecutionException -> 0x0078
            r0.<init>(r3, r2)     // Catch: InterruptedException | ExecutionException -> 0x0078
            r1.A00(r0)     // Catch: InterruptedException | ExecutionException -> 0x0078
            r4.A04()     // Catch: InterruptedException | ExecutionException -> 0x0078
            java.util.concurrent.CountDownLatch r0 = r3.A00     // Catch: InterruptedException | ExecutionException -> 0x0078
            r0.await()     // Catch: InterruptedException | ExecutionException -> 0x0078
        L_0x0052:
            boolean r0 = r4.A0A()     // Catch: InterruptedException | ExecutionException -> 0x0078
            if (r0 == 0) goto L_0x005f
            java.lang.Object r0 = r4.A01()     // Catch: InterruptedException | ExecutionException -> 0x0078
            android.os.Bundle r0 = (android.os.Bundle) r0     // Catch: InterruptedException | ExecutionException -> 0x0078
            return r0
        L_0x005f:
            boolean r0 = r4.A05     // Catch: InterruptedException | ExecutionException -> 0x0078
            if (r0 == 0) goto L_0x006b
            java.lang.String r1 = "Task is already canceled"
            java.util.concurrent.CancellationException r0 = new java.util.concurrent.CancellationException     // Catch: InterruptedException | ExecutionException -> 0x0078
            r0.<init>(r1)     // Catch: InterruptedException | ExecutionException -> 0x0078
            throw r0     // Catch: InterruptedException | ExecutionException -> 0x0078
        L_0x006b:
            java.lang.Exception r1 = r4.A00()     // Catch: InterruptedException | ExecutionException -> 0x0078
            java.util.concurrent.ExecutionException r0 = new java.util.concurrent.ExecutionException     // Catch: InterruptedException | ExecutionException -> 0x0078
            r0.<init>(r1)     // Catch: InterruptedException | ExecutionException -> 0x0078
            throw r0     // Catch: InterruptedException | ExecutionException -> 0x0078
        L_0x0075:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0078:
            r4 = move-exception
            r0 = 3
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r0 = android.util.Log.isLoggable(r3, r0)
            if (r0 == 0) goto L_0x00a0
            java.lang.String r2 = java.lang.String.valueOf(r4)
            int r0 = r2.length()
            int r0 = r0 + 22
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Error making request: "
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            android.util.Log.d(r3, r0)
        L_0x00a0:
            java.lang.Throwable r0 = r4.getCause()
            boolean r0 = r0 instanceof X.C13680k9
            if (r0 == 0) goto L_0x00cd
            java.lang.Throwable r0 = r4.getCause()
            X.0k9 r0 = (X.C13680k9) r0
            int r1 = r0.zza
            r0 = 4
            if (r1 != r0) goto L_0x00cd
        L_0x00b3:
            android.os.Bundle r2 = r5.A02(r6)
            if (r2 == 0) goto L_0x00ce
            java.lang.String r1 = "google.messenger"
            boolean r0 = r2.containsKey(r1)
            if (r0 == 0) goto L_0x00ce
            android.os.Bundle r2 = r5.A02(r6)
            if (r2 == 0) goto L_0x00ce
            boolean r0 = r2.containsKey(r1)
            if (r0 == 0) goto L_0x00ce
        L_0x00cd:
            r2 = 0
        L_0x00ce:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13520jr.A01(android.os.Bundle):android.os.Bundle");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:74:? */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1, types: [X.0jz] */
    /* JADX WARN: Type inference failed for: r4v3 */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00f7 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle A02(android.os.Bundle r13) {
        /*
        // Method dump skipped, instructions count: 302
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C13520jr.A02(android.os.Bundle):android.os.Bundle");
    }

    public final void A03(String str, Bundle bundle) {
        String str2;
        AnonymousClass00O r4 = this.A04;
        synchronized (r4) {
            C13690kA r0 = (C13690kA) r4.remove(str);
            if (r0 == null) {
                String valueOf = String.valueOf(str);
                if (valueOf.length() != 0) {
                    str2 = "Missing callback for ".concat(valueOf);
                } else {
                    str2 = new String("Missing callback for ");
                }
                Log.w("FirebaseInstanceId", str2);
            } else {
                r0.A01(bundle);
            }
        }
    }
}
