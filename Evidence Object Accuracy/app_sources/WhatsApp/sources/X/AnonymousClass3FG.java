package X;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;

/* renamed from: X.3FG  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3FG {
    public float A00 = 1.0f;
    public int A01;
    public AnonymousClass5Pr A02;
    public final AudioManager A03;
    public final C97994hy A04;

    public AnonymousClass3FG(Context context, Handler handler, AnonymousClass5Pr r5) {
        this.A03 = (AudioManager) context.getApplicationContext().getSystemService("audio");
        this.A02 = r5;
        this.A04 = new C97994hy(handler, this);
        this.A01 = 0;
    }

    public final void A00() {
        if (this.A01 != 0) {
            if (AnonymousClass3JZ.A01 < 26) {
                this.A03.abandonAudioFocus(this.A04);
            }
            A02(0);
        }
    }

    public final void A01(int i) {
        AnonymousClass5Pr r0 = this.A02;
        if (r0 != null) {
            C47492Ax r2 = ((SurfaceHolder$CallbackC67643Sh) r0).A00;
            boolean AFj = r2.AFj();
            int i2 = 1;
            if (AFj && i != 1) {
                i2 = 2;
            }
            r2.A06(i, i2, AFj);
        }
    }

    public final void A02(int i) {
        if (this.A01 != i) {
            this.A01 = i;
            float f = 1.0f;
            if (i == 3) {
                f = 0.2f;
            }
            if (this.A00 != f) {
                this.A00 = f;
                AnonymousClass5Pr r0 = this.A02;
                if (r0 != null) {
                    C47492Ax r3 = ((SurfaceHolder$CallbackC67643Sh) r0).A00;
                    r3.A09(Float.valueOf(r3.A00 * r3.A0O.A00), 1, 2);
                }
            }
        }
    }
}
