package X;

import android.content.Context;
import android.os.Looper;

/* renamed from: X.3nk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77723nk extends AnonymousClass1U8 {
    public final AnonymousClass2RP A00;

    public C77723nk(AnonymousClass2RP r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass1U8
    public final Context A02() {
        return this.A00.A01;
    }

    @Override // X.AnonymousClass1U8
    public final Looper A03() {
        return this.A00.A02;
    }

    @Override // X.AnonymousClass1U8
    public final AnonymousClass1UI A05(AnonymousClass1UI r3) {
        this.A00.A02(r3, 0);
        return r3;
    }

    @Override // X.AnonymousClass1U8
    public final AnonymousClass1UI A06(AnonymousClass1UI r3) {
        this.A00.A02(r3, 1);
        return r3;
    }
}
