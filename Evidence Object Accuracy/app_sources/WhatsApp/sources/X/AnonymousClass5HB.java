package X;

import android.os.Process;

/* renamed from: X.5HB  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5HB extends Thread {
    public AnonymousClass5HB(ThreadGroup threadGroup) {
        super(threadGroup, "GmsDynamite");
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public final void run() {
        Process.setThreadPriority(19);
        synchronized (this) {
            while (true) {
                try {
                    wait();
                } catch (InterruptedException unused) {
                    return;
                }
            }
        }
    }
}
