package X;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: X.05R  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass05R extends AnonymousClass05P {
    public final Object A00 = new Object();
    public final ExecutorService A01 = Executors.newFixedThreadPool(4, new AnonymousClass05S(this));
    public volatile Handler A02;

    @Override // X.AnonymousClass05P
    public void A01(Runnable runnable) {
        this.A01.execute(runnable);
    }

    @Override // X.AnonymousClass05P
    public void A02(Runnable runnable) {
        Handler handler;
        if (this.A02 == null) {
            synchronized (this.A00) {
                if (this.A02 == null) {
                    Looper mainLooper = Looper.getMainLooper();
                    if (Build.VERSION.SDK_INT >= 28) {
                        handler = Handler.createAsync(mainLooper);
                    } else {
                        try {
                            handler = (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(mainLooper, null, Boolean.TRUE);
                        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException unused) {
                            handler = new Handler(mainLooper);
                        }
                    }
                    this.A02 = handler;
                }
            }
        }
        this.A02.post(runnable);
    }

    @Override // X.AnonymousClass05P
    public boolean A03() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
}
