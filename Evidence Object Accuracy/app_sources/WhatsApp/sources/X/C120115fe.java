package X;

import com.whatsapp.payments.ui.PaymentMethodsListPickerFragment;
import java.util.List;

/* renamed from: X.5fe  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120115fe extends AnonymousClass4UZ {
    public final /* synthetic */ PaymentMethodsListPickerFragment A00;

    public C120115fe(PaymentMethodsListPickerFragment paymentMethodsListPickerFragment) {
        this.A00 = paymentMethodsListPickerFragment;
    }

    @Override // X.AnonymousClass4UZ
    public void A00() {
        PaymentMethodsListPickerFragment paymentMethodsListPickerFragment = this.A00;
        C117315Zl.A0R(paymentMethodsListPickerFragment.A00, C117305Zk.A0C(paymentMethodsListPickerFragment.A05), new AbstractC14590lg() { // from class: X.6EK
            @Override // X.AbstractC14590lg
            public final void accept(Object obj) {
                List list = (List) obj;
                PaymentMethodsListPickerFragment paymentMethodsListPickerFragment2 = C120115fe.this.A00;
                C117615aH r1 = paymentMethodsListPickerFragment2.A07;
                AbstractC1311861p r0 = paymentMethodsListPickerFragment2.A08;
                if (r0 != null) {
                    list = r0.A9w(list);
                }
                r1.A02 = list;
                r1.notifyDataSetChanged();
            }
        });
    }
}
