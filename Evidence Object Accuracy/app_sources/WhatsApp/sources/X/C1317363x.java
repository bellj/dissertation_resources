package X;

import android.text.Editable;
import android.text.TextWatcher;
import com.whatsapp.payments.ui.NoviEditTransactionDescriptionFragment;

/* renamed from: X.63x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C1317363x implements TextWatcher {
    public final /* synthetic */ NoviEditTransactionDescriptionFragment A00;

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public C1317363x(NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment) {
        this.A00 = noviEditTransactionDescriptionFragment;
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        NoviEditTransactionDescriptionFragment noviEditTransactionDescriptionFragment = this.A00;
        noviEditTransactionDescriptionFragment.A00.setEnabled(!noviEditTransactionDescriptionFragment.A06.equals(charSequence.toString()));
    }
}
