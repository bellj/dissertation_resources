package X;

import android.animation.ValueAnimator;

/* renamed from: X.0Uo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06670Uo implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0AA A00;

    public C06670Uo(AnonymousClass0AA r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float f;
        AnonymousClass0AA r0 = this.A00;
        AnonymousClass0HG r3 = r0.A08;
        if (r3 != null) {
            AnonymousClass09T r1 = r0.A0K;
            C05540Py r02 = r1.A06;
            if (r02 == null) {
                f = 0.0f;
            } else {
                float f2 = r1.A00;
                float f3 = r02.A02;
                f = (f2 - f3) / (r02.A00 - f3);
            }
            r3.A01(f);
        }
    }
}
