package X;

import java.util.Map;

/* renamed from: X.0uY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19710uY extends AbstractC19720uZ implements AbstractC19700uX, AbstractC19580uL, AbstractC19730ua {
    public int A00;
    public final C19640uR A01;
    public final C17120qI A02;
    public final C19600uN A03;
    public final /* synthetic */ C19570uK A04;
    public final /* synthetic */ C19690uW A05;

    @Override // X.AbstractC19580uL
    public boolean A5Z(String str) {
        return this.A04.A5Z(str);
    }

    @Override // X.AbstractC19700uX
    public void A6h() {
        this.A05.A6h();
    }

    @Override // X.AbstractC19700uX
    public AnonymousClass01E AEX(String str, String str2, Map map, Map map2, int i) {
        C16700pc.A0E(str2, 3);
        return this.A05.AEX(str, str2, map, map2, i);
    }

    @Override // X.AbstractC19580uL
    public void AHw(String str, String str2) {
        this.A04.AHw(str, str2);
    }

    @Override // X.AbstractC19700uX
    public void AYb(String str, String str2, String str3, String str4, Map map, Map map2, int i) {
        C16700pc.A0E(str4, 5);
        this.A05.AYb(str, str2, str3, str4, map, map2, i);
    }

    @Override // X.AbstractC19700uX
    public void AYh(AnonymousClass4A3 r10, String str, String str2, String str3, String str4, Map map, Map map2, int i) {
        C16700pc.A0E(str4, 6);
        this.A05.AYh(r10, str, str2, str3, str4, map, map2, i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C19710uY(C19680uV r2, C19630uQ r3, C19570uK r4, C19640uR r5, C19690uW r6, C17120qI r7, C19600uN r8) {
        super(r2, r3);
        C16700pc.A0E(r2, 2);
        C16700pc.A0E(r3, 5);
        C16700pc.A0E(r7, 7);
        this.A03 = r8;
        this.A01 = r5;
        this.A02 = r7;
        this.A04 = r4;
        this.A05 = r6;
    }

    public static /* synthetic */ void A00(C19710uY r2, AnonymousClass6E8 r3) {
        C16700pc.A0E(r2, 0);
        C16700pc.A0E(r3, 1);
        r2.A01.A01(r3.A00);
    }

    public final void A04(AnonymousClass3Dy r8) {
        C19600uN r1 = this.A03;
        String str = r8.A01;
        C16700pc.A0B(str);
        String A00 = C130355zH.A00(Integer.valueOf(this.A00));
        C16700pc.A0C(A00);
        C16700pc.A0B(A00);
        r1.A01(null, Boolean.TRUE, str, A00, C71373cp.A00);
    }

    @Override // X.AbstractC19730ua
    public void AZR(AnonymousClass3D5 r4, Map map, int i) {
        C16700pc.A0E(map, 0);
        C19600uN r2 = this.A03;
        Object obj = map.get("app_id");
        if (obj != null) {
            r2.A02(r4, (String) obj, C71373cp.A00, i);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
    }
}
