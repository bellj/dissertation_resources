package X;

import android.os.CountDownTimer;
import com.whatsapp.R;
import com.whatsapp.authentication.FingerprintBottomSheet;
import com.whatsapp.authentication.FingerprintView;

/* renamed from: X.2Zw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51972Zw extends CountDownTimer {
    public final /* synthetic */ int A00 = R.string.payment_pin_timeout;
    public final /* synthetic */ long A01;
    public final /* synthetic */ FingerprintBottomSheet A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51972Zw(FingerprintBottomSheet fingerprintBottomSheet, long j, long j2) {
        super(j, 1000);
        this.A02 = fingerprintBottomSheet;
        this.A01 = j2;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        FingerprintBottomSheet fingerprintBottomSheet = this.A02;
        fingerprintBottomSheet.A01 = null;
        if (this.A01 <= fingerprintBottomSheet.A08.A00() && !fingerprintBottomSheet.A0A) {
            FingerprintView fingerprintView = fingerprintBottomSheet.A06;
            if (fingerprintView != null) {
                fingerprintView.A02(fingerprintView.A06);
            }
            fingerprintBottomSheet.A1J();
        }
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        FingerprintBottomSheet fingerprintBottomSheet = this.A02;
        FingerprintView fingerprintView = fingerprintBottomSheet.A06;
        if (fingerprintView != null) {
            int i = this.A00;
            fingerprintView.A03(C12970iu.A0q(fingerprintBottomSheet, C38131nZ.A04(fingerprintBottomSheet.A09, C12980iv.A0D(j)), C12970iu.A1b(), 0, i));
        }
    }
}
