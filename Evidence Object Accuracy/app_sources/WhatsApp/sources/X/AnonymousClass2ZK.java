package X;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;

/* renamed from: X.2ZK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2ZK extends ContentObserver {
    public final /* synthetic */ C616231b A00;

    @Override // android.database.ContentObserver
    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass2ZK(Handler handler, C616231b r2) {
        super(handler);
        this.A00 = r2;
    }

    @Override // android.database.ContentObserver
    public void onChange(boolean z) {
        int count;
        StringBuilder A0k = C12960it.A0k("documentsgalleryfragment/onchange ");
        A0k.append(z);
        C12960it.A1F(A0k);
        C616231b r1 = this.A00;
        Cursor cursor = ((AbstractC54432gi) r1).A01;
        if (cursor == null) {
            count = 0;
        } else {
            count = cursor.getCount();
        }
        r1.A00 = count;
        r1.A02();
    }
}
