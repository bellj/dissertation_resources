package X;

import com.whatsapp.contact.picker.ContactPickerFragment;
import java.lang.ref.WeakReference;
import java.util.HashSet;

/* renamed from: X.38l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C627538l extends AbstractC16350or {
    public final C18330sH A00;
    public final C238013b A01;
    public final C15550nR A02;
    public final C19780uf A03;
    public final C15600nX A04;
    public final C14850m9 A05;
    public final AbstractC14640lm A06;
    public final C17070qD A07;
    public final WeakReference A08;
    public final HashSet A09;
    public final boolean A0A;
    public final boolean A0B;
    public final boolean A0C;
    public final boolean A0D;
    public final boolean A0E;
    public final boolean A0F;
    public final boolean A0G;
    public final boolean A0H;
    public final boolean A0I;
    public final boolean A0J;
    public final boolean A0K;
    public final boolean A0L;

    public C627538l(C18330sH r2, C238013b r3, C15550nR r4, ContactPickerFragment contactPickerFragment, C19780uf r6, C15600nX r7, C14850m9 r8, AbstractC14640lm r9, C17070qD r10, HashSet hashSet, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12) {
        this.A08 = C12970iu.A10(contactPickerFragment);
        this.A09 = hashSet;
        this.A06 = r9;
        this.A0E = z;
        this.A0B = z2;
        this.A0L = z3;
        this.A0D = z4;
        this.A0G = z5;
        this.A0K = z6;
        this.A0C = z7;
        this.A0F = z8;
        this.A0H = z9;
        this.A0J = z10;
        this.A0I = z11;
        this.A0A = z12;
        this.A05 = r8;
        this.A02 = r4;
        this.A07 = r10;
        this.A01 = r3;
        this.A00 = r2;
        this.A03 = r6;
        this.A04 = r7;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006d, code lost:
        if (r10.contains(43) != false) goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0086, code lost:
        if (r10.contains(42) != false) goto L_0x0088;
     */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r18) {
        /*
        // Method dump skipped, instructions count: 455
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C627538l.A05(java.lang.Object[]):java.lang.Object");
    }
}
