package X;

/* renamed from: X.3iD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74523iD extends AnonymousClass015 {
    public final AbstractC14640lm A00;
    public final AnonymousClass13Q A01;
    public final AnonymousClass13P A02;
    public final C16030oK A03;
    public final C27691It A04 = new C27691It();

    public C74523iD(AbstractC14640lm r3, C16030oK r4) {
        AnonymousClass57W r1 = new AnonymousClass57W(this);
        this.A02 = r1;
        AnonymousClass57V r0 = new AnonymousClass57V(this);
        this.A01 = r0;
        this.A03 = r4;
        this.A00 = r3;
        r4.A0X(r1);
        r4.A0W(r0);
    }

    @Override // X.AnonymousClass015
    public void A03() {
        C16030oK r2 = this.A03;
        r2.A0c.remove(this.A02);
        r2.A0b.remove(this.A01);
    }
}
