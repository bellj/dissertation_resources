package X;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.08Z  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08Z extends AbstractC005102i {
    public Window.Callback A00;
    public AbstractC017508e A01;
    public ArrayList A02 = new ArrayList();
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public final AbstractC017308c A06;
    public final Runnable A07 = new RunnableC017108a(this);

    @Override // X.AbstractC005102i
    public void A0L(boolean z) {
    }

    @Override // X.AbstractC005102i
    public void A0Q(boolean z) {
    }

    public AnonymousClass08Z(Window.Callback callback, Toolbar toolbar, CharSequence charSequence) {
        C017208b r2 = new C017208b(this);
        this.A06 = r2;
        C017408d r1 = new C017408d(toolbar, false);
        this.A01 = r1;
        C018008k r0 = new C018008k(callback, this);
        this.A00 = r0;
        r1.A07 = r0;
        toolbar.A0R = r2;
        r1.setWindowTitle(charSequence);
    }

    @Override // X.AbstractC005102i
    public float A00() {
        return AnonymousClass028.A00(((C017408d) this.A01).A09);
    }

    @Override // X.AbstractC005102i
    public int A01() {
        return ((C017408d) this.A01).A01;
    }

    @Override // X.AbstractC005102i
    public Context A02() {
        return ((C017408d) this.A01).A09.getContext();
    }

    @Override // X.AbstractC005102i
    public View A03() {
        return ((C017408d) this.A01).A06;
    }

    @Override // X.AbstractC005102i
    public void A05() {
        ((C017408d) this.A01).A09.removeCallbacks(this.A07);
    }

    @Override // X.AbstractC005102i
    public void A06() {
        ((C017408d) this.A01).A09.setVisibility(8);
    }

    @Override // X.AbstractC005102i
    public void A07(float f) {
        AnonymousClass028.A0V(((C017408d) this.A01).A09, f);
    }

    @Override // X.AbstractC005102i
    public void A08(int i) {
        C017408d r3 = (C017408d) this.A01;
        r3.AcM(C012005t.A01().A04(r3.A09.getContext(), R.drawable.ic_pip_close));
    }

    @Override // X.AbstractC005102i
    public void A09(int i) {
        AbstractC017508e r1 = this.A01;
        r1.Acw(((C017408d) r1).A09.getContext().getText(i));
    }

    @Override // X.AbstractC005102i
    public void A0A(int i) {
        AbstractC017508e r1 = this.A01;
        r1.Ad1(i != 0 ? ((C017408d) r1).A09.getContext().getText(i) : null);
    }

    @Override // X.AbstractC005102i
    public void A0C(Drawable drawable) {
        ((C017408d) this.A01).A09.setBackground(drawable);
    }

    @Override // X.AbstractC005102i
    public void A0D(Drawable drawable) {
        this.A01.AcM(drawable);
    }

    @Override // X.AbstractC005102i
    public void A0E(Drawable drawable) {
        C017408d r0 = (C017408d) this.A01;
        r0.A04 = drawable;
        r0.A00();
    }

    @Override // X.AbstractC005102i
    public void A0F(View view) {
        A0G(view, new C009604u(-2, -2));
    }

    @Override // X.AbstractC005102i
    public void A0G(View view, C009604u r3) {
        if (view != null) {
            view.setLayoutParams(r3);
        }
        this.A01.Ac3(view);
    }

    @Override // X.AbstractC005102i
    public void A0H(CharSequence charSequence) {
        this.A01.Acw(charSequence);
    }

    @Override // X.AbstractC005102i
    public void A0I(CharSequence charSequence) {
        this.A01.Ad1(charSequence);
    }

    @Override // X.AbstractC005102i
    public void A0J(CharSequence charSequence) {
        this.A01.setWindowTitle(charSequence);
    }

    @Override // X.AbstractC005102i
    public void A0K(boolean z) {
        if (z != this.A03) {
            this.A03 = z;
            ArrayList arrayList = this.A02;
            if (0 < arrayList.size()) {
                arrayList.get(0);
                throw new NullPointerException("onMenuVisibilityChanged");
            }
        }
    }

    @Override // X.AbstractC005102i
    public void A0M(boolean z) {
        int i = 0;
        if (z) {
            i = 4;
        }
        A0Y(i, 4);
    }

    @Override // X.AbstractC005102i
    public void A0N(boolean z) {
        A0Y(16, 16);
    }

    @Override // X.AbstractC005102i
    public void A0O(boolean z) {
        A0Y(0, 2);
    }

    @Override // X.AbstractC005102i
    public void A0P(boolean z) {
        int i = 0;
        if (z) {
            i = 8;
        }
        A0Y(i, 8);
    }

    @Override // X.AbstractC005102i
    public boolean A0R() {
        AnonymousClass0XQ r0;
        ActionMenuView actionMenuView = ((C017408d) this.A01).A09.A0O;
        if (actionMenuView == null || (r0 = actionMenuView.A08) == null || !r0.A01()) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC005102i
    public boolean A0S() {
        Toolbar toolbar = ((C017408d) this.A01).A09;
        Runnable runnable = this.A07;
        toolbar.removeCallbacks(runnable);
        toolbar.postOnAnimation(runnable);
        return true;
    }

    @Override // X.AbstractC005102i
    public boolean A0T() {
        AnonymousClass0XQ r0;
        ActionMenuView actionMenuView = ((C017408d) this.A01).A09.A0O;
        if (actionMenuView == null || (r0 = actionMenuView.A08) == null || !r0.A03()) {
            return false;
        }
        return true;
    }

    @Override // X.AbstractC005102i
    public boolean A0U() {
        C07340Xp r0;
        AnonymousClass0XN r02 = ((C017408d) this.A01).A09.A0Q;
        if (r02 == null || (r0 = r02.A01) == null) {
            return false;
        }
        r0.collapseActionView();
        return true;
    }

    @Override // X.AbstractC005102i
    public boolean A0V(int i, KeyEvent keyEvent) {
        Menu A0X = A0X();
        if (A0X == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent.getDeviceId()).getKeyboardType() == 1) {
            z = false;
        }
        A0X.setQwertyMode(z);
        return A0X.performShortcut(i, keyEvent, 0);
    }

    @Override // X.AbstractC005102i
    public boolean A0W(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            A0T();
        }
        return true;
    }

    public final Menu A0X() {
        if (!this.A04) {
            AbstractC017508e r0 = this.A01;
            ((C017408d) r0).A09.setMenuCallbacks(new AnonymousClass0XL(this), new AnonymousClass0XE(this));
            this.A04 = true;
        }
        return ((C017408d) this.A01).A09.getMenu();
    }

    public void A0Y(int i, int i2) {
        AbstractC017508e r2 = this.A01;
        r2.Ac5((i & i2) | ((i2 ^ -1) & ((C017408d) r2).A01));
    }
}
