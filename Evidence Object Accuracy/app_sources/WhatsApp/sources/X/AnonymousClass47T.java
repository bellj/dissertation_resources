package X;

import android.graphics.drawable.Drawable;
import com.whatsapp.community.CommunityFragment;

/* renamed from: X.47T  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47T extends AbstractC54672h6 {
    public final /* synthetic */ CommunityFragment A00;

    @Override // X.AbstractC54672h6
    public boolean A03(int i, int i2) {
        if (i == 8) {
            return i2 == 4 || i2 == 13;
        }
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass47T(Drawable drawable, CommunityFragment communityFragment) {
        super(drawable);
        this.A00 = communityFragment;
    }
}
