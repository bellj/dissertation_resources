package X;

import android.util.Pair;
import com.whatsapp.Me;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/* renamed from: X.0zQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22650zQ {
    public static final AnonymousClass00N A06;
    public static final Charset A07 = AnonymousClass01V.A0A;
    public static final Pattern A08 = Pattern.compile("\t");
    public AnonymousClass1NC A00 = new AnonymousClass1NC(0);
    public String A01 = "";
    public final C22680zT A02;
    public final C15570nT A03;
    public final C16590pI A04;
    public final Object A05 = new Object();

    static {
        AnonymousClass00N r2 = new AnonymousClass00N(59);
        A06 = r2;
        r2.put("af", Arrays.asList("ZA", "NA"));
        r2.put("ar", Arrays.asList("EG", "SA", "SY", "IQ"));
        r2.put("sq", Arrays.asList("AL", "XK", "GR", "MK", "IT"));
        r2.put("az", Collections.singletonList("AZ"));
        r2.put("bn", Arrays.asList("IN", "BD"));
        r2.put("bg", Collections.singletonList("BG"));
        r2.put("ca", Arrays.asList("ES", "AD"));
        r2.put("zh-Hans", Arrays.asList("MY", "SG", "CN", "HK"));
        r2.put("zh-Hant", Arrays.asList("HK", "TW", "MY", "MO"));
        r2.put("hr", Arrays.asList("HR", "BA"));
        r2.put("cs", Collections.singletonList("CZ"));
        r2.put("da", Collections.singletonList("DK"));
        r2.put("nl", Arrays.asList("NL", "BE", "SR"));
        r2.put("en", Arrays.asList("IN", "PK", "ZA", "GB", "US"));
        r2.put("et", Collections.singletonList("EE"));
        r2.put("fil", Collections.singletonList("PH"));
        r2.put("fi", Collections.singletonList("FI"));
        r2.put("fr", Collections.singletonList("FR"));
        r2.put("de", Arrays.asList("DE", "AT", "CH"));
        r2.put("el", Arrays.asList("GR", "CY"));
        r2.put("gu", Collections.singletonList("IN"));
        r2.put("he", Collections.singletonList("IL"));
        r2.put("hi", Collections.singletonList("IN"));
        r2.put("hu", Arrays.asList("HU", "RO"));
        r2.put("id", Collections.singletonList("ID"));
        r2.put("ga", Arrays.asList("IE", "GB"));
        r2.put("it", Arrays.asList("IT", "CH"));
        r2.put("ja", Collections.singletonList("JP"));
        r2.put("kn", Collections.singletonList("IN"));
        r2.put("kk", Arrays.asList("KZ", "UZ", "MN"));
        r2.put("ko", Collections.singletonList("KR"));
        r2.put("lo", Collections.singletonList("LA"));
        r2.put("lv", Collections.singletonList("LV"));
        r2.put("lt", Collections.singletonList("LT"));
        r2.put("mk", Collections.singletonList("MK"));
        r2.put("ms", Collections.singletonList("MY"));
        r2.put("ml", Collections.singletonList("IN"));
        r2.put("mr", Collections.singletonList("IN"));
        r2.put("nb", Collections.singletonList("NO"));
        r2.put("fa", Arrays.asList("IR", "AF"));
        r2.put("pl", Collections.singletonList("PL"));
        r2.put("pt-BR", Collections.singletonList("BR"));
        r2.put("pt-PT", Arrays.asList("PT", "AO", "BR", "MZ"));
        r2.put("pa", Collections.singletonList("IN"));
        r2.put("ro", Arrays.asList("RO", "MD"));
        r2.put("ru", Arrays.asList("RU", "KZ", "KG", "UA"));
        r2.put("sr", Arrays.asList("RS", "BA", "ME"));
        r2.put("sk", Collections.singletonList("SK"));
        r2.put("sl", Collections.singletonList("SI"));
        r2.put("es", Arrays.asList("MX", "AR", "CL", "CO", "ES", "PE"));
        r2.put("sw", Arrays.asList("TZ", "KE", "RW", "BI"));
        r2.put("sv", Arrays.asList("SE", "FI"));
        r2.put("ta", Arrays.asList("IN", "LK", "MY", "SG"));
        r2.put("te", Collections.singletonList("IN"));
        r2.put("th", Arrays.asList("TH", "LA"));
        r2.put("tr", Collections.singletonList("TR"));
        r2.put("uk", Collections.singletonList("UA"));
        r2.put("ur", Arrays.asList("PK", "IN"));
        r2.put("uz", Arrays.asList("UZ", "RU", "KZ", "KG"));
        r2.put("vi", Collections.singletonList("VN"));
    }

    public C22650zQ(C22680zT r3, C15570nT r4, C16590pI r5) {
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public static String A00(String str) {
        try {
            int parseInt = Integer.parseInt(str);
            String str2 = (String) AnonymousClass1NE.A01.get(parseInt);
            return "ZZ".equals(str2) ? (String) AnonymousClass1NE.A00.get(parseInt) : str2;
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public static String A01(String str, String str2) {
        Pattern pattern;
        try {
            int parseInt = Integer.parseInt(str);
            String str3 = (String) AnonymousClass1NE.A01.get(parseInt);
            if (str3 != null) {
                if (!str3.equals("ZZ")) {
                    return str3;
                }
                List list = (List) AnonymousClass1NE.A02.get(parseInt);
                for (int i = 0; i < list.size(); i++) {
                    Pair pair = (Pair) list.get(i);
                    Object obj = pair.second;
                    if (obj instanceof String) {
                        pattern = Pattern.compile((String) obj);
                        list.set(i, Pair.create(pair.first, pattern));
                    } else {
                        pattern = (Pattern) obj;
                    }
                    if (pattern.matcher(str2).matches()) {
                        return (String) pair.first;
                    }
                }
            }
            return "ZZ";
        } catch (NumberFormatException unused) {
            return "ZZ";
        }
    }

    public String A02(AnonymousClass018 r7, String str) {
        String str2;
        synchronized (this.A05) {
            String A04 = AbstractC27291Gt.A04(AnonymousClass018.A00(r7.A00));
            if (!A04.equals(this.A01)) {
                List<AnonymousClass1NF> A03 = A03(A04);
                if (A03.isEmpty()) {
                    A03 = A03("en");
                }
                this.A00 = new AnonymousClass1NC(A03.size());
                for (AnonymousClass1NF r0 : A03) {
                    this.A00.A03(r0.A00, r0.A01);
                }
                this.A01 = A04;
            }
            str2 = (String) this.A00.A01(str);
        }
        return str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List A03(java.lang.String r9) {
        /*
            r8 = this;
            java.lang.String r0 = "country_names_"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r9)
            java.lang.String r0 = ".tsv"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r3 = 0
            X.0pI r0 = r8.A04     // Catch: IOException -> 0x0069
            android.content.Context r0 = r0.A00     // Catch: IOException -> 0x0069
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch: IOException -> 0x0069
            java.io.InputStream r2 = r0.open(r1)     // Catch: IOException -> 0x0069
            java.nio.charset.Charset r1 = X.C22650zQ.A07     // Catch: IOException -> 0x0069
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch: IOException -> 0x0069
            r0.<init>(r2, r1)     // Catch: IOException -> 0x0069
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch: IOException -> 0x0069
            r7.<init>(r0)     // Catch: IOException -> 0x0069
            r0 = 243(0xf3, float:3.4E-43)
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch: all -> 0x0061
            r6.<init>(r0)     // Catch: all -> 0x0061
            java.lang.String r1 = r7.readLine()     // Catch: all -> 0x005f
        L_0x0037:
            if (r1 == 0) goto L_0x005b
            java.util.regex.Pattern r0 = X.C22650zQ.A08     // Catch: all -> 0x005f
            java.lang.String[] r5 = r0.split(r1)     // Catch: all -> 0x005f
            int r4 = r5.length     // Catch: all -> 0x005f
            r3 = 1
            r2 = 0
            r1 = 2
            r0 = 0
            if (r4 != r1) goto L_0x0047
            r0 = 1
        L_0x0047:
            X.AnonymousClass009.A0F(r0)     // Catch: all -> 0x005f
            r2 = r5[r2]     // Catch: all -> 0x005f
            r1 = r5[r3]     // Catch: all -> 0x005f
            X.1NF r0 = new X.1NF     // Catch: all -> 0x005f
            r0.<init>(r2, r1)     // Catch: all -> 0x005f
            r6.add(r0)     // Catch: all -> 0x005f
            java.lang.String r1 = r7.readLine()     // Catch: all -> 0x005f
            goto L_0x0037
        L_0x005b:
            r7.close()     // Catch: IOException -> 0x0067
            goto L_0x007c
        L_0x005f:
            r0 = move-exception
            goto L_0x0063
        L_0x0061:
            r0 = move-exception
            r6 = r3
        L_0x0063:
            r7.close()     // Catch: all -> 0x0066
        L_0x0066:
            throw r0     // Catch: IOException -> 0x0067
        L_0x0067:
            r2 = move-exception
            goto L_0x006b
        L_0x0069:
            r2 = move-exception
            r6 = r3
        L_0x006b:
            java.lang.String r1 = "countryutils/getcountrylist error:"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r1)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.whatsapp.util.Log.e(r0)
        L_0x007c:
            if (r6 != 0) goto L_0x0082
            java.util.List r6 = java.util.Collections.emptyList()
        L_0x0082:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22650zQ.A03(java.lang.String):java.util.List");
    }

    public boolean A04() {
        C15570nT r0 = this.A03;
        r0.A08();
        Me me = r0.A00;
        if (me == null) {
            return false;
        }
        try {
            return "eu".equals(this.A02.A03(me.cc));
        } catch (IOException e) {
            StringBuilder sb = new StringBuilder("countryutils/is-eu failed for ");
            sb.append(me.cc);
            Log.e(sb.toString(), e);
            return false;
        }
    }

    public boolean A05(String str) {
        C15570nT r0 = this.A03;
        r0.A08();
        Me me = r0.A00;
        if (me == null) {
            return false;
        }
        return str.equals(A01(me.cc, me.number));
    }
}
