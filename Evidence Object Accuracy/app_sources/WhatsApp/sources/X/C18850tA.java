package X;

import android.content.SharedPreferences;
import android.util.Pair;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.util.RunnableTRunnableShape13S0200000_I0;
import com.whatsapp.util.RunnableTRunnableShape17S0100000_I0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

/* renamed from: X.0tA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C18850tA extends AbstractC18860tB implements AbstractC18870tC, AbstractC18880tD, AbstractC18890tE, AbstractC18900tF {
    public AbstractC41171t5 A00 = null;
    public Runnable A01;
    public boolean A02;
    public final AbstractC15710nm A03;
    public final C15570nT A04;
    public final C15450nH A05;
    public final AnonymousClass16N A06;
    public final C16240og A07;
    public final AnonymousClass16U A08;
    public final C27761Jc A09;
    public final C41191t7 A0A;
    public final C232310w A0B;
    public final AnonymousClass16S A0C;
    public final AnonymousClass16T A0D;
    public final C234411r A0E;
    public final C18920tH A0F;
    public final AnonymousClass1GG A0G = new AnonymousClass1GG(this);
    public final AnonymousClass1GH A0H = new AnonymousClass1GH(this);
    public final C233411h A0I;
    public final C18930tI A0J;
    public final C233711k A0K;
    public final C233311g A0L;
    public final C234011n A0M;
    public final C239313o A0N;
    public final C15550nR A0O;
    public final C241614l A0P;
    public final C230910i A0Q;
    public final C16590pI A0R;
    public final C15680nj A0S;
    public final C21910yB A0T;
    public final AnonymousClass1GD A0U = new AnonymousClass1JN(this);
    public final C22100yW A0V;
    public final C234211p A0W;
    public final C232510y A0X;
    public final C233511i A0Y;
    public final C14840m8 A0Z;
    public final C22900zp A0a;
    public final C234811v A0b;
    public final AnonymousClass16R A0c;
    public final ExecutorC27271Gr A0d;
    public final AbstractC14440lR A0e;
    public final C21560xc A0f;
    public final C232010t A0g;

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARA() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARB() {
    }

    @Override // X.AbstractC18870tC
    public /* synthetic */ void ARC() {
    }

    @Override // X.AbstractC18880tD
    public void AUt() {
    }

    @Override // X.AbstractC18880tD
    public void AXJ() {
    }

    public C18850tA(AbstractC15710nm r37, C15570nT r38, C15450nH r39, AnonymousClass16N r40, C16240og r41, AnonymousClass16U r42, C232310w r43, AnonymousClass16S r44, AnonymousClass16T r45, C240313y r46, C234411r r47, C234511s r48, C18920tH r49, C234711u r50, C233411h r51, C18930tI r52, C233711k r53, C233211f r54, C233811l r55, C233311g r56, C234011n r57, C239313o r58, C15550nR r59, C241614l r60, C230910i r61, C14830m7 r62, C16590pI r63, C233611j r64, C15680nj r65, C21910yB r66, C22100yW r67, C234211p r68, C232510y r69, C233511i r70, C14850m9 r71, C17220qS r72, C14840m8 r73, C22900zp r74, C234811v r75, AnonymousClass16R r76, AbstractC14440lR r77, C21560xc r78, C232010t r79) {
        this.A0R = r63;
        this.A03 = r37;
        this.A04 = r38;
        this.A0e = r77;
        this.A05 = r39;
        this.A0T = r66;
        this.A0f = r78;
        this.A0O = r59;
        this.A0L = r56;
        this.A0M = r57;
        this.A0a = r74;
        this.A0I = r51;
        this.A0g = r79;
        this.A07 = r41;
        this.A0Z = r73;
        this.A0F = r49;
        this.A0Y = r70;
        this.A0Q = r61;
        this.A0S = r65;
        this.A06 = r40;
        this.A0J = r52;
        this.A0X = r69;
        this.A0V = r67;
        this.A0K = r53;
        this.A0B = r43;
        this.A0c = r76;
        this.A0W = r68;
        this.A0C = r44;
        this.A0E = r47;
        this.A0D = r45;
        this.A0P = r60;
        this.A0b = r75;
        this.A0N = r58;
        this.A08 = r42;
        this.A0d = new ExecutorC27271Gr(r77, false);
        C41191t7 r1 = new C41191t7(r37, r38, r46, r48, new C41181t6(r38, r43, r47, this, r54, r76), r51, r53, r55, r56, r57, r61, r62, r64, r66, r67, r69, r70, r71, new C32881ct(new Random()));
        this.A0A = r1;
        this.A09 = new C27761Jc(r38, r1, r50, r51, r52, r69, r72, r77);
    }

    public static /* synthetic */ void A00(C18850tA r1, boolean z) {
        synchronized (r1) {
            r1.A02 = false;
            if (z) {
                r1.A0G();
            }
        }
    }

    public final Pair A03(Collection collection, Collection collection2) {
        C21560xc r2 = this.A0f;
        List A07 = C15380n4.A07(UserJid.class, r2.A04("SYNC_MANAGER_CONTACTS_JID_ADDED"));
        List A072 = C15380n4.A07(UserJid.class, r2.A04("SYNC_MANAGER_CONTACTS_JID_REMOVED"));
        collection.addAll(A07);
        collection2.addAll(A072);
        HashSet hashSet = new HashSet();
        C15380n4.A0D(collection, hashSet);
        HashSet hashSet2 = new HashSet();
        C15380n4.A0D(collection2, hashSet2);
        return new Pair(hashSet, hashSet2);
    }

    public Set A04(AbstractC14640lm r11, long j, boolean z) {
        C27311Gv r0 = (C27311Gv) this.A0M.A03("mute");
        if (r0 == null) {
            return Collections.emptySet();
        }
        return A0A(Collections.singletonList(new C34611gQ(null, r11, null, j, r0.A00.A00(), z, false)));
    }

    public Set A05(AbstractC14640lm r11, boolean z) {
        C27321Gw r2;
        C234011n r1 = this.A0M;
        C27331Gx r5 = (C27331Gx) r1.A03("archive");
        if (r5 != null) {
            C15570nT r0 = this.A04;
            r0.A08();
            if (r0.A05 != null && A0Q()) {
                ArrayList arrayList = new ArrayList();
                if (z && (r2 = (C27321Gw) r1.A03("pin_v1")) != null) {
                    r2.A01.A06(1);
                    arrayList.add(new C34651gU(r11, r2.A03.A00(), false));
                }
                arrayList.add(new C34631gS(r5.A00.A04(r11, false), r11, r5.A03.A00(), z));
                return A0A(arrayList);
            }
        }
        return Collections.emptySet();
    }

    public Set A06(AbstractC14640lm r11, boolean z) {
        C27331Gx r1;
        C234011n r12 = this.A0M;
        C27321Gw r2 = (C27321Gw) r12.A03("pin_v1");
        if (r2 != null) {
            C15570nT r0 = this.A04;
            r0.A08();
            if (r0.A05 != null && A0Q()) {
                ArrayList arrayList = new ArrayList();
                if (z && (r1 = (C27331Gx) r12.A03("archive")) != null) {
                    arrayList.add(new C34631gS(r1.A00.A04(r11, false), r11, r1.A03.A00(), false));
                }
                r2.A01.A06(1);
                arrayList.add(new C34651gU(r11, r2.A03.A00(), z));
                return A0A(arrayList);
            }
        }
        return Collections.emptySet();
    }

    public Set A07(AbstractC14640lm r11, boolean z) {
        C234011n r3 = this.A0M;
        AnonymousClass1H0 r2 = (AnonymousClass1H0) r3.A03("deleteChat");
        if (r2 == null || !A0Q()) {
            return Collections.emptySet();
        }
        Set A06 = A06(r11, false);
        synchronized (r3) {
            r3.A03.A0H(A06);
        }
        r2.A02.A06(3);
        return A0A(Collections.singletonList(new C34371g2(null, r2.A01.A03(r11), r11, null, r2.A03.A00(), z, true)));
    }

    public Set A08(AbstractC14640lm r10, boolean z) {
        AnonymousClass1H2 r1 = (AnonymousClass1H2) this.A0M.A03("markChatAsRead");
        if (r1 == null) {
            return Collections.emptySet();
        }
        return A0A(Collections.singletonList(new C34521gH(null, r1.A00.A04(r10, false), r10, null, r1.A02.A00(), z, false)));
    }

    public Set A09(AbstractC14640lm r11, boolean z, boolean z2) {
        AnonymousClass1H5 r2 = (AnonymousClass1H5) this.A0M.A03("clearChat");
        if (r2 != null) {
            C15570nT r0 = this.A04;
            r0.A08();
            if (r0.A05 != null) {
                C233411h r1 = r2.A02;
                int i = 5;
                if (z) {
                    i = 4;
                }
                r1.A06(i);
                return A0A(Collections.singletonList(new C34671gW(null, r2.A01.A03(r11), r11, null, r2.A03.A00(), z, z2, true)));
            }
        }
        return Collections.emptySet();
    }

    public final Set A0A(Collection collection) {
        Set A0B;
        if (!A0Q()) {
            return Collections.emptySet();
        }
        C234011n r1 = this.A0M;
        synchronized (r1) {
            A0B = r1.A03.A0B(collection);
        }
        return A0B;
    }

    public Set A0B(Collection collection, boolean z) {
        AbstractC14640lm A0B;
        C27341Gy r1 = (C27341Gy) this.A0M.A03("star");
        if (r1 != null) {
            C15570nT r0 = this.A04;
            r0.A08();
            if (r0.A05 != null) {
                long A00 = r1.A02.A00();
                ArrayList arrayList = new ArrayList(collection.size());
                Iterator it = collection.iterator();
                while (it.hasNext()) {
                    AbstractC15340mz r2 = (AbstractC15340mz) it.next();
                    AnonymousClass1IS r8 = r2.A0z;
                    AbstractC14640lm r12 = r8.A00;
                    if (C15380n4.A0J(r12) || C15380n4.A0F(r12)) {
                        A0B = r2.A0B();
                    } else {
                        A0B = null;
                    }
                    arrayList.add(new C34591gO(null, A0B, r8, null, A00, z, false));
                }
                return A0A(arrayList);
            }
        }
        return Collections.emptySet();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:29:0x0077 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r18v0, types: [X.0tA] */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.util.Collection] */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.util.AbstractCollection, java.util.ArrayList] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Set A0C(java.util.Collection r19, boolean r20) {
        /*
            r18 = this;
            r4 = r18
            X.11n r1 = r4.A0M
            java.lang.String r0 = "deleteMessageForMe"
            java.lang.Object r1 = r1.A03(r0)
            X.1H4 r1 = (X.AnonymousClass1H4) r1
            if (r1 != 0) goto L_0x0013
            java.util.Set r0 = java.util.Collections.emptySet()
            return r0
        L_0x0013:
            boolean r0 = r19.isEmpty()
            if (r0 != 0) goto L_0x0073
            X.0m7 r0 = r1.A00
            long r12 = r0.A00()
            int r0 = r19.size()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r0)
            java.util.Iterator r6 = r19.iterator()
        L_0x002c:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0077
            java.lang.Object r5 = r6.next()
            X.0mz r5 = (X.AbstractC15340mz) r5
            X.1IS r10 = r5.A0z
            X.0lm r1 = r10.A00
            boolean r0 = X.C15380n4.A0J(r1)
            if (r0 != 0) goto L_0x006e
            boolean r0 = X.C15380n4.A0F(r1)
            if (r0 != 0) goto L_0x006e
            r9 = 0
        L_0x0049:
            boolean r0 = r10.A02
            r1 = 0
            if (r0 == 0) goto L_0x006b
            int r0 = r5.A0C
            if (r0 != 0) goto L_0x0064
            r14 = 0
        L_0x0055:
            r8 = 0
            r17 = 0
            r16 = r20
            r11 = r8
            X.1g6 r7 = new X.1g6
            r7.<init>(r8, r9, r10, r11, r12, r14, r16, r17)
            r3.add(r7)
            goto L_0x002c
        L_0x0064:
            long r14 = r5.A0H
            int r0 = (r14 > r1 ? 1 : (r14 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x006b
            goto L_0x0055
        L_0x006b:
            long r14 = r5.A0I
            goto L_0x0055
        L_0x006e:
            X.0lm r9 = r5.A0B()
            goto L_0x0049
        L_0x0073:
            java.util.List r3 = java.util.Collections.emptyList()
        L_0x0077:
            java.util.Set r0 = r4.A0A(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18850tA.A0C(java.util.Collection, boolean):java.util.Set");
    }

    /* JADX WARNING: Removed duplicated region for block: B:180:0x01c7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x01c2 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0D() {
        /*
        // Method dump skipped, instructions count: 730
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18850tA.A0D():void");
    }

    public void A0E() {
        C233711k r3 = this.A0K;
        if (r3.A01().getBoolean("syncd_one_time_cleanup_for_non_md_user", false) && r3.A01().getInt("syncd_bootstrap_state", 0) == 0 && this.A05.A05(AbstractC15460nI.A0g) && !A0R()) {
            this.A04.A08();
            C233511i r1 = this.A0Y;
            if (r1.A0J() || r1.A0I()) {
                A0J(7);
            }
        }
        if (r3.A01().getBoolean("syncd_one_time_cleanup_for_non_md_user", false) && this.A05.A05(AbstractC15460nI.A0g)) {
            r3.A08(false);
        }
    }

    public void A0F() {
        this.A04.A08();
        Object A03 = this.A0M.A03("time_format");
        if (A03 != null) {
            this.A0e.Ab2(new RunnableBRunnable0Shape2S0200000_I0_2(this, 19, A03));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006f, code lost:
        if (r0 != false) goto L_0x0071;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G() {
        /*
        // Method dump skipped, instructions count: 382
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C18850tA.A0G():void");
    }

    public final void A0H() {
        List singletonList;
        C27831Jk A02;
        if (A0Q()) {
            this.A04.A08();
            C249917r r3 = (C249917r) this.A0M.A03("primary_feature");
            if (r3 != null) {
                r3.A00.A08();
                AnonymousClass009.A0F(true);
                C14850m9 r1 = r3.A03;
                boolean A07 = r1.A07(808);
                boolean A072 = r1.A07(1312);
                ArrayList arrayList = new ArrayList();
                if (A07) {
                    arrayList.add("contact_except");
                }
                if (A072) {
                    arrayList.add("ddm_settings");
                }
                arrayList.add("reactions_send");
                C233511i r12 = ((AbstractC250017s) r3).A00;
                List A09 = r12.A09("primary_feature", true);
                if (A09.isEmpty()) {
                    A09 = r12.A09("primary_feature", false);
                }
                if (!A09.isEmpty() && (A02 = ((AnonymousClass1JQ) A09.get(0)).A02()) != null) {
                    C34681gX r0 = A02.A0G;
                    if (r0 == null) {
                        r0 = C34681gX.A01;
                    }
                    if (!(!arrayList.equals(r0.A00))) {
                        singletonList = Collections.emptyList();
                        A0L(singletonList);
                    }
                }
                singletonList = Collections.singletonList(new C34691gY(null, null, arrayList, r3.A02.A00()));
                A0L(singletonList);
            }
        }
    }

    public final void A0I() {
        synchronized (this) {
            Runnable runnable = this.A01;
            if (runnable != null) {
                this.A0e.AaP(runnable);
                this.A01 = null;
                Log.i("sync-manager/forceSync removed scheduled sync");
            }
            A0G();
        }
    }

    public void A0J(int i) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("sync-manager/deleteAllSyncdData: isSyncing = ");
            sb.append(this.A02);
            Log.i(sb.toString());
            this.A00 = new C41201t8(this, i);
            if (!this.A02) {
                A0P();
            }
        }
    }

    public void A0K(Integer num) {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append("sync-manager/handleFatalFailure: errorCode = ");
            sb.append(num);
            sb.append("; isSyncing = ");
            sb.append(this.A02);
            Log.i(sb.toString());
            this.A00 = new C41211t9(this, num);
            if (!this.A02) {
                A0P();
            }
        }
    }

    public final void A0L(Collection collection) {
        if (A0Q()) {
            this.A0M.A07(collection);
        }
    }

    public void A0M(Collection collection, boolean z) {
        C15570nT r1 = this.A04;
        r1.A08();
        synchronized (this) {
            boolean z2 = false;
            if (this.A0Z.A03()) {
                r1.A08();
                if (((AnonymousClass1H1) this.A0M.A03("contact")) != null && A0Q()) {
                    z2 = true;
                }
                Set hashSet = new HashSet();
                Set hashSet2 = new HashSet();
                if (z2) {
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it = collection.iterator();
                    while (it.hasNext()) {
                        C15370n3 r2 = (C15370n3) it.next();
                        UserJid userJid = (UserJid) r2.A0B(UserJid.class);
                        if (userJid != null) {
                            if (r2.A0f) {
                                arrayList.add(userJid);
                            } else {
                                arrayList2.add(userJid);
                            }
                        }
                    }
                    Pair A03 = A03(arrayList, arrayList2);
                    hashSet = (Set) A03.first;
                    hashSet2 = (Set) A03.second;
                }
                C16310on A02 = this.A0g.A02();
                AnonymousClass1Lx A00 = A02.A00();
                C21560xc r12 = this.A0f;
                r12.A06("SYNC_MANAGER_CONTACTS_JID_ADDED", hashSet);
                r12.A06("SYNC_MANAGER_CONTACTS_JID_REMOVED", hashSet2);
                if (z) {
                    this.A0O.A0V(collection);
                } else {
                    this.A0O.A0X(collection);
                }
                A00.A00();
                A00.close();
                A02.close();
                if (z2) {
                    A0D();
                }
            }
        }
    }

    public void A0N(Set set) {
        if (!set.isEmpty()) {
            C234011n r4 = this.A0M;
            synchronized (r4) {
                C16310on A02 = r4.A03.A02.A02();
                AnonymousClass1Lx A00 = A02.A00();
                C233511i.A01(A02.A03, (String[]) set.toArray(AnonymousClass01V.A0H));
                A00.A00();
                A00.close();
                A02.close();
            }
        }
    }

    public void A0O(Set set) {
        if (!set.isEmpty()) {
            C234011n r1 = this.A0M;
            synchronized (r1) {
                r1.A03.A0H(set);
            }
            A0I();
        }
    }

    public boolean A0P() {
        boolean z;
        synchronized (this) {
            AbstractC41171t5 r2 = this.A00;
            if (r2 == null) {
                z = false;
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("sync-manager/handleSyncdStateIfRequired: syncdState = ");
                sb.append(r2);
                Log.i(sb.toString());
                AbstractC41171t5 r1 = this.A00;
                if (!(r1 instanceof C41211t9)) {
                    ((C41201t8) r1).A00.A0C.A00();
                } else {
                    C41211t9 r12 = (C41211t9) r1;
                    r12.A01.A0A.A04(null, r12.A00);
                }
                this.A00 = null;
                z = true;
            }
        }
        return z;
    }

    public boolean A0Q() {
        if (!A0R()) {
            if (!this.A0Z.A03()) {
                this.A04.A08();
            } else {
                C233511i r1 = this.A0Y;
                if (r1.A0J() || r1.A0I()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean A0R() {
        if (this.A0C.A03()) {
            return true;
        }
        this.A0E.A00.A08();
        return false;
    }

    @Override // X.AbstractC18890tE
    public void AQ6(AbstractC30271Wt r4) {
        if (r4 instanceof C34821gl) {
            try {
                C233311g r2 = this.A0L;
                C34821gl r42 = (C34821gl) r4;
                if (r42.A01 && !r2.A0A()) {
                    r2.A07();
                    r2.A09(new HashSet(r42.A14().values()));
                    this.A0e.Ab2(new RunnableTRunnableShape17S0100000_I0(this, 0));
                }
            } catch (AnonymousClass1K1 e) {
                A0K(Integer.valueOf(e.errorCode));
            }
        }
    }

    @Override // X.AbstractC18870tC
    public void AR9() {
        this.A0e.Ab4(new RunnableBRunnable0Shape4S0100000_I0_4(this, 20), "SyncManager/onHandlerConnected");
    }

    @Override // X.AbstractC18900tF
    public void ASC() {
        this.A04.A08();
        A0F();
        C27351Gz r2 = (C27351Gz) this.A0M.A03("setting_locale");
        if (r2 != null) {
            this.A0e.Ab2(new RunnableTRunnableShape13S0200000_I0(this, r2));
        }
    }

    @Override // X.AbstractC18880tD
    public void AXI() {
        C239313o r1 = this.A0N;
        synchronized (r1) {
            Set set = r1.A00;
            if (set != null) {
                set.clear();
            }
        }
        this.A0T.A04();
        SharedPreferences.Editor edit = this.A0K.A01().edit();
        for (String str : C233711k.A02) {
            edit.remove(str);
        }
        edit.apply();
        C21560xc r2 = this.A0f;
        r2.A05("SYNC_MANAGER_CONTACTS_JID_ADDED", null);
        r2.A05("SYNC_MANAGER_CONTACTS_JID_REMOVED", null);
    }
}
