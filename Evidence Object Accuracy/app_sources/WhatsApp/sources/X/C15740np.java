package X;

import android.content.Context;
import android.os.CancellationSignal;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.0np  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15740np {
    public Context A00;

    public C15740np(Context context) {
        this.A00 = context;
    }

    public static final String A00(File file, String str) {
        File canonicalFile = file.getCanonicalFile();
        File canonicalFile2 = new File(canonicalFile, str).getCanonicalFile();
        String path = canonicalFile.getPath();
        String path2 = canonicalFile2.getPath();
        if (!path2.startsWith(path)) {
            StringBuilder sb = new StringBuilder("Invalid relative path (escapes parent): ");
            sb.append(path2);
            throw new IOException(sb.toString());
        } else if (!path2.equals(path)) {
            String substring = path2.substring(path.length() + 1);
            if (substring.length() != 0) {
                return substring;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid relative path (points to root): ");
            sb2.append(path2);
            throw new IOException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid relative path (points to root): ");
            sb3.append(path2);
            throw new IOException(sb3.toString());
        }
    }

    public static void A01(CancellationSignal cancellationSignal, InputStream inputStream, OutputStream outputStream, byte[] bArr) {
        while (true) {
            int read = inputStream.read(bArr);
            if (read >= 0) {
                cancellationSignal.throwIfCanceled();
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public String A02(String str) {
        Context context = this.A00;
        File file = new File(context.getFilesDir(), "__relative_root_1");
        File file2 = new File(context.getFilesDir(), "__relative_root_2");
        String A00 = A00(file, str);
        if (A00.equals(A00(file2, str))) {
            return A00;
        }
        StringBuilder sb = new StringBuilder("Invalid relative path: ");
        sb.append(str);
        throw new IOException(sb.toString());
    }

    public void A03(CancellationSignal cancellationSignal, File file, File file2, byte[] bArr) {
        File canonicalFile = file.getCanonicalFile();
        File canonicalFile2 = file2.getCanonicalFile();
        if (!canonicalFile.equals(canonicalFile2)) {
            File parentFile = canonicalFile2.getParentFile();
            if (parentFile != null) {
                parentFile.mkdirs();
            }
            if (!canonicalFile.renameTo(canonicalFile2)) {
                File canonicalFile3 = canonicalFile.getCanonicalFile();
                File canonicalFile4 = canonicalFile2.getCanonicalFile();
                if (!canonicalFile3.equals(canonicalFile4)) {
                    File parentFile2 = canonicalFile4.getParentFile();
                    if (parentFile2 != null) {
                        parentFile2.mkdirs();
                    }
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(canonicalFile4);
                        FileInputStream fileInputStream = new FileInputStream(canonicalFile3);
                        try {
                            A01(cancellationSignal, fileInputStream, fileOutputStream, bArr);
                            fileInputStream.close();
                            fileOutputStream.close();
                        } catch (Throwable th) {
                            try {
                                fileInputStream.close();
                            } catch (Throwable unused) {
                            }
                            throw th;
                        }
                    } catch (IOException e) {
                        canonicalFile4.delete();
                        throw e;
                    }
                }
                canonicalFile.delete();
            }
        }
    }
}
