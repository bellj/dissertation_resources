package X;

import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.util.Log;

/* renamed from: X.5Au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C111805Au implements AbstractC47972Dm {
    public final /* synthetic */ CallsHistoryFragment A00;

    public C111805Au(CallsHistoryFragment callsHistoryFragment) {
        this.A00 = callsHistoryFragment;
    }

    @Override // X.AbstractC47972Dm
    public void ANV() {
        Log.i("voip/CallsFragment/onCallLogDeleted");
        this.A00.A1C();
    }

    @Override // X.AbstractC47972Dm
    public void ANX(AnonymousClass1YT r2) {
        Log.i("CallsFragment/onCallLogUpdated");
        this.A00.A1C();
    }
}
