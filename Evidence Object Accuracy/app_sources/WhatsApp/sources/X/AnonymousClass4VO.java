package X;

/* renamed from: X.4VO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4VO {
    public short A00;
    public byte[] A01;

    public AnonymousClass4VO(byte[] bArr, short s) {
        this.A00 = s;
        this.A01 = bArr;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("WtExtension{extensionType=");
        A0k.append((int) this.A00);
        A0k.append(", extensionData=");
        A0k.append(AnonymousClass3JS.A03(this.A01));
        return C12970iu.A0v(A0k);
    }
}
