package X;

import android.os.SystemClock;

/* renamed from: X.04d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008104d extends AnonymousClass030 {
    @Override // X.AnonymousClass030
    public AnonymousClass032 A01() {
        return new C008004c();
    }

    @Override // X.AnonymousClass030
    public boolean A02(AnonymousClass032 r3) {
        C008004c r32 = (C008004c) r3;
        if (r32 != null) {
            r32.realtimeMs = SystemClock.elapsedRealtime();
            r32.uptimeMs = SystemClock.uptimeMillis();
            return true;
        }
        throw new IllegalArgumentException("Null value passed to getSnapshot!");
    }
}
