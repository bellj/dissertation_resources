package X;

import com.whatsapp.nativelibloader.WhatsAppLibLoader;
import com.whatsapp.support.DescribeProblemActivity;

/* renamed from: X.23P  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass23P extends ActivityC13790kL {
    public boolean A00 = false;

    public AnonymousClass23P() {
        A0R(new C103614r2(this));
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            DescribeProblemActivity describeProblemActivity = (DescribeProblemActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) A1l().generatedComponent());
            AnonymousClass01J r2 = r3.A1E;
            ((ActivityC13830kP) describeProblemActivity).A05 = (AbstractC14440lR) r2.ANe.get();
            ((ActivityC13810kN) describeProblemActivity).A0C = (C14850m9) r2.A04.get();
            ((ActivityC13810kN) describeProblemActivity).A05 = (C14900mE) r2.A8X.get();
            ((ActivityC13810kN) describeProblemActivity).A03 = (AbstractC15710nm) r2.A4o.get();
            ((ActivityC13810kN) describeProblemActivity).A04 = (C14330lG) r2.A7B.get();
            ((ActivityC13810kN) describeProblemActivity).A0B = (AnonymousClass19M) r2.A6R.get();
            ((ActivityC13810kN) describeProblemActivity).A0A = (C18470sV) r2.AK8.get();
            ((ActivityC13810kN) describeProblemActivity).A06 = (C15450nH) r2.AII.get();
            ((ActivityC13810kN) describeProblemActivity).A08 = (AnonymousClass01d) r2.ALI.get();
            ((ActivityC13810kN) describeProblemActivity).A0D = (C18810t5) r2.AMu.get();
            ((ActivityC13810kN) describeProblemActivity).A09 = (C14820m6) r2.AN3.get();
            ((ActivityC13810kN) describeProblemActivity).A07 = (C18640sm) r2.A3u.get();
            ((ActivityC13790kL) describeProblemActivity).A05 = (C14830m7) r2.ALb.get();
            ((ActivityC13790kL) describeProblemActivity).A0D = (C252718t) r2.A9K.get();
            ((ActivityC13790kL) describeProblemActivity).A01 = (C15570nT) r2.AAr.get();
            ((ActivityC13790kL) describeProblemActivity).A04 = (C15810nw) r2.A73.get();
            ((ActivityC13790kL) describeProblemActivity).A09 = r3.A06();
            ((ActivityC13790kL) describeProblemActivity).A06 = (C14950mJ) r2.AKf.get();
            ((ActivityC13790kL) describeProblemActivity).A00 = (AnonymousClass12P) r2.A0H.get();
            ((ActivityC13790kL) describeProblemActivity).A02 = (C252818u) r2.AMy.get();
            ((ActivityC13790kL) describeProblemActivity).A03 = (C22670zS) r2.A0V.get();
            ((ActivityC13790kL) describeProblemActivity).A0A = (C21840y4) r2.ACr.get();
            ((ActivityC13790kL) describeProblemActivity).A07 = (C15880o3) r2.ACF.get();
            ((ActivityC13790kL) describeProblemActivity).A0C = (C21820y2) r2.AHx.get();
            ((ActivityC13790kL) describeProblemActivity).A0B = (C15510nN) r2.AHZ.get();
            ((ActivityC13790kL) describeProblemActivity).A08 = (C249317l) r2.A8B.get();
            describeProblemActivity.A07 = (C16120oU) r2.ANE.get();
            describeProblemActivity.A05 = (C18790t3) r2.AJw.get();
            describeProblemActivity.A0G = (C22650zQ) r2.A4n.get();
            describeProblemActivity.A04 = (AnonymousClass19Y) r2.AI6.get();
            describeProblemActivity.A0F = (C252018m) r2.A7g.get();
            describeProblemActivity.A0H = (C22190yg) r2.AB6.get();
            describeProblemActivity.A0C = (C17070qD) r2.AFC.get();
            describeProblemActivity.A03 = (AnonymousClass10G) r2.A5K.get();
            describeProblemActivity.A06 = (C17050qB) r2.ABL.get();
            describeProblemActivity.A0A = (WhatsAppLibLoader) r2.ANa.get();
            describeProblemActivity.A08 = (AnonymousClass11G) r2.AKq.get();
            describeProblemActivity.A0D = (C254819o) r2.A4D.get();
        }
    }
}
