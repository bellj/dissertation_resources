package X;

import android.app.Application;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S1200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.GetVNameCertificateJob;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.0lE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C14310lE extends AnonymousClass014 {
    public int A00;
    public C14320lF A01;
    public C14530la A02;
    public C14360lJ A03;
    public Runnable A04 = null;
    public String A05;
    public boolean A06;
    public boolean A07 = false;
    public final Handler A08;
    public final AnonymousClass017 A09;
    public final AnonymousClass016 A0A;
    public final AnonymousClass016 A0B;
    public final AnonymousClass016 A0C;
    public final C14900mE A0D;
    public final C18790t3 A0E;
    public final C20670w8 A0F;
    public final C16240og A0G;
    public final C14650lo A0H;
    public final AbstractC13940ka A0I;
    public final C19850um A0J;
    public final AnonymousClass19T A0K;
    public final AnonymousClass19X A0L;
    public final C22700zV A0M;
    public final AnonymousClass2IA A0N;
    public final AnonymousClass018 A0O;
    public final C22180yf A0P;
    public final C17220qS A0Q;
    public final AnonymousClass5WG A0R;
    public final AbstractC14440lR A0S;
    public final List A0T;

    public C14310lE(Application application, Handler handler, C14900mE r5, C18790t3 r6, C20670w8 r7, C16240og r8, C14650lo r9, AbstractC13940ka r10, C19850um r11, AnonymousClass19T r12, AnonymousClass19X r13, C22700zV r14, AnonymousClass2IA r15, AnonymousClass018 r16, C22180yf r17, C17220qS r18, AbstractC14440lR r19) {
        super(application);
        AnonymousClass016 r1 = new AnonymousClass016();
        this.A0A = r1;
        this.A09 = AnonymousClass0R2.A00(new AnonymousClass02O() { // from class: X.4rS
            @Override // X.AnonymousClass02O
            public final Object apply(Object obj) {
                C14310lE r0 = C14310lE.this;
                C14320lF r3 = (C14320lF) obj;
                if (r3 == null || !r3.A0K.equals(r0.A05)) {
                    return null;
                }
                return r3;
            }
        }, r1);
        this.A0B = new AnonymousClass016();
        this.A0C = new AnonymousClass016();
        this.A0T = new LinkedList();
        this.A0R = new C69623Zy(this);
        this.A0D = r5;
        this.A0I = r10;
        this.A0S = r19;
        this.A0E = r6;
        this.A0L = r13;
        this.A0F = r7;
        this.A0Q = r18;
        this.A0K = r12;
        this.A0P = r17;
        this.A0O = r16;
        this.A0G = r8;
        this.A0M = r14;
        this.A0J = r11;
        this.A0H = r9;
        this.A0N = r15;
        this.A08 = handler;
    }

    @Override // X.AnonymousClass015
    public void A03() {
        for (Runnable runnable : this.A0T) {
            runnable.run();
        }
        Runnable runnable2 = this.A04;
        if (runnable2 != null) {
            this.A08.removeCallbacks(runnable2);
            this.A04 = null;
        }
    }

    public void A04(Editable editable, C37071lG r7) {
        C14320lF r1 = this.A01;
        if (!(r1 instanceof AnonymousClass2KT)) {
            String A01 = C33771f3.A01(editable.toString());
            A08(A01);
            r1 = null;
            if (A01 != null && !A01.equals(null)) {
                A09(null);
                C14320lF r0 = this.A01;
                if (r0 == null || !TextUtils.equals(r0.A0K, A01)) {
                    if (this.A0P.A0A(A01)) {
                        UserJid A012 = C22180yf.A01(Uri.parse(A01));
                        if (A012 != null) {
                            A05(r7, A012, A01);
                        }
                        this.A0A.A0A(C49132Jk.A00.get(A01));
                    } else {
                        AnonymousClass19X r2 = this.A0L;
                        if (r2.A02() && r2.A07.A01(A01)) {
                            this.A01 = new C83783xt(this.A0E, A01);
                            r2.A01(this.A0R, A01);
                        }
                        this.A0A.A0A(C49132Jk.A00.get(A01));
                    }
                    if (this.A01 == null) {
                        boolean z = this.A07;
                        Runnable runnable = this.A04;
                        if (runnable != null) {
                            this.A08.removeCallbacks(runnable);
                            this.A04 = null;
                        }
                        if (z) {
                            A06(r7, A01);
                            return;
                        }
                        RunnableBRunnable0Shape0S1200000_I0 runnableBRunnable0Shape0S1200000_I0 = new RunnableBRunnable0Shape0S1200000_I0(this, r7, A01, 15);
                        this.A04 = runnableBRunnable0Shape0S1200000_I0;
                        this.A08.postDelayed(runnableBRunnable0Shape0S1200000_I0, (long) 700);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        this.A0A.A0B(r1);
    }

    public final void A05(C37071lG r7, UserJid userJid, String str) {
        this.A01 = new C14630lk(this.A0E, userJid, str);
        int dimensionPixelSize = ((AnonymousClass014) this).A00.getResources().getDimensionPixelSize(R.dimen.thumbnail_size);
        C44741zT A06 = this.A0J.A06(userJid);
        if (A06 != null) {
            r7.A02(null, A06, null, new AnonymousClass3VA(this, userJid), 2);
            return;
        }
        AnonymousClass19T r2 = this.A0K;
        if (r2.A08(userJid)) {
            int i = 1;
            if (r2.A08.A0F(userJid)) {
                i = 4;
            }
            r2.A05(userJid, dimensionPixelSize, i * 6, false);
            return;
        }
        this.A0S.Ab2(new RunnableBRunnable0Shape3S0200000_I0_3(this, 15, userJid));
    }

    public void A06(C37071lG r9, String str) {
        if (str != null) {
            if (this.A0P.A0A(str)) {
                UserJid A01 = C22180yf.A01(Uri.parse(str));
                if (A01 != null) {
                    A05(r9, A01, str);
                    return;
                }
            } else {
                AnonymousClass19X r2 = this.A0L;
                if (r2.A02() && r2.A07.A01(str)) {
                    this.A01 = new C83783xt(this.A0E, str);
                    r2.A01(this.A0R, str);
                    return;
                }
            }
            long elapsedRealtime = SystemClock.elapsedRealtime();
            C14900mE r22 = this.A0D;
            AbstractC14440lR r6 = this.A0S;
            C49132Jk.A00(r22, this.A0E, new AnonymousClass3UR(this, elapsedRealtime), this.A0O, r6, str);
        }
    }

    public final void A07(UserJid userJid) {
        if (this.A01 != null) {
            AnonymousClass1M2 A00 = this.A0M.A00(userJid);
            if (A00 != null) {
                C14320lF r1 = this.A01;
                r1.A0E = A00.A08;
                this.A0A.A0A(r1);
                return;
            }
            this.A0F.A00(new GetVNameCertificateJob(userJid));
        }
    }

    public void A08(String str) {
        if (!C29941Vi.A00(str, this.A05)) {
            this.A00 = 0;
            this.A05 = str;
            this.A06 = true;
            this.A01 = null;
            this.A03 = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        if (android.text.TextUtils.equals(r0.A02, r4) == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.String r0 = r3.A05
            boolean r0 = android.text.TextUtils.equals(r0, r4)
            if (r0 == 0) goto L_0x002d
            r0 = 0
            r3.A06 = r0
            r2 = 0
            r3.A01 = r2
            r3.A03 = r2
            X.0la r0 = r3.A02
            if (r0 == 0) goto L_0x001d
            java.lang.String r0 = r0.A02
            boolean r0 = android.text.TextUtils.equals(r0, r4)
            r1 = 1
            if (r0 != 0) goto L_0x001e
        L_0x001d:
            r1 = 0
        L_0x001e:
            X.0la r0 = r3.A02
            if (r0 == 0) goto L_0x002b
            if (r1 == 0) goto L_0x002b
            X.016 r1 = r3.A0C
            X.0lU r0 = r0.A01
            r1.A0A(r0)
        L_0x002b:
            r3.A02 = r2
        L_0x002d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C14310lE.A09(java.lang.String):void");
    }
}
