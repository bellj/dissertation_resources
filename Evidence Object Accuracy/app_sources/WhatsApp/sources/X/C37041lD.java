package X;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1lD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C37041lD extends AnonymousClass015 {
    public boolean A00;
    public boolean A01;
    public final AnonymousClass016 A02;
    public final AnonymousClass016 A03;
    public final AnonymousClass016 A04;
    public final AnonymousClass016 A05;
    public final AnonymousClass016 A06;
    public final AnonymousClass016 A07;
    public final AnonymousClass016 A08;
    public final AnonymousClass016 A09;
    public final AnonymousClass016 A0A = new AnonymousClass016();
    public final AnonymousClass016 A0B;
    public final AnonymousClass016 A0C;
    public final C16170oZ A0D;
    public final C20670w8 A0E;
    public final C14650lo A0F;
    public final C25831Az A0G;
    public final C48202Ev A0H;
    public final AnonymousClass2EM A0I;
    public final AnonymousClass19Q A0J;
    public final AnonymousClass2E4 A0K;
    public final C22700zV A0L;
    public final C15610nY A0M;
    public final C14830m7 A0N;
    public final UserJid A0O;
    public final C19840ul A0P;
    public final AbstractC14440lR A0Q;

    public C37041lD(C16170oZ r13, C20670w8 r14, C14650lo r15, C25831Az r16, C48202Ev r17, AnonymousClass2EM r18, AnonymousClass19Q r19, AnonymousClass2E4 r20, C22700zV r21, C15610nY r22, C14830m7 r23, UserJid userJid, C19840ul r25, AbstractC14440lR r26) {
        AnonymousClass016 r10 = new AnonymousClass016();
        this.A07 = r10;
        AnonymousClass016 r9 = new AnonymousClass016();
        this.A0B = r9;
        AnonymousClass016 r8 = new AnonymousClass016();
        this.A02 = r8;
        AnonymousClass016 r7 = new AnonymousClass016();
        this.A04 = r7;
        AnonymousClass016 r6 = new AnonymousClass016();
        this.A09 = r6;
        AnonymousClass016 r5 = new AnonymousClass016();
        this.A08 = r5;
        AnonymousClass016 r4 = new AnonymousClass016();
        this.A05 = r4;
        AnonymousClass016 r3 = new AnonymousClass016();
        this.A03 = r3;
        AnonymousClass016 r2 = new AnonymousClass016();
        this.A06 = r2;
        AnonymousClass016 r1 = new AnonymousClass016();
        this.A0C = r1;
        this.A01 = true;
        this.A0N = r23;
        this.A0O = userJid;
        this.A0I = r18;
        this.A0D = r13;
        this.A0E = r14;
        this.A0P = r25;
        this.A0K = r20;
        this.A0M = r22;
        this.A0G = r16;
        this.A0L = r21;
        this.A0J = r19;
        this.A0F = r15;
        this.A0H = r17;
        this.A0Q = r26;
        r18.A05 = r10;
        r18.A00 = r8;
        r18.A08 = r9;
        r18.A07 = r6;
        r18.A02 = r7;
        r18.A06 = r5;
        r18.A03 = r4;
        r18.A09 = r1;
        r18.A01 = r3;
        r18.A04 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        if (r0 == null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        r1 = r0.A01.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        if (r1.isEmpty() != false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0038, code lost:
        r10 = (X.C44741zT) r1.get(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003f, code lost:
        if (r10 == null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        if (r5 == null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        if (r5.A02 == null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
        r2 = new X.AnonymousClass2E6(r16, r15, r5, r19, r20, r17.A0E());
        r13 = r15.A0K;
        r13.A00 = r2;
        r18.A01(null, r10, new X.C1098053c(), new X.C1098653i(r13), r13, 3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        r0 = ((X.C84493zO) r1).A00;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(X.ActivityC000800j r16, X.C37051lE r17, X.C37071lG r18, java.lang.String r19, java.lang.String r20) {
        /*
            r15 = this;
            r4 = r15
            X.016 r0 = r15.A0B
            java.lang.Object r5 = r0.A01()
            X.2EA r5 = (X.AnonymousClass2EA) r5
            r3 = r17
            java.util.List r1 = r3.A07
            boolean r0 = r1.isEmpty()
            r9 = 0
            if (r0 != 0) goto L_0x006b
            java.util.Iterator r2 = r1.iterator()     // Catch: Exception -> 0x006b
        L_0x0018:
            boolean r0 = r2.hasNext()     // Catch: Exception -> 0x006b
            if (r0 == 0) goto L_0x006b
            java.lang.Object r1 = r2.next()     // Catch: Exception -> 0x006b
            X.4JV r1 = (X.AnonymousClass4JV) r1     // Catch: Exception -> 0x006b
            boolean r0 = r1 instanceof X.C84493zO     // Catch: Exception -> 0x006b
            if (r0 == 0) goto L_0x0018
            X.3zO r1 = (X.C84493zO) r1     // Catch: Exception -> 0x006b
            X.4Wm r0 = r1.A00     // Catch: Exception -> 0x006b
            if (r0 == 0) goto L_0x006b
            X.1zO r0 = r0.A01     // Catch: Exception -> 0x006b
            java.util.List r1 = r0.A06     // Catch: Exception -> 0x006b
            boolean r0 = r1.isEmpty()     // Catch: Exception -> 0x006b
            if (r0 != 0) goto L_0x006b
            r0 = 0
            java.lang.Object r10 = r1.get(r0)     // Catch: Exception -> 0x006b
            X.1zT r10 = (X.C44741zT) r10     // Catch: Exception -> 0x006b
            if (r10 == 0) goto L_0x006b
            if (r5 == 0) goto L_0x006b
            java.lang.String r0 = r5.A02
            if (r0 == 0) goto L_0x006b
            int r8 = r3.A0E()
            r3 = r16
            r7 = r20
            r6 = r19
            X.2E6 r2 = new X.2E6
            r2.<init>(r3, r4, r5, r6, r7, r8)
            X.2E4 r13 = r15.A0K
            r13.A00 = r2
            X.53c r11 = new X.53c
            r11.<init>()
            X.53i r12 = new X.53i
            r12.<init>()
            r14 = 3
            r8 = r18
            r8.A01(r9, r10, r11, r12, r13, r14)
            return
        L_0x006b:
            X.016 r1 = r4.A09
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r1.A0A(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C37041lD.A04(X.00j, X.1lE, X.1lG, java.lang.String, java.lang.String):void");
    }

    public void A05(BottomSheetDialogFragment bottomSheetDialogFragment, boolean z) {
        UserJid userJid = this.A0O;
        Context A01 = bottomSheetDialogFragment.A01();
        if (!z) {
            bottomSheetDialogFragment.A1B();
            return;
        }
        Intent A0M = C14960mK.A0M(A01, userJid, null, 14);
        A0M.addFlags(603979776);
        A01.startActivity(A0M);
    }
}
