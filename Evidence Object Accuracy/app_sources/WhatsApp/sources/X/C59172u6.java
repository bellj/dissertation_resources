package X;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/* renamed from: X.2u6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59172u6 extends AbstractC16930pz {
    public static final DateFormat A01;
    public final C30711Yn A00;

    static {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        A01 = simpleDateFormat;
    }

    public C59172u6(C30711Yn r1) {
        this.A00 = r1;
    }
}
