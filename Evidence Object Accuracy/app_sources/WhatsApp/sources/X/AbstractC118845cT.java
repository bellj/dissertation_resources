package X;

import android.view.View;
import android.widget.RadioButton;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;

/* renamed from: X.5cT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractC118845cT extends AnonymousClass03U {
    public final RadioButton A00;
    public final TextEmojiLabel A01;
    public final TextEmojiLabel A02;

    public AbstractC118845cT(View view) {
        super(view);
        this.A02 = C12970iu.A0T(view, R.id.payment_title);
        this.A01 = C12970iu.A0T(view, R.id.payment_subtitle);
        this.A00 = (RadioButton) AnonymousClass028.A0D(view, R.id.payment_radio_button);
    }

    public void A08(AbstractC128945wv r3) {
        this.A00.setChecked(r3.A00);
        C117295Zj.A0o(this.A0H, this, r3, 30);
    }
}
