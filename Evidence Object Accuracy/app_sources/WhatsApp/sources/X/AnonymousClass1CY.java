package X;

import android.os.Build;
import android.os.Environment;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.1CY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1CY {
    public final C14900mE A00;
    public final C15450nH A01;
    public final C15890o4 A02;
    public final AnonymousClass018 A03;
    public final C14950mJ A04;

    public AnonymousClass1CY(C14900mE r1, C15450nH r2, C15890o4 r3, AnonymousClass018 r4, C14950mJ r5) {
        this.A00 = r1;
        this.A01 = r2;
        this.A04 = r5;
        this.A03 = r4;
        this.A02 = r3;
    }

    public void A00() {
        int A02 = this.A01.A02(AbstractC15460nI.A28);
        this.A00.A0E(this.A03.A0I(new Object[]{Integer.valueOf(A02)}, R.plurals.video_status_truncation_info, (long) A02), 1);
    }

    public void A01(ActivityC13810kN r5) {
        String externalStorageState = Environment.getExternalStorageState();
        if (!"mounted".equals(externalStorageState) && !"mounted_ro".equals(externalStorageState)) {
            boolean A00 = C14950mJ.A00();
            int i = R.string.need_sd_card_shared_storage;
            if (A00) {
                i = R.string.need_sd_card;
            }
            r5.Ado(i);
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 23 || this.A02.A02("android.permission.READ_EXTERNAL_STORAGE") != -1) {
            boolean A002 = C14950mJ.A00();
            int i3 = R.string.gallery_media_not_exist_shared_storage;
            if (A002) {
                i3 = R.string.gallery_media_not_exist;
            }
            r5.Ado(i3);
            return;
        }
        int i4 = R.string.permission_storage_need_write_access_v30;
        if (i2 < 30) {
            i4 = R.string.permission_storage_need_write_access;
        }
        RequestPermissionActivity.A0K(r5, R.string.permission_storage_need_write_access_request, i4);
    }

    public void A02(ActivityC13810kN r6) {
        String externalStorageState = Environment.getExternalStorageState();
        if (!"mounted".equals(externalStorageState) && !"mounted_ro".equals(externalStorageState)) {
            C14900mE r2 = this.A00;
            boolean A00 = C14950mJ.A00();
            int i = R.string.need_sd_card_shared_storage;
            if (A00) {
                i = R.string.need_sd_card;
            }
            r2.A07(i, 1);
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 23 || this.A02.A02("android.permission.READ_EXTERNAL_STORAGE") != -1) {
            C14900mE r22 = this.A00;
            boolean A002 = C14950mJ.A00();
            int i3 = R.string.gallery_media_not_exist_shared_storage;
            if (A002) {
                i3 = R.string.gallery_media_not_exist;
            }
            r22.A07(i3, 1);
            return;
        }
        int i4 = R.string.permission_storage_need_write_access_v30;
        if (i2 < 30) {
            i4 = R.string.permission_storage_need_write_access;
        }
        RequestPermissionActivity.A0K(r6, R.string.permission_storage_need_write_access_request, i4);
    }
}
