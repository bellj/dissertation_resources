package X;

/* renamed from: X.0Rg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05860Rg {
    public int A00;
    public long A01;

    public C05860Rg(long j, int i) {
        this.A01 = j;
        this.A00 = i;
    }

    public static C05860Rg A00(String str, int i, int i2) {
        if (i < i2) {
            long j = 0;
            int i3 = i;
            while (i3 < i2) {
                char charAt = str.charAt(i3);
                if (charAt < '0' || charAt > '9') {
                    break;
                }
                j = (j * 10) + ((long) (charAt - '0'));
                if (j > 2147483647L) {
                    break;
                }
                i3++;
            }
            if (i3 != i) {
                return new C05860Rg(j, i3);
            }
        }
        return null;
    }
}
