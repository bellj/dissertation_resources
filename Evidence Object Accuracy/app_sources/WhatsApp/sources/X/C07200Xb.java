package X;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

/* renamed from: X.0Xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07200Xb implements AbstractC11680gg {
    public final /* synthetic */ C02460Cj A00;

    public C07200Xb(C02460Cj r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC11680gg
    public void A9E(Canvas canvas, Paint paint, RectF rectF, float f) {
        canvas.drawRoundRect(rectF, f, f, paint);
    }
}
