package X;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.view.View;

/* renamed from: X.2Zg  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2Zg extends InsetDrawable {
    public int A00;
    public final Drawable A01;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass2Zg(android.graphics.drawable.Drawable r3) {
        /*
            r2 = this;
            android.graphics.drawable.Drawable r1 = X.C015607k.A03(r3)
            r0 = 0
            r2.<init>(r1, r0)
            r2.A01 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2Zg.<init>(android.graphics.drawable.Drawable):void");
    }

    public static void A00(Drawable drawable, View view) {
        int paddingLeft = view.getPaddingLeft();
        int paddingRight = view.getPaddingRight();
        view.setBackground(drawable);
        view.setPadding(paddingLeft, 0, paddingRight, 0);
    }

    @Override // android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        Drawable drawable = this.A01;
        drawable.setBounds(bounds.left + 0, bounds.top + this.A00, bounds.right - 0, bounds.bottom - 0);
        drawable.draw(canvas);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTint(int i) {
        C015607k.A0A(this.A01, i);
    }

    @Override // android.graphics.drawable.Drawable, android.graphics.drawable.DrawableWrapper
    public void setTintList(ColorStateList colorStateList) {
        C015607k.A04(colorStateList, this.A01);
    }

    @Override // android.graphics.drawable.Drawable
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.A01;
        if (mode == null) {
            mode = PorterDuff.Mode.SRC_IN;
        }
        C015607k.A07(mode, drawable);
    }
}
