package X;

import com.facebook.redex.RunnableBRunnable0Shape4S0100000_I0_4;

/* renamed from: X.0q7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17010q7 {
    public final C15570nT A00;
    public final C15550nR A01;
    public final AnonymousClass118 A02;
    public final C20730wE A03;
    public final C20840wP A04;
    public final C14830m7 A05;
    public final C14850m9 A06;
    public final AbstractC14440lR A07;

    public C17010q7(C15570nT r1, C15550nR r2, AnonymousClass118 r3, C20730wE r4, C20840wP r5, C14830m7 r6, C14850m9 r7, AbstractC14440lR r8) {
        this.A05 = r6;
        this.A06 = r7;
        this.A00 = r1;
        this.A07 = r8;
        this.A01 = r2;
        this.A03 = r4;
        this.A04 = r5;
        this.A02 = r3;
    }

    public void A00() {
        this.A00.A08();
        int A02 = this.A06.A02(1144);
        if (A02 != -1 && this.A05.A00() - this.A04.A01().getLong("last_out_contact_sync_time", -1) > ((long) A02) * 60000) {
            this.A07.Ab4(new RunnableBRunnable0Shape4S0100000_I0_4(this, 30), "ContactDiscoverySyncHelper/syncOutContact");
        }
    }
}
