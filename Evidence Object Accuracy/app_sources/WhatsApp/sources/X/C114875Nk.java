package X;

/* renamed from: X.5Nk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114875Nk extends AnonymousClass5NT {
    public C114875Nk(AnonymousClass5NT r2) {
        super(AnonymousClass1T7.A02(r2.A00));
    }

    @Override // X.AnonymousClass5NT
    public String toString() {
        return C12960it.A0d(AnonymousClass1T7.A02(this.A00), C12960it.A0k("VerisignCzagExtension: "));
    }
}
