package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.google.common.base.Strings;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.3mz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77253mz extends AbstractC76783mE {
    public static final Pattern A05 = Pattern.compile("(?:(\\d+):)?(\\d+):(\\d+)[:.](\\d+)");
    public float A00;
    public float A01;
    public Map A02;
    public final C93684aZ A03;
    public final boolean A04;

    public C77253mz() {
        this(null);
    }

    public C77253mz(List list) {
        super("SsaDecoder");
        this.A01 = -3.4028235E38f;
        this.A00 = -3.4028235E38f;
        if (list == null || list.isEmpty()) {
            this.A04 = false;
            this.A03 = null;
            return;
        }
        this.A04 = true;
        String str = new String(C72463ee.A0b(list, 0), C88814Hf.A05);
        C95314dV.A03(str.startsWith("Format:"));
        this.A03 = C93684aZ.A00(str);
        A08(new C95304dT(C72463ee.A0b(list, 1)));
    }

    public static int A01(long j) {
        int i = (int) j;
        if (C12960it.A1T((((long) i) > j ? 1 : (((long) i) == j ? 0 : -1)))) {
            return i;
        }
        throw C12970iu.A0f(Strings.A00("Out of range: %s", Long.valueOf(j)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        r4.add(r3, java.lang.Long.valueOf(r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        if (r3 != 0) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        r0 = X.C12960it.A0l();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        r5.add(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        r0 = X.C12980iv.A0x((java.util.Collection) r5.get(r3 - 1));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int A02(java.util.List r4, java.util.List r5, long r6) {
        /*
            int r3 = r4.size()
        L_0x0004:
            int r3 = r3 + -1
            if (r3 < 0) goto L_0x0038
            long r1 = X.C72453ed.A0Z(r4, r3)
            int r0 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x002a
            long r1 = X.C72453ed.A0Z(r4, r3)
            int r0 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x0004
            int r3 = r3 + 1
        L_0x001a:
            java.lang.Long r0 = java.lang.Long.valueOf(r6)
            r4.add(r3, r0)
            if (r3 != 0) goto L_0x002b
            java.util.ArrayList r0 = X.C12960it.A0l()
        L_0x0027:
            r5.add(r3, r0)
        L_0x002a:
            return r3
        L_0x002b:
            int r0 = r3 + -1
            java.lang.Object r0 = r5.get(r0)
            java.util.Collection r0 = (java.util.Collection) r0
            java.util.ArrayList r0 = X.C12980iv.A0x(r0)
            goto L_0x0027
        L_0x0038:
            r3 = 0
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77253mz.A02(java.util.List, java.util.List, long):int");
    }

    public static long A03(String str) {
        Matcher matcher = A05.matcher(str.trim());
        if (!matcher.matches()) {
            return -9223372036854775807L;
        }
        return (AbstractC76783mE.A00(matcher, 1) * 60 * 60 * SearchActionVerificationClientService.MS_TO_NS) + (AbstractC76783mE.A00(matcher, 2) * 60 * SearchActionVerificationClientService.MS_TO_NS) + (AbstractC76783mE.A00(matcher, 3) * SearchActionVerificationClientService.MS_TO_NS) + (AbstractC76783mE.A00(matcher, 4) * 10000);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(11:30|117|31|(2:(1:34)|35)(6:(1:37)|119|39|141|(3:140|43|145)|142)|38|119|39|141|(1:143)(5:138|41|140|43|145)|142|28) */
    @Override // X.AbstractC76783mE
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC116805Wy A07(byte[] r24, int r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 718
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77253mz.A07(byte[], int, boolean):X.5Wy");
    }

    /* JADX WARNING: Removed duplicated region for block: B:78:0x01ac A[Catch: RuntimeException -> 0x01d8, TryCatch #0 {RuntimeException -> 0x01d8, blocks: (B:51:0x00f9, B:53:0x0106, B:54:0x0110, B:56:0x0114, B:58:0x011e, B:60:0x0128, B:61:0x0132, B:65:0x0141, B:67:0x0146, B:68:0x0159, B:69:0x0183, B:71:0x0187, B:72:0x018d, B:74:0x0193, B:76:0x01a8, B:78:0x01ac, B:79:0x01b6, B:81:0x01ba, B:82:0x01c4), top: B:109:0x00f9, inners: #1, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01ba A[Catch: RuntimeException -> 0x01d8, TryCatch #0 {RuntimeException -> 0x01d8, blocks: (B:51:0x00f9, B:53:0x0106, B:54:0x0110, B:56:0x0114, B:58:0x011e, B:60:0x0128, B:61:0x0132, B:65:0x0141, B:67:0x0146, B:68:0x0159, B:69:0x0183, B:71:0x0187, B:72:0x018d, B:74:0x0193, B:76:0x01a8, B:78:0x01ac, B:79:0x01b6, B:81:0x01ba, B:82:0x01c4), top: B:109:0x00f9, inners: #1, #3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A08(X.C95304dT r26) {
        /*
        // Method dump skipped, instructions count: 636
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C77253mz.A08(X.4dT):void");
    }
}
