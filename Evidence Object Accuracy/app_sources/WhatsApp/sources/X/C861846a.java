package X;

/* renamed from: X.46a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C861846a extends AbstractC89694Ky {
    public String A00 = "";

    public C861846a(int i) {
        super(i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C861846a)) {
            return false;
        }
        return C29941Vi.A00(this.A00, ((C861846a) obj).A00);
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        C12960it.A1O(A1a, super.A00);
        return C12960it.A06(this.A00, A1a);
    }
}
