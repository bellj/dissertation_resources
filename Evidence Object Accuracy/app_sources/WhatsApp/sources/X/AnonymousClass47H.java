package X;

/* renamed from: X.47H  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47H extends AnonymousClass4OU {
    public final int A00;

    public AnonymousClass47H(int i, String str, int i2) {
        super(i, str);
        this.A00 = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.A00 == ((AnonymousClass47H) obj).A00) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.A00;
    }
}
