package X;

/* renamed from: X.1f8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C33821f8 extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public Boolean A02;
    public Boolean A03;
    public Boolean A04;
    public Boolean A05;
    public Boolean A06;
    public Boolean A07;
    public Boolean A08;
    public Boolean A09;
    public Boolean A0A;
    public Double A0B;
    public Integer A0C;
    public Integer A0D;
    public Integer A0E;
    public Integer A0F;
    public Integer A0G;
    public Integer A0H;
    public Integer A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;
    public Long A0M;
    public Long A0N;
    public Long A0O;
    public Long A0P;
    public Long A0Q;

    public C33821f8() {
        super(854, new AnonymousClass00E(1, 5, 5), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(31, this.A0J);
        r3.Abe(25, this.A0C);
        r3.Abe(30, this.A0D);
        r3.Abe(23, this.A00);
        r3.Abe(21, this.A0K);
        r3.Abe(35, this.A01);
        r3.Abe(22, this.A02);
        r3.Abe(8, this.A03);
        r3.Abe(4, this.A04);
        r3.Abe(7, this.A05);
        r3.Abe(29, this.A06);
        r3.Abe(24, this.A07);
        r3.Abe(3, this.A0E);
        r3.Abe(1, this.A0F);
        r3.Abe(17, this.A08);
        r3.Abe(11, this.A0L);
        r3.Abe(2, this.A0G);
        r3.Abe(32, this.A0M);
        r3.Abe(28, this.A0N);
        r3.Abe(16, this.A0O);
        r3.Abe(33, this.A0P);
        r3.Abe(34, this.A0H);
        r3.Abe(27, this.A0Q);
        r3.Abe(38, this.A09);
        r3.Abe(18, this.A0A);
        r3.Abe(20, this.A0B);
        r3.Abe(36, this.A0I);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        String obj3;
        String obj4;
        String obj5;
        String obj6;
        String obj7;
        StringBuilder sb = new StringBuilder("WamMessageSend {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceCount", this.A0J);
        Integer num = this.A0C;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "deviceSizeBucket", obj);
        Integer num2 = this.A0D;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "disappearingChatInitiator", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "e2eBackfill", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "ephemeralityDuration", this.A0K);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isAReply", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "isViewOnce", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "mediaCaptionPresent", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsForward", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsInternational", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsInvisible", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageIsRevoke", this.A07);
        Integer num3 = this.A0E;
        if (num3 == null) {
            obj3 = null;
        } else {
            obj3 = num3.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageMediaType", obj3);
        Integer num4 = this.A0F;
        if (num4 == null) {
            obj4 = null;
        } else {
            obj4 = num4.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageSendResult", obj4);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageSendResultIsTerminal", this.A08);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageSendT", this.A0L);
        Integer num5 = this.A0G;
        if (num5 == null) {
            obj5 = null;
        } else {
            obj5 = num5.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "messageType", obj5);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "participantCount", this.A0M);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "receiverDefaultDisappearingDuration", this.A0N);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "resendCount", this.A0O);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "revokeDuration", this.A0P);
        Integer num6 = this.A0H;
        if (num6 == null) {
            obj6 = null;
        } else {
            obj6 = num6.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "revokeType", obj6);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "senderDefaultDisappearingDuration", this.A0Q);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsAvatar", this.A09);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "stickerIsFirstParty", this.A0A);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "thumbSize", this.A0B);
        Integer num7 = this.A0I;
        if (num7 == null) {
            obj7 = null;
        } else {
            obj7 = num7.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "typeOfGroup", obj7);
        sb.append("}");
        return sb.toString();
    }
}
