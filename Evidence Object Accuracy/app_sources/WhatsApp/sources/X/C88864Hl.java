package X;

import java.util.concurrent.TimeUnit;

/* renamed from: X.4Hl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88864Hl {
    public static AnonymousClass4ZL A00 = AnonymousClass4ZL.A00;
    public static final int A01;
    public static final int A02;
    public static final long A03 = TimeUnit.SECONDS.toNanos(C88274Ey.A00("kotlinx.coroutines.scheduler.keep.alive.sec", 60, 1, Long.MAX_VALUE));
    public static final long A04 = C88274Ey.A00("kotlinx.coroutines.scheduler.resolution.ns", 100000, 1, Long.MAX_VALUE);
    public static final C89714La A05 = new C89714La(1);
    public static final C89714La A06 = new C89714La(0);

    static {
        int i = C88594Gg.A00;
        if (i < 2) {
            i = 2;
        }
        long j = (long) 1;
        A01 = (int) C88274Ey.A00("kotlinx.coroutines.scheduler.core.pool.size", (long) i, j, (long) Integer.MAX_VALUE);
        long j2 = (long) 2097150;
        A02 = (int) C88274Ey.A00("kotlinx.coroutines.scheduler.max.pool.size", j2, j, j2);
    }
}
