package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.23e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C457923e extends LinkedHashMap<AnonymousClass1IS, Long> {
    public final /* synthetic */ AnonymousClass1BD this$0;

    public C457923e(AnonymousClass1BD r1) {
        this.this$0 = r1;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.util.Map$Entry] */
    @Override // java.util.LinkedHashMap
    public boolean removeEldestEntry(Map.Entry<AnonymousClass1IS, Long> entry) {
        return ((long) size()) > 240;
    }
}
