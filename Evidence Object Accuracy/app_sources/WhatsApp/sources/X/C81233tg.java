package X;

import java.io.Serializable;

/* renamed from: X.3tg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C81233tg extends AbstractC112285Cu implements Serializable {
    public static final C81233tg INSTANCE = new C81233tg();
    public static final long serialVersionUID = 0;

    @Override // java.lang.Object
    public String toString() {
        return "Ordering.natural().reverse()";
    }

    public int compare(Comparable comparable, Comparable comparable2) {
        if (comparable == comparable2) {
            return 0;
        }
        return comparable2.compareTo(comparable);
    }

    private Object readResolve() {
        return INSTANCE;
    }

    @Override // X.AbstractC112285Cu
    public AbstractC112285Cu reverse() {
        return AbstractC112285Cu.natural();
    }
}
