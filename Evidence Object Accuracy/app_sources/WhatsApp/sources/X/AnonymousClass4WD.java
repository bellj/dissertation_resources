package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/* renamed from: X.4WD  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4WD {
    public Bitmap A00;
    public Drawable A01;
    public C90534Og A02;
    public AnonymousClass4BK A03;
    public AnonymousClass4BH A04;
    public C92744Xg A05;
    public EnumC87284Ax A06;
    public boolean A07;
    public boolean A08;
    public final int A09 = AnonymousClass4GW.A00;
    public final Context A0A;
    public final Path A0B = new Path();
    public final Path A0C = new Path();
    public final Path A0D = new Path();
    public final PorterDuffXfermode A0E = new PorterDuffXfermode(PorterDuff.Mode.DST_OUT);
    public final PorterDuffXfermode A0F = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
    public final Rect A0G = new Rect();
    public final RectF A0H = new RectF();
    public final RectF A0I = new RectF();
    public final RectF A0J = new RectF();
    public final RectF A0K = new RectF();
    public final AnonymousClass018 A0L;
    public final C106174vH A0M;
    public final AbstractC16710pd A0N = new AnonymousClass1WL(new AnonymousClass5J7());
    public final AbstractC16710pd A0O = new AnonymousClass1WL(new AnonymousClass5J8());
    public final AbstractC16710pd A0P = new AnonymousClass1WL(new AnonymousClass5J9());
    public final AbstractC16710pd A0Q = new AnonymousClass1WL(new C113945Jp(this));

    public /* synthetic */ AnonymousClass4WD(Context context, AnonymousClass018 r4, C106174vH r5, AnonymousClass4BK r6, AnonymousClass4BH r7, boolean z) {
        this.A0A = context;
        this.A0L = r4;
        this.A08 = z;
        this.A04 = r7;
        this.A03 = r6;
        this.A0M = r5;
        this.A05 = C93014Yp.A01(this.A04).A00(context);
        this.A06 = EnumC87284Ax.A01;
    }

    public final void A00() {
        AbstractC16710pd r2 = this.A0Q;
        ((Paint) r2.getValue()).setStrokeWidth(this.A05.A01);
        ((Paint) r2.getValue()).setColor(AnonymousClass00T.A00(this.A0A, this.A06.statusColor));
    }
}
