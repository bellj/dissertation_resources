package X;

/* renamed from: X.5Mh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114585Mh extends AnonymousClass1TM {
    public AnonymousClass5NF A00 = AnonymousClass5NF.A01;
    public AnonymousClass5NG A01;

    public static C114585Mh A00(Object obj) {
        if (obj instanceof C114585Mh) {
            return (C114585Mh) obj;
        }
        if (obj != null) {
            return new C114585Mh(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    public C114585Mh(AbstractC114775Na r4) {
        if (r4.A0B() == 0) {
            this.A00 = null;
            return;
        }
        if (r4.A0D(0) instanceof AnonymousClass5NF) {
            this.A00 = AnonymousClass5NF.A00(r4.A0D(0));
        } else {
            this.A00 = null;
            this.A01 = AnonymousClass5NG.A00(r4.A0D(0));
        }
        if (r4.A0B() <= 1) {
            return;
        }
        if (this.A00 != null) {
            this.A01 = AnonymousClass5NG.A00(r4.A0D(1));
            return;
        }
        throw C12970iu.A0f("wrong sequence in constructor");
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co A00 = C94954co.A00();
        AnonymousClass5NF r0 = this.A00;
        if (r0 != null) {
            A00.A06(r0);
        }
        AnonymousClass5NG r02 = this.A01;
        if (r02 != null) {
            A00.A06(r02);
        }
        return new AnonymousClass5NZ(A00);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000f, code lost:
        if (r1 == 0) goto L_0x0011;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r4 = this;
            X.5NG r3 = r4.A01
            java.lang.String r0 = "BasicConstraints: isCa("
            java.lang.StringBuilder r2 = X.C12960it.A0j(r0)
            X.5NF r0 = r4.A00
            if (r0 == 0) goto L_0x0011
            byte r1 = r0.A00
            r0 = 1
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r3 != 0) goto L_0x0021
            r2.append(r0)
            java.lang.String r0 = ")"
            r2.append(r0)
        L_0x001c:
            java.lang.String r0 = r2.toString()
            return r0
        L_0x0021:
            r2.append(r0)
            java.lang.String r0 = "), pathLenConstraint = "
            r2.append(r0)
            byte[] r1 = r3.A01
            java.math.BigInteger r0 = new java.math.BigInteger
            r0.<init>(r1)
            r2.append(r0)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C114585Mh.toString():java.lang.String");
    }
}
