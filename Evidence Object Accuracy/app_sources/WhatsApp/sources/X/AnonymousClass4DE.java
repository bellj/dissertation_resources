package X;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.4DE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4DE {
    public static List A00(byte[] bArr) {
        int i = (bArr[11] & 255) << 8;
        ArrayList A0w = C12980iv.A0w(3);
        A0w.add(bArr);
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.nativeOrder());
        order.putLong((((long) ((bArr[10] & 255) | i)) * 1000000000) / 48000);
        A0w.add(order.array());
        ByteBuffer order2 = ByteBuffer.allocate(8).order(ByteOrder.nativeOrder());
        order2.putLong(80000000);
        A0w.add(order2.array());
        return A0w;
    }
}
