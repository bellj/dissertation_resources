package X;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.0uI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19550uI {
    public final AnonymousClass01H A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;

    public C19550uI(AnonymousClass01H r1, AnonymousClass01H r2, AnonymousClass01H r3) {
        this.A02 = r2;
        this.A01 = r3;
        this.A00 = r1;
    }

    public C64063Ec A00(C16820po r15) {
        String str;
        Long l;
        Map A03 = ((AnonymousClass17I) this.A00.get()).A03();
        if (!A03.isEmpty() && (str = (String) A03.get(r15.A00)) != null && !str.isEmpty()) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                long j = jSONObject.getLong("fbid");
                String string = jSONObject.getString("password");
                String string2 = jSONObject.getString("access_token");
                long j2 = jSONObject.getLong("timestamp");
                String str2 = null;
                if (jSONObject.has("ttl")) {
                    l = Long.valueOf(jSONObject.optLong("ttl"));
                } else {
                    l = null;
                }
                if (jSONObject.has("analytics_claim")) {
                    str2 = jSONObject.optString("analytics_claim");
                }
                return new C64063Ec(new C16820po(jSONObject.getString("usertype")), l, string, string2, str2, j, j2);
            } catch (JSONException e) {
                AnonymousClass009.A0D(e);
            }
        }
        return null;
    }

    public void A01(C64063Ec r9, AbstractC116695Wl r10, AnonymousClass4VZ r11) {
        ((AnonymousClass17H) this.A02.get()).A01(new AnonymousClass303(r9, r10, r10, this, r11), r9.A01);
    }
}
