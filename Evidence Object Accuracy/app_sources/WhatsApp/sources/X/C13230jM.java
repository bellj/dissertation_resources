package X;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: X.0jM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C13230jM implements AbstractC13240jN {
    public static AtomicReference A00 = new AtomicReference();

    @Override // X.AbstractC13240jN
    public void AMq(boolean z) {
        synchronized (C13030j1.A09) {
            Iterator it = new ArrayList(C13030j1.A0A.values()).iterator();
            while (it.hasNext()) {
                C13030j1 r2 = (C13030j1) it.next();
                if (r2.A07.get()) {
                    Log.d("FirebaseApp", "Notifying background state change listeners.");
                    Iterator it2 = r2.A05.iterator();
                    if (it2.hasNext()) {
                        it2.next();
                        throw new NullPointerException("onBackgroundStateChanged");
                    }
                }
            }
        }
    }
}
