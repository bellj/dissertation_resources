package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.2ro  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58852ro extends AbstractC84023yH {
    public final /* synthetic */ C65113Ie A00;
    public final /* synthetic */ C64473Fr A01;
    public final /* synthetic */ C64413Fl A02;

    public C58852ro(C65113Ie r1, C64473Fr r2, C64413Fl r3) {
        this.A01 = r2;
        this.A00 = r1;
        this.A02 = r3;
    }

    @Override // X.AbstractC84023yH, X.AnonymousClass4UU
    public /* bridge */ /* synthetic */ Object A00(int i) {
        C68133Uf r2 = new AnonymousClass5TJ() { // from class: X.3Uf
            @Override // X.AnonymousClass5TJ
            public final void AOt(long j) {
                C64473Fr r5 = C58852ro.this.A01;
                AtomicLong atomicLong = r5.A0k;
                atomicLong.addAndGet(j);
                if (j >= 10240) {
                    r5.A0J.A08(atomicLong.get(), r5.A0i.get());
                }
            }
        };
        C64473Fr r0 = this.A01;
        C44791zY r1 = r0.A0P;
        C65113Ie r4 = this.A00;
        C64413Fl r5 = this.A02;
        AnonymousClass3HV A05 = r1.A05(r2, r0.A0M, r4, r5, i);
        r5.A02();
        return A05;
    }
}
