package X;

import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.Provider;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.PSSParameterSpec;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.4dj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95444dj {
    public static final Map A00;
    public static final AnonymousClass5N9 A01 = AnonymousClass5ME.A00;

    static {
        HashMap A11 = C12970iu.A11();
        A00 = A11;
        A11.put(AbstractC116895Xi.A00, "Ed25519");
        A11.put(AbstractC116895Xi.A01, "Ed448");
        A11.put(AnonymousClass1TW.A05, "SHA1withDSA");
        A11.put(AbstractC72433ea.A0g, "SHA1withDSA");
    }

    public static String A00(AnonymousClass1TK r4) {
        String str = (String) C88614Gj.A00.get(r4);
        if (str == null) {
            str = r4.A01;
        }
        int indexOf = str.indexOf(45);
        if (indexOf <= 0 || str.startsWith("SHA3")) {
            return str;
        }
        StringBuilder A0h = C12960it.A0h();
        C72453ed.A1O(str, A0h, 0, indexOf);
        return C12960it.A0d(str.substring(indexOf + 1), A0h);
    }

    public static String A01(C114725Mv r5) {
        AnonymousClass5N9 r1;
        StringBuilder A0h;
        String str;
        C114655Mo r2;
        AnonymousClass1TN r22 = r5.A00;
        if (!(r22 == null || (r1 = A01) == r22 || r1.A0A(r22.Aer()))) {
            AnonymousClass1TK r12 = r5.A01;
            if (r12.A04(AnonymousClass1TJ.A0I)) {
                if (r22 instanceof C114655Mo) {
                    r2 = (C114655Mo) r22;
                } else {
                    r2 = new C114655Mo(AbstractC114775Na.A04(r22));
                }
                A0h = C12960it.A0h();
                A0h.append(A00(r2.A02.A01));
                str = "withRSAandMGF1";
            } else if (r12.A04(AbstractC72433ea.A0Y)) {
                AbstractC114775Na A04 = AbstractC114775Na.A04(r22);
                A0h = C12960it.A0h();
                A0h.append(A00((AnonymousClass1TK) AbstractC114775Na.A00(A04)));
                str = "withECDSA";
            }
            return C12960it.A0d(str, A0h);
        }
        Map map = A00;
        AnonymousClass1TK r52 = r5.A01;
        String str2 = (String) map.get(r52);
        if (str2 != null) {
            return str2;
        }
        Provider provider = Security.getProvider("SC");
        if (provider != null) {
            String property = provider.getProperty(C12960it.A0b("Alg.Alias.Signature.", r52));
            if (property != null) {
                return property;
            }
            String property2 = provider.getProperty(C12960it.A0b("Alg.Alias.Signature.OID.", r52));
            if (property2 != null) {
                return property2;
            }
        }
        Provider[] providers = Security.getProviders();
        for (int i = 0; i != providers.length; i++) {
            if (provider != providers[i]) {
                Provider provider2 = providers[i];
                String property3 = provider2.getProperty(C12960it.A0b("Alg.Alias.Signature.", r52));
                if (property3 != null) {
                    return property3;
                }
                String property4 = provider2.getProperty(C12960it.A0b("Alg.Alias.Signature.OID.", r52));
                if (property4 != null) {
                    return property4;
                }
            }
        }
        return r52.A01;
    }

    public static void A02(String str, StringBuffer stringBuffer, byte[] bArr) {
        int length = bArr.length;
        stringBuffer.append("            Signature: ");
        if (length > 20) {
            stringBuffer.append(C72463ee.A0I(bArr, 20));
            stringBuffer.append(str);
            int i = 20;
            while (i < length) {
                int i2 = length - 20;
                stringBuffer.append("                       ");
                stringBuffer.append(AnonymousClass1T7.A02(i < i2 ? C95374db.A02(bArr, i, 20) : C95374db.A02(bArr, i, length - i)));
                stringBuffer.append(str);
                i += 20;
            }
            return;
        }
        stringBuffer.append(C72463ee.A0I(bArr, length));
        stringBuffer.append(str);
    }

    public static void A03(Signature signature, AnonymousClass1TN r4) {
        AnonymousClass5N9 r1;
        if (r4 != null && (r1 = A01) != r4 && !r1.A0A(r4.Aer())) {
            AlgorithmParameters instance = AlgorithmParameters.getInstance(signature.getAlgorithm(), signature.getProvider());
            try {
                instance.init(r4.Aer().A01());
                if (signature.getAlgorithm().endsWith("MGF1")) {
                    try {
                        signature.setParameter(instance.getParameterSpec(PSSParameterSpec.class));
                    } catch (GeneralSecurityException e) {
                        throw new SignatureException(C12960it.A0d(e.getMessage(), C12960it.A0k("Exception extracting parameters: ")));
                    }
                }
            } catch (IOException e2) {
                throw new SignatureException(C12960it.A0d(e2.getMessage(), C12960it.A0k("IOException decoding parameters: ")));
            }
        }
    }
}
