package X;

import java.io.File;

/* renamed from: X.0xG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21340xG {
    public final C21330xF A00;
    public final C16590pI A01;

    public C21340xG(C21330xF r1, C16590pI r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public File A00() {
        File file = new File(this.A01.A00.getFilesDir(), "biz_directory");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, "directory_recent_search_history");
    }
}
