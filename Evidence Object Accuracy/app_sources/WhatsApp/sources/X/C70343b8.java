package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.3b8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70343b8 implements AbstractC41521tf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass35U A01;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70343b8(AnonymousClass35U r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r5) {
        AnonymousClass35U r1 = this.A01;
        r1.A05.A05(bitmap);
        r1.A02 = true;
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        AnonymousClass35U r2 = this.A01;
        PhotoView photoView = r2.A05;
        photoView.A0J = null;
        photoView.A04 = 0.0f;
        r2.A02 = false;
    }
}
