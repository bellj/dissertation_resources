package X;

/* renamed from: X.5Ij  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113635Ij extends AbstractC112665Eg {
    public AbstractC113635Ij(AnonymousClass5WO r3) {
        super(r3);
        if (r3.ABi() != C112775Er.A00) {
            throw C12970iu.A0f("Coroutines with restricted suspension must have EmptyCoroutineContext");
        }
    }

    @Override // X.AnonymousClass5WO
    public AnonymousClass5X4 ABi() {
        return C112775Er.A00;
    }
}
