package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* renamed from: X.1cj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C32781cj {
    public static final long A00 = new GregorianCalendar(2022, 3, 12).getTimeInMillis();
    public static final long A01 = new GregorianCalendar(2022, 2, 15).getTimeInMillis();
    public static final byte[] A02 = {0, 1};
    public static final byte[] A03 = {0, 1};
    public static final byte[] A04 = {0, 2};

    public static int A00(int i) {
        if (i == 0) {
            return 0;
        }
        if (i != 1) {
            if (i == 2) {
                return 7;
            }
            if (i != 3) {
                StringBuilder sb = new StringBuilder("Unexpected backup result value: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            }
        }
        return 6;
    }

    public static int A01(String str, String str2) {
        if (str2.equals(str)) {
            return 0;
        }
        if (str.endsWith(".crypt")) {
            return 1;
        }
        String[] split = str.split(".crypt");
        if (split.length != 2) {
            StringBuilder sb = new StringBuilder();
            sb.append("msgstore/get-version/unexpected-filename ");
            sb.append(str);
            Log.e(sb.toString());
            return -1;
        }
        try {
            return Integer.parseInt(split[1]);
        } catch (NumberFormatException e) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("msgstore/get-version/unexpected-filename ");
            sb2.append(str);
            Log.e(sb2.toString(), e);
            return -1;
        }
    }

    public static long A02(C14850m9 r7, boolean z) {
        if (z) {
            return -1;
        }
        Calendar instance = Calendar.getInstance();
        instance.set(14, 0);
        instance.set(13, 0);
        instance.set(12, 0);
        instance.set(11, r7.A02(954) + 2);
        return Math.max(instance.getTimeInMillis(), System.currentTimeMillis() + TimeUnit.MINUTES.toMillis((long) r7.A02(955)));
    }

    public static C32821cn A03(InputStream inputStream) {
        int length = A02.length;
        byte[] bArr = new byte[length];
        byte[] bArr2 = new byte[32];
        byte[] bArr3 = new byte[16];
        byte[] bArr4 = new byte[16];
        byte[] bArr5 = new byte[length + 1 + 32 + 16 + 16];
        inputStream.read(bArr5);
        System.arraycopy(bArr5, 0, bArr, 0, length);
        int i = length + 0;
        if (Arrays.equals(bArr, A04) || Arrays.equals(bArr, A03)) {
            int i2 = i + 1;
            String valueOf = String.valueOf((int) bArr5[i]);
            byte[][] bArr6 = {bArr2, bArr3, bArr4};
            int i3 = 0;
            do {
                byte[] bArr7 = bArr6[i3];
                System.arraycopy(bArr5, i2, bArr7, 0, bArr7.length);
                i2 += bArr7.length;
                i3++;
            } while (i3 < 3);
            return new C32821cn(valueOf, bArr, bArr2, bArr3, bArr4);
        }
        throw new AnonymousClass03G();
    }

    public static C32821cn A04(InputStream inputStream) {
        EnumC87184An r1;
        C44641zJ r2 = (C44641zJ) AbstractC27091Fz.A0D(C44641zJ.A05, inputStream);
        if (r2 == null) {
            return null;
        }
        int i = r2.A01;
        if (i == 0 || i != 1) {
            r1 = EnumC87184An.A02;
        } else {
            r1 = EnumC87184An.A01;
        }
        if (r1 != EnumC87184An.A02) {
            return null;
        }
        C57482n8 r12 = r2.A04;
        if (r12 == null) {
            r12 = C57482n8.A06;
        }
        return new C32821cn(r12.A05, r12.A01.A04(), r12.A04.A04(), r12.A03.A04(), r12.A02.A04());
    }

    public static C44541z8 A05(Context context) {
        File file = new File(context.getFilesDir(), "key");
        if (!file.exists()) {
            Log.w("backupkey/getinfo/does-not-exist");
        } else {
            byte[] A0G = C003501n.A0G(file);
            if (A0G != null) {
                int length = A0G.length;
                byte[] bArr = A02;
                int length2 = bArr.length;
                if (length >= length2 + 1 + 32 + 16 + 32 + 16 + 32) {
                    byte[] bArr2 = new byte[length2];
                    System.arraycopy(A0G, 0, bArr2, 0, length2);
                    int i = length2 + 0;
                    if (Arrays.equals(bArr2, bArr)) {
                        int i2 = i + 1;
                        String valueOf = String.valueOf((int) A0G[i]);
                        byte[] bArr3 = new byte[32];
                        System.arraycopy(A0G, i2, bArr3, 0, 32);
                        int i3 = i2 + 32;
                        byte[] bArr4 = new byte[16];
                        System.arraycopy(A0G, i3, bArr4, 0, 16);
                        int i4 = i3 + 16;
                        byte[] bArr5 = new byte[32];
                        System.arraycopy(A0G, i4, bArr5, 0, 32);
                        byte[] A0D = C003501n.A0D(16);
                        byte[] bArr6 = new byte[32];
                        System.arraycopy(A0G, i4 + 32 + 16, bArr6, 0, 32);
                        return new C44541z8(valueOf, bArr2, bArr3, bArr4, bArr5, A0D, bArr6);
                    }
                    throw new AnonymousClass03G();
                }
                StringBuilder sb = new StringBuilder();
                sb.append(file);
                sb.append(" size mismatch");
                throw new InvalidParameterException(sb.toString());
            }
        }
        return null;
    }

    public static ArrayList A06(File file, List list) {
        File[] listFiles;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        ArrayList arrayList = new ArrayList();
        String pattern = simpleDateFormat.toPattern();
        String A012 = C004502a.A01(file.getName());
        String A022 = C004502a.A02(file.getName(), "");
        int size = list.size();
        String[] strArr = new String[size];
        for (int i = 0; i < size; i++) {
            strArr[i] = C004502a.A02(file.getName(), (String) list.get(i));
        }
        File parentFile = file.getParentFile();
        if (!(parentFile == null || (listFiles = parentFile.listFiles()) == null)) {
            for (File file2 : listFiles) {
                String name = file2.getName();
                if (name.startsWith(A012)) {
                    boolean endsWith = name.endsWith(A022);
                    int i2 = 0;
                    while (true) {
                        if (i2 < size) {
                            String str = strArr[i2];
                            if (endsWith) {
                                break;
                            }
                            endsWith = name.endsWith(str);
                            i2++;
                        } else if (!endsWith) {
                        }
                    }
                    if (name.length() > A012.length() + pattern.length()) {
                        arrayList.add(file2);
                    }
                }
            }
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            StringBuilder sb = new StringBuilder();
            sb.append(file.getPath());
            sb.append((String) it.next());
            File file3 = new File(sb.toString());
            if (file3.exists()) {
                arrayList.add(file3);
            }
        }
        Collections.sort(arrayList, new C112255Cr(A012, simpleDateFormat));
        return arrayList;
    }

    public static List A07(EnumC16570pG r5, EnumC16570pG r6) {
        if (r5.version <= r6.version) {
            EnumC16570pG[] A042 = EnumC16570pG.A04(r5, r6);
            int length = A042.length;
            ArrayList arrayList = new ArrayList(length);
            for (EnumC16570pG r2 : A042) {
                StringBuilder sb = new StringBuilder(".crypt");
                sb.append(r2.version);
                arrayList.add(sb.toString());
            }
            return arrayList;
        }
        StringBuilder sb2 = new StringBuilder("msgstore/get-db-crypt-extension-range/illegal-range [");
        sb2.append(r5);
        sb2.append(", ");
        sb2.append(r6);
        sb2.append(")");
        throw new IllegalArgumentException(sb2.toString());
    }

    public static void A08(Context context) {
        new File(context.getFilesDir(), "key").delete();
    }

    public static void A09(Context context, String str, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        StringBuilder sb = new StringBuilder("backupkey/set/v=");
        sb.append(str);
        Log.i(sb.toString());
        Arrays.toString(bArr);
        Arrays.toString(bArr2);
        File file = new File(context.getFilesDir(), "key");
        byte[] bArr5 = new byte[16];
        Arrays.toString(bArr5);
        byte[] bArr6 = {Byte.parseByte(str)};
        byte[] bArr7 = A02;
        byte[] bArr8 = new byte[bArr7.length + 1 + bArr.length + bArr4.length + bArr3.length + 16 + bArr2.length];
        byte[][] bArr9 = {bArr7, bArr6, bArr, bArr4, bArr3, bArr5, bArr2};
        int i = 0;
        int i2 = 0;
        do {
            byte[] bArr10 = bArr9[i];
            System.arraycopy(bArr10, 0, bArr8, i2, bArr10.length);
            i2 += bArr10.length;
            i++;
        } while (i < 7);
        Arrays.toString(bArr8);
        try {
            C003501n.A08(file, bArr8);
            byte[] A0G = C003501n.A0G(file);
            A05(context);
            Arrays.equals(bArr8, A0G);
        } catch (Exception e) {
            Log.e("backupkey/set/unable-to-write ", e);
        }
    }

    public static void A0A(AnonymousClass03D r3, AbstractC14590lg r4, InputStream inputStream, OutputStream outputStream, long j) {
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read < 0) {
                r4.accept(Long.valueOf(j));
                return;
            } else if (r3.Aek(null)) {
                outputStream.write(bArr, 0, read);
                j += (long) read;
                r4.accept(Long.valueOf(j));
            } else {
                return;
            }
        }
    }

    public static void A0B(AbstractC15710nm r3, byte[] bArr) {
        int length = bArr.length;
        if (length != 32) {
            StringBuilder sb = new StringBuilder("");
            sb.append(length);
            r3.AaV("crypto-iq-incorrect-account-hash-size", sb.toString(), true);
        }
    }

    public static void A0C(File file, ArrayList arrayList) {
        Collections.sort(arrayList, new C112255Cr(C004502a.A01(file.getName()), new SimpleDateFormat("yyyy-MM-dd", Locale.US)));
    }

    public static boolean A0D(Context context) {
        C32821cn r0;
        File file = new File(context.getFilesDir(), "key");
        if (!file.exists()) {
            return true;
        }
        String str = null;
        try {
            C44541z8 A05 = A05(context);
            if (!(A05 == null || (r0 = A05.A00) == null)) {
                str = r0.A00;
            }
        } catch (AnonymousClass03G | InvalidParameterException unused) {
        }
        if (TextUtils.isEmpty(str) || "1".equals(str)) {
            return true;
        }
        long lastModified = file.lastModified();
        if (A01 > lastModified || lastModified >= A00 || !"3".equals(str)) {
            return false;
        }
        return true;
    }

    public static boolean A0E(C44641zJ r4, String str) {
        boolean z = false;
        if ((r4.A00 & 8) == 8) {
            z = true;
        }
        if (z) {
            C44631zI r2 = r4.A02;
            if (r2 == null) {
                r2 = C44631zI.A0e;
            }
            if ((r2.A01 & 4) == 4) {
                String str2 = r2.A06;
                if (!TextUtils.isEmpty(str2) && !str.endsWith(str2)) {
                    StringBuilder sb = new StringBuilder("EncBackupUtils/has-jid-user-mismatch/expected-jid-user-ends-with: ");
                    sb.append(str2);
                    sb.append("  actual-jid-user: ");
                    sb.append(str);
                    Log.e(sb.toString());
                    return true;
                }
            }
        }
        return false;
    }

    public static byte[] A0F(byte[] bArr) {
        MessageDigest messageDigest;
        Arrays.toString(bArr);
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Log.e("calculateahash/digester ", e);
            messageDigest = null;
        }
        if (messageDigest == null) {
            Log.e("calculateahash/digester is null");
            return null;
        }
        messageDigest.reset();
        messageDigest.update(bArr);
        return messageDigest.digest();
    }
}
