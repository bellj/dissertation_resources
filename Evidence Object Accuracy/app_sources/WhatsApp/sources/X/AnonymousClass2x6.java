package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import org.json.JSONObject;

/* renamed from: X.2x6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2x6 extends C30211Wn {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(45);
    public final String A00;
    public final String A01;

    public AnonymousClass2x6(Parcel parcel) {
        super(parcel);
        this.A01 = parcel.readString();
        this.A00 = parcel.readString();
    }

    public AnonymousClass2x6(String str, String str2) {
        super(str, str2);
        this.A01 = "";
        this.A00 = null;
    }

    public AnonymousClass2x6(String str, String str2, String str3, String str4) {
        super(str, str2);
        this.A01 = str3;
        this.A00 = str4;
    }

    public static AnonymousClass2x6 A01(JSONObject jSONObject) {
        String string = jSONObject.getString("id");
        AnonymousClass009.A04(string);
        String string2 = jSONObject.getString("name");
        AnonymousClass009.A04(string2);
        return new AnonymousClass2x6(string, string2, jSONObject.optString("icon_url", ""), jSONObject.optString("bg_color"));
    }

    @Override // X.C30211Wn, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A01);
        parcel.writeString(this.A00);
    }
}
