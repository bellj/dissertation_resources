package X;

import android.widget.FrameLayout;

/* renamed from: X.537  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass537 implements AbstractC49122Jj {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ FrameLayout A02;
    public final /* synthetic */ AnonymousClass35V A03;

    public AnonymousClass537(FrameLayout frameLayout, AnonymousClass35V r2, int i, int i2) {
        this.A03 = r2;
        this.A02 = frameLayout;
        this.A01 = i;
        this.A00 = i2;
    }

    @Override // X.AbstractC49122Jj
    public void ATN(C14320lF r6, boolean z) {
        this.A03.A0E(this.A02, r6.A07, this.A01, this.A00);
    }
}
