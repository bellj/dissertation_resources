package X;

import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.util.Date;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5OP  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5OP extends AbstractC113485Ht implements AnonymousClass5S3 {
    public AnonymousClass5S3 attrCarrier;
    public final Object cacheLock;
    public volatile int hashValue;
    public volatile boolean hashValueSet;
    public AnonymousClass5OO internalCertificateValue;
    public X500Principal issuerValue;
    public PublicKey publicKeyValue;
    public X500Principal subjectValue;
    public long[] validityValues;

    public final AnonymousClass5OO A00() {
        byte[] bArr;
        AnonymousClass5OO r0;
        synchronized (this.cacheLock) {
            AnonymousClass5OO r02 = this.internalCertificateValue;
            if (r02 != null) {
                return r02;
            }
            try {
                bArr = getEncoded();
            } catch (CertificateEncodingException unused) {
                bArr = null;
            }
            AnonymousClass5S2 r6 = this.bcHelper;
            C114565Mf r5 = this.c;
            AnonymousClass5OO r2 = new AnonymousClass5OO(this.sigAlgName, this.basicConstraints, r5, r6, this.sigAlgParams, bArr, this.keyUsage);
            synchronized (this.cacheLock) {
                r0 = this.internalCertificateValue;
                if (r0 == null) {
                    this.internalCertificateValue = r2;
                    r0 = r2;
                }
            }
            return r0;
        }
    }

    @Override // X.AbstractC113485Ht, java.security.cert.X509Certificate
    public X500Principal getIssuerX500Principal() {
        X500Principal x500Principal;
        synchronized (this.cacheLock) {
            X500Principal x500Principal2 = this.issuerValue;
            if (x500Principal2 != null) {
                return x500Principal2;
            }
            X500Principal issuerX500Principal = super.getIssuerX500Principal();
            synchronized (this.cacheLock) {
                x500Principal = this.issuerValue;
                if (x500Principal == null) {
                    this.issuerValue = issuerX500Principal;
                    x500Principal = issuerX500Principal;
                }
            }
            return x500Principal;
        }
    }

    @Override // X.AbstractC113485Ht, java.security.cert.Certificate
    public PublicKey getPublicKey() {
        synchronized (this.cacheLock) {
            PublicKey publicKey = this.publicKeyValue;
            if (publicKey != null) {
                return publicKey;
            }
            super.getPublicKey();
            return null;
        }
    }

    @Override // X.AbstractC113485Ht, java.security.cert.X509Certificate
    public X500Principal getSubjectX500Principal() {
        X500Principal x500Principal;
        synchronized (this.cacheLock) {
            X500Principal x500Principal2 = this.subjectValue;
            if (x500Principal2 != null) {
                return x500Principal2;
            }
            X500Principal subjectX500Principal = super.getSubjectX500Principal();
            synchronized (this.cacheLock) {
                x500Principal = this.subjectValue;
                if (x500Principal == null) {
                    this.subjectValue = subjectX500Principal;
                    x500Principal = subjectX500Principal;
                }
            }
            return x500Principal;
        }
    }

    @Override // java.security.cert.Certificate, java.lang.Object
    public int hashCode() {
        if (!this.hashValueSet) {
            this.hashValue = A00().hashCode();
            this.hashValueSet = true;
        }
        return this.hashValue;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass5OP(X.C114565Mf r12, X.AnonymousClass5S2 r13) {
        /*
            r11 = this;
            java.lang.String r0 = "2.5.29.19"
            r7 = r12
            byte[] r0 = X.AbstractC113485Ht.A03(r0, r12)     // Catch: Exception -> 0x0097
            if (r0 != 0) goto L_0x000b
            r6 = 0
            goto L_0x0013
        L_0x000b:
            X.1TL r0 = X.AnonymousClass1TL.A03(r0)     // Catch: Exception -> 0x0097
            X.5Mh r6 = X.C114585Mh.A00(r0)     // Catch: Exception -> 0x0097
        L_0x0013:
            java.lang.String r0 = "2.5.29.15"
            byte[] r0 = X.AbstractC113485Ht.A03(r0, r12)     // Catch: Exception -> 0x008a
            if (r0 != 0) goto L_0x001d
            r10 = 0
            goto L_0x004c
        L_0x001d:
            X.1TL r0 = X.AnonymousClass1TL.A03(r0)     // Catch: Exception -> 0x008a
            X.5MA r1 = X.AnonymousClass5MA.A00(r0)     // Catch: Exception -> 0x008a
            byte[] r5 = r1.A0B()     // Catch: Exception -> 0x008a
            int r0 = r5.length     // Catch: Exception -> 0x008a
            int r4 = r0 << 3
            int r0 = r1.A00     // Catch: Exception -> 0x008a
            int r4 = r4 - r0
            r0 = 9
            if (r4 < r0) goto L_0x0034
            r0 = r4
        L_0x0034:
            boolean[] r10 = new boolean[r0]     // Catch: Exception -> 0x008a
            r3 = 0
        L_0x0037:
            if (r3 == r4) goto L_0x004c
            int r0 = r3 / 8
            byte r2 = r5[r0]     // Catch: Exception -> 0x008a
            r1 = 128(0x80, float:1.794E-43)
            int r0 = r3 % 8
            int r1 = r1 >>> r0
            r2 = r2 & r1
            boolean r0 = X.C12960it.A1S(r2)
            r10[r3] = r0     // Catch: Exception -> 0x008a
            int r3 = r3 + 1
            goto L_0x0037
        L_0x004c:
            X.5Mv r0 = r12.A02     // Catch: Exception -> 0x007d
            java.lang.String r5 = X.C95444dj.A01(r0)     // Catch: Exception -> 0x007d
            X.1TN r0 = r0.A00     // Catch: Exception -> 0x0070
            if (r0 != 0) goto L_0x0057
            goto L_0x005c
        L_0x0057:
            byte[] r9 = X.C72463ee.A0c(r0)     // Catch: Exception -> 0x0070
            goto L_0x005d
        L_0x005c:
            r9 = 0
        L_0x005d:
            r4 = r11
            r8 = r13
            r4.<init>(r5, r6, r7, r8, r9, r10)
            java.lang.Object r0 = X.C12970iu.A0l()
            r11.cacheLock = r0
            X.5GU r0 = new X.5GU
            r0.<init>()
            r11.attrCarrier = r0
            return
        L_0x0070:
            r1 = move-exception
            java.lang.String r0 = "cannot construct SigAlgParams: "
            java.lang.String r1 = X.C12960it.A0b(r0, r1)
            java.security.cert.CertificateParsingException r0 = new java.security.cert.CertificateParsingException
            r0.<init>(r1)
            throw r0
        L_0x007d:
            r1 = move-exception
            java.lang.String r0 = "cannot construct SigAlgName: "
            java.lang.String r1 = X.C12960it.A0b(r0, r1)
            java.security.cert.CertificateParsingException r0 = new java.security.cert.CertificateParsingException
            r0.<init>(r1)
            throw r0
        L_0x008a:
            r1 = move-exception
            java.lang.String r0 = "cannot construct KeyUsage: "
            java.lang.String r1 = X.C12960it.A0b(r0, r1)
            java.security.cert.CertificateParsingException r0 = new java.security.cert.CertificateParsingException
            r0.<init>(r1)
            throw r0
        L_0x0097:
            r1 = move-exception
            java.lang.String r0 = "cannot construct BasicConstraints: "
            java.lang.String r1 = X.C12960it.A0b(r0, r1)
            java.security.cert.CertificateParsingException r0 = new java.security.cert.CertificateParsingException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass5OP.<init>(X.5Mf, X.5S2):void");
    }

    @Override // X.AbstractC113485Ht, java.security.cert.X509Certificate
    public void checkValidity(Date date) {
        long[] jArr;
        long time = date.getTime();
        synchronized (this.cacheLock) {
            jArr = this.validityValues;
            if (jArr == null) {
                long[] jArr2 = {super.getNotBefore().getTime(), super.getNotAfter().getTime()};
                synchronized (this.cacheLock) {
                    jArr = this.validityValues;
                    if (jArr == null) {
                        this.validityValues = jArr2;
                        jArr = jArr2;
                    }
                }
            }
        }
        if (time > jArr[1]) {
            throw new CertificateExpiredException(C12960it.A0d(this.c.A03.A0A.A03(), C12960it.A0k("certificate expired on ")));
        } else if (time < jArr[0]) {
            throw new CertificateNotYetValidException(C12960it.A0d(this.c.A03.A0B.A03(), C12960it.A0k("certificate not valid till ")));
        }
    }

    @Override // java.security.cert.Certificate, java.lang.Object
    public boolean equals(Object obj) {
        AnonymousClass5MA r1;
        if (obj == this) {
            return true;
        }
        if (obj instanceof AnonymousClass5OP) {
            AnonymousClass5OP r3 = (AnonymousClass5OP) obj;
            if (!this.hashValueSet || !r3.hashValueSet) {
                if ((this.internalCertificateValue == null || r3.internalCertificateValue == null) && (r1 = this.c.A01) != null && !r1.A04(r3.c.A01)) {
                    return false;
                }
            } else if (this.hashValue != r3.hashValue) {
                return false;
            }
        }
        return A00().equals(obj);
    }
}
