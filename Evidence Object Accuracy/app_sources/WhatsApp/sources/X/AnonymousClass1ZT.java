package X;

import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: X.1ZT  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZT {
    public static String A00(C22680zT r1, String str, String str2) {
        try {
            str2 = r1.A02(Integer.parseInt(str), str2);
            return str2;
        } catch (IOException e) {
            Log.e("phonenumberutils/trim/error", e);
            return str2;
        }
    }

    public static String A01(String str) {
        if (str != null) {
            Matcher matcher = Pattern.compile("^([17]|2[07]|3[0123469]|4[013456789]|5[12345678]|6[0123456]|8[1246]|9[0123458]|\\d{3})\\d*?(\\d{4,6})$").matcher(str.replaceAll("\\D", ""));
            if (matcher.find()) {
                return matcher.group(1);
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A02(java.lang.String r8, java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            int r0 = r11.hashCode()
            r1 = 0
            switch(r0) {
                case 1693: goto L_0x002b;
                case 1695: goto L_0x001f;
                case 1696: goto L_0x0015;
                case 49686: goto L_0x0009;
                default: goto L_0x0008;
            }
        L_0x0008:
            return r1
        L_0x0009:
            java.lang.String r0 = "237"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0008
            r2 = 3
            java.lang.String r7 = "6"
            goto L_0x0036
        L_0x0015:
            java.lang.String r0 = "55"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0008
            r2 = 4
            goto L_0x0028
        L_0x001f:
            java.lang.String r0 = "54"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0008
            r2 = 2
        L_0x0028:
            java.lang.String r7 = "9"
            goto L_0x0036
        L_0x002b:
            java.lang.String r0 = "52"
            boolean r0 = r11.equals(r0)
            if (r0 == 0) goto L_0x0008
            r2 = 2
            java.lang.String r7 = "1"
        L_0x0036:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r11)
            r0.append(r9)
            java.lang.String r6 = r0.toString()
            boolean r5 = A03(r8, r9, r6, r7, r2)
            boolean r4 = A03(r10, r9, r6, r7, r2)
            int r0 = r11.length()
            int r1 = r2 - r0
            boolean r3 = A03(r9, r8, r10, r7, r1)
            boolean r2 = A03(r6, r8, r10, r7, r2)
            boolean r0 = A03(r8, r9, r6, r7, r1)
            boolean r1 = A03(r10, r9, r6, r7, r1)
            if (r5 != 0) goto L_0x0070
            if (r4 != 0) goto L_0x0070
            if (r3 != 0) goto L_0x0070
            if (r2 != 0) goto L_0x0070
            if (r0 != 0) goto L_0x0070
            r0 = 0
            if (r1 == 0) goto L_0x0071
        L_0x0070:
            r0 = 1
        L_0x0071:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1ZT.A02(java.lang.String, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    public static boolean A03(String str, String str2, String str3, String str4, int i) {
        if (str.length() < i) {
            return false;
        }
        StringBuilder sb = new StringBuilder(str);
        sb.insert(i, str4);
        String obj = sb.toString();
        if (obj.equals(str2) || obj.equals(str3)) {
            return true;
        }
        return false;
    }
}
