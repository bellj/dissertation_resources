package X;

import android.animation.ValueAnimator;

/* renamed from: X.3Jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C65413Jm implements ValueAnimator.AnimatorUpdateListener {
    public int A00 = 0;
    public final /* synthetic */ AbstractC15160mf A01;

    public C65413Jm(AbstractC15160mf r2) {
        this.A01 = r2;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
        boolean z = AbstractC15160mf.A09;
        C34291fu r1 = this.A01.A05;
        if (z) {
            AnonymousClass028.A0Y(r1, A05 - this.A00);
        } else {
            r1.setTranslationY((float) A05);
        }
        this.A00 = A05;
    }
}
