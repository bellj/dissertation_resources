package X;

import java.io.Serializable;

/* renamed from: X.2Ie  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC48872Ie implements Serializable {
    public static final long serialVersionUID = 0;

    @Override // java.lang.Object
    public abstract boolean equals(Object obj);

    @Override // java.lang.Object
    public abstract int hashCode();

    @Override // java.lang.Object
    public abstract String toString();
}
