package X;

import java.util.List;

/* renamed from: X.42Y  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42Y extends AbstractC451920n {
    public final List A00;

    public /* synthetic */ AnonymousClass42Y(AbstractC451720l r1, C241414j r2, AbstractC248317b r3, AbstractC14440lR r4, List list) {
        super(r1, r2, r3, r4);
        this.A00 = list;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        return A08(this.A00);
    }
}
