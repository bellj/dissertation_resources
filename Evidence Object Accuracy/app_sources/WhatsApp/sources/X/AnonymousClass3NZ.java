package X;

import android.os.Build;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationSet;
import com.whatsapp.components.CircularRevealView;

/* renamed from: X.3NZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3NZ implements ViewTreeObserver.OnGlobalLayoutListener {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C42561vP A01;
    public final /* synthetic */ boolean A02;

    public AnonymousClass3NZ(C42561vP r1, int i, boolean z) {
        this.A01 = r1;
        this.A02 = z;
        this.A00 = i;
    }

    @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
    public void onGlobalLayout() {
        boolean z;
        C42561vP r5 = this.A01;
        CircularRevealView circularRevealView = r5.A0B;
        C12980iv.A1E(circularRevealView, this);
        r5.A06();
        if (Build.VERSION.SDK_INT >= 18 || !(z = this.A02)) {
            circularRevealView.A00();
            return;
        }
        int[] A07 = C13000ix.A07();
        r5.A06.getLocationOnScreen(A07);
        AnimationSet A00 = C42561vP.A00(r5.A00 + A07[0], z, true);
        A00.setDuration((long) this.A00);
        circularRevealView.A01(A00);
    }
}
