package X;

import java.util.Comparator;

/* renamed from: X.5CV  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5CV implements Comparator {
    @Override // java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return ((String) obj).compareToIgnoreCase((String) obj2);
    }
}
