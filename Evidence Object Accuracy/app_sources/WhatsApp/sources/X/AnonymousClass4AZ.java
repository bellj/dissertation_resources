package X;

/* renamed from: X.4AZ  reason: invalid class name */
/* loaded from: classes3.dex */
public enum AnonymousClass4AZ {
    VIDEO_CODEC_VP8,
    VIDEO_CODEC_VP9,
    VIDEO_CODEC_H264,
    VIDEO_CODEC_H265
}
