package X;

import com.whatsapp.payments.ui.ConfirmReceivePaymentFragment;

/* renamed from: X.5fb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120085fb extends AnonymousClass4UZ {
    public final /* synthetic */ ConfirmReceivePaymentFragment A00;

    public C120085fb(ConfirmReceivePaymentFragment confirmReceivePaymentFragment) {
        this.A00 = confirmReceivePaymentFragment;
    }

    @Override // X.AnonymousClass4UZ
    public void A00() {
        ConfirmReceivePaymentFragment confirmReceivePaymentFragment = this.A00;
        C14580lf r0 = confirmReceivePaymentFragment.A01;
        if (r0 != null) {
            r0.A04();
        }
        confirmReceivePaymentFragment.A01 = C117305Zk.A0C(confirmReceivePaymentFragment.A04);
    }
}
