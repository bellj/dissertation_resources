package X;

import com.whatsapp.biz.cart.view.fragment.CartFragment;

/* renamed from: X.52l  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1096352l implements AbstractC116455Vm {
    public final /* synthetic */ CartFragment A00;

    public C1096352l(CartFragment cartFragment) {
        this.A00 = cartFragment;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A0n);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        AbstractC36671kL.A08(this.A00.A0n, iArr, 65536);
    }
}
