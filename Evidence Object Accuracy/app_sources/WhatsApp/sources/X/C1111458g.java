package X;

/* renamed from: X.58g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1111458g implements AbstractC27731Iz {
    public final long A00;

    @Override // X.AbstractC27731Iz
    public int AHd() {
        return 2;
    }

    public C1111458g(long j) {
        this.A00 = j;
    }

    @Override // X.AbstractC27731Iz
    public boolean A9Y(AbstractC27731Iz r7) {
        return (r7 instanceof C1111458g) && this.A00 == r7.AGJ();
    }

    @Override // X.AbstractC27731Iz
    public long AGJ() {
        return this.A00;
    }
}
