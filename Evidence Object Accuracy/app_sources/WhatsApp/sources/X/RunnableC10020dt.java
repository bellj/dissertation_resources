package X;

import android.graphics.Typeface;
import android.widget.TextView;

/* renamed from: X.0dt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10020dt implements Runnable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ Typeface A01;
    public final /* synthetic */ TextView A02;
    public final /* synthetic */ AnonymousClass086 A03;

    public RunnableC10020dt(Typeface typeface, TextView textView, AnonymousClass086 r3, int i) {
        this.A03 = r3;
        this.A02 = textView;
        this.A01 = typeface;
        this.A00 = i;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.A02.setTypeface(this.A01, this.A00);
    }
}
