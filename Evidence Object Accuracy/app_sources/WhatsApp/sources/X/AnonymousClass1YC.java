package X;

import android.database.sqlite.SQLiteTransactionListener;

/* renamed from: X.1YC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YC implements SQLiteTransactionListener {
    public final /* synthetic */ C29621Ty A00;
    public final /* synthetic */ Runnable A01;

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onBegin() {
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onRollback() {
    }

    public AnonymousClass1YC(C29621Ty r1, Runnable runnable) {
        this.A00 = r1;
        this.A01 = runnable;
    }

    @Override // android.database.sqlite.SQLiteTransactionListener
    public void onCommit() {
        this.A01.run();
    }
}
