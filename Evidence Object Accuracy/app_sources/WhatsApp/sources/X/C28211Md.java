package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.1Md  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C28211Md {
    public static final C28221Me A01 = new C28221Me(null);
    public final Map A00 = new HashMap();

    public final void A00(int i, Object obj) {
        if (obj == null) {
            this.A00.remove(Integer.valueOf(i));
            return;
        }
        C28221Me r3 = new C28221Me(obj);
        Map map = this.A00;
        Integer valueOf = Integer.valueOf(i);
        if (!map.containsKey(valueOf) || !map.get(valueOf).equals(r3)) {
            map.put(valueOf, r3);
        }
    }
}
