package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.R;
import java.util.List;

/* renamed from: X.1jY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36241jY extends AnonymousClass02M {
    public final List A00;
    public final boolean A01;
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A02;

    public C36241jY(AbstractView$OnCreateContextMenuListenerC35851ir r1, List list, boolean z) {
        this.A02 = r1;
        this.A00 = list;
        this.A01 = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r3.A01 != false) goto L_0x0015;
     */
    @Override // X.AnonymousClass02M
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A0D() {
        /*
            r3 = this;
            java.util.List r0 = r3.A00
            int r2 = r0.size()
            X.1ir r1 = r3.A02
            X.1Yr r0 = r1.A0n
            if (r0 != 0) goto L_0x0015
            X.1Yr r0 = r1.A0m
            if (r0 != 0) goto L_0x0015
            boolean r1 = r3.A01
            r0 = 1
            if (r1 == 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            int r2 = r2 + r0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C36241jY.A0D():int");
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r4, int i) {
        List list;
        AbstractC75713kI r42 = (AbstractC75713kI) r4;
        AbstractView$OnCreateContextMenuListenerC35851ir r1 = this.A02;
        if (r1.A0n != null || r1.A0m != null || this.A01) {
            list = this.A00;
        } else if (i != 0) {
            list = this.A00;
            i--;
        } else {
            return;
        }
        C30751Yr r2 = (C30751Yr) list.get(i);
        C15370n3 A0A = r1.A14.A0A(r2.A06);
        if (A0A != null) {
            r42.A08(A0A, r2);
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        int i2;
        if (i == 0) {
            i2 = R.layout.location_sharing_participant_my_row;
        } else if (i == 1) {
            i2 = R.layout.location_sharing_participant_row;
        } else if (i != 3) {
            i2 = R.layout.location_sharing_participant_ended;
            if (i != 4) {
                i2 = R.layout.location_sharing_participant_selected_row;
            }
        } else {
            i2 = R.layout.location_sharing_participant_share_back_row;
        }
        AbstractView$OnCreateContextMenuListenerC35851ir r2 = this.A02;
        View inflate = r2.A0E.getLayoutInflater().inflate(i2, viewGroup, false);
        if (i == 2) {
            return new C618432t(inflate, r2);
        }
        if (i == 3) {
            return new AnonymousClass455(inflate, r2);
        }
        if (i != 4) {
            return new C618532u(inflate, r2);
        }
        return new C618332s(inflate, r2);
    }

    @Override // X.AnonymousClass02M
    public int getItemViewType(int i) {
        if (this.A01) {
            return 2;
        }
        AbstractView$OnCreateContextMenuListenerC35851ir r3 = this.A02;
        if (r3.A0n == null && r3.A0m == null) {
            if (i == 0) {
                return 3;
            }
            i--;
        }
        List list = this.A00;
        if (list.get(i) == r3.A0m) {
            return 4;
        }
        return list.get(i) == r3.A0n ? 0 : 1;
    }
}
