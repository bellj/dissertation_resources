package X;

import java.io.EOFException;

/* renamed from: X.4wm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107094wm implements AbstractC116585Wa {
    public int A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public long A07;
    public final long A08;
    public final long A09;
    public final C92864Xs A0A;
    public final AnonymousClass4Y4 A0B;

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r9 <= r7) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C107094wm(X.AnonymousClass4Y4 r6, long r7, long r9, long r11, long r13, boolean r15) {
        /*
            r5 = this;
            r5.<init>()
            r2 = 0
            r3 = 0
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 < 0) goto L_0x000f
            int r1 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            r0 = 1
            if (r1 > 0) goto L_0x0010
        L_0x000f:
            r0 = 0
        L_0x0010:
            X.C95314dV.A03(r0)
            r5.A0B = r6
            r5.A09 = r7
            r5.A08 = r9
            long r9 = r9 - r7
            int r0 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r0 == 0) goto L_0x002a
            if (r15 != 0) goto L_0x002a
            r5.A00 = r2
        L_0x0022:
            X.4Xs r0 = new X.4Xs
            r0.<init>()
            r5.A0A = r0
            return
        L_0x002a:
            r5.A07 = r13
            r0 = 4
            r5.A00 = r0
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C107094wm.<init>(X.4Y4, long, long, long, long, boolean):void");
    }

    @Override // X.AbstractC116585Wa
    public /* bridge */ /* synthetic */ AnonymousClass5WY A8W() {
        if (this.A07 != 0) {
            return new C106884wR(this);
        }
        return null;
    }

    @Override // X.AbstractC116585Wa
    public long AZo(AnonymousClass5Yf r23) {
        long j;
        long j2;
        int i = this.A00;
        if (i == 0) {
            long AFo = r23.AFo();
            this.A03 = AFo;
            this.A00 = 1;
            long j3 = this.A08 - 65307;
            if (j3 > AFo) {
                return j3;
            }
        } else if (i != 1) {
            if (i == 2) {
                long j4 = this.A04;
                long j5 = this.A01;
                if (j4 != j5) {
                    long AFo2 = r23.AFo();
                    C92864Xs r6 = this.A0A;
                    if (!r6.A00(r23, j5)) {
                        j = this.A04;
                        if (j == AFo2) {
                            throw C12990iw.A0i("No ogg page can be found.");
                        }
                    } else {
                        r6.A01(r23, false);
                        r23.Aaj();
                        long j6 = this.A06;
                        long j7 = r6.A04;
                        long j8 = j6 - j7;
                        int i2 = r6.A01 + r6.A00;
                        if (0 > j8 || j8 >= 72000) {
                            if (j8 < 0) {
                                this.A01 = AFo2;
                                this.A02 = j7;
                            } else {
                                this.A04 = r23.AFo() + ((long) i2);
                                this.A05 = j7;
                            }
                            long j9 = this.A01;
                            j = this.A04;
                            long j10 = j9 - j;
                            if (j10 < 100000) {
                                this.A01 = j;
                            } else {
                                long j11 = (long) i2;
                                if (j8 <= 0) {
                                    j2 = 2;
                                } else {
                                    j2 = 1;
                                }
                                j = Math.max(j, Math.min((r23.AFo() - (j11 * j2)) + ((j8 * j10) / (this.A02 - this.A05)), j9 - 1));
                            }
                        }
                    }
                    if (j != -1) {
                        return j;
                    }
                }
                this.A00 = 3;
            } else if (i != 3) {
                if (i == 4) {
                    return -1;
                }
                throw C72463ee.A0D();
            }
            while (true) {
                C92864Xs r5 = this.A0A;
                r5.A00(r23, -1);
                r5.A01(r23, false);
                if (r5.A04 > this.A06) {
                    r23.Aaj();
                    this.A00 = 4;
                    return -(this.A05 + 2);
                }
                r23.Ae3(r5.A01 + r5.A00);
                this.A04 = r23.AFo();
                this.A05 = r5.A04;
            }
        }
        C92864Xs r8 = this.A0A;
        r8.A03 = 0;
        r8.A04 = 0;
        r8.A02 = 0;
        r8.A01 = 0;
        r8.A00 = 0;
        if (!r8.A00(r23, -1)) {
            throw new EOFException();
        }
        do {
            r8.A01(r23, false);
            r23.Ae3(r8.A01 + r8.A00);
            if ((r8.A03 & 4) == 4 || !r8.A00(r23, -1)) {
                break;
            }
        } while (r23.AFo() < this.A08);
        this.A07 = r8.A04;
        this.A00 = 4;
        return this.A03;
    }

    @Override // X.AbstractC116585Wa
    public void AeD(long j) {
        long j2 = this.A07;
        this.A06 = Math.max(0L, Math.min(j, j2 - 1));
        this.A00 = 2;
        this.A04 = this.A09;
        this.A01 = this.A08;
        this.A05 = 0;
        this.A02 = j2;
    }
}
