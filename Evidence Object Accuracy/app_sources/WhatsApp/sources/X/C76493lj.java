package X;

import com.google.android.exoplayer2.Timeline;

/* renamed from: X.3lj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C76493lj extends AbstractC76623lw {
    public final int A00;
    public final int A01;
    public final int A02;
    public final Timeline A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C76493lj(Timeline timeline, int i) {
        super(new C107624xg(i));
        boolean z = false;
        this.A03 = timeline;
        int A00 = timeline.A00();
        this.A00 = A00;
        this.A01 = timeline.A01();
        this.A02 = i;
        if (A00 > 0) {
            C95314dV.A02("LoopingMediaSource contains too many periods", i <= Integer.MAX_VALUE / A00 ? true : z);
        }
    }
}
