package X;

/* renamed from: X.4aJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93524aJ {
    public int[] A00;
    public int[] A01;
    public int[] A02;

    public C93524aJ() {
        this.A02 = new int[10];
        this.A01 = new int[10];
        this.A00 = new int[10];
    }

    public C93524aJ(int[] iArr, int[] iArr2, int[] iArr3) {
        this.A02 = iArr;
        this.A01 = iArr2;
        this.A00 = iArr3;
    }

    public static C93524aJ A00(int[] iArr, int[] iArr2, int[] iArr3) {
        return new C93524aJ(iArr, iArr2, iArr3);
    }

    public static void A01(int[] iArr, int[] iArr2, int[] iArr3, Object[] objArr, int i) {
        objArr[i] = new C93524aJ(iArr, iArr2, iArr3);
    }
}
