package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4wS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106894wS implements AnonymousClass5WY {
    public final long A00;
    public final C95424dg A01;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public C106894wS(C95424dg r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A01.A02();
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        long j2;
        C95424dg r1 = this.A01;
        C89884Lt r0 = r1.A0A;
        C95314dV.A01(r0);
        long[] jArr = r0.A01;
        long[] jArr2 = r0.A00;
        long j3 = (long) r1.A07;
        int A06 = AnonymousClass3JZ.A06(jArr, C72453ed.A0X(C72453ed.A0W(j, j3), r1.A09 - 1), false);
        long j4 = 0;
        if (A06 == -1) {
            j2 = 0;
        } else {
            j2 = jArr[A06];
            if (A06 != -1) {
                j4 = jArr2[A06];
            }
        }
        long j5 = this.A00;
        C94324bc r10 = new C94324bc((j2 * SearchActionVerificationClientService.MS_TO_NS) / j3, j5 + j4);
        if (r10.A01 == j || A06 == jArr.length - 1) {
            return new C92684Xa(r10, r10);
        }
        int i = A06 + 1;
        return C92684Xa.A00(r10, (jArr[i] * SearchActionVerificationClientService.MS_TO_NS) / j3, j5 + jArr2[i]);
    }
}
