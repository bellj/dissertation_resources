package X;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.whatsapp.R;
import java.lang.ref.WeakReference;

/* renamed from: X.3aQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69903aQ implements AbstractC38791oi {
    public final String A00;
    public final WeakReference A01;

    public C69903aQ(ImageView imageView, String str) {
        this.A01 = C12970iu.A10(imageView);
        this.A00 = str;
        imageView.setTag(str);
    }

    @Override // X.AbstractC38791oi
    public void ARs(Bitmap bitmap) {
        ImageView imageView = (ImageView) this.A01.get();
        if (imageView != null && this.A00.equals(imageView.getTag())) {
            imageView.setImageBitmap(bitmap);
        }
    }

    @Override // X.AbstractC38791oi
    public void AS0() {
        ImageView imageView = (ImageView) this.A01.get();
        if (imageView != null && this.A00.equals(imageView.getTag())) {
            imageView.setImageResource(R.drawable.selector_sticker_pack_error);
        }
    }

    @Override // X.AbstractC38791oi
    public void AS5(Bitmap bitmap) {
        ImageView imageView = (ImageView) this.A01.get();
        if (imageView != null && this.A00.equals(imageView.getTag())) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
