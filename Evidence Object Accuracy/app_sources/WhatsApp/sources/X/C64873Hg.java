package X;

import android.os.Bundle;
import android.util.Log;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/* renamed from: X.3Hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64873Hg {
    public final AnonymousClass3CX A00 = new AnonymousClass3CX();
    public final HashMap A01 = C12970iu.A11();
    public final Stack A02 = new Stack();

    public static final void A00(HashMap hashMap) {
        Iterator A0s = C12990iw.A0s(hashMap);
        while (A0s.hasNext()) {
            ((AnonymousClass3FE) C12970iu.A15(A0s).getValue()).A00 = false;
        }
        hashMap.clear();
    }

    public AnonymousClass3FE A01(C14230l4 r3, AbstractC14200l1 r4, String str) {
        AnonymousClass3FE r1 = new AnonymousClass3FE(r3, r4, this.A00);
        this.A01.put(str, r1);
        return r1;
    }

    public String A02(String str) {
        Stack stack = this.A02;
        if (stack.isEmpty()) {
            return null;
        }
        return (String) ((AbstractMap) stack.peek()).get(str);
    }

    public void A03() {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            ((Map) it.next()).clear();
        }
        A00(this.A01);
        this.A00.A01.clear();
    }

    public void A04(Bundle bundle) {
        Stack stack = this.A02;
        if (!stack.isEmpty()) {
            ArrayList A0w = C12980iv.A0w(stack.size());
            Iterator it = stack.iterator();
            while (it.hasNext()) {
                A0w.add(new HashMap((Map) it.next()));
            }
            bundle.putSerializable("screen_manager_saved_state", A0w);
        }
    }

    public void A05(Map map) {
        Iterator A0n = C12960it.A0n(map);
        while (A0n.hasNext()) {
            Map.Entry A15 = C12970iu.A15(A0n);
            Stack stack = this.A02;
            if (stack.peek() != null) {
                C12990iw.A1Q(A15.getKey(), (AbstractMap) stack.peek(), A15);
            }
        }
    }

    public void A06(boolean z) {
        AnonymousClass3CX r3 = this.A00;
        StringBuilder A0k = C12960it.A0k("BloksCallbackQueue/setActive(");
        A0k.append(z);
        A0k.append(")/queue size=");
        Queue queue = r3.A01;
        Log.d("Whatsapp", C12960it.A0f(A0k, queue.size()));
        r3.A00 = z;
        if (z) {
            while (!queue.isEmpty()) {
                Runnable runnable = (Runnable) queue.poll();
                if (runnable != null) {
                    runnable.run();
                } else {
                    throw C12980iv.A0n("");
                }
            }
        }
    }
}
