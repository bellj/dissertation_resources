package X;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.5b6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117995b6 extends AnonymousClass015 {
    public AnonymousClass016 A00 = C12980iv.A0T();
    public AnonymousClass016 A01 = C12980iv.A0T();
    public AnonymousClass016 A02 = C12980iv.A0T();
    public AnonymousClass016 A03 = C12980iv.A0T();
    public AnonymousClass016 A04 = C12980iv.A0T();
    public AnonymousClass016 A05 = C12980iv.A0T();
    public AnonymousClass016 A06 = C12980iv.A0T();
    public AnonymousClass016 A07 = C12980iv.A0T();
    public AnonymousClass016 A08 = C12980iv.A0T();
    public AnonymousClass016 A09 = C12980iv.A0T();
    public C27691It A0A = C13000ix.A03();
    public String A0B;
    public final C15450nH A0C;
    public final C16590pI A0D;
    public final AnonymousClass018 A0E;
    public final C1329668y A0F;
    public final C21860y6 A0G;
    public final C17900ra A0H;

    public C117995b6(C15450nH r2, C16590pI r3, AnonymousClass018 r4, C1329668y r5, C21860y6 r6, C17900ra r7) {
        this.A0D = r3;
        this.A0C = r2;
        this.A0E = r4;
        this.A0G = r6;
        this.A0H = r7;
        this.A0F = r5;
    }

    public void A04(String str, String str2) {
        Context context;
        int i;
        String A0d;
        int i2;
        C1310661b A00;
        C1310661b r2 = null;
        if (!(str == null || (A00 = C1310661b.A00(Uri.parse(str), str2)) == null)) {
            A00.A03 = str;
            r2 = A00;
        }
        String A002 = C1329668y.A00(this.A0F);
        if (r2 != null) {
            String str3 = r2.A0C;
            String str4 = r2.A04;
            String str5 = r2.A06;
            String str6 = r2.A05;
            String str7 = r2.A07;
            if (C1309360o.A00(str3) && !str3.equalsIgnoreCase(A002) && !TextUtils.isEmpty(str4) && ((str5 == null || str6 == null || C28421Nd.A02(str5, 0.0f).floatValue() <= C28421Nd.A02(str6, 0.0f).floatValue()) && AnonymousClass614.A03(str7))) {
                this.A0B = str2;
                this.A06.A0B(r2);
                AbstractC30791Yv A003 = this.A0H.A00();
                AnonymousClass018 r4 = this.A0E;
                if (TextUtils.isEmpty(r2.A05)) {
                    A0d = null;
                } else {
                    StringBuilder A0h = C12960it.A0h();
                    if (!TextUtils.isEmpty(str5) && !r2.A05.equals(str5)) {
                        A0h.append(A003.AAA(r4, C117305Zk.A0E(A003, str5), 0));
                        A0h.append(" - ");
                    }
                    A0d = C12960it.A0d(A003.AAA(r4, C117305Zk.A0E(A003, r2.A05), 0), A0h);
                }
                this.A01.A0B(r2.A04);
                this.A00.A0B(r2.A0C);
                AnonymousClass016 r3 = this.A03;
                if (!this.A0G.A0B()) {
                    i2 = R.string.btn_continue;
                } else {
                    boolean isEmpty = TextUtils.isEmpty(A0d);
                    i2 = R.string.payment_qr_send_payment_cta;
                    if (isEmpty) {
                        i2 = R.string.payment_qr_new_payment_cta;
                    }
                }
                C12960it.A1A(r3, i2);
                if (!TextUtils.isEmpty(A0d)) {
                    this.A07.A0B(new C130595zf(A0d));
                }
                this.A02.A0B(C12960it.A0V());
                if (this.A0C.A05(AbstractC15460nI.A0v) && TextUtils.isEmpty(r2.A09)) {
                    C117315Zl.A0Q(this.A09);
                    C12990iw.A1J(this.A08, TextUtils.isEmpty(r2.A05));
                    return;
                }
                return;
            }
        }
        this.A0A.A0B(new C127525ud(10));
        AnonymousClass016 r32 = this.A04;
        if (r2 == null || TextUtils.isEmpty(r2.A0C) || !r2.A0C.equals(A002)) {
            context = this.A0D.A00;
            i = R.string.payments_deeplink_invalid_param;
        } else {
            context = this.A0D.A00;
            i = R.string.payments_deeplink_cannot_send_self;
        }
        r32.A0B(context.getString(i));
        this.A02.A0B(C12970iu.A0h());
    }
}
