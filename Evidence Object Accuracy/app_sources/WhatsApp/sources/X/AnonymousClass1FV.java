package X;

/* renamed from: X.1FV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FV extends AbstractC17250qV {
    public final C22920zr A00;
    public final AnonymousClass101 A01;
    public final C14820m6 A02;
    public final C15990oG A03;
    public final C18240s8 A04;
    public final C14850m9 A05;
    public final C17230qT A06;

    public AnonymousClass1FV(AbstractC15710nm r8, C22920zr r9, AnonymousClass101 r10, C14820m6 r11, C15990oG r12, C18240s8 r13, C14850m9 r14, C17220qS r15, C17230qT r16, AbstractC14440lR r17) {
        super(r8, r15, r16, r17, new int[]{241}, false);
        this.A05 = r14;
        this.A04 = r13;
        this.A00 = r9;
        this.A03 = r12;
        this.A06 = r16;
        this.A02 = r11;
        this.A01 = r10;
    }
}
