package X;

/* renamed from: X.1qN  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1qN {
    public final C39741qT A00;
    public final C14620lj A01 = new C14620lj();
    public final C14620lj A02 = new C14620lj();
    public final C14620lj A03 = new C14620lj();
    public final C14620lj A04 = new C14620lj();
    public final C39781qX A05 = new C39781qX(this);
    public final C39771qW A06 = new C39771qW(this);
    public final AbstractC39761qV A07 = new AbstractC39761qV() { // from class: X.57l
        @Override // X.AbstractC39761qV
        public final void AQa(AbstractC39731qS r2) {
            AnonymousClass1qN.this.A04.A04(r2);
        }
    };
    public final AnonymousClass1qQ A08;

    public AnonymousClass1qN(C39741qT r2, AnonymousClass1qQ r3) {
        this.A08 = r3;
        this.A00 = r2;
    }
}
