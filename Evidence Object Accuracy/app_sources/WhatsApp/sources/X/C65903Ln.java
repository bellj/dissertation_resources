package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.3Ln  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65903Ln implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        C16700pc.A0E(parcel, 0);
        String readString = parcel.readString();
        C16700pc.A0C(readString);
        C16700pc.A0B(readString);
        String readString2 = parcel.readString();
        C16700pc.A0C(readString2);
        C16700pc.A0B(readString2);
        int readInt = parcel.readInt();
        String readString3 = parcel.readString();
        C16700pc.A0C(readString3);
        C16700pc.A0B(readString3);
        int readInt2 = parcel.readInt();
        int readInt3 = parcel.readInt();
        String readString4 = parcel.readString();
        C16700pc.A0C(readString4);
        C16700pc.A0B(readString4);
        return new C65943Lr(readString, readString2, readString3, readString4, readInt, readInt2, readInt3);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C65943Lr[i];
    }
}
