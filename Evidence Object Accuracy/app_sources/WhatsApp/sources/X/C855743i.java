package X;

/* renamed from: X.43i  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C855743i extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public Long A03;
    public String A04;

    public C855743i() {
        super(3494, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
        r3.Abe(3, this.A02);
        r3.Abe(4, this.A04);
        r3.Abe(5, this.A03);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamCommunityHomeAction {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "communityHomeGroupDiscoveries", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "communityHomeGroupJoins", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "communityHomeGroupNavigations", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "communityHomeId", this.A04);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "communityHomeViews", this.A03);
        return C12960it.A0d("}", A0k);
    }
}
