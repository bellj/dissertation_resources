package X;

import android.graphics.Bitmap;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: X.0PI  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0PI {
    public Rect A00;
    public final Bitmap A01;
    public final List A02;
    public final List A03;

    public AnonymousClass0PI(Bitmap bitmap) {
        ArrayList arrayList = new ArrayList();
        this.A03 = arrayList;
        ArrayList arrayList2 = new ArrayList();
        this.A02 = arrayList2;
        if (!bitmap.isRecycled()) {
            arrayList2.add(C06030Rx.A05);
            this.A01 = bitmap;
            arrayList.add(C06010Rv.A07);
            arrayList.add(C06010Rv.A09);
            arrayList.add(C06010Rv.A05);
            arrayList.add(C06010Rv.A06);
            arrayList.add(C06010Rv.A08);
            arrayList.add(C06010Rv.A04);
            return;
        }
        throw new IllegalArgumentException("Bitmap is not valid");
    }

    public C06030Rx A00() {
        AbstractC11320g6[] r0;
        float f;
        float f2;
        Bitmap bitmap = this.A01;
        if (bitmap != null) {
            Bitmap bitmap2 = bitmap;
            int width = bitmap.getWidth() * bitmap.getHeight();
            if (width > 12544) {
                double sqrt = Math.sqrt(((double) 12544) / ((double) width));
                if (sqrt > 0.0d) {
                    bitmap2 = Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * sqrt), (int) Math.ceil(((double) bitmap.getHeight()) * sqrt), false);
                }
            }
            Rect rect = this.A00;
            if (!(bitmap2 == bitmap || rect == null)) {
                double width2 = ((double) bitmap2.getWidth()) / ((double) bitmap.getWidth());
                rect.left = (int) Math.floor(((double) rect.left) * width2);
                rect.top = (int) Math.floor(((double) rect.top) * width2);
                rect.right = Math.min((int) Math.ceil(((double) rect.right) * width2), bitmap2.getWidth());
                rect.bottom = Math.min((int) Math.ceil(((double) rect.bottom) * width2), bitmap2.getHeight());
            }
            int width3 = bitmap2.getWidth();
            int height = bitmap2.getHeight();
            int[] iArr = new int[width3 * height];
            bitmap2.getPixels(iArr, 0, width3, 0, 0, width3, height);
            Rect rect2 = this.A00;
            if (rect2 != null) {
                int width4 = rect2.width();
                int height2 = this.A00.height();
                int[] iArr2 = new int[width4 * height2];
                for (int i = 0; i < height2; i++) {
                    Rect rect3 = this.A00;
                    System.arraycopy(iArr, ((rect3.top + i) * width3) + rect3.left, iArr2, i * width4, width4);
                }
                iArr = iArr2;
            }
            List list = this.A02;
            if (list.isEmpty()) {
                r0 = null;
            } else {
                r0 = (AbstractC11320g6[]) list.toArray(new AbstractC11320g6[list.size()]);
            }
            C06360Th r1 = new C06360Th(iArr, r0);
            if (bitmap2 != bitmap) {
                bitmap2.recycle();
            }
            C06030Rx r8 = new C06030Rx(r1.A00, this.A03);
            List list2 = r8.A03;
            int size = list2.size();
            for (int i2 = 0; i2 < size; i2++) {
                C06010Rv r5 = (C06010Rv) list2.get(i2);
                float[] fArr = r5.A03;
                int length = fArr.length;
                float f3 = 0.0f;
                for (float f4 : fArr) {
                    if (f4 > 0.0f) {
                        f3 += f4;
                    }
                }
                if (f3 != 0.0f) {
                    for (int i3 = 0; i3 < length; i3++) {
                        if (fArr[i3] > 0.0f) {
                            fArr[i3] = fArr[i3] / f3;
                        }
                    }
                }
                Map map = r8.A04;
                List list3 = r8.A02;
                int size2 = list3.size();
                float f5 = 0.0f;
                AnonymousClass0QA r3 = null;
                for (int i4 = 0; i4 < size2; i4++) {
                    AnonymousClass0QA r2 = (AnonymousClass0QA) list3.get(i4);
                    float[] A01 = r2.A01();
                    int i5 = 1;
                    float f6 = A01[1];
                    float[] fArr2 = r5.A02;
                    if (f6 >= fArr2[0] && f6 <= fArr2[2]) {
                        float f7 = A01[2];
                        float[] fArr3 = r5.A01;
                        if (f7 >= fArr3[0] && f7 <= fArr3[2] && !r8.A00.get(r2.A08)) {
                            float[] A012 = r2.A01();
                            AnonymousClass0QA r02 = r8.A01;
                            if (r02 != null) {
                                i5 = r02.A06;
                            }
                            float[] fArr4 = r5.A03;
                            float f8 = fArr4[0];
                            float f9 = 0.0f;
                            if (f8 > 0.0f) {
                                f = f8 * (1.0f - Math.abs(A012[1] - fArr2[1]));
                            } else {
                                f = 0.0f;
                            }
                            float f10 = fArr4[1];
                            if (f10 > 0.0f) {
                                f2 = f10 * (1.0f - Math.abs(A012[2] - fArr3[1]));
                            } else {
                                f2 = 0.0f;
                            }
                            float f11 = fArr4[2];
                            if (f11 > 0.0f) {
                                f9 = f11 * (((float) r2.A06) / ((float) i5));
                            }
                            float f12 = f + f2 + f9;
                            if (r3 == null || f12 > f5) {
                                r3 = r2;
                                f5 = f12;
                            }
                        }
                    }
                }
                if (r3 != null && r5.A00) {
                    r8.A00.append(r3.A08, true);
                }
                map.put(r5, r3);
            }
            r8.A00.clear();
            return r8;
        }
        throw new AssertionError();
    }

    public void A01(int i, int i2, int i3, int i4) {
        Bitmap bitmap = this.A01;
        if (bitmap != null) {
            Rect rect = this.A00;
            if (rect == null) {
                rect = new Rect();
                this.A00 = rect;
            }
            rect.set(0, 0, bitmap.getWidth(), bitmap.getHeight());
            if (!this.A00.intersect(i, i2, i3, i4)) {
                throw new IllegalArgumentException("The given region must intersect with the Bitmap's dimensions.");
            }
        }
    }
}
