package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.biz.catalog.view.EllipsizedTextEmojiLabel;

/* renamed from: X.3xS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C83573xS extends AbstractC52172aN {
    public final /* synthetic */ EllipsizedTextEmojiLabel A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C83573xS(Context context, EllipsizedTextEmojiLabel ellipsizedTextEmojiLabel, int i) {
        super(context, i);
        this.A00 = ellipsizedTextEmojiLabel;
    }

    @Override // X.AbstractC116465Vn
    public void onClick(View view) {
        EllipsizedTextEmojiLabel ellipsizedTextEmojiLabel = this.A00;
        View.OnClickListener onClickListener = ellipsizedTextEmojiLabel.A03;
        if (onClickListener != null) {
            onClickListener.onClick(view);
        }
        ellipsizedTextEmojiLabel.A00 = Integer.MAX_VALUE;
        ellipsizedTextEmojiLabel.A0F(ellipsizedTextEmojiLabel.A04, ellipsizedTextEmojiLabel.A05, ellipsizedTextEmojiLabel.A01, true);
    }
}
