package X;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.whatsapp.util.Log;

/* renamed from: X.1hE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C35101hE extends AbstractC35031h7 {
    public final C14850m9 A00;

    public C35101hE(C14850m9 r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC35031h7
    public void A02(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            Bundle bundle = new Bundle();
            bundle.putInt("app_badge_count", i);
            bundle.putString("app_badge_packageName", "com.whatsapp");
            try {
                context.getContentResolver().call(Uri.parse("content://com.android.badge/badge"), "setAppBadgeCount", (String) null, bundle);
            } catch (Exception e) {
                Log.e("cannot update badge", e);
            }
        }
    }
}
