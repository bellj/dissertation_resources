package X;

import androidx.core.view.inputmethod.EditorInfoCompat;
import com.whatsapp.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;

/* renamed from: X.1au  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C31661au {
    public C31321aM A00;

    public C31661au() {
        this.A00 = (C31321aM) C31321aM.A0E.A0T().A02();
    }

    public C31661au(C31321aM r1) {
        this.A00 = r1;
    }

    public C31661au(C31661au r2) {
        this.A00 = (C31321aM) r2.A00.A0T().A02();
    }

    public static void A00(Throwable th) {
        String str = "";
        Throwable th2 = th;
        while (true) {
            if (th2 != null) {
                if (th2 instanceof UnknownHostException) {
                    break;
                }
                th2 = th2.getCause();
            } else {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                th.printStackTrace(printWriter);
                printWriter.flush();
                str = stringWriter.toString();
                break;
            }
        }
        if (C27201Gk.A00 != null) {
            StringBuilder sb = new StringBuilder("SignalProtocolLogger (");
            sb.append("SessionRecordV2");
            sb.append("): ");
            sb.append(str);
            Log.w(sb.toString());
        }
    }

    public C31641as A01() {
        try {
            return new C31641as(this.A00.A06.A04());
        } catch (C31561ak e) {
            throw new AssertionError(e);
        }
    }

    public C31641as A02() {
        try {
            C31321aM r2 = this.A00;
            if ((r2.A00 & 4) == 4) {
                return new C31641as(r2.A07.A04());
            }
            return null;
        } catch (C31561ak e) {
            A00(e);
            return null;
        }
    }

    public C31491ad A03() {
        try {
            C31881bG r0 = this.A00.A0A;
            if (r0 == null) {
                r0 = C31881bG.A05;
            }
            return C31481ac.A00(r0.A02.A04());
        } catch (C31561ak e) {
            throw new AssertionError(e);
        }
    }

    public C31261aG A04() {
        C31321aM r2 = this.A00;
        C31881bG r0 = r2.A0A;
        if (r0 == null) {
            r0 = C31881bG.A05;
        }
        C31861bE r1 = r0.A04;
        if (r1 == null) {
            r1 = C31861bE.A03;
        }
        int i = r2.A04;
        if (i == 0) {
            i = 2;
        }
        return new C31261aG(AbstractC31231aD.A00(i), r1.A02.A04(), r1.A01);
    }

    public final C32021bU A05(C31491ad r6) {
        int i = 0;
        for (C31881bG r2 : this.A00.A09) {
            try {
            } catch (C31561ak e) {
                A00(e);
            }
            if (C31481ac.A00(r2.A02.A04()).equals(r6)) {
                return new C32021bU(r2, Integer.valueOf(i));
            }
            continue;
            i++;
        }
        return null;
    }

    public void A06() {
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        C31321aM r2 = (C31321aM) A0T.A00;
        r2.A00 |= 1;
        r2.A04 = 3;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A07(int i) {
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        C31321aM r1 = (C31321aM) A0T.A00;
        r1.A00 |= 512;
        r1.A01 = i;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A08(int i) {
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        C31321aM r1 = (C31321aM) A0T.A00;
        r1.A00 |= 256;
        r1.A03 = i;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A09(C31641as r5) {
        AnonymousClass1G4 A0T = this.A00.A0T();
        byte[] A00 = r5.A00.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C31321aM r1 = (C31321aM) A0T.A00;
        r1.A00 |= 2;
        r1.A06 = A01;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A0A(C31641as r5) {
        AnonymousClass1G4 A0T = this.A00.A0T();
        byte[] A00 = r5.A00.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        A0T.A03();
        C31321aM r1 = (C31321aM) A0T.A00;
        r1.A00 |= 4;
        r1.A07 = A01;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A0B(C31491ad r6, C31261aG r7) {
        C31871bF r3 = (C31871bF) C31861bE.A03.A0T();
        byte[] bArr = r7.A02;
        r3.A06(AbstractC27881Jp.A01(bArr, 0, bArr.length));
        r3.A05(r7.A00);
        C31891bH r32 = (C31891bH) C31881bG.A05.A0T();
        r32.A05((C31861bE) r3.A02());
        byte[] A00 = r6.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        r32.A03();
        C31881bG r1 = (C31881bG) r32.A00;
        r1.A00 |= 1;
        r1.A02 = A01;
        AbstractC27091Fz A02 = r32.A02();
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        C31321aM r2 = (C31321aM) A0T.A00;
        AnonymousClass1K6 r12 = r2.A09;
        if (!((AnonymousClass1K7) r12).A00) {
            r12 = AbstractC27091Fz.A0G(r12);
            r2.A09 = r12;
        }
        r12.add(A02);
        C31321aM r0 = (C31321aM) A0T.A02();
        this.A00 = r0;
        if (r0.A09.size() > 5) {
            AnonymousClass1G4 A0T2 = this.A00.A0T();
            A0T2.A03();
            C31321aM r33 = (C31321aM) A0T2.A00;
            AnonymousClass1K6 r13 = r33.A09;
            if (!((AnonymousClass1K7) r13).A00) {
                r13 = AbstractC27091Fz.A0G(r13);
                r33.A09 = r13;
            }
            r13.remove(0);
            this.A00 = (C31321aM) A0T2.A02();
        }
    }

    public void A0C(C31731b1 r6, C31261aG r7) {
        C31871bF r3 = (C31871bF) C31861bE.A03.A0T();
        byte[] bArr = r7.A02;
        r3.A06(AbstractC27881Jp.A01(bArr, 0, bArr.length));
        r3.A05(r7.A00);
        C31891bH r32 = (C31891bH) C31881bG.A05.A0T();
        byte[] A00 = r6.A01.A00();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(A00, 0, A00.length);
        r32.A03();
        C31881bG r1 = (C31881bG) r32.A00;
        r1.A00 |= 1;
        r1.A02 = A01;
        byte[] bArr2 = r6.A00.A00;
        AbstractC27881Jp A012 = AbstractC27881Jp.A01(bArr2, 0, bArr2.length);
        r32.A03();
        C31881bG r12 = (C31881bG) r32.A00;
        r12.A00 |= 2;
        r12.A01 = A012;
        r32.A05((C31861bE) r3.A02());
        AnonymousClass1G4 A0T = this.A00.A0T();
        A0T.A03();
        C31321aM r13 = (C31321aM) A0T.A00;
        r13.A0A = (C31881bG) r32.A02();
        r13.A00 |= 32;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A0D(C31251aF r5) {
        AnonymousClass1G4 A0T = this.A00.A0T();
        byte[] bArr = r5.A01;
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C31321aM r1 = (C31321aM) A0T.A00;
        r1.A00 |= 8;
        r1.A08 = A01;
        this.A00 = (C31321aM) A0T.A02();
    }

    public void A0E(byte[] bArr) {
        AnonymousClass1G4 A0T = this.A00.A0T();
        AbstractC27881Jp A01 = AbstractC27881Jp.A01(bArr, 0, bArr.length);
        A0T.A03();
        C31321aM r1 = (C31321aM) A0T.A00;
        r1.A00 |= EditorInfoCompat.MEMORY_EFFICIENT_TEXT_LENGTH;
        r1.A05 = A01;
        this.A00 = (C31321aM) A0T.A02();
    }
}
