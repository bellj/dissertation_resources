package X;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.qrcode.contactqr.ContactQrContactCardView;

/* renamed from: X.3Cz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63803Cz {
    public final /* synthetic */ C15370n3 A00;
    public final /* synthetic */ String A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ boolean A03;

    public C63803Cz(C15370n3 r1, String str, String str2, boolean z) {
        this.A00 = r1;
        this.A03 = z;
        this.A02 = str;
        this.A01 = str2;
    }

    public Bitmap A00(ActivityC13810kN r10) {
        ContactQrContactCardView contactQrContactCardView;
        Resources resources = r10.getResources();
        AnonymousClass025 A1V = r10.A1V();
        int i = ((LayoutInflater$Factory2C011505o) A1V).A01;
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 17) {
            A1V.A0B(1);
            contactQrContactCardView = new ContactQrContactCardView(r10);
            contactQrContactCardView.setGravity(17);
            contactQrContactCardView.setStyle(1);
            contactQrContactCardView.A02(this.A00, this.A03);
            contactQrContactCardView.setPrompt(this.A02);
            contactQrContactCardView.setQrCode(this.A01);
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setCornerRadius(resources.getDimension(R.dimen.contact_qr_corner_radius));
            gradientDrawable.setColor(resources.getColor(R.color.contact_qr_card_background));
            contactQrContactCardView.findViewById(R.id.qr_card).setBackground(gradientDrawable);
        } else {
            contactQrContactCardView = new ContactQrContactCardView(C41691tw.A01(r10));
            contactQrContactCardView.setGravity(17);
            contactQrContactCardView.setStyle(1);
            contactQrContactCardView.A02(this.A00, this.A03);
            contactQrContactCardView.setPrompt(this.A02);
            contactQrContactCardView.setQrCode(this.A01);
        }
        contactQrContactCardView.measure(View.MeasureSpec.makeMeasureSpec(resources.getDimensionPixelSize(R.dimen.contact_qr_share_card_width), 1073741824), View.MeasureSpec.makeMeasureSpec(resources.getDimensionPixelSize(R.dimen.contact_qr_share_card_height), 1073741824));
        contactQrContactCardView.layout(0, 0, contactQrContactCardView.getMeasuredWidth(), contactQrContactCardView.getMeasuredHeight());
        Bitmap createBitmap = Bitmap.createBitmap(contactQrContactCardView.getWidth(), contactQrContactCardView.getHeight(), Bitmap.Config.ARGB_8888);
        contactQrContactCardView.draw(new Canvas(createBitmap));
        if (i2 < 17) {
            A1V.A0B(i);
        }
        return createBitmap;
    }
}
