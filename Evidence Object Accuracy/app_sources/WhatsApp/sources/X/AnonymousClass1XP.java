package X;

import android.database.Cursor;

/* renamed from: X.1XP  reason: invalid class name */
/* loaded from: classes2.dex */
public abstract class AnonymousClass1XP extends AbstractC15340mz {
    public double A00;
    public double A01;
    public int A02;

    public AnonymousClass1XP(AnonymousClass1IS r2, byte b, long j) {
        super(r2, b, j);
        super.A02 = 1;
        this.A02 = 0;
    }

    public AnonymousClass1XP(AnonymousClass1IS r10, AnonymousClass1XP r11, byte b, long j, boolean z) {
        super(r11, r10, b, j, z);
        super.A02 = 1;
        this.A00 = r11.A00;
        this.A01 = r11.A01;
        this.A02 = r11.A02;
    }

    @Override // X.AbstractC15340mz
    public C16460p3 A0G() {
        C16460p3 A0G = super.A0G();
        AnonymousClass009.A05(A0G);
        return A0G;
    }

    public void A14(Cursor cursor) {
        this.A00 = cursor.getDouble(cursor.getColumnIndexOrThrow("latitude"));
        this.A01 = cursor.getDouble(cursor.getColumnIndexOrThrow("longitude"));
        A0G().A03(cursor.getBlob(cursor.getColumnIndexOrThrow("thumbnail")), true);
    }

    public void A15(Cursor cursor, C15570nT r4) {
        this.A00 = cursor.getDouble(cursor.getColumnIndexOrThrow("latitude"));
        this.A01 = cursor.getDouble(cursor.getColumnIndexOrThrow("longitude"));
        this.A02 = cursor.getInt(cursor.getColumnIndexOrThrow("map_download_status"));
    }
}
