package X;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import com.whatsapp.calling.videoparticipant.MaximizedParticipantVideoDialogFragment;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;

/* renamed from: X.3Nj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66383Nj implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ MaximizedParticipantVideoDialogFragment A00;

    public ViewTreeObserver$OnPreDrawListenerC66383Nj(MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment) {
        this.A00 = maximizedParticipantVideoDialogFragment;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        float f;
        MaximizedParticipantVideoDialogFragment maximizedParticipantVideoDialogFragment = this.A00;
        C12980iv.A1G(maximizedParticipantVideoDialogFragment.A07, this);
        int[] A07 = C13000ix.A07();
        maximizedParticipantVideoDialogFragment.A07.getLocationOnScreen(A07);
        maximizedParticipantVideoDialogFragment.A02 = maximizedParticipantVideoDialogFragment.A0H - A07[0];
        maximizedParticipantVideoDialogFragment.A03 = maximizedParticipantVideoDialogFragment.A0I - A07[1];
        float f2 = 1.0f;
        if (maximizedParticipantVideoDialogFragment.A07.getWidth() != 0) {
            f = ((float) maximizedParticipantVideoDialogFragment.A0G) / C12990iw.A02(maximizedParticipantVideoDialogFragment.A07);
        } else {
            f = 1.0f;
        }
        maximizedParticipantVideoDialogFragment.A01 = f;
        if (maximizedParticipantVideoDialogFragment.A07.getHeight() != 0) {
            f2 = ((float) maximizedParticipantVideoDialogFragment.A0F) / C12990iw.A03(maximizedParticipantVideoDialogFragment.A07);
        }
        maximizedParticipantVideoDialogFragment.A00 = f2;
        VideoCallParticipantView videoCallParticipantView = maximizedParticipantVideoDialogFragment.A07;
        AnonymousClass009.A03(videoCallParticipantView);
        AnonymousClass009.A03(maximizedParticipantVideoDialogFragment.A06);
        AnonymousClass009.A03(maximizedParticipantVideoDialogFragment.A05);
        videoCallParticipantView.setPivotX(0.0f);
        maximizedParticipantVideoDialogFragment.A07.setPivotY(0.0f);
        maximizedParticipantVideoDialogFragment.A07.setScaleX(maximizedParticipantVideoDialogFragment.A01);
        maximizedParticipantVideoDialogFragment.A07.setScaleY(maximizedParticipantVideoDialogFragment.A00);
        maximizedParticipantVideoDialogFragment.A07.setTranslationX((float) maximizedParticipantVideoDialogFragment.A02);
        maximizedParticipantVideoDialogFragment.A07.setTranslationY((float) maximizedParticipantVideoDialogFragment.A03);
        maximizedParticipantVideoDialogFragment.A07.animate().setDuration(250).scaleX(1.0f).scaleY(1.0f).translationX(0.0f).translationY(0.0f).setInterpolator(new DecelerateInterpolator(1.5f));
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(250);
        maximizedParticipantVideoDialogFragment.A06.startAnimation(alphaAnimation);
        if (maximizedParticipantVideoDialogFragment.A05.getVisibility() == 0) {
            maximizedParticipantVideoDialogFragment.A05.startAnimation(alphaAnimation);
        }
        View view = maximizedParticipantVideoDialogFragment.A04;
        AnonymousClass009.A03(view);
        view.setAlpha(0.0f);
        maximizedParticipantVideoDialogFragment.A04.animate().setDuration(250).alpha(0.4f);
        return true;
    }
}
