package X;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.PowerManager;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0Zo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07640Zo implements AbstractC11960h9, AbstractC12450hw, AbstractC11490gN {
    public static final String A09 = C06390Tk.A01("DelayMetCommandHandler");
    public int A00 = 0;
    public PowerManager.WakeLock A01;
    public boolean A02 = false;
    public final int A03;
    public final Context A04;
    public final C07620Zm A05;
    public final AnonymousClass0Zu A06;
    public final Object A07 = new Object();
    public final String A08;

    public C07640Zo(Context context, C07620Zm r4, String str, int i) {
        this.A04 = context;
        this.A03 = i;
        this.A05 = r4;
        this.A08 = str;
        this.A06 = new AnonymousClass0Zu(context, this, r4.A08);
    }

    public final void A00() {
        synchronized (this.A07) {
            this.A06.A00();
            AnonymousClass0SJ r0 = this.A05.A07;
            String str = this.A08;
            r0.A00(str);
            PowerManager.WakeLock wakeLock = this.A01;
            if (wakeLock != null && wakeLock.isHeld()) {
                C06390Tk.A00().A02(A09, String.format("Releasing wakelock %s for WorkSpec %s", this.A01, str), new Throwable[0]);
                this.A01.release();
            }
        }
    }

    public final void A01() {
        synchronized (this.A07) {
            if (this.A00 < 2) {
                this.A00 = 2;
                C06390Tk A00 = C06390Tk.A00();
                String str = A09;
                String str2 = this.A08;
                A00.A02(str, String.format("Stopping work for WorkSpec %s", str2), new Throwable[0]);
                Context context = this.A04;
                Intent intent = new Intent(context, SystemAlarmService.class);
                intent.setAction("ACTION_STOP_WORK");
                intent.putExtra("KEY_WORKSPEC_ID", str2);
                C07620Zm r4 = this.A05;
                int i = this.A03;
                RunnableC09950dm r1 = new RunnableC09950dm(intent, r4, i);
                Handler handler = r4.A03;
                handler.post(r1);
                if (r4.A04.A05(str2)) {
                    C06390Tk.A00().A02(str, String.format("WorkSpec %s needs to be rescheduled", str2), new Throwable[0]);
                    Intent intent2 = new Intent(context, SystemAlarmService.class);
                    intent2.setAction("ACTION_SCHEDULE_WORK");
                    intent2.putExtra("KEY_WORKSPEC_ID", str2);
                    handler.post(new RunnableC09950dm(intent2, r4, i));
                } else {
                    C06390Tk.A00().A02(str, String.format("Processor does not have WorkSpec %s. No need to reschedule ", str2), new Throwable[0]);
                }
            } else {
                C06390Tk.A00().A02(A09, String.format("Already stopped work for %s", this.A08), new Throwable[0]);
            }
        }
    }

    @Override // X.AbstractC12450hw
    public void AM8(List list) {
        String str = this.A08;
        if (list.contains(str)) {
            synchronized (this.A07) {
                if (this.A00 == 0) {
                    this.A00 = 1;
                    C06390Tk.A00().A02(A09, String.format("onAllConstraintsMet for %s", str), new Throwable[0]);
                    C07620Zm r2 = this.A05;
                    if (r2.A04.A04(null, str)) {
                        AnonymousClass0SJ r8 = r2.A07;
                        synchronized (r8.A00) {
                            C06390Tk.A00().A02(AnonymousClass0SJ.A05, String.format("Starting timer for %s", str), new Throwable[0]);
                            r8.A00(str);
                            RunnableC09690dM r5 = new RunnableC09690dM(r8, str);
                            r8.A02.put(str, r5);
                            r8.A01.put(str, this);
                            r8.A03.schedule(r5, 600000, TimeUnit.MILLISECONDS);
                        }
                    } else {
                        A00();
                    }
                } else {
                    C06390Tk.A00().A02(A09, String.format("Already started work for %s", str), new Throwable[0]);
                }
            }
        }
    }

    @Override // X.AbstractC12450hw
    public void AM9(List list) {
        A01();
    }

    @Override // X.AbstractC11960h9
    public void AQ1(String str, boolean z) {
        C06390Tk.A00().A02(A09, String.format("onExecuted %s, %s", str, Boolean.valueOf(z)), new Throwable[0]);
        A00();
        if (z) {
            Context context = this.A04;
            String str2 = this.A08;
            Intent intent = new Intent(context, SystemAlarmService.class);
            intent.setAction("ACTION_SCHEDULE_WORK");
            intent.putExtra("KEY_WORKSPEC_ID", str2);
            C07620Zm r2 = this.A05;
            r2.A03.post(new RunnableC09950dm(intent, r2, this.A03));
        }
        if (this.A02) {
            Intent intent2 = new Intent(this.A04, SystemAlarmService.class);
            intent2.setAction("ACTION_CONSTRAINTS_CHANGED");
            C07620Zm r22 = this.A05;
            r22.A03.post(new RunnableC09950dm(intent2, r22, this.A03));
        }
    }
}
