package X;

import java.util.HashMap;
import java.util.Map;

/* renamed from: X.10f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C230610f {
    public AnonymousClass10C A00;
    public final AbstractC15710nm A01;
    public final Map A02 = new HashMap();
    public final Map A03 = new HashMap();

    public C230610f(AbstractC15710nm r2) {
        this.A01 = r2;
    }

    public void A00(Exception exc) {
        Map map = this.A03;
        synchronized (map) {
            for (Map.Entry entry : map.entrySet()) {
                ((AnonymousClass1VC) entry.getValue()).A00(exc);
            }
            map.clear();
        }
    }
}
