package X;

import com.whatsapp.jid.UserJid;
import java.util.Arrays;

/* renamed from: X.5sm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C126375sm {
    public final AnonymousClass1V8 A00;

    public C126375sm(UserJid userJid, C128665wT r8) {
        C41141sy A0M = C117295Zj.A0M();
        C41141sy r5 = new C41141sy("account");
        C41141sy.A01(r5, "action", "request-merchant-payment-account-status");
        if (userJid == null) {
            AnonymousClass009.A07(String.format("Received null value for non-optional '%s'.", "account->merchant"));
        } else {
            r5.A04(new AnonymousClass1W9(userJid, "merchant"));
        }
        C117295Zj.A1H(r5, A0M);
        AnonymousClass1V8 r2 = r8.A00;
        A0M.A07(r2, C12960it.A0l());
        A0M.A09(r2, Arrays.asList(new String[0]), C12960it.A0l());
        this.A00 = A0M.A03();
    }
}
