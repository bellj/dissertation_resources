package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.0xh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C21610xh {
    public final C16510p9 A00;
    public final C19990v2 A01;
    public final C16490p7 A02;
    public final AnonymousClass134 A03;

    public C21610xh(C16510p9 r1, C19990v2 r2, C16490p7 r3, AnonymousClass134 r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = r4;
        this.A02 = r3;
    }

    public int A00(AbstractC14640lm r6) {
        int i = 0;
        String[] strArr = {String.valueOf(this.A00.A02(r6))};
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT COUNT(*)  FROM deleted_messages_ids_view WHERE chat_row_id = ? AND message_type != 8", strArr);
            if (A09 != null) {
                if (A09.moveToNext()) {
                    i = A09.getInt(0);
                    StringBuilder sb = new StringBuilder();
                    sb.append("msgstore/countmessagestodelete/count: ");
                    sb.append(i);
                    Log.i(sb.toString());
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("msgstore/countmessagestodelete/db no message for ");
                    sb2.append(r6);
                    Log.i(sb2.toString());
                }
                A09.close();
            } else {
                Log.e("msgstore/getmessagesatid/cursor is null");
            }
            A01.close();
            return i;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final C27441Hl A01(long j) {
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT _id, chat_row_id, block_size, deleted_message_row_id, deleted_starred_message_row_id, deleted_messages_remove_files, deleted_categories_message_row_id, deleted_categories_starred_message_row_id, deleted_categories_remove_files, deleted_message_categories, singular_message_delete_rows_id, delete_files_singular_delete FROM deleted_chat_job WHERE chat_row_id = ? ORDER BY _id DESC LIMIT 1", new String[]{Long.toString(j)});
            if (A09 != null) {
                if (A09.moveToFirst()) {
                    C27441Hl A02 = A02(A09);
                    A09.close();
                    A01.close();
                    return A02;
                }
                A09.close();
            }
            A01.close();
            return null;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public final C27441Hl A02(Cursor cursor) {
        ArrayList arrayList;
        boolean z = true;
        long j = cursor.getLong(1);
        AbstractC14640lm A05 = this.A00.A05(j);
        if (A05 == null) {
            return null;
        }
        long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("deleted_message_row_id"));
        if (j2 == 0) {
            j2 = Long.MIN_VALUE;
        }
        long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("deleted_starred_message_row_id"));
        if (j3 == 0) {
            j3 = Long.MIN_VALUE;
        }
        long j4 = cursor.getLong(cursor.getColumnIndexOrThrow("deleted_categories_message_row_id"));
        if (j4 == 0) {
            j4 = Long.MIN_VALUE;
        }
        long j5 = cursor.getLong(cursor.getColumnIndexOrThrow("deleted_categories_starred_message_row_id"));
        if (j5 == 0) {
            j5 = Long.MIN_VALUE;
        }
        long j6 = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
        int i = cursor.getInt(cursor.getColumnIndexOrThrow("block_size"));
        boolean z2 = false;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("deleted_messages_remove_files")) != 0) {
            z2 = true;
        }
        boolean z3 = false;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("delete_files_singular_delete")) != 0) {
            z3 = true;
        }
        if (cursor.getInt(cursor.getColumnIndexOrThrow("deleted_categories_remove_files")) == 0) {
            z = false;
        }
        String string = cursor.getString(cursor.getColumnIndexOrThrow("deleted_message_categories"));
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow("singular_message_delete_rows_id"));
        if (string2 == null || string2.isEmpty()) {
            arrayList = null;
        } else {
            String[] split = string2.replace("\"", "").split(",");
            arrayList = new ArrayList();
            for (String str : split) {
                arrayList.add(Long.valueOf(str));
            }
        }
        return new C27441Hl(A05, string, arrayList, i, j6, j, j2, j3, j4, j5, z2, z3, z);
    }

    public C27441Hl A03(AbstractC14640lm r45, Long l, List list, int i, boolean z, boolean z2, boolean z3) {
        long j;
        long max;
        boolean z4;
        long j2;
        long j3;
        long j4;
        String str;
        boolean z5 = z2;
        C27441Hl r24 = null;
        C16490p7 r7 = this.A02;
        C16310on A02 = r7.A02();
        try {
            AnonymousClass1Lx A00 = A02.A00();
            C16510p9 r5 = this.A00;
            long A022 = r5.A02(r45);
            if (l != null) {
                max = l.longValue();
            } else {
                AnonymousClass1PE A06 = this.A01.A06(r45);
                long A062 = this.A03.A06(r45);
                if (A06 != null) {
                    j = A06.A0N;
                } else {
                    j = Long.MIN_VALUE;
                }
                max = Math.max(A062, j);
            }
            if (TextUtils.isEmpty(null)) {
                j4 = z ? max : Long.MIN_VALUE;
                j3 = Long.MIN_VALUE;
                j2 = Long.MIN_VALUE;
                z4 = false;
            } else {
                j2 = z ? max : Long.MIN_VALUE;
                z4 = z5;
                j3 = max;
                z5 = false;
                max = Long.MIN_VALUE;
                j4 = Long.MIN_VALUE;
            }
            C27441Hl r0 = new C27441Hl(r45, null, list, i, -1, A022, max, j4, j3, j2, z5, z3, z4);
            C16310on A023 = r7.A02();
            AnonymousClass1Lx A002 = A023.A00();
            long j5 = r0.A01;
            AbstractC14640lm A05 = r5.A05(j5);
            if (A05 != null) {
                int i2 = r0.A00;
                AnonymousClass1PE A063 = this.A01.A06(A05);
                C27441Hl A01 = A01(j5);
                if (A01 == null || TextUtils.isEmpty(null) || TextUtils.isEmpty(A01.A08)) {
                    long j6 = r0.A04;
                    long j7 = r0.A05;
                    boolean z6 = r0.A0C;
                    long j8 = r0.A02;
                    long j9 = r0.A03;
                    boolean z7 = r0.A0A;
                    String str2 = null;
                    List list2 = r0.A09;
                    if (list2 == null) {
                        str = null;
                    } else {
                        String[] strArr = new String[list2.size()];
                        for (int i3 = 0; i3 < list2.size(); i3++) {
                            strArr[i3] = ((Long) list2.get(i3)).toString();
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append("\"");
                        sb.append(AnonymousClass1US.A09("\",\"", strArr));
                        sb.append("\"");
                        str = sb.toString();
                    }
                    boolean z8 = r0.A0B;
                    if (A01 != null) {
                        if (TextUtils.isEmpty(null) || TextUtils.isEmpty(A01.A08)) {
                            str2 = A01.A08;
                            if (!TextUtils.isEmpty(str2)) {
                                z7 = A01.A0A;
                            } else {
                                str2 = null;
                            }
                            j6 = Math.max(j6, A01.A04);
                            j7 = Math.max(j7, A01.A05);
                            j8 = Math.max(j8, A01.A02);
                            j9 = Math.max(j9, A01.A03);
                        }
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("chat_row_id", Long.valueOf(j5));
                    contentValues.put("block_size", Integer.valueOf(i2));
                    contentValues.put("deleted_message_row_id", Long.valueOf(j6));
                    contentValues.put("deleted_starred_message_row_id", Long.valueOf(j7));
                    contentValues.put("deleted_messages_remove_files", Boolean.valueOf(z6));
                    contentValues.put("deleted_categories_message_row_id", Long.valueOf(j8));
                    contentValues.put("deleted_categories_starred_message_row_id", Long.valueOf(j9));
                    contentValues.put("deleted_message_categories", str2);
                    contentValues.put("deleted_categories_remove_files", Boolean.valueOf(z7));
                    contentValues.put("delete_files_singular_delete", Boolean.valueOf(z8));
                    C30021Vq.A04(contentValues, "singular_message_delete_rows_id", str);
                    C16330op r52 = A023.A03;
                    long A024 = r52.A02(contentValues, "deleted_chat_job");
                    if (A01 != null) {
                        r52.A01("deleted_chat_job", "_id = ?", new String[]{Long.toString(A01.A06)});
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("msgstore/deletemsgs/mark jid:");
                    sb2.append(A05);
                    sb2.append(" lastDeletedMessageSortId:");
                    sb2.append(j6);
                    sb2.append(" lastDeletedStarredMessageSortId:");
                    sb2.append(j7);
                    Log.i(sb2.toString());
                    if (A024 > 0 && A063 != null) {
                        synchronized (A063) {
                            A063.A0E = j6;
                            A063.A0F = j7;
                            A063.A0C = j8;
                            A063.A0D = j9;
                            A063.A0d = str2;
                        }
                    }
                    A002.A00();
                    r24 = new C27441Hl(A05, str2, list2, i2, A024, j5, j6, j7, j8, j9, z6, z8, z7);
                    A002.close();
                    A023.close();
                    A00.A00();
                    A00.close();
                    A02.close();
                    return r24;
                }
            }
            A002.close();
            A023.close();
            A00.close();
            A02.close();
            return r24;
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public Set A04() {
        HashSet hashSet = new HashSet();
        C16310on A01 = this.A02.get();
        try {
            Cursor A09 = A01.A03.A09("SELECT DISTINCT chat_row_id FROM deleted_chat_job", null);
            if (A09 != null) {
                while (A09.moveToNext()) {
                    AbstractC14640lm A05 = this.A00.A05(A09.getLong(0));
                    if (A05 != null) {
                        hashSet.add(A05);
                    }
                }
            }
            if (A09 != null) {
                A09.close();
            }
            A01.close();
            return hashSet;
        } catch (Throwable th) {
            try {
                A01.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    public void A05(C27441Hl r11) {
        C19990v2 r0 = this.A01;
        AbstractC14640lm r4 = r11.A07;
        AnonymousClass1PE A06 = r0.A06(r4);
        C16310on A02 = this.A02.A02();
        try {
            A02.A03.A01("deleted_chat_job", "_id = ?", new String[]{String.valueOf(r11.A06)});
            if (A06 != null && A01(r11.A01) == null) {
                synchronized (A06) {
                    A06.A0E = Long.MIN_VALUE;
                    A06.A0F = Long.MIN_VALUE;
                    A06.A0C = Long.MIN_VALUE;
                    A06.A0D = Long.MIN_VALUE;
                    A06.A0d = null;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("msgstore/deletemsgs/unmark jid:");
            sb.append(r4);
            Log.i(sb.toString());
            A02.close();
        } catch (Throwable th) {
            try {
                A02.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }
}
