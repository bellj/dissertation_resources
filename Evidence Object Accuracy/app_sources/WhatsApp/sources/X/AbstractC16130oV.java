package X;

import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.io.File;

/* renamed from: X.0oV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC16130oV extends AbstractC15340mz {
    public int A00;
    public long A01;
    public C16150oX A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public final Object A0B = new Object();
    public volatile C38101nW A0C;

    public AbstractC16130oV(C16150oX r10, AnonymousClass1IS r11, AbstractC16130oV r12, byte b, long j, boolean z) {
        super(r12, r11, b, j, z);
        this.A02 = r10;
        this.A03 = r12.A03;
        this.A00 = r12.A00;
        this.A04 = r12.A04;
        this.A05 = r12.A05;
        this.A06 = r12.A06;
        this.A07 = r12.A07;
        this.A01 = r12.A01;
        this.A08 = r12.A08;
        this.A09 = r12.A09;
        C38101nW A14 = r12.A14();
        if (A14 == null) {
            return;
        }
        if (A14.A04()) {
            C38101nW A142 = A14();
            AnonymousClass009.A05(A142);
            A142.A03(A14.A05(), A14.A06());
            return;
        }
        StringBuilder sb = new StringBuilder("FMessageVideo/Cloned message should have sidecar, but original message doesn't have it: sidecar=");
        sb.append(this.A0C);
        Log.e(sb.toString());
    }

    public AbstractC16130oV(AnonymousClass1IS r2, byte b, long j) {
        super(r2, b, j);
    }

    public C38101nW A14() {
        if (this.A0C == null && C38101nW.A00(C14370lK.A01(this.A0y, super.A08))) {
            synchronized (this.A0B) {
                if (this.A0C == null) {
                    this.A0C = new C38101nW(this);
                }
            }
        }
        return this.A0C;
    }

    public String A15() {
        C28891Pk r0;
        if (this instanceof AnonymousClass1XU) {
            r0 = ((AnonymousClass1XU) this).A00;
        } else if (this instanceof AnonymousClass1XS) {
            r0 = ((AnonymousClass1XS) this).A00;
        } else if (!(this instanceof AnonymousClass1XQ)) {
            return this.A03;
        } else {
            r0 = ((AnonymousClass1XQ) this).A00;
        }
        return r0.A01;
    }

    public String A16() {
        if (!(this instanceof C16440p1)) {
            return this.A07;
        }
        String str = this.A07;
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        String A00 = AnonymousClass14P.A00(this.A06);
        if (TextUtils.isEmpty(A00)) {
            return A15();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(A15());
        sb.append(".");
        sb.append(A00);
        return sb.toString();
    }

    public void A17(Cursor cursor, C16150oX r4) {
        this.A02 = r4;
        A0m(cursor.getString(cursor.getColumnIndexOrThrow("multicast_id")));
        this.A06 = cursor.getString(cursor.getColumnIndexOrThrow("mime_type"));
        this.A08 = cursor.getString(cursor.getColumnIndexOrThrow("message_url"));
        this.A01 = cursor.getLong(cursor.getColumnIndexOrThrow("file_length"));
        this.A07 = cursor.getString(cursor.getColumnIndexOrThrow("media_name"));
        this.A05 = cursor.getString(cursor.getColumnIndexOrThrow("file_hash"));
        this.A00 = cursor.getInt(cursor.getColumnIndexOrThrow("media_duration"));
        this.A04 = cursor.getString(cursor.getColumnIndexOrThrow("enc_file_hash"));
    }

    public void A18(Cursor cursor, C16150oX r5) {
        this.A02 = r5;
        this.A06 = cursor.getString(cursor.getColumnIndexOrThrow("mime_type"));
        this.A08 = cursor.getString(cursor.getColumnIndexOrThrow("message_url"));
        this.A01 = cursor.getLong(cursor.getColumnIndexOrThrow("file_length"));
        this.A07 = cursor.getString(cursor.getColumnIndexOrThrow("media_name"));
        this.A05 = cursor.getString(cursor.getColumnIndexOrThrow("file_hash"));
        this.A00 = cursor.getInt(cursor.getColumnIndexOrThrow("media_duration"));
        this.A04 = cursor.getString(cursor.getColumnIndexOrThrow("enc_file_hash"));
        C16460p3 A0G = A0G();
        if (A0G != null) {
            A0G.A03(cursor.getBlob(cursor.getColumnIndexOrThrow("thumbnail")), true);
        }
    }

    public void A19(String str) {
        StringBuilder sb;
        String str2;
        String obj;
        AnonymousClass1IS r4 = this.A0z;
        if (TextUtils.isEmpty(str)) {
            StringBuilder sb2 = new StringBuilder("MessageUtil/isValidIncomingUrl/error empty media url received. message.key=");
            sb2.append(r4);
            obj = sb2.toString();
        } else {
            Uri parse = Uri.parse(str);
            if (!"https".equalsIgnoreCase(parse.getScheme())) {
                sb = new StringBuilder();
                str2 = "MessageUtil/isValidIncomingUrl/error invalid scheme on received media url; url=";
            } else if (TextUtils.isEmpty(parse.getHost()) || !parse.getHost().endsWith(".whatsapp.net")) {
                sb = new StringBuilder();
                str2 = "MessageUtil/isValidIncomingUrl/error invalid host on received media url; url=";
            } else {
                this.A08 = str;
                return;
            }
            sb.append(str2);
            sb.append(str);
            sb.append("; message.key=");
            sb.append(r4);
            obj = sb.toString();
        }
        Log.w(obj);
        throw new C43271wi(15);
    }

    public boolean A1A() {
        File file;
        C16150oX r0 = this.A02;
        return (r0 == null || (file = r0.A0F) == null || !file.canRead()) ? false : true;
    }
}
