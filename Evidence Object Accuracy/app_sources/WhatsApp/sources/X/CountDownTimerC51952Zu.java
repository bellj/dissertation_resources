package X;

import android.os.CountDownTimer;
import android.widget.ProgressBar;
import com.whatsapp.R;
import com.whatsapp.registration.VerifyTwoFactorAuth;

/* renamed from: X.2Zu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class CountDownTimerC51952Zu extends CountDownTimer {
    public final /* synthetic */ long A00;
    public final /* synthetic */ VerifyTwoFactorAuth A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CountDownTimerC51952Zu(VerifyTwoFactorAuth verifyTwoFactorAuth, long j, long j2) {
        super(j, 1000);
        this.A01 = verifyTwoFactorAuth;
        this.A00 = j2;
    }

    @Override // android.os.CountDownTimer
    public void onFinish() {
        VerifyTwoFactorAuth verifyTwoFactorAuth = this.A01;
        CountDownTimer countDownTimer = verifyTwoFactorAuth.A04;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            verifyTwoFactorAuth.A04 = null;
        }
        verifyTwoFactorAuth.A08.setEnabled(true);
        verifyTwoFactorAuth.A05.setProgress(100);
        verifyTwoFactorAuth.A06.setVisibility(4);
        verifyTwoFactorAuth.A06.setText(C12960it.A0X(verifyTwoFactorAuth, C12990iw.A0j(), new Object[1], 0, R.string.two_factor_auth_enter_code_description_with_placeholder));
        C12990iw.A11(verifyTwoFactorAuth.getPreferences(0).edit(), "code_retry_time");
    }

    @Override // android.os.CountDownTimer
    public void onTick(long j) {
        ProgressBar progressBar = this.A01.A05;
        long j2 = this.A00;
        progressBar.setProgress((int) ((((double) (j2 - j)) * 100.0d) / ((double) j2)));
    }
}
