package X;

/* renamed from: X.1uC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C41811uC extends AbstractC16110oT {
    public Double A00;
    public Integer A01;

    public C41811uC() {
        super(2958, new AnonymousClass00E(1, 1, 1), 2, 248614979);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A01);
        r3.Abe(2, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        StringBuilder sb = new StringBuilder("WamTestAnonymousDailyId {");
        Integer num = this.A01;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psTestEnumField", obj);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "psTestFloatField", this.A00);
        sb.append("}");
        return sb.toString();
    }
}
