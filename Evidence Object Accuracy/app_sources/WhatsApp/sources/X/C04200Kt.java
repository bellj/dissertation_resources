package X;

import android.os.Build;
import android.view.ScaleGestureDetector;

/* renamed from: X.0Kt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C04200Kt {
    public static void A00(ScaleGestureDetector scaleGestureDetector) {
        if (Build.VERSION.SDK_INT >= 19) {
            C04190Ks.A00(scaleGestureDetector, false);
        }
    }
}
