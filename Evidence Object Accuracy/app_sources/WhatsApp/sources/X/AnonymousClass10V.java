package X;

import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;

/* renamed from: X.10V  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass10V {
    public final C22260yn A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C15550nR A03;
    public final AnonymousClass10S A04;
    public final AnonymousClass10T A05;
    public final C14820m6 A06;
    public final C15650ng A07;
    public final AnonymousClass10Y A08;
    public final C20700wB A09;

    public AnonymousClass10V(C22260yn r1, C14900mE r2, C15570nT r3, C15550nR r4, AnonymousClass10S r5, AnonymousClass10T r6, C14820m6 r7, C15650ng r8, AnonymousClass10Y r9, C20700wB r10) {
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A04 = r5;
        this.A07 = r8;
        this.A09 = r10;
        this.A08 = r9;
        this.A06 = r7;
        this.A05 = r6;
    }

    public void A00(C15370n3 r3, int i, int i2) {
        boolean A0F = this.A02.A0F(r3.A0D);
        r3.A04 = i;
        r3.A05 = i2;
        if (A0F) {
            this.A06.A00.edit().putInt("profile_photo_full_id", i).putInt("profile_photo_thumb_id", i2).apply();
            return;
        }
        r3.A0A = System.currentTimeMillis();
        this.A03.A0M(r3);
    }

    public void A01(C15370n3 r3, byte[] bArr, byte[] bArr2) {
        if (bArr != null) {
            try {
                File A00 = this.A05.A00(r3);
                if (A00 != null) {
                    C14350lI.A0F(A00, bArr);
                } else {
                    Log.e("ContactPhotoUpdater/updatePhotoFiles/no full photo file when expected");
                }
            } catch (IOException e) {
                Log.e("ContactPhotoUpdater/updatePhotoFiles", e);
                return;
            }
        }
        if (bArr2 != null) {
            File A01 = this.A05.A01(r3);
            if (A01 != null) {
                C14350lI.A0F(A01, bArr2);
            } else {
                Log.e("ContactPhotoUpdater/updatePhotoFiles/no thumb photo file when expected");
            }
        }
    }

    public void A02(AbstractC14640lm r4) {
        this.A00.A01(new RunnableBRunnable0Shape2S0200000_I0_2(this, 34, r4));
    }
}
