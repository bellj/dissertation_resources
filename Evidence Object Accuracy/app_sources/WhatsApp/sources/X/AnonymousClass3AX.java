package X;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.components.button.ThumbnailButton;

/* renamed from: X.3AX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3AX {
    public static void A00(View view, int i, int i2, int i3, int i4) {
        C42631vX.A00(view);
        ThumbnailButton thumbnailButton = (ThumbnailButton) AnonymousClass028.A0D(view, R.id.contactpicker_row_photo);
        thumbnailButton.setImageDrawable(AnonymousClass2GE.A01(view.getContext(), i, i2));
        thumbnailButton.setScaleType(ImageView.ScaleType.CENTER);
        thumbnailButton.setBackgroundResource(i3);
        thumbnailButton.A07 = true;
        TextView A0J = C12960it.A0J(view, R.id.contactpicker_row_name);
        C27531Hw.A06(A0J);
        A0J.setText(i4);
        view.findViewById(R.id.contactpicker_row_status).setVisibility(8);
    }
}
