package X;

import com.whatsapp.util.Log;

/* renamed from: X.1zZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44801zZ extends AbstractC44761zV {
    public C16240og A00;
    public C22730zY A01;
    public C17220qS A02;
    public boolean A03 = true;
    public boolean A04 = true;
    public boolean A05 = true;
    public boolean A06 = true;
    public final Object A07 = new Object();

    @Override // X.AbstractC44761zV
    public String toString() {
        return "backup-condition-for-backup-worker";
    }

    public C44801zZ(C16240og r2, C22730zY r3, C17220qS r4) {
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r3;
    }

    public void A06() {
        synchronized (this.A07) {
            this.A06 = true;
            this.A05 = true;
            this.A03 = true;
            this.A04 = true;
            C22730zY r1 = this.A01;
            if (!r1.A09) {
                Log.i("google-backup-worker-task-condition/refreshConditions sd card is not available");
                this.A05 = false;
            }
            if (!r1.A0g.get()) {
                Log.i("google-backup-worker-task-condition/refreshConditions network is not available for backup");
                this.A04 = false;
            }
            if (!r1.A05) {
                Log.i("google-backup-worker-task-condition/refreshConditions battery is not available for backup");
                this.A03 = false;
            }
            if (this.A00.A04 != 2) {
                Log.i("google-backup-worker-task-condition/refreshConditions xmpp is not connected");
                this.A06 = false;
            }
        }
    }
}
