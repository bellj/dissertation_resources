package X;

/* renamed from: X.5yM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129825yM {
    public AnonymousClass60L A00;
    public AnonymousClass60L A01;

    public C129825yM(AnonymousClass60L r1, AnonymousClass60L r2) {
        this.A01 = r1;
        this.A00 = r2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C129825yM r5 = (C129825yM) obj;
            if (!this.A01.equals(r5.A01) || !this.A00.equals(r5.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] A1a = C12980iv.A1a();
        A1a[0] = this.A01;
        return C12960it.A06(this.A00, A1a);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("Range{minDate=");
        A0k.append(this.A01);
        A0k.append(", maxDate=");
        A0k.append(this.A00);
        return C12970iu.A0v(A0k);
    }
}
