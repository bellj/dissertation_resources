package X;

import com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity;

/* renamed from: X.5dR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119265dR extends ActivityC13790kL {
    public AbstractActivityC119265dR() {
        C117295Zj.A0p(this, 99);
    }

    public static AnonymousClass18P A02(AnonymousClass2FL r1, AnonymousClass01J r2, PaymentTransactionDetailsListActivity paymentTransactionDetailsListActivity, AnonymousClass01N r4) {
        paymentTransactionDetailsListActivity.A0J = (AnonymousClass18S) r4.get();
        paymentTransactionDetailsListActivity.A0L = (C22710zW) r2.AF7.get();
        paymentTransactionDetailsListActivity.A0K = (C17900ra) r2.AF5.get();
        paymentTransactionDetailsListActivity.A0R = (AnonymousClass1AB) r2.AKI.get();
        paymentTransactionDetailsListActivity.A0H = (AnonymousClass18T) r2.AE9.get();
        paymentTransactionDetailsListActivity.A0E = (AnonymousClass109) r2.AI8.get();
        paymentTransactionDetailsListActivity.A0N = (C22460z7) r2.AEF.get();
        paymentTransactionDetailsListActivity.A0P = (C129395xe) r2.AEy.get();
        paymentTransactionDetailsListActivity.A0U = r1.A0L();
        paymentTransactionDetailsListActivity.A0Q = (AnonymousClass14X) r2.AFM.get();
        paymentTransactionDetailsListActivity.A05 = (AnonymousClass1AA) r2.ADr.get();
        return (AnonymousClass18P) r2.AEn.get();
    }

    public static void A03(AnonymousClass01J r1, C249317l r2, AbstractActivityC121465iB r3) {
        ((ActivityC13790kL) r3).A08 = r2;
        r3.A00 = (C130105yo) r1.ADZ.get();
        r3.A01 = (C128375w0) r1.ADW.get();
    }

    public static void A09(AnonymousClass01J r1, C249317l r2, PaymentTransactionDetailsListActivity paymentTransactionDetailsListActivity) {
        ((ActivityC13790kL) paymentTransactionDetailsListActivity).A08 = r2;
        paymentTransactionDetailsListActivity.A03 = (C18790t3) r1.AJw.get();
        paymentTransactionDetailsListActivity.A0G = (AnonymousClass13H) r1.ABY.get();
        paymentTransactionDetailsListActivity.A0S = (C253118x) r1.AAW.get();
        paymentTransactionDetailsListActivity.A02 = (C239613r) r1.AI9.get();
        paymentTransactionDetailsListActivity.A04 = (C16170oZ) r1.AM4.get();
        paymentTransactionDetailsListActivity.A00 = (AnonymousClass18U) r1.AAU.get();
    }

    public static void A0A(AnonymousClass01J r1, PaymentTransactionDetailsListActivity paymentTransactionDetailsListActivity) {
        paymentTransactionDetailsListActivity.A0D = (C241414j) r1.AEr.get();
        paymentTransactionDetailsListActivity.A01 = (AnonymousClass19Y) r1.AI6.get();
        paymentTransactionDetailsListActivity.A09 = (C21270x9) r1.A4A.get();
        paymentTransactionDetailsListActivity.A08 = (C15610nY) r1.AMe.get();
        paymentTransactionDetailsListActivity.A07 = (C15550nR) r1.A45.get();
        paymentTransactionDetailsListActivity.A06 = (AnonymousClass130) r1.A41.get();
        paymentTransactionDetailsListActivity.A0C = (AnonymousClass018) r1.ANb.get();
        paymentTransactionDetailsListActivity.A0M = (C17070qD) r1.AFC.get();
        paymentTransactionDetailsListActivity.A0T = (AnonymousClass19O) r1.ACO.get();
        paymentTransactionDetailsListActivity.A0B = (C15890o4) r1.AN1.get();
        paymentTransactionDetailsListActivity.A0F = (C22370yy) r1.AB0.get();
    }

    public static void A0B(ActivityC13790kL r2, AbstractC118045bB r3) {
        r3.A06(r2, r2, new C125985s8(0));
    }
}
