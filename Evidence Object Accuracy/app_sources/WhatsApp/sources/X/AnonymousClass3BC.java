package X;

import com.whatsapp.util.Log;
import org.json.JSONException;

/* renamed from: X.3BC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3BC {
    public final String A00;

    public AnonymousClass3BC(String str) {
        String string;
        if (str != null) {
            try {
                string = C13000ix.A05(str).getString("allowed_product_type");
            } catch (JSONException e) {
                Log.e(C12960it.A0d(e.getMessage(), C12960it.A0k("OrderDetailsQuickPayConfig/init failed to parse config json: ")));
            }
            this.A00 = string;
        }
        string = "none";
        this.A00 = string;
    }
}
