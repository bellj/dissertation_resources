package X;

import java.util.Arrays;

/* renamed from: X.4y1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107834y1 implements AnonymousClass5VZ {
    public int A00;
    public int A01;
    public int A02;
    public AnonymousClass4IL[] A03;
    public final int A04;
    public final boolean A05 = true;
    public final AnonymousClass4IL[] A06;

    public C107834y1(int i) {
        this.A04 = i;
        this.A01 = 0;
        this.A03 = new AnonymousClass4IL[100];
        this.A06 = new AnonymousClass4IL[1];
    }

    public synchronized void A00(int i) {
        boolean A1Y = C12990iw.A1Y(i, this.A02);
        this.A02 = i;
        if (A1Y) {
            Af6();
        }
    }

    @Override // X.AnonymousClass5VZ
    public synchronized void Aa5(AnonymousClass4IL[] r7) {
        int i = this.A01;
        int length = r7.length;
        int i2 = length + i;
        AnonymousClass4IL[] r4 = this.A03;
        int length2 = r4.length;
        if (i2 >= length2) {
            r4 = (AnonymousClass4IL[]) Arrays.copyOf(r4, Math.max(length2 << 1, i2));
            this.A03 = r4;
        }
        for (AnonymousClass4IL r2 : r7) {
            int i3 = this.A01;
            this.A01 = i3 + 1;
            r4[i3] = r2;
        }
        this.A00 -= length;
        notifyAll();
    }

    @Override // X.AnonymousClass5VZ
    public synchronized void Af6() {
        int i = this.A02;
        int i2 = this.A04;
        int max = Math.max(0, (((i + i2) - 1) / i2) - this.A00);
        int i3 = this.A01;
        if (max < i3) {
            Arrays.fill(this.A03, max, i3, (Object) null);
            this.A01 = max;
        }
    }
}
