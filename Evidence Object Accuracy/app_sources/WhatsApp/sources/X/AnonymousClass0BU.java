package X;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.CheckedTextView;

/* renamed from: X.0BU  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0BU extends CheckedTextView {
    public static final int[] A01 = {16843016};
    public final AnonymousClass086 A00;

    public AnonymousClass0BU(Context context, AttributeSet attributeSet) {
        super(AnonymousClass083.A00(context), attributeSet, 16843720);
        AnonymousClass084.A03(getContext(), this);
        AnonymousClass086 r0 = new AnonymousClass086(this);
        this.A00 = r0;
        r0.A0A(attributeSet, 16843720);
        r0.A02();
        C013406h A00 = C013406h.A00(getContext(), attributeSet, A01, 16843720, 0);
        setCheckMarkDrawable(A00.A02(0));
        A00.A04();
    }

    @Override // android.widget.CheckedTextView, android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass086 r0 = this.A00;
        if (r0 != null) {
            r0.A02();
        }
    }

    @Override // android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        AnonymousClass08D.A00(this, editorInfo, onCreateInputConnection);
        return onCreateInputConnection;
    }

    @Override // android.widget.CheckedTextView
    public void setCheckMarkDrawable(int i) {
        setCheckMarkDrawable(C012005t.A01().A04(getContext(), i));
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(AnonymousClass04D.A02(callback, this));
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        AnonymousClass086 r0 = this.A00;
        if (r0 != null) {
            r0.A05(context, i);
        }
    }
}
