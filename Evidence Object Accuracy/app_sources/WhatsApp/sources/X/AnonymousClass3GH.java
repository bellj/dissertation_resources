package X;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONObject;

/* renamed from: X.3GH  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3GH {
    public static String A00(String str, JSONObject jSONObject) {
        String optString = jSONObject.optString(str);
        if (TextUtils.isEmpty(optString) || optString.equalsIgnoreCase("null")) {
            return null;
        }
        return optString;
    }

    public static boolean A01(Set set, JSONObject jSONObject) {
        HashSet A12 = C12970iu.A12();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String A0x = C12970iu.A0x(it);
            if (!jSONObject.has(A0x)) {
                A12.add(A0x);
            }
        }
        return A12.isEmpty();
    }
}
