package X;

import android.content.SharedPreferences;
import android.os.ConditionVariable;
import java.util.Map;

/* renamed from: X.3Xi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68943Xi implements AbstractC28461Nh {
    public final /* synthetic */ ConditionVariable A00;
    public final /* synthetic */ C251718j A01;

    @Override // X.AbstractC28461Nh
    public /* synthetic */ void AOt(long j) {
    }

    public C68943Xi(ConditionVariable conditionVariable, C251718j r2) {
        this.A01 = r2;
        this.A00 = conditionVariable;
    }

    @Override // X.AbstractC28461Nh
    public void AOZ(String str) {
    }

    @Override // X.AbstractC28461Nh
    public void APq(String str) {
        SharedPreferences.Editor putInt;
        C251718j r4 = this.A01;
        SharedPreferences sharedPreferences = r4.A06.A01.A00;
        int A01 = C12970iu.A01(sharedPreferences, "qpl_failed_upload_count") + 1;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (A01 == 0) {
            putInt = edit.remove("qpl_failed_upload_count");
        } else {
            putInt = edit.putInt("qpl_failed_upload_count", A01);
        }
        putInt.apply();
        if (A01 >= 5) {
            AbstractC20460vn r2 = r4.A05;
            StringBuilder A0j = C12960it.A0j(str);
            A0j.append(" (");
            A0j.append(A01);
            r2.A9d(C12960it.A0d(")", A0j));
        }
        r4.A00 = false;
        this.A00.open();
    }

    @Override // X.AbstractC28461Nh
    public void AV9(String str, Map map) {
        C251718j r1 = this.A01;
        r1.A00 = true;
        C12990iw.A11(C12960it.A08(r1.A06.A01), "qpl_failed_upload_count");
        this.A00.open();
    }
}
