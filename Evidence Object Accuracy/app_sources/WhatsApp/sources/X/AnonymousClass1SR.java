package X;

import android.app.ActivityManager;
import android.os.DeadObjectException;
import android.os.Process;
import com.whatsapp.util.Log;
import java.util.LinkedList;
import java.util.List;

/* renamed from: X.1SR  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1SR extends AnonymousClass1MS {
    public boolean A00;
    public boolean A01;
    public final long A02;
    public final AnonymousClass01d A03;
    public final Object A04 = new Object();
    public volatile AnonymousClass1SQ A05;
    public final /* synthetic */ AnonymousClass1SP A06;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AnonymousClass1SR(AnonymousClass1SP r2, AnonymousClass1SQ r3, AnonymousClass01d r4, long j) {
        super("ProcessAnrErrorMonitorThread");
        this.A06 = r2;
        this.A05 = r3;
        this.A02 = j;
        this.A00 = true;
        this.A03 = r4;
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        AnonymousClass1SP r3;
        AnonymousClass1SQ r2;
        int i;
        boolean z;
        AnonymousClass4JP r6 = new AnonymousClass4JP();
        do {
            try {
                r3 = this.A06;
                ActivityManager A03 = this.A03.A03();
                AnonymousClass009.A05(A03);
                List<ActivityManager.ProcessErrorStateInfo> processesInErrorState = A03.getProcessesInErrorState();
                LinkedList linkedList = new LinkedList();
                int myUid = Process.myUid();
                int myPid = Process.myPid();
                if (processesInErrorState != null) {
                    for (ActivityManager.ProcessErrorStateInfo processErrorStateInfo : processesInErrorState) {
                        if (processErrorStateInfo.condition == 2 && processErrorStateInfo.uid == myUid) {
                            C90804Ph r1 = new C90804Ph();
                            r1.A01 = processErrorStateInfo.shortMsg;
                            r1.A02 = processErrorStateInfo.tag;
                            int i2 = processErrorStateInfo.pid;
                            r1.A00 = i2;
                            if (i2 == myPid) {
                                linkedList.addFirst(r1);
                            } else {
                                linkedList.addLast(r1);
                            }
                        }
                    }
                }
                if (this.A00) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("ProcessANRErrorMonitor/Starting process monitor checks for process ");
                    sb.append(Process.myPid());
                    Log.w(sb.toString());
                    this.A00 = false;
                    r3.A00(this.A05, null, null, 0);
                }
                C90804Ph r5 = null;
                if (!linkedList.isEmpty()) {
                    C90804Ph r22 = (C90804Ph) linkedList.getFirst();
                    if (r22.A00 == Process.myPid()) {
                        r5 = r22;
                    }
                }
                if (r5 == null) {
                    int i3 = r6.A00 + 1;
                    r6.A00 = i3;
                    if (i3 >= 120) {
                        r3.A00(this.A05, null, null, 2);
                        Log.w("ProcessANRErrorMonitor/Stopping checks because of MAX_NUMBER_BEFORE_ERROR");
                        return;
                    }
                    Object obj = this.A04;
                    synchronized (obj) {
                        z = this.A01;
                        if (!z) {
                            try {
                                obj.wait((long) 500);
                            } catch (InterruptedException unused) {
                            }
                            z = this.A01;
                        }
                    }
                } else {
                    StringBuilder sb2 = new StringBuilder("ProcessANRErrorMonitor/ANR detected Short msg: ");
                    sb2.append(r5.A01);
                    sb2.append(" Tag: ");
                    sb2.append(r5.A02);
                    Log.w(sb2.toString());
                    r3.A00(this.A05, r5.A01, r5.A02, 1);
                    return;
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
                if (e.getCause() instanceof DeadObjectException) {
                    r3 = this.A06;
                    r2 = this.A05;
                    i = 4;
                } else {
                    throw e;
                }
            }
        } while (!z);
        r2 = this.A05;
        i = 3;
        r3.A00(r2, null, null, i);
    }
}
