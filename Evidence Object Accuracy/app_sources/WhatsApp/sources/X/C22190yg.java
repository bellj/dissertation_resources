package X;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.whatsapp.GifHelper;
import com.whatsapp.R;
import com.whatsapp.gallerypicker.GalleryPicker;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: X.0yg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22190yg extends C22200yh {
    public static final byte[] A09 = {-1, -39};
    public final AbstractC15710nm A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final AnonymousClass01d A03;
    public final C16590pI A04;
    public final AnonymousClass018 A05;
    public final C14950mJ A06;
    public final C15690nk A07;
    public final AbstractC14440lR A08;

    public C22190yg(AbstractC15710nm r3, C14330lG r4, C14900mE r5, AnonymousClass01d r6, C16590pI r7, AnonymousClass018 r8, C14950mJ r9, C15690nk r10, AbstractC14440lR r11) {
        this.A04 = r7;
        this.A02 = r5;
        this.A00 = r3;
        this.A08 = r11;
        this.A01 = r4;
        this.A06 = r9;
        this.A03 = r6;
        this.A05 = r8;
        this.A07 = r10;
        r10.A01.add("com.whatsapp.provider.MigrationContentProvider");
    }

    public static int A00(AbstractC16130oV r5, C15860o1 r6) {
        C14370lK A01 = C14370lK.A01(r5.A0y, ((AbstractC15340mz) r5).A08);
        AnonymousClass1IS r2 = r5.A0z;
        boolean z = r2.A02;
        if (A01 == C14370lK.A0S) {
            return 2;
        }
        if (z) {
            return 3;
        }
        AbstractC14640lm r22 = r2.A00;
        AnonymousClass009.A05(r22);
        C19990v2 r1 = r6.A0K;
        C15550nR r0 = r6.A09;
        if (r22 != null && C32341c0.A00(r0, r1, r22) > 0) {
            return 2;
        }
        int i = r6.A08(r22.getRawString()).A00;
        if ((i == 0 && (i = r6.A05().A00) == 0) || i == 2) {
            return 1;
        }
        return 2;
    }

    public static void A01(Activity activity, C14330lG r6, C14900mE r7, AbstractC14640lm r8, C16630pM r9, int i) {
        Intent intent;
        if (i == 4) {
            intent = new Intent("android.media.action.VIDEO_CAPTURE");
            if (Build.VERSION.SDK_INT == 18 && Build.MODEL.contains("Nexus")) {
                intent.putExtra("output", Uri.fromFile(C39311pe.A00(r6, r9, C14370lK.A0X, ".mp4", 1)));
            }
        } else if (i != 5) {
            switch (i) {
                case 21:
                    intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    break;
                case 22:
                    intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI, activity, GalleryPicker.class);
                    intent.putExtra("max_items", 30);
                    break;
                case 23:
                    intent = new Intent("android.media.action.IMAGE_CAPTURE").putExtra("output", C14350lI.A01(activity, C39311pe.A00(r6, r9, C14370lK.A0B, ".jpg", 1)));
                    intent.setFlags(2);
                    break;
                default:
                    intent = null;
                    break;
            }
        } else {
            intent = new Intent();
            intent.setClassName(activity.getPackageName(), "com.whatsapp.audiopicker.AudioPickerActivity");
            intent.putExtra("jid", r8.getRawString());
        }
        try {
            activity.startActivityForResult(intent, i);
        } catch (ActivityNotFoundException e) {
            Log.e("mediafileutils/start-activity ", e);
            r7.A07(R.string.activity_not_found, 0);
        }
    }

    public static void A02(Uri.Builder builder, Uri uri, int i) {
        int i2;
        if (uri.getQueryParameter("rotation") != null) {
            i2 = Integer.parseInt(uri.getQueryParameter("rotation")) + 0;
        } else {
            i2 = 0;
        }
        int i3 = (i2 + i) % 360;
        if (i3 != 0) {
            builder.appendQueryParameter("rotation", Integer.toString(i3));
        }
    }

    public static boolean A03(C20870wS r14, C37891nB r15, File file, File file2, OutputStream outputStream, String str, byte[] bArr, int i, int i2, int i3, long j) {
        try {
            FileInputStream A0K = C22200yh.A0K(file);
            C39281pa r8 = new C39281pa(outputStream, i2);
            new C39291pb(r15, bArr, j).A00(A0K, r8, 0, 0, (long) i);
            r8.close();
            A0K.close();
            return true;
        } catch (C39301pc e) {
            StringBuilder sb = new StringBuilder("CreateProgressiveThumbnail failed  tomatch hash for progressive jpeg thumbnail; mediaHash=");
            sb.append(str);
            Log.e(sb.toString(), e);
            r14.A0H(0L, new int[]{i2}, i3, j, false);
            file2.delete();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        if (r2 != null) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte A04(android.net.Uri r4) {
        /*
            r3 = this;
            X.01d r0 = r3.A03
            android.content.ContentResolver r1 = r0.A0C()
            if (r1 != 0) goto L_0x0016
            java.lang.String r0 = "media-file-utils/get-media-mime cr=null"
            com.whatsapp.util.Log.w(r0)
        L_0x000d:
            java.lang.String r0 = X.C22200yh.A0M(r4)
            java.lang.String r2 = X.C22200yh.A0O(r0)
            goto L_0x001c
        L_0x0016:
            java.lang.String r2 = r1.getType(r4)
            if (r2 == 0) goto L_0x000d
        L_0x001c:
            java.lang.String r0 = "image/gif"
            boolean r0 = r0.equals(r2)     // Catch: IOException -> 0x0036
            if (r0 == 0) goto L_0x003c
            X.0nk r0 = r3.A07     // Catch: IOException -> 0x0036
            X.C38911ou.A01(r1, r4, r0)     // Catch: IOException -> 0x003c, IOException -> 0x0036
            X.1ov r0 = X.C38911ou.A01(r1, r4, r0)     // Catch: IOException -> 0x0036
            boolean r0 = r0.A02     // Catch: IOException -> 0x0036
            r1 = r0 ^ 1
            r0 = 13
            if (r1 == 0) goto L_0x0046
            goto L_0x0045
        L_0x0036:
            r1 = move-exception
            java.lang.String r0 = "Media file cannot be read"
            com.whatsapp.util.Log.e(r0, r1)
        L_0x003c:
            byte r0 = X.C22200yh.A04(r2)
            if (r0 != 0) goto L_0x0046
            r0 = 9
            return r0
        L_0x0045:
            r0 = 1
        L_0x0046:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22190yg.A04(android.net.Uri):byte");
    }

    public byte A05(C39341ph r4) {
        Byte A06 = r4.A06();
        if (A06 == null) {
            byte A04 = A04(r4.A0G);
            A06 = Byte.valueOf(A04);
            if (A04 == 3 && GifHelper.A01(r4.A05())) {
                A06 = (byte) 13;
            }
        }
        return A06.byteValue();
    }

    @Deprecated
    public Bitmap A06(BitmapFactory.Options options, Matrix matrix, Uri uri, int i, int i2, boolean z) {
        InputStream A0C = A0C(uri, z);
        try {
            Bitmap A02 = C37501mV.A02(options, A0C);
            if (A02 == null || A02.getWidth() == 0 || A02.getHeight() == 0) {
                throw new C39351pj();
            }
            A0C.close();
            return C22200yh.A0B(A02, matrix, i, i2);
        } catch (Throwable th) {
            try {
                A0C.close();
            } catch (Throwable unused) {
            }
            throw th;
        }
    }

    @Deprecated
    public Bitmap A07(Uri uri, int i, int i2) {
        return A08(uri, i, i2, true, true);
    }

    @Deprecated
    public Bitmap A08(Uri uri, int i, int i2, boolean z, boolean z2) {
        Bitmap bitmap;
        if (!TextUtils.isEmpty(uri.toString())) {
            Matrix A0D = C22200yh.A0D(this.A03.A0C(), uri);
            BitmapFactory.Options A092 = A09(uri, i, z, z2);
            try {
                bitmap = A06(A092, A0D, uri, i, i2, z);
            } catch (OutOfMemoryError e) {
                int i3 = A092.inSampleSize << 1;
                A092.inSampleSize = i3;
                StringBuilder sb = new StringBuilder("sample_rotate_image/oom ");
                sb.append(i3);
                Log.i(sb.toString(), e);
                bitmap = A06(A092, A0D, uri, i, i2, z);
            }
            bitmap.isMutable();
            StringBuilder sb2 = new StringBuilder("sample_rotate_image/final_size:");
            sb2.append(bitmap.getWidth());
            sb2.append(" | ");
            sb2.append(bitmap.getHeight());
            Log.i(sb2.toString());
            return bitmap;
        }
        StringBuilder sb3 = new StringBuilder("No file ");
        sb3.append(uri);
        throw new FileNotFoundException(sb3.toString());
    }

    @Deprecated
    public BitmapFactory.Options A09(Uri uri, int i, boolean z, boolean z2) {
        int i2;
        if (!TextUtils.isEmpty(uri.toString())) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            InputStream A0C = A0C(uri, z);
            try {
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(A0C, null, options);
                A0C.close();
                int i3 = options.outWidth;
                if (i3 <= 0 || (i2 = options.outHeight) <= 0) {
                    throw new C39351pj();
                }
                options.inSampleSize = 1;
                int i4 = 1;
                int max = Math.max(i3, i2);
                while (true) {
                    max >>= 1;
                    if (max > (i << 3) / 10) {
                        i4 <<= 1;
                        options.inSampleSize = i4;
                    } else {
                        options.inDither = true;
                        options.inJustDecodeBounds = false;
                        options.inScaled = false;
                        options.inPurgeable = true;
                        options.inInputShareable = true;
                        StringBuilder sb = new StringBuilder("sample_rotate_image/width=");
                        sb.append(i3);
                        sb.append(" | height=");
                        sb.append(i2);
                        sb.append(" | sample_size=");
                        sb.append(i4);
                        Log.i(sb.toString());
                        options.inPreferQualityOverSpeed = true;
                        options.inMutable = z2;
                        return options;
                    }
                }
            } catch (Throwable th) {
                try {
                    A0C.close();
                } catch (Throwable unused) {
                }
                throw th;
            }
        } else {
            StringBuilder sb2 = new StringBuilder("No file ");
            sb2.append(uri);
            throw new FileNotFoundException(sb2.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01cf A[Catch: IOException -> 0x01e2, all -> 0x01e7, TRY_ENTER, TRY_LEAVE, TryCatch #9 {IOException -> 0x01e2, blocks: (B:44:0x0130, B:61:0x01cf, B:67:0x01e1), top: B:94:0x0130, outer: #2 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File A0A(android.net.Uri r27) {
        /*
        // Method dump skipped, instructions count: 548
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22190yg.A0A(android.net.Uri):java.io.File");
    }

    public File A0B(String str, long j) {
        long A02 = this.A06.A02();
        if (j < 0 || A02 - j <= 104857600) {
            StringBuilder sb = new StringBuilder("mediafileutils/getsharedfileforsize/returning external file; size=");
            sb.append(j);
            sb.append("; internalAvailable=");
            sb.append(A02);
            Log.w(sb.toString());
            return this.A01.A0M(str);
        }
        File file = this.A01.A04().A09;
        C14330lG.A03(file, false);
        return C14330lG.A00(file, str);
    }

    public final InputStream A0C(Uri uri, boolean z) {
        InputStream openInputStream;
        Uri build = uri.buildUpon().query(null).build();
        File A03 = C14350lI.A03(build);
        if (A03 != null) {
            openInputStream = new FileInputStream(A03);
        } else {
            ContentResolver A0C = this.A03.A0C();
            if (A0C != null) {
                openInputStream = A0C.openInputStream(build);
                if (openInputStream == null) {
                    StringBuilder sb = new StringBuilder("Unable to open stream for uri=");
                    sb.append(build);
                    throw new IOException(sb.toString());
                }
            } else {
                throw new IOException("Could not get content resolver");
            }
        }
        if ((openInputStream instanceof FileInputStream) && z) {
            this.A07.A05((FileInputStream) openInputStream);
        }
        return openInputStream;
    }

    public void A0D(Uri uri, AbstractC13860kS r10, AbstractC39321pf r11) {
        this.A08.Aaz(new C39331pg(uri, r10, this.A02, this.A05, r11, this), new Void[0]);
    }

    public void A0E(File file) {
        if (file != null) {
            try {
                if (this.A01.A0T(file)) {
                    C14350lI.A0M(file);
                }
            } catch (IOException e) {
                StringBuilder sb = new StringBuilder("mediafileutils/unable to delete file ");
                sb.append(file);
                Log.e(sb.toString(), e);
            }
        }
    }
}
