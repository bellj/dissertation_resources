package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.5pi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class EnumC124545pi extends Enum {
    public static final /* synthetic */ EnumC124545pi[] A00;
    public static final EnumC124545pi A01;
    public static final EnumC124545pi A02;
    public static final EnumC124545pi A03;

    public static EnumC124545pi valueOf(String str) {
        return (EnumC124545pi) Enum.valueOf(EnumC124545pi.class, str);
    }

    public static EnumC124545pi[] values() {
        return (EnumC124545pi[]) A00.clone();
    }

    static {
        EnumC124545pi r5 = new EnumC124545pi("NONE", 0);
        A03 = r5;
        EnumC124545pi r4 = new EnumC124545pi("ACTIVE", 1);
        A01 = r4;
        EnumC124545pi r2 = new EnumC124545pi("INACTIVE", 2);
        A02 = r2;
        EnumC124545pi[] r1 = new EnumC124545pi[3];
        C12990iw.A1P(r5, r4, r1);
        r1[2] = r2;
        A00 = r1;
    }

    public EnumC124545pi(String str, int i) {
    }
}
