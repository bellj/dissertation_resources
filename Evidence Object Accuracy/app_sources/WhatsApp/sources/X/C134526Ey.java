package X;

/* renamed from: X.6Ey  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C134526Ey implements AnonymousClass18X {
    public final AnonymousClass01H A00;
    public final AnonymousClass01H A01;

    public C134526Ey(AnonymousClass01H r1, AnonymousClass01H r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass18X
    public boolean AKA() {
        AnonymousClass01H r6 = this.A01;
        int i = ((C14820m6) r6.get()).A00.getInt("shops_privacy_notice", -1);
        AnonymousClass01H r2 = this.A00;
        C15450nH r0 = ((AnonymousClass18Y) r2.get()).A00;
        C16140oW r1 = AbstractC15460nI.A25;
        if (i >= r0.A02(r1)) {
            return true;
        }
        ((C14820m6) r6.get()).A00.getInt("shops_privacy_notice", -1);
        ((AnonymousClass18Y) r2.get()).A00.A02(r1);
        return false;
    }
}
