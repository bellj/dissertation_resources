package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.4dv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95544dv {
    public static boolean addAllImpl(AnonymousClass5Z2 r1, AbstractC80943tD r2) {
        if (r2.isEmpty()) {
            return false;
        }
        r2.addTo(r1);
        return true;
    }

    public static boolean addAllImpl(AnonymousClass5Z2 r3, AnonymousClass5Z2 r4) {
        if (r4 instanceof AbstractC80943tD) {
            return addAllImpl(r3, (AbstractC80943tD) r4);
        }
        if (r4.isEmpty()) {
            return false;
        }
        for (AnonymousClass4Y5 r0 : r4.entrySet()) {
            r3.add(r0.getElement(), r0.getCount());
        }
        return true;
    }

    public static boolean addAllImpl(AnonymousClass5Z2 r1, Collection collection) {
        if (collection instanceof AnonymousClass5Z2) {
            return addAllImpl(r1, cast(collection));
        }
        if (collection.isEmpty()) {
            return false;
        }
        return AnonymousClass1I4.addAll(r1, collection.iterator());
    }

    public static AnonymousClass5Z2 cast(Iterable iterable) {
        return (AnonymousClass5Z2) iterable;
    }

    public static boolean equalsImpl(AnonymousClass5Z2 r6, Object obj) {
        if (obj != r6) {
            if (obj instanceof AnonymousClass5Z2) {
                AnonymousClass5Z2 r7 = (AnonymousClass5Z2) obj;
                if (r6.size() == r7.size() && r6.entrySet().size() == r7.entrySet().size()) {
                    for (AnonymousClass4Y5 r2 : r7.entrySet()) {
                        if (r6.count(r2.getElement()) != r2.getCount()) {
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public static Iterator iteratorImpl(AnonymousClass5Z2 r2) {
        return new AnonymousClass5DF(r2, r2.entrySet().iterator());
    }

    public static boolean removeAllImpl(AnonymousClass5Z2 r1, Collection collection) {
        if (collection instanceof AnonymousClass5Z2) {
            collection = ((AnonymousClass5Z2) collection).elementSet();
        }
        return r1.elementSet().removeAll(collection);
    }

    public static boolean retainAllImpl(AnonymousClass5Z2 r1, Collection collection) {
        if (collection instanceof AnonymousClass5Z2) {
            collection = ((AnonymousClass5Z2) collection).elementSet();
        }
        return r1.elementSet().retainAll(collection);
    }
}
