package X;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;

/* renamed from: X.3Ni  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ViewTreeObserver$OnPreDrawListenerC66373Ni implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ AnonymousClass23G A00;

    public ViewTreeObserver$OnPreDrawListenerC66373Ni(AnonymousClass23G r1) {
        this.A00 = r1;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        long j;
        AnonymousClass23G r4 = this.A00;
        View view = r4.A02;
        C12980iv.A1G(view, this);
        int height = view.getHeight();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = r4.A00;
        view.setLayoutParams(layoutParams);
        r4.A01.setAlpha(0.0f);
        ValueAnimator ofInt = ValueAnimator.ofInt(r4.A00, height);
        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: X.3Jc
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                AnonymousClass23G r1 = AnonymousClass23G.this;
                int A05 = C12960it.A05(valueAnimator.getAnimatedValue());
                View view2 = r1.A02;
                ViewGroup.LayoutParams layoutParams2 = view2.getLayoutParams();
                layoutParams2.height = A05;
                view2.setLayoutParams(layoutParams2);
            }
        });
        if (r4.A00 == height) {
            j = 0;
        } else {
            j = 350;
        }
        ofInt.setDuration(j);
        ofInt.setInterpolator(new AccelerateDecelerateInterpolator());
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        C12980iv.A11(ofFloat, r4, 2);
        ofFloat.setDuration(250L);
        ofFloat.addListener(new C72743f6(ofInt, r4));
        ofFloat.start();
        return false;
    }
}
