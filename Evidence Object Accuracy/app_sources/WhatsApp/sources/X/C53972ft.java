package X;

import com.whatsapp.registration.directmigration.RestoreFromConsumerDatabaseActivity;

/* renamed from: X.2ft  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53972ft extends AnonymousClass0Yo {
    public final /* synthetic */ RestoreFromConsumerDatabaseActivity A00;

    public C53972ft(RestoreFromConsumerDatabaseActivity restoreFromConsumerDatabaseActivity) {
        this.A00 = restoreFromConsumerDatabaseActivity;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C44121yH.class)) {
            RestoreFromConsumerDatabaseActivity restoreFromConsumerDatabaseActivity = this.A00;
            C14900mE r1 = ((ActivityC13810kN) restoreFromConsumerDatabaseActivity).A05;
            C15570nT r12 = ((ActivityC13790kL) restoreFromConsumerDatabaseActivity).A01;
            AbstractC14440lR r13 = ((ActivityC13830kP) restoreFromConsumerDatabaseActivity).A05;
            C14950mJ r14 = ((ActivityC13790kL) restoreFromConsumerDatabaseActivity).A06;
            C19890uq r15 = restoreFromConsumerDatabaseActivity.A0D;
            C19490uC r16 = restoreFromConsumerDatabaseActivity.A08;
            C27021Fs r152 = restoreFromConsumerDatabaseActivity.A0B;
            C15830ny r142 = restoreFromConsumerDatabaseActivity.A0O;
            AbstractC15850o0 r132 = restoreFromConsumerDatabaseActivity.A0N;
            C15860o1 r122 = restoreFromConsumerDatabaseActivity.A0M;
            C17050qB r11 = restoreFromConsumerDatabaseActivity.A07;
            C15880o3 r10 = ((ActivityC13790kL) restoreFromConsumerDatabaseActivity).A07;
            C20740wF r9 = restoreFromConsumerDatabaseActivity.A0E;
            C16490p7 r8 = restoreFromConsumerDatabaseActivity.A0A;
            C18350sJ r7 = restoreFromConsumerDatabaseActivity.A0G;
            C14820m6 r6 = ((ActivityC13810kN) restoreFromConsumerDatabaseActivity).A09;
            C26991Fp r5 = restoreFromConsumerDatabaseActivity.A0H;
            C27001Fq r4 = restoreFromConsumerDatabaseActivity.A0K;
            AnonymousClass15E r3 = restoreFromConsumerDatabaseActivity.A0L;
            return new C44121yH(r1, r12, r11, r6, r14, r16, r10, restoreFromConsumerDatabaseActivity.A09, r8, r152, r15, r9, restoreFromConsumerDatabaseActivity.A0F, r7, r5, restoreFromConsumerDatabaseActivity.A0J, r4, r3, r122, r132, r142, r13);
        }
        throw C12970iu.A0f("Invalid viewModel");
    }
}
