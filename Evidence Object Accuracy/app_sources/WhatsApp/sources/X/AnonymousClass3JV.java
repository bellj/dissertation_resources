package X;

import android.animation.Animator;
import android.content.Context;
import android.util.SparseArray;
import com.whatsapp.R;
import java.util.AbstractMap;

/* renamed from: X.3JV  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3JV {
    public static Animator A00(C14260l7 r1, String str) {
        return (Animator) ((AbstractMap) r1.A02(R.id.bk_context_key_animations)).get(str);
    }

    public static C14260l7 A01(Context context, SparseArray sparseArray, C14270l8 r8, C64173En r9, String str) {
        SparseArray clone = r9.A00().clone();
        for (int i = 0; i < sparseArray.size(); i++) {
            clone.put(sparseArray.keyAt(i), sparseArray.valueAt(i));
        }
        clone.put(R.id.bk_context_key_states, C12970iu.A11());
        if (r8 == null) {
            AnonymousClass3JI A01 = AnonymousClass3JI.A01(new AnonymousClass28D(-1));
            C1093551j r4 = C1093551j.A00;
            AnonymousClass4ZD r3 = AnonymousClass4ZD.A00;
            r8 = new C14270l8(A01.A00, A01.A01, r3, r4);
        }
        clone.put(R.id.bk_context_key_tree, r8);
        clone.put(R.id.bk_context_key_scoped_client_id_mapper, new AnonymousClass3CV());
        clone.put(R.id.bk_context_key_animations, C12970iu.A11());
        clone.put(R.id.bk_context_key_timers, C12970iu.A11());
        clone.put(R.id.bk_context_key_logging_id, str);
        if (clone.get(R.id.bk_context_key_performance_logger) == null) {
            clone.put(R.id.bk_context_key_performance_logger, AnonymousClass4ZC.A00);
        }
        C14260l7 r1 = new C14260l7(context, clone, r9);
        clone.get(R.id.bk_context_key_bloks_context_creation_listener);
        return r1;
    }

    public static C14190l0 A02(C14260l7 r1, String str) {
        return (C14190l0) ((AbstractMap) r1.A02(R.id.bk_context_key_timers)).get(str);
    }

    public static C14270l8 A03(C14260l7 r1) {
        return (C14270l8) r1.A02(R.id.bk_context_key_tree);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0143, code lost:
        if (r8 != null) goto L_0x0145;
     */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0147  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object A04(X.C14260l7 r9, X.AnonymousClass28D r10) {
        /*
        // Method dump skipped, instructions count: 382
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3JV.A04(X.0l7, X.28D):java.lang.Object");
    }

    public static void A05(Animator animator, C14260l7 r2, String str) {
        Animator animator2 = (Animator) ((AbstractMap) r2.A02(R.id.bk_context_key_animations)).put(str, animator);
        if (animator2 != null) {
            animator2.cancel();
            Object[] A1b = C12970iu.A1b();
            A1b[0] = str;
            C28691Op.A00("BloksAnimation", String.format("Found previously started animator with key %s. Canceling it.", A1b));
        }
    }

    public static void A06(C14260l7 r1) {
        C14270l8 A03 = A03(r1);
        if (A03 != null) {
            AnonymousClass3J3.A02("Tree operations are only supported from the UI Thread");
            int i = A03.A00;
            if (i > 0) {
                int i2 = i - 1;
                A03.A00 = i2;
                if (i2 == 0 && A03.A0I.size() > 0) {
                    A03.A04();
                    return;
                }
                return;
            }
            throw C12960it.A0U("Negative recursion level.");
        }
    }

    public static void A07(C14260l7 r2, C14190l0 r3, String str) {
        AbstractMap abstractMap = (AbstractMap) r2.A02(R.id.bk_context_key_timers);
        C14190l0 r0 = (C14190l0) abstractMap.get(str);
        if (r0 != null) {
            r0.A01();
            StringBuilder A0k = C12960it.A0k("Timer with id ");
            A0k.append(str);
            C28691Op.A00("BloksTimer", C12960it.A0d(" already exists. Overwriting previous timer.", A0k));
        }
        abstractMap.put(str, r3);
    }

    public static void A08(C14260l7 r1, String str) {
        ((AbstractMap) r1.A02(R.id.bk_context_key_animations)).remove(str);
    }

    public static boolean A09(C14260l7 r2) {
        Boolean bool = (Boolean) r2.A01.get(R.id.bk_context_key_clip_children_bool);
        if (bool == null) {
            return true;
        }
        return bool.booleanValue();
    }
}
