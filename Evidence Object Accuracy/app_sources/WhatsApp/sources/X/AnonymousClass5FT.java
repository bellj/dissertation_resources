package X;

/* renamed from: X.5FT  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5FT implements AnonymousClass5VN {
    public final /* synthetic */ C94904cj A00;

    public AnonymousClass5FT(C94904cj r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5VN
    public void AgH(Appendable appendable, Object obj, C94884ch r8) {
        double[] dArr = (double[]) obj;
        appendable.append('[');
        boolean z = false;
        for (double d : dArr) {
            z = C72453ed.A1U(appendable, z);
            appendable.append(Double.toString(d));
        }
        appendable.append(']');
    }
}
