package X;

/* renamed from: X.0Ko  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04150Ko {
    public static void A00(Object obj, StringBuilder sb) {
        String hexString;
        int lastIndexOf;
        if (obj == null) {
            hexString = "null";
        } else {
            Class<?> cls = obj.getClass();
            String simpleName = cls.getSimpleName();
            if (simpleName.length() <= 0 && (lastIndexOf = (simpleName = cls.getName()).lastIndexOf(46)) > 0) {
                simpleName = simpleName.substring(lastIndexOf + 1);
            }
            sb.append(simpleName);
            sb.append('{');
            hexString = Integer.toHexString(System.identityHashCode(obj));
        }
        sb.append(hexString);
    }
}
