package X;

import java.security.KeyPair;

/* renamed from: X.5fJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119915fJ extends AbstractC128495wC {
    public final /* synthetic */ AnonymousClass3EB A00;
    public final /* synthetic */ AnonymousClass68O A01;
    public final /* synthetic */ String A02;
    public final /* synthetic */ KeyPair A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C119915fJ(AnonymousClass3EB r1, AnonymousClass3EB r2, AnonymousClass68O r3, String str, KeyPair keyPair) {
        super(r1);
        this.A01 = r3;
        this.A03 = keyPair;
        this.A02 = str;
        this.A00 = r2;
    }
}
