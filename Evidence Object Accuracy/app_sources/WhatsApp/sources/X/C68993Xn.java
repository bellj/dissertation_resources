package X;

import com.whatsapp.chatinfo.ContactInfoActivity;

/* renamed from: X.3Xn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68993Xn implements AnonymousClass13Q {
    public final /* synthetic */ ContactInfoActivity A00;

    public C68993Xn(ContactInfoActivity contactInfoActivity) {
        this.A00 = contactInfoActivity;
    }

    @Override // X.AnonymousClass13Q
    public void AWN(AbstractC14640lm r4) {
        ContactInfoActivity contactInfoActivity = this.A00;
        if (r4.equals(contactInfoActivity.A2v())) {
            C14900mE.A01(((ActivityC13810kN) contactInfoActivity).A05, contactInfoActivity, 4);
        }
    }

    @Override // X.AnonymousClass13Q
    public void AWr(AbstractC14640lm r4) {
        ContactInfoActivity contactInfoActivity = this.A00;
        if (r4.equals(contactInfoActivity.A2v())) {
            C14900mE.A01(((ActivityC13810kN) contactInfoActivity).A05, contactInfoActivity, 5);
        }
    }
}
