package X;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import com.whatsapp.R;
import java.util.ArrayList;

/* renamed from: X.02s  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C005602s {
    public int A00 = 0;
    public int A01 = 0;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06 = 0;
    public Notification A07;
    public Notification A08;
    public PendingIntent A09;
    public PendingIntent A0A;
    public Context A0B;
    public Bitmap A0C;
    public Bundle A0D;
    public RemoteViews A0E;
    public AbstractC006703e A0F;
    public CharSequence A0G;
    public CharSequence A0H;
    public String A0I;
    public String A0J;
    public String A0K;
    public String A0L;
    public String A0M;
    public ArrayList A0N = new ArrayList();
    public ArrayList A0O = new ArrayList();
    @Deprecated
    public ArrayList A0P;
    public ArrayList A0Q = new ArrayList();
    public boolean A0R;
    public boolean A0S;
    public boolean A0T = false;
    public boolean A0U;
    public boolean A0V = true;
    public boolean A0W;

    public C005602s(Context context, String str) {
        Notification notification = new Notification();
        this.A07 = notification;
        this.A0B = context;
        this.A0J = str;
        notification.when = System.currentTimeMillis();
        notification.audioStreamType = -1;
        this.A03 = 0;
        this.A0P = new ArrayList();
        this.A0R = true;
    }

    public static CharSequence A00(CharSequence charSequence) {
        return (charSequence == null || charSequence.length() <= 5120) ? charSequence : charSequence.subSequence(0, 5120);
    }

    public Notification A01() {
        return new C07310Xm(this).A01();
    }

    public void A02(int i) {
        Notification notification = this.A07;
        notification.defaults = i;
        if ((i & 4) != 0) {
            notification.flags |= 1;
        }
    }

    public void A03(int i, int i2, boolean z) {
        this.A05 = i;
        this.A04 = i2;
        this.A0U = z;
    }

    public void A04(int i, CharSequence charSequence, PendingIntent pendingIntent) {
        this.A0N.add(new AnonymousClass03l(i, charSequence, pendingIntent));
    }

    public void A05(long j) {
        this.A07.when = j;
    }

    public void A06(Bitmap bitmap) {
        if (bitmap != null && Build.VERSION.SDK_INT < 27) {
            Resources resources = this.A0B.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() > dimensionPixelSize || bitmap.getHeight() > dimensionPixelSize2) {
                double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
                bitmap = Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(((double) bitmap.getHeight()) * min), true);
            }
        }
        this.A0C = bitmap;
    }

    public void A07(Uri uri) {
        Notification notification = this.A07;
        notification.sound = uri;
        notification.audioStreamType = -1;
        if (Build.VERSION.SDK_INT >= 21) {
            notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
        }
    }

    public void A08(AbstractC006703e r2) {
        if (this.A0F != r2) {
            this.A0F = r2;
            if (r2.A00 != this) {
                r2.A00 = this;
                A08(r2);
            }
        }
    }

    public void A09(CharSequence charSequence) {
        this.A0G = A00(charSequence);
    }

    public void A0A(CharSequence charSequence) {
        this.A0H = A00(charSequence);
    }

    public void A0B(CharSequence charSequence) {
        this.A07.tickerText = A00(charSequence);
    }

    @Deprecated
    public void A0C(String str) {
        if (str != null && !str.isEmpty()) {
            this.A0P.add(str);
        }
    }

    public void A0D(boolean z) {
        Notification notification = this.A07;
        int i = notification.flags;
        int i2 = 16 | i;
        if (!z) {
            i2 = -17 & i;
        }
        notification.flags = i2;
    }

    public void A0E(boolean z) {
        Notification notification = this.A07;
        int i = notification.flags;
        int i2 = 2 | i;
        if (!z) {
            i2 = -3 & i;
        }
        notification.flags = i2;
    }
}
