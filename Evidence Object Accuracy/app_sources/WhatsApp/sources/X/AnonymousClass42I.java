package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.42I  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass42I extends C60972zC {
    @Override // X.AbstractC64483Fs
    public String A01() {
        return "novi_view_transaction";
    }

    @Override // X.AbstractC64483Fs
    public String A02(Context context, AnonymousClass1Z8 r3) {
        return context.getString(R.string.native_flow_view_transaction);
    }
}
