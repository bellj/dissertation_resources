package X;

import com.whatsapp.util.Log;

/* renamed from: X.4V0  reason: invalid class name */
/* loaded from: classes3.dex */
public final /* synthetic */ class AnonymousClass4V0 {
    public final /* synthetic */ AnonymousClass194 A00;

    public /* synthetic */ AnonymousClass4V0(AnonymousClass194 r1) {
        this.A00 = r1;
    }

    public final void A00(boolean z) {
        AnonymousClass194 r1 = this.A00;
        StringBuilder A0k = C12960it.A0k("DictionarySearchProvider/update/prepareCallback/onPrepare/fetchable:");
        A0k.append(z);
        Log.i(A0k.toString());
        if (r1.A02 != z) {
            r1.A02 = z;
            AnonymousClass5UG r0 = r1.A00;
            if (r0 != null) {
                r0.AVg(z);
            }
        }
        r1.A01 = false;
    }
}
