package X;

import android.content.BroadcastReceiver;
import org.npci.commonlibrary.GetCredential;

/* renamed from: X.5Zr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117375Zr extends BroadcastReceiver {
    public final /* synthetic */ GetCredential A00;

    public /* synthetic */ C117375Zr(GetCredential getCredential) {
        this.A00 = getCredential;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00cf, code lost:
        continue;
     */
    @Override // android.content.BroadcastReceiver
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r15, android.content.Intent r16) {
        /*
            r14 = this;
            java.lang.String r0 = r16.getAction()
            if (r0 == 0) goto L_0x00fe
            java.lang.String r1 = r16.getAction()
            java.lang.String r0 = "android.provider.Telephony.SMS_RECEIVED"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00fe
            android.os.Bundle r1 = r16.getExtras()
            if (r1 == 0) goto L_0x00fe
            java.lang.String r0 = "pdus"
            java.lang.Object r7 = r1.get(r0)
            java.lang.Object[] r7 = (java.lang.Object[]) r7
            if (r7 == 0) goto L_0x00fe
            int r6 = r7.length
            android.telephony.SmsMessage[] r5 = new android.telephony.SmsMessage[r6]
            r4 = 0
        L_0x0026:
            if (r4 >= r6) goto L_0x00fe
            r0 = r7[r4]
            byte[] r0 = (byte[]) r0
            android.telephony.SmsMessage r0 = android.telephony.SmsMessage.createFromPdu(r0)
            r5[r4] = r0
            r0 = r5[r4]
            java.lang.String r1 = r0.getOriginatingAddress()
            org.npci.commonlibrary.GetCredential r9 = r14.A00
            X.018 r0 = r9.A05
            java.util.Locale r0 = X.C12970iu.A14(r0)
            java.lang.String r10 = r1.toUpperCase(r0)
            r0 = r5[r4]
            java.lang.String r1 = r0.getMessageBody()
            X.018 r0 = r9.A05
            java.util.Locale r0 = X.C12970iu.A14(r0)
            java.lang.String r8 = r1.toUpperCase(r0)
            android.content.Context r0 = r9.A0G
            X.5t8 r3 = new X.5t8
            r3.<init>(r0)
            r2 = 0
        L_0x005c:
            org.json.JSONArray r1 = r3.A00     // Catch: JSONException -> 0x00d2
            int r0 = r1.length()     // Catch: JSONException -> 0x00d2
            if (r2 >= r0) goto L_0x00fa
            org.json.JSONObject r1 = r1.getJSONObject(r2)     // Catch: JSONException -> 0x00d2
            java.lang.String r0 = "sender"
            org.json.JSONArray r12 = r1.getJSONArray(r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            r11 = 0
        L_0x006f:
            int r0 = r12.length()     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            if (r11 >= r0) goto L_0x00cf
            java.lang.String r13 = r12.getString(r11)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            r0 = 2
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r13, r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            java.util.regex.Matcher r0 = r0.matcher(r10)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            boolean r0 = r0.find()     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            if (r0 == 0) goto L_0x00cc
            java.lang.String r0 = "message"
            java.lang.String r11 = r1.getString(r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            r0 = 2
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r11, r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            java.util.regex.Matcher r0 = r0.matcher(r8)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            boolean r0 = r0.find()     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            if (r0 == 0) goto L_0x00cf
            java.lang.String r0 = "otp"
            java.lang.Object r0 = r1.get(r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            java.lang.String r0 = (java.lang.String) r0     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            java.util.regex.Matcher r1 = r0.matcher(r8)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            boolean r0 = r1.find()     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            if (r0 == 0) goto L_0x00cf
            int r0 = r1.groupCount()     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            if (r0 < 0) goto L_0x00cf
            r0 = 0
            java.lang.String r10 = r1.group(r0)     // Catch: JSONException -> 0x00cf, JSONException -> 0x00d2
            org.npci.commonlibrary.NPCIFragment r9 = r9.A0D
            java.util.ArrayList r8 = r9.A0B
            int r1 = r9.A00
            X.5ag r2 = X.C117305Zk.A0s(r8, r1)
            r0 = -1
            if (r1 == r0) goto L_0x00fa
            goto L_0x00d8
        L_0x00cc:
            int r11 = r11 + 1
            goto L_0x006f
        L_0x00cf:
            int r2 = r2 + 1
            goto L_0x005c
        L_0x00d2:
            java.lang.String r0 = "PAY: failed to extract otp from text"
            com.whatsapp.util.Log.e(r0)
            goto L_0x00fa
        L_0x00d8:
            if (r10 == 0) goto L_0x00fa
            int r1 = r2.A00
            int r0 = r10.length()
            if (r1 < r0) goto L_0x00fa
            r3 = 1
            r9.A0A = r3
            r2.setText(r10)
            int r2 = r8.size()
            int r1 = r9.A00
            int r0 = r1 + 1
            if (r2 <= r0) goto L_0x00fa
            int r1 = r1 + r3
            X.6My r0 = X.C117325Zm.A02(r8, r1)
            r0.AA5()
        L_0x00fa:
            int r4 = r4 + 1
            goto L_0x0026
        L_0x00fe:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C117375Zr.onReceive(android.content.Context, android.content.Intent):void");
    }
}
