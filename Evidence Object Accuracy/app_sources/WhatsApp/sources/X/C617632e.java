package X;

import java.util.List;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: X.32e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C617632e extends C37621mj implements AbstractC117175Yu {
    public final HttpsURLConnection A00;

    public C617632e(HttpsURLConnection httpsURLConnection) {
        super(null, httpsURLConnection);
        this.A00 = httpsURLConnection;
    }

    @Override // X.AbstractC117175Yu
    public String ABg() {
        return AnonymousClass1P1.A00(this.A00.getInputStream());
    }

    @Override // X.AbstractC117175Yu
    public String ACk() {
        return AnonymousClass1P1.A01(this.A00);
    }

    @Override // X.AbstractC117175Yu
    public String AGG() {
        return this.A01.getResponseMessage();
    }

    @Override // X.AbstractC117175Yu
    public List AIQ(String str) {
        return this.A01.getHeaderFields().get(str);
    }
}
