package X;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import com.facebook.redex.RunnableBRunnable0Shape0S0200000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0210000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S1411000_I0;
import com.facebook.redex.RunnableBRunnable0Shape1S0100000_I0_1;
import com.facebook.redex.RunnableBRunnable0Shape4S0200000_I0_4;
import com.whatsapp.R;
import com.whatsapp.jid.GroupJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: X.0oZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C16170oZ {
    public PowerManager.WakeLock A00;
    public final C22490zA A01;
    public final AbstractC15710nm A02;
    public final C22260yn A03;
    public final C244615p A04;
    public final C14900mE A05;
    public final C22930zs A06;
    public final C15570nT A07;
    public final C002701f A08;
    public final C15450nH A09;
    public final C18790t3 A0A;
    public final C26681Ek A0B;
    public final C18330sH A0C;
    public final C16240og A0D;
    public final C21290xB A0E;
    public final C14650lo A0F;
    public final C246816l A0G;
    public final C238013b A0H;
    public final C243915i A0I;
    public final C18850tA A0J;
    public final C15550nR A0K;
    public final C26311Cv A0L;
    public final C18780t0 A0M;
    public final C15610nY A0N;
    public final C250417w A0O;
    public final C17050qB A0P;
    public final AnonymousClass01d A0Q;
    public final C14830m7 A0R;
    public final C16590pI A0S;
    public final C15890o4 A0T;
    public final AnonymousClass018 A0U;
    public final C14950mJ A0V;
    public final C22610zM A0W;
    public final C17650rA A0X;
    public final C22320yt A0Y;
    public final C18740sw A0Z;
    public final C20650w6 A0a;
    public final C19990v2 A0b;
    public final C20830wO A0c;
    public final C21320xE A0d;
    public final C15680nj A0e;
    public final C15650ng A0f;
    public final C238113c A0g;
    public final C21410xN A0h;
    public final C25621Ac A0i;
    public final C15600nX A0j;
    public final C26691El A0k;
    public final AnonymousClass10Y A0l;
    public final C21620xi A0m;
    public final AnonymousClass12H A0n;
    public final C22830zi A0o;
    public final C242114q A0p;
    public final AnonymousClass10U A0q;
    public final AnonymousClass15K A0r;
    public final C242414t A0s;
    public final C239113m A0t;
    public final C15670ni A0u;
    public final C22180yf A0v;
    public final C20030v6 A0w;
    public final C14850m9 A0x;
    public final C16120oU A0y;
    public final C18810t5 A0z;
    public final C16030oK A10;
    public final C14410lO A11;
    public final AnonymousClass109 A12;
    public final AnonymousClass14D A13;
    public final C17040qA A14;
    public final C22370yy A15;
    public final C239713s A16;
    public final AnonymousClass14R A17;
    public final C22390z0 A18;
    public final C17220qS A19;
    public final C20740wF A1A;
    public final C20660w7 A1B;
    public final C22230yk A1C;
    public final C20220vP A1D;
    public final C22280yp A1E;
    public final C239913u A1F;
    public final C20320vZ A1G;
    public final C26661Ei A1H;
    public final C22600zL A1I;
    public final C15860o1 A1J;
    public final AnonymousClass1BD A1K;
    public final C22210yi A1L;
    public final C235512c A1M;
    public final C26671Ej A1N;
    public final AnonymousClass1CY A1O;
    public final C22190yg A1P;
    public final AbstractC14440lR A1Q;
    public final JniBridge A1R;
    public final C14860mA A1S;

    public C16170oZ(C22490zA r2, AbstractC15710nm r3, C22260yn r4, C244615p r5, C14900mE r6, C22930zs r7, C15570nT r8, C002701f r9, C15450nH r10, C18790t3 r11, C26681Ek r12, C18330sH r13, C16240og r14, C21290xB r15, C14650lo r16, C246816l r17, C238013b r18, C243915i r19, C18850tA r20, C15550nR r21, C26311Cv r22, C18780t0 r23, C15610nY r24, C250417w r25, C17050qB r26, AnonymousClass01d r27, C14830m7 r28, C16590pI r29, C15890o4 r30, AnonymousClass018 r31, C14950mJ r32, C22610zM r33, C17650rA r34, C22320yt r35, C18740sw r36, C20650w6 r37, C19990v2 r38, C20830wO r39, C21320xE r40, C15680nj r41, C15650ng r42, C238113c r43, C21410xN r44, C25621Ac r45, C15600nX r46, C26691El r47, AnonymousClass10Y r48, C21620xi r49, AnonymousClass12H r50, C22830zi r51, C242114q r52, AnonymousClass10U r53, AnonymousClass15K r54, C242414t r55, C239113m r56, C15670ni r57, C22180yf r58, C20030v6 r59, C14850m9 r60, C16120oU r61, C18810t5 r62, C16030oK r63, C14410lO r64, AnonymousClass109 r65, AnonymousClass14D r66, C17040qA r67, C22370yy r68, C239713s r69, AnonymousClass14R r70, C22390z0 r71, C17220qS r72, C20740wF r73, C20660w7 r74, C22230yk r75, C20220vP r76, C22280yp r77, C239913u r78, C20320vZ r79, C26661Ei r80, C22600zL r81, C15860o1 r82, AnonymousClass1BD r83, C22210yi r84, C235512c r85, C26671Ej r86, AnonymousClass1CY r87, C22190yg r88, AbstractC14440lR r89, JniBridge jniBridge, C14860mA r91) {
        this.A0S = r29;
        this.A0R = r28;
        this.A0x = r60;
        this.A1O = r87;
        this.A05 = r6;
        this.A02 = r3;
        this.A07 = r8;
        this.A1Q = r89;
        this.A0b = r38;
        this.A1R = jniBridge;
        this.A0A = r11;
        this.A0y = r61;
        this.A1S = r91;
        this.A0a = r37;
        this.A1B = r74;
        this.A09 = r10;
        this.A0J = r20;
        this.A0Z = r36;
        this.A11 = r64;
        this.A0V = r32;
        this.A1I = r81;
        this.A0r = r54;
        this.A0E = r15;
        this.A0K = r21;
        this.A0v = r58;
        this.A0i = r45;
        this.A19 = r72;
        this.A1P = r88;
        this.A1L = r84;
        this.A0Q = r27;
        this.A16 = r69;
        this.A0N = r24;
        this.A1C = r75;
        this.A0U = r31;
        this.A03 = r4;
        this.A1G = r79;
        this.A1E = r77;
        this.A0H = r18;
        this.A0I = r19;
        this.A0f = r42;
        this.A0g = r43;
        this.A0l = r48;
        this.A0w = r59;
        this.A0n = r50;
        this.A0D = r14;
        this.A1J = r82;
        this.A06 = r7;
        this.A0P = r26;
        this.A1F = r78;
        this.A1M = r85;
        this.A0C = r13;
        this.A0Y = r35;
        this.A17 = r70;
        this.A1A = r73;
        this.A0q = r53;
        this.A14 = r67;
        this.A0m = r49;
        this.A1D = r76;
        this.A0p = r52;
        this.A0z = r62;
        this.A0T = r30;
        this.A0u = r57;
        this.A0e = r41;
        this.A04 = r5;
        this.A15 = r68;
        this.A1H = r80;
        this.A0o = r51;
        this.A1K = r83;
        this.A13 = r66;
        this.A0s = r55;
        this.A0d = r40;
        this.A0h = r44;
        this.A18 = r71;
        this.A0M = r23;
        this.A10 = r63;
        this.A0F = r16;
        this.A0O = r25;
        this.A0X = r34;
        this.A0G = r17;
        this.A12 = r65;
        this.A0j = r46;
        this.A0L = r22;
        this.A0c = r39;
        this.A1N = r86;
        this.A0W = r33;
        this.A0t = r56;
        this.A08 = r9;
        this.A01 = r2;
        this.A0B = r12;
        this.A0k = r47;
    }

    public static String A00(AbstractC16130oV r5) {
        File file;
        if (!TextUtils.isEmpty(r5.A06)) {
            return r5.A06;
        }
        C16150oX r0 = r5.A02;
        if (!(r0 == null || (file = r0.A0F) == null)) {
            try {
                byte b = r5.A0y;
                if (b == 2) {
                    return AnonymousClass152.A06(AnonymousClass152.A03(file));
                }
                if (b == 3 || b == 13) {
                    int i = AnonymousClass152.A04(file, false).A01;
                    if (i == 7) {
                        return "video/quicktime";
                    }
                    return i == 3 ? "video/3gpp" : "video/mp4";
                }
            } catch (C37881nA | IOException e) {
                Log.e("useractions/getmediamimetype exception", e);
            }
        }
        byte b2 = r5.A0y;
        if (b2 == 1) {
            return "image/jpeg";
        }
        if (b2 == 2) {
            return "audio/*";
        }
        if (b2 == 3 || b2 == 13) {
            return "video/*";
        }
        if (b2 == 23 || b2 == 25 || b2 == 37 || b2 == 42) {
            return "image/jpeg";
        }
        if (b2 != 43) {
            return "*/*";
        }
        return "video/*";
    }

    public static /* synthetic */ void A01(Uri uri, AnonymousClass12P r10, C16170oZ r11, AbstractC16130oV r12, WeakReference weakReference) {
        String str;
        String A0Y;
        Intent intent;
        int i;
        r11.A05.A03();
        if (weakReference.get() != null) {
            Context context = (Context) weakReference.get();
            boolean z = false;
            if (((AbstractC15340mz) r12).A05 >= 127) {
                z = true;
            }
            C15570nT r7 = r11.A07;
            byte b = r12.A0y;
            if (b == 23) {
                str = ((AnonymousClass1XV) r12).A09;
            } else if (b != 44) {
                if (b == 1) {
                    i = R.string.share_email_subject_image;
                } else if (b == 2) {
                    i = R.string.share_email_subject_audio;
                } else if (b == 3) {
                    i = R.string.share_email_subject_video;
                } else if (b != 9) {
                    i = R.string.share_email_subject_gif;
                    if (b != 13) {
                        i = R.string.share_email_subject_file;
                    }
                } else {
                    i = R.string.share_email_subject_document;
                }
                str = context.getString(i, r7.A05());
            } else {
                str = ((AnonymousClass1XF) r12).A07;
            }
            AnonymousClass018 r1 = r11.A0U;
            if (C35011h5.A04(r12)) {
                A0Y = C35011h5.A00(r12);
            } else if (b != 44) {
                A0Y = r12.A15();
            } else {
                A0Y = AnonymousClass243.A0Y(r1, (AnonymousClass1XF) r12);
            }
            String A00 = A00(r12);
            Intent intent2 = new Intent("android.intent.action.SEND");
            intent2.setType(A00);
            if (!TextUtils.isEmpty(str)) {
                intent2.putExtra("android.intent.extra.SUBJECT", str);
            }
            if (!TextUtils.isEmpty(A0Y)) {
                intent2.putExtra("android.intent.extra.TEXT", A0Y);
            }
            intent2.putExtra("origin", 3);
            intent2.putExtra("android.intent.extra.STREAM", uri);
            if (Build.VERSION.SDK_INT >= 29 || !z) {
                intent = Intent.createChooser(intent2, null);
            } else {
                ArrayList arrayList = new ArrayList();
                List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent2, 0);
                if (queryIntentActivities != null) {
                    for (ResolveInfo resolveInfo : queryIntentActivities) {
                        ActivityInfo activityInfo = resolveInfo.activityInfo;
                        String str2 = activityInfo.name;
                        String str3 = activityInfo.applicationInfo.packageName;
                        Intent intent3 = new Intent(intent2);
                        intent3.setClassName(str3, str2);
                        intent3.setPackage(str3);
                        arrayList.add(intent3);
                    }
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Intent intent4 = (Intent) it.next();
                    if ("com.whatsapp".equals(intent4.getPackage()) || "com.whatsapp.w4b".equals(intent4.getPackage())) {
                        intent4.putExtra("enforce_hfm_limit", z);
                    }
                }
                intent = C38211ni.A01(null, null, arrayList);
            }
            intent.setFlags(1);
            r10.A06(context, intent);
        }
    }

    public void A02(Activity activity, AnonymousClass12P r9, AbstractC15340mz r10) {
        String str;
        File file;
        if (!(r10 instanceof AbstractC16130oV)) {
            str = "app/share/message-is-not-media-message";
        } else {
            AbstractC16130oV r6 = (AbstractC16130oV) r10;
            C16150oX r0 = r6.A02;
            if (r0 == null || (file = r0.A0F) == null || !file.exists()) {
                str = "app/share/media-does-not-exist";
            } else {
                AbstractC14440lR r5 = this.A1Q;
                r5.Ab2(new RunnableBRunnable0Shape0S0200000_I0(this, 26, r10));
                WeakReference weakReference = new WeakReference(activity);
                C14900mE r4 = this.A05;
                r4.A06(0, R.string.loading_spinner);
                AbstractC15710nm r3 = this.A02;
                C15670ni r02 = this.A0u;
                AnonymousClass24Q r2 = new AbstractC14590lg(r9, this, r6, weakReference) { // from class: X.24Q
                    public final /* synthetic */ AnonymousClass12P A00;
                    public final /* synthetic */ C16170oZ A01;
                    public final /* synthetic */ AbstractC16130oV A02;
                    public final /* synthetic */ WeakReference A03;

                    {
                        this.A01 = r2;
                        this.A03 = r4;
                        this.A00 = r1;
                        this.A02 = r3;
                    }

                    @Override // X.AbstractC14590lg
                    public final void accept(Object obj) {
                        Uri uri = (Uri) obj;
                        C16170oZ.A01(uri, this.A00, this.A01, this.A02, this.A03);
                    }
                };
                C38971p2 r1 = new C38971p2(r3, r02, r6);
                r1.A01(r2, r4.A06);
                r5.Ab2(r1);
                return;
            }
        }
        Log.w(str);
        C14900mE r32 = this.A05;
        boolean A00 = C14950mJ.A00();
        int i = R.string.gallery_media_not_exist_shared_storage;
        if (A00) {
            i = R.string.gallery_media_not_exist;
        }
        r32.A07(i, 0);
    }

    public void A03(ActivityC13810kN r11, AbstractC16130oV r12, boolean z) {
        C17050qB r6 = this.A0P;
        C14950mJ r7 = this.A0V;
        if (r6.A04(new AnonymousClass249(r11, r7))) {
            AbstractC28771Oy r2 = null;
            if (z) {
                boolean z2 = r12 instanceof AnonymousClass1XV;
                C14900mE r4 = this.A05;
                C16120oU r9 = this.A0y;
                C15610nY r5 = this.A0N;
                C15370n3 A04 = C30041Vv.A04(this.A0c, r12);
                if (z2) {
                    r2 = new AnonymousClass24A(r11, r4, r5, r6, r7, A04, r9);
                } else {
                    r2 = new AnonymousClass24B(r11, r4, r5, r6, r7, A04, r9);
                }
            }
            this.A15.A07(r2, r12, 0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00c9, code lost:
        if (r6.A04(r7.A01, (long) r7.A00) == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003e, code lost:
        if (r1 != X.C14370lK.A04) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A04(X.C38421o4 r21, X.AbstractC14470lU r22, X.AbstractC16130oV r23, byte[] r24, boolean r25, boolean r26, boolean r27) {
        /*
        // Method dump skipped, instructions count: 331
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16170oZ.A04(X.1o4, X.0lU, X.0oV, byte[], boolean, boolean, boolean):void");
    }

    public void A05(C38421o4 r9, byte[] bArr, boolean z, boolean z2) {
        A04(r9, null, null, bArr, false, z, z2);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r37v0, types: [X.0oZ, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r0v45, types: [X.1XV] */
    /* JADX WARN: Type inference failed for: r0v47, types: [X.1X7] */
    /* JADX WARN: Type inference failed for: r0v54, types: [X.1X6] */
    /* JADX WARN: Type inference failed for: r0v55, types: [X.1X7] */
    /* JADX WARN: Type inference failed for: r0v57, types: [X.1XJ] */
    /* JADX WARN: Type inference failed for: r0v58, types: [X.1X7] */
    /* JADX WARN: Type inference failed for: r0v68, types: [X.1X1] */
    /* JADX WARN: Type inference failed for: r0v69, types: [X.1X2] */
    /* JADX WARN: Type inference failed for: r0v71, types: [X.1X2] */
    /* JADX WARN: Type inference failed for: r0v72, types: [X.1X2] */
    /* JADX WARN: Type inference failed for: r0v75, types: [X.1XR] */
    /* JADX WARN: Type inference failed for: r0v76, types: [X.1XR] */
    /* JADX WARN: Type inference failed for: r0v90, types: [X.1Xi] */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x022d, code lost:
        if (r21.hasNext() == false) goto L_0x0782;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x022f, code lost:
        r8 = (X.AbstractC14640lm) r21.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0235, code lost:
        r9 = r37.A1G;
        r11 = r37.A0R.A00();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x023d, code lost:
        if (r23 != false) goto L_0x0242;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x023f, code lost:
        r10 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0240, code lost:
        if (r22 == false) goto L_0x0243;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0242, code lost:
        r10 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0249, code lost:
        if (r40.A12(androidx.core.view.inputmethod.EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH) == false) goto L_0x026d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x024b, code lost:
        r2 = r9.A07.A02(r8, true);
        r5 = r40.A0N;
        r0 = new X.C28861Ph(r2, r11);
        r0.A0l(r5.A06);
        r0.A0N = r5;
        r0.A0T(androidx.core.view.inputmethod.EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0264, code lost:
        r2 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0269, code lost:
        if (X.C30041Vv.A0r(r40) == false) goto L_0x05b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x026f, code lost:
        if ((r40 instanceof X.AnonymousClass1XE) == false) goto L_0x028a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0271, code lost:
        r2 = r9.A07.A02(r8, true);
        r5 = r40.A0I();
        r0 = new X.C28861Ph(r2, r11);
        r0.A0l(X.AnonymousClass1US.A03(65536, r5));
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x028a, code lost:
        if (r13 == false) goto L_0x0591;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x028c, code lost:
        r2 = (X.AbstractC16130oV) r40;
        r0 = r2.A02;
        X.AnonymousClass009.A05(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0296, code lost:
        if (r10 == false) goto L_0x029d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0298, code lost:
        r5 = r0.A02();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x029d, code lost:
        r5 = new X.C16150oX(r0);
        r5.A0P = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02ab, code lost:
        if (r5.A0B != 0) goto L_0x02b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02ad, code lost:
        r5.A0B = r2.A0I;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02b1, code lost:
        r29 = r9.A07.A02(r8, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02b9, code lost:
        if ((r2 instanceof X.C30061Vy) != false) goto L_0x0529;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02bd, code lost:
        if ((r2 instanceof X.AnonymousClass1XF) != false) goto L_0x0768;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02c1, code lost:
        if ((r2 instanceof X.AnonymousClass1XI) != false) goto L_0x0760;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x02c5, code lost:
        if ((r2 instanceof X.AnonymousClass1X7) != false) goto L_0x03ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02c9, code lost:
        if ((r2 instanceof X.AnonymousClass1XG) != false) goto L_0x0758;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x02cd, code lost:
        if ((r2 instanceof X.AnonymousClass1X2) != false) goto L_0x04ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x02d1, code lost:
        if ((r2 instanceof X.AnonymousClass1XR) != false) goto L_0x03d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x02d5, code lost:
        if ((r2 instanceof X.C16440p1) != false) goto L_0x0351;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02d7, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.C30421Xi(r5, r29, (X.C30421Xi) r2, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x02ec, code lost:
        r9 = r9.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x02fb, code lost:
        if (X.C30041Vv.A0Y(r9, r2, X.C15380n4.A0N(r0.A0z.A00)) == false) goto L_0x0328;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02fd, code lost:
        r6 = r2.A0T;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x02ff, code lost:
        if (r6 == null) goto L_0x0328;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0305, code lost:
        if (r6.A01() == false) goto L_0x0328;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0307, code lost:
        r6 = r2.A0G();
        X.AnonymousClass009.A05(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0312, code lost:
        if (r6.A04() == false) goto L_0x0328;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x031e, code lost:
        if (java.util.Arrays.equals(r2.A0T.A09, r5.A0U) != false) goto L_0x0339;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0320, code lost:
        com.whatsapp.util.Log.e("FMessageFactory/newFMessageForForward/thumbnail and media file key not the same");
        r0.A0i(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x032c, code lost:
        if (X.C30041Vv.A0U(r9, r0) == false) goto L_0x0540;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x032e, code lost:
        r2 = r0.A14();
        X.AnonymousClass009.A05(r2);
        r2.A05 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0339, code lost:
        r11 = r2.A0T.A00();
        r11.A02 = r0.A0B;
        r11.A08 = r10;
        r11.A0B = X.C30041Vv.A0X(r9, r0);
        r0.A0i(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0351, code lost:
        r6 = (X.C16440p1) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0356, code lost:
        if ((r6 instanceof X.AnonymousClass1XT) != false) goto L_0x0373;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x035a, code lost:
        if ((r6 instanceof X.AnonymousClass1X0) != false) goto L_0x037d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x035c, code lost:
        X.AnonymousClass009.A05(r5);
        r34 = false;
        r7 = r6.A0y;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0363, code lost:
        r0 = new X.C16440p1(r5, r29, r6, r7, r11, r34);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0373, code lost:
        X.AnonymousClass009.A05(r5);
        r34 = false;
        r7 = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x037b, code lost:
        r6 = null;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x037d, code lost:
        r6 = (X.AnonymousClass1X0) r6;
        X.AnonymousClass009.A05(r5);
        X.AnonymousClass009.A05(r6.ACL());
        r16 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0395, code lost:
        if (r6.ACL().A02() == X.AnonymousClass24E.A02) goto L_0x0399;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0397, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0399, code lost:
        X.AnonymousClass009.A0E(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x03a6, code lost:
        if (r6.ACL().A02() != X.AnonymousClass24E.A01) goto L_0x03c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x03a8, code lost:
        r0 = new X.C16440p1(r5, r29, r6, (byte) 9, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03b9, code lost:
        r6 = null;
        ((X.AbstractC16130oV) r0).A03 = null;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x03bc, code lost:
        r0.A01 = r6;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03c0, code lost:
        r0 = new X.AnonymousClass1X0(r5, r29, r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03d0, code lost:
        r6 = (X.AnonymousClass1XR) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x03d5, code lost:
        if ((r6 instanceof X.AnonymousClass1XQ) != false) goto L_0x03eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x03d7, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1XR(r5, r29, r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03eb, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1XR(((X.AbstractC16130oV) r6).A02, r29, r6, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x03ff, code lost:
        r6 = (X.AnonymousClass1X7) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x0404, code lost:
        if ((r6 instanceof X.AnonymousClass1XV) != false) goto L_0x0496;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x0408, code lost:
        if ((r6 instanceof X.AnonymousClass1XU) != false) goto L_0x0485;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x040c, code lost:
        if ((r6 instanceof X.AnonymousClass1X6) != false) goto L_0x043c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x0410, code lost:
        if ((r6 instanceof X.AnonymousClass1XJ) != false) goto L_0x0426;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0412, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1X7(r5, r29, r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x0426, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1XJ(r5, r29, (X.AnonymousClass1XJ) r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x043c, code lost:
        r6 = (X.AnonymousClass1X6) r6;
        X.AnonymousClass009.A05(r5);
        X.AnonymousClass009.A05(r6.ACL());
        r16 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0454, code lost:
        if (r6.ACL().A02() == X.AnonymousClass24E.A02) goto L_0x0458;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0456, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0458, code lost:
        X.AnonymousClass009.A0E(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x0465, code lost:
        if (r6.ACL().A02() != X.AnonymousClass24E.A01) goto L_0x0475;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0467, code lost:
        r0 = new X.AnonymousClass1X7(r5, r29, r6, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0475, code lost:
        r0 = new X.AnonymousClass1X6(r5, r29, r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0485, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1X7(r5, r29, r6, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x0496, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1XV(r5, r29, (X.AnonymousClass1XV) r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x04ab, code lost:
        r6 = (X.AnonymousClass1X2) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x04b0, code lost:
        if ((r6 instanceof X.AnonymousClass1X1) != false) goto L_0x04e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x04b4, code lost:
        if ((r6 instanceof X.AnonymousClass1XS) != false) goto L_0x04cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x04b6, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1X2(r5, r29, r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x04c8, code lost:
        ((X.AbstractC16130oV) r0).A03 = null;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x04cd, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.AnonymousClass1X2(((X.AbstractC16130oV) r6).A02, r29, r6, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x04e0, code lost:
        r6 = (X.AnonymousClass1X1) r6;
        X.AnonymousClass009.A05(r5);
        X.AnonymousClass009.A05(r6.ACL());
        r16 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x04f8, code lost:
        if (r6.ACL().A02() == X.AnonymousClass24E.A02) goto L_0x04fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x04fa, code lost:
        r16 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x04fc, code lost:
        X.AnonymousClass009.A0E(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0509, code lost:
        if (r6.ACL().A02() != X.AnonymousClass24E.A01) goto L_0x0519;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x050b, code lost:
        r0 = new X.AnonymousClass1X2(r5, r29, r6, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x0519, code lost:
        r0 = new X.AnonymousClass1X1(r5, r29, r6, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0529, code lost:
        X.AnonymousClass009.A05(r5);
        r0 = new X.C30061Vy(r5, r29, (X.C30061Vy) r2, r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x0540, code lost:
        if (r10 == false) goto L_0x0547;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x0542, code lost:
        r0.A0Y(2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x0549, code lost:
        if ((r0 instanceof X.AnonymousClass1X7) == false) goto L_0x0585;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x054b, code lost:
        ((X.AbstractC16130oV) r0).A05 = null;
        ((X.AbstractC16130oV) r0).A04 = null;
        r2 = ((X.AbstractC16130oV) r0).A02;
        X.AnonymousClass009.A05(r2);
        r2.A0K = null;
        r2.A0J = null;
        r9 = r0.A14();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x055c, code lost:
        if (r9 == null) goto L_0x0585;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x055e, code lost:
        r10 = r9.A06();
        r9.A01();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x0565, code lost:
        if (r10 == null) goto L_0x0585;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0567, code lost:
        r12 = r10.length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0569, code lost:
        if (r12 != 4) goto L_0x0585;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x056b, code lost:
        r16 = 0;
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x056e, code lost:
        r16 = r16 + ((long) r10[r11]);
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x0575, code lost:
        if (r11 < r12) goto L_0x056e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x057d, code lost:
        if (((X.AbstractC16130oV) r0).A02.A0A != r16) goto L_0x0585;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x057f, code lost:
        monitor-enter(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0580, code lost:
        r9.A03 = r10;
        r9.A01 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x0584, code lost:
        monitor-exit(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x0585, code lost:
        r0.A0Y(1);
        ((X.AbstractC16130oV) r0).A08 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x058a, code lost:
        r0.A0m(r20);
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x0591, code lost:
        X.AnonymousClass009.A0F(r40 instanceof X.AbstractC16410oy);
        r0 = ((X.AbstractC16410oy) r40).A7L(r9.A07.A02(r8, true), r11);
        r2 = r40.A0T;
        r0 = r0;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x05a5, code lost:
        if (r10 == false) goto L_0x0264;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x05a7, code lost:
        if (r2 == null) goto L_0x0264;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x05a9, code lost:
        r0.A0i(r2.A00());
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x05b2, code lost:
        r2 = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x05b3, code lost:
        r0.A08 = r2;
        r9 = X.C15380n4.A0N(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x05b9, code lost:
        if (r9 == false) goto L_0x0657;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x05bd, code lost:
        if ((r0 instanceof X.C28861Ph) == false) goto L_0x0657;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x05bf, code lost:
        r6 = new com.whatsapp.TextData();
        r8 = X.AnonymousClass24D.A01;
        r6.backgroundColor = r8[java.lang.Math.abs(X.AnonymousClass24D.A00.nextInt()) % r8.length];
        r6.textColor = -1;
        r6.fontStyle = 0;
        ((X.C28861Ph) r0).A15(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x05e6, code lost:
        if (X.C35011h5.A04(r0) == false) goto L_0x0642;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x05e8, code lost:
        r7 = new java.lang.StringBuilder();
        r5 = r0.A0I();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x05f5, code lost:
        if (android.text.TextUtils.isEmpty(r5) != false) goto L_0x05fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x05f7, code lost:
        r7.append(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x05fa, code lost:
        r5 = X.C35011h5.A01(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0604, code lost:
        if (android.text.TextUtils.isEmpty(r5) != false) goto L_0x0612;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x060a, code lost:
        if (android.text.TextUtils.isEmpty(r7) != false) goto L_0x060f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x060c, code lost:
        r7.append("\n\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x060f, code lost:
        r7.append(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x0612, code lost:
        r5 = r0.A0F().A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x0618, code lost:
        if (r5 == null) goto L_0x0630;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0620, code lost:
        if (android.text.TextUtils.isEmpty(r5.A01) != false) goto L_0x0630;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x0626, code lost:
        if (android.text.TextUtils.isEmpty(r7) != false) goto L_0x062b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x0628, code lost:
        r7.append("\n\n");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x062b, code lost:
        r7.append(r5.A01);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x0630, code lost:
        r5 = r7.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x0638, code lost:
        if (android.text.TextUtils.isEmpty(r5) != false) goto L_0x0657;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x063a, code lost:
        r0.A0l(X.AnonymousClass24D.A06(r5));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x064a, code lost:
        if (android.text.TextUtils.isEmpty(r40.A0I()) != false) goto L_0x0657;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x064c, code lost:
        r0.A0l(X.AnonymousClass24D.A06(r0.A0I()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x0657, code lost:
        if (r19 == false) goto L_0x065e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0659, code lost:
        r0.A0T(1);
        r0.A05 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x0660, code lost:
        if ((r0 instanceof X.C30061Vy) == false) goto L_0x066c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:270:0x0662, code lost:
        ((X.C30061Vy) r0).A02 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x066c, code lost:
        A0M(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x066f, code lost:
        if (r9 == false) goto L_0x0675;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x0671, code lost:
        r0.A0K = r39;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x0675, code lost:
        r2 = r0.A0y;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x0677, code lost:
        if (r2 == 0) goto L_0x067b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x0679, code lost:
        if (r2 != 1) goto L_0x0689;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x067b, code lost:
        if (r9 == false) goto L_0x0689;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x067d, code lost:
        r37.A1K.A05(r0, 1, 0, false, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x0689, code lost:
        r14.add(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x06b7, code lost:
        if (X.C14390lM.A00(new X.C14390lM(r10, r5), r37.A0R.A00()) == false) goto L_0x06b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x071c, code lost:
        if (r10 < r5) goto L_0x071e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:317:0x071e, code lost:
        r23 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:318:0x0720, code lost:
        if (r17 == false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:326:0x073c, code lost:
        if (r2.A01() != false) goto L_0x071e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x075f, code lost:
        throw new X.AnonymousClass24F("ViewOnce messages can not be forwarded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x0767, code lost:
        throw new X.AnonymousClass24F("ViewOnce messages can not be forwarded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x076f, code lost:
        throw new X.AnonymousClass24F("Order messages can not be forwarded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:341:0x0770, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:342:0x0771, code lost:
        com.whatsapp.util.Log.e("UserActions/userActionForwardMessage ", r1);
        r37.A02.AaV("UserActions/userActionForwardMessage", r1.getMessage(), true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:343:0x0781, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:345:0x0786, code lost:
        if (X.C30041Vv.A0G(r0) == false) goto L_0x07cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:346:0x0788, code lost:
        if (r23 != false) goto L_0x07cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:347:0x078a, code lost:
        X.AnonymousClass009.A0F(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x078d, code lost:
        if (r22 == false) goto L_0x079c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:349:0x078f, code lost:
        r37.A1Q.Ab2(new com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0(r37, r40, r14, 8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:0x079b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:351:0x079c, code lost:
        r4 = new java.util.ArrayList(r41.size());
        r2 = r14.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:353:0x07ad, code lost:
        if (r2.hasNext() == false) goto L_0x07b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:0x07af, code lost:
        r4.add(r2.next());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:355:0x07b7, code lost:
        A04(new X.C38421o4(r4), null, (X.AbstractC16130oV) r40, r18, false, false, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:0x07ca, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x07cb, code lost:
        A0O(r40, new com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0(r37, r40, r14, 7), r14, r18);
        r2 = X.C15380n4.A0N(r0);
        r0 = X.C15380n4.A0J(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:358:0x07de, code lost:
        if (r2 == false) goto L_0x07ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x07e0, code lost:
        r5 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:0x07e1, code lost:
        r37.A1Q.Ab2(new com.facebook.redex.RunnableBRunnable0Shape0S0201000_I0((java.lang.Object) r37, r40, r5, 6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:0x07ec, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:362:0x07ed, code lost:
        r5 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:363:0x07ee, code lost:
        if (r0 == false) goto L_0x07e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:364:0x07f0, code lost:
        r5 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0214, code lost:
        if (r16 != false) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0216, code lost:
        if (r0 == false) goto L_0x0223;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0218, code lost:
        r22 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0221, code lost:
        if (X.C30041Vv.A14((X.AnonymousClass1X2) r40) != false) goto L_0x0225;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0223, code lost:
        r22 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0225, code lost:
        r21 = r41.iterator();
     */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x06bc  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x06e6  */
    /* JADX WARNING: Removed duplicated region for block: B:315:0x071a  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x072a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A06(X.C239613r r38, X.C32731ce r39, X.AbstractC15340mz r40, java.util.List r41) {
        /*
        // Method dump skipped, instructions count: 2042
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16170oZ.A06(X.13r, X.1ce, X.0mz, java.util.List):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        if (X.AnonymousClass24D.A09(r19, X.AnonymousClass24D.A01(r24)) == false) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A07(X.C14320lF r19, X.C14310lE r20, X.C32731ce r21, X.C32361c2 r22, X.AbstractC15340mz r23, java.lang.String r24, java.util.List r25, java.util.List r26, boolean r27, boolean r28, boolean r29) {
        /*
        // Method dump skipped, instructions count: 249
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16170oZ.A07(X.0lF, X.0lE, X.1ce, X.1c2, X.0mz, java.lang.String, java.util.List, java.util.List, boolean, boolean, boolean):void");
    }

    public void A08(C14320lF r13, C32731ce r14, AbstractC15340mz r15, String str, List list, List list2, boolean z, boolean z2) {
        A07(r13, null, r14, null, r15, str, list, list2, z, z2, false);
    }

    public void A09(AbstractC14640lm r5) {
        if (r5 != null && !C15380n4.A0F(r5) && !this.A07.A0F(r5)) {
            C22930zs r0 = this.A06;
            r0.A00.obtainMessage(1, 0, 0, r0.A00(r5)).sendToTarget();
        }
    }

    public final void A0A(AbstractC14640lm r7) {
        C15860o1 r5 = this.A1J;
        if (r5.A08(r7.getRawString()).A0F) {
            C18850tA r4 = this.A0J;
            Set A06 = r4.A06(r7, false);
            r5.A0A(r7, 0, false);
            r4.A0O(A06);
        }
    }

    public void A0B(AbstractC14640lm r4, int i) {
        if (r4 != null && !C15380n4.A0F(r4)) {
            if (C15380n4.A0J(r4)) {
                this.A1E.A04(r4);
            }
            if (!this.A07.A0F(r4)) {
                C22930zs r0 = this.A06;
                r0.A00.obtainMessage(0, 0, i, r0.A00(r4)).sendToTarget();
            }
        }
    }

    public void A0C(AbstractC14640lm r5, int i) {
        if (r5 != null) {
            if (this.A00 == null) {
                PowerManager A0I = this.A0Q.A0I();
                if (A0I == null) {
                    Log.w("useractions/get-web-wakelock pm=null");
                } else {
                    PowerManager.WakeLock A00 = C39151pN.A00(A0I, "Web Client", 1);
                    this.A00 = A00;
                    A00.setReferenceCounted(false);
                }
            }
            PowerManager.WakeLock wakeLock = this.A00;
            if (wakeLock != null) {
                wakeLock.acquire(C26061Bw.A0L);
            }
            if (!C15380n4.A0F(r5)) {
                if (C15380n4.A0J(r5)) {
                    this.A1E.A04(r5);
                }
                if (!this.A07.A0F(r5)) {
                    C22930zs r0 = this.A06;
                    r0.A00.obtainMessage(3, 0, i, r0.A00(r5)).sendToTarget();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
        if (r17.A0j.A0C((com.whatsapp.jid.GroupJid) r18) == false) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A0D(X.AbstractC14640lm r18, long r19, boolean r21) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16170oZ.A0D(X.0lm, long, boolean):void");
    }

    public void A0E(AbstractC14640lm r14, long j, boolean z, boolean z2) {
        A0D(r14, j, z);
        C18850tA r6 = this.A0J;
        long j2 = -1;
        if (j != -1) {
            j2 = this.A0R.A00() + (j - System.currentTimeMillis());
        }
        Set A04 = r6.A04(r14, j2, true);
        if (this.A1J.A0T(r14, j, z)) {
            r6.A0O(A04);
            if (z2) {
                this.A1C.A04(r14, 5, 0, j);
                return;
            }
            return;
        }
        r6.A0N(A04);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0F(X.AbstractC14640lm r21, X.AbstractC15340mz r22, X.AnonymousClass1KS r23, java.lang.Integer r24, boolean r25) {
        /*
            r20 = this;
            X.0oX r8 = new X.0oX
            r8.<init>()
            r4 = r23
            java.lang.String r2 = r4.A08
            if (r2 == 0) goto L_0x0039
            int r1 = r4.A01
            r0 = 3
            if (r1 != r0) goto L_0x0032
            android.net.Uri r7 = android.net.Uri.parse(r2)
        L_0x0014:
            int r0 = r4.A00
            long r5 = (long) r0
            r1 = 1048576(0x100000, double:5.180654E-318)
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            r3 = r20
            if (r0 <= 0) goto L_0x003b
            X.0pI r0 = r3.A0S
            android.content.Context r1 = r0.A00
            r0 = 2131891981(0x7f12170d, float:1.9418697E38)
            java.lang.String r2 = r1.getString(r0)
            X.0mE r1 = r3.A05
            r0 = 1
            r1.A0E(r2, r0)
            return
        L_0x0032:
            java.io.File r0 = new java.io.File
            r0.<init>(r2)
            r8.A0F = r0
        L_0x0039:
            r7 = 0
            goto L_0x0014
        L_0x003b:
            int r0 = r4.A03
            r8.A08 = r0
            int r0 = r4.A02
            r8.A06 = r0
            X.0lO r6 = r3.A11
            r2 = 0
            r16 = 20
            r9 = 0
            r13 = r9
            r14 = r9
            r15 = r9
            r18 = 0
            r17 = 0
            r11 = r22
            r10 = r21
            r19 = r25
            r12 = r9
            X.0oV r1 = r6.A03(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
            X.1Vy r1 = (X.C30061Vy) r1
            java.lang.String r0 = r4.A0C
            r1.A05 = r0
            java.lang.String r0 = r4.A0B
            r1.A06 = r0
            if (r0 != 0) goto L_0x006b
            java.lang.String r0 = "image/webp"
            r1.A06 = r0
        L_0x006b:
            X.1KB r0 = r4.A04
            r1.A01 = r0
            r0 = r24
            r1.A02 = r0
            java.util.List r1 = java.util.Collections.singletonList(r1)
            X.1o4 r0 = new X.1o4
            r0.<init>(r1)
            r3.A05(r0, r9, r2, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16170oZ.A0F(X.0lm, X.0mz, X.1KS, java.lang.Integer, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0G(X.AbstractC14640lm r34, X.AnonymousClass1P4 r35, java.lang.String r36, java.util.List r37, boolean r38) {
        /*
        // Method dump skipped, instructions count: 1272
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C16170oZ.A0G(X.0lm, X.1P4, java.lang.String, java.util.List, boolean):void");
    }

    public void A0H(AbstractC14640lm r6, String str, String str2, String str3, long j) {
        C20320vZ r4 = this.A1G;
        AnonymousClass1XE r3 = new AnonymousClass1XE(r4.A07.A02(r6, true), this.A0R.A00());
        C15650ng r2 = this.A0f;
        AbstractC15340mz A00 = r2.A0K.A00(j);
        if (A00 != null) {
            r4.A04(r3, A00);
        }
        r3.A00 = new AnonymousClass1ZL(new AnonymousClass1ZM(str2, str3), str);
        r2.A0S(r3);
    }

    public void A0I(AbstractC14640lm r10, boolean z) {
        A0D(r10, 0, false);
        C18850tA r2 = this.A0J;
        Set A04 = r2.A04(r10, 0, false);
        if (this.A1J.A0R(r10)) {
            r2.A0O(A04);
            if (z) {
                this.A1C.A04(r10, 5, 0, 0);
                return;
            }
            return;
        }
        r2.A0N(A04);
    }

    public void A0J(AbstractC14640lm r11, boolean z, boolean z2) {
        Set emptySet;
        C30141Wg A00;
        this.A0I.A00(r11, 6);
        C15370n3 A0A = this.A0K.A0A(r11);
        C26681Ek r4 = this.A0B;
        boolean z3 = true;
        if (A0A == null || !A0A.A0K() || r4.A03.A02((GroupJid) A0A.A0B(GroupJid.class)) != 1) {
            z3 = false;
        }
        C20220vP r2 = this.A1D;
        r2.A09(r11, null);
        if (z2) {
            emptySet = r4.A01.A07(r11, z);
        } else {
            emptySet = Collections.emptySet();
        }
        r4.A04.A0P(r11, z);
        r4.A01.A0O(emptySet);
        if (A0A != null) {
            if (A0A.A0K() || C15380n4.A0F(A0A.A0D)) {
                this.A0C.A06(A0A);
            }
            this.A0C.A07(r11);
            this.A1J.A0K(r11, A0A.A0K());
            A0A(r11);
            this.A0O.A02((AbstractC14640lm) A0A.A0B(AbstractC14640lm.class), true, true);
            r2.A07();
            r4.A00(A0A, r11, z3, z2);
            UserJid userJid = (UserJid) A0A.A0B(UserJid.class);
            if (userJid != null) {
                C18780t0 r8 = this.A0M;
                C16310on A02 = r8.A03.A00.A02();
                try {
                    AbstractC21570xd.A02(A02, "wa_trusted_contacts", "wa_trusted_contacts.jid = ?", new String[]{userJid.getRawString()});
                    AbstractC21570xd.A02(A02, "wa_trusted_contacts_send", "wa_trusted_contacts_send.jid = ?", new String[]{userJid.getRawString()});
                    A02.close();
                    r8.A06().remove(userJid);
                } catch (Throwable th) {
                    try {
                        A02.close();
                    } catch (Throwable unused) {
                    }
                    throw th;
                }
            }
            if (A0A.A0J() && (A00 = this.A0F.A09.A00(userJid)) != null && A00.A0H) {
                C246816l r0 = this.A0G;
                String rawString = r11.getRawString();
                C14820m6 r02 = r0.A08;
                r02.A0Z(rawString);
                SharedPreferences sharedPreferences = r02.A00;
                SharedPreferences.Editor edit = sharedPreferences.edit();
                StringBuilder sb = new StringBuilder("smb_business_direct_connection_enc_string_");
                sb.append(rawString);
                edit.remove(sb.toString()).apply();
                SharedPreferences.Editor edit2 = sharedPreferences.edit();
                StringBuilder sb2 = new StringBuilder("smb_business_direct_connection_enc_string_expired_timestamp_");
                sb2.append(rawString);
                edit2.remove(sb2.toString()).apply();
                SharedPreferences.Editor edit3 = sharedPreferences.edit();
                StringBuilder sb3 = new StringBuilder("dc_user_postcode_");
                sb3.append(rawString);
                edit3.remove(sb3.toString()).apply();
                SharedPreferences.Editor edit4 = sharedPreferences.edit();
                StringBuilder sb4 = new StringBuilder("dc_location_name_");
                sb4.append(rawString);
                edit4.remove(sb4.toString()).apply();
                SharedPreferences.Editor edit5 = sharedPreferences.edit();
                StringBuilder sb5 = new StringBuilder("dc_default_postcode_");
                sb5.append(rawString);
                edit5.remove(sb5.toString()).apply();
                SharedPreferences.Editor edit6 = sharedPreferences.edit();
                StringBuilder sb6 = new StringBuilder("dc_business_domain_");
                sb6.append(rawString);
                edit6.remove(sb6.toString()).apply();
            }
        }
        if (C15380n4.A0G(r11)) {
            C20660w7 r6 = this.A1B;
            AnonymousClass248 r3 = new AnonymousClass248(this.A0d, (C29901Ve) r11, this.A1S);
            if (r6.A01.A06) {
                Log.i("sendmethods/sendDeleteBroadcastList");
                r6.A06.A08(Message.obtain(null, 0, 60, 0, r3), false);
            }
        }
        this.A0C.A02();
        C38331nu.A00.put(r11, false);
    }

    public void A0K(UserJid userJid, int i) {
        AnonymousClass1PG A08 = this.A0b.A08(userJid);
        if (A08 == null || A08.expiration != i) {
            C20320vZ r1 = this.A1G;
            AnonymousClass1XK r12 = new AnonymousClass1XK(r1.A07.A02(userJid, true), this.A0R.A00());
            r12.A00 = i;
            r12.A0e(userJid);
            this.A0f.A0S(r12);
        }
    }

    public void A0L(AbstractC14470lU r9, AbstractC16130oV r10) {
        A04(new C38421o4(Collections.singletonList(r10)), r9, null, null, false, false, false);
    }

    public void A0M(AbstractC15340mz r9) {
        C40511ri A00;
        AbstractC14640lm r2 = r9.A0z.A00;
        if (C15380n4.A0N(r2)) {
            r9.A0e(C29831Uv.A00);
        } else if (C15380n4.A0F(r2)) {
            r9.A0r = true;
            AnonymousClass1YM A02 = this.A0j.A02((AbstractC15590nW) r2);
            r9.A0k = this.A0c.A01(r2).A0K;
            ArrayList arrayList = new ArrayList(new HashSet(A02.A06().A00));
            C15570nT r0 = this.A07;
            r0.A08();
            arrayList.remove(r0.A05);
            r9.A0u(arrayList);
        }
        C19990v2 r1 = this.A0b;
        C14850m9 r5 = this.A0x;
        if ((r9 instanceof C27671Iq) || (!(r9 instanceof AnonymousClass1Iv) && r1.A02(C15580nU.A02(r2)) == 3 && r5.A07(1921))) {
            r9.A0t = true;
            byte[] bArr = new byte[32];
            C002901h.A00().nextBytes(bArr);
            if (r9.A10()) {
                r9.A1D = bArr;
            }
        }
        C21550xb A01 = this.A0X.A01();
        UserJid of = UserJid.of(r2);
        if (of != null) {
            if (A01.A05.A00()) {
                if (A01.A01.A00.A07(823)) {
                    C17690rE r12 = A01.A02;
                    r12.A06(r12.A00(of), r9);
                } else {
                    A01.A01(of, (AnonymousClass22F) A01.A08.A00.get(of));
                }
            }
            AnonymousClass22F r7 = (AnonymousClass22F) A01.A08.A00.get(of);
            if (A01.A06.A00() && r7 != null) {
                if (System.currentTimeMillis() - r7.A01 <= AnonymousClass22F.A06 && !r7.A02 && r7.A00 == 0) {
                    r9.A0d = r7.A04;
                    r9.A0e = r7.A05;
                }
            }
        }
        if (r5.A07(508) && of != null) {
            C40501rh r02 = this.A0W.A01;
            String string = r02.A00.A01(r02.A01).getString(of.getRawString(), null);
            if (string != null && (A00 = C40501rh.A00(string)) != null) {
                r9.A0j = A00.A06;
                r9.A0i = A00.A05;
                r9.A03 = (int) TimeUnit.SECONDS.convert(System.currentTimeMillis() - A00.A01, TimeUnit.MILLISECONDS);
            }
        }
    }

    public void A0N(AbstractC15340mz r4) {
        byte b = r4.A0y;
        AnonymousClass009.A0F(!C30041Vv.A0G(b));
        AnonymousClass009.A0F(!C30041Vv.A0I(b));
        this.A1A.A02(r4, false);
        if (r4 instanceof AnonymousClass1Iv) {
            C15650ng r0 = this.A0f;
            r4 = r0.A0K.A03(((AnonymousClass1Iv) r4).A14());
        }
        this.A0n.A08(r4, -1);
    }

    public final void A0O(AbstractC15340mz r4, Runnable runnable, Collection collection, byte[] bArr) {
        if (bArr != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                C16460p3 A0G = ((AbstractC15340mz) it.next()).A0G();
                AnonymousClass009.A05(A0G);
                A0G.A02(bArr);
            }
        }
        if (r4 == null) {
            this.A05.A0I(runnable);
        } else {
            this.A1F.A01(r4, new RunnableBRunnable0Shape0S0300000_I0(r4, collection, runnable, 10));
        }
    }

    public void A0P(AbstractC15340mz r8, String str, String str2, String str3, String str4, List list, byte[] bArr) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C20320vZ r4 = this.A1G;
            C16380ov r3 = new C16380ov(r4.A07.A02((AbstractC14640lm) it.next(), true), (byte) 55, this.A0R.A00());
            r3.Abv(new C16470p4(new AnonymousClass1Z6(str2, str3, bArr), new AnonymousClass1ZE(1, str, 1), str4));
            A0M(r3);
            if (r8 != null) {
                r4.A04(r3, r8);
            }
            this.A0f.A0S(r3);
            AnonymousClass24P r32 = new AnonymousClass24P();
            r32.A00 = 6;
            r32.A01 = 33;
            this.A0y.A0B(r32, new AnonymousClass00E(1, 1), false);
        }
    }

    public void A0Q(AbstractC15340mz r18, String str, String str2, List list, boolean z) {
        C16590pI r3;
        String str3 = str;
        if (str2 != null) {
            try {
                byte[] bytes = str2.getBytes(AnonymousClass01V.A08);
                if (bytes != null) {
                    long length = (long) bytes.length;
                    C15450nH r8 = this.A09;
                    if (length > ((long) r8.A02(AbstractC15460nI.A2E)) * 1024) {
                        this.A05.A0F(this.A0U.A0D(1, R.plurals.contact_too_large), 1);
                        return;
                    }
                    long A02 = ((long) r8.A02(AbstractC15460nI.A2D)) * 1024;
                    if (A02 > 0 && length > A02) {
                        if (str == null && (str3 = C30721Yo.A00(this.A0F, this.A0K, (r3 = this.A0S), this.A0U, str2)) == null) {
                            str3 = r3.A00.getString(R.string.contact);
                        }
                        this.A1Q.Ab2(new RunnableBRunnable0Shape0S1411000_I0(this, list, bytes, r18, str3, 1, 1, z));
                        return;
                    }
                }
            } catch (UnsupportedEncodingException unused) {
            }
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C20320vZ r5 = this.A1G;
            C30411Xh r2 = new C30411Xh(r5.A07.A02((AbstractC14640lm) it.next(), true), this.A0R.A00());
            r2.A16(str2);
            r2.A00 = str3;
            r5.A04(r2, r18);
            if (z) {
                r2.A0T(4);
            }
            A0M(r2);
            this.A0f.A0S(r2);
        }
    }

    public void A0R(AbstractC15340mz r29, List list, List list2, boolean z) {
        String string;
        String join = TextUtils.join("\n", list2);
        byte[] bArr = null;
        if (join != null) {
            try {
                bArr = join.getBytes(AnonymousClass01V.A08);
            } catch (UnsupportedEncodingException unused) {
            }
        }
        if (bArr != null) {
            int length = bArr.length;
            int size = list2.size();
            long j = (long) length;
            C15450nH r10 = this.A09;
            if (j > ((long) r10.A02(AbstractC15460nI.A2E)) * 1024) {
                this.A05.A0F(this.A0U.A0D((long) size, R.plurals.contact_too_large), 1);
                return;
            }
            long A02 = ((long) r10.A02(AbstractC15460nI.A2D)) * 1024;
            if (A02 > 0 && j > A02 && list2.size() > 0) {
                C16590pI r3 = this.A0S;
                C15550nR r2 = this.A0K;
                AnonymousClass018 r9 = this.A0U;
                C14650lo r1 = this.A0F;
                int size2 = list2.size();
                String A00 = C30721Yo.A00(r1, r2, r3, r9, (String) list2.get(0));
                if (A00 != null) {
                    int i = size2 - 1;
                    string = r9.A0I(new Object[]{A00, Integer.valueOf(i)}, R.plurals.contacts_array_title, (long) i);
                } else {
                    string = r3.A00.getString(R.string.contacts);
                }
                this.A1Q.Ab2(new RunnableBRunnable0Shape0S1411000_I0(this, list, bArr, r29, string, size, 1, z));
                return;
            }
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C20320vZ r7 = this.A1G;
            long A002 = this.A0R.A00();
            String A0I = this.A0U.A0I(new Object[]{Integer.valueOf(list2.size())}, R.plurals.n_contacts_message_title, (long) list2.size());
            C16590pI r6 = r7.A03;
            C30351Xb r12 = new C30351Xb(r7.A01, r7.A02, r6, r7.A04, r7.A07.A02((AbstractC14640lm) it.next(), true), A002);
            r12.A00 = A0I;
            r12.A15(list2);
            r7.A04(r12, r29);
            if (z) {
                r12.A0T(4);
            }
            A0M(r12);
            this.A0f.A0S(r12);
        }
    }

    public void A0S(AnonymousClass1XP r17) {
        AbstractC14440lR r1 = this.A1Q;
        C14830m7 r7 = this.A0R;
        AbstractC15710nm r4 = this.A02;
        C18790t3 r6 = this.A0A;
        C15650ng r9 = this.A0f;
        AnonymousClass12H r11 = this.A0n;
        C18810t5 r13 = this.A0z;
        C15890o4 r8 = this.A0T;
        C244615p r5 = this.A04;
        C22830zi r12 = this.A0o;
        C16030oK r14 = this.A10;
        r1.Aaz(new AnonymousClass24M(this.A01, r4, r5, r6, r7, r8, r9, this.A0j, r11, r12, r13, r14, r17), new Void[0]);
    }

    public void A0T(Collection collection, boolean z) {
        StringBuilder sb = new StringBuilder("useractions/userActionDeleteMessages ");
        sb.append(collection.size());
        Log.i(sb.toString());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r2 = (AbstractC15340mz) it.next();
            r2.A1A = true;
            if ((r2 instanceof AbstractC16130oV) && r2.A0z.A02) {
                this.A12.A05(r2, false);
            }
            this.A0m.A03(r2.A0z);
        }
        this.A0Y.A01(new RunnableBRunnable0Shape0S0210000_I0(this, collection, 2, z), 20);
    }

    public void A0U(Collection collection, boolean z, boolean z2) {
        C30061Vy r6;
        AnonymousClass1KB r0;
        this.A0p.A01(collection, true, z);
        ArrayList arrayList = new ArrayList();
        Iterator it = collection.iterator();
        long j = 0;
        while (it.hasNext()) {
            AbstractC15340mz r4 = (AbstractC15340mz) it.next();
            if (r4.A0y == 20 && ((r0 = (r6 = (C30061Vy) r4).A01) == null || !r0.A05 || r4.A0z.A02)) {
                C16150oX r02 = ((AbstractC16130oV) r6).A02;
                if (r02 != null && r02.A0P) {
                    j = Math.max(j, r4.A0I);
                    arrayList.add(r6.A1C());
                }
            }
        }
        if (!arrayList.isEmpty()) {
            boolean A01 = AnonymousClass01I.A01();
            C235512c r3 = this.A1M;
            if (A01) {
                r3.A0K(arrayList);
            } else {
                r3.A0L(arrayList, true);
            }
        }
        if (z2 && j > 0) {
            this.A05.A0H(new RunnableBRunnable0Shape1S0100000_I0_1(this, 25));
        }
    }

    public void A0V(Set set, boolean z) {
        long j;
        C30321Wy r6;
        boolean A07 = this.A0x.A07(1292);
        Iterator it = set.iterator();
        while (it.hasNext()) {
            AbstractC15340mz r5 = (AbstractC15340mz) it.next();
            r5.A1A = true;
            if (!C30041Vv.A0m(r5)) {
                AnonymousClass1IS r7 = r5.A0z;
                if (r7.A02) {
                    if (r5 instanceof AbstractC16130oV) {
                        this.A12.A05(r5, false);
                    }
                    if (C15380n4.A0N(r7.A00)) {
                        AnonymousClass1BD r62 = this.A1K;
                        AnonymousClass24L r4 = new AnonymousClass24L();
                        C458223h r0 = r62.A00;
                        if (r0 == null) {
                            j = 0;
                        } else {
                            j = r0.A05;
                        }
                        r4.A02 = Long.valueOf(j);
                        r4.A00 = Integer.valueOf(C20870wS.A01(r62.A08, r5));
                        r4.A01 = Long.valueOf(r62.A04.A00() - r5.A0I);
                        r62.A0A.A06(r4);
                    }
                    if (r5 instanceof C28581Od) {
                        C28581Od r42 = (C28581Od) r5;
                        if (this.A0R.A00() < r42.A01 * 1000 && !r42.A07) {
                            C20660w7 r3 = this.A1B;
                            C15580nU r2 = r42.A02;
                            AnonymousClass009.A05(r2);
                            r3.A00(null, r2, null, Collections.singletonList(r42.A0z.A00));
                        }
                    }
                    r6 = this.A1G.A02(r5, this.A0R.A00());
                } else if (A07) {
                    C20320vZ r1 = this.A1G;
                    long A00 = this.A0R.A00();
                    C30321Wy r63 = new C30321Wy(r1.A07.A02(r7.A00, false), r5.A0I);
                    ((C30331Wz) r63).A00 = A00;
                    ((C30331Wz) r63).A01 = r7.A01;
                    r63.A0M = r5.A0B();
                    r63.A0p = r5.A0R();
                    r63.A06 = r5.A06();
                    C15570nT r02 = this.A07;
                    r02.A08();
                    r63.A00 = r02.A05;
                    r6 = r63;
                }
                C238113c r32 = this.A0g;
                r32.A02.A01(new RunnableBRunnable0Shape0S0210000_I0(r32, r6, 11, z), 31);
            }
        }
    }

    public boolean A0W(AbstractC15340mz r18, String str, boolean z) {
        C16120oU r4;
        int A01;
        int A04;
        int i;
        C238013b r5 = this.A0H;
        AnonymousClass1IS r2 = r18.A0z;
        AbstractC14640lm r42 = r2.A00;
        if (r5.A0I(UserJid.of(r42)) && !TextUtils.isEmpty(str)) {
            return false;
        }
        C40711sC r8 = new C40711sC(r18.A0C(), r2);
        C40711sC A012 = C40721sD.A01(r18);
        long j = r18.A11;
        C20320vZ r22 = this.A1G;
        AnonymousClass009.A05(r42);
        C14830m7 r0 = this.A0R;
        AnonymousClass1X9 r6 = new AnonymousClass1X9(r22.A07.A02(r42, true), r8, A012, str, r0.A00(), j, r0.A00());
        C239113m r52 = this.A0t;
        r52.A05.A01(new RunnableBRunnable0Shape4S0200000_I0_4(r52, 37, r6), 53);
        if (TextUtils.isEmpty(str)) {
            C26661Ei r02 = this.A1H;
            r4 = r02.A01;
            A01 = C20870wS.A01(r02.A00, r18);
            A04 = C20870wS.A04(r6);
            i = 2;
        } else if (!z) {
            return true;
        } else {
            C26661Ei r03 = this.A1H;
            r4 = r03.A01;
            A01 = C20870wS.A01(r03.A00, r18);
            A04 = C20870wS.A04(r6);
            i = 3;
        }
        AnonymousClass24O r1 = new AnonymousClass24O();
        r1.A00 = Integer.valueOf(A01);
        r1.A01 = Integer.valueOf(A04);
        r1.A02 = Integer.valueOf(i);
        r4.A07(r1);
        return true;
    }
}
