package X;

import android.content.Context;
import android.view.View;
import com.whatsapp.R;

/* renamed from: X.2hQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54872hQ extends AnonymousClass03U {
    public C54872hQ(View.OnClickListener onClickListener, View view, AnonymousClass12P r14, C14900mE r15, AnonymousClass01d r16, C252018m r17, String str, int i) {
        super(view);
        AnonymousClass028.A0D(view, R.id.cancel_warning_button).setOnClickListener(onClickListener);
        Context context = view.getContext();
        C42971wC.A08(context, r17.A03(str), r14, r15, C12970iu.A0T(view, R.id.warning_text), r16, C12960it.A0X(context, "learn-more", C12970iu.A1b(), 0, i), "learn-more");
    }
}
