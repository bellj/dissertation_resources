package X;

/* renamed from: X.5bU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118235bU extends AnonymousClass0Yo {
    public final /* synthetic */ C128375w0 A00;

    public C118235bU(C128375w0 r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass0Yo, X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        if (cls.isAssignableFrom(C123395n5.class)) {
            C128375w0 r0 = this.A00;
            C16590pI r15 = r0.A0B;
            C14850m9 r14 = r0.A0J;
            C14830m7 r13 = r0.A0A;
            C15570nT r12 = r0.A03;
            AnonymousClass018 r11 = r0.A0C;
            AnonymousClass60Y r10 = r0.A0c;
            C20920wX r9 = r0.A00;
            C130155yt r8 = r0.A0W;
            C129685y8 r7 = r0.A0l;
            AnonymousClass61F r6 = r0.A0d;
            C129235xO r5 = r0.A0k;
            C130125yq r4 = r0.A0a;
            return new C123395n5(r9, r12, r13, r15, r11, r0.A0H, r14, r0.A0O, r8, r4, r10, r6, r5, r7, r0.A0m, r0.A0o);
        }
        throw C12970iu.A0f("Invalid viewModel for NoviPayHubActivity");
    }
}
