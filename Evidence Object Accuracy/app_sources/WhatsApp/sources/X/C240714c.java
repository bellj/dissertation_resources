package X;

import com.whatsapp.data.ConversationDeleteWorker;
import java.util.Map;

/* renamed from: X.14c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C240714c {
    public final C19990v2 A00;
    public final C21610xh A01;
    public final C21710xr A02;

    public C240714c(C19990v2 r1, C21610xh r2, C21710xr r3) {
        this.A00 = r1;
        this.A02 = r3;
        this.A01 = r2;
    }

    public void A00(C27441Hl r9, String str) {
        C004201x r4 = new C004201x(ConversationDeleteWorker.class);
        C006403a r7 = new C006403a();
        Map map = r7.A00;
        map.put("delete_action", str);
        AbstractC14640lm r5 = r9.A07;
        map.put("jid_to_delete", r5.getRawString());
        map.put("job_id", Long.valueOf(r9.A06));
        r4.A00.A0A = r7.A00();
        r4.A01.add(String.valueOf(r5.hashCode()));
        EnumC006603c r2 = EnumC006603c.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        C004401z r1 = r4.A00;
        r1.A0H = true;
        r1.A0C = r2;
        ((AnonymousClass022) this.A02.get()).A06(r4.A00());
    }
}
