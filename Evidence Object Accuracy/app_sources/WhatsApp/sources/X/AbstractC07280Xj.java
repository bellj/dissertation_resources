package X;

import java.util.List;

/* renamed from: X.0Xj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public abstract class AbstractC07280Xj implements AbstractC11720gk {
    public int A00;
    public int A01 = 0;
    public AnonymousClass0JR A02;
    public AnonymousClass0QV A03;
    public C07270Xi A04 = new C07270Xi(this);
    public C07270Xi A05 = new C07270Xi(this);
    public C02580Cy A06 = new C02580Cy(this);
    public AnonymousClass0PH A07;
    public AnonymousClass0J2 A08 = AnonymousClass0J2.NONE;
    public boolean A09 = false;

    public abstract void A06();

    public abstract void A07();

    public abstract void A08();

    public abstract boolean A0B();

    @Override // X.AbstractC11720gk
    public abstract void AfH(AbstractC11720gk v);

    public AbstractC07280Xj(AnonymousClass0QV r2) {
        this.A03 = r2;
    }

    public static final C07270Xi A01(AnonymousClass0Q7 r2) {
        AnonymousClass0Q7 r0 = r2.A03;
        if (r0 != null) {
            AnonymousClass0QV r1 = r0.A06;
            switch (r0.A05.ordinal()) {
                case 1:
                    return r1.A0c.A05;
                case 2:
                    return r1.A0d.A05;
                case 3:
                    return r1.A0c.A04;
                case 4:
                    return r1.A0d.A04;
                case 5:
                    return r1.A0d.A00;
            }
        }
        return null;
    }

    public static final C07270Xi A02(AnonymousClass0Q7 r3, int i) {
        AbstractC07280Xj r1;
        AnonymousClass0Q7 r32 = r3.A03;
        if (r32 != null) {
            AnonymousClass0QV r0 = r32.A06;
            if (i == 0) {
                r1 = r0.A0c;
            } else {
                r1 = r0.A0d;
            }
            switch (r32.A05.ordinal()) {
                case 1:
                case 2:
                    return r1.A05;
                case 3:
                case 4:
                    return r1.A04;
            }
        }
        return null;
    }

    public static final void A03(C07270Xi r1, C07270Xi r2, int i) {
        r1.A08.add(r2);
        r1.A00 = i;
        r2.A07.add(r1);
    }

    public final int A04(int i, int i2) {
        int i3;
        int i4;
        AnonymousClass0QV r0 = this.A03;
        if (i2 == 0) {
            i3 = r0.A0F;
            i4 = r0.A0H;
        } else {
            i3 = r0.A0E;
            i4 = r0.A0G;
        }
        int max = Math.max(i4, i);
        if (i3 > 0) {
            max = Math.min(i3, i);
        }
        if (max != i) {
            return max;
        }
        return i;
    }

    public long A05() {
        C02580Cy r1 = this.A06;
        if (r1.A0B) {
            return (long) r1.A02;
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (((X.AbstractC07280Xj) r1).A00 == 3) goto L_0x0054;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A09(X.AnonymousClass0Q7 r16, X.AnonymousClass0Q7 r17, int r18) {
        /*
        // Method dump skipped, instructions count: 220
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC07280Xj.A09(X.0Q7, X.0Q7, int):void");
    }

    public final void A0A(C07270Xi r3, C07270Xi r4, C02580Cy r5, int i) {
        List list = r3.A08;
        list.add(r4);
        list.add(this.A06);
        r3.A01 = i;
        r3.A05 = r5;
        r4.A07.add(r3);
        r5.A07.add(r3);
    }
}
