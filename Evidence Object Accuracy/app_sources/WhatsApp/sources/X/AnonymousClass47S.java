package X;

import android.graphics.drawable.Drawable;
import com.whatsapp.community.CommunityFragment;

/* renamed from: X.47S  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass47S extends AbstractC54672h6 {
    public final /* synthetic */ CommunityFragment A00;

    @Override // X.AbstractC54672h6
    public boolean A03(int i, int i2) {
        return i2 == -1 ? i != 10 : i2 == 8;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass47S(Drawable drawable, CommunityFragment communityFragment) {
        super(drawable);
        this.A00 = communityFragment;
    }
}
