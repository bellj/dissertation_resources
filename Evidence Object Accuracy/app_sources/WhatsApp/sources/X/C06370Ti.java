package X;

import android.graphics.Color;

/* renamed from: X.0Ti  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06370Ti {
    public final float A00;
    public final float A01;
    public final float A02;
    public final float A03;
    public final float A04;
    public final float A05;

    public C06370Ti(float f, float f2, float f3, float f4, float f5, float f6) {
        this.A03 = f;
        this.A02 = f2;
        this.A04 = f3;
        this.A05 = f4;
        this.A00 = f5;
        this.A01 = f6;
    }

    public static C06370Ti A00(float f, float f2, float f3) {
        float log = ((float) Math.log((((double) (f2 * AnonymousClass0S3.A0A.A03)) * 0.0228d) + 1.0d)) * 43.85965f;
        double d = (double) ((3.1415927f * f3) / 180.0f);
        return new C06370Ti(f3, f2, f, (1.7f * f) / ((0.007f * f) + 1.0f), log * ((float) Math.cos(d)), log * ((float) Math.sin(d)));
    }

    public static C06370Ti A01(int i) {
        float f;
        AnonymousClass0S3 r7 = AnonymousClass0S3.A0A;
        float A00 = AnonymousClass0RS.A00(Color.red(i));
        float A002 = AnonymousClass0RS.A00(Color.green(i));
        float A003 = AnonymousClass0RS.A00(Color.blue(i));
        float[][] fArr = AnonymousClass0RS.A02;
        float[] fArr2 = fArr[0];
        float f2 = (fArr2[0] * A00) + (fArr2[1] * A002) + (fArr2[2] * A003);
        float[] fArr3 = fArr[1];
        float f3 = (fArr3[0] * A00) + (fArr3[1] * A002) + (fArr3[2] * A003);
        float[] fArr4 = fArr[2];
        float f4 = (A00 * fArr4[0]) + (A002 * fArr4[1]) + (A003 * fArr4[2]);
        float[] fArr5 = {f2, f3, f4};
        float[][] fArr6 = AnonymousClass0RS.A03;
        float f5 = fArr5[0];
        float[] fArr7 = fArr6[0];
        float f6 = fArr5[1];
        float f7 = (f5 * fArr7[0]) + (fArr7[1] * f6) + (fArr7[2] * f4);
        float[] fArr8 = fArr6[1];
        float f8 = (f5 * fArr8[0]) + (fArr8[1] * f6) + (fArr8[2] * f4);
        float[] fArr9 = fArr6[2];
        float f9 = (f5 * fArr9[0]) + (f6 * fArr9[1]) + (f4 * fArr9[2]);
        float[] fArr10 = r7.A09;
        float f10 = fArr10[0] * f7;
        float f11 = fArr10[1] * f8;
        float f12 = fArr10[2] * f9;
        float f13 = r7.A02;
        float pow = (float) Math.pow(((double) (Math.abs(f10) * f13)) / 100.0d, 0.42d);
        float pow2 = (float) Math.pow(((double) (Math.abs(f11) * f13)) / 100.0d, 0.42d);
        float pow3 = (float) Math.pow(((double) (f13 * Math.abs(f12))) / 100.0d, 0.42d);
        float signum = ((Math.signum(f10) * 400.0f) * pow) / (pow + 27.13f);
        float signum2 = ((Math.signum(f11) * 400.0f) * pow2) / (pow2 + 27.13f);
        float signum3 = ((Math.signum(f12) * 400.0f) * pow3) / (pow3 + 27.13f);
        double d = (double) signum3;
        float f14 = ((float) (((((double) signum) * 11.0d) + (((double) signum2) * -12.0d)) + d)) / 11.0f;
        float f15 = ((float) (((double) (signum + signum2)) - (d * 2.0d))) / 9.0f;
        float f16 = signum2 * 20.0f;
        float f17 = (((signum * 20.0f) + f16) + (21.0f * signum3)) / 20.0f;
        float f18 = (((signum * 40.0f) + f16) + signum3) / 20.0f;
        float atan2 = (((float) Math.atan2((double) f15, (double) f14)) * 180.0f) / 3.1415927f;
        if (atan2 < 0.0f) {
            atan2 += 360.0f;
        } else if (atan2 >= 360.0f) {
            atan2 -= 360.0f;
        }
        float f19 = (3.1415927f * atan2) / 180.0f;
        float pow4 = ((float) Math.pow((double) ((f18 * r7.A05) / r7.A00), (double) (r7.A01 * r7.A08))) * 100.0f;
        float f20 = r7.A03;
        if (((double) atan2) < 20.14d) {
            f = 360.0f + atan2;
        } else {
            f = atan2;
        }
        float pow5 = ((float) Math.pow(1.64d - Math.pow(0.29d, (double) r7.A04), 0.73d)) * ((float) Math.pow((double) ((((((((float) (Math.cos(((((double) f) * 3.141592653589793d) / 180.0d) + 2.0d) + 3.8d)) * 0.25f) * 3846.1538f) * r7.A06) * r7.A07) * ((float) Math.sqrt((double) ((f14 * f14) + (f15 * f15))))) / (f17 + 0.305f)), 0.9d)) * ((float) Math.sqrt(((double) pow4) / 100.0d));
        float f21 = (1.7f * pow4) / ((0.007f * pow4) + 1.0f);
        float log = ((float) Math.log((double) ((0.0228f * pow5 * f20) + 1.0f))) * 43.85965f;
        double d2 = (double) f19;
        return new C06370Ti(atan2, pow5, pow4, f21, log * ((float) Math.cos(d2)), log * ((float) Math.sin(d2)));
    }

    public int A02(AnonymousClass0S3 r18) {
        float f;
        float f2 = this.A02;
        if (((double) f2) != 0.0d) {
            double d = (double) this.A04;
            if (d != 0.0d) {
                f = f2 / ((float) Math.sqrt(d / 100.0d));
                float pow = (float) Math.pow(((double) f) / Math.pow(1.64d - Math.pow(0.29d, (double) r18.A04), 0.73d), 1.1111111111111112d);
                double d2 = (double) ((this.A03 * 3.1415927f) / 180.0f);
                float cos = ((float) (Math.cos(2.0d + d2) + 3.8d)) * 0.25f * 3846.1538f * r18.A06 * r18.A07;
                float pow2 = (r18.A00 * ((float) Math.pow(((double) this.A04) / 100.0d, (1.0d / ((double) r18.A01)) / ((double) r18.A08)))) / r18.A05;
                float sin = (float) Math.sin(d2);
                float cos2 = (float) Math.cos(d2);
                float f3 = (((0.305f + pow2) * 23.0f) * pow) / (((cos * 23.0f) + ((11.0f * pow) * cos2)) + ((pow * 108.0f) * sin));
                float f4 = cos2 * f3;
                float f5 = f3 * sin;
                float f6 = pow2 * 460.0f;
                float f7 = (((451.0f * f4) + f6) + (288.0f * f5)) / 1403.0f;
                float f8 = ((f6 - (891.0f * f4)) - (261.0f * f5)) / 1403.0f;
                float f9 = ((f6 - (f4 * 220.0f)) - (f5 * 6300.0f)) / 1403.0f;
                double abs = (double) Math.abs(f7);
                float signum = Math.signum(f7);
                float f10 = 100.0f / r18.A02;
                float pow3 = signum * f10 * ((float) Math.pow((double) ((float) Math.max(0.0d, (abs * 27.13d) / (400.0d - abs))), 2.380952380952381d));
                double abs2 = (double) Math.abs(f8);
                float signum2 = Math.signum(f8) * f10 * ((float) Math.pow((double) ((float) Math.max(0.0d, (abs2 * 27.13d) / (400.0d - abs2))), 2.380952380952381d));
                double abs3 = (double) Math.abs(f9);
                float signum3 = Math.signum(f9) * f10 * ((float) Math.pow((double) ((float) Math.max(0.0d, (abs3 * 27.13d) / (400.0d - abs3))), 2.380952380952381d));
                float[] fArr = r18.A09;
                float f11 = pow3 / fArr[0];
                float f12 = signum2 / fArr[1];
                float f13 = signum3 / fArr[2];
                float[][] fArr2 = AnonymousClass0RS.A01;
                float[] fArr3 = fArr2[0];
                float f14 = (fArr3[0] * f11) + (fArr3[1] * f12) + (fArr3[2] * f13);
                float[] fArr4 = fArr2[1];
                float f15 = (fArr4[0] * f11) + (fArr4[1] * f12) + (fArr4[2] * f13);
                float[] fArr5 = fArr2[2];
                return C016907y.A02((double) f14, (double) f15, (double) ((f11 * fArr5[0]) + (f12 * fArr5[1]) + (f13 * fArr5[2])));
            }
        }
        f = 0.0f;
        float pow = (float) Math.pow(((double) f) / Math.pow(1.64d - Math.pow(0.29d, (double) r18.A04), 0.73d), 1.1111111111111112d);
        double d2 = (double) ((this.A03 * 3.1415927f) / 180.0f);
        float cos = ((float) (Math.cos(2.0d + d2) + 3.8d)) * 0.25f * 3846.1538f * r18.A06 * r18.A07;
        float pow2 = (r18.A00 * ((float) Math.pow(((double) this.A04) / 100.0d, (1.0d / ((double) r18.A01)) / ((double) r18.A08)))) / r18.A05;
        float sin = (float) Math.sin(d2);
        float cos2 = (float) Math.cos(d2);
        float f3 = (((0.305f + pow2) * 23.0f) * pow) / (((cos * 23.0f) + ((11.0f * pow) * cos2)) + ((pow * 108.0f) * sin));
        float f4 = cos2 * f3;
        float f5 = f3 * sin;
        float f6 = pow2 * 460.0f;
        float f7 = (((451.0f * f4) + f6) + (288.0f * f5)) / 1403.0f;
        float f8 = ((f6 - (891.0f * f4)) - (261.0f * f5)) / 1403.0f;
        float f9 = ((f6 - (f4 * 220.0f)) - (f5 * 6300.0f)) / 1403.0f;
        double abs = (double) Math.abs(f7);
        float signum = Math.signum(f7);
        float f10 = 100.0f / r18.A02;
        float pow3 = signum * f10 * ((float) Math.pow((double) ((float) Math.max(0.0d, (abs * 27.13d) / (400.0d - abs))), 2.380952380952381d));
        double abs2 = (double) Math.abs(f8);
        float signum2 = Math.signum(f8) * f10 * ((float) Math.pow((double) ((float) Math.max(0.0d, (abs2 * 27.13d) / (400.0d - abs2))), 2.380952380952381d));
        double abs3 = (double) Math.abs(f9);
        float signum3 = Math.signum(f9) * f10 * ((float) Math.pow((double) ((float) Math.max(0.0d, (abs3 * 27.13d) / (400.0d - abs3))), 2.380952380952381d));
        float[] fArr = r18.A09;
        float f11 = pow3 / fArr[0];
        float f12 = signum2 / fArr[1];
        float f13 = signum3 / fArr[2];
        float[][] fArr2 = AnonymousClass0RS.A01;
        float[] fArr3 = fArr2[0];
        float f14 = (fArr3[0] * f11) + (fArr3[1] * f12) + (fArr3[2] * f13);
        float[] fArr4 = fArr2[1];
        float f15 = (fArr4[0] * f11) + (fArr4[1] * f12) + (fArr4[2] * f13);
        float[] fArr5 = fArr2[2];
        return C016907y.A02((double) f14, (double) f15, (double) ((f11 * fArr5[0]) + (f12 * fArr5[1]) + (f13 * fArr5[2])));
    }
}
