package X;

import com.whatsapp.R;
import java.util.HashMap;

/* renamed from: X.3dR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71743dR extends HashMap<String, Integer> {
    public C71743dR() {
        Integer valueOf = Integer.valueOf((int) R.drawable.large_e022);
        put("️", valueOf);
        put("❤", valueOf);
        put("💓", Integer.valueOf((int) R.drawable.large_1f493));
        put("💕", Integer.valueOf((int) R.drawable.large_1f495));
        put("💖", Integer.valueOf((int) R.drawable.large_1f496));
        put("💗", Integer.valueOf((int) R.drawable.large_1f497));
        put("💘", Integer.valueOf((int) R.drawable.large_1f498));
        put("💙", Integer.valueOf((int) R.drawable.large_1f499));
        put("💚", Integer.valueOf((int) R.drawable.large_1f49a));
        put("💛", Integer.valueOf((int) R.drawable.large_1f49b));
        put("💜", Integer.valueOf((int) R.drawable.large_1f49c));
        put("💝", Integer.valueOf((int) R.drawable.large_1f49d));
        put("💞", Integer.valueOf((int) R.drawable.large_1f49e));
        put("🖤", Integer.valueOf((int) R.drawable.large_1f5a4));
        put("🤍", Integer.valueOf((int) R.drawable.large_1f90d));
        put("🤎", Integer.valueOf((int) R.drawable.large_1f90e));
        put("❤‍🔥", Integer.valueOf((int) R.drawable.large_2764_fe0f_200d_1f525));
    }
}
