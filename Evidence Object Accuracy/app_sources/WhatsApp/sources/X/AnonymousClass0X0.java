package X;

import android.widget.CompoundButton;
import androidx.preference.CheckBoxPreference;

/* renamed from: X.0X0  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X0 implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ CheckBoxPreference A00;

    public AnonymousClass0X0(CheckBoxPreference checkBoxPreference) {
        this.A00 = checkBoxPreference;
    }

    @Override // android.widget.CompoundButton.OnCheckedChangeListener
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        CheckBoxPreference checkBoxPreference = this.A00;
        if (!checkBoxPreference.A0Q(Boolean.valueOf(z))) {
            compoundButton.setChecked(!z);
        } else {
            checkBoxPreference.A0T(z);
        }
    }
}
