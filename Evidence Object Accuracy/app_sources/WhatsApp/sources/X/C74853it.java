package X;

import com.whatsapp.scroller.RecyclerFastScroller;

/* renamed from: X.3it  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74853it extends AnonymousClass0QE {
    public final /* synthetic */ RecyclerFastScroller A00;

    public C74853it(RecyclerFastScroller recyclerFastScroller) {
        this.A00 = recyclerFastScroller;
    }

    @Override // X.AnonymousClass0QE
    public void A00() {
        this.A00.requestLayout();
    }
}
