package X;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: X.5D5  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5D5 implements Iterator {
    public int A00 = 0;
    public final int A01;
    public final Object[] A02;

    public AnonymousClass5D5(Object[] objArr, int i) {
        this.A02 = objArr;
        this.A01 = i;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return C12990iw.A1Y(this.A00, this.A02.length);
    }

    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Object next() {
        if (hasNext()) {
            int i = this.A00;
            int i2 = this.A01;
            Object[] objArr = this.A02;
            int length = objArr.length;
            int min = Math.min(i + i2, length);
            if (length > i2 || i != 0) {
                objArr = Arrays.copyOfRange(objArr, i, min);
            }
            this.A00 += i2;
            return objArr;
        }
        throw new NoSuchElementException("There are no more chunks to provide.");
    }
}
