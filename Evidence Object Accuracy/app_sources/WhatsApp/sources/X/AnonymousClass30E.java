package X;

/* renamed from: X.30E  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass30E extends AbstractC16110oT {
    public Integer A00;
    public Long A01;

    public AnonymousClass30E() {
        super(594, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A01);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamGroupCreate {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "ephemeralityDuration", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "groupCreateEntryPoint", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
