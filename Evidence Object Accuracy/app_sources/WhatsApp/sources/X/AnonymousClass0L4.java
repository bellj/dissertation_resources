package X;

import android.os.Build;
import android.widget.EdgeEffect;

/* renamed from: X.0L4  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0L4 {
    public static void A00(EdgeEffect edgeEffect, float f, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0L3.A00(edgeEffect, f, f2);
        } else {
            edgeEffect.onPull(f);
        }
    }
}
