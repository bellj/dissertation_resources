package X;

import com.whatsapp.group.GroupChatInfo;

/* renamed from: X.3Xo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69003Xo implements AnonymousClass13Q {
    public final /* synthetic */ GroupChatInfo A00;

    public C69003Xo(GroupChatInfo groupChatInfo) {
        this.A00 = groupChatInfo;
    }

    @Override // X.AnonymousClass13Q
    public void AWN(AbstractC14640lm r4) {
        GroupChatInfo groupChatInfo = this.A00;
        if (r4.equals(groupChatInfo.A1C)) {
            C14900mE.A01(((ActivityC13810kN) groupChatInfo).A05, groupChatInfo, 35);
        }
    }

    @Override // X.AnonymousClass13Q
    public void AWr(AbstractC14640lm r4) {
        GroupChatInfo groupChatInfo = this.A00;
        if (r4.equals(groupChatInfo.A1C)) {
            C14900mE.A01(((ActivityC13810kN) groupChatInfo).A05, groupChatInfo, 36);
        }
    }
}
