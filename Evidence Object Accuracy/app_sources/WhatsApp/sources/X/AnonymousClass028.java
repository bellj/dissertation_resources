package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.WindowManager;
import com.whatsapp.R;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.028  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass028 {
    public static ThreadLocal A00;
    public static Field A01;
    public static Method A02;
    public static Method A03;
    public static WeakHashMap A04;
    public static WeakHashMap A05;
    public static boolean A06;
    public static boolean A07;
    public static final AnonymousClass013 A08 = new AnonymousClass013() { // from class: X.06L
        @Override // X.AnonymousClass013
        public final AnonymousClass0SR AUf(AnonymousClass0SR r1) {
            return r1;
        }
    };
    public static final AnonymousClass06M A09 = new AnonymousClass06M();
    public static final AtomicInteger A0A = new AtomicInteger(1);

    public static float A00(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0Uc.A00(view);
        }
        return 0.0f;
    }

    public static float A01(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0Uc.A01(view);
        }
        return 0.0f;
    }

    public static int A02() {
        AtomicInteger atomicInteger;
        int i;
        int i2;
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06N.A00();
        }
        do {
            atomicInteger = A0A;
            i = atomicInteger.get();
            i2 = i + 1;
            if (i2 > 16777215) {
                i2 = 1;
            }
        } while (!atomicInteger.compareAndSet(i, i2));
        return i;
    }

    public static int A03(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return AnonymousClass08X.A00(view);
        }
        return 0;
    }

    public static int A04(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return C05700Qp.A00(view);
        }
        return 0;
    }

    public static int A05(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06N.A01(view);
        }
        return 0;
    }

    public static int A06(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06N.A02(view);
        }
        return view.getPaddingRight();
    }

    public static int A07(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06N.A03(view);
        }
        return view.getPaddingLeft();
    }

    public static ColorStateList A08(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0Uc.A02(view);
        }
        if (view instanceof AnonymousClass012) {
            return ((AnonymousClass012) view).getSupportBackgroundTintList();
        }
        return null;
    }

    public static PorterDuff.Mode A09(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0Uc.A03(view);
        }
        if (view instanceof AnonymousClass012) {
            return ((AnonymousClass012) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    public static Rect A0A(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return C05680Qn.A00(view);
        }
        return null;
    }

    public static Display A0B(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06N.A04(view);
        }
        if (A0q(view)) {
            return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }

    public static View.AccessibilityDelegate A0C(View view) {
        if (Build.VERSION.SDK_INT >= 29) {
            return C05710Qq.A00(view);
        }
        if (A06) {
            return null;
        }
        if (A01 == null) {
            try {
                Field declaredField = View.class.getDeclaredField("mAccessibilityDelegate");
                A01 = declaredField;
                declaredField.setAccessible(true);
            } catch (Throwable unused) {
                A06 = true;
                return null;
            }
        }
        Object obj = A01.get(view);
        if (obj instanceof View.AccessibilityDelegate) {
            return (View.AccessibilityDelegate) obj;
        }
        return null;
    }

    public static View A0D(View view, int i) {
        if (Build.VERSION.SDK_INT >= 28) {
            return (View) AnonymousClass0US.A01(view, i);
        }
        View findViewById = view.findViewById(i);
        if (findViewById != null) {
            return findViewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this View");
    }

    public static AnonymousClass0SR A0E(View view, AnonymousClass0SR r4) {
        AnonymousClass013 r3;
        if (Log.isLoggable("ViewCompat", 3)) {
            StringBuilder sb = new StringBuilder("performReceiveContent: ");
            sb.append(r4);
            sb.append(", view=");
            sb.append(view.getClass().getSimpleName());
            sb.append("[");
            sb.append(view.getId());
            sb.append("]");
            Log.d("ViewCompat", sb.toString());
        }
        if (Build.VERSION.SDK_INT >= 31) {
            return C05730Qs.A00(view, r4);
        }
        AbstractC11750gn r0 = (AbstractC11750gn) view.getTag(R.id.tag_on_receive_content_listener);
        if (r0 != null && (r4 = r0.AUe(view, r4)) == null) {
            return null;
        }
        if (view instanceof AnonymousClass013) {
            r3 = (AnonymousClass013) view;
        } else {
            r3 = A08;
        }
        return r3.AUf(r4);
    }

    public static AnonymousClass0QQ A0F(View view) {
        WeakHashMap weakHashMap = A05;
        if (weakHashMap == null) {
            weakHashMap = new WeakHashMap();
            A05 = weakHashMap;
        }
        AnonymousClass0QQ r1 = (AnonymousClass0QQ) weakHashMap.get(view);
        if (r1 != null) {
            return r1;
        }
        AnonymousClass0QQ r12 = new AnonymousClass0QQ(view);
        A05.put(view, r12);
        return r12;
    }

    public static C018408o A0G(View view) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            return C05690Qo.A00(view);
        }
        if (i >= 21) {
            return AnonymousClass0Uc.A04(view);
        }
        return null;
    }

    public static C018408o A0H(View view, C018408o r3) {
        WindowInsets A072;
        if (Build.VERSION.SDK_INT >= 21 && (A072 = r3.A07()) != null) {
            WindowInsets A002 = AnonymousClass0T5.A00(view, A072);
            if (!A002.equals(A072)) {
                return C018408o.A01(view, A002);
            }
        }
        return r3;
    }

    public static C018408o A0I(View view, C018408o r3) {
        WindowInsets A072;
        if (Build.VERSION.SDK_INT >= 21 && (A072 = r3.A07()) != null) {
            WindowInsets A012 = AnonymousClass0T5.A01(view, A072);
            if (!A012.equals(A072)) {
                return C018408o.A01(view, A012);
            }
        }
        return r3;
    }

    public static String A0J(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0Uc.A05(view);
        }
        WeakHashMap weakHashMap = A04;
        if (weakHashMap == null) {
            return null;
        }
        return (String) weakHashMap.get(view);
    }

    public static void A0K() {
        try {
            A03 = View.class.getDeclaredMethod("dispatchStartTemporaryDetach", new Class[0]);
            A02 = View.class.getDeclaredMethod("dispatchFinishTemporaryDetach", new Class[0]);
        } catch (NoSuchMethodException e) {
            Log.e("ViewCompat", "Couldn't find method", e);
        }
        A07 = true;
    }

    public static void A0L(Context context, TypedArray typedArray, AttributeSet attributeSet, View view, int[] iArr, int i) {
        if (Build.VERSION.SDK_INT >= 29) {
            C05710Qq.A01(context, typedArray, attributeSet, view, iArr, i, 0);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if (X.AnonymousClass0Uc.A03(r3) != null) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0M(android.content.res.ColorStateList r2, android.view.View r3) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0032
            X.AnonymousClass0Uc.A06(r2, r3)
            if (r1 != r0) goto L_0x0031
            android.graphics.drawable.Drawable r2 = r3.getBackground()
            android.content.res.ColorStateList r0 = X.AnonymousClass0Uc.A02(r3)
            if (r0 != 0) goto L_0x001c
            android.graphics.PorterDuff$Mode r1 = X.AnonymousClass0Uc.A03(r3)
            r0 = 0
            if (r1 == 0) goto L_0x001d
        L_0x001c:
            r0 = 1
        L_0x001d:
            if (r2 == 0) goto L_0x0031
            if (r0 == 0) goto L_0x0031
            boolean r0 = r2.isStateful()
            if (r0 == 0) goto L_0x002e
            int[] r0 = r3.getDrawableState()
            r2.setState(r0)
        L_0x002e:
            r3.setBackground(r2)
        L_0x0031:
            return
        L_0x0032:
            boolean r0 = r3 instanceof X.AnonymousClass012
            if (r0 == 0) goto L_0x0031
            X.012 r3 = (X.AnonymousClass012) r3
            r3.setSupportBackgroundTintList(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass028.A0M(android.content.res.ColorStateList, android.view.View):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        if (X.AnonymousClass0Uc.A03(r3) != null) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0N(android.graphics.PorterDuff.Mode r2, android.view.View r3) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 21
            if (r1 < r0) goto L_0x0032
            X.AnonymousClass0Uc.A07(r2, r3)
            if (r1 != r0) goto L_0x0031
            android.graphics.drawable.Drawable r2 = r3.getBackground()
            android.content.res.ColorStateList r0 = X.AnonymousClass0Uc.A02(r3)
            if (r0 != 0) goto L_0x001c
            android.graphics.PorterDuff$Mode r1 = X.AnonymousClass0Uc.A03(r3)
            r0 = 0
            if (r1 == 0) goto L_0x001d
        L_0x001c:
            r0 = 1
        L_0x001d:
            if (r2 == 0) goto L_0x0031
            if (r0 == 0) goto L_0x0031
            boolean r0 = r2.isStateful()
            if (r0 == 0) goto L_0x002e
            int[] r0 = r3.getDrawableState()
            r2.setState(r0)
        L_0x002e:
            r3.setBackground(r2)
        L_0x0031:
            return
        L_0x0032:
            boolean r0 = r3 instanceof X.AnonymousClass012
            if (r0 == 0) goto L_0x0031
            X.012 r3 = (X.AnonymousClass012) r3
            r3.setSupportBackgroundTintMode(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass028.A0N(android.graphics.PorterDuff$Mode, android.view.View):void");
    }

    public static void A0O(Rect rect, View view, C018408o r4) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0Uc.A08(rect, view, r4);
        }
    }

    public static void A0P(View view) {
        if (Build.VERSION.SDK_INT >= 24) {
            AnonymousClass0T6.A01(view);
            return;
        }
        if (!A07) {
            A0K();
        }
        Method method = A02;
        if (method != null) {
            try {
                method.invoke(view, new Object[0]);
            } catch (Exception e) {
                Log.d("ViewCompat", "Error calling dispatchFinishTemporaryDetach", e);
            }
        } else {
            view.onFinishTemporaryDetach();
        }
    }

    public static void A0Q(View view) {
        if (Build.VERSION.SDK_INT >= 24) {
            AnonymousClass0T6.A02(view);
            return;
        }
        if (!A07) {
            A0K();
        }
        Method method = A03;
        if (method != null) {
            try {
                method.invoke(view, new Object[0]);
            } catch (Exception e) {
                Log.d("ViewCompat", "Error calling dispatchStartTemporaryDetach", e);
            }
        } else {
            view.onStartTemporaryDetach();
        }
    }

    public static void A0R(View view) {
        if (Build.VERSION.SDK_INT >= 20) {
            AnonymousClass0T5.A02(view);
        } else {
            view.requestFitSystemWindows();
        }
    }

    public static void A0S(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            C05700Qp.A01(view, 8);
        }
    }

    public static void A0T(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0Uc.A09(view);
        } else if (view instanceof AnonymousClass02E) {
            ((AnonymousClass02E) view).stopNestedScroll();
        }
    }

    public static void A0U(View view) {
        float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }

    public static void A0V(View view, float f) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0Uc.A0A(view, f);
        }
    }

    /* JADX WARN: Type inference failed for: r0v7, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r0v24, types: [java.lang.Object] */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0028, code lost:
        if (r4.getWindowVisibility() != 0) goto L_0x002a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A0W(android.view.View r4, int r5) {
        /*
            android.content.Context r1 = r4.getContext()
            java.lang.String r0 = "accessibility"
            java.lang.Object r3 = r1.getSystemService(r0)
            android.view.accessibility.AccessibilityManager r3 = (android.view.accessibility.AccessibilityManager) r3
            boolean r0 = r3.isEnabled()
            if (r0 == 0) goto L_0x005d
            X.0DZ r0 = new X.0DZ
            r0.<init>()
            java.lang.Object r0 = r0.A00(r4)
            if (r0 == 0) goto L_0x002a
            boolean r0 = r4.isShown()
            if (r0 == 0) goto L_0x002a
            int r0 = r4.getWindowVisibility()
            r2 = 1
            if (r0 == 0) goto L_0x002b
        L_0x002a:
            r2 = 0
        L_0x002b:
            int r1 = A03(r4)
            r0 = 32
            if (r1 != 0) goto L_0x0091
            if (r2 != 0) goto L_0x0091
            if (r5 != r0) goto L_0x005e
            android.view.accessibility.AccessibilityEvent r2 = android.view.accessibility.AccessibilityEvent.obtain()
            r4.onInitializeAccessibilityEvent(r2)
            r2.setEventType(r0)
            X.AnonymousClass08X.A03(r2, r5)
            r2.setSource(r4)
            r4.onPopulateAccessibilityEvent(r2)
            java.util.List r1 = r2.getText()
            X.0DZ r0 = new X.0DZ
            r0.<init>()
            java.lang.Object r0 = r0.A00(r4)
            r1.add(r0)
            r3.sendAccessibilityEvent(r2)
        L_0x005d:
            return
        L_0x005e:
            android.view.ViewParent r0 = r4.getParent()
            if (r0 == 0) goto L_0x005d
            android.view.ViewParent r0 = r4.getParent()
            X.AnonymousClass08X.A02(r4, r4, r0, r5)     // Catch: AbstractMethodError -> 0x006c
            goto L_0x0090
        L_0x006c:
            r2 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            android.view.ViewParent r0 = r4.getParent()
            java.lang.Class r0 = r0.getClass()
            java.lang.String r0 = r0.getSimpleName()
            r1.append(r0)
            java.lang.String r0 = " does not fully implement ViewParent"
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = "ViewCompat"
            android.util.Log.e(r0, r1, r2)
            return
        L_0x0090:
            return
        L_0x0091:
            android.view.accessibility.AccessibilityEvent r3 = android.view.accessibility.AccessibilityEvent.obtain()
            if (r2 != 0) goto L_0x0099
            r0 = 2048(0x800, float:2.87E-42)
        L_0x0099:
            r3.setEventType(r0)
            X.AnonymousClass08X.A03(r3, r5)
            if (r2 == 0) goto L_0x00d1
            java.util.List r1 = r3.getText()
            X.0DZ r0 = new X.0DZ
            r0.<init>()
            java.lang.Object r0 = r0.A00(r4)
            r1.add(r0)
            int r0 = r4.getImportantForAccessibility()
            if (r0 != 0) goto L_0x00bb
            r0 = 1
            r4.setImportantForAccessibility(r0)
        L_0x00bb:
            android.view.ViewParent r2 = r4.getParent()
        L_0x00bf:
            boolean r0 = r2 instanceof android.view.View
            if (r0 == 0) goto L_0x00d1
            r0 = r2
            android.view.View r0 = (android.view.View) r0
            int r1 = r0.getImportantForAccessibility()
            r0 = 4
            if (r1 != r0) goto L_0x00d5
            r0 = 2
            r4.setImportantForAccessibility(r0)
        L_0x00d1:
            r4.sendAccessibilityEventUnchecked(r3)
            return
        L_0x00d5:
            android.view.ViewParent r2 = r2.getParent()
            goto L_0x00bf
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass028.A0W(android.view.View, int):void");
    }

    public static void A0X(View view, int i) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            view.offsetLeftAndRight(i);
        } else if (i2 >= 21) {
            ThreadLocal threadLocal = A00;
            if (threadLocal == null) {
                threadLocal = new ThreadLocal();
                A00 = threadLocal;
            }
            Rect rect = (Rect) threadLocal.get();
            if (rect == null) {
                rect = new Rect();
                A00.set(rect);
            }
            rect.setEmpty();
            boolean z = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                rect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !rect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            view.offsetLeftAndRight(i);
            if (view.getVisibility() == 0) {
                A0U(view);
                ViewParent parent2 = view.getParent();
                if (parent2 instanceof View) {
                    A0U((View) parent2);
                }
            }
            if (z && rect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(rect);
            }
        } else {
            view.offsetLeftAndRight(i);
            if (view.getVisibility() == 0) {
                A0U(view);
                ViewParent parent3 = view.getParent();
                if (parent3 instanceof View) {
                    A0U((View) parent3);
                }
            }
        }
    }

    public static void A0Y(View view, int i) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            view.offsetTopAndBottom(i);
        } else if (i2 >= 21) {
            ThreadLocal threadLocal = A00;
            if (threadLocal == null) {
                threadLocal = new ThreadLocal();
                A00 = threadLocal;
            }
            Rect rect = (Rect) threadLocal.get();
            if (rect == null) {
                rect = new Rect();
                A00.set(rect);
            }
            rect.setEmpty();
            boolean z = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                rect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !rect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            view.offsetTopAndBottom(i);
            if (view.getVisibility() == 0) {
                A0U(view);
                ViewParent parent2 = view.getParent();
                if (parent2 instanceof View) {
                    A0U((View) parent2);
                }
            }
            if (z && rect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(rect);
            }
        } else {
            view.offsetTopAndBottom(i);
            if (view.getVisibility() == 0) {
                A0U(view);
                ViewParent parent3 = view.getParent();
                if (parent3 instanceof View) {
                    A0U((View) parent3);
                }
            }
        }
    }

    public static void A0Z(View view, int i) {
        if (Build.VERSION.SDK_INT >= 19) {
            AnonymousClass08X.A01(view, i);
        }
    }

    public static void A0a(View view, int i) {
        if (Build.VERSION.SDK_INT < 19 && i == 4) {
            i = 2;
        }
        view.setImportantForAccessibility(i);
    }

    public static void A0b(View view, int i) {
        if (Build.VERSION.SDK_INT >= 17) {
            AnonymousClass06N.A05(view, i);
        }
    }

    public static void A0c(View view, int i) {
        if (Build.VERSION.SDK_INT >= 17) {
            AnonymousClass06N.A06(view, i);
        }
    }

    public static void A0d(View view, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            C05690Qo.A01(view, i, 3);
        }
    }

    public static void A0e(View view, int i, int i2, int i3, int i4) {
        if (Build.VERSION.SDK_INT >= 17) {
            AnonymousClass06N.A07(view, i, i2, i3, i4);
        } else {
            view.setPadding(i, i2, i3, i4);
        }
    }

    public static void A0f(View view, Rect rect) {
        if (Build.VERSION.SDK_INT >= 18) {
            C05680Qn.A01(view, rect);
        }
    }

    public static void A0g(View view, AnonymousClass04v r2) {
        View.AccessibilityDelegate accessibilityDelegate;
        if (r2 == null) {
            if (A0C(view) instanceof AnonymousClass07X) {
                r2 = new AnonymousClass04v();
            } else {
                accessibilityDelegate = null;
                view.setAccessibilityDelegate(accessibilityDelegate);
            }
        }
        accessibilityDelegate = r2.A00;
        view.setAccessibilityDelegate(accessibilityDelegate);
    }

    public static void A0h(View view, AnonymousClass07F r3) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0Uc.A0C(view, r3);
        }
    }

    public static void A0i(View view, AnonymousClass0S7 r3) {
        if (Build.VERSION.SDK_INT >= 24) {
            AnonymousClass0T6.A00((PointerIcon) r3.A01(), view);
        }
    }

    public static void A0j(View view, CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 19) {
            new AnonymousClass0Da().A02(view, charSequence);
        }
    }

    public static void A0k(View view, String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0Uc.A0D(view, str);
            return;
        }
        WeakHashMap weakHashMap = A04;
        if (weakHashMap == null) {
            weakHashMap = new WeakHashMap();
            A04 = weakHashMap;
        }
        weakHashMap.put(view, str);
    }

    public static void A0l(View view, boolean z) {
        new AnonymousClass0Db().A02(view, Boolean.valueOf(z));
    }

    public static void A0m(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 21) {
            AnonymousClass0Uc.A0E(view, z);
        } else if (view instanceof AnonymousClass02E) {
            ((AnonymousClass02E) view).setNestedScrollingEnabled(z);
        }
    }

    public static boolean A0n(KeyEvent keyEvent, View view) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        C06350Tg r6 = (C06350Tg) view.getTag(R.id.tag_unhandled_key_event_manager);
        if (r6 == null) {
            r6 = new C06350Tg();
            view.setTag(R.id.tag_unhandled_key_event_manager, r6);
        }
        if (keyEvent.getAction() == 0) {
            WeakHashMap weakHashMap = r6.A02;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            ArrayList arrayList = C06350Tg.A03;
            if (!arrayList.isEmpty()) {
                synchronized (arrayList) {
                    if (r6.A02 == null) {
                        r6.A02 = new WeakHashMap();
                    }
                    int size = arrayList.size();
                    while (true) {
                        size--;
                        if (size < 0) {
                            break;
                        }
                        View view2 = (View) ((WeakReference) arrayList.get(size)).get();
                        if (view2 == null) {
                            arrayList.remove(size);
                        } else {
                            r6.A02.put(view2, Boolean.TRUE);
                            for (ViewParent parent = view2.getParent(); parent instanceof View; parent = parent.getParent()) {
                                r6.A02.put((View) parent, Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }
        View A012 = r6.A01(keyEvent, view);
        if (keyEvent.getAction() == 0) {
            int keyCode = keyEvent.getKeyCode();
            if (A012 == null) {
                return false;
            }
            if (KeyEvent.isModifierKey(keyCode)) {
                return true;
            }
            SparseArray sparseArray = r6.A00;
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                r6.A00 = sparseArray;
            }
            sparseArray.put(keyCode, new WeakReference(A012));
            return true;
        } else if (A012 != null) {
            return true;
        } else {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        if (r0 == null) goto L_0x0053;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean A0o(android.view.KeyEvent r3, android.view.View r4) {
        /*
            int r1 = android.os.Build.VERSION.SDK_INT
            r0 = 28
            if (r1 >= r0) goto L_0x0023
            r0 = 2131366303(0x7f0a119f, float:1.8352496E38)
            java.lang.Object r1 = r4.getTag(r0)
            X.0Tg r1 = (X.C06350Tg) r1
            if (r1 != 0) goto L_0x0019
            X.0Tg r1 = new X.0Tg
            r1.<init>()
            r4.setTag(r0, r1)
        L_0x0019:
            java.lang.ref.WeakReference r0 = r1.A01
            if (r0 == 0) goto L_0x0025
            java.lang.Object r0 = r0.get()
            if (r0 != r3) goto L_0x0025
        L_0x0023:
            r0 = 0
            return r0
        L_0x0025:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r3)
            r1.A01 = r0
            android.util.SparseArray r2 = r1.A00
            if (r2 != 0) goto L_0x0037
            android.util.SparseArray r2 = new android.util.SparseArray
            r2.<init>()
            r1.A00 = r2
        L_0x0037:
            int r1 = r3.getAction()
            r0 = 1
            if (r1 != r0) goto L_0x0053
            int r0 = r3.getKeyCode()
            int r1 = r2.indexOfKey(r0)
            if (r1 < 0) goto L_0x0053
            java.lang.Object r0 = r2.valueAt(r1)
            java.lang.ref.Reference r0 = (java.lang.ref.Reference) r0
            r2.removeAt(r1)
            if (r0 != 0) goto L_0x005f
        L_0x0053:
            int r0 = r3.getKeyCode()
            java.lang.Object r0 = r2.get(r0)
            java.lang.ref.Reference r0 = (java.lang.ref.Reference) r0
            if (r0 == 0) goto L_0x0023
        L_0x005f:
            java.lang.Object r1 = r0.get()
            android.view.View r1 = (android.view.View) r1
            if (r1 == 0) goto L_0x0070
            boolean r0 = A0q(r1)
            if (r0 == 0) goto L_0x0070
            X.C06350Tg.A00(r1)
        L_0x0070:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass028.A0o(android.view.KeyEvent, android.view.View):boolean");
    }

    public static boolean A0p(View view) {
        Boolean bool = (Boolean) new AnonymousClass0Db().A00(view);
        return bool != null && bool.booleanValue();
    }

    public static boolean A0q(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return AnonymousClass08X.A04(view);
        }
        return view.getWindowToken() != null;
    }

    public static boolean A0r(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return AnonymousClass08X.A05(view);
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    public static boolean A0s(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return AnonymousClass0Uc.A0F(view);
        }
        if (view instanceof AnonymousClass02E) {
            return ((AnonymousClass02E) view).isNestedScrollingEnabled();
        }
        return false;
    }

    public static boolean A0t(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return AnonymousClass06N.A08(view);
        }
        return false;
    }

    public static boolean A0u(View view) {
        Boolean bool = (Boolean) new AnonymousClass0DY().A00(view);
        return bool != null && bool.booleanValue();
    }

    public static String[] A0v(View view) {
        if (Build.VERSION.SDK_INT >= 31) {
            return C05730Qs.A01(view);
        }
        return (String[]) view.getTag(R.id.tag_on_receive_content_mime_types);
    }
}
