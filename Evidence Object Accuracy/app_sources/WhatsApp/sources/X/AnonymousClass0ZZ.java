package X;

/* renamed from: X.0ZZ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0ZZ implements AbstractC12840ie {
    public final C05400Pk A00;
    public final AnonymousClass0S9 A01;
    public final AnonymousClass0SA A02;

    public AnonymousClass0ZZ(C05400Pk r4, AnonymousClass0S9 r5, AnonymousClass0SA r6) {
        this.A00 = r4;
        this.A02 = r6;
        this.A01 = r5;
        int i = r4.A02;
        int i2 = r4.A01;
        if (i - i2 == 0 && r4.A00 - r4.A03 == 0) {
            throw new IllegalArgumentException("Bounds must be non zero");
        } else if (i2 != 0 && r4.A03 != 0) {
            throw new IllegalArgumentException("Bounding rectangle must start at the top or left window edge for folding features");
        }
    }

    public boolean equals(Object obj) {
        Class<?> cls;
        if (this != obj) {
            if (obj == null) {
                cls = null;
            } else {
                cls = obj.getClass();
            }
            if (AnonymousClass0ZZ.class.equals(cls)) {
                if (obj != null) {
                    AnonymousClass0ZZ r5 = (AnonymousClass0ZZ) obj;
                    if (!C16700pc.A0O(this.A00, r5.A00) || !C16700pc.A0O(this.A02, r5.A02) || !C16700pc.A0O(this.A01, r5.A01)) {
                    }
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type androidx.window.layout.HardwareFoldingFeature");
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.A00.hashCode() * 31) + this.A02.hashCode()) * 31) + this.A01.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append((Object) "HardwareFoldingFeature");
        sb.append(" { ");
        sb.append(this.A00);
        sb.append(", type=");
        sb.append(this.A02);
        sb.append(", state=");
        sb.append(this.A01);
        sb.append(" }");
        return sb.toString();
    }
}
