package X;

import android.content.res.ColorStateList;
import android.content.res.Resources;

/* renamed from: X.0Qi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C05630Qi {
    public static int A00(Resources.Theme theme, Resources resources, int i) {
        return resources.getColor(i, theme);
    }

    public static ColorStateList A01(Resources.Theme theme, Resources resources, int i) {
        return resources.getColorStateList(i, theme);
    }
}
