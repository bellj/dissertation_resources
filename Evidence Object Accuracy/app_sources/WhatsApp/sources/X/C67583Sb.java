package X;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.ViewOutlineProvider;
import java.io.IOException;

/* renamed from: X.3Sb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67583Sb implements AnonymousClass5WW {
    public static int A00(C14260l7 r3, AnonymousClass28D r4) {
        int i;
        AnonymousClass28D A0F = r4.A0F(45);
        String A0I = r4.A0I(44);
        if (A0I != null) {
            i = AnonymousClass3JW.A05(A0I);
        } else {
            i = 0;
        }
        if (A0F == null) {
            return i;
        }
        try {
            String A0I2 = A0F.A0I(35);
            if (A0I2 == null || AnonymousClass3JW.A05(A0I2) == 0) {
                String A0I3 = A0F.A0I(36);
                if (A0I3 == null) {
                    return i;
                }
                if (AnonymousClass3JW.A05(A0I3) == 0) {
                    return i;
                }
            }
        } catch (AnonymousClass491 unused) {
            C28691Op.A00("ThemedColorUtils", "Error parsing themed color");
        }
        return AnonymousClass4Di.A00(r3, A0F);
    }

    public static void A01(C14260l7 r11, AnonymousClass28D r12, C55972k3 r13) {
        String A0I = r12.A0I(48);
        if (A0I != null) {
            float A01 = AnonymousClass3JW.A01(A0I);
            if (A01 != 0.0f) {
                r13.setElevation(A01);
                r13.setOutlineProvider(new C73773gm(r12));
            }
        }
        float A00 = AnonymousClass28D.A00(r12, 46);
        int i = (A00 > 0.0f ? 1 : (A00 == 0.0f ? 0 : -1));
        if (r12.A0O(43, false)) {
            if (i != 0) {
                r13.setOutlineProvider(new C52572bK(r12, A00));
                float A002 = AnonymousClass28D.A00(r12, 40);
                int A003 = C64973Hq.A00(r12.A0M(56), 0);
                if (A002 != 0.0f || !C64973Hq.A02(A003)) {
                    AnonymousClass3DV r3 = r13.A00;
                    r3.A04 = true;
                    r3.A0B.setLayerType(2, null);
                    r3.A00(C64963Hp.A02(r12.A0M(62)), A002, A00, AnonymousClass28D.A00(r12, 63), A00(r11, r12), C64963Hp.A00(r11, r12), A003);
                    return;
                }
                r13.setClipToOutline(true);
            }
        } else if (i != 0) {
            r13.setOutlineProvider(new C52552bI(r12));
        }
    }

    public static void A02(C55972k3 r1) {
        r1.setElevation(0.0f);
        r1.setClipToOutline(false);
        r1.setOutlineProvider(ViewOutlineProvider.BACKGROUND);
    }

    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        float f;
        float f2;
        float f3;
        AnonymousClass2k6 r4 = (AnonymousClass2k6) obj;
        C55992k9 r1 = (C55992k9) obj2;
        AnonymousClass28D r3 = r1.A08;
        if (r3 != null) {
            try {
                if (Build.VERSION.SDK_INT <= 20) {
                    C14260l7 r5 = r1.A07;
                    C55972k3 r42 = (C55972k3) r4;
                    if (r3.A0O(43, false)) {
                        AnonymousClass3DV r6 = r42.A00;
                        r6.A04 = true;
                        r6.A0B.setLayerType(2, null);
                        int A00 = A00(r5, r3);
                        int A002 = C64963Hp.A00(r5, r3);
                        String A0I = r3.A0I(40);
                        if (A0I == null) {
                            f = 0.0f;
                        } else {
                            f = AnonymousClass3JW.A01(A0I);
                        }
                        float f4 = (float) ((int) f);
                        String A0I2 = r3.A0I(46);
                        if (A0I2 == null) {
                            f2 = 0.0f;
                        } else {
                            f2 = AnonymousClass3JW.A01(A0I2);
                        }
                        float f5 = (float) ((int) f2);
                        int A003 = C64973Hq.A00(r3.A0M(56), 0);
                        float[] A02 = C64963Hp.A02(r3.A0M(62));
                        String A0I3 = r3.A0I(63);
                        if (A0I3 == null) {
                            f3 = 0.0f;
                        } else {
                            f3 = AnonymousClass3JW.A01(A0I3);
                        }
                        r6.A00(A02, f4, f5, f3, A00, A002, A003);
                    }
                } else {
                    A01(r1.A07, r3, (C55972k3) r4);
                }
                Drawable drawable = r1.A05;
                Drawable drawable2 = r1.A04;
                if (drawable instanceof Animatable) {
                    ((Animatable) drawable).start();
                }
                if (drawable2 instanceof Animatable) {
                    ((Animatable) drawable2).start();
                }
            } catch (IOException e) {
                C28691Op.A01("HostWithDecoratorRenderUnit", "Parse exception while binding Box Decoration", e);
            }
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return true;
    }

    @Override // X.AnonymousClass5WW
    public void Af8(Context context, Object obj, Object obj2, Object obj3) {
        AnonymousClass2k6 r5 = (AnonymousClass2k6) obj;
        C55992k9 r6 = (C55992k9) obj2;
        if (r6.A08 != null) {
            C55972k3 r52 = (C55972k3) r5;
            AnonymousClass3DV r0 = r52.A00;
            r0.A04 = false;
            r0.A0B.setLayerType(0, null);
            if (Build.VERSION.SDK_INT >= 21) {
                A02(r52);
            }
            Drawable drawable = r6.A05;
            Drawable drawable2 = r6.A04;
            if (drawable instanceof Animatable) {
                ((Animatable) drawable).stop();
            }
            if (drawable2 instanceof Animatable) {
                ((Animatable) drawable2).stop();
            }
        }
    }
}
