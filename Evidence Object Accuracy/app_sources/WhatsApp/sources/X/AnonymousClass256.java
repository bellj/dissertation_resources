package X;

import com.google.protobuf.CodedOutputStream;
import java.util.Arrays;

/* renamed from: X.256  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass256 {
    public static final AnonymousClass256 A04 = new AnonymousClass256(new int[0], new Object[0], 0, false);
    public int A00 = -1;
    public boolean A01;
    public int[] A02;
    public Object[] A03;
    public int count;

    public AnonymousClass256(int[] iArr, Object[] objArr, int i, boolean z) {
        this.count = i;
        this.A02 = iArr;
        this.A03 = objArr;
        this.A01 = z;
    }

    public int A00() {
        int A06;
        int A01;
        int i;
        int i2 = this.A00;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.count; i4++) {
            int i5 = this.A02[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 != 0) {
                if (i7 == 1) {
                    A01 = CodedOutputStream.A01((i6 << 3) | 0);
                    i = 8;
                } else if (i7 == 2) {
                    A06 = CodedOutputStream.A09((AbstractC27881Jp) this.A03[i4], i6);
                } else if (i7 == 3) {
                    A01 = CodedOutputStream.A01((i6 << 3) | 0) << 1;
                    i = ((AnonymousClass256) this.A03[i4]).A00();
                } else if (i7 == 5) {
                    A01 = CodedOutputStream.A01((i6 << 3) | 0);
                    i = 4;
                } else {
                    throw new IllegalStateException(new C28971Pt("Protocol message tag had invalid wire type."));
                }
                A06 = A01 + i;
            } else {
                A06 = CodedOutputStream.A06(i6, ((Number) this.A03[i4]).longValue());
            }
            i3 += A06;
        }
        this.A00 = i3;
        return i3;
    }

    public final void A01(int i, Object obj) {
        int i2 = this.count;
        int[] iArr = this.A02;
        if (i2 == iArr.length) {
            int i3 = i2 >> 1;
            if (i2 < 4) {
                i3 = 8;
            }
            int i4 = i2 + i3;
            this.A02 = Arrays.copyOf(iArr, i4);
            this.A03 = Arrays.copyOf(this.A03, i4);
        }
        int[] iArr2 = this.A02;
        int i5 = this.count;
        iArr2[i5] = i;
        this.A03[i5] = obj;
        this.count = i5 + 1;
    }

    public void A02(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.count; i++) {
            int i2 = this.A02[i];
            int i3 = i2 >>> 3;
            int i4 = i2 & 7;
            if (i4 == 0) {
                codedOutputStream.A0H(i3, ((Number) this.A03[i]).longValue());
            } else if (i4 == 1) {
                codedOutputStream.A0G(i3, ((Number) this.A03[i]).longValue());
            } else if (i4 == 2) {
                codedOutputStream.A0K((AbstractC27881Jp) this.A03[i], i3);
            } else if (i4 == 3) {
                int i5 = i3 << 3;
                codedOutputStream.A0C(3 | i5);
                ((AnonymousClass256) this.A03[i]).A02(codedOutputStream);
                codedOutputStream.A0C(i5 | 4);
            } else if (i4 == 5) {
                codedOutputStream.A0D(i3, ((Number) this.A03[i]).intValue());
            } else {
                throw new C28971Pt("Protocol message tag had invalid wire type.");
            }
        }
    }

    public boolean A03(AnonymousClass253 r8, int i) {
        int A03;
        if (this.A01) {
            int i2 = i >>> 3;
            int i3 = i & 7;
            if (i3 == 0) {
                A01(i, Long.valueOf(r8.A06()));
                return true;
            } else if (i3 == 1) {
                A01(i, Long.valueOf(r8.A05()));
                return true;
            } else if (i3 == 2) {
                A01(i, r8.A08());
                return true;
            } else if (i3 == 3) {
                AnonymousClass256 r1 = new AnonymousClass256(new int[8], new Object[8], 0, true);
                do {
                    A03 = r8.A03();
                    if (A03 == 0) {
                        break;
                    }
                } while (r1.A03(r8, A03));
                r8.A0C((i2 << 3) | 4);
                A01(i, r1);
                return true;
            } else if (i3 == 4) {
                return false;
            } else {
                if (i3 == 5) {
                    A01(i, Integer.valueOf(r8.A01()));
                    return true;
                }
                throw new C28971Pt("Protocol message tag had invalid wire type.");
            }
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj != null && (obj instanceof AnonymousClass256)) {
                AnonymousClass256 r5 = (AnonymousClass256) obj;
                if (this.count != r5.count || !Arrays.equals(this.A02, r5.A02) || !Arrays.deepEquals(this.A03, r5.A03)) {
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((527 + this.count) * 31) + Arrays.hashCode(this.A02)) * 31) + Arrays.deepHashCode(this.A03);
    }
}
