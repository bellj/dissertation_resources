package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4Bb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class EnumC87324Bb extends Enum {
    public static final /* synthetic */ EnumC87324Bb[] A00;
    public static final EnumC87324Bb A01;
    public static final EnumC87324Bb A02;
    public static final EnumC87324Bb A03;
    public static final EnumC87324Bb A04;
    public static final EnumC87324Bb A05;
    public static final EnumC87324Bb A06;
    public static final EnumC87324Bb A07;
    public static final EnumC87324Bb A08;
    public static final EnumC87324Bb A09;
    public static final EnumC87324Bb A0A;
    public static final EnumC87324Bb A0B;
    public final int value;

    static {
        EnumC87324Bb r14 = new EnumC87324Bb(0, "REVOKE", 0);
        A0B = r14;
        EnumC87324Bb r12 = new EnumC87324Bb(1, "EPHEMERAL_SETTING", 3);
        A04 = r12;
        EnumC87324Bb r11 = new EnumC87324Bb(2, "EPHEMERAL_SYNC_RESPONSE", 4);
        A05 = r11;
        EnumC87324Bb r10 = new EnumC87324Bb(3, "HISTORY_SYNC_NOTIFICATION", 5);
        A06 = r10;
        EnumC87324Bb r9 = new EnumC87324Bb(4, "APP_STATE_SYNC_KEY_SHARE", 6);
        A03 = r9;
        EnumC87324Bb r8 = new EnumC87324Bb(5, "APP_STATE_SYNC_KEY_REQUEST", 7);
        A02 = r8;
        EnumC87324Bb r7 = new EnumC87324Bb(6, "MSG_FANOUT_BACKFILL_REQUEST", 8);
        A08 = r7;
        EnumC87324Bb r6 = new EnumC87324Bb(7, "INITIAL_SECURITY_NOTIFICATION_SETTING_SYNC", 9);
        A07 = r6;
        EnumC87324Bb r4 = new EnumC87324Bb(8, "APP_STATE_FATAL_EXCEPTION_NOTIFICATION", 10);
        A01 = r4;
        EnumC87324Bb r3 = new EnumC87324Bb(9, "REQUEST_MEDIA_UPLOAD_MESSAGE", 12);
        A09 = r3;
        EnumC87324Bb r2 = new EnumC87324Bb(10, "REQUEST_MEDIA_UPLOAD_RESPONSE_MESSAGE", 13);
        A0A = r2;
        EnumC87324Bb[] r1 = new EnumC87324Bb[11];
        r1[0] = r14;
        r1[1] = r12;
        C12980iv.A1P(r11, r10, r9, r1);
        C12970iu.A1R(r8, r7, r6, r4, r1);
        r1[9] = r3;
        r1[10] = r2;
        A00 = r1;
    }

    public EnumC87324Bb(int i, String str, int i2) {
        this.value = i2;
    }

    public static EnumC87324Bb A00(int i) {
        if (i == 0) {
            return A0B;
        }
        if (i == 12) {
            return A09;
        }
        if (i == 13) {
            return A0A;
        }
        switch (i) {
            case 3:
                return A04;
            case 4:
                return A05;
            case 5:
                return A06;
            case 6:
                return A03;
            case 7:
                return A02;
            case 8:
                return A08;
            case 9:
                return A07;
            case 10:
                return A01;
            default:
                return null;
        }
    }

    public static EnumC87324Bb valueOf(String str) {
        return (EnumC87324Bb) Enum.valueOf(EnumC87324Bb.class, str);
    }

    public static EnumC87324Bb[] values() {
        return (EnumC87324Bb[]) A00.clone();
    }
}
