package X;

import java.security.cert.CertSelector;
import java.security.cert.PKIXParameters;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: X.4am  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C93814am {
    public int A00 = 0;
    public List A01 = C12960it.A0l();
    public List A02 = C12960it.A0l();
    public Map A03 = C12970iu.A11();
    public Map A04 = C12970iu.A11();
    public Set A05;
    public AnonymousClass5GZ A06;
    public boolean A07;
    public boolean A08 = false;
    public final PKIXParameters A09;
    public final Date A0A;
    public final Date A0B;

    public C93814am(C112085Bz r3) {
        this.A09 = r3.A01;
        this.A0B = r3.A03;
        this.A0A = r3.A02;
        this.A06 = r3.A09;
        this.A02 = C12980iv.A0x(r3.A05);
        this.A04 = new HashMap(r3.A07);
        this.A01 = C12980iv.A0x(r3.A04);
        this.A03 = new HashMap(r3.A06);
        this.A08 = r3.A0B;
        this.A00 = r3.A00;
        this.A07 = r3.A0A;
        this.A05 = r3.A08;
    }

    public C93814am(PKIXParameters pKIXParameters) {
        this.A09 = (PKIXParameters) pKIXParameters.clone();
        CertSelector targetCertConstraints = pKIXParameters.getTargetCertConstraints();
        if (targetCertConstraints != null) {
            this.A06 = new AnonymousClass5GZ(new C89734Lc(targetCertConstraints).A00);
        }
        Date date = pKIXParameters.getDate();
        this.A0B = date;
        this.A0A = date == null ? new Date() : date;
        this.A07 = pKIXParameters.isRevocationEnabled();
        this.A05 = pKIXParameters.getTrustAnchors();
    }
}
