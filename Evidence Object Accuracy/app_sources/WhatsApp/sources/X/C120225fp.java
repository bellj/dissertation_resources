package X;

import android.content.Context;

/* renamed from: X.5fp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120225fp extends AbstractC451020e {
    public final /* synthetic */ C129365xb A00;
    public final /* synthetic */ C128545wH A01;
    public final /* synthetic */ C128925wt A02;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C120225fp(Context context, C14900mE r2, C18650sn r3, C129365xb r4, C128545wH r5, C128925wt r6) {
        super(context, r2, r3);
        this.A00 = r4;
        this.A02 = r6;
        this.A01 = r5;
    }

    @Override // X.AbstractC451020e
    public void A02(C452120p r2) {
        this.A02.A00(r2);
    }

    @Override // X.AbstractC451020e
    public void A03(C452120p r1) {
        A02(r1);
    }

    @Override // X.AbstractC451020e
    public void A04(AnonymousClass1V8 r5) {
        C452120p r3;
        int i;
        AnonymousClass1V8 A0c = C117305Zk.A0c(r5);
        if (A0c != null) {
            r3 = C452120p.A00(A0c);
            if (!(r3 == null || (i = r3.A00) == 0)) {
                if (i == 1448) {
                    C128545wH.A00(r3, this.A00.A07, this.A01);
                }
                this.A02.A00(r3);
            }
        } else {
            r3 = null;
        }
        C130015yf r2 = this.A00.A08;
        r2.A01();
        r2.A02(0);
        this.A02.A00(r3);
    }
}
