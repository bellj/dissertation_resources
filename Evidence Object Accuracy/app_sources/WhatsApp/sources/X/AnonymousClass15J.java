package X;

import java.io.File;

/* renamed from: X.15J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass15J {
    public final AnonymousClass1EP A00;

    public AnonymousClass15J(AnonymousClass1EP r1) {
        this.A00 = r1;
    }

    public File A00(AnonymousClass1KS r22, File file) {
        AbstractC38621oP r8;
        AnonymousClass1EP r1 = this.A00;
        if (r22.A0G) {
            AnonymousClass01J r4 = r1.A00.A00.A01;
            C18790t3 r13 = (C18790t3) r4.AJw.get();
            C15450nH r12 = (C15450nH) r4.AII.get();
            C14950mJ r14 = (C14950mJ) r4.AKf.get();
            C20110vE A3O = r4.A3O();
            C14900mE r10 = (C14900mE) r4.A8X.get();
            AbstractC15710nm r9 = (AbstractC15710nm) r4.A4o.get();
            C22370yy r15 = (C22370yy) r4.AB0.get();
            r8 = new C38581oL(r9, r10, (C002701f) r4.AHU.get(), r12, r13, r14, (C14850m9) r4.A04.get(), A3O, r15, (C22600zL) r4.AHm.get(), r22, file);
        } else {
            AnonymousClass01J r82 = r1.A01.A00.A01;
            C18790t3 r6 = (C18790t3) r82.AJw.get();
            C15450nH r5 = (C15450nH) r82.AII.get();
            C14950mJ r42 = (C14950mJ) r82.AKf.get();
            C20110vE A3O2 = r82.A3O();
            C14900mE r92 = (C14900mE) r82.A8X.get();
            C22370yy r16 = (C22370yy) r82.AB0.get();
            r8 = new C38631oQ(r92, (C002701f) r82.AHU.get(), r5, r6, r42, (C14850m9) r82.A04.get(), A3O2, r16, (C22600zL) r82.AHm.get(), r22, file);
        }
        if (r8.A9B().A00 == 0) {
            return file;
        }
        return null;
    }
}
