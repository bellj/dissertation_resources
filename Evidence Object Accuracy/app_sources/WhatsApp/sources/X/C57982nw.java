package X;

/* renamed from: X.2nw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C57982nw extends AnonymousClass4WH {
    public final int A00;
    public final Object A01;

    public C57982nw(int i, Object obj) {
        this.A00 = i;
        this.A01 = obj;
    }

    @Override // X.AnonymousClass4WH
    public /* bridge */ /* synthetic */ void A00(Object obj) {
        ((AnonymousClass28D) obj).A02.put(this.A00, this.A01);
    }

    @Override // X.AnonymousClass4WH
    public /* bridge */ /* synthetic */ boolean A01(Object obj) {
        int i = this.A00;
        Object obj2 = this.A01;
        Object obj3 = ((AnonymousClass28D) obj).A02.get(i);
        if (obj3 == obj2) {
            return false;
        }
        if (!(obj2 instanceof Number)) {
            return !C87834De.A00(obj3, obj2);
        }
        if (obj3 instanceof Number) {
            Number number = (Number) obj3;
            Number number2 = (Number) obj2;
            if (number.longValue() == number2.longValue() && number.doubleValue() == number2.doubleValue()) {
                return false;
            }
        }
        return true;
    }
}
