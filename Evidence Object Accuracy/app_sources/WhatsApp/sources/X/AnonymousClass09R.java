package X;

import android.animation.LayoutTransition;

/* renamed from: X.09R  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass09R extends LayoutTransition {
    @Override // android.animation.LayoutTransition
    public boolean isChangingLayout() {
        return true;
    }
}
