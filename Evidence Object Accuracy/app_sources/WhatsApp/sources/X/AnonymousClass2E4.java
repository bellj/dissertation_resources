package X;

import android.graphics.Bitmap;
import com.facebook.redex.RunnableBRunnable0Shape1S0200000_I0_1;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

/* renamed from: X.2E4  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2E4 implements AnonymousClass2E5 {
    public AnonymousClass2E6 A00;
    public final C14330lG A01;
    public final C22190yg A02;
    public final AbstractC14440lR A03;

    public AnonymousClass2E4(C14330lG r1, C22190yg r2, AbstractC14440lR r3) {
        this.A03 = r3;
        this.A01 = r1;
        this.A02 = r2;
    }

    public static AnonymousClass01T A00(Bitmap bitmap, C14330lG r5) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append(UUID.randomUUID());
        sb.append(".jpeg");
        File A0M = r5.A0M(sb.toString());
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(A0M);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
            fileOutputStream.close();
            z = true;
        } catch (Exception e) {
            StringBuilder sb2 = new StringBuilder("product-details/send-product/save-to-storage/failed: ");
            sb2.append(e);
            Log.e(sb2.toString());
            z = false;
        }
        return new AnonymousClass01T(Boolean.valueOf(z), A0M);
    }

    @Override // X.AnonymousClass2E5
    public void AS6(Bitmap bitmap, C68203Um r5, boolean z) {
        this.A03.Ab2(new RunnableBRunnable0Shape1S0200000_I0_1(this, 17, bitmap));
    }
}
