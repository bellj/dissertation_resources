package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1qy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40051qy extends AnonymousClass1JW {
    public final UserJid A00;
    public final C30461Xm A01;

    public C40051qy(AbstractC15710nm r1, C15450nH r2, UserJid userJid, C30461Xm r4, boolean z) {
        super(r1, r2, z);
        this.A01 = r4;
        this.A00 = userJid;
    }
}
