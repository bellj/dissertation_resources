package X;

import android.app.Activity;
import java.lang.ref.WeakReference;

/* renamed from: X.5vE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C127895vE {
    public String A00;
    public String A01;
    public String A02;
    public final C17120qI A03;
    public final WeakReference A04;

    public C127895vE(Activity activity, C17120qI r5, String str, String str2, String str3) {
        this.A03 = r5;
        this.A04 = C12970iu.A10(activity);
        this.A02 = str;
        this.A00 = str3;
        this.A01 = str2;
        r5.A02(str2).A00(new AbstractC50172Ok() { // from class: X.6Dr
            @Override // X.AbstractC50172Ok
            public final void APz(Object obj) {
                Activity activity2;
                C127895vE r2 = C127895vE.this;
                if (!((AnonymousClass6E7) obj).A00.contains(r2.A02) && (activity2 = (Activity) r2.A04.get()) != null) {
                    activity2.finish();
                }
            }
        }, AnonymousClass6E7.class, this);
    }
}
