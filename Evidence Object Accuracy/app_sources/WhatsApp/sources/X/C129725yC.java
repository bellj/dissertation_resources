package X;

import android.content.Context;
import android.text.TextUtils;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;
import com.whatsapp.wamsys.JniBridge;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5yC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129725yC {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final C18640sm A03;
    public final C14830m7 A04;
    public final AnonymousClass102 A05;
    public final C17220qS A06;
    public final AnonymousClass60Z A07;
    public final C18650sn A08;
    public final C18610sj A09;
    public final C17070qD A0A;
    public final AnonymousClass6Lp A0B;
    public final C129095xA A0C;

    public C129725yC(Context context, C14900mE r2, C15570nT r3, C18640sm r4, C14830m7 r5, AnonymousClass102 r6, C17220qS r7, AnonymousClass60Z r8, C18650sn r9, C18610sj r10, C17070qD r11, AnonymousClass6Lp r12, C129095xA r13) {
        this.A04 = r5;
        this.A00 = context;
        this.A01 = r2;
        this.A02 = r3;
        this.A06 = r7;
        this.A0A = r11;
        this.A09 = r10;
        this.A0C = r13;
        this.A05 = r6;
        this.A07 = r8;
        this.A03 = r4;
        this.A08 = r9;
        this.A0B = r12;
    }

    public void A00(C30881Ze r9) {
        if (r9 == null) {
            this.A0B.AP5(null, C117305Zk.A0L(), null);
            return;
        }
        AnonymousClass1ZY r0 = r9.A08;
        AnonymousClass009.A05(r0);
        if (TextUtils.isEmpty(((C119775f5) r0).A06)) {
            new C129205xL(this.A00, this.A01, this.A03, this.A08, this.A09, new AbstractC136266Lw(r9, this) { // from class: X.6Ab
                public final /* synthetic */ C30881Ze A00;
                public final /* synthetic */ C129725yC A01;

                {
                    this.A01 = r2;
                    this.A00 = r1;
                }

                @Override // X.AbstractC136266Lw
                public final void AVO(C452120p r5, String str) {
                    C129725yC r3 = this.A01;
                    C30881Ze r2 = this.A00;
                    C119775f5 r1 = (C119775f5) r2.A08;
                    if (TextUtils.isEmpty(str) || r1 == null) {
                        r3.A0B.AP5(null, C117305Zk.A0L(), null);
                        return;
                    }
                    r1.A06 = str;
                    r3.A0A.A00().A03(
                    /*  JADX ERROR: Method code generation error
                        jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001d: INVOKE  
                          (wrap: X.1nR : 0x0014: INVOKE  (r1v3 X.1nR A[REMOVE]) = (wrap: X.0qD : 0x0012: IGET  (r0v2 X.0qD A[REMOVE]) = (r3v0 'r3' X.5yC) X.5yC.A0A X.0qD) type: VIRTUAL call: X.0qD.A00():X.1nR)
                          (wrap: X.67i : 0x001a: CONSTRUCTOR  (r0v3 X.67i A[REMOVE]) = (r2v0 'r2' X.1Ze), (r3v0 'r3' X.5yC) call: X.67i.<init>(X.1Ze, X.5yC):void type: CONSTRUCTOR)
                          (r2v0 'r2' X.1Ze)
                         type: VIRTUAL call: X.1nR.A03(X.20l, X.1Pl):void in method: X.6Ab.AVO(X.20p, java.lang.String):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                        	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                        	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.dex.regions.Region.generate(Region.java:35)
                        	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                        	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                        	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
                        	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                        	at java.util.ArrayList.forEach(ArrayList.java:1259)
                        	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                        	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                        Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001a: CONSTRUCTOR  (r0v3 X.67i A[REMOVE]) = (r2v0 'r2' X.1Ze), (r3v0 'r3' X.5yC) call: X.67i.<init>(X.1Ze, X.5yC):void type: CONSTRUCTOR in method: X.6Ab.AVO(X.20p, java.lang.String):void, file: classes4.dex
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                        	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                        	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                        	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                        	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                        	... 19 more
                        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: X.67i, state: NOT_LOADED
                        	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                        	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                        	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                        	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                        	... 25 more
                        */
                    /*
                        this = this;
                        X.5yC r3 = r4.A01
                        X.1Ze r2 = r4.A00
                        X.1ZY r1 = r2.A08
                        X.5f5 r1 = (X.C119775f5) r1
                        boolean r0 = android.text.TextUtils.isEmpty(r6)
                        if (r0 != 0) goto L_0x0021
                        if (r1 == 0) goto L_0x0021
                        r1.A06 = r6
                        X.0qD r0 = r3.A0A
                        X.1nR r1 = r0.A00()
                        X.67i r0 = new X.67i
                        r0.<init>(r2, r3)
                        r1.A03(r0, r2)
                        return
                    L_0x0021:
                        X.6Lp r2 = r3.A0B
                        X.20p r1 = X.C117305Zk.A0L()
                        r0 = 0
                        r2.AP5(r0, r1, r0)
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.C133256Ab.AVO(X.20p, java.lang.String):void");
                }
            }).A00(r9.A0A);
            return;
        }
        A01(r9);
    }

    public final void A01(C30881Ze r11) {
        String str;
        Log.i("PAY: BrazilDeviceBindingAction starts to bind device");
        C14830m7 r3 = this.A04;
        C15570nT r2 = this.A02;
        String A0j = C117305Zk.A0j(r2, r3);
        String A00 = this.A0C.A00(r11.A01);
        String A0j2 = C117305Zk.A0j(r2, r3);
        AnonymousClass60Z r32 = this.A07;
        String A01 = r32.A01(1);
        String str2 = null;
        if (!TextUtils.isEmpty(A01)) {
            byte[] bArr = (byte[]) JniBridge.jvidispatchOOO(8, A01.getBytes(), C12960it.A0d(A00, C12960it.A0k("C=US,ST=California,L=Menlo Park,O=Facebook,OU=WhatsApp,CN=")).getBytes());
            if (bArr != null) {
                str2 = new String(bArr).split("\u0000")[0];
            }
        }
        AnonymousClass1ZY r0 = r11.A08;
        AnonymousClass009.A05(r0);
        String str3 = ((C119775f5) r0).A06;
        try {
            JSONObject A0a = C117295Zj.A0a();
            A0a.put("alg", "PS256");
            A0a.put("typ", "JOSE");
            str = r32.A06(C117305Zk.A0l(A00, "kid", A0a), AnonymousClass60Z.A00(A00, A0j, str3));
        } catch (JSONException e) {
            Log.w("PAY: generateDeviceBindingJwsToken threw creating json string: ", e);
            str = null;
        }
        C17220qS r1 = this.A06;
        String A012 = r1.A01();
        C117295Zj.A1B(r1, new IDxRCallbackShape2S0100000_3_I1(this.A00, this.A01, this.A08, this, 1), new C126215sW(new AnonymousClass3CT(A012), r11.A0A, str2, str, A0j, A00, A0j2).A00, A012);
    }
}
