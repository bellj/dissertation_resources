package X;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;
import com.whatsapp.calling.videoparticipant.VideoCallParticipantView;

/* renamed from: X.3gq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73813gq extends ViewOutlineProvider {
    public final /* synthetic */ int A00;
    public final /* synthetic */ VideoCallParticipantView A01;

    public C73813gq(VideoCallParticipantView videoCallParticipantView, int i) {
        this.A01 = videoCallParticipantView;
        this.A00 = i;
    }

    @Override // android.view.ViewOutlineProvider
    public void getOutline(View view, Outline outline) {
        outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), (float) this.A00);
    }
}
