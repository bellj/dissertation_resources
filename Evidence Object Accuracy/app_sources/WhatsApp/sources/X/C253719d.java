package X;

import android.content.Context;
import android.widget.ImageView;
import com.whatsapp.Mp4Ops;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/* renamed from: X.19d  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C253719d {
    public static final AbstractC39381po A0D = new C39371pn();
    public C38721ob A00;
    public ThreadPoolExecutor A01;
    public final AbstractC15710nm A02;
    public final C14330lG A03;
    public final C14900mE A04;
    public final Mp4Ops A05;
    public final C18790t3 A06;
    public final C18720su A07;
    public final C17050qB A08;
    public final C14830m7 A09;
    public final C16590pI A0A;
    public final C18810t5 A0B;
    public final AbstractC14440lR A0C;

    public C253719d(AbstractC15710nm r1, C14330lG r2, C14900mE r3, Mp4Ops mp4Ops, C18790t3 r5, C18720su r6, C17050qB r7, C14830m7 r8, C16590pI r9, C18810t5 r10, AbstractC14440lR r11) {
        this.A0A = r9;
        this.A09 = r8;
        this.A07 = r6;
        this.A05 = mp4Ops;
        this.A04 = r3;
        this.A02 = r1;
        this.A0C = r11;
        this.A03 = r2;
        this.A06 = r5;
        this.A08 = r7;
        this.A0B = r10;
    }

    public final ThreadPoolExecutor A00() {
        AnonymousClass009.A01();
        ThreadPoolExecutor threadPoolExecutor = this.A01;
        if (threadPoolExecutor != null) {
            return threadPoolExecutor;
        }
        ThreadPoolExecutor A8a = this.A0C.A8a("GifCacheWorker", new LinkedBlockingQueue(), 4, 4, 10, 1);
        this.A01 = A8a;
        return A8a;
    }

    public void A01(ImageView imageView, String str) {
        Context context = imageView.getContext();
        AnonymousClass009.A01();
        C38721ob r0 = this.A00;
        if (r0 == null) {
            File file = new File(this.A0A.A00.getCacheDir(), "GifsCache");
            if (!file.mkdirs() && !file.isDirectory()) {
                Log.w("gif/cache/unable to create gifs directory");
            }
            C38771og r3 = new C38771og(this.A04, this.A06, this.A0B, file, "gif-cache");
            r3.A00 = context.getResources().getDimensionPixelSize(R.dimen.gif_cache_thumb_loader_size);
            r0 = r3.A00();
            this.A00 = r0;
        }
        r0.A01(imageView, str);
    }

    public final byte[] A02(String str) {
        AnonymousClass009.A01();
        C39391pp A00 = this.A07.A06().A00(str);
        if (A00 != null) {
            return A00.A02;
        }
        return null;
    }
}
