package X;

/* renamed from: X.0Jd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03790Jd {
    A01(1),
    /* Fake field, exist only in values array */
    EF16(2);
    
    public final int value;

    EnumC03790Jd(int i) {
        this.value = i;
    }
}
