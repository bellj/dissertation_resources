package X;

import android.os.Build;
import java.util.Arrays;

/* renamed from: X.07h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C015407h {
    public static int A00(Object... objArr) {
        if (Build.VERSION.SDK_INT >= 19) {
            return C015507i.A00(objArr);
        }
        return Arrays.hashCode(objArr);
    }

    public static boolean A01(Object obj, Object obj2) {
        if (Build.VERSION.SDK_INT >= 19) {
            return C015507i.A01(obj, obj2);
        }
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }
}
