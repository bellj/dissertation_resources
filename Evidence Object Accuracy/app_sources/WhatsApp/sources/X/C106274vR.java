package X;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;

/* renamed from: X.4vR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106274vR implements AnonymousClass5WW {
    @Override // X.AnonymousClass5WW
    public void A6O(Context context, Object obj, Object obj2, Object obj3) {
        View view = (View) obj;
        C56002kA r5 = (C56002kA) obj2;
        if (Build.VERSION.SDK_INT >= 17) {
            view.setBackgroundColor(r5.A00);
        } else {
            view.setBackgroundDrawable(new ColorDrawable(r5.A00));
        }
    }

    @Override // X.AnonymousClass5WW
    public boolean Adc(Object obj, Object obj2, Object obj3, Object obj4) {
        return C12980iv.A1V(((C56002kA) obj).A00, ((C56002kA) obj2).A00);
    }

    @Override // X.AnonymousClass5WW
    public /* bridge */ /* synthetic */ void Af8(Context context, Object obj, Object obj2, Object obj3) {
    }
}
