package X;

import android.text.TextUtils;
import com.whatsapp.conversation.conversationrow.message.MessageDetailsActivity;
import com.whatsapp.jid.UserJid;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.3cf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C71273cf implements Comparator {
    public Map A00;
    public final C71333cl A01;
    public final /* synthetic */ MessageDetailsActivity A02;

    public C71273cf(MessageDetailsActivity messageDetailsActivity) {
        this.A02 = messageDetailsActivity;
        this.A01 = new C71333cl(messageDetailsActivity.A09, ((ActivityC13830kP) messageDetailsActivity).A01);
        this.A00 = new HashMap(messageDetailsActivity.A0c.size());
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        AnonymousClass1lQ r8 = (AnonymousClass1lQ) obj;
        AnonymousClass1lQ r9 = (AnonymousClass1lQ) obj2;
        int A00 = r8.A00();
        int A002 = r9.A00();
        if (A00 == A002) {
            UserJid userJid = r8.A01;
            if (userJid != null) {
                UserJid userJid2 = r9.A01;
                if (userJid2 == null) {
                    return -1;
                }
                Map map = this.A00;
                C15370n3 r3 = (C15370n3) map.get(userJid);
                if (r3 == null) {
                    r3 = this.A02.A07.A0B(userJid);
                    map.put(userJid, r3);
                }
                C15370n3 r2 = (C15370n3) map.get(userJid2);
                if (r2 == null) {
                    r2 = this.A02.A07.A0B(userJid2);
                    map.put(userJid2, r2);
                }
                boolean z = !TextUtils.isEmpty(r3.A0K);
                if (z == (!TextUtils.isEmpty(r2.A0K))) {
                    return this.A01.compare(r3, r2);
                }
                return z ? -1 : 1;
            } else if (r9.A01 == null) {
                return 0;
            } else {
                return 1;
            }
        } else if (C37381mH.A00(A00, A002) < 0) {
            return 1;
        } else {
            return -1;
        }
    }
}
