package X;

/* renamed from: X.0rZ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C17890rZ implements AbstractC17410ql {
    public final C18160s0 A00;

    @Override // X.AbstractC17410ql
    public void AMk() {
    }

    public C17890rZ(C18160s0 r2) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
    }

    @Override // X.AbstractC17410ql
    public void AMj() {
        this.A00.A02(false);
    }

    @Override // X.AbstractC17410ql
    public void AMl(boolean z) {
        this.A00.A02(true);
    }
}
