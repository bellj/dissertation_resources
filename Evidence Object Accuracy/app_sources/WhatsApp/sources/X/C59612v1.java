package X;

import com.google.android.gms.maps.model.LatLng;

/* renamed from: X.2v1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59612v1 extends C37171lc {
    public String A00;
    public boolean A01 = true;
    public boolean A02 = true;
    public final int A03;
    public final LatLng A04;
    public final AnonymousClass3M8 A05;
    public final C90944Pv A06;
    public final AnonymousClass5W0 A07;
    public final AbstractC115945Tn A08;
    public final boolean A09;

    public C59612v1(LatLng latLng, AnonymousClass3M8 r3, C90944Pv r4, AnonymousClass5W0 r5, AbstractC115945Tn r6, int i, boolean z) {
        super(AnonymousClass39o.A02);
        this.A03 = i;
        this.A09 = z;
        this.A05 = r3;
        this.A04 = latLng;
        this.A07 = r5;
        this.A08 = r6;
        this.A06 = r4;
    }

    @Override // X.C37171lc
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass() || !super.equals(obj)) {
            return false;
        }
        AnonymousClass3M8 r1 = this.A05;
        AnonymousClass009.A05(r1.A07);
        return r1.equals(((C59612v1) obj).A05);
    }

    @Override // X.C37171lc
    public int hashCode() {
        String str = this.A05.A07;
        AnonymousClass009.A05(str);
        return str.hashCode();
    }
}
