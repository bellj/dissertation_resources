package X;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Parcelable;
import com.facebook.redex.RunnableBRunnable0Shape0S0300100_I0;
import com.whatsapp.jid.Jid;
import com.whatsapp.location.LocationSharingService;
import com.whatsapp.util.Log;

/* renamed from: X.0o7  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15910o7 implements AbstractC15920o8 {
    public final AbstractC15710nm A00;
    public final C15990oG A01;
    public final C18240s8 A02;
    public final C16030oK A03;
    public final C17220qS A04;

    public C15910o7(AbstractC15710nm r1, C15990oG r2, C18240s8 r3, C16030oK r4, C17220qS r5) {
        this.A00 = r1;
        this.A04 = r5;
        this.A02 = r3;
        this.A01 = r2;
        this.A03 = r4;
    }

    @Override // X.AbstractC15920o8
    public int[] ADF() {
        return new int[]{117, 206};
    }

    @Override // X.AbstractC15920o8
    public boolean AI8(Message message, int i) {
        StringBuilder sb;
        String str;
        AbstractC14640lm r7;
        if (i == 117) {
            Bundle data = message.getData();
            Parcelable parcelable = data.getParcelable("jid");
            long j = data.getLong("elapsed");
            C15930o9 r14 = (C15930o9) message.obj;
            StringBuilder sb2 = new StringBuilder("LiveLocationXmppMessageHandler/on-location-update; jid=");
            sb2.append(parcelable);
            sb2.append("; elapsed=");
            sb2.append(j);
            Log.i(sb2.toString());
            int i2 = r14.A01;
            if (i2 != 2) {
                sb = new StringBuilder();
                str = "LiveLocationXmppMessageHandler/invalid ciphertext version; ciphertextVersion=";
            } else {
                i2 = r14.A00;
                if (i2 != 3) {
                    sb = new StringBuilder();
                    str = "LiveLocationXmppMessageHandler/invalid ciphertext type; ciphertextType=";
                } else {
                    C18240s8 r2 = this.A02;
                    r2.A00.execute(new RunnableBRunnable0Shape0S0300100_I0(this, parcelable, r14, 3, j));
                    return true;
                }
            }
            sb.append(str);
            sb.append(i2);
            Log.w(sb.toString());
            return true;
        } else if (i != 206) {
            return false;
        } else {
            AnonymousClass1V8 r6 = (AnonymousClass1V8) message.obj;
            String A0I = r6.A0I("id", null);
            int i3 = 0;
            AnonymousClass1V8 A0D = r6.A0D(0);
            Jid A0A = r6.A0A(this.A00, Jid.class, "from");
            AnonymousClass009.A05(A0A);
            if (AnonymousClass1V8.A02(A0D, "start")) {
                String A0I2 = A0D.A0I("duration", null);
                long j2 = 0;
                if (A0I2 != null) {
                    j2 = Long.parseLong(A0I2);
                }
                C16030oK r8 = this.A03;
                if (A0A instanceof AbstractC14640lm) {
                    r7 = (AbstractC14640lm) A0A;
                } else {
                    r7 = null;
                }
                AnonymousClass009.A05(r7);
                long j3 = j2 * 1000;
                StringBuilder sb3 = new StringBuilder("LocationSharingManager/onStartLocationReporting; jid=");
                sb3.append(r7);
                sb3.append("; duration=");
                sb3.append(j3);
                Log.i(sb3.toString());
                if (r8.A0f(r7)) {
                    Context context = r8.A0L.A00;
                    LocationSharingService.A00(context, new Intent(context, LocationSharingService.class).setAction("com.whatsapp.ShareLocationService.START_LOCATION_REPORTING").putExtra("duration", j3));
                    synchronized (r8.A0V) {
                        r8.A00 = 2 | r8.A00;
                    }
                    i3 = 0;
                } else {
                    StringBuilder sb4 = new StringBuilder("LocationSharingManager/onStartLocationReporting/sharing not enabled; jid=");
                    sb4.append(r7);
                    Log.w(sb4.toString());
                    i3 = 401;
                }
            } else if (AnonymousClass1V8.A02(A0D, "stop")) {
                this.A03.A0G();
            } else if (!AnonymousClass1V8.A02(A0D, "enable")) {
                i3 = 501;
            }
            C17220qS r3 = this.A04;
            Message obtain = Message.obtain(null, 0, 225, i3, A0A);
            obtain.getData().putString("id", A0I);
            r3.A06(obtain);
            return true;
        }
    }
}
