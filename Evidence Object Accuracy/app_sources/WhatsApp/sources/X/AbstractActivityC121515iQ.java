package X;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import com.whatsapp.R;
import com.whatsapp.payments.ui.IndiaUpiOnboardingErrorEducationActivity;

/* renamed from: X.5iQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC121515iQ extends AbstractActivityC121625j3 implements AnonymousClass6MS, AbstractC136246Lu {
    public C30861Zc A00;
    public C120505gI A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public final BroadcastReceiver A06 = new C117355Zp(this);
    public final C30931Zj A07 = C117295Zj.A0G("IndiaUpiBaseResetPinActivity");

    @Override // X.AbstractActivityC121665jA, X.ActivityC13810kN
    public void A2A(int i) {
        if (i == R.string.payments_set_pin_success) {
            A2q();
            AbstractActivityC119235dO.A1f(this);
            return;
        }
        super.A2A(i);
    }

    @Override // X.AbstractActivityC121545iU
    public void A38() {
        super.A38();
        AfX(getString(R.string.payments_upi_pin_setup_connecting_to_npci));
    }

    @Override // X.AbstractActivityC121545iU
    public void A3C() {
        A2C(R.string.payments_upi_pin_setup_connecting_to_npci);
        super.A3C();
    }

    public final void A3H(AnonymousClass60V r4) {
        AaN();
        if (r4.A00 == 0) {
            r4.A00 = R.string.payments_set_pin_error;
        }
        if (((AbstractActivityC121665jA) this).A0N) {
            A2q();
            Intent A0D = C12990iw.A0D(this, IndiaUpiOnboardingErrorEducationActivity.class);
            if (C12960it.A1W(r4.A01)) {
                A0D.putExtra("error", r4.A01(this));
            }
            A0D.putExtra("error", r4.A00);
            AbstractActivityC119235dO.A0n(A0D, this);
            return;
        }
        Adp(r4.A01(this));
    }

    @Override // X.AnonymousClass6MS
    public void ARr(C452120p r5, String str) {
        Integer num;
        C30861Zc r0;
        AnonymousClass1ZY r1;
        ((AbstractActivityC121665jA) this).A0D.A04(this.A00, r5, 1);
        if (!TextUtils.isEmpty(str) && (r0 = this.A00) != null && (r1 = r0.A08) != null) {
            this.A01.A01((C119755f3) r1, this);
        } else if (r5 != null && !AnonymousClass69E.A02(this, "upi-list-keys", r5.A00, true)) {
            if (((AbstractActivityC121545iU) this).A06.A07("upi-list-keys")) {
                ((AbstractActivityC121665jA) this).A0B.A0C();
                ((AbstractActivityC121545iU) this).A09.A00();
                return;
            }
            C30931Zj r3 = this.A07;
            StringBuilder A0k = C12960it.A0k("onListKeys: ");
            AnonymousClass1ZY r12 = null;
            if (str != null) {
                num = Integer.valueOf(str.length());
            } else {
                num = null;
            }
            A0k.append(num);
            A0k.append(" bankAccount: ");
            A0k.append(this.A00);
            A0k.append(" countrydata: ");
            C30861Zc r02 = this.A00;
            if (r02 != null) {
                r12 = r02.A08;
            }
            A0k.append(r12);
            r3.A06(C12960it.A0d(" failed; ; showErrorAndFinish", A0k));
            A39();
        }
    }

    @Override // X.AbstractC136246Lu
    public void AV4(C452120p r11) {
        ((AbstractActivityC121665jA) this).A0D.A04(this.A00, r11, 16);
        if (r11 == null) {
            this.A05 = AbstractActivityC119235dO.A0O(this);
            ((AbstractActivityC121545iU) this).A06.A03("upi-get-credential");
            AaN();
            String A0A = ((AbstractActivityC121665jA) this).A0B.A0A();
            C30861Zc r0 = this.A00;
            A3E((C119755f3) r0.A08, A0A, r0.A0B, this.A05, (String) C117295Zj.A0R(r0.A09), 1);
        } else if (!AnonymousClass69E.A02(this, "upi-generate-otp", r11.A00, true)) {
            this.A07.A06("onRequestOtp failed; showErrorAndFinish");
            A3H(new AnonymousClass60V(R.string.payments_set_pin_opt_not_requested));
        }
    }

    @Override // X.AnonymousClass6MS
    public void AVu(C452120p r5) {
        int i;
        ((AbstractActivityC121665jA) this).A0D.A04(this.A00, r5, 6);
        if (r5 == null) {
            this.A07.A06("onSetPin success; showSuccessAndFinish");
            C12960it.A1E(new C124005oI(this), ((ActivityC13830kP) this).A05);
            return;
        }
        AaN();
        if (!AnonymousClass69E.A02(this, "upi-set-mpin", r5.A00, true)) {
            Bundle A0D = C12970iu.A0D();
            A0D.putInt("error_code", r5.A00);
            C30861Zc r0 = this.A00;
            if (!(r0 == null || r0.A08 == null)) {
                int i2 = r5.A00;
                if (i2 == 11460 || i2 == 11461) {
                    i = 14;
                } else if (i2 == 11456 || i2 == 11471) {
                    i = 13;
                } else if (i2 == 11458 || i2 == 11457) {
                    i = 17;
                } else {
                    i = 10;
                    if (i2 != 11459) {
                        i = 16;
                        if (i2 != 11496) {
                            if (i2 == 11499) {
                                i = 23;
                            } else {
                                this.A07.A06("onSetPin failed; showErrorAndFinish");
                            }
                        }
                    }
                }
                C36021jC.A02(this, A0D, i);
                return;
            }
            A39();
        }
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121665jA, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C14850m9 r0 = ((ActivityC13810kN) this).A0C;
        C14900mE r15 = ((ActivityC13810kN) this).A05;
        C15570nT r14 = ((ActivityC13790kL) this).A01;
        C17220qS r13 = ((AbstractActivityC121685jC) this).A0H;
        C18590sh r11 = ((AbstractActivityC121545iU) this).A0C;
        C17070qD r10 = ((AbstractActivityC121685jC) this).A0P;
        C21860y6 r9 = ((AbstractActivityC121685jC) this).A0I;
        C1308460e r8 = ((AbstractActivityC121665jA) this).A0A;
        C18610sj r7 = ((AbstractActivityC121685jC) this).A0M;
        AnonymousClass102 r6 = ((AbstractActivityC121545iU) this).A02;
        AnonymousClass6BE r5 = ((AbstractActivityC121665jA) this).A0D;
        this.A01 = new C120505gI(this, r15, r14, ((ActivityC13810kN) this).A07, r6, r0, r13, r8, ((AbstractActivityC121665jA) this).A0B, r9, ((AbstractActivityC121685jC) this).A0K, r7, r10, r5, ((AbstractActivityC121545iU) this).A0B, r11);
        C06380Tj.A00(getApplicationContext()).A02(this.A06, new IntentFilter("TRIGGER_OTP"));
    }

    @Override // X.AbstractActivityC121545iU, android.app.Activity
    public Dialog onCreateDialog(int i) {
        return onCreateDialog(i, null);
    }

    @Override // android.app.Activity
    public Dialog onCreateDialog(int i, Bundle bundle) {
        if (i == 10) {
            String A0A = ((AbstractActivityC121665jA) this).A0B.A0A();
            return A31(new Runnable(A0A) { // from class: X.6I8
                public final /* synthetic */ String A01;

                {
                    this.A01 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121515iQ r3 = AbstractActivityC121515iQ.this;
                    String str = this.A01;
                    if (!TextUtils.isEmpty(str)) {
                        r3.A05 = AbstractActivityC119235dO.A0O(r3);
                        r3.A01.A01((C119755f3) r3.A00.A08, null);
                        C30861Zc r0 = r3.A00;
                        r3.A3E((C119755f3) r0.A08, str, r0.A0B, r3.A05, (String) C117295Zj.A0R(r0.A09), 1);
                        return;
                    }
                    r3.A3C();
                }
            }, ((AbstractActivityC121545iU) this).A03.A01(bundle, getString(R.string.payments_set_pin_invalid_pin_retry)), i, R.string.yes, R.string.no);
        } else if (i == 23) {
            return A31(new Runnable() { // from class: X.6Fp
                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121515iQ r3 = AbstractActivityC121515iQ.this;
                    r3.A2C(R.string.payments_upi_pin_setup_connecting_to_npci);
                    ((AbstractActivityC121685jC) r3).A0M.A08(new C1327568c(r3), 2);
                }
            }, ((AbstractActivityC121545iU) this).A03.A01(bundle, getString(R.string.payments_set_pin_incorrect_format_error)), i, R.string.payments_try_again, R.string.cancel);
        } else if (i == 13) {
            ((AbstractActivityC121665jA) this).A0B.A0D();
            return A31(new Runnable() { // from class: X.6Fq
                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121515iQ r1 = AbstractActivityC121515iQ.this;
                    r1.A2C(R.string.payments_upi_pin_setup_connecting_to_npci);
                    r1.A36();
                }
            }, ((AbstractActivityC121545iU) this).A03.A01(bundle, getString(R.string.payments_set_pin_retry)), i, R.string.yes, R.string.no);
        } else if (i == 14) {
            return A31(new Runnable() { // from class: X.6Fr
                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121515iQ r2 = AbstractActivityC121515iQ.this;
                    r2.A2C(R.string.payments_upi_pin_setup_connecting_to_npci);
                    r2.A01.A01((C119755f3) r2.A00.A08, r2);
                }
            }, ((AbstractActivityC121545iU) this).A03.A01(bundle, getString(R.string.payments_set_pin_otp_incorrect)), i, R.string.payments_try_again, R.string.cancel);
        } else if (i == 16) {
            return A31(new Runnable() { // from class: X.6Fs
                @Override // java.lang.Runnable
                public final void run() {
                    AbstractActivityC121515iQ r2 = AbstractActivityC121515iQ.this;
                    r2.A2C(R.string.payments_upi_pin_setup_connecting_to_npci);
                    r2.A01.A01((C119755f3) r2.A00.A08, r2);
                }
            }, ((AbstractActivityC121545iU) this).A03.A01(bundle, getString(R.string.payments_set_pin_atm_pin_incorrect)), i, R.string.payments_try_again, R.string.cancel);
        } else if (i != 17) {
            return super.onCreateDialog(i);
        } else {
            return A31(null, ((AbstractActivityC121545iU) this).A03.A01(bundle, C12960it.A0X(this, 6, C12970iu.A1b(), 0, R.string.payments_card_or_expiry_incorrect_with_placeholder)), i, R.string.payments_try_again, R.string.cancel);
        }
    }

    @Override // X.AbstractActivityC121545iU, X.AbstractActivityC121685jC, X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC000800j, X.ActivityC000900k, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        C06380Tj.A00(getApplicationContext()).A01(this.A06);
    }

    @Override // android.app.Activity
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        ((AbstractActivityC121665jA) this).A0N = bundle.getBoolean("inSetupSavedInst");
        C30861Zc r0 = (C30861Zc) bundle.getParcelable("bankAccountSavedInst");
        if (r0 != null) {
            this.A00 = r0;
            this.A00.A08 = (AnonymousClass1ZY) bundle.getParcelable("countryDataSavedInst");
        }
        if (bundle.containsKey("debitLast6SavedInst")) {
            this.A04 = bundle.getString("debitLast6SavedInst");
        }
        if (bundle.containsKey("debitExpiryMonthSavedInst")) {
            this.A02 = bundle.getString("debitExpiryMonthSavedInst");
        }
        if (bundle.containsKey("debitExpiryYearSavedInst")) {
            this.A03 = bundle.getString("debitExpiryYearSavedInst");
        }
        if (bundle.containsKey("seqNumSavedInst")) {
            this.A05 = bundle.getString("seqNumSavedInst");
        }
    }

    @Override // X.AbstractActivityC121545iU, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        AnonymousClass1ZY r1;
        super.onSaveInstanceState(bundle);
        if (((AbstractActivityC121665jA) this).A0N) {
            bundle.putBoolean("inSetupSavedInst", true);
        }
        C30861Zc r12 = this.A00;
        if (r12 != null) {
            bundle.putParcelable("bankAccountSavedInst", r12);
        }
        C30861Zc r0 = this.A00;
        if (!(r0 == null || (r1 = r0.A08) == null)) {
            bundle.putParcelable("countryDataSavedInst", r1);
        }
        String str = this.A04;
        if (str != null) {
            bundle.putString("debitLast6SavedInst", str);
        }
        String str2 = this.A02;
        if (str2 != null) {
            bundle.putString("debitExpiryMonthSavedInst", str2);
        }
        String str3 = this.A03;
        if (str3 != null) {
            bundle.putString("debitExpiryYearSavedInst", str3);
        }
        String str4 = this.A05;
        if (str4 != null) {
            bundle.putString("seqNumSavedInst", str4);
        }
    }
}
