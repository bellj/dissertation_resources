package X;

import android.media.MediaPlayer;
import com.whatsapp.util.Log;
import com.whatsapp.videoplayback.VideoSurfaceView;

/* renamed from: X.4iF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98164iF implements MediaPlayer.OnVideoSizeChangedListener {
    public final /* synthetic */ VideoSurfaceView A00;

    public C98164iF(VideoSurfaceView videoSurfaceView) {
        this.A00 = videoSurfaceView;
    }

    @Override // android.media.MediaPlayer.OnVideoSizeChangedListener
    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        VideoSurfaceView videoSurfaceView = this.A00;
        videoSurfaceView.A08 = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        videoSurfaceView.A07 = videoHeight;
        StringBuilder A0k = C12960it.A0k("videoview/onVideoSizeChanged: ");
        A0k.append(videoSurfaceView.A08);
        Log.i(C12960it.A0e("x", A0k, videoHeight));
        if (videoSurfaceView.A08 != 0 && videoSurfaceView.A07 != 0) {
            videoSurfaceView.getHolder().setFixedSize(videoSurfaceView.A08, videoSurfaceView.A07);
            videoSurfaceView.requestLayout();
        }
    }
}
