package X;

import android.graphics.Rect;
import android.view.View;

/* renamed from: X.0du  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10030du implements Runnable {
    public final /* synthetic */ Rect A00;
    public final /* synthetic */ View A01;
    public final /* synthetic */ AnonymousClass0EJ A02;
    public final /* synthetic */ AbstractC06480Tu A03;

    public RunnableC10030du(Rect rect, View view, AnonymousClass0EJ r3, AbstractC06480Tu r4) {
        this.A02 = r3;
        this.A03 = r4;
        this.A01 = view;
        this.A00 = rect;
    }

    @Override // java.lang.Runnable
    public void run() {
        AbstractC06480Tu.A00(this.A01, this.A00);
    }
}
