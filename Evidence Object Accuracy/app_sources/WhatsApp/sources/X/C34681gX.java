package X;

import com.google.protobuf.CodedOutputStream;

/* renamed from: X.1gX  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34681gX extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final C34681gX A01;
    public static volatile AnonymousClass255 A02;
    public AnonymousClass1K6 A00 = AnonymousClass277.A01;

    static {
        C34681gX r0 = new C34681gX();
        A01 = r0;
        r0.A0W();
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.A00.size(); i3++) {
            i2 += CodedOutputStream.A0B((String) this.A00.get(i3));
        }
        int size = 0 + i2 + this.A00.size() + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = size;
        return size;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        for (int i = 0; i < this.A00.size(); i++) {
            codedOutputStream.A0I(1, (String) this.A00.get(i));
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
