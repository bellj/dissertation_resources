package X;

import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.5hl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C121385hl extends AbstractC130285z6 {
    public AnonymousClass619 A00;
    public String A01;
    public List A02;

    public C121385hl(AnonymousClass619 r1, String str, List list) {
        this.A01 = str;
        this.A00 = r1;
        this.A02 = list;
    }

    public static C121385hl A00(AnonymousClass1V8 r25) {
        AnonymousClass619 r2;
        Object obj;
        AnonymousClass1V8 A0E;
        String A0H = r25.A0H("step");
        r25.A0I("editable_after_submission", null);
        AnonymousClass1V8 A0E2 = r25.A0E("supported_documents");
        List A0J = r25.A0J("question");
        if (A0E2 == null || (A0E = A0E2.A0E("text_entity")) == null) {
            r2 = null;
        } else {
            r2 = AnonymousClass619.A00(A0E);
        }
        ArrayList A0l = C12960it.A0l();
        try {
            Iterator it = A0J.iterator();
            while (it.hasNext()) {
                AnonymousClass1V8 A0d = C117305Zk.A0d(it);
                boolean equals = "true".equals(A0d.A0H("is_optional"));
                boolean equals2 = "true".equals(A0d.A0H("is_editable"));
                String A0H2 = A0d.A0H("title");
                String A0H3 = A0d.A0H("question_type");
                String A0W = C117295Zj.A0W(A0d, "format");
                if (A0W != null) {
                    char c = 65535;
                    switch (A0W.hashCode()) {
                        case -1237104919:
                            if (A0W.equals("KycDateQuestion")) {
                                c = 0;
                                break;
                            }
                            break;
                        case 102280552:
                            if (A0W.equals("KycTextQuestion")) {
                                c = 1;
                                break;
                            }
                            break;
                        case 620160460:
                            if (A0W.equals("KycDropdownQuestion")) {
                                c = 2;
                                break;
                            }
                            break;
                    }
                    switch (c) {
                        case 0:
                            AnonymousClass1V8 A0E3 = A0d.A0E("prefilled_value");
                            AnonymousClass1V8 A0E4 = A0d.A0E("date_range");
                            C129825yM r11 = null;
                            AnonymousClass60L A00 = A0E3 != null ? AnonymousClass60L.A00(A0E3) : null;
                            if (A0E4 != null) {
                                r11 = new C129825yM(AnonymousClass60L.A00(A0E4.A0F("min_date")), AnonymousClass60L.A00(A0E4.A0F("max_date")));
                            }
                            obj = new C121285hb(r11, A00, A0H2, A0W, A0H3, equals, equals2);
                            break;
                        case 1:
                            obj = new C121275ha(A0H2, A0W, A0H3, C117295Zj.A0W(A0d, "prefilled_value"), equals, equals2);
                            break;
                        case 2:
                            String A0W2 = C117295Zj.A0W(A0d, "preselected_value");
                            List A0J2 = A0d.A0J("option");
                            ArrayList A0l2 = C12960it.A0l();
                            try {
                                Iterator it2 = A0J2.iterator();
                                while (it2.hasNext()) {
                                    AnonymousClass1V8 A0d2 = C117305Zk.A0d(it2);
                                    String A0W3 = C117295Zj.A0W(A0d2, "text");
                                    A0d2.A0H("value");
                                    A0l2.add(new C125745rk(A0W3));
                                }
                            } catch (AnonymousClass1V9 unused) {
                                Log.e("PAY: KycQuestionStep/parseQuestions failed.");
                            }
                            obj = new C121295hc(A0H2, A0W, A0H3, A0W2, A0l2, equals, equals2);
                            break;
                        default:
                            throw C12980iv.A0u(String.format("PAY: KycQuestionStep/build %s not supported", A0W));
                    }
                    A0l.add(obj);
                }
            }
        } catch (AnonymousClass1V9 unused2) {
            Log.e("PAY: KycQuestionStep/parseQuestions failed.");
        }
        return new C121385hl(r2, A0H, A0l);
    }
}
