package X;

/* renamed from: X.4wf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107024wf implements AbstractC117075Yd {
    public final long A00;
    public final long A01;
    public final long[] A02;
    public final long[] A03;

    @Override // X.AnonymousClass5WY
    public boolean AK2() {
        return true;
    }

    public C107024wf(long[] jArr, long[] jArr2, long j, long j2) {
        this.A03 = jArr;
        this.A02 = jArr2;
        this.A01 = j;
        this.A00 = j2;
    }

    @Override // X.AbstractC117075Yd
    public long ACO() {
        return this.A00;
    }

    @Override // X.AnonymousClass5WY
    public long ACc() {
        return this.A01;
    }

    @Override // X.AnonymousClass5WY
    public C92684Xa AGX(long j) {
        long[] jArr = this.A03;
        int A06 = AnonymousClass3JZ.A06(jArr, j, true);
        long j2 = jArr[A06];
        long[] jArr2 = this.A02;
        C94324bc r4 = new C94324bc(j2, jArr2[A06]);
        if (r4.A01 >= j || A06 == jArr.length - 1) {
            return new C92684Xa(r4, r4);
        }
        int i = A06 + 1;
        return C92684Xa.A00(r4, jArr[i], jArr2[i]);
    }

    @Override // X.AbstractC117075Yd
    public long AHD(long j) {
        return this.A03[AnonymousClass3JZ.A06(this.A02, j, true)];
    }
}
