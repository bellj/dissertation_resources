package X;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;

/* renamed from: X.2kc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56272kc extends AnonymousClass2RP {
    public static final AbstractC77683ng A00;
    public static final AnonymousClass4DN A01;
    public static final AnonymousClass1UE A02;

    static {
        AnonymousClass4DN r3 = new AnonymousClass4DN();
        A01 = r3;
        C77553nT r2 = new C77553nT();
        A00 = r2;
        A02 = new AnonymousClass1UE(r2, r3, "AccountTransfer.ACCOUNT_TRANSFER_API");
    }

    public C56272kc(Activity activity) {
        super(activity, activity, null, A02, new C93404a7(Looper.getMainLooper(), new C108294yp()));
    }

    public C56272kc(Context context) {
        super(null, context, null, A02, new C93404a7(Looper.getMainLooper(), new C108294yp()));
    }
}
