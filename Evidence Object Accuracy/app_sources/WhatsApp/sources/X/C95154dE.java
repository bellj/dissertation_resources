package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4dE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C95154dE {
    public static final ClassLoader A00 = C95154dE.class.getClassLoader();

    public static void A00(Parcel parcel, Parcelable parcelable) {
        if (parcelable == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcelable.writeToParcel(parcel, 0);
    }
}
