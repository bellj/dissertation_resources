package X;

import com.whatsapp.biz.product.view.activity.ProductDetailActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3zA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84353zA extends AnonymousClass2Cu {
    public final /* synthetic */ ProductDetailActivity A00;

    public C84353zA(ProductDetailActivity productDetailActivity) {
        this.A00 = productDetailActivity;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        C53852fO r0;
        ProductDetailActivity productDetailActivity = this.A00;
        if (productDetailActivity.A0h.equals(userJid) && (r0 = ((AnonymousClass283) productDetailActivity).A0M) != null) {
            r0.A05();
        }
    }
}
