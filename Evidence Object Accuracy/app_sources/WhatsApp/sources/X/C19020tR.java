package X;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteBindOrColumnIndexOutOfRangeException;
import android.database.sqlite.SQLiteBlobTooBigException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatatypeMismatchException;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOutOfMemoryException;
import com.whatsapp.util.Log;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.0tR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C19020tR extends AbstractC18500sY implements AbstractC19010tQ {
    public final C15240mn A00;
    public final Map A01 = new HashMap();

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AM5() {
    }

    @Override // X.AbstractC19010tQ
    public /* synthetic */ void AND() {
    }

    public C19020tR(C15240mn r3, C18480sW r4) {
        super(r4, "message_fts", Integer.MIN_VALUE);
        this.A00 = r3;
    }

    @Override // X.AbstractC18500sY
    public AnonymousClass2Ez A09(Cursor cursor) {
        boolean z;
        try {
            z = true;
            if (cursor.getCount() != 1) {
                z = false;
            }
        } catch (SQLiteBindOrColumnIndexOutOfRangeException | SQLiteBlobTooBigException | SQLiteConstraintException | SQLiteDatatypeMismatchException | SQLiteFullException | SQLiteOutOfMemoryException unused) {
        }
        try {
            C15240mn r8 = this.A00;
            Map map = this.A01;
            C34981h1 A00 = C34981h1.A00(-5);
            int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
            int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow("chat_row_id");
            int i = 0;
            while (cursor.moveToNext()) {
                AbstractC15340mz A0A = r8.A0A(cursor, columnIndexOrThrow2, columnIndexOrThrow);
                if (A0A == null) {
                    i++;
                    A00 = new C34981h1(-6, Long.MIN_VALUE, cursor.getLong(columnIndexOrThrow));
                } else {
                    A00 = r8.A07(A0A, map, 5, true);
                    if (A00.A00 == -6) {
                        A00 = new C34981h1(1, A0A.A12, A0A.A11);
                    }
                    r8.A05.A05(A0A.A0z);
                    i++;
                }
            }
            return new AnonymousClass2Ez(A00.A01, i);
        } catch (SQLiteBindOrColumnIndexOutOfRangeException | SQLiteBlobTooBigException | SQLiteConstraintException | SQLiteDatatypeMismatchException | SQLiteFullException | SQLiteOutOfMemoryException unused2) {
            if (z) {
                Log.e("FtsDatabaseMigration/skipping single row");
            }
            return new AnonymousClass2Ez(-1, 0);
        }
    }

    @Override // X.AbstractC18500sY
    public void A0H() {
        super.A0H();
        this.A06.A05("fts_ready", 5);
    }

    @Override // X.AbstractC18500sY
    public void A0I() {
        C15240mn r4 = this.A00;
        C28181Ma r3 = new C28181Ma("FtsMessageStore/optimize");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("messages_fts", "optimize");
        C16490p7 r0 = r4.A0C;
        r0.A04();
        r0.A05.AHr().A02(contentValues, "messages_fts");
        r3.A01();
    }

    @Override // X.AbstractC18500sY
    public boolean A0U(SQLException sQLException, int i) {
        if (i != 1 || !(sQLException instanceof SQLiteBlobTooBigException)) {
            return super.A0U(sQLException, i);
        }
        Log.e("FtsDatabaseMigration/skipping BlobTooBigException single row");
        return true;
    }

    /* JADX INFO: finally extract failed */
    @Override // X.AbstractC19010tQ
    public void onRollback() {
        AnonymousClass009.A0F(false);
        C28181Ma r6 = new C28181Ma("FtsMessageStore/reset");
        C15240mn r0 = this.A00;
        r0.A0J();
        C16490p7 r9 = r0.A0C;
        C16310on A02 = r9.A02();
        try {
            r9.A04();
            C29561To r8 = r9.A05;
            C28181Ma r10 = new C28181Ma("databasehelper/createFtsTable");
            AnonymousClass1Lx A00 = A02.A00();
            C16330op AHr = r8.AHr();
            C29741Um.A00(AHr);
            C16330op r2 = A02.A03;
            r8.A0D(AHr, r8.A06(r2).booleanValue(), r8.A0F(r2));
            A00.A00();
            A00.close();
            StringBuilder sb = new StringBuilder("databasehelper/createFtsDeprecatedTable time spent:");
            sb.append(r10.A01());
            Log.i(sb.toString());
            A02.close();
            A02 = r9.A02();
            try {
                r9.A04();
                C28181Ma r4 = new C28181Ma("databasehelper/createFtsDeprecatedTable");
                AnonymousClass1Lx A002 = A02.A00();
                C16330op r22 = A02.A03;
                r22.A0B("CREATE VIRTUAL TABLE messages_fts USING FTS3()");
                r8.A0D(r22, r8.A06(r22).booleanValue(), r8.A0F(r22));
                A002.A00();
                A002.close();
                StringBuilder sb2 = new StringBuilder("databasehelper/createFtsDeprecatedTable time spent:");
                sb2.append(r4.A01());
                Log.i(sb2.toString());
                A02.close();
                r6.A01();
            } finally {
                try {
                    A02.close();
                } catch (Throwable unused) {
                }
            }
        } catch (Throwable th) {
            throw th;
        }
    }
}
