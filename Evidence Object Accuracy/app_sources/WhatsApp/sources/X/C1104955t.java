package X;

import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.55t  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1104955t implements AbstractC32851cq {
    public final /* synthetic */ ActivityC000900k A00;
    public final /* synthetic */ AnonymousClass1AL A01;

    public C1104955t(ActivityC000900k r1, AnonymousClass1AL r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        C1105155v.A00(this.A00);
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        ActivityC000900k r4 = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_record_audio_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_record_audio;
        }
        RequestPermissionActivity.A0K(r4, R.string.permission_storage_need_write_access_on_record_audio_request, i2);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        C1105155v.A00(this.A00);
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        ActivityC000900k r4 = this.A00;
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_record_audio_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_record_audio;
        }
        RequestPermissionActivity.A0K(r4, R.string.permission_storage_need_write_access_on_record_audio_request, i2);
    }
}
