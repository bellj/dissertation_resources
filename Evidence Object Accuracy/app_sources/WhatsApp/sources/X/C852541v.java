package X;

/* renamed from: X.41v  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C852541v extends C71333cl {
    public C852541v(C15610nY r1, AnonymousClass018 r2) {
        super(r1, r2);
    }

    @Override // X.C71333cl
    public int A00(C15370n3 r5, C15370n3 r6) {
        boolean z = false;
        boolean A1W = C12960it.A1W(r5.A0C);
        if (r6.A0C != null) {
            z = true;
        }
        if (A1W == z) {
            return super.compare(r5, r6);
        }
        if (A1W) {
            return -1;
        }
        return 1;
    }
}
