package X;

import java.util.List;

/* renamed from: X.3iH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74543iH extends AnonymousClass0Q0 {
    public List A00;
    public List A01;

    public C74543iH(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        return this.A01.get(i).equals(this.A00.get(i2));
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        return ((C64363Fg) this.A01.get(i)).A0S.equals(((C64363Fg) this.A00.get(i2)).A0S);
    }
}
