package X;

import android.content.res.Resources;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import com.whatsapp.R;
import com.whatsapp.calling.controls.viewmodel.ParticipantsListViewModel;
import com.whatsapp.calling.views.VoipCallControlRingingDotsIndicator;
import com.whatsapp.components.Button;

/* renamed from: X.2wE  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C60212wE extends AbstractC75743kL {
    public C849140h A00;
    public final View A01;
    public final ImageView A02;
    public final ImageView A03;
    public final C28801Pb A04;
    public final VoipCallControlRingingDotsIndicator A05;
    public final Button A06;
    public final AnonymousClass28F A07;
    public final AnonymousClass1J1 A08;
    public final AnonymousClass01d A09;
    public final Runnable A0A = new RunnableBRunnable0Shape14S0100000_I1(this, 46);

    public C60212wE(View view, ParticipantsListViewModel participantsListViewModel, C15610nY r5, AnonymousClass28F r6, AnonymousClass1J1 r7, AnonymousClass01d r8, AnonymousClass12F r9) {
        super(view, participantsListViewModel);
        this.A01 = AnonymousClass028.A0D(view, R.id.name);
        this.A09 = r8;
        this.A07 = r6;
        this.A08 = r7;
        this.A04 = new C28801Pb(view, r5, r9, (int) R.id.name);
        this.A02 = C12970iu.A0K(view, R.id.avatar);
        this.A03 = C12970iu.A0K(view, R.id.connect_icon);
        this.A06 = (Button) AnonymousClass028.A0D(view, R.id.ring_btn);
        this.A05 = (VoipCallControlRingingDotsIndicator) AnonymousClass028.A0D(view, R.id.ringing_dots);
    }

    public static void A00(View view, float f, float f2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2);
        alphaAnimation.setInterpolator(AnonymousClass0L1.A00(0.0f, 0.0f, 0.6f, 1.0f));
        alphaAnimation.setDuration(750);
        alphaAnimation.setRepeatCount(1);
        alphaAnimation.setRepeatMode(2);
        view.startAnimation(alphaAnimation);
    }

    @Override // X.AnonymousClass03U
    public boolean A07() {
        return C12960it.A1W(this.A00);
    }

    @Override // X.AbstractC75743kL
    public void A08() {
        this.A01.clearAnimation();
        this.A02.clearAnimation();
        VoipCallControlRingingDotsIndicator voipCallControlRingingDotsIndicator = this.A05;
        voipCallControlRingingDotsIndicator.clearAnimation();
        this.A00 = null;
        voipCallControlRingingDotsIndicator.removeCallbacks(this.A0A);
    }

    @Override // X.AbstractC75743kL
    public void A09(C92194Ux r19) {
        Resources resources;
        int i;
        boolean z = r19 instanceof C849140h;
        AnonymousClass009.A0A("Unknown list item type", z);
        if (z) {
            VoipCallControlRingingDotsIndicator voipCallControlRingingDotsIndicator = this.A05;
            voipCallControlRingingDotsIndicator.removeCallbacks(this.A0A);
            this.A00 = (C849140h) r19;
            View view = this.A0H;
            AnonymousClass028.A0g(view, null);
            view.setClickable(false);
            C849140h r3 = this.A00;
            if (!(r3.A00 == 11 && r3.A04 && this.A06.getVisibility() == 0)) {
                this.A06.setVisibility(8);
            }
            ImageView imageView = this.A03;
            imageView.setVisibility(8);
            voipCallControlRingingDotsIndicator.setVisibility(8);
            View view2 = this.A01;
            view2.clearAnimation();
            ImageView imageView2 = this.A02;
            imageView2.clearAnimation();
            voipCallControlRingingDotsIndicator.clearAnimation();
            view.setTag(this.A00.A02);
            C849140h r0 = this.A00;
            if (r0 != null) {
                this.A08.A02(imageView2, this.A07, r0.A01, true);
            }
            C849140h r1 = this.A00;
            if (r1.A03) {
                this.A04.A02();
                imageView2.setOnClickListener(null);
                AnonymousClass028.A0a(imageView2, 2);
                return;
            }
            boolean z2 = r1.A04;
            C28801Pb r7 = this.A04;
            r7.A06(r1.A01);
            int i2 = r1.A00;
            if (i2 == 1) {
                view2.setAlpha(1.0f);
                imageView2.setAlpha(1.0f);
                if (!z2) {
                    AnimationSet animationSet = new AnimationSet(false);
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration(500);
                    ScaleAnimation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation.setDuration(500);
                    scaleAnimation.setInterpolator(AnonymousClass0L1.A00(0.2f, 1.65f, 0.55f, 1.0f));
                    animationSet.addAnimation(scaleAnimation);
                    animationSet.addAnimation(alphaAnimation);
                    imageView.startAnimation(animationSet);
                    imageView.setVisibility(0);
                }
                resources = view.getResources();
                i = R.string.voip_joinable_connected_participant_description;
            } else if (i2 == 11 || !z2) {
                imageView2.setAlpha(0.3f);
                view2.setAlpha(0.3f);
                if (z2) {
                    A0A();
                    return;
                } else {
                    resources = view.getResources();
                    i = R.string.voip_joinable_invited_participant_description;
                }
            } else {
                A0B();
                return;
            }
            view.setContentDescription(C12990iw.A0o(resources, r7.A01.getText(), new Object[1], 0, i));
        }
    }

    public final void A0A() {
        this.A02.setAlpha(0.3f);
        this.A01.setAlpha(0.3f);
        this.A05.setVisibility(8);
        View view = this.A06;
        if (view.getVisibility() != 0) {
            view.setVisibility(0);
            view.setAlpha(0.0f);
            view.animate().setDuration(500).alpha(1.0f).start();
        }
        if (AnonymousClass23N.A05(this.A09.A0P())) {
            view = this.A0H;
        }
        AbstractView$OnClickListenerC34281fs.A00(view, this, 38);
        View view2 = this.A0H;
        C65293Iy.A03(view2, C12990iw.A0o(view2.getResources(), this.A04.A01.getText(), C12970iu.A1b(), 0, R.string.voip_joinable_invited_participant_with_ring_button_description), null);
    }

    public final void A0B() {
        this.A06.setVisibility(8);
        VoipCallControlRingingDotsIndicator voipCallControlRingingDotsIndicator = this.A05;
        voipCallControlRingingDotsIndicator.setVisibility(0);
        View view = this.A0H;
        view.setContentDescription(C12990iw.A0o(view.getResources(), this.A04.A01.getText(), C12970iu.A1b(), 0, R.string.voip_joinable_ringing_participant_description));
        ParticipantsListViewModel participantsListViewModel = ((AbstractC75743kL) this).A00;
        if (!(participantsListViewModel == null || participantsListViewModel.A05() == null)) {
            voipCallControlRingingDotsIndicator.postDelayed(new RunnableBRunnable0Shape14S0100000_I1(this, 47), 2000);
        }
        C52602bN r1 = new C52602bN(voipCallControlRingingDotsIndicator);
        r1.setRepeatCount(-1);
        r1.setAnimationListener(new C83433xE(this));
        voipCallControlRingingDotsIndicator.startAnimation(r1);
    }
}
