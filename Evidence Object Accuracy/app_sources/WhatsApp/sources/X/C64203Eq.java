package X;

import java.util.List;

/* renamed from: X.3Eq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64203Eq {
    public List A00;
    public boolean A01;
    public final int A02;
    public final int A03;
    public final int A04;
    public final int A05 = 100;
    public final int A06;
    public final long A07;
    public final C30031Vu A08;
    public final AbstractC15340mz A09;
    public final boolean A0A;

    public C64203Eq(C30031Vu r2, AbstractC15340mz r3, List list, int i, int i2, int i3, int i4, long j, boolean z, boolean z2) {
        this.A0A = z;
        this.A02 = i;
        this.A06 = i2;
        this.A09 = r3;
        this.A07 = j;
        this.A04 = i3;
        this.A03 = i4;
        this.A08 = r2;
        this.A00 = list;
        this.A01 = z2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            C64203Eq r7 = (C64203Eq) obj;
            if (!(this.A0A == r7.A0A && this.A02 == r7.A02 && this.A06 == r7.A06 && this.A05 == r7.A05 && this.A07 == r7.A07 && this.A04 == r7.A04 && this.A03 == r7.A03 && this.A09.equals(r7.A09) && C29941Vi.A00(this.A08, r7.A08) && this.A00.equals(r7.A00) && this.A01 == r7.A01)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Object[] objArr = new Object[11];
        objArr[0] = Boolean.valueOf(this.A0A);
        C12980iv.A1T(objArr, this.A02);
        C12990iw.A1V(objArr, this.A06);
        objArr[3] = this.A09;
        objArr[4] = Integer.valueOf(this.A05);
        objArr[5] = Long.valueOf(this.A07);
        objArr[6] = Integer.valueOf(this.A04);
        objArr[7] = Integer.valueOf(this.A03);
        objArr[8] = this.A08;
        objArr[9] = this.A00;
        return C12980iv.A0B(Boolean.valueOf(this.A01), objArr, 10);
    }
}
