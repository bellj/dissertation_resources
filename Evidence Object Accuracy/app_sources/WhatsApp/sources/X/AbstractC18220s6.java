package X;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.whatsapp.AlarmBroadcastReceiver;

/* renamed from: X.0s6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC18220s6 {
    public final Context A00;

    public AbstractC18220s6(Context context) {
        this.A00 = context;
    }

    public PendingIntent A00(String str, int i) {
        Context context = this.A00;
        return AnonymousClass1UY.A01(context, 0, new Intent(str, null, context, AlarmBroadcastReceiver.class), i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:64:0x018d A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(android.content.Intent r17) {
        /*
        // Method dump skipped, instructions count: 516
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC18220s6.A01(android.content.Intent):void");
    }
}
