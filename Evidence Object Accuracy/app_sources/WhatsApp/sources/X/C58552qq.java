package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaFrameLayout;
import com.whatsapp.WaImageView;
import com.whatsapp.WaTextView;

/* renamed from: X.2qq  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58552qq extends WaFrameLayout {
    public final TextEmojiLabel A00 = C12970iu.A0T(this, R.id.subtitle);
    public final TextEmojiLabel A01 = C12970iu.A0T(this, R.id.header_title);
    public final WaImageView A02 = C12980iv.A0X(this, R.id.thumbnail);
    public final WaTextView A03 = C12960it.A0N(this, R.id.surface);

    public C58552qq(Context context) {
        super(context, null);
        LayoutInflater.from(context).inflate(R.layout.conversation_row_header_shop_storefront, (ViewGroup) this, true);
    }
}
