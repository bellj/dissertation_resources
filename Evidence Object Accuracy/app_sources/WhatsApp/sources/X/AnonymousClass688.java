package X;

import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.688  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass688 implements AnonymousClass5W2 {
    public final C18790t3 A00;
    public final C16590pI A01;
    public final C1329668y A02;
    public final C21860y6 A03;
    public final C18660so A04;
    public final C17070qD A05;

    public AnonymousClass688(C18790t3 r1, C16590pI r2, C1329668y r3, C21860y6 r4, C18660so r5, C17070qD r6) {
        this.A01 = r2;
        this.A00 = r1;
        this.A05 = r6;
        this.A03 = r4;
        this.A02 = r3;
        this.A04 = r5;
    }

    @Override // X.AnonymousClass5W2
    public void A5s(List list) {
        C32641cU[] r2;
        int length;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1ZY r1 = C117305Zk.A0H(it).A08;
            if (r1 instanceof C119755f3) {
                if (C12970iu.A1Y(((C119755f3) r1).A05.A00)) {
                    this.A03.A07("2fa");
                }
            } else if (r1 instanceof C119795f7) {
                C119795f7 r12 = (C119795f7) r1;
                if (!TextUtils.isEmpty(r12.A02) && !AnonymousClass1ZS.A02(r12.A00) && (length = (r2 = C17930rd.A0E.A0B).length) > 0) {
                    this.A04.A06(r2[length - 1]);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0102, code lost:
        if (r0 != null) goto L_0x0104;
     */
    @Override // X.AnonymousClass5W2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC28901Pl A6M(X.AbstractC28901Pl r9) {
        /*
        // Method dump skipped, instructions count: 472
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass688.A6M(X.1Pl):X.1Pl");
    }
}
