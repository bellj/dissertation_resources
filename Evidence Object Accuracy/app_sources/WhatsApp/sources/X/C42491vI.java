package X;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.redex.ViewOnClickCListenerShape0S0200000_I0;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.Set;

/* renamed from: X.1vI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C42491vI extends AbstractC42501vJ {
    public C16590pI A00;
    public C21860y6 A01;
    public AbstractC38141na A02;
    public AnonymousClass14X A03;
    public C22590zK A04;
    public final FrameLayout A05;
    public final ImageView A06;
    public final TextEmojiLabel A07 = ((TextEmojiLabel) findViewById(R.id.get_started));
    public final TextEmojiLabel A08 = ((TextEmojiLabel) AnonymousClass028.A0D(this, R.id.invite_description));
    public final C69973aX A09;

    public C42491vI(Context context, AbstractC13890kV r7, AbstractC15340mz r8) {
        super(context, r7, r8);
        C69973aX r0;
        FrameLayout frameLayout = (FrameLayout) AnonymousClass028.A0D(this, R.id.payment_container);
        this.A05 = frameLayout;
        this.A06 = (ImageView) AnonymousClass028.A0D(this, R.id.payment_brand_logo);
        ViewStub viewStub = (ViewStub) AnonymousClass028.A0D(this, R.id.payment_invite_right_view_stub);
        frameLayout.setForeground(getInnerFrameForegroundDrawable());
        if (this.A19.A07()) {
            this.A02 = this.A1A.A02().AFL();
        }
        AbstractC38141na r02 = this.A02;
        C16590pI r3 = this.A00;
        AbstractC14440lR r2 = this.A1P;
        C22590zK r1 = this.A04;
        if (r02 != null) {
            r0 = r02.ADb(r3, r1, r2);
        } else {
            r0 = new C69973aX(r3, r1, r2);
        }
        this.A09 = r0;
        C88094Eg.A00(viewStub, r0);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A0s() {
        A1H(false);
        A1M();
    }

    @Override // X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r3, boolean z) {
        boolean z2 = false;
        if (r3 != getFMessage()) {
            z2 = true;
        }
        super.A1D(r3, z);
        if (z || z2) {
            A1M();
        }
    }

    public final void A1M() {
        AnonymousClass4S0 r3;
        Intent AAY;
        int ADZ;
        this.A08.setText(getInviteContext());
        AbstractC38141na r5 = this.A02;
        if (r5 != null) {
            r3 = r5.ADa();
        } else {
            r3 = new AnonymousClass4S0(null, null, R.drawable.payment_invite_bubble_icon, false);
        }
        this.A09.A6Q(new AnonymousClass4OZ(2, r3));
        if (!(r5 == null || (ADZ = r5.ADZ()) == -1)) {
            ImageView imageView = this.A06;
            imageView.setVisibility(0);
            imageView.setImageResource(ADZ);
        }
        TextEmojiLabel textEmojiLabel = this.A07;
        if (textEmojiLabel == null) {
            return;
        }
        if (!this.A19.A07() || r5 == null || (AAY = r5.AAY(getFMessage())) == null) {
            textEmojiLabel.setVisibility(8);
            return;
        }
        textEmojiLabel.setVisibility(0);
        textEmojiLabel.setOnClickListener(new ViewOnClickCListenerShape0S0200000_I0(this, 25, AAY));
    }

    @Override // X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_payment_invite_left;
    }

    @Override // X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_payment_invite_left;
    }

    @Override // X.AnonymousClass1OY
    public Set getInnerFrameLayouts() {
        Set innerFrameLayouts = super.getInnerFrameLayouts();
        innerFrameLayouts.add(this.A05);
        return innerFrameLayouts;
    }

    private CharSequence getInviteContext() {
        AbstractC15340mz fMessage = getFMessage();
        AnonymousClass14X r3 = this.A03;
        Context context = getContext();
        AnonymousClass1IS r0 = fMessage.A0z;
        boolean z = r0.A02;
        AbstractC14640lm r02 = r0.A00;
        AnonymousClass009.A05(r02);
        C38151nb A0B = r3.A0B(context, r02, z);
        String str = A0B.A00;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        String str2 = A0B.A01;
        int indexOf = str.indexOf(str2);
        spannableStringBuilder.setSpan(new C52292aZ(getContext()), indexOf, str2.length() + indexOf, 0);
        return spannableStringBuilder;
    }

    @Override // X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_payment_invite_right;
    }
}
