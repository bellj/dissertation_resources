package X;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import androidx.window.layout.WindowInfoTrackerImpl$windowLayoutInfo$1;
import java.util.Iterator;

/* renamed from: X.2Iz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49062Iz implements AnonymousClass2ID {
    public View A00;
    public View A01;
    public AnonymousClass0PZ A02;
    public boolean A03;
    public boolean A04;
    public final Activity A05;
    public final AnonymousClass016 A06 = new AnonymousClass016();
    public final AnonymousClass016 A07 = new AnonymousClass016();
    public final C07550Zd A08;
    public final C103934rY A09;
    public final C14820m6 A0A;

    public C49062Iz(ActivityC000900k r4, C14820m6 r5) {
        this.A05 = r4;
        this.A0A = r5;
        this.A08 = new C07550Zd(AbstractC12760iS.A00.A02(r4));
        this.A09 = new C103934rY(this);
        SharedPreferences sharedPreferences = r5.A00;
        this.A03 = sharedPreferences.getBoolean("detect_device_foldable", false);
        this.A04 = sharedPreferences.getBoolean("detect_device_foldable_bookmode", false);
    }

    public final void A00(View view, AnonymousClass016 r13) {
        AnonymousClass3HT r2;
        AnonymousClass0S8 r1;
        if (this.A02 != null && view != null) {
            if (Build.VERSION.SDK_INT < 24 || !this.A05.isInPictureInPictureMode()) {
                Iterator it = this.A02.A00.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    AbstractC11390gD r3 = (AbstractC11390gD) it.next();
                    if (r3 instanceof AbstractC12840ie) {
                        AbstractC12840ie r32 = (AbstractC12840ie) r3;
                        if (r32 != null) {
                            View view2 = (View) view.getParent();
                            if (view2.getHeight() != 0) {
                                if (!this.A03) {
                                    this.A0A.A00.edit().putBoolean("detect_device_foldable", true).apply();
                                    this.A03 = true;
                                }
                                if (!this.A04) {
                                    C05400Pk r6 = ((AnonymousClass0ZZ) r32).A00;
                                    if (r6.A02 - r6.A01 > r6.A00 - r6.A03) {
                                        r1 = AnonymousClass0S8.A01;
                                    } else {
                                        r1 = AnonymousClass0S8.A02;
                                    }
                                    if (r1 == AnonymousClass0S8.A02) {
                                        this.A0A.A00.edit().putBoolean("detect_device_foldable_bookmode", true).apply();
                                        this.A04 = true;
                                    }
                                }
                                AnonymousClass0ZZ r33 = (AnonymousClass0ZZ) r32;
                                AnonymousClass0SA r12 = r33.A02;
                                if (C16700pc.A0O(r12, AnonymousClass0SA.A02) || (C16700pc.A0O(r12, AnonymousClass0SA.A01) && C16700pc.A0O(r33.A01, AnonymousClass0S9.A02))) {
                                    int[] iArr = new int[2];
                                    view2.getLocationOnScreen(iArr);
                                    int i = iArr[0];
                                    Rect rect = new Rect(i, iArr[1], i + view2.getWidth(), iArr[1] + view2.getHeight());
                                    C05400Pk r0 = r33.A00;
                                    Rect rect2 = new Rect(new Rect(r0.A01, r0.A03, r0.A02, r0.A00));
                                    boolean intersect = rect2.intersect(rect);
                                    if (!(rect2.width() == 0 && rect2.height() == 0) && intersect) {
                                        rect2.offset(-iArr[0], -iArr[1]);
                                        if (rect2.left == 0) {
                                            r2 = new AnonymousClass3HT(new Point(view2.getWidth(), view2.getHeight()), rect2, 2);
                                        } else if (rect2.top == 0) {
                                            r2 = new AnonymousClass3HT(new Point(view2.getWidth(), view2.getHeight()), rect2, 1);
                                        } else {
                                            return;
                                        }
                                    }
                                }
                            } else {
                                return;
                            }
                        }
                    }
                }
            }
            r2 = AnonymousClass3HT.A03;
            Object A01 = r13.A01();
            if (!(A01 == null && r2.A00 == 0) && !C29941Vi.A00(A01, r2)) {
                r13.A0A(r2);
            }
        }
    }

    @Override // X.AnonymousClass2ID
    public AnonymousClass017 AB7() {
        return this.A06;
    }

    @Override // X.AnonymousClass2ID
    public AnonymousClass017 AB8() {
        return this.A07;
    }

    @Override // X.AnonymousClass2ID
    public void AWH(View view) {
        this.A01 = view;
        C07550Zd r6 = this.A08;
        Activity activity = this.A05;
        ExecutorC10620ev r4 = ExecutorC10620ev.A00;
        C103934rY r3 = this.A09;
        C16700pc.A0E(activity, 0);
        r6.A03(r3, r4, C88254Ew.A00(new WindowInfoTrackerImpl$windowLayoutInfo$1(activity, (C07540Zc) r6.A00, null)));
    }

    @Override // X.AnonymousClass2ID
    public void AWn() {
        this.A08.A02(this.A09);
    }

    @Override // X.AnonymousClass2ID
    public void Abq(View view) {
        this.A00 = view;
        A00(view, this.A06);
    }

    @Override // X.AnonymousClass2ID
    public void onGlobalLayout() {
        A00(this.A01, this.A07);
        A00(this.A00, this.A06);
    }
}
