package X;

import android.animation.ValueAnimator;
import android.view.ViewPropertyAnimator;

/* renamed from: X.0Kv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C04220Kv {
    public static void A00(ValueAnimator.AnimatorUpdateListener animatorUpdateListener, ViewPropertyAnimator viewPropertyAnimator) {
        viewPropertyAnimator.setUpdateListener(animatorUpdateListener);
    }
}
