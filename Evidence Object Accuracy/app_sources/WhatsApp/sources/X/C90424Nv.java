package X;

import java.util.Collections;
import java.util.List;

/* renamed from: X.4Nv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C90424Nv {
    public final List A00;
    public final boolean A01;

    public C90424Nv(List list) {
        this.A01 = C12960it.A1W(list);
        this.A00 = list == null ? Collections.emptyList() : list;
    }
}
