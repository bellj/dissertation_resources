package X;

import android.util.Log;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.PriorityQueue;

/* renamed from: X.4xw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC107784xw implements AbstractC117065Yc {
    public long A00;
    public long A01;
    public C77263n0 A02;
    public final ArrayDeque A03 = new ArrayDeque();
    public final ArrayDeque A04;
    public final PriorityQueue A05;

    @Override // X.AnonymousClass5XF
    public void release() {
    }

    public AbstractC107784xw() {
        int i = 0;
        int i2 = 0;
        do {
            this.A03.add(new C77263n0());
            i2++;
        } while (i2 < 10);
        this.A04 = new ArrayDeque();
        do {
            this.A04.add(new C77283n2(new AnonymousClass5SI() { // from class: X.4vx
                @Override // X.AnonymousClass5SI
                public final void Aa8(AbstractC76693m3 r2) {
                    AbstractC107784xw r0 = AbstractC107784xw.this;
                    r2.clear();
                    r0.A04.add(r2);
                }
            }));
            i++;
        } while (i < 2);
        this.A05 = new PriorityQueue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0036, code lost:
        r4.clear();
        r8.A03.add(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        return r5;
     */
    /* renamed from: A00 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.AbstractC76773mC A8o() {
        /*
            r8 = this;
            java.util.ArrayDeque r6 = r8.A04
            boolean r0 = r6.isEmpty()
            r5 = 0
            if (r0 == 0) goto L_0x000a
        L_0x0009:
            return r5
        L_0x000a:
            java.util.PriorityQueue r7 = r8.A05
            boolean r0 = r7.isEmpty()
            if (r0 != 0) goto L_0x0009
            java.lang.Object r0 = r7.peek()
            X.3mA r0 = (X.C76763mA) r0
            long r3 = r0.A00
            long r1 = r8.A00
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 > 0) goto L_0x0009
            java.lang.Object r4 = r7.poll()
            X.3m8 r4 = (X.C76743m8) r4
            boolean r0 = X.AnonymousClass4YO.A00(r4)
            if (r0 == 0) goto L_0x003f
            java.lang.Object r5 = r6.pollFirst()
            X.3mC r5 = (X.AbstractC76773mC) r5
            r0 = 4
            r5.addFlag(r0)
        L_0x0036:
            r4.clear()
            java.util.ArrayDeque r0 = r8.A03
            r0.add(r4)
            return r5
        L_0x003f:
            r8.A01(r4)
            r3 = r8
            boolean r2 = r8 instanceof X.C77293n3
            if (r2 != 0) goto L_0x0077
            r0 = r3
            X.3n4 r0 = (X.C77303n4) r0
            java.util.List r1 = r0.A07
            java.util.List r0 = r0.A08
        L_0x004e:
            boolean r0 = X.C12960it.A1X(r1, r0)
            if (r0 == 0) goto L_0x007f
            if (r2 != 0) goto L_0x0070
            X.3n4 r3 = (X.C77303n4) r3
            java.util.List r0 = r3.A07
            r3.A08 = r0
        L_0x005c:
            X.4xn r2 = new X.4xn
            r2.<init>(r0)
            java.lang.Object r5 = r6.pollFirst()
            X.3mC r5 = (X.AbstractC76773mC) r5
            long r0 = r4.A00
            r5.timeUs = r0
            r5.A01 = r2
            r5.A00 = r0
            goto L_0x0036
        L_0x0070:
            X.3n3 r3 = (X.C77293n3) r3
            java.util.List r0 = r3.A04
            r3.A05 = r0
            goto L_0x005c
        L_0x0077:
            r0 = r3
            X.3n3 r0 = (X.C77293n3) r0
            java.util.List r1 = r0.A04
            java.util.List r0 = r0.A05
            goto L_0x004e
        L_0x007f:
            r4.clear()
            java.util.ArrayDeque r0 = r8.A03
            r0.add(r4)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC107784xw.A8o():X.3mC");
    }

    public void A01(C76743m8 r13) {
        AnonymousClass4R2 r4;
        byte[] bArr;
        int i;
        C77293n3 r5 = (C77293n3) this;
        ByteBuffer byteBuffer = r13.A01;
        byte[] array = byteBuffer.array();
        C95304dT r6 = r5.A08;
        r6.A0U(array, byteBuffer.limit());
        while (C95304dT.A00(r6) >= 3) {
            int A0C = r6.A0C() & 7;
            int i2 = A0C & 3;
            boolean z = false;
            boolean A1V = C12960it.A1V(A0C & 4, 4);
            byte A0C2 = (byte) r6.A0C();
            byte A0C3 = (byte) r6.A0C();
            if (i2 == 2 || i2 == 3) {
                if (A1V) {
                    if (i2 == 3) {
                        r5.A03();
                        int i3 = (A0C2 & 192) >> 6;
                        int i4 = r5.A01;
                        if (!(i4 == -1 || i3 == (i4 + 1) % 4)) {
                            r5.A04();
                            StringBuilder A0k = C12960it.A0k("Sequence number discontinuity. previous=");
                            A0k.append(r5.A01);
                            Log.w("Cea708Decoder", C12960it.A0e(" current=", A0k, i3));
                        }
                        r5.A01 = i3;
                        int i5 = A0C2 & 63;
                        if (i5 == 0) {
                            i5 = 64;
                        }
                        r4 = new AnonymousClass4R2(i3, i5);
                        r5.A03 = r4;
                        bArr = r4.A03;
                        i = r4.A00;
                    } else {
                        if (i2 == 2) {
                            z = true;
                        }
                        C95314dV.A03(z);
                        r4 = r5.A03;
                        if (r4 == null) {
                            Log.e("Cea708Decoder", "Encountered DTVCC_PACKET_DATA before DTVCC_PACKET_START");
                        } else {
                            bArr = r4.A03;
                            int i6 = r4.A00;
                            i = i6 + 1;
                            r4.A00 = i;
                            bArr[i6] = A0C2;
                        }
                    }
                    int i7 = i + 1;
                    r4.A00 = i7;
                    bArr[i] = A0C3;
                    if (i7 == (r4.A01 << 1) - 1) {
                        r5.A03();
                    }
                }
            }
        }
    }

    @Override // X.AnonymousClass5XF
    public /* bridge */ /* synthetic */ Object A8n() {
        C95314dV.A04(C12980iv.A1X(this.A02));
        ArrayDeque arrayDeque = this.A03;
        if (arrayDeque.isEmpty()) {
            return null;
        }
        C77263n0 r0 = (C77263n0) arrayDeque.pollFirst();
        this.A02 = r0;
        return r0;
    }

    @Override // X.AnonymousClass5XF
    public /* bridge */ /* synthetic */ void AZl(Object obj) {
        C76743m8 r5 = (C76743m8) obj;
        C95314dV.A03(C12970iu.A1Z(r5, this.A02));
        C77263n0 r52 = (C77263n0) r5;
        if (C12960it.A1V(r52.flags & Integer.MIN_VALUE, Integer.MIN_VALUE)) {
            r52.clear();
            this.A03.add(r52);
        } else {
            long j = this.A01;
            this.A01 = 1 + j;
            r52.A00 = j;
            this.A05.add(r52);
        }
        this.A02 = null;
    }

    @Override // X.AbstractC117065Yc
    public void Aca(long j) {
        this.A00 = j;
    }

    @Override // X.AnonymousClass5XF
    public void flush() {
        this.A01 = 0;
        this.A00 = 0;
        while (true) {
            PriorityQueue priorityQueue = this.A05;
            if (priorityQueue.isEmpty()) {
                break;
            }
            AnonymousClass4YO r1 = (AnonymousClass4YO) priorityQueue.poll();
            r1.clear();
            this.A03.add(r1);
        }
        C77263n0 r12 = this.A02;
        if (r12 != null) {
            r12.clear();
            this.A03.add(r12);
            this.A02 = null;
        }
    }

    @Override // X.AnonymousClass5XF
    public String getName() {
        return !(this instanceof C77293n3) ? "Cea608Decoder" : "Cea708Decoder";
    }
}
