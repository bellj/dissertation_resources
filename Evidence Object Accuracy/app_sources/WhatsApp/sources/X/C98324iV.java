package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.4iV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C98324iV implements IInterface {
    public final IBinder A00;
    public final String A01 = "com.google.android.gms.analytics.internal.IAnalyticsService";

    public C98324iV(IBinder iBinder) {
        this.A00 = iBinder;
    }

    @Override // android.os.IInterface
    public final IBinder asBinder() {
        return this.A00;
    }
}
