package X;

import android.util.Base64;

/* renamed from: X.18g  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C251418g {
    public final C14820m6 A00;

    public C251418g(C14820m6 r1) {
        this.A00 = r1;
    }

    public void A00(byte[] bArr) {
        if (bArr.length <= 256) {
            C14820m6 r1 = this.A00;
            r1.A00.edit().putString("routing_info", Base64.encodeToString(bArr, 3)).apply();
            return;
        }
        throw new IllegalArgumentException("The routing info should be smaller than 256 bytes.");
    }
}
