package X;

import android.app.Activity;
import android.os.Build;
import com.whatsapp.R;
import com.whatsapp.RequestPermissionActivity;

/* renamed from: X.249  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass249 implements AbstractC32851cq {
    public final Activity A00;
    public final C14950mJ A01;

    public AnonymousClass249(Activity activity, C14950mJ r2) {
        this.A00 = activity;
        this.A01 = r2;
    }

    @Override // X.AbstractC32851cq
    public void AUa(String str) {
        Activity activity = this.A00;
        if (!activity.isFinishing()) {
            AnonymousClass009.A05(activity);
            AbstractC13860kS r3 = (AbstractC13860kS) activity;
            boolean A00 = C14950mJ.A00();
            int i = R.string.conversation_cannot_download_media_read_only_media_card_shared_storage;
            if (A00) {
                i = R.string.conversation_cannot_download_media_read_only_media_card;
            }
            r3.Adr(new Object[0], R.string.download_failed, i);
        }
    }

    @Override // X.AbstractC32851cq
    public void AUb() {
        Activity activity = this.A00;
        AnonymousClass009.A05(activity);
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_msg_download_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_msg_download;
        }
        RequestPermissionActivity.A0K(activity, R.string.permission_storage_need_write_access_on_msg_download_request, i2);
    }

    @Override // X.AbstractC32851cq
    public void AXw(String str) {
        Activity activity = this.A00;
        if (!activity.isFinishing()) {
            AnonymousClass009.A05(activity);
            AbstractC13860kS r3 = (AbstractC13860kS) activity;
            boolean A00 = C14950mJ.A00();
            int i = R.string.conversation_cannot_download_media_no_media_card_shared_storage;
            if (A00) {
                i = R.string.conversation_cannot_download_media_no_media_card;
            }
            r3.Adr(new Object[0], R.string.download_failed, i);
        }
    }

    @Override // X.AbstractC32851cq
    public void AXx() {
        Activity activity = this.A00;
        AnonymousClass009.A05(activity);
        int i = Build.VERSION.SDK_INT;
        int i2 = R.string.permission_storage_need_write_access_on_msg_download_v30;
        if (i < 30) {
            i2 = R.string.permission_storage_need_write_access_on_msg_download;
        }
        RequestPermissionActivity.A0K(activity, R.string.permission_storage_need_write_access_on_msg_download_request, i2);
    }
}
