package X;

import java.util.Locale;

/* renamed from: X.4E6  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4E6 {
    public static boolean A00(Locale locale) {
        String upperCase = locale.getCountry().toUpperCase();
        return upperCase.equals(Locale.UK.getCountry()) || upperCase.equals(Locale.US.getCountry());
    }
}
