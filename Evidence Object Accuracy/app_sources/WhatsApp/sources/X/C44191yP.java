package X;

import com.facebook.redex.RunnableBRunnable0Shape11S0100000_I0_11;
import com.whatsapp.settings.SettingsPrivacy;

/* renamed from: X.1yP  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C44191yP implements AnonymousClass13Q {
    public final /* synthetic */ SettingsPrivacy A00;

    public C44191yP(SettingsPrivacy settingsPrivacy) {
        this.A00 = settingsPrivacy;
    }

    @Override // X.AnonymousClass13Q
    public void AWN(AbstractC14640lm r4) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape11S0100000_I0_11(this, 47));
    }

    @Override // X.AnonymousClass13Q
    public void AWr(AbstractC14640lm r4) {
        ((ActivityC13810kN) this.A00).A05.A0H(new RunnableBRunnable0Shape11S0100000_I0_11(this, 46));
    }
}
