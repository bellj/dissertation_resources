package X;

/* renamed from: X.6Cs  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C133946Cs implements AbstractC136356Mf {
    public final /* synthetic */ C123505nG A00;

    @Override // X.AbstractC136356Mf
    public void AOm() {
    }

    public C133946Cs(C123505nG r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC136356Mf
    public void AOE() {
        C123505nG r3 = this.A00;
        C12960it.A0t(C117295Zj.A05(((AbstractC118085bF) r3).A06), "payments_home_onboarding_banner_dismissed", true);
        AbstractC118085bF.A00((AbstractC118085bF) r3);
    }
}
