package X;

/* renamed from: X.19Y  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass19Y {
    public final C15570nT A00;
    public final AbstractC16830pp A01;
    public final C15510nN A02;

    public AnonymousClass19Y(C15570nT r2, C17070qD r3, C15510nN r4) {
        this.A00 = r2;
        this.A02 = r4;
        this.A01 = r3.A02();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r0 != false) goto L_0x002a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.Intent A00(android.app.Activity r9, android.os.Bundle r10, X.AnonymousClass1MJ r11, java.lang.Integer r12, java.lang.String r13, java.lang.String r14, java.util.ArrayList r15, java.util.ArrayList r16, boolean r17) {
        /*
            r8 = this;
            r6 = r13
            boolean r0 = X.AnonymousClass1MK.A00(r13)
            r4 = r10
            r3 = r9
            r7 = r14
            if (r17 == 0) goto L_0x0028
            if (r0 != 0) goto L_0x002a
            X.0nT r0 = r8.A00
            r0.A08()
            com.whatsapp.Me r0 = r0.A00
            if (r0 == 0) goto L_0x004f
            X.0nN r0 = r8.A02
            boolean r0 = r0.A01()
            if (r0 == 0) goto L_0x004f
            X.0mK r2 = new X.0mK
            r2.<init>()
            r5 = r11
            android.content.Intent r2 = r2.A0e(r3, r4, r5, r6, r7)
        L_0x0027:
            return r2
        L_0x0028:
            if (r0 == 0) goto L_0x004f
        L_0x002a:
            X.0pp r1 = r8.A01
            java.lang.Class r0 = r1.ABd()
            if (r0 == 0) goto L_0x004f
            java.lang.Class r0 = r1.ABd()
            android.content.Intent r2 = new android.content.Intent
            r2.<init>(r9, r0)
            if (r10 == 0) goto L_0x0027
            java.lang.String r1 = "com.whatsapp.support.DescribeProblemActivity.paymentFBTxnId"
            boolean r0 = r10.containsKey(r1)
            if (r0 == 0) goto L_0x0027
            java.lang.String r1 = r10.getString(r1)
            java.lang.String r0 = "extra_transaction_id"
            r2.putExtra(r0, r1)
            return r2
        L_0x004f:
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r1 = r9.getPackageName()
            java.lang.String r0 = "com.whatsapp.support.DescribeProblemActivity"
            r2.setClassName(r1, r0)
            java.lang.String r0 = "com.whatsapp.support.DescribeProblemActivity.from"
            r2.putExtra(r0, r13)
            java.lang.String r0 = "com.whatsapp.support.DescribeProblemActivity.serverstatus"
            r2.putExtra(r0, r14)
            if (r12 == 0) goto L_0x006e
            java.lang.String r0 = "com.whatsapp.support.DescribeProblemActivity.type"
            r2.putExtra(r0, r12)
        L_0x006e:
            if (r15 == 0) goto L_0x0075
            java.lang.String r0 = "com.whatsapp.support.DescribeProblemActivity.description.paymentSupportTopicIDs"
            r2.putStringArrayListExtra(r0, r15)
        L_0x0075:
            r1 = r16
            if (r16 == 0) goto L_0x007e
            java.lang.String r0 = "com.whatsapp.support.DescribeProblemActivity.description.paymentSupportTopicTitles"
            r2.putStringArrayListExtra(r0, r1)
        L_0x007e:
            if (r10 == 0) goto L_0x0027
            r2.putExtras(r10)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass19Y.A00(android.app.Activity, android.os.Bundle, X.1MJ, java.lang.Integer, java.lang.String, java.lang.String, java.util.ArrayList, java.util.ArrayList, boolean):android.content.Intent");
    }
}
