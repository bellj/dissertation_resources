package X;

import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.HttpEntity;

/* renamed from: X.3Ij  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65163Ij {
    public static void A00(AbstractC117175Yu r4, String str, boolean z) {
        String str2;
        int i = -1;
        if (!z) {
            List AIQ = r4.AIQ("Retry-After");
            if (AIQ == null || AIQ.isEmpty()) {
                str2 = "drive-util/getRetryAfter/no retry after header";
            } else {
                if (AIQ.size() != 1) {
                    Log.e(C12960it.A0b("drive-util/getRetryAfter/too many retry after headers: ", AIQ));
                }
                String A0g = C12960it.A0g(AIQ, 0);
                i = C28421Nd.A00(A0g, -1);
                if (i < 0) {
                    StringBuilder A0k = C12960it.A0k("drive-util/getRetryAfter/invalid retry after (");
                    A0k.append(A0g);
                    str2 = C12960it.A0d(")", A0k);
                }
            }
            Log.e(str2);
        }
        String ACk = r4.ACk();
        StringBuilder A0k2 = C12960it.A0k("drive-util/");
        A0k2.append(str);
        A0k2.append("/too-many-requests (");
        A0k2.append(ACk);
        Log.w(C12960it.A0e(") retry-after=", A0k2, i));
        throw new C84173yX(ACk, i);
    }

    public static void A01(String str, HttpsURLConnection httpsURLConnection, boolean z) {
        A00(new C617632e(httpsURLConnection), str, z);
        throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
    }

    public static void A02(HttpEntity httpEntity) {
        if (httpEntity != null) {
            try {
                httpEntity.consumeContent();
            } catch (IOException e) {
                Log.e("gdrive-api/consume-entity", e);
            }
        }
    }
}
