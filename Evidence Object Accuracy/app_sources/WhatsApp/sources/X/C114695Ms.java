package X;

/* renamed from: X.5Ms  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C114695Ms extends AnonymousClass1TM {
    public AbstractC114775Na A00;
    public AnonymousClass5N0 A01;
    public C114845Nh A02;
    public boolean A03;
    public boolean A04;
    public boolean A05;
    public boolean A06;

    public static C114695Ms A00(Object obj) {
        if (obj instanceof C114695Ms) {
            return (C114695Ms) obj;
        }
        if (obj != null) {
            return new C114695Ms(AbstractC114775Na.A04(obj));
        }
        return null;
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        return this.A00;
    }

    public C114695Ms(AbstractC114775Na r5) {
        AnonymousClass5N0 r0;
        this.A00 = r5;
        for (int i = 0; i != r5.A0B(); i++) {
            AnonymousClass5NU A01 = AnonymousClass5NU.A01(r5.A0D(i));
            int i2 = A01.A00;
            if (i2 == 0) {
                AnonymousClass5NU A012 = AnonymousClass5NU.A01(AnonymousClass5NU.A00(A01));
                if (A012 != null) {
                    r0 = new AnonymousClass5N0(A012);
                } else {
                    r0 = null;
                }
                this.A01 = r0;
            } else if (i2 == 1) {
                this.A06 = C12960it.A1S(AnonymousClass5NF.A01(A01).A00);
            } else if (i2 == 2) {
                this.A05 = C12960it.A1S(AnonymousClass5NF.A01(A01).A00);
            } else if (i2 == 3) {
                this.A02 = new C114845Nh(AnonymousClass5MA.A01(A01));
            } else if (i2 == 4) {
                this.A03 = C12960it.A1S(AnonymousClass5NF.A01(A01).A00);
            } else if (i2 == 5) {
                this.A04 = C12960it.A1S(AnonymousClass5NF.A01(A01).A00);
            } else {
                throw C12970iu.A0f("unknown tag in IssuingDistributionPoint");
            }
        }
    }

    public String toString() {
        String str = AnonymousClass1T7.A00;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("IssuingDistributionPoint: [");
        stringBuffer.append(str);
        AnonymousClass5N0 r0 = this.A01;
        if (r0 != null) {
            C72453ed.A1N("distributionPoint", str, r0.toString(), stringBuffer);
        }
        if (this.A06) {
            C72453ed.A1N("onlyContainsUserCerts", str, "true", stringBuffer);
        }
        if (this.A05) {
            C72453ed.A1N("onlyContainsCACerts", str, "true", stringBuffer);
        }
        C114845Nh r02 = this.A02;
        if (r02 != null) {
            C72453ed.A1N("onlySomeReasons", str, r02.toString(), stringBuffer);
        }
        if (this.A04) {
            C72453ed.A1N("onlyContainsAttributeCerts", str, "true", stringBuffer);
        }
        if (this.A03) {
            C72453ed.A1N("indirectCRL", str, "true", stringBuffer);
        }
        stringBuffer.append("]");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }
}
