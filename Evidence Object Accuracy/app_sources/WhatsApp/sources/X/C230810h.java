package X;

import java.util.concurrent.atomic.AtomicLong;

/* renamed from: X.10h  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C230810h {
    public C16120oU A00;
    public final AbstractC15710nm A01;
    public final C14830m7 A02;
    public final C17230qT A03;
    public final AtomicLong A04 = new AtomicLong(1);

    public C230810h(AbstractC15710nm r4, C14830m7 r5, C16120oU r6, C17230qT r7) {
        this.A02 = r5;
        this.A01 = r4;
        this.A00 = r6;
        this.A03 = r7;
    }
}
