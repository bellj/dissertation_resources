package X;

/* renamed from: X.4XZ  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4XZ {
    public final long A00;

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof AnonymousClass4XZ) && this.A00 == ((AnonymousClass4XZ) obj).A00);
    }

    public AnonymousClass4XZ(long j) {
        this.A00 = j;
    }

    public int hashCode() {
        return C72453ed.A0B(this.A00);
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("AvatarDeleteConfigResult(clientMutationId=");
        A0k.append(this.A00);
        return C12970iu.A0u(A0k);
    }
}
