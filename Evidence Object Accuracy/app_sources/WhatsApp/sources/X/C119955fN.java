package X;

/* renamed from: X.5fN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C119955fN extends AbstractC16110oT {
    public Boolean A00;
    public Boolean A01;
    public String A02;

    public C119955fN() {
        super(3580, new AnonymousClass00E(1, 1, 1), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A02);
        r3.Abe(3, this.A00);
        r3.Abe(4, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamAndroidRootedDeviceCheck {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "checkLocation", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "rwPaths", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "suExists", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
