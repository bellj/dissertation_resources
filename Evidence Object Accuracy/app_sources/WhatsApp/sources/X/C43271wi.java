package X;

/* renamed from: X.1wi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C43271wi extends Exception {
    public String description = null;
    public final Integer e2eFailureReason;

    public C43271wi(Integer num) {
        this.e2eFailureReason = num;
    }

    public C43271wi(String str, Integer num) {
        this.e2eFailureReason = num;
        this.description = str;
    }
}
