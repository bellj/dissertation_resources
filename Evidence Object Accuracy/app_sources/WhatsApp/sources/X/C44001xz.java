package X;

/* renamed from: X.1xz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C44001xz extends AbstractC16110oT {
    public Double A00;
    public Double A01;
    public Double A02;
    public Integer A03;
    public Integer A04;
    public Long A05;
    public Long A06;
    public Long A07;
    public Long A08;
    public String A09;

    public C44001xz() {
        super(1912, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(5, this.A00);
        r3.Abe(4, this.A01);
        r3.Abe(9, this.A02);
        r3.Abe(1, this.A09);
        r3.Abe(10, this.A03);
        r3.Abe(2, this.A04);
        r3.Abe(3, this.A05);
        r3.Abe(6, this.A06);
        r3.Abe(7, this.A07);
        r3.Abe(8, this.A08);
    }

    @Override // java.lang.Object
    public String toString() {
        String obj;
        String obj2;
        StringBuilder sb = new StringBuilder("WamAndroidDatabaseMigrationEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(sb, "afterMigrationMsgstoreSize", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "beforeMigrationMsgstoreSize", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "freeSpaceAvailable", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationName", this.A09);
        Integer num = this.A03;
        if (num == null) {
            obj = null;
        } else {
            obj = num.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationSkipReason", obj);
        Integer num2 = this.A04;
        if (num2 == null) {
            obj2 = null;
        } else {
            obj2 = num2.toString();
        }
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationStatus", obj2);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "migrationT", this.A05);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "retryCount", this.A06);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rowProcessedCnt", this.A07);
        AbstractC16110oT.appendFieldToStringBuilder(sb, "rowSkippedCnt", this.A08);
        sb.append("}");
        return sb.toString();
    }
}
