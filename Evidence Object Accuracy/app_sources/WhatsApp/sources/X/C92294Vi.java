package X;

import java.util.LinkedList;

/* renamed from: X.4Vi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92294Vi {
    public int A00;
    public C92294Vi A01;
    public C92294Vi A02 = null;
    public LinkedList A03;

    public /* synthetic */ C92294Vi(LinkedList linkedList, int i) {
        this.A00 = i;
        this.A03 = linkedList;
        this.A01 = null;
    }

    public String toString() {
        StringBuilder A0k = C12960it.A0k("LinkedEntry(key: ");
        A0k.append(this.A00);
        return C12960it.A0d(")", A0k);
    }
}
