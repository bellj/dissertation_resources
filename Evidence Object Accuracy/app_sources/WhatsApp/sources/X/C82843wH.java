package X;

/* renamed from: X.3wH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C82843wH extends AnonymousClass5H8 {
    public C82843wH() {
    }

    public C82843wH(String str) {
        super(str);
    }

    public C82843wH(String str, Throwable th) {
        super(str, th);
    }

    public C82843wH(Throwable th) {
        super(th);
    }

    public static C82843wH A00(String str) {
        return new C82843wH(str);
    }
}
