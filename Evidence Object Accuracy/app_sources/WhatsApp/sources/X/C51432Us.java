package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.redex.ViewOnClickCListenerShape0S0101000_I0;
import com.whatsapp.R;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: X.2Us  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C51432Us extends AnonymousClass02M {
    public final int A00;
    public final Context A01;
    public final C006202y A02;
    public final AnonymousClass018 A03;
    public final AnonymousClass19M A04;
    public final C453421e A05;
    public final C457522x A06;
    public final C453321d A07;
    public final AnonymousClass2BB A08;
    public final AnonymousClass21x A09;
    public final AnonymousClass1AB A0A;
    public final C22190yg A0B;
    public final HashSet A0C;
    public final Set A0D = new HashSet();
    public final boolean A0E;

    public C51432Us(Context context, C006202y r3, AnonymousClass018 r4, AnonymousClass19M r5, C453421e r6, C457522x r7, C453321d r8, AnonymousClass2BB r9, AnonymousClass21x r10, AnonymousClass1AB r11, C22190yg r12, HashSet hashSet, int i, boolean z) {
        A07(true);
        this.A01 = context;
        this.A03 = r4;
        this.A04 = r5;
        this.A0A = r11;
        this.A09 = r10;
        this.A08 = r9;
        this.A0B = r12;
        this.A06 = r7;
        this.A05 = r6;
        this.A0C = hashSet;
        this.A02 = r3;
        this.A07 = r8;
        this.A00 = i;
        this.A0E = z;
    }

    @Override // X.AnonymousClass02M
    public long A00(int i) {
        return (long) ((List) this.A07.A02.A01()).get(i).hashCode();
    }

    @Override // X.AnonymousClass02M
    public int A0D() {
        return ((List) this.A07.A02.A01()).size();
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ void ANH(AnonymousClass03U r15, int i) {
        Context context;
        int i2;
        C75173jQ r152 = (C75173jQ) r15;
        C457522x r2 = this.A06;
        if (r2 != null) {
            C616531g r10 = (C616531g) r152.A0H;
            C453321d r5 = this.A07;
            boolean z = false;
            if (((Number) r5.A01.A01()).intValue() == i) {
                z = true;
            }
            r10.setSelected(z);
            r2.A01((AnonymousClass23D) r10.getTag());
            Uri uri = (Uri) ((List) r5.A02.A01()).get(i);
            C39341ph A00 = this.A05.A00(uri);
            r10.A02 = A00;
            r10.A04 = r152;
            C22190yg r12 = this.A0B;
            byte A05 = r12.A05(A00);
            A00.A0C(Byte.valueOf(A05));
            if (A05 == 3) {
                context = this.A01;
                r10.A01 = AnonymousClass00T.A04(context, R.drawable.mark_video);
                i2 = R.string.conversations_most_recent_video;
            } else if (A05 != 13) {
                r10.A01 = null;
                context = this.A01;
                i2 = R.string.conversations_most_recent_image;
            } else {
                context = this.A01;
                r10.A01 = AnonymousClass00T.A04(context, R.drawable.mark_gif);
                i2 = R.string.conversations_most_recent_gif;
            }
            r10.setContentDescription(context.getString(i2));
            r10.setOnClickListener(new ViewOnClickCListenerShape0S0101000_I0(this, i, 4));
            r10.setOnTouchListener(new View.OnTouchListener() { // from class: X.3N4
                /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
                    if (r1 != 3) goto L_0x0016;
                 */
                @Override // android.view.View.OnTouchListener
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final boolean onTouch(android.view.View r15, android.view.MotionEvent r16) {
                    /*
                    // Method dump skipped, instructions count: 308
                    */
                    throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3N4.onTouch(android.view.View, android.view.MotionEvent):boolean");
                }
            });
            AnonymousClass3X7 r52 = new AnonymousClass3X7(uri, this.A03, this.A04, A00, r10, this.A0A, r12, this.A00);
            this.A0D.add(r52);
            r10.setTag(r52);
            C006202y r4 = this.A02;
            AnonymousClass3XH r3 = new AnonymousClass3XH(r4, r52, r10);
            Bitmap bitmap = (Bitmap) r4.A04(r52.AH5());
            if (bitmap == null) {
                r2.A02(r52, r3);
            } else {
                r3.AWw(bitmap, true);
            }
        }
    }

    @Override // X.AnonymousClass02M
    public /* bridge */ /* synthetic */ AnonymousClass03U AOl(ViewGroup viewGroup, int i) {
        return new C75173jQ(new C616531g(this.A01, this.A08, this.A0C, this.A00, this.A0E));
    }
}
