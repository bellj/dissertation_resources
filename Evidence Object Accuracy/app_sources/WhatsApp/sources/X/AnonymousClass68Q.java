package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.payments.ui.BrazilPaymentSettingsFragment;

/* renamed from: X.68Q  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68Q implements AnonymousClass6ML {
    public final /* synthetic */ BrazilPaymentSettingsFragment A00;

    public AnonymousClass68Q(BrazilPaymentSettingsFragment brazilPaymentSettingsFragment) {
        this.A00 = brazilPaymentSettingsFragment;
    }

    @Override // X.AnonymousClass6ML
    public void AO2(String str, String str2) {
        BrazilPaymentSettingsFragment brazilPaymentSettingsFragment = this.A00;
        AbstractC1308360d r1 = brazilPaymentSettingsFragment.A0r;
        if (r1 != null) {
            r1.A04((ActivityC13790kL) brazilPaymentSettingsFragment.A0C(), str);
        }
    }

    @Override // X.AnonymousClass6ML
    public void ARq() {
        BrazilPaymentSettingsFragment brazilPaymentSettingsFragment = this.A00;
        Context A01 = brazilPaymentSettingsFragment.A01();
        Intent A0A = C12970iu.A0A();
        A0A.setClassName(A01.getPackageName(), "com.whatsapp.framework.alerts.ui.AlertCardListActivity");
        brazilPaymentSettingsFragment.A0v(A0A);
    }
}
