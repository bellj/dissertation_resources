package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

/* renamed from: X.08G  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass08G extends AnonymousClass08H {
    public final WeakReference A00;

    public AnonymousClass08G(Context context, Resources resources) {
        super(resources);
        this.A00 = new WeakReference(context);
    }

    @Override // android.content.res.Resources
    public Drawable getDrawable(int i) {
        Drawable drawable = super.A00.getDrawable(i);
        Context context = (Context) this.A00.get();
        if (!(drawable == null || context == null)) {
            C012005t.A01().A09(context, drawable, i);
        }
        return drawable;
    }
}
