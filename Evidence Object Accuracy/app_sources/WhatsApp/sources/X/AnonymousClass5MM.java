package X;

import java.util.Enumeration;

/* renamed from: X.5MM  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5MM extends AnonymousClass1TM {
    public AnonymousClass5NS A00;
    public AnonymousClass5NG A01;
    public AnonymousClass5NH A02;
    public AnonymousClass5NV A03;
    public C114725Mv A04;

    public AnonymousClass5MM(AbstractC114775Na r7) {
        Enumeration A0C = r7.A0C();
        AnonymousClass5NG A00 = AnonymousClass5NG.A00(A0C.nextElement());
        this.A01 = A00;
        int A0B = A00.A0B();
        if (A0B < 0 || A0B > 1) {
            throw C12970iu.A0f("invalid version for private key info");
        }
        this.A04 = C114725Mv.A00(A0C.nextElement());
        this.A02 = AnonymousClass5NH.A01(A0C.nextElement());
        int i = -1;
        while (A0C.hasMoreElements()) {
            AnonymousClass5NU r2 = (AnonymousClass5NU) A0C.nextElement();
            int i2 = r2.A00;
            if (i2 > i) {
                if (i2 == 0) {
                    this.A03 = AnonymousClass5NV.A01(r2);
                } else if (i2 != 1) {
                    throw C12970iu.A0f("unknown optional field in private key info");
                } else if (A0B >= 1) {
                    this.A00 = AnonymousClass5MA.A01(r2);
                } else {
                    throw C12970iu.A0f("'publicKey' requires version v2(1) or later");
                }
                i = i2;
            } else {
                throw C12970iu.A0f("invalid optional field in private key info");
            }
        }
    }

    @Override // X.AnonymousClass1TM, X.AnonymousClass1TN
    public AnonymousClass1TL Aer() {
        C94954co r3 = new C94954co(5);
        r3.A06(this.A01);
        r3.A06(this.A04);
        r3.A06(this.A02);
        AnonymousClass5NV r0 = this.A03;
        if (r0 != null) {
            C94954co.A03(r0, r3, false);
        }
        AnonymousClass5NS r1 = this.A00;
        if (r1 != null) {
            C94954co.A02(r1, r3, 1, false);
        }
        return new AnonymousClass5NZ(r3);
    }
}
