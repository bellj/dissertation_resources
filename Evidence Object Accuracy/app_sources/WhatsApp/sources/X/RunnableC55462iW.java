package X;

import android.view.View;
import com.facebook.redex.EmptyBaseRunnable0;
import java.util.List;

/* renamed from: X.2iW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class RunnableC55462iW extends EmptyBaseRunnable0 implements Runnable {
    public final /* synthetic */ AnonymousClass28D A00;
    public final /* synthetic */ List A01;

    public RunnableC55462iW(AnonymousClass28D r1, List list) {
        this.A01 = list;
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        String valueOf;
        View view;
        int i = -1;
        for (Object obj : this.A01) {
            AnonymousClass28D r2 = this.A00;
            if (obj == null) {
                valueOf = null;
            } else if (obj instanceof String) {
                valueOf = (String) obj;
            } else {
                valueOf = String.valueOf(obj);
            }
            AnonymousClass28D A00 = C87854Dg.A00(r2, new C1093351h(valueOf));
            if (A00 != null) {
                C89054Im r0 = A00.A03;
                if (r0 != null && (view = r0.A00) != null) {
                    view.setFocusable(true);
                    if (view.getId() == -1) {
                        view.setId(View.generateViewId());
                    }
                    if (i != -1) {
                        view.setAccessibilityTraversalAfter(i);
                    }
                    i = view.getId();
                } else {
                    return;
                }
            } else {
                throw C12960it.A0U(C12960it.A0b("Component does not exists in this hierarchy for id: ", obj));
            }
        }
    }
}
