package X;

import com.whatsapp.payments.ui.ViralityLinkVerifierActivity;

/* renamed from: X.5di  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public abstract class AbstractActivityC119385di extends ActivityC13790kL {
    public boolean A00 = false;

    public AbstractActivityC119385di() {
        C117295Zj.A0p(this, 109);
    }

    @Override // X.AbstractActivityC13800kM, X.AbstractActivityC13820kO, X.AbstractActivityC13850kR
    public void A1k() {
        if (!this.A00) {
            this.A00 = true;
            ViralityLinkVerifierActivity viralityLinkVerifierActivity = (ViralityLinkVerifierActivity) this;
            AnonymousClass2FL r3 = (AnonymousClass2FL) ((AnonymousClass2FJ) generatedComponent());
            AnonymousClass01J A1M = ActivityC13830kP.A1M(r3, viralityLinkVerifierActivity);
            ActivityC13810kN.A10(A1M, viralityLinkVerifierActivity);
            ((ActivityC13790kL) viralityLinkVerifierActivity).A08 = ActivityC13790kL.A0S(r3, A1M, viralityLinkVerifierActivity, ActivityC13790kL.A0Y(A1M, viralityLinkVerifierActivity));
            viralityLinkVerifierActivity.A0C = C117315Zl.A0F(A1M);
            viralityLinkVerifierActivity.A0B = C117305Zk.A0P(A1M);
            viralityLinkVerifierActivity.A08 = C117315Zl.A0A(A1M);
            viralityLinkVerifierActivity.A09 = C117305Zk.A0M(A1M);
            viralityLinkVerifierActivity.A0A = C117305Zk.A0O(A1M);
        }
    }
}
