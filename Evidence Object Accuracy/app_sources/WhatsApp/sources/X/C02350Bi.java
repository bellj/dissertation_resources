package X;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.view.animation.Animation;
import android.widget.ImageView;

/* renamed from: X.0Bi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C02350Bi extends ImageView {
    public int A00;
    public Animation.AnimationListener A01;

    public C02350Bi(Context context) {
        super(context);
        ShapeDrawable shapeDrawable;
        float f = getContext().getResources().getDisplayMetrics().density;
        int i = (int) (1.75f * f);
        int i2 = (int) (0.0f * f);
        int i3 = (int) (3.5f * f);
        this.A00 = i3;
        if (Build.VERSION.SDK_INT >= 21) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            AnonymousClass028.A0V(this, f * 4.0f);
        } else {
            shapeDrawable = new ShapeDrawable(new AnonymousClass0AC(this, i3));
            setLayerType(1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float) this.A00, (float) i2, (float) i, 503316480);
            int i4 = this.A00;
            setPadding(i4, i4, i4, i4);
        }
        shapeDrawable.getPaint().setColor(-328966);
        setBackground(shapeDrawable);
    }

    @Override // android.view.View
    public void onAnimationEnd() {
        super.onAnimationEnd();
        Animation.AnimationListener animationListener = this.A01;
        if (animationListener != null) {
            animationListener.onAnimationEnd(getAnimation());
        }
    }

    @Override // android.view.View
    public void onAnimationStart() {
        super.onAnimationStart();
        Animation.AnimationListener animationListener = this.A01;
        if (animationListener != null) {
            animationListener.onAnimationStart(getAnimation());
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (Build.VERSION.SDK_INT < 21) {
            setMeasuredDimension(getMeasuredWidth() + (this.A00 << 1), getMeasuredHeight() + (this.A00 << 1));
        }
    }

    public void setAnimationListener(Animation.AnimationListener animationListener) {
        this.A01 = animationListener;
    }

    @Override // android.view.View
    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }

    public void setBackgroundColorRes(int i) {
        setBackgroundColor(AnonymousClass00T.A00(getContext(), i));
    }
}
