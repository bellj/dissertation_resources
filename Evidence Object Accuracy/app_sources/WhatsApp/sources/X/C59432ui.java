package X;

/* renamed from: X.2ui  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59432ui extends AbstractC16320oo {
    public final double A00;
    public final double A01;
    public final int A02;

    public C59432ui(AbstractC15710nm r14, C14900mE r15, C16430p0 r16, C48122Ek r17, AnonymousClass2K1 r18, AnonymousClass1B4 r19, C16340oq r20, C16590pI r21, C17170qN r22, AnonymousClass018 r23, C14850m9 r24, AbstractC14440lR r25) {
        super(r14, r15, r16, r18, r19, r20, r22, r23, r24, r25);
        this.A00 = r17.A02().doubleValue();
        this.A01 = r17.A03().doubleValue();
        this.A02 = C16590pI.A00(r21).getDisplayMetrics().densityDpi;
    }
}
