package X;

import android.util.Pair;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import java.util.Map;

/* renamed from: X.3ZP  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3ZP implements AbstractC21730xt {
    public final AbstractC15710nm A00;
    public final C15570nT A01;
    public final C14830m7 A02;
    public final C14850m9 A03;
    public final C20710wC A04;
    public final AbstractC36361jl A05;
    public final C63323Bd A06;
    public final C17220qS A07;
    public final String A08;

    public AnonymousClass3ZP(AbstractC15710nm r2, C15570nT r3, C14830m7 r4, C14850m9 r5, C20710wC r6, AbstractC36361jl r7, C63323Bd r8, C17220qS r9) {
        this.A02 = r4;
        this.A03 = r5;
        this.A00 = r2;
        this.A01 = r3;
        this.A07 = r9;
        this.A04 = r6;
        this.A05 = r7;
        this.A08 = r8.A02.getRawString();
        this.A06 = r8;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0035, code lost:
        if (r20.A03.A07(1173) == false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0039, code lost:
        if (r3 != false) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        if (r20.A03.A07(1173) == false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0043, code lost:
        r16 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        if (android.text.TextUtils.isEmpty(r9) == false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        r16 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004d, code lost:
        r0 = (((X.C12970iu.A09(r12) + (r19 ? 1 : 0)) + r18) + r17) + r16;
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
        if (r0 == 0) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005c, code lost:
        r6 = new X.AnonymousClass1V8[r0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0060, code lost:
        if (r12 == null) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0066, code lost:
        if (r12.size() <= 0) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0068, code lost:
        r3 = 0;
        r15 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006e, code lost:
        if (r3 >= r12.size()) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0070, code lost:
        r2 = new X.AnonymousClass1W9[r1];
        r2[0] = new X.AnonymousClass1W9((com.whatsapp.jid.Jid) r12.get(r3), "jid");
        r6[r15] = new X.AnonymousClass1V8("participant", r2);
        r3 = r3 + 1;
        r15 = r15 + 1;
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0098, code lost:
        r15 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0099, code lost:
        if (r19 == false) goto L_0x00b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
        r3 = new X.AnonymousClass1W9[r1];
        r3[0] = new X.AnonymousClass1W9("expiration", java.lang.String.valueOf(r13));
        r6[r15] = new X.AnonymousClass1V8("ephemeral", r3);
        r15 = r15 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b7, code lost:
        if (r18 == 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b9, code lost:
        r6[r15] = new X.AnonymousClass1V8("linked_parent", new X.AnonymousClass1W9[]{new X.AnonymousClass1W9(r11, "jid")});
        r15 = r15 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d0, code lost:
        if (r17 == 0) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d2, code lost:
        r6[r15] = new X.AnonymousClass1V8("parent", null);
        r15 = r15 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00df, code lost:
        if (r16 == 0) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e1, code lost:
        r0 = X.AnonymousClass15O.A01(r20.A01, r20.A02, false);
        X.AnonymousClass009.A05(r0);
        r3 = new X.AnonymousClass1W9[1];
        X.C12960it.A1M("id", X.C003501n.A03(r0), r3, 0);
        r6[r15] = new X.AnonymousClass1V8(new X.AnonymousClass1V8("body", r9, (X.AnonymousClass1W9[]) null), "description", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x010a, code lost:
        r10 = r20.A08;
        r9 = android.text.TextUtils.isEmpty(r10);
        r1 = new X.AnonymousClass1W9[(!r9 ? 1 : 0) + 1];
        r2 = X.C12990iw.A1a("subject", r8, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x011d, code lost:
        if (r9 != false) goto L_0x0124;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x011f, code lost:
        X.C12960it.A1M("key", r10, r1, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0126, code lost:
        if (r6 != null) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0128, code lost:
        r8 = new X.AnonymousClass1V8("create", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x012d, code lost:
        r0 = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x012f, code lost:
        if (r7 != null) goto L_0x0132;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0131, code lost:
        r0 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0132, code lost:
        r6 = new X.AnonymousClass1W9[r0];
        X.C12960it.A1M("xmlns", "w:g2", r6, r2 ? 1 : 0);
        X.C12960it.A1M("id", r5, r6, 1);
        X.C12960it.A1M("type", "set", r6, 2);
        r6[3] = new X.AnonymousClass1W9(X.C29991Vn.A00, "to");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0159, code lost:
        if (r7 == null) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x015b, code lost:
        X.C12960it.A1M("web", r7.A00, r6, 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0163, code lost:
        r20.A07.A0D(r20, new X.AnonymousClass1V8(r8, "iq", r6), r5, 14, 20000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0176, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0177, code lost:
        r8 = new X.AnonymousClass1V8("create", r1, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if (r20.A03.A07(1173) == false) goto L_0x0029;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A00() {
        /*
        // Method dump skipped, instructions count: 381
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass3ZP.A00():void");
    }

    @Override // X.AbstractC21730xt
    public void AP1(String str) {
        this.A05.AXX();
    }

    @Override // X.AbstractC21730xt
    public void APv(AnonymousClass1V8 r4, String str) {
        Pair A01 = C41151sz.A01(r4);
        if (A01 != null) {
            int A05 = C12960it.A05(A01.first);
            String str2 = (String) A01.second;
            if (A05 == 500 && str2 != null && str2.equals("internal-server-error")) {
                A05 = -500;
            }
            this.A05.APl(A05);
        }
    }

    @Override // X.AbstractC21730xt
    public void AX9(AnonymousClass1V8 r19, String str) {
        AnonymousClass1V8 A0C = r19.A0C();
        AnonymousClass1V8.A01(A0C, "group");
        try {
            String A0I = A0C.A0I("id", null);
            AnonymousClass009.A05(A0I);
            C15580nU A01 = C15380n4.A01(A0I);
            AbstractC15710nm r13 = this.A00;
            Jid A0A = A0C.A0A(r13, UserJid.class, "creator");
            String A0I2 = A0C.A0I("subject", null);
            Jid A0A2 = A0C.A0A(r13, UserJid.class, "s_o");
            String A0I3 = A0C.A0I("type", null);
            C47572Bl r6 = new C47572Bl(A01);
            C50892Rt.A00(r13, r19, r6, "group");
            StringBuilder A0k = C12960it.A0k("groupmgr/onGroupCreated/");
            A0k.append(A01);
            A0k.append("/");
            A0k.append(A0A);
            A0k.append("/");
            A0k.append(C28421Nd.A01(A0C.A0I("creation", null), 0) * 1000);
            A0k.append("/");
            C12980iv.A1Q(A0A2, A0I2, "/", A0k);
            A0k.append("/");
            A0k.append(C28421Nd.A01(A0C.A0I("s_t", null), 0) * 1000);
            A0k.append("/");
            A0k.append(A0I3);
            A0k.append("/");
            A0k.append(r6.A03.keySet());
            C12960it.A1F(A0k);
            Map map = r6.A01;
            if (!map.isEmpty()) {
                C20710wC.A02(3001, map);
            }
            this.A05.AWy(A01, r6);
        } catch (AnonymousClass1MW unused) {
            this.A05.APl(800);
        }
    }
}
