package X;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/* renamed from: X.5oY  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5oY extends AbstractC16350or {
    public final AnonymousClass3FE A00;
    public final C17070qD A01;

    public /* synthetic */ AnonymousClass5oY(AnonymousClass3FE r1, C17070qD r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        List A0Z = C117295Zj.A0Z(this.A01);
        if (A0Z.size() <= 0) {
            return null;
        }
        Collections.sort(A0Z, new Comparator() { // from class: X.6KK
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                AbstractC28901Pl r11 = (AbstractC28901Pl) obj2;
                AbstractC30871Zd r0 = (AbstractC30871Zd) ((AbstractC28901Pl) obj).A08;
                long j = Long.MAX_VALUE;
                if (r0 != null) {
                    long j2 = r0.A06;
                    if (j2 >= 0) {
                        j = j2;
                    }
                }
                AbstractC30871Zd r02 = (AbstractC30871Zd) r11.A08;
                long j3 = Long.MAX_VALUE;
                if (r02 != null) {
                    long j4 = r02.A06;
                    if (j4 >= 0) {
                        j3 = j4;
                    }
                }
                return (j > j3 ? 1 : (j == j3 ? 0 : -1));
            }
        });
        AbstractC28901Pl r5 = (AbstractC28901Pl) C12980iv.A0o(A0Z);
        AnonymousClass1ZY r0 = r5.A08;
        if (r0 == null || ((AbstractC30871Zd) r0).A06 < 0) {
            return null;
        }
        return C117315Zl.A05(Integer.valueOf(A0Z.size()), r5);
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        String str;
        AnonymousClass01T r6 = (AnonymousClass01T) obj;
        if (r6 != null) {
            Object obj2 = r6.A01;
            AnonymousClass009.A05(obj2);
            AbstractC28901Pl r4 = (AbstractC28901Pl) obj2;
            HashMap A11 = C12970iu.A11();
            A11.put("credential_id", r4.A0A);
            A11.put("last4", C117295Zj.A0R(r4.A09));
            A11.put("remaining_cards", C12970iu.A0s(r6.A00, C12960it.A0k("")));
            AbstractC30871Zd r0 = (AbstractC30871Zd) r4.A08;
            if (r0 != null) {
                str = C12960it.A0f(C12960it.A0j(""), r0.A04);
            } else {
                str = "-1";
            }
            A11.put("remaining_retries", str);
            this.A00.A01("on_success", A11);
            return;
        }
        C117315Zl.A0S(this.A00);
    }
}
