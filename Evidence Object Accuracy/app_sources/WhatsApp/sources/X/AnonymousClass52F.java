package X;

/* renamed from: X.52F  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass52F implements AnonymousClass5T9 {
    public final C92364Vp A00;
    public final C92574Wl A01;
    public final Object A02;
    public final String A03;

    public AnonymousClass52F(C92364Vp r2, C92574Wl r3, Object obj) {
        this.A01 = r3;
        this.A03 = obj.toString();
        this.A00 = r2;
        this.A02 = r3.A00(r2, obj, obj).A00();
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            AnonymousClass52F r5 = (AnonymousClass52F) obj;
            if (!AnonymousClass08r.A00(this.A01, r5.A01) || !this.A03.equals(r5.A03) || !AnonymousClass08r.A00(this.A00, r5.A00)) {
                return false;
            }
        }
        return true;
    }

    @Override // X.AnonymousClass5T9
    public Object get() {
        return this.A02;
    }
}
