package X;

import java.util.regex.Pattern;

/* renamed from: X.4dd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95394dd {
    public static final Pattern A02 = Pattern.compile("\\[voice=\"([^\"]*)\"\\]");
    public final C95304dT A00 = new C95304dT();
    public final StringBuilder A01 = C12960it.A0h();

    public static String A00(C95304dT r6, StringBuilder sb) {
        boolean z = false;
        sb.setLength(0);
        int i = r6.A01;
        int i2 = r6.A00;
        while (i < i2 && !z) {
            char c = (char) r6.A02[i];
            if ((c < 'A' || c > 'Z') && ((c < 'a' || c > 'z') && !((c >= '0' && c <= '9') || c == '#' || c == '-' || c == '.' || c == '_'))) {
                z = true;
            } else {
                i++;
                sb.append(c);
            }
        }
        r6.A0T(i - i);
        return sb.toString();
    }

    public static String A01(C95304dT r3, StringBuilder sb) {
        A02(r3);
        if (C95304dT.A00(r3) == 0) {
            return null;
        }
        String A00 = A00(r3, sb);
        if (!"".equals(A00)) {
            return A00;
        }
        return C72463ee.A0H(C12960it.A0j(""), (char) r3.A0C());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0036, code lost:
        r1 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0038, code lost:
        if (r1 >= r7) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003d, code lost:
        if (((char) r5[r3]) != '*') goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0042, code lost:
        if (((char) r5[r1]) != '/') goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
        r3 = r1 + 1;
        r7 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0048, code lost:
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004a, code lost:
        r8.A0T(r7 - r6);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void A02(X.C95304dT r8) {
        /*
        L_0x0000:
            r1 = 1
        L_0x0001:
            int r7 = r8.A00
            int r6 = r8.A01
            int r0 = r7 - r6
            if (r0 <= 0) goto L_0x0056
            if (r1 == 0) goto L_0x0056
            byte[] r5 = r8.A02
            byte r2 = r5[r6]
            char r1 = (char) r2
            r0 = 9
            if (r1 == r0) goto L_0x0051
            r0 = 10
            if (r1 == r0) goto L_0x0051
            r0 = 12
            if (r1 == r0) goto L_0x0051
            r0 = 13
            if (r1 == r0) goto L_0x0051
            r0 = 32
            if (r1 == r0) goto L_0x0051
            int r0 = r6 + 2
            if (r0 > r7) goto L_0x004f
            int r0 = r6 + 1
            r4 = 47
            if (r2 != r4) goto L_0x004f
            int r3 = r0 + 1
            byte r0 = r5[r0]
            r2 = 42
            if (r0 != r2) goto L_0x004f
        L_0x0036:
            int r1 = r3 + 1
            if (r1 >= r7) goto L_0x004a
            byte r0 = r5[r3]
            char r0 = (char) r0
            if (r0 != r2) goto L_0x0048
            byte r0 = r5[r1]
            char r0 = (char) r0
            if (r0 != r4) goto L_0x0048
            int r3 = r1 + 1
            r7 = r3
            goto L_0x0036
        L_0x0048:
            r3 = r1
            goto L_0x0036
        L_0x004a:
            int r7 = r7 - r6
            r8.A0T(r7)
            goto L_0x0000
        L_0x004f:
            r1 = 0
            goto L_0x0001
        L_0x0051:
            r0 = 1
            r8.A0T(r0)
            goto L_0x0000
        L_0x0056:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95394dd.A02(X.4dT):void");
    }
}
