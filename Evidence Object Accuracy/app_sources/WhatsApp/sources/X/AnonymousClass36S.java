package X;

import android.view.View;
import android.widget.AdapterView;
import com.whatsapp.calling.callhistory.CallsHistoryFragment;
import com.whatsapp.util.Log;

/* renamed from: X.36S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass36S extends AbstractC102384p3 {
    public final /* synthetic */ CallsHistoryFragment A00;

    public AnonymousClass36S(CallsHistoryFragment callsHistoryFragment) {
        this.A00 = callsHistoryFragment;
    }

    @Override // X.AbstractC102384p3
    public void A00(AdapterView adapterView, View view, int i, long j) {
        AbstractC92184Uw r2 = (AbstractC92184Uw) view.getTag();
        if (r2 == null) {
            Log.e("voip/CallsFragment/onItemClick/empty");
        } else {
            this.A00.A1H(r2.A00, r2);
        }
    }

    @Override // X.AbstractC102384p3, android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.A00.A01 == null) {
            super.onItemClick(adapterView, view, i, j);
        } else {
            A00(adapterView, view, i, j);
        }
    }
}
