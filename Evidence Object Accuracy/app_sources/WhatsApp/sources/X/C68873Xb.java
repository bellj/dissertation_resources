package X;

import java.io.IOException;

/* renamed from: X.3Xb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68873Xb implements AbstractC44401yr {
    public final /* synthetic */ C64063Ec A00;
    public final /* synthetic */ C68883Xc A01;
    public final /* synthetic */ AbstractC44401yr A02;

    public C68873Xb(C64063Ec r1, C68883Xc r2, AbstractC44401yr r3) {
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r2) {
        this.A02.A6t(r2);
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        this.A02.AOz(iOException);
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        if (!(exc instanceof C87564Bz) || ((C87564Bz) exc).error.A00 != 190) {
            this.A02.APp(exc);
            return;
        }
        C68883Xc r4 = this.A01;
        r4.A01.A01(this.A00, new C68803Wu(r4, this.A02), null);
    }
}
