package X;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

/* renamed from: X.4vn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C106494vn implements AnonymousClass5Xx {
    public float A00;
    public float A01;
    public int A02;
    public long A03;
    public long A04;
    public C94084bE A05;
    public C94084bE A06;
    public C94084bE A07;
    public C94084bE A08;
    public AnonymousClass4YE A09;
    public ByteBuffer A0A;
    public ByteBuffer A0B;
    public ShortBuffer A0C;
    public boolean A0D;
    public boolean A0E;

    public C106494vn() {
        A00(this);
    }

    public static void A00(C106494vn r2) {
        r2.A01 = 1.0f;
        r2.A00 = 1.0f;
        C94084bE r0 = C94084bE.A04;
        r2.A07 = r0;
        r2.A08 = r0;
        r2.A05 = r0;
        r2.A06 = r0;
        ByteBuffer byteBuffer = AnonymousClass5Xx.A00;
        r2.A0A = byteBuffer;
        r2.A0C = byteBuffer.asShortBuffer();
        r2.A0B = byteBuffer;
        r2.A02 = -1;
    }

    @Override // X.AnonymousClass5Xx
    public C94084bE A7V(C94084bE r5) {
        if (r5.A02 == 2) {
            int i = this.A02;
            if (i == -1) {
                i = r5.A03;
            }
            this.A07 = r5;
            C94084bE r1 = new C94084bE(i, r5.A01, 2);
            this.A08 = r1;
            this.A0E = true;
            return r1;
        }
        throw new C87414Bk(r5);
    }

    @Override // X.AnonymousClass5Xx
    public ByteBuffer AEn() {
        int i;
        AnonymousClass4YE r8 = this.A09;
        if (r8 != null && (i = (r8.A05 * r8.A0G) << 1) > 0) {
            if (this.A0A.capacity() < i) {
                ByteBuffer A0x = C72453ed.A0x(i);
                this.A0A = A0x;
                this.A0C = A0x.asShortBuffer();
            } else {
                this.A0A.clear();
                this.A0C.clear();
            }
            ShortBuffer shortBuffer = this.A0C;
            int remaining = shortBuffer.remaining();
            int i2 = r8.A0G;
            int min = Math.min(remaining / i2, r8.A05);
            int i3 = i2 * min;
            shortBuffer.put(r8.A0B, 0, i3);
            int i4 = r8.A05 - min;
            r8.A05 = i4;
            short[] sArr = r8.A0B;
            System.arraycopy(sArr, i3, sArr, 0, i4 * i2);
            this.A04 += (long) i;
            this.A0A.limit(i);
            this.A0B = this.A0A;
        }
        ByteBuffer byteBuffer = this.A0B;
        this.A0B = AnonymousClass5Xx.A00;
        return byteBuffer;
    }

    @Override // X.AnonymousClass5Xx
    public boolean AJD() {
        int i = this.A08.A03;
        if (i != -1) {
            return Math.abs(this.A01 - 1.0f) >= 1.0E-4f || Math.abs(this.A00 - 1.0f) >= 1.0E-4f || i != this.A07.A03;
        }
        return false;
    }

    @Override // X.AnonymousClass5Xx
    public boolean AJN() {
        if (!this.A0D) {
            return false;
        }
        AnonymousClass4YE r0 = this.A09;
        return r0 == null || ((r0.A05 * r0.A0G) << 1) == 0;
    }

    @Override // X.AnonymousClass5Xx
    public void AZj() {
        AnonymousClass4YE r7 = this.A09;
        if (r7 != null) {
            int i = r7.A00;
            float f = r7.A0F;
            float f2 = r7.A0D;
            int i2 = r7.A05 + ((int) ((((((float) i) / (f / f2)) + ((float) r7.A06)) / (r7.A0E * f2)) + 0.5f));
            short[] sArr = r7.A0A;
            int i3 = r7.A0J << 1;
            short[] A04 = r7.A04(sArr, i, i3 + i);
            r7.A0A = A04;
            int i4 = 0;
            while (true) {
                int i5 = r7.A0G;
                if (i4 >= i3 * i5) {
                    break;
                }
                A04[(i5 * i) + i4] = 0;
                i4++;
            }
            r7.A00 += i3;
            r7.A01();
            if (r7.A05 > i2) {
                r7.A05 = i2;
            }
            r7.A00 = 0;
            r7.A09 = 0;
            r7.A06 = 0;
        }
        this.A0D = true;
    }

    @Override // X.AnonymousClass5Xx
    public void AZk(ByteBuffer byteBuffer) {
        if (byteBuffer.hasRemaining()) {
            AnonymousClass4YE r5 = this.A09;
            ShortBuffer asShortBuffer = byteBuffer.asShortBuffer();
            int remaining = byteBuffer.remaining();
            this.A03 += (long) remaining;
            int remaining2 = asShortBuffer.remaining();
            int i = r5.A0G;
            int i2 = remaining2 / i;
            short[] A04 = r5.A04(r5.A0A, r5.A00, i2);
            r5.A0A = A04;
            asShortBuffer.get(A04, r5.A00 * i, ((i * i2) << 1) >> 1);
            r5.A00 += i2;
            r5.A01();
            byteBuffer.position(byteBuffer.position() + remaining);
        }
    }

    @Override // X.AnonymousClass5Xx
    public void flush() {
        if (AJD()) {
            C94084bE r2 = this.A07;
            this.A05 = r2;
            C94084bE r1 = this.A08;
            this.A06 = r1;
            if (this.A0E) {
                this.A09 = new AnonymousClass4YE(this.A01, this.A00, r2.A03, r2.A01, r1.A03);
            } else {
                AnonymousClass4YE r12 = this.A09;
                if (r12 != null) {
                    r12.A00 = 0;
                    r12.A05 = 0;
                    r12.A06 = 0;
                    r12.A04 = 0;
                    r12.A03 = 0;
                    r12.A09 = 0;
                    r12.A08 = 0;
                    r12.A07 = 0;
                    r12.A02 = 0;
                    r12.A01 = 0;
                }
            }
        }
        this.A0B = AnonymousClass5Xx.A00;
        this.A03 = 0;
        this.A04 = 0;
        this.A0D = false;
    }

    @Override // X.AnonymousClass5Xx
    public void reset() {
        A00(this);
        this.A0E = false;
        this.A09 = null;
        this.A03 = 0;
        this.A04 = 0;
        this.A0D = false;
    }
}
