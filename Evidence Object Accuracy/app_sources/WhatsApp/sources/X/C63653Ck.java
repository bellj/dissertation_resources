package X;

/* renamed from: X.3Ck  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63653Ck {
    public final C15990oG A00;
    public final C19460u9 A01;
    public final C28941Pp A02;

    public C63653Ck(C15990oG r1, C19460u9 r2, C28941Pp r3) {
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
    }

    public static AnonymousClass1K6 A00(AnonymousClass1G4 r2) {
        r2.A03();
        C32101bc r22 = (C32101bc) r2.A00;
        AnonymousClass1K6 r1 = r22.A02;
        if (((AnonymousClass1K7) r1).A00) {
            return r1;
        }
        AnonymousClass1K6 A0G = AbstractC27091Fz.A0G(r1);
        r22.A02 = A0G;
        return A0G;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:146:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C16020oJ A01(X.C15950oC r79, X.AnonymousClass31D r80, X.AnonymousClass2LM r81, X.C15930o9 r82, boolean r83) {
        /*
        // Method dump skipped, instructions count: 1298
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C63653Ck.A01(X.0oC, X.31D, X.2LM, X.0o9, boolean):X.0oJ");
    }
}
