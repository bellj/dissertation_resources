package X;

import android.app.Activity;
import android.view.View;

/* renamed from: X.47a  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C863747a extends AbstractView$OnClickListenerC34281fs {
    public final Activity A00;
    public final C14850m9 A01;
    public final AbstractC14640lm A02;
    public final Integer A03;
    public final String A04;

    public C863747a(Activity activity, C14850m9 r2, AbstractC14640lm r3, Integer num, String str) {
        this.A00 = activity;
        this.A02 = r3;
        this.A04 = str;
        this.A01 = r2;
        this.A03 = num;
    }

    @Override // X.AbstractView$OnClickListenerC34281fs
    public void A04(View view) {
        AnonymousClass3DG r1 = new AnonymousClass3DG(this.A01, this.A02, this.A03);
        r1.A02 = this.A04;
        r1.A00(this.A00, view);
    }
}
