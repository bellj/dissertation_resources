package X;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.1Gw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C27321Gw extends AbstractC250017s {
    public final C250217u A00;
    public final C233411h A01;
    public final C21370xJ A02;
    public final C14830m7 A03;
    public final C16510p9 A04;
    public final C19990v2 A05;
    public final C233511i A06;
    public final C15860o1 A07;

    public C27321Gw(C250217u r1, C233411h r2, C21370xJ r3, C14830m7 r4, C16510p9 r5, C19990v2 r6, C233511i r7, C15860o1 r8) {
        super(r7);
        this.A03 = r4;
        this.A04 = r5;
        this.A05 = r6;
        this.A02 = r3;
        this.A00 = r1;
        this.A01 = r2;
        this.A07 = r8;
        this.A06 = r7;
    }

    public final void A08(List list) {
        AnonymousClass1PE A06;
        List<AnonymousClass1JQ> A09 = this.A06.A09("pin_v1", false);
        HashMap hashMap = new HashMap();
        for (AnonymousClass1JQ r1 : A09) {
            hashMap.put(AnonymousClass1JQ.A00(r1.A06()), r1);
        }
        Iterator it = list.iterator();
        while (it.hasNext()) {
            AnonymousClass1JQ r12 = (AnonymousClass1JQ) it.next();
            hashMap.put(AnonymousClass1JQ.A00(r12.A06()), r12);
        }
        ArrayList arrayList = new ArrayList(hashMap.values());
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            if (!((C34651gU) it2.next()).A01) {
                it2.remove();
            }
        }
        Collections.sort(arrayList, new Comparator() { // from class: X.5CE
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return (((AnonymousClass1JQ) obj2).A04 > ((AnonymousClass1JQ) obj).A04 ? 1 : (((AnonymousClass1JQ) obj2).A04 == ((AnonymousClass1JQ) obj).A04 ? 0 : -1));
            }
        });
        List<C34651gU> subList = arrayList.subList(0, Math.min(arrayList.size(), 3));
        C15860o1 r3 = this.A07;
        Set<AbstractC14640lm> A0D = r3.A0D();
        for (AbstractC14640lm r6 : A0D) {
            Iterator it3 = subList.iterator();
            while (true) {
                if (it3.hasNext()) {
                    if (((C34651gU) it3.next()).A00 == r6) {
                        break;
                    }
                } else {
                    this.A01.A06(2);
                    r3.A0A(r6, 0, false);
                    break;
                }
            }
        }
        for (C34651gU r13 : subList) {
            AbstractC14640lm r8 = r13.A00;
            if (!A0D.contains(r8)) {
                long j = r13.A04;
                r3.A09(r8, j);
                C19990v2 r14 = this.A05;
                if (r14.A0D(r8) && (A06 = r14.A06(r8)) != null && A06.A0f) {
                    A06.A0f = false;
                    this.A04.A09(A06);
                    this.A02.A00();
                    super.A00.A0F(Collections.singleton(new C34631gS(this.A00.A04(r8, false), r8, j, false)));
                }
            }
        }
        list.retainAll(subList);
    }
}
