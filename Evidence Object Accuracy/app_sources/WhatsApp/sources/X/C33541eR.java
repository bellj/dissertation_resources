package X;

import java.util.List;

/* renamed from: X.1eR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C33541eR {
    public final /* synthetic */ int A00;
    public final /* synthetic */ C23000zz A01;
    public final /* synthetic */ List A02;
    public final /* synthetic */ boolean A03;

    public C33541eR(C23000zz r1, List list, int i, boolean z) {
        this.A01 = r1;
        this.A03 = z;
        this.A00 = i;
        this.A02 = list;
    }

    public void A00(int i) {
        C23000zz r5 = this.A01;
        List<String> list = this.A02;
        if (i != -1) {
            if (i == 0 || i == 400) {
                for (String str : list) {
                    AnonymousClass164 r2 = r5.A08;
                    r2.A01(str, 3);
                    r2.A02(str, System.currentTimeMillis());
                }
            } else if (i == 500) {
                C32881ct r6 = r5.A09;
                Long A00 = r6.A00();
                if (A00 != null) {
                    r5.A0B.AbK(r5.A0C, "ToSGatingRepository/requestRefresh", A00.longValue());
                    return;
                }
                for (String str2 : list) {
                    r5.A08.A02(str2, System.currentTimeMillis());
                }
                r6.A01();
                return;
            } else {
                return;
            }
        }
        r5.A09.A01();
    }
}
