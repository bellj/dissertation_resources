package X;

/* renamed from: X.0aF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07890aF implements AbstractC12000hD {
    public final /* synthetic */ AnonymousClass0AA A00;
    public final /* synthetic */ C06430To A01;
    public final /* synthetic */ AnonymousClass0SF A02;
    public final /* synthetic */ Object A03;

    public C07890aF(AnonymousClass0AA r1, C06430To r2, AnonymousClass0SF r3, Object obj) {
        this.A00 = r1;
        this.A01 = r2;
        this.A03 = obj;
        this.A02 = r3;
    }

    @Override // X.AbstractC12000hD
    public void Aax(C05540Py r5) {
        this.A00.A0A(this.A01, this.A02, this.A03);
    }
}
