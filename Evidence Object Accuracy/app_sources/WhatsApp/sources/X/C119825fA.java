package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5fA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C119825fA extends AbstractC30891Zf {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(29);
    public long A00;
    public AbstractC1316063k A01;
    public C1316663q A02;
    public String A03;

    @Override // X.AbstractC30891Zf
    public int A05() {
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public int A06() {
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0B() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0C() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public AnonymousClass1ZR A0D() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0F() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0G() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0I() {
        return null;
    }

    @Override // X.AbstractC30891Zf
    public void A0K(int i) {
    }

    @Override // X.AbstractC30891Zf
    public void A0L(int i) {
    }

    @Override // X.AbstractC30891Zf
    public void A0M(int i) {
    }

    @Override // X.AbstractC30891Zf
    public void A0S(String str) {
    }

    @Override // X.AbstractC30891Zf
    public void A0T(String str) {
    }

    @Override // X.AbstractC30891Zf
    public void A0U(String str) {
    }

    @Override // X.AbstractC30891Zf
    public void A0V(String str) {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // X.AnonymousClass1ZP
    public void A01(AnonymousClass102 r2, AnonymousClass1V8 r3, int i) {
        try {
            A0Z(r2, r3, i);
        } catch (AnonymousClass1MW | AnonymousClass1V9 unused) {
            Log.w("PAY: NoviTransactionCountryData fromNetwork threw exception");
        }
    }

    @Override // X.AnonymousClass1ZP
    public void A02(List list, int i) {
        if (!TextUtils.isEmpty(this.A03)) {
            C117295Zj.A1O("client_request_id", this.A03, list);
        }
    }

    @Override // X.AnonymousClass1ZP
    public String A03() {
        return A0H();
    }

    @Override // X.AbstractC30891Zf, X.AnonymousClass1ZP
    public void A04(String str) {
        A0W(str, 0);
    }

    @Override // X.AbstractC30891Zf
    public long A07() {
        return this.A00;
    }

    @Override // X.AbstractC30891Zf
    public long A08() {
        AbstractC1316063k r0 = this.A01;
        if (r0 != null) {
            return r0.A01;
        }
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public long A09() {
        AbstractC1316063k r0 = this.A01;
        if (r0 != null) {
            return r0.A00;
        }
        return 0;
    }

    @Override // X.AbstractC30891Zf
    public String A0E() {
        AbstractC1316063k r0 = this.A01;
        if (r0 != null) {
            return r0.A05;
        }
        return null;
    }

    @Override // X.AbstractC30891Zf
    public String A0H() {
        try {
            JSONObject A0J = A0J();
            long j = this.A00;
            if (j > 0) {
                A0J.put("expiryTs", j);
            }
            String str = this.A03;
            if (str != null) {
                A0J.put("client_request_id", str);
            }
            AbstractC1316063k r0 = this.A01;
            if (r0 != null) {
                A0J.put("transaction", r0.A04());
            }
            C1316663q r02 = this.A02;
            if (r02 != null) {
                A0J.put("step-up", r02.A01());
            }
            return A0J.toString();
        } catch (JSONException unused) {
            Log.w("PAY: NoviTransactionCountryData toDBString threw exception");
            return null;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0N(long j) {
        this.A00 = j;
    }

    @Override // X.AbstractC30891Zf
    public void A0O(long j) {
        AbstractC1316063k r0 = this.A01;
        if (r0 != null) {
            r0.A01 = j;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0Q(AnonymousClass102 r6, AnonymousClass1IR r7, AnonymousClass1V8 r8, int i) {
        try {
            A0Z(r6, r8, i);
            AbstractC1316063k r1 = this.A01;
            r7.A08 = r1.A02();
            String A03 = r1.A03();
            r7.A0I = A03;
            r7.A07 = r6.A02(A03);
            long j = this.A01.A00;
            if (j > 0) {
                r7.A06 = j / 1000;
            }
        } catch (AnonymousClass1MW | AnonymousClass1V9 unused) {
            Log.w("PAY: NoviTransactionCountryData fromNetwork threw exception");
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0R(AbstractC30891Zf r6) {
        super.A0R(r6);
        C119825fA r62 = (C119825fA) r6;
        long j = r62.A00;
        if (j > 0) {
            this.A00 = j;
        }
        String str = r62.A03;
        if (str != null) {
            this.A03 = str;
        }
        AbstractC1316063k r0 = r62.A01;
        if (r0 != null) {
            this.A01 = r0;
        }
    }

    @Override // X.AbstractC30891Zf
    public void A0W(String str, int i) {
        C121035h9 r0;
        C120985h4 r02;
        AbstractC1316063k r03;
        C120995h5 r04;
        super.A04(str);
        try {
            JSONObject A05 = C13000ix.A05(str);
            this.A00 = A05.optLong("expiryTs", this.A00);
            this.A03 = A05.optString("client_request_id", this.A03);
            this.A02 = C1316663q.A00(A05.optString("step-up", ""));
            String optString = C13000ix.A05(str).optString("transaction", "");
            if (i == 6) {
                if (!TextUtils.isEmpty(optString)) {
                    try {
                        r0 = new C121035h9(optString);
                    } catch (JSONException unused) {
                        Log.w("PAY:NoviDepositTransaction failed to create transaction from the JSON");
                    }
                    this.A01 = r0;
                    return;
                }
                r0 = null;
                this.A01 = r0;
                return;
            } else if (i == 7) {
                if (!TextUtils.isEmpty(optString)) {
                    try {
                        r02 = new C120985h4(optString);
                    } catch (JSONException unused2) {
                        Log.w("PAY:NoviTransactionRefund/fromJsonString: Error while creating a refund from a JSON");
                    }
                    this.A01 = r02;
                    return;
                }
                r02 = null;
                this.A01 = r02;
                return;
            } else if (i != 8) {
                if (!TextUtils.isEmpty(optString)) {
                    try {
                        r04 = new C120995h5(optString);
                    } catch (JSONException e) {
                        Log.w("PAY:NoviTransactionP2P/fromJsonString: Error while creating a transaction from a JSON", e);
                    }
                    this.A01 = r04;
                    return;
                }
                r04 = null;
                this.A01 = r04;
                return;
            } else {
                AbstractC1316063k r2 = null;
                if (!TextUtils.isEmpty(optString)) {
                    try {
                        int i2 = C13000ix.A05(optString).getInt("type");
                        if (i2 == 1) {
                            r03 = new C121015h7(optString);
                        } else if (i2 == 2) {
                            r03 = new C121005h6(optString);
                        }
                        r2 = r03;
                    } catch (JSONException unused3) {
                        Log.w("PAY:NoviTransactionWithdrawal/fromJsonString: Error while creating a withdrawal from a JSON");
                    }
                }
                this.A01 = r2;
                return;
            }
        } catch (JSONException unused4) {
            Log.w("PAY: NoviTransactionCountryData fromDBString(String,int) threw exception");
        }
        Log.w("PAY: NoviTransactionCountryData fromDBString(String,int) threw exception");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00a2, code lost:
        if ("true".equals(r22.A0I("is_unilateral", null)) == false) goto L_0x00a4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A0Z(X.AnonymousClass102 r21, X.AnonymousClass1V8 r22, int r23) {
        /*
            r20 = this;
            r1 = 6
            r0 = r20
            r15 = r22
            r3 = r23
            r2 = r21
            if (r3 == r1) goto L_0x00d5
            r1 = 7
            if (r3 == r1) goto L_0x00cd
            r1 = 8
            if (r3 == r1) goto L_0x00c6
            java.lang.String r4 = "sender-info"
            X.1V8 r1 = r15.A0F(r4)
            java.lang.String r6 = "phone_number"
            r3 = 0
            java.lang.String r16 = r1.A0I(r6, r3)
            java.lang.String r5 = "receiver-info"
            X.1V8 r1 = r15.A0F(r5)
            java.lang.String r17 = r1.A0I(r6, r3)
            java.lang.String r1 = "receiver"
            java.lang.String r1 = r15.A0I(r1, r3)
            com.whatsapp.jid.UserJid r7 = com.whatsapp.jid.UserJid.getNullable(r1)
            java.lang.String r1 = "quote"
            X.1V8 r1 = r15.A0F(r1)
            X.63p r9 = X.C1315863i.A00(r2, r1)
            java.lang.String r1 = "note"
            java.lang.String r18 = r15.A0I(r1, r3)
            X.1V8 r4 = r15.A0F(r4)
            java.lang.String r1 = "transaction-amount"
            X.1V8 r4 = r4.A0F(r1)
            X.63n r12 = X.C1316363n.A00(r2, r4)
            X.1V8 r4 = r15.A0F(r5)
            X.1V8 r4 = r4.A0F(r1)
            X.63n r13 = X.C1316363n.A00(r2, r4)
            java.lang.String r4 = "claim"
            X.1V8 r4 = r15.A0E(r4)
            X.63o r6 = X.C1316463o.A00(r4)
            java.lang.String r4 = "id"
            java.lang.String r5 = r15.A0H(r4)
            java.lang.String r4 = "transaction"
            X.1V8 r4 = r15.A0E(r4)
            X.5h9 r10 = X.C121035h9.A00(r2, r4, r5)
            java.lang.String r4 = "balance_debit"
            X.1V8 r4 = r15.A0E(r4)
            if (r4 == 0) goto L_0x00c4
            X.6F2 r8 = X.AnonymousClass6F2.A00(r2, r4)
        L_0x0083:
            java.lang.String r4 = "refund_transaction"
            X.1V8 r4 = r15.A0E(r4)
            X.63r r11 = X.C1316763r.A01(r4)
            java.lang.String r4 = "is_unilateral"
            r5 = 0
            java.lang.String r3 = r15.A0I(r4, r3)
            if (r3 == 0) goto L_0x00a4
            java.lang.String r4 = r15.A0I(r4, r5)
            java.lang.String r3 = "true"
            boolean r3 = r3.equals(r4)
            r19 = 1
            if (r3 != 0) goto L_0x00a6
        L_0x00a4:
            r19 = 0
        L_0x00a6:
            java.lang.String r3 = "final-receiver-info"
            X.1V8 r4 = r15.A0E(r3)
            if (r4 == 0) goto L_0x00c2
            X.1V8 r3 = r15.A0F(r3)
            X.1V8 r1 = r3.A0F(r1)
            X.63n r14 = X.C1316363n.A00(r2, r1)
        L_0x00ba:
            X.5h5 r5 = new X.5h5
            r5.<init>(r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
            r0.A01 = r5
            return
        L_0x00c2:
            r14 = 0
            goto L_0x00ba
        L_0x00c4:
            r8 = 0
            goto L_0x0083
        L_0x00c6:
            X.5h8 r1 = X.AbstractC121025h8.A00(r2, r15)
            r0.A01 = r1
            return
        L_0x00cd:
            X.5h4 r1 = new X.5h4
            r1.<init>(r2, r15)
            r0.A01 = r1
            return
        L_0x00d5:
            r1 = 0
            X.5h9 r1 = X.C121035h9.A00(r2, r15, r1)
            r0.A01 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C119825fA.A0Z(X.102, X.1V8, int):void");
    }

    @Override // X.AbstractC30891Zf, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.A03);
        parcel.writeParcelable(this.A01, i);
    }
}
