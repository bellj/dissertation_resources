package X;

import com.whatsapp.biz.BusinessProfileExtraFieldsActivity;
import java.util.Set;

/* renamed from: X.44S  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass44S extends AbstractC33331dp {
    public final /* synthetic */ BusinessProfileExtraFieldsActivity A00;

    public AnonymousClass44S(BusinessProfileExtraFieldsActivity businessProfileExtraFieldsActivity) {
        this.A00 = businessProfileExtraFieldsActivity;
    }

    @Override // X.AbstractC33331dp
    public void A00(Set set) {
        this.A00.A2e();
    }
}
