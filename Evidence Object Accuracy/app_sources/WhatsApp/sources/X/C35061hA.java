package X;

import android.app.Notification;
import android.content.Context;
import android.os.Build;
import com.whatsapp.util.Log;

/* renamed from: X.1hA  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C35061hA extends AbstractC35031h7 {
    @Override // X.AbstractC35031h7
    public void A01(Notification notification, Context context, int i) {
        if (Build.VERSION.SDK_INT >= 19 && Build.MANUFACTURER.equalsIgnoreCase("Xiaomi")) {
            try {
                Object obj = notification.getClass().getDeclaredField("extraNotification").get(notification);
                obj.getClass().getDeclaredMethod("setMessageCount", Integer.TYPE).invoke(obj, Integer.valueOf(i));
            } catch (Exception unused) {
                Log.e("Could not set badge for Xiaomi notification");
            }
        }
    }
}
