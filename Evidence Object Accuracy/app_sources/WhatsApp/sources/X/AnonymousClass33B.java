package X;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.whatsapp.R;
import com.whatsapp.emoji.EmojiDescriptor;
import org.json.JSONObject;

/* renamed from: X.33B  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass33B extends AnonymousClass45J {
    public Drawable A00;
    public C37471mS A01;
    public final Context A02;
    public final AnonymousClass19M A03;
    public final boolean A04;

    @Override // X.AbstractC454821u
    public String A0G() {
        return "emoji";
    }

    @Override // X.AbstractC454821u
    public boolean A0J() {
        return false;
    }

    @Override // X.AbstractC454821u
    public boolean A0K() {
        return false;
    }

    public AnonymousClass33B(Context context, AnonymousClass19M r4, JSONObject jSONObject) {
        this.A02 = context;
        this.A03 = r4;
        this.A04 = false;
        if (jSONObject.has("emoji")) {
            this.A01 = new C37471mS(jSONObject.getString("emoji"));
            A0S(true);
            super.A0A(jSONObject);
        }
    }

    public AnonymousClass33B(Context context, C37471mS r3, AnonymousClass19M r4, boolean z) {
        this.A01 = r3;
        this.A02 = context;
        this.A03 = r4;
        this.A04 = z;
        A0S(false);
    }

    @Override // X.AbstractC454821u
    public Drawable A0F() {
        return this.A00;
    }

    @Override // X.AbstractC454821u
    public String A0H(Context context) {
        C37471mS r0 = this.A01;
        return r0 == null ? context.getString(R.string.emoji_button_description) : r0.toString();
    }

    @Override // X.AbstractC454821u
    public void A0I(Canvas canvas) {
        A0P(canvas);
    }

    @Override // X.AbstractC454821u
    public void A0N(JSONObject jSONObject) {
        super.A0N(jSONObject);
        C37471mS r0 = this.A01;
        if (r0 != null) {
            jSONObject.put("emoji", r0.toString());
        }
    }

    @Override // X.AbstractC454821u
    public void A0P(Canvas canvas) {
        Drawable drawable = this.A00;
        if (drawable != null) {
            RectF rectF = super.A02;
            drawable.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
            canvas.save();
            C12990iw.A12(canvas, rectF, super.A00);
            this.A00.draw(canvas);
            canvas.restore();
        }
    }

    @Override // X.AnonymousClass45J, X.AbstractC454821u
    public void A0Q(RectF rectF, float f, float f2, float f3, float f4) {
        super.A0Q(rectF, f, f2, f3, f4);
        if (!this.A04) {
            RectF rectF2 = super.A02;
            if (rectF2.width() > 128.0f || rectF2.height() > 128.0f) {
                A06(Math.min(128.0f / rectF2.width(), 128.0f / rectF2.height()));
            }
        }
    }

    @Override // X.AnonymousClass45J
    public float A0R() {
        Drawable drawable = this.A00;
        if (drawable == null) {
            return 0.0f;
        }
        return ((float) drawable.getIntrinsicWidth()) / ((float) this.A00.getIntrinsicHeight());
    }

    public final void A0S(boolean z) {
        Drawable A05;
        C37471mS r0 = this.A01;
        if (r0 != null) {
            C39511q1 r5 = new C39511q1(r0.A00);
            long A00 = EmojiDescriptor.A00(r5, false);
            if (this.A04) {
                A05 = this.A03.A05(this.A02.getResources(), r5, A00);
            } else if (z) {
                AnonymousClass19M r4 = this.A03;
                Resources resources = this.A02.getResources();
                C39541q4 A06 = r4.A06(r5, A00);
                if (A06 == null) {
                    A05 = null;
                } else {
                    A05 = AnonymousClass19M.A01(resources, A06, null, r4.A01);
                    if (A05 == null) {
                        A05 = AnonymousClass19M.A01(resources, A06, new C39571q7(r4), r4.A02);
                    }
                }
            } else {
                A05 = this.A03.A03(this.A02.getResources(), new AnonymousClass56T(this), r5, A00);
            }
            this.A00 = A05;
        }
    }
}
