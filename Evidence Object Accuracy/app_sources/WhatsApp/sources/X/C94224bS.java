package X;

/* renamed from: X.4bS  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C94224bS {
    public static final C94224bS A02;
    public static final C94224bS A03;
    public final long A00;
    public final long A01;

    static {
        C94224bS r5 = new C94224bS(0, 0);
        A03 = r5;
        new C94224bS(Long.MAX_VALUE, Long.MAX_VALUE);
        new C94224bS(Long.MAX_VALUE, 0);
        new C94224bS(0, Long.MAX_VALUE);
        A02 = r5;
    }

    public C94224bS(long j, long j2) {
        boolean z = true;
        C95314dV.A03(C12990iw.A1W((j > 0 ? 1 : (j == 0 ? 0 : -1))));
        C95314dV.A03(j2 < 0 ? false : z);
        this.A01 = j;
        this.A00 = j2;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || C94224bS.class != obj.getClass()) {
                return false;
            }
            C94224bS r7 = (C94224bS) obj;
            if (!(this.A01 == r7.A01 && this.A00 == r7.A00)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return (((int) this.A01) * 31) + ((int) this.A00);
    }
}
