package X;

/* renamed from: X.0XK  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0XK implements AbstractC12280hf {
    public final /* synthetic */ AnonymousClass0XQ A00;

    public AnonymousClass0XK(AnonymousClass0XQ r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12280hf
    public void AOF(AnonymousClass07H r3, boolean z) {
        if (r3 instanceof AnonymousClass0CK) {
            r3.A01().A0F(false);
        }
        AbstractC12280hf r0 = this.A00.A0B;
        if (r0 != null) {
            r0.AOF(r3, z);
        }
    }

    @Override // X.AbstractC12280hf
    public boolean ATF(AnonymousClass07H r4) {
        AnonymousClass0XQ r2 = this.A00;
        if (r4 == r2.A0A) {
            return false;
        }
        ((AnonymousClass0CK) r4).getItem().getItemId();
        AbstractC12280hf r0 = r2.A0B;
        if (r0 != null) {
            return r0.ATF(r4);
        }
        return false;
    }
}
