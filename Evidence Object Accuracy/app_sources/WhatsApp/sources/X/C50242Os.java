package X;

import java.util.List;

/* renamed from: X.2Os  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C50242Os {
    public final int A00;
    public final AnonymousClass1JA A01;
    public final List A02;
    public final boolean A03;

    public C50242Os(AnonymousClass1JA r1, List list, int i, boolean z) {
        this.A01 = r1;
        this.A02 = list;
        this.A03 = z;
        this.A00 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r3 == X.AnonymousClass1JA.A01) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C50242Os A00(X.AnonymousClass1JA r3, com.whatsapp.jid.UserJid r4, java.lang.String r5, int r6, boolean r7) {
        /*
            X.1JA r0 = X.AnonymousClass1JA.A0C
            r2 = 1
            if (r3 == r0) goto L_0x000a
            X.1JA r1 = X.AnonymousClass1JA.A01
            r0 = 0
            if (r3 != r1) goto L_0x000b
        L_0x000a:
            r0 = 1
        L_0x000b:
            X.AnonymousClass009.A0F(r0)
            X.1Mg r0 = new X.1Mg
            r0.<init>(r4, r5)
            r0.A09 = r2
            r0.A0I = r2
            r0.A0G = r2
            r0.A08 = r2
            r0.A0C = r2
            r0.A0E = r7
            X.1uf r0 = r0.A01()
            java.util.List r1 = java.util.Collections.singletonList(r0)
            X.2Os r0 = new X.2Os
            r0.<init>(r3, r1, r6, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C50242Os.A00(X.1JA, com.whatsapp.jid.UserJid, java.lang.String, int, boolean):X.2Os");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[mode=");
        AnonymousClass1JA r2 = this.A01;
        sb.append(r2.mode.modeString);
        StringBuilder sb2 = new StringBuilder(sb.toString());
        sb2.append(" context=");
        sb2.append(r2.context.contextString);
        sb2.append(" requests=");
        sb2.append(this.A02.size());
        sb2.append("]");
        return sb2.toString();
    }
}
