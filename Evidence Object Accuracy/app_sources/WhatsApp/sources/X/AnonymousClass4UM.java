package X;

import android.graphics.Bitmap;
import java.io.IOException;
import java.io.OutputStream;

/* renamed from: X.4UM  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass4UM {
    public C08870bz A00(Bitmap.Config config, int i, int i2) {
        OutputStream outputStream;
        C75843kY r6 = (C75843kY) this;
        if (r6.A00) {
            AnonymousClass4I1 r0 = r6.A02;
            Bitmap createBitmap = Bitmap.createBitmap(i, i2, config);
            C105964uu r1 = C105964uu.A00;
            if (r1 == null) {
                r1 = new C105964uu();
                C105964uu.A00 = r1;
            }
            AbstractC12230ha r02 = r0.A00;
            if (createBitmap != null) {
                return new C08870bz(r02, r1, createBitmap);
            }
            return null;
        }
        try {
            short s = (short) i;
            short s2 = (short) i2;
            outputStream = null;
            try {
                AbstractC11590gX r8 = r6.A01.A00;
                byte[] bArr = C93234Zq.A01;
                int length = bArr.length;
                byte[] bArr2 = C93234Zq.A02;
                C75823kW r12 = new C75823kW(((C105944us) r8).A01, length + bArr2.length + 4);
                r12.write(bArr);
                r12.write((byte) (s2 >> 8));
                r12.write((byte) (s2 & 255));
                r12.write((byte) (s >> 8));
                r12.write((byte) (s & 255));
                r12.write(bArr2);
                if (C08870bz.A01(r12.A01)) {
                    C08870bz A00 = C08870bz.A00(C08870bz.A05, new C105934ur(r12.A01, r12.A00));
                    r12.close();
                    try {
                        AnonymousClass5B9 r4 = new AnonymousClass5B9(A00);
                        r4.A00 = C88704Gs.A01;
                        C08870bz A01 = r6.A03.A01(config, r4, ((AbstractC12930in) A00.A04()).size());
                        if (!((Bitmap) A01.A04()).isMutable()) {
                            A01.close();
                            r6.A00 = true;
                            AbstractC12710iN r13 = AnonymousClass0UN.A00;
                            if (r13.AJi(6)) {
                                r13.AgJ("HoneycombBitmapFactory", "Immutable bitmap returned by decoder");
                            }
                            AnonymousClass4I1 r03 = r6.A02;
                            Bitmap createBitmap2 = Bitmap.createBitmap(i, i2, config);
                            C105964uu r14 = C105964uu.A00;
                            if (r14 == null) {
                                r14 = new C105964uu();
                                C105964uu.A00 = r14;
                            }
                            AbstractC12230ha r04 = r03.A00;
                            A01 = null;
                            if (createBitmap2 != null) {
                                A01 = new C08870bz(r04, r14, createBitmap2);
                            }
                        } else {
                            ((Bitmap) A01.A04()).setHasAlpha(true);
                            ((Bitmap) A01.A04()).eraseColor(0);
                        }
                        r4.close();
                        return A01;
                    } finally {
                        A00.close();
                    }
                } else {
                    throw new C113205Go();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (Throwable th) {
            if (0 != 0) {
                outputStream.close();
            }
            throw th;
        }
    }
}
