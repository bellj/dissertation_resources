package X;

import android.view.View;
import com.whatsapp.R;

/* renamed from: X.3jw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75493jw extends AnonymousClass03U {
    public final View A00;
    public final View A01;

    public /* synthetic */ C75493jw(View view) {
        super(view);
        this.A00 = view.findViewById(R.id.empty_space_view);
        this.A01 = view.findViewById(R.id.top_highlight);
    }
}
