package X;

import android.view.View;
import com.whatsapp.WaImageButton;

/* renamed from: X.3hr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74363hr extends AnonymousClass04v {
    public final /* synthetic */ WaImageButton A00;
    public final /* synthetic */ C60872ym A01;

    public C74363hr(WaImageButton waImageButton, C60872ym r2) {
        this.A01 = r2;
        this.A00 = waImageButton;
    }

    @Override // X.AnonymousClass04v
    public void A06(View view, AnonymousClass04Z r3) {
        super.A06(view, r3);
        r3.A07(this.A00);
    }
}
