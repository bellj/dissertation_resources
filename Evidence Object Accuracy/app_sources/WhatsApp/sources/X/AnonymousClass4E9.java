package X;

import com.whatsapp.voipcalling.Voip;

/* renamed from: X.4E9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4E9 {
    public static boolean A00(AnonymousClass3HK r2) {
        Voip.CallState callState = r2.A05;
        return callState == Voip.CallState.PRE_ACCEPT_RECEIVED || callState == Voip.CallState.CALLING || callState == Voip.CallState.RECEIVED_CALL || callState == Voip.CallState.REJOINING || callState == Voip.CallState.ACCEPT_SENT || r2.A0B;
    }
}
