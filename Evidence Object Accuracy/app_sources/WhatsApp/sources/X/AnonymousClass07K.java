package X;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.AutoCompleteTextView;
import com.whatsapp.R;

/* renamed from: X.07K  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass07K extends AutoCompleteTextView implements AnonymousClass012 {
    public static final int[] A02 = {16843126};
    public final AnonymousClass085 A00;
    public final AnonymousClass086 A01;

    public AnonymousClass07K(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.autoCompleteTextViewStyle);
    }

    public AnonymousClass07K(Context context, AttributeSet attributeSet, int i) {
        super(AnonymousClass083.A00(context), attributeSet, i);
        AnonymousClass084.A03(getContext(), this);
        C013406h A00 = C013406h.A00(getContext(), attributeSet, A02, i, 0);
        if (A00.A02.hasValue(0)) {
            setDropDownBackgroundDrawable(A00.A02(0));
        }
        A00.A04();
        AnonymousClass085 r0 = new AnonymousClass085(this);
        this.A00 = r0;
        r0.A05(attributeSet, i);
        AnonymousClass086 r02 = new AnonymousClass086(this);
        this.A01 = r02;
        r02.A0A(attributeSet, i);
        r02.A02();
    }

    @Override // android.widget.TextView, android.view.View
    public void drawableStateChanged() {
        super.drawableStateChanged();
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A00();
        }
        AnonymousClass086 r02 = this.A01;
        if (r02 != null) {
            r02.A02();
        }
    }

    @Override // X.AnonymousClass012
    public ColorStateList getSupportBackgroundTintList() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A00;
    }

    @Override // X.AnonymousClass012
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        C016107p r0;
        AnonymousClass085 r02 = this.A00;
        if (r02 == null || (r0 = r02.A01) == null) {
            return null;
        }
        return r0.A01;
    }

    @Override // android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        AnonymousClass08D.A00(this, editorInfo, onCreateInputConnection);
        return onCreateInputConnection;
    }

    @Override // android.view.View
    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A01();
        }
    }

    @Override // android.view.View
    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A02(i);
        }
    }

    @Override // android.widget.TextView
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(AnonymousClass04D.A02(callback, this));
    }

    @Override // android.widget.AutoCompleteTextView
    public void setDropDownBackgroundResource(int i) {
        setDropDownBackgroundDrawable(C012005t.A01().A04(getContext(), i));
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A03(colorStateList);
        }
    }

    @Override // X.AnonymousClass012
    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        AnonymousClass085 r0 = this.A00;
        if (r0 != null) {
            r0.A04(mode);
        }
    }

    @Override // android.widget.TextView
    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        AnonymousClass086 r0 = this.A01;
        if (r0 != null) {
            r0.A05(context, i);
        }
    }
}
