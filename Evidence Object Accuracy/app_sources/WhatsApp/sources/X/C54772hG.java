package X;

import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.CatalogImageListActivity;

/* renamed from: X.2hG  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54772hG extends AbstractC05270Ox {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ int A02;
    public final /* synthetic */ AbstractC005102i A03;
    public final /* synthetic */ CatalogImageListActivity A04;

    public C54772hG(AbstractC005102i r1, CatalogImageListActivity catalogImageListActivity, int i, int i2, int i3) {
        this.A04 = catalogImageListActivity;
        this.A00 = i;
        this.A01 = i2;
        this.A03 = r1;
        this.A02 = i3;
    }

    @Override // X.AbstractC05270Ox
    public void A01(RecyclerView recyclerView, int i, int i2) {
        CatalogImageListActivity catalogImageListActivity = this.A04;
        float f = 1.0f;
        if (catalogImageListActivity.A02.A1A() == 0) {
            int top = catalogImageListActivity.A02.A0C(0).getTop();
            int i3 = catalogImageListActivity.A04.A01;
            f = Math.min(Math.max(0.0f, ((float) (i3 - top)) / ((float) i3)), 1.0f);
        }
        int i4 = this.A00;
        int i5 = this.A01;
        this.A03.A0C(new ColorDrawable(C016907y.A03(f, i4, i5)));
        if (CatalogImageListActivity.A0B) {
            catalogImageListActivity.getWindow().setStatusBarColor(C016907y.A03(f, this.A02, i5));
        }
    }
}
