package X;

import android.util.Log;

/* renamed from: X.4wo  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107114wo implements AnonymousClass5X7 {
    public int A00;
    public int A01;
    public long A02;
    public AnonymousClass5X6 A03;
    public boolean A04;
    public final C95304dT A05 = C95304dT.A05(10);

    @Override // X.AnonymousClass5X7
    public void A7a(C95304dT r9) {
        C95314dV.A01(this.A03);
        if (this.A04) {
            int i = r9.A00;
            int i2 = r9.A01;
            int i3 = i - i2;
            int i4 = this.A00;
            if (i4 < 10) {
                int min = Math.min(i3, 10 - i4);
                byte[] bArr = r9.A02;
                C95304dT r4 = this.A05;
                System.arraycopy(bArr, i2, r4.A02, i4, min);
                if (this.A00 + min == 10) {
                    r4.A0S(0);
                    if (73 == r4.A0C() && 68 == r4.A0C() && 51 == r4.A0C()) {
                        r4.A0T(3);
                        this.A01 = r4.A0B() + 10;
                    } else {
                        Log.w("Id3Reader", "Discarding invalid ID3 tag");
                        this.A04 = false;
                        return;
                    }
                }
            }
            int A02 = C72463ee.A02(this.A01, this.A00, i3);
            this.A03.AbC(r9, A02);
            this.A00 += A02;
        }
    }

    @Override // X.AnonymousClass5X7
    public void A8b(AbstractC14070ko r4, C92824Xo r5) {
        r5.A03();
        AnonymousClass5X6 Af4 = r4.Af4(r5.A01(), 5);
        this.A03 = Af4;
        C93844ap A00 = C93844ap.A00();
        A00.A0O = r5.A02();
        A00.A0R = "application/id3";
        C72453ed.A17(A00, Af4);
    }

    @Override // X.AnonymousClass5X7
    public void AYo() {
        int i;
        AnonymousClass5X6 r1 = this.A03;
        C95314dV.A01(r1);
        if (this.A04 && (i = this.A01) != 0 && this.A00 == i) {
            r1.AbG(null, 1, i, 0, this.A02);
            this.A04 = false;
        }
    }

    @Override // X.AnonymousClass5X7
    public void AYp(long j, int i) {
        if ((i & 4) != 0) {
            this.A04 = true;
            this.A02 = j;
            this.A01 = 0;
            this.A00 = 0;
        }
    }

    @Override // X.AnonymousClass5X7
    public void AbP() {
        this.A04 = false;
    }
}
