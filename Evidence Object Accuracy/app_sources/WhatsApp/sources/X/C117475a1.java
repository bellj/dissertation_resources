package X;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import com.whatsapp.payments.ui.IndiaUpiEditTransactionDescriptionFragment;

/* renamed from: X.5a1  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C117475a1 extends ClickableSpan {
    public final /* synthetic */ IndiaUpiEditTransactionDescriptionFragment A00;

    public C117475a1(IndiaUpiEditTransactionDescriptionFragment indiaUpiEditTransactionDescriptionFragment) {
        this.A00 = indiaUpiEditTransactionDescriptionFragment;
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        IndiaUpiEditTransactionDescriptionFragment indiaUpiEditTransactionDescriptionFragment = this.A00;
        indiaUpiEditTransactionDescriptionFragment.A07.AKi(null, C12960it.A0V(), 9, "payment_description", null);
        indiaUpiEditTransactionDescriptionFragment.A0v(C117295Zj.A04("https://faq.whatsapp.com/general/payments/about-the-security-of-your-payment-descriptions"));
    }

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
        C117295Zj.A0k(this.A00.A02(), textPaint);
    }
}
