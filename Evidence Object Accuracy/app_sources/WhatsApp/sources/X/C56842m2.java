package X;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.UnsafeUtil;

/* renamed from: X.2m2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C56842m2 extends CodedOutputStream {
    public int A00;
    public final int A01;
    public final byte[] A02;

    public C56842m2(byte[] bArr, int i) {
        int length = bArr.length;
        int i2 = 0 + i;
        if ((0 | i | (length - i2)) >= 0) {
            this.A02 = bArr;
            this.A00 = 0;
            this.A01 = i2;
            return;
        }
        Object[] objArr = new Object[3];
        C12960it.A1P(objArr, length, 0);
        C12960it.A1P(objArr, 0, 1);
        C12960it.A1P(objArr, i, 2);
        throw C12970iu.A0f(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", objArr));
    }

    public final void A0O(long j) {
        if (CodedOutputStream.A02) {
            int i = this.A01;
            int i2 = this.A00;
            if (i - i2 >= 10) {
                long j2 = CodedOutputStream.A00 + ((long) i2);
                while (true) {
                    int i3 = ((j & -128) > 0 ? 1 : ((j & -128) == 0 ? 0 : -1));
                    byte[] bArr = this.A02;
                    if (i3 == 0) {
                        UnsafeUtil.A00(bArr, (byte) ((int) j), j2);
                        this.A00++;
                        return;
                    }
                    UnsafeUtil.A00(bArr, (byte) ((((int) j) & 127) | 128), j2);
                    this.A00++;
                    j >>>= 7;
                    j2 = 1 + j2;
                }
            }
        }
        while ((j & -128) != 0) {
            try {
                byte[] bArr2 = this.A02;
                int i4 = this.A00;
                this.A00 = i4 + 1;
                bArr2[i4] = (byte) ((((int) j) & 127) | 128);
                j >>>= 7;
            } catch (IndexOutOfBoundsException e) {
                Object[] objArr = new Object[3];
                C12960it.A1P(objArr, this.A00, 0);
                C12960it.A1P(objArr, this.A01, 1);
                C12960it.A1P(objArr, 1, 2);
                throw new AnonymousClass494(String.format("Pos: %d, limit: %d, len: %d", objArr), e);
            }
        }
        byte[] bArr3 = this.A02;
        int i5 = this.A00;
        this.A00 = i5 + 1;
        bArr3[i5] = (byte) ((int) j);
    }
}
