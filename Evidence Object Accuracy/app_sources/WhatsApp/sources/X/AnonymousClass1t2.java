package X;

import java.io.File;

/* renamed from: X.1t2  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1t2 extends AbstractC38641oR {
    public final File A00;
    public final File A01;
    public final String A02;
    public final String A03;
    public final String A04;
    public final String A05;
    public final /* synthetic */ C234511s A06;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1t2(X.C234511s r14, X.AnonymousClass1K3 r15, java.lang.String r16) {
        /*
            r13 = this;
            r5 = r13
            r13.A06 = r14
            X.0m9 r9 = r14.A03
            X.0t3 r7 = r14.A01
            X.0nH r6 = r14.A00
            X.0mJ r8 = r14.A02
            X.0zL r11 = r14.A06
            X.0vE r10 = r14.A05
            r12 = 0
            r5.<init>(r6, r7, r8, r9, r10, r11, r12)
            X.0xy r0 = r14.A04     // Catch: IOException -> 0x0088
            X.1Hj r2 = r0.A00     // Catch: IOException -> 0x0088
            java.lang.String r1 = ""
            java.io.File r0 = r2.A00(r1)     // Catch: IOException -> 0x0088
            r13.A01 = r0     // Catch: IOException -> 0x0088
            java.io.File r0 = r2.A00(r1)     // Catch: IOException -> 0x0088
            r13.A00 = r0     // Catch: IOException -> 0x0088
            int r3 = r15.A00
            r1 = 1
            r0 = r3 & r1
            r2 = r16
            if (r0 != r1) goto L_0x0080
            r1 = 2
            r0 = r3 & r1
            if (r0 != r1) goto L_0x0078
            r1 = 16
            r0 = r3 & r1
            if (r0 != r1) goto L_0x0070
            r0 = 32
            r3 = r3 & r0
            if (r3 != r0) goto L_0x0068
            X.1Jp r0 = r15.A04
            byte[] r4 = r0.A04()
            X.1Jp r0 = r15.A03
            byte[] r3 = r0.A04()
            X.1Jp r0 = r15.A02
            byte[] r2 = r0.A04()
            java.lang.String r0 = r15.A05
            r13.A03 = r0
            r1 = 0
            java.lang.String r0 = android.util.Base64.encodeToString(r4, r1)
            r13.A02 = r0
            java.lang.String r0 = android.util.Base64.encodeToString(r3, r1)
            r13.A05 = r0
            java.lang.String r0 = android.util.Base64.encodeToString(r2, r1)
            r13.A04 = r0
            return
        L_0x0068:
            r1 = 52
            X.1K1 r0 = new X.1K1
            r0.<init>(r1, r2)
            throw r0
        L_0x0070:
            r1 = 51
            X.1K1 r0 = new X.1K1
            r0.<init>(r1, r2)
            throw r0
        L_0x0078:
            r1 = 50
            X.1K1 r0 = new X.1K1
            r0.<init>(r1, r2)
            throw r0
        L_0x0080:
            r1 = 49
            X.1K1 r0 = new X.1K1
            r0.<init>(r1, r2)
            throw r0
        L_0x0088:
            r2 = move-exception
            java.lang.String r0 = "external-mutations-downloader: Failed to prepare location for encryptedFile/destinationFile"
            com.whatsapp.util.Log.e(r0)
            java.lang.String r1 = "Failed to prepare location for encryptedFile/destinationFile "
            X.1KE r0 = new X.1KE
            r0.<init>(r1, r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1t2.<init>(X.11s, X.1K3, java.lang.String):void");
    }
}
