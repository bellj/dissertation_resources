package X;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.3BW  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass3BW {
    public Integer A00;
    public final C108184yd A01;
    public final String A02;
    public final String A03;
    public final Map A04;
    public final Set A05;
    public final Set A06;

    public AnonymousClass3BW(C108184yd r4, String str, String str2, Map map, Set set) {
        Set unmodifiableSet;
        if (set == null) {
            unmodifiableSet = Collections.emptySet();
        } else {
            unmodifiableSet = Collections.unmodifiableSet(set);
        }
        this.A05 = unmodifiableSet;
        map = map == null ? Collections.emptyMap() : map;
        this.A04 = map;
        this.A02 = str;
        this.A03 = str2;
        this.A01 = r4 == null ? C108184yd.A00 : r4;
        HashSet hashSet = new HashSet(unmodifiableSet);
        Iterator A0o = C12960it.A0o(map);
        if (A0o.hasNext()) {
            A0o.next();
            throw C12980iv.A0n("zaa");
        } else {
            this.A06 = Collections.unmodifiableSet(hashSet);
        }
    }
}
