package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import com.whatsapp.status.playback.content.BlurFrameLayout;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;

/* renamed from: X.1Nt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28521Nt extends AbstractC16350or {
    public RenderScript A00;
    public ScriptIntrinsicBlur A01;
    public final WeakReference A02;

    public C28521Nt(C16590pI r3, BlurFrameLayout blurFrameLayout) {
        this.A02 = new WeakReference(blurFrameLayout);
        Context context = r3.A00;
        if (C28391Mz.A00()) {
            RenderScript create = RenderScript.create(context);
            this.A00 = create;
            ScriptIntrinsicBlur create2 = ScriptIntrinsicBlur.create(create, Element.U8_4(create));
            this.A01 = create2;
            create2.setRadius(16.0f);
        }
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ Object A05(Object[] objArr) {
        Bitmap bitmap = ((Bitmap[]) objArr)[0];
        if (C28391Mz.A00()) {
            try {
                Bitmap copy = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                ScriptIntrinsicBlur scriptIntrinsicBlur = this.A01;
                if (scriptIntrinsicBlur != null) {
                    RenderScript renderScript = this.A00;
                    Allocation createFromBitmap = Allocation.createFromBitmap(renderScript, bitmap, Allocation.MipmapControl.MIPMAP_NONE, 1);
                    Allocation createTyped = Allocation.createTyped(renderScript, createFromBitmap.getType());
                    createFromBitmap.copyFrom(bitmap);
                    scriptIntrinsicBlur.setInput(createFromBitmap);
                    scriptIntrinsicBlur.forEach(createTyped);
                    createTyped.copyTo(copy);
                }
                return copy;
            } finally {
                RenderScript renderScript2 = this.A00;
                if (renderScript2 != null) {
                    renderScript2.destroy();
                }
            }
        } else {
            Bitmap copy2 = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            int width = copy2.getWidth();
            int height = copy2.getHeight();
            int i = width * height;
            int[] iArr = new int[i];
            copy2.getPixels(iArr, 0, width, 0, 0, width, height);
            int i2 = width - 1;
            int i3 = height - 1;
            int[] iArr2 = new int[i];
            int[] iArr3 = new int[i];
            int[] iArr4 = new int[i];
            int[] iArr5 = new int[Math.max(width, height)];
            int i4 = 34 >> 1;
            int i5 = i4 * i4;
            int i6 = i5 << 8;
            int[] iArr6 = new int[i6];
            for (int i7 = 0; i7 < i6; i7++) {
                iArr6[i7] = i7 / i5;
            }
            int[][] iArr7 = (int[][]) Array.newInstance(int.class, 33, 3);
            int i8 = 0;
            int i9 = 0;
            for (int i10 = 0; i10 < height; i10++) {
                int i11 = 0;
                int i12 = 0;
                int i13 = 0;
                int i14 = 0;
                int i15 = 0;
                int i16 = 0;
                int i17 = 0;
                int i18 = 0;
                int i19 = 0;
                for (int i20 = -16; i20 <= 16; i20++) {
                    int i21 = iArr[i8 + Math.min(i2, Math.max(i20, 0))];
                    int[] iArr8 = iArr7[i20 + 16];
                    iArr8[0] = (i21 & 16711680) >> 16;
                    iArr8[1] = (i21 & 65280) >> 8;
                    int i22 = i21 & 255;
                    iArr8[2] = i22;
                    int abs = 17 - Math.abs(i20);
                    int i23 = iArr8[0];
                    i11 += i23 * abs;
                    int i24 = iArr8[1];
                    i12 += i24 * abs;
                    i13 += i22 * abs;
                    if (i20 > 0) {
                        i17 += i23;
                        i18 += i24;
                        i19 += iArr8[2];
                    } else {
                        i14 += i23;
                        i15 += i24;
                        i16 += iArr8[2];
                    }
                }
                int i25 = 16;
                for (int i26 = 0; i26 < width; i26++) {
                    iArr2[i8] = iArr6[i11];
                    iArr3[i8] = iArr6[i12];
                    iArr4[i8] = iArr6[i13];
                    int i27 = i11 - i14;
                    int i28 = i12 - i15;
                    int i29 = i13 - i16;
                    int[] iArr9 = iArr7[((i25 - 16) + 33) % 33];
                    int i30 = i14 - iArr9[0];
                    int i31 = i15 - iArr9[1];
                    int i32 = i16 - iArr9[2];
                    if (i10 == 0) {
                        iArr5[i26] = Math.min(i26 + 16 + 1, i2);
                    }
                    int i33 = iArr[i9 + iArr5[i26]];
                    iArr9[0] = (i33 & 16711680) >> 16;
                    iArr9[1] = (i33 & 65280) >> 8;
                    int i34 = i33 & 255;
                    iArr9[2] = i34;
                    int i35 = i17 + iArr9[0];
                    int i36 = i18 + iArr9[1];
                    int i37 = i19 + i34;
                    i11 = i27 + i35;
                    i12 = i28 + i36;
                    i13 = i29 + i37;
                    i25 = (i25 + 1) % 33;
                    int[] iArr10 = iArr7[i25 % 33];
                    int i38 = iArr10[0];
                    i14 = i30 + i38;
                    int i39 = iArr10[1];
                    i15 = i31 + i39;
                    int i40 = iArr10[2];
                    i16 = i32 + i40;
                    i17 = i35 - i38;
                    i18 = i36 - i39;
                    i19 = i37 - i40;
                    i8++;
                }
                i9 += width;
            }
            for (int i41 = 0; i41 < width; i41++) {
                int i42 = -16;
                int i43 = i42 * width;
                int i44 = 0;
                int i45 = 0;
                int i46 = 0;
                int i47 = 0;
                int i48 = 0;
                int i49 = 0;
                int i50 = 0;
                int i51 = 0;
                int i52 = 0;
                while (i42 <= 16) {
                    int max = Math.max(0, i43) + i41;
                    int[] iArr11 = iArr7[i42 + 16];
                    iArr11[0] = iArr2[max];
                    iArr11[1] = iArr3[max];
                    iArr11[2] = iArr4[max];
                    int abs2 = 17 - Math.abs(i42);
                    i44 += iArr2[max] * abs2;
                    i45 += iArr3[max] * abs2;
                    i46 += iArr4[max] * abs2;
                    int i53 = iArr11[0];
                    if (i42 > 0) {
                        i50 += i53;
                        i51 += iArr11[1];
                        i52 += iArr11[2];
                    } else {
                        i47 += i53;
                        i48 += iArr11[1];
                        i49 += iArr11[2];
                    }
                    if (i42 < i3) {
                        i43 += width;
                    }
                    i42++;
                }
                int i54 = 16;
                int i55 = i41;
                for (int i56 = 0; i56 < height; i56++) {
                    iArr[i55] = (iArr[i55] & -16777216) | (iArr6[i44] << 16) | (iArr6[i45] << 8) | iArr6[i46];
                    int i57 = i44 - i47;
                    int i58 = i45 - i48;
                    int i59 = i46 - i49;
                    int[] iArr12 = iArr7[((i54 - 16) + 33) % 33];
                    int i60 = i47 - iArr12[0];
                    int i61 = i48 - iArr12[1];
                    int i62 = i49 - iArr12[2];
                    if (i41 == 0) {
                        iArr5[i56] = Math.min(i56 + 16 + 1, i3) * width;
                    }
                    int i63 = iArr5[i56] + i41;
                    iArr12[0] = iArr2[i63];
                    iArr12[1] = iArr3[i63];
                    int i64 = iArr4[i63];
                    iArr12[2] = i64;
                    int i65 = i50 + iArr12[0];
                    int i66 = i51 + iArr12[1];
                    int i67 = i52 + i64;
                    i44 = i57 + i65;
                    i45 = i58 + i66;
                    i46 = i59 + i67;
                    i54 = (i54 + 1) % 33;
                    int[] iArr13 = iArr7[i54];
                    int i68 = iArr13[0];
                    i47 = i60 + i68;
                    int i69 = iArr13[1];
                    i48 = i61 + i69;
                    int i70 = iArr13[2];
                    i49 = i62 + i70;
                    i50 = i65 - i68;
                    i51 = i66 - i69;
                    i52 = i67 - i70;
                    i55 += width;
                }
            }
            copy2.setPixels(iArr, 0, width, 0, 0, width, height);
            return copy2;
        }
    }
}
