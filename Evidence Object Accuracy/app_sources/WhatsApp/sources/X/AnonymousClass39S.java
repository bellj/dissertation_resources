package X;

import android.telecom.CallAudioState;
import android.text.TextUtils;
import com.whatsapp.voipcalling.CallInfo;
import com.whatsapp.voipcalling.Voip;

/* renamed from: X.39S  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39S extends AnonymousClass2ON {
    public final /* synthetic */ AnonymousClass2Nu A00;

    public AnonymousClass39S(AnonymousClass2Nu r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2ON
    public void A00(CallAudioState callAudioState, String str) {
        AnonymousClass009.A01();
        StringBuilder A0k = C12960it.A0k("voip/audio_route/selfManagedConnectionListener/onCallAudioStateChanged ");
        A0k.append(str);
        A0k.append(", ");
        AnonymousClass2Nu r3 = this.A00;
        A0k.append(Voip.A05(r3.A00));
        A0k.append(" -> ");
        A0k.append(callAudioState);
        C12960it.A1F(A0k);
        CallInfo callInfo = Voip.getCallInfo();
        if (Voip.A08(callInfo) && TextUtils.equals(str, callInfo.callId) && callInfo.callState != Voip.CallState.ACTIVE_ELSEWHERE) {
            AnonymousClass009.A05(callInfo);
            if (callAudioState.isMuted() && callInfo.callState == Voip.CallState.ACTIVE && !callInfo.self.A09) {
                r3.A04();
            }
            r3.A04 = false;
            int i = r3.A00;
            if (i == 3 || i == 4 || (callInfo.videoEnabled && callInfo.callState == Voip.CallState.ACTIVE)) {
                r3.A07(callInfo, null);
                return;
            }
            r3.A06(callInfo);
            r3.A08(callInfo, null);
        }
    }
}
