package X;

import java.io.File;
import java.util.List;
import java.util.Map;

/* renamed from: X.1s9  reason: invalid class name */
/* loaded from: classes2.dex */
public interface AnonymousClass1s9 {
    void A7E();

    void AA4(float f, float f2);

    boolean AJS();

    boolean AJV();

    boolean AJy();

    boolean AK9();

    boolean ALa();

    void ALf();

    String ALg();

    void Aap();

    void Aar();

    int AdD(int i);

    void AeK(File file, int i);

    void AeT();

    boolean Aee();

    void Aei(AnonymousClass2AQ v, boolean z);

    void Af2();

    int getCameraApi();

    int getCameraType();

    String getFlashMode();

    List getFlashModes();

    int getMaxZoom();

    int getNumberOfCameras();

    long getPictureResolution();

    int getStoredFlashModeCount();

    long getVideoResolution();

    void pause();

    void setCameraCallback(AbstractC467527b v);

    void setQrDecodeHints(Map map);

    void setQrScanningEnabled(boolean z);
}
