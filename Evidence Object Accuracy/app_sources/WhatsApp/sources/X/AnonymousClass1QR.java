package X;

/* renamed from: X.1QR  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1QR extends UnsatisfiedLinkError {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AnonymousClass1QR(java.lang.String r3, java.lang.Throwable r4) {
        /*
            r2 = this;
            java.lang.String r0 = "APK was built for a different platform. Supported ABIs: "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String[] r0 = X.AnonymousClass3G2.A01()
            java.lang.String r0 = java.util.Arrays.toString(r0)
            r1.append(r0)
            java.lang.String r0 = " error: "
            r1.append(r0)
            r1.append(r3)
            java.lang.String r0 = r1.toString()
            r2.<init>(r0)
            r2.initCause(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass1QR.<init>(java.lang.String, java.lang.Throwable):void");
    }
}
