package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1rw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40651rw {
    public String A00;
    public String A01;

    public C40651rw(String str, String str2) {
        this.A00 = str;
        this.A01 = str2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        if ("null".equals(r2) != false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C40651rw A00(java.lang.String r4) {
        /*
            java.lang.String r0 = ","
            java.lang.String[] r4 = r4.split(r0)
            r0 = 0
            int r1 = r4.length
            r3 = 0
            if (r1 <= r0) goto L_0x0015
            r2 = r4[r0]
            java.lang.String r0 = "null"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            r2 = r3
        L_0x0016:
            r0 = 1
            if (r1 <= r0) goto L_0x0024
            r1 = r4[r0]
            java.lang.String r0 = "null"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0024
            r3 = r1
        L_0x0024:
            X.1rw r0 = new X.1rw
            r0.<init>(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C40651rw.A00(java.lang.String):X.1rw");
    }

    public String toString() {
        return TextUtils.join(",", Arrays.asList(this.A00, this.A01));
    }
}
