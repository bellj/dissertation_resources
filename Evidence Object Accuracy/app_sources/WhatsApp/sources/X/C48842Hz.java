package X;

/* renamed from: X.2Hz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48842Hz {
    public int A00;
    public AnonymousClass04S A01;
    public AbstractC15710nm A02;
    public C48832Hy A03;
    public C16430p0 A04;
    public AnonymousClass3WR A05;
    public C15550nR A06;
    public C14830m7 A07;
    public C22610zM A08;
    public String A09;

    public C48842Hz(AbstractC15710nm r1, C48832Hy r2, C16430p0 r3, C15550nR r4, C14830m7 r5, C22610zM r6) {
        this.A07 = r5;
        this.A02 = r1;
        this.A06 = r4;
        this.A03 = r2;
        this.A08 = r6;
        this.A04 = r3;
    }

    public void A00() {
        AnonymousClass3WR r0 = this.A05;
        if (r0 != null) {
            r0.A01.A03(true);
            this.A05.A00 = null;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 9, insn: 0x00aa: INVOKE  (r9 I:X.2Hz), (r8 I:android.view.View), (r10 I:X.4Pv), (r0 I:X.1v4), (r12 I:java.lang.String) type: VIRTUAL call: X.2Hz.A02(android.view.View, X.4Pv, X.1v4, java.lang.String):void, block:B:16:0x00a8
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public void A01(
/*
[179] Method generation error in method: X.2Hz.A01(android.view.View, X.4Pv, X.5Tn, java.lang.String):void, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r14v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0007, code lost:
        if (r0 != 0) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A02(android.view.View r6, X.C90944Pv r7, X.C42351v4 r8, java.lang.String r9) {
        /*
            r5 = this;
            if (r8 == 0) goto L_0x0009
            int r0 = r8.A00
            r4 = 2131886504(0x7f1201a8, float:1.9407589E38)
            if (r0 == 0) goto L_0x000c
        L_0x0009:
            r4 = 2131886493(0x7f12019d, float:1.9407566E38)
        L_0x000c:
            X.04S r0 = r5.A01
            if (r0 == 0) goto L_0x0013
            r0.cancel()
        L_0x0013:
            android.content.Context r0 = r6.getContext()
            android.app.Activity r3 = X.AnonymousClass12P.A00(r0)
            android.content.Context r0 = r6.getContext()
            X.2dp r2 = new X.2dp
            r2.<init>(r0)
            r0 = 2131890036(0x7f120f74, float:1.9414752E38)
            java.lang.String r1 = r3.getString(r0)
            X.00m r3 = (X.AbstractActivityC001100m) r3
            X.4uE r0 = new X.4uE
            r0.<init>(r6, r5, r7, r9)
            r2.A0G(r3, r0, r1)
            r0 = 2131886585(0x7f1201f9, float:1.9407753E38)
            r2.A07(r0)
            r2.A06(r4)
            X.04S r0 = r2.create()
            r5.A01 = r0
            r0.show()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C48842Hz.A02(android.view.View, X.4Pv, X.1v4, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        if (r3 == 1) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(android.view.View r18, X.C90944Pv r19, java.lang.String r20) {
        /*
            r17 = this;
            r0 = r17
            X.0p0 r1 = r0.A04
            java.lang.String r2 = r1.A01
            r8 = 0
            r4 = 0
            if (r2 != 0) goto L_0x0011
            X.0nm r3 = r0.A02
            java.lang.String r2 = "directorySessionIdIsNull"
            r3.AaV(r2, r8, r4)
        L_0x0011:
            r2 = r19
            if (r19 == 0) goto L_0x009b
            int r15 = r2.A00
            X.3M8 r3 = r2.A01
            double r11 = r3.A00
            double r13 = r3.A01
            X.3Vw r2 = r2.A02
            java.lang.String r7 = r2.A0D
            java.lang.String r8 = r3.A09
            int r3 = r2.A03
            r2 = 1
            if (r3 != r2) goto L_0x0029
        L_0x0028:
            r2 = 0
        L_0x0029:
            java.lang.String r9 = r0.A09
            java.lang.String r10 = r1.A01
            java.lang.Integer r6 = java.lang.Integer.valueOf(r4)
            r16 = r2
            X.2VB r5 = new X.2VB
            r5.<init>(r6, r7, r8, r9, r10, r11, r13, r15, r16)
            com.whatsapp.jid.Jid r2 = com.whatsapp.jid.Jid.getNullable(r20)
            X.AnonymousClass009.A05(r2)
            X.0n3 r3 = new X.0n3
            r3.<init>(r2)
            android.content.Context r2 = r18.getContext()
            android.app.Activity r4 = X.AnonymousClass12P.A00(r2)
            X.0mK r2 = new X.0mK
            r2.<init>()
            android.content.Intent r3 = r2.A0f(r4, r3)
            java.lang.String r2 = "user_actions_on_business_profile_common_fields"
            android.content.Intent r2 = r3.putExtra(r2, r5)
            r4.startActivity(r2)
            com.whatsapp.jid.UserJid r3 = com.whatsapp.jid.UserJid.getNullable(r20)
            X.AnonymousClass009.A05(r3)
            long r6 = java.lang.System.currentTimeMillis()
            long r8 = java.lang.System.currentTimeMillis()
            java.lang.String r4 = "directory"
            java.lang.String r5 = "whatsapp"
            X.1rj r2 = new X.1rj
            r2.<init>(r3, r4, r5, r6, r8)
            X.1ri r3 = new X.1ri
            r3.<init>(r2)
            X.0zM r2 = r0.A08
            r2.A00(r3)
            int r0 = r0.A00
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            r0 = 51
            X.1Ny r2 = new X.1Ny
            r2.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.A06 = r0
            r2.A03 = r3
            r1.A03(r2)
            return
        L_0x009b:
            r15 = 0
            r13 = 0
            r11 = 0
            r7 = r8
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C48842Hz.A03(android.view.View, X.4Pv, java.lang.String):void");
    }
}
