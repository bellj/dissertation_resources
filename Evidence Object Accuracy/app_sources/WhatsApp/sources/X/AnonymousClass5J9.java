package X;

import android.graphics.Paint;

/* renamed from: X.5J9  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5J9 extends AnonymousClass1WI implements AnonymousClass1WK {
    public AnonymousClass5J9() {
        super(0);
    }

    @Override // X.AnonymousClass1WK
    public Object AJ3() {
        Paint paint = new Paint(1);
        paint.setStyle(Paint.Style.FILL);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        return paint;
    }
}
