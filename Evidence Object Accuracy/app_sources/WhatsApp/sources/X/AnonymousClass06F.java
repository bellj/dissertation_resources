package X;

/* renamed from: X.06F  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass06F implements AnonymousClass02T {
    public final AnonymousClass06H A00;

    public abstract boolean A00();

    public AnonymousClass06F(AnonymousClass06H r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass02T
    public boolean AK0(CharSequence charSequence, int i, int i2) {
        if (i2 < 0 || charSequence.length() - i2 < 0) {
            throw new IllegalArgumentException();
        }
        AnonymousClass06H r0 = this.A00;
        if (r0 != null) {
            int A78 = r0.A78(charSequence, 0, i2);
            if (A78 == 0) {
                return true;
            }
            if (A78 == 1) {
                return false;
            }
        }
        return A00();
    }
}
