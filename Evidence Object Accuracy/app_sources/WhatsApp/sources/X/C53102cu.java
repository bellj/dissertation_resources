package X;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape3S0200000_I1_1;
import com.whatsapp.R;
import com.whatsapp.checkbox.RtlCheckBox;
import java.util.List;

/* renamed from: X.2cu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53102cu extends LinearLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public /* synthetic */ C53102cu(Context context, String str, String str2, List list) {
        super(context);
        if (!this.A01) {
            this.A01 = true;
            generatedComponent();
        }
        LayoutInflater.from(context).inflate(R.layout.dialog_fragment_custom_view, this);
        setOrientation(1);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.dialog_fragment_custom_view_padding_horizontal);
        int i = 0;
        setPadding(dimensionPixelSize, context.getResources().getDimensionPixelSize(R.dimen.dialog_fragment_custom_view_padding_top), dimensionPixelSize, 0);
        View A0D = AnonymousClass028.A0D(this, R.id.image);
        TextView A0I = C12960it.A0I(this, R.id.title);
        TextView A0I2 = C12960it.A0I(this, R.id.subtitle);
        A0D.setVisibility(8);
        C12980iv.A1J(A0I, str);
        C12980iv.A1J(A0I2, str2);
        int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.dialog_fragment_custom_view_first_checkbox_margin_top);
        int dimensionPixelSize3 = getResources().getDimensionPixelSize(R.dimen.dialog_fragment_custom_view_checkbox_margin_top);
        while (i < list.size()) {
            C90234Nc r4 = (C90234Nc) list.get(i);
            int i2 = i == 0 ? dimensionPixelSize2 : dimensionPixelSize3;
            RtlCheckBox rtlCheckBox = new RtlCheckBox(getContext(), null);
            rtlCheckBox.setTextSize(2, 16.0f);
            C12960it.A0s(getContext(), rtlCheckBox, R.color.secondary_text);
            addView(rtlCheckBox);
            C12970iu.A0H(rtlCheckBox).topMargin = i2;
            rtlCheckBox.setText(r4.A01);
            rtlCheckBox.setChecked(false);
            if (r4.A00 != null) {
                rtlCheckBox.setOnClickListener(new ViewOnClickCListenerShape3S0200000_I1_1(r4, 5, rtlCheckBox));
            }
            i++;
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
