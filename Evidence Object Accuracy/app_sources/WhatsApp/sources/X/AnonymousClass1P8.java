package X;

/* renamed from: X.1P8  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1P8 extends AnonymousClass1G4 implements AnonymousClass1G2 {
    public AnonymousClass1P8() {
        super(AnonymousClass1P7.A0D);
    }

    public void A05(AnonymousClass1PB r4) {
        A03();
        AnonymousClass1P7 r2 = (AnonymousClass1P7) this.A00;
        AnonymousClass1K6 r1 = r2.A07;
        if (!((AnonymousClass1K7) r1).A00) {
            r1 = AbstractC27091Fz.A0G(r1);
            r2.A07 = r1;
        }
        r1.add(r4);
    }

    public void A06(AnonymousClass1P9 r3) {
        A03();
        AnonymousClass1P7 r1 = (AnonymousClass1P7) this.A00;
        r1.A01 |= 1;
        r1.A04 = r3.value;
    }
}
