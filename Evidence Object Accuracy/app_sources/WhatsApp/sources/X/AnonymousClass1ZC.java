package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.1ZC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1ZC implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100274le();
    public final UserJid A00;
    public final AnonymousClass1ZH A01;
    public final List A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass1ZC(Parcel parcel) {
        ArrayList arrayList = new ArrayList();
        this.A02 = arrayList;
        parcel.readList(arrayList, AnonymousClass1ZF.class.getClassLoader());
        Parcelable readParcelable = parcel.readParcelable(UserJid.class.getClassLoader());
        AnonymousClass009.A05(readParcelable);
        this.A00 = (UserJid) readParcelable;
        Parcelable readParcelable2 = parcel.readParcelable(AnonymousClass1ZH.class.getClassLoader());
        AnonymousClass009.A05(readParcelable2);
        this.A01 = (AnonymousClass1ZH) readParcelable2;
    }

    public AnonymousClass1ZC(UserJid userJid, AnonymousClass1ZH r2, List list) {
        this.A02 = list;
        this.A01 = r2;
        this.A00 = userJid;
    }

    public int A00() {
        int i = 0;
        for (AnonymousClass1ZF r0 : this.A02) {
            i += r0.A01.size();
        }
        return i;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.A02);
        parcel.writeParcelable(this.A00, i);
        parcel.writeParcelable(this.A01, 0);
    }
}
