package X;

import android.graphics.Bitmap;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: X.1q2  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C39521q2 {
    public final Map A00 = new HashMap();

    public Bitmap A00(C39541q4 r6) {
        Bitmap bitmap;
        Map map = this.A00;
        synchronized (map) {
            SoftReference softReference = (SoftReference) map.get(r6);
            if (softReference == null) {
                bitmap = null;
            } else {
                bitmap = (Bitmap) softReference.get();
                if (bitmap == null) {
                    Set entrySet = map.entrySet();
                    AnonymousClass1q9 r2 = new AnonymousClass03D() { // from class: X.1q9
                        @Override // X.AnonymousClass03D
                        public final boolean Aek(Object obj) {
                            return ((Reference) ((Map.Entry) obj).getValue()).get() == null;
                        }
                    };
                    Iterator it = entrySet.iterator();
                    while (it.hasNext()) {
                        if (r2.Aek(it.next())) {
                            it.remove();
                        }
                    }
                }
            }
        }
        return bitmap;
    }
}
