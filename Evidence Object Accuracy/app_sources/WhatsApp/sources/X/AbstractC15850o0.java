package X;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.whatsapp.R;
import com.whatsapp.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0o0  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15850o0 {
    public boolean A00;
    public final C15570nT A01;
    public final C15820nx A02;
    public final C15810nw A03;
    public final C17050qB A04;
    public final C16590pI A05;
    public final C19490uC A06;
    public final C21780xy A07;
    public final AbstractC15870o2 A08;
    public final C16600pJ A09;
    public final AnonymousClass15D A0A;

    public AbstractC15850o0(C15570nT r1, C15820nx r2, C15810nw r3, C17050qB r4, C16590pI r5, C19490uC r6, C21780xy r7, AbstractC15870o2 r8, C16600pJ r9, AnonymousClass15D r10) {
        this.A05 = r5;
        this.A0A = r10;
        this.A01 = r1;
        this.A03 = r3;
        this.A06 = r6;
        this.A02 = r2;
        this.A04 = r4;
        this.A08 = r8;
        this.A09 = r9;
        this.A07 = r7;
    }

    public static Point A00(Context context) {
        Point point = new Point();
        AnonymousClass01d.A02(context).getDefaultDisplay().getSize(point);
        if (context.getResources().getConfiguration().orientation == 2) {
            int i = point.y;
            point.y = point.x;
            point.x = i;
        }
        point.y -= ((int) context.getResources().getDimension(R.dimen.abc_action_bar_default_height_material)) + C27531Hw.A02(context, AnonymousClass01d.A02(context));
        return point;
    }

    public static C41591tm A01(Point point, boolean z) {
        int i = point.x;
        int i2 = point.y;
        Long valueOf = Long.valueOf(AnonymousClass01V.A00 / 32);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inDither = z;
        return new C41591tm(options, valueOf, i, i2, false);
    }

    public static List A02(C15810nw r5) {
        List A07 = C32781cj.A07(EnumC16570pG.A06, EnumC16570pG.A00());
        File file = new File(r5.A02(), "wallpapers.backup");
        ArrayList A06 = C32781cj.A06(file, A07);
        File file2 = new File(r5.A02(), "Wallpapers");
        if (file2.exists()) {
            A06.add(file2);
        }
        C32781cj.A0C(file, A06);
        return A06;
    }

    public Drawable A03(AnonymousClass2JR r5) {
        if (!(this instanceof C252218o)) {
            if (r5 == null) {
                return null;
            }
            return r5.A00;
        } else if (r5 == null) {
            return null;
        } else {
            Drawable drawable = r5.A00;
            Integer num = r5.A01;
            if (num == null || drawable == null) {
                return drawable;
            }
            AnonymousClass2JP.A03(this.A05.A00, drawable, num.intValue());
            return drawable;
        }
    }

    public Uri A04() {
        if (this instanceof C252218o) {
            return ((C252218o) this).A03.A04();
        }
        C252118n r4 = (C252118n) this;
        C15570nT r0 = r4.A05;
        r0.A08();
        C27631Ih r02 = r0.A05;
        AnonymousClass009.A05(r02);
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(r02.getRawString());
        sb2.append(System.currentTimeMillis());
        sb.append(C003501n.A01(sb2.toString()));
        sb.append(".jpg");
        String obj = sb.toString();
        File file = r4.A03.A04().A0P;
        C14330lG.A03(file, false);
        return Uri.fromFile(new File(file, obj));
    }

    public AnonymousClass2JR A05(Context context, Uri uri, AbstractC14640lm r10, boolean z) {
        InputStream inputStream;
        if (!(this instanceof C252218o)) {
            C252118n r3 = (C252118n) this;
            StringBuilder sb = new StringBuilder("wallpaper/set with Uri with size (width x height): ");
            sb.append(0);
            sb.append("x");
            sb.append(0);
            Log.i(sb.toString());
            r3.A00 = null;
            try {
                InputStream A0C = r3.A08.A0C(uri, true);
                Bitmap bitmap = C37501mV.A05(A01(A00(context), false), A0C).A02;
                if (bitmap != null) {
                    r3.A00 = new BitmapDrawable(context.getResources(), bitmap);
                } else {
                    r3.A04.A05(R.string.error_load_wallpaper, 0);
                }
                ((AbstractC15850o0) r3).A00 = true;
                A0C.close();
            } catch (IOException e) {
                Log.e(e);
            }
            Drawable drawable = r3.A00;
            if (drawable != null) {
                r3.A0B(context, drawable);
            }
            return new AnonymousClass2JR(r3.A00, 0, "DOWNLOADED", true);
        }
        C252218o r4 = (C252218o) this;
        boolean z2 = false;
        BitmapDrawable bitmapDrawable = null;
        try {
            C22190yg r1 = r4.A04;
            if (z) {
                inputStream = r1.A0C(uri, true);
            } else {
                inputStream = new FileInputStream(C14350lI.A03(uri));
            }
            Bitmap bitmap2 = C37501mV.A05(A01(A00(context), false), inputStream).A02;
            if (bitmap2 != null) {
                bitmapDrawable = new BitmapDrawable(context.getResources(), bitmap2);
            } else {
                r4.A01.A05(R.string.error_load_wallpaper, 0);
            }
            inputStream.close();
        } catch (IOException unused) {
            r4.A01.A05(R.string.error_load_wallpaper, 0);
        }
        if (bitmapDrawable == null) {
            return r4.A06(context, r10);
        }
        if (r10 == null) {
            z2 = true;
        }
        return r4.A0B(context, r4.A0C(context, bitmapDrawable, r10), z2);
    }

    public AnonymousClass2JR A06(Context context, AbstractC14640lm r7) {
        C33191db A0D;
        if (!(this instanceof C252218o)) {
            return ((C252118n) this).A0A(context, false);
        }
        C252218o r4 = (C252218o) this;
        boolean A08 = C41691tw.A08(context);
        boolean z = true;
        if (r7 == null || (A0D = r4.A08.AHh(r7, A08)) == null) {
            A0D = r4.A0D(context, A08);
        } else {
            z = false;
        }
        AnonymousClass01T r0 = new AnonymousClass01T(A0D, Boolean.valueOf(z));
        Object obj = r0.A00;
        AnonymousClass009.A05(obj);
        Object obj2 = r0.A01;
        AnonymousClass009.A05(obj2);
        return r4.A0B(context, (C33191db) obj, ((Boolean) obj2).booleanValue());
    }

    public File A07() {
        if (!(this instanceof C252218o)) {
            return new File(this.A05.A00.getFilesDir(), "wallpaper.jpg");
        }
        return ((C252218o) this).A03.A07();
    }

    public void A08() {
        if (this instanceof C252218o) {
            ((C252218o) this).A00.A0B(0);
        }
    }

    public boolean A09() {
        if (!(this instanceof C252218o)) {
            C252118n r0 = (C252118n) this;
            return r0.A06.A03(new File(((AbstractC15850o0) r0).A05.A00.getFilesDir(), "wallpaper.jpg"), "wallpaper") == 19;
        }
        C252218o r1 = (C252218o) this;
        boolean A09 = r1.A03.A09();
        r1.A0E();
        return A09;
    }
}
