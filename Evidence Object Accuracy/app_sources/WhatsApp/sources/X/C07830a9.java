package X;

/* renamed from: X.0a9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C07830a9 implements AbstractC12000hD {
    public final /* synthetic */ float A00;
    public final /* synthetic */ AnonymousClass0AA A01;

    public C07830a9(AnonymousClass0AA r1, float f) {
        this.A01 = r1;
        this.A00 = f;
    }

    @Override // X.AbstractC12000hD
    public void Aax(C05540Py r5) {
        AnonymousClass0AA r3 = this.A01;
        float f = this.A00;
        C05540Py r0 = r3.A04;
        if (r0 == null) {
            r3.A0L.add(new C07830a9(r3, f));
            return;
        }
        float f2 = r0.A02;
        r3.A07((int) (f2 + (f * (r0.A00 - f2))));
    }
}
