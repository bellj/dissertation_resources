package X;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.whatsapp.KeyboardPopupLayout;

/* renamed from: X.2My  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC49832My extends RelativeLayout implements AnonymousClass004 {
    public AnonymousClass2P7 A00;
    public boolean A01;

    public AbstractC49832My(Context context) {
        super(context);
        A01();
    }

    public AbstractC49832My(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        A01();
    }

    public AbstractC49832My(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        A01();
    }

    public AbstractC49832My(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        A01();
    }

    public void A01() {
        if (!this.A01) {
            this.A01 = true;
            ((KeyboardPopupLayout) this).A05 = (C252718t) ((AnonymousClass2P6) ((AnonymousClass2P5) generatedComponent())).A06.A9K.get();
        }
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A00;
        if (r0 == null) {
            r0 = new AnonymousClass2P7(this);
            this.A00 = r0;
        }
        return r0.generatedComponent();
    }
}
