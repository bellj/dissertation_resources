package X;

import com.facebook.redex.RunnableBRunnable0Shape14S0100000_I1;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.3d4  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class ThreadFactoryC71523d4 implements ThreadFactory {
    public final AtomicInteger A00 = new AtomicInteger(1);

    @Override // java.util.concurrent.ThreadFactory
    public Thread newThread(Runnable runnable) {
        return new AnonymousClass1MS(new RunnableBRunnable0Shape14S0100000_I1(runnable, 16), C12960it.A0f(C12960it.A0k("Google Drive Checksum Calculation Worker #"), this.A00.getAndIncrement()));
    }
}
