package X;

import android.view.ViewTreeObserver;
import com.whatsapp.conversation.conversationrow.album.MediaAlbumActivity;

/* renamed from: X.4oD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class ViewTreeObserver$OnPreDrawListenerC101864oD implements ViewTreeObserver.OnPreDrawListener {
    public final /* synthetic */ MediaAlbumActivity A00;

    public ViewTreeObserver$OnPreDrawListenerC101864oD(MediaAlbumActivity mediaAlbumActivity) {
        this.A00 = mediaAlbumActivity;
    }

    @Override // android.view.ViewTreeObserver.OnPreDrawListener
    public boolean onPreDraw() {
        MediaAlbumActivity mediaAlbumActivity = this.A00;
        C12980iv.A1G(mediaAlbumActivity.A2e(), this);
        mediaAlbumActivity.A0d();
        return true;
    }
}
