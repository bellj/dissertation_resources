package X;

import android.hardware.camera2.CaptureRequest;
import java.util.concurrent.Callable;

/* renamed from: X.6Kp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class CallableC135996Kp implements Callable {
    public final /* synthetic */ int A00;
    public final /* synthetic */ int A01;
    public final /* synthetic */ long A02;
    public final /* synthetic */ CaptureRequest.Builder A03;
    public final /* synthetic */ AnonymousClass66P A04;
    public final /* synthetic */ AbstractC136406Mk A05;
    public final /* synthetic */ C1308060a A06;
    public final /* synthetic */ AnonymousClass66I A07;
    public final /* synthetic */ C129845yO A08;
    public final /* synthetic */ String A09;
    public final /* synthetic */ boolean A0A = true;

    public CallableC135996Kp(CaptureRequest.Builder builder, AnonymousClass66P r3, AbstractC136406Mk r4, C1308060a r5, AnonymousClass66I r6, C129845yO r7, String str, int i, int i2, long j) {
        this.A06 = r5;
        this.A09 = str;
        this.A08 = r7;
        this.A00 = i;
        this.A01 = i2;
        this.A04 = r3;
        this.A05 = r4;
        this.A03 = builder;
        this.A07 = r6;
        this.A02 = j;
    }

    @Override // java.util.concurrent.Callable
    public /* bridge */ /* synthetic */ Object call() {
        C1308060a r4 = this.A06;
        String str = this.A09;
        C129845yO r9 = this.A08;
        int i = this.A00;
        int i2 = this.A01;
        boolean z = this.A0A;
        AnonymousClass60Q A01 = r4.A01(this.A03, this.A04, this.A05, this.A07, r9, str, i, i2, z);
        r4.A06 = A01;
        A01.A02(AnonymousClass60Q.A0O, Long.valueOf(this.A02));
        return r4.A06;
    }
}
