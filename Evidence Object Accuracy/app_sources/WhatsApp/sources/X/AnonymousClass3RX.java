package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3RX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3RX implements AbstractC009404s {
    public final C48882Ih A00;
    public final UserJid A01;

    public AnonymousClass3RX(C48882Ih r1, UserJid userJid) {
        this.A00 = r1;
        this.A01 = userJid;
    }

    public static AnonymousClass015 A00(AbstractC001400p r2, C48882Ih r3, UserJid userJid) {
        return new AnonymousClass02A(new AnonymousClass3RX(r3, userJid), r2).A00(C53852fO.class);
    }

    @Override // X.AbstractC009404s
    public AnonymousClass015 A7r(Class cls) {
        C48882Ih r0 = this.A00;
        UserJid userJid = this.A01;
        AnonymousClass01J r1 = r0.A00.A03;
        AbstractC14440lR A0T = C12960it.A0T(r1);
        return new C53852fO(AbstractC250617y.A00(r1.AO3), C12980iv.A0Y(r1), (AnonymousClass10A) r1.A2W.get(), C12980iv.A0a(r1), userJid, A0T);
    }
}
