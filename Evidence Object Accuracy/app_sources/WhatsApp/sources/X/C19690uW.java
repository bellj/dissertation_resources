package X;

import android.content.Context;
import android.content.Intent;
import com.whatsapp.wabloks.base.BkFcsPreloadingScreenFragment;
import com.whatsapp.wabloks.ui.WaFcsPreloadedBloksActivity;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: X.0uW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19690uW implements AbstractC19700uX {
    public final C16590pI A00;
    public final C19600uN A01;

    public C19690uW(C16590pI r2, C19600uN r3) {
        C16700pc.A0E(r2, 1);
        this.A00 = r2;
        this.A01 = r3;
    }

    @Override // X.AbstractC19700uX
    public void A6h() {
        Context context = this.A00.A00;
        Intent intent = new Intent();
        intent.setClassName(context.getPackageName(), "com.whatsapp.wabloks.ui.WaFcsModalActivity");
        intent.setFlags(872415232);
        context.startActivity(intent);
    }

    @Override // X.AbstractC19700uX
    public AnonymousClass01E AEX(String str, String str2, Map map, Map map2, int i) {
        String str3;
        C16700pc.A0E(str2, 3);
        Object obj = map.get("app_id");
        if ((obj instanceof String) && (str3 = (String) obj) != null) {
            return BkFcsPreloadingScreenFragment.A00(this.A01.A02, str3, new JSONObject(map2).toString(), C130355zH.A00(Integer.valueOf(i)), str2);
        }
        throw new IllegalArgumentException("FcsScreenOpenerBloksDelegate: app_id is not present");
    }

    @Override // X.AbstractC19700uX
    public void AYb(String str, String str2, String str3, String str4, Map map, Map map2, int i) {
        String str5;
        C16700pc.A0E(str4, 5);
        Object obj = map.get("app_id");
        if (!(obj instanceof String) || (str5 = (String) obj) == null) {
            throw new IllegalArgumentException("FcsScreenOpenerBloksDelegate: app_id is not present");
        }
        Context context = this.A00.A00;
        Intent A03 = WaFcsPreloadedBloksActivity.A03(context, this.A01.A02, Integer.valueOf(i), str5, new JSONObject(map2).toString(), str4, str, str2, str3);
        A03.setFlags(268435456);
        context.startActivity(A03);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004f, code lost:
        if (r0 == null) goto L_0x003b;
     */
    @Override // X.AbstractC19700uX
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void AYh(X.AnonymousClass4A3 r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, java.util.Map r19, java.util.Map r20, int r21) {
        /*
            r13 = this;
            r0 = 6
            r9 = r18
            X.C16700pc.A0E(r9, r0)
            java.lang.String r0 = "app_id"
            r1 = r19
            java.lang.Object r7 = r1.get(r0)
            boolean r0 = r7 instanceof java.lang.String
            if (r0 == 0) goto L_0x0052
            java.lang.String r7 = (java.lang.String) r7
            if (r7 == 0) goto L_0x0052
            X.0pI r0 = r13.A00
            android.content.Context r4 = r0.A00
            X.4A3 r2 = X.AnonymousClass4A3.A01
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r3 = r20
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>(r3)
            r11 = r16
            r10 = r15
            r12 = r17
            if (r14 != r2) goto L_0x003f
            java.lang.String r5 = r0.toString()
            r6 = r9
            r7 = r15
            r8 = r11
            r9 = r12
            android.content.Intent r0 = com.whatsapp.wabloks.ui.WaFcsBottomsheetModalActivity.A02(r4, r5, r6, r7, r8, r9)
        L_0x0038:
            r0.setFlags(r1)
        L_0x003b:
            r4.startActivity(r0)
            return
        L_0x003f:
            java.lang.String r8 = r0.toString()
            X.0uN r0 = r13.A01
            X.3Lt r5 = r0.A02
            java.lang.Integer r6 = java.lang.Integer.valueOf(r21)
            android.content.Intent r0 = com.whatsapp.wabloks.ui.WaFcsModalActivity.A03(r4, r5, r6, r7, r8, r9, r10, r11, r12)
            if (r0 != 0) goto L_0x0038
            goto L_0x003b
        L_0x0052:
            java.lang.String r1 = "FcsScreenOpenerBloksDelegate: app_id is not present"
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C19690uW.AYh(X.4A3, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map, java.util.Map, int):void");
    }
}
