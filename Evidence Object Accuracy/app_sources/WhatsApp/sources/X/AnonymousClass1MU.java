package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.whatsapp.jid.UserJid;

/* renamed from: X.1MU  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1MU extends UserJid implements Parcelable {
    public static final Parcelable.Creator CREATOR = new C100014lE();

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public String getServer() {
        return "lid";
    }

    @Override // com.whatsapp.jid.UserJid, com.whatsapp.jid.Jid
    public int getType() {
        return 18;
    }

    public AnonymousClass1MU(Parcel parcel) {
        super(parcel);
    }

    public AnonymousClass1MU(String str) {
        super(str);
        int length = str.length();
        if (length < 1 || length > 15) {
            StringBuilder sb = new StringBuilder();
            sb.append("Invalid LID: ");
            sb.append(str);
            sb.append(" - length must be between 1 and 15");
            throw new AnonymousClass1MW(sb.toString());
        } else if (!str.startsWith("0")) {
            char[] charArray = str.toCharArray();
            for (char c : charArray) {
                if (c < '0' || c > '9') {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid LID: ");
                    sb2.append(str);
                    sb2.append(" - must be numeric only");
                    throw new AnonymousClass1MW(sb2.toString());
                }
            }
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid LID: ");
            sb3.append(str);
            sb3.append(" - cannot start with 0");
            throw new AnonymousClass1MW(sb3.toString());
        }
    }
}
