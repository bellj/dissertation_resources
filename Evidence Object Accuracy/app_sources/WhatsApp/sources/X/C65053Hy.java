package X;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.3Hy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C65053Hy {
    public static List A00(JSONArray jSONArray) {
        ArrayList A0l = C12960it.A0l();
        for (int i = 0; i < jSONArray.length(); i++) {
            Object obj = jSONArray.get(i);
            if (obj instanceof JSONArray) {
                obj = A00((JSONArray) obj);
            } else if (obj instanceof JSONObject) {
                obj = A02((JSONObject) obj);
            }
            A0l.add(obj);
        }
        return A0l;
    }

    public static Map A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return C12970iu.A11();
        }
        try {
            return A02(C13000ix.A05(str));
        } catch (JSONException unused) {
            return C12970iu.A11();
        }
    }

    public static Map A02(JSONObject jSONObject) {
        HashMap A11 = C12970iu.A11();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String A0x = C12970iu.A0x(keys);
            Object obj = jSONObject.get(A0x);
            if (obj instanceof JSONArray) {
                obj = A00((JSONArray) obj);
            } else if (obj instanceof JSONObject) {
                obj = A02((JSONObject) obj);
            }
            A11.put(A0x, obj);
        }
        return A11;
    }
}
