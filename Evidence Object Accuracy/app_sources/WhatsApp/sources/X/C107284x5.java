package X;

import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.4x5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C107284x5 implements AbstractC116605Wc {
    public int A00;
    public long A01;
    public long A02;
    public final int A03;
    public final C100614mC A04;
    public final AbstractC14070ko A05;
    public final AnonymousClass5X6 A06;
    public final AnonymousClass4T4 A07;

    public C107284x5(AbstractC14070ko r7, AnonymousClass5X6 r8, AnonymousClass4T4 r9, String str, int i) {
        this.A05 = r7;
        this.A06 = r8;
        this.A07 = r9;
        int i2 = r9.A04;
        int i3 = (r9.A00 * i2) >> 3;
        int i4 = r9.A01;
        if (i4 == i3) {
            int i5 = r9.A03;
            int i6 = i5 * i3;
            int i7 = i6 << 3;
            int max = Math.max(i3, i6 / 10);
            this.A03 = max;
            C93844ap A00 = C93844ap.A00();
            A00.A0R = str;
            A00.A03 = i7;
            A00.A0A = i7;
            A00.A08 = max;
            A00.A04 = i2;
            A00.A0D = i5;
            A00.A09 = i;
            this.A04 = new C100614mC(A00);
            return;
        }
        StringBuilder A0k = C12960it.A0k("Expected block size: ");
        A0k.append(i3);
        throw AnonymousClass496.A00(C12960it.A0e("; got: ", A0k, i4));
    }

    @Override // X.AbstractC116605Wc
    public void AIZ(int i, long j) {
        this.A05.AbR(new C106934wW(this.A07, 1, (long) i, j));
        this.A06.AA6(this.A04);
    }

    @Override // X.AbstractC116605Wc
    public void Aaf(long j) {
        this.A02 = j;
        this.A00 = 0;
        this.A01 = 0;
    }

    @Override // X.AbstractC116605Wc
    public boolean AbE(AnonymousClass5Yf r18, long j) {
        int i;
        int i2;
        long j2 = j;
        while (true) {
            if (j2 <= 0 || (i = this.A00) >= (i2 = this.A03)) {
                break;
            }
            int AbF = this.A06.AbF(r18, (int) Math.min((long) (i2 - i), j2), 0, true);
            if (AbF == -1) {
                j2 = 0;
                break;
            }
            this.A00 += AbF;
            j2 -= (long) AbF;
        }
        AnonymousClass4T4 r4 = this.A07;
        int i3 = r4.A01;
        int i4 = this.A00;
        int i5 = i4 / i3;
        if (i5 > 0) {
            int i6 = i5 * i3;
            int i7 = i4 - i6;
            this.A06.AbG(null, 1, i6, i7, this.A02 + AnonymousClass3JZ.A07(this.A01, SearchActionVerificationClientService.MS_TO_NS, (long) r4.A03));
            this.A01 += (long) i5;
            this.A00 = i7;
        }
        return j2 <= 0;
    }
}
