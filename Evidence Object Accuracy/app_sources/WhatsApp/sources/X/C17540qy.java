package X;

/* renamed from: X.0qy  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C17540qy extends C17550qz {
    public static final int A03(int i) {
        if (i < 0) {
            return i;
        }
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        }
        return Integer.MAX_VALUE;
    }
}
