package X;

import java.util.Arrays;

/* renamed from: X.1V1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1V1 {
    public long A00;
    public long A01;
    public String A02;
    public String A03;
    public String A04;
    public final AbstractC15340mz A05;

    public AnonymousClass1V1(AbstractC15340mz r6) {
        this.A05 = r6;
        this.A04 = null;
        this.A00 = 0;
        this.A01 = Long.MAX_VALUE;
        this.A03 = null;
        this.A02 = null;
    }

    public AnonymousClass1V1(AbstractC15340mz r4, String str, long j) {
        this.A05 = r4;
        this.A04 = str;
        this.A00 = j;
        this.A01 = Long.MAX_VALUE;
        this.A03 = null;
        this.A02 = null;
    }

    public AnonymousClass1V1(AbstractC15340mz r3, String str, String str2, String str3, long j) {
        this.A05 = r3;
        this.A04 = str;
        this.A00 = j;
        this.A01 = Long.MAX_VALUE;
        this.A03 = str2;
        this.A02 = str3;
    }

    public boolean A00() {
        String str;
        AbstractC15340mz r1 = this.A05;
        return (!(r1 instanceof AbstractC16130oV) || (str = ((AbstractC16130oV) r1).A08) == null || !str.contains("static.whatsapp.net/downloadable?category=PSA")) && this.A04 == null;
    }

    public boolean A01(long j) {
        long j2 = this.A01;
        if (j2 == Long.MAX_VALUE) {
            if (j > this.A00) {
                return true;
            }
            return false;
        } else if (j2 + 86400000 < j) {
            return true;
        } else {
            return false;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1V1)) {
            return false;
        }
        AnonymousClass1V1 r7 = (AnonymousClass1V1) obj;
        if (!C29941Vi.A00(this.A05, r7.A05) || !C29941Vi.A00(this.A04, r7.A04) || this.A00 != r7.A00 || this.A01 != r7.A01 || !C29941Vi.A00(this.A03, r7.A03) || !C29941Vi.A00(this.A02, r7.A02)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A05, this.A04, Long.valueOf(this.A00), Long.valueOf(this.A01), this.A03, this.A02});
    }
}
