package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;
import com.whatsapp.jid.DeviceJid;

/* renamed from: X.34J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass34J extends AnonymousClass1OT {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(64);
    public long A00;
    public long A01;
    public DeviceJid A02;
    public String A03;

    public AnonymousClass34J(Parcel parcel) {
        super(parcel);
        this.A02 = (DeviceJid) C12990iw.A0I(parcel, DeviceJid.class);
        this.A03 = C12990iw.A0p(parcel);
        this.A00 = parcel.readLong();
        this.A01 = parcel.readLong();
    }

    public AnonymousClass34J(DeviceJid deviceJid, AnonymousClass1OT r2, String str, long j, long j2) {
        super(r2);
        this.A02 = deviceJid;
        this.A03 = str;
        this.A00 = j;
        this.A01 = j2;
    }

    @Override // X.AnonymousClass1OT, android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A02, i);
        parcel.writeString(this.A03);
        parcel.writeLong(this.A00);
        parcel.writeLong(this.A01);
    }
}
