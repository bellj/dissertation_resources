package X;

/* renamed from: X.5vt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C128305vt {
    public final AbstractC14640lm A00;
    public final C134146Dm A01;
    public final AbstractC1310961f A02;
    public final AbstractC136516Mv A03;
    public final C128295vs A04;
    public final C127785v3 A05;
    public final C127135u0 A06;
    public final C126095sK A07;
    public final C127145u1 A08;
    public final C127155u2 A09;
    public final AnonymousClass1KS A0A;
    public final Integer A0B;
    public final String A0C;
    public final String A0D;
    public final boolean A0E;

    public C128305vt(AbstractC14640lm r1, C134146Dm r2, AbstractC1310961f r3, AbstractC136516Mv r4, C128295vs r5, C127785v3 r6, C127135u0 r7, C126095sK r8, C127145u1 r9, C127155u2 r10, AnonymousClass1KS r11, Integer num, String str, String str2, boolean z) {
        this.A00 = r1;
        this.A0E = z;
        this.A0C = str;
        this.A0A = r11;
        this.A0B = num;
        this.A0D = str2;
        this.A02 = r3;
        this.A09 = r10;
        this.A07 = r8;
        this.A06 = r7;
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
        this.A08 = r9;
        this.A05 = r6;
    }
}
