package X;

import java.math.BigDecimal;

/* renamed from: X.3C7  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3C7 {
    public AnonymousClass2EC A00(AnonymousClass1V8 r8) {
        C30711Yn r2;
        if (r8 == null) {
            return null;
        }
        AnonymousClass1V8 A0E = r8.A0E("subtotal");
        AnonymousClass1V8 A0E2 = r8.A0E("total");
        AnonymousClass1V8 A0E3 = r8.A0E("currency");
        String A04 = C13000ix.A04(r8.A0E("price_status"));
        if (A0E3 == null || AnonymousClass1US.A0C(A0E3.A0G())) {
            r2 = null;
        } else {
            r2 = new C30711Yn(A0E3.A0G());
        }
        BigDecimal A0v = C12980iv.A0v(r2, A0E2);
        BigDecimal A0v2 = C12980iv.A0v(r2, A0E);
        if (A04 == null || r2 == null || A0v == null || A0v2 == null) {
            return null;
        }
        return new AnonymousClass2EC(r2, A0v2, A0v);
    }
}
