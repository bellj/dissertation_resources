package X;

import android.content.SharedPreferences;

/* renamed from: X.1o6  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C38441o6 {
    public static int A00(C14820m6 r1, int i) {
        SharedPreferences sharedPreferences;
        String str;
        int i2;
        if (i == 0) {
            return 0;
        }
        if (i == 1) {
            sharedPreferences = r1.A00;
            str = "autodownload_wifi_mask";
            i2 = 15;
        } else if (i == 2) {
            sharedPreferences = r1.A00;
            str = "autodownload_cellular_mask";
            i2 = 1;
        } else if (i == 3) {
            sharedPreferences = r1.A00;
            str = "autodownload_roaming_mask";
            i2 = 0;
        } else {
            throw new IllegalArgumentException("network_type not valid");
        }
        return sharedPreferences.getInt(str, i2);
    }
}
