package X;

import java.util.Map;

/* renamed from: X.2Kr  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC49422Kr {
    public Map A00;
    public final AbstractC15710nm A01;
    public final C14850m9 A02;
    public final C16120oU A03;
    public final C450720b A04;

    public AbstractC49422Kr(AbstractC15710nm r1, C14850m9 r2, C16120oU r3, C450720b r4, Map map) {
        this.A02 = r2;
        this.A01 = r1;
        this.A03 = r3;
        this.A04 = r4;
        this.A00 = map;
    }

    public void A00(Integer num, Integer num2, Integer num3, Integer num4, String str, String str2, String str3, String str4) {
        if (num != null) {
            if (num.intValue() == 11) {
                AbstractC15710nm r2 = this.A01;
                if (str2 != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("-");
                    sb.append(str2);
                    str = sb.toString();
                }
                r2.AaV("offline-count-11", str, false);
            }
            if (num.intValue() > 10) {
                C615030p r22 = new C615030p();
                r22.A03 = Long.valueOf(num.longValue());
                r22.A02 = num2;
                r22.A00 = num3;
                r22.A04 = str3;
                r22.A05 = str4;
                r22.A01 = num4;
                this.A03.A07(r22);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0180  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A01(X.AnonymousClass1V8 r40) {
        /*
        // Method dump skipped, instructions count: 404
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC49422Kr.A01(X.1V8):void");
    }
}
