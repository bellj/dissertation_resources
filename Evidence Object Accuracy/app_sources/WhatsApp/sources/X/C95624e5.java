package X;

import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Logger;
import sun.misc.Unsafe;

/* renamed from: X.4e5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95624e5 {
    public static final long A00 = ((long) A00(byte[].class));
    public static final long A01;
    public static final AnonymousClass4YX A02;
    public static final Class A03;
    public static final Logger A04 = C72463ee.A0M(C95624e5.class);
    public static final Unsafe A05;
    public static final boolean A06;
    public static final boolean A07;
    public static final boolean A08 = C12970iu.A1Z(ByteOrder.nativeOrder(), ByteOrder.BIG_ENDIAN);

    public static int A00(Class cls) {
        if (A06) {
            return A02.A00.arrayBaseOffset(cls);
        }
        return -1;
    }

    public static Object A01(Object obj, long j) {
        return A02.A00.getObject(obj, j);
    }

    public static Unsafe A03() {
        try {
            return (Unsafe) AccessController.doPrivileged(new C112005Br());
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void A04(Class cls) {
        if (A06) {
            A02.A00.arrayIndexScale(cls);
        }
    }

    public static void A07(Object obj, long j, Object obj2) {
        A02.A00.putObject(obj, j, obj2);
    }

    public static void A08(byte[] bArr, byte b, long j) {
        A02.A08(bArr, A00 + j, b);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(21:2|(1:7)(2:75|6)|9|(1:14)(2:69|13)|16|(1:(1:27)(1:(1:22)(1:(1:26))))|23|(3:71|28|(13:(1:34)(1:33)|38|39|(1:41)(4:67|49|(1:54)(1:53)|55)|42|(1:48)(1:46)|47|73|58|59|(1:63)|64|65))|37|39|(0)(0)|42|(1:44)|48|47|73|58|59|(2:61|63)|64|65) */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x013a A[EXC_TOP_SPLITTER, SYNTHETIC] */
    static {
        /*
        // Method dump skipped, instructions count: 586
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95624e5.<clinit>():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0007, code lost:
        if (X.AnonymousClass4ZU.A01 != false) goto L_0x0009;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.reflect.Field A02() {
        /*
            java.lang.Class r0 = X.AnonymousClass4ZU.A00
            if (r0 == 0) goto L_0x0009
            boolean r1 = X.AnonymousClass4ZU.A01
            r0 = 1
            if (r1 == 0) goto L_0x000a
        L_0x0009:
            r0 = 0
        L_0x000a:
            if (r0 == 0) goto L_0x001d
            java.lang.Class<java.nio.Buffer> r1 = java.nio.Buffer.class
            java.lang.String r0 = "effectiveDirectAddress"
            java.lang.reflect.Field r2 = r1.getDeclaredField(r0)     // Catch: all -> 0x0019
            r0 = 1
            r2.setAccessible(r0)     // Catch: all -> 0x0019
            goto L_0x001a
        L_0x0019:
            r2 = 0
        L_0x001a:
            if (r2 == 0) goto L_0x001d
            return r2
        L_0x001d:
            java.lang.Class<java.nio.Buffer> r1 = java.nio.Buffer.class
            java.lang.String r0 = "address"
            java.lang.reflect.Field r2 = r1.getDeclaredField(r0)     // Catch: all -> 0x002a
            r0 = 1
            r2.setAccessible(r0)     // Catch: all -> 0x002a
            goto L_0x002b
        L_0x002a:
            r2 = 0
        L_0x002b:
            if (r2 == 0) goto L_0x0036
            java.lang.Class r1 = r2.getType()
            java.lang.Class r0 = java.lang.Long.TYPE
            if (r1 != r0) goto L_0x0036
            return r2
        L_0x0036:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95624e5.A02():java.lang.reflect.Field");
    }

    public static void A05(Object obj, long j, byte b) {
        long j2 = -4 & j;
        AnonymousClass4YX r4 = A02;
        r4.A0B(obj, j2, C72453ed.A09(((int) j) ^ -1, r4.A04(obj, j2), b));
    }

    public static void A06(Object obj, long j, byte b) {
        long j2 = -4 & j;
        AnonymousClass4YX r4 = A02;
        r4.A0B(obj, j2, C72453ed.A09((int) j, r4.A04(obj, j2), b));
    }
}
