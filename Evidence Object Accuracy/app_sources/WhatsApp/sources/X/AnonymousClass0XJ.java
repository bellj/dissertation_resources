package X;

import android.view.Window;

/* renamed from: X.0XJ  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0XJ implements AbstractC12280hf {
    public final /* synthetic */ LayoutInflater$Factory2C011505o A00;

    public AnonymousClass0XJ(LayoutInflater$Factory2C011505o r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC12280hf
    public void AOF(AnonymousClass07H r10, boolean z) {
        AnonymousClass07H r0;
        AnonymousClass07H A01 = r10.A01();
        boolean z2 = false;
        if (A01 != r10) {
            z2 = true;
        }
        LayoutInflater$Factory2C011505o r5 = this.A00;
        if (z2) {
            r10 = A01;
        }
        AnonymousClass08Y[] r4 = r5.A0j;
        if (r4 != null) {
            for (AnonymousClass08Y r2 : r4) {
                if (r2 != null && (r0 = r2.A0A) == r10) {
                    if (z2) {
                        int i = r2.A01;
                        if (A01 == null) {
                            A01 = r0;
                        }
                        if (r2.A0C && !r5.A0b) {
                            ((Window$CallbackC013306g) r5.A0D).A00.onPanelClosed(i, A01);
                        }
                        r5.A0T(r2, true);
                        return;
                    } else {
                        r5.A0T(r2, z);
                        return;
                    }
                }
            }
        }
    }

    @Override // X.AbstractC12280hf
    public boolean ATF(AnonymousClass07H r4) {
        Window.Callback callback;
        if (r4 != r4.A01()) {
            return true;
        }
        LayoutInflater$Factory2C011505o r2 = this.A00;
        if (!r2.A0Z || (callback = r2.A08.getCallback()) == null || r2.A0b) {
            return true;
        }
        callback.onMenuOpened(C43951xu.A03, r4);
        return true;
    }
}
