package X;

import android.os.Message;
import com.whatsapp.util.Log;
import java.util.Map;

/* renamed from: X.2Kx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49482Kx extends AbstractC49422Kr {
    public C49482Kx(AbstractC15710nm r1, C14850m9 r2, C16120oU r3, C450720b r4, Map map) {
        super(r1, r2, r3, r4, map);
    }

    @Override // X.AbstractC49422Kr
    public void A01(AnonymousClass1V8 r8) {
        AnonymousClass1V8 A0D;
        C450720b r1;
        int i;
        String A0I = r8.A0I("type", null);
        String A0I2 = r8.A0I("id", null);
        String A0I3 = r8.A0I("xmlns", null);
        if (A0I != null) {
            if (A0I.equals("result")) {
                AbstractC35941j2 r0 = (AbstractC35941j2) this.A00.remove(A0I2);
                if (r0 != null) {
                    r0.A02(r8);
                }
            } else if (A0I.equals("error")) {
                AbstractC35941j2 r02 = (AbstractC35941j2) this.A00.remove(A0I2);
                if (r02 != null) {
                    r02.A01(r8);
                }
            } else if (A0I.equals("get")) {
                AnonymousClass1V8 A0D2 = r8.A0D(0);
                if ("urn:xmpp:ping".equals(A0I3)) {
                    long A01 = C28421Nd.A01(r8.A0I("t", null), 0);
                    C450720b r4 = this.A04;
                    Log.i("xmpp/reader/read/ping");
                    AbstractC450820c r6 = r4.A00;
                    Message obtain = Message.obtain(null, 0, 4, 0);
                    obtain.getData().putLong("timestamp", A01);
                    r6.AYY(obtain);
                } else if (AnonymousClass1V8.A02(A0D2, "relay")) {
                    String A0I4 = A0D2.A0I("pin", null);
                    A0D2.A0I("ip", null);
                    A0D2.A05("timeout", 0);
                    if (A0I4 != null) {
                        Log.e("onRelayRequest");
                    }
                }
            } else if (A0I.equals("set")) {
                if ("location".equals(A0I3)) {
                    r1 = this.A04;
                    i = 206;
                } else if ("md".equals(A0I3) && (A0D = r8.A0D(0)) != null) {
                    if (AnonymousClass1V8.A02(A0D, "pair-device")) {
                        r1 = this.A04;
                        i = 242;
                    } else if (AnonymousClass1V8.A02(A0D, "pair-success")) {
                        r1 = this.A04;
                        i = 243;
                    } else {
                        StringBuilder sb = new StringBuilder("unknown tag in multidevice IQ: ");
                        sb.append(A0D.A00);
                        throw new AnonymousClass1V9(sb.toString());
                    }
                }
                r1.A01(r8, null, i);
            } else {
                StringBuilder sb2 = new StringBuilder("unknown iq type attribute: ");
                sb2.append(A0I);
                throw new AnonymousClass1V9(sb2.toString());
            }
            if (A0I2 != null) {
                C450720b r2 = this.A04;
                StringBuilder sb3 = new StringBuilder("xmpp/reader/on-iq-response; id=");
                sb3.append(A0I2);
                Log.i(sb3.toString());
                r2.A00.ARW(r8, A0I2);
                return;
            }
            return;
        }
        throw new AnonymousClass1V9("missing 'type' attribute in iq stanza");
    }
}
