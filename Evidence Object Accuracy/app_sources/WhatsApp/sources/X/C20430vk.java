package X;

import android.content.SharedPreferences;

/* renamed from: X.0vk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C20430vk implements AbstractC16890pv {
    public final C20420vj A00;
    public final AnonymousClass01H A01;
    public final AnonymousClass01H A02;
    public final AnonymousClass01H A03;
    public final AnonymousClass01H A04;

    public C20430vk(C20420vj r2, AnonymousClass01H r3, AnonymousClass01H r4, AnonymousClass01H r5, AnonymousClass01H r6) {
        C16700pc.A0E(r3, 1);
        C16700pc.A0E(r4, 2);
        C16700pc.A0E(r2, 3);
        C16700pc.A0E(r5, 4);
        C16700pc.A0E(r6, 5);
        this.A04 = r3;
        this.A02 = r4;
        this.A00 = r2;
        this.A01 = r5;
        this.A03 = r6;
    }

    @Override // X.AbstractC16890pv
    public void AMJ() {
        C20420vj r5 = this.A00;
        synchronized (r5) {
            SharedPreferences.Editor edit = r5.A01().edit();
            edit.putInt("total_cold_start_count_pref", r5.A01().getInt("total_cold_start_count_pref", 0) + 1);
            AnonymousClass1SW.A00();
            edit.putInt("last_cold_start_time_min", (int) ((((r5.A01.A00() / 1000) / 60) / 10) * 10));
            edit.apply();
        }
    }
}
