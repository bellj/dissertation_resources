package X;

import android.text.TextUtils;
import java.util.Iterator;
import java.util.Stack;

/* renamed from: X.2Lp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C49642Lp extends Stack<C49652Lq> {
    public C49642Lp() {
        push(new C49652Lq(null, 0, "", 0));
    }

    public void A00(C49652Lq r6) {
        Integer num;
        C49652Lq peek;
        int i;
        if (size() > 0 && ((i = (peek = peek()).A00) != 1 ? i == 3 && r6.A00 == 3 && peek.A01 == r6.A01 : r6.A00 == 1)) {
            pop();
        }
        Iterator<C49652Lq> it = iterator();
        while (it.hasNext()) {
            if (it.next().A00 == r6.A00) {
                it.remove();
            }
        }
        int i2 = r6.A00;
        if (i2 == 3) {
            if (r6.A01 == null) {
                return;
            }
        } else if (i2 == 1) {
            if (TextUtils.isEmpty(r6.A04)) {
                return;
            }
        } else if (i2 == 2 && ((num = r6.A03) == null || num.intValue() == 0)) {
            return;
        }
        push(r6);
    }
}
