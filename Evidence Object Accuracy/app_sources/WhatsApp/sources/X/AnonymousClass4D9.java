package X;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

/* renamed from: X.4D9  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass4D9 {
    public static boolean A00(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (Build.VERSION.SDK_INT < 17 || (applicationInfo.flags & 4194304) == 0 || AnonymousClass4D8.A00(context) != 1) {
            return false;
        }
        return true;
    }
}
