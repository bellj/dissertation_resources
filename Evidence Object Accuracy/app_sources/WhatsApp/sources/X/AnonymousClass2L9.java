package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.2L9  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2L9 {
    public final /* synthetic */ HandlerThreadC43491x7 A00;

    public /* synthetic */ AnonymousClass2L9(HandlerThreadC43491x7 r1) {
        this.A00 = r1;
    }

    public void A00(AbstractC15340mz r12) {
        String str;
        UserJid userJid;
        AnonymousClass1IS r6 = r12.A0z;
        AnonymousClass2L8 r2 = this.A00.A00;
        if (r12.A1B) {
            if (r2.A0G.A05()) {
                str = "sender";
            }
            str = null;
        } else if (r12 instanceof AnonymousClass1X9) {
            str = "inactive";
        } else if (r12 instanceof C34771gg) {
            str = "hist_sync";
        } else {
            if (r12 instanceof C34811gk) {
                str = "peer_msg";
            }
            str = null;
        }
        AbstractC14640lm A0B = r12.A0B();
        DeviceJid deviceJid = r12.A17;
        if (r12.A1B) {
            AbstractC14640lm r1 = r6.A00;
            if (!(r1 instanceof AbstractC15590nW)) {
                userJid = UserJid.of(r1);
                r2.A0C(A0B, deviceJid, userJid, r6, str, null, r12.A14);
                StringBuilder sb = new StringBuilder("xmpp/writer/write/message-received; message.key=");
                sb.append(r6);
                Log.i(sb.toString());
            }
        }
        userJid = null;
        r2.A0C(A0B, deviceJid, userJid, r6, str, null, r12.A14);
        StringBuilder sb = new StringBuilder("xmpp/writer/write/message-received; message.key=");
        sb.append(r6);
        Log.i(sb.toString());
    }

    public void A01(AnonymousClass1OT r7, boolean z) {
        AnonymousClass2L8 r4 = this.A00.A00;
        AnonymousClass1V8 r3 = null;
        if (z) {
            r3 = new AnonymousClass1V8("features", new AnonymousClass1W9[]{new AnonymousClass1W9("readreceipts", "disable")}, (AnonymousClass1V8[]) null);
        }
        r4.A0J(r3, r7);
        StringBuilder sb = new StringBuilder("xmpp/writer/write/read-receipt-received; stanzaKey=");
        sb.append(r7);
        sb.append("; disable=");
        sb.append(z);
        Log.i(sb.toString());
    }
}
