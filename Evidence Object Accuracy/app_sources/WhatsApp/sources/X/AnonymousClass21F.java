package X;

/* renamed from: X.21F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21F {
    public final AnonymousClass21C A00;
    public final AnonymousClass218 A01;
    public final AnonymousClass217 A02;

    public AnonymousClass21F(AnonymousClass21C r1, AnonymousClass218 r2, AnonymousClass217 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.A02.A01);
        sb.append("|");
        sb.append(this.A01.A03);
        sb.append("|");
        sb.append(this.A00.A01);
        return sb.toString();
    }
}
