package X;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* renamed from: X.0Vz  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0Vz implements GestureDetector.OnGestureListener {
    public final /* synthetic */ AnonymousClass0BZ A00;

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onLongPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public void onShowPress(MotionEvent motionEvent) {
    }

    @Override // android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public AnonymousClass0Vz(AnonymousClass0BZ r1) {
        this.A00 = r1;
    }
}
