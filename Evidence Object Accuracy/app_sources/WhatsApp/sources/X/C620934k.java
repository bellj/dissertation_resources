package X;

import android.content.Context;
import android.text.TextUtils;
import android.widget.FrameLayout;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.whatsapp.R;
import com.whatsapp.search.SearchViewModel;
import java.util.Iterator;
import java.util.List;

/* renamed from: X.34k  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C620934k extends AbstractC621034l {
    public C15610nY A00;
    public boolean A01;

    public C620934k(Context context) {
        super(context);
        A01();
        addOnLayoutChangeListener(new View$OnLayoutChangeListenerC66113Mi(this));
        C12970iu.A18(getContext(), this, R.color.primary_surface);
    }

    @Override // X.AbstractC74153hP
    public void A01() {
        if (!this.A01) {
            this.A01 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            ((AbstractC621034l) this).A00 = C12960it.A0R(A00);
            this.A00 = C12960it.A0P(A00);
        }
    }

    public void A02(SearchViewModel searchViewModel, List list) {
        ChipGroup chipGroup = super.A01;
        chipGroup.removeAllViews();
        int maxChipWidth = getMaxChipWidth();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            C15370n3 A0a = C12970iu.A0a(it);
            if (A0a != null) {
                Chip chip = new Chip(getContext(), null);
                chip.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                String A04 = this.A00.A04(A0a);
                AnonymousClass02S A03 = ((AbstractC621034l) this).A00.A03();
                chip.setText(C12990iw.A0o(getResources(), A03.A02(A03.A01, A04), new Object[1], 0, R.string.search_contact_token_prefix));
                chip.setId(R.id.search_contact_token);
                chip.setClickable(true);
                C12960it.A14(chip, searchViewModel, A0a, 25);
                C12960it.A0s(getContext(), chip, R.color.secondary_text);
                chip.setChipBackgroundColorResource(R.color.searchChipBackground);
                chip.setEllipsize(TextUtils.TruncateAt.END);
                if (maxChipWidth > 0) {
                    chip.setMaxWidth(maxChipWidth);
                }
                chipGroup.addView(chip);
            }
        }
    }

    /* access modifiers changed from: private */
    public int getMaxChipWidth() {
        return (C12960it.A04(this, getWidth()) - super.A01.A01) >> 1;
    }
}
