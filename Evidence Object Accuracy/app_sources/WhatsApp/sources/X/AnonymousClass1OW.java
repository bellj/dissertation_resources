package X;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: X.1OW  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1OW {
    public static final Set A04;
    public final int A00;
    public final AnonymousClass1OX A01;
    public final AbstractC14440lR A02;
    public final ConcurrentMap A03 = new ConcurrentHashMap();

    static {
        HashSet hashSet = new HashSet();
        A04 = hashSet;
        hashSet.add((byte) 0);
    }

    public AnonymousClass1OW(AnonymousClass1OX r2, C14850m9 r3, AbstractC14440lR r4) {
        this.A02 = r4;
        this.A01 = r2;
        this.A00 = r3.A02(546);
    }
}
