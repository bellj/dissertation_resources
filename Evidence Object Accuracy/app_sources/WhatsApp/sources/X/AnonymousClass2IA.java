package X;

/* renamed from: X.2IA  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass2IA {
    public final C14850m9 A00;

    public AnonymousClass2IA(C14850m9 r1) {
        this.A00 = r1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r2.A07(1307) == false) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32361c2 A00(X.AnonymousClass2KT r15, X.C32701cb r16) {
        /*
            r14 = this;
            X.1c2 r0 = r15.A00
            if (r0 == 0) goto L_0x0024
            java.lang.String r1 = r0.A08
            java.lang.String r2 = r0.A02
            int r10 = r0.A01
            java.lang.String r3 = r0.A07
            java.lang.String r4 = r0.A03
            byte[] r8 = r0.A0C
            byte[] r9 = r0.A00
            java.lang.String r5 = r0.A05
            java.lang.String r6 = r0.A04
            java.lang.String r7 = r0.A06
            boolean r11 = r0.A0A
            boolean r12 = r0.A0B
            boolean r13 = r0.A09
        L_0x001e:
            X.1c2 r0 = new X.1c2
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            return r0
        L_0x0024:
            X.0m9 r2 = r14.A00
            r0 = 1388(0x56c, float:1.945E-42)
            boolean r0 = r2.A07(r0)
            r1 = 1307(0x51b, float:1.831E-42)
            if (r0 == 0) goto L_0x004c
            r0 = r16
            boolean r0 = r0.A05
            if (r0 == 0) goto L_0x003d
            boolean r0 = r2.A07(r1)
            r12 = 1
            if (r0 != 0) goto L_0x003e
        L_0x003d:
            r12 = 0
        L_0x003e:
            r1 = 0
            java.lang.String r7 = r15.A09
            r10 = 0
            r3 = r1
            r4 = r1
            r5 = r1
            r6 = r1
            r8 = r1
            r9 = r1
            r11 = 0
            r13 = 0
            r2 = r1
            goto L_0x001e
        L_0x004c:
            boolean r12 = r2.A07(r1)
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2IA.A00(X.2KT, X.1cb):X.1c2");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if ("ad".equals(r18.A00.A01) == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0035, code lost:
        if (r3.A07(1325) != false) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
        r13 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0038, code lost:
        r0 = r18.A06;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003a, code lost:
        if (r0 == null) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        r1 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        r15 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0045, code lost:
        if (r1.hasNext() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0047, code lost:
        r0 = (X.C92664Wv) r1.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        if (r15 != false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        if ((!android.text.TextUtils.isEmpty(r0.A01)) == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0059, code lost:
        r15 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005b, code lost:
        r15 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
        r3 = r18.A04;
        r4 = r18.A03;
        r1 = r18.A02;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0062, code lost:
        if (r1 == null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0064, code lost:
        r12 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0065, code lost:
        r0 = r18.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0067, code lost:
        if (r0 != null) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0069, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006a, code lost:
        if (r1 == null) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006c, code lost:
        r6 = r1.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006e, code lost:
        if (r0 != null) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0070, code lost:
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0071, code lost:
        r0 = r18.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007f, code lost:
        return new X.C32361c2(r3, r4, r5, r6, r0.A01, r0.A00, r0.A02, r10, null, r12, r13, r14, r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0080, code lost:
        r10 = r0.A01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0083, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0085, code lost:
        r5 = r0.A00;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0088, code lost:
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008b, code lost:
        if (r18.A01 == null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x008d, code lost:
        r12 = 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public X.C32361c2 A01(X.C32701cb r17, X.AnonymousClass1JF r18) {
        /*
            r16 = this;
            r2 = r18
            if (r18 != 0) goto L_0x0006
            r0 = 0
            return r0
        L_0x0006:
            r0 = r16
            X.0m9 r3 = r0.A00
            r0 = 1388(0x56c, float:1.945E-42)
            boolean r0 = r3.A07(r0)
            r1 = 1307(0x51b, float:1.831E-42)
            if (r0 == 0) goto L_0x001a
            r0 = r17
            boolean r0 = r0.A05
            if (r0 == 0) goto L_0x002d
        L_0x001a:
            boolean r0 = r3.A07(r1)
            if (r0 == 0) goto L_0x002d
            X.4QY r0 = r2.A00
            java.lang.String r1 = r0.A01
            java.lang.String r0 = "ad"
            boolean r0 = r0.equals(r1)
            r14 = 1
            if (r0 != 0) goto L_0x0037
        L_0x002d:
            r14 = 0
            r0 = 1325(0x52d, float:1.857E-42)
            boolean r0 = r3.A07(r0)
            r13 = 0
            if (r0 == 0) goto L_0x0038
        L_0x0037:
            r13 = 1
        L_0x0038:
            java.util.List r0 = r2.A06
            if (r0 == 0) goto L_0x005b
            java.util.Iterator r1 = r0.iterator()
        L_0x0040:
            r15 = 0
        L_0x0041:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x005c
            java.lang.Object r0 = r1.next()
            X.4Wv r0 = (X.C92664Wv) r0
            if (r15 != 0) goto L_0x0059
            java.lang.String r0 = r0.A01
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0040
        L_0x0059:
            r15 = 1
            goto L_0x0041
        L_0x005b:
            r15 = 0
        L_0x005c:
            java.lang.String r3 = r2.A04
            java.lang.String r4 = r2.A03
            X.4Kz r1 = r2.A02
            if (r1 == 0) goto L_0x0088
            r12 = 2
        L_0x0065:
            X.4OC r0 = r2.A01
            if (r0 != 0) goto L_0x0085
            r5 = 0
        L_0x006a:
            if (r1 == 0) goto L_0x0083
            java.lang.String r6 = r1.A00
        L_0x006e:
            if (r0 != 0) goto L_0x0080
            r10 = 0
        L_0x0071:
            r11 = 0
            X.4QY r0 = r2.A00
            java.lang.String r7 = r0.A01
            java.lang.String r8 = r0.A00
            java.lang.String r9 = r0.A02
            X.1c2 r2 = new X.1c2
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return r2
        L_0x0080:
            byte[] r10 = r0.A01
            goto L_0x0071
        L_0x0083:
            r6 = 0
            goto L_0x006e
        L_0x0085:
            java.lang.String r5 = r0.A00
            goto L_0x006a
        L_0x0088:
            X.4OC r0 = r2.A01
            r12 = 0
            if (r0 == 0) goto L_0x0065
            r12 = 1
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2IA.A01(X.1cb, X.1JF):X.1c2");
    }
}
