package X;

import android.content.Intent;
import com.whatsapp.instrumentation.service.InstrumentationFGService;
import java.util.HashSet;
import java.util.Set;

/* renamed from: X.137  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass137 {
    public final C16590pI A00;
    public final C22900zp A01;
    public final Set A02 = new HashSet();

    public AnonymousClass137(C16590pI r2, C22900zp r3) {
        this.A00 = r2;
        this.A01 = r3;
    }

    public final synchronized void A00(int i) {
        this.A02.add(Integer.valueOf(i));
        this.A01.A03(this.A00.A00, new Intent(), InstrumentationFGService.class);
    }

    public final synchronized void A01(int i) {
        Set set = this.A02;
        set.remove(Integer.valueOf(i));
        if (set.isEmpty()) {
            this.A01.A01(this.A00.A00, InstrumentationFGService.class);
        }
    }
}
