package X;

/* renamed from: X.5xT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C129285xT {
    public boolean A00 = false;
    public final ActivityC13790kL A01;
    public final C1329568x A02;
    public final AnonymousClass61M A03;
    public final C17070qD A04;
    public final AnonymousClass61E A05;
    public final AnonymousClass605 A06;
    public final C130015yf A07;
    public final AnonymousClass6LZ A08;
    public final AbstractC14440lR A09;

    public C129285xT(ActivityC13790kL r2, C1329568x r3, AnonymousClass61M r4, C17070qD r5, AnonymousClass61E r6, AnonymousClass605 r7, C130015yf r8, AbstractC14440lR r9) {
        this.A06 = r7;
        this.A07 = r8;
        this.A03 = r4;
        this.A05 = r6;
        this.A01 = r2;
        this.A08 = (AnonymousClass6LZ) r2;
        this.A09 = r9;
        this.A04 = r5;
        this.A02 = r3;
    }
}
