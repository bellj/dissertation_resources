package X;

/* renamed from: X.4W2  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4W2 {
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public long A04;
    public boolean A05;
    public final byte[] A06 = new byte[10];

    public void A00(AnonymousClass4WE r8) {
        if (this.A02 > 0) {
            AnonymousClass5X6 r0 = r8.A0c;
            long j = this.A04;
            r0.AbG(r8.A0b, this.A00, this.A03, this.A01, j);
            this.A02 = 0;
        }
    }
}
