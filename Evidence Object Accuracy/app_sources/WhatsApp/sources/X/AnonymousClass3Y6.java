package X;

import com.whatsapp.registration.report.BanReportViewModel;

/* renamed from: X.3Y6  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3Y6 implements AbstractC28771Oy {
    public final /* synthetic */ BanReportViewModel A00;

    @Override // X.AbstractC28771Oy
    public /* synthetic */ void APN(long j) {
    }

    public AnonymousClass3Y6(BanReportViewModel banReportViewModel) {
        this.A00 = banReportViewModel;
    }

    @Override // X.AbstractC28771Oy
    public void APP(boolean z) {
        C12960it.A1A(this.A00.A02, 1);
    }

    @Override // X.AbstractC28771Oy
    public void APQ(AnonymousClass1RN r4, C28781Oz r5) {
        int i = 0;
        if (r4.A00 == 0) {
            i = 1;
        }
        C12960it.A1A(this.A00.A02, C12980iv.A03(i));
    }
}
