package X;

import android.content.Context;
import com.whatsapp.mediacomposer.MediaComposerActivity;

/* renamed from: X.4qN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103204qN implements AbstractC009204q {
    public final /* synthetic */ MediaComposerActivity A00;

    public C103204qN(MediaComposerActivity mediaComposerActivity) {
        this.A00 = mediaComposerActivity;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
