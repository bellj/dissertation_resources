package X;

import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.jobqueue.job.RehydrateHsmJob;
import com.whatsapp.util.Log;

/* renamed from: X.1FA  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1FA {
    public final C15450nH A00;
    public final C20670w8 A01;
    public final C14830m7 A02;
    public final AnonymousClass018 A03;
    public final C15650ng A04;
    public final C26691El A05;
    public final C22170ye A06;
    public final C20660w7 A07;
    public final C20320vZ A08;

    public AnonymousClass1FA(C15450nH r1, C20670w8 r2, C14830m7 r3, AnonymousClass018 r4, C15650ng r5, C26691El r6, C22170ye r7, C20660w7 r8, C20320vZ r9) {
        this.A02 = r3;
        this.A07 = r8;
        this.A00 = r1;
        this.A06 = r7;
        this.A01 = r2;
        this.A03 = r4;
        this.A08 = r9;
        this.A04 = r5;
        this.A05 = r6;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 5, insn: 0x00bc: IGET  (r2 I:X.0w7) = (r5 I:X.1FA) X.1FA.A07 X.0w7, block:B:34:0x00bc
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
        	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
        */
    public final android.util.Pair A00(
/*
[270] Method generation error in method: X.1FA.A00(X.31D, X.4Kp, X.1Fy, X.1Pp):android.util.Pair, file: classes2.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r19v0 ??
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:228)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:195)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:151)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:344)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:271)
    	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.util.ArrayList.forEach(ArrayList.java:1259)
    	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    
*/

    public final AbstractC15340mz A01(AnonymousClass31D r18, C49692Lu r19, C28941Pp r20, C20320vZ r21) {
        r18.A06 = 1;
        String A03 = r20.A03();
        if ((r19.A00 & 8) == 8) {
            AnonymousClass2Ly r4 = r19.A03;
            if (r4 == null) {
                r4 = AnonymousClass2Ly.A07;
            }
            if (r4.A0b() == EnumC630139s.A02 && r4.A01 != 2) {
                StringBuilder sb = new StringBuilder("MessageUtil/validateHydratedTemplateMessage/error no title with text title, message key=");
                sb.append(A03);
                Log.w(sb.toString());
                throw new C43271wi(0);
            } else if ((r4.A00 & 32) == 32) {
                String str = r20.A0k;
                Jid jid = r20.A0g;
                AbstractC14640lm A00 = C15380n4.A00(jid);
                AnonymousClass009.A05(A00);
                long j = r20.A0f;
                AbstractC15340mz A002 = AnonymousClass3I0.A00(A00, UserJid.of(C15380n4.A00(r20.A08)), r19, r21, r20.A0P, r20.A0M, str, r20.A05, r20.A02, j, false, false);
                if (A002 != null) {
                    return A002;
                }
                this.A07.A0B(C15380n4.A00(jid), C15380n4.A00(r20.A08), null, str, null, null);
                return null;
            } else {
                StringBuilder sb2 = new StringBuilder("MessageUtil/validateHydratedTemplateMessage/error no content, message key=");
                sb2.append(A03);
                Log.w(sb2.toString());
                throw new C43271wi(0);
            }
        } else {
            throw new C43271wi(0);
        }
    }

    public final boolean A02(AnonymousClass31D r20, C89604Kp r21, C27081Fy r22, C28941Pp r23) {
        int i = r23.A01;
        if (i == 0) {
            r20.A06 = 1;
            String A03 = r23.A03();
            if ((r22.A00 & DefaultCrypto.BUFFER_SIZE) == 8192) {
                AnonymousClass229 r0 = r22.A0I;
                if (r0 == null) {
                    r0 = AnonymousClass229.A0A;
                }
                C32401c6.A0E(r0, A03);
                long A00 = this.A02.A00();
                C20670w8 r02 = this.A01;
                AnonymousClass018 r5 = this.A03;
                C20320vZ r9 = this.A08;
                String str = r23.A0k;
                AbstractC14640lm A002 = C15380n4.A00(r23.A0g);
                AnonymousClass009.A05(A002);
                r02.A00(new RehydrateHsmJob(r5, A002, C15380n4.A00(r23.A08), r22, r9, r23.A0P, null, str, r23.A05, r23.A02, r23.A0f, 86400000 + A00));
                return false;
            }
            throw new C43271wi(0);
        }
        StringBuilder sb = new StringBuilder("decryptioncallbackv2/invalid-edit-version edit=");
        sb.append(i);
        sb.append(", type=hsm, id=");
        sb.append(r23.A0k);
        Log.e(sb.toString());
        r21.A00.A00(19);
        return true;
    }
}
