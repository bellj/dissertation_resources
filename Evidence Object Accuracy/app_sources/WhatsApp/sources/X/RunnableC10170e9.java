package X;

import android.os.Build;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ListView;

/* renamed from: X.0e9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC10170e9 implements Runnable {
    public final /* synthetic */ AnonymousClass0WV A00;

    public RunnableC10170e9(AnonymousClass0WV r1) {
        this.A00 = r1;
    }

    @Override // java.lang.Runnable
    public void run() {
        View childAt;
        AnonymousClass0WV r5 = this.A00;
        if (r5.A04) {
            if (r5.A07) {
                r5.A07 = false;
                C05190Op r7 = r5.A0G;
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                r7.A06 = currentAnimationTimeMillis;
                r7.A07 = -1;
                r7.A05 = currentAnimationTimeMillis;
                r7.A00 = 0.5f;
            }
            C05190Op r9 = r5.A0G;
            if ((r9.A07 <= 0 || AnimationUtils.currentAnimationTimeMillis() <= r9.A07 + ((long) r9.A02)) && r5.A03()) {
                if (r5.A06) {
                    r5.A06 = false;
                    long uptimeMillis = SystemClock.uptimeMillis();
                    MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    r5.A0D.onTouchEvent(obtain);
                    obtain.recycle();
                }
                if (r9.A05 != 0) {
                    long currentAnimationTimeMillis2 = AnimationUtils.currentAnimationTimeMillis();
                    float A00 = r9.A00(currentAnimationTimeMillis2);
                    r9.A05 = currentAnimationTimeMillis2;
                    int i = (int) (((float) (currentAnimationTimeMillis2 - r9.A05)) * ((-4.0f * A00 * A00) + (A00 * 4.0f)) * r9.A01);
                    ListView listView = r5.A0F;
                    if (Build.VERSION.SDK_INT >= 19) {
                        C05790Qz.A00(listView, i);
                    } else {
                        int firstVisiblePosition = listView.getFirstVisiblePosition();
                        if (!(firstVisiblePosition == -1 || (childAt = listView.getChildAt(0)) == null)) {
                            listView.setSelectionFromTop(firstVisiblePosition, childAt.getTop() - i);
                        }
                    }
                    r5.A0D.postOnAnimation(this);
                    return;
                }
                throw new RuntimeException("Cannot compute scroll delta before calling start()");
            }
            r5.A04 = false;
        }
    }
}
