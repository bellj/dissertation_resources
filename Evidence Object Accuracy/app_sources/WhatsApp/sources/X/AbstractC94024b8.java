package X;

import android.os.IBinder;
import android.os.Parcel;

/* renamed from: X.4b8  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC94024b8 {
    public final int A00;
    public final boolean A01;
    public final C78603pB[] A02;

    @Deprecated
    public AbstractC94024b8() {
        this.A02 = null;
        this.A01 = false;
        this.A00 = 0;
    }

    public AbstractC94024b8(C78603pB[] r2, int i, boolean z) {
        this.A02 = r2;
        boolean z2 = false;
        if (r2 != null && z) {
            z2 = true;
        }
        this.A01 = z2;
        this.A00 = i;
    }

    public void A00(AnonymousClass5QV r6, C13690kA r7) {
        C98414ie r4;
        Parcel obtain;
        int i;
        IBinder asBinder;
        IBinder asBinder2;
        if (this instanceof C77803ns) {
            C77803ns r0 = (C77803ns) this;
            r0.A00 = r7;
            BinderC78843pd r1 = new BinderC78843pd(r0);
            C98444ih r42 = (C98444ih) ((AnonymousClass5Y4) ((AbstractC95064d1) r6).A03());
            Parcel obtain2 = Parcel.obtain();
            obtain2.writeInterfaceToken(r42.A01);
            obtain2.writeStrongBinder(r1.asBinder());
            Parcel obtain3 = Parcel.obtain();
            try {
                C12990iw.A18(r42.A00, obtain2, obtain3, 1);
            } finally {
                obtain2.recycle();
                obtain3.recycle();
            }
        } else if (!(this instanceof C77793nr)) {
            AbstractC77813nt r12 = (AbstractC77813nt) this;
            r12.A00 = r7;
            AnonymousClass5Y7 r43 = (AnonymousClass5Y7) ((AbstractC95064d1) r6).A03();
            if (r12 instanceof C77493nN) {
                C77493nN r13 = (C77493nN) r12;
                BinderC78973pq r2 = new BinderC78973pq(r13, r13);
                C78053oI r14 = r13.A00;
                r4 = (C98414ie) r43;
                obtain = Parcel.obtain();
                obtain.writeInterfaceToken(r4.A01);
                obtain.writeStrongBinder(r2.asBinder());
                C95154dE.A00(obtain, r14);
                i = 6;
            } else if (!(r12 instanceof C77473nL)) {
                C77463nK r15 = (C77463nK) r12;
                AbstractBinderC78993ps r22 = ((AbstractC77483nM) r15).A00;
                C78303oh r16 = r15.A00;
                r4 = (C98414ie) r43;
                obtain = Parcel.obtain();
                obtain.writeInterfaceToken(r4.A01);
                if (r22 == null) {
                    asBinder2 = null;
                } else {
                    asBinder2 = r22.asBinder();
                }
                obtain.writeStrongBinder(asBinder2);
                C95154dE.A00(obtain, r16);
                i = 5;
            } else {
                C77473nL r17 = (C77473nL) r12;
                AbstractBinderC78993ps r23 = ((AbstractC77483nM) r17).A00;
                C78293og r18 = r17.A00;
                r4 = (C98414ie) r43;
                obtain = Parcel.obtain();
                obtain.writeInterfaceToken(r4.A01);
                if (r23 == null) {
                    asBinder = null;
                } else {
                    asBinder = r23.asBinder();
                }
                obtain.writeStrongBinder(asBinder);
                C95154dE.A00(obtain, r18);
                i = 9;
            }
            r4.A01(i, obtain);
        } else {
            ((C77793nr) this).A00.A01.A5Y(r6, r7);
        }
    }
}
