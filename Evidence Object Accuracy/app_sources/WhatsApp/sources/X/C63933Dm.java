package X;

import com.whatsapp.util.Log;
import org.json.JSONException;

/* renamed from: X.3Dm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63933Dm {
    public final /* synthetic */ AbstractC16320oo A00;
    public final /* synthetic */ C90934Pu A01;

    public C63933Dm(AbstractC16320oo r1, C90934Pu r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public void A00(AnonymousClass4N2 r5) {
        C90934Pu r1;
        int i;
        try {
            AbstractC16320oo r0 = this.A00;
            r1 = this.A01;
            AbstractC16320oo.A00(r0, r5, r1);
        } catch (JSONException e) {
            this.A00.A02.AaV("BusinessDirectoryNetworkRequest/createResponseCallback/onResponseReceived: Error while parsing the JSON: ", e.getMessage(), true);
            r1 = this.A01;
            i = 2;
            r1.A00 = i;
            this.A00.A06(r1);
        } catch (Exception e2) {
            Log.e("BusinessDirectoryNetworkRequest/createResponseCallback/onResponseReceived: generic error - ", e2);
            r1 = this.A01;
            i = 3;
            r1.A00 = i;
            this.A00.A06(r1);
        }
        this.A00.A06(r1);
    }
}
