package X;

/* renamed from: X.5Xp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public interface AbstractC116965Xp {
    public static final AnonymousClass1TK A00 = C72453ed.A12("1.3.6.1.4.1.11591.14");
    public static final AnonymousClass1TK A01 = C72453ed.A12("1.3.6.1.4.1.11591.14.1");
    public static final AnonymousClass1TK A02;
    public static final AnonymousClass1TK A03 = C72453ed.A12("1.3.6.1.4.1.11591.1");
    public static final AnonymousClass1TK A04 = C72453ed.A12("1.3.6.1.4.1.11591.2");
    public static final AnonymousClass1TK A05 = C72453ed.A12("1.3.6.1.4.1.11591.3");
    public static final AnonymousClass1TK A06 = C72453ed.A12("1.3.6.1.4.1.11591.13.2");
    public static final AnonymousClass1TK A07 = C72453ed.A12("1.3.6.1.4.1.11591.13.2.2");
    public static final AnonymousClass1TK A08 = C72453ed.A12("1.3.6.1.4.1.11591.13.2.4");
    public static final AnonymousClass1TK A09 = C72453ed.A12("1.3.6.1.4.1.11591.13.2.1");
    public static final AnonymousClass1TK A0A = C72453ed.A12("1.3.6.1.4.1.11591.13.2.3");
    public static final AnonymousClass1TK A0B = C72453ed.A12("1.3.6.1.4.1.11591.13.2.22");
    public static final AnonymousClass1TK A0C = C72453ed.A12("1.3.6.1.4.1.11591.13.2.24");
    public static final AnonymousClass1TK A0D = C72453ed.A12("1.3.6.1.4.1.11591.13.2.21");
    public static final AnonymousClass1TK A0E = C72453ed.A12("1.3.6.1.4.1.11591.13.2.23");
    public static final AnonymousClass1TK A0F = C72453ed.A12("1.3.6.1.4.1.11591.13.2.42");
    public static final AnonymousClass1TK A0G = C72453ed.A12("1.3.6.1.4.1.11591.13.2.44");
    public static final AnonymousClass1TK A0H = C72453ed.A12("1.3.6.1.4.1.11591.13.2.41");
    public static final AnonymousClass1TK A0I = C72453ed.A12("1.3.6.1.4.1.11591.13.2.43");
    public static final AnonymousClass1TK A0J = C72453ed.A12("1.3.6.1.4.1.11591.12.2");
    public static final AnonymousClass1TK A0K = C72453ed.A12("1.3.6.1.4.1.11591.12");
    public static final AnonymousClass1TK A0L;
    public static final AnonymousClass1TK A0M = C72453ed.A12("1.3.6.1.4.1.11591.13");
    public static final AnonymousClass1TK A0N = C72453ed.A12("1.3.6.1.4.1.11591.2.1");
    public static final AnonymousClass1TK A0O = C72453ed.A12("1.3.6.1.4.1.11591.2.1.1");

    static {
        AnonymousClass1TK A12 = C72453ed.A12("1.3.6.1.4.1.11591.15");
        A0L = A12;
        A02 = AnonymousClass1TL.A02("1", A12);
    }
}
