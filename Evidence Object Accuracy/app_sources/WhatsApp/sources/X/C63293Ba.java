package X;

/* renamed from: X.3Ba  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C63293Ba {
    public final C15570nT A00;
    public final AnonymousClass17J A01;
    public final C14830m7 A02;
    public final C235812f A03;
    public final C18840t8 A04;
    public final AnonymousClass01H A05;
    public final AnonymousClass01N A06;

    public C63293Ba(C15570nT r2, AnonymousClass17J r3, C14830m7 r4, C235812f r5, C18840t8 r6, AnonymousClass01H r7, AnonymousClass01N r8) {
        C16700pc.A0G(r4, r6);
        C16700pc.A0E(r2, 3);
        C16700pc.A0E(r5, 5);
        C16700pc.A0E(r7, 6);
        C16700pc.A0E(r8, 7);
        this.A02 = r4;
        this.A04 = r6;
        this.A00 = r2;
        this.A01 = r3;
        this.A03 = r5;
        this.A05 = r7;
        this.A06 = r8;
    }
}
