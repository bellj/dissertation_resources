package X;

import android.widget.ProgressBar;
import com.whatsapp.R;

/* renamed from: X.68I  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass68I implements AnonymousClass2DH {
    public final /* synthetic */ ProgressBar A00;
    public final /* synthetic */ AbstractActivityC121705jc A01;

    public AnonymousClass68I(ProgressBar progressBar, AbstractActivityC121705jc r2) {
        this.A01 = r2;
        this.A00 = progressBar;
    }

    @Override // X.AnonymousClass2DH
    public void ALn() {
        this.A00.setVisibility(8);
        AbstractActivityC121705jc r3 = this.A01;
        ((ActivityC13810kN) r3).A05.A07(R.string.payments_not_ready, 0);
        r3.finish();
    }

    @Override // X.AnonymousClass2DH
    public void APk() {
        this.A01.finish();
    }

    @Override // X.AnonymousClass2DH
    public void AWu() {
        this.A00.setVisibility(8);
        this.A01.A2j();
    }

    @Override // X.AnonymousClass2DH
    public void AXX() {
        this.A00.setVisibility(8);
        AbstractActivityC121705jc r3 = this.A01;
        ((ActivityC13810kN) r3).A05.A07(R.string.payments_not_ready, 0);
        r3.finish();
    }
}
