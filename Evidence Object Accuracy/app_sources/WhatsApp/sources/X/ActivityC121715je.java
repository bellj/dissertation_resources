package X;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.R;
import com.whatsapp.payments.ui.widget.PayToolbar;

/* renamed from: X.5je  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class ActivityC121715je extends AbstractActivityC119265dR {
    public RecyclerView A00;
    public PayToolbar A01;
    public final C30931Zj A02 = C117305Zk.A0V("PaymentComponentListActivity", "infra");
    public final C118575c2 A03 = new C118575c2(this);

    public AnonymousClass03U A2e(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater;
        int i2;
        this.A02.A04(C12960it.A0W(i, "Create view holder for "));
        switch (i) {
            case 100:
                return new C122325lH(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_common_component_section_header));
            case 101:
            default:
                throw C12990iw.A0m(C30931Zj.A01("PaymentComponentListActivity", C12960it.A0W(i, "no valid mapping for: ")));
            case 102:
                layoutInflater = C12960it.A0E(viewGroup);
                i2 = R.layout.payment_common_component_section_separator;
                break;
            case 103:
                layoutInflater = C12960it.A0E(viewGroup);
                i2 = R.layout.divider;
                break;
            case 104:
                return new C122405lP(C12960it.A0F(C12960it.A0E(viewGroup), viewGroup, R.layout.payment_common_component_centered_title));
        }
        return new C122495lY(C12960it.A0F(layoutInflater, viewGroup, i2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0077, code lost:
        if (r1 == false) goto L_0x0079;
     */
    @Override // X.ActivityC13790kL, X.ActivityC13810kN, X.ActivityC13830kP, X.AbstractActivityC13840kQ, X.ActivityC000900k, X.ActivityC001000l, X.AbstractActivityC001100m, android.app.Activity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            super.onCreate(r10)
            r1 = r9
            boolean r0 = r9 instanceof com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity
            if (r0 != 0) goto L_0x00b3
            boolean r0 = r9 instanceof com.whatsapp.payments.ui.IndiaUpiMandateHistoryActivity
            if (r0 != 0) goto L_0x00b9
        L_0x000c:
            boolean r0 = r9 instanceof com.whatsapp.payments.ui.NoviPayHubManageTopUpActivity
            if (r0 != 0) goto L_0x00ae
            r0 = 2131559463(0x7f0d0427, float:1.874427E38)
        L_0x0013:
            r9.setContentView(r0)
            r3 = r9
            boolean r0 = r9 instanceof com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity
            if (r0 != 0) goto L_0x009d
            boolean r0 = r9 instanceof X.AbstractActivityC121465iB
            if (r0 != 0) goto L_0x0056
            r0 = 2131100178(0x7f060212, float:1.781273E38)
            int r2 = X.AnonymousClass00T.A00(r9, r0)
            r0 = 2131364886(0x7f0a0c16, float:1.8349622E38)
            android.view.View r0 = r9.findViewById(r0)
            com.whatsapp.payments.ui.widget.PayToolbar r0 = (com.whatsapp.payments.ui.widget.PayToolbar) r0
            r9.A01 = r0
            r9.A1e(r0)
            X.02i r1 = r9.A1U()
            if (r1 == 0) goto L_0x0040
            r0 = 2131888219(0x7f12085b, float:1.9411067E38)
            X.C117295Zj.A0h(r9, r1, r0, r2)
        L_0x0040:
            r0 = 2131364907(0x7f0a0c2b, float:1.8349664E38)
            android.view.View r0 = r9.findViewById(r0)
            androidx.recyclerview.widget.RecyclerView r0 = (androidx.recyclerview.widget.RecyclerView) r0
            r9.A00 = r0
            X.C12990iw.A1K(r0)
            androidx.recyclerview.widget.RecyclerView r1 = r9.A00
            X.5c2 r0 = r9.A03
            r1.setAdapter(r0)
            return
        L_0x0056:
            X.5iB r3 = (X.AbstractActivityC121465iB) r3
            boolean r0 = r3 instanceof com.whatsapp.payments.ui.NoviPayHubActivity
            com.whatsapp.payments.ui.widget.PayToolbar r5 = X.C117305Zk.A0a(r3)
            r3.A01 = r5
            X.018 r4 = r3.A01
            if (r0 != 0) goto L_0x0096
            boolean r1 = r3 instanceof com.whatsapp.payments.ui.NoviPayHubSecurityActivity
            if (r1 != 0) goto L_0x0092
            boolean r0 = r3 instanceof com.whatsapp.payments.ui.NoviPayHubManageTopUpActivity
            if (r0 != 0) goto L_0x008e
            boolean r0 = r3 instanceof com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity
            if (r0 != 0) goto L_0x008b
            r0 = 2131889707(0x7f120e2b, float:1.9414085E38)
        L_0x0073:
            java.lang.String r6 = r3.getString(r0)
            if (r1 != 0) goto L_0x0089
        L_0x0079:
            boolean r0 = r3 instanceof com.whatsapp.payments.ui.NoviPayHubManageTopUpActivity
            if (r0 != 0) goto L_0x0089
            boolean r0 = r3 instanceof com.whatsapp.payments.ui.NoviPayHubAddPaymentMethodActivity
            if (r0 == 0) goto L_0x0089
            r8 = 1
        L_0x0082:
            r7 = 2131231700(0x7f0803d4, float:1.8079488E38)
        L_0x0085:
            X.C130305zC.A01(r3, r4, r5, r6, r7, r8)
            goto L_0x0040
        L_0x0089:
            r8 = 0
            goto L_0x0082
        L_0x008b:
            java.lang.String r6 = ""
            goto L_0x0079
        L_0x008e:
            r0 = 2131889788(0x7f120e7c, float:1.941425E38)
            goto L_0x0073
        L_0x0092:
            r0 = 2131889910(0x7f120ef6, float:1.9414497E38)
            goto L_0x0073
        L_0x0096:
            java.lang.String r6 = ""
            r8 = 1
            r7 = 2131231819(0x7f08044b, float:1.807973E38)
            goto L_0x0085
        L_0x009d:
            com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity r3 = (com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity) r3
            com.whatsapp.payments.ui.widget.PayToolbar r2 = X.C117305Zk.A0a(r3)
            r3.A01 = r2
            X.018 r1 = r3.A0C
            r0 = 2131892265(0x7f121829, float:1.9419273E38)
            X.C130305zC.A00(r3, r1, r2, r0)
            goto L_0x0040
        L_0x00ae:
            r0 = 2131559367(0x7f0d03c7, float:1.8744076E38)
            goto L_0x0013
        L_0x00b3:
            com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity r1 = (com.whatsapp.payments.ui.PaymentTransactionDetailsListActivity) r1
            boolean r0 = r1 instanceof com.whatsapp.payments.ui.NoviPaymentTransactionDetailsActivity
            if (r0 != 0) goto L_0x000c
        L_0x00b9:
            r0 = 2131559462(0x7f0d0426, float:1.8744269E38)
            r9.setContentView(r0)
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: X.ActivityC121715je.onCreate(android.os.Bundle):void");
    }
}
