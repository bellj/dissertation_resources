package X;

/* renamed from: X.3hg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74263hg extends C006202y {
    public AnonymousClass5VB A00;

    public C74263hg(int i) {
        super(i);
    }

    @Override // X.C006202y
    public void A09(Object obj, Object obj2, Object obj3, boolean z) {
        AnonymousClass5VB r0 = this.A00;
        if (r0 != null) {
            r0.APj(obj, obj2, obj3, z);
        }
    }
}
