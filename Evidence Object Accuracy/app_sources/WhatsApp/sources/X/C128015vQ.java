package X;

import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import java.util.List;

/* renamed from: X.5vQ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C128015vQ {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass3FE A01;
    public final /* synthetic */ BrazilPayBloksActivity A02;
    public final /* synthetic */ String A03;
    public final /* synthetic */ List A04;
    public final /* synthetic */ List A05;

    public C128015vQ(AnonymousClass3FE r1, BrazilPayBloksActivity brazilPayBloksActivity, String str, List list, List list2, int i) {
        this.A02 = brazilPayBloksActivity;
        this.A04 = list;
        this.A05 = list2;
        this.A03 = str;
        this.A01 = r1;
        this.A00 = i;
    }
}
