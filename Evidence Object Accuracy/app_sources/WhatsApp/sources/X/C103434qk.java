package X;

import android.content.Context;
import com.whatsapp.settings.SettingsAccount;

/* renamed from: X.4qk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103434qk implements AbstractC009204q {
    public final /* synthetic */ SettingsAccount A00;

    public C103434qk(SettingsAccount settingsAccount) {
        this.A00 = settingsAccount;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
