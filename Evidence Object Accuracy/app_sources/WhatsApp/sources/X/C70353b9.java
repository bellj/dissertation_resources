package X;

import android.graphics.Bitmap;
import android.view.View;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.3b9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C70353b9 implements AbstractC41521tf {
    public final /* synthetic */ int A00;
    public final /* synthetic */ AnonymousClass35X A01;

    @Override // X.AbstractC41521tf
    public /* synthetic */ void AQV() {
    }

    public C70353b9(AnonymousClass35X r1, int i) {
        this.A01 = r1;
        this.A00 = i;
    }

    @Override // X.AbstractC41521tf
    public int AGm() {
        return this.A00;
    }

    @Override // X.AbstractC41521tf
    public void Adg(Bitmap bitmap, View view, AbstractC15340mz r4) {
        this.A01.A0F.A05(bitmap);
    }

    @Override // X.AbstractC41521tf
    public void Adu(View view) {
        PhotoView photoView = this.A01.A0F;
        photoView.A0J = null;
        photoView.A04 = 0.0f;
    }
}
