package X;

import java.util.Arrays;

/* renamed from: X.0jm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C13470jm {
    public final long A00;
    public final String A01;

    public C13470jm(String str, long j) {
        C13020j0.A01(str);
        this.A01 = str;
        this.A00 = j;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof C13470jm) {
            C13470jm r7 = (C13470jm) obj;
            if (this.A00 == r7.A00 && this.A01.equals(r7.A01)) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, Long.valueOf(this.A00)});
    }
}
