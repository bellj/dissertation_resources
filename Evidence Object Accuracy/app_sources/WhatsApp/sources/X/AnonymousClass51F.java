package X;

import com.whatsapp.reactions.ReactionsBottomSheetDialogFragment;

/* renamed from: X.51F  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass51F implements AbstractC467627l {
    public final /* synthetic */ ReactionsBottomSheetDialogFragment A00;

    public AnonymousClass51F(ReactionsBottomSheetDialogFragment reactionsBottomSheetDialogFragment) {
        this.A00 = reactionsBottomSheetDialogFragment;
    }

    @Override // X.AbstractC467727m
    public void AXN(AnonymousClass3FN r2) {
        ReactionsBottomSheetDialogFragment.A00(r2, this.A00);
    }

    @Override // X.AbstractC467727m
    public void AXO(AnonymousClass3FN r2) {
        ReactionsBottomSheetDialogFragment.A00(r2, this.A00);
    }
}
