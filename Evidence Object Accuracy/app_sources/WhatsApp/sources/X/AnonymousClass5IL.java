package X;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.5IL  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5IL extends LinkedHashMap<String, String> {
    public final /* synthetic */ AnonymousClass1NS this$1;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass5IL(AnonymousClass1NS r4) {
        super(64, 0.75f, true);
        this.this$1 = r4;
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.util.Map$Entry] */
    @Override // java.util.LinkedHashMap
    public boolean removeEldestEntry(Map.Entry<String, String> entry) {
        return C72463ee.A0Y(size(), 64);
    }
}
