package X;

import com.whatsapp.jid.GroupJid;
import java.util.List;

/* renamed from: X.3Bd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63323Bd {
    public final int A00;
    public final GroupJid A01;
    public final AnonymousClass1JV A02;
    public final AnonymousClass1P4 A03;
    public final String A04;
    public final String A05;
    public final List A06;
    public final boolean A07;

    public /* synthetic */ C63323Bd(GroupJid groupJid, AnonymousClass1JV r2, AnonymousClass1P4 r3, String str, String str2, List list, int i, boolean z) {
        this.A02 = r2;
        this.A05 = str;
        this.A04 = str2;
        this.A06 = list;
        this.A03 = r3;
        this.A00 = i;
        this.A01 = groupJid;
        this.A07 = z;
    }
}
