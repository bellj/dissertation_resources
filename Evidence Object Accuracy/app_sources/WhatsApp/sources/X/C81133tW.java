package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.3tW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81133tW extends C81243th<K, Collection<V>> {
    public final /* synthetic */ AbstractC80933tC this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C81133tW(AbstractC80933tC r1, Map map) {
        super(map);
        this.this$0 = r1;
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public void clear() {
        AnonymousClass1I4.clear(iterator());
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean containsAll(Collection collection) {
        return map().keySet().containsAll(collection);
    }

    @Override // java.util.AbstractSet, java.util.Collection, java.util.Set, java.lang.Object
    public boolean equals(Object obj) {
        return this == obj || map().keySet().equals(obj);
    }

    @Override // java.util.AbstractSet, java.util.Collection, java.util.Set, java.lang.Object
    public int hashCode() {
        return map().keySet().hashCode();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator iterator() {
        return new AnonymousClass5DE(this, C12960it.A0n(map()));
    }

    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean remove(Object obj) {
        Collection collection = (Collection) map().remove(obj);
        if (collection == null) {
            return false;
        }
        int size = collection.size();
        collection.clear();
        AbstractC80933tC.access$220(this.this$0, size);
        if (size > 0) {
            return true;
        }
        return false;
    }
}
