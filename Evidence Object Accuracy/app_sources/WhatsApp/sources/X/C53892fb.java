package X;

import com.facebook.redex.RunnableBRunnable0Shape15S0100000_I1_1;
import com.facebook.redex.RunnableBRunnable0Shape1S0110000_I1;
import com.whatsapp.R;

/* renamed from: X.2fb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C53892fb extends AnonymousClass015 {
    public boolean A00;
    public boolean A01;
    public final AnonymousClass016 A02 = C12980iv.A0T();
    public final AnonymousClass016 A03;
    public final C14900mE A04;
    public final C21220x4 A05;
    public final C18640sm A06;
    public final C14840m8 A07;
    public final C27691It A08 = C13000ix.A03();
    public final C27691It A09 = C13000ix.A03();
    public final AbstractC14440lR A0A;

    public C53892fb(C14900mE r4, C21220x4 r5, C18640sm r6, C14820m6 r7, C14840m8 r8, AbstractC14440lR r9, boolean z, boolean z2) {
        AnonymousClass016 A0T = C12980iv.A0T();
        this.A03 = A0T;
        this.A04 = r4;
        this.A0A = r9;
        this.A00 = z;
        this.A07 = r8;
        this.A01 = z2;
        this.A05 = r5;
        this.A06 = r6;
        C12990iw.A1J(A0T, C12980iv.A1W(r7.A00, "companion_reg_opt_in_enabled"));
    }

    public final void A04(boolean z) {
        int i;
        if (!this.A06.A0B()) {
            this.A09.A0B(new AnonymousClass4KJ(R.string.connectivity_check_connection));
            return;
        }
        AnonymousClass016 r1 = this.A02;
        if (this.A07.A02()) {
            i = R.string.md_forced_opt_in_in_progress;
        } else {
            i = R.string.md_opt_out_in_progress;
            if (z) {
                i = R.string.md_opt_in_in_progress;
            }
        }
        C12960it.A1A(r1, i);
        if (z || !this.A00) {
            this.A0A.Ab2(new RunnableBRunnable0Shape1S0110000_I1(this, 4, z));
            this.A00 = false;
            this.A01 = false;
            this.A04.A0J(new RunnableBRunnable0Shape1S0110000_I1(this, 5, z), 1000);
            return;
        }
        this.A0A.Ab2(new RunnableBRunnable0Shape15S0100000_I1_1(this, 19));
    }
}
