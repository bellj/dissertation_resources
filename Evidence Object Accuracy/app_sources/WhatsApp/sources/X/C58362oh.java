package X;

import android.view.View;
import android.view.ViewGroup;
import com.whatsapp.mediaview.MediaViewBaseFragment;
import com.whatsapp.mediaview.PhotoView;

/* renamed from: X.2oh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C58362oh extends AbstractC75803kU {
    public final AnonymousClass5XC A00;
    public final /* synthetic */ MediaViewBaseFragment A01;

    @Override // X.AnonymousClass01A
    public CharSequence A04(int i) {
        return "";
    }

    public C58362oh(AnonymousClass5XC r1, MediaViewBaseFragment mediaViewBaseFragment) {
        this.A01 = mediaViewBaseFragment;
        this.A00 = r1;
    }

    @Override // X.AnonymousClass01A
    public int A01() {
        return this.A00.getCount();
    }

    @Override // X.AnonymousClass01A
    public void A0A(ViewGroup viewGroup) {
        this.A00.AQb();
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ int A0F(Object obj) {
        Object obj2;
        AnonymousClass01T r3 = (AnonymousClass01T) obj;
        if (r3.A00 == null || (obj2 = r3.A01) == null) {
            return -2;
        }
        return this.A00.AFp(obj2);
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ Object A0G(ViewGroup viewGroup, int i) {
        AnonymousClass01T A8c = this.A00.A8c(i);
        Object obj = A8c.A00;
        if (obj != null) {
            View view = (View) obj;
            Object obj2 = A8c.A01;
            if (MediaViewBaseFragment.A0H) {
                this.A01.A1K(view);
            }
            view.setTag(obj2);
            viewGroup.addView(view, 0);
        }
        return A8c;
    }

    @Override // X.AbstractC75803kU
    public /* bridge */ /* synthetic */ void A0I(ViewGroup viewGroup, Object obj, int i) {
        Object obj2 = ((AnonymousClass01T) obj).A00;
        if (obj2 != null) {
            View view = (View) obj2;
            viewGroup.removeView(view);
            PhotoView.A00(view);
        }
        this.A00.A8t(i);
    }

    @Override // X.AbstractC75803kU
    public boolean A0J(View view, Object obj) {
        return C12970iu.A1Z(view, ((AnonymousClass01T) obj).A00);
    }
}
