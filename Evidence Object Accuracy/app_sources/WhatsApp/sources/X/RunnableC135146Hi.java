package X;

import java.util.List;

/* renamed from: X.6Hi  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class RunnableC135146Hi implements Runnable {
    public final /* synthetic */ C128965wx A00;
    public final /* synthetic */ List A01;

    public RunnableC135146Hi(C128965wx r1, List list) {
        this.A00 = r1;
        this.A01 = list;
    }

    @Override // java.lang.Runnable
    public void run() {
        List list = this.A01;
        if (0 < list.size()) {
            list.get(0);
            throw C12980iv.A0n("onPreviewStopped");
        }
    }
}
