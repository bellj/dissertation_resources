package X;

import com.facebook.redex.RunnableBRunnable0Shape0S0300000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0400000_I0;
import com.facebook.redex.RunnableBRunnable0Shape0S0510000_I0;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: X.1Or  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1Or {
    public final int A00;
    public final C28741Ov A01;
    public final CopyOnWriteArrayList A02;

    public AnonymousClass1Or(C28741Ov r1, CopyOnWriteArrayList copyOnWriteArrayList, int i) {
        this.A02 = copyOnWriteArrayList;
        this.A00 = i;
        this.A01 = r1;
    }

    public final long A00(long j) {
        long A02 = C95214dK.A02(j);
        if (A02 != -9223372036854775807L) {
            return 0 + A02;
        }
        return -9223372036854775807L;
    }

    public void A01(C28721Ot r9, C28731Ou r10) {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            AnonymousClass4M2 r0 = (AnonymousClass4M2) it.next();
            AnonymousClass3JZ.A0E(r0.A00, new RunnableBRunnable0Shape0S0400000_I0(this, r0.A01, r9, r10, 2));
        }
    }

    public void A02(C28721Ot r9, C28731Ou r10) {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            AnonymousClass4M2 r0 = (AnonymousClass4M2) it.next();
            AnonymousClass3JZ.A0E(r0.A00, new RunnableBRunnable0Shape0S0400000_I0(this, r0.A01, r9, r10, 0));
        }
    }

    public void A03(C28721Ot r9, C28731Ou r10) {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            AnonymousClass4M2 r0 = (AnonymousClass4M2) it.next();
            AnonymousClass3JZ.A0E(r0.A00, new RunnableBRunnable0Shape0S0400000_I0(this, r0.A01, r9, r10, 1));
        }
    }

    public void A04(C28721Ot r11, C28731Ou r12, IOException iOException, boolean z) {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            AnonymousClass4M2 r0 = (AnonymousClass4M2) it.next();
            AnonymousClass3JZ.A0E(r0.A00, new RunnableBRunnable0Shape0S0510000_I0(this, r0.A01, r11, r12, iOException, 0, z));
        }
    }

    public void A05(C28731Ou r6) {
        Iterator it = this.A02.iterator();
        while (it.hasNext()) {
            AnonymousClass4M2 r0 = (AnonymousClass4M2) it.next();
            AnonymousClass3JZ.A0E(r0.A00, new RunnableBRunnable0Shape0S0300000_I0(this, r0.A01, r6, 0));
        }
    }
}
