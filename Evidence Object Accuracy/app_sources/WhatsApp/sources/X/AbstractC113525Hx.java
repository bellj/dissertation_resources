package X;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: X.5Hx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC113525Hx<E> extends AbstractCollection<E> implements AnonymousClass5Z2<E> {
    public transient Set elementSet;
    public transient Set entrySet;

    @Override // X.AnonymousClass5Z2
    public abstract int add(Object obj, int i);

    @Override // java.util.AbstractCollection, java.util.Collection
    public abstract void clear();

    public abstract int distinctElements();

    public abstract Iterator elementIterator();

    public abstract Iterator entryIterator();

    @Override // X.AnonymousClass5Z2
    public abstract int remove(Object obj, int i);

    @Override // X.AnonymousClass5Z2
    public abstract boolean setCount(Object obj, int i, int i2);

    @Override // java.util.AbstractCollection, java.util.Collection, X.AnonymousClass5Z2
    public final boolean add(Object obj) {
        add(obj, 1);
        return true;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean addAll(Collection collection) {
        return C95544dv.addAllImpl(this, collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection, X.AnonymousClass5Z2
    public boolean contains(Object obj) {
        return C12960it.A1U(count(obj));
    }

    public Set createElementSet() {
        return new C81263tj(this);
    }

    public Set createEntrySet() {
        return new C81253ti(this);
    }

    @Override // X.AnonymousClass5Z2
    public Set elementSet() {
        Set set = this.elementSet;
        if (set != null) {
            return set;
        }
        Set createElementSet = createElementSet();
        this.elementSet = createElementSet;
        return createElementSet;
    }

    @Override // X.AnonymousClass5Z2
    public Set entrySet() {
        Set set = this.entrySet;
        if (set != null) {
            return set;
        }
        Set createEntrySet = createEntrySet();
        this.entrySet = createEntrySet;
        return createEntrySet;
    }

    @Override // java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        return C95544dv.equalsImpl(this, obj);
    }

    @Override // java.util.Collection, java.lang.Object
    public final int hashCode() {
        return entrySet().hashCode();
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @Override // java.util.AbstractCollection, java.util.Collection, X.AnonymousClass5Z2
    public final boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean removeAll(Collection collection) {
        return C95544dv.removeAllImpl(this, collection);
    }

    @Override // java.util.AbstractCollection, java.util.Collection
    public final boolean retainAll(Collection collection) {
        return C95544dv.retainAllImpl(this, collection);
    }

    @Override // java.util.AbstractCollection, java.lang.Object
    public final String toString() {
        return entrySet().toString();
    }
}
