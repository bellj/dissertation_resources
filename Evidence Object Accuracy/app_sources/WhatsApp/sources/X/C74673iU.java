package X;

import com.whatsapp.R;
import com.whatsapp.settings.chat.wallpaper.WallpaperGridLayoutManager;

/* renamed from: X.3iU  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74673iU extends AbstractC05290Oz {
    public final /* synthetic */ WallpaperGridLayoutManager A00;

    public C74673iU(WallpaperGridLayoutManager wallpaperGridLayoutManager) {
        this.A00 = wallpaperGridLayoutManager;
    }

    @Override // X.AbstractC05290Oz
    public int A00(int i) {
        WallpaperGridLayoutManager wallpaperGridLayoutManager = this.A00;
        int itemViewType = wallpaperGridLayoutManager.A01.getItemViewType(i);
        if (itemViewType == 0 || itemViewType == 1 || itemViewType == 2 || itemViewType == 3) {
            return 4 / wallpaperGridLayoutManager.A00.getResources().getInteger(R.integer.wallpaper_category_columns_divisor);
        }
        if (itemViewType == 4 || itemViewType == 5) {
            return 4;
        }
        throw C12980iv.A0u(C12960it.A0W(itemViewType, "Invalid viewType: "));
    }
}
