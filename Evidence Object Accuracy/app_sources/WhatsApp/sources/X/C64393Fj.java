package X;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.redex.ViewOnClickCListenerShape1S0400000_I1;
import com.google.android.search.verification.client.SearchActionVerificationClientService;
import com.whatsapp.CircularProgressBar;
import com.whatsapp.R;
import com.whatsapp.audiopicker.AudioPickerActivity;
import com.whatsapp.components.SelectionCheckView;
import java.io.File;
import java.util.LinkedHashMap;

/* renamed from: X.3Fj  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64393Fj {
    public final int A00;
    public final View A01;
    public final View A02;
    public final FrameLayout A03;
    public final ImageButton A04;
    public final ImageView A05;
    public final TextView A06;
    public final TextView A07;
    public final TextView A08;
    public final TextView A09;
    public final CircularProgressBar A0A;
    public final SelectionCheckView A0B;
    public final /* synthetic */ AudioPickerActivity A0C;

    public C64393Fj(View view, AudioPickerActivity audioPickerActivity, int i) {
        this.A0C = audioPickerActivity;
        this.A00 = i;
        this.A02 = view;
        this.A03 = (FrameLayout) view.findViewById(R.id.audio_file_thumb_frame);
        this.A05 = C12970iu.A0L(view, R.id.audio_file_thumb);
        this.A0B = (SelectionCheckView) view.findViewById(R.id.selection_check);
        this.A09 = C12960it.A0J(view, R.id.audio_file_title);
        this.A06 = C12960it.A0J(view, R.id.audio_file_artist);
        this.A07 = C12960it.A0J(view, R.id.audio_file_duration);
        this.A08 = C12960it.A0J(view, R.id.audio_file_size);
        this.A01 = view.findViewById(R.id.bullet_duration_size);
        this.A04 = (ImageButton) view.findViewById(R.id.audio_file_play_btn);
        this.A0A = (CircularProgressBar) view.findViewById(R.id.progress_bar);
    }

    public final void A00(Context context) {
        ImageButton imageButton = this.A04;
        AudioPickerActivity audioPickerActivity = this.A0C;
        C12960it.A0r(audioPickerActivity, imageButton, R.string.pause);
        imageButton.setBackground(null);
        AnonymousClass2GF.A01(context, imageButton, ((ActivityC13830kP) audioPickerActivity).A01, R.drawable.pause);
        AnonymousClass2GE.A05(audioPickerActivity, imageButton, R.color.audio_picker_stop_button_tint);
        this.A0A.setVisibility(0);
    }

    public final void A01(Context context, boolean z) {
        CircularProgressBar circularProgressBar;
        int i;
        ImageButton imageButton = this.A04;
        AudioPickerActivity audioPickerActivity = this.A0C;
        C12960it.A0r(audioPickerActivity, imageButton, R.string.play);
        if (z) {
            imageButton.setBackground(AnonymousClass2GF.A00(context, ((ActivityC13830kP) audioPickerActivity).A01, R.drawable.audio_picker_row_start_button_background));
            AnonymousClass2GF.A01(context, imageButton, ((ActivityC13830kP) audioPickerActivity).A01, R.drawable.play_button_audio);
            AnonymousClass2GE.A05(audioPickerActivity, imageButton, R.color.audio_picker_default_button_tint);
            circularProgressBar = this.A0A;
            i = 8;
        } else {
            imageButton.setBackground(null);
            AnonymousClass2GF.A01(context, imageButton, ((ActivityC13830kP) audioPickerActivity).A01, R.drawable.toggle_play);
            AnonymousClass2GE.A05(audioPickerActivity, imageButton, R.color.audio_picker_stop_button_tint);
            circularProgressBar = this.A0A;
            i = 0;
        }
        circularProgressBar.setVisibility(i);
    }

    public final void A02(View view) {
        String A0I;
        C14900mE r8;
        String string;
        AudioPickerActivity audioPickerActivity = this.A0C;
        C52842bm r1 = audioPickerActivity.A09;
        C63353Bg A00 = r1.A00((Cursor) r1.getItem(this.A00));
        if (A00 != null) {
            LinkedHashMap linkedHashMap = audioPickerActivity.A0O;
            if (linkedHashMap.size() < 30 || linkedHashMap.containsKey(Integer.valueOf(A00.A00))) {
                long j = (long) A00.A01;
                C15450nH r0 = ((ActivityC13810kN) audioPickerActivity).A06;
                C16140oW r9 = AbstractC15460nI.A1p;
                if (j >= ((long) r0.A02(r9)) * SearchActionVerificationClientService.MS_TO_NS) {
                    r8 = ((ActivityC13810kN) audioPickerActivity).A05;
                    Object[] objArr = new Object[1];
                    C12960it.A1P(objArr, ((ActivityC13810kN) audioPickerActivity).A06.A02(r9), 0);
                    string = audioPickerActivity.getString(R.string.max_file_size_to_send_error_message, objArr);
                } else {
                    int i = A00.A00;
                    LinkedHashMap linkedHashMap2 = audioPickerActivity.A0O;
                    Integer valueOf = Integer.valueOf(i);
                    boolean containsKey = linkedHashMap2.containsKey(valueOf);
                    A04(A00, !containsKey);
                    LinkedHashMap linkedHashMap3 = audioPickerActivity.A0O;
                    if (containsKey) {
                        linkedHashMap3.remove(valueOf);
                        view.setSelected(false);
                        view.setBackgroundResource(0);
                        ((SelectionCheckView) view.findViewById(R.id.selection_check)).A04(false, true);
                    } else {
                        linkedHashMap3.put(valueOf, A00);
                        view.setSelected(true);
                        view.setBackgroundResource(R.color.audio_picker_row_selection);
                        ((SelectionCheckView) view.findViewById(R.id.selection_check)).A04(true, true);
                    }
                    int size = audioPickerActivity.A0O.size();
                    ImageButton imageButton = audioPickerActivity.A03;
                    if (size == 0) {
                        C63223At.A00(imageButton, false, true);
                        A0I = audioPickerActivity.getString(R.string.tap_to_select);
                    } else {
                        C63223At.A00(imageButton, true, true);
                        Object[] objArr2 = new Object[1];
                        C12960it.A1P(objArr2, size, 0);
                        A0I = ((ActivityC13830kP) audioPickerActivity).A01.A0I(objArr2, R.plurals.n_selected, (long) size);
                    }
                    AbstractC005102i A1U = audioPickerActivity.A1U();
                    AnonymousClass009.A06(A1U, "supportActionBar is null");
                    A1U.A0H(A0I);
                    return;
                }
            } else {
                r8 = ((ActivityC13810kN) audioPickerActivity).A05;
                AnonymousClass018 r4 = ((ActivityC13830kP) audioPickerActivity).A01;
                Object[] objArr3 = new Object[1];
                C12960it.A1P(objArr3, 30, 0);
                string = r4.A0I(objArr3, R.plurals.max_files_to_send_error_message, 30);
            }
            r8.A0E(string, 0);
        }
    }

    public void A03(ActivityC13810kN r21, C63353Bg r22) {
        long A02;
        TextView textView;
        float f;
        View view = this.A02;
        C12960it.A10(view, this, 14);
        view.setOnLongClickListener(new View.OnLongClickListener() { // from class: X.4n0
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                C64393Fj.this.A02(view2);
                return true;
            }
        });
        String str = r22.A03;
        File file = null;
        if (str != null) {
            file = new File(str);
        }
        int i = r22.A00;
        AnonymousClass3X1 r2 = new AnonymousClass3X1(this, (long) i);
        AnonymousClass3X8 r1 = new AnonymousClass3X8(this);
        AudioPickerActivity audioPickerActivity = this.A0C;
        audioPickerActivity.A0I.A02(r2, r1);
        if (Build.VERSION.SDK_INT >= 21) {
            this.A05.setClipToOutline(true);
        }
        C12990iw.A1E(this.A05);
        TextView textView2 = this.A09;
        textView2.setText(AnonymousClass3J9.A02(r21, ((ActivityC13830kP) audioPickerActivity).A01, r22.A07, audioPickerActivity.A0N));
        String str2 = r22.A02;
        TextView textView3 = this.A06;
        if (str2 != null) {
            textView3.setVisibility(0);
            textView3.setText(AnonymousClass3J9.A02(r21, ((ActivityC13830kP) audioPickerActivity).A01, str2, audioPickerActivity.A0N));
        } else {
            textView3.setVisibility(8);
        }
        String str3 = r22.A05;
        boolean isEmpty = str3.isEmpty();
        TextView textView4 = this.A07;
        if (!isEmpty) {
            textView4.setVisibility(0);
            textView4.setText(str3);
        } else {
            textView4.setVisibility(8);
        }
        String str4 = r22.A06;
        boolean isEmpty2 = str4.isEmpty();
        if (!isEmpty2) {
            if (((long) r22.A01) >= ((long) ((ActivityC13810kN) audioPickerActivity).A06.A02(AbstractC15460nI.A1p)) * SearchActionVerificationClientService.MS_TO_NS) {
                SpannableString spannableString = new SpannableString(str4);
                spannableString.setSpan(new ForegroundColorSpan(-65536), 0, str4.length(), 33);
                textView = this.A08;
                textView.setText(spannableString, TextView.BufferType.SPANNABLE);
                f = 0.5f;
            } else {
                textView = this.A08;
                textView.setText(str4);
                f = 1.0f;
            }
            textView2.setAlpha(f);
            textView.setVisibility(0);
        } else {
            this.A08.setVisibility(8);
        }
        if (isEmpty || isEmpty2) {
            this.A01.setVisibility(8);
        } else {
            this.A01.setVisibility(0);
        }
        boolean containsKey = audioPickerActivity.A0O.containsKey(Integer.valueOf(i));
        if (containsKey) {
            view.setSelected(true);
            view.setBackgroundResource(R.color.audio_picker_row_selection);
            SelectionCheckView selectionCheckView = this.A0B;
            selectionCheckView.setVisibility(0);
            selectionCheckView.A04(true, false);
        } else {
            view.setBackgroundResource(0);
            SelectionCheckView selectionCheckView2 = this.A0B;
            selectionCheckView2.A04(false, false);
            selectionCheckView2.setVisibility(4);
        }
        A04(r22, containsKey);
        CircularProgressBar circularProgressBar = this.A0A;
        circularProgressBar.A0B = AnonymousClass00T.A00(r21, R.color.audio_picker_stop_button_outline);
        circularProgressBar.A0C = AnonymousClass00T.A00(r21, R.color.audio_picker_stop_button_progress);
        circularProgressBar.A05 = 0.1f;
        circularProgressBar.A06 = 10.0f;
        circularProgressBar.setIndeterminate(false);
        C30421Xi r23 = new C30421Xi(new AnonymousClass1IS(null, Integer.toString(i), true), 0);
        ((AbstractC15340mz) r23).A08 = 2;
        C16150oX r0 = new C16150oX();
        r0.A0F = file;
        ((AbstractC16130oV) r23).A02 = r0;
        if (!audioPickerActivity.A0G.A0D(r23)) {
            A01(r21, true);
            circularProgressBar.setMax(((AbstractC16130oV) r23).A00 * 1000);
            circularProgressBar.setProgress(0);
            A02 = 0;
        } else {
            C35191hP A00 = audioPickerActivity.A0G.A00();
            if (A00 != null) {
                circularProgressBar.setMax(A00.A03);
                if (A00.A0I()) {
                    A00(r21);
                } else if (A00.A02() > 0) {
                    A01(r21, false);
                } else {
                    A01(r21, true);
                    circularProgressBar.setProgress(0);
                    circularProgressBar.setMax(A00.A03);
                    A00.A0K = new AnonymousClass3WP(r21, this, A00, r23);
                    A02 = (long) A00.A02();
                }
                circularProgressBar.setProgress(A00.A02());
                circularProgressBar.setMax(A00.A03);
                A00.A0K = new AnonymousClass3WP(r21, this, A00, r23);
                A02 = (long) A00.A02();
            }
            this.A04.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(this, r23, r22, r21, 0));
        }
        circularProgressBar.setContentDescription(C12960it.A0X(audioPickerActivity, C38131nZ.A06(((ActivityC13830kP) audioPickerActivity).A01, A02), C12970iu.A1b(), 0, R.string.voice_message_time_elapsed));
        this.A04.setOnClickListener(new ViewOnClickCListenerShape1S0400000_I1(this, r23, r22, r21, 0));
    }

    public final void A04(C63353Bg r11, boolean z) {
        int i;
        Object[] objArr;
        String str = r11.A02;
        View view = this.A02;
        AudioPickerActivity audioPickerActivity = this.A0C;
        if (str != null) {
            i = R.string.audio_picker_row_content_description;
            if (z) {
                i = R.string.audio_picker_selected_row_content_description;
            }
            objArr = new Object[]{r11.A07, str, r11.A04, r11.A06};
        } else {
            i = R.string.audio_picker_row_content_description_no_artist;
            if (z) {
                i = R.string.audio_picker_selected_row_content_description_no_artist;
            }
            objArr = new Object[]{r11.A07, r11.A04, r11.A06};
        }
        view.setContentDescription(audioPickerActivity.getString(i, objArr));
    }
}
