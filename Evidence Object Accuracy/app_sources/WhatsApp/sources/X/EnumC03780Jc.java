package X;

/* renamed from: X.0Jc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public enum EnumC03780Jc {
    none,
    xMinYMin,
    xMidYMin,
    xMaxYMin,
    xMinYMid,
    xMidYMid,
    xMaxYMid,
    xMinYMax,
    xMidYMax,
    xMaxYMax
}
