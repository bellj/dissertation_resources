package X;

/* renamed from: X.43X  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43X extends AbstractC16110oT {
    public Integer A00;
    public Long A01;
    public Long A02;

    public AnonymousClass43X() {
        super(1946, AbstractC16110oT.DEFAULT_SAMPLING_RATE, 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(3, this.A01);
        r3.Abe(2, this.A02);
        r3.Abe(1, this.A00);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamTemplateTruncation {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "buttonIndex", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "originalLength", this.A02);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "templateComponent", C12960it.A0Y(this.A00));
        return C12960it.A0d("}", A0k);
    }
}
