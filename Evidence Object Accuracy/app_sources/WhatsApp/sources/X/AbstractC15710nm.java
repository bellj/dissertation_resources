package X;

import android.content.ContentResolver;
import android.provider.Settings;
import com.whatsapp.Me;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: X.0nm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC15710nm implements AnonymousClass01O {
    public String A00() {
        StringBuilder sb;
        AnonymousClass16A r3 = (AnonymousClass16A) this;
        C15570nT r2 = r3.A03;
        r2.A08();
        Me me = r2.A00;
        if (me != null) {
            sb = new StringBuilder();
            sb.append(me.jabber_id);
            sb.append(":");
            sb.append(0);
        } else {
            String replaceAll = r2.A05().toLowerCase(Locale.US).replaceAll("\\W", "-");
            if (!replaceAll.equals("")) {
                return replaceAll;
            }
            ContentResolver A0C = r3.A06.A0C();
            if (A0C == null) {
                Log.w("crashlogs/get-from-parameter cr=null");
                return replaceAll;
            }
            String string = Settings.Secure.getString(A0C, "android_id");
            if (string == null || string.length() < 6) {
                string = "123456";
            }
            sb = new StringBuilder("new-");
            sb.append(string.substring(string.length() - 6));
        }
        return sb.toString();
    }

    public String A01(String str, String str2, boolean z) {
        AnonymousClass16A r2 = (AnonymousClass16A) this;
        AnonymousClass1Nk r1 = new AnonymousClass1Nk();
        C28471Ni r4 = new C28471Ni(r2.A04, new AnonymousClass1Nl(r1, r2), r2.A0B, "https://crashlogs.whatsapp.net/wa_fls_upload_check", r2.A0D.A00(), 6, false, false, false);
        r4.A06("access_token", "1063127757113399|745146ffa34413f9dbb5469f5370b7af");
        r4.A06("from_jid", str);
        r4.A06("type", str2);
        r4.A06("support_exception_only_upload", String.valueOf(z));
        int A02 = r4.A02(null);
        if (A02 == 200) {
            return r1.A00;
        }
        if (A02 == 403) {
            r1.A00 = "no_upload";
            return "no_upload";
        } else if (A02 != 500) {
            StringBuilder sb = new StringBuilder("Unknown response code ");
            sb.append(A02);
            sb.append(" from crash upload server");
            throw new IOException(sb.toString());
        } else {
            throw new IOException("Response 500 received from server");
        }
    }

    public void A02(String str, String str2, Throwable th) {
        Log.e(str, th);
        ((AnonymousClass16A) this).A08(new C28411Nc(str, th), str, str2, new HashMap(), true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x018a A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void A03(java.util.HashSet r22, java.util.Map r23, boolean r24, boolean r25, boolean r26, boolean r27) {
        /*
        // Method dump skipped, instructions count: 400
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AbstractC15710nm.A03(java.util.HashSet, java.util.Map, boolean, boolean, boolean, boolean):void");
    }

    @Override // X.AnonymousClass01O
    public void AaV(String str, String str2, boolean z) {
        ((AnonymousClass16A) this).A08(new C28411Nc(str), str, str2, new HashMap(), z);
    }
}
