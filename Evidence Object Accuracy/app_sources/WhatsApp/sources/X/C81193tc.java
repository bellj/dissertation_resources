package X;

/* renamed from: X.3tc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C81193tc extends AnonymousClass4Y5 {
    public final Object key;
    public int lastKnownIndex;
    public final /* synthetic */ C95614e4 this$0;

    public C81193tc(C95614e4 r2, int i) {
        this.this$0 = r2;
        this.key = r2.keys[i];
        this.lastKnownIndex = i;
    }

    @Override // X.AnonymousClass4Y5
    public int getCount() {
        updateLastKnownIndex();
        int i = this.lastKnownIndex;
        if (i == -1) {
            return 0;
        }
        return this.this$0.values[i];
    }

    @Override // X.AnonymousClass4Y5
    public Object getElement() {
        return this.key;
    }

    public void updateLastKnownIndex() {
        int i = this.lastKnownIndex;
        if (i != -1) {
            C95614e4 r2 = this.this$0;
            if (i < r2.size() && AnonymousClass28V.A00(this.key, r2.keys[i])) {
                return;
            }
        }
        this.lastKnownIndex = this.this$0.indexOf(this.key);
    }
}
