package X;

/* renamed from: X.21P  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass21P {
    public final int A00;
    public final long A01;
    public final AbstractC28771Oy A02;
    public final AbstractC16130oV A03;
    public final boolean A04;

    public AnonymousClass21P(AbstractC28771Oy r1, AbstractC16130oV r2, int i, long j, boolean z) {
        this.A03 = r2;
        this.A00 = i;
        this.A01 = j;
        this.A02 = r1;
        this.A04 = z;
    }
}
