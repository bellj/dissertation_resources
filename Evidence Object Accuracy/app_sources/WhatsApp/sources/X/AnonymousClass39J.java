package X;

import android.view.View;
import com.whatsapp.audioRecording.AudioRecordFactory;
import com.whatsapp.audioRecording.OpusRecorderFactory;
import com.whatsapp.status.playback.MessageReplyActivity;

/* renamed from: X.39J  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass39J extends AbstractC14670lq {
    public final /* synthetic */ MessageReplyActivity A00;
    public final /* synthetic */ AnonymousClass2IC A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AnonymousClass39J(View view, ActivityC000900k r40, AbstractC15710nm r41, AbstractC13860kS r42, C14330lG r43, C14900mE r44, C15450nH r45, C16170oZ r46, AudioRecordFactory audioRecordFactory, OpusRecorderFactory opusRecorderFactory, C18280sC r49, AnonymousClass11P r50, AnonymousClass2IA r51, AnonymousClass01d r52, C14830m7 r53, C14820m6 r54, AnonymousClass018 r55, C21310xD r56, C14850m9 r57, C22050yP r58, C14410lO r59, AnonymousClass109 r60, C16630pM r61, C20320vZ r62, AnonymousClass1AL r63, MessageReplyActivity messageReplyActivity, AnonymousClass199 r65, C255819y r66, AbstractC14440lR r67, C17020q8 r68, C254419k r69, C14680lr r70, AnonymousClass19A r71, C63683Cn r72, AnonymousClass2IC r73, C21260x8 r74, boolean z) {
        super(view, r40, r41, r42, r43, r44, r45, r46, audioRecordFactory, opusRecorderFactory, r49, r50, r51, r52, r53, r54, r55, r56, r57, r58, r59, r60, r61, r62, r63, r65, r66, r67, r68, r69, r70, r71, r72, r74, true, z);
        this.A01 = r73;
        this.A00 = messageReplyActivity;
    }
}
