package X;

/* renamed from: X.20F  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass20F {
    public static AnonymousClass20G A00(byte[] bArr, byte[] bArr2) {
        AnonymousClass20G r5 = new AnonymousClass20G();
        C113075Fx r2 = new C113075Fx(new AnonymousClass20K(bArr, bArr.length), bArr2);
        byte[] bArr3 = r2.A01;
        if (bArr3 == null || bArr3.length != 24) {
            StringBuilder sb = new StringBuilder("XSalsa20");
            sb.append(" requires exactly ");
            sb.append(24);
            sb.append(" bytes of IV");
            throw new IllegalArgumentException(sb.toString());
        }
        AnonymousClass20L r1 = r2.A00;
        if (r1 == null) {
            if (!r5.A05) {
                StringBuilder sb2 = new StringBuilder("XSalsa20");
                sb2.append(" KeyParameter can not be null for first initialisation");
                throw new IllegalStateException(sb2.toString());
            }
        } else if (r1 instanceof AnonymousClass20K) {
            byte[] bArr4 = ((AnonymousClass20K) r1).A00;
            if (bArr4 != null) {
                if (bArr4.length == 32) {
                    int i = 0;
                    int i2 = 16;
                    int[] iArr = r5.A07;
                    int[] iArr2 = AnonymousClass20G.A0B;
                    iArr[0] = iArr2[4];
                    iArr[5] = iArr2[5];
                    iArr[10] = iArr2[6];
                    iArr[15] = iArr2[7];
                    int i3 = 0;
                    do {
                        iArr[1 + i3] = AbstractC95434di.A00(bArr4, i);
                        i += 4;
                        i3++;
                    } while (i3 < 4);
                    int i4 = 0;
                    do {
                        iArr[11 + i4] = AbstractC95434di.A00(bArr4, i2);
                        i2 += 4;
                        i4++;
                    } while (i4 < 4);
                    int i5 = 0;
                    int i6 = 0;
                    do {
                        iArr[6 + i6] = AbstractC95434di.A00(bArr3, i5);
                        i5 += 4;
                        i6++;
                    } while (i6 < 2);
                    int i7 = 8;
                    int i8 = 0;
                    do {
                        iArr[8 + i8] = AbstractC95434di.A00(bArr3, i7);
                        i7 += 4;
                        i8++;
                    } while (i8 < 2);
                    int[] iArr3 = new int[iArr.length];
                    AnonymousClass20G.A00(iArr, iArr3, 20);
                    iArr[1] = iArr3[0] - iArr[0];
                    iArr[2] = iArr3[5] - iArr[5];
                    iArr[3] = iArr3[10] - iArr[10];
                    iArr[4] = iArr3[15] - iArr[15];
                    iArr[11] = iArr3[6] - iArr[6];
                    iArr[12] = iArr3[7] - iArr[7];
                    iArr[13] = iArr3[8] - iArr[8];
                    iArr[14] = iArr3[9] - iArr[9];
                    int i9 = 16;
                    int i10 = 0;
                    do {
                        iArr[6 + i10] = AbstractC95434di.A00(bArr3, i9);
                        i9 += 4;
                        i10++;
                    } while (i10 < 2);
                    r5.A03 = 0;
                    r5.A00 = 0;
                    r5.A01 = 0;
                    r5.A02 = 0;
                    int i11 = 0;
                    iArr[9] = 0;
                    iArr[8] = 0;
                    byte[] bArr5 = r5.A06;
                    int i12 = r5.A04;
                    int[] iArr4 = r5.A08;
                    AnonymousClass20G.A00(iArr, iArr4, i12);
                    for (int i13 : iArr4) {
                        AbstractC95434di.A02(bArr5, i13, i11);
                        i11 += 4;
                    }
                    r5.A05 = true;
                    return r5;
                }
                StringBuilder sb3 = new StringBuilder("XSalsa20");
                sb3.append(" requires a 256 bit key");
                throw new IllegalArgumentException(sb3.toString());
            }
        } else {
            StringBuilder sb4 = new StringBuilder("XSalsa20");
            sb4.append(" Init parameters must contain a KeyParameter (or null for re-init)");
            throw new IllegalArgumentException(sb4.toString());
        }
        StringBuilder sb5 = new StringBuilder("XSalsa20");
        sb5.append(" doesn't support re-init with null key");
        throw new IllegalArgumentException(sb5.toString());
    }
}
