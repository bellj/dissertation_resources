package X;

import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import com.whatsapp.R;
import com.whatsapp.documentpicker.DocumentPickerActivity;
import com.whatsapp.util.Log;
import java.io.IOException;
import java.util.List;

/* renamed from: X.3Om  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C66673Om implements AnonymousClass02Q {
    public MenuItem A00;
    public final /* synthetic */ DocumentPickerActivity A01;

    public C66673Om(DocumentPickerActivity documentPickerActivity) {
        this.A01 = documentPickerActivity;
    }

    @Override // X.AnonymousClass02Q
    public boolean ALr(MenuItem menuItem, AbstractC009504t r5) {
        if (menuItem.getItemId() != R.id.menuitem_share) {
            return false;
        }
        DocumentPickerActivity documentPickerActivity = this.A01;
        List list = documentPickerActivity.A0N;
        if (list.isEmpty()) {
            return false;
        }
        try {
            documentPickerActivity.A2j(list);
            return false;
        } catch (IOException e) {
            Log.e(e);
            return false;
        }
    }

    @Override // X.AnonymousClass02Q
    public boolean AOg(Menu menu, AbstractC009504t r5) {
        MenuItem add = menu.add(0, R.id.menuitem_share, 0, R.string.send);
        this.A00 = add;
        add.setShowAsAction(2);
        return true;
    }

    @Override // X.AnonymousClass02Q
    public void AP3(AbstractC009504t r3) {
        DocumentPickerActivity documentPickerActivity = this.A01;
        documentPickerActivity.A0N.clear();
        documentPickerActivity.A03 = null;
        documentPickerActivity.A0B.notifyDataSetChanged();
    }

    @Override // X.AnonymousClass02Q
    public boolean AU7(Menu menu, AbstractC009504t r10) {
        DocumentPickerActivity documentPickerActivity = this.A01;
        List list = documentPickerActivity.A0N;
        if (list.isEmpty()) {
            r10.A08(R.string.select_multiple_title);
        } else {
            Resources resources = documentPickerActivity.getResources();
            int size = list.size();
            Object[] objArr = new Object[1];
            C12960it.A1P(objArr, list.size(), 0);
            r10.A0B(resources.getQuantityString(R.plurals.n_selected, size, objArr));
        }
        this.A00.setVisible(!list.isEmpty());
        return true;
    }
}
