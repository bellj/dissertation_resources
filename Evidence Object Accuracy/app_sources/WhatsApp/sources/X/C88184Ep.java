package X;

import java.io.Closeable;

/* renamed from: X.4Ep  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C88184Ep {
    public static final void A00(Closeable closeable, Throwable th) {
        try {
            closeable.close();
        } catch (Throwable th2) {
            C88164En.A00(th, th2);
        }
    }
}
