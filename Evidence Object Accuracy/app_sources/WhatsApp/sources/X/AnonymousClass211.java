package X;

import com.whatsapp.util.Log;
import java.util.Comparator;
import java.util.HashMap;

/* renamed from: X.211  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass211 implements Comparator {
    public HashMap A00;
    public final AnonymousClass210[] A01;

    public AnonymousClass211(AnonymousClass210[] r1) {
        this.A01 = r1;
    }

    public final int A00(C37471mS r3) {
        if (A01().containsKey(r3)) {
            return ((Number) A01().get(r3)).intValue();
        }
        StringBuilder sb = new StringBuilder("EmojiPickerComparator/found an emoji that doesn't exist in emoji picker ");
        sb.append(r3.toString());
        Log.e(sb.toString());
        return 0;
    }

    public final synchronized HashMap A01() {
        HashMap hashMap;
        hashMap = this.A00;
        if (hashMap == null) {
            hashMap = new HashMap();
            int i = 0;
            for (AnonymousClass210 r0 : this.A01) {
                for (int[] iArr : ((EnumC452920z) r0).emojiData) {
                    hashMap.put(new C37471mS(iArr), Integer.valueOf(i));
                    i++;
                }
            }
            this.A00 = hashMap;
        }
        return hashMap;
    }

    @Override // java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return A00((C37471mS) obj) - A00((C37471mS) obj2);
    }
}
