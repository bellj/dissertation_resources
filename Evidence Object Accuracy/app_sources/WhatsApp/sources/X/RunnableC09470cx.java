package X;

import androidx.biometric.BiometricFragment;

/* renamed from: X.0cx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09470cx implements Runnable {
    public final /* synthetic */ BiometricFragment A00;
    public final /* synthetic */ C04700Ms A01;

    public RunnableC09470cx(BiometricFragment biometricFragment, C04700Ms r2) {
        this.A00 = biometricFragment;
        this.A01 = r2;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0EP r0 = this.A00.A01;
        AnonymousClass0PW r1 = r0.A04;
        if (r1 == null) {
            r1 = new C02450Ci(r0);
            r0.A04 = r1;
        }
        r1.A02(this.A01);
    }
}
