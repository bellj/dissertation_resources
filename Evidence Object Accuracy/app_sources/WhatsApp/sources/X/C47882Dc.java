package X;

import java.util.Collections;
import java.util.Set;

/* renamed from: X.2Dc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47882Dc {
    public final Long A00;
    public final String A01;
    public final Set A02;

    public C47882Dc(Long l, String str, Set set) {
        this.A01 = str;
        this.A02 = Collections.unmodifiableSet(set);
        this.A00 = l;
    }
}
