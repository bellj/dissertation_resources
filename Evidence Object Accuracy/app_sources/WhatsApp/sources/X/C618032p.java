package X;

import android.content.Context;
import android.location.Location;
import com.whatsapp.location.GroupChatLiveLocationsActivity;

/* renamed from: X.32p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C618032p extends AnonymousClass294 {
    public final /* synthetic */ GroupChatLiveLocationsActivity A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C618032p(Context context, AnonymousClass0O0 r2, GroupChatLiveLocationsActivity groupChatLiveLocationsActivity) {
        super(context, r2);
        this.A00 = groupChatLiveLocationsActivity;
    }

    @Override // X.AnonymousClass294
    public Location getMyLocation() {
        Location location;
        AbstractView$OnCreateContextMenuListenerC35851ir r0 = this.A00.A0L;
        if (r0 == null || (location = r0.A0J) == null) {
            return super.getMyLocation();
        }
        return location;
    }
}
