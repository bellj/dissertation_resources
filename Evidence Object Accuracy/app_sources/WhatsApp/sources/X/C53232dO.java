package X;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.reactions.ReactionEmojiTextView;
import com.whatsapp.reactions.ReactionsTrayViewModel;

/* renamed from: X.2dO */
/* loaded from: classes2.dex */
public class C53232dO extends LinearLayout implements AnonymousClass004 {
    public static final Interpolator A0B = AnonymousClass0L1.A00(0.65f, 0.0f, 0.35f, 1.0f);
    public static final Interpolator A0C = AnonymousClass0L1.A00(0.87f, 0.0f, 0.13f, 1.0f);
    public static final Interpolator A0D = AnonymousClass0L1.A00(0.45f, 0.0f, 0.55f, 1.0f);
    public AnimatorSet A00;
    public C15570nT A01;
    public AnonymousClass018 A02;
    public C14850m9 A03;
    public C16630pM A04;
    public C26661Ei A05;
    public AnonymousClass2P7 A06;
    public boolean A07;
    public final AnimatorSet A08;
    public final ReactionsTrayViewModel A09;
    public final int[] A0A;

    public C53232dO(Context context, ReactionsTrayViewModel reactionsTrayViewModel) {
        super(context);
        if (!this.A07) {
            this.A07 = true;
            AnonymousClass01J A00 = AnonymousClass2P6.A00(generatedComponent());
            this.A03 = C12960it.A0S(A00);
            this.A01 = C12970iu.A0S(A00);
            this.A02 = C12960it.A0R(A00);
            this.A05 = (C26661Ei) A00.AHE.get();
            this.A04 = C12990iw.A0e(A00);
        }
        int i = ReactionsTrayViewModel.A0B;
        this.A0A = new int[i + 1];
        this.A08 = new AnimatorSet();
        this.A09 = reactionsTrayViewModel;
        setId(R.id.reactions_tray_layout);
        setClipChildren(false);
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        setOrientation(0);
        float dimension = C12960it.A09(this).getDimension(R.dimen.reaction_tray_bg_radius);
        int dimension2 = (int) C12960it.A09(this).getDimension(R.dimen.reaction_tray_elevation);
        int A002 = AnonymousClass00T.A00(getContext(), R.color.black_alpha_07);
        int A003 = AnonymousClass00T.A00(getContext(), R.color.reactions_bubble_background);
        float[] fArr = {dimension, dimension, dimension, dimension, dimension, dimension, dimension, dimension};
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.setPadding(new Rect(dimension2, dimension2, dimension2, dimension2));
        shapeDrawable.getPaint().setColor(A003);
        shapeDrawable.getPaint().setShadowLayer((float) dimension2, 0.0f, (float) (dimension2 >> 2), A002);
        if (Build.VERSION.SDK_INT < 28) {
            setLayerType(1, shapeDrawable.getPaint());
        }
        shapeDrawable.setShape(new RoundRectShape(fArr, null, null));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{shapeDrawable});
        layerDrawable.setLayerInset(0, dimension2, dimension2, dimension2, dimension2);
        setBackground(layerDrawable);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.reaction_tray_padding) + C12990iw.A07(this, R.dimen.reaction_tray_elevation);
        setPadding(dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        ReactionsTrayViewModel reactionsTrayViewModel2 = this.A09;
        String str = ((AnonymousClass4X9) reactionsTrayViewModel2.A0A.A01()).A00;
        for (int i2 = 0; i2 < reactionsTrayViewModel2.A03.size(); i2++) {
            String A0g = C12960it.A0g(reactionsTrayViewModel2.A03, i2);
            boolean A1U = C12960it.A1U(i2);
            TextEmojiLabel textEmojiLabel = (TextEmojiLabel) LinearLayout.inflate(getContext(), R.layout.reaction_tray_emoji, null);
            textEmojiLabel.A0G(null, A0g);
            if (A0g.equals(str)) {
                textEmojiLabel.setSelected(true);
            }
            AnonymousClass23N.A02(textEmojiLabel, textEmojiLabel.isSelected() ? R.string.accessibility_action_tap_reaction_tray_revoke : R.string.accessibility_action_tap_reaction_tray_react);
            addView(textEmojiLabel);
            if (A1U) {
                ViewGroup.LayoutParams layoutParams = textEmojiLabel.getLayoutParams();
                C42941w9.A07(textEmojiLabel, this.A02, getResources().getDimensionPixelSize(R.dimen.reaction_tray_emoji_kerning), 0);
                textEmojiLabel.setLayoutParams(layoutParams);
            }
            AbstractView$OnClickListenerC34281fs.A01(textEmojiLabel, this, 18);
        }
        if (reactionsTrayViewModel2.A06.A07(1524) && reactionsTrayViewModel2.A03.size() == i) {
            LinearLayout.inflate(getContext(), R.layout.reaction_plus_view, this);
            View findViewById = findViewById(R.id.reactions_plus_button);
            ViewGroup.LayoutParams layoutParams2 = findViewById.getLayoutParams();
            C42941w9.A07(findViewById, this.A02, getResources().getDimensionPixelSize(R.dimen.reaction_tray_emoji_kerning), 0);
            findViewById.setLayoutParams(layoutParams2);
            C12960it.A0y(findViewById, this, 39);
        }
    }

    public static /* synthetic */ Animator A00(C53232dO r6, ReactionEmojiTextView reactionEmojiTextView) {
        ObjectAnimator duration = ObjectAnimator.ofFloat(reactionEmojiTextView, "backgroundScale", 1.0f, 0.0f).setDuration(100L);
        duration.addListener(new C72653ex(r6, reactionEmojiTextView, 0.0f));
        duration.setInterpolator(AnonymousClass3J7.A00);
        duration.addListener(new C72573ep(r6, reactionEmojiTextView));
        return duration;
    }

    @Override // X.AnonymousClass005
    public final Object generatedComponent() {
        AnonymousClass2P7 r0 = this.A06;
        if (r0 == null) {
            r0 = AnonymousClass2P7.A00(this);
            this.A06 = r0;
        }
        return r0.generatedComponent();
    }

    public void setChildrenVisibility(int i) {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            getChildAt(i2).setVisibility(i);
        }
    }
}
