package X;

import android.view.KeyEvent;
import android.widget.TextView;
import androidx.appcompat.widget.SearchView;

/* renamed from: X.0X9  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0X9 implements TextView.OnEditorActionListener {
    public final /* synthetic */ SearchView A00;

    public AnonymousClass0X9(SearchView searchView) {
        this.A00 = searchView;
    }

    @Override // android.widget.TextView.OnEditorActionListener
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        this.A00.A08();
        return true;
    }
}
