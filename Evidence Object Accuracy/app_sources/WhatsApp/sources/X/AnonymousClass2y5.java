package X;

import android.content.Context;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.2y5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2y5 extends C42441vD {
    public boolean A00;
    public final ImageView A01;
    public final ImageView A02 = C12970iu.A0L(this, R.id.photo_new);
    public final ImageView A03 = C12970iu.A0L(this, R.id.photo_old);

    @Override // X.C42441vD
    public int getBackgroundResource() {
        return 0;
    }

    public AnonymousClass2y5(Context context, AbstractC13890kV r5, AnonymousClass1XB r6) {
        super(context, r5, r6);
        A0Z();
        ImageView A0L = C12970iu.A0L(this, R.id.arrow);
        this.A01 = A0L;
        AnonymousClass2GF.A01(context, A0L, ((AbstractC28551Oa) this).A0K, R.drawable.ic_chat_icon_change_arrow);
        A0X();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void A0X() {
        /*
            r7 = this;
            X.0mz r2 = r7.A0O
            X.1XB r2 = (X.AnonymousClass1XB) r2
            X.1IS r3 = r2.A0z
            boolean r0 = r3.A02
            r4 = 0
            if (r0 == 0) goto L_0x00a2
            int r1 = r2.A00
            r0 = 6
            if (r1 != r0) goto L_0x00a2
            X.1Xq r2 = (X.C30501Xq) r2
            com.whatsapp.data.ProfilePhotoChange r2 = r2.A00
            if (r2 == 0) goto L_0x00a2
            byte[] r1 = r2.newPhoto
            if (r1 == 0) goto L_0x00a2
            byte[] r0 = r2.oldPhoto
            if (r0 == 0) goto L_0x00a2
            int r0 = r1.length
            android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeByteArray(r1, r4, r0)
            byte[] r1 = r2.oldPhoto
            int r0 = r1.length
            android.graphics.Bitmap r6 = android.graphics.BitmapFactory.decodeByteArray(r1, r4, r0)
            if (r5 == 0) goto L_0x00a2
            if (r6 == 0) goto L_0x00a2
            X.0wC r1 = r7.A11
            X.0lm r0 = r3.A00
            X.0nU r0 = X.C15580nU.A02(r0)
            boolean r0 = r1.A0b(r0)
            if (r0 == 0) goto L_0x006b
            int r0 = r6.getWidth()
            r1 = -822083584(0xffffffffcf000000, float:-2.14748365E9)
            android.graphics.Bitmap r2 = X.C21270x9.A00(r6, r1, r0)
            int r0 = r5.getWidth()
            android.graphics.Bitmap r1 = X.C21270x9.A00(r5, r1, r0)
            android.widget.ImageView r0 = r7.A03
            r0.setImageBitmap(r2)
            android.widget.ImageView r0 = r7.A02
            r0.setImageBitmap(r1)
        L_0x0058:
            r1 = 1
        L_0x0059:
            android.widget.ImageView r0 = r7.A03
            if (r1 == 0) goto L_0x00a4
            r0.setVisibility(r4)
            android.widget.ImageView r0 = r7.A02
            r0.setVisibility(r4)
            android.widget.ImageView r0 = r7.A01
            r0.setVisibility(r4)
            return
        L_0x006b:
            android.content.res.Resources r0 = X.C12960it.A09(r7)
            int r3 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r3 < r1) goto L_0x009c
            X.0DM r2 = new X.0DM
            r2.<init>(r0, r6)
        L_0x007a:
            android.content.res.Resources r0 = X.C12960it.A09(r7)
            if (r3 < r1) goto L_0x0096
            X.0DM r1 = new X.0DM
            r1.<init>(r0, r5)
        L_0x0085:
            r2.A00()
            r1.A00()
            android.widget.ImageView r0 = r7.A03
            r0.setImageDrawable(r2)
            android.widget.ImageView r0 = r7.A02
            r0.setImageDrawable(r1)
            goto L_0x0058
        L_0x0096:
            X.0DL r1 = new X.0DL
            r1.<init>(r0, r5)
            goto L_0x0085
        L_0x009c:
            X.0DL r2 = new X.0DL
            r2.<init>(r0, r6)
            goto L_0x007a
        L_0x00a2:
            r1 = 0
            goto L_0x0059
        L_0x00a4:
            r1 = 8
            r0.setVisibility(r1)
            android.widget.ImageView r0 = r7.A02
            r0.setVisibility(r1)
            android.widget.ImageView r0 = r7.A01
            r0.setVisibility(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass2y5.A0X():void");
    }

    @Override // X.AbstractC42451vE, X.AnonymousClass1OZ, X.AbstractC28561Ob
    public void A0Z() {
        if (!this.A00) {
            this.A00 = true;
            AnonymousClass2P6 A07 = AnonymousClass1OY.A07(this);
            AnonymousClass01J A08 = AnonymousClass1OY.A08(A07, this);
            AnonymousClass1OY.A0L(A08, this);
            AnonymousClass1OY.A0M(A08, this);
            AnonymousClass1OY.A0K(A08, this);
            AnonymousClass1OY.A0I(A07, A08, this, AnonymousClass1OY.A09(A08, this, AnonymousClass1OY.A0B(A08, this)));
            ((C42441vD) this).A04 = C12970iu.A0b(A08);
            ((C42441vD) this).A02 = (C18750sx) A08.A2p.get();
            ((C42441vD) this).A05 = (AnonymousClass11G) A08.AKq.get();
            ((C42441vD) this).A03 = (C236812p) A08.AAC.get();
            ((C42441vD) this).A06 = (AnonymousClass11H) A08.AEp.get();
            ((C42441vD) this).A09 = (C17140qK) A08.AMZ.get();
            ((C42441vD) this).A00 = (AnonymousClass11L) A08.ALG.get();
            ((C42441vD) this).A01 = (AnonymousClass11J) A08.A3m.get();
            ((C42441vD) this).A07 = (AnonymousClass1AT) A08.AG2.get();
            ((C42441vD) this).A0A = C18000rk.A00(A07.A03.A0G);
        }
    }

    @Override // X.C42441vD, X.AnonymousClass1OY
    public void A0s() {
        A0X();
        super.A0s();
    }

    @Override // X.C42441vD, X.AnonymousClass1OY
    public void A1D(AbstractC15340mz r2, boolean z) {
        boolean A1X = C12960it.A1X(r2, ((AbstractC28551Oa) this).A0O);
        super.A1D(r2, z);
        if (z || A1X) {
            A0X();
        }
    }

    @Override // X.C42441vD, X.AbstractC28551Oa
    public int getCenteredLayoutId() {
        return R.layout.conversation_row_photo_change;
    }

    @Override // X.C42441vD, X.AbstractC28551Oa
    public int getIncomingLayoutId() {
        return R.layout.conversation_row_photo_change;
    }

    @Override // X.C42441vD, X.AbstractC28551Oa
    public int getOutgoingLayoutId() {
        return R.layout.conversation_row_photo_change;
    }
}
