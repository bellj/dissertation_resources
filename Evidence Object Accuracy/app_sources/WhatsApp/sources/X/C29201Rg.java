package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.1Rg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29201Rg {
    public static C15950oC A00(C31601ao r4) {
        UserJid nullable = UserJid.getNullable(r4.A01);
        AnonymousClass009.A05(nullable);
        String str = nullable.user;
        int i = 0;
        if (nullable instanceof AnonymousClass1MU) {
            i = 1;
        }
        return new C15950oC(i, str, r4.A00);
    }

    public static String A01(C15950oC r3) {
        String str;
        if (r3.A01 == 0) {
            str = "s.whatsapp.net";
        } else {
            str = "lid";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(r3.A02);
        sb.append('@');
        sb.append(str);
        return sb.toString();
    }

    public static C31351aP A02(C15980oF r3) {
        C15950oC r0 = r3.A00;
        return new C31351aP(r3.A01, new C31601ao(A01(r0), r0.A00));
    }
}
