package X;

import java.util.Locale;

/* renamed from: X.2SQ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2SQ implements AnonymousClass1FK {
    public int A00 = 0;
    public final /* synthetic */ AnonymousClass17Y A01;
    public final /* synthetic */ AnonymousClass103 A02;

    public AnonymousClass2SQ(AnonymousClass17Y r2, AnonymousClass103 r3) {
        this.A02 = r3;
        this.A01 = r2;
    }

    public final void A00() {
        int i = this.A00;
        AnonymousClass103 r0 = this.A02;
        if (i < 3) {
            r0.A0C.A08(this, 2);
            this.A00++;
            return;
        }
        r0.A0F.A05(String.format(Locale.US, "AccountRecoveryNotification: sendGetPaymentMethods retry failed, attempts made: %d", Integer.valueOf(i)));
    }

    @Override // X.AnonymousClass1FK
    public void AV3(C452120p r3) {
        this.A02.A0F.A05("AccountRecoveryNotification: sendGetPaymentMethods request error");
        A00();
    }

    @Override // X.AnonymousClass1FK
    public void AVA(C452120p r3) {
        this.A02.A0F.A05("AccountRecoveryNotification: sendGetPaymentMethods response error");
        A00();
    }

    @Override // X.AnonymousClass1FK
    public void AVB(C452220q r4) {
        AnonymousClass103 r2 = this.A02;
        r2.A0F.A06("AccountRecoveryNotification: sendGetPaymentMethods success");
        C17070qD r22 = r2.A0E;
        r22.A03();
        C241414j r1 = r22.A09;
        if (r1 != null) {
            r22.A03();
            if (r1.A06() != null) {
                r22.A03();
                AbstractC28901Pl A06 = r1.A06();
                if (A06 != null) {
                    AnonymousClass17Y r23 = this.A01;
                    r23.AfV(A06.A08);
                    r23.A8l(null, false);
                }
            }
        }
    }
}
