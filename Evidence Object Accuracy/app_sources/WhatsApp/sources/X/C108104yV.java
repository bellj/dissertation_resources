package X;

/* renamed from: X.4yV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C108104yV implements AbstractC116615Wd {
    public AbstractC117055Yb A00;
    public AbstractC116615Wd A01;
    public boolean A02 = true;
    public boolean A03;
    public final AnonymousClass5Ps A04;
    public final C108114yW A05;

    public C108104yV(AnonymousClass5Ps r2, AnonymousClass5Xd r3) {
        this.A04 = r2;
        this.A05 = new C108114yW(r3);
    }

    @Override // X.AbstractC116615Wd
    public C94344be AFk() {
        AbstractC116615Wd r0 = this.A01;
        if (r0 != null) {
            return r0.AFk();
        }
        return this.A05.A02;
    }

    @Override // X.AbstractC116615Wd
    public long AFr() {
        if (this.A02) {
            return this.A05.AFr();
        }
        return this.A01.AFr();
    }

    @Override // X.AbstractC116615Wd
    public void AcX(C94344be r2) {
        AbstractC116615Wd r0 = this.A01;
        if (r0 != null) {
            r0.AcX(r2);
            r2 = this.A01.AFk();
        }
        this.A05.AcX(r2);
    }
}
