package X;

import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CRLException;
import java.security.cert.X509CRLEntry;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5Hp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C113445Hp extends X509CRLEntry {
    public AnonymousClass5N2 A00;
    public AnonymousClass5MS A01;
    public volatile int A02;
    public volatile boolean A03;

    @Override // java.security.cert.X509CRLEntry, java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C113445Hp)) {
            return super.equals(this);
        }
        C113445Hp r3 = (C113445Hp) obj;
        if (!this.A03 || !r3.A03 || this.A02 == r3.A02) {
            return this.A01.equals(r3.A01);
        }
        return false;
    }

    @Override // java.security.cert.X509CRLEntry
    public X500Principal getCertificateIssuer() {
        AnonymousClass5N2 r0 = this.A00;
        if (r0 == null) {
            return null;
        }
        try {
            return new X500Principal(r0.A01());
        } catch (IOException unused) {
            return null;
        }
    }

    @Override // java.security.cert.X509Extension
    public Set getCriticalExtensionOIDs() {
        return A00(true);
    }

    @Override // java.security.cert.X509CRLEntry
    public byte[] getEncoded() {
        try {
            return this.A01.A02("DER");
        } catch (IOException e) {
            throw new CRLException(e.toString());
        }
    }

    @Override // java.security.cert.X509Extension
    public Set getNonCriticalExtensionOIDs() {
        return A00(false);
    }

    @Override // java.security.cert.X509Extension
    public boolean hasUnsupportedCriticalExtension() {
        Set criticalExtensionOIDs = getCriticalExtensionOIDs();
        return criticalExtensionOIDs != null && !criticalExtensionOIDs.isEmpty();
    }

    @Override // java.security.cert.X509CRLEntry, java.lang.Object
    public int hashCode() {
        if (!this.A03) {
            this.A02 = super.hashCode();
            this.A03 = true;
        }
        return this.A02;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        r4 = X.AnonymousClass5N2.A00(r3[r2].A01);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C113445Hp(X.AnonymousClass5N2 r6, X.AnonymousClass5MS r7, boolean r8) {
        /*
            r5 = this;
            r5.<init>()
            r5.A01 = r7
            r4 = 0
            if (r8 == 0) goto L_0x0036
            X.1TK r1 = X.C114715Mu.A0A
            X.5MX r0 = r7.A03()
            if (r0 == 0) goto L_0x0035
            X.5Mu r0 = X.AnonymousClass5MX.A00(r1, r0)
            if (r0 == 0) goto L_0x0035
            X.1TN r0 = r0.A03()     // Catch: Exception -> 0x0036
            X.5N1[] r3 = X.C114705Mt.A01(r0)     // Catch: Exception -> 0x0036
            r2 = 0
        L_0x001f:
            int r0 = r3.length     // Catch: Exception -> 0x0036
            if (r2 >= r0) goto L_0x0036
            r0 = r3[r2]     // Catch: Exception -> 0x0036
            int r1 = r0.A00     // Catch: Exception -> 0x0036
            r0 = 4
            if (r1 != r0) goto L_0x0032
            r0 = r3[r2]     // Catch: Exception -> 0x0036
            X.1TN r0 = r0.A01     // Catch: Exception -> 0x0036
            X.5N2 r4 = X.AnonymousClass5N2.A00(r0)     // Catch: Exception -> 0x0036
            goto L_0x0036
        L_0x0032:
            int r2 = r2 + 1
            goto L_0x001f
        L_0x0035:
            r4 = r6
        L_0x0036:
            r5.A00 = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C113445Hp.<init>(X.5N2, X.5MS, boolean):void");
    }

    public final Set A00(boolean z) {
        AnonymousClass5MX A03 = this.A01.A03();
        if (A03 == null) {
            return null;
        }
        HashSet A12 = C12970iu.A12();
        Enumeration elements = A03.A01.elements();
        while (elements.hasMoreElements()) {
            AnonymousClass1TK r1 = (AnonymousClass1TK) elements.nextElement();
            if (z == AnonymousClass5MX.A00(r1, A03).A02) {
                A12.add(r1.A01);
            }
        }
        return A12;
    }

    @Override // java.security.cert.X509Extension
    public byte[] getExtensionValue(String str) {
        C114715Mu r0;
        AnonymousClass1TK A12 = C72453ed.A12(str);
        AnonymousClass5MX A03 = this.A01.A03();
        if (A03 != null) {
            r0 = AnonymousClass5MX.A00(A12, A03);
        } else {
            r0 = null;
        }
        if (r0 == null) {
            return null;
        }
        try {
            return r0.A01.A01();
        } catch (Exception e) {
            throw C12960it.A0U(C12960it.A0d(e.toString(), C12960it.A0k("Exception encoding: ")));
        }
    }

    @Override // java.security.cert.X509CRLEntry
    public Date getRevocationDate() {
        return C114765Mz.A00(AbstractC114775Na.A01(this.A01.A00)).A04();
    }

    @Override // java.security.cert.X509CRLEntry
    public BigInteger getSerialNumber() {
        return new BigInteger(AnonymousClass5NG.A00(AbstractC114775Na.A00(this.A01.A00)).A01);
    }

    @Override // java.security.cert.X509CRLEntry
    public boolean hasExtensions() {
        return C12960it.A1W(this.A01.A03());
    }

    @Override // java.security.cert.X509CRLEntry, java.lang.Object
    public String toString() {
        Object A00;
        StringBuffer stringBuffer = new StringBuffer();
        String str = AnonymousClass1T7.A00;
        stringBuffer.append("      userCertificate: ");
        stringBuffer.append(getSerialNumber());
        stringBuffer.append(str);
        stringBuffer.append("       revocationDate: ");
        stringBuffer.append(getRevocationDate());
        stringBuffer.append(str);
        stringBuffer.append("       certificateIssuer: ");
        stringBuffer.append(getCertificateIssuer());
        stringBuffer.append(str);
        AnonymousClass5MX A03 = this.A01.A03();
        if (A03 != null) {
            Enumeration elements = A03.A01.elements();
            if (elements.hasMoreElements()) {
                String str2 = "   crlEntryExtensions:";
                loop0: while (true) {
                    stringBuffer.append(str2);
                    while (true) {
                        stringBuffer.append(str);
                        while (elements.hasMoreElements()) {
                            AnonymousClass1TK r5 = (AnonymousClass1TK) elements.nextElement();
                            C114715Mu A002 = AnonymousClass5MX.A00(r5, A03);
                            AnonymousClass5NH r0 = A002.A01;
                            if (r0 != null) {
                                AnonymousClass1TO A003 = AnonymousClass5NH.A00(stringBuffer, r0, A002);
                                try {
                                    if (r5.A04(C114715Mu.A0T)) {
                                        A00 = C114675Mq.A00(AnonymousClass5ND.A00(A003.A05()));
                                    } else if (r5.A04(C114715Mu.A0A)) {
                                        stringBuffer.append("Certificate issuer: ");
                                        A00 = C114705Mt.A00(A003.A05());
                                    } else {
                                        stringBuffer.append(r5.A01);
                                        stringBuffer.append(" value = ");
                                        stringBuffer.append(C93034Ys.A00(A003.A05()));
                                        stringBuffer.append(str);
                                    }
                                    stringBuffer.append(A00);
                                    stringBuffer.append(str);
                                } catch (Exception unused) {
                                    stringBuffer.append(r5.A01);
                                    stringBuffer.append(" value = ");
                                    str2 = "*****";
                                }
                            }
                        }
                    }
                }
            }
        }
        return stringBuffer.toString();
    }
}
