package X;

/* renamed from: X.43J  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass43J extends AbstractC16110oT {
    public Integer A00;
    public String A01;

    public AnonymousClass43J() {
        super(3436, AbstractC16110oT.A00(), 2, 0);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(1, this.A00);
        r3.Abe(2, this.A01);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamDirectoryBusinessProfileViewsEvent {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "businessProfileEvent", C12960it.A0Y(this.A00));
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "directoryBusinessJid", this.A01);
        return C12960it.A0d("}", A0k);
    }
}
