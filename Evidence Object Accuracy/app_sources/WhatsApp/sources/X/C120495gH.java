package X;

import android.content.Context;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;

/* renamed from: X.5gH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120495gH extends C126705tJ {
    public final Context A00;
    public final C14900mE A01;
    public final C15570nT A02;
    public final AnonymousClass102 A03;
    public final C14850m9 A04;
    public final C17220qS A05;
    public final C1329668y A06;
    public final C21860y6 A07;
    public final C18650sn A08;
    public final C17070qD A09;
    public final AnonymousClass6BE A0A;
    public final C121265hX A0B;

    public C120495gH(Context context, C14900mE r2, C15570nT r3, AnonymousClass102 r4, C14850m9 r5, C17220qS r6, C1329668y r7, C21860y6 r8, C18650sn r9, C64513Fv r10, C18610sj r11, C17070qD r12, AnonymousClass6BE r13, C121265hX r14) {
        super(r10, r11);
        this.A00 = context;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A05 = r6;
        this.A09 = r12;
        this.A07 = r8;
        this.A03 = r4;
        this.A0A = r13;
        this.A08 = r9;
        this.A06 = r7;
        this.A0B = r14;
    }

    public void A00(UserJid userJid, AnonymousClass6MQ r13, Boolean bool) {
        Log.i(C12960it.A0b("PAY: sendGetContactInfoForJid: ", userJid));
        this.A0A.AeG();
        this.A0B.A02(185477767, "in_upi_get_vpa_tag");
        C64513Fv r7 = super.A00;
        if (r7 != null) {
            r7.A04("upi-get-vpa");
        }
        String str = null;
        if (bool != null && this.A07.A0A() && this.A04.A07(1450)) {
            str = bool.booleanValue() ? "true" : "false";
        }
        C17220qS r2 = this.A05;
        String A01 = r2.A01();
        C130485zU r10 = new C130485zU(userJid, new AnonymousClass3CS(A01), str);
        C117325Zm.A06(r2, new C120875gt(this.A00, this.A01, this.A08, r7, r13, this, r10), r10.A00, A01);
    }

    public void A01(AnonymousClass6MQ r4) {
        C15570nT r0 = this.A02;
        r0.A08();
        A00(r0.A05, new AnonymousClass6AQ(r4, this), null);
    }
}
