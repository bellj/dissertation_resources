package X;

/* renamed from: X.3UE  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UE implements AbstractC116455Vm {
    public final /* synthetic */ DialogC58332oe A00;

    public AnonymousClass3UE(DialogC58332oe r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC116455Vm
    public void AMr() {
        C12960it.A0v(this.A00.A01);
    }

    @Override // X.AbstractC116455Vm
    public void APc(int[] iArr) {
        DialogC58332oe r0 = this.A00;
        AbstractC36671kL.A08(r0.A01, iArr, r0.A0B);
    }
}
