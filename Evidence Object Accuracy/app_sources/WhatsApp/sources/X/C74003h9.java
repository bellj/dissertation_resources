package X;

import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

/* renamed from: X.3h9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C74003h9 extends TranslateAnimation {
    public final /* synthetic */ AbstractC36001jA A00;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C74003h9(AbstractC36001jA r10) {
        super(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        this.A00 = r10;
    }

    @Override // android.view.animation.TranslateAnimation, android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        super.applyTransformation(f, transformation);
        AbstractC36001jA r2 = this.A00;
        r2.A0G((int) (((float) r2.A0J.getHeight()) * (1.0f - f)));
    }
}
