package X;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

/* renamed from: X.5LI  reason: invalid class name */
/* loaded from: classes3.dex */
public abstract class AnonymousClass5LI extends AnonymousClass4WM {
    public static final /* synthetic */ AtomicReferenceFieldUpdater A00 = AtomicReferenceFieldUpdater.newUpdater(AnonymousClass5LI.class, Object.class, "_consensus");
    public volatile /* synthetic */ Object _consensus = C88554Gc.A00;

    public Object A01(Object obj) {
        if (((AnonymousClass5L6) ((AnonymousClass5LA) this).A00).size == 0) {
            return null;
        }
        return C88574Ge.A00;
    }
}
