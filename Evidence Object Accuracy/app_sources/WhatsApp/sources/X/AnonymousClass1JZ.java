package X;

import android.util.Pair;
import java.util.Arrays;

/* renamed from: X.1JZ  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1JZ {
    public final C27821Ji A00;
    public final AnonymousClass1JR A01;

    public AnonymousClass1JZ(C27821Ji r1, AnonymousClass1JR r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    public static Pair A00(C34861gp r6) {
        AnonymousClass1JZ r1;
        C34851go r12 = r6.A02;
        if (r12 == null) {
            r12 = C34851go.A02;
        }
        if ((r12.A00 & 1) == 1) {
            AnonymousClass1JR r5 = new AnonymousClass1JR(r12.A01.A04());
            if ((r6.A00 & 2) == 2) {
                C57282mm r62 = r6.A01;
                if (r62 == null) {
                    r62 = C57282mm.A04;
                }
                int i = r62.A00;
                if ((i & 4) == 4 || (i & 2) == 2 || (i & 1) == 1) {
                    C34311fw r0 = r62.A03;
                    if (r0 == null) {
                        r0 = C34311fw.A05;
                    }
                    C34321fx A00 = C34321fx.A00(r0);
                    if (A00 != null) {
                        r1 = new AnonymousClass1JZ(new C27821Ji(A00, r62.A02.A04(), r62.A01), r5);
                    }
                }
                throw new AnonymousClass1K1(57, null);
            }
            r1 = null;
            return new Pair(r5, r1);
        }
        throw new AnonymousClass1K1(53, null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass1JZ)) {
            return false;
        }
        AnonymousClass1JZ r4 = (AnonymousClass1JZ) obj;
        if (!this.A01.equals(r4.A01) || !this.A00.equals(r4.A00)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.A01, this.A00});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SyncdKey{keyId=");
        sb.append(this.A01);
        sb.append(", syncdKeyData=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
