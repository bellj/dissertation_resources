package X;

import com.whatsapp.jid.DeviceJid;
import com.whatsapp.util.Log;

/* renamed from: X.3UK  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3UK implements AbstractC27101Ga {
    public final /* synthetic */ C64263Ew A00;

    @Override // X.AbstractC27101Ga
    public void AQx(DeviceJid deviceJid, int i) {
    }

    @Override // X.AbstractC27101Ga
    public void ARG(DeviceJid deviceJid) {
    }

    public AnonymousClass3UK(C64263Ew r1) {
        this.A00 = r1;
    }

    @Override // X.AbstractC27101Ga
    public void ARH(DeviceJid deviceJid) {
        AnonymousClass3CL r1 = this.A00.A04;
        Log.i(C12960it.A0b("VoiceService/notifyDeviceIdentityChanged ", deviceJid));
        C29631Ua.A03(r1.A00, deviceJid, false);
    }

    @Override // X.AbstractC27101Ga
    public void ARI(DeviceJid deviceJid) {
        AnonymousClass3CL r1 = this.A00.A04;
        Log.i(C12960it.A0b("VoiceService/notifyDeviceIdentityDeleted ", deviceJid));
        C29631Ua.A03(r1.A00, deviceJid, true);
    }
}
