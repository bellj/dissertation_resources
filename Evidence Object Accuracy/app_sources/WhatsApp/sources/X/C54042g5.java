package X;

import java.util.List;

/* renamed from: X.2g5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C54042g5 extends AnonymousClass0Q0 {
    public final List A00;
    public final List A01;

    public C54042g5(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        AnonymousClass5TM r3 = (AnonymousClass5TM) this.A01.get(i);
        AnonymousClass5TM r2 = (AnonymousClass5TM) this.A00.get(i2);
        int type = r3.getType();
        if (type != r2.getType()) {
            return false;
        }
        if (type == 0) {
            return ((C1097953b) r3).A00.equals(((C1097953b) r2).A00);
        }
        if (type == 1) {
            return ((C1097853a) r3).A00.equals(((C1097853a) r2).A00);
        }
        return true;
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        String str;
        String str2;
        AnonymousClass5TM r3 = (AnonymousClass5TM) this.A01.get(i);
        AnonymousClass5TM r2 = (AnonymousClass5TM) this.A00.get(i2);
        int type = r3.getType();
        if (type != r2.getType()) {
            return false;
        }
        if (type == 0) {
            str = ((C1097953b) r3).A00.A0D;
            str2 = ((C1097953b) r2).A00.A0D;
        } else if (type != 1) {
            return true;
        } else {
            str = ((C1097853a) r3).A00;
            str2 = ((C1097853a) r2).A00;
        }
        return str.equals(str2);
    }
}
