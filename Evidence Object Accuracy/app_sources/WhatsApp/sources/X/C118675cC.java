package X;

import androidx.recyclerview.widget.RecyclerView;
import com.whatsapp.payments.ui.IndiaUpiBankPickerActivity;

/* renamed from: X.5cC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118675cC extends AbstractC05270Ox {
    public final /* synthetic */ IndiaUpiBankPickerActivity A00;

    public C118675cC(IndiaUpiBankPickerActivity indiaUpiBankPickerActivity) {
        this.A00 = indiaUpiBankPickerActivity;
    }

    @Override // X.AbstractC05270Ox
    public void A00(RecyclerView recyclerView, int i) {
        this.A00.A0L = true;
    }
}
