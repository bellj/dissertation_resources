package X;

import android.content.Context;
import android.location.Address;
import android.text.TextUtils;
import com.whatsapp.R;

/* renamed from: X.2El  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C48132El {
    public static String A00(Context context, Address address, float f) {
        String thoroughfare = address.getThoroughfare();
        if (((double) f) <= 200.0d && !TextUtils.isEmpty(thoroughfare)) {
            String subThoroughfare = address.getSubThoroughfare();
            return !TextUtils.isEmpty(subThoroughfare) ? context.getString(R.string.biz_dir_address_number_format, thoroughfare, subThoroughfare) : thoroughfare;
        } else if (!TextUtils.isEmpty(address.getSubLocality())) {
            return address.getSubLocality();
        } else {
            if (!TextUtils.isEmpty(address.getLocality())) {
                return address.getLocality();
            }
            if (!TextUtils.isEmpty(address.getSubAdminArea())) {
                return address.getSubAdminArea();
            }
            return null;
        }
    }
}
