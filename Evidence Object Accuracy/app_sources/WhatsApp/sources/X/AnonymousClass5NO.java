package X;

import java.util.Arrays;

/* renamed from: X.5NO  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5NO extends AnonymousClass1TL implements AnonymousClass5VP {
    public byte[] A00;

    public AnonymousClass5NO(byte[] bArr) {
        this.A00 = AnonymousClass1TT.A02(bArr);
    }

    @Override // X.AnonymousClass1TL
    public void A08(AnonymousClass1TP r3, boolean z) {
        r3.A06(this.A00, 20, z);
    }

    @Override // X.AnonymousClass1TL
    public boolean A09() {
        return false;
    }

    @Override // X.AnonymousClass5VP
    public String AGy() {
        return AnonymousClass1T7.A02(this.A00);
    }

    @Override // X.AnonymousClass1TL, X.AnonymousClass1TM
    public int hashCode() {
        return AnonymousClass1TT.A00(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public int A05() {
        return C72453ed.A0H(this.A00);
    }

    @Override // X.AnonymousClass1TL
    public boolean A0A(AnonymousClass1TL r3) {
        if (!(r3 instanceof AnonymousClass5NO)) {
            return false;
        }
        return Arrays.equals(this.A00, ((AnonymousClass5NO) r3).A00);
    }

    public String toString() {
        return AnonymousClass1T7.A02(this.A00);
    }
}
