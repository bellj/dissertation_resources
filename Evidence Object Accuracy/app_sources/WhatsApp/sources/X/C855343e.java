package X;

/* renamed from: X.43e  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C855343e extends AbstractC16110oT {
    public Long A00;
    public Long A01;
    public Long A02;
    public String A03;

    public C855343e() {
        super(2796, AbstractC16110oT.A00(), 0, -1);
    }

    @Override // X.AbstractC16110oT
    public void serialize(AnonymousClass1N6 r3) {
        r3.Abe(2, this.A00);
        r3.Abe(3, this.A01);
        r3.Abe(4, this.A03);
        r3.Abe(1, this.A02);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder A0k = C12960it.A0k("WamStickerContextualSuggestionDailyCount {");
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numClicked", this.A00);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numStickersSent", this.A01);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numStickersSuggestionsArray", this.A03);
        AbstractC16110oT.appendFieldToStringBuilder(A0k, "numTriggered", this.A02);
        return C12960it.A0d("}", A0k);
    }
}
