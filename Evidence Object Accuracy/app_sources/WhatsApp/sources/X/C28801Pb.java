package X;

import android.content.Context;
import android.os.BadParcelableException;
import android.text.TextPaint;
import android.view.View;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import java.util.List;

/* renamed from: X.1Pb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28801Pb {
    public final Context A00;
    public final TextEmojiLabel A01;
    public final C15610nY A02;
    public final AnonymousClass12F A03;

    public C28801Pb(Context context, TextEmojiLabel textEmojiLabel, C15610nY r3, AnonymousClass12F r4) {
        this.A00 = context;
        this.A01 = textEmojiLabel;
        this.A02 = r3;
        this.A03 = r4;
    }

    public C28801Pb(View view, C15610nY r4, AnonymousClass12F r5, int i) {
        this(view.getContext(), (TextEmojiLabel) view.findViewById(i), r4, r5);
    }

    public static void A00(Context context, C28801Pb r1, int i) {
        r1.A04(AnonymousClass00T.A00(context, i));
    }

    public TextPaint A01() {
        return this.A01.getPaint();
    }

    public void A02() {
        TextEmojiLabel textEmojiLabel = this.A01;
        textEmojiLabel.setText(this.A00.getString(R.string.you));
        textEmojiLabel.setCompoundDrawables(null, null, null, null);
    }

    public void A03() {
        TextEmojiLabel textEmojiLabel = this.A01;
        textEmojiLabel.setText(this.A00.getString(R.string.my_status));
        textEmojiLabel.setCompoundDrawables(null, null, null, null);
    }

    public void A04(int i) {
        this.A01.setTextColor(i);
    }

    public void A05(int i) {
        TextEmojiLabel textEmojiLabel;
        int i2;
        if (i != 0) {
            if (i == 1) {
                textEmojiLabel = this.A01;
                i2 = R.drawable.ic_verified;
            } else if (i == 2) {
                textEmojiLabel = this.A01;
                i2 = R.drawable.ic_verified_large;
            } else {
                return;
            }
            textEmojiLabel.A0B(i2);
            return;
        }
        this.A01.setCompoundDrawables(null, null, null, null);
    }

    public void A06(C15370n3 r3) {
        A07(r3, null, -1);
    }

    public void A07(C15370n3 r5, List list, int i) {
        String str;
        if (r5.A0M()) {
            str = C15610nY.A02(r5, false);
        } else {
            str = this.A02.A0B(r5, i, false);
        }
        try {
            this.A01.A0F(str, list, 256, false);
            A05(r5.A0M() ? 1 : 0);
        } catch (BadParcelableException e) {
            throw e;
        }
    }

    public void A08(CharSequence charSequence) {
        this.A01.setText(charSequence);
    }

    public void A09(List list, CharSequence charSequence) {
        if (!(this instanceof C58442pT)) {
            this.A01.A0F(charSequence, list, 0, false);
        } else {
            this.A01.A0D(null, charSequence, list, 256, false);
        }
    }
}
