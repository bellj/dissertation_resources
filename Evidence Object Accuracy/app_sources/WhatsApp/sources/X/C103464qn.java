package X;

import android.content.Context;
import com.whatsapp.settings.SettingsPrivacy;

/* renamed from: X.4qn  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C103464qn implements AbstractC009204q {
    public final /* synthetic */ SettingsPrivacy A00;

    public C103464qn(SettingsPrivacy settingsPrivacy) {
        this.A00 = settingsPrivacy;
    }

    @Override // X.AbstractC009204q
    public void AOc(Context context) {
        this.A00.A1k();
    }
}
