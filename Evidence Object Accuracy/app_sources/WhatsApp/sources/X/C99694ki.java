package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.4ki  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C99694ki implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        return new C30181Wk(parcel);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C30181Wk[i];
    }
}
