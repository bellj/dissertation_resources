package X;

import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.Adler32;
import java.util.zip.Checksum;

/* renamed from: X.02V  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass02V {
    public static final long A07 = new Adler32().getValue();
    public int A00;
    public int A01 = 0;
    public final int A02;
    public final AnonymousClass16F A03;
    public final RandomAccessFile A04;
    public final ByteBuffer A05;
    public final Checksum A06;

    public AnonymousClass02V(AnonymousClass16F r4, RandomAccessFile randomAccessFile, int i, int i2) {
        this.A04 = randomAccessFile;
        this.A02 = i;
        ByteBuffer allocate = ByteBuffer.allocate(i2);
        this.A05 = allocate;
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        this.A06 = new Adler32();
        this.A00 = 0;
        this.A03 = r4;
    }

    public final long A00() {
        Checksum checksum = this.A06;
        ByteBuffer byteBuffer = this.A05;
        checksum.update(byteBuffer.array(), this.A00, byteBuffer.position() - this.A00);
        this.A00 = byteBuffer.position();
        return checksum.getValue();
    }

    public final ByteBuffer A01() {
        ByteBuffer asReadOnlyBuffer = this.A05.asReadOnlyBuffer();
        asReadOnlyBuffer.flip();
        return asReadOnlyBuffer;
    }

    public void A02() {
        this.A05.clear();
        this.A01 = 0;
        this.A06.reset();
        this.A00 = 0;
    }

    public void A03() {
        ByteBuffer byteBuffer;
        int i;
        RandomAccessFile randomAccessFile = this.A04;
        if (randomAccessFile != null && (byteBuffer = this.A05).position() != (i = this.A01)) {
            A06((long) (this.A02 + i));
            try {
                randomAccessFile.write(byteBuffer.array(), this.A01, byteBuffer.position() - this.A01);
                this.A01 = byteBuffer.position();
            } catch (IOException e) {
                AnonymousClass16F r0 = this.A03;
                r0.A09();
                r0.A05();
                throw e;
            }
        }
    }

    public void A04(int i) {
        A06((long) this.A02);
        try {
            RandomAccessFile randomAccessFile = this.A04;
            ByteBuffer byteBuffer = this.A05;
            randomAccessFile.readFully(byteBuffer.array(), 0, i);
            byteBuffer.position(i);
            this.A01 = i;
            this.A06.reset();
            this.A00 = 0;
        } catch (EOFException e) {
            AnonymousClass16F r0 = this.A03;
            r0.A06();
            r0.A05();
            throw e;
        } catch (IOException e2) {
            AnonymousClass16F r02 = this.A03;
            r02.A08();
            r02.A05();
            throw e2;
        }
    }

    public final void A05(long j) {
        if (j < 0 || j > 4294967295L) {
            throw new IllegalArgumentException("Value is not an unsigned integer");
        }
        this.A05.putInt((int) j);
    }

    public final void A06(long j) {
        try {
            this.A04.seek(j);
        } catch (IOException e) {
            AnonymousClass16F r0 = this.A03;
            r0.A07();
            r0.A05();
            throw e;
        }
    }
}
