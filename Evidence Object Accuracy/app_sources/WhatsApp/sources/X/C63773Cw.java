package X;

import com.whatsapp.jid.GroupJid;

/* renamed from: X.3Cw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63773Cw {
    public final AbstractC15710nm A00;
    public final C15580nU A01;
    public final C17220qS A02;
    public final AnonymousClass5WD A03;

    public C63773Cw(AbstractC15710nm r1, C15580nU r2, C17220qS r3, AnonymousClass5WD r4) {
        this.A00 = r1;
        this.A01 = r2;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A00(GroupJid groupJid) {
        C17220qS r8 = this.A02;
        String A01 = r8.A01();
        AnonymousClass1W9[] r3 = new AnonymousClass1W9[2];
        boolean A1a = C12990iw.A1a("type", "sub_group", r3);
        r3[1] = new AnonymousClass1W9(groupJid, "jid");
        AnonymousClass1V8 r4 = new AnonymousClass1V8("query_linked", r3);
        AnonymousClass1W9[] r32 = new AnonymousClass1W9[4];
        C12960it.A1M("id", A01, r32, A1a ? 1 : 0);
        C12960it.A1M("xmlns", "w:g2", r32, 1);
        C12960it.A1M("type", "get", r32, 2);
        r32[3] = new AnonymousClass1W9(this.A01, "to");
        r8.A09(new AnonymousClass3Z7(this.A00, this.A03), new AnonymousClass1V8(r4, "iq", r32), A01, 298, 32000);
    }
}
