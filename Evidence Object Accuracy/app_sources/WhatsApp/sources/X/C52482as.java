package X;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import com.whatsapp.stickers.StickerView;

/* renamed from: X.2as  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52482as extends View {
    public final Rect A00 = C12980iv.A0J();
    public final /* synthetic */ AnonymousClass3FR A01;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C52482as(Context context, AnonymousClass3FR r3) {
        super(context);
        this.A01 = r3;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isSelected()) {
            AnonymousClass3FR r2 = this.A01;
            StickerView stickerView = r2.A0A;
            Rect rect = this.A00;
            stickerView.getDrawingRect(rect);
            C64533Fx r0 = ((AbstractC28551Oa) r2.A0B).A0b;
            if (r0 != null) {
                canvas.drawRect(rect, r0.A00);
            }
        }
    }

    @Override // android.view.View
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        StickerView stickerView = this.A01.A0A;
        int measuredHeight = stickerView.getMeasuredHeight();
        int measuredWidth = stickerView.getMeasuredWidth();
        if (mode != 1073741824) {
            if (mode == Integer.MIN_VALUE) {
                size = Math.min(measuredWidth, size);
            } else {
                size = measuredWidth;
            }
        }
        if (mode2 != 1073741824) {
            if (mode2 == Integer.MIN_VALUE) {
                size2 = Math.min(size2, measuredHeight);
            } else {
                size2 = measuredHeight;
            }
        }
        setMeasuredDimension(size, size2);
    }
}
