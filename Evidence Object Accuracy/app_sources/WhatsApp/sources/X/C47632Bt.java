package X;

import com.whatsapp.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* renamed from: X.2Bt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C47632Bt {
    public int A00 = 0;
    public C14850m9 A01;
    public String A02;
    public HashMap A03;
    public byte[] A04 = null;
    public C47682By[] A05;
    public RandomAccessFile[] A06;
    public boolean[] A07;
    public final C14820m6 A08;
    public final AnonymousClass16D A09;
    public final AnonymousClass16F A0A;

    public C47632Bt(C14820m6 r5, C14850m9 r6, AnonymousClass16D r7, AnonymousClass16F r8) {
        this.A0A = r8;
        this.A09 = r7;
        this.A01 = r6;
        this.A08 = r5;
        this.A03 = new HashMap();
        boolean[] zArr = new boolean[8];
        this.A07 = zArr;
        int i = 0;
        do {
            zArr[i] = false;
            i++;
        } while (i < 8);
        this.A02 = "";
        this.A05 = new C47682By[3];
        this.A06 = new RandomAccessFile[3];
    }

    public void A00(String str) {
        Exception e;
        this.A02 = str;
        int i = 0;
        do {
            boolean[] zArr = this.A07;
            zArr[i] = false;
            StringBuilder sb = new StringBuilder();
            sb.append(this.A02);
            sb.append("wampsid");
            sb.append(Integer.toString(i));
            File file = new File(sb.toString());
            RandomAccessFile randomAccessFile = null;
            if (file.exists()) {
                try {
                    RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rwd");
                    try {
                        C47682By r6 = new C47682By(this.A0A, randomAccessFile2, i);
                        RandomAccessFile randomAccessFile3 = r6.A07;
                        randomAccessFile3.seek(0);
                        byte[] bArr = C47682By.A08;
                        byte[] bArr2 = new byte[bArr.length];
                        randomAccessFile3.read(bArr2);
                        if (Arrays.equals(bArr2, bArr)) {
                            r6.A01();
                            this.A03.put(r6.A03, r6);
                            zArr[i] = true;
                        } else {
                            Log.e("privatestatsuploadqueue/initfromqueuefile invalid queue file");
                            try {
                                randomAccessFile2.close();
                                file.delete();
                                return;
                            } catch (IOException | SecurityException e2) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("privatestatsuploadqueue/initfromqueuefile failed to delete the corrupted queue file ");
                                sb2.append(e2.toString());
                                Log.e(sb2.toString());
                                return;
                            }
                        }
                    } catch (Exception e3) {
                        e = e3;
                        randomAccessFile = randomAccessFile2;
                        StringBuilder sb3 = new StringBuilder("privatestatsuploadqueue/initfromqueuefile failed to load the queue file ");
                        sb3.append(e.toString());
                        Log.e(sb3.toString());
                        if (randomAccessFile != null) {
                            try {
                                randomAccessFile.close();
                            } catch (IOException | SecurityException e4) {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("privatestatsuploadqueue/initfromqueuefile failed to delete the corrupted queue file ");
                                sb4.append(e4.toString());
                                Log.e(sb4.toString());
                            }
                        }
                        i++;
                        if (i >= 8) {
                        }
                    }
                } catch (Exception e5) {
                    e = e5;
                }
            }
            i++;
        } while (i >= 8);
    }

    public boolean A01() {
        for (Map.Entry entry : this.A03.entrySet()) {
            if (((C47682By) entry.getValue()).A05()) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0095 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean A02(byte[] r9, int r10, java.lang.String r11) {
        /*
            r8 = this;
            r7 = 0
            java.util.HashMap r4 = r8.A03
            java.lang.Object r6 = r4.get(r11)
            X.2By r6 = (X.C47682By) r6
            if (r6 != 0) goto L_0x0099
            r3 = 0
        L_0x000c:
            boolean[] r2 = r8.A07
            boolean r0 = r2[r3]
            if (r0 != 0) goto L_0x008f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch: Exception -> 0x0075
            r1.<init>()     // Catch: Exception -> 0x0075
            java.lang.String r0 = r8.A02     // Catch: Exception -> 0x0075
            r1.append(r0)     // Catch: Exception -> 0x0075
            java.lang.String r0 = "wampsid"
            r1.append(r0)     // Catch: Exception -> 0x0075
            java.lang.String r0 = java.lang.Integer.toString(r3)     // Catch: Exception -> 0x0075
            r1.append(r0)     // Catch: Exception -> 0x0075
            java.lang.String r0 = r1.toString()     // Catch: Exception -> 0x0075
            java.io.File r5 = new java.io.File     // Catch: Exception -> 0x0075
            r5.<init>(r0)     // Catch: Exception -> 0x0075
            java.lang.String r0 = "rwd"
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch: Exception -> 0x0075
            r1.<init>(r5, r0)     // Catch: Exception -> 0x0075
            X.2By r5 = new X.2By     // Catch: Exception -> 0x0075
            X.16F r0 = r8.A0A     // Catch: Exception -> 0x0075
            r5.<init>(r0, r1, r3)     // Catch: Exception -> 0x0075
            X.16D r6 = r8.A09     // Catch: Exception -> 0x0078
            android.content.SharedPreferences r1 = r6.A01()     // Catch: Exception -> 0x0078
            r0 = -1
            int r0 = r1.getInt(r11, r0)     // Catch: Exception -> 0x0078
            int r1 = r0 + 1
            r0 = 65535(0xffff, float:9.1834E-41)
            if (r1 <= r0) goto L_0x0053
            r1 = 0
        L_0x0053:
            android.content.SharedPreferences r0 = r6.A01()     // Catch: Exception -> 0x0078
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch: Exception -> 0x0078
            android.content.SharedPreferences$Editor r0 = r0.putInt(r11, r1)     // Catch: Exception -> 0x0078
            r0.apply()     // Catch: Exception -> 0x0078
            r5.A04(r11, r1)     // Catch: Exception -> 0x0078
            r0 = 1
            r2[r3] = r0     // Catch: Exception -> 0x0078
            int r1 = r8.A00     // Catch: Exception -> 0x0078
            if (r1 <= 0) goto L_0x0071
            byte[] r0 = r8.A04     // Catch: Exception -> 0x0078
            r5.A06(r0, r1)     // Catch: Exception -> 0x0078
        L_0x0071:
            r4.put(r11, r5)     // Catch: Exception -> 0x0078
            goto L_0x0098
        L_0x0075:
            r2 = move-exception
            r5 = r6
            goto L_0x0079
        L_0x0078:
            r2 = move-exception
        L_0x0079:
            java.lang.String r0 = "privatestatsuploadqueue/writeToQueueForPsId failed to create new QueueFile "
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = r2.toString()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.whatsapp.util.Log.e(r0)
            r6 = r5
        L_0x008f:
            int r3 = r3 + 1
            r0 = 8
            if (r3 < r0) goto L_0x000c
            if (r6 != 0) goto L_0x0099
            return r7
        L_0x0098:
            r6 = r5
        L_0x0099:
            boolean r0 = r6.A06(r9, r10)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C47632Bt.A02(byte[], int, java.lang.String):boolean");
    }
}
