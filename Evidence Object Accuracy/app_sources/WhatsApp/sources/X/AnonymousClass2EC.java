package X;

import java.math.BigDecimal;

/* renamed from: X.2EC  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2EC {
    public final C30711Yn A00;
    public final BigDecimal A01;
    public final BigDecimal A02;

    public AnonymousClass2EC(C30711Yn r1, BigDecimal bigDecimal, BigDecimal bigDecimal2) {
        this.A01 = bigDecimal;
        this.A02 = bigDecimal2;
        this.A00 = r1;
    }
}
