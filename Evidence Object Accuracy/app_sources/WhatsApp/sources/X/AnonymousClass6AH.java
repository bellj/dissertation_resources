package X;

import android.content.Intent;
import com.whatsapp.R;
import com.whatsapp.payments.ui.BrazilPayBloksActivity;
import com.whatsapp.payments.ui.BrazilPaymentCardDetailsActivity;
import com.whatsapp.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/* renamed from: X.6AH  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6AH implements AnonymousClass6MO {
    public final /* synthetic */ BrazilPaymentCardDetailsActivity A00;

    public AnonymousClass6AH(BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity) {
        this.A00 = brazilPaymentCardDetailsActivity;
    }

    @Override // X.AnonymousClass6MO
    public void ANl(C30881Ze r3) {
        Log.i("PAY: BrazilPaymentCardDetailsActivity BrazilGetVerificationMethods - onCardVerified");
        BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity = this.A00;
        brazilPaymentCardDetailsActivity.AaN();
        ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A0D.A00().A03(new AbstractC451720l(r3, this) { // from class: X.67t
            public final /* synthetic */ C30881Ze A00;
            public final /* synthetic */ AnonymousClass6AH A01;

            {
                this.A01 = r2;
                this.A00 = r1;
            }

            @Override // X.AbstractC451720l
            public final void AM7(List list) {
                AnonymousClass6AH r0 = this.A01;
                C30881Ze r4 = this.A00;
                BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity2 = r0.A00;
                ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity2).A09 = r4;
                C130065yk r1 = brazilPaymentCardDetailsActivity2.A0D;
                Intent A0D = C12990iw.A0D(brazilPaymentCardDetailsActivity2, BrazilPayBloksActivity.class);
                A0D.putExtra("screen_params", r1.A02(r4, null));
                A0D.putExtra("screen_name", "brpay_p_card_verified");
                brazilPaymentCardDetailsActivity2.A2E(A0D, 1);
            }
        }, r3);
    }

    @Override // X.AnonymousClass6MO
    public void AVP(C452120p r10, ArrayList arrayList) {
        int i;
        JSONArray A02;
        BrazilPaymentCardDetailsActivity brazilPaymentCardDetailsActivity = this.A00;
        brazilPaymentCardDetailsActivity.AaN();
        if (r10 != null) {
            i = r10.A00;
            if (!(i == 443 || i == 10229 || (arrayList != null && !arrayList.isEmpty()))) {
                i = -233;
            }
        } else if (arrayList == null || arrayList.isEmpty() || (A02 = brazilPaymentCardDetailsActivity.A03.A02(arrayList)) == null || C1309660r.A01(arrayList)) {
            i = 0;
        } else {
            Intent A00 = brazilPaymentCardDetailsActivity.A0D.A00(brazilPaymentCardDetailsActivity, (C30881Ze) ((AbstractView$OnClickListenerC121765jx) brazilPaymentCardDetailsActivity).A09, A02.toString());
            AbstractActivityC119645em.A0O(A00, "referral_screen", "payment_method_details");
            brazilPaymentCardDetailsActivity.A2E(A00, 1);
            return;
        }
        Log.i(C12960it.A0W(i, "PAY: BrazilGetVerificationMethods Error: "));
        brazilPaymentCardDetailsActivity.A04.A01(brazilPaymentCardDetailsActivity, ((ActivityC13810kN) brazilPaymentCardDetailsActivity).A0C, ((AbstractActivityC121755jw) brazilPaymentCardDetailsActivity).A02, i, R.string.payment_verify_card_error).show();
    }
}
