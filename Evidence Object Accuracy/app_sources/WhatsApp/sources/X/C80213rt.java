package X;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: X.3rt  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C80213rt extends AnonymousClass5I0<Integer> implements AnonymousClass5Z6<Integer>, AbstractC115265Qv, RandomAccess {
    public static final C80213rt A02;
    public int A00;
    public int[] A01;

    static {
        C80213rt r0 = new C80213rt(new int[0], 0);
        A02 = r0;
        ((AnonymousClass5I0) r0).A00 = false;
    }

    public C80213rt() {
        this(new int[10], 0);
    }

    public C80213rt(int[] iArr, int i) {
        this.A01 = iArr;
        this.A00 = i;
    }

    public final void A03(int i) {
        A02();
        int i2 = this.A00;
        int[] iArr = this.A01;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[((i2 * 3) >> 1) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.A01 = iArr2;
            iArr = iArr2;
        }
        int i3 = this.A00;
        this.A00 = i3 + 1;
        iArr[i3] = i;
    }

    public final void A04(int i) {
        if (i < 0 || i >= this.A00) {
            throw C72453ed.A0i(i, this.A00);
        }
    }

    @Override // X.AnonymousClass5Z6
    public final /* synthetic */ AnonymousClass5Z6 Agl(int i) {
        if (i >= this.A00) {
            return new C80213rt(Arrays.copyOf(this.A01, i), this.A00);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        int A05 = C12960it.A05(obj);
        A02();
        if (i < 0 || i > (i2 = this.A00)) {
            throw C72453ed.A0i(i, this.A00);
        }
        int[] iArr = this.A01;
        if (i2 < iArr.length) {
            C72463ee.A0T(iArr, i, i2);
        } else {
            int[] iArr2 = new int[((i2 * 3) >> 1) + 1];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.A01, i, iArr2, i + 1, this.A00 - i);
            this.A01 = iArr2;
        }
        this.A01[i] = A05;
        this.A00++;
        ((AbstractList) this).modCount++;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final /* synthetic */ boolean add(Object obj) {
        A03(C12960it.A05(obj));
        return true;
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean addAll(Collection collection) {
        A02();
        if (!(collection instanceof C80213rt)) {
            return super.addAll(collection);
        }
        C80213rt r7 = (C80213rt) collection;
        int i = r7.A00;
        if (i == 0) {
            return false;
        }
        int i2 = this.A00;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.A01;
            if (i3 > iArr.length) {
                iArr = Arrays.copyOf(iArr, i3);
                this.A01 = iArr;
            }
            System.arraycopy(r7.A01, 0, iArr, this.A00, r7.A00);
            this.A00 = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final boolean contains(Object obj) {
        return C12980iv.A1V(indexOf(obj), -1);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof C80213rt)) {
                return super.equals(obj);
            }
            C80213rt r8 = (C80213rt) obj;
            int i = this.A00;
            if (i == r8.A00) {
                int[] iArr = r8.A01;
                for (int i2 = 0; i2 < i; i2++) {
                    if (this.A01[i2] == iArr[i2]) {
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object get(int i) {
        A04(i);
        return Integer.valueOf(this.A01[i]);
    }

    @Override // X.AnonymousClass5I0, java.util.AbstractList, java.util.List, java.util.Collection, java.lang.Object
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.A00; i2++) {
            i = (i * 31) + this.A01[i2];
        }
        return i;
    }

    @Override // java.util.AbstractList, java.util.List
    public final int indexOf(Object obj) {
        if (obj instanceof Integer) {
            int A05 = C12960it.A05(obj);
            int size = size();
            for (int i = 0; i < size; i++) {
                if (this.A01[i] == A05) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object remove(int i) {
        A02();
        A04(i);
        int[] iArr = this.A01;
        int i2 = iArr[i];
        AnonymousClass5I0.A01(iArr, this.A00, i);
        this.A00--;
        ((AbstractList) this).modCount++;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractList
    public final void removeRange(int i, int i2) {
        A02();
        if (i2 >= i) {
            int[] iArr = this.A01;
            System.arraycopy(iArr, i2, iArr, i, this.A00 - i2);
            this.A00 -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @Override // java.util.AbstractList, java.util.List
    public final /* synthetic */ Object set(int i, Object obj) {
        int A05 = C12960it.A05(obj);
        A02();
        A04(i);
        int[] iArr = this.A01;
        int i2 = iArr[i];
        iArr[i] = A05;
        return Integer.valueOf(i2);
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public final int size() {
        return this.A00;
    }
}
