package X;

import com.whatsapp.jid.UserJid;

/* renamed from: X.3z9  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84343z9 extends AnonymousClass2Cu {
    public final /* synthetic */ C53852fO A00;

    public C84343z9(C53852fO r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        C53852fO r1 = this.A00;
        if (r1.A05.equals(userJid)) {
            r1.A05();
        }
    }
}
