package X;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;

/* renamed from: X.0Q2  reason: invalid class name */
/* loaded from: classes.dex */
public abstract class AnonymousClass0Q2 {
    public BroadcastReceiver A00;
    public final /* synthetic */ LayoutInflater$Factory2C011505o A01;

    public abstract int A00();

    public abstract IntentFilter A01();

    public abstract void A04();

    public AnonymousClass0Q2(LayoutInflater$Factory2C011505o r1) {
        this.A01 = r1;
    }

    public void A02() {
        BroadcastReceiver broadcastReceiver = this.A00;
        if (broadcastReceiver != null) {
            try {
                this.A01.A0k.unregisterReceiver(broadcastReceiver);
            } catch (IllegalArgumentException unused) {
            }
            this.A00 = null;
        }
    }

    public void A03() {
        A02();
        IntentFilter A01 = A01();
        if (A01 != null && A01.countActions() != 0) {
            BroadcastReceiver broadcastReceiver = this.A00;
            if (broadcastReceiver == null) {
                broadcastReceiver = new C019309c(this);
                this.A00 = broadcastReceiver;
            }
            this.A01.A0k.registerReceiver(broadcastReceiver, A01);
        }
    }
}
