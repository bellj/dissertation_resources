package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape3S0200000_I0_3;

/* renamed from: X.3UR  reason: invalid class name */
/* loaded from: classes2.dex */
public final /* synthetic */ class AnonymousClass3UR implements AbstractC49122Jj {
    public final /* synthetic */ long A00;
    public final /* synthetic */ C14310lE A01;

    public /* synthetic */ AnonymousClass3UR(C14310lE r1, long j) {
        this.A01 = r1;
        this.A00 = j;
    }

    @Override // X.AbstractC49122Jj
    public final void ATN(C14320lF r8, boolean z) {
        C14310lE r6 = this.A01;
        long elapsedRealtime = 1000 - (SystemClock.elapsedRealtime() - this.A00);
        if (elapsedRealtime < 0) {
            elapsedRealtime = 0;
        }
        r6.A0D.A0J(new RunnableBRunnable0Shape3S0200000_I0_3(r6, 14, r8), elapsedRealtime);
    }
}
