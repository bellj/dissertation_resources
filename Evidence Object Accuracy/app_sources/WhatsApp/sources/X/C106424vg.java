package X;

import android.os.Looper;
import android.util.SparseArray;
import android.view.Surface;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.redex.IDxEventShape18S0100000_2_I1;
import com.facebook.redex.IDxEventShape6S0200000_2_I1;
import com.google.android.exoplayer2.Timeline;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

/* renamed from: X.4vg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C106424vg implements AnonymousClass5XZ, AbstractC72373eU, AnonymousClass5XS, AbstractC28711Os, AnonymousClass5Q3, AnonymousClass5QM {
    public AbstractC47512Az A00;
    public C92874Xt A01;
    public boolean A02;
    public final SparseArray A03;
    public final AnonymousClass4YJ A04;
    public final C94404bl A05;
    public final AnonymousClass3HD A06;
    public final AnonymousClass5Xd A07;

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AQ4(boolean z) {
    }

    @Override // X.AnonymousClass5XZ
    public /* synthetic */ void AXW(Timeline timeline, Object obj, int i) {
    }

    public C106424vg(AnonymousClass5Xd r7) {
        this.A07 = r7;
        Looper myLooper = Looper.myLooper();
        this.A01 = new C92874Xt(myLooper == null ? Looper.getMainLooper() : myLooper, r7, new AnonymousClass5SV() { // from class: X.4yS
            @Override // X.AnonymousClass5SV
            public final void AJ8(AnonymousClass4WQ r1, Object obj) {
            }
        }, new AbstractC115805Sz() { // from class: X.51P
            @Override // X.AbstractC115805Sz
            public final Object get() {
                return new C77393nD();
            }
        }, new CopyOnWriteArraySet());
        AnonymousClass4YJ r1 = new AnonymousClass4YJ();
        this.A04 = r1;
        this.A05 = new C94404bl();
        this.A06 = new AnonymousClass3HD(r1);
        this.A03 = new SparseArray();
    }

    public static AnonymousClass4XT A00(C106424vg r1) {
        return r1.A03(r1.A06.A02);
    }

    public static AnonymousClass4XT A01(C106424vg r1) {
        return r1.A03(r1.A06.A00);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if (r21 != r18.A00.ACF()) goto L_0x002c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final X.AnonymousClass4XT A02(com.google.android.exoplayer2.Timeline r19, X.C28741Ov r20, int r21) {
        /*
            r18 = this;
            r6 = r20
            r4 = r19
            int r0 = r4.A01()
            boolean r3 = X.C12960it.A1T(r0)
            if (r3 == 0) goto L_0x000f
            r6 = 0
        L_0x000f:
            long r10 = android.os.SystemClock.elapsedRealtime()
            r1 = r18
            X.2Az r0 = r1.A00
            com.google.android.exoplayer2.Timeline r0 = r0.ACE()
            boolean r0 = r4.equals(r0)
            r8 = r21
            if (r0 == 0) goto L_0x002c
            X.2Az r0 = r1.A00
            int r0 = r0.ACF()
            r2 = 1
            if (r8 == r0) goto L_0x002d
        L_0x002c:
            r2 = 0
        L_0x002d:
            r12 = 0
            if (r6 == 0) goto L_0x0075
            boolean r0 = r6.A00()
            if (r0 == 0) goto L_0x0075
            if (r2 == 0) goto L_0x0053
            X.2Az r0 = r1.A00
            int r2 = r0.AC2()
            int r0 = r6.A00
            if (r2 != r0) goto L_0x0053
            X.2Az r0 = r1.A00
            int r2 = r0.AC3()
            int r0 = r6.A01
            if (r2 != r0) goto L_0x0053
            X.2Az r0 = r1.A00
            long r12 = r0.AC9()
        L_0x0053:
            X.3HD r0 = r1.A06
            X.1Ov r7 = r0.A00
            X.2Az r0 = r1.A00
            com.google.android.exoplayer2.Timeline r5 = r0.ACE()
            X.2Az r0 = r1.A00
            int r9 = r0.ACF()
            X.2Az r0 = r1.A00
            long r14 = r0.AC9()
            X.2Az r0 = r1.A00
            long r16 = r0.AHF()
            X.4XT r3 = new X.4XT
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r12, r14, r16)
            return r3
        L_0x0075:
            if (r2 == 0) goto L_0x007e
            X.2Az r0 = r1.A00
            long r12 = r0.ABh()
            goto L_0x0053
        L_0x007e:
            if (r3 != 0) goto L_0x0053
            X.4bl r0 = r1.A05
            r4.A0B(r0, r8, r12)
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C106424vg.A02(com.google.android.exoplayer2.Timeline, X.1Ov, int):X.4XT");
    }

    public final AnonymousClass4XT A03(C28741Ov r5) {
        Timeline timeline;
        if (r5 != null && (timeline = (Timeline) this.A06.A04.get(r5)) != null) {
            return A02(timeline, r5, AnonymousClass4YJ.A00(this.A04, timeline, r5.A04));
        }
        int ACF = this.A00.ACF();
        Timeline ACE = this.A00.ACE();
        if (ACF >= ACE.A01()) {
            ACE = Timeline.A00;
        }
        return A02(ACE, null, ACF);
    }

    public final AnonymousClass4XT A04(C28741Ov r3, int i) {
        AbstractC47512Az r0 = this.A00;
        if (r3 == null) {
            Timeline ACE = r0.ACE();
            if (i >= ACE.A01()) {
                ACE = Timeline.A00;
            }
            return A02(ACE, null, i);
        } else if (this.A06.A04.get(r3) != null) {
            return A03(r3);
        } else {
            return A02(Timeline.A00, r3, i);
        }
    }

    public final void A05(AnonymousClass4XT r2, AnonymousClass5SU r3, int i) {
        this.A03.put(i, r2);
        C92874Xt r0 = this.A01;
        r0.A02(r3, i);
        r0.A00();
    }

    @Override // X.AbstractC72373eU
    public final void AMR(String str, long j, long j2) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 22), 1009);
    }

    @Override // X.AbstractC72373eU
    public final void AMS(String str) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 19), 1013);
    }

    @Override // X.AbstractC72373eU
    public final void AMT(AnonymousClass4U3 r4) {
        AnonymousClass4XT A03 = A03(this.A06.A01);
        A05(A03, new IDxEventShape6S0200000_2_I1(r4, 8, A03), 1014);
    }

    @Override // X.AbstractC72373eU
    public final void AMU(AnonymousClass4U3 r4) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape6S0200000_2_I1(r4, 7, A00), 1008);
    }

    @Override // X.AbstractC72373eU
    public final void AMV(C100614mC r4, AnonymousClass4XN r5) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new AnonymousClass5SU(r4, A00, r5) { // from class: X.4yM
            public final /* synthetic */ C100614mC A00;
            public final /* synthetic */ AnonymousClass4XT A01;
            public final /* synthetic */ AnonymousClass4XN A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 1010);
    }

    @Override // X.AbstractC72373eU
    public final void AMX(long j) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 17), 1011);
    }

    @Override // X.AbstractC72373eU
    public final void AMY(Exception exc) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape6S0200000_2_I1(exc, 12, A00), 1018);
    }

    @Override // X.AbstractC72373eU
    public final void AMZ(int i, long j, long j2) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 16), 1012);
    }

    @Override // X.AbstractC28711Os
    public final void APT(C28731Ou r4, C28741Ov r5, int i) {
        AnonymousClass4XT A04 = A04(r5, i);
        A05(A04, new IDxEventShape6S0200000_2_I1(r4, 11, A04), 1004);
    }

    @Override // X.AnonymousClass5XS
    public final void APX(int i, long j) {
        AnonymousClass4XT A03 = A03(this.A06.A01);
        A05(A03, new IDxEventShape18S0100000_2_I1(A03, 14), 1023);
    }

    @Override // X.AnonymousClass5XZ
    public final void ARX(boolean z) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 23), 4);
    }

    @Override // X.AnonymousClass5XZ
    public void ARY(boolean z) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 25), 8);
    }

    @Override // X.AbstractC28711Os
    public final void ARu(C28721Ot r4, C28731Ou r5, C28741Ov r6, int i) {
        AnonymousClass4XT A04 = A04(r6, i);
        A05(A04, new AnonymousClass5SU(r4, r5) { // from class: X.4yO
            public final /* synthetic */ C28721Ot A01;
            public final /* synthetic */ C28731Ou A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 1002);
    }

    @Override // X.AbstractC28711Os
    public final void ARv(C28721Ot r4, C28731Ou r5, C28741Ov r6, int i) {
        AnonymousClass4XT A04 = A04(r6, i);
        A05(A04, new AnonymousClass5SU(r4, r5) { // from class: X.4yN
            public final /* synthetic */ C28721Ot A01;
            public final /* synthetic */ C28731Ou A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 1001);
    }

    @Override // X.AbstractC28711Os
    public final void ARy(C28721Ot r4, C28731Ou r5, C28741Ov r6, IOException iOException, int i, boolean z) {
        AnonymousClass4XT A04 = A04(r6, i);
        A05(A04, new AnonymousClass5SU(r4, r5, iOException) { // from class: X.4yR
            public final /* synthetic */ C28721Ot A01;
            public final /* synthetic */ C28731Ou A02;
            public final /* synthetic */ IOException A03;

            {
                this.A01 = r2;
                this.A02 = r3;
                this.A03 = r4;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 1003);
    }

    @Override // X.AbstractC28711Os
    public final void AS4(C28721Ot r4, C28731Ou r5, C28741Ov r6, int i) {
        AnonymousClass4XT A04 = A04(r6, i);
        A05(A04, new AnonymousClass5SU(r4, r5) { // from class: X.4yP
            public final /* synthetic */ C28721Ot A01;
            public final /* synthetic */ C28731Ou A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 1000);
    }

    @Override // X.AnonymousClass5XZ
    public final void ASS(AnonymousClass4XL r4, int i) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape6S0200000_2_I1(r4, 3, A01), 1);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATm(boolean z, int i) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 27), 6);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATo(C94344be r4) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape6S0200000_2_I1(r4, 4, A01), 13);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATq(int i) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 11), 5);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATr(int i) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 29), 7);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATs(AnonymousClass3A1 r5) {
        C28741Ov r0;
        C28751Ow r1 = r5.mediaPeriodId;
        if (r1 != null) {
            r0 = new C28741Ov(r1);
        } else {
            r0 = this.A06.A00;
        }
        AnonymousClass4XT A03 = A03(r0);
        A05(A03, new IDxEventShape6S0200000_2_I1(r5, 2, A03), 11);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATt(boolean z, int i) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 26), -1);
    }

    @Override // X.AnonymousClass5XZ
    public final void ATx(int i) {
        if (i == 1) {
            this.A02 = false;
        }
        AnonymousClass3HD r4 = this.A06;
        AbstractC47512Az r3 = this.A00;
        AnonymousClass1Mr r2 = r4.A03;
        C28741Ov A00 = AnonymousClass3HD.A00(r3, r4.A05, r4.A01, r2);
        r4.A00 = A00;
        AnonymousClass4XT A03 = A03(A00);
        A05(A03, new IDxEventShape18S0100000_2_I1(A03, 12), 12);
    }

    @Override // X.AnonymousClass5XS
    public final void AUw(Surface surface) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape6S0200000_2_I1(surface, 1, A00), 1027);
    }

    @Override // X.AnonymousClass5XZ
    public final void AVk() {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape18S0100000_2_I1(A01, 28), -1);
    }

    @Override // X.AbstractC72373eU
    public final void AW9(boolean z) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 24), 1017);
    }

    @Override // X.AnonymousClass5XZ
    public final void AWU(List list) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new IDxEventShape6S0200000_2_I1(list, 13, A01), 3);
    }

    @Override // X.AnonymousClass5XZ
    public final void AXV(Timeline timeline, int i) {
        AnonymousClass3HD r4 = this.A06;
        AbstractC47512Az r3 = this.A00;
        AnonymousClass1Mr r2 = r4.A03;
        r4.A00 = AnonymousClass3HD.A00(r3, r4.A05, r4.A01, r2);
        r4.A01(r3.ACE());
        AnonymousClass4XT A03 = A03(r4.A00);
        A05(A03, new IDxEventShape18S0100000_2_I1(A03, 10), 0);
    }

    @Override // X.AnonymousClass5XZ
    public final void AXl(C100564m7 r4, C92524Wg r5) {
        AnonymousClass4XT A01 = A01(this);
        A05(A01, new AnonymousClass5SU(r4, r5) { // from class: X.4yQ
            public final /* synthetic */ C100564m7 A01;
            public final /* synthetic */ C92524Wg A02;

            {
                this.A01 = r2;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 2);
    }

    @Override // X.AnonymousClass5XS
    public final void AYE(String str, long j, long j2) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 21), 1021);
    }

    @Override // X.AnonymousClass5XS
    public final void AYF(String str) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 20), EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH);
    }

    @Override // X.AnonymousClass5XS
    public final void AYG(AnonymousClass4U3 r4) {
        AnonymousClass4XT A03 = A03(this.A06.A01);
        A05(A03, new IDxEventShape6S0200000_2_I1(r4, 6, A03), 1025);
    }

    @Override // X.AnonymousClass5XS
    public final void AYH(AnonymousClass4U3 r4) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape6S0200000_2_I1(r4, 9, A00), 1020);
    }

    @Override // X.AnonymousClass5XS
    public final void AYI(long j, int i) {
        AnonymousClass4XT A03 = A03(this.A06.A01);
        A05(A03, new IDxEventShape18S0100000_2_I1(A03, 18), 1026);
    }

    @Override // X.AnonymousClass5XS
    public final void AYJ(C100614mC r4, AnonymousClass4XN r5) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new AnonymousClass5SU(r4, A00, r5) { // from class: X.4yL
            public final /* synthetic */ C100614mC A00;
            public final /* synthetic */ AnonymousClass4XT A01;
            public final /* synthetic */ AnonymousClass4XN A02;

            {
                this.A01 = r2;
                this.A00 = r1;
                this.A02 = r3;
            }

            @Override // X.AnonymousClass5SU
            public final void AJ7(Object obj) {
            }
        }, 1022);
    }

    @Override // X.AnonymousClass5XS
    public final void AYK(float f, int i, int i2, int i3) {
        AnonymousClass4XT A00 = A00(this);
        A05(A00, new IDxEventShape18S0100000_2_I1(A00, 13), 1028);
    }
}
