package X;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.TextEmojiLabel;
import com.whatsapp.WaTextView;

/* renamed from: X.5m1  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass5m1 extends AbstractC118835cS {
    public final Context A00;
    public final TextView A01;
    public final TextView A02;
    public final TextView A03;
    public final TextView A04;
    public final TextEmojiLabel A05;
    public final WaTextView A06;
    public final AnonymousClass01d A07;

    public AnonymousClass5m1(View view, AnonymousClass01d r3) {
        super(view);
        this.A07 = r3;
        this.A00 = view.getContext();
        this.A06 = C12960it.A0N(view, R.id.status_icon);
        this.A03 = C12960it.A0I(view, R.id.transaction_status);
        this.A04 = C12960it.A0I(view, R.id.transaction_time);
        this.A05 = C12970iu.A0T(view, R.id.status_error_text);
        this.A02 = C12960it.A0I(view, R.id.status_tertiary_text);
        this.A01 = C12960it.A0I(view, R.id.status_action_button);
    }

    @Override // X.AbstractC118835cS
    public void A08(AbstractC125975s7 r8, int i) {
        CharSequence charSequence;
        C123325my r82 = (C123325my) r8;
        WaTextView waTextView = this.A06;
        Drawable background = waTextView.getBackground();
        Context context = this.A00;
        background.setColorFilter(context.getResources().getColor(r82.A01), PorterDuff.Mode.SRC_IN);
        waTextView.setText(r82.A03);
        waTextView.setContentDescription(r82.A04);
        float f = r82.A00;
        if (f != 0.0f) {
            waTextView.setTextSize(f);
        }
        boolean isEmpty = TextUtils.isEmpty(r82.A05);
        TextView textView = this.A03;
        if (!isEmpty) {
            textView.setTypeface(C27531Hw.A03(context));
            textView.setText(r82.A05);
            C12980iv.A14(context.getResources(), textView, r82.A01);
            if (!TextUtils.isEmpty(r82.A07)) {
                this.A04.setText(r82.A07);
            }
        } else {
            textView.setTypeface(Typeface.create(Typeface.SANS_SERIF, 0));
            textView.setText(r82.A06);
            C12960it.A0s(context, textView, R.color.settings_item_subtitle_text);
            this.A04.setText("");
        }
        boolean isEmpty2 = TextUtils.isEmpty(r82.A0B);
        TextEmojiLabel textEmojiLabel = this.A05;
        if (!isEmpty2) {
            AbstractC28491Nn.A03(textEmojiLabel);
            AbstractC28491Nn.A04(textEmojiLabel, this.A07);
            charSequence = AnonymousClass23M.A07(null, r82.A0B, r82.A0C, context.getResources().getColor(R.color.link_color));
        } else {
            charSequence = r82.A0A;
        }
        textEmojiLabel.setText(charSequence);
        boolean isEmpty3 = TextUtils.isEmpty(r82.A09);
        TextView textView2 = this.A02;
        if (!isEmpty3) {
            textView2.setText(r82.A09);
            textView2.setVisibility(0);
        } else {
            textView2.setVisibility(8);
        }
        if (!TextUtils.isEmpty(r82.A08)) {
            TextView textView3 = this.A01;
            textView3.setText(r82.A08);
            textView3.setVisibility(0);
            textView3.setOnClickListener(r82.A02);
            return;
        }
        this.A01.setVisibility(8);
    }
}
