package X;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.view.View;

/* renamed from: X.0Ic  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03520Ic extends AnonymousClass2k7 {
    public C03520Ic(C14260l7 r1, AnonymousClass28D r2) {
        super(r1, r2);
    }

    @Override // X.AnonymousClass2k7
    public void A06(View view, C14260l7 r16, AnonymousClass28D r17, Object obj) {
        float f;
        AnonymousClass0NC r7;
        AnonymousClass0NC r8;
        AnonymousClass0J7 r9;
        C02330Bg r15 = (C02330Bg) view;
        int A0B = r17.A0B(40, 0);
        AnonymousClass28D A0F = r17.A0F(35);
        String A0J = r17.A0J(36, "4.0dp");
        AnonymousClass28D A0F2 = r17.A0F(41);
        AnonymousClass28D A0F3 = r17.A0F(38);
        String A0J2 = r17.A0J(42, "rectangle");
        int A00 = A0F != null ? AnonymousClass4Di.A00(r16, A0F) : -1;
        try {
            f = AnonymousClass3JW.A01(A0J);
        } catch (AnonymousClass491 e) {
            C28691Op.A02("CDSGlimmerViewUtils", e);
            f = AnonymousClass0LQ.A00(r16.A00(), 4.0f);
        }
        if (A0F2 != null) {
            r7 = new AnonymousClass0NC(A0F2.A09(36, 0.3f), A0F2.A09(35, 0.5f));
        } else {
            r7 = new AnonymousClass0NC(0.3f, 0.5f);
        }
        if (A0F3 != null) {
            r8 = new AnonymousClass0NC(A0F3.A09(36, 0.066f), A0F3.A09(35, 0.11f));
        } else {
            r8 = new AnonymousClass0NC(0.066f, 0.11f);
        }
        if ("circle".equalsIgnoreCase(A0J2)) {
            r9 = AnonymousClass0J7.CIRCLE;
        } else {
            r9 = AnonymousClass0J7.RECTANGLE;
        }
        AnonymousClass0A9 r5 = new AnonymousClass0A9(r15.getContext(), r7, r8, r9, r16, f, A0B, A00);
        AnonymousClass0A9 r0 = null;
        if (r5 instanceof Animatable) {
            r0 = r5;
        }
        r15.A00 = r0;
        r15.setImageDrawable(r5);
        Animatable animatable = r15.A00;
        if (animatable != null) {
            animatable.start();
        }
        r15.A01 = true;
    }

    @Override // X.AnonymousClass2k7
    public void A07(View view, C14260l7 r3, AnonymousClass28D r4, Object obj) {
        C02330Bg r2 = (C02330Bg) view;
        Animatable animatable = r2.A00;
        if (animatable != null) {
            animatable.stop();
        }
        r2.A01 = false;
    }

    @Override // X.AnonymousClass2k7, X.AnonymousClass5SC
    public /* bridge */ /* synthetic */ Object A8B(Context context) {
        return new C02330Bg(context);
    }
}
