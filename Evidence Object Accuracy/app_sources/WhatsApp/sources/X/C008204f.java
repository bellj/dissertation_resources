package X;

/* renamed from: X.04f  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C008204f extends AnonymousClass030 {
    public final ThreadLocal A00 = new ThreadLocal();
    public final ThreadLocal A01 = new ThreadLocal();

    public static double A00(AnonymousClass033 r6) {
        double A00 = (((double) r6.A00()) * 1.0d) / ((double) AnonymousClass0RI.A00);
        r6.A04();
        return A00;
    }

    @Override // X.AnonymousClass030
    public AnonymousClass032 A01() {
        return new AnonymousClass04e();
    }

    @Override // X.AnonymousClass030
    public /* bridge */ /* synthetic */ boolean A02(AnonymousClass032 r6) {
        AnonymousClass04e r62 = (AnonymousClass04e) r6;
        if (r62 != null) {
            try {
                ThreadLocal threadLocal = this.A01;
                AnonymousClass033 r2 = (AnonymousClass033) threadLocal.get();
                if (r2 == null) {
                    r2 = new AnonymousClass033("/proc/self/stat");
                    threadLocal.set(r2);
                }
                r2.A02();
                if (!r2.A05) {
                    return false;
                }
                int i = 0;
                do {
                    r2.A04();
                    i++;
                } while (i < 13);
                r62.userTimeS = A00(r2);
                r62.systemTimeS = A00(r2);
                r62.childUserTimeS = A00(r2);
                r62.childSystemTimeS = A00(r2);
                ThreadLocal threadLocal2 = this.A00;
                if (threadLocal2.get() == null) {
                    threadLocal2.set(new AnonymousClass04e());
                }
                AnonymousClass04e r4 = (AnonymousClass04e) threadLocal2.get();
                if (Double.compare(r62.userTimeS, r4.userTimeS) < 0 || Double.compare(r62.systemTimeS, r4.systemTimeS) < 0 || Double.compare(r62.childUserTimeS, r4.childUserTimeS) < 0 || Double.compare(r62.childSystemTimeS, r4.childSystemTimeS) < 0) {
                    return false;
                }
                r4.A03(r62);
                return true;
            } catch (C10760fA unused) {
                return false;
            }
        } else {
            throw new IllegalArgumentException("Null value passed to getSnapshot!");
        }
    }
}
