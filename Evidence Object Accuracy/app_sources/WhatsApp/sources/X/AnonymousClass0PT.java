package X;

import android.text.TextUtils;
import android.util.LruCache;
import android.util.SparseArray;
import com.facebook.common.time.AwakeTimeSinceBootClock;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/* renamed from: X.0PT  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0PT {
    public final int A00;
    public final int A01;
    public final int A02;
    public final int A03;
    public final long A04;
    public final SparseArray A05;
    public final String A06;
    public final String A07;
    public final HashMap A08;
    public final List A09;
    public final Map A0A;
    public final Map A0B;

    public /* synthetic */ AnonymousClass0PT(C04870Nj r6) {
        int hashCode;
        String str = r6.A01;
        this.A06 = str;
        HashMap hashMap = r6.A02;
        this.A08 = hashMap == null ? new HashMap() : hashMap;
        this.A0B = new HashMap();
        int i = r6.A00;
        this.A02 = i <= 0 ? 719983200 : i;
        int i2 = 0;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        int hashCode2 = (hashCode * 31) + (hashMap != null ? hashMap.hashCode() : i2);
        if (TextUtils.isEmpty(null)) {
            synchronized (AnonymousClass0RH.class) {
                LruCache lruCache = AnonymousClass0RH.A00;
                Integer valueOf = Integer.valueOf(hashCode2);
                Integer num = (Integer) lruCache.get(valueOf);
                hashCode2 = num != null ? (num.intValue() + 1) * 31 : hashCode2;
                lruCache.put(valueOf, Integer.valueOf(hashCode2));
            }
            this.A01 = hashCode2;
            this.A07 = UUID.randomUUID().toString();
            this.A04 = AwakeTimeSinceBootClock.INSTANCE.now();
            this.A09 = r6.A03;
            this.A03 = -1;
            this.A00 = -1;
            this.A05 = new SparseArray();
            this.A0A = new HashMap();
            return;
        }
        throw new NullPointerException("hashCode");
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AnonymousClass0PT)) {
            return false;
        }
        AnonymousClass0PT r4 = (AnonymousClass0PT) obj;
        String str = this.A06;
        String str2 = r4.A06;
        if (str == null) {
            if (str2 != null) {
                return false;
            }
        } else if (!str.equals(str2)) {
            return false;
        }
        if (!this.A08.equals(r4.A08)) {
            return false;
        }
        if (!TextUtils.isEmpty(null)) {
            throw new NullPointerException("equals");
        } else if (TextUtils.isEmpty(null)) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        int hashCode;
        String str = this.A06;
        HashMap hashMap = this.A08;
        int i = 0;
        if (str == null) {
            hashCode = 0;
        } else {
            hashCode = str.hashCode();
        }
        int i2 = hashCode * 31;
        if (hashMap != null) {
            i = hashMap.hashCode();
        }
        int i3 = i2 + i;
        if (TextUtils.isEmpty(null)) {
            return i3;
        }
        throw new NullPointerException("hashCode");
    }
}
