package X;

import android.content.Context;
import com.whatsapp.R;

/* renamed from: X.3Bz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C63543Bz {
    public String A00;
    public String A01;
    public String A02;
    public String A03;
    public String A04;
    public String A05;
    public String A06;
    public String A07;
    public String A08;
    public String A09;
    public String A0A;
    public String A0B;
    public String A0C;
    public String A0D;
    public String A0E;
    public String A0F;
    public String A0G;

    public C63543Bz(Context context) {
        context.getString(R.string.group_sync_tap_retry);
        this.A04 = context.getString(R.string.group_created_failed);
        this.A05 = context.getString(R.string.group_creating);
        this.A06 = context.getString(R.string.group_invite);
        this.A0A = context.getString(R.string.parent_group_invite);
        this.A07 = context.getString(R.string.conversations_most_recent_image);
        this.A00 = context.getString(R.string.conversations_most_recent_audio);
        this.A0F = context.getString(R.string.conversations_most_recent_voice);
        this.A0D = context.getString(R.string.conversations_most_recent_video);
        this.A03 = context.getString(R.string.conversations_most_recent_gif);
        this.A0C = context.getString(R.string.conversations_most_recent_sticker);
        this.A09 = context.getString(R.string.conversations_most_recent_location);
        this.A08 = context.getString(R.string.conversations_most_recent_live_location);
        this.A01 = context.getString(R.string.conversations_most_recent_contact);
        context.getString(R.string.conversations_most_recent_contact_array);
        this.A02 = context.getString(R.string.conversations_most_recent_document);
        this.A0G = context.getString(R.string.group_subject_changed_you_pronoun);
        this.A0B = context.getString(R.string.attach_product);
        context.getString(R.string.conversations_most_recent_cart);
        this.A0E = context.getString(R.string.view_once_opened);
        context.getString(R.string.view_once_expired);
    }
}
