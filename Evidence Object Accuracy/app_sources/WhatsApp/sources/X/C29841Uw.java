package X;

import java.util.Iterator;

/* renamed from: X.1Uw  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C29841Uw implements Iterable {
    public final int A00;
    public final Object[] A01;

    public C29841Uw(Object[] objArr, int i) {
        if (i >= 1) {
            this.A01 = objArr;
            this.A00 = i;
            return;
        }
        throw new IllegalArgumentException("Chunk size must be positive.");
    }

    @Override // java.lang.Iterable
    public Iterator iterator() {
        return new AnonymousClass5D5(this.A01, this.A00);
    }
}
