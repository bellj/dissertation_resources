package X;

/* renamed from: X.4Xv  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C92894Xv {
    public int A00;
    public int A01 = 1;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public int A06;
    public int A07;
    public int A08;
    public int A09;
    public int A0A;
    public int A0B;
    public int A0C;
    public int A0D;
    public C95474dn A0E;
    public C95474dn A0F;
    public C95474dn A0G;
    public C95474dn A0H;
    public C94914ck A0I;
    public C95044cz A0J;
    public C95044cz A0K;
    public C95044cz A0L;
    public C95044cz A0M;
    public AnonymousClass4U9 A0N;
    public AnonymousClass4U9 A0O;
    public AnonymousClass5M6 A0P;
    public AnonymousClass5M6 A0Q;
    public AnonymousClass4UH A0R;
    public AnonymousClass4U2 A0S;
    public AnonymousClass4U2 A0T;
    public int[] A0U;
    public final AnonymousClass4YW A0V = new AnonymousClass4YW(this);

    public static void A00(String str, C95044cz r1, AnonymousClass4YW r2, int i) {
        r1.A04(r2.A0A(str, i).A03);
    }

    public final AnonymousClass4YT A01(String str, String str2, String str3, String[] strArr, int i) {
        AnonymousClass5M6 r1 = new AnonymousClass5M6(str, str2, str3, this.A0V, strArr, i, this.A01);
        if (this.A0P == null) {
            this.A0P = r1;
        } else {
            ((AnonymousClass4YT) this.A0Q).A00 = r1;
        }
        this.A0Q = r1;
        return r1;
    }

    public final void A02(String str, String str2, String str3, String[] strArr, int i, int i2) {
        int i3;
        int length;
        this.A0D = i;
        this.A00 = i2;
        AnonymousClass4YW r3 = this.A0V;
        int i4 = i & 65535;
        r3.A03 = i4;
        r3.A05 = str;
        this.A0C = r3.A0A(str, 7).A03;
        if (str2 != null) {
            this.A09 = r3.A02(str2);
        }
        if (str3 == null) {
            i3 = 0;
        } else {
            i3 = r3.A0A(str3, 7).A03;
        }
        this.A0B = i3;
        if (strArr != null && (length = strArr.length) > 0) {
            this.A04 = length;
            this.A0U = new int[length];
            for (int i5 = 0; i5 < this.A04; i5++) {
                this.A0U[i5] = r3.A0A(strArr[i5], 7).A03;
            }
        }
        if (this.A01 == 1 && i4 >= 51) {
            this.A01 = 2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 1540
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:66)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public byte[] A03() {
        /*
        // Method dump skipped, instructions count: 9076
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C92894Xv.A03():byte[]");
    }
}
