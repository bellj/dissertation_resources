package X;

/* renamed from: X.4ux  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C105994ux implements AnonymousClass5WT {
    public final AnonymousClass3HI A00;

    public C105994ux(AnonymousClass3HI r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass5WT
    public int AD9(int i) {
        return this.A00.A08[i];
    }

    @Override // X.AnonymousClass5WT
    public int getFrameCount() {
        return this.A00.A04.getFrameCount();
    }

    @Override // X.AnonymousClass5WT
    public int getLoopCount() {
        return this.A00.A04.getLoopCount();
    }
}
