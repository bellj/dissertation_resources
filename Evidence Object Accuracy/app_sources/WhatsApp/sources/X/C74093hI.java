package X;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import com.whatsapp.R;
import com.whatsapp.catalogcategory.view.CategoryThumbnailLoader;
import java.util.List;
import java.util.Map;

/* renamed from: X.3hI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74093hI extends BaseExpandableListAdapter {
    public List A00 = AnonymousClass1WF.A00;
    public Map A01 = C71373cp.A00;
    public final CategoryThumbnailLoader A02;

    @Override // android.widget.ExpandableListAdapter
    public long getChildId(int i, int i2) {
        return (long) ((i * 1000) + i2);
    }

    @Override // android.widget.BaseExpandableListAdapter, android.widget.HeterogeneousExpandableList
    public int getChildTypeCount() {
        return 5;
    }

    @Override // android.widget.ExpandableListAdapter
    public long getGroupId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseExpandableListAdapter, android.widget.HeterogeneousExpandableList
    public int getGroupTypeCount() {
        return 5;
    }

    @Override // android.widget.ExpandableListAdapter
    public boolean hasStableIds() {
        return true;
    }

    @Override // android.widget.ExpandableListAdapter
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public C74093hI(CategoryThumbnailLoader categoryThumbnailLoader) {
        C16700pc.A0E(categoryThumbnailLoader, 1);
        this.A02 = categoryThumbnailLoader;
    }

    public static final View A00(View view, ViewGroup viewGroup, AnonymousClass1J7 r3, int i) {
        if (view != null) {
            return view;
        }
        View inflate = C12960it.A0E(viewGroup).inflate(i, viewGroup, false);
        C16700pc.A0B(inflate);
        inflate.setTag(r3.AJ4(inflate));
        return inflate;
    }

    /* renamed from: A01 */
    public AnonymousClass4K7 getChild(int i, int i2) {
        AnonymousClass4K7 r2 = (AnonymousClass4K7) this.A00.get(i);
        if (r2 instanceof AnonymousClass2wS) {
            Map map = this.A01;
            String str = ((AnonymousClass2wS) r2).A00.A01;
            C16700pc.A0B(str);
            return (AnonymousClass4K7) ((List) C17530qx.A00(map, str)).get(i2);
        }
        throw C12960it.A0U("Unhandled category parent type in getChild()");
    }

    @Override // android.widget.BaseExpandableListAdapter, android.widget.HeterogeneousExpandableList
    public int getChildType(int i, int i2) {
        return getChild(i, i2).A00;
    }

    @Override // android.widget.ExpandableListAdapter
    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        Object obj;
        C16700pc.A0E(viewGroup, 4);
        AnonymousClass4K7 A01 = getChild(i, i2);
        if (A01 instanceof AnonymousClass2wR) {
            view2 = A00(view, viewGroup, new AnonymousClass5K7(this), R.layout.list_item_category_group_child);
            obj = view2.getTag();
            if (obj == null) {
                throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.adapter.viewholder.CatalogCategoryViewHolder");
            }
        } else if (A01 instanceof C849640p) {
            view2 = A00(view, viewGroup, new C114005Jv(), R.layout.list_item_category_child_shimmer);
            obj = view2.getTag();
            if (obj == null) {
                throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.adapter.viewholder.CatalogCategoryViewHolder");
            }
        } else {
            throw C12960it.A0U("Unhandled group-child type in getChildView()");
        }
        ((AbstractC75663kD) obj).A08(A01);
        return view2;
    }

    @Override // android.widget.ExpandableListAdapter
    public int getChildrenCount(int i) {
        AnonymousClass4K7 r2 = (AnonymousClass4K7) this.A00.get(i);
        if (!(r2 instanceof AnonymousClass2wS)) {
            return 0;
        }
        Map map = this.A01;
        String str = ((AnonymousClass2wS) r2).A00.A01;
        C16700pc.A0B(str);
        return C72453ed.A0C(C17530qx.A00(map, str));
    }

    @Override // android.widget.ExpandableListAdapter
    public /* bridge */ /* synthetic */ Object getGroup(int i) {
        return this.A00.get(i);
    }

    @Override // android.widget.ExpandableListAdapter
    public int getGroupCount() {
        return this.A00.size();
    }

    @Override // android.widget.BaseExpandableListAdapter, android.widget.HeterogeneousExpandableList
    public int getGroupType(int i) {
        return ((AnonymousClass4K7) this.A00.get(i)).A00;
    }

    @Override // android.widget.ExpandableListAdapter
    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        Object obj;
        int i2;
        C16700pc.A0E(viewGroup, 3);
        AnonymousClass4K7 r4 = (AnonymousClass4K7) this.A00.get(i);
        if (r4 instanceof AnonymousClass2wS) {
            View A00 = A00(view, viewGroup, new AnonymousClass5K8(this), R.layout.list_item_catalog_category_expandable);
            Object tag = A00.getTag();
            if (tag != null) {
                C850240v r0 = (C850240v) tag;
                r0.A08(r4);
                ImageView imageView = (ImageView) r0.A02.getValue();
                if (z) {
                    i2 = R.drawable.ic_catalog_expand_less;
                } else {
                    i2 = R.drawable.ic_catalog_expand_more;
                }
                imageView.setImageResource(i2);
                return A00;
            }
            throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.adapter.viewholder.CategoryGroupExpandableItemViewHolder");
        }
        if (r4 instanceof AnonymousClass2wR) {
            view2 = A00(view, viewGroup, new AnonymousClass5K9(this), R.layout.list_item_catalog_category);
            obj = view2.getTag();
            if (obj == null) {
                throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.adapter.viewholder.CatalogCategoryViewHolder");
            }
        } else if (r4 instanceof C849740q) {
            view2 = A00(view, viewGroup, new C114015Jw(), R.layout.list_item_shimmer_category);
            obj = view2.getTag();
            if (obj == null) {
                throw C12980iv.A0n("null cannot be cast to non-null type com.whatsapp.catalogcategory.view.adapter.viewholder.CatalogCategoryViewHolder");
            }
        } else {
            throw C12960it.A0U("Unhandled group type in getGroupView()");
        }
        ((AbstractC75663kD) obj).A08(r4);
        return view2;
    }
}
