package X;

import java.text.Format;
import java.text.SimpleDateFormat;

/* renamed from: X.55z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1105555z implements AbstractC51202Tg {
    @Override // X.AbstractC51202Tg
    public Format AD6(AnonymousClass018 r4) {
        try {
            return new SimpleDateFormat("LLLL yyyy", AnonymousClass018.A00(r4.A00));
        } catch (IllegalArgumentException unused) {
            return new SimpleDateFormat("MMMM yyyy", AnonymousClass018.A00(r4.A00));
        }
    }
}
