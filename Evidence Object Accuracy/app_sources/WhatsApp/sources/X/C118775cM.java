package X;

import android.view.View;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.WaImageView;

/* renamed from: X.5cM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C118775cM extends AnonymousClass03U {
    public final TextView A00;
    public final TextView A01;
    public final WaImageView A02;
    public final /* synthetic */ C118545bz A03;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C118775cM(View view, C118545bz r3) {
        super(view);
        this.A03 = r3;
        this.A02 = (WaImageView) view.findViewById(R.id.service_row_icon);
        this.A01 = C12960it.A0J(view, R.id.service_row_title);
        this.A00 = C12960it.A0J(view, R.id.service_row_description);
    }
}
