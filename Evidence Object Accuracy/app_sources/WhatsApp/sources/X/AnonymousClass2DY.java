package X;

import com.whatsapp.jobqueue.job.GetStatusPrivacyJob;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: X.2DY  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2DY {
    public final /* synthetic */ GetStatusPrivacyJob A00;
    public final /* synthetic */ AtomicInteger A01;

    public AnonymousClass2DY(GetStatusPrivacyJob getStatusPrivacyJob, AtomicInteger atomicInteger) {
        this.A00 = getStatusPrivacyJob;
        this.A01 = atomicInteger;
    }
}
