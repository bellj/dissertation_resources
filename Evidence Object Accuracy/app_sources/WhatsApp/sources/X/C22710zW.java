package X;

import java.util.Set;

/* renamed from: X.0zW  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C22710zW {
    public final C15450nH A00;
    public final C15550nR A01;
    public final C14830m7 A02;
    public final C14850m9 A03;
    public final C21860y6 A04;
    public final C18600si A05;
    public final C17900ra A06;
    public final AnonymousClass162 A07;

    public C22710zW(C15450nH r1, C15550nR r2, C14830m7 r3, C14850m9 r4, C21860y6 r5, C18600si r6, C17900ra r7, AnonymousClass162 r8) {
        this.A02 = r3;
        this.A03 = r4;
        this.A00 = r1;
        this.A01 = r2;
        this.A05 = r6;
        this.A04 = r5;
        this.A06 = r7;
        this.A07 = r8;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r1 == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A00(com.whatsapp.jid.UserJid r4) {
        /*
            r3 = this;
            java.lang.String r2 = X.C248917h.A04(r4)
            if (r4 == 0) goto L_0x0015
            X.0nR r0 = r3.A01
            X.0n3 r0 = r0.A0A(r4)
            if (r0 == 0) goto L_0x0015
            boolean r1 = r0.A0J()
            r0 = 1
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            int r0 = r3.A02(r2, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22710zW.A00(com.whatsapp.jid.UserJid):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0013, code lost:
        if (r1 == false) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int A01(java.lang.String r3) {
        /*
            r2 = this;
            com.whatsapp.jid.UserJid r1 = com.whatsapp.jid.UserJid.getNullable(r3)
            if (r1 == 0) goto L_0x0015
            X.0nR r0 = r2.A01
            X.0n3 r0 = r0.A0A(r1)
            if (r0 == 0) goto L_0x0015
            boolean r1 = r0.A0J()
            r0 = 1
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            int r0 = r2.A02(r3, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C22710zW.A01(java.lang.String):int");
    }

    public int A02(String str, boolean z) {
        Set<C17930rd> set = (Set) AnonymousClass3B4.A00.get(C17930rd.A01(AnonymousClass1ZT.A01(str)));
        C17900ra r4 = this.A06;
        C17930rd A01 = r4.A01();
        if (set == null || A01 == null) {
            return 1;
        }
        for (C17930rd r0 : set) {
            if (r0.A03.equals(A01.A03)) {
                if (A04() && z) {
                    return 1;
                }
                r4.A01();
                return 2;
            }
        }
        return 1;
    }

    public boolean A03() {
        if (A04()) {
            C18600si r6 = this.A05;
            if (r6.A01().getLong("payments_enabled_till", -1) != -1) {
                r6.A01().edit().remove("payments_enabled_till").apply();
            }
        } else {
            boolean A05 = this.A00.A05(AbstractC15460nI.A0p);
            if (A05) {
                C18600si r62 = this.A05;
                if (r62.A01().getLong("payments_enabled_till", -1) != -1) {
                    r62.A01().edit().remove("payments_enabled_till").apply();
                }
            }
            if (!this.A06.A04()) {
                return false;
            }
            if (A05 || this.A02.A00() < this.A05.A01().getLong("payments_enabled_till", -1)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public boolean A04() {
        return this.A00.A05(AbstractC15460nI.A0t);
    }

    public boolean A05() {
        return this.A00.A05(AbstractC15460nI.A0o) && this.A03.A07(1586);
    }

    public boolean A06() {
        if (A03()) {
            C17930rd r1 = C17930rd.A0D;
            C17900ra r2 = this.A06;
            if (r1 == r2.A01() && A08()) {
                return true;
            }
            if (C17930rd.A0E == r2.A01() && this.A03.A07(733)) {
                return true;
            }
            if (A04() && this.A03.A07(544)) {
                return true;
            }
        }
        C17930rd r12 = C17930rd.A0D;
        C17900ra r22 = this.A06;
        return (r12 == r22.A01() || C17930rd.A0E == r22.A01()) && this.A03.A07(888);
    }

    public boolean A07() {
        return A03();
    }

    public boolean A08() {
        return this.A06.A04() && this.A00.A05(AbstractC15460nI.A0o) && this.A03.A07(1158);
    }
}
