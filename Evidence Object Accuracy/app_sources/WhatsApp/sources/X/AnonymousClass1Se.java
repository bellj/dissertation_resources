package X;

import android.content.Context;
import android.util.Log;
import androidx.core.view.inputmethod.EditorInfoCompat;
import com.facebook.profilo.mmapbuf.core.Buffer;
import com.facebook.profilo.mmapbuf.core.MmapBufferManager;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/* renamed from: X.1Se  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass1Se implements AbstractC29391Sf {
    public static AtomicReference A0A = new AtomicReference(null);
    public AnonymousClass1Sd A00;
    public AnonymousClass1Sh A01;
    public MmapBufferManager A02;
    public AnonymousClass1SY[] A03;
    public AnonymousClass1SY[] A04;
    public final AnonymousClass1Sj A05;
    public final Object A06 = new Object();
    public final Random A07;
    public final boolean A08;
    public volatile AnonymousClass1Sg A09;

    public AnonymousClass1Se(Context context, AnonymousClass1Sd r7, File file, AnonymousClass1SY[] r9) {
        this.A00 = r7;
        this.A09 = null;
        this.A01 = new AnonymousClass1Sh(context, file);
        this.A07 = new Random();
        this.A05 = new AnonymousClass1Sj();
        this.A08 = true;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (AnonymousClass1SY r0 : r9) {
            arrayList2.add(r0);
        }
        this.A03 = (AnonymousClass1SY[]) arrayList2.toArray(new AnonymousClass1SY[arrayList2.size()]);
        this.A04 = (AnonymousClass1SY[]) arrayList.toArray(new AnonymousClass1SY[arrayList.size()]);
    }

    public static void A00(File file) {
        if (file.isDirectory()) {
            String[] list = file.list();
            if (list != null) {
                for (String str : list) {
                    File file2 = new File(file, str);
                    if (file2.isDirectory()) {
                        A00(file2);
                    } else {
                        file2.delete();
                    }
                }
            }
            file.delete();
        }
    }

    public static void A01(File file, String str, ZipOutputStream zipOutputStream) {
        File absoluteFile = new File(file, str).getAbsoluteFile();
        URI uri = file.toURI();
        for (String str2 : absoluteFile.list()) {
            File file2 = new File(absoluteFile, str2);
            if (file2.exists()) {
                String path = uri.relativize(file2.toURI()).getPath();
                if (file2.isFile()) {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(new File(file, path));
                        byte[] bArr = new byte[EditorInfoCompat.MAX_INITIAL_SELECTION_LENGTH];
                        zipOutputStream.putNextEntry(new ZipEntry(path));
                        while (true) {
                            int read = fileInputStream.read(bArr);
                            if (read <= 0) {
                                break;
                            }
                            zipOutputStream.write(bArr, 0, read);
                        }
                        fileInputStream.close();
                    } finally {
                        zipOutputStream.closeEntry();
                    }
                } else if (file2.isDirectory()) {
                    A01(file, path, zipOutputStream);
                }
            }
        }
    }

    public final void A02(C29441Sr r6) {
        for (Buffer buffer : r6.A0F) {
            if (!this.A02.deallocateBuffer(buffer)) {
                StringBuilder sb = new StringBuilder("Could not release memory for buffer for trace: ");
                sb.append(r6.A0D);
                Log.e("Profilo/TraceOrchestrator", sb.toString());
            }
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXh(C29441Sr r5, int i) {
        String str;
        try {
            this.A05.AXh(r5, i);
            StringBuilder sb = new StringBuilder();
            sb.append("Trace is aborted with code: ");
            switch (i) {
                case 1:
                    str = "unknown";
                    break;
                case 2:
                    str = "controller_init";
                    break;
                case 3:
                    str = "missed_event";
                    break;
                case 4:
                    str = "timeout";
                    break;
                case 5:
                    str = "new_start";
                    break;
                case 6:
                    str = "condition_not_met";
                    break;
                case 7:
                default:
                    StringBuilder sb2 = new StringBuilder("UNKNOWN REASON ");
                    sb2.append(i);
                    str = sb2.toString();
                    break;
                case 8:
                    str = "writer_exception";
                    break;
                case 9:
                    str = "logout";
                    break;
            }
            sb.append(str);
            Log.w("Profilo/TraceOrchestrator", sb.toString());
            AnonymousClass1ST r2 = AnonymousClass1ST.A0B;
            if (r2 != null) {
                r2.A04(r5.A06, i);
                if (this.A08) {
                    File file = r5.A0A;
                    if (file.exists()) {
                        synchronized (this) {
                        }
                        try {
                            A00(file);
                        } catch (Exception e) {
                            Log.e("Profilo/TraceOrchestrator", "failed to delete directory", e);
                        }
                    }
                }
                return;
            }
            throw new IllegalStateException("No TraceControl when cleaning up aborted trace");
        } finally {
            A02(r5);
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXi(C29441Sr r19) {
        File file;
        List asList;
        boolean z;
        List asList2;
        AnonymousClass4TN r7;
        try {
            AnonymousClass1Sj r6 = this.A05;
            r6.AXi(r19);
            File file2 = r19.A0A;
            if (file2.exists() && this.A08) {
                boolean z2 = true;
                if (!file2.isDirectory() || file2.list().length <= 1) {
                    z2 = false;
                }
                boolean z3 = false;
                if (z2) {
                    file = null;
                    if (file2.isDirectory()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(file2.getName());
                        sb.append(".zip.tmp");
                        File file3 = new File(file2.getParent(), sb.toString());
                        try {
                            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file3), 262144);
                            try {
                                ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
                                A01(file2, ".", zipOutputStream);
                                zipOutputStream.flush();
                                zipOutputStream.finish();
                                zipOutputStream.close();
                                bufferedOutputStream.close();
                                file = file3;
                            } catch (Throwable th) {
                                try {
                                    bufferedOutputStream.close();
                                } catch (Throwable unused) {
                                }
                                throw th;
                            }
                        } catch (IOException unused2) {
                            file3.delete();
                        }
                    }
                    String format = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss", Locale.US).format(new Date());
                    File parentFile = file.getParentFile();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(format);
                    sb2.append("-");
                    sb2.append(file.getName());
                    File file4 = new File(parentFile, sb2.toString());
                    if (file.renameTo(file4)) {
                        file = file4;
                    }
                    try {
                        A00(file2);
                    } catch (Exception e) {
                        Log.e("Profilo/TraceOrchestrator", "failed to delete directory", e);
                    }
                } else {
                    File[] listFiles = file2.listFiles();
                    if (listFiles != null) {
                        if (listFiles.length != 0) {
                            file = listFiles[0];
                            if (file == null) {
                            }
                        }
                    }
                }
                if (!r6.A72(r19, file)) {
                    Log.d("Profilo/TraceOrchestrator", "Not allowed to upload.");
                } else {
                    synchronized (this) {
                        if ((r19.A03 & 3) != 0) {
                            z3 = true;
                        }
                        AnonymousClass1Sh r3 = this.A01;
                        String name = file.getName();
                        int lastIndexOf = name.lastIndexOf(46);
                        if (lastIndexOf != -1) {
                            name = name.substring(0, lastIndexOf);
                        }
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(name);
                        sb3.append(".log");
                        String obj = sb3.toString();
                        if (z3) {
                            StringBuilder sb4 = new StringBuilder("override-");
                            sb4.append(obj);
                            obj = sb4.toString();
                        }
                        File file5 = r3.A05;
                        if (file5.isDirectory() || file5.mkdirs()) {
                            boolean renameTo = file.renameTo(new File(file5, obj));
                            AnonymousClass4TN r1 = r3.A02;
                            if (renameTo) {
                                r1.A00++;
                            } else {
                                r1.A03++;
                            }
                            File file6 = r3.A06;
                            long j = r3.A01;
                            FilenameFilter filenameFilter = AnonymousClass1Sh.A07;
                            FilenameFilter filenameFilter2 = AnonymousClass1Sh.A08;
                            FilenameFilter[] filenameFilterArr = {filenameFilter, filenameFilter2};
                            if (file5.exists() || file5.isDirectory()) {
                                ArrayList arrayList = new ArrayList();
                                int i = 0;
                                do {
                                    File[] listFiles2 = file5.listFiles(filenameFilterArr[i]);
                                    if (listFiles2 == null) {
                                        asList = Collections.EMPTY_LIST;
                                    } else {
                                        asList = Arrays.asList(listFiles2);
                                    }
                                    arrayList.addAll(asList);
                                    i++;
                                } while (i < 2);
                                long currentTimeMillis = System.currentTimeMillis() - j;
                                Iterator it = arrayList.iterator();
                                while (it.hasNext()) {
                                    File file7 = (File) it.next();
                                    if (file7.lastModified() < currentTimeMillis) {
                                        if (file7.renameTo(new File(file6, file7.getName()))) {
                                            z = true;
                                        } else {
                                            r3.A02.A03++;
                                            if (file7.exists() && !file7.delete()) {
                                                r3.A02.A02++;
                                            }
                                            z = false;
                                        }
                                        AnonymousClass4TN r12 = r3.A02;
                                        if (z) {
                                            r12.A05++;
                                        } else {
                                            r12.A04++;
                                        }
                                    }
                                }
                            }
                            int i2 = r3.A00;
                            FilenameFilter[] filenameFilterArr2 = {filenameFilter, filenameFilter2};
                            if (file6.exists() || file6.isDirectory()) {
                                ArrayList arrayList2 = new ArrayList();
                                int i3 = 0;
                                do {
                                    File[] listFiles3 = file6.listFiles(filenameFilterArr2[i3]);
                                    if (listFiles3 == null) {
                                        asList2 = Collections.EMPTY_LIST;
                                    } else {
                                        asList2 = Arrays.asList(listFiles3);
                                    }
                                    arrayList2.addAll(asList2);
                                    i3++;
                                } while (i3 < 2);
                                if (arrayList2.size() > i2) {
                                    Collections.sort(arrayList2, new AnonymousClass5CX(r3));
                                    for (File file8 : arrayList2.subList(0, arrayList2.size() - i2)) {
                                        boolean delete = file8.delete();
                                        AnonymousClass4TN r13 = r3.A02;
                                        if (delete) {
                                            r13.A06++;
                                        } else {
                                            r13.A04++;
                                        }
                                    }
                                }
                            }
                        } else {
                            r3.A02.A01++;
                        }
                        r7 = r3.A02;
                        r3.A02 = new AnonymousClass4TN();
                    }
                    r6.AXe(r19);
                    r6.AXd(r7.A02 + r7.A03 + r7.A01 + r7.A04, r7.A06, r7.A05, r7.A00);
                }
            }
        } finally {
            A02(r19);
        }
    }

    @Override // X.AbstractC29391Sf
    public void AXj(C29441Sr r3, Throwable th) {
        Log.e("Profilo/TraceOrchestrator", "Write exception", th);
        this.A05.AXj(r3, th);
        AXh(r3, 8);
    }

    @Override // X.AbstractC29391Sf
    public void AXk(C29441Sr r2) {
        this.A05.AXk(r2);
    }
}
