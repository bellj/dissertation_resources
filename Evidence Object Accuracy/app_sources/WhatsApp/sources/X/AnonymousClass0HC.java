package X;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;

/* renamed from: X.0HC  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0HC extends AbstractC08070aX {
    @Override // X.AbstractC08070aX
    public void A06(Canvas canvas, Matrix matrix, int i) {
    }

    public AnonymousClass0HC(AnonymousClass0AA r1, AnonymousClass0PU r2) {
        super(r1, r2);
    }

    @Override // X.AbstractC08070aX, X.AbstractC12860ig
    public void AAy(Matrix matrix, RectF rectF, boolean z) {
        super.AAy(matrix, rectF, z);
        rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
    }
}
