package X;

import android.animation.ValueAnimator;
import android.view.animation.Interpolator;

/* renamed from: X.0Us  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06710Us implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ AnonymousClass0A8 A00;

    public C06710Us(AnonymousClass0A8 r1) {
        this.A00 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        float floatValue = ((Number) valueAnimator.getAnimatedValue()).floatValue() * 4.0f;
        int i = (int) floatValue;
        float f = floatValue - ((float) i);
        float[] fArr = AnonymousClass0A8.A0C;
        Interpolator interpolator = AnonymousClass0A8.A0A;
        float f2 = fArr[i];
        int i2 = i + 1;
        float f3 = fArr[i2 % fArr.length];
        float interpolation = interpolator.getInterpolation(f);
        float f4 = (f2 * (1.0f - interpolation)) + (f3 * interpolation);
        float[] fArr2 = AnonymousClass0A8.A0D;
        Interpolator interpolator2 = AnonymousClass0A8.A09;
        float f5 = fArr2[i];
        float f6 = fArr2[i2 % fArr2.length];
        float interpolation2 = interpolator2.getInterpolation(f);
        float f7 = (f5 * (1.0f - interpolation2)) + (f6 * interpolation2);
        float[] fArr3 = AnonymousClass0A8.A0B;
        float f8 = fArr3[i];
        float f9 = fArr3[i2 % fArr3.length];
        float interpolation3 = interpolator2.getInterpolation(f);
        AnonymousClass0A8 r3 = this.A00;
        r3.A01 = ((f7 * 360.0f) + f4) - 90.0f;
        r3.A00 = (((f8 * (1.0f - interpolation3)) + (f9 * interpolation3)) - f7) * 360.0f;
        r3.invalidateSelf();
    }
}
