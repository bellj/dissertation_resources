package X;

/* renamed from: X.51x  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C1094951x implements AnonymousClass5T7 {
    @Override // X.AnonymousClass5T7
    public boolean A9i(AbstractC94534c0 r4, AbstractC94534c0 r5, AnonymousClass4RG r6) {
        int compareTo;
        if (C72473ef.A00(r4 instanceof C83013wY ? 1 : 0) && (r5 instanceof C83013wY)) {
            compareTo = AbstractC94534c0.A01(r4, r5);
        } else if ((r4 instanceof C82973wU) && (r5 instanceof C82973wU)) {
            compareTo = C82973wU.A00(r4, r5);
        } else if (!(r4 instanceof C82983wV) || !(r5 instanceof C82983wV)) {
            return false;
        } else {
            compareTo = r4.A05().A08().compareTo(r5.A05().A08());
        }
        if (compareTo > 0) {
            return false;
        }
        return true;
    }
}
