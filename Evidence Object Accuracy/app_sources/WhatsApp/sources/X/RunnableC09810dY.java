package X;

import android.graphics.Typeface;

/* renamed from: X.0dY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09810dY implements Runnable {
    public final /* synthetic */ Typeface A00;
    public final /* synthetic */ AnonymousClass0MR A01;
    public final /* synthetic */ AnonymousClass0OI A02;

    public RunnableC09810dY(Typeface typeface, AnonymousClass0MR r2, AnonymousClass0OI r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = typeface;
    }

    @Override // java.lang.Runnable
    public void run() {
        AnonymousClass0MR r0 = this.A01;
        Typeface typeface = this.A00;
        AnonymousClass08K r02 = r0.A00;
        if (r02 != null) {
            r02.A02(typeface);
        }
    }
}
