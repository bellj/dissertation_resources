package X;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.whatsapp.R;
import com.whatsapp.group.GroupChatInfo;
import com.whatsapp.jid.UserJid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1kH  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C36661kH extends BaseAdapter implements Filterable {
    public String A00;
    public ArrayList A01;
    public List A02 = new ArrayList();
    public List A03 = new ArrayList();
    public final Filter A04 = new C52852bn(this);
    public final C89474Kc A05;
    public final Map A06 = new HashMap();
    public final /* synthetic */ GroupChatInfo A07;

    @Override // android.widget.BaseAdapter, android.widget.ListAdapter
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override // android.widget.Adapter
    public long getItemId(int i) {
        return (long) i;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getViewTypeCount() {
        return 3;
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public boolean hasStableIds() {
        return true;
    }

    public C36661kH(C89474Kc r2, GroupChatInfo groupChatInfo) {
        this.A07 = groupChatInfo;
        this.A05 = r2;
    }

    public void A00(List list) {
        GroupChatInfo groupChatInfo = this.A07;
        if (((AbstractActivityC33001d7) groupChatInfo).A0H.A0Y(groupChatInfo.A0o) || groupChatInfo.A19.A00(groupChatInfo.A0o)) {
            list = Collections.emptyList();
        }
        this.A02 = list;
        String str = this.A00;
        this.A00 = str;
        if (TextUtils.isEmpty(str)) {
            A01(this.A02);
        } else {
            getFilter().filter(str);
        }
    }

    public final void A01(List list) {
        GroupChatInfo groupChatInfo = this.A07;
        if (((AbstractActivityC33001d7) groupChatInfo).A0H.A05(groupChatInfo.A0o) == 1) {
            this.A03 = Collections.emptyList();
        } else {
            this.A03 = list;
            this.A01 = C32751cg.A02(((AbstractActivityC33001d7) groupChatInfo).A08, this.A00);
        }
        notifyDataSetChanged();
    }

    @Override // android.widget.Adapter
    public int getCount() {
        return this.A03.size();
    }

    @Override // android.widget.Filterable
    public Filter getFilter() {
        return this.A04;
    }

    @Override // android.widget.Adapter
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return this.A03.get(i);
    }

    @Override // android.widget.BaseAdapter, android.widget.Adapter
    public int getItemViewType(int i) {
        Object obj = this.A03.get(i);
        if (obj instanceof AbstractC36111jL) {
            return 0;
        }
        if ((obj instanceof C36151jP) || (obj instanceof C36141jO)) {
            return 1;
        }
        return obj instanceof C36131jN ? 2 : -1;
    }

    @Override // android.widget.Adapter
    public View getView(int i, View view, ViewGroup viewGroup) {
        C92434Vw r2;
        UserJid of;
        Object r1;
        AbstractC36121jM r3 = (AbstractC36121jM) this.A03.get(i);
        if (view == null) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                GroupChatInfo groupChatInfo = this.A07;
                view = groupChatInfo.getLayoutInflater().inflate(R.layout.group_chat_info_row_v2, viewGroup, false);
                r1 = new AnonymousClass32N(view, this.A05, groupChatInfo);
            } else if (itemViewType == 1) {
                GroupChatInfo groupChatInfo2 = this.A07;
                view = groupChatInfo2.getLayoutInflater().inflate(R.layout.group_chat_info_button_item, viewGroup, false);
                r1 = new AnonymousClass32L(view, groupChatInfo2);
            } else if (itemViewType == 2) {
                GroupChatInfo groupChatInfo3 = this.A07;
                view = groupChatInfo3.getLayoutInflater().inflate(R.layout.group_chat_info_no_results_item, viewGroup, false);
                r1 = new AnonymousClass32M(view, groupChatInfo3);
            } else {
                StringBuilder sb = new StringBuilder("Unknown type: ");
                sb.append(itemViewType);
                throw new IllegalStateException(sb.toString());
            }
            view.setTag(r1);
        }
        if (!(r3 instanceof AbstractC36111jL) || (of = UserJid.of(((AbstractC36111jL) r3).A00.A0D)) == null) {
            r2 = null;
        } else {
            r2 = (C92434Vw) this.A06.get(of);
        }
        ((AnonymousClass4V1) view.getTag()).A00(r3, r2, this.A01);
        return view;
    }

    @Override // android.widget.BaseAdapter, android.widget.ListAdapter
    public boolean isEnabled(int i) {
        return ((AbstractC36121jM) this.A03.get(i)).isEnabled();
    }
}
