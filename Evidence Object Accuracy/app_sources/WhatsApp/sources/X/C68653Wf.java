package X;

import android.app.Activity;
import com.whatsapp.R;
import com.whatsapp.settings.SettingsChat;
import com.whatsapp.util.Log;
import java.util.Locale;

/* renamed from: X.3Wf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68653Wf implements AbstractC45001zu {
    public final /* synthetic */ Activity A00;
    public final /* synthetic */ AbstractC13860kS A01;
    public final /* synthetic */ C19500uD A02;
    public final /* synthetic */ C15890o4 A03;
    public final /* synthetic */ AnonymousClass018 A04;
    public final /* synthetic */ C14950mJ A05;
    public final /* synthetic */ C18260sA A06;
    public final /* synthetic */ AbstractC116175Uk A07;

    public C68653Wf(Activity activity, AbstractC13860kS r2, C19500uD r3, C15890o4 r4, AnonymousClass018 r5, C14950mJ r6, C18260sA r7, AbstractC116175Uk r8) {
        this.A00 = activity;
        this.A04 = r5;
        this.A06 = r7;
        this.A07 = r8;
        this.A02 = r3;
        this.A01 = r2;
        this.A05 = r6;
        this.A03 = r4;
    }

    @Override // X.AbstractC45001zu
    public void AM2(int i) {
        int i2;
        this.A06.A0H.A04(this);
        Activity activity = this.A00;
        C36021jC.A00(activity, 600);
        SettingsChat.A0U = null;
        this.A07.ASB(i);
        if (i == 3) {
            C32781cj.A08(activity.getApplicationContext());
            byte[] A0D = C003501n.A0D(16);
            byte[] A0F = C32781cj.A0F(A0D);
            if (A0F != null) {
                this.A02.A01(null, A0F, A0D, 1);
                this.A01.Ado(R.string.msg_store_backup_failed_try_again_later);
                Log.w("settings/backup/failed/missing-or-mismatch");
                return;
            }
        } else if (i == 2) {
            AbstractC13860kS r3 = this.A01;
            boolean A00 = C14950mJ.A00();
            StringBuilder A0h = C12960it.A0h();
            if (A00) {
                C12990iw.A0y(activity, " ", A0h, R.string.msg_store_backup_failed_out_of_space);
                i2 = R.string.remove_files_from_sd_card;
            } else {
                C12990iw.A0y(activity, " ", A0h, R.string.msg_store_backup_failed_out_of_space_shared_storage);
                i2 = R.string.remove_files_from_shared_storage;
            }
            r3.Adp(C12960it.A0d(activity.getString(i2), A0h));
            return;
        } else if (i != 1) {
            if (!this.A03.A07()) {
                Log.i("settings/backup/failed/missing-permissions");
                this.A01.Ado(R.string.msg_store_backup_failed);
                return;
            }
            return;
        }
        this.A01.Ado(R.string.msg_store_backup_failed);
        Log.w("settings/backup/failed/null");
    }

    @Override // X.AbstractC45001zu
    public void ANC() {
        C36021jC.A01(this.A00, 600);
    }

    @Override // X.AbstractC45001zu
    public void AUO(int i) {
        ProgressDialogC48342Fq r6 = SettingsChat.A0U;
        if (r6 != null) {
            r6.setMessage(C12960it.A0X(this.A00, C12970iu.A0r(this.A04, i), new Object[1], 0, R.string.settings_backup_db_now_message_with_progress_percentage_placeholder));
        }
        int i2 = i % 10;
        Locale locale = Locale.ENGLISH;
        Object[] objArr = new Object[1];
        C12960it.A1P(objArr, i, 0);
        if (i2 == 0) {
            C12990iw.A1R("settings/backup/msgstore/progress/%d%%", locale, objArr);
        } else {
            String.format(locale, "settings/backup/msgstore/progress/%d%%", objArr);
        }
    }
}
