package X;

import android.graphics.SurfaceTexture;
import android.view.Surface;

/* renamed from: X.3gb  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C73663gb extends Surface {
    public static int A02;
    public static boolean A03;
    public boolean A00;
    public final HandlerThreadC73403gB A01;

    public /* synthetic */ C73663gb(SurfaceTexture surfaceTexture, HandlerThreadC73403gB r2) {
        super(surfaceTexture);
        this.A01 = r2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (A01(r5) != false) goto L_0x000a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static X.C73663gb A00(android.content.Context r5, boolean r6) {
        /*
            r4 = 0
            if (r6 == 0) goto L_0x000a
            boolean r1 = A01(r5)
            r0 = 0
            if (r1 == 0) goto L_0x000b
        L_0x000a:
            r0 = 1
        L_0x000b:
            X.C95314dV.A04(r0)
            X.3gB r3 = new X.3gB
            r3.<init>()
            if (r6 == 0) goto L_0x0017
            int r4 = X.C73663gb.A02
        L_0x0017:
            r3.start()
            android.os.Looper r0 = r3.getLooper()
            android.os.Handler r1 = new android.os.Handler
            r1.<init>(r0, r3)
            r3.A00 = r1
            X.5Bm r0 = new X.5Bm
            r0.<init>(r1)
            r3.A01 = r0
            monitor-enter(r3)
            android.os.Handler r2 = r3.A00     // Catch: all -> 0x0061
            r0 = 1
            r1 = 0
            android.os.Message r0 = r2.obtainMessage(r0, r4, r1)     // Catch: all -> 0x0061
            r0.sendToTarget()     // Catch: all -> 0x0061
        L_0x0038:
            X.3gb r0 = r3.A02     // Catch: all -> 0x0061
            if (r0 != 0) goto L_0x004a
            java.lang.RuntimeException r0 = r3.A04     // Catch: all -> 0x0061
            if (r0 != 0) goto L_0x004a
            java.lang.Error r0 = r3.A03     // Catch: all -> 0x0061
            if (r0 != 0) goto L_0x004a
            r3.wait()     // Catch: InterruptedException -> 0x0048, all -> 0x0061
            goto L_0x0038
        L_0x0048:
            r1 = 1
            goto L_0x0038
        L_0x004a:
            monitor-exit(r3)     // Catch: all -> 0x0061
            if (r1 == 0) goto L_0x0054
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r0.interrupt()
        L_0x0054:
            java.lang.RuntimeException r0 = r3.A04
            if (r0 != 0) goto L_0x0060
            java.lang.Error r0 = r3.A03
            if (r0 != 0) goto L_0x005f
            X.3gb r0 = r3.A02
            return r0
        L_0x005f:
            throw r0
        L_0x0060:
            throw r0
        L_0x0061:
            r0 = move-exception
            monitor-exit(r3)     // Catch: all -> 0x0061
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C73663gb.A00(android.content.Context, boolean):X.3gb");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0062, code lost:
        if (r0 == false) goto L_0x0064;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean A01(android.content.Context r7) {
        /*
            java.lang.Class<X.3gb> r6 = X.C73663gb.class
            monitor-enter(r6)
            boolean r0 = X.C73663gb.A03     // Catch: all -> 0x0072
            r5 = 1
            if (r0 != 0) goto L_0x006b
            int r2 = X.AnonymousClass3JZ.A01     // Catch: all -> 0x0072
            r4 = 0
            r0 = 24
            if (r2 < r0) goto L_0x0066
            r3 = 26
            if (r2 >= r3) goto L_0x0028
            java.lang.String r1 = X.AnonymousClass3JZ.A04     // Catch: all -> 0x0072
            java.lang.String r0 = "samsung"
            boolean r0 = r0.equals(r1)     // Catch: all -> 0x0072
            if (r0 != 0) goto L_0x0066
            java.lang.String r1 = X.AnonymousClass3JZ.A05     // Catch: all -> 0x0072
            java.lang.String r0 = "XT1650"
            boolean r0 = r0.equals(r1)     // Catch: all -> 0x0072
            if (r0 == 0) goto L_0x0028
            goto L_0x0066
        L_0x0028:
            if (r2 >= r3) goto L_0x0037
            android.content.pm.PackageManager r1 = r7.getPackageManager()     // Catch: all -> 0x0072
            java.lang.String r0 = "android.hardware.vr.high_performance"
            boolean r0 = r1.hasSystemFeature(r0)     // Catch: all -> 0x0072
            if (r0 != 0) goto L_0x0037
            goto L_0x0066
        L_0x0037:
            android.opengl.EGLDisplay r1 = android.opengl.EGL14.eglGetDisplay(r4)     // Catch: all -> 0x0072
            r0 = 12373(0x3055, float:1.7338E-41)
            java.lang.String r1 = android.opengl.EGL14.eglQueryString(r1, r0)     // Catch: all -> 0x0072
            if (r1 == 0) goto L_0x0066
            java.lang.String r0 = "EGL_EXT_protected_content"
            boolean r0 = r1.contains(r0)     // Catch: all -> 0x0072
            if (r0 == 0) goto L_0x0066
            r0 = 17
            if (r2 < r0) goto L_0x0064
            android.opengl.EGLDisplay r1 = android.opengl.EGL14.eglGetDisplay(r4)     // Catch: all -> 0x0072
            r0 = 12373(0x3055, float:1.7338E-41)
            java.lang.String r1 = android.opengl.EGL14.eglQueryString(r1, r0)     // Catch: all -> 0x0072
            if (r1 == 0) goto L_0x0064
            java.lang.String r0 = "EGL_KHR_surfaceless_context"
            boolean r0 = r1.contains(r0)     // Catch: all -> 0x0072
            r1 = 1
            if (r0 != 0) goto L_0x0067
        L_0x0064:
            r1 = 2
            goto L_0x0067
        L_0x0066:
            r1 = 0
        L_0x0067:
            X.C73663gb.A02 = r1     // Catch: all -> 0x0072
            X.C73663gb.A03 = r5     // Catch: all -> 0x0072
        L_0x006b:
            int r0 = X.C73663gb.A02     // Catch: all -> 0x0072
            if (r0 != 0) goto L_0x0070
            r5 = 0
        L_0x0070:
            monitor-exit(r6)
            return r5
        L_0x0072:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C73663gb.A01(android.content.Context):boolean");
    }

    @Override // android.view.Surface
    public void release() {
        super.release();
        HandlerThreadC73403gB r2 = this.A01;
        synchronized (r2) {
            if (!this.A00) {
                r2.A00.sendEmptyMessage(2);
                this.A00 = true;
            }
        }
    }
}
