package X;

import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.facebook.msys.mci.DefaultCrypto;
import com.whatsapp.util.Log;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.5qh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public final class C125105qh {
    public static final Uri A00(String str) {
        String str2;
        Uri uri = null;
        if (str != null) {
            Object[] array = AnonymousClass03B.A09(str, new String[]{"."}, 0).toArray(new String[0]);
            if (array != null) {
                String[] strArr = (String[]) array;
                if (strArr.length > 1) {
                    byte[] decode = Base64.decode(strArr[1], 0);
                    try {
                        if (Build.VERSION.SDK_INT >= 19) {
                            C16700pc.A0B(decode);
                            Charset charset = StandardCharsets.UTF_8;
                            C16700pc.A0B(charset);
                            str2 = new String(decode, charset);
                        } else {
                            C16700pc.A0B(decode);
                            Charset forName = Charset.forName(DefaultCrypto.UTF_8);
                            C16700pc.A0B(forName);
                            str2 = new String(decode, forName);
                        }
                    } catch (UnsupportedEncodingException unused) {
                        Log.e("UnsupportedEncodingException: Credential Push data cannot be decoded");
                        str2 = null;
                    }
                    if (str2 != null) {
                        try {
                            JSONObject A05 = C13000ix.A05(str2);
                            String string = A05.getString("callbackURL");
                            String string2 = A05.getJSONArray("pushAccountReceipts").getString(0);
                            if (!TextUtils.isEmpty(string)) {
                                Uri.Builder buildUpon = Uri.parse(string).buildUpon();
                                StringBuilder A0h = C12960it.A0h();
                                A0h.append("results[");
                                A0h.append((Object) string2);
                                A0h.append(']');
                                uri = buildUpon.appendQueryParameter(A0h.toString(), "CANCELLED").build();
                                return uri;
                            }
                        } catch (JSONException e) {
                            Log.e(C16700pc.A08("JSONException: cannot parse callback url from json, ", e.getMessage()));
                            return uri;
                        }
                    }
                }
            } else {
                throw C12980iv.A0n("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
            }
        }
        return null;
    }
}
