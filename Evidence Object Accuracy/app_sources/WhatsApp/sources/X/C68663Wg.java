package X;

import java.util.Iterator;

/* renamed from: X.3Wg  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C68663Wg implements AbstractC38351nw {
    public int A00;
    public int A01;
    public int A02;
    public final /* synthetic */ int A03;
    public final /* synthetic */ int A04;
    public final /* synthetic */ C27441Hl A05;
    public final /* synthetic */ C90224Nb A06;
    public final /* synthetic */ C21590xf A07;

    @Override // X.AbstractC38361nx
    public boolean Ada() {
        return false;
    }

    public C68663Wg(C27441Hl r1, C90224Nb r2, C21590xf r3, int i, int i2) {
        this.A07 = r3;
        this.A04 = i;
        this.A03 = i2;
        this.A05 = r1;
        this.A06 = r2;
    }

    @Override // X.AbstractC38351nw
    public void AQZ() {
        C21610xh r1 = this.A07.A02;
        C27441Hl r0 = this.A05;
        r1.A05(r0);
        AbstractC14640lm r5 = r0.A07;
        C90224Nb r4 = this.A06;
        C21600xg r7 = r4.A01;
        C20120vF r6 = r7.A05;
        C28011Kc A04 = r6.A04(r5);
        C12990iw.A11(C12960it.A08(r7.A02).remove("storage_usage_deletion_jid").remove("storage_usage_deletion_current_msg_cnt"), "storage_usage_deletion_all_msg_cnt");
        r7.A01.A0I(new C28021Kd(r6.A04(r5), r5));
        Iterator it = r7.A08.iterator();
        while (it.hasNext()) {
            ((AbstractC28051Kk) it.next()).AOw(A04, r5);
        }
        r4.A00.AQZ();
    }

    @Override // X.AbstractC38351nw
    public void AUM(int i, int i2) {
        int i3 = this.A02;
        if (i3 == -1) {
            i3 = Math.max(this.A03 / 100, 1);
            this.A02 = i3;
        }
        int i4 = this.A04 + i;
        this.A00 = i4;
        if (i4 - this.A01 > i3) {
            C21590xf.A00(this.A06, this.A05.A07, this.A03, i4);
            this.A01 = this.A00;
        }
    }

    @Override // X.AbstractC38351nw
    public void AWF() {
        this.A00 = this.A04;
    }
}
