package X;

/* renamed from: X.4Yd  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C92954Yd {
    public static boolean A00(C95304dT r3, int i, boolean z) {
        int A00 = C95304dT.A00(r3);
        if (A00 < 7) {
            if (!z) {
                throw AnonymousClass496.A00(C12960it.A0W(A00, "too short header: "));
            }
        } else if (r3.A0C() != i) {
            if (!z) {
                throw AnonymousClass496.A00(C12960it.A0d(Integer.toHexString(i), C12960it.A0k("expected header type ")));
            }
        } else if (r3.A0C() == 118 && r3.A0C() == 111 && r3.A0C() == 114 && r3.A0C() == 98 && r3.A0C() == 105 && r3.A0C() == 115) {
            return true;
        } else {
            if (!z) {
                throw AnonymousClass496.A00("expected characters 'vorbis'");
            }
        }
        return false;
    }
}
