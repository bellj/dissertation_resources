package X;

import android.os.SystemClock;
import com.facebook.redex.RunnableBRunnable0Shape2S0200000_I0_2;

/* renamed from: X.3Ze  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C69423Ze implements AnonymousClass5WE {
    public final C18790t3 A00;
    public final AnonymousClass10V A01;
    public final C16590pI A02;
    public final C22050yP A03;
    public final C17080qE A04;
    public final C20750wG A05;
    public final C37241lo A06;
    public final C18800t4 A07;
    public final C20700wB A08;
    public final C19930uu A09;
    public final AbstractC14440lR A0A;

    public /* synthetic */ C69423Ze(C18790t3 r1, AnonymousClass10V r2, C16590pI r3, C22050yP r4, C17080qE r5, C20750wG r6, C37241lo r7, C18800t4 r8, C20700wB r9, C19930uu r10, AbstractC14440lR r11) {
        this.A02 = r3;
        this.A09 = r10;
        this.A0A = r11;
        this.A00 = r1;
        this.A08 = r9;
        this.A03 = r4;
        this.A07 = r8;
        this.A01 = r2;
        this.A04 = r5;
        this.A05 = r6;
        this.A06 = r7;
    }

    @Override // X.AnonymousClass5WE
    public void AUJ(AbstractC14640lm r6, String str, int i, long j) {
        C20700wB r1 = this.A08;
        r1.A01.A01(r6);
        r1.A02.A01(r6);
        this.A05.A00(r6, i);
        this.A03.A0A(null, Long.valueOf(System.currentTimeMillis() - j), 6, C12990iw.A04("preview".equals(str) ? 1 : 0));
    }

    @Override // X.AnonymousClass5WE
    public void AUK(C41831uE r14, long j) {
        C37241lo r0 = this.A06;
        int i = 2;
        if (r0 != null) {
            r0.A00(2);
        }
        if (r14.A01 == -1) {
            C20700wB r2 = this.A08;
            int i2 = r14.A02;
            if (i2 == 1 || i2 == 2) {
                r2.A02.A01(r14.A03);
            }
            this.A01.A02(r14.A03);
            return;
        }
        int i3 = r14.A02;
        if (r14.A04 != null) {
            this.A04.A01(r14, j);
        } else if (r14.A05 != null) {
            C16590pI r4 = this.A02;
            C19930uu r8 = this.A09;
            AbstractC14440lR r9 = this.A0A;
            AnonymousClass393.A01(this.A00, this.A01, r4, this.A03, r14, this.A07, r8, r9, 0, j);
        } else {
            AnonymousClass10V r3 = this.A01;
            r3.A00.A01(new RunnableBRunnable0Shape2S0200000_I0_2(r3, 35, r14));
            C22050yP r32 = this.A03;
            if (i3 != 1) {
                i = 1;
            }
            r32.A0A(null, Long.valueOf(SystemClock.elapsedRealtime() - j), 1, i);
        }
    }
}
