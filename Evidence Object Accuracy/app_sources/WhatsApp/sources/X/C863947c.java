package X;

import android.content.Context;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import com.whatsapp.R;

/* renamed from: X.47c  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C863947c extends AnonymousClass3J9 {
    @Override // X.AnonymousClass3J9
    public void A03(Context context, Spannable spannable, int i, int i2) {
        spannable.setSpan(new ForegroundColorSpan(AnonymousClass00T.A00(context, R.color.search_text_highlight)), i, i2, 33);
    }
}
