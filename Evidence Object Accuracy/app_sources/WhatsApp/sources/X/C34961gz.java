package X;

import java.util.Arrays;

/* renamed from: X.1gz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C34961gz {
    public static int A00(int i) {
        return Arrays.hashCode(new Object[]{Long.valueOf(System.nanoTime()), Integer.valueOf(i)});
    }

    public static void A01(C21230x5 r2, Integer num, String str) {
        if (num != null) {
            r2.ALC(926875649, str, num.intValue());
        }
    }
}
