package X;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: X.63W  reason: invalid class name */
/* loaded from: classes4.dex */
public final class AnonymousClass63W implements Parcelable.Creator {
    @Override // android.os.Parcelable.Creator
    public Object createFromParcel(Parcel parcel) {
        C16700pc.A0E(parcel, 0);
        return new C1315963j(EnumC124535ph.A02, -1, -1);
    }

    @Override // android.os.Parcelable.Creator
    public Object[] newArray(int i) {
        return new C1315963j[i];
    }
}
