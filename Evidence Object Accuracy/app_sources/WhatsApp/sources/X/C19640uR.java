package X;

import com.whatsapp.util.Log;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: X.0uR  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C19640uR {
    public AnonymousClass2DR A00;
    public String A01;
    public final AbstractC17160qM A02;
    public final C19610uO A03;
    public final C19630uQ A04;

    public C19640uR(AbstractC17160qM r2, C19610uO r3, C19630uQ r4) {
        C16700pc.A0E(r3, 2);
        C16700pc.A0E(r4, 3);
        this.A02 = r2;
        this.A03 = r3;
        this.A04 = r4;
    }

    public final void A00(Map map) {
        AnonymousClass2DR r2 = this.A00;
        if (r2 == null) {
            return;
        }
        if (C16700pc.A0O(map.get(EnumC629739j.A05.key), "false")) {
            Log.w("Finishing Bloks resource with failure");
            r2.A00(map);
            return;
        }
        Log.w("Finishing Bloks resource with success");
        r2.A01(map);
    }

    public final void A01(Map map) {
        int i;
        String str;
        Long l;
        String str2 = (String) map.get("event");
        String str3 = (String) map.get("size");
        String str4 = (String) map.get("fdsInstanceKey");
        if (str4 != null && str2 != null) {
            switch (str2.hashCode()) {
                case -1161549703:
                    if (str2.equals("startPrefetch")) {
                        C19610uO r3 = this.A03;
                        r3.A00(Integer.parseInt(str4), "prefetchPerfTracker");
                        r3.A02.A03(Integer.parseInt(str4), "start");
                        int parseInt = Integer.parseInt(str4);
                        Object obj = map.get("app_id");
                        if (obj != null) {
                            r3.A02(parseInt, "app_id", (String) obj);
                            return;
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
                    }
                    return;
                case -1145605794:
                    if (str2.equals("bloksCacheHit")) {
                        i = Integer.parseInt(str4);
                        C16700pc.A0C(str3);
                        l = Long.valueOf(Long.parseLong(str3));
                        str = "blok_cache_size";
                        break;
                    } else {
                        return;
                    }
                case -55045288:
                    if (str2.equals("prefetchResponse")) {
                        C19610uO r6 = this.A03;
                        int parseInt2 = Integer.parseInt(str4);
                        AnonymousClass1Q5 r32 = r6.A02;
                        r32.A03(parseInt2, "prefetchResponse");
                        if (map.get("error_code") != null) {
                            int parseInt3 = Integer.parseInt(str4);
                            Object obj2 = map.get("error_code");
                            if (obj2 != null) {
                                r6.A02(parseInt3, "error_code", (String) obj2);
                            } else {
                                throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
                            }
                        }
                        r32.A05(Integer.parseInt(str4), 467);
                        return;
                    }
                    return;
                case 332886870:
                    if (str2.equals("openScreen")) {
                        this.A04.A02.A03(Integer.parseInt(str4), str2);
                        return;
                    }
                    return;
                case 1415672482:
                    if (str2.equals("bloksPayloadResponse")) {
                        i = Integer.parseInt(str4);
                        C16700pc.A0C(str3);
                        l = Long.valueOf(Long.parseLong(str3));
                        str = "blok_payload_size";
                        break;
                    } else {
                        return;
                    }
                default:
                    return;
            }
            C19630uQ r2 = this.A04;
            r2.A02.A03(i, str2);
            r2.A01(i, str, l.longValue());
        }
    }

    public final boolean A02(AnonymousClass2DR r9, String str, Map map, int i) {
        this.A00 = r9;
        if (map.get("app_id") == null) {
            Log.w("No parameters or no app_id");
            r9.A00(null);
            return false;
        }
        Object obj = map.get("app_id");
        if (obj != null) {
            String str2 = (String) obj;
            this.A01 = str2;
            C16700pc.A0C(str2);
            C19630uQ r1 = this.A04;
            r1.A02.A03(i, "requestBloksScreen");
            if (str2 != null) {
                r1.A02(i, "app_id", str2);
            }
            Object obj2 = map.get("data");
            if (obj2 != null) {
                if (obj2 instanceof Map) {
                    Map map2 = (Map) obj2;
                    Object obj3 = map.get(AnonymousClass49S.A00.key);
                    if (obj3 != null) {
                        Map map3 = (Map) obj3;
                        C16700pc.A0E(map2, 0);
                        C16700pc.A0E(map3, 1);
                        LinkedHashMap linkedHashMap = new LinkedHashMap(map2);
                        linkedHashMap.putAll(map3);
                        obj2 = linkedHashMap;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.collections.Map<*, *>");
                    }
                }
                AbstractC17160qM r5 = this.A02;
                C17520qw r0 = new C17520qw(this.A01, obj2);
                Map singletonMap = Collections.singletonMap(r0.first, r0.second);
                C16700pc.A0B(singletonMap);
                C17520qw r02 = new C17520qw("job_id", str);
                Map singletonMap2 = Collections.singletonMap(r02.first, r02.second);
                C16700pc.A0B(singletonMap2);
                r5.AZe("data", C17530qx.A02(new C17520qw("data", singletonMap), new C17520qw("context", singletonMap2)));
            }
            return true;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
    }
}
