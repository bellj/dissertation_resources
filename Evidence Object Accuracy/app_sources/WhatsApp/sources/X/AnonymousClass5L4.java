package X;

/* renamed from: X.5L4  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass5L4 extends AbstractC114155Kk implements AbstractC11610gZ {
    public final String A00;
    public final Throwable A01;

    public AnonymousClass5L4(String str, Throwable th) {
        this.A01 = th;
        this.A00 = str;
    }

    @Override // X.AbstractC114155Kk, X.AbstractC10990fX
    public AbstractC10990fX A02(int i) {
        A06();
        throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
    }

    @Override // X.AbstractC10990fX
    public boolean A03(AnonymousClass5X4 r2) {
        A06();
        throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
    }

    @Override // X.AbstractC10990fX
    public /* bridge */ /* synthetic */ void A04(Runnable runnable, AnonymousClass5X4 r3) {
        A06();
        throw C12990iw.A0m("Redex: Unreachable code after no-return invoke");
    }

    public final void A06() {
        String A08;
        Throwable th = this.A01;
        if (th != null) {
            String str = this.A00;
            String str2 = "";
            if (!(str == null || (A08 = C16700pc.A08(". ", str)) == null)) {
                str2 = A08;
            }
            throw new IllegalStateException(C16700pc.A08("Module with the Main dispatcher had failed to initialize", str2), th);
        }
        throw C12960it.A0U("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android' and ensure it has the same version as 'kotlinx-coroutines-core'");
    }

    @Override // X.AbstractC10990fX, java.lang.Object
    public String toString() {
        String str;
        StringBuilder A0k = C12960it.A0k("Dispatchers.Main[missing");
        Throwable th = this.A01;
        if (th != null) {
            str = C16700pc.A08(", cause=", th);
        } else {
            str = "";
        }
        A0k.append(str);
        return C72453ed.A0t(A0k);
    }
}
