package X;

import android.text.TextUtils;
import java.util.Arrays;

/* renamed from: X.1rp  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C40581rp {
    public long A00;
    public long A01;
    public long A02;
    public long A03;
    public long A04;
    public long A05;
    public long A06;
    public Boolean A07;
    public Boolean A08;
    public Long A09;
    public Long A0A;
    public Long A0B;
    public Long A0C;
    public Long A0D;
    public Long A0E;
    public Long A0F;
    public Long A0G;
    public Long A0H;
    public Long A0I;
    public Long A0J;
    public Long A0K;
    public Long A0L;

    public C40581rp(Boolean bool, Boolean bool2, Long l, Long l2, Long l3, Long l4, Long l5, Long l6, Long l7, Long l8, Long l9, Long l10, Long l11, Long l12, Long l13, long j, long j2, long j3, long j4, long j5, long j6, long j7) {
        this.A03 = j;
        this.A02 = j2;
        this.A01 = j3;
        this.A00 = j4;
        this.A06 = j5;
        this.A05 = j6;
        this.A04 = j7;
        this.A0C = l;
        this.A0I = l2;
        this.A0H = l3;
        this.A0L = l4;
        this.A0K = l5;
        this.A0F = l6;
        this.A0G = l7;
        this.A0B = l8;
        this.A07 = bool;
        this.A09 = l9;
        this.A0E = l10;
        this.A08 = bool2;
        this.A0D = l11;
        this.A0A = l12;
        this.A0J = l13;
    }

    public static C40581rp A00(String str) {
        String[] split = str.split(",");
        long A01 = C40601rr.A01(split, 0);
        long A012 = C40601rr.A01(split, 1);
        long A013 = C40601rr.A01(split, 2);
        long A014 = C40601rr.A01(split, 3);
        long A015 = C40601rr.A01(split, 4);
        long A016 = C40601rr.A01(split, 5);
        long A017 = C40601rr.A01(split, 6);
        Long A03 = C40601rr.A03(split, 18);
        Long A032 = C40601rr.A03(split, 7);
        Long A033 = C40601rr.A03(split, 8);
        Long A034 = C40601rr.A03(split, 9);
        Long A035 = C40601rr.A03(split, 10);
        Long A036 = C40601rr.A03(split, 11);
        Long A037 = C40601rr.A03(split, 12);
        Long A038 = C40601rr.A03(split, 19);
        return new C40581rp(C40601rr.A02(split, 13), C40601rr.A02(split, 16), A03, A032, A033, A034, A035, A036, A037, A038, C40601rr.A03(split, 14), C40601rr.A03(split, 15), C40601rr.A03(split, 17), C40601rr.A03(split, 20), C40601rr.A03(split, 21), A01, A012, A013, A014, A015, A016, A017);
    }

    public void A01(int i) {
        switch (i) {
            case 0:
                this.A03++;
                return;
            case 1:
                this.A02++;
                return;
            case 2:
                this.A01++;
                return;
            case 3:
                this.A00++;
                return;
            case 4:
                this.A06++;
                return;
            case 5:
                this.A05++;
                return;
            case 6:
                this.A04++;
                return;
            case 7:
                this.A0I = Long.valueOf(C40601rr.A00(this.A0I, 1));
                return;
            case 8:
                this.A0H = Long.valueOf(C40601rr.A00(this.A0H, 1));
                return;
            case 9:
                this.A0L = Long.valueOf(C40601rr.A00(this.A0L, 1));
                return;
            case 10:
                this.A0K = Long.valueOf(C40601rr.A00(this.A0K, 1));
                return;
            case 11:
                this.A0F = Long.valueOf(C40601rr.A00(this.A0F, 1));
                return;
            case 12:
                this.A0G = Long.valueOf(C40601rr.A00(this.A0G, 1));
                return;
            default:
                switch (i) {
                    case 19:
                        this.A0B = Long.valueOf(C40601rr.A00(this.A0B, 1));
                        return;
                    case C43951xu.A01 /* 20 */:
                        this.A0A = Long.valueOf(C40601rr.A00(this.A0A, 1));
                        return;
                    case 21:
                        this.A0J = Long.valueOf(C40601rr.A00(this.A0J, 1));
                        return;
                    default:
                        StringBuilder sb = new StringBuilder("EngagementRowCount/update - unhandled fieldIdx: ");
                        sb.append(i);
                        throw new IllegalArgumentException(sb.toString());
                }
        }
    }

    public String toString() {
        return TextUtils.join(",", Arrays.asList(Long.valueOf(this.A03), Long.valueOf(this.A02), Long.valueOf(this.A01), Long.valueOf(this.A00), Long.valueOf(this.A06), Long.valueOf(this.A05), Long.valueOf(this.A04), this.A0I, this.A0H, this.A0L, this.A0K, this.A0F, this.A0G, this.A07, this.A09, this.A0E, this.A08, this.A0D, this.A0C, this.A0B, this.A0A, this.A0J));
    }
}
