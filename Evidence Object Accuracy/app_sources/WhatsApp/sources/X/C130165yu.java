package X;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;
import com.whatsapp.util.Log;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

/* renamed from: X.5yu  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C130165yu {
    public KeyStore A00;
    public final C30931Zj A01;
    public final C126585t7 A02;

    public C130165yu(Context context, C16630pM r7) {
        C30931Zj A00 = C30931Zj.A00("CLKeyStorageManager", "onboarding", "IN");
        this.A01 = A00;
        this.A02 = new C126585t7(r7);
        if (Build.VERSION.SDK_INT >= 18) {
            try {
                A00.A06("initializing KS");
                KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
                this.A00 = instance;
                instance.load(null);
                A04(context);
            } catch (Exception e) {
                this.A01.A08("onboarding", null, e);
                throw C117315Zl.A0J(e);
            }
        }
    }

    public String A00() {
        SharedPreferences sharedPreferences = this.A02.A00;
        String string = sharedPreferences.getString("k0", "");
        return (this.A00 == null || !sharedPreferences.getBoolean("use_ks", false)) ? string : A02("k0", "aes_k0", string);
    }

    public String A01() {
        SharedPreferences sharedPreferences = this.A02.A00;
        String string = sharedPreferences.getString("token", "");
        return (this.A00 == null || !sharedPreferences.getBoolean("use_ks", false)) ? string : A02("token", "aes_token", string);
    }

    public final String A02(String str, String str2, String str3) {
        try {
            this.A01.A09("onboarding", "decrypt the key", new C31021Zs[]{new C31021Zs("alias", str)});
            byte[] A08 = A08(Base64.decode(this.A02.A00.getString(str2, ""), 0), str);
            byte[] decode = Base64.decode(str3, 0);
            byte[] bArr = new byte[16];
            System.arraycopy(decode, 0, bArr, 0, 16);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
            int length = decode.length - 16;
            byte[] bArr2 = new byte[length];
            System.arraycopy(decode, 16, bArr2, 0, length);
            SecretKeySpec secretKeySpec = new SecretKeySpec(A08, "AES");
            int i = Build.VERSION.SDK_INT;
            if (i >= 18) {
                Cipher instance = Cipher.getInstance(i >= 23 ? "AES/GCM/NoPadding" : "AES/CBC/PKCS7Padding");
                instance.init(2, secretKeySpec, ivParameterSpec);
                return AnonymousClass61J.A00(instance.doFinal(bArr2));
            }
            throw new IllegalArgumentException();
        } catch (Exception e) {
            Log.w(C12960it.A0d(e.toString(), C12960it.A0k("PAY")));
            throw C117315Zl.A0J(e);
        }
    }

    public final String A03(String str, String str2, String str3) {
        try {
            this.A01.A09("onboarding", " encrypt the key ", new C31021Zs[]{new C31021Zs("alias", str)});
            byte[] A08 = A08(Base64.decode(this.A02.A00.getString(str2, ""), 0), str);
            int i = Build.VERSION.SDK_INT;
            if (i >= 18) {
                Cipher instance = Cipher.getInstance(i >= 23 ? "AES/GCM/NoPadding" : "AES/CBC/PKCS7Padding");
                SecretKeySpec secretKeySpec = new SecretKeySpec(A08, "AES");
                byte[] A1a = C117305Zk.A1a(16);
                instance.init(1, secretKeySpec, new IvParameterSpec(A1a));
                byte[] doFinal = instance.doFinal(AnonymousClass61J.A01(str3));
                int length = doFinal.length;
                byte[] bArr = new byte[length + 16];
                System.arraycopy(A1a, 0, bArr, 0, 16);
                System.arraycopy(doFinal, 0, bArr, 16, length);
                return Base64.encodeToString(bArr, 0);
            }
            throw new IllegalArgumentException();
        } catch (Exception e) {
            this.A01.A08("onboarding", null, e);
            throw C117315Zl.A0J(e);
        }
    }

    public final synchronized void A04(Context context) {
        SharedPreferences sharedPreferences = this.A02.A00;
        if (!sharedPreferences.getBoolean("use_ks", false)) {
            try {
                this.A00.load(null);
                A05(context, "k0");
                A05(context, "token");
                A06("aes_k0", "k0");
                A06("aes_token", "token");
                C12960it.A0t(sharedPreferences.edit(), "use_ks", true);
            } catch (Exception e) {
                C30931Zj r3 = this.A01;
                StringBuilder A0h = C12960it.A0h();
                A0h.append(" Skip Android KeyStore setup because: ");
                r3.A08("onboarding", C12960it.A0d(e.toString(), A0h), e);
            }
        }
    }

    public final void A05(Context context, String str) {
        try {
            this.A01.A09("onboarding", "generate rsa key pairs for", new C31021Zs[]{new C31021Zs("alias", str)});
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            instance2.add(2, 1);
            KeyPairGeneratorSpec.Builder alias = new KeyPairGeneratorSpec.Builder(context).setAlias(str);
            StringBuilder A0h = C12960it.A0h();
            A0h.append("CN=");
            KeyPairGeneratorSpec build = alias.setSubject(new X500Principal(C12960it.A0d(str, A0h))).setSerialNumber(BigInteger.TEN).setStartDate(instance.getTime()).setEndDate(instance2.getTime()).build();
            KeyPairGenerator instance3 = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
            instance3.initialize(build);
            instance3.generateKeyPair();
        } catch (Exception e) {
            this.A01.A08("onboarding", null, e);
            throw C117315Zl.A0J(e);
        }
    }

    public final void A06(String str, String str2) {
        C30931Zj r6 = this.A01;
        r6.A09("onboarding", "generate and store aes key", new C31021Zs[]{new C31021Zs("alias", str2)});
        byte[] A1a = C117305Zk.A1a(16);
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) this.A00.getEntry(str2, null);
            int i = Build.VERSION.SDK_INT;
            if (i >= 18) {
                C12970iu.A1D(this.A02.A00.edit(), str, Base64.encodeToString(C117295Zj.A0P(i >= 23 ? "RSA/ECB/OAEPPadding" : "RSA/ECB/PKCS1Padding", privateKeyEntry, A1a).toByteArray(), 0));
                return;
            }
            throw new IllegalArgumentException();
        } catch (Exception e) {
            r6.A08("onboarding", null, e);
            throw C117315Zl.A0J(e);
        }
    }

    public synchronized void A07(String str, String str2) {
        if (this.A00 != null) {
            SharedPreferences sharedPreferences = this.A02.A00;
            if (sharedPreferences.getBoolean("use_ks", false)) {
                try {
                    String A03 = A03("k0", "aes_k0", str);
                    str2 = A03("token", "aes_token", str2);
                    str = A03;
                } catch (Exception e) {
                    C12960it.A0t(sharedPreferences.edit(), "use_ks", false);
                    C30931Zj r3 = this.A01;
                    StringBuilder A0h = C12960it.A0h();
                    A0h.append(" Skip using Android KeyStore ");
                    r3.A08("onboarding", C12960it.A0d(e.toString(), A0h), e);
                }
            }
        }
        SharedPreferences sharedPreferences2 = this.A02.A00;
        C12970iu.A1D(sharedPreferences2.edit(), "k0", str);
        C12970iu.A1D(sharedPreferences2.edit(), "token", str2);
    }

    public final byte[] A08(byte[] bArr, String str) {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) this.A00.getEntry(str, null);
            int i = Build.VERSION.SDK_INT;
            if (i >= 18) {
                Cipher instance = Cipher.getInstance(i >= 23 ? "RSA/ECB/OAEPPadding" : "RSA/ECB/PKCS1Padding");
                instance.init(2, privateKeyEntry.getPrivateKey());
                CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(bArr), instance);
                ArrayList A0l = C12960it.A0l();
                while (true) {
                    int read = cipherInputStream.read();
                    if (read == -1) {
                        break;
                    }
                    A0l.add(Byte.valueOf((byte) read));
                }
                int size = A0l.size();
                byte[] bArr2 = new byte[size];
                for (int i2 = 0; i2 < size; i2++) {
                    bArr2[i2] = ((Byte) A0l.get(i2)).byteValue();
                }
                return bArr2;
            }
            throw new IllegalArgumentException();
        } catch (Exception e) {
            this.A01.A08("onboarding", null, e);
            throw C117315Zl.A0J(e);
        }
    }
}
