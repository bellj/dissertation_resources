package X;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;

/* renamed from: X.0RG  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0RG {
    public static final C04600Mi A00 = new C04600Mi();

    public static Activity A00(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return A00(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }
}
