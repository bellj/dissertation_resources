package X;

import com.whatsapp.chatinfo.ContactInfoActivity;
import com.whatsapp.jid.UserJid;

/* renamed from: X.3zB  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C84363zB extends AnonymousClass2Cu {
    public final /* synthetic */ ContactInfoActivity A00;

    public C84363zB(ContactInfoActivity contactInfoActivity) {
        this.A00 = contactInfoActivity;
    }

    @Override // X.AnonymousClass2Cu
    public void A01(UserJid userJid) {
        ContactInfoActivity contactInfoActivity = this.A00;
        if (userJid.equals(UserJid.getNullable(contactInfoActivity.getIntent().getStringExtra("jid"))) && contactInfoActivity.A00 != null) {
            contactInfoActivity.A3A(false, false);
        }
    }
}
