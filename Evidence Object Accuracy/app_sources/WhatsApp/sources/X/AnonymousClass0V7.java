package X;

import android.app.Dialog;
import android.content.DialogInterface;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.0V7  reason: invalid class name */
/* loaded from: classes.dex */
public class AnonymousClass0V7 implements DialogInterface.OnDismissListener {
    public final /* synthetic */ DialogFragment A00;

    public AnonymousClass0V7(DialogFragment dialogFragment) {
        this.A00 = dialogFragment;
    }

    @Override // android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        DialogFragment dialogFragment = this.A00;
        Dialog dialog = dialogFragment.A03;
        if (dialog != null) {
            dialogFragment.onDismiss(dialog);
        }
    }
}
