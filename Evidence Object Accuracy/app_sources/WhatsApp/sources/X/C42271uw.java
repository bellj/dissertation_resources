package X;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: X.1uw */
/* loaded from: classes2.dex */
public class C42271uw {
    public AnonymousClass1JB A00;
    public boolean A01;
    public boolean A02;
    public boolean A03;
    public boolean A04;
    public final AnonymousClass1JA A05;
    public final List A06 = new ArrayList();
    public final Set A07 = new HashSet();

    public C42271uw(AnonymousClass1JA r2) {
        this.A05 = r2;
    }

    public static /* synthetic */ void A00(C42271uw r2, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            r2.A02((byte[]) it.next());
        }
    }

    public C42251uu A01() {
        if (this.A00.A00()) {
            return new C42251uu(this);
        }
        throw new IllegalArgumentException("none of the syncs protocols enabled");
    }

    public void A02(byte[] bArr) {
        List<byte[]> list = this.A06;
        for (byte[] bArr2 : list) {
            if (Arrays.equals(bArr2, bArr)) {
                return;
            }
        }
        list.add(bArr);
    }
}
