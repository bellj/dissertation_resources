package X;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.whatsapp.util.Log;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: X.13z  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C240413z extends AbstractC16280ok {
    public final AbstractC15710nm A00;
    public final C231410n A01;

    public C240413z(AbstractC15710nm r8, C16590pI r9, C231410n r10, C14850m9 r11) {
        super(r9.A00, r8, "stickers.db", new ReentrantReadWriteLock(), 31, r11.A07(781));
        this.A00 = r8;
        this.A01 = r10;
    }

    public static final void A00(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS downloadable_sticker_packs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS stickers");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS installed_sticker_packs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS starred_stickers");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sticker_pack_order");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS recent_stickers");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS unseen_sticker_packs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS third_party_whitelist_packs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS new_sticker_packs");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS third_party_sticker_emoji_mapping");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sticker_md_upload");
    }

    public static final void A01(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append(str);
            sb.append(" ADD ");
            sb.append(str2);
            sb.append(" ");
            sb.append("INTEGER");
            sb.append(" ");
            sb.append(" NOT NULL DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        } catch (SQLiteException e) {
            StringBuilder sb2 = new StringBuilder("StickerDBHelper/addColumnIfNotExists/alter_table ");
            sb2.append(str2);
            Log.w(sb2.toString(), e);
        }
    }

    public static final void A02(SQLiteDatabase sQLiteDatabase, String str, String str2, String str3) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append(str);
            sb.append(" ADD ");
            sb.append(str2);
            sb.append(" ");
            sb.append(str3);
            sQLiteDatabase.execSQL(sb.toString());
        } catch (SQLiteException e) {
            StringBuilder sb2 = new StringBuilder("StickerDBHelper/addColumnIfNotExists/alter_table ");
            sb2.append(str2);
            Log.w(sb2.toString(), e);
        }
    }

    @Override // X.AbstractC16280ok
    public C16330op A03() {
        return AnonymousClass1Tx.A01(super.A00(), this.A01);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE stickers (plain_file_hash TEXT NOT NULL, encrypted_file_hash TEXT, media_key TEXT, mime_type TEXT, height INTEGER NOT NULL, width INTEGER NOT NULL, sticker_pack_id TEXT, file_path TEXT, url TEXT, file_size INTEGER, direct_path TEXT, emojis TEXT, hash_of_image_part TEXT, is_avatar INTEGER NOT NULL DEFAULT 0, PRIMARY KEY(plain_file_hash))");
        sQLiteDatabase.execSQL("CREATE TABLE downloadable_sticker_packs (id TEXT NOT NULL, name TEXT, publisher TEXT, description TEXT, size INTEGER NOT NULL, tray_image_id TEXT, tray_image_preview_id TEXT, preview_image_id_array TEXT, image_data_hash TEXT NOT NULL, animated_pack INTEGER NOT NULL, PRIMARY KEY(id))");
        sQLiteDatabase.execSQL("CREATE TABLE installed_sticker_packs (installed_id TEXT NOT NULL, installed_name TEXT, installed_publisher TEXT, installed_description TEXT, installed_size INTEGER NOT NULL, installed_image_data_hash TEXT NOT NULL, installed_tray_image_id TEXT NOT NULL, installed_tray_image_preview_id TEXT, installed_animated_pack INTEGER NOT NULL, installed_is_avatar_pack INTEGER NOT NULL DEFAULT 0, PRIMARY KEY(installed_id))");
        sQLiteDatabase.execSQL("CREATE TABLE starred_stickers (plaintext_hash TEXT  NOT NULL , timestamp LONG , hash_of_image_part TEXT , url TEXT , enc_hash TEXT , direct_path TEXT , mimetype TEXT , media_key TEXT , file_size INTEGER , width INTEGER , height INTEGER , emojis TEXT , is_first_party INTEGER , is_avatar INTEGER  NOT NULL  DEFAULT 0 , PRIMARY KEY(plaintext_hash))");
        sQLiteDatabase.execSQL("CREATE TABLE sticker_pack_order (sticker_pack_id TEXT PRIMARY KEY NOT NULL, pack_order INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE recent_stickers (plaintext_hash TEXT  NOT NULL , entry_weight FLOAT  NOT NULL , hash_of_image_part TEXT , url TEXT , enc_hash TEXT , direct_path TEXT , mimetype TEXT , media_key TEXT , file_size INTEGER , width INTEGER , height INTEGER , emojis TEXT , is_first_party INTEGER , is_avocado INTEGER  NOT NULL  DEFAULT 0 , PRIMARY KEY(plaintext_hash))");
        sQLiteDatabase.execSQL("CREATE TABLE unseen_sticker_packs (pack_id TEXT PRIMARY KEY NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE third_party_whitelist_packs (authority TEXT NOT NULL, sticker_pack_id TEXT NOT NULL, sticker_pack_name TEXT, sticker_pack_publisher TEXT, sticker_pack_image_data_hash TEXT, avoid_cache INTEGER, is_animated_pack INTEGER, PRIMARY KEY (authority,sticker_pack_id))");
        sQLiteDatabase.execSQL("CREATE TABLE new_sticker_packs (pack_id TEXT PRIMARY KEY NOT NULL)");
        sQLiteDatabase.execSQL("CREATE TABLE third_party_sticker_emoji_mapping (plaintext_hash TEXT  NOT NULL , authority TEXT  NOT NULL , sticker_pack_id TEXT  NOT NULL , emojis TEXT , hash_of_image_part TEXT , PRIMARY KEY(plaintext_hash))");
        sQLiteDatabase.execSQL("CREATE TABLE sticker_md_upload (plaintext_hash TEXT  NOT NULL , last_uploaded_time LONG , media_key TEXT  NOT NULL , direct_path TEXT , handle TEXT , is_favorite INTEGER , PRIMARY KEY(plaintext_hash))");
        try {
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS recent_sticker_is_avocado_index ON recent_stickers (is_avocado)");
        } catch (SQLiteException e) {
            AnonymousClass009.A08("StickersDBHelper/addRecentStickerAvocadoColumnIndex", e);
        }
        try {
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS starred_sticker_is_avatar_index ON starred_stickers (is_avatar)");
        } catch (SQLiteException e2) {
            AnonymousClass009.A08("StickersDBHelper/addStarredStickerAvocadoColumnIndex", e2);
        }
        try {
            sQLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS installed_sticker_packs_avatar_pack_index ON installed_sticker_packs (installed_is_avatar_pack)");
        } catch (SQLiteException e3) {
            AnonymousClass009.A08("StickerDBHelper/addInstalledIsAvatarStickerPackColumnIndex", e3);
        }
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        StringBuilder sb = new StringBuilder("StickerDBHelper/onDowngrade/oldVersion:");
        sb.append(i);
        sb.append(", newVersion:");
        sb.append(i2);
        Log.i(sb.toString());
        A00(sQLiteDatabase);
        onCreate(sQLiteDatabase);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0342, code lost:
        if (r3 != null) goto L_0x0344;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x01dc A[Catch: all -> 0x033b, TryCatch #3 {all -> 0x033b, blocks: (B:24:0x01cb, B:27:0x01d7, B:29:0x01dc, B:30:0x01e4, B:32:0x01ea, B:34:0x01f6, B:35:0x0217, B:36:0x023b, B:38:0x0241, B:39:0x0265, B:41:0x026f, B:43:0x0275, B:44:0x0289), top: B:82:0x01cb }] */
    @Override // android.database.sqlite.SQLiteOpenHelper
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onUpgrade(android.database.sqlite.SQLiteDatabase r32, int r33, int r34) {
        /*
        // Method dump skipped, instructions count: 1076
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C240413z.onUpgrade(android.database.sqlite.SQLiteDatabase, int, int):void");
    }
}
