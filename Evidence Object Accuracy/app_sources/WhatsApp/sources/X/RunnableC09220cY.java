package X;

import androidx.fragment.app.DialogFragment;

/* renamed from: X.0cY  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class RunnableC09220cY implements Runnable {
    public final /* synthetic */ DialogFragment A00;

    public RunnableC09220cY(DialogFragment dialogFragment) {
        this.A00 = dialogFragment;
    }

    @Override // java.lang.Runnable
    public void run() {
        DialogFragment dialogFragment = this.A00;
        dialogFragment.A05.onDismiss(dialogFragment.A03);
    }
}
