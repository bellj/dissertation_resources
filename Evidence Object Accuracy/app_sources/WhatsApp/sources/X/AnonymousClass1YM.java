package X;

import android.util.Base64;
import com.whatsapp.jid.DeviceJid;
import com.whatsapp.jid.Jid;
import com.whatsapp.jid.UserJid;
import com.whatsapp.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: X.1YM  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1YM {
    public String A00;
    public String A01;
    public Map A02 = new ConcurrentHashMap();
    public final AbstractC15590nW A03;
    public final Object A04 = new Object();
    public final Object A05 = new Object();
    public volatile boolean A06 = false;

    public AnonymousClass1YM(AbstractC15590nW r2) {
        AnonymousClass009.A05(r2);
        this.A03 = r2;
    }

    public static String A00(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            arrayList.add(((Jid) it.next()).getRawString());
        }
        Collections.sort(arrayList);
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                instance.update(((String) it2.next()).getBytes());
            }
            byte[] digest = instance.digest();
            byte[] bArr = new byte[6];
            System.arraycopy(digest, 0, bArr, 0, 6);
            StringBuilder sb = new StringBuilder("1:");
            sb.append(Base64.encodeToString(bArr, 2));
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static Set A01(Collection collection) {
        AnonymousClass009.A0F(!collection.isEmpty());
        HashSet hashSet = new HashSet(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            hashSet.add(new AnonymousClass1YP((DeviceJid) it.next(), false));
        }
        return hashSet;
    }

    public AnonymousClass1YO A02(UserJid userJid) {
        AnonymousClass1YO r0 = (AnonymousClass1YO) this.A02.remove(userJid);
        if (r0 != null) {
            A0D();
        }
        return r0;
    }

    public final AnonymousClass1YO A03(UserJid userJid, Collection collection, int i, boolean z, boolean z2) {
        AnonymousClass1YO r1 = (AnonymousClass1YO) this.A02.get(userJid);
        if (r1 != null) {
            r1.A01 = i;
            r1.A02 = z;
        } else {
            r1 = new AnonymousClass1YO(userJid, A01(collection), i, z);
            r1.A00 = this.A02.size();
            this.A02.put(userJid, r1);
            this.A06 = true;
            if (z2) {
                A0D();
                return r1;
            }
        }
        return r1;
    }

    public AnonymousClass1YN A04(AnonymousClass1JO r9, UserJid userJid, boolean z) {
        AnonymousClass1YO r0 = (AnonymousClass1YO) this.A02.get(userJid);
        boolean z2 = false;
        if (r0 == null) {
            StringBuilder sb = new StringBuilder("GroupParticipants/refreshDevices/participant ");
            sb.append(userJid);
            sb.append(" doesn't exist");
            Log.w(sb.toString());
            return new AnonymousClass1YN(false, false, false);
        }
        ConcurrentHashMap concurrentHashMap = r0.A04;
        AnonymousClass1JO A00 = AnonymousClass1JO.A00(concurrentHashMap.keySet());
        Iterator it = r9.iterator();
        boolean z3 = false;
        while (it.hasNext()) {
            DeviceJid deviceJid = (DeviceJid) it.next();
            if (!A00.A00.contains(deviceJid)) {
                this.A06 = true;
                AnonymousClass1YP r2 = new AnonymousClass1YP(deviceJid, false);
                DeviceJid deviceJid2 = r2.A01;
                if (!concurrentHashMap.containsKey(deviceJid2)) {
                    concurrentHashMap.put(deviceJid2, r2);
                }
                z3 = true;
            }
        }
        boolean z4 = !z;
        Iterator it2 = A00.iterator();
        boolean z5 = false;
        while (it2.hasNext()) {
            Object next = it2.next();
            if (!r9.A00.contains(next)) {
                AnonymousClass1YP r02 = (AnonymousClass1YP) concurrentHashMap.remove(next);
                if (r02 != null) {
                    z4 |= r02.A00;
                }
                z5 = true;
            }
        }
        if (z3 || z5) {
            A0D();
            if (z5 && z4) {
                z2 = true;
                A0E();
            }
        }
        return new AnonymousClass1YN(z3, z5, z2);
    }

    public AnonymousClass1YM A05(AnonymousClass03D r6) {
        AnonymousClass1YM r4 = new AnonymousClass1YM(this.A03);
        HashSet hashSet = new HashSet();
        Iterator it = A07().iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if (r6.Aek(next)) {
                hashSet.add(next);
            }
        }
        r4.A0F(hashSet);
        return r4;
    }

    public AnonymousClass1JO A06() {
        return AnonymousClass1JO.A00(this.A02.keySet());
    }

    public AnonymousClass1JO A07() {
        return AnonymousClass1JO.A00(this.A02.values());
    }

    public String A08() {
        String str;
        synchronized (this.A04) {
            str = this.A00;
            AnonymousClass009.A05(str);
        }
        return str;
    }

    public String A09() {
        String str;
        synchronized (this.A05) {
            str = this.A01;
            if (str == null) {
                str = A00(this.A02.keySet());
                this.A01 = str;
            }
        }
        return str;
    }

    public ArrayList A0A() {
        ArrayList arrayList = new ArrayList();
        for (AnonymousClass1YO r1 : this.A02.values()) {
            if (r1.A01 != 0) {
                arrayList.add(r1);
            }
        }
        return arrayList;
    }

    public Set A0B() {
        HashSet hashSet = new HashSet();
        for (Map.Entry entry : this.A02.entrySet()) {
            Iterator it = AnonymousClass1JO.A00(((AnonymousClass1YO) entry.getValue()).A04.values()).iterator();
            while (it.hasNext()) {
                hashSet.add(((AnonymousClass1YP) it.next()).A01);
            }
        }
        return hashSet;
    }

    public Set A0C(C15570nT r6) {
        HashSet hashSet = new HashSet();
        for (Map.Entry entry : this.A02.entrySet()) {
            Iterator it = AnonymousClass1JO.A00(((AnonymousClass1YO) entry.getValue()).A04.values()).iterator();
            while (it.hasNext()) {
                AnonymousClass1YP r1 = (AnonymousClass1YP) it.next();
                if (!r1.A00) {
                    DeviceJid deviceJid = r1.A01;
                    r6.A08();
                    if (!deviceJid.equals(r6.A04)) {
                        hashSet.add(deviceJid);
                    }
                }
            }
        }
        return hashSet;
    }

    public void A0D() {
        synchronized (this.A04) {
            this.A00 = AnonymousClass1YK.A00(A0B());
        }
        synchronized (this.A05) {
            if (this.A01 != null) {
                this.A01 = null;
            }
        }
    }

    public final void A0E() {
        for (AnonymousClass1YO r0 : this.A02.values()) {
            for (AnonymousClass1YP r1 : r0.A04.values()) {
                r1.A00 = false;
            }
        }
    }

    public void A0F(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            AnonymousClass1YO r1 = (AnonymousClass1YO) it.next();
            A03(r1.A03, new HashSet(AnonymousClass1JO.A00(r1.A04.keySet()).A00), r1.A01, r1.A02, false);
        }
        A0D();
    }

    public void A0G(Collection collection) {
        boolean z = false;
        for (Object obj : collection) {
            if (this.A02.remove(obj) != null) {
                z = true;
            }
        }
        if (z) {
            A0D();
        }
    }

    public boolean A0H(C15570nT r4) {
        r4.A08();
        C27631Ih r2 = r4.A05;
        AnonymousClass1MU A02 = r4.A02();
        if (r2 == null || !this.A02.containsKey(r2)) {
            return A02 != null && this.A02.containsKey(A02);
        }
        return true;
    }

    public boolean A0I(Collection collection) {
        for (Object obj : collection) {
            AnonymousClass1YO r0 = (AnonymousClass1YO) this.A02.get(obj);
            if (r0 != null) {
                Iterator it = AnonymousClass1JO.A00(r0.A04.values()).iterator();
                while (it.hasNext()) {
                    if (((AnonymousClass1YP) it.next()).A00) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            AnonymousClass1YM r5 = (AnonymousClass1YM) obj;
            if (this.A03.equals(r5.A03) && this.A02.equals(r5.A02)) {
                String str = this.A00;
                String str2 = r5.A00;
                if (str != null) {
                    return str.equals(str2);
                }
                if (str2 != null) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        int hashCode = ((this.A03.hashCode() * 31) + this.A02.hashCode()) * 31;
        String str = this.A00;
        return hashCode + (str != null ? str.hashCode() : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupParticipants{groupJid='");
        sb.append(this.A03);
        sb.append('\'');
        sb.append(", participants=");
        sb.append(this.A02);
        sb.append(", participantHash='");
        sb.append(this.A00);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
