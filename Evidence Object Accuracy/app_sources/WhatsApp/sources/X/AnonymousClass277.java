package X;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* renamed from: X.277  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass277<E> extends AnonymousClass1K7<E> {
    public static final AnonymousClass277 A01;
    public final List A00;

    static {
        AnonymousClass277 r1 = new AnonymousClass277(new ArrayList(10));
        A01 = r1;
        ((AnonymousClass1K7) r1).A00 = false;
    }

    public AnonymousClass277(List list) {
        this.A00 = list;
    }

    @Override // X.AnonymousClass1K6
    public /* bridge */ /* synthetic */ AnonymousClass1K6 ALZ(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.A00);
            return new AnonymousClass277(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @Override // java.util.AbstractList, java.util.List
    public void add(int i, Object obj) {
        A00();
        this.A00.add(i, obj);
        ((AbstractList) this).modCount++;
    }

    @Override // java.util.AbstractList, java.util.List
    public Object get(int i) {
        return this.A00.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public Object remove(int i) {
        A00();
        Object remove = this.A00.remove(i);
        ((AbstractList) this).modCount++;
        return remove;
    }

    @Override // java.util.AbstractList, java.util.List
    public Object set(int i, Object obj) {
        A00();
        Object obj2 = this.A00.set(i, obj);
        ((AbstractList) this).modCount++;
        return obj2;
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public int size() {
        return this.A00.size();
    }
}
