package X;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import com.whatsapp.R;

/* renamed from: X.1nk  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public abstract class AbstractC38231nk {
    public final AnonymousClass14X A00;

    public abstract void A02(Context context, View view, View view2, View view3, Button button, AnonymousClass1IR v, AnonymousClass1In v2, String str);

    public abstract void A03(Context context, View view, AnonymousClass1IR v);

    public AbstractC38231nk(AnonymousClass14X r1) {
        this.A00 = r1;
    }

    public String A00(AnonymousClass1IR r3) {
        return this.A00.A05.A00.getString(R.string.payment);
    }

    public String A01(AnonymousClass1IR r2) {
        return this.A00.A0H(r2);
    }
}
