package X;

import android.content.Context;
import com.whatsapp.payments.IDxRCallbackShape2S0100000_3_I1;
import com.whatsapp.util.Log;

/* renamed from: X.5fz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120325fz extends AbstractC124175oj {
    public final int A00;
    public final Context A01;
    public final C14900mE A02;
    public final C18640sm A03;
    public final AnonymousClass102 A04;
    public final C18650sn A05;
    public final C18610sj A06;
    public final C17070qD A07;
    public final C128585wL A08;
    public final C18590sh A09;
    public final AbstractC14440lR A0A;
    public final String A0B;
    public final String A0C;
    public final String A0D;
    public final String A0E;

    public C120325fz(Context context, C14900mE r9, C18640sm r10, AnonymousClass102 r11, C18650sn r12, C18600si r13, C18610sj r14, C17070qD r15, C128585wL r16, C18590sh r17, AnonymousClass1BY r18, C22120yY r19, AbstractC14440lR r20, String str, String str2, String str3, String str4, int i) {
        super(r10, r13, r14, r18, r19);
        this.A01 = context;
        this.A02 = r9;
        this.A0A = r20;
        this.A09 = r17;
        this.A07 = r15;
        this.A06 = r14;
        this.A04 = r11;
        this.A03 = r10;
        this.A05 = r12;
        this.A0B = str;
        this.A0D = str2;
        this.A0C = str3;
        this.A00 = i;
        this.A0E = str4;
        this.A08 = r16;
    }

    @Override // X.AbstractC16350or
    public /* bridge */ /* synthetic */ void A07(Object obj) {
        AnonymousClass01T r9 = (AnonymousClass01T) obj;
        String str = (String) r9.A00;
        C452120p r1 = (C452120p) r9.A01;
        if (str == null) {
            Log.e(C12960it.A0b("PAY: BrazilUpdateMerchantAccountAction token error: ", r1));
            this.A08.A00(r1);
            return;
        }
        C18610sj r3 = this.A06;
        AnonymousClass1W9[] r4 = new AnonymousClass1W9[7];
        C12960it.A1M("action", "br-update-merchant-account", r4, 0);
        C12960it.A1M("bank-token", str, r4, 1);
        C12960it.A1M("bank-code", this.A0D, r4, 2);
        C12960it.A1M("bank-branch", this.A0C, r4, 3);
        C12960it.A1M("bank-account-type", String.valueOf(this.A00), r4, 4);
        C12960it.A1M("device-id", this.A09.A01(), r4, 5);
        C12960it.A1M("nonce", this.A0E, r4, 6);
        C117305Zk.A1J(r3, new IDxRCallbackShape2S0100000_3_I1(this.A01, this.A02, this.A05, this, 6), C117315Zl.A0G(r4));
    }
}
