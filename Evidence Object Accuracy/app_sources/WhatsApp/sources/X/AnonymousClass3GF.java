package X;

import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.DialogFragment;

/* renamed from: X.3GF  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3GF {
    public static void A00(DialogFragment dialogFragment, C15370n3 r4) {
        Bundle A0D = C12970iu.A0D();
        A0D.putString("jid", C15380n4.A03(r4.A0D));
        dialogFragment.A0U(A0D);
    }

    public static boolean A01(AbstractC15340mz r6, AnonymousClass1YY r7) {
        if (r7 != null) {
            long j = r6.A0I;
            if (r6 instanceof C30331Wz) {
                j = ((C30331Wz) r6).A00;
            }
            AnonymousClass1Iv r3 = r7.A02;
            if (r3.A0I > j) {
                AnonymousClass1X9 r32 = (AnonymousClass1X9) r3;
                if (!TextUtils.isEmpty(r32.A01) && !"□".equals(r32.A01)) {
                    return true;
                }
            }
        }
        return false;
    }
}
