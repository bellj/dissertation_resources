package X;

import com.whatsapp.jid.DeviceJid;
import java.util.List;
import java.util.Map;

/* renamed from: X.1F8  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1F8 {
    public final AbstractC15710nm A00;
    public final C14900mE A01;
    public final C14650lo A02;
    public final AnonymousClass10A A03;
    public final C15550nR A04;
    public final C22700zV A05;
    public final C15650ng A06;
    public final C16120oU A07;
    public final C20660w7 A08;
    public final C22410z2 A09;

    public AnonymousClass1F8(AbstractC15710nm r1, C14900mE r2, C14650lo r3, AnonymousClass10A r4, C15550nR r5, C22700zV r6, C15650ng r7, C16120oU r8, C20660w7 r9, C22410z2 r10) {
        this.A01 = r2;
        this.A00 = r1;
        this.A07 = r8;
        this.A08 = r9;
        this.A04 = r5;
        this.A06 = r7;
        this.A05 = r6;
        this.A09 = r10;
        this.A02 = r3;
        this.A03 = r4;
    }

    public void A00(DeviceJid deviceJid, List list, Map map) {
        C32141bg r4;
        int i;
        int i2;
        String str;
        C32141bg r3;
        int i3;
        AnonymousClass1M2 r2 = (AnonymousClass1M2) map.get(deviceJid.getUserJid());
        AnonymousClass1M2 A00 = this.A05.A00(deviceJid.getUserJid());
        String str2 = null;
        if (A00 != null) {
            r4 = A00.A00();
        } else {
            r4 = null;
        }
        int A002 = C38301nr.A00(A00);
        if (A00 != null) {
            i = A00.A03;
            str2 = A00.A08;
        } else {
            i = 0;
        }
        if (r2 != null) {
            i2 = r2.A03;
            str = r2.A08;
            r3 = r2.A00();
            i3 = C38301nr.A00(r2);
        } else {
            i2 = i;
            str = str2;
            r3 = r4;
            i3 = A002;
        }
        list.add(deviceJid.getUserJid());
        this.A06.A0N(deviceJid.getUserJid(), new C42181un(r3, r4, str, str2, i2, i, i3, A002).A01());
    }
}
