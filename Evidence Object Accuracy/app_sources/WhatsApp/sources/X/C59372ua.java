package X;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.whatsapp.R;
import com.whatsapp.jid.UserJid;

/* renamed from: X.2ua  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C59372ua extends AbstractC75723kJ {
    public int A00;
    public final Button A01;
    public final LinearLayout A02;
    public final TextView A03;
    public final AnonymousClass12P A04;
    public final C15570nT A05;
    public final C15550nR A06;
    public final C22700zV A07;
    public final C15610nY A08;
    public final UserJid A09;

    public C59372ua(View view, AnonymousClass12P r3, C15570nT r4, C15550nR r5, C22700zV r6, C15610nY r7, UserJid userJid) {
        super(view);
        this.A05 = r4;
        this.A04 = r3;
        this.A02 = (LinearLayout) view.findViewById(R.id.catalog_list_footer_end_of_results);
        this.A03 = C12960it.A0J(view, R.id.catalog_list_footer_end_of_results_title);
        this.A08 = r7;
        this.A07 = r6;
        this.A06 = r5;
        this.A09 = userJid;
        this.A01 = (Button) view.findViewById(R.id.end_of_results_button);
    }

    @Override // X.AbstractC75723kJ
    public /* bridge */ /* synthetic */ void A09(AbstractC89244Jf r12) {
        String str;
        Context context;
        int i;
        View view = this.A0H;
        view.setVisibility(0);
        LinearLayout linearLayout = this.A02;
        linearLayout.setVisibility(8);
        Button button = this.A01;
        button.setVisibility(8);
        TextView textView = this.A03;
        textView.setVisibility(8);
        int i2 = this.A00;
        if (i2 != 1) {
            if (i2 == 2) {
                linearLayout.setVisibility(0);
                textView.setVisibility(0);
                context = view.getContext();
                i = R.string.catalog_server_error_retrieving_products;
            } else if (i2 != 3) {
                if (i2 == 4) {
                    linearLayout.setVisibility(0);
                    textView.setVisibility(0);
                    context = view.getContext();
                    i = R.string.catalog_error_retrieving_products;
                } else if (i2 == 5) {
                    linearLayout.setVisibility(8);
                    textView.setVisibility(8);
                    return;
                } else {
                    return;
                }
            }
            C12970iu.A19(context, textView, i);
            return;
        }
        C15570nT r0 = this.A05;
        UserJid userJid = this.A09;
        if (!r0.A0F(userJid)) {
            linearLayout.setVisibility(0);
            AnonymousClass1M2 A00 = this.A07.A00(userJid);
            if (A00 == null) {
                str = null;
            } else {
                str = A00.A08;
            }
            C15370n3 A0B = this.A06.A0B(userJid);
            Context context2 = view.getContext();
            Object[] objArr = new Object[1];
            if (AnonymousClass1US.A0C(str)) {
                str = this.A08.A04(A0B);
            }
            textView.setText(C12960it.A0X(context2, str, objArr, 0, R.string.business_product_catalog_end_of_results_title));
            C12970iu.A19(view.getContext(), button, R.string.business_product_catalog_end_of_results_button);
            button.setVisibility(0);
            textView.setVisibility(0);
            AbstractView$OnClickListenerC34281fs.A02(button, this, A0B, 15);
        }
    }
}
