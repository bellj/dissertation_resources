package X;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.redex.IDxCreatorShape0S0000000_I1;

/* renamed from: X.0Vl  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C06880Vl implements Parcelable {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape0S0000000_I1(25);
    public int A00;
    public int A01;
    public boolean A02;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public C06880Vl() {
    }

    public C06880Vl(C06880Vl r2) {
        this.A01 = r2.A01;
        this.A00 = r2.A00;
        this.A02 = r2.A02;
    }

    public C06880Vl(Parcel parcel) {
        this.A01 = parcel.readInt();
        this.A00 = parcel.readInt();
        this.A02 = parcel.readInt() != 1 ? false : true;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.A01);
        parcel.writeInt(this.A00);
        parcel.writeInt(this.A02 ? 1 : 0);
    }
}
