package X;

import android.util.Pair;

/* renamed from: X.4dC  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C95134dC {
    public static final byte[] A00 = "OpusHead".getBytes(C88814Hf.A05);

    public static Pair A00(C95304dT r4, int i) {
        r4.A0S(i + 8 + 4);
        r4.A0T(1);
        do {
        } while ((r4.A0C() & 128) == 128);
        int A01 = C95304dT.A01(r4, 2);
        if ((A01 & 128) != 0) {
            r4.A0T(2);
        }
        if ((A01 & 64) != 0) {
            r4.A0T(r4.A0F());
        }
        if ((A01 & 32) != 0) {
            r4.A0T(2);
        }
        r4.A0T(1);
        do {
        } while ((r4.A0C() & 128) == 128);
        String A02 = C95554dx.A02(r4.A0C());
        if ("audio/mpeg".equals(A02) || "audio/vnd.dts".equals(A02) || "audio/vnd.dts.hd".equals(A02)) {
            return Pair.create(A02, null);
        }
        r4.A0T(12);
        int A012 = C95304dT.A01(r4, 1);
        int i2 = A012 & 127;
        while ((A012 & 128) == 128) {
            A012 = r4.A0C();
            i2 = (i2 << 7) | (A012 & 127);
        }
        byte[] bArr = new byte[i2];
        r4.A0V(bArr, 0, i2);
        return Pair.create(A02, bArr);
    }

    public static Pair A01(C95304dT r19, int i, int i2) {
        int i3;
        int i4;
        int i5 = r19.A01;
        while (i5 - i < i2) {
            int A03 = C95304dT.A03(r19, i5);
            C95314dV.A02("childAtomSize should be positive", C12960it.A1U(A03));
            if (r19.A07() == 1936289382) {
                int i6 = i5 + 8;
                boolean z = false;
                AnonymousClass4SP r12 = null;
                String str = null;
                Integer num = null;
                int i7 = -1;
                int i8 = 0;
                while (i6 - i5 < A03) {
                    int A032 = C95304dT.A03(r19, i6);
                    int A07 = r19.A07();
                    if (A07 == 1718775137) {
                        num = Integer.valueOf(r19.A07());
                    } else if (A07 == 1935894637) {
                        r19.A0T(4);
                        str = r19.A0O(4);
                    } else if (A07 == 1935894633) {
                        i7 = i6;
                        i8 = A032;
                    }
                    i6 += A032;
                }
                if ("cenc".equals(str) || "cbc1".equals(str) || "cens".equals(str) || "cbcs".equals(str)) {
                    if (num != null) {
                        if (i7 != -1) {
                            z = true;
                        }
                        C95314dV.A02("schi atom is mandatory", z);
                        int i9 = i7 + 8;
                        while (true) {
                            byte[] bArr = null;
                            if (i9 - i7 >= i8) {
                                break;
                            }
                            int A033 = C95304dT.A03(r19, i9);
                            if (r19.A07() == 1952804451) {
                                int A072 = (r19.A07() >> 24) & 255;
                                r19.A0T(1);
                                if (A072 == 0) {
                                    r19.A0T(1);
                                    i3 = 0;
                                    i4 = 0;
                                } else {
                                    int A0C = r19.A0C();
                                    i3 = (A0C & 240) >> 4;
                                    i4 = A0C & 15;
                                }
                                boolean A1V = C12960it.A1V(r19.A0C(), 1);
                                int A0C2 = r19.A0C();
                                byte[] bArr2 = new byte[16];
                                r19.A0V(bArr2, 0, 16);
                                if (A1V && A0C2 == 0) {
                                    int A0C3 = r19.A0C();
                                    bArr = new byte[A0C3];
                                    r19.A0V(bArr, 0, A0C3);
                                }
                                r12 = new AnonymousClass4SP(str, bArr2, bArr, A0C2, i3, i4, A1V);
                            } else {
                                i9 += A033;
                            }
                        }
                        if (r12 != null) {
                            Pair create = Pair.create(num, r12);
                            if (create != null) {
                                return create;
                            }
                        } else {
                            throw C12960it.A0U(String.valueOf("tenc atom is mandatory"));
                        }
                    } else {
                        throw C12960it.A0U(String.valueOf("frma atom is mandatory"));
                    }
                }
            }
            i5 += A03;
        }
        return null;
    }

    public static C93844ap A02(String str, int i) {
        C93844ap r1 = new C93844ap();
        r1.A0O = Integer.toString(i);
        r1.A0R = str;
        return r1;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v225, resolved type: X.4SP[] */
    /* JADX DEBUG: Multi-variable search result rejected for r0v325, resolved type: X.4SP[] */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:334:0x06c3, code lost:
        if (r9 <= 0) goto L_0x06c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:591:0x0c61, code lost:
        if (r8 == 1) goto L_0x0c63;
     */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0368  */
    /* JADX WARNING: Removed duplicated region for block: B:458:0x08d6  */
    /* JADX WARNING: Removed duplicated region for block: B:520:0x0a93 A[LOOP:13: B:520:0x0a93->B:523:0x0a9b, LOOP_START, PHI: r4 r11 
      PHI: (r4v24 long) = (r4v20 long), (r4v26 long) binds: [B:519:0x0a91, B:523:0x0a9b] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r11v11 int) = (r11v9 int), (r11v13 int) binds: [B:519:0x0a91, B:523:0x0a9b] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:545:0x0b1d A[LOOP:15: B:545:0x0b1d->B:560:0x0b78, LOOP_START, PHI: r20 
      PHI: (r20v6 int) = (r20v5 int), (r20v7 int) binds: [B:544:0x0b1b, B:560:0x0b78] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:557:0x0b69  */
    /* JADX WARNING: Removed duplicated region for block: B:559:0x0b75  */
    /* JADX WARNING: Removed duplicated region for block: B:565:0x0bb9  */
    /* JADX WARNING: Removed duplicated region for block: B:567:0x0bd1  */
    /* JADX WARNING: Removed duplicated region for block: B:697:0x0b17 A[EDGE_INSN: B:697:0x0b17->B:543:0x0b17 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List A03(X.C112295Cv r44, X.C93974b3 r45, X.C76883mO r46, X.AbstractC469028d r47, long r48, boolean r50) {
        /*
        // Method dump skipped, instructions count: 3508
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C95134dC.A03(X.5Cv, X.4b3, X.3mO, X.28d, long, boolean):java.util.List");
    }
}
