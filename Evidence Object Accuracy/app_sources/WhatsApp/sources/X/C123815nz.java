package X;

import android.content.Context;
import android.text.Editable;
import com.whatsapp.mentions.MentionableEntry;

/* renamed from: X.5nz  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C123815nz extends C469928m {
    public final /* synthetic */ C134146Dm A00;

    public C123815nz(C134146Dm r1) {
        this.A00 = r1;
    }

    @Override // X.C469928m, android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        C134146Dm r0 = this.A00;
        Context context = r0.A0E;
        AnonymousClass19M r5 = r0.A0I;
        AnonymousClass01d r4 = r0.A0G;
        C16630pM r6 = r0.A0K;
        MentionableEntry mentionableEntry = r0.A09;
        AnonymousClass009.A03(mentionableEntry);
        C42971wC.A06(context, mentionableEntry.getPaint(), editable, r4, r5, r6);
    }
}
