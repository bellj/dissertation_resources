package X;

import android.util.JsonReader;
import android.util.JsonToken;

/* renamed from: X.3Ti  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C67903Ti implements AnonymousClass5XA {
    public Boolean A00;
    public String A01;
    public final JsonToken A02;

    public C67903Ti(JsonReader jsonReader) {
        JsonToken peek = jsonReader.peek();
        this.A02 = peek;
        int i = C88504Fx.A00[peek.ordinal()];
        if (i == 1) {
            this.A00 = Boolean.valueOf(jsonReader.nextBoolean());
        } else if (i == 2) {
            jsonReader.nextNull();
        } else if (i == 3 || i == 4) {
            this.A01 = jsonReader.nextString();
        } else {
            throw C12960it.A0U("can't read value");
        }
    }

    @Override // X.AnonymousClass5XA
    public boolean A6g() {
        Boolean bool = this.A00;
        if (bool != null) {
            return bool.booleanValue();
        }
        throw C12990iw.A0i(C12970iu.A0s(this.A02, C12960it.A0k("type mis matching")));
    }

    @Override // X.AnonymousClass5XA
    public boolean AJn() {
        return C12970iu.A1Z(this.A02, JsonToken.NULL);
    }

    @Override // X.AnonymousClass5XA
    public long AKo() {
        String str = this.A01;
        if (str != null) {
            return Long.valueOf(str).longValue();
        }
        throw C12990iw.A0i(C12970iu.A0s(this.A02, C12960it.A0k("type mis matching")));
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:10:0x0004 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.String */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.lang.Long, java.lang.Number] */
    @Override // X.AnonymousClass5XA
    public Number ALj() {
        String str = this.A01;
        if (str != 0) {
            try {
                str = Long.valueOf((String) str);
                return str;
            } catch (NumberFormatException unused) {
                return Double.valueOf(str);
            }
        } else {
            throw C12990iw.A0i(C12970iu.A0s(this.A02, C12960it.A0k("type mis matching")));
        }
    }

    @Override // X.AnonymousClass5XA
    public String AeX() {
        String str = this.A01;
        if (str != null) {
            return str;
        }
        throw C12990iw.A0i(C12970iu.A0s(this.A02, C12960it.A0k("type mis matching")));
    }
}
