package X;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: X.3h5  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C73963h5 extends Animation {
    public final int A00;
    public final int A01;
    public final View A02;
    public final /* synthetic */ AbstractView$OnCreateContextMenuListenerC35851ir A03;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public C73963h5(View view, AbstractView$OnCreateContextMenuListenerC35851ir r3, int i) {
        this.A03 = r3;
        this.A02 = view;
        this.A01 = i;
        this.A00 = view.getHeight();
    }

    @Override // android.view.animation.Animation
    public void applyTransformation(float f, Transformation transformation) {
        int i = this.A00;
        int i2 = i + ((int) (((float) (this.A01 - i)) * f));
        View view = this.A02;
        view.getLayoutParams().height = i2;
        view.requestLayout();
        AbstractView$OnCreateContextMenuListenerC35851ir r2 = this.A03;
        AbstractView$OnCreateContextMenuListenerC35851ir.A02(r2, (float) (i2 + r2.A0S.getHeight()), false);
    }
}
