package X;

import com.whatsapp.util.Log;
import java.io.IOException;
import org.json.JSONObject;

/* renamed from: X.3XX  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass3XX implements AbstractC44401yr {
    public final /* synthetic */ AnonymousClass23a A00;
    public final /* synthetic */ C18340sI A01;

    public AnonymousClass3XX(AnonymousClass23a r1, C18340sI r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // X.AbstractC44401yr
    public void A6t(AnonymousClass23Z r6) {
        C18340sI r4 = this.A01;
        AnonymousClass23a r3 = this.A00;
        AnonymousClass18A r1 = (AnonymousClass18A) r6.A02;
        if (r6.A00 == 0) {
            try {
                C457723b A01 = r1.A01((JSONObject) r1.A00);
                r4.A01(A01);
                Log.i("BanAppealRepository/clearFormReviewDraft");
                C12990iw.A11(C12960it.A08(r4.A04), "support_ban_appeal_form_review_draft");
                r3.AX3(A01);
            } catch (Exception unused) {
                r4.A00(r6, r3);
            }
        } else {
            r4.A00(r6, r3);
        }
    }

    @Override // X.AbstractC44401yr
    public void AOz(IOException iOException) {
        AnonymousClass23a r1 = this.A00;
        Integer A0g = C12970iu.A0g();
        if (r1 != null) {
            r1.AQB(A0g);
        }
    }

    @Override // X.AbstractC44401yr
    public void APp(Exception exc) {
        AnonymousClass23a r1 = this.A00;
        Integer A0g = C12970iu.A0g();
        if (r1 != null) {
            r1.AQB(A0g);
        }
    }
}
