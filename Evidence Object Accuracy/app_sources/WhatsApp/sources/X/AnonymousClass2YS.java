package X;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.os.Handler;

/* renamed from: X.2YS  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2YS extends AnimatorListenerAdapter {
    public final /* synthetic */ AnonymousClass1IO A00;
    public final /* synthetic */ AnonymousClass3BU A01;

    public AnonymousClass2YS(AnonymousClass1IO r1, AnonymousClass3BU r2) {
        this.A01 = r2;
        this.A00 = r1;
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        AnonymousClass3BU r2 = this.A01;
        ValueAnimator valueAnimator = r2.A03;
        if (!valueAnimator.isRunning() && r2.A05) {
            valueAnimator.start();
        }
    }

    @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        AnonymousClass1IO r0 = this.A00;
        Handler handler = r0.A00;
        if (handler == null) {
            handler = C12970iu.A0E();
            r0.A00 = handler;
        }
        handler.post(r0.A01);
    }
}
