package X;

import com.whatsapp.util.Log;

/* renamed from: X.1H5  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass1H5 extends AbstractC250017s {
    public final C250317v A00;
    public final C250217u A01;
    public final C233411h A02;
    public final C14830m7 A03;
    public final C19990v2 A04;

    public AnonymousClass1H5(C250317v r1, C250217u r2, C233411h r3, C14830m7 r4, C19990v2 r5, C233511i r6) {
        super(r6);
        this.A03 = r4;
        this.A04 = r5;
        this.A01 = r2;
        this.A02 = r3;
        this.A00 = r1;
    }

    public final void A08(C34671gW r9) {
        C19990v2 r0 = this.A04;
        AbstractC14640lm r5 = r9.A01;
        if (r0.A06(r5) != null) {
            Log.i("clear-chat-handler/clearChat deleteMessagesForRange");
            C250317v r7 = this.A00;
            boolean z = r9.A02;
            boolean z2 = r9.A03;
            C34361g1 r3 = r9.A00;
            int A00 = C34361g1.A00(r7.A02.A04(r5, true), r3);
            if (A00 == 2 || A00 == 1) {
                Log.i("DeleteMessagesForRange/clearChat use default service");
                if (z2) {
                    r7.A05.A02(r5, null);
                }
                C15650ng r1 = r7.A04;
                r1.A0Q(r5, true);
                r1.A0O(r5, null, true, z);
                return;
            }
            Log.i("DeleteMessagesForRange/clearChat use deleteMessages");
            r7.A01(r3, r5, z, z2);
        }
    }
}
