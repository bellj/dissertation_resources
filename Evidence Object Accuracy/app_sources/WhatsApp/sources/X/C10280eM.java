package X;

import java.util.Collection;
import java.util.Iterator;

/* renamed from: X.0eM  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public final class C10280eM implements Collection<V> {
    public final /* synthetic */ AbstractC008904n A00;

    public C10280eM(AbstractC008904n r1) {
        this.A00 = r1;
    }

    @Override // java.util.Collection
    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Collection
    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Collection
    public void clear() {
        this.A00.A06();
    }

    @Override // java.util.Collection
    public boolean contains(Object obj) {
        return ((C02470Ck) this.A00).A00.A04(obj) >= 0;
    }

    @Override // java.util.Collection
    public boolean containsAll(Collection collection) {
        for (Object obj : collection) {
            if (!contains(obj)) {
                return false;
            }
        }
        return true;
    }

    @Override // java.util.Collection
    public boolean isEmpty() {
        return this.A00.A01() == 0;
    }

    @Override // java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return new C009104p(this.A00, 1);
    }

    @Override // java.util.Collection
    public boolean remove(Object obj) {
        AbstractC008904n r1 = this.A00;
        int A04 = ((C02470Ck) r1).A00.A04(obj);
        if (A04 < 0) {
            return false;
        }
        r1.A07(A04);
        return true;
    }

    @Override // java.util.Collection
    public boolean removeAll(Collection collection) {
        AbstractC008904n r5 = this.A00;
        int A01 = r5.A01();
        int i = 0;
        boolean z = false;
        while (i < A01) {
            if (collection.contains(r5.A03(i, 1))) {
                r5.A07(i);
                i--;
                A01--;
                z = true;
            }
            i++;
        }
        return z;
    }

    @Override // java.util.Collection
    public boolean retainAll(Collection collection) {
        AbstractC008904n r5 = this.A00;
        int A01 = r5.A01();
        int i = 0;
        boolean z = false;
        while (i < A01) {
            if (!collection.contains(r5.A03(i, 1))) {
                r5.A07(i);
                i--;
                A01--;
                z = true;
            }
            i++;
        }
        return z;
    }

    @Override // java.util.Collection
    public int size() {
        return this.A00.A01();
    }

    @Override // java.util.Collection
    public Object[] toArray() {
        AbstractC008904n r5 = this.A00;
        int A01 = r5.A01();
        Object[] objArr = new Object[A01];
        for (int i = 0; i < A01; i++) {
            objArr[i] = r5.A03(i, 1);
        }
        return objArr;
    }

    @Override // java.util.Collection
    public Object[] toArray(Object[] objArr) {
        return this.A00.A08(objArr, 1);
    }
}
