package X;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: X.3pf  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C78863pf extends C98374ia implements IInterface {
    public C78863pf(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
    }
}
