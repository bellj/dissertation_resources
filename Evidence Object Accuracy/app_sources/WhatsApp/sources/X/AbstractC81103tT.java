package X;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;

/* renamed from: X.3tT  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC81103tT<E> extends AbstractC81113tU<E> implements NavigableSet<E>, AnonymousClass5Z0<E> {
    public final transient Comparator comparator;
    public transient AbstractC81103tT descendingSet;

    @Override // java.util.NavigableSet
    public abstract Object ceiling(Object obj);

    public abstract AbstractC81103tT createDescendingSet();

    @Override // java.util.NavigableSet
    public abstract /* bridge */ /* synthetic */ Iterator descendingIterator();

    @Override // java.util.SortedSet
    public abstract Object first();

    @Override // java.util.NavigableSet
    public abstract Object floor(Object obj);

    public abstract AbstractC81103tT headSetImpl(Object obj, boolean z);

    @Override // java.util.NavigableSet
    public abstract Object higher(Object obj);

    @Override // X.AbstractC17940re, X.AbstractC17950rf, java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.Set
    public abstract AnonymousClass1I5 iterator();

    @Override // java.util.SortedSet
    public abstract Object last();

    @Override // java.util.NavigableSet
    public abstract Object lower(Object obj);

    public abstract AbstractC81103tT subSetImpl(Object obj, boolean z, Object obj2, boolean z2);

    public abstract AbstractC81103tT tailSetImpl(Object obj, boolean z);

    public AbstractC81103tT(Comparator comparator) {
        this.comparator = comparator;
    }

    @Override // java.util.SortedSet, X.AnonymousClass5Z0
    public Comparator comparator() {
        return this.comparator;
    }

    public static AbstractC81103tT construct(Comparator comparator, int i, Object... objArr) {
        if (i == 0) {
            return emptySet(comparator);
        }
        C28331Mt.checkElementsNotNull(objArr, i);
        Arrays.sort(objArr, 0, i, comparator);
        int i2 = 1;
        for (int i3 = 1; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (comparator.compare(obj, objArr[i2 - 1]) != 0) {
                objArr[i2] = obj;
                i2++;
            }
        }
        Arrays.fill(objArr, i2, i, (Object) null);
        if (i2 < (objArr.length >> 1)) {
            objArr = Arrays.copyOf(objArr, i2);
        }
        return new C81093tS(AnonymousClass1Mr.asImmutableList(objArr, i2), comparator);
    }

    @Override // java.util.NavigableSet
    public AbstractC81103tT descendingSet() {
        AbstractC81103tT r0 = this.descendingSet;
        if (r0 != null) {
            return r0;
        }
        AbstractC81103tT createDescendingSet = createDescendingSet();
        this.descendingSet = createDescendingSet;
        createDescendingSet.descendingSet = this;
        return createDescendingSet;
    }

    public static C81093tS emptySet(Comparator comparator) {
        if (AbstractC112285Cu.natural().equals(comparator)) {
            return C81093tS.NATURAL_EMPTY_SET;
        }
        return new C81093tS(AnonymousClass1Mr.of(), comparator);
    }

    @Override // java.util.NavigableSet, java.util.SortedSet
    public AbstractC81103tT headSet(Object obj) {
        return headSet(obj, false);
    }

    @Override // java.util.NavigableSet
    public AbstractC81103tT headSet(Object obj, boolean z) {
        return headSetImpl(obj, z);
    }

    @Override // java.util.NavigableSet
    @Deprecated
    public final Object pollFirst() {
        throw C12970iu.A0z();
    }

    @Override // java.util.NavigableSet
    @Deprecated
    public final Object pollLast() {
        throw C12970iu.A0z();
    }

    private void readObject(ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @Override // java.util.NavigableSet, java.util.SortedSet
    public AbstractC81103tT subSet(Object obj, Object obj2) {
        return subSet(obj, true, obj2, false);
    }

    @Override // java.util.NavigableSet
    public AbstractC81103tT subSet(Object obj, boolean z, Object obj2, boolean z2) {
        if (this.comparator.compare(obj, obj2) <= 0) {
            return subSetImpl(obj, z, obj2, z2);
        }
        throw C72453ed.A0h();
    }

    @Override // java.util.NavigableSet, java.util.SortedSet
    public AbstractC81103tT tailSet(Object obj) {
        return tailSet(obj, true);
    }

    @Override // java.util.NavigableSet
    public AbstractC81103tT tailSet(Object obj, boolean z) {
        return tailSetImpl(obj, z);
    }

    public int unsafeCompare(Object obj, Object obj2) {
        return unsafeCompare(this.comparator, obj, obj2);
    }

    public static int unsafeCompare(Comparator comparator, Object obj, Object obj2) {
        return comparator.compare(obj, obj2);
    }

    @Override // X.AbstractC17940re, X.AbstractC17950rf
    public Object writeReplace() {
        return new AnonymousClass5BQ(this.comparator, toArray());
    }
}
