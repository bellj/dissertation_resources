package X;

import android.widget.TextView;
import com.whatsapp.payments.ui.widget.PaymentAmountInputField;

/* renamed from: X.6HX  reason: invalid class name */
/* loaded from: classes4.dex */
public final /* synthetic */ class AnonymousClass6HX implements Runnable {
    public final /* synthetic */ PaymentAmountInputField A00;

    public /* synthetic */ AnonymousClass6HX(PaymentAmountInputField paymentAmountInputField) {
        this.A00 = paymentAmountInputField;
    }

    @Override // java.lang.Runnable
    public final void run() {
        PaymentAmountInputField paymentAmountInputField = this.A00;
        TextView textView = paymentAmountInputField.A0A;
        if (textView != null) {
            textView.startAnimation(paymentAmountInputField.A09);
        }
    }
}
