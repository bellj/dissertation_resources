package X;

import android.text.InputFilter;
import android.text.Spanned;
import java.lang.ref.WeakReference;

/* renamed from: X.4mF  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C100644mF implements InputFilter {
    public final WeakReference A00;

    public C100644mF(AbstractC14670lq r2) {
        this.A00 = C12970iu.A10(r2);
    }

    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        AbstractC14670lq r0 = (AbstractC14670lq) this.A00.get();
        if (r0 == null || r0.A0P == null) {
            return null;
        }
        return "";
    }
}
