package X;

import java.net.InetAddress;

/* renamed from: X.1Zx  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31071Zx {
    public final long A00;
    public final InetAddress A01;

    public C31071Zx(InetAddress inetAddress, long j) {
        this.A01 = inetAddress;
        this.A00 = j;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("InetAddressWithExpiry{address=");
        sb.append(this.A01);
        sb.append(", expireTimeMillis=");
        sb.append(this.A00);
        sb.append('}');
        return sb.toString();
    }
}
