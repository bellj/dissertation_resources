package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.whatsapp.util.Log;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: X.6F2  reason: invalid class name */
/* loaded from: classes4.dex */
public class AnonymousClass6F2 implements Comparable, Parcelable {
    public static final Parcelable.Creator CREATOR = C117315Zl.A07(15);
    public final AbstractC30791Yv A00;
    public final C30821Yy A01;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AnonymousClass6F2(AbstractC30791Yv r1, C30821Yy r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    public static AnonymousClass6F2 A00(AnonymousClass102 r7, AnonymousClass1V8 r8) {
        long A07;
        AbstractC30791Yv A02;
        AnonymousClass1V8 A0E = r8.A0E("money");
        if (A0E != null) {
            String A0H = A0E.A0H("currency");
            long A072 = A0E.A07("offset");
            long A073 = A0E.A07("value");
            A02 = r7.A02(A0H);
            A07 = new BigDecimal(Double.toString(((double) A073) / ((double) A072))).movePointRight(C117315Zl.A00((AbstractC30781Yu) A02)).longValue();
        } else {
            A07 = r8.A07("amount");
            String A0W = C117295Zj.A0W(r8, "iso_code");
            if (TextUtils.isEmpty(A0W)) {
                A0W = r8.A0H("iso-code");
            }
            A02 = r7.A02(A0W);
        }
        AbstractC30781Yu r3 = (AbstractC30781Yu) A02;
        return C117305Zk.A0R(A02, BigDecimal.valueOf(A07, C117315Zl.A00(r3)), r3.A01);
    }

    public static AnonymousClass6F2 A01(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return A02(C13000ix.A05(str));
        } catch (JSONException unused) {
            Log.w("PAY: CurrencyAmount fromJsonString threw exception");
            return null;
        }
    }

    public static AnonymousClass6F2 A02(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        long optLong = jSONObject.optLong("amount", 0);
        jSONObject.optString("iso-code", "");
        AbstractC30791Yv A01 = AnonymousClass102.A01(jSONObject.optJSONObject("currency"), jSONObject.optInt("currencyType", -1));
        AbstractC30781Yu r2 = (AbstractC30781Yu) A01;
        return C117305Zk.A0R(A01, BigDecimal.valueOf(optLong, C117315Zl.A00(r2)), r2.A01);
    }

    /* renamed from: A03 */
    public int compareTo(AnonymousClass6F2 r6) {
        AbstractC30791Yv r2 = r6.A00;
        String str = ((AbstractC30781Yu) r2).A04;
        AbstractC30791Yv r1 = this.A00;
        if (C117305Zk.A1V(r1, str)) {
            return (C130325zE.A00(r1, this.A01) > C130325zE.A00(r2, r6.A01) ? 1 : (C130325zE.A00(r1, this.A01) == C130325zE.A00(r2, r6.A01) ? 0 : -1));
        }
        throw C12970iu.A0f("Can't compare two varying currency amounts");
    }

    public AnonymousClass6F2 A04(AnonymousClass6F2 r5) {
        String str = ((AbstractC30781Yu) r5.A00).A04;
        AbstractC30791Yv r3 = this.A00;
        AbstractC30781Yu r2 = (AbstractC30781Yu) r3;
        if (str.equals(r2.A04)) {
            return C117305Zk.A0R(r3, this.A01.A00.add(r5.A01.A00), r2.A01);
        }
        throw C12970iu.A0f("Can't subtract two varying currency amounts");
    }

    public AnonymousClass6F2 A05(C127695uu r5) {
        BigDecimal multiply;
        int A00;
        BigDecimal bigDecimal = this.A01.A00;
        int i = r5.A00;
        BigDecimal bigDecimal2 = r5.A02;
        if (i == 0) {
            multiply = bigDecimal.divide(bigDecimal2, C117315Zl.A00((AbstractC30781Yu) r5.A01), RoundingMode.HALF_EVEN);
        } else {
            multiply = bigDecimal.multiply(bigDecimal2);
        }
        AbstractC30791Yv r2 = r5.A01;
        AbstractC30781Yu r0 = (AbstractC30781Yu) r2;
        if (r5.A03) {
            A00 = r0.A01;
        } else {
            A00 = C117315Zl.A00(r0);
        }
        return C117305Zk.A0R(r2, multiply, A00);
    }

    public String A06(AnonymousClass018 r4) {
        return this.A00.AAA(r4, this.A01, 0);
    }

    public JSONObject A07() {
        JSONObject A0a = C117295Zj.A0a();
        try {
            AbstractC30791Yv A01 = C130325zE.A01(this, "amount", A0a);
            AbstractC30781Yu r2 = (AbstractC30781Yu) A01;
            A0a.put("iso-code", r2.A04);
            A0a.put("currencyType", r2.A00);
            A0a.put("currency", A01.Aew());
            return A0a;
        } catch (JSONException unused) {
            Log.w("PAY: TransactionAmount toJson threw exception");
            return A0a;
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AnonymousClass6F2)) {
            return false;
        }
        AnonymousClass6F2 r4 = (AnonymousClass6F2) obj;
        if (!C117305Zk.A1V(r4.A00, ((AbstractC30781Yu) this.A00).A04) || !this.A01.equals(r4.A01)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (this.A00.hashCode() * 31) + (this.A01.hashCode() * 31);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        this.A00.writeToParcel(parcel, i);
        parcel.writeParcelable(this.A01, i);
    }
}
