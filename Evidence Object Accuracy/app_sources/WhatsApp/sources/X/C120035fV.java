package X;

import java.io.File;

/* renamed from: X.5fV  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes4.dex */
public class C120035fV extends AbstractC38591oM {
    public final C126655tE A00;
    public final File A01;

    public C120035fV(C15450nH r10, C18790t3 r11, C14950mJ r12, C14850m9 r13, C20110vE r14, C126655tE r15, C22600zL r16, File file) {
        super(r10, r11, r12, r13, r14, r16, null);
        this.A00 = r15;
        this.A01 = file;
    }

    @Override // X.AbstractRunnableC38601oN
    public C28781Oz A00(AnonymousClass1RN r2) {
        return new C28781Oz();
    }

    @Override // X.AbstractRunnableC38601oN
    public /* bridge */ /* synthetic */ Object A02() {
        C126655tE r0 = this.A00;
        return new C93644aV(C14370lK.A08, new C37751mw(r0.A01, r0.A00), this.A01);
    }
}
