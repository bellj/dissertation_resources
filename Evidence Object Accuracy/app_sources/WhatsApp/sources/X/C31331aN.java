package X;

import com.whatsapp.util.Log;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Iterator;

@Deprecated
/* renamed from: X.1aN  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public final class C31331aN {
    public final C29291Rp A00;
    public final C15990oG A01;
    public final C18240s8 A02;

    public C31331aN(C29291Rp r1, C15990oG r2, C18240s8 r3) {
        this.A02 = r3;
        this.A01 = r2;
        this.A00 = r1;
    }

    public C29311Rr A00(C31351aP r5) {
        WeakReference weakReference = this.A02.A01;
        if (weakReference != null) {
            weakReference.get();
        }
        AnonymousClass009.A05(r5);
        C29301Rq A01 = this.A00.A01(new C15980oF(C29201Rg.A00(r5.A01), r5.A00));
        try {
            if (A01 == null) {
                return new C29311Rr();
            }
            return new C29311Rr(A01.A01);
        } catch (IOException e) {
            Log.w("axolotl ioexception while reading sender key record", e);
            return new C29311Rr();
        }
    }

    public void A01(C31351aP r9, C29311Rr r10) {
        C15990oG r6 = this.A01;
        C15980oF r7 = new C15980oF(C29201Rg.A00(r9.A01), r9.A00);
        AnonymousClass1G4 A0T = C32081ba.A01.A0T();
        Iterator it = r10.A00.iterator();
        while (it.hasNext()) {
            C32101bc r3 = ((C32091bb) it.next()).A00;
            A0T.A03();
            C32081ba r2 = (C32081ba) A0T.A00;
            AnonymousClass1K6 r1 = r2.A00;
            if (!((AnonymousClass1K7) r1).A00) {
                r1 = AbstractC27091Fz.A0G(r1);
                r2.A00 = r1;
            }
            r1.add(r3);
        }
        r6.A0d(r7, A0T.A02().A02());
    }
}
