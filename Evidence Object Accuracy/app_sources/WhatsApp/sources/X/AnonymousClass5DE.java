package X;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/* renamed from: X.5DE  reason: invalid class name */
/* loaded from: classes3.dex */
public class AnonymousClass5DE implements Iterator {
    public Map.Entry entry;
    public final /* synthetic */ C81133tW this$1;
    public final /* synthetic */ Iterator val$entryIterator;

    public AnonymousClass5DE(C81133tW r1, Iterator it) {
        this.this$1 = r1;
        this.val$entryIterator = it;
    }

    @Override // java.util.Iterator
    public boolean hasNext() {
        return this.val$entryIterator.hasNext();
    }

    @Override // java.util.Iterator
    public Object next() {
        Map.Entry A15 = C12970iu.A15(this.val$entryIterator);
        this.entry = A15;
        return A15.getKey();
    }

    @Override // java.util.Iterator
    public void remove() {
        C28291Mn.A05("no calls to next() since the last call to remove()", C12960it.A1W(this.entry));
        Collection collection = (Collection) this.entry.getValue();
        this.val$entryIterator.remove();
        AbstractC80933tC.access$220(this.this$1.this$0, collection.size());
        collection.clear();
        this.entry = null;
    }
}
