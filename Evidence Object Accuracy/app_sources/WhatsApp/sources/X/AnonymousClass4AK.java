package X;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: X.4AK  reason: invalid class name */
/* loaded from: classes3.dex */
public final class AnonymousClass4AK extends Enum {
    public static final /* synthetic */ AnonymousClass4AK[] A00;
    public static final AnonymousClass4AK A01;
    public static final AnonymousClass4AK A02;
    public static final AnonymousClass4AK A03;

    public static AnonymousClass4AK valueOf(String str) {
        return (AnonymousClass4AK) Enum.valueOf(AnonymousClass4AK.class, str);
    }

    public static AnonymousClass4AK[] values() {
        return (AnonymousClass4AK[]) A00.clone();
    }

    static {
        AnonymousClass4AK r4 = new AnonymousClass4AK("AUTO", 0);
        A01 = r4;
        AnonymousClass4AK r3 = new AnonymousClass4AK("HORIZONTAL", 1);
        A02 = r3;
        AnonymousClass4AK r1 = new AnonymousClass4AK("VERTICAL", 2);
        A03 = r1;
        AnonymousClass4AK[] r0 = new AnonymousClass4AK[3];
        C12970iu.A1U(r4, r3, r0);
        r0[2] = r1;
        A00 = r0;
    }

    public AnonymousClass4AK(String str, int i) {
    }
}
