package X;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.facebook.profilo.logger.MultiBufferLogger;
import com.facebook.redex.RunnableBRunnable0Shape0S1201000_I0;
import com.facebook.redex.RunnableBRunnable0Shape9S0100000_I0_9;
import com.google.android.search.verification.client.SearchActionVerificationClientService;

/* renamed from: X.0mm  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C15230mm {
    public int A00 = -1;
    public int A01 = 1;
    public AnonymousClass00E A02;
    public AnonymousClass1Q5 A03;
    public C29431Sq A04;
    public C29421Sp A05;
    public Long A06;
    public String A07;
    public boolean A08;
    public boolean A09;
    public final Handler A0A = new Handler(Looper.getMainLooper());
    public final AnonymousClass126 A0B;
    public final C15450nH A0C;
    public final C14830m7 A0D;
    public final C16590pI A0E;
    public final AnonymousClass018 A0F;
    public final C14850m9 A0G;
    public final C16120oU A0H;
    public final AnonymousClass006 A0I;
    public final C20420vj A0J;
    public final AnonymousClass00E A0K = new AnonymousClass00E(1, 200, 1000);
    public final AnonymousClass00E A0L = new AnonymousClass00E(10, 2000, SearchActionVerificationClientService.NOTIFICATION_ID);
    public final AnonymousClass00E A0M = new AnonymousClass00E(10, 1000, 25000);
    public final AnonymousClass00E A0N = new AnonymousClass00E(100, SearchActionVerificationClientService.NOTIFICATION_ID, 250000);
    public final AnonymousClass00E A0O = new AnonymousClass00E(100, SearchActionVerificationClientService.NOTIFICATION_ID, 250000);
    public final AnonymousClass00E A0P = new AnonymousClass00E(10, 2000, SearchActionVerificationClientService.NOTIFICATION_ID);
    public final C002801g A0Q;
    public final C21230x5 A0R;
    public final AbstractC21180x0 A0S;
    public final AbstractC14440lR A0T;

    public C15230mm(AnonymousClass126 r7, C15450nH r8, C14830m7 r9, C16590pI r10, AnonymousClass018 r11, C14850m9 r12, C16120oU r13, AnonymousClass006 r14, C20420vj r15, C002801g r16, C21230x5 r17, AbstractC21180x0 r18, AbstractC14440lR r19) {
        this.A0D = r9;
        this.A0G = r12;
        this.A0E = r10;
        this.A0T = r19;
        this.A0B = r7;
        this.A0H = r13;
        this.A0C = r8;
        this.A0I = r14;
        this.A0F = r11;
        this.A0J = r15;
        this.A0Q = r16;
        this.A0S = r18;
        this.A0R = r17;
    }

    public void A00() {
        C003401m.A01("wa_startup_complete");
        C003401m.A00();
        AnonymousClass1Q5 r1 = this.A03;
        if (r1 != null) {
            r1.A07("render");
            AnonymousClass1Q5 r3 = this.A03;
            if (r3 != null) {
                r3.A0A("locale", this.A0F.A07(), true);
                this.A03.A0C(2);
            }
        }
    }

    public void A01() {
        AnonymousClass1SW.A00();
        A02();
        this.A0A.post(new RunnableBRunnable0Shape9S0100000_I0_9(this, 25));
        this.A02 = this.A0K;
        this.A01 = 1;
        A03(24772609, "AppInit");
    }

    public final void A02() {
        AnonymousClass1Q5 r2 = this.A03;
        if (r2 == null) {
            AnonymousClass1Q5 r0 = new AnonymousClass1Q5(this.A0D, this.A0H, this.A0R, this.A0S, "StartupTracker", 703928054);
            this.A03 = r0;
            r0.A06.A03 = true;
            return;
        }
        r2.A0B("is_object_already_create", true, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void A03(int r40, java.lang.String r41) {
        /*
        // Method dump skipped, instructions count: 1326
        */
        throw new UnsupportedOperationException("Method not decompiled: X.C15230mm.A03(int, java.lang.String):void");
    }

    public void A04(View view, Runnable runnable, String str, int i) {
        RunnableBRunnable0Shape0S1201000_I0 runnableBRunnable0Shape0S1201000_I0 = new RunnableBRunnable0Shape0S1201000_I0(this, runnable, str, i, 3);
        view.getViewTreeObserver().addOnPreDrawListener(new AnonymousClass1SV(this.A0A, view, runnableBRunnable0Shape0S1201000_I0));
    }

    public void A05(View view, Runnable runnable, String str, int i) {
        if (this.A00 != -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" onRestart");
            A0A(sb.toString());
        } else if (!this.A08) {
            this.A08 = true;
            A02();
            A04(view, runnable, str, i);
            this.A01 = 2;
            this.A02 = this.A0L;
            A03(24772611, str);
        }
    }

    public void A06(String str) {
        AnonymousClass1ST r2;
        int i = this.A00;
        if (i != -1) {
            if (this.A09 && AnonymousClass1SS.A00 == 2 && (r2 = AnonymousClass1ST.A0B) != null) {
                r2.A03(AnonymousClass1SU.A02, 0, 2, (long) i);
            }
            if (this.A03 != null && this.A0G.A07(1807)) {
                this.A03.A0A("abort_reason", str, true);
            }
            AnonymousClass1Q5 r3 = this.A03;
            if (r3 != null) {
                r3.A0A("locale", this.A0F.A07(), true);
                this.A03.A0C(105);
            }
            this.A00 = -1;
            this.A07 = null;
            this.A09 = false;
            this.A06 = null;
        }
    }

    public void A07(String str) {
        AnonymousClass1Q5 r0 = this.A03;
        if (r0 != null) {
            r0.A07(str);
        }
    }

    public void A08(String str) {
        AnonymousClass1Q5 r0 = this.A03;
        if (r0 != null) {
            r0.A08(str);
        }
    }

    public void A09(String str) {
        if (this.A00 != -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" onCreate");
            A0A(sb.toString());
        } else if (!this.A08) {
            this.A08 = true;
            A02();
            this.A01 = 3;
            this.A02 = this.A0P;
            A03(24772610, str);
        }
    }

    public void A0A(String str) {
        Long l;
        int i = this.A00;
        if (i != -1) {
            if (this.A09 && (l = this.A06) != null) {
                C29421Sp r0 = this.A05;
                long longValue = l.longValue();
                MultiBufferLogger multiBufferLogger = r0.A00;
                multiBufferLogger.writeBytesEntry(1, 83, multiBufferLogger.writeStandardEntry(7, 50, 0, 0, i, 0, longValue), str);
            }
            AnonymousClass1Q5 r02 = this.A03;
            if (r02 != null) {
                r02.A09(str);
            }
        }
    }
}
