package X;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import com.facebook.redex.IDxCreatorShape1S0000000_2_I1;

/* renamed from: X.2ap  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C52452ap extends View.BaseSavedState {
    public static final Parcelable.Creator CREATOR = new IDxCreatorShape1S0000000_2_I1(42);
    public long A00;
    public long A01;
    public boolean A02;

    public /* synthetic */ C52452ap(Parcel parcel) {
        super(parcel);
        this.A01 = (long) parcel.readInt();
        this.A00 = (long) parcel.readInt();
        this.A02 = C12970iu.A1W(parcel.readInt());
    }

    public C52452ap(Parcelable parcelable) {
        super(parcelable);
    }

    @Override // android.view.View.BaseSavedState, android.os.Parcelable, android.view.AbsSavedState
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(this.A01);
        parcel.writeLong(this.A00);
        parcel.writeInt(this.A02 ? 1 : 0);
    }
}
