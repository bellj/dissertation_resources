package X;

import android.os.Parcel;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.nio.ByteBuffer;

/* renamed from: X.3no  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C77763no extends AnonymousClass1UI {
    public final C78613pC A00;

    public C77763no(C78613pC r2, AnonymousClass1U8 r3) {
        super(C93784aj.A0B, r3);
        this.A00 = r2;
    }

    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ AnonymousClass5SX A02(Status status) {
        return status;
    }

    @Override // X.AnonymousClass1UI
    public final /* synthetic */ void A07(AnonymousClass5QV r8) {
        AbstractC95064d1 r82 = (AbstractC95064d1) r8;
        BinderC79513qi r6 = new BinderC79513qi(this);
        try {
            C78613pC r5 = this.A00;
            C79493qg r3 = r5.A08;
            int A03 = r3.A03();
            ((AbstractC94974cq) r3).A00 = A03;
            byte[] bArr = new byte[A03];
            try {
                C95484do r0 = new C95484do(bArr, A03);
                r3.A05(r0);
                ByteBuffer byteBuffer = r0.A02;
                if (byteBuffer.remaining() == 0) {
                    r5.A02 = bArr;
                    C98454ii r1 = (C98454ii) ((AnonymousClass5Y9) r82.A03());
                    Parcel obtain = Parcel.obtain();
                    obtain.writeInterfaceToken(r1.A01);
                    obtain.writeStrongBinder(r6.asBinder());
                    obtain.writeInt(1);
                    r5.writeToParcel(obtain, 0);
                    try {
                        r1.A00.transact(1, obtain, null, 1);
                    } finally {
                        obtain.recycle();
                    }
                } else {
                    Object[] objArr = new Object[1];
                    C12960it.A1P(objArr, byteBuffer.remaining(), 0);
                    throw C12960it.A0U(String.format("Did not write as much data as expected, %s bytes remaining.", objArr));
                }
            } catch (IOException e) {
                throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
            }
        } catch (RuntimeException e2) {
            Log.e("ClearcutLoggerApiImpl", "derived ClearcutLogger.MessageProducer ", e2);
            A09(new Status(10, "MessageProducer"));
        }
    }
}
