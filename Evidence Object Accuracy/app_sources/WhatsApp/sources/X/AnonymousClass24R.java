package X;

import android.net.Uri;
import java.lang.ref.WeakReference;
import java.util.List;

/* renamed from: X.24R  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass24R extends AbstractC16350or {
    public final Uri A00;
    public final C14330lG A01;
    public final C14900mE A02;
    public final C16170oZ A03;
    public final AnonymousClass01d A04;
    public final C16590pI A05;
    public final AnonymousClass018 A06;
    public final C14850m9 A07;
    public final C16120oU A08;
    public final C14410lO A09;
    public final AbstractC15340mz A0A;
    public final C26511Dt A0B;
    public final C22190yg A0C;
    public final String A0D;
    public final String A0E;
    public final WeakReference A0F;
    public final List A0G;
    public final List A0H;
    public final boolean A0I;

    public /* synthetic */ AnonymousClass24R(Uri uri, AbstractC13860kS r3, C14330lG r4, C14900mE r5, C16170oZ r6, AnonymousClass01d r7, C16590pI r8, AnonymousClass018 r9, C14850m9 r10, C16120oU r11, C14410lO r12, AbstractC15340mz r13, C26511Dt r14, C22190yg r15, String str, String str2, List list, List list2, boolean z) {
        this.A07 = r10;
        this.A02 = r5;
        this.A05 = r8;
        this.A01 = r4;
        this.A08 = r11;
        this.A03 = r6;
        this.A09 = r12;
        this.A0B = r14;
        this.A0C = r15;
        this.A04 = r7;
        this.A06 = r9;
        this.A0G = list;
        this.A00 = uri;
        this.A0E = str;
        this.A0A = r13;
        this.A0F = new WeakReference(r3);
        this.A0I = z;
        this.A0D = str2;
        this.A0H = list2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x025c A[Catch: Exception -> 0x0292, TryCatch #9 {Exception -> 0x0292, blocks: (B:2:0x0000, B:4:0x0032, B:6:0x0038, B:7:0x003c, B:10:0x0066, B:13:0x0076, B:16:0x0080, B:19:0x008a, B:22:0x0094, B:25:0x009e, B:28:0x00a8, B:29:0x00ac, B:31:0x00bd, B:33:0x00ca, B:34:0x00db, B:36:0x0101, B:38:0x0109, B:39:0x0115, B:42:0x011f, B:54:0x015e, B:59:0x0185, B:61:0x0187, B:62:0x018a, B:63:0x018b, B:65:0x0193, B:72:0x01c6, B:77:0x01ec, B:79:0x01ee, B:80:0x01f3, B:81:0x01f4, B:84:0x01fe, B:91:0x021b, B:97:0x0242, B:99:0x0244, B:100:0x0247, B:102:0x0249, B:103:0x024c, B:104:0x0256, B:106:0x025c, B:108:0x0266, B:109:0x0271, B:112:0x0278, B:113:0x027b, B:115:0x027d, B:116:0x0285, B:117:0x0286, B:118:0x0291), top: B:137:0x0000, inners: #13, #15 }] */
    @Override // X.AbstractC16350or
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* bridge */ /* synthetic */ java.lang.Object A05(java.lang.Object[] r25) {
        /*
        // Method dump skipped, instructions count: 660
        */
        throw new UnsupportedOperationException("Method not decompiled: X.AnonymousClass24R.A05(java.lang.Object[]):java.lang.Object");
    }
}
