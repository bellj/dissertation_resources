package X;

/* renamed from: X.2J1  reason: invalid class name */
/* loaded from: classes2.dex */
public class AnonymousClass2J1 implements AnonymousClass2J2 {
    public final /* synthetic */ C48302Fl A00;

    public AnonymousClass2J1(C48302Fl r1) {
        this.A00 = r1;
    }

    @Override // X.AnonymousClass2J2
    public C59432ui A83(C48122Ek r15, AnonymousClass2K1 r16, AnonymousClass1B4 r17) {
        AnonymousClass01J r1 = this.A00.A03;
        C14900mE r3 = (C14900mE) r1.A8X.get();
        AbstractC15710nm r2 = (AbstractC15710nm) r1.A4o.get();
        C16590pI r9 = (C16590pI) r1.AMg.get();
        AnonymousClass018 r11 = (AnonymousClass018) r1.ANb.get();
        C16340oq r8 = (C16340oq) r1.A5z.get();
        C17170qN r10 = (C17170qN) r1.AMt.get();
        return new C59432ui(r2, r3, (C16430p0) r1.A5y.get(), r15, r16, r17, r8, r9, r10, r11, (C14850m9) r1.A04.get(), (AbstractC14440lR) r1.ANe.get());
    }
}
