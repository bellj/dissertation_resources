package X;

/* renamed from: X.5Eh  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public abstract class AbstractC112675Eh implements AnonymousClass5ZU {
    public final AbstractC115495Rt key;

    public AbstractC112675Eh(AbstractC115495Rt r2) {
        C16700pc.A0E(r2, 1);
        this.key = r2;
    }

    @Override // X.AnonymousClass5X4
    public Object fold(Object obj, AnonymousClass5ZQ r3) {
        C16700pc.A0E(r3, 2);
        return r3.AJ5(obj, this);
    }

    @Override // X.AnonymousClass5ZU, X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r2) {
        return C95104d9.A01(this, r2);
    }

    @Override // X.AnonymousClass5ZU
    public AbstractC115495Rt getKey() {
        return this.key;
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r2) {
        return C95104d9.A02(this, r2);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 plus(AnonymousClass5X4 r2) {
        return C95104d9.A03(this, r2);
    }
}
