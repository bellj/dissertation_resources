package X;

import java.util.Collections;
import java.util.Set;

/* renamed from: X.3Ge  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C64603Ge {
    public static final Set A02;
    public final C63753Cu A00;
    public final C17220qS A01;

    static {
        Integer[] numArr = new Integer[4];
        numArr[0] = 499;
        C12980iv.A1T(numArr, 400);
        C12990iw.A1V(numArr, 500);
        A02 = Collections.unmodifiableSet(C12970iu.A13(0, numArr, 3));
    }

    public C64603Ge(C63753Cu r1, C17220qS r2) {
        this.A01 = r2;
        this.A00 = r1;
    }
}
