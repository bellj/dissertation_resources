package X;

import android.view.View;
import android.widget.ImageView;
import com.whatsapp.R;

/* renamed from: X.3js  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public class C75453js extends AnonymousClass03U {
    public final View A00;
    public final ImageView A01;

    public /* synthetic */ C75453js(View view) {
        super(view);
        this.A00 = view.findViewById(R.id.shape_picker_subcategory_selected_indicator);
        this.A01 = C12970iu.A0L(view, R.id.shape_picker_subcategory_icon);
    }
}
