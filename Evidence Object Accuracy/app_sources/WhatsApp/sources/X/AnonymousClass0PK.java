package X;

/* renamed from: X.0PK  reason: invalid class name */
/* loaded from: classes.dex */
public final class AnonymousClass0PK {
    public AnonymousClass4WQ A00;
    public boolean A01;
    public boolean A02;
    public final Object A03;

    public AnonymousClass0PK(AbstractC115805Sz r2, Object obj) {
        this.A03 = obj;
        this.A00 = (AnonymousClass4WQ) r2.get();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AnonymousClass0PK.class != obj.getClass()) {
            return false;
        }
        return this.A03.equals(((AnonymousClass0PK) obj).A03);
    }

    public int hashCode() {
        return this.A03.hashCode();
    }
}
