package X;

/* renamed from: X.5Ei  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C112685Ei implements AnonymousClass5ZU {
    public static final C112715El A02 = new C112715El();
    public final Throwable A00;
    public final AbstractC115495Rt A01 = A02;

    public C112685Ei(Throwable th) {
        this.A00 = th;
    }

    @Override // X.AnonymousClass5X4
    public Object fold(Object obj, AnonymousClass5ZQ r3) {
        C16700pc.A0E(r3, 2);
        return r3.AJ5(obj, this);
    }

    @Override // X.AnonymousClass5ZU, X.AnonymousClass5X4
    public AnonymousClass5ZU get(AbstractC115495Rt r2) {
        return C95104d9.A01(this, r2);
    }

    @Override // X.AnonymousClass5ZU
    public AbstractC115495Rt getKey() {
        return this.A01;
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 minusKey(AbstractC115495Rt r2) {
        return C95104d9.A02(this, r2);
    }

    @Override // X.AnonymousClass5X4
    public AnonymousClass5X4 plus(AnonymousClass5X4 r2) {
        return C95104d9.A03(this, r2);
    }
}
