package X;

import com.google.protobuf.CodedOutputStream;
import java.io.IOException;

/* renamed from: X.22H  reason: invalid class name */
/* loaded from: classes2.dex */
public final class AnonymousClass22H extends AbstractC27091Fz implements AnonymousClass1G2 {
    public static final AnonymousClass22H A09;
    public static volatile AnonymousClass255 A0A;
    public int A00;
    public int A01;
    public int A02;
    public int A03;
    public int A04;
    public int A05;
    public String A06 = "";
    public String A07 = "";
    public String A08 = "";

    static {
        AnonymousClass22H r0 = new AnonymousClass22H();
        A09 = r0;
        r0.A0W();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // X.AbstractC27091Fz
    public final Object A0V(AnonymousClass25B r8, Object obj, Object obj2) {
        int A02;
        int i;
        switch (r8.ordinal()) {
            case 0:
                return A09;
            case 1:
                AbstractC462925h r9 = (AbstractC462925h) obj;
                AnonymousClass22H r10 = (AnonymousClass22H) obj2;
                int i2 = this.A00;
                boolean z = true;
                if ((i2 & 1) != 1) {
                    z = false;
                }
                String str = this.A07;
                int i3 = r10.A00;
                boolean z2 = true;
                if ((i3 & 1) != 1) {
                    z2 = false;
                }
                this.A07 = r9.Afy(str, r10.A07, z, z2);
                boolean z3 = false;
                if ((i2 & 2) == 2) {
                    z3 = true;
                }
                String str2 = this.A06;
                boolean z4 = false;
                if ((i3 & 2) == 2) {
                    z4 = true;
                }
                this.A06 = r9.Afy(str2, r10.A06, z3, z4);
                boolean z5 = false;
                if ((i2 & 4) == 4) {
                    z5 = true;
                }
                int i4 = this.A04;
                boolean z6 = false;
                if ((i3 & 4) == 4) {
                    z6 = true;
                }
                this.A04 = r9.Afp(i4, r10.A04, z5, z6);
                boolean z7 = false;
                if ((i2 & 8) == 8) {
                    z7 = true;
                }
                int i5 = this.A05;
                boolean z8 = false;
                if ((i3 & 8) == 8) {
                    z8 = true;
                }
                this.A05 = r9.Afp(i5, r10.A05, z7, z8);
                boolean z9 = false;
                if ((i2 & 16) == 16) {
                    z9 = true;
                }
                int i6 = this.A03;
                boolean z10 = false;
                if ((i3 & 16) == 16) {
                    z10 = true;
                }
                this.A03 = r9.Afp(i6, r10.A03, z9, z10);
                boolean z11 = false;
                if ((i2 & 32) == 32) {
                    z11 = true;
                }
                int i7 = this.A02;
                boolean z12 = false;
                if ((i3 & 32) == 32) {
                    z12 = true;
                }
                this.A02 = r9.Afp(i7, r10.A02, z11, z12);
                boolean z13 = false;
                if ((i2 & 64) == 64) {
                    z13 = true;
                }
                int i8 = this.A01;
                boolean z14 = false;
                if ((i3 & 64) == 64) {
                    z14 = true;
                }
                this.A01 = r9.Afp(i8, r10.A01, z13, z14);
                boolean z15 = false;
                if ((i2 & 128) == 128) {
                    z15 = true;
                }
                String str3 = this.A08;
                boolean z16 = false;
                if ((i3 & 128) == 128) {
                    z16 = true;
                }
                this.A08 = r9.Afy(str3, r10.A08, z15, z16);
                if (r9 == C463025i.A00) {
                    this.A00 = i2 | i3;
                }
                return this;
            case 2:
                AnonymousClass253 r92 = (AnonymousClass253) obj;
                while (true) {
                    try {
                        try {
                            int A03 = r92.A03();
                            if (A03 == 0) {
                                break;
                            } else if (A03 == 10) {
                                String A0A2 = r92.A0A();
                                this.A00 = 1 | this.A00;
                                this.A07 = A0A2;
                            } else if (A03 == 18) {
                                String A0A3 = r92.A0A();
                                this.A00 |= 2;
                                this.A06 = A0A3;
                            } else if (A03 == 24) {
                                this.A00 |= 4;
                                this.A04 = r92.A02();
                            } else if (A03 == 32) {
                                int A022 = r92.A02();
                                if (A022 == 0 || A022 == 1 || A022 == 2 || A022 == 3) {
                                    this.A00 |= 8;
                                    this.A05 = A022;
                                } else {
                                    super.A0X(4, A022);
                                }
                            } else if (A03 == 40) {
                                A02 = r92.A02();
                                if (AnonymousClass4BW.A00(A02) == null) {
                                    i = 5;
                                    super.A0X(i, A02);
                                } else {
                                    this.A00 |= 16;
                                    this.A03 = A02;
                                }
                            } else if (A03 == 48) {
                                this.A00 |= 32;
                                this.A02 = r92.A02();
                            } else if (A03 == 56) {
                                A02 = r92.A02();
                                if (A02 == 0 || A02 == 1 || A02 == 2) {
                                    this.A00 |= 64;
                                    this.A01 = A02;
                                } else {
                                    i = 7;
                                    super.A0X(i, A02);
                                }
                            } else if (A03 == 66) {
                                String A0A4 = r92.A0A();
                                this.A00 |= 128;
                                this.A08 = A0A4;
                            } else if (!A0a(r92, A03)) {
                                break;
                            }
                        } catch (IOException e) {
                            C28971Pt r1 = new C28971Pt(e.getMessage());
                            r1.unfinishedMessage = this;
                            throw new RuntimeException(r1);
                        }
                    } catch (C28971Pt e2) {
                        e2.unfinishedMessage = this;
                        throw new RuntimeException(e2);
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AnonymousClass22H();
            case 5:
                return new C81543uB();
            case 6:
                break;
            case 7:
                if (A0A == null) {
                    synchronized (AnonymousClass22H.class) {
                        if (A0A == null) {
                            A0A = new AnonymousClass255(A09);
                        }
                    }
                }
                return A0A;
            default:
                throw new UnsupportedOperationException();
        }
        return A09;
    }

    @Override // X.AnonymousClass1G1
    public int AGd() {
        int i = ((AbstractC27091Fz) this).A00;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if ((this.A00 & 1) == 1) {
            i2 = 0 + CodedOutputStream.A07(1, this.A07);
        }
        if ((this.A00 & 2) == 2) {
            i2 += CodedOutputStream.A07(2, this.A06);
        }
        int i3 = this.A00;
        if ((i3 & 4) == 4) {
            i2 += CodedOutputStream.A04(3, this.A04);
        }
        if ((i3 & 8) == 8) {
            i2 += CodedOutputStream.A02(4, this.A05);
        }
        if ((i3 & 16) == 16) {
            i2 += CodedOutputStream.A02(5, this.A03);
        }
        if ((i3 & 32) == 32) {
            i2 += CodedOutputStream.A04(6, this.A02);
        }
        if ((i3 & 64) == 64) {
            i2 += CodedOutputStream.A02(7, this.A01);
        }
        if ((i3 & 128) == 128) {
            i2 += CodedOutputStream.A07(8, this.A08);
        }
        int A00 = i2 + this.unknownFields.A00();
        ((AbstractC27091Fz) this).A00 = A00;
        return A00;
    }

    @Override // X.AnonymousClass1G1
    public void AgI(CodedOutputStream codedOutputStream) {
        if ((this.A00 & 1) == 1) {
            codedOutputStream.A0I(1, this.A07);
        }
        if ((this.A00 & 2) == 2) {
            codedOutputStream.A0I(2, this.A06);
        }
        if ((this.A00 & 4) == 4) {
            codedOutputStream.A0F(3, this.A04);
        }
        if ((this.A00 & 8) == 8) {
            codedOutputStream.A0E(4, this.A05);
        }
        if ((this.A00 & 16) == 16) {
            codedOutputStream.A0E(5, this.A03);
        }
        if ((this.A00 & 32) == 32) {
            codedOutputStream.A0F(6, this.A02);
        }
        if ((this.A00 & 64) == 64) {
            codedOutputStream.A0E(7, this.A01);
        }
        if ((this.A00 & 128) == 128) {
            codedOutputStream.A0I(8, this.A08);
        }
        this.unknownFields.A02(codedOutputStream);
    }
}
