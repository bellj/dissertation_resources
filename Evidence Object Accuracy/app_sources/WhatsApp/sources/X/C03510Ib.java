package X;

import android.graphics.Rect;
import android.graphics.RectF;
import android.text.Layout;
import android.text.Spanned;
import android.text.TextPaint;
import android.view.View;
import com.facebook.rendercore.text.RCTextView;

/* renamed from: X.0Ib  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C03510Ib extends AbstractC73473gI {
    public final C14260l7 A00;
    public final AnonymousClass28D A01;
    public final AnonymousClass28D A02;
    public final AbstractC14200l1 A03;
    public final boolean A04;

    @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
    public void updateDrawState(TextPaint textPaint) {
    }

    public C03510Ib(C14260l7 r1, AnonymousClass28D r2, AnonymousClass28D r3, AbstractC14200l1 r4, String str, String str2, boolean z) {
        super(str, str2);
        this.A03 = r4;
        this.A02 = r2;
        this.A01 = r3;
        this.A00 = r1;
        this.A04 = z;
    }

    public final AnonymousClass4RE A00(RCTextView rCTextView) {
        Spanned spanned = (Spanned) rCTextView.getText();
        Layout layout = rCTextView.getLayout();
        double spanStart = (double) spanned.getSpanStart(this);
        double spanEnd = (double) spanned.getSpanEnd(this);
        int i = (int) spanStart;
        double primaryHorizontal = (double) layout.getPrimaryHorizontal(i);
        double primaryHorizontal2 = (double) layout.getPrimaryHorizontal((int) spanEnd);
        int lineForOffset = layout.getLineForOffset(i);
        Rect rect = new Rect();
        layout.getLineBounds(lineForOffset, rect);
        RectF rectF = new RectF(rect);
        float paddingBottom = (float) (((double) rectF.left) + ((((double) rCTextView.getPaddingBottom()) + primaryHorizontal) - ((double) rCTextView.getScrollX())) + ((double) rCTextView.getLayoutTranslationX()));
        rectF.left = paddingBottom;
        rectF.right = (float) ((((double) paddingBottom) + primaryHorizontal2) - primaryHorizontal);
        double scrollY = (double) (((float) (rCTextView.getScrollY() + rCTextView.getPaddingTop())) + rCTextView.getLayoutTranslationY());
        rectF.top = (float) (((double) rectF.top) + scrollY);
        rectF.bottom = (float) (((double) rectF.bottom) + scrollY);
        rCTextView.getMatrix().mapRect(rectF);
        rectF.offset((float) rCTextView.getLeft(), (float) rCTextView.getTop());
        return new AnonymousClass4RE(rectF.left + (rectF.width() / 2.0f), rectF.top + (rectF.height() / 2.0f), rectF.width(), rectF.height());
    }

    @Override // android.text.style.ClickableSpan
    public void onClick(View view) {
        C14210l2 r4 = new C14210l2();
        r4.A05(this.A02, 0);
        if (!this.A04 || !(view instanceof RCTextView)) {
            r4.A05(this.A00, 1);
        } else {
            r4.A05(A00((RCTextView) view), 1);
        }
        AnonymousClass28D r3 = this.A01;
        AbstractC14200l1 r2 = this.A03;
        C28701Oq.A01(this.A00, r3, r4.A03(), r2);
    }
}
