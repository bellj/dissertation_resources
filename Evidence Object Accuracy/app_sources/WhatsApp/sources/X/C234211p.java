package X;

import com.whatsapp.util.Log;
import java.util.Iterator;

/* renamed from: X.11p  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C234211p {
    public C40891sV A00 = new C40891sV();
    public Runnable A01;
    public final C15570nT A02;
    public final C15450nH A03;
    public final AnonymousClass1GJ A04 = new AnonymousClass1GJ();
    public final AnonymousClass1GE A05 = new AnonymousClass1GE();
    public final AbstractC14440lR A06;

    public C234211p(C15570nT r2, C15450nH r3, AbstractC14440lR r4) {
        this.A02 = r2;
        this.A06 = r4;
        this.A03 = r3;
    }

    public final void A00() {
        C40891sV r4 = this.A00;
        if (r4.A02()) {
            boolean A01 = r4.A01();
            boolean z = r4.A02;
            StringBuilder sb = new StringBuilder("CriticalDataUploadManager/handleCompleted isSuccess=");
            sb.append(A01);
            sb.append(", hasTimedOut=");
            sb.append(z);
            Log.i(sb.toString());
            synchronized (this) {
                Runnable runnable = this.A01;
                if (runnable != null) {
                    this.A06.AaP(runnable);
                    this.A01 = null;
                }
                AnonymousClass1HL r1 = r4.A01;
                synchronized (r1) {
                    r1.A00 = 1;
                }
                AnonymousClass1HL r12 = r4.A00;
                synchronized (r12) {
                    r12.A00 = 1;
                }
                r4.A02 = false;
            }
            Iterator it = this.A04.A01().iterator();
            if (A01) {
                while (it.hasNext()) {
                    ((AnonymousClass1GL) it.next()).AWu();
                }
                return;
            }
            while (it.hasNext()) {
                ((AnonymousClass1GL) it.next()).AQ7(z);
            }
        }
    }

    public void A01(boolean z) {
        C40891sV r2 = this.A00;
        AnonymousClass1HL r1 = r2.A01;
        if ((!r1.A05() || !r2.A00.A05()) && C40891sV.A00(r1, z)) {
            StringBuilder sb = new StringBuilder("CriticalDataUploadManager/onHistorySyncComplete isSuccess=");
            sb.append(z);
            Log.i(sb.toString());
            A00();
        }
    }

    public synchronized void A02(boolean z) {
        C40891sV r1 = this.A00;
        if ((!r1.A01.A05() || !r1.A00.A05()) && C40891sV.A00(r1.A00, z)) {
            StringBuilder sb = new StringBuilder();
            sb.append("CriticalDataUploadManager/onAppStateSyncComplete isSuccess=");
            sb.append(z);
            Log.i(sb.toString());
            A00();
        }
    }
}
