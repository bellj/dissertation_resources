package X;

import android.graphics.drawable.Drawable;

/* renamed from: X.2iD  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C55322iD extends AnonymousClass03J {
    public final /* synthetic */ AnonymousClass3FR A00;
    public final /* synthetic */ AbstractC16130oV A01;

    public C55322iD(AnonymousClass3FR r1, AbstractC16130oV r2) {
        this.A00 = r1;
        this.A01 = r2;
    }

    @Override // X.AnonymousClass03J
    public void A01(Drawable drawable) {
        AbstractC13890kV r1 = ((AbstractC28551Oa) this.A00.A0B).A0a;
        if (r1 != null && (drawable instanceof C39631qF)) {
            r1.AfZ(this.A01);
        }
    }
}
