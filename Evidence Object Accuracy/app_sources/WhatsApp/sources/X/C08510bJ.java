package X;

import java.util.ArrayList;
import java.util.List;

/* renamed from: X.0bJ  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes.dex */
public class C08510bJ implements AbstractC12670iJ {
    public float A00;
    public float A01;
    public int A02 = -1;
    public C05460Pq A03 = null;
    public List A04 = new ArrayList();
    public boolean A05;
    public boolean A06 = true;
    public boolean A07 = false;
    public final /* synthetic */ C06540Ua A08;

    public C08510bJ(C08520bK r3, C06540Ua r4) {
        this.A08 = r4;
        if (r3 != null) {
            r3.A02(this);
            if (this.A05) {
                this.A04.get(-1);
                throw new NullPointerException("add");
            }
        }
    }

    @Override // X.AbstractC12670iJ
    public void A69(float f, float f2, float f3, float f4, float f5, boolean z, boolean z2) {
        this.A07 = true;
        this.A06 = false;
        C05460Pq r0 = this.A03;
        C06540Ua.A04(this, r0.A02, r0.A03, f, f2, f3, f4, f5, z, z2);
        this.A06 = true;
        this.A05 = false;
    }

    @Override // X.AbstractC12670iJ
    public void A8d(float f, float f2, float f3, float f4, float f5, float f6) {
        if (this.A06 || this.A07) {
            C05460Pq r1 = this.A03;
            r1.A00(f, f2);
            this.A04.add(r1);
            this.A07 = false;
        }
        this.A03 = new C05460Pq(this.A08, f5, f6, f5 - f3, f6 - f4);
        this.A05 = false;
    }

    @Override // X.AbstractC12670iJ
    public void AKS(float f, float f2) {
        C05460Pq r1 = this.A03;
        r1.A00(f, f2);
        this.A04.add(r1);
        C06540Ua r2 = this.A08;
        C05460Pq r12 = this.A03;
        this.A03 = new C05460Pq(r2, f, f2, f - r12.A02, f2 - r12.A03);
        this.A05 = false;
    }

    @Override // X.AbstractC12670iJ
    public void ALW(float f, float f2) {
        if (this.A05) {
            C05460Pq r1 = this.A03;
            List list = this.A04;
            r1.A01((C05460Pq) list.get(this.A02));
            list.set(this.A02, this.A03);
            this.A05 = false;
        }
        C05460Pq r12 = this.A03;
        if (r12 != null) {
            this.A04.add(r12);
        }
        this.A00 = f;
        this.A01 = f2;
        this.A03 = new C05460Pq(this.A08, f, f2, 0.0f, 0.0f);
        this.A02 = this.A04.size();
    }

    @Override // X.AbstractC12670iJ
    public void AZh(float f, float f2, float f3, float f4) {
        C05460Pq r1 = this.A03;
        r1.A00(f, f2);
        this.A04.add(r1);
        this.A03 = new C05460Pq(this.A08, f3, f4, f3 - f, f4 - f2);
        this.A05 = false;
    }

    @Override // X.AbstractC12670iJ
    public void close() {
        this.A04.add(this.A03);
        AKS(this.A00, this.A01);
        this.A05 = true;
    }
}
