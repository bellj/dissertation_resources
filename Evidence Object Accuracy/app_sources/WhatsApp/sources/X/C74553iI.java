package X;

import java.util.List;

/* renamed from: X.3iI  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes3.dex */
public final class C74553iI extends AnonymousClass0Q0 {
    public final List A00;
    public final List A01;

    public C74553iI(List list, List list2) {
        this.A01 = list;
        this.A00 = list2;
    }

    @Override // X.AnonymousClass0Q0
    public int A00() {
        return this.A00.size();
    }

    @Override // X.AnonymousClass0Q0
    public int A01() {
        return this.A01.size();
    }

    @Override // X.AnonymousClass0Q0
    public boolean A03(int i, int i2) {
        return C16700pc.A0O(((C460124c) this.A01.get(i)).A06, ((C460124c) this.A00.get(i2)).A06);
    }

    @Override // X.AnonymousClass0Q0
    public boolean A04(int i, int i2) {
        return C16700pc.A0O(this.A01.get(i), this.A00.get(i2));
    }
}
