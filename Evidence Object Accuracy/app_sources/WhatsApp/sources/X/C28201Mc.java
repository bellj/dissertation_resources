package X;

import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: X.1Mc  reason: invalid class name and case insensitive filesystem */
/* loaded from: classes2.dex */
public class C28201Mc {
    public static final List A02 = Collections.unmodifiableList(Arrays.asList("FBAN", "FBAV", "FBLC", "FBSV"));
    public static final List A03 = Collections.unmodifiableList(Arrays.asList("FBBR", "FBBD", "FBBV", "FBCA", "FBPN", "FBDM"));
    public String A00;
    public final Map A01;

    public C28201Mc() {
        HashMap hashMap = new HashMap();
        this.A01 = hashMap;
        hashMap.put("FBBR", Build.BOARD);
        hashMap.put("FBBD", Build.BRAND);
        hashMap.put("FBDM", Resources.getSystem().getDisplayMetrics().toString());
        hashMap.put("FBSV", Build.VERSION.RELEASE);
        hashMap.put("FBCA", String.format(null, "%s:%s", Build.CPU_ABI, Build.CPU_ABI2));
    }

    public String A00(String str) {
        String str2;
        if (TextUtils.isEmpty(str) || str == null) {
            return "null";
        }
        int length = str.length();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '&') {
                str2 = "&amp;";
            } else if (charAt < ' ' || charAt > '~') {
                sb.append("&#");
                sb.append(Integer.toString(charAt));
                str2 = ";";
            } else {
                sb.append(charAt);
            }
            sb.append(str2);
        }
        return sb.toString().replace("/", "-").replace(";", "-");
    }
}
